<?php
/**
 * @var yii\web\View $this
 * @var CashOrderFlows $_controlModel
 * @var CashOrderFlows[] $models
 * @var yii\bootstrap4\ActiveForm $form
 * @var int $foreign
 */

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\project\Project;
use frontend\modules\analytics\assets\credits\CashFormAsset;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\cash\models\CashContractorType;
use frontend\widgets\TableConfigWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\rbac\UserRole;
use frontend\widgets\ContractorDropdown;
use frontend\themes\kub\modules\cash\widgets\InvoiceListInputWidget;
use frontend\modules\cash\controllers\OrderController;
use common\models\project\ProjectSearch;

CashFormAsset::register($this);

///////////////////////////////
$model = $_controlModel;
$VISIBLE_MODELS_COUNT = 2;
///////////////////////////////

$user = Yii::$app->user->identity;
$company = $user->company;
$userConfig = $user->config;

if (empty($multiWallet)) {
    $multiWallet = false;
}
if (empty($isPlan)) {
    $isPlan = false;
}

$userCashboxList = Yii::$app->user->identity->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $contractorListOptions = null;
    $contractorType = Contractor::TYPE_SELLER;
    $expenditureItems = InvoiceExpenditureItem::getSelect2Data(
        Yii::$app->user->identity->company->id,
        [],
        //Yii::$app->user->can(UserRole::ROLE_CHIEF)
    );
    $contractorData = array_merge(
        [], //['add-modal-contractor' => Icon::PLUS . ' <span class="bold">Добавить поставщика</span> '],
        ArrayHelper::map(
            CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
            ->all(), 'name', 'text'));
} else {
    $contractorListOptions = null;
    $contractorType = Contractor::TYPE_CUSTOMER;
    $incomeItems = InvoiceIncomeItem::getSelect2Data(
        Yii::$app->user->identity->company->id,
        [],
        //Yii::$app->user->can(\frontend\rbac\UserRole::ROLE_CHIEF)
    );
    $contractorData = array_merge(
        [], //['add-modal-contractor' => Icon::PLUS . ' <span class="bold">Добавить покупателя</span> '],
        ArrayHelper::map(
            CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->all(), 'name', 'text'));
}

// Project
$hasProject = Yii::$app->user->identity->menuItem->project_item;
$projects = ($hasProject) ? ProjectSearch::getSelect2Data(null) : [];
// SalePoint
$hasSalePoint = SalePoint::find()->where(['company_id' => $model->company->id])->exists();
$salePoints = ($hasSalePoint) ? SalePoint::getSelect2Data() : [];
// Industry
$hasCompanyIndustry = CompanyIndustry::find()->where(['company_id' => $model->company->id])->exists();
$companyIndustries = ($hasCompanyIndustry) ? CompanyIndustry::getSelect2Data() : [];

$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Редактировать')) . ($multiWallet ? (($isPlan ? ' плановые' : '') . ' операции') : ' операции по кассе');
$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/img/svg/svgSprite.svg#calendar"></use></svg></div>';

$tabConfigClass = [
    'recognition_date' => 'col_order_many_create_recognition_date' . ($userConfig->order_many_create_recognition_date ? '' : ' hidden'),
    'invoices_list' => 'col_order_many_create_invoices_list' . ($userConfig->order_many_create_invoices_list ? '' : ' hidden'),
    'project' => 'col_order_many_create_project' . ($userConfig->order_many_create_project ? '' : ' hidden'),
    'sale_point' => 'col_order_many_create_sale_point' . ($userConfig->order_many_create_sale_point ? '' : ' hidden'),
    'industry' => 'col_order_many_create_direction' . ($userConfig->order_many_create_direction ? '' : ' hidden'),
];

$tabConfigWidth = [
    'amount' => 'width:107px',
    'date' => 'width:127px',
    'contractor' => 'width:97px',
    'article' => 'width:97px',
    'invoices_list' => 'width:97px',
    'project' => 'width:97px',
    'sale_point' => 'width:97px',
    'industry' => 'width:97px',
    '_btn' => 'width:15px'
];

$multiWallet = $multiWallet ?? false;
?>

<?php Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => '.currency_type_link',
]); ?>

<?= $this->render('_change_one_many_create', [
    'create_type' => OrderController::TYPE_CREATE_MANY,
    'flow_type' => $model->flow_type,
    'cashbox_id' => $model->cashbox_id
]) ?>

<?php $form = ActiveForm::begin([
    'id' => 'cash-order-form',
    'action' => empty($action) ? '' : $action,
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'data-foreign' => 0,
        'class' => 'cash_movement_form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= $this->render('_change_one_many_create', [
    'create_type' => OrderController::TYPE_CREATE_MANY,
    'flow_type' => $model->flow_type,
    'cashbox_id' => $model->cashbox_id
]) ?>

    <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>
    <?php endif ?>

    <div class="row" style="margin-bottom: -22px">
        <div class="col-6">
            <div class="form-group">
                <label>&nbsp;</label>
                <div class="form-filter d-flex flex-wrap">
                    <?= Html::radio($input_name ?? null, $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME, [
                        'class' => 'flow-type-toggle-input',
                        'value' => CashFlowsBase::FLOW_TYPE_INCOME,
                        'label' => '<span class="radio-txt-bold">Приход</span>',
                        'labelOptions' => [
                            'class' => 'mb-3 mr-4 mt-2  ajax-modal-btn',
                            'data' => [
                                'title' => 'Добавить операцию по кассе',
                                'url' => Url::current([
                                    'many-create',
                                    'type' => CashFlowsBase::FLOW_TYPE_INCOME,
                                    'internal' => null,
                                ]),
                            ],
                        ],
                    ]) ?>
                    <?= Html::radio($input_name ?? null, $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE, [
                        'class' => 'flow-type-toggle-input',
                        'value' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                        'label' => '<span class="radio-txt-bold">Расход</span>',
                        'labelOptions' => [
                            'class' => 'mb-3 mr-4 mt-2  ajax-modal-btn',
                            'data' => [
                                'title' => 'Добавить операцию по кассе',
                                'url' => Url::current([
                                    'many-create',
                                    'type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                                    'internal' => null,
                                ]),
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-6">
            <?php if ($multiWallet): ?>
                <?= $this->render('/bank/_account_select_multi', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form
                ]) ?>
            <?php elseif (count($userCashboxList) == 1) : ?>
                <?= $form->field($model, 'cashbox_id')->hiddenInput(['value' => key($userCashboxList)])->label(false) ?>
            <?php else : ?>
                <?= $this->render('_cashbox_select', [
                    'company' => $company,
                    'model' => $model,
                    'form' => $form,
                    'foreign' => $foreign,
                    'userCashboxList' => $userCashboxList,
                ]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="row">

        <div class="col-12">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'order_many_create_recognition_date'],
                    ['attribute' => 'order_many_create_invoices_list'],
                    ['attribute' => 'order_many_create_project'],
                    ['attribute' => 'order_many_create_sale_point'],
                    ['attribute' => 'order_many_create_direction']
                ]
            ]); ?>
        </div>

        <div class="col-12">
            <table class="table-order-many-create">
                <thead>
                    <tr>
                        <th><!-- copy --></th>
                        <th>Сумма</th>
                        <th width="127">Дата</th>
                        <th width="127" class="<?= $tabConfigClass['recognition_date'] ?>">Дата признания <?=($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) ? 'дохода' : 'расхода' ?></th>
                        <th><?=($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) ? 'Покупатель' : 'Поставщик' ?></th>
                        <th><?=($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) ? 'Статья прихода' : 'Статья расхода' ?></th>
                        <th width="50%">Назначение</th>
                        <th class="<?= $tabConfigClass['invoices_list'] ?>">Опл. счета</th>
                        <th class="<?= $tabConfigClass['project'] ?>">Проект</th>
                        <th class="<?= $tabConfigClass['sale_point'] ?>">Точка продаж</th>
                        <th class="<?= $tabConfigClass['industry'] ?>">Направ&shy;ление</th>
                        <th><!-- delete --></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($models as $k => $m): /** @var $m CashOrderFlows */ ?>
                        <tr class="<?=($k > $VISIBLE_MODELS_COUNT ? 'hidden':'')?>">
                            <!-- _copy -->
                            <td>
                                <?= $form->field($m, 'flow_type')->hiddenInput()->label(false) ?>
                                <?= $form->field($m, 'number')->hiddenInput()->label(false) ?>
                                <div style="<?= $tabConfigWidth['_btn'] ?>">
                                    <button class="copy-many-create-order-position button-clr" type="button" title="Копировать из строки выше">
                                        <svg class="table-count-icon svg-icon"><use xlink:href="/img/svg/svgSprite.svg#copy-link"></use></svg>
                                    </button>
                                </div>
                            </td>
                            <!-- Amount -->
                            <td data-attr="amount">
                                <?= $form->field($m, 'amount')->textInput([
                                    'class' => 'form-control js_input_to_money_format',
                                    'style' => $tabConfigWidth['amount']
                                ])->label(false); ?>
                            </td>
                            <!-- Date -->
                            <td data-attr="date">
                                <?= $form->field($m, 'date', [
                                    'inputTemplate' => $inputCalendarTemplate,
                                    'options' => [
                                        'class' => 'form-group',
                                        'style' => $tabConfigWidth['date']
                                    ],
                                    'labelOptions' => [
                                        'class' => 'label repeated-date-start'
                                    ]
                                ])->textInput([
                                    'class' => 'form-control __date-picker date-picker-master',
                                    'value' => ($m->date && ($d = date_create($m->date))) ? $d->format('d.m.Y') : $m->date,
                                    'data' => [
                                        'date-viewmode' => 'years',
                                    ],
                                ])->label(false); ?>
                            </td>
                            <!-- Recognition Date -->
                            <td data-attr="recognition_date" class="<?= $tabConfigClass['recognition_date'] ?>">
                                <?= $form->field($m, 'recognition_date', [
                                    'inputTemplate' => $inputCalendarTemplate,
                                    'options' => [
                                        'class' => 'form-group',
                                        'style' => $tabConfigWidth['date']
                                    ],
                                    'labelOptions' => [
                                        'class' => 'label repeated-date-start'
                                    ]
                                ])->textInput([
                                    'class' => 'form-control __date-picker date-picker-slave',
                                    'value' => ($m->recognition_date && ($d = date_create($m->recognition_date))) ? $d->format('d.m.Y') : $m->recognition_date,
                                    'data' => [
                                        'date-viewmode' => 'years',
                                    ],
                                ])->label(false); ?>
                            </td>
                            <!-- Contractor -->
                            <td data-attr="contractor">
                                <?= $form->field($m, 'contractorInput', [
                                    'options' => ['style' => $tabConfigWidth['contractor']],
                                ])->dropdownList($contractorData, [
                                    'prompt' => '',
                                    'class' => 'form-control contractor-items-depend-in-table-row ' . ($m->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? 'seller' : 'customer'),
                                    'data-items-url' => Url::to(['/cash/default/items-full', 'cid' => '_cid_', 'flowType' => $m->flow_type])
                                ])->label(false); ?>
                            </td>
                            <!-- Income/Expenditure Item -->
                            <td data-attr="article">
                                <?php if ($m->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) : ?>
                                    <?= $form->field($m, 'expenditure_item_id', [
                                        'options' => ['style' => $tabConfigWidth['article']],
                                    ])->dropdownList($expenditureItems['list'], [
                                        'prompt' => '',
                                        'class' => 'form-control flow-expense-items'
                                    ])->label(false); ?>
                                <?php else: ?>
                                    <?= $form->field($m, 'income_item_id', [
                                        'options' => ['style' => $tabConfigWidth['article']],
                                    ])->dropdownList($incomeItems['list'], [
                                        'prompt' => '',
                                        'class' => 'form-control flow-income-items',
                                    ])->label(false); ?>
                                <?php endif; ?>
                            </td>
                            <!-- Description -->
                            <td data-attr="description">
                                <?= $form->field($m, 'description')->textInput()->label(false); ?>
                            </td>
                            <!-- Invoices list -->
                            <td data-attr="invoices_list" class="<?= $tabConfigClass['invoices_list'] ?>">

                                <?= $form->field($m, 'invoices_list', [
                                    'options' => [
                                        'class' => 'invoice_list_widget_invoices',
                                        'style' => $tabConfigWidth['invoices_list'],
                                    ],
                                ])->dropdownList([], [
                                    'multiple' => true,
                                    'class' => 'invoices-items-depend-in-table-row',
                                    'style' => ['height' => 0]
                                ])->label(false); ?>
                            </td>
                            <!-- Project -->
                            <td data-attr="project" class="<?= $tabConfigClass['project'] ?>">
                                <?= $form->field($m, 'project_id', [
                                    'options' => ['style' => $tabConfigWidth['project']],
                                ])->dropdownList($projects, [ // ajax load
                                    'prompt' => '',
                                    'class' => 'form-control',
                                    'disabled' => !$hasProject
                                ])->label(false); ?>
                            </td>
                            <!-- Sale point -->
                            <td data-attr="sale_point" class="<?= $tabConfigClass['sale_point'] ?>">
                                <?= $form->field($m, 'sale_point_id', [
                                    'options' => ['style' => $tabConfigWidth['sale_point']],
                                ])->dropdownList($salePoints, [ // ajax load
                                    'prompt' => '',
                                    'class' => 'form-control',
                                    'disabled' => !$hasSalePoint
                                ])->label(false); ?>
                            </td>
                            <!-- Direction -->
                            <td data-attr="industry" class="<?= $tabConfigClass['industry'] ?>">
                                <?= $form->field($m, 'industry_id', [
                                    'options' => ['style' => $tabConfigWidth['industry']],
                                ])->dropdownList($companyIndustries, [ // ajax load
                                    'prompt' => '',
                                    'class' => 'form-control',
                                    'disabled' => !$hasCompanyIndustry
                                ])->label(false); ?>
                            </td>
                            <!-- _delete -->
                            <td>
                                <div style="<?= $tabConfigWidth['_btn'] ?>">
                                    <button class="delete-many-create-order-position button-clr" type="button" title="Удалить">
                                        <svg class="table-count-icon svg-icon"><use xlink:href="/img/svg/svgSprite.svg#circle-close"></use></svg>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="mb-3" style="padding-left: 19px">
                <span id="add-many-create-order-position" class="button-regular button-hover-content-red" style="<?= $tabConfigWidth['amount'] ?>">
                    <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
                    <span>Добавить</span>
                </span>
            </div>

        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">

    // title
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#update-movement-pjax").closest(".modal-content");
        if (modalContent) {
            $(modalContent).parent().addClass('modal-dialog-extra-large');
            modalContent.find(".modal-title").html("<?= $header ?>");
            ManyCreateModal.render();
            // REFRESH_UNIFORMS
            //refreshUniform();
            //refreshDatepicker();
            // REFRESH DROPDOWNS
            //modalContent.find("button[data-toggle=dropdown]").click();
        }
    <?php endif ?>

</script>

<?php Pjax::end(); ?>
