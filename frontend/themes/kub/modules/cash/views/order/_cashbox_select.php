<?php

use common\models\currency\Currency;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/** @var array $userCashboxList */
/** @var $foreign int */

if (!isset($userCashboxArray)) {
    $userCashboxArray = Yii::$app->user->identity->getCashboxes()->orderBy([
        'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])->indexBy('id')->all();
}

if (!isset($userCashboxList)) {
    $userCashboxList = ArrayHelper::map($userCashboxArray, 'id', 'name');
}

$cashboxBalance = $company->getCashOrderFlows()
    ->select([
        'sum' => 'SUM( IF([[flow_type]] = 1, [[amount]], -[[amount]]) )',
        'cashbox_id',
    ])
    ->groupBy('cashbox_id')
    ->indexBy('cashbox_id')
    ->column() +
    $company->getCashOrderForeignCurrencyFlows()
    ->select([
        'sum' => 'SUM( IF([[flow_type]] = 1, [[amount]], -[[amount]]) )',
        'cashbox_id',
    ])
    ->groupBy('cashbox_id')
    ->indexBy('cashbox_id')
    ->column();

$userCashboxOptions = [];
foreach ($userCashboxArray as $cashbox) {
    $userCashboxOptions[$cashbox->id] = [
        'data-balance' => ArrayHelper::getValue($cashboxBalance, $cashbox->id, 0),
        'data-foreign' => $cashbox->getIsForeign() ? 1 : 0,
        'data-currency' => $cashbox->currency->name,
        'data-symbol' => ArrayHelper::getValue(Currency::$currencySymbols, $cashbox->currency->name),
        'data-url' => Url::current([
            0 => 'create',
            'type' => $model->flow_type,
            'id' => $cashbox->id,
        ])
    ];
}

$jsTemplate = new yii\web\JsExpression(<<<JS
    function(data) {
        if (data.element) {
            const cashbox_name = data.text;
            const balance = number_format(Number(data.element.dataset.balance) / 100, 2, ',', ' ');
            const currency_symbol = data.element.dataset.symbol;
            return '<div class="row">' +
                     '<div class="col-6 pr-1 no-overflow font-16">' + cashbox_name + '</div>' +
                     '<div class="col-6 pl-1 no-overflow font-16 text-right">' + balance + ' ' + currency_symbol + '</div>' +
                   '</div>';
        }

        return data.text;
    }
JS);
?>

<?= $form->field($model, 'cashbox_id')->widget(Select2::class, [
    'data' => $userCashboxList,
    'options' => [
        'class' => 'flow_form_rs_select',
        'options' => $userCashboxOptions,
        'placeholder' => '',
        'data' => [
            'prefix' => CashContractorType::ORDER_TEXT . '.',
            'target' => '#' . Html::getInputId($model, 'contractorInput'),
            'foreign' => $foreign,
        ],
    ],
    'pluginOptions' => [
        'width' => '100%',
        'templateResult' => $jsTemplate,
        'templateSelection' => $jsTemplate,
        'escapeMarkup' => new yii\web\JsExpression('function(markup) { return markup; }'),
    ],
]); ?>

<div class="hidden">
    <?= Html::a('', Url::current(), [
        'class' => 'flow_form_rs_toggle_link currency_type_link',
    ]) ?>
</div>
