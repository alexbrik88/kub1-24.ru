<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.02.2019
 * Time: 19:23
 */

use common\models\cash\CashBankFlows;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceExpenditureItem;
use common\models\project\ProjectSearch;
use common\models\TaxKbk;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\themes\kub\helpers\Icon;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\components\helpers\Html;
use common\components\helpers\ArrayHelper;
use frontend\modules\cash\models\CashContractorType;
use common\models\Contractor;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use kartik\select2\Select2;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Url;
use common\components\date\DateHelper;

/* @var $this yii\web\View
 * @var $model PlanCashFlows
 * @var $company Company
 */

$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Редактировать')) . ' плановую операцию';
//$accounArray = $company->getCheckingAccountants()
//    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();
//$rsArray = \yii\helpers\ArrayHelper::map($accounArray, 'id', function ($data) {
//    return $data->rs . ', ' . $data->name;
//});
//$userCashboxList = Yii::$app->user->identity->getCashboxes()
//    ->select(['name'])
//    ->andWhere(['is_closed' => false])
//    ->orderBy([
//        'is_main' => SORT_DESC,
//        'name' => SORT_ASC,
//    ])
//    ->indexBy('id')
//    ->column();
//$emoneyData = $company->getEmoneys()->select('name')->orderBy([
//    'is_closed' => SORT_ASC,
//    'is_main' => SORT_DESC,
//    'name' => SORT_ASC,
//])->indexBy('id')->column();

$flowTypes = $flowTypes ?? PlanCashFlows::$flowTypes;
if (!$model->isNewRecord) {
    $flowTypes = [$model->flow_type => PlanCashFlows::$flowTypes[$model->flow_type]];
}

// AJAX LOAD LIST
$sellerArray = [];
$customerArray = [];

$sellerArray = array_merge($sellerArray, \yii\helpers\ArrayHelper::map(CashContractorType::find()
    ->andWhere(['!=', 'name', CashContractorType::BALANCE_TEXT])->all(), 'name', 'text'));
$customerArray = array_merge($customerArray, ArrayHelper::map(CashContractorType::find()
    ->all(), 'name', 'text'));

if ($model->contractor !== null) {
    if ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME) {
        $customerArray[$model->contractor->id] = $model->contractor->nameWithType;
    } elseif ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE) {
        $sellerArray[$model->contractor->id] = $model->contractor->nameWithType;
    }
}

$cashContractorArray = ArrayHelper::map(CashContractorType::find()->all(), 'name', 'text');

$income = 'income' . ($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? '' : ' hidden');

if ($model->first_flow_id) {
    $model->is_repeated = true;
}

$hasProject = Yii::$app->user->identity->menuItem->project_item;
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
$hasCompanyIndustry = \common\models\company\CompanyIndustry::find()->where(['company_id' => $company->id])->exists();

?>
<?php Pjax::begin([
    'id' => 'plan_item-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 10000,
    'scrollTo' => false,
]); ?>
<?php $form = ActiveForm::begin([
    'action' => $model->isNewRecord ?
        Url::to($createUrl ?? ['/cash/plan/create']) :
        Url::to($updateUrl ?? ['/cash/plan/update', 'id' => $model->id]),
    'id' => 'js-plan_cash_flow_form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data' => [
            'type-income' => CashFlowsBase::FLOW_TYPE_INCOME,
            'type-expense' => CashFlowsBase::FLOW_TYPE_EXPENSE,
        ],
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'flow_type')->radioList($flowTypes, [
            'class' => 'd-flex flex-wrap',
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                return \yii\helpers\Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'mb-2 mr-4 mt-2',
                    ],
                ]);
            },
        ])->label('Тип'); ?>
    </div>
    <div class="col-6">
        <?= $this->render('_plan_account_select_multi', [
            'company' => $company,
            'model' => $model,
            'form' => $form
        ]) ?>
    </div>
</div>
<?php /*
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'payment_type')->radioList(PlanCashFlows::$paymentTypes, [
            'class' => 'd-flex flex-wrap',
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                return \yii\helpers\Html::radio($name, $checked, [
                    'class' => 'payment-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ],
                ]);
            },
        ])->label('Тип оплаты'); ?>
    </div>
    <div class="col-6">
        <div class="payment-type-toggle" data-id="<?= PlanCashFlows::PAYMENT_TYPE_BANK ?>">
            <?= $form->field($model, 'checking_accountant_id')->widget(Select2::class, [
                'hideSearch' => true,
                'data' => $rsArray,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->payment_type != PlanCashFlows::PAYMENT_TYPE_BANK
                ],
                'pluginOptions' => [
                    //'allowClear' => true,
                    'width' => '100%',
                    'escapeMarkup' => new yii\web\JsExpression('function(markup) {
                        return markup;
                    }'),
                    'templateResult' => new yii\web\JsExpression('function(data) {
                        console.log(data);
                        return data.text;
                    }'),
                ],
            ]); ?>
        </div>
        <div class="payment-type-toggle hidden" data-id="<?= PlanCashFlows::PAYMENT_TYPE_ORDER ?>">
            <?= $form->field($model, 'cashbox_id')->widget(Select2::class, [
                'hideSearch' => true,
                'data' => $userCashboxList,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->payment_type != PlanCashFlows::PAYMENT_TYPE_ORDER,
                    'data' => [
                        'prefix' => CashContractorType::ORDER_TEXT . '.',
                        'target' => '#' . \yii\helpers\Html::getInputId($model, 'contractorInput'),
                    ],
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <div class="payment-type-toggle hidden" data-id="<?= PlanCashFlows::PAYMENT_TYPE_EMONEY ?>">
            <?= $form->field($model, 'emoney_id')->widget(Select2::classname(), [
                'hideSearch' => true,
                'data' => $emoneyData,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->payment_type != PlanCashFlows::PAYMENT_TYPE_EMONEY,
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        </div>
    </div>
</div>
*/ ?>

<div class="flow-type-toggle <?= $expense ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_SELLER,
            'staticData' => $sellerArray,
            'options' => [
                'id' => 'seller_contractor_id',
                'class' => 'contractor-items-depend seller',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Поставщик'); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::classname(), [
            'loadAssets' => false,
            'options' => [
                'class' => 'flow-expense-items',
                'prompt' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
            ],
            'pluginOptions' => [
                'width' => '100%',
                'placeholder' => '',
            ]
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-expenditure_item_id',
        ]) ?>

    </div>
</div>

<div class="flow-type-toggle <?= $income ?> row">
    <div class="col-6">
        <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $customerArray,
            'options' => [
                'id' => 'customer_contractor_id',
                'class' => 'contractor-items-depend customer cash_contractor_id_select',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Покупатель'); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::classname(), [
            'loadAssets' => false,
            'income' => true,
            'options' => [
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
            ],
            'pluginOptions' => [
                'width' => '100%',
                'placeholder' => '',
            ]
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-income_item_id',
            'type' => 'income',
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'amount')->textInput([
            'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : null,
            'class' => 'form-control js_input_to_money_format' . ($model->amount ? ' edited' : ''),
        ])->label($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Сумма, RUB' : 'Сумма, RUB'); ?>
    </div>
    <div class="col-6">
        <?php if (!$model->isNewRecord && $model->date != $model->first_date): ?>
            <div style="max-width: 170px;">
                <div class="form-group">
                    <label class="label">Дата по первонач. плану</label>
                    <input class="form-control" disabled style="color:#e30611" value="<?= DateHelper::format($model->first_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>">
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="row">
            <div class="col-6">
                <div style="max-width: 170px">
                    <?= $form->field($model, 'date', [
                        'options' => [
                            'class' => 'form-group'
                        ],
                    ])->textInput([
                        'value' => DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        'class' => 'form-control date-picker ico',
                    ])->label($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Дата плановой оплаты' : 'Дата плановой оплаты'); ?>
                </div>
            </div>
            <div class="col-6 repeated-fields hidden">
                <div style="max-width: 170px">
                    <?= $form->field($model, 'planEndDate', [
                        'options' => [
                            'class' => 'form-group',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker ico',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'is_repeated', [
            'options' => [
                'class' => 'form-group',
                'style' => 'padding-top: 36px'
            ],
        ])->checkbox(['class' => '', 'disabled' => ($disableRepeatedFlow ?? false) || !$model->isNewRecord && ($model->is_repeated || $model->first_flow_id)])
        ->label('Повторяющаяся плановая операция'); ?>
    </div>
</div>

<div class="row">
    <div class="col-6 repeated-fields hidden">
        <?= $form->field($model, 'period', [
            'template' => "{label}\n{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'style' => 'position: relative;transition: 0.2s ease all;color: #999;',
            ],
        ])->widget(Select2::classname(), [
            'data' => PlanCashFlows::$periods,
            'options' => [
                'id' => 'period_id',
                'class' => 'period-items-depend',
            ],
            'pluginOptions' => ['width' => '100%'],
        ]); ?>
    </div>
</div>

<?= $form->field($model, 'description')->textarea([
    'style' => 'resize: none;',
    'rows' => '2',
]); ?>

<?= $form->field($model, 'updateRepeatedType')->hiddenInput()->label(false); ?>

<?php if ($hasProject || $hasSalePoint || $hasCompanyIndustry): ?>
    <div class="row">
        <?php if ($hasProject): ?>
            <div class="col-6">
                <?= $form->field($model, 'project_id')
                    ->widget(Select2::class, [
                        'data' => ProjectSearch::getSelect2Data($model->project_id),
                        'options' => [
                            'placeholder' => 'Без проекта',
                            'disabled' => !$model->canChangeProject(),
                            'class' => 'flow-project-id'
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Проект' . \yii\helpers\Html::tag('span', Icon::QUESTION, ['class' => 'tooltip-modal-help', 'data-tooltip-content' => '#tooltip_modal_help_project'])); ?>
            </div>
        <?php endif; ?>
        <?php if ($hasCompanyIndustry): ?>
            <div class="<?= ($hasSalePoint) ? 'col-3' : 'col-6' ?>">
                <?= $form->field($model, 'industry_id')
                    ->widget(Select2::class, [
                        'data' => CompanyIndustry::getSelect2Data(),
                        'options' => [
                            'placeholder' => 'Без направления',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Направление'); ?>
            </div>
        <?php endif; ?>
        <?php if ($hasSalePoint): ?>
            <div class="<?= ($hasCompanyIndustry) ? 'col-3' : 'col-6' ?>">
                <?= $form->field($model, 'sale_point_id')
                    ->widget(Select2::class, [
                        'data' => SalePoint::getSelect2Data(),
                        'options' => [
                            'placeholder' => 'Без точки продаж',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ])->label('Точка продаж'); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<div class="mt-3 d-flex justify-content-between">
    <?= \yii\helpers\Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php $form->end(); ?>
<?php Pjax::end(); ?>

<?php $this->registerJs('
    var $cashContractorArray = ' . json_encode($cashContractorArray) . ';

    $(document).on("click", "#show-plancashflowsdate", function(e) {
        $("#plancashflows-date").datepicker("show");
    });

    $(document).on("change", "form#js-plan_cash_flow_form .payment-type-toggle-input[type=radio]", function(e) {
        var form = this.form;
        var value = $("input.payment-type-toggle-input:checked", form).val();
        if ($(".payment-type-toggle", form).length > 0) {
            $(".payment-type-toggle", form).addClass("hidden").find("select").prop("disabled", true);
            $(".payment-type-toggle", form).filter("[data-id=\'"+value+"\']").removeClass("hidden").find("select").prop("disabled", false);
        }
    });

    $(document).on("change", "form#js-plan_cash_flow_form input.flow-type-toggle-input[type=radio]", function(e) {
        var form = this.form;
        var value = $("input.flow-type-toggle-input:checked", form).val();
        var isRepeated = $("#plancashflows-is_repeated", form).is(":checked");
        var $dateLabel = $(".field-plancashflows-date label", form);
        var $dateEndLabel = $(".field-plancashflows-planenddate label", form);

        if ($(".flow-type-toggle", form).length > 0) {
            $(".has-error", form).removeClass("has-error");
            $(".help-block.help-block-error", form).html("");
            $(".flow-type-toggle", form).addClass("hidden");
            $(".flow-type-toggle input, .flow-type-toggle select", form).prop("disabled", true);
            if (value == $(form).data("type-expense")) {
                $(".flow-type-toggle.expense", form).removeClass("hidden");
                $(".flow-type-toggle.expense input, .flow-type-toggle.expense select", form).prop("disabled", false);
                $(".field-plancashflows-amount label", form).text("Сумма планового расхода");
                $dateEndLabel.text("Плановая дата последнего расхода");
                if (isRepeated) {
                    $dateLabel.text("Плановая дата первого расхода");
                } else {
                    $dateLabel.text("Дата планового расхода");
                }
            } else if (value == $(form).data("type-income")) {
                $(".flow-type-toggle.income", form).removeClass("hidden");
                $(".flow-type-toggle.income input, .flow-type-toggle.income select", form).prop("disabled", false);
                $(".field-plancashflows-amount label", form).text("Сумма планового прихода");
                $dateEndLabel.text("Плановая дата последнего прихода");
                if (isRepeated) {
                    $dateLabel.text("Плановая дата первого прихода");
                } else {
                    $dateLabel.text("Дата планового прихода");
                }
            }
        }
    });
    $(document).on("change", "#plancashflows-is_repeated", function(e) {
        var form = this.form;
        var $flowType = $("input.flow-type-toggle-input[type=radio]:checked", form).val();
        var $flowTypeText = "";
        var $repeatedFields = $(".repeated-fields", form);
        var $dateStartLabel = $(".field-plancashflows-date label", form);

        if ($flowType == $(form).data("type-expense")) {
            $flowTypeText = "расхода";
        } else if ($flowType == $(form).data("type-income")) {
            $flowTypeText = "прихода";
        }
        if ($(this).is(":checked")) {
            $dateStartLabel.text("Плановая дата первого " + $flowTypeText);
            $repeatedFields.removeClass("hidden");
        } else {
            $dateStartLabel.text("Дата планового " + $flowTypeText);
            $repeatedFields.addClass("hidden");
        }
    });

    $("#plancashflows-date, #plancashflows-planenddate").change(function (e) {
        if ($(this).val() !== "" && !$(this).hasClass("edited")) {
            $(this).addClass("edited")
        }
    });

    $("form#js-plan_cash_flow_form .date-picker").datepicker({
        keyboardNavigation: false,
        forceParse: false,
        language: "ru",
        format: "dd.mm.yyyy",
        autoclose: true
    });

    $("form#js-plan_cash_flow_form input[type=radio]").uniform();

    $("#plancashflows-payment_type, #plancashflows-flow_type").change(function (e) {
        checkCashContractor();
    });

    function checkCashContractor() {
        var $paymentType = $("#plancashflows-payment_type input:checked").val();
        var $flowType = $("#plancashflows-flow_type input:checked").val();
        var $contractorInput = $("#customer_contractor_id");
        var $cashContractorIndividual = $.extend([], $cashContractorArray);

        if ($flowType == 0) {
            $contractorInput = $("#seller_contractor_id");
            delete $cashContractorIndividual.balance;
        }
        for ($item in $cashContractorArray) {
            $contractorInput.find("option[value=" + $item + "]").remove();
        }
        if ($paymentType == 1) {
            delete $cashContractorIndividual.bank;
        } else if ($paymentType == 2) {
            delete $cashContractorIndividual.order;
        } else if ($paymentType == 3) {
            delete $cashContractorIndividual.emoney;
        }
        var $newOptions = "";
        for ($item in $cashContractorIndividual) {
            $newOptions += "<option value=" + $item + ">" + $cashContractorIndividual[$item] + "</option>";
        }
        if ($newOptions !== "") {
            $contractorInput.prepend($newOptions);
        }
    };
'); ?>
