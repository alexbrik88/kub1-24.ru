<?php

use yii\bootstrap4\Modal;
use yii\helpers\Url;
use \common\models\Company;
use yii\widgets\Pjax;

/**
 * @var Company $company
 * @var int $uploadStatementCashboxId
 */

if (empty($uploadStatementCashboxId)) {
    $uploadStatementCashboxId = ($mainCashbox = $company->getCashboxes()->orderBy(['is_closed' => SORT_ASC, 'is_main' => SORT_DESC])->one()) ?
        $mainCashbox->id : null;
}
?>

<div id="modal-upload-statements" class="fade modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">

            <h4 class="modal-title mb-0">Импорт операций из</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

                <?= \yii\bootstrap4\Tabs::widget([
                    'options' => [
                        'class' => 'nav nav-tabs nav-tabs-tight nav-tabs_border_bottom_grey w-100 mt-3 mb-3',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link',
                    ],
                    'tabContentOptions' => [
                        'class' => 'tab-pane mt-1',
                        'style' => 'width: 100%'
                    ],
                    'headerOptions' => [
                        'class' => 'nav-item',
                    ],
                    'items' => [
                        [
                            'label' => 'Банк',
                            'content' => $this->render('_upload_statements/tab_banking', [
                                'company' => $company,
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link active',
                            ],
                            'active' => true
                        ],
                        [
                            'label' => 'Интернет эквайринг',
                            'content' => $this->render('_upload_statements/tab_acquiring', [
                                'company' => $company,
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ],
                        ],
                        [
                            'label' => 'Личные карты',
                            'content' => $this->render('_upload_statements/tab_personal_card', [
                                'company' => $company,
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ],
                        ],
                        [
                            'label' => 'ОФД',
                            'content' => $this->render('_upload_statements/tab_ofd', [
                                'company' => $company,
                                'uploadStatementCashboxId' => $uploadStatementCashboxId
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ],
                        ],
                        [
                            'label' => 'Excel',
                            'content' => $this->render('_upload_statements/tab_excel', [
                                'company' => $company,
                                'uploadStatementCashboxId' => $uploadStatementCashboxId
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ],
                        ],
                        [
                            'label' => '1C',
                            'content' => $this->render('_upload_statements/tab_1c', [
                                'company' => $company,
                            ]),
                            'linkOptions' => [
                                'class' => 'nav-link',
                            ],
                        ],
                    ],
                ]); ?>

            </div>

        </div>
    </div>
</div>

<!-- ADDITIONAL MODALS -->
<?= $this->render('_upload_statements/modals_1c') ?>

<?php
$this->registerJs(<<<JS
/* STATEMENTS MODAL */
$('#modal-upload-statements').on('show.bs.modal', function (e) {
    
    const modal = this;
    const tab_banking = $(modal).find('.tab_banking');
    const tab_ofd = $(modal).find('.tab_ofd');
    const tab_excel = $(modal).find('.tab_excel');
    const tab_1c = $(modal).find('.tab_1c');
    
    if (vidimusUploader1c) {
        $(tab_banking).find('.upload-1C-show').hide();
        $(tab_banking).find('.upload-1C-hide').show();        
        $(tab_banking).find('.file-list .item').remove();
        vidimusUploader1c.clearQueue();
    }
    if (vidimusUploader) {
        $(tab_excel).find('.xls-error').html('').hide();     
        $(tab_excel).find('.file-list .item').remove();        
        vidimusUploader.clearQueue();
    }
    if (vidimusUploader2) { 
        $(tab_ofd).find('.upload-1C-show').hide();
        $(tab_ofd).find('.upload-1C-hide').show();        
        $(tab_ofd).find('.xls-error-2').html('').hide();     
        $(tab_ofd).find('.file-list-2 .item').remove();        
        vidimusUploader2.clearQueue();
    }
    
    $(tab_ofd).find('.subtab_ofd_main').show();
    $(tab_ofd).find('.subtab_ofd_choose').hide();
    
    $(tab_1c).find('.subtab_1c_main').show();
    $(tab_1c).find('.subtab_1c_load').hide();    
});

/* BANKING MODAL */
$(document).on('show.bs.modal', "#banking-module-modal", function() {
    setTimeout("$('#modal-upload-statements').modal('hide')", 200);
});
$(document).on('click', '.banking-module-close-link', function() {
    $('#modal-upload-statements').modal('show');    
    setTimeout("$('#banking-module-modal').modal('hide')", 200);
});
/* OFD MODAL */
$(document).on('show.bs.modal', "#ofd-module-modal", function() {
    setTimeout("$('#modal-upload-statements').modal('hide')", 200);
});
$(document).on('click', '.ofd-module-close-link', function() {
    $('#modal-upload-statements').modal('show');    
    setTimeout("$('#ofd-module-modal').modal('hide')", 200);
});

/* VIDIMUS BUTTON */
$(document).on('mouseover', 'input[name="uploadfile"]', function() {
    $('.add-xls-file-2').css({'color': '#335A82', 'cursor':'pointer'});
});
$(document).on('mouseout', 'input[name="uploadfile"]', function() {
    $('.add-xls-file-2').css('color', '#001424');
});

// OFD SUBTABS
$('.show_subtab_ofd_choose').on('click', function() {
    const tab_ofd = $(this).closest('.tab_ofd');
    $(tab_ofd).find('.subtab_ofd_choose').show();
    $(tab_ofd).find('.subtab_ofd_main').hide();
});
$('.show_subtab_ofd_main').on('click', function() {
    const tab_ofd = $(this).closest('.tab_ofd');
    $(tab_ofd).find('.subtab_ofd_main').show();
    $(tab_ofd).find('.subtab_ofd_choose').hide();
});

// 1C SUBTABS
$('.show_subtab_1c_load').on('click', function() {
    const tab_1c = $(this).closest('.tab_1c');
    $(tab_1c).find('.subtab_1c_load').show();
    $(tab_1c).find('.subtab_1c_main').hide();
});
$('.show_subtab_1c_main').on('click', function() {
    const tab_1c = $(this).closest('.tab_1c');
    $(tab_1c).find('.subtab_1c_main').show();
    $(tab_1c).find('.subtab_1c_load').hide();
});

JS, \yii\web\View::POS_READY);
