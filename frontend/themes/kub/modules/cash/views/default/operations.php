<?php

use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use frontend\components\Icon;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\modules\telegram\widgets\CodeModalWidget;
use frontend\themes\kub\widgets\ImportDialogWidget;
use frontend\rbac\permissions;
use yii\helpers\Html;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\cash\models\CashSearch;
use frontend\themes\kub\widgets\CashFilterWidget;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel CashSearch
 * @var $user Employee
 * @var $company Company
 * @var $currentRsModel CheckingAccountant|null
 * @var $widgetWallet string|null
 * @var $multiCompanyManager AnalyticsMultiCompanyManager
 */

$this->title = 'Операции по деньгам' . ($multiCompanyManager->getIsModeOn() ? ' консолидированно' : '');
$this->params['breadcrumbs'][] = $this->title;

$isDemo = Yii::$app->user->id == (Yii::$app->params['service']['demo_employee_id'] ?? -1);
$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canProjectUpdate = $canUpdate && Yii::$app->user->identity->menuItem->project_item;
$canSalePointUpdate = $canUpdate && SalePoint::find()->where(['company_id' => $company->id])->exists();
$canIndustryUpdate = $canUpdate && CompanyIndustry::find()->where(['company_id' => $company->id])->exists();
$canCreditUpdate = $canUpdate;

$bankIds =  array_keys($searchModel->bankItems);
$cashboxIds = array_keys($searchModel->cashboxItems);
$emoneyIds = array_keys($searchModel->emoneyItems);

@list($walletType, $walletId) = explode('_', $widgetWallet);
if ($walletType == CashSearch::WALLET_ORDER && $walletId > 0) {
    $uploadStatementCashboxId = (int)$walletId; // chosen in accountWidget
} else {
    $uploadStatementCashboxId = reset($cashboxIds);
}

$userConfig = $user->config;
$strictMode = !trim($company->name_short) || !trim($company->inn);
$onboarding = boolval($company->show_cash_operations_onboarding);
?>

<div class="stop-zone-for-fixed-elems cash-operations-index">

    <div class="cash-filter-widget">
        <div class="row">
            <div class="col-6">
            <?= CashFilterWidget::widget(); ?>
            </div>
        </div>
    </div>

    <div class="mb-3">
        <?php if ($isDemo || $canCreate): ?>
            <?= $this->render('_partial/create-buttons', [
                'isDemo' => $isDemo,
                'canCreate' => $canCreate,
                'strictMode' => $strictMode,
            ]) ?>
        <?php else: ?>
            <div class="create-all-flows-button page-head d-flex flex-wrap align-items-center">
                <div class="ml-auto">
                    <?= Html::button(Icon::get('add-icon') . '<span class="mr-3 ml-2">Добавить</span>',
                        ['class' => 'button-regular button-regular_padding_medium button-regular_red button-clr mb-0', 'disabled' => true]); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <?= $this->render('_partial/head', [
        'company' => $user->company,
        'searchModel' => $searchModel,
        'userConfig' => $userConfig,
        'strictMode' => $strictMode,
        'onboarding' => $onboarding,
        'canCreate' => $canCreate,
        'showMultiCompanyButton' => true,
        'showInstructionButton' => true
    ]) ?>

    <?= $this->render('_partial/table-filter', [
        'searchModel' => $searchModel
    ]) ?>

    <?= $this->render('_partial/table', [
        'user' => $user,
        'company' => $user->company,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'multiCompanyManager' => $multiCompanyManager,
        'strictMode' => $strictMode,
    ]) ?>
</div>

<?= $this->render('_partial/table-summary-select', [
    'showButtonsAsDropdown' => $canProjectUpdate || $canSalePointUpdate || $canIndustryUpdate,
    'beforeButtons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', 'javascript:void(0)', [
            'id' => 'button-copy-flow-item',
            'class' => 'button-clr button-regular button-width button-hover-transparent',
        ]) : null
    ],
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'calendar']).' <span>План. дата</span>', '#many-plan-date', [
            'id' => 'many-plan-date-button',
            'class' => 'hidden button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canCreditUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Договор кредитный</span>', '#many-credit', [
            'id' => 'many-credit-button',
            'class' => 'hidden button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canProjectUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'project']).' <span>Проект</span>', '#many-project', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canSalePointUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'pc-shop']).' <span>Точка продаж</span>', '#many-sale-point', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndustryUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span>Направление</span>', '#many-company-industry', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'afterButtons' => [
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null
    ],
    'bankIds' => $bankIds,
    'cashboxIds' => $cashboxIds,
    'emoneyIds' => $emoneyIds
]); ?>

<?= $this->render('_modal/modal-many-project'); ?>
<?= $this->render('_modal/modal-many-sale-point'); ?>
<?= $this->render('_modal/modal-many-company-industry'); ?>
<?= $this->render('_modal/modal-many-credit'); ?>
<?= $this->render('_partial/table-modals', [
    'searchModel' => $searchModel
]) ?>
<?= $this->render('@frontend/modules/analytics/views/multi-company/modal') ?>

<?php
if ($canCreate) {
    echo CodeModalWidget::widget([
        'title' => 'Добавлять операции через Telegram',
        'viewFile' => 'code-cashbox-modal-content'
    ]);

    echo $this->render('modal_upload_statements', [
        'company' => $company,
        'uploadStatementCashboxId' => $uploadStatementCashboxId
    ]);

    echo OfdModalWidget::widget();

    echo BankingModalWidget::widget();

    echo ImportDialogWidget::widget();
}

if ($onboarding) {
$this->params['black-screen-show'] = true;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.onboarding_help_item',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'contentAsHTML' => true,
        'interactive' => true,
    ],
]);

$this->registerJs(<<<JS
var cashOperationsOnboardingOff = function() {
    $('body>.black-screen').toggleClass('show', false);
    $('#company_inn_autocomplete').toggleClass('pulse', true);
    setTimeout(function() {
        $('#company_inn_autocomplete').toggleClass('pulse', false);
    }, 30000);
    $.post('/cash/default/onboarding-off');
};
$('#onboarding_help_operation_buttons').css('z-index', 9999).tooltipster('open');
$(document).on('click', '#onboarding_help_operation_buttons_ok', function() {
    $('#onboarding_help_operation_buttons').css('z-index', 'auto').tooltipster('close').find('.onboarding_help_cover').remove();
    $('#onboarding_help_create_button').css('z-index', 9999).tooltipster('open');
});
$(document).on('click', '#onboarding_help_create_button_ok', function() {
    $('#onboarding_help_create_button').css('z-index', 'auto').tooltipster('close').find('.onboarding_help_cover').remove();
    if ($('#onboarding_help_demo_account').length) {
        $('header.header').toggleClass('no_sticky', true);
        $('#onboarding_help_demo_account').css('z-index', 9999).tooltipster('open');
    } else {
        cashOperationsOnboardingOff();
    }
});
$(document).on('click', '#onboarding_help_demo_account_ok', function() {
    $('header.header').toggleClass('no_sticky', false);
    $('#onboarding_help_demo_account').css('z-index', 'auto').tooltipster('close').find('.onboarding_help_cover').remove();
    cashOperationsOnboardingOff();
});
JS);
}

$this->registerJs(<<<JS

/////////////////////////////////
if (window.CashModalUpdatePieces)
    CashModalUpdatePieces.init();
/////////////////////////////////

// SEARCH
$("#cashsearch-contractor_name").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});
// REFRESH_UNIFORMS
$(document).on("pjax:complete", "#ofd_module_pjax", function(event) {
    refreshUniform();
    refreshDatepicker();
    if ($("#kktRegNumber").length) {
        createSimpleSelect2("kktRegNumber");
    }
});
// CATCH CHECKBOX
$(document).on("click", ".joint-checkbox-td, .joint-main-checkbox-td", function(e) {
    //console.log(e.target);
    if ($(e.target).is("td") || $(e.target).is("th")) {
        const checkbox = $(this).find("input:checkbox");
        console.log(checkbox);
        if (checkbox.length === 1)
            checkbox.click();
    }
});
JS, \yii\web\View::POS_READY);
?>