<?php

use common\models\currency\Currency;
use kartik\widgets\Select2;
use yii\bootstrap\Html;
use yii\db\Expression;
use yii\helpers\Url;
use common\models\project\ProjectSearch;
use yii\bootstrap4\Modal;

$cashboxes = Yii::$app->user->identity->getCashboxes()
    ->with('currency')
    ->orderBy(new Expression(
        'IF(currency_id = '.Currency::DEFAULT_ID.', 0, currency_id), is_main DESC, name'))
    ->indexBy('id')
    ->all();

$cashboxesList = [];
foreach ($cashboxes as $c) {
    $cashboxesList[$c->id] = ($c->currency_id == Currency::DEFAULT_ID) ?  $c->name : ($c->currency->name.' '.$c->name);
}

$this->registerJs('

    $(document).on("shown.bs.modal", "#many-cashbox", function () {
        var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
        var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
        var $modal = $(this);
    
        if ($includeExpenditureItem) {
            $(".expense-block", $modal).removeClass("hidden");
        } else {
            $(".expense-block", $modal).addClass("hidden");
        }
        if ($includeIncomeItem) {
            $(".income-block", $modal).removeClass("hidden");
        } else {
            $(".income-block", $modal).addClass("hidden");
        }
    });
    
    $(document).on("hide.bs.modal", "#many-cashbox", function () {
        var $modal = $(this);
        $(".income-block, .expense-block", $modal).addClass("hidden");
    });
');

?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-cashbox',
]); ?>
<h4 class="modal-title mb-4">Изменить кассу</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">

        <div class="col-12 income-block hidden">

            <div class="row">
                <label class="col-12" for="cashorderflowsform-flow_type">
                    Для типа
                </label>
                <div class="col-12" style="margin:0 0 8px">
                    <div id="cashorderflowsform-flow_type" aria-required="true">
                        <div class="">
                            <?= Html::radio(null, true, [
                                'label' => 'Приход',
                                'labelOptions' => [
                                    'class' => 'radio-txt-bold font-14',
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <label for="contractor-responsible_employee">
                Изменить кассу на
            </label>
            <?= Select2::widget([
                'name' => 'cashboxIdManyItemIncome',
                'options' => [
                    'class' => 'operation-many-cashbox-field'
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => $cashboxesList,
            ]); ?>
        </div>

        <div class="col-12 expense-block hidden">

            <div class="row">
                <label class="col-12" for="cashorderflowsform-flow_type">
                    Для типа
                </label>
                <div class="col-12" style="margin:0 0 8px">
                    <div id="cashorderflowsform-flow_type" aria-required="true">
                        <div class="">
                            <?= Html::radio(null, true, [
                                'label' => 'Расход',
                                'labelOptions' => [
                                    'class' => 'radio-txt-bold font-14',
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

            <label for="contractor-responsible_employee">
                Изменить кассу на
            </label>
            <?= Select2::widget([
                'name' => 'cashboxIdManyItemExpense',
                'options' => [
                    'class' => 'operation-many-cashbox-field'
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => $cashboxesList,
            ]); ?>
        </div>

    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= \yii\helpers\Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-cashbox button-regular button-width button-regular_red button-clr ladda-button',
        'data-url' => Url::to(['/cash/default/many-change-flow-cashbox']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>