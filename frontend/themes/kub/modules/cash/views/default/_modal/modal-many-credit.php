<?php

use common\components\date\DateHelper;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditRepository;
use kartik\widgets\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

$items = [0 => 'Без договора'];
$options = [];

$repository = new CreditRepository(['company_id' => Yii::$app->user->identity->company->id]);
$provider = new ActiveDataProvider([
    'query' => $repository->getQuery()->andWhere(['!=', 'credit_status', Credit::STATUS_ARCHIVE]),
    'pagination' => false,
]);
/** @var Credit $model */
foreach ($provider->getModels() as $model) {
    $items[$model->credit_id] = sprintf(
        'Договор № %s от %s',
        $model->agreement_number,
        DateHelper::format($model->agreement_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
    );
    $options[$model->credit_id] = ['data-contractor' => $model->contractor_id];
}

?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-credit'
]); ?>
<h4 class="modal-title mb-4">Изменить договор кредитный</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных операций изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Договор кредитный
            </label>
            <?= Select2::widget([
                'id' => 'operation-many-credit',
                'name' => 'creditIdManyItem',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'templateResult' => new \yii\web\JsExpression('
                        function (data, container) {
                            const selectByContractor = $("#operation-many-credit").data("contractor_id");
                            if(data.element) {
                                if (data.element.dataset.contractor > 0 && selectByContractor != data.element.dataset.contractor) {                            
                                    $(container).addClass("hidden");
                                }
                            }
                            return data.text;
                        }                    
                    ')
                ],
                'data' => $items,
                'options' => [
                    'options' => $options,
                    'data-contractor_id' => null
                ]
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= \yii\helpers\Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-credit button-regular button-width button-regular_red button-clr ladda-button',
        'data-url' => Url::to(['/cash/default/many-change-flow-credit']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>