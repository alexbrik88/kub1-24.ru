<?php

use kartik\widgets\Select2;
use yii\helpers\Url;
use common\models\companyStructure\SalePoint;
use yii\bootstrap4\Modal;
?>

<?php Modal::begin([
    'title' => null,
    'closeButton' => false,
    'id' => 'many-sale-point',
]); ?>
<h4 class="modal-title mb-4">Изменить точку продаж</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="form-body">
    <div class="row form-horizontal">
        <div class="col-12 mb-3">
            <strong>
                Для выбранных операций изменить на:
            </strong>
        </div>
        <div class="col-6">
            <label for="contractor-responsible_employee" class="label">
                Точка продаж
            </label>
            <?= Select2::widget([
                'id' => 'operation-many-sale-point',
                'name' => 'salePointIdManyItem',
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                ],
                'data' => SalePoint::getSelect2Data(),
            ]); ?>
        </div>
    </div>
</div>
<br>
<div class="mt-3 d-flex justify-content-between">
    <?= \yii\helpers\Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
        'class' => 'modal-many-change-sale-point button-regular button-width button-regular_red button-clr ladda-button',
        'data-url' => Url::to(['/cash/default/many-change-flow-sale-point']),
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
<?php Modal::end(); ?>