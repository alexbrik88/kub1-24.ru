<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;

$this->registerJs('
$("#company-has_chief_patronymic:not(.md-check)");
$(document).on("change", "#company-has_chief_patronymic", function () {
    if ($(this).prop("checked")) {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .val("")
            .attr("readonly", true)
            .trigger("change");
        $(".field-company-chief_patronymic, .field-company-ip_patronymic")
            .removeClass("has-error")
            .find(".help-block").html("");
    } else {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .attr("readonly", false);
    }
});
');

$form = ActiveForm::begin([
    'id' => 'new-company-cash-form',
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group col-6'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]);

$companyTypes = \common\models\company\CompanyType::getCompanyTypeVariants($model->company_type_id);
?>
<style type="text/css">
#company-inn::placeholder {
  color: red;
  opacity: 1;
}

#company-inn:-ms-input-placeholder {
  color: red;
}

#company-inn::-ms-input-placeholder {
  color: red;
}
</style>
<?= Html::activeHiddenInput($model, 'self_employed') ?>
<div class="row">
    <?php if ($model->self_employed) : ?>
        <?= $this->render('_self', [
            'model' => $model,
            'form' => $form,
        ]) ?>
    <?php elseif ($model->company_type_id == CompanyType::TYPE_IP) : ?>
        <?= $this->render('_ip', [
            'model' => $model,
            'form' => $form,
        ]) ?>
    <?php else : ?>
        <?= $this->render('_other', [
            'model' => $model,
            'form' => $form,
        ]) ?>
    <?php endif; ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= \yii\bootstrap4\Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr form-submit-btn ladda-button',
        'data-style' => 'expand-right',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php ActiveForm::end(); ?>