<?php

use frontend\components\Icon;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\telegram\widgets\CodeButtonWidget;
use frontend\themes\kub\widgets\VideoInstructionWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/** @var CashSearch $searchModel */

$showCalendar = $showCalendar ?? true;
$showHelpButton = $showHelpButton ?? true;
$showChartButton = $showChartButton ?? true;
$showBalanceChart = $showBalanceChart ?? true;
$showInstructionButton = $showInstructionButton ?? true;

$showHelpPanel = $userConfig->cash_operations_help;
$showChartPanel = $userConfig->cash_operations_chart;
$chartCustomPeriod = ($userConfig->cash_operations_chart_period == 0) ? "days" : "months";

$showMultiCompanyButton = $showMultiCompanyButton ?? false;
if (!isset($strictMode)) {
    $strictMode = false;
}
if (!isset($onboarding)) {
    $onboarding = false;
}
if (!isset($canCreate)) {
    $canCreate = false;
}
$moreItems = [];
if (Yii::$app->user->can(\frontend\rbac\permissions\Cash::LINKAGE)) {
    $moreItems[] = [
        'label' => 'Настройка связок',
        'url' => '#',
        'linkOptions' => [
            'class' => 'ajax-modal-btn',
            'data-title' => 'Настройка связок по операциям',
            'data-url' => Url::to(['/cash/linkage/index']),
        ]
    ];
}
if ($canCreate) {
    $moreItems[] = CodeButtonWidget::widget(['options' => [
        'tag' => 'a',
        'href' => '#',
        'class' => '',
        'label' => CodeButtonWidget::isActive() ? 'Telegram бот' : 'Telegram бот включен',
        'icon' => '/img/telegram/logo.svg'
    ]]);
}
?>

<input type="hidden" id="isAnalyticsModule" value="1"/>

<div class="wrap pt-1 pb-0 pl-4 pr-3">
    <div class="pt-1 pl-2 pr-2">
        <div class="row align-items-center">
            <div class="column mr-auto">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <?php if ($onboarding) : ?>
                <?= Html::beginTag('div', [
                    'id' => 'onboarding_help_operation_buttons',
                    'class' => 'onboarding_help_item d-flex flex-nowrap',
                    'data-tooltip-content' => '#tooltip_content_1',
                ]); ?>
                <div class="hidden">
                    <div id="tooltip_content_1">
                        <div class="mb-2">
                            <strong>Иконки для быстрого доступа</strong>
                        </div>
                        <div class="mb-1">
                            <?= Icon::get('three-squares-plus') ?>
                            создание консолидированной отчетности
                        </div>
                        <div class="mb-1">
                            <?= Icon::get('diagram') ?>
                            открывает / закрывает графики
                        </div>
                        <div class="mb-1">
                            <?= Icon::get('book') ?>
                            открывает / закрывает описание страницы
                        </div>
                        <div class="mb-1">
                            <?= Icon::get('video-instruction-2') ?>
                            открывает видеоинструкцию по странице
                        </div>
                        <div class="mt-3">
                            <?= Html::button('Отлично', [
                                'id' => 'onboarding_help_operation_buttons_ok',
                                'class' => 'button-regular button-regular_red px-4',
                            ]) ?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($showMultiCompanyButton): ?>
                <?= $this->render('@frontend/modules/analytics/views/multi-company/button') ?>
            <?php endif; ?>
            <?php if ($showChartButton): ?>
                <div class="column pl-1 <?= ($showHelpButton) ? 'pr-2' : 'pr-0' ?>">
                    <?= Html::button(Icon::get('diagram'),
                        [
                            'id' => 'btnChartCollapse',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' .
                                (!$showChartPanel ? ' collapsed' : ''),
                            'data-toggle' => 'collapse',
                            'href' => '#chartCollapse',
                            'data-tooltip-content' => '#tooltip_chart_collapse',
                            'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                        ]) ?>
                </div>
            <?php endif; ?>
            <?php if ($showHelpButton): ?>
            <div class="column pl-1 <?= ($showCalendar) ? 'pr-2' : 'pr-0' ?>">
                <?= Html::button(Icon::get('book'),
                    [
                        'id' => 'btnHelpCollapse',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-2' .
                            (!$showHelpPanel ? ' collapsed' : ''),
                        'data-toggle' => 'collapse',
                        'href' => '#helpCollapse',
                        'data-tooltip-content' => '#tooltip_help_collapse',
                        'onclick' => new \yii\web\JsExpression('$(this).tooltipster("hide")')
                    ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($showInstructionButton): ?>
            <div class="column pl-1 pr-2">
                <?= VideoInstructionWidget::widget([
                    'title' => 'Видео инструкция',
                    'src' => 'https://www.youtube.com/embed/CO_Fv4GV-eE'
                ]) ?>
            </div>
            <?php endif; ?>
            <?php if ($onboarding) : ?>
                    <div class="onboarding_help_cover"></div>
                <?= Html::endTag('div') // onboarding_help_operation_buttons ?>
            <?php endif ?>
            <?php if ($showCalendar): ?>
            <div class="column pl-1 pr-0" style="margin-top:-9px">
                <?= \frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <div style="display: none">
        <div id="tooltip_help_collapse" data-open="Открыть описание отчёта" data-close="Закрыть описание отчёта"><?= $showHelpPanel ? 'Закрыть описание отчёта' : 'Открыть описание отчёта' ?></div>
        <div id="tooltip_chart_collapse" data-open="Открыть график" data-close="Закрыть график"><?= $showChartPanel ? 'Закрыть график' : 'Открыть график' ?></div>
    </div>

</div>

<?php if ($showChartButton): ?>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 collapse <?= $showChartPanel ? 'show' : '' ?>" id="chartCollapse" data-attribute="cash_operations_chart" style="margin-top: -10px">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('head-chart', [
            'userConfig' => $userConfig,
            'model' => $searchModel,
            'customPeriod' => $chartCustomPeriod,
            'showBalanceChart' => $showBalanceChart
        ]);
        ?>
    </div>
</div>
<?php endif; ?>

<?php if ($showHelpButton): ?>
<div class="jsSaveStateCollapse wrap pt-2 pb-1 pl-4 pr-3 collapse <?= $showHelpPanel ? 'show' : '' ?>" id="helpCollapse" data-attribute="cash_operations_help" style="margin-top: -10px">
    <div class="pt-4 pb-3">
        <?php
        echo $this->render('head-help-panel')
        ?>
    </div>
</div>
<?php endif; ?>

<div class="wrap wrap_count" style="margin-top: -10px">
    <div class="row" style="margin: 0 -6px;">
        <div class="col" style="flex: 4 1 80%; padding: 0 6px;">
            <div class="row wide-statistics">
                <?= $this->render('head-statistics', ['model' => $searchModel]) ?>
            </div>
        </div>
        <div class="col" style="flex: 1 1 20%; padding: 0 6px;">
            <?php if ($strictMode) : ?>
                <div class="form-group mb-2 company_inn_autocomplete_wrap">
                    <label class="label">
                        Ваша компания
                        <?= Icon::get('question', [
                            'class' => 'tooltip-question-icon ml-2',
                            'title' => 'Введите ИНН по вашей компании или ИП, остальные реквизиты заполнятся автоматически.',
                        ]) ?>
                    </label>
                    <style type="text/css">
                        #company_inn_autocomplete::placeholder {
                            color: red;
                        }
                    </style>
                    <?= Html::textInput('new_company_inn', '', [
                        'id' => 'company_inn_autocomplete',
                        'class' => 'form-control',
                        'data-url' => '/cash/default/add-modal-company',
                        'data-title' => 'Данные по вашей компании',
                        'placeholder' => 'Введите ваш ИНН',
                    ]); ?>

                    <?php $this->registerJs('
                        $("input#company_inn_autocomplete").suggestions({
                            serviceUrl: "https://dadata.ru/api/v2",
                            token: "78497656dfc90c2b00308d616feb9df60c503f51",
                            type: "PARTY",
                            count: 10,

                            onSelect: function(suggestion) {
                                var ajaxModal = $("#ajax-modal-box");
                                var inn = suggestion.data.inn;
                                var url = $("input#company_inn_autocomplete").data("url")+"?inn="+inn;
                                $("input#company_inn_autocomplete").val(inn);
                                $(".ajax-modal-content", ajaxModal).load(url);
                                $(".ajax-modal-title", ajaxModal).html($("input#company_inn_autocomplete").data("title"));
                                ajaxModal.modal("show");
                            }
                        });
                    ') ?>
                </div>
            <?php endif ?>

            <?php if ($moreItems) : ?>
                <div class="dropdown">
                    <?= Html::a('<span>Еще</span>' . Icon::get('shevron', ['class' => 'svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute']), '#', [
                        'class' => 'button-width button-clr button-regular button-hover-transparent w-100',
                        'data-toggle' => 'dropdown',
                        'aria-expanded' => 'false'
                    ]); ?>

                    <?= Dropdown::widget([
                        'options' => [
                            'class' => 'dropdown-menu form-filter-list list-clr w-100'
                        ],
                        'items' => $moreItems,
                    ]); ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<?php

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'interactive' => true
    ],
]);

$this->registerJs(<<<JS
    // COLLAPSES
    $("#chartCollapse").on("show.bs.collapse", function() {
        $("#helpCollapse").collapse("hide");
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('close'));
    });
    $("#chartCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_chart_collapse').html($('#tooltip_chart_collapse').data('open'));
    });
    $("#helpCollapse").on("show.bs.collapse", function() {
        $("#chartCollapse").collapse("hide");
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('close'));
    });
    $("#helpCollapse").on("hide.bs.collapse", function() {
        $('#tooltip_help_collapse').html($('#tooltip_help_collapse').data('open'));
    });
JS);