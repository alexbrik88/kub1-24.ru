<?php
use common\components\date\DateHelper;
use frontend\modules\cash\models\CashSearch;
use frontend\components\StatisticPeriod;

/**
 * @var CashSearch $model
 */
$dateStart = $model->periodStartDate;
$dateEnd = $model->periodEndDate;

$statistics = $model->calcStatistics();

echo frontend\modules\cash\widgets\Statistic::widget([
    'items' => [
        [
            'class' => 'count-card_yellow',
            'text' => 'Баланс на начало',
            'amount' => $statistics['balanceAtBegin'],
            'statistic_text' => $dateStart,
            'statistic_text_class' => '_yellow_statistic',
        ],

        [
            'class' => 'count-card_turquoise',
            'text' => 'Приход' . ($model->isPlan() ? ' План' : ' Факт'),
            'amount' => $statistics['totalIncome'],
            'statistic_text' => 'Всего поступлений: ' . $statistics['countIncome'],
            'statistic_text_class' => '_green_statistic',
        ],

        [
            'class' => 'count-card_red',
            'text' => 'Расход' . ($model->isPlan() ? ' План' : ' Факт'),
            'amount' => $statistics['totalExpense'],
            'statistic_text' => 'Всего списаний: ' . $statistics['countExpense'],
            'statistic_text_class' => '_red_statistic',
        ],

        [
            'class' => 'count-card_yellow',
            'text' => 'Баланс на конец',
            'amount' => $statistics['balanceAtEnd'],
            'statistic_text' => $dateEnd,
            'statistic_text_class' => '_yellow_statistic',
        ],
    ],

    'model' => $model,
]);