<?php

use common\models\cash\CashFlowsBase;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $buttons array */
$buttons = isset($buttons) ? (array)$buttons : [];
$beforeButtons = isset($beforeButtons) ? (array)$beforeButtons : [];
$afterButtons = isset($afterButtons) ? (array)$afterButtons : [];
$asDropdown = $showButtonsAsDropdown ?? false;

$incomeCredit = \common\models\document\InvoiceIncomeItem::ITEM_CREDIT;
$incomeLoan = \common\models\document\InvoiceIncomeItem::ITEM_LOAN;
$expenseCredit =  \common\models\document\InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT;
$expenseLoan =  \common\models\document\InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT;
$expensePercents =  \common\models\document\InvoiceExpenditureItem::ITEM_PAYMENT_PERCENT;
?>
<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-panel-checkbox" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Приход: <strong class="total-income ml-1">0</strong>
        </div>
        <div class="column column-expense total-txt-foot mr-3">
            Расход: <strong class="total-expense ml-1">0</strong>
        </div>
        <div class="column total-txt-foot mr-3">
            Сальдо: <strong class="total-difference ml-1">0</strong>
        </div>
        <div class="column ml-auto"></div>

        <!-- beforeButtons -->
        <?php foreach (array_filter($beforeButtons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>

        <!-- buttons -->
        <?php if ($buttons): ?>
            <?php if ($asDropdown): ?>
                <div class="column">
                    <div class="dropup dropup-right-align">
                        <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle button-width',
                            'data-toggle' => 'dropdown',
                        ]) ?>
                        <?= \yii\bootstrap4\Dropdown::widget([
                            'items' => array_filter(array_map(function($a) {
                                $a = preg_replace('/\s?<svg[^>]*?>.*?<\/svg>\s?/si', ' ', $a); // remove tag "svg"
                                $a = preg_replace('/(class="[^"]*")/', 'class="dropdown-item"', $a); // remove all link classes
                                return $a;
                            }, array_filter($buttons))),
                            'options' => [
                                'class' => 'form-filter-list list-clr',
                                'style' => 'margin-left: -16px'
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php else: ?>
                <?php foreach (array_filter($buttons) as $key => $button) : ?>
                    <div class="column"><?= $button ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endif; ?>

        <!-- afterButtons -->
        <?php foreach (array_filter($afterButtons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>
        
    </div>
</div>

<!-- need for JS (copy flow) -->
<div id="create-flow-buttons-hidden" style="display: none; height: 0!important;">
    <a data-wallet-type="cash_bank_flows" href="<?= Url::to(['/cash/bank/create', 'redirect' => Url::current()]) ?>">
        Создать операцию по Банку
    </a>
    <a data-wallet-type="cash_order_flows" href="<?= Url::to(['/cash/order/create', 'type' => CashFlowsBase::FLOW_TYPE_INCOME, 'id' => reset($cashboxIds), 'redirect' => Url::current()]) ?>">
        Создать операцию по Кассе
    </a>
    <a data-wallet-type="cash_emoney_flows" href="<?= Url::to(['/cash/e-money/create', 'type' => CashFlowsBase::FLOW_TYPE_INCOME, 'id' => reset($emoneyIds), 'showForm' => 1, 'redirect' => Url::current()]) ?>">
        Создать операцию по E-money
    </a>
</div>

<?php
$this->registerJs(<<<SCRIPT

canShowCreditButton = function(checkboxes) {
    const isSingleContractor = (checkboxes.length === checkboxes.filter('[data-contractor_id='+checkboxes.first().data('contractor_id')+']').length);
    let   isCreditItems = true;
    $(checkboxes).each(function() {
        const incomeItemId = Number($(this).data('income_item_id'));
        const expenseItemId = Number($(this).data('expenditure_item_id'));
        if (incomeItemId !== {$incomeCredit} && incomeItemId !== {$incomeLoan}) {
            if (expenseItemId !== {$expenseCredit} && expenseItemId !== {$expenseLoan} && expenseItemId !== {$expensePercents}) {
                isCreditItems = false;
                return false;
            }
        }
    }); 

    return (isSingleContractor && isCreditItems);
}

canShowPlanDateButton = function(checkboxes) {
    let isPlanItems = true;
    $(checkboxes).each(function() {
        if (!$(this).data('is_plan')) {
            isPlanItems = false;
            return false;
        }
    }); 

    return isPlanItems;    
}

// fast joint operations checkboxes

$(document).on('change', '.joint-main-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');

    panelCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.prop('checked', $(this).prop('checked')).uniform('refresh');
    checkboxes.last().trigger('change');
});

$(document).on('change', '.joint-checkbox', function() {
    const table = $(this).closest('table');
    const checkboxes = table.find('.joint-checkbox');
    const mainCheckbox = table.find('.joint-main-checkbox');
    const panelCheckbox = $('.joint-panel-checkbox');
    const checkedCheckboxes = checkboxes.filter(':checked');
    const copyButton = $('#summary-container').find('#button-copy-flow-item');
    const creditButton = $('#summary-container').find('#many-credit-button');
    const planDateButton = $('#summary-container').find('#many-plan-date-button');

    if (checkboxes.length === checkedCheckboxes.length) {
        mainCheckbox.prop('checked', true).uniform('refresh');
        panelCheckbox.prop('checked', true).uniform('refresh');
    } else {
        mainCheckbox.prop('checked', false).uniform('refresh');
        panelCheckbox.prop('checked', false).uniform('refresh');
    }

    // copy button

    if (checkedCheckboxes.length === 1 && $(checkedCheckboxes).data('copy-url')) {
        copyButton
            .attr('href', $(checkedCheckboxes).data('copy-url'))
            .addClass('ajax-modal-btn')
            .removeClass('hidden');
    } else {
        copyButton
            .attr('href', 'javascript:void(0)')
            .removeClass('ajax-modal-btn')
            .addClass('hidden');
    }
    
    // credit button
   
    if (creditButton.length && checkedCheckboxes.length) {
        const creditSelect2 = $('#operation-many-credit');
        const creditContractor = checkedCheckboxes.data('contractor_id');    
        if (canShowCreditButton(checkedCheckboxes)) {
            creditSelect2.data('contractor_id', creditContractor).val("").trigger("change"); 
            creditButton.removeClass('hidden');
        } else {
            creditButton.addClass('hidden');
            creditSelect2.data('contractor_id', null);
        }
    }
    
    // plan date button
    
    if (planDateButton.length && checkedCheckboxes.length) {
        if (canShowPlanDateButton(checkedCheckboxes)) {
            planDateButton.removeClass('hidden');
        } else {
            planDateButton.addClass('hidden');
        }    
    }

});

$(document).on('change', '.joint-panel-checkbox', function() {
    const table = $('.table-all-cash').first();
    const mainCheckbox = table.find('.joint-main-checkbox');

    mainCheckbox.prop('checked', $(this).prop('checked')).uniform('refresh').trigger('change');
});

$(document).on('change', '.joint-checkbox', function(e) {
    let countChecked = 0;
    let inSum = 0;
    let outSum = 0;
    let diff = 0;
    $('.joint-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

$(document).on('click', '.modal-many-change-project, .modal-many-change-credit, .modal-many-change-sale-point, .modal-many-change-company-industry', function () {
    if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
    $('#ajax-loading').show();
    var that = $(this);
    $('#many-send-error .form-body .row').remove();
    if (!that.hasClass('clicked')) {
        if ($('.joint-checkbox:checked').length > 0) {
            that.addClass('clicked');
            $.post(that.data('url'), $('.joint-checkbox, #operation-many-project, #operation-many-credit, #operation-many-sale-point, #operation-many-company-industry').serialize(), function (data) {
                $('#ajax-loading').hide();
                if (data.notSend !== undefined) {
                    $('.joint-checkbox:checked').click();
                    $('#many-send-error .form-body').append('<div class="row">' + data.notSend + '</div>');
                    $('#many-send-error').modal();
                    that.removeClass('clicked');
                }
                if (data.message !== undefined) {

                    window.toastr.success(data.message, "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 1000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false,
                    });
                }
            });
        }
    }
});


SCRIPT);
?>