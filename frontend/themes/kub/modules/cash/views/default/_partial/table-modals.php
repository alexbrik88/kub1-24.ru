<?php

use common\components\helpers\Html;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url; ?>

<!-- Modal Many Delete -->
<div id="many-delete" class="modal-many-delete-item confirm-modal fade modal"
     role="modal" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'btn-confirm-yes button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                    'data-style' => 'expand-right',
                    'data-url' => Url::to(['/cash/default/many-delete-flow-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Update Item -->
<div id="update-movement" class="modal fade modal-update-flow" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 id="js-modal_update_title" class="modal-title">Редактировать операцию</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<!-- Modal Many Item -->
<div id="many-item" class="modal fade modal-many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['/cash/default/many-change-flow-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-income_item_id_wrapper',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <?= Html::radio(null, true, [
                                        'label' => 'Расход изменить на:',
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'class' => 'form-group js-expenditure_item_id_wrapper',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr ladda-button',
                            'data-style' => 'expand-right',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add Contractor -->
<div id="add-new" class="modal fade t-p-f modal_scroll_center mobile-modal" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<!-- Modal Many Plan Date -->
<div id="many-plan-date" class="modal fade modal-many-plan-date" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить дату планового платежа</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin([
                    'fieldConfig' => [
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                        'wrapperOptions' => [
                            'class' => '',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ],
                    'action' => Url::to(['/cash/default/many-change-plan-flow-date']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'form-many-plan-date',
                ]); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="position:relative">
                            <?= $form->field($searchModel, 'incomeManyPlanDate')->label(false)->textInput([
                                'class' => 'form-control date-picker ico width_date',
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                            ])->label('Дата планового прихода', ['class' => 'label']); ?>
                        </div>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <?= Html::radio(null, true, [
                                        'label' => 'Расход изменить на:',
                                        'labelOptions' => [
                                            'class' => '',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div style="position:relative">
                            <?= $form->field($searchModel, 'expenditureManyPlanDate')->label(false)->textInput([
                                'class' => 'form-control date-picker ico width_date',
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                            ])->label('Дата планового расхода', ['class' => 'label']); ?>
                        </div>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn-save button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(<<<JS
// update show
$(document).on("show.bs.modal", "#update-movement", function(event) {

    const button = $(event.relatedTarget);
    const modal = $(this);

    $.ajax({
        type: "get",
        url: button.attr("href"),
        data: {},
        success: function(data) {
            data = data.replace(/<link href=\"[^\"]+\" rel=\"stylesheet\">/g, '');
            modal.find(".modal-body").append(data);
        }
    });
});
// update hide
$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
        $("#js-modal_update_title").html("Редактировать операцию");
    }
});

// confirm delete
$(document).on("click", ".modal-delete-flow-item .btn-confirm-yes", function() {
    let l = Ladda.create(this);
    l.start();
    $.post($(this).data('url'), null, function(data) {});
});

// confirm many delete
$(document).on("click", ".modal-many-delete-item .btn-confirm-yes", function() {
    if (!$(this).hasClass('clicked')) {
        if ($('.joint-checkbox:checked').length > 0) {
            $(this).addClass('clicked');
            $.post($(this).data('url'), $('.joint-checkbox').serialize(), function(data) {});
        }
    }
});

// many item show
$(document).on("shown.bs.modal", "#many-item, #many-plan-date", function () {
    const modal = $(this);
    const header = modal.find(".modal-header h1");    
    const includeExpenditureItem = $(".joint-checkbox.expense-item:checked").length > 0;
    const includeIncomeItem = $(".joint-checkbox.income-item:checked").length > 0;
    
    let additionalHeaderText = null;

    if (includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if (includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    if (includeExpenditureItem && includeIncomeItem) {
        additionalHeaderText = " прихода / расхода";
    } else if (includeExpenditureItem) {
        additionalHeaderText = " расхода";
    } else if (includeIncomeItem) {
        additionalHeaderText = " прихода";
    }
    header.append("<span class=additional-header-text>" + additionalHeaderText + "</span>");
    $(".joint-checkbox:checked").each(function() {
        modal.find("form").prepend($(this).clone().hide());
    });
});
// many item hide
$(document).on("hidden.bs.modal", "#many-item, #many-plan-date", function () {
    const modal = $(this);    
    modal.find(".expenditure-item-block").addClass("hidden");
    modal.find(".income-item-block").addClass("hidden");
    modal.find(".additional-header-text").remove();
    modal.find(".joint-checkbox").remove();
});

// many item (update dates)
$(document).on('submit', '#form-many-plan-date', function(e) {

    const l = Ladda.create($(this).find(".btn-save")[0]);
    let hasError = false;

    l.start();
    $(".modal-many-plan-date").find("[type='text']:visible").each(function () {
        var parent = $(this).closest(".form-group");
        var reg = /^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
        $(parent).removeClass("has-error");
        $(parent).find(".help-block").text("");
        if (!$(this).val().match(reg)) {
            hasError = true;
            $(parent).addClass("has-error");
            $(parent).find(".help-block").text("Необходимо заполнить.");
        }
    });

    if (hasError) {
        Ladda.stopAll();
        return false;
    }

    return true;
});


// ajax-modal: refresh uniforms
$(document).on("shown.bs.modal", "#ajax-modal-box", function() {
    refreshUniform();
    refreshDatepicker();
});
JS);

// fix z-index overflows
$this->registerJs(<<<JS
    $('#many-delete').appendTo('body');
    $('#update-movement').appendTo('body');
    $('#many-item').appendTo('body');
    $('#add-new').appendTo('body');
    $('#article-dropdown-modal-container').appendTo('body');
    $('#many-plan-date').appendTo('body');
JS, \yii\web\View::POS_READY);

$this->registerCss(<<<CSS
.modal-open header { z-index: 2!important; }
.modal-open .page-sidebar { z-index: 0!important; }
.modal-open .page-content { z-index: 4!important; }
CSS);

?>