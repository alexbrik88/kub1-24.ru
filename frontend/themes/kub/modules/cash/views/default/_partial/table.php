<?php
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\TextHelper;
use frontend\modules\cash\models\CashSearch;
use yii\helpers\ArrayHelper;
use frontend\modules\analytics\models\PlanCashFlows;
use common\models\project\Project;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;
use common\models\cash\CashBankTinChildrenHelper as TinChildrenHelper;

/** @var CashSearch $searchModel */
/** @var AnalyticsMultiCompanyManager $multiCompanyManager */

if (!isset($multiCompanyManager)) {
    // view from contractor page (tab operations)
    $multiCompanyManager = \Yii::$app->multiCompanyManager;
}

if (!isset($strictMode)) {
    $strictMode = !trim($company->name_short) || !trim($company->inn);
}

$userConfig = $user->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_cash');
$tabConfigClass = [
    'company_id' => $multiCompanyManager->getIsModeOn() ? '' : ' hidden',
    'billPaying' => 'col_cash_column_paying' . ($userConfig->cash_column_paying ? '' : ' hidden'),
    'incomeExpense' => 'col_cash_column_income_expense' . ($userConfig->cash_column_income_expense ? '' : ' hidden'),
    'income' => 'col_invert_cash_column_income_expense'  . (!$userConfig->cash_column_income_expense ? '' : ' hidden'),
    'expense' => 'col_invert_cash_column_income_expense' . (!$userConfig->cash_column_income_expense ? '' : ' hidden'),
    'recognitionDate' => 'col_cash_column_recognition_date' . ($userConfig->cash_column_recognition_date ? '' : ' hidden'),
    'project' => 'col_cash_column_project' . ($userConfig->cash_column_project ? '' : ' hidden'),
    'priority' => 'col_cash_column_priority' . ($userConfig->cash_column_priority ? '' : ' hidden'),
    'wallet' => 'col_cash_column_wallet' . ($userConfig->cash_column_wallet ? '' : ' hidden'),
    'salePoint' => 'col_cash_column_sale_point' . ($userConfig->cash_column_sale_point ? '' : ' hidden'),
    'companyIndustry' => 'col_cash_column_industry' . ($userConfig->cash_column_industry ? '' : ' hidden'),
];
$slugs = [
    CashBankFlows::tableName() => 'bank',
    CashOrderFlows::tableName() => 'order',
    CashEmoneyFlows::tableName() => 'e-money',
    PlanCashFlows::tableName() => 'plan'
];
$currentUrl = Url::current();

$emptyText = 'В указанном периоде нет операций.';
if ($strictMode) {
    $emptyText .= ' ' . Html::tag('span', 'Добавить операцию', [
        'class' => 'link ajax-modal-btn',
        'data-url' => '/cash/default/add-modal-company',
        'data-title' => 'Данные по вашей компании',
    ]) . ' или ' . Html::tag('span', 'импортировать из банка', [
        'class' => 'link ajax-modal-btn',
        'data-url' => '/cash/default/add-modal-company',
        'data-title' => 'Данные по вашей компании',
    ]);
} else {
    $emptyText .= ' ' . Html::tag('span', 'Добавить операцию', [
        'class' => 'link ajax-modal-btn',
        'data-url' => $user->company->getRubleAccounts()->exists() ?
            Url::to(['/cash/multi-wallet/create-bank-flow', 'type' => CashFlowsBase::FLOW_TYPE_INCOME]) :
            Url::to(['/cash/multi-wallet/create-order-flow', 'type' => CashFlowsBase::FLOW_TYPE_INCOME]),
    ]) . ' или ' . Html::tag('span', 'импортировать из банка', [
        'class' => 'link',
        'data-toggle' => 'modal',
        'data-target' => '#modal-upload-statements',
    ]);
}

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyText,
    'tableOptions' => [
        'class' => 'table table-style table-count-list table-all-cash ' . $tabViewClass,
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'joint-main-checkbox-td text-left',
                'width' => '1%',
            ],
            'contentOptions' => [
                'class' => 'text-center joint-checkbox-td',
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($slugs, $currentUrl) {
                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                    $typeCss = 'income-item';
                    $income = round($flows['amount'] / 100, 2);
                    $expense = ($flows['is_internal_transfer'] && $flows['wallet_id'] != 'plan') ? $income : 0;
                } else {
                    $typeCss = 'expense-item';
                    $expense = round($flows['amount'] / 100, 2);
                    $income = ($flows['is_internal_transfer'] && $flows['wallet_id'] != 'plan') ? $expense : 0;
                }
                $slug = $slugs[$flows['tb']] ?? null;

                return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                    'class' => 'joint-checkbox ' . $typeCss,
                    'value' => $flows['id'],
                    'data' => [
                        'income' => $income,
                        'expense' => $expense,
                        'wallet-type' => $flows['tb'],
                        'contractor_id' => $flows['contractor_id'],
                        'income_item_id' => $flows['income_item_id'],
                        'expenditure_item_id' => $flows['expenditure_item_id'],
                        'is_plan' => intval($flows['tb'] === PlanCashFlows::tableName()),
                        'copy-url' => $slug ? Url::to([
                            "/cash/{$slug}/create-copy",
                            'id' => $flows['id'],
                            'redirect' => $currentUrl,
                        ]) : null,
                    ],
                ]);
            },
        ],
        // DEBUG
        //[
        //    'attribute' => 'id',
        //    'value' => function ($flows) use ($searchModel) {
        //        return $flows['id'];
        //    },
        //],
        //[
        //    'attribute' => 'transfer_key',
        //    'value' => function ($flows) use ($searchModel) {
        //        return $flows['transfer_key'];
        //    },
        //],
        [
            'attribute' => 'date',
            'label' => 'Дата',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'value' => function ($flows) use ($searchModel) {
                return DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'recognition_date',
            'label' => 'Дата признания',
            'headerOptions' => [
                'class' => 'sorting ' . $tabConfigClass['recognitionDate'],
                'width' => '10%',
                'style' => 'max-width:50px'
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['recognitionDate']
            ],
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = DateHelper::format($flows['recognition_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                if ($pieces = TinChildrenHelper::getPieces($flows, 'recognition_date'))
                    $ret .= $pieces;

                return $ret;

            }
        ],
        [
            'attribute' => 'amountIncomeExpense',
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => $tabConfigClass['incomeExpense'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right ' . $tabConfigClass['incomeExpense'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($multiCompanyManager) {

                $income = '+ ' . TextHelper::invoiceMoneyFormat(($flows['amountIncome'] > 0) ? $flows['amount'] : 0, 2);
                $expense = '- ' . TextHelper::invoiceMoneyFormat(($flows['amountExpense'] > 0) ? $flows['amount'] : 0, 2);
                $isEditable = $multiCompanyManager->canEdit($flows);

                if ($isEditable) {
                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        return Html::tag('div', CashSearch::getUpdateFlowLink($income, $flows), ['class' => 'green-link']) .
                            Html::tag('div', CashSearch::getUpdateFlowLink($expense, $flows), ['class' => 'red-link']);
                    }
                    if ($flows['amountIncome'] > 0) {
                        $link = CashSearch::getUpdateFlowLink($income, $flows);
                        if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                            $link .= $pieces;
                        return Html::tag('div', $link, ['class' => 'green-link']);
                    }
                    if ($flows['amountExpense'] > 0) {
                        $link = CashSearch::getUpdateFlowLink($expense, $flows);
                        if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                            $link .= $pieces;
                        return Html::tag('div', $link, ['class' => 'red-link']);
                    }
                } else {
                    if ($flows['is_internal_transfer'] && ($flows['wallet_id'] != 'plan')) {
                        return Html::tag('div', CashSearch::getUpdateFlowSpan($income, $flows), ['class' => 'green-link']) .
                            Html::tag('div', CashSearch::getUpdateFlowSpan($income, $flows), ['class' => 'red-link']);
                    }
                    if ($flows['amountIncome'] > 0) {
                        $span = CashSearch::getUpdateFlowSpan($income, $flows);
                        if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                            $span .= $pieces;
                        return Html::tag('div', $span, ['class' => 'green-link']);
                    }
                    if ($flows['amountExpense'] > 0) {
                        $span = CashSearch::getUpdateFlowSpan($expense, $flows);
                        if ($pieces = TinChildrenHelper::getPieces($flows, 'tin_child_amount'))
                            $span .= $pieces;
                        return Html::tag('div', $span, ['class' => 'red-link']);
                    }
                }

                return '';
            },
        ],
        [
            'attribute' => 'amountIncome',
            'label' => 'Приход',
            'headerOptions' => [
                'class' => $tabConfigClass['income'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['income'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($multiCompanyManager) {

                $formattedAmount = ($flows['amountIncome'] > 0)
                    ? TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2)
                    : '-';

                return ($multiCompanyManager->canEdit($flows) && $flows['amountIncome'] > 0) ?
                    CashSearch::getUpdateFlowLink($formattedAmount, $flows) :
                    Html::tag('span', $formattedAmount, ['class' => $flows['wallet_id'] == 'plan' ? 'plan':'']);
            },
        ],
        [
            'attribute' => 'amountExpense',
            'label' => 'Расход',
            'headerOptions' => [
                'class' => $tabConfigClass['expense'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'sum-cell text-right black-link ' . $tabConfigClass['expense'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($multiCompanyManager) {

                $formattedAmount = ($flows['amountExpense'] > 0)
                    ? TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2)
                    : '-';

                return ($multiCompanyManager->canEdit($flows) && $flows['amountExpense'] > 0) ?
                    CashSearch::getUpdateFlowLink($formattedAmount, $flows) :
                    Html::tag('span', $formattedAmount, ['class' => $flows['wallet_id'] == 'plan' ? 'plan':'']);
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'company_id',
            'label' => 'Компания',
            'headerOptions' => [
                'class' => $tabConfigClass['company_id'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'company-cell text-left ' . $tabConfigClass['company_id'],
            ],
            'filter' => ['' => 'Все'] + $searchModel->companyItems,
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                $companyName = ArrayHelper::getValue($searchModel->companyItems, $flows['company_id']);
                return '<span title="' . htmlspecialchars($companyName) . '">' . $companyName . '</span>';

            },
        ],
        [
            'attribute' => 'wallet_ids',
            'label' => 'Счета',
            'headerOptions' => [
                'width' => '10%',
                'class' => $tabConfigClass['wallet'],
            ],
            'contentOptions' => [
                'class' => 'text-left wallet-cell ' . $tabConfigClass['wallet'],
            ],
            'format' => 'raw',
            'filter' => $searchModel->getWalletFilterItems(),
            's2width' => '250px',
            's2callbacks' => [
                'escapeMarkup' => new \yii\web\JsExpression('function(markup) { return markup; }'),
                'templateResult' => new \yii\web\JsExpression('function(data, container) { return (isNaN(data.id)) ? ("<span class=\"pl-2\">"+data.text+"</span>") : (!data.id ? data.text : ("<b>"+data.text+"</b>")); }'),
            ],
            'value' => function ($flows) use ($searchModel) {

                switch ($flows['tb']) {
                    // RUB
                    case CashBankFlows::tableName():
                        $model = CashBankFlows::findOne($flows['id']);
                        $name = ArrayHelper::getValue($searchModel->bankItems, $flows['rs']) . '<br/>' . $flows['rs'];
                        break;
                    case CashOrderFlows::tableName():
                        $model = CashOrderFlows::findOne($flows['id']);
                        $name = ArrayHelper::getValue($searchModel->cashboxItems, $flows['cashbox_id']);
                        break;
                    case CashEmoneyFlows::tableName():
                        $model = CashEmoneyFlows::findOne($flows['id']);
                        $name = ArrayHelper::getValue($searchModel->emoneyItems, $flows['emoney_id']);
                        break;
                    case PlanCashFlows::tableName():
                        $model = PlanCashFlows::findOne($flows['id']);
                        switch ($model->payment_type) {
                            case CashFlowsBase::WALLET_BANK:
                                $rs = $searchModel->getBankRsById($flows['rs']);
                                $name = ArrayHelper::getValue($searchModel->bankItems, $rs) . '<br/>' . $rs;
                                break;
                            case CashFlowsBase::WALLET_CASHBOX:
                                $name = ArrayHelper::getValue($searchModel->cashboxItems, $flows['cashbox_id']);
                                break;
                            case CashFlowsBase::WALLET_EMONEY:
                                $name = ArrayHelper::getValue($searchModel->emoneyItems, $flows['emoney_id']);
                                break;
                            default:
                                $name = '';
                        }
                        break;
                    // RUB ACQUIRING
                    case AcquiringOperation::tableName():
                        $model = AcquiringOperation::findOne($flows['id']);
                        $name = ArrayHelper::getValue($searchModel->acquiringItems, $flows['acquiring_id']);
                        break;
                    // RUB CARD
                    case CardOperation::tableName():
                        $model = CardOperation::findOne($flows['id']);
                        $name = ArrayHelper::getValue($searchModel->cardItems, $flows['card_id']);
                        break;

                    // FOREIGN
                    case CashBankForeignCurrencyFlows::tableName():
                        $model = CashBankForeignCurrencyFlows::findOne($flows['id']);
                        $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->bankItems, $flows['rs']) . '<br/>' . $flows['rs'];
                        break;
                    case CashOrderForeignCurrencyFlows::tableName():
                        $model = CashOrderForeignCurrencyFlows::findOne($flows['id']);
                        $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->cashboxItems, $flows['cashbox_id']);
                        break;
                    case CashEmoneyForeignCurrencyFlows::tableName():
                        $model = CashEmoneyForeignCurrencyFlows::findOne($flows['id']);
                        $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->emoneyItems, $flows['emoney_id']);
                        break;
                    case PlanCashForeignCurrencyFlows::tableName():
                        $model = PlanCashForeignCurrencyFlows::findOne($flows['id']);
                        switch ($model->payment_type) {
                            case CashFlowsBase::WALLET_FOREIGN_BANK:
                                $rs = $searchModel->getBankRsById($flows['rs']);
                                $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->bankItems, $rs) . '<br/>' . $rs;
                                break;
                            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
                                $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->cashboxItems, $flows['cashbox_id']);
                                break;
                            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                                $name = $model->currency->name .' '. ArrayHelper::getValue($searchModel->emoneyItems, $flows['emoney_id']);
                                break;
                            default:
                                $name = '';
                        }
                        break;

                    default:
                        return '';
                }

                if ($flows['is_internal_transfer'] && ($internalTransferAccount = $model->internalTransferAccount)) {
                    $name = str_replace('<br/>', ' ', $name);
                    switch ($internalTransferAccount::tableName()) {
                        case CheckingAccountant::tableName():
                            $recipientName = $internalTransferAccount->name . ' ' . $internalTransferAccount->rs;
                            break;
                        case Cashbox::tableName():
                            $recipientName = $internalTransferAccount->name;
                            break;
                        case Emoney::tableName():
                            $recipientName = $internalTransferAccount->name;
                            break;
                        default:
                            $recipientName = '';
                    }

                    $fullName = ($flows['flow_type'] == 1) ?
                        ($name. '<br/>' . $recipientName) :
                        ($recipientName. '<br/>' . $name);

                    $tooltipFullName = ($flows['flow_type'] == 1) ?
                        (htmlspecialchars($name) . '<br/>' . htmlspecialchars($recipientName)) :
                        (htmlspecialchars($recipientName) . '<br/>' . htmlspecialchars($name));

                    return Html::tag('span', $fullName, [
                        'title' => $tooltipFullName,
                        'title-as-html' => 1
                    ]);
                }

                return Html::tag('span', $name, [
                    'title' => htmlspecialchars(str_replace('<br/>', ' ', $name)),
                    'title-as-html' => 1
                ]);
            },
        ],
        [
            'attribute' => 'contractor_ids',
            'label' => 'Контрагент',
            'headerOptions' => [
                'width' => '30%',
                'data-filter' => 'contractor_ids'
            ],
            'contentOptions' => [
                'class' => 'contractor-cell',
                'data-filter' => 'contractor_ids'
            ],
            'format' => 'raw',
            'filter' => ['' => 'Все контрагенты'] + $searchModel->getContractorFilterItems(),
            's2width' => '250px',
            'hideSearch' => false,
            'value' => function ($flows) use ($multiCompanyManager) {

                if ($flows['is_internal_transfer'])
                    return '';

                if ($flows['contractor_id'] > 0 && ($contractor = Contractor::findOne($flows['contractor_id'])) !== null) {

                    if (!$multiCompanyManager->canEdit($flows))
                        return Html::encode($contractor->nameWithType);

                    return Html::a(Html::encode($contractor->nameWithType), [
                        '/contractor/view',
                        'type' => $contractor->type,
                        'id' => $contractor->id,
                    ], ['target' => '_blank', 'title' => $contractor->nameWithType]);

                } else {

                    switch ($flows['tb']) {
                        case CashBankFlows::tableName():
                            $model = CashBankFlows::findOne($flows['id']);
                            break;
                        case CashOrderFlows::tableName():
                            $model = CashOrderFlows::findOne($flows['id']);
                            break;
                        case CashEmoneyFlows::tableName():
                            $model = CashEmoneyFlows::findOne($flows['id']);
                            break;
                        default:
                            return '';
                    }

                    if ($model && $model->cashContractor)
                        return Html::tag('span', Html::encode($model->cashContractor->text),
                            ['title' => $model->cashContractor->text]);
                }

                return '---';
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'project_id',
            'label' => 'Проект',
            'headerOptions' => [
                'class' => $tabConfigClass['project'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['project'],
            ],
            'filter' => ['' => 'Все проекты'] + $searchModel->getProjectFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['project_id'] && ($project = Project::findOne(['id' => $flows['project_id']]))) {
                    $ret .= Html::tag('span', Html::encode($project->name), ['title' => $project->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'project_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'sale_point_id',
            'label' => 'Точка продаж',
            'headerOptions' => [
                'class' => $tabConfigClass['salePoint'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['salePoint'],
            ],
            'filter' => ['' => 'Все точки продаж'] + $searchModel->getSalePointFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['sale_point_id'] && ($salePoint = SalePoint::findOne(['id' => $flows['sale_point_id']]))) {
                    $ret .= Html::tag('span', Html::encode($salePoint->name), ['title' => $salePoint->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'sale_point_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'industry_id',
            'label' => 'Направление',
            'headerOptions' => [
                'class' => $tabConfigClass['companyIndustry'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['companyIndustry'],
            ],
            'filter' => ['' => 'Все направления'] + $searchModel->getCompanyIndustryFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) {

                $ret = '';

                if ($flows['industry_id'] && ($companyIndustry = CompanyIndustry::findOne(['id' => $flows['industry_id']]))) {
                    $ret .= Html::tag('span', Html::encode($companyIndustry->name), ['title' => $companyIndustry->name]);
                } else {
                    $ret .= '-';
                }

                if ($pieces = TinChildrenHelper::getPieces($flows, 'industry_id'))
                    $ret .= $pieces;

                return $ret;
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'payment_priority',
            'label' => 'Приоритет в оплате',
            'headerOptions' => [
                'class' => $tabConfigClass['priority'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell ' . $tabConfigClass['priority'],
            ],
            'filter' => ['' => 'Все'] + $searchModel::$paymentPriorityNames,
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                return ArrayHelper::getValue($searchModel::$paymentPriorityNames, $flows['payment_priority']);
            },
        ],
        [
            'attribute' => 'description',
            'label' => 'Назначение',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '30%',
            ],
            'contentOptions' => [
                'class' => 'purpose-cell'
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                if ($flows['description']) {
                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                }

                return '';
            },
        ],
        [
            'attribute' => 'billPaying',
            'label' => 'Опл. счета',
            'headerOptions' => [
                'class' => $tabConfigClass['billPaying'],
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => $tabConfigClass['billPaying'],
            ],
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {
                switch ($flows['tb']) {
                    case CashBankFlows::tableName():
                        $model = CashBankFlows::findOne($flows['id']);
                        break;
                    case CashOrderFlows::tableName():
                        $model = CashOrderFlows::findOne($flows['id']);
                        break;
                    case CashEmoneyFlows::tableName():
                        $model = CashEmoneyFlows::findOne($flows['id']);
                        break;
                    case PlanCashFlows::tableName():
                        $model = PlanCashFlows::findOne($flows['id']);
                        $currDate = date('Y-m-d');
                        if ($model->first_date < $currDate && $model->date != $model->first_date)
                            return 'Перенос';
                        elseif ($model->date >= $currDate)
                            return 'План';
                        elseif ($model->date < $currDate)
                            return 'Просрочен';
                        else
                            return '';
                    default:
                        return '';
                }

                if ($model->credit_id) {
                    return $model->getCreditPaying();
                }

                $invoicesList = $model->getBillPaying(false);

                if ($model->getAvailableAmount() > 0) {
                    return ($invoicesList ? ($invoicesList . '<br />') : '') .
                        Html::a('<span class="red-link">Уточнить</span>',
                            CashSearch::getUpdateFlowLink(false, $flows, false),
                            [
                                'class' => 'update-flow-item red-link',
                                'title' => 'Прикрепите счета, которые были оплачены',
                                'data' => [
                                    'toggle' => 'modal',
                                    'target' => '#update-movement',
                                    'pjax' => 0
                                ],
                            ]);
                } else {
                    return $invoicesList;
                }
            },
        ],
        [
            's2width' => '200px',
            'attribute' => 'reason_ids',
            'label' => 'Статья',
            'headerOptions' => [
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'text-left reason-cell'
            ],
            'filter' => ['' => 'Все статьи', 'empty' => '-'] + $searchModel->getReasonFilterItems(),
            'format' => 'raw',
            'value' => function ($flows) use ($searchModel) {

                    if ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {

                        $mainReason = $reason = (($item = InvoiceIncomeItem::findOne($flows['income_item_id'])) ?
                            $item->fullName : "id={$flows['income_item_id']}");

                        if ($pieces = TinChildrenHelper::getPieces($flows, 'income_item_id'))
                            $reason .= $pieces;

                    } else {

                        $mainReason = $reason = (($item = InvoiceExpenditureItem::findOne($flows['expenditure_item_id'])) ?
                            $item->fullName : "id={$flows['expenditure_item_id']}");

                        if ($pieces = TinChildrenHelper::getPieces($flows, 'expenditure_item_id'))
                            $reason .= $pieces;
                    }

                    return $reason ? Html::tag('span', $reason, ['title-as-html' => 1, 'title' => htmlspecialchars($mainReason)]) : '-';
            },
        ],
        [
            'class' => ActionColumn::className(),
            'template' => '{update}{delete}',
            'headerOptions' => [
                'width' => '3%',
            ],
            'contentOptions' => [
                'class' => 'action-line nowrap',
            ],
            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
            'buttons' => [
                'update' => function ($url, $flows, $key) use ($multiCompanyManager) {

                    if (!$multiCompanyManager->canEdit($flows))
                        return '';

                    $url = CashSearch::getUpdateFlowLink(false, $flows, false);

                    return Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#pencil"></use></svg>', $url, [
                        'title' => 'Редактировать',
                        'class' => 'update-flow-item button-clr link mr-1',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#update-movement',
                            'pjax' => 0
                        ],
                    ]);
                },
                'delete' => function ($url, $flows) use ($multiCompanyManager) {

                    if (!$multiCompanyManager->canEdit($flows))
                        return '';

                    $url = CashSearch::getDeleteFlowLink($flows);

                    return \frontend\themes\kub\widgets\BtnConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#garbage"></use></svg>',
                            'class' => 'delete-flow-item button-clr link',
                            'tag' => 'a',
                        ],
                        'options' => [
                            'id' => 'delete-flow-item-' . $flows['id'],
                            'class' => 'modal-delete-flow-item',
                        ],
                        'confirmUrl' => $url,
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить операцию?',
                    ]);
                },
            ],
        ],
    ],
]);