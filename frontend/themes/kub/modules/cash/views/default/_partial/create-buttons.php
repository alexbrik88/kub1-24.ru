<?php

use common\models\Company;
use frontend\components\Icon;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\cash\CashFlowsBase;
use frontend\themes\kub\assets\CashModalAsset;

CashModalAsset::register($this);

/** @var $buttons array|null */

$company = Yii::$app->user->identity->company;
$isDemo = Yii::$app->user->id == (Yii::$app->params['service']['demo_employee_id'] ?? -1);

// after registration
$hasBanks = (bool)$company->mainCheckingAccountant;
if (!isset($strictMode)) {
    $strictMode = !trim($company->name_short) || !trim($company->inn);
}
$modalFlowType = $type ?? 1;

$lastCreateUrl = $defaultCreateUrl = ($hasBanks) ?
    ['/cash/multi-wallet/create-bank-flow', 'type' => $modalFlowType] :
    ['/cash/multi-wallet/create-order-flow', 'type' => $modalFlowType];

if ($_lastUrl = Yii::$app->session->get('multi_wallet_last_create_url')) {
    if (is_array($_lastUrl)) {
        $lastCreateUrl = $_lastUrl;
        $lastCreateUrl['type'] = $modalFlowType;
        $lastCreateUrl += ['showForm' => 1]; // need in EmoneyController
    }
}
if (isset($contractorId)) {
    $defaultCreateUrl['contractorId'] = $contractorId;
    $lastCreateUrl['contractorId'] = $contractorId;
}
if (isset($creditId)) {
    $defaultCreateUrl['creditId'] = $creditId;
    $lastCreateUrl['creditId'] = $creditId;
}

$dropItems = [];
if (!isset($buttons) || $buttons['create']) {
    $dropItems[] = ($strictMode) ?
        [
            'label' => 'Операцию',
            'url' => 'javascript:void(0)',
            'linkOptions' => [
                'data-url' => '/cash/default/add-modal-company',
                'data-title' => 'Данные по вашей компании',
                'class' => 'ajax-modal-btn dropdown-item',
                'style' => 'border-bottom:1px solid rgb(238, 238, 238)',
                'data-pjax' => '0',
            ]
        ] :
        [
            'label' => 'Операцию',
            'url' => 'javascript:void(0)',
            'linkOptions' => [
                'data-url' => Url::to($lastCreateUrl),
                'data-title' => 'Добавить операцию',
                'class' => 'ajax-modal-btn dropdown-item',
                'style' => 'border-bottom:1px solid rgb(238, 238, 238)',
                'data-pjax' => '0',
            ]
        ];
}
if (!isset($buttons) || $buttons['import']) {
    $dropItems[] =
        [
            'label' => 'Импорт',
            'url' => 'javascript:void(0)',
            'linkOptions' => [
                'data-toggle' => 'modal',
                'data-target' => '#modal-upload-statements'
            ]
        ];
}
$onboarding = boolval($company->show_cash_operations_onboarding);
?>
<div class="create-all-flows-button page-head d-flex flex-wrap align-items-center">
    <?php if ($isDemo || $canCreate): ?>
        <div class="dropdown ml-auto">
            <?php if ($onboarding) : ?>
                <?= Html::beginTag('div', [
                    'id' => 'onboarding_help_create_button',
                    'class' => 'onboarding_help_item',
                    'data-tooltip-content' => '#tooltip_content_2',
                ]); ?>
                <div class="hidden">
                    <div id="tooltip_content_2">
                        <div class="mb-2">
                            Добавление операций по деньгам
                            <br>
                            вручную и настройка импорта из
                            <br>
                            банков, 1С, ОФД, Excel и т.д.
                        </div>
                        <div class="mt-3">
                            <?= Html::button('Отлично', [
                                'id' => 'onboarding_help_create_button_ok',
                                'class' => 'button-regular button-regular_red px-4',
                            ]) ?>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <?= Html::button(
                    Icon::get('add-icon') .
                    '<span class="mr-3 ml-2">Добавить</span>' .
                    '<span style="margin-top: 0;">' . Icon::get('shevron') . '</span>',
                ['class' => 'button-regular button-regular_padding_medium button-regular_red button-clr',
                'data-toggle' => 'dropdown',
                //'disabled' => !$hasBanks,
                //'title' => (!$hasBanks) ? 'Чтобы добавить операцию, укажите расчетный счет в разделе Настройки-Профиль компании' : null
            ]); ?>
            <?php if ($onboarding) : ?>
                    <div class="onboarding_help_cover"></div>
                <?= Html::endTag('div') // onboarding_help_create_button ?>
            <?php endif ?>
            <?= Dropdown::widget([
                'options' => [
                    'class' => 'dropdown-menu form-filter-list list-clr dropdown-w-100 text-center',
                    'style' => 'z-index: 1003'
                ],
                'items' => $dropItems,
            ]); ?>
        </div>
    <?php else: ?>
        <div class="ml-auto">
            <?= Html::button(Icon::get('add-icon') . '<span class="mr-3 ml-2">Добавить</span>',
                ['class' => 'button-regular button-regular_padding_medium button-regular_red button-clr', 'disabled' => true]); ?>
        </div>
    <?php endif; ?>
</div>

<?php

$this->registerJs('

    ///////////////////
    MultiWallet.init();
    ///////////////////

    /////////////////
    PlanModal.init();
    /////////////////
    
', \yii\web\View::POS_END);

// hide company modal & show add operation modal
if ($strictMode)
    $this->registerJs('
    $(document).on("submit", "#new-company-cash-form", function (event) {
        event.preventDefault();
        $.post(this.action, $(this).serialize(), function (data) {
            if (data.success) {
                if ($(".company_inn_autocomplete_wrap").length) {
                    $(".company_inn_autocomplete_wrap").remove();
                }

                const ajaxModal = $("#ajax-modal-box");
                const addBtn = $(".page-head .ajax-modal-btn");
                addBtn.data("url", "'.(Url::to($defaultCreateUrl)).'");
                addBtn.data("title", "Добавить операцию");

                window.toastr.success("Данные компании сохранены", "", {
                    "closeButton": true,
                    "showDuration": 1000,
                    "hideDuration": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 1000,
                    "escapeHtml": false
                });

                $(".ajax-modal-content", ajaxModal).load(addBtn.data("url"));
                $(".ajax-modal-title", ajaxModal).html(addBtn.data("title"));
                $.pjax.reload("#sidemenu-pjax-container");
            }
        });
    });
');
?>

<?php /*
<script>

    PlanModal =
    {
        modalCreate: '#ajax-modal-box',
        modalUpdate: '#update-movement',
        repeated: {
            checkbox: '#is_repeated_plan_flow',
            inputDateEnd: '#flow_repeated_date_end',
        },
        const: {
            bank: {
                input: '#cashbankflowsform-date, #cashbankflows-date'
            },
            cashbox: {
                input: '#cashorderflows-date'
            },
            emoney: {
                input: '#cashemoneyflows-date'
            },
            foreign_bank: {
                input: '#cashbankforeigncurrencyflows-date_input'
            },
            foreign_cashbox: {
                input: '#cashorderforeigncurrencyflows-date_input'
            },
            foreign_emoney: {
                input: '#cashemoneyforeigncurrencyflows-date_input'
            },
        },
        init: function()
        {
            this.bindEvents();
        },
        bindEvents: function()
        {
            const obj = this;
            $(document).on('change', obj.const.bank.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.cashbox.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.emoney.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_bank.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_cashbox.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.const.foreign_emoney.input, function() {
                obj.togglePlanFactFields(this);
                obj.checkRepeatedDateEnd(this);
            });
            $(document).on('change', obj.repeated.checkbox, function() {
                const modal = obj._getActiveModal();
                if ($(this).prop('checked')) {
                    $(modal).find('.label.repeated-date-start').text('Плановая дата первая');
                    $(modal).find('.in-plan-flow-show-repeated').show();
                } else {
                    $(modal).find('.label.repeated-date-start').text('Дата плановой оплаты');
                    $(modal).find('.in-plan-flow-show-repeated').hide();
                }
            });
            $(document).on('blur', obj.repeated.inputDateEnd, function() {
                const modal = obj._getActiveModal();
                const inputDateStart = $(modal).find('.repeated-date-start').closest('.form-group').find('input.date-picker');
                const inputDateEnd = $(obj.repeated.inputDateEnd);
                const startDate = obj._parseDate(inputDateStart.val());
                const endDate = obj._parseDate(inputDateEnd.val());

                if (endDate instanceof Date && startDate instanceof Date) {
                    if (endDate < startDate)
                        inputDateEnd.data('datepicker').selectDate(startDate);
                } else if (startDate instanceof Date) {
                    inputDateEnd.data('datepicker').selectDate(startDate);
                }
            });
        },
        checkRepeatedDateEnd: function(select)
        {
            const obj = this;
            const inputDateEnd = $(obj.repeated.inputDateEnd);
            const startDate = obj._parseDate(select.value);
            const endDate = obj._parseDate(inputDateEnd.val());

            if (inputDateEnd.length && startDate instanceof Date) {
                if (endDate < startDate)
                    inputDateEnd.data('datepicker').selectDate(startDate);
            }
            //console.log(startDate, endDate)
        },
        togglePlanFactFields: function(select)
        {
            const obj = this;
            const modal = obj._getActiveModal();
            const date = obj._parseDate(select.value);
            const currDate = new Date();

            if (date instanceof Date) {
                const isPlan = date.setHours(0,0,0,0) > currDate.setHours(0,0,0,0);
                if (isPlan) {
                    $(modal).find('.modal-title').text((obj._isUpdateAction() ? 'Редактировать' : 'Добавить') + ' плановую операцию');
                    $(modal).find('.label.repeated-date-start').text('Дата плановой оплаты');
                    $(modal).find('.in-plan-flow-show').show();
                    $(modal).find('.in-fact-flow-show').hide();
                    $(modal).find('#is_plan_flow').val('1');
                } else {
                    $(modal).find('.modal-title').text((obj._isUpdateAction() ? 'Редактировать' : 'Добавить') + ' операцию');
                    $(modal).find('.label.repeated-date-start').text('Дата оплаты');
                    $(modal).find('.in-fact-flow-show').show();
                    $(modal).find('.in-plan-flow-show').hide();
                    $(modal).find('.in-plan-flow-show-repeated').hide();
                    $(modal).find('#is_plan_flow').val('');
                    $(modal).find('#is_repeated_plan_flow').prop('checked', false).uniform('refresh');
                }
            }
        },
        _getActiveModal: function()
        {
            if ($(this.modalCreate).hasClass('show')) {
                console.log('modal create');
                return $(this.modalCreate);
            }
            if ($(this.modalUpdate).hasClass('show')) {
                console.log('modal update');
                return $(this.modalUpdate);
            }

            return null;
        },
        _parseDate: function(str)
        {
            if (typeof str === "undefined") {
                console.log('str is undefined');
                return null;
            }

            const f = str.split(".");
            const d = new Date(Date.parse(f[2] +'-'+ f[1] +'-'+ f[0]));

            return !isNaN(d.getTime()) ? d : null;
        },
        _isUpdateAction: function()
        {
            return $(this.modalUpdate).hasClass('show');
        },
    };

    MultiWallet =
    {
        modalCreate: '#ajax-modal-box',
        modalUpdate: '#update-movement',
        const: {
            bank: {
                id: <?= CashFlowsBase::WALLET_BANK ?>,
                urlCreate: '/cash/multi-wallet/create-bank-flow?',
                urlUpdate: '/cash/multi-wallet/update-bank-flow?',
                hiddenInput: '#cash_bank_flows_id',
            },
            cashbox: {
                id: <?= CashFlowsBase::WALLET_CASHBOX ?>,
                urlCreate: '/cash/multi-wallet/create-order-flow?',
                urlUpdate: '/cash/multi-wallet/update-order-flow?',
                hiddenInput: '#cash_order_flows_id',
            },
            emoney: {
                id: <?= CashFlowsBase::WALLET_EMONEY ?>,
                urlCreate: '/cash/multi-wallet/create-emoney-flow?',
                urlUpdate: '/cash/multi-wallet/update-emoney-flow?',
                hiddenInput: '#cash_emoney_flows_id',
            },
            plan: {
                urlUpdate: '/cash/multi-wallet/update-plan-flow?'
            },
            foreign_bank: {
                id: <?= CashFlowsBase::WALLET_FOREIGN_BANK ?>,
                urlCreate: '/cash/multi-wallet/create-bank-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-bank-flow?&isForeign=1',
                hiddenInput: '#cash_bank_flows_id',
            },
            foreign_cashbox: {
                id: <?= CashFlowsBase::WALLET_FOREIGN_CASHBOX ?>,
                urlCreate: '/cash/multi-wallet/create-order-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-order-flow?&isForeign=1',
                hiddenInput: '#cash_order_flows_id',
            },
            foreign_emoney: {
                id: <?= CashFlowsBase::WALLET_FOREIGN_EMONEY ?>,
                urlCreate: '/cash/multi-wallet/create-emoney-flow?&isForeign=1',
                urlUpdate: '/cash/multi-wallet/update-emoney-flow?&isForeign=1',
                hiddenInput: '#cash_emoney_flows_id',
            },
            foreign_plan: {
                urlUpdate: '/cash/multi-wallet/update-plan-flow?&isForeign=1'
            },
        },
        currentVal: null,
        currentWallet: null,
        originWallet: null,
        originFlow: null,
        init: function()
        {
            this.bindEvents();
        },
        bindEvents: function()
        {
            const obj = this;

            // open multi-wallet select
            $(document).on('select2:open', '#multi_wallet_select', function() {
                if (obj.currentWallet === null) {
                    obj.currentWallet = Number($(this).find('option:selected').attr('data-wallet'));
                    obj.currentVal = $(this).val();
                }
            });

            // close create modal
            $(document).on('hide.bs.modal', obj.modalCreate, function() {
                obj.currentWallet = obj.currentVal = null;
            });

            // close update modal
            $(document).on('hide.bs.modal', obj.modalUpdate, function() {
                obj.currentWallet = obj.currentVal = null;
                obj.originWallet = obj.originFlow = null;
            });

            $(document).on('click', '.update-flow-item:not(.internal)', function() {
                const href = $(this).attr('href');
                const _wallet = href.match(/originWallet=(\d+)/);
                const _flow = href.match(/originFlow=(\d+)/);
                if (_wallet && _flow) {
                    obj.originWallet = _wallet[1];
                    obj.originFlow = _flow[1];
                }
            });

            // change wallet
            $(document).on('change', '.flow_form_wallet_select', function(e) {
                const selectedOption = $("option:selected", this);
                const selectedWallet = Number($(selectedOption).attr('data-wallet'));

                if (!selectedWallet) {
                    if (this.value === 'add-modal-rs') {
                        $(this).val(obj.currentVal).trigger('change');
                        $("#add_company_account_modal").appendTo("body").modal("show");
                        return;
                    }
                } else if (selectedWallet !== obj.currentWallet) {
                    if (obj.changeWalletModal(selectedOption)) {
                        obj.currentWallet = selectedWallet;
                        obj.currentVal = $(this).val();
                    } else {
                        alert('Невозможно изменить кошелек');
                    }
                }
                else {
                    obj.changeWalletId(selectedOption);
                }
            });
        },
        changeWalletId: function(selectedOption)
        {
            const obj = this;
            const selectedWallet = Number($(selectedOption).attr('data-wallet'));

            switch (selectedWallet) {
                case obj.const.bank.id:
                    $(obj.const.bank.hiddenInput).val(selectedOption.attr('data-rs'));
                    break;
                case obj.const.cashbox.id:
                    $(obj.const.cashbox.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.emoney.id:
                    $(obj.const.emoney.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.foreign_bank.id:
                    $(obj.const.foreign_bank.hiddenInput).val(selectedOption.attr('data-rs'));
                    break;
                case obj.const.foreign_cashbox.id:
                    $(obj.const.foreign_cashbox.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                case obj.const.foreign_emoney.id:
                    $(obj.const.foreign_emoney.hiddenInput).val(selectedOption.attr('data-id'));
                    break;
                default:
                    alert('Кошелек не найден (1)');
                    break;
            }
        },
        changeWalletModal: function(selectedOption)
        {
            const obj = this;
            const ajaxModal = (obj._isUpdateAction()) ? $(obj.modalUpdate) : $(obj.modalCreate);
            const selectedWallet = Number($(selectedOption).attr('data-wallet'));
            const isPlan = $('#is_plan_flow').val() || false;
            const type = $(ajaxModal).find('.flow-type-toggle-input:checked').val() || 1;
            const rs = $(selectedOption).attr("data-rs") || "";
            const id = $(selectedOption).attr("data-id") || "";
            const contractorId = $(ajaxModal).find('select.contractor-items-depend').val() || '';

            if (isPlan && obj._isUpdateAction()) {
                //console.log('plan modal pseudo reload');
                return true;
            }

            let url;
            switch (selectedWallet) {
                case obj.const.bank.id:
                    url = (obj._isUpdateAction() ? obj.const.bank.urlUpdate : obj.const.bank.urlCreate) + '&wallet=' + selectedWallet+'_'+rs + '&type=' + type + '&rs=' + rs + '&contractorId=' + contractorId;
                    break;
                case obj.const.cashbox.id:
                    url = (obj._isUpdateAction() ? obj.const.cashbox.urlUpdate : obj.const.cashbox.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.emoney.id:
                    url = (obj._isUpdateAction() ? obj.const.emoney.urlUpdate : obj.const.emoney.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_bank.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_bank.urlUpdate : obj.const.foreign_bank.urlCreate) + '&wallet=' + selectedWallet+'_'+rs + '&type=' + type + '&rs=' + rs + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_cashbox.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_cashbox.urlUpdate : obj.const.foreign_cashbox.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                case obj.const.foreign_emoney.id:
                    url = (obj._isUpdateAction() ? obj.const.foreign_emoney.urlUpdate : obj.const.foreign_emoney.urlCreate) + '&wallet=' + selectedWallet+'_'+id + '&type=' + type + '&id=' + id + '&contractorId=' + contractorId;
                    break;
                default:
                    alert('Кошелек не найден (2)');
                    break;
            }

            if (url) {

                if (obj._isUpdateAction()) {

                    if (!obj._validateWallet() || !obj._validateFlow())
                        return false;

                    url += '&originWallet=' + obj.originWallet + '&originFlow=' + obj.originFlow;
                }

                ajaxModal.find('.modal-body > div').first().load(url);

                return true;
            }

            return false;
        },
        _isUpdateAction: function()
        {
            return $(this.modalUpdate).hasClass('show');
        },
        _validateWallet: function()
        {
            if (!this.originFlow) {
                alert ('Кошелек не определен (3)');
                $(this.modalUpdate).modal('hide');
                return false;
            }

            return true;
        },
        _validateFlow: function()
        {
            if (!this.originWallet) {
                alert ('Операция не определена (3)');
                $(this.modalUpdate).modal('hide');
                return false;
            }

            return true;
        }
    };

    ///////////////////
    MultiWallet.init();
    ///////////////////

    /////////////////
    PlanModal.init();
    /////////////////

</script>
 */ ?>