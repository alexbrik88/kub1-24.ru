<?php

use frontend\components\Icon;
use yii\helpers\Html;

$icon = [
    'cog' => '<svg class="svg-icon help-icon"> <use xlink:href="/images/svg-sprite/svgSprite.svg#cog"></use> </svg>',
    'diagram' => '<svg class="svg-icon help-icon"> <use xlink:href="/images/svg-sprite/svgSprite.svg#diagram"></use> </svg>',
    'book' => '<svg class="svg-icon help-icon"> <use xlink:href="/images/svg-sprite/svgSprite.svg#book"></use> </svg>',
    'arrow' => '<svg class="svg-icon help-icon" style="transform:scale(0.7) rotate(90deg)"> <use xlink:href="/images/svg-sprite/svgSprite.svg#arrow"></use> </svg>',
]

?>
<div class="panel-block">
    <div class="main-block" style="line-height: 1.5">

        <p>На данной странице все операции по деньгам.</p>
        <ul style="list-style: decimal; padding-left: 17px;">
            <li>Добавление банковских счетов через кнопку <b>ДОБАВИТЬ счет или кассу</b> в левом верхнем  углу.</li>
            <li>Добавление операций через кнопку <b>ДОБАВИТЬ</b> в правом верхнем углу.</li>
            <li>Через эту же кнопку <b>ДОБАВИТЬ</b>, можно делать ИМПОРТ операций из банка, кассы, ОФД и других источников.</li>
            <li class="mt-1">Ниже над таблицей иконка <?=($icon['cog'])?> – <b>НАСТРОЙКА ТАБЛИЦЫ</b>, поможет вывести нужные вам столбцы.</li>
            <li class="mt-1">Иконка график <?=($icon['diagram'])?> открывает и закрывает графики.</li>
            <li class="mt-1">Иконка книжка <?=($icon['book'])?> открывает и закрывает описание страницы. Например то, которое вы сейчас читаете.</li>
            <li class="mt-1">Иконка видео <?=Icon::get('video-instruction-2', ['class' => 'help-icon'])?> видеоинструкция по работе с данным отчетом.</li>
            <li>В блоке период вы устанавливаете желаемый вами период отображения операций в таблице.</li>
            <li><b>Настройка статей прихода и расхода</b> в меню Финансы -> Контроль денег -> <a href="/analytics/articles/index?type=2">Статьи операций.</a></li>
            <li>Слева наверху иконка <?=($icon['arrow'])?> – сворачивает левое меню и увеличивает рабочую поверхность.</li>
        </ul>

        <p class="italic">Остались вопросы?</p>
        <p>Справа внизу <b>иконка чата</b> – задавайте вопросы или позвоните нам по телефону <b>8 800 500 5436</b>.</p>

        <?= Html::button('Всё понятно, закрыть', [
            'class' => 'button-regular button-hover-transparent',
            'data-toggle' => 'collapse',
            'href' => '#helpCollapse',
        ]) ?>
    </div>
</div>