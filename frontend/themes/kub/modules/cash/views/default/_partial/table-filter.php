<?php
use frontend\modules\cash\models\CashBankSearch;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use frontend\components\Icon;
use yii\helpers\Url;
use kartik\select2\Select2;
use common\models\cash\excel\CashXlsHelper;

/** @var $searchModel CashBankSearch */

$showContractorFilter = $showContractorFilter ?? true;
$showCreateButtonInline = $showCreateButtonInline ?? false;
$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);

$reasonItems = ['' => 'Все статьи', 'empty' => '-'] + $searchModel->getReasonFilterItems();
$contractorItems = ['' => 'Все контрагенты'] + $searchModel->getContractorFilterItems();
$walletItems = array_filter($searchModel->getWalletFilterItems(), function($k) { return !is_numeric($k); }, ARRAY_FILTER_USE_KEY);

// fix filter defaults
if ($searchModel->flow_type === null) $searchModel->flow_type = -1;
if ($searchModel->operation_type === null) $searchModel->operation_type = 0;

$isFilters = [
    'flow_type' => (bool)($searchModel->flow_type > -1),
    'wallet_ids' => (bool)($searchModel->wallet_ids),
    'contractor_ids' => (bool)$searchModel->contractor_ids && $showContractorFilter,
    'reason_ids' => (bool)($searchModel->reason_ids),
    'operation_type' => (bool)($searchModel->operation_type),
    'project_id' => (bool)($searchModel->project_id),
    'contractor_name_2' => (bool)$searchModel->contractor_name_2,
    'description_2' => (bool)$searchModel->description_2,
    'amount_2' => (bool)$searchModel->amount_2,
    'flow_type_2' => (bool)($searchModel->flow_type_2 > -1)
];
$hasFilters = array_sum($isFilters);

$filterClass = (new \ReflectionClass($searchModel))->getShortName();
?>

    <?php if ($hasFilters): ?>
        <div class="table-settings row row_indents_s">
            <div class="col-5"></div>
            <div class="col-7">
                <div class="top-filter-block-wrapper">
                    <div class="row row_indents_s">
                        <?php if ($isFilters['operation_type']): ?>
                            <div class="col-3" data-filter="operation_type">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['operation_type' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getOperationTypeFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['flow_type']): ?>
                            <div class="col-3" data-filter="flow_type">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['flow_type' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getFlowTypeFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['contractor_ids'] && $showContractorFilter): ?>
                            <div class="col-3" data-filter="contractor_ids">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['contractor_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getContractorFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['project_id']): ?>
                            <div class="col-3" data-filter="project_id">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['project_id' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getProjectFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['reason_ids']): ?>
                            <div class="col-3" data-filter="reason_ids">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['reason_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getReasonFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['wallet_ids']): ?>
                            <div class="col-3" data-filter="wallet_ids">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['wallet_ids' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getWalletFilterName() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['flow_type_2']): ?>
                            <div class="col-3" data-filter="flow_type">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['flow_type_2' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getFlowTypeFilterName2() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['contractor_name_2']): ?>
                            <div class="col-3" data-filter="contractor_name_2">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['contractor_name_2' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getContractorFilterName2() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['description_2']): ?>
                            <div class="col-3" data-filter="description_2">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['description_2' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getDescriptionFilterName2() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($isFilters['amount_2']): ?>
                            <div class="col-3" data-filter="amount_2">
                                <div class="top-filter-block">
                                    <a class="close" href="<?= Url::current([$filterClass => ['amount_2' => null]]) ?>"><?= Icon::get('close') ?></a>
                                    <span><?= $searchModel->getAmountFilterName2() ?></span>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="table-settings row row_indents_s">
        <div class="col-5">
            <div class="row align-items-center">
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                    <?= TableConfigWidget::widget([
                        'items' => $tableConfig ?? [
                            [
                                'attribute' => 'cash_column_income_expense',
                                'invert_attribute' => 'invert_cash_column_income_expense'
                            ],
                            [
                                'attribute' => 'cash_column_wallet'
                            ],
                            [
                                'attribute' => 'cash_column_recognition_date'
                            ],
                            [
                                'attribute' => 'cash_column_paying'
                            ],
                            [
                                'attribute' => 'cash_column_project'
                            ],
                            [
                                'attribute' => 'cash_column_sale_point'
                            ],
                            [
                                'attribute' => 'cash_column_industry'
                            ],
                            [
                                'attribute' => 'cash_column_priority'
                            ],
                            [
                                'attribute' => 'cash_index_hide_plan',
                                'style' => 'border-top: 1px solid #ddd',
                                'refresh-page' => true
                            ],
                        ],
                    ]); ?>
                    <?php if (in_array(Yii::$app->controller->id, [CashXlsHelper::BANK, CashXlsHelper::ORDER])
                        || Yii::$app->controller->action->id === CashXlsHelper::OPERATIONS): ?>
                        <?= Html::a(Icon::get('exel'), array_merge(
                                ['get-xls'],
                                ['foreign' => $searchModel->foreign ?? null],
                                ['currency' => $searchModel->currency_name ?? null],
                                Yii::$app->request->queryParams
                        ), [
                            'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                            'title' => 'Скачать в Excel',
                        ]); ?>
                    <?php endif; ?>
                    <?= TableViewWidget::widget($tableView ?? ['attribute' => 'table_view_cash']) ?>
                </div>
            </div>
        </div>
        <div class="col-7">
            <?php $form = ActiveForm::begin([
                'id' => 'cash_bank_filters',
                'method' => 'GET',
                'action' => $filterUrl ?? 'operations',
                'enableClientValidation' => false,
                'enableAjaxValidation' => false,
            ]); ?>
            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group mr-2 pr-1">
                    <!-- filter -->
                    <div class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter cash-filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown" style="z-index: 1001">
                        <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="button-txt">Фильтр</span>
                            <svg class="svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="filter">
                            <div class="popup-dropdown-in p-3">
                                <div class="p-1">
                                    <div class="row">
                                        <div class="col-6" data-filter="flow_type">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown1">
                                                    <div class="label">Приход/Расход</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown1">
                                                        <span>
                                                            <?= $searchModel->getFlowTypeFilterName() ?>
                                                        </span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown1">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'flow_type', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->radioList($searchModel->getFlowTypeFilterItems(), [
                                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                                    return Html::radio($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'labelOptions' => [
                                                                            'class' => 'radio-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" data-filter="reason_ids">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown3">
                                                    <div class="label">Статья</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown3">
                                                        <span><?= ($searchModel->reason_ids) ? (count($searchModel->reason_ids) . ' шт.') : 'Все статьи' ?></span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown3">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'reason_ids', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->checkboxList($reasonItems, [
                                                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                                                    if ($index == 0) {
                                                                        $checked = !$searchModel->reason_ids;
                                                                    }
                                                                    return Html::checkbox($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                        'labelOptions' => [
                                                                            'class' => 'checkbox-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" data-filter="contractor_ids">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown2">
                                                    <div class="label">Контрагент</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown2">
                                                        <span><?= ($searchModel->contractor_ids) ? (count($searchModel->contractor_ids) . ' шт.') : 'Все контрагенты' ?></span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown2">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'contractor_ids', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->checkboxList($contractorItems, [
                                                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                                                    if ($index == 0) {
                                                                        $checked = !$searchModel->contractor_ids;
                                                                    }
                                                                    return Html::checkbox($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                        'labelOptions' => [
                                                                            'class' => 'checkbox-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" data-filter="wallet_ids">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown4">
                                                    <div class="label">Мой счет</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown4">
                                                        <span><?= ($searchModel->wallet_ids) ? (count($searchModel->wallet_ids) . ' шт.') : reset($walletItems) ?></span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown4">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'wallet_ids', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->checkboxList($walletItems, [
                                                                'item' => function ($index, $label, $name, $checked, $value) use ($searchModel) {
                                                                    if ($index == 0) {
                                                                        $checked = !$searchModel->wallet_ids;
                                                                    }
                                                                    return Html::checkbox($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'class' => ($index === 0) ? 'checkbox-main' : '',
                                                                        'labelOptions' => [
                                                                            'class' => 'checkbox-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" data-filter="operation_type">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown5">
                                                    <div class="label">Факт/План</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown5">
                                                        <span>
                                                            <?= $searchModel->getOperationTypeFilterName() ?>
                                                        </span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown5">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'operation_type', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->radioList($searchModel->getOperationTypeFilterItems(), [
                                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                                    return Html::radio($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'labelOptions' => [
                                                                            'class' => 'radio-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6" data-filter="project_id">
                                            <div class="form-group mb-3">
                                                <div class="dropdown-drop" data-id="dropdown6">
                                                    <div class="label">Проект</div>
                                                    <a class="a-filter button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#"
                                                       data-target="dropdown6">
                                                        <span>
                                                            <?= $searchModel->getProjectFilterName() ?>
                                                        </span>
                                                        <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                        </svg>
                                                    </a>
                                                    <div class="dropdown-drop-menu" data-id="dropdown6">
                                                        <div class="dropdown-drop-in">
                                                            <?= $form->field($searchModel, 'project_id', [
                                                                'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                            ])->label(false)->radioList([0 => 'Все'] + $searchModel->getProjectFilterItems(), [
                                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                                    return Html::radio($name, $checked, [
                                                                        'value' => $value,
                                                                        'label' => $label,
                                                                        'labelOptions' => [
                                                                            'class' => 'radio-label p-2 no-border',
                                                                        ],
                                                                        'data-default' => ($checked) ? 1:0
                                                                    ]);
                                                                },
                                                            ]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>
                                    <div class="row justify-content-between">
                                        <div class="col-6 pr-0">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                Применить
                                            </button>
                                        </div>
                                        <div class="col-6 pl-0 text-right">
                                            <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                                Сбросить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group flex-grow-1 mr-2" data-filter="contractor_name">
                    <?= $form->field($searchModel, 'contractor_name')->label(false)->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => 'Поиск по контрагенту...']); ?>
                    <?= Html::tag('div', Icon::get('mixer'), [
                        'class' => 'cash-filter-2-toggle' . ($searchModel->contractor_name ? ' hidden' : ''),
                        'style' => ' transform:rotate(90deg)',
                        'data-target' => '.cash-filter-2',
                        'title' => 'Расширенный поиск'
                    ]) ?>
                    <!-- Start FILTER 2 -->
                    <div class="cash-filter-2 hidden">
                        <div class="p-3">
                            <table style="width: 100%">
                                <tr>
                                    <td class="pb-2" colspan="3">
                                        Расширенный поиск:
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pb-2 pr-2" width="1%">
                                        <span class="label m-0 p-0">Контрагент</span>
                                    </td>
                                    <td class="pb-2" colspan="2"><?= $form->field($searchModel, 'contractor_name_2')->label(false)->textInput(['type' => 'search', 'class' => 'form-control m-0', 'placeholder' => 'Укажите название или ИНН']) ?></td>
                                </tr>
                                <tr>
                                    <td class="pb-2 pr-2">
                                        <span class="label m-0 p-0">Назначение</span>
                                    </td>
                                    <td class="pb-2" colspan="2"><?= $form->field($searchModel, 'description_2')->label(false)->textInput(['type' => 'search', 'class' => 'form-control m-0', 'placeholder' => 'Укажите слова']) ?></td>
                                </tr>
                                <tr>
                                    <td class="pb-2 pr-2">
                                        <span class="label m-0 p-0">Сумма</span>
                                    </td>
                                    <td class="pb-2" width="50%">
                                        <div class="select2-wrapper">
                                        <?= $form->field($searchModel, 'amount_type_2')->label(false)->widget(Select2::class, [
                                            'hideSearch' => true,
                                            'data' => [
                                                CashBankSearch::FILTER_AMOUNT_TYPE_EQUAL => 'Равно',
                                                CashBankSearch::FILTER_AMOUNT_TYPE_MORE_THAN => 'Больше',
                                                CashBankSearch::FILTER_AMOUNT_TYPE_LESS_THAN => 'Меньше',
                                            ],
                                            'pluginOptions' => [
                                                'width' => '100%'
                                            ],
                                        ]) ?>
                                        </div>
                                    </td>
                                    <td class="pb-2" width="50%">
                                        <?= $form->field($searchModel, 'amount_2')->label(false)->textInput(['type' => 'search', 'class' => 'form-control m-0', 'placeholder' => '0,00']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pb-2 pr-2">
                                        <span class="label m-0 p-0">Поиск</span>
                                    </td>
                                    <td class="pb-2" colspan="2">
                                        <div class="select2-wrapper">
                                        <?= $form->field($searchModel, 'flow_type_2')->label(false)->widget(Select2::class, [
                                            'hideSearch' => true,
                                            'data' => [
                                                CashBankSearch::FILTER_FLOW_TYPE_ALL => 'Приход и Расход',
                                                CashBankSearch::FILTER_FLOW_TYPE_INCOME => 'Приход',
                                                CashBankSearch::FILTER_FLOW_TYPE_EXPENSE => 'Расход',
                                            ],
                                            'pluginOptions' => [
                                                'width' => '100%'
                                            ],
                                        ]) ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="text-right pt-2">
                                        <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red m-0']) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- End of FILTER 2 -->
                </div>
                <div class="form-group" data-filter="contractor_name">
                    <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if ($showCreateButtonInline): ?>
                <?php if ($canCreate): ?>
                    <?= $this->render('@frontend/modules/cash/views/default/_partial/create-buttons', [
                        'canCreate' => $canCreate,
                        'buttons' => [
                            'create' => true,
                            'import' => false
                        ],
                        'contractorId' => $contractorId ?? null,
                        'type' => $type ?? 1
                    ]) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

<script>
    
CashFilter = {
    selector: '.cash-filter',
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        // sub-dropdown
        $(that.selector + ' .popup-dropdown-in').on('click', function(e) {
            e.stopPropagation();

            const el = that.normalizeElement(e.target);

            if (that.detectElement.isFilter(el)) {
                that.toggleDropdown(el);
                return false;
            }
            else if (that.detectElement.isCheckboxMain(el)) {
                that.toggleCheckboxes(el, true);
                that.setDropdownValue(el);
                return true;
            }
            else if (that.detectElement.isCheckbox(el)) {
                that.toggleCheckboxes(el, false);
                that.setDropdownValue(el);
                return true;
            }
            else if (that.detectElement.isRadio(el)) {
                that.setDropdownValue(el);
                return true;
            }
            else if (that.detectElement.isButton(el)) {
                return true;
            }
            else if (that.detectElement.isSelfBody(el)) {
                return false;
            }
            else {
                that.closeAllDropdowns();
                return false;
            }
        });
        // reset filter
        $(that.selector + ' .cash_filters_reset').on('click', function() {
            that.resetFilter();
            that.refreshSubmitColor();
        });
        // change submit color
        $(that.selector + ' input').on('change', function(e) {
            e.preventDefault();
            that.refreshSubmitColor();
        });
    },
    toggleCheckboxes: function(el, isMain) {
        const wrapper = $(el).closest('.dropdown-drop-in');
        const main = wrapper.find('input.checkbox-main');
        const plain = $(el).find('input:checkbox');
        const others = wrapper.find('input:checkbox').not('.checkbox-main');
        if (isMain) {
            if (main.prop('checked'))
                others.prop('checked', false).uniform('refresh');
            else
                others.first().prop('checked', true).uniform('refresh');
        } else {
            if (plain.prop('checked'))
                main.prop('checked', false).uniform('refresh');
            else if (!others.filter(':checked').length)
                main.prop('checked', true).uniform('refresh');
        }
    },
    normalizeElement: function(el) {
        if ($(el).attr('type') === 'checkbox')
            return $(el).closest('.checkbox-label')[0];
        if ($(el).attr('type') === 'radio')
            return $(el).closest('.radio-label')[0];

        return el;
    },
    detectElement: {
        isFilter: function(el) {
            return $(el).hasClass('a-filter');
        },
        isCheckboxMain: function(el) {
            return $(el).hasClass('checkbox-label') && $(el).find('input:checkbox').hasClass('checkbox-main');
        },
        isCheckbox: function(el) {
            return $(el).hasClass('checkbox-label');
        },
        isRadio: function(el) {
            return $(el).hasClass('radio-label');
        },
        isButton: function(el) {
            return $(el).is('button');
        },
        isSelfBody: function(el) {
            return $(el).hasClass('dropdown-drop-in');
        }
    },
    toggleDropdown: function(el) {
        const that = this;
        const target = $(el).data('target');
        $(that.selector).find(' [data-id="'+target+'"]').toggleClass('visible show');
        $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').not('[data-id="'+target+'"]').removeClass('visible show');
    },
    closeDropdown: function(el) {
        $(el).parents('.dropdown-drop-menu, .dropdown-drop').removeClass('visible show');
    },
    closeAllDropdowns: function() {
        const that = this;
        $(that.selector).find('.dropdown-drop-menu.show, .dropdown-drop.show').removeClass('visible show');
    },
    setDropdownValue: function(el) {
        const wrapper = $(el).closest('.dropdown-drop-in');
        const checkedInputs = $(wrapper).find('input:checked');
        const value = (checkedInputs.length > 1) ? (checkedInputs.length + ' шт.') : $(checkedInputs).closest('label').text();
        $(el).parents('.dropdown-drop').find('a > span').html(value);
    },
    resetFilter: function() {
        const that = this;
        $(that.selector).find('.dropdown-drop-in').each(function(i, el) {
            const radio = $(this).find('input:radio');
            const checkbox = $(this).find('input:checkbox');
            if (radio.length) {
                radio.prop('checked', false).uniform().first().prop('checked', true).uniform();
            }
            if (checkbox.length) {
                checkbox.prop('checked', false).uniform().first().prop('checked', true).uniform();
            }
            that.setDropdownValue(el);
        });
    },
    refreshSubmitColor: function() {
        const that = this;
        let filter_on = false;
        $(that.selector).find('.form-filter-list').each(function() {
            let a_val = $(this).find('input').filter('[data-default=1]').val();
            let i_val = $(this).find('input:checked').val();
            if (i_val === undefined) {
                i_val = $(this).find('label:first-child').find('input').val();
            }
            if (a_val === undefined) {
                a_val = $(this).find('label:first-child').find('input').val();
            }
            if (a_val !== i_val) {
                filter_on = true;
                return false;
            }
        });

        $(that.selector).find('[type=submit]')
            .addClass(filter_on ? 'button-regular_red' : 'button-hover-content-red')
            .removeClass(filter_on ? 'button-hover-content-red' : 'button-regular_red');
    }
};

CashFilter2 = {
    mainInput: '#cashsearch-contractor_name',
    button: '.cash-filter-2-toggle',
    modal: '.cash-filter-2',
    init: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        const that = this;
        $(document).on('click', that.button, function() {
            if ($(that.modal).hasClass('hidden')) {
                $(that.modal).removeClass('hidden');
            } else {
                $(that.modal).addClass('hidden');
            }
        });
        $(document).on('paste change keyup', that.mainInput, function() {
            const btn = $(that.button);
            if (this.value.length)
                btn.addClass('hidden');
            else
                btn.removeClass('hidden');
        });
        $('body').click(function (event) {
            if ($(event.target).is(that.button) || $(event.target).parent().is(that.button))
                return;
            if(!$(event.target).closest(that.modal).length && !$(event.target).is(that.modal)) {
                $(that.modal).addClass('hidden');
            }
        });
    }
};

//////////////////
CashFilter.init();
CashFilter2.init();
//////////////////

</script>