<?php

use frontend\components\XlsHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\themes\kub\widgets\VideoInstructionWidget;

/** @var \common\models\Company $company */
$bankingAccountsArray = $company->bankingAccountants;
$hasBanks = (bool)Yii::$app->user->identity->company->mainCheckingAccountant;
?>

<div class="tab_banking">
<?php if ($hasBanks): ?>
<div class="row pb-1">
    <div class="form-group col-6 mb-0 upload-txt">
        <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button"
                onclick="$(this).siblings('.add-vidimus-file').first().click()">
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
            </svg>
            <span>Загрузить файл</span>
        </button>

        <button class="add-vidimus-file" style="display: none"></button>

        <div class="upload-1C-hide mt-2">
            Выгрузите из Клиент-Банка файл формата 1С <br>
            (txt файл) и загрузите его сюда.
        </div>
    </div>
    <div class="form-group col-6 mb-0 upload-1C-hide">
        <?php if (count($bankingAccountsArray) > 1): ?>
            <div class="dropdown ml-auto">
                <button type="button" class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" data-toggle="dropdown">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#bank-3"></use>
                    </svg>
                    <span>Напрямую из банка</span>
                </button>
                <div class="dropdown-menu form-filter-list list-clr dropdown-w-100">
                    <?php foreach ($bankingAccountsArray as $bankingAccount): ?>
                        <?php $bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
                                Url::to([
                                    "/cash/banking/{$module}/default/index",
                                    'account_id' => $bankingAccount->id,
                                    'p' => Banking::currentRouteEncode(),
                                ]) : 'javascript:void(0)'
                        ?>
                        <a class="banking-module-open-link operations-hide-statement-modal dropdown-item no-overflow" href="<?= $bankingUrl ?>">
                            <?= $bankingAccount->rs .' '. $bankingAccount->name ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>

        <?php else: ?>
            <?php
            $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
            $bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
                Url::to([
                    "/cash/banking/{$module}/default/index",
                    'account_id' => $bankingAccount->id,
                    'p' => Banking::currentRouteEncode(),
                ]) :
                Url::to([
                    '/cash/banking/default/select',
                    'p' => Banking::currentRouteEncode(),
                    'show_no_bank' => true
                ]);
            ?>
            <a href="<?= $bankingUrl ?>" class="banking-module-open-link operations-hide-statement-modal button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#bank-3"></use>
                </svg>
                <span>Напрямую из банка</span>
            </a>
        <?php endif; ?>

        <div style="position: absolute; top:0; right:15px;">
            <?= VideoInstructionWidget::widget([
                'buttonTitle' => 'Видео инструкция',
                'title' => 'Интеграция с банком для автоматической загрузки выписки на примере банка Точка',
                'titleWidth' => '85%',
                'width' => '100%',
                'height' => '500px',
                'src' => '/video/integration_findir_tochka.mp4'
            ]) ?>
        </div>

        <div class="mt-2">
            Мы уже интегрировались с несколькими банками. <br>
            Если у вас есть счет в этом банке, то выписка будет <br>
            загружаться напрямую из банка.
        </div>
    </div>
    <div class="form-group col-6 mb-0 upload-xls">
        <div class="form-group upload-1C-hide">
            <button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreDetails" aria-expanded="false" aria-controls="moreDetails">
                <span class="link-txt">Загрузка выписки из иностранного банка</span>
                <svg class="link-shevron svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                </svg>
            </button>
        </div>
        <div class="collapse in" id="moreDetails">
            <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button"
                    onclick="$(this).siblings('.add-vidimus-xls-file').first().click()">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Загрузить файл Excel</span>
            </button>

            <button class="add-vidimus-xls-file" style="display: none"></button>

            <br><br>
            <div class="upload-1C-hide">
                Если нет формата 1С (txt файл), то воспользуйтесь<br>
                шаблоном ниже. Подходит для загрузки выписки из<br>
                банков по всему миру.<br><br>
                <?= Html::a('Скачать шаблон Excel', Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_EXCERPT])) ?>
            </div>
        </div>
    </div>
    <div class="form-group col-6 mb-0 upload-1C-show" style="display: none; ">
        <div class="upload-button text-right">
            <a href="#" class="button-clr button-regular button-regular_red button-width disabled">
                Загрузить
            </a>
        </div>
    </div>
</div>
<?php else: ?>
<div class="row pb-1">
    <div class="form-group col-6 mb-0">
        <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr"
            onclick="$('#modal-upload-statements').modal('hide'); $('#add-company-rs').modal('show');">
            <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
            <span>Загрузить файл</span>
        </button>
        <div class="upload-1C-hide mt-2">
            Выгрузите из Клиент-Банка файл формата 1С <br>
            (txt файл) и загрузите его сюда.
        </div>
    </div>
    <div class="form-group col-6 mb-0">
        <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr"
            onclick="$('#modal-upload-statements').modal('hide'); $('#add-company-rs').modal('show');" >
            <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#bank-3"></use></svg>
            <span>Напрямую из банка</span>
        </button>
        <div class="mt-2">
            Мы уже интегрировались с несколькими банками. <br>
            Если у вас есть счет в этом банке, то выписка будет <br>
            загружаться напрямую из банка.
        </div>
    </div>
</div>
<?php endif; ?>

<!-- IMPORT FILE PART -->
<?php
$uploadAction = Url::to([
    '/cash/banking/default/file-upload/',
    'p' => Banking::currentRouteEncode(),
    'cancel_button' => 'dismiss-modal'
])
?>
<div class="row upload-1C-show" style="display: none;">
    <div class="col-12">
        <div class="vidimus-error">Что-то пошло не так :( Попробуйте загрузить файлы еще раз</div>
        <div class="row">
            <form enctype="multipart/form-data" action="<?= $uploadAction ?>" class="upload-txt" method="post" id="vidimus-ajax-form">
                <div class="file-list col-12">
                </div>
            </form>
            <form enctype="multipart/form-data" action="<?= $uploadAction ?>" class="upload-xls" method="post" id="vidimus-ajax-form-xls">
                <div class="file-list col-12">
                </div>
            </form>
            <div class="vidimus-loader"></div>
        </div>
    </div>
    <div class="col-12">
        <div id="progressBox"></div>
    </div>

    <div class="col-12">
        <span class="vidimus-hr"></span>
    </div>
    <div class="col-12 upload-1C-show">
        <div id="vidimus-table"></div>
    </div>
    <?php if (\Yii::$app->request->isAjax) : ?>
        <script type="text/javascript">
            initialiseVidimusUploader();
        </script>
    <?php endif; ?>
</div>
</div>