<?php
use yii\bootstrap4\Modal; 
?>

<!-- 1C INSTRUCTION -->
<?php Modal::begin([
    'id' => 'import-1c-instruction',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>
    <h4 class="modal-title">Загрузка в КУБ из 1С</h4>
    <p><strong>1</strong>. <a href="/xls/download-epf?type=import1C">Скачать обработку для 1С Бухгалтерия.</a></p>
    <p><strong>2</strong>. Откройте базу 1С на своем ПК.</p>
    <p><strong>3</strong>. Откройте папку с обработкой, которую скачали из КУБ24:</p>
    <p><img src="/img/documents/kub/import_1c_1.jpg"/></p>
    <p><strong>4</strong>. Перенесите обработку в 1С, зажав файл левой кнопкой мыши.</p>
    <p><strong>5</strong>. В базе 1С откроется обработка:</p>
    <p><img src="/img/documents/kub/import_1c_2.jpg"/></p>
    <p><strong>5.1</strong>. Выберите период выгрузки;</p>
    <p><strong>5.2</strong>. Укажите папку на своем ПК, куда будете сохранен файл выгрузки из 1С;</p>
    <p><strong>5.3</strong>. Выберите данные которые хотите выгрузить;</p>
    <p><strong>5.4</strong>. Нажмите на кнопку сохранить в правом нижнем углу:</p>
    <p><img src="/img/documents/kub/import_1c_3.jpg"/></p>
    <p><strong>6</strong>. Перейдите в папку, которую указывали при выгрузке. В ней будет находиться файл выгрузки, который нужно будет загрузить в КУБ24:</p>
    <p><img src="/img/documents/kub/import_1c_4.jpg"/></p>
<?php Modal::end() ?>

<!-- 1C AUTOLOAD -->
<?= $this->render('@frontend/modules/import/views/one-s/_viewAuto/_modal') ?>

<script>

    $(document).on('click', '.show_modal_autoload_1c', function(e) {
        e.preventDefault();
        $('#email-upload-modal').modal();
        $.pjax({
            url: '/import/one-s/get-autoload-modal',
            container: '#email-upload-pjax',
            data: {},
            push: false,
            timeout: 5000
        });
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    let $xmlError1C = $('#import-xml .xml-error');
    let $xmlButton1C = $('#import-xml .xml-buttons');
    let $xmlCreatedModels1C = $('#import-xml .created-models');

    function validateXml(fileExtension, fileSize) {
        let maxUploadedFileSize = 100 * 1024 * 1024;
        let $extensions = ['xml'];
        let $validation = true;

        if (fileSize > maxUploadedFileSize) {
            $xmlError1C.html('Превышен максимальный размер файла. Допустимый размер файла: 100 Мб');
            $xmlError1C.show();
            $validation = false;
        }

        if ($.inArray(fileExtension, $extensions) == -1) {
            $xmlError1C.html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + $extensions.join(', '));
            $xmlError1C.show();
            $validation = false;
        }

        if ($validation) {
            $xmlError1C.hide();
        }
        return $validation;
    }

    function initXmlUpload() {
        vidimusUploader = new ss.SimpleUpload({
            button: $('.add-xml-file'),
            url: '/import/one-s-ajax/upload', // server side handler
            progressUrl: '/import/one-s-ajax/progress', // enables cross-browser progress support (more info below)
            responseType: 'json',
            name: 'uploadfile',
            allowedExtensions: ['xml'], // for example, if we were uploading pics
            hoverClass: 'ui-state-hover',
            focusClass: 'ui-state-focus',
            disabledClass: 'ui-state-disabled',
            form: $('#xml-ajax-form'),
            multiple: false,
            multipleSelect: false,
            onSubmit: function (filename, extension) {
                let progress = document.createElement('div'),
                    bar = document.createElement('div'),
                    fileSize = document.createElement('div'),
                    wrapper = document.createElement('div'),
                    progressBox = $('#import-xml .progressBox')[0];
                $xmlError1C.hide();
                progress.className = 'progress progress-striped';
                bar.className = 'progress-bar progress-bar-success';
                fileSize.className = 'size';
                wrapper.className = 'wrapper';
                progress.appendChild(bar);
                wrapper.innerHTML = '<div class="name">' + filename + '</div>';
                wrapper.appendChild(progress);
                progressBox.appendChild(wrapper);
                this.setProgressBar(bar);
                this.setFileSizeBox(fileSize);
                this.setProgressContainer(wrapper);
            },
            onChange: function (filename, extension, uploadBtn, fileSize, file) {
                let $maxFilesCount = 1;
                let $filesQueueKey = vidimusUploader.getQueueSize();
                let $filesCount = $filesQueueKey + 1;
                if (!validateXml(extension, fileSize)) {
                    this.removeCurrent();
                    return false;
                }
                $xmlButton1C.hide();
                $xmlError1C.hide();
                $('.upload-xml-button a').removeClass('disabled');
                $('.upload-xml-button a.tooltipstered').tooltipster('disable');
                if ($filesCount == $maxFilesCount) {
                    $('.add-xml-file').addClass('disabled');
                }
                let $fileItemHtml = $('<div class="item">'
                    + '<div class="file-name">' + filename + '</div>'
                    + '<a href="#" data-queue="0" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                    + '<div></div>'
                    + '</div>');
                $('.file-list').append($fileItemHtml);
            },
            onComplete: function (filename, data) {

                $('.add-xml-file').addClass('disabled');
                $('input[name="uploadfile"]').prop('disabled', true);

                ///////////////////
                /// START PARSE ///
                ///////////////////
                let inProcess = false;
                let uploadTotals = '';
                let progressTimerId;
                let showTotals = function(title, data) {
                    // totals
                    uploadTotals = title + '<br/>';
                    uploadTotals += '<div class="info">';
                    uploadTotals += '<b>Загружено:</b>' + '<br/>' + data.uploadedItems;
                    uploadTotals += '<b>Дубли загрузки:</b>' + '<br/>' + data.doubleItems;
                    uploadTotals += '</div>';
                    $xmlError1C.html(uploadTotals);
                    $xmlError1C.show();
                };
                let refreshTotals = function() {
                    $.post('/import/progress/one-s', [], function (data) {
                        if (inProcess) {
                            showTotals('Дождитесь окончания загрузки ' + '<img width="24" src="/img/import-preloader.gif">', data);

                            if (data.completed) {
                                uploadsCompleted(data);
                            }
                        }
                    });
                };
                let uploadsCompleted = function(data) {
                    inProcess = false;
                    clearInterval(progressTimerId);
                    showTotals('Данные успешно загружены!', data);
                    // $.pjax.reload('#import-list');
                    window.toastr.success("Данные успешно загружены", "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                    $('.add-xml-file').removeClass('disabled');
                    $('input[name="uploadfile"]').prop('disabled', false);
                    $('#modal-upload-statements .modal-close, #modal-upload-statements .modal-close-2').prop('disabled', false);
                };

                if (data.result) {

                    inProcess = true;

                    showTotals('Дождитесь окончания загрузки ' + '<img width="24" src="/img/import-preloader.gif">', data);

                    $.post('/import/one-s-ajax/parse', [], function (data) {});

                    progressTimerId = setInterval(refreshTotals, 2000);

                } else {

                    window.toastr.success(data.message, "", {
                        "closeButton": true,
                        "showDuration": 1000,
                        "hideDuration": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 1000,
                        "escapeHtml": false
                    });
                }
            },
        });
    }

    $(document).ready(function () {

        $(document).on('click', '.upload-xml-button a', function (e) {
            e.preventDefault();
            $('.file-list .item').remove();
            $('.upload-xml-button a').addClass('disabled');
            $('.upload-xml-button a.tooltipstered').tooltipster('enable');
            $('#modal-upload-statements .modal-close, #modal-upload-statements .modal-close-2').prop('disabled', true);
            $('#xml-ajax-form').submit();
        });

        $('.file-list').on('click', '.delete-file', function (e) {
            e.preventDefault();
            $(this).parent().remove();
            vidimusUploader['_queue'].splice(0, 1);
            $('.upload-xml-button a').addClass('disabled');
            $('.upload-xml-button a.tooltipstered').tooltipster('enable');
            $('.add-xml-file').removeClass('disabled');
        });

        ////////////////
        initXmlUpload();
        ///////////////
    });
</script>

<style>
    .modal-close:disabled {
        opacity: .25!important;
        cursor: not-allowed!important;
    }
</style>