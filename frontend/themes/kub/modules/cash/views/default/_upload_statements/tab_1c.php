<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="tab_1c">

    <div class="subtab_1c_main">
        <div class="row pb-1">
            <div class="col-12">
                <div class="mb-3 w-100">
                    Импорт из 1С позволит анализировать каждую операцию по деньгам вплоть до единицы проданного товара/услуги.
                    Вы сможете анализировать в разрезе каждого товара: кому, когда и почем был продан, а также у кого, когда и по каким ценам закупался.
                </div>
                <button class="show_subtab_1c_load button-hover-content-red button-regular button-regular_padding_bigger button-clr ofd-inline-btn" type="button">
                    Загрузить данные из 1С
                </button>
                <button class="show_modal_autoload_1c button-hover-content-red button-regular button-regular_padding_bigger button-clr ofd-inline-btn" type="button">
                    Настройка автозагрузки
                </button>
                <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr ofd-inline-btn" type="button" data-toggle="modal" data-target="#import-1c-instruction">
                    Инструкция
                </button>
            </div>
        </div>
    </div>

    <div id="import-xml" class="subtab_1c_load" style="display: none">

        <div class="mb-3">
            <div class="row">
                <div class="col-6">
                    <?= \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']) . '<span>Выбрать файл</span>', 'javascript:;', [
                        'class' => 'button-regular button-width button-hover-content-red button-clr add-xml-file',
                        'style' => 'width:160px'
                    ]); ?>
                </div>
                <div class="col-6 upload-xml-button ta-r">
                    <?= \yii\bootstrap\Html::a('<span>Загрузить</span>', 'javascript:;', [
                        'class' => 'button-regular button-width button-hover-content-red button-clr disabled',
                        'data-tooltip-content' => '#upload-xml-tooltip',
                        'data-tooltipster' => Json::encode([
                            'contentAsHTML' => true,
                        ]),
                        'style' => 'width:160px'
                    ]); ?>
                    <div class="hidden">
                        <div id="upload-xml-tooltip">
                            Сначала добавьте файл для загрузки
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12" style="margin-top: 20px;">
                    <div class="xml-error"></div>
                    <div class="row">
                        <?php ActiveForm::begin([
                            'action' => '#',
                            'method' => 'post',
                            'id' => 'xml-ajax-form',
                            'options' => [
                                'enctype' => 'multipart/form-data',
                            ],
                        ]); ?>
                        <?= Html::hiddenInput('createdModels', null, [
                            'class' => 'created-models',
                        ]); ?>
                        <div class="file-list col-12">
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="col-12">
                    <div id="progressBox"></div>
                </div>
            </div>
            <div class="row xls-title">
                <div class="gray-alert col-12 mt-2">
                    Для загрузки данных из 1С, скачайте обработку для вашей 1С.<br/>
                    Откройте её в 1С и дальше действуйте по инструкции.<br/>
                    Выгруженный файл загрузите по кнопке "Выбрать файл".
                </div>
            </div>
            <div class="row">
                <div class="col-12" style="margin-top: 20px;">
                    <?= Html::a('Скачать обработку для 1С Бухгалтерия', Url::to(['/xls/download-epf', 'type' => 'import1C']), [
                        'class' => 'upload-xls-template-url',
                    ]); ?>
                    <br/>
                    <?= Html::a('Скачать обработку для 1С Управление Торговлей', Url::to(['/xls/download-epf', 'type' => 'import1C_UT']), [
                        'class' => 'upload-xls-template-url',
                    ]); ?>
                    <br/>
                    <?= Html::a('Скачать обработку для 1С Комплексная автоматизация', Url::to(['/xls/download-epf', 'type' => 'import1C_KA']), [
                        'class' => 'upload-xls-template-url',
                    ]); ?>
                    <br/>
                    <?= Html::a('Скачать обработку для 1С Fresh', Url::to(['/xls/download-epf', 'type' => 'import1C_Fresh']), [
                        'class' => 'upload-xls-template-url',
                    ]); ?>
                </div>
                <?php /*
                    <div class="column mr-auto xml-action-buttons mt-4">
                        <?= Html::button('Закрыть', [
                            'class' => 'button-regular button-width button-clr modal-close-2',
                            'data-dismiss' => 'modal',
                        ]); ?>
                    </div>
                    */ ?>
                <div class="col-12">
                    <div class="progressBox"></div>
                </div>
            </div>
        </div>

        <button class="show_subtab_1c_main button-width button-regular button-hover-transparent" type="button">
            <span>Назад</span>
        </button>

    </div>

</div>