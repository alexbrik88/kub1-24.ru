<?php

use frontend\modules\cash\modules\banking\components\Banking;
//use frontend\modules\cash\modules\ofd\components\Ofd;
use common\models\ofd\Ofd;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\XlsHelper;
use yii\widgets\ActiveForm;
use frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdModel as Taxcom;
use frontend\modules\cash\modules\ofd\models\OfdParams as TaxcomParams;

/** @var int $uploadStatementCashboxId */

$formData  = Html::hiddenInput('className', 'CashOrderFlows');
$formData .= Html::hiddenInput('cashbox', $uploadStatementCashboxId);

$uploadXlsTemplateUrl = Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_ORDER]);

$employee = Yii::$app->user->identity;

$p = Banking::currentRouteEncode();

$hasIntegration = [
    Ofd::PLATFORMA => $employee->getOfdUser(Ofd::PLATFORMA)->exists(),
    Ofd::TAXCOM => $employee->getOfdUser(Ofd::TAXCOM)->exists()
];
$integrationUrl = [
    Ofd::PLATFORMA => Url::to(['/ofd/platforma/default/integration', 'p' => $p]),
    Ofd::TAXCOM => Url::to(['/ofd/taxcom/default/integration', 'p' => $p])
];
$receiptUrl = [
    Ofd::PLATFORMA => Url::to(['/ofd/platforma/default/receipt', 'p' => $p]),
    Ofd::TAXCOM => Url::to(['/ofd/taxcom/default/receipt', 'p' => $p])
];
?>
<div class="tab_ofd">

    <div class="subtab_ofd_main">
        <div class="row pb-1">
            <div class="form-group col-6 mb-0">
                <button class="add-xls-file-2 button-hover-content-red button-regular button-regular_padding_bigger button-clr"
                        type="button" style="max-width: 179px" data-modal-id="#modal-upload-statements">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Загрузить файл</span>
                </button>
                <div class="upload-1C-hide mt-2">
                    Заполните шаблон данными из ОФД <br>
                    и загрузите его сюда.
                </div>
            </div>
            <div class="form-group col-6 mb-0 upload-1C-show" style="display: none; ">
                <div class="upload-xls-button-2">
                    <a href="#" class="button-clr button-regular button-regular_red width-160 pull-right disabled">
                        Загрузить
                    </a>
                </div>
            </div>
            <div class="form-group col-6 mb-0 upload-1C-hide">

                <div class="dropdown ml-auto">
                    <button type="button" class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" data-toggle="dropdown">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#bank-3"></use>
                        </svg>
                        <span>Напрямую из ОФД</span>
                    </button>
                    <div class="dropdown-menu form-filter-list list-clr dropdown-w-100">
                        <a href="<?= ($hasIntegration[Ofd::PLATFORMA]) ? $receiptUrl[Ofd::PLATFORMA] : $integrationUrl[Ofd::PLATFORMA] ?>"
                           class="ofd-module-open-link operations-hide-statement-modal dropdown-item no-overflow"
                           style="padding-top: 11px; padding-bottom: 11px;"
                           type="button"
                           title="Платформа ОФД">
                            <?= Html::img("/images/analytics/options/ofd_platforma.png", ['width' => '80px']) ?>
                        </a>
                        <a href="<?= ($hasIntegration[Ofd::TAXCOM]) ? $receiptUrl[Ofd::TAXCOM] : $integrationUrl[Ofd::TAXCOM] ?>"
                           class="ofd-module-open-link operations-hide-statement-modal dropdown-item no-overflow"
                           style="padding-top: 14px; padding-bottom: 14px;"
                           type="button"
                           title="TaxCom">
                            <?= Html::img("/images/analytics/options/ofd_taxcom.png") ?>
                        </a>
                    </div>
                </div>
                <div class="mt-2">
                    Мы уже интегрировались с несколькими ОФД. <br>
                    Если у вас касса в этом ОФД, то операции по кассе  <br>
                    будут загружаться напрямую из ОФД.
                </div>
            </div>
        </div>

        <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
            'class' => 'upload-xls-template-url',
            'data-pjax' => '0',
            'target' => '_blank',
        ]); ?>

        <!-- IMPORT FILE PART -->
        <div class="row">
            <div class="col-12" style="margin-top: 20px;">
                <div class="xls-error-2"></div>
                <div class="row">
                    <?php ActiveForm::begin([
                        'action' => '#',
                        'method' => 'post',
                        'id' => 'xls-ajax-form-2',
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
                    ]); ?>
                    <?= $formData; ?>
                    <?= Html::hiddenInput('createdModels', null, [
                        'class' => 'created-models-2',
                    ]); ?>
                    <div class="file-list-2 col-sm-12">
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>
            <div class="row action-buttons buttons-fixed xls-buttons-2"
                 style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
                <div class="button-bottom-page-lg col-sm-6 col-xs-6"
                     style="text-align: left;width: 50%">
                    <?= Html::a('Сохранить', 'javascript:;', [
                        'class' => 'button-regular button-width button-regular_red button-clr undelete-models',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-6 col-xs-6"
                     style="text-align: right;width: 50%;">
                    <?= Html::a('Отменить', 'javascript:;', [
                        'class' => 'button-clr button-width button-regular button-hover-transparent xls-close',
                    ]); ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="progressBox2"></div>
            </div>
        </div>
    </div>

</div>

<?php $this->registerJs(<<<JS
    $('#modal-upload-statements')
        .on('click', '.upload-xls-button-2 a', function (e) {
            e.preventDefault();
            const currTab = $(this).closest('div.tab-pane');
            currTab.find('.file-list-2 .item').remove();
            currTab.find('.upload-xls-button-2 a').addClass('disabled');
            currTab.find('.add-xls-file-2').removeClass('disabled');
            currTab.find('#xls-ajax-form-2').submit();
        })
        .on('click', '.delete-file-2', function (e) {
            e.preventDefault();
            const currTab = $(this).closest('div.tab-pane');
            $(this).parent().remove();
            currTab.find('.xls-title').show();
            currTab.find('.upload-xls-template-url').show();
            currTab.find('.upload-xls-button-2 a').addClass('disabled');
            currTab.find('.add-xls-file-2').removeClass('disabled');
            vidimusUploader['_queue'].splice(0, 1);
        });
JS, \yii\web\View::POS_READY);