<?php

namespace frontend\modules\cash\views;

use frontend\modules\cards\widgets\ZenmoneyButtonWidget;

?>
<div class="tab_personal_card">
    <div class="mb-3 w-100">
        Импорт из 1С позволит анализировать каждую операцию по деньгам вплоть до единицы проданного товара/услуги.
        Вы сможете анализировать в разрезе каждого товара: кому, когда и почем был продан, а также у кого, когда и по каким ценам закупался.
    </div>

    <?= ZenmoneyButtonWidget::widget([
        'cssClass' => 'button-regular button-hover-content-red button-clr ofd-inline-btn',
        'content' => <<<HTML
            <img src="/img/zenmoney/logo.png" alt="" style="height: 22px;">
            <span class="ml-1">Дзен мани</span>
HTML,
        'hasImport' => true,
        'hasBlock' => false,
    ]) ?>
</div>
