<?php

namespace frontend\modules\cash\views;

use frontend\modules\acquiring\widgets\MonetaButtonWidget;
use frontend\modules\acquiring\widgets\YookassaButtonWidget;
use yii\helpers\Html;

?>
<div class="tab_acquiring" style="margin-bottom: 64px">
    <?= YookassaButtonWidget::widget([
        'cssClass' => 'button-regular button-hover-content-red button-clr ofd-inline-btn',
        'content' => Html::img("/img/yookassa/integration.png", ['style' => 'height: 40px;']),
        'hasImport' => true,
        'hasBlock' => false,
    ]) ?>

    <?= MonetaButtonWidget::widget([
        'cssClass' => 'button-regular button-hover-content-red button-clr ofd-inline-btn',
        'content' => Html::img("/images/analytics/options/acquiring_moneta.png"),
        'hasImport' => true,
        'hasBlock' => false,
    ]) ?>

    <span class="disabled button-regular button-hover-content-red button-clr ofd-inline-btn" type="button" title="Скоро будет">
        <?= Html::img("/images/analytics/options/acquiring_robokassa.png") ?>
    </span>
    <span class="disabled button-regular button-hover-content-red button-clr ofd-inline-btn" type="button" title="Скоро будет">
        <?= Html::img("/images/analytics/options/acquiring_tinkoff.png") ?>
    </span>
</div>
