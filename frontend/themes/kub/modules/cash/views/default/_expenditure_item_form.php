<?php

use frontend\rbac\UserRole;
use yii\helpers\Url;
use frontend\modules\reference\models\ArticlesSearch;

/* @var \yii\web\View $this */
/* @var string $inputId */

if (empty($type) || $type == 'expenditure') {
    $type = 'expenditure';
    $articleType = ArticlesSearch::TYPE_EXPENSE;
} else {
    $type = 'income';
    $articleType = ArticlesSearch::TYPE_INCOME;
}
?>

<?php if (true || Yii::$app->getUser()->can(UserRole::ROLE_CHIEF)) : ?>

<script type="text/javascript">
    $(document).on('change', '#<?= $inputId ?>', function(e)
    {
        const url = '<?= Url::to(['/reference/articles/create-from-dropdown', 'type' => $articleType, 'inputSelector' => $inputId]) ?>';
        const title = '<?= $type == 'income' ? 'Добавить статью прихода' : 'Добавить статью расхода' ?>';

        if (this.value === 'add-modal-<?=($type)?>-item') {
            e.preventDefault();
            $('#<?= $inputId ?>').val('').trigger('change');

            $.pjax({url: url, container: '#article-dropdown-form-container', push: false});
            $(document).on('pjax:success', '#article-dropdown-form-container', function(e, data) {
                $('#<?= $inputId ?>').select2('close');
                $('#article-dropdown-modal-container .modal-title').text(title);
                $('#article-dropdown-modal-container').modal();
            });
        }
    });
</script>

<?php endif ?>