<?php

use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data array|null */

$canCompany = YII_ENV_DEV || (Yii::$app->user->identity->company->id ?? null) == 486;
$hasData = strval($data['kubBalance'] ?? "") !== "" && strval($data['bankBalance'] ?? "") !== "";
$visible = $canCompany && $hasData && $data['kubBalance'] != $data['bankBalance'];
?>

<?php if ($visible) : ?>
    <?php $this->registerJs("
        $('#stmt_upload_balance_modal-toggle').toggleClass('hidden', false);
    ") ?>
    <?php Modal::begin([
        'id' => 'stmt_upload_balance_modal',
        'title' => 'Остаток денег по банку не совпадает с остатком в КУБ24',
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'clientOptions' => [
            'show' => true,
        ],
    ]) ?>
        <table>
            <tbody>
                <tr>
                    <td class="p-1" colspan="3" style="font-weight: bold;">
                        <?= $data['account']['bank_name'] ?? '' ?>
                        <?= $data['account']['rs'] ?? '' ?>
                    </td>
                </tr>
                <tr>
                    <td class="text-nowrap p-1">
                        Остаток по банку на <?= $data['date'] ?>:
                    </td>
                    <td class="text-nowrap p-1 text-right">
                        <?= Yii::$app->formatter->asMoney($data['bankBalance']) ?>
                        ₽
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-nowrap p-1">
                        Остаток в КУБ24:
                    </td>
                    <td class="text-nowrap p-1 text-right">
                        <?= Yii::$app->formatter->asMoney($data['kubBalance']) ?>
                        ₽
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-nowrap p-1" style="font-weight: bold;">
                        Разница:
                    </td>
                    <td class="text-nowrap p-1 text-right">
                        <span class="text-danger">
                            <?= Yii::$app->formatter->asMoney($data['bankBalance']-$data['kubBalance']) ?>
                        </span>
                        ₽
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-style table-count-list border mt-3">
            <thead>
                <tr class="heading">
                    <th class="text-nowrap">
                        №№
                    </th>
                    <th class="text-nowrap">
                        Возможные причины
                    </th>
                    <th class="text-nowrap">
                        Что делать
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        Какая-то банковская операция задублировалась.
                    </td>
                    <td rowspan="2">
                        Справа кнопка «Еще» - нажмите «Поиск дублей».
                        Внимательно проверьте и удалите дубли операции.
                    </td>
                </tr>
                <tr>
                    <td>
                        2
                    </td>
                    <td>
                        Вы руками проставили оплату счета и по выписке операция задублировалась.
                    </td>
                </tr>
                <tr>
                    <td>
                        3
                    </td>
                    <td>
                        Банк списал комиссию задним числом.
                    </td>
                    <td rowspan="2">
                        Нужно загрузить выписку за предыдущий период.
                    </td>
                </tr>
                <tr>
                    <td>
                        4
                    </td>
                    <td>
                        Вы удалили операцию.
                    </td>
                </tr>
                <tr>
                    <td>
                        5
                    </td>
                    <td>
                        Не правильно заведен «Баланс начальный».
                    </td>
                    <td>
                        Нужно проверить «Баланс на начало» на самую первую дату загрузки выписки в КУБ24.
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="mt-3">
            Если не сможете сами идентифицировать расхождение, то напишите в техподдержку.
        </div>
        <div class="text-center mt-3">
            <?= Html::button('OK', [
                'class' => 'button-regular button-hover-transparent button-regular_padding_bigger',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    <?php Modal::end() ?>
<?php endif ?>
