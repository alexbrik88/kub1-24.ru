<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$rs = ArrayHelper::getValue(json_decode(base64_decode(Yii::$app->request->get('p')), true), 'rs');
$bankingAccount = null;
$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;

if ($rs && $rs == 'all') {
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
}
elseif ($rs && $bankingAccountsArray) {
    foreach ((array)$bankingAccountsArray as $account) {
        if ($account->rs == $rs) {
            $bankingAccount = $account;
            break;
        }
    }
}

$bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$module}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
        'show_no_bank' => true
    ]);

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'is_special_offer' => 1,
    'bik' => Banking::bikList(),
])->all();
?>

<h4 class="modal-title">Загрузка выписки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row pb-1">
        <div class="form-group col-6 mb-0">
            <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button"
                onclick="$(this).siblings('.add-vidimus-file').first().click()">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Загрузить файл</span>
            </button>

            <button class="add-vidimus-file" style="display: none"></button>

            <br><br>
            <div class="upload-1C-hide">
                Выгрузите из Клиент-Банка файл формата 1С <br>
                (txt файл) и загрузите его сюда.
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-show" style="display: none; ">
            <div class="upload-button text-right">
                <a href="#" class="button-clr button-regular button-regular_red button-width disabled">
                    Загрузить
                </a>
            </div>
        </div>
    </div>
    <?= $this->render('_vidimus_upload') ?>
</div>