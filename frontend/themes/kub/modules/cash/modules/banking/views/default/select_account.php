<?php
/**
 * @var $this  yii\web\View
 * @var $model common\components\banking\AbstractService||null
 */

use backend\models\Bank;
use common\models\Contractor;
use common\models\cash\CashBankReasonType;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \common\models\company\CheckingAccountant;

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'bik' => Banking::bikList(),
])->indexBy('bik')->all();

$rs = ArrayHelper::getValue(json_decode(base64_decode(Yii::$app->request->get('p')), true), 'rs');
$bankingAccount = null;
$mainAccount = Yii::$app->user->identity->company->mainCheckingAccountant;
$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;

if ($rs && $rs == 'all') {
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
}
elseif ($rs) {
    if ($bankingAccount = CheckingAccountant::findOne(['company_id' => Yii::$app->user->identity->company->id, 'rs' => $rs])) {
        $bankName = $bankingAccount->bank_name;
    }
}

if (!$bankingAccount) {
    $bankName = ($mainAccount && $mainAccount->bank) ? $mainAccount->bank->name : null;
    //$bankUrl = ($mainAccount && $mainAccount->sysBank) ? $mainAccount->sysBank->url : null;
}
?>

<h4 class="modal-title">Загрузка выписки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row pb-1">
        <div class="form-group column mb-0">
            <button class="add-vidimus-file  button-regular button-regular_padding_bigger button-clr" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Загрузить файл</span>
            </button>
            <div class="upload-button upload-1C-show" style="display: none">
                <a href="#" class="button-clr button-regular button-regular_red disabled">
                    Загрузить
                </a>
            </div>
        </div>

        <div class="upload-1C-hide" style="width: 100%">
            <div class="col-12">
                <div class="alert alert-warning p-3 mt-3 mb-3" role="alert">
                    <div class="d-flex flex-nowrap p-1">
                        <svg class="svg-icon flex-shrink-0 mr-2">
                            <use xlink:href="/img/svg/svgSprite.svg#attention"></use>
                        </svg>
                        <div class="d-flex flex-column justify-content-center flex-grow-1">
                            <div>
                                УПС =(
                                <?php if ($bankName) : ?>
                                    С <b><?= $bankName ?></b> мы еще не интегрировались
                                <?php else : ?>
                                    С вашим банком мы еще не интегрировались
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                Выгрузите из Клиент-Банка выписку в формате 1С и загрузите этот файл с помощью левой кнопки.
                <strong class="d-block mb-3">Мы уже интегрировались и автоматически загружаем выписки из банков партнеров:</strong>
            </div>
            <div class="row pt-2" style="margin-bottom: -15px;">
                <?php
                // custom order banks
                $bikOrder = ['044525225', '044525985', '044525974', '044525999', '044525092', '044525593'];
                foreach ($bikOrder as $bankBik) {
                    if (isset($bankArray[$bankBik])) {
                        echo $this->render('_select_account_item', ['model' => $bankArray[$bankBik]]);
                        unset($bankArray[$bankBik]);
                    }
                }
                // other
                foreach ($bankArray as $bank) {
                    echo $this->render('_select_account_item', ['model' => $bank]);
                }
                ?>
            </div>
        </div>
    </div>

    <?= $this->render('_vidimus_upload') ?>

</div>