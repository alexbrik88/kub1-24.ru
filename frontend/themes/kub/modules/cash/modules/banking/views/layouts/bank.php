<?php
/**
 * @var $this  yii\web\View
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

if ($bankModel = ArrayHelper::getValue($this->params, 'bankingModel')) {
    $bankingUrl = Url::to([
        "/cash/banking/{$bankModel::$alias}/default/index",
        'account_id' => $bankModel->account_id,
        'p' => Yii::$app->request->get('p'),
    ]);
} else {
    $bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
    $bankingUrl = $bankingAccount && ($alias = Banking::aliasByBik($bankingAccount->bik)) ? Url::to([
        "/cash/banking/{$alias}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) : Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);
}
$bankName = Html::encode(ArrayHelper::getValue($bankModel, ['bank', 'name'], ''));

$this->beginContent('@frontend/modules/cash/modules/banking/views/layouts/main.php');
?>

<h4 class="modal-title">Запрос выписки из банка</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body">

    <div class="bank-form">
        <div class="row pb-3">
            <?php if ($bankModel && ($bankPartner = $bankModel->bankPartner)) : ?>
                <div class="col-5 d-flex flex-column <?= ($bankModel->showSecureText) ? 'align-items-center justify-content-center' : '' ?>">
                    <div class="logo-border text-center">
                        <?= ImageHelper::getThumb($bankPartner->getUploadDirectory() . $bankPartner->logo_link, [250, 150], [
                            'class' => 'flex-shrink-0',
                        ]); ?>
                    </div>
                </div>
                <?php if ($bankModel->showSecureText) : ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3">
                            <div class="p-1">
                                Для обеспечения безопасности данных используется
                                протокол зашифрованного соединения. <br/>
                                SSL - надежный протокол для передачи конфиденциальной
                                банковской информации и соблюдаются требования
                                международного стандарта PCI DSS по хранению и передаче
                                конфиденциальной информации в банковской сфере.
                            </div>
                        </div>
                    </div>
                    <style>
                        .bank-form .logo-border {
                            border: none;
                            border-radius: unset;
                            align-items: unset;
                            display: unset;
                        }
                        .bank-form .logo-border img {
                            height: unset;
                        }
                    </style>
                <?php else: ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3" style="text-align: right; height: 100px">
                            <span style="font-weight: bold">
                                <?= $bankName ?><br>
                                <?= $bankModel->currentAccount ? 'р/с ' . $bankModel->currentAccount->rs : ''; ?>
                            </span>
                        </div>
                    </div>
                    <style>
                        .bank-form .logo-border {
                            border: 1px solid #ddd;
                            border-radius: 8px;
                            align-items: center;
                            display: flex;
                        }
                        .bank-form .logo-border img {
                            height: 98px;
                        }
                    </style>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>

    </div>

</div>

<?php if (Yii::$app->request->get('start_load_urgently')): ?>
    <script>
        $(document).ready(function() {
            $('#statement-request-form').submit();
        });
    </script>
<?php endif; ?>

<?php $this->endContent(); ?>
