<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\dictionary\bik\BikDictionary
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div>
    <?= Html::a(Html::encode($model->name), [
        "bank{$model->bik}/default/index",
        'p' => Yii::$app->request->get('p'),
    ], [
        'class' => 'banking-module-link',
    ]) ?>
</div>