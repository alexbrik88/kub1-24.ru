<?php
use yii\helpers\Html;
use yii\helpers\Url;

$action = Url::to([
    '/cash/banking/default/file-upload/',
    'p' => Yii::$app->request->get('p'),
])
?>

<div class="row upload-1C-show" style="display: none;">
    <div class="col-12">
        <div class="vidimus-error">Что-то пошло не так :( Попробуйте загрузить файлы еще раз</div>
        <div class="row">
            <form enctype="multipart/form-data" action="<?= $action ?>" class="upload-txt" method="post" id="vidimus-ajax-form">
                <div class="file-list col-12">
                </div>
            </form>
            <form enctype="multipart/form-data" action="<?= $action ?>" class="upload-xls" method="post" id="vidimus-ajax-form-xls">
                <div class="file-list col-12">
                </div>
            </form>
            <div class="vidimus-loader"></div>
        </div>
    </div>
    <div class="col-12">
        <div id="progressBox"></div>
    </div>

    <div class="col-12">
        <span class="vidimus-hr"></span>
    </div>
    <div class="col-12 upload-1C-show">
        <div id="vidimus-table"></div>
    </div>
<?php if (\Yii::$app->request->isAjax) : ?>
    <script type="text/javascript">
        initialiseVidimusUploader();
        initialiseVidimusXlsUploader();
    </script>
<?php endif; ?>
</div>