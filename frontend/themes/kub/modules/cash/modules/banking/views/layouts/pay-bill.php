<?php

use frontend\themes\kub\assets\KubAsset;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$canSave = ArrayHelper::getValue($this->params, 'canSave', false);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<?php NavBar::begin([
    'brandLabel' => 'Сервис выставления счетов',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-fixed-top',
        'style' => 'border-bottom: 1px solid #ccc;',
    ],
]); ?>

<?php NavBar::end(); ?>

<div class="page-content">
    <?= $content ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
