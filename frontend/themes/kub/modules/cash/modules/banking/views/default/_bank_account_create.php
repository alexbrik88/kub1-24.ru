<?php
use frontend\widgets\Alert;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $bank backend\models\Bank */
/* @var $model common\models\company\ApplicationToBank */

?>

<h4 class="modal-title">Заявка на открытие расчетного счета</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <?php $form = ActiveForm::begin([
        'action' => [
            'account',
            'bankId' => $bank->id,
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'id' => 'form-apply-bank-' . $bank->id,
            'data' => [
                'pjax' => true,
                'pjax-container' => 'banking-module-pjax'
            ]
        ],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'enableAjaxValidation' => true,
    ]); ?>

        <div class="form-body">
            <?php if ($bank->description) : ?>
                <div class="description-bank mb-3">
                    <?= nl2br($bank->description); ?>
                </div>
            <?php endif; ?>

            <?= Alert::widget(); ?>

            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'company_name'); ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'inn'); ?>
                </div>
                <div class="col-3">
                    <?= $form->field($model, 'kpp'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'legal_address'); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'fio'); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <?= $form->field($model, 'contact_phone')->textInput(['data-inputmask' => "'mask': '+7(9{3}) 9{3}-9{2}-9{2}'"]); ?>
                </div>
                <div class="col-6">
                    <?= $form->field($model, 'contact_email'); ?>
                </div>
            </div>

            <div class="license-bank mb-3">
                <span>
                <?= 'Нажимая кнопку «Открыть счет», вы даете свое согласие на ',
                    Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank']),
                    " и отправку этих данных в банк «{$bank->bank_name}»."; ?>
                </span>
            </div>

            <div class="d-flex justify-content-between">
                <?= Html::submitButton('Открыть счет', [
                    'class' => 'button-regular button-width button-regular_red button-clr',
                ]); ?>
                <?= Html::a('Отменить', [
                    '/cash/banking/default/index',
                    'p' => Yii::$app->request->get('p'),
                ], [
                    'class' => 'banking-module-link banking-cancel button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>

        </div>

    <?php $form->end(); ?>

</div>