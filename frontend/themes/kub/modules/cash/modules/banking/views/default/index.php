<?php

use backend\models\Bank;
use frontend\components\XlsHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\kub\widgets\VideoInstructionWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$rs = ArrayHelper::getValue(json_decode(base64_decode(Yii::$app->request->get('p')), true), 'rs');
$bankingAccount = null;
$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;

if (!$rs || $rs == 'all') {
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
} elseif ($rs && $bankingAccountsArray) {
    foreach ((array)$bankingAccountsArray as $account) {
        if ($account->rs == $rs) {
            $bankingAccount = $account;
            break;
        }
    }
}

$bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$module}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
        'show_no_bank' => true
    ]);

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'is_special_offer' => 1,
    'bik' => Banking::bikList(),
])->all();
?>

<h4 class="modal-title">Загрузка выписки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row pb-1">
        <div class="form-group col-6 mb-0 upload-txt">
            <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button"
                onclick="$(this).siblings('.add-vidimus-file').first().click()">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Загрузить файл *.txt</span>
            </button>

            <button class="add-vidimus-file" style="display: none"></button>

            <br><br>
            <div class="upload-1C-hide">
                Выгрузите из Клиент-Банка файл формата 1С <br>
                (txt файл) и загрузите его сюда.
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-hide">
            <a href="<?= $bankingUrl ?>" class="banking-module-link button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#bank-3"></use>
                </svg>
                <span>Напрямую из банка</span>
            </a>
            <br><br>
            <div>
                Мы уже интегрировались с несколькими банками. <br>
                Если у вас есть счет в этом банке, то выписка будет <br>
                загружаться напрямую из банка.
            </div>
            <div style="position: absolute; top:0; right:15px;">
                <?php VideoInstructionWidget::setModalNumber(1) ?>
                <?= VideoInstructionWidget::widget([
                    'buttonTitle' => 'Видео инструкция',
                    'title' => 'Интеграция с банком для автоматической загрузки выписки на примере Сбербанка',
                    'titleWidth' => '85%',
                    'width' => '100%',
                    'height' => '500px',
                    'src' => '/video/integration_with_sberbank.mp4'
                ]) ?>
            </div>

        </div>
        <div class="form-group col-6 mb-0 upload-xls">
            <div class="form-group upload-1C-hide">
                <button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreDetails" aria-expanded="false" aria-controls="moreDetails">
                    <span class="link-txt">Загрузка выписки из иностранного банка</span>
                    <svg class="link-shevron svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                    </svg>
                </button>
            </div>
            <div class="collapse in" id="moreDetails">
                <button class="button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button"
                        onclick="$(this).siblings('.add-vidimus-xls-file').first().click()">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Загрузить файл Excel</span>
                </button>

                <button class="add-vidimus-xls-file" style="display: none"></button>

                <br><br>
                <div class="upload-1C-hide">
                    Если нет формата 1С (txt файл), то воспользуйтесь<br>
                    шаблоном ниже. Подходит для загрузки выписки из<br>
                    банков по всему миру.<br><br>
                    <?= Html::a('Скачать шаблон Excel', Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_EXCERPT])) ?>
                </div>
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-show" style="display: none; ">
            <div class="upload-button text-right">
                <a href="#" class="button-clr button-regular button-regular_red button-width disabled">
                    Загрузить
                </a>
            </div>
        </div>
        <?php /*
        <div class="col-12 mt-3">
            Выгрузите из Клиент-Банка выписку в формате 1С и загрузите этот файл с помощью левой кнопки.
            <strong class="d-block mb-3">Мы уже интегрировались и автоматически загружаем выписки из банков партнеров:</strong>
        </div>*/ ?>

    </div>
    <?php /*
    <div class="row pt-2" style="margin-bottom: -15px;">
        <?php
        foreach ($bankArray as $bank) {
            echo $this->render('_select_account_item', ['model' => $bank]);
        }
        ?>
    </div>
    */ ?>

    <?= $this->render('_vidimus_upload') ?>
</div>
