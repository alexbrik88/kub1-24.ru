<?php

use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\Banking;

/* @var $cancelUrl array|string */

?>

<div class="row pt-3">
    <div class="col-4">
        <?= Html::submitButton('Подтвердить интеграцию', [
            'class' => 'button-clr button-regular button-regular_red w-100'
        ]) ?>
    </div>
    <div class="column ml-auto">
        <?php if (Banking::isCashOperationsPage()): ?>

            <?= Html::a('Отменить', 'javascript:void(0)', [
                'class' => 'button-regular button-hover-transparent button-clr button-width banking-module-close-link',
            ]) ?>

        <?php else: ?>

            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-regular button-hover-transparent button-clr button-width banking-module-link banking-cancel',
            ]) ?>

        <?php endif; ?>
    </div>
</div>