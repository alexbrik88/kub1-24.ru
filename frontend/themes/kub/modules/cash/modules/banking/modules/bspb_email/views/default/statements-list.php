<?php

use common\components\date\DateHelper;
use frontend\modules\cash\modules\banking\models\BankingEmail;
use frontend\modules\cash\modules\banking\modules\bspb_email\models\BankModel;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var BankingEmail $bankingEmail
 * @var BankModel $model;
 * @var array $filesList
 */

$p = Yii::$app->request->get('p');
$manyUploadUrl = ['/cash/banking/bspb_email/default/upload-file', 'p' => $p, 'account_id' => $model->account_id];
$manyDeleteUrl = ['/cash/banking/bspb_email/default/delete-file', 'p' => $p, 'account_id' => $model->account_id];
$autoUploadUrl = ['/cash/banking/bspb_email/default/toggle-autoload-mode', 'p' => $p, 'account_id' => $model->account_id];
$isAutoLoad = $bankingEmail->isAutoloadMode;
?>

<!-- Statements table -->
<table class="table table-style table-count-list table-compact table-bleak">
    <tr>
        <th width="1%" style="padding-left: 10px">
            <?= Html::checkbox('-', false, ['class' => 'all-statement-checkbox']) ?>
        </th>
        <th>Дата</th>
        <th>Период</th>
        <th>Кол-во операций</th>
        <th></th>
        <th></th>
    </tr>

    <?php if (empty($filesList)): ?>
        <tr>
            <td colspan="6">
                Нет новых выписок.
            </td>
        </tr>
    <?php else: ?>
        <?php foreach ($filesList as $s): ?>
            <?php $uploadUrl = $manyUploadUrl + ['file_id[]' => $s['id']]; ?>
            <?php $deleteUrl = $manyDeleteUrl + ['file_id[]' => $s['id']]; ?>
            <tr>
                <td>
                    <?= Html::checkbox('-', false, ['class' => 'statement-checkbox', 'data-id' => $s['id'], 'data-statements_count' => $s['statements_count']]) ?>
                </td>
                <td>
                    <?= DateHelper::format($s['date'], 'd.m.Y', 'Y-m-d') ?>
                </td>
                <td>
                    <?= DateHelper::format($s['period_from'], 'd.m.Y', 'Y-m-d') ?>
                    -
                    <?= DateHelper::format($s['period_to'], 'd.m.Y', 'Y-m-d') ?>
                </td>
                <td class="text-right">
                    <?= $s['statements_count'] ?>
                </td>
                <td class="text-center">
                    <?= Html::a('Просмотреть и загрузить', $uploadUrl, ['class' => 'link banking-module-link']) ?>
                </td>
                <td class="text-center">
                    <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                        'class' => 'link delete-statement-button',
                        'data-pjax' => 0,
                        'data-href' => Url::to($deleteUrl),
                        'title' => 'Удалить'
                    ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
</table>

<?php if (count($filesList)): ?>

<!-- Summary in modal -->
<div id="modal-summary-container" style="display: none">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3 pt-2 pb-2">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Количество операций: <strong class="total-statements-count ml-1">0</strong>
        </div>
        <div class="column ml-auto">
            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'video-instruction-2']) . '<span>Автозагрузка</span>', [
                'class' => 'auto-upload-statement-button button-regular button-clr ' . ($isAutoLoad ? 'button-regular_red' : 'button-hover-transparent'),
                'style' => 'width:170px',
                'data-href' => Url::to($autoUploadUrl),
                'data-pjax' => 0,
                'data-autoload' => ($isAutoLoad) ? '1' : '',
                'title' => ($isAutoLoad)
                    ? 'По нажатии на кнопку автозагрузка отключится'
                    : 'Выписки будут загружаться автоматически'
            ]); ?>
            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'download']) . '<span>Загрузить</span>', [
                'class' => 'many-upload-statement-button button-regular button-hover-transparent button-clr button-width',
                'data-href' => Url::to($manyUploadUrl)
            ]) ?>
            <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>', [
                'class' => 'many-delete-statement-button button-regular button-hover-transparent button-clr button-width',
                'data-pjax' => 0,
                'data-href' => Url::to($manyDeleteUrl),
            ]) ?>
        </div>
    </div>
</div>

<?php else: ?>

<div class="row">
    <div class="column ml-auto">
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'video-instruction-2']) . '<span>Автозагрузка</span>', [
            'class' => 'auto-upload-statement-button button-regular button-clr ' . ($isAutoLoad ? 'button-regular_red' : 'button-hover-transparent'),
            'style' => 'width:170px',
            'data-href' => Url::to($autoUploadUrl),
            'data-pjax' => 0,
            'data-autoload' => ($isAutoLoad) ? '1' : '',
            'title' => ($isAutoLoad)
                ? 'По нажатии на кнопку автозагрузка отключится'
                : 'Выписки будут загружаться автоматически'
        ]); ?>
    </div>
</div>

<?php endif; ?>

<!-- Confirm modal -->
<div id="dialog-banking-email-list" class="confirm-modal modal fade" style="margin-left:-7px">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">?</h4>
                <div class="text-center">
                    <a class="btn-yes banking-module-link button-clr button-regular button-hover-transparent button-width-medium mr-2 link-bleak" href="#">Да</a>
                    <button class="btn-no button-clr button-regular button-hover-transparent button-width-medium ml-1 link-bleak" type="button" onclick="$(this).closest('.modal').modal('hide')">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script>
    // delete one
    $('.delete-statement-button').on('click', function() {
        const modal = $('#dialog-banking-email-list');
        modal.find('.modal-title').html('Вы уверены, что хотите удалить выписку?');
        modal.find('.btn-yes').attr('href', $(this).data('href'));
        modal.modal('show');
    });
    // delete some
    $('.many-delete-statement-button').on('click', function() {
        const modal = $('#dialog-banking-email-list');
        const selectedCheckboxes = $('#banking-module-modal').find('.statement-checkbox').filter(':checked');
        modal.find('.modal-title').html('Вы уверены, что хотите удалить выбранные выписки?');

        let selectedIds = [];
        $(selectedCheckboxes).each(function(i, cbx) {
            selectedIds.push('file_id[]=' + $(cbx).data('id'));
        });

        modal.find('.btn-yes').attr('href', $(this).data('href') + '&' + selectedIds.join('&'));
        modal.modal('show');
    });
    // upload some
    $('.many-upload-statement-button').on('click', function() {
        const modal = $('#dialog-banking-email-list');
        const selectedCheckboxes = $('#banking-module-modal').find('.statement-checkbox').filter(':checked');
        modal.find('.modal-title').html('Вы уверены, что хотите загрузить выбранные выписки?');

        let selectedIds = [];
        $(selectedCheckboxes).each(function(i, cbx) {
            selectedIds.push('file_id[]=' + $(cbx).data('id'));
        });

        modal.find('.btn-yes').attr('href', $(this).data('href') + '&' + selectedIds.join('&'));
        modal.modal('show');
    });
    // toggle autoload
    $('.auto-upload-statement-button').on('click', function() {
        const modal = $('#dialog-banking-email-list');
        const isAutoload = $(this).data('autoload');
        modal.find('.modal-title').html('Вы уверены, что хотите ' + (isAutoload ? 'отключить' : 'включить') + ' автозагрузку?');
        modal.find('.btn-yes').attr('href', $(this).data('href'));
        modal.modal('show');
    });
    // change checkboxes
    $('.statement-checkbox').on('change', function() {
        const modal = $('#banking-module-modal');
        const summary = $('#modal-summary-container');
        const selectedCheckboxes = modal.find('.statement-checkbox').filter(':checked');
        if (selectedCheckboxes.length) {
            let total = selectedCheckboxes.length;
            let totalStatements = 0;
            $(selectedCheckboxes).each(function() {
                totalStatements += Number($(this).data('statements_count'));
            });
            summary.find('.total-count').html(total);
            summary.find('.total-statements-count').html(totalStatements);
            summary.show();
        } else {
            summary.hide();
            $('.all-statement-checkbox').prop('checked', false).uniform('refresh');
        }
    });
    // change all checkbox
    $('.all-statement-checkbox').on('change', function() {
        const modal = $('#banking-module-modal');
        const checkboxes = modal.find('.statement-checkbox');
        if ($(this).prop('checked')) {
            $(checkboxes).each(function() {
                if (!$(this).prop('checked'))
                    $(this).prop('checked', true).uniform('refresh');
            });
            $(checkboxes).first().trigger('change');
        } else {
            $(checkboxes).each(function() {
                if ($(this).prop('checked'))
                    $(this).prop('checked', false).uniform('refresh');
            });
            $(checkboxes).first().trigger('change');
        }
    });
</script>