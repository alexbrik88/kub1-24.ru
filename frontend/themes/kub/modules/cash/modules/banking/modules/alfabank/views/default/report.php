<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\alfabank\models\BankModel */

$url = Url::to([
    '/cash/bank/index',
    'p' => Yii::$app->request->get('p'),
]);
echo Html::script('$("#statement-request").modal("hide");window.location.href = "' . $url . '";', ['type' => "text/javascript"]);
echo Html::a('Новый запрос', [
    '/cash/banking/default/index',
    'p' => Yii::$app->request->get('p'),
], [
    'class' => 'btn btn-primary banking-module-link banking-cancel',
]);
