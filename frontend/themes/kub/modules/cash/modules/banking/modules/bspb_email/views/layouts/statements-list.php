<?php

/**
 * @var $this yii\web\View
 * @var $filesList array
 */

$this->beginContent('@frontend/modules/cash/modules/banking/views/layouts/main.php');
?>

<h4 class="modal-title">Полученные, но не загруженные в КУБ24 выписки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body">

    <?= $content ?>

</div>

<?php $this->endContent(); ?>
