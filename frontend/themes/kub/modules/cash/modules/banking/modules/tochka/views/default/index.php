<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\tochka\models\BankModel */

?>

<div id='statement-request-form-container' style="overflow-y: auto; overflow-x: hidden;">
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'index',
            'account_id' => Yii::$app->request->get('account_id'),
            'redirect_url' => Yii::$app->request->get('redirect_url'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row pt-3">
        <div class="col-6">
            <?= AccountSelectWidget::widget([
                'form' => $form,
                'bankModel' => $model,
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
        <div class="col-12" style="font-size: 14px;">
            Для автоматической загрузки выписки по счёту, нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в интернет-банке
            <b><?= $model->getBankName(); ?></b>. После этого вы будете перенаправлены обратно в сервис для загрузки
            банковских операций.
        </div>
    </div>

    <?= $form->field($model, 'auth_redirect', ['template' => "{input}"])->hiddenInput(['value' => 1]); ?>

    <?= $this->render('@banking/views/all-banks/buttons/auth', [
        'cancelUrl' => [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ]
    ]) ?>

    <?php $form->end() ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>
