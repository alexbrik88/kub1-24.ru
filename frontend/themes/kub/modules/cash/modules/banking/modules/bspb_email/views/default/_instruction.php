<button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreBankDetails" aria-expanded="false" aria-controls="moreBankDetails">
    <span class="link-txt">Инструкция по автозагрузке выписки из Банка Санкт-Петербург</span>
    <?= frontend\components\Icon::get('shevron') ?>
</button>
<div class="collapse" id="moreBankDetails">
    <div class="font-14 mt-2">
        <div class="mb-1">
            1) В верхнем меню заходите в «Обзор». Там нажимаете на «Уведомлять по SMS/E-mail»<br/>
            <img class="pt-2 pb-2" src="/images/bspb_email/help1.jpg"/><br/>
        </div>
        <div class="mb-1">
            2) На странице Информирование, нажмите на вкладку «Подписка на выписку», затем на кнопку «Подключить»<br/>
            <img class="pt-2 pb-2" src="/images/bspb_email/help2.jpg"/><br/>
        </div>
        <div class="mb-1">
            3) Выберите нужный расчетный счет, период загрузки<br/>
            <img class="pt-2 pb-2" src="/images/bspb_email/help3.jpg"/><br/>
        </div>
        <div class="mb-1">
            4) Вид и формат обязательно указать Полная и 1С соответственно<br>
            <img class="pt-2 pb-2" src="/images/bspb_email/help4.jpg"/><br/>
        </div>
        <div class="mb-1">
            5) Введите E-mail выданный КУБом и нажмите на кнопку «Подключить»<br>
            <img class="pt-2 pb-2" src="/images/bspb_email/help5.jpg"/><br/>
        </div>
        <strong>Технологии рулят :)</strong>
    </div>
</div>