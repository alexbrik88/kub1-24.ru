<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cash\modules\banking\modules\otkrytiye\models\BankModel */

?>

<?php $form = ActiveForm::begin([
    'id' => 'statement-result-form',
    'action' => [
        'result',
        'account_id' => Yii::$app->request->get('account_id'),
        'p' => Yii::$app->request->get('p'),
    ],
    'options' => [
        'data' => [
            'status-ready' => $model->statusReady,
            'status-progress' => $model->statusProgress,
            'status-url' => Url::to([
                'status',
                'p' => Yii::$app->request->get('p'),
            ]),
            'pjax' => true,
        ]
    ]
]); ?>

    <?= $form->field($model, 'account_id')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'request_id')->hiddenInput()->label(false); ?>

    <div class="progress-container">
        <div class="formation-status form-group">
            <label class="control-label">
                <span>идет формирование выписки </span>
                <span class="progress-value"></span>
            </label>
            <div class="progress progress-striped active">
                <div class="progress-bar"
                     role="progressbar"
                     aria-valuenow="0" aria-valuemax="100" style="width: 0%;"></div>
            </div>
        </div>
        <?= Html::a('Отменить', [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'btn btn-primary banking-module-link banking-cancel',
            'style' => 'float:right;',
        ]); ?>
    </div>

<?php $form->end(); ?>

<?= $this->render('result_script') ?>
