<button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreBankDetails" aria-expanded="false" aria-controls="moreBankDetails">
    <span class="link-txt">Инструкция по автозагрузке выписки из Альфа банка</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
    </svg>
</button>
<div class="collapse" id="moreBankDetails">
    <div class="font-14 mt-2">
        <div class="mb-1">
            Данные шаги можно сделать как в «Новом интернет банке», так и в «Альфа-Бизнес Мобайле».<br/>
            Ниже шаги для «Нового интернет банка»
        </div>
        <div class="mb-1">
            1) В «Новом интернет банке» в левом меню заходите в «Выписки». Там нажимаете на «Рассылка выписки»<br/>
            <img class="pt-2 pb-2" src="/images/alfabank_email/help1.png"/><br/>
            <strong>Важно!</strong> Это новая функций в Альфа банке и если вы не видите «Рассылка выписки», то необходимо очистить кэш в вашем браузере.<br/>
        </div>
        <div class="mb-1">
            2) Далее нажимаете на кнопку «Создать рассылку»<br/>
            <img class="pt-2 pb-2" src="/images/alfabank_email/help2.png"/><br/>
        </div>
        <div class="mb-1">
            3) Далее<br/>
            <img class="pt-2 pb-2" src="/images/alfabank_email/help3.png"/><br/>
        </div>
        <div class="mb-1">
            4) На момент создания данной инструкции 03.02.2021, стоимость рассылки выписки у Альфа банка  – бесплатно.
        </div>
        <div class="mb-1">
            5) Возвращаетесь в КУБ24, ставите галочку ниже и нажимаете на кнопку «Подтверждаю получение выписки».
        </div>
        <div class="mb-1">
            6) В соответствии с выбранной вами периодичностью приходит выписка в КУБ24.
        </div>
        <strong>Технологии рулят :)</strong>
    </div>
</div>