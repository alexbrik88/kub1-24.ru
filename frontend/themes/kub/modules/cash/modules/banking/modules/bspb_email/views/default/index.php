<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\BankingEmail;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\bspb_email\models\BankModel */

$p = Yii::$app->request->get('p');
$existsEmailModel = ($model->account_id) ? BankingEmail::findOne(['checking_accountant_id' => $model->account_id]) : null;
$model->email = ($existsEmailModel) ? $existsEmailModel->email : $model->generateEmail();
?>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'confirm-email',
            'p' => $p,
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <?php  if ($model->currentAccount) {
        echo Html::hiddenInput('account', $model->currentAccount->rs, ['id' => 'currentAccount']);
    } ?>

    <div class="row pt-3">
        <div class="col-6">
            <?= AccountSelectWidget::widget([
                'form' => $form,
                'bankModel' => $model,
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'email')->textInput(['readonly' => 1]) ?>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-12">
            <?= $this->render('_instruction') ?>
        </div>
    </div>

    <?php if ($existsEmailModel): ?>

        <div class="row">
            <div class="column">
                <div class="banking-email-enabled">
                    <label>
                    <?= Html::checkbox('-', true, [
                        'class' => 'disable-banking-email-btn',
                        'style' => 'margin-top:-1px',
                        'data-href' => Url::to(['/cash/banking/bspb_email/default/drop-email', 'p' => $p, 'account_id' => $model->account_id])
                    ]) ?>

                        E-mail включен.

                    </label>
                </div>
            </div>
            <div class="column ml-auto">
                <?= Html::a('ОК', 'javascript:void(0)', [
                    'class' => 'button-regular button-hover-transparent button-clr button-width',
                    'style' => 'width: 120px!important;',
                    'data-dismiss' => 'modal'
                ]); ?>
            </div>
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-12 mt-2">
                <?= $form->field($model, 'is_confirmed')
                    ->label('Указал в банке e-mail для отправки выписки в КУБ24')
                    ->checkbox() ?>
            </div>
        </div>

        <div class="row pb-3">
            <div class="column">
                <?= Html::submitButton('Подтверждаю получение выписки', [
                    'class' => 'button-clr button-regular button-regular_red w-100',
                    'style' => '',
                ]); ?>
            </div>
            <div class="column ml-auto">
                <?php if (Banking::isCashOperationsPage()): ?>

                    <?= Html::a('Отменить', 'javascript:void(0)', [
                        'class' => 'button-regular button-hover-transparent button-clr button-width banking-module-close-link',
                        'style' => 'width: 120px!important;',
                    ]); ?>

                <?php else: ?>

                    <?= Html::a('Отменить', [
                        '/cash/banking/default/index',
                        'p' => Yii::$app->request->get('p'),
                    ], [
                        'class' => 'button-regular button-hover-transparent button-clr button-width banking-cancel',
                        'style' => 'width: 120px!important;',
                    ]); ?>

                <?php endif; ?>
            </div>
        </div>

    <?php endif; ?>

    <?php $form->end() ?>

</div>

<!-- Confirm modal -->
<div id="dialog-banking-email" class="confirm-modal modal fade" style="margin-left:-7px">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите отключить получение выписок из Банк Санкт-Петербург в КУБ24?
                </h4>
                <div class="text-center">
                    <a class="btn-yes banking-module-link button-clr button-regular button-hover-transparent button-width-medium mr-2 link-bleak" href="#">Да</a>
                    <button class="btn-no button-clr button-regular button-hover-transparent button-width-medium ml-1 link-bleak" type="button" onclick="$(this).closest('.modal').modal('hide')">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.disable-banking-email-btn').on('click', function() {
        const modal = $('#dialog-banking-email');
        modal.find('.btn-yes').attr('href', $(this).data('href'));
        modal.modal('show');
    });
    $('#dialog-banking-email .btn-no').on('click', function() {
        $('.disable-banking-email-btn').prop('checked', true).uniform('refresh');
    });
</script>