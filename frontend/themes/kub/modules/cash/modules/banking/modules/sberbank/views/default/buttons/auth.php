<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $cancelUrl array|string */

?>
<style>
    .tooltipster-kub.tooltipster-top {margin-left:2px;}
</style>

<div class="row pt-3 align-items-end">
    <div class="col-4">
        <div id="sberbank-choose-token" class="collapse mb-2" style="text-align:center;">
            <div class="mar-b-5 text-bold text-center">У вас есть токен?</div>
            <div class="btn-group" style="border: 1px solid #ddd; border-radius:4px;">
                <button class="btn sberbank-token">Да</button>
                <button class="btn sberbank-web">Нет</button>
            </div>
        </div>
        <?= Html::button('Подтвердить интеграцию', [
            'class' => 'button-clr button-regular button-regular_red w-100 tooltip-sberbank-token collapsed',
            'data-target' => '#sberbank-choose-token',
            'data-toggle' => 'collapse'
        ]) ?>
    </div>
    <div class="column ml-auto">

        <?php if (Banking::isCashOperationsPage()): ?>

            <?= Html::a('Отменить', 'javascript:void(0)', [
                'class' => 'button-regular button-hover-transparent button-clr button-width banking-module-close-link',
            ]) ?>

        <?php else: ?>

            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-regular button-hover-transparent button-clr button-width banking-module-link banking-cancel',
            ]) ?>

        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click', '.sberbank-token', function() {
            $('#bankmodel-user_type').val('1');
            $('#statement-request-form').submit();
        });
        $(document).on('click', '.sberbank-web', function() {
            $('#bankmodel-user_type').val('0');
            $('#statement-request-form').submit();
        });
    });
</script>