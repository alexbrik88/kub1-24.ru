<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdModel
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div id='statement-request-form-container' style="overflow-y: hidden; overflow-x: hidden;">
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'index',
            'cashbox_id' => Yii::$app->request->get('cashbox_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row">
        <div class="col-6">

        </div>
        <div class="col-12" style="font-size: 14px;">
            Для автоматической загрузки выписки нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в ОФД <b style="display: contents"><?= $model->getOfdName(); ?></b>. <br>
            После этого вы будете перенаправлены обратно в сервис для загрузки операций.
        </div>
    </div>

    <?= $form->field($model, 'auth_redirect', ['template' => "{input}"])->hiddenInput(['value' => 1]); ?>

    <?= $this->render('@ofd/views/default/buttons/auth', [
        'cancelUrl' => [
            '/cash/ofd/default/index',
            'p' => Yii::$app->request->get('p'),
        ]
    ]) ?>

    <?php $form->end() ?>

</div>

<?= $this->render('@ofd/views/default/delete_ask') ?>