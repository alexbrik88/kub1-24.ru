<?php
/**
 * @var $this  yii\web\View
 * @var $declaration
 */
?>
<div id='statement-request-form-container' style="overflow-y: hidden; overflow-x: hidden;">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'import',
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row">
        <div class="col-6">

        </div>
        <div class="col-12" style="font-size: 14px;">
            Для загрузки налоговой декларации за <?= $declaration->tax_year ?> г. нужно ПОДТВЕРДИТЬ ИНТЕГРАЦИЮ в <b>Такском СПРИНТЕР</b>.<br/>
            После этого вы будете перенаправлены обратно в сервис.
        </div>
    </div>

    <?= $form->field($model, 'auth_redirect', ['template' => "{input}"])->hiddenInput(['value' => 1]); ?>

    <?= $this->render('@ofd/views/default/buttons/auth', [
        'cancelUrl' => [
            '/cash/ofd/default/import',
            'p' => Yii::$app->request->get('p'),
        ]
    ]) ?>

    <?php $form->end() ?>

</div>
