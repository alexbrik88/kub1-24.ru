<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $widget frontend\modules\cash\widgets\SummarySelectWidget */


$css = <<<STYLE
STYLE;

$js = <<<SCRIPT
$(document).on('change', '.joint-operation-checkbox', function(){

    const table = $(this).closest('table');
    const checkedCheckboxes = table.find('.joint-operation-checkbox').filter(':checked');
    const copyButton = $('#summary-container').find('#button-copy-flow-item');

    let countChecked = 0;
    let inSum = 0;
    let outSum = 0;
    let diff = 0;
    
    $(checkedCheckboxes).each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    
    diff = inSum - outSum;
    
    if (countChecked > 0) {
        // $('#summary-container').removeClass('hidden');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("div").hide();
        } else {
            $('#summary-container .total-difference').closest("div").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
    
    // COPY BUTTON
    if (copyButton.length) {
        if (checkedCheckboxes.length === 1 && $(checkedCheckboxes).data('copy-url')) {
            copyButton
                .attr('href', $(checkedCheckboxes).data('copy-url'))
                .addClass('ajax-modal-btn')
                .removeClass('hidden');
        } else {
            copyButton
                .attr('href', 'javascript:void(0)')
                .removeClass('ajax-modal-btn')
                .addClass('hidden');
        }
    }    
});
SCRIPT;

$this->registerCss($css);
$this->registerJs($js);
?>

<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column column-income total-txt-foot mr-3">
            Приход: <strong class="total-income ml-1">0</strong>
        </div>
        <div class="column column-expense total-txt-foot mr-3">
            Расход: <strong class="total-expense ml-1">0</strong>
        </div>
        <div style="display:none!important;" class="column total-txt-foot mr-3">
            Сальдо: <strong class="total-difference ml-1">0</strong>
        </div>
        <div class="column ml-auto"></div>

        <!-- beforeButtons -->
        <?php foreach (array_filter($widget->beforeButtons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>

        <!-- buttons -->
        <?php if ($widget->buttons): ?>
            <?php if ($widget->buttonsViewType == $widget::VIEW_DROPDOWN): ?>
                <div class="column">
                    <div class="dropup dropup-right-align">
                        <?= Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle button-width',
                            'data-toggle' => 'dropdown',
                        ]) ?>
                        <?= \yii\bootstrap4\Dropdown::widget([
                            'items' => array_filter(array_map(function($a) {
                                $a = preg_replace('/\s?<svg[^>]*?>.*?<\/svg>\s?/si', ' ', $a); // remove tag "svg"
                                $a = preg_replace('/(class="[^"]*")/', 'class="dropdown-item"', $a); // remove all link classes
                                return $a;
                            }, array_filter($widget->buttons))),
                            'options' => [
                                'class' => 'form-filter-list list-clr',
                                'style' => 'margin-left: -16px'
                            ],
                        ]) ?>
                    </div>
                </div>
            <?php else: ?>
                <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
                    <div class="column"><?= $button ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endif; ?>

        <!-- afterButtons -->
        <?php foreach (array_filter($widget->afterButtons) as $key => $button) : ?>
            <div class="column"><?= $button ?></div>
        <?php endforeach; ?>

    </div>
</div>
