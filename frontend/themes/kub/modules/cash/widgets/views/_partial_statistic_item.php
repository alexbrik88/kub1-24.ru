<?php

use common\components\TextHelper;
use yii\helpers\Html;

/**
 * @var $text string
 * @var $class string
 * @var $amount int
 * @var $statistic_text string
 * @var $statistic_text_class string
 * @var $last_item boolean
 * @var string $dataValue
 */

?>

<div class="count-card-column col-6">
    <div class="count-card <?= $class; ?> wrap"<?= ($dataValue !== '') ? sprintf(' data-value="%s"', Html::encode($dataValue)) : ''; ?>>
        <div class="count-card-main">
            <?= TextHelper::$format($amount, $decimals); ?>
            <?= $icon ?>
        </div>
        <div class="count-card-title d-inline-block text-truncate" title="<?= $text; ?>">
            <?= $text; ?>
        </div>
        <?php if ($last_item && (YII_ENV_DEV || (Yii::$app->user->identity->company->id ?? null) == 486)) : ?>
            <div id="stmt_upload_balance_modal-toggle" class="hidden" style="position: relative;">
                <?= Html::tag('span', 'Не сходится!', [
                    'data-toggle' => 'modal',
                    'data-target' => '#stmt_upload_balance_modal',
                    'class' => 'link',
                    'style' => [
                        'font-family' => '"Corpid E3 SCd", sans-serif',
                        'cursor' => 'pointer',
                        'position' => 'absolute',
                        'bottom' => '-18px',
                    ],
                ]) ?>
            </div>
        <?php endif ?>
        <hr>
        <div class="count-card-foot"><?= $statistic_text; ?></div>
    </div>
</div>

