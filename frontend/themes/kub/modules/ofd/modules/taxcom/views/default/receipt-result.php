<?php

use frontend\modules\ofd\modules\platforma\components\Helper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $result array */

$this->title = 'Загрузить чеки';

$newRequestUrl = Url::current([0 => 'receipt']);
?>

<div class="platforma-default-receipt-result">
    <table class="table table-style table-count-list">
        <thead>
            <tr>
                <th>Наименование ККТ</th>
                <th>Период</th>
                <th>Кол-во чеков</th>
                <th>На сумму</th>
                <th>Кол-во дублей</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?= Html::encode($result['kkt']->name) ?>
                </td>
                <td>
                    С
                    <?= $result['period']['from']->format('d.m.Y') ?>
                    <br>
                    по
                    <?= $result['period']['till']->format('d.m.Y') ?>
                </td>
                <td>
                    <?= $result['count'] ?>
                </td>
                <td>
                    <?= number_format($result['sum']/100, 2, ',', ' ') ?>
                </td>
                <td>
                    <?= $result['count'] - $result['insert'] ?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="d-flex justify-content-between mt-4">
        <?= Html::a('Новый запрос', $newRequestUrl, [
            'class' => 'button-regular button-regular_red min-w-130 ofd_module_link',
        ]); ?>
    </div>
</div>
