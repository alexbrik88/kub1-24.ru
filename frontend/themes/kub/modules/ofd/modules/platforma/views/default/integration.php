<?php

use frontend\modules\ofd\modules\platforma\components\Helper;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $employee common\models\employee\Employee */

$ofdName = "\"{$ofd->name}\"";
$this->title = 'Интеграция c ' . $ofdName;

?>

<div class="platforma-default-integration">
    <?php if ($ofdUser === null) : ?>
        <h4>Инструкция</h4>
        <p>Для загрузки данных из <?= Html::encode($ofdName) ?> выполните следующие шаги:</p>
        <ul>
            <li>
                Зайдите в ваш личный кабинет в <?= Html::encode($ofdName) ?>.
            </li>
            <li>
                Зайдите в Магазин Приложений.
            </li>
            <li>
                Выберите приложение
                "<?= Html::a('Интеграция с КУБ24', 'https://market.platformaofd.ru/store/apps/220f4b03-3091-4b29-9df2-7b9813b2d77b', [
                    'target' => '_blank',
                ]) ?>".
            </li>
            <li>
                Нажмите на кнопку УСТАНОВИТЬ и введите логин и пароль от вашего личного кабинета в "КУБ24".
            </li>
        </ul>
        <?php if ($companyCount > 1) { ?>
            <hr/>
            <p>
                <b>Внимание!</b>
                <br>
                Интеграция происходит для компании
                <?= Html::encode($employee->company->getShortTitle()) ?>
                вашего аккаунта "КУБ24" и вашего личного кабинета
                <?= Html::encode($ofdName) ?>.
                <br>
                Если вы хотите подключить другую компанию из аккаунта "КУБ24", то вам нужно войти в эту компанию.
            </p>
        <?php } ?>
    <?php else : ?>
        <div>
            <strong>Дата подключения:</strong>
            <?= date('d.m.Y', $ofdUser->created_at) ?>
        </div>
        <div class="mt-5 d-flex justify-content-between">
            <div class="">
                <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'tag' => 'a',
                        'label' => 'Удалить интеграцию с ' . $ofdName,
                        'class' => 'link',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'p' => Yii::$app->request->get('p'),
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить интеграцию с {$ofdName}?",
                ]) ?>
            </div>
            <div class="ml-auto">
                <?= Html::button('Отмена', [
                    'class' => 'button-regular button-hover-transparent min-w-130',
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        </div>
    <?php endif ?>
</div>
