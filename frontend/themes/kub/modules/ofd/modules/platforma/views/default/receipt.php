<?php

use frontend\components\Icon;
use frontend\modules\ofd\modules\platforma\components\Helper;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\ofd\modules\platforma\models\ReceiptForm */

$ofdName = "\"{$model->helper->ofd->name}\"";
$this->title = 'Запрос чеков из ОФД';

?>

<div class="platforma-default-receipt">

    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'id' => 'receipt_form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data' => [
                'pjax' => true,
            ],
        ],
    ])); ?>

        <div class="row">
            <div class="col-10">
                <?= $form->field($model, 'store_id')->dropDownList($model->storeDropdownList()); ?>
            </div>
            <div class="col-2" style="padding-top: 30px;">
                <?= Html::tag('span', Icon::get('repeat'), [
                    'class' => 'link import-ofd-store-kkt-data',
                    'title' => 'Импорт данных по магазинам и терминалам',
                    'data' => [
                        'url' => Url::to(['import']),
                    ],
                    'style' => [
                        'font-size' => '24px',
                        'cursor' => 'pointer',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-10">
                <?= $form->field($model, 'kkt_id')->dropDownList($model->kktDropdownList()); ?>
            </div>
            <div class="col-2" style="padding-top: 30px;">
                <?= Html::tag('span', Icon::get('repeat'), [
                    'class' => 'link import-ofd-store-kkt-data',
                    'title' => 'Импорт данных по магазинам и терминалам',
                    'data' => [
                        'url' => Url::to(['import']),
                    ],
                    'style' => [
                        'font-size' => '24px',
                        'cursor' => 'pointer',
                    ],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'from')->textInput([
                    'class' => 'form-control date-picker ico',
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'till')->textInput([
                    'class' => 'form-control date-picker ico',
                ]); ?>
            </div>
        </div>

        <div class="d-flex justify-content-between">
            <?= Html::submitButton('Загрузить', [
                'class' => 'button-regular button-regular_red ladda-button min-w-130',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-regular_red min-w-130',
                'class' => 'button-regular button-hover-transparent min-w-130',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    <?php $form->end(); ?>

    <?= \frontend\modules\ofd\widgets\AutoloadWidget::widget([
        'store' => $model->store,
    ]); ?>

    <div class="mt-3">
        <?= \frontend\modules\ofd\widgets\IntegrationDeleteWidget::widget([
            'ofdName' => $ofdName,
        ]) ?>
    </div>
</div>
