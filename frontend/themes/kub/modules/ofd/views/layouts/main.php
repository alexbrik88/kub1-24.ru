<?php

use frontend\components\Icon;
use frontend\themes\kub\assets\KubAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this \yii\web\View */
/* @var $content string */

KubAsset::register($this);

$hasHeader = ArrayHelper::getValue($this->params, 'modal-header') !== false;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?php Pjax::begin([
            'id' => 'ofd_module_pjax',
            'enablePushState' => false,
            'enableReplaceState' => false,
            'timeout' => 1800 * 1000,
            'options' => [
                'class' => 'modal-content',
            ],
        ]); ?>
            <style type="text/css">
                #ofd_module_pjax .form-control:focus {
                    border-color: #e5e2eb;
                }
            </style>

            <?php if ($hasHeader) : ?>
                <div class="modal-header">
                    <h4><?= Html::encode($this->title) ?></h4>
                    <button type="button" class="modal-close close" data-dismiss="modal" aria-hidden="true">
                        <?= Icon::get('close') ?>
                    </button>
                </div>
            <?php endif ?>

            <div class="modal-body">
                <?= $content ?>
            </div>

        <?php Pjax::end(); ?>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>