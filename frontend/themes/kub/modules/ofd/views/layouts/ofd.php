<?php
/**
 * @var $this  yii\web\View
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$ofd = $this->context->ofd;

$this->beginContent('@frontend/modules/ofd/views/layouts/main.php');
?>

<style>
    .ofd-content.logo-border {
        border: none;
        border-radius: unset;
        align-items: unset;
        display: unset;
    }
    .bank-form .logo-border img {
        height: unset;
    }
</style>
<div class="ofd-content" style="position: relative; min-height: 110px;">
    <div class="row pb-3">
        <div class="col-5 d-flex flex-column align-items-center justify-content-center">
            <div class="logo-border text-center">
                <?= Html::img('/img/ofd/' . $ofd->logo, [
                    'class' => 'flex-shrink-0',
                    'style' => 'max-width: 100%; max-height: 150px;',
                    'alt' => '',
                ]); ?>
            </div>
        </div>
        <div class="col-7">
            <div class="bank-form-notify p-3">
                <div class="p-1">
                    Для обеспечения безопасности данных используется
                    протокол зашифрованного соединения. <br>
                    SSL - надежный протокол для передачи конфиденциальной
                    банковской информации и соблюдаются требования
                    международного стандарта PCI DSS по хранению и передаче
                    конфиденциальной информации в банковской сфере.
                </div>
            </div>
        </div>
    </div>

    <?= Alert::widget(); ?>

    <?php echo $content ?>

    <div class="statement-loader"></div>
</div>

<?php $this->endContent(); ?>
