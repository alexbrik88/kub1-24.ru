<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Запрос чеков из ОФД';


/** @var $employee  */
/** @var $company  */
/** @var $ofdUserArray array */
?>

<?php if ($ofdUserArray) : ?>
    <div class="row">
        <?php foreach ($ofdUserArray as $ofdUser) : ?>
            <?php $ofd = $ofdUser->ofd ?>
            <div class="col-xs-6">
                <?= Html::a($ofd->name, [
                    "/ofd/{$ofd->alias}/default/integration",
                ], [
                    'class' => 'button-regular button-hover-transparent ofd_module_open_link',
                ]) ?>
            </div>
        <?php endforeach ?>
    </div>
<?php else : ?>
    <?php $this->params['modal-header'] = false; ?>
    <h4 class="text-center">
        Для загрузки чеков нужно настроить интеграцию с вашим ОФД
    </h4>
    <h4 class="text-center">
        Перейти на вкладку подключения ОФД?
    </h4>
    <div class="text-center" style="padding-top: 13px;">
        <?= Html::a('Да', [
            '/retail/ofd/index',
        ], [
            'class' => 'button-regular button-hover-transparent button-width-medium',
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-regular button-hover-transparent button-width-medium ml-2',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php endif ?>
