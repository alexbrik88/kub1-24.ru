<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ofd\Ofd */

$this->title = $model->name;
?>
<div class="ofd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'alias',
            'name',
            'url:url',
            'is_active',
            'logo',
        ],
    ]) ?>

</div>
