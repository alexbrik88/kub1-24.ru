<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $modalOptions array */
/* @var $pjaxOptions array */
/* @var $content mixed */

$js = <<<JS
$(document).on("click", ".ofd_module_open_link", function(e) {
    e.preventDefault();
    $.pjax({url: this.href, container: "#ofd_module_pjax", push: $("#ofd_module_modal").data("push-state")});
    $("#ofd_module_modal").modal("show");
});

$(document).on("click", ".link.import-ofd-store-kkt-data", function(e) {
    e.preventDefault();
    $(this).addClass("active");
    $.post($(this).data("url"), function (data) {
        $.pjax.reload('#ofd_module_pjax');
    });
});

$(document).on("hide.bs.modal", "#ofd_module_modal", function (e) {
    if (e.target.id == this.id) {
        var modal = $(this);
        if (modal.data("push-state")) {
            history.pushState({}, modal.data("page-title"), modal.data("page-url"));
        }
    }
});

$(document).on("hidden.bs.modal", "#ofd_module_modal", function (e) {
    if (e.target.id == this.id) {
        $("#ofd_module_pjax", this).html("");
        if (window.ofdReseiptInserted) {
            window.location.href = window.location.href;
        }
    }
});

$(document).on("change", "#receiptform-store_id", function (e) {
    let uid = this.value;
    $.pjax.reload('#ofd_module_pjax', {
        method: "post",
        data: {store_id: uid}
    });
});
JS;

$this->registerJs($js);

if ($show) {
    $this->registerJs('
        $("#ofd_module_modal").modal("show");
    ');
}
?>

<?= Html::beginTag('div', $modalOptions); ?>
    <div class="modal-dialog">
        <?php Pjax::begin($pjaxOptions); ?>

        <?= $content ?>

        <?php Pjax::end(); ?>
    </div>
<?= Html::endTag('div') ?>