<?php

use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model frontend\modules\ofd\models\AutoloadForm */
/* @var $store common\models\ofd\OfdStore */
/* @var $uploadedArray array */

?>

<?php if ($model !== null) : ?>

<div class="banking-autoload-wrapper mt-4">
    <button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreBankDetails" aria-expanded="false" aria-controls="moreBankDetails">
        <span class="link-txt">Автоматически загружать выписку</span>
        <svg class="link-shevron svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
        </svg>
    </button>
    <div class="collapse" id="moreBankDetails">
        <div class="row">
            <div class="col-5">
                <?= $this->render('_autoload_form', [
                    'model' => $model,
                    'store' => $store,
                ]) ?>
            </div>
            <div class="col-7">
                <div class="bank-form-wrap" style="margin-top: -18px;">
                    <strong class="mb-2 d-block">Последниие автозагрузки</strong>
                    <!-- <table class="table table-style table-style_2 mb-0"> -->
                    <table class="table table-style table-count-list last-auto-operations-table">
                        <thead>
                        <tr>
                            <th class="align-middle">Период</th>
                            <th class="align-middle">Дата <br> загрузки</th>
                            <th class="align-middle">Загружено <br> операций</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($uploadedArray) : ?>
                            <?php foreach ($uploadedArray as $uploaded) : ?>
                                <?php
                                $from = new \DateTime($uploaded->period_from);
                                $till = new \DateTime($uploaded->period_till);
                                ?>
                                <tr>
                                    <td>
                                        <?= ($from && $till) ? $from->format('d.m.y') . '—' . $till->format('d.m.y') : '' ?>
                                    </td>
                                    <td>
                                        <?= date('d.m.Y', $uploaded->created_at) ?>
                                    </td>
                                    <td>
                                        <?= $uploaded->saved_count ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php else : ?>
                            <tr>
                                <td colspan="3">
                                    Автозагрузки не найдены
                                </td>
                            </tr>
                        <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('submit', '#ofd_autoload_mode_form', function(e) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#autoload_save_report').show();
            setTimeout(function() {
                $('#autoload_save_report').hide();
            }, 3000);
        });
    });
</script>

<?php endif ?>
