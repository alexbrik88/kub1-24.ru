<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\ofd\models\AutoloadForm */
/* @var $store common\models\ofd\OfdStore */

?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'ofd_autoload_mode_form',
    'action' => ['set-autoload', 'storeId' => $store->id],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
])); ?>

    <div class="mt-3">
        <?= $form->field($model, 'mode')->radioList($model::$modeList, [
            'class' => 'd-flex flex-wrap',
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => '<span class="radio-txt">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'radio d-block mb-3',
                        'style' => 'width:100%'
                    ],
                ]);
            },
        ])->label(false) ?>
    </div>

    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_red button-clr button-width',
        'data-tooltip-content' => '#tooltip_mode-selector',
        'style' => 'margin-right: 10px'
    ]) ?>

    <span id="autoload_save_report" style="display: none; color: green;">
        Настройки сохранены
    </span>
<?php $form->end(); ?>
