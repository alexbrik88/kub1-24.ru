<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ofdName string */

?>

<?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => [
        'tag' => 'a',
        'label' => "Удалить интеграцию с {$ofdName}",
        'class' => 'link',
    ],
    'confirmUrl' => Url::toRoute([
        'delete',
        'p' => Yii::$app->request->get('p'),
    ]),
    'confirmParams' => [],
    'message' => "Вы уверены, что хотите удалить интеграцию с {$ofdName}?",
]) ?>