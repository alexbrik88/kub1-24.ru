<?php

use common\components\TextHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\document\Document;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use frontend\rbac\permissions;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
/* @var $actEssence \common\models\document\ActEssence */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}

$canUpdate = $model->status_out_id != ActStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$actCount = count($model->orderActs);
$invoiceCount = $model->invoice->getOrders()
    ->joinWith('product', false)
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])
    ->count();

$isAddCan = $invoiceCount > $actCount;

$invoice = $model->invoice;
$hasNds = $invoice->hasNds;
$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;

if ($isNdsExclude) {
    $sum = $model->getPrintAmountNoNds();
} else {
    $sum = $model->getPrintAmountWithNds();
}
$precision = $model->invoice->price_precision;
$cancelUrl = Url::previous('lastPage');
$hasInvoiceFacture = array_reduce($model->invoices, function($carry, $item) { return $carry || $item->has_invoice_facture; }, false);
// add new service
$ioType = $model->type;
$onlyServices = true;
$onlyProducts = $goodsCount = 0;
$serviceCount = $model->company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale($ioType == Documents::IO_TYPE_OUT ? false : null)->count();
$goodsCount = $model->company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale($ioType == Documents::IO_TYPE_OUT ? false : null)->count();
$productCount = $serviceCount + $goodsCount;
$canAddProduct = Yii::$app->user->can(permissions\Product::CREATE);
$addProductTypesText = ($onlyServices) ? 'новую услугу' : ($onlyProducts ? 'новый товар' : 'новый товар/услугу');
?>
    <div class="form">

        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

        <?= Html::beginForm('', 'post', [
            'id' => 'edit-act',
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate',
            'enctype' => 'multipart/form-data',
        ]); ?>

        <?= $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'message' => $message,
            'dateFormatted' => $dateFormatted,
            'canUpdate' => $canUpdate,
            'actEssence' => $actEssence,
        ]); ?>

        <div class="wrap">
            <?= Html::hiddenInput("OrderAct") ?>

            <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'precision' => $precision,
                'ioType' => $ioType,
                'onlyServices' => $onlyServices,
                'onlyProducts' => $onlyProducts,
                'serviceCount' => $serviceCount,
                'goodsCount' => $goodsCount,
                'productCount' => $productCount,
                'canAddProduct' => $canAddProduct,
                'addProductTypesText' => $addProductTypesText,
            ]); ?>

            <input type="hidden" class="document-type" value="<?= $invoice->type ?>"/>
            <input type="hidden" class="nds-view-type" value="<?= $invoice->nds_view_type_id ?>"/>

            <div class="row align-flex-start justify-content-start mt-3">
                <div class="column">
                    <span id="add-one-more-position" class="btn-add-line-table button-regular button-hover-content-red">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span>Добавить</span>
                    </span>
                </div>
                <div class="column">
                    <span id="plusbtn" class="button-regular button-hover-content-red <?= $isAddCan ? '' : 'hide' ?>">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span>Добавить из счета</span>
                    </span>
                </div>
                <div class="column ml-auto">
                    <div class="total-txt">
                        <table>
                            <tr>
                                <td class="ta-r">
                                    Итого:
                                </td>
                                <td class="ta-r">
                                    <strong id="total_price">
                                        <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="ta-r">
                                    <?php if ($hasNds) : ?>
                                        <?php if ($isNdsExclude) : ?>
                                            НДС сверху:
                                        <?php else : ?>
                                            В том числе НДС:
                                        <?php endif ?>
                                    <?php else : ?>
                                        Без налога (НДС):
                                    <?php endif ?>
                                </td>
                                <td class="ta-r">
                                    <strong id="total_nds">
                                        <?= $hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-'; ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="ta-r">
                                    Всего к оплате:
                                </td>
                                <td class="ta-r">
                                    <strong id="total_amount">
                                        <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?>
                                    </strong>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
            <div class="wrap">
                <label class="label" for="input2">
                    <span>Комментарий в акте для покупателя</span>
                </label>
                <div class="form-group mb-2">
                    <?= \yii\bootstrap4\Html::activeTextarea($model, 'comment', [
                        'maxlength' => true,
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                        'value' => $actEssence->is_checked && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT ?
                            $actEssence->text : $model->comment,
                        'data' => [
                            'currency' => 'Оплата счета производится в российских рублях по курсу ЦБ РФ на день оплаты.',
                            'paylimit' => 'Счет действителен для оплаты до {{date}}.',
                        ]
                    ]); ?>
                </div>
                <div class="checkbox">
                    <label class="checkbox-label label" for="actessence-is_checked">
                        <?= Html::activeCheckbox($actEssence, 'is_checked', [
                            'id' => 'actessence-is_checked',
                            'class' => 'checkbox-input input-hidden',
                            'label' => false,
                        ]); ?>
                        <span class="checkbox-txt">Сохранить текст для всех актов</span>
                    </label>
                </div>
            </div>
        <?php endif ?>

        <div class="wrap wrap_btns check-condition visible mb-0">
            <div class="row align-items-center justify-content-between">
                <div class="column">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                        'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                </div>
                <div class="column">
                    <?= Html::a('Отменить', $cancelUrl, [
                        'class' => 'button-width button-clr button-regular button-hover-grey',
                    ]); ?>
                </div>
            </div>
        </div>

        <?= Html::endForm(); ?>
    </div>

<!-- remove product -->
<div id="modal-remove-one-product" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">Вы уверены, что хотите удалить эту позицию из акта?</div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<!-- add invoice after update -->
<div id="modal-add-invoice" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы увеличили сумму изначального Акта и она больше, чем сумма выставленного счёта.
                </h4>
                <div class="text-center mb-3">
                    При нажатии на кнопку Сохранить автоматически сформируется счет на доплату разницы,<br/>
                    в размере <span class="diff-amount bold"><!--js--></span> <span class="bold">₽</span>
                    <?php if ($hasInvoiceFacture): ?>
                        а также будет увеличена сумма в счет-фактуре.
                    <?php endif; ?>
                </div>
                <div class="text-center">
                    <?= Html::button('<span class="ladda-label">Да</span>', [
                        'class' => 'yes button-clr button-regular button-hover-transparent button-width-medium mr-2 ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add new product -->
<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/form/_form_product_existed'); ?>
<?= $this->render('@frontend/themes/kub/modules/documents/views/invoice/form/_form_product_new'); ?>

<!-- add agreement-->
<?php
Modal::begin(['id' => 'agreement-modal-container', 'title' => '<h4 id="agreement-modal-header">Добавить договор</h4>']);
Pjax::begin(['id' => 'agreement-form-container', 'enablePushState' => false, 'linkSelector' => false]);
Pjax::end();
Modal::end();
?>

<?php
$count = $model->getOrderActs()->count();
$urlAdd = Url::to('/documents/act-ajax/add-new-row');
$urlDel = Url::to('/documents/act-ajax/delete-row');
$urlSub = Url::to('/documents/act-ajax/subsitution');
$urlClose = Url::to('/documents/act-ajax/close');
$urlEdit = Url::to('/documents/act-ajax/edit');
$script = <<< JS
var invoiceCount = $invoiceCount;
var actCount = $actCount;

$('#plusbtn').click( function() {
    $('#plusbtn').addClass('hide');
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);

    jQuery.post({
        url : '$urlAdd',
        data : {key : actCount, invoice_id: '$model->invoice_id', act_id: '$model->id', active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            actCount++;
            $(".editable-field").addClass("hide");
            $(".input-editable-field").removeClass("hide");
            if (invoiceCount > $('tr.order').length) {
                $('#plusbtn').removeClass('hide');
            } else {
                $('#plusbtn').addClass('hide');
            }
            createSimpleSelect2('choose-product-in-table');
        }
    });
});

$(document).on('click', '.delete-row', function() {
    $(this).closest('tr').remove();
    actCount--;
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);
    jQuery.post({
        url : '$urlDel',
        data : {act_id : '$model->id',active : active},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            if (ACT === Object(ACT)) {
                ACT.recalculateTable();
            }
        }
    });
});

$(document).on('change','.dropdownlist',function() {
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);
    var addRow = $(this).closest('tr');
    jQuery.post({
        url : '$urlSub',
        data : {order_id: $(this).val(), act_id : '$model->id',active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
            if (invoiceCount > $('tr.order').length) {
                $('#plusbtn').removeClass('hide');
            } else {
                $('#plusbtn').addClass('hide');
            }
            addRow.remove();
            if (ACT === Object(ACT)) {
                ACT.recalculateTable();
            }            
        }
    });
});

$(document).on('click','.btn-cancel', function() {
    $('.edit-in').hide();
    $('#plusbtn').addClass('hide');
    jQuery.get({
        url : '$urlClose',
        data : {act_id : '$model->id',ioType : 2} ,
        success : function(data) {
            $('.out-act_table').replaceWith(data);
            $('.edit-in').show();
        }
    })
});

$(document).on('click','.edit-in', function() {
    $('#plusbtn').addClass('hide');
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {act_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});

$(document).on("pjax:success", "#agreement-form-container", function() {
    refreshDatepicker();
    refreshUniform();
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#act-agreement").val(window.AgreementValue).trigger("change");
    }
});

//////////////////////////
if (ACT === Object(ACT)) {
    window.ACT.init();
}
//////////////////////////

JS;
$this->registerJs($script, $this::POS_READY);