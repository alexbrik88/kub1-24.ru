<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

// $addStamp = 1;
$invoice = $model->invoice;
$company = $invoice->company;
$contractor = $invoice->contractor;

$dateFormatted = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$director_name = '';
$nameArray = [];
if (!empty($invoice->contractor_director_name) && $invoice->contractor_director_name != 'ФИО Руководителя') {
    $nameArray = explode(' ', $invoice->contractor_director_name);
} elseif (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
    $nameArray = explode(' ', $contractor->director_name);
}
if ($nameArray) {
    foreach ($nameArray as $key => $value) {
        $director_name .= $key ? mb_substr($value, 0, 1) . '.' : $value . ' ';
    }
}

if ($model->signed_by_name) {
    $signatureLink = (empty($addStamp) || !$model->employeeSignature) ? false :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (empty($addStamp) || !$company->chief_signature_link) ? false :
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
}

$printLink = (empty($addStamp) || !$company->print_link) ? false :
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$signatureLinkStyle = $signatureLink ? " background: url($signatureLink); background-repeat: no-repeat; background-position: 0 50px;" : '';
$printLinkStyle = $printLink ? " background: url($printLink); background-repeat: no-repeat; background-position: 145px 0px;" : '';

$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$amount = $isNdsExclude ? $model->totalAmountNoNds : $model->totalAmountWithNds;
$ndsName = $invoice->ndsViewType->name;
$ndsValue = $invoice->hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-';
$precision = $model->invoice->price_precision;
?>

<style>
    table td.va-middle {vertical-align: middle;}
    table td.bordered, table .bordered td {border:1px solid #000;}
    table td.border-bottom {border-bottom:1px solid #000;}
    table td.tip {padding:0 0 3px 0;font-style: italic;}
</style>

<div class="page-content-in p-center pad-pdf-p">
    <table class="table no-border">
        <tr>
            <td></td>
            <td width="25%" class="font-size-8 text-center" style="font-style: italic">
                Приложение 50<br/>к приказу Министра финансов<br/>Республики Казахстан<br/> от 20 декабря 2012 года №562
            </td>
        </tr>
        <tr>
            <td></td>
            <td><br/></td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-8 text-right">Форма Р-1</td>
        </tr>
    </table>
    <table class="table-no-margin no-border va-bottom">
        <tr>
            <td width="13%" class="font-size-8">Заказчик</td>
            <td class="border-bottom"><?= $model->getCustomerShortRequisites() ?></td>
            <td width="5%"></td>
            <td width="20%" class="text-center va-middle bordered font-bold">XXXXXXXXXXX</td>
        </tr>
        <tr>
            <td></td>
            <td class="tip text-center font-size-8">полное наименование, адрес, данные о средствах связи</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="font-size-8">Исполнитель</td>
            <td class="border-bottom"><?= $model->getExecutorShortRequisites() ?></td>
            <td></td>
            <td width="20%" class="text-center va-middle bordered font-bold">XXXXXXXXXXX</td>
        </tr>
        <tr>
            <td></td>
            <td class="tip text-center font-size-8">полное наименование, адрес, данные о средствах связи</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table-no-margin no-border va-bottom">
        <tr>
            <td width="17%" class="font-size-8">Договор (контракт)</td>
            <td class="border-bottom"><?= $model->getBasisName(); ?></td>
            <td width="5%"></td>
            <td width="12%" class="bordered font-size-8 text-center" style="border-bottom:none;padding-top:0;padding-bottom:0;">Номер</td>
            <td width="12%" class="bordered font-size-8 text-center" style="border-bottom:none;padding-top:0;padding-bottom:0;">Дата</td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td class="bordered font-size-8 text-center" style="border-top:none;padding-top:0;vertical-align:top;">документа</td>
            <td class="bordered font-size-8 text-center" style="border-top:none;padding-top:0;vertical-align:top;">составления</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-10-bold text-center">АКТ ВЫПОЛНЕННЫХ РАБОТ (ОКАЗАННЫХ УСЛУГ)</td>
            <td class="bordered text-center font-bold"><?= $model->fullNumber; ?></td>
            <td class="bordered text-center font-bold"><?= $dateFormatted; ?></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-top:10px">
        <thead>
        <tr>
            <th class="font-size-8">Номер по порядку</th>
            <th class="font-size-8">Наименование работ (услуг) (в разрезе их подвидов в соответствии с технической спецификацией, заданием, графиком выполнения работ (услуг) при их наличии)</th>
            <th class="font-size-8">Дата выполнения работ (оказания услуг)</th>
            <th class="font-size-8">Сведения об отчете о научных исследованиях, маркетинговых, консультационных и прочих услугах (дата, номер, количество страниц) (при их наличии)</th>
            <th class="font-size-8">Единица измерения</th>
            <th class="font-size-8">Кол-вo</th>
            <th class="font-size-8">Цена за единицу</th>
            <th class="font-size-8">Стоимость</th>
        </tr>
        <tr class="text-center bordered">
            <td class="font-size-8">1</td>
            <td class="font-size-8">2</td>
            <td class="font-size-8">3</td>
            <td class="font-size-8">4</td>
            <td class="font-size-8">5</td>
            <td class="font-size-8">6</td>
            <td class="font-size-8">7</td>
            <td class="font-size-8">8</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->getOrderActs()->all() as $key => $order): ?>
            <?php
            $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;
            $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
            $orderAmount = $isNdsExclude ? $order->amountNoNds : $order->amountWithNds;
            ?>
            <tr class="bordered">
                <td style="text-align: center"><?= $key + 1; ?></td>
                <td><?= $order->order->product_title; ?></td>
                <td></td>
                <td></td>
                <td style="text-align: center"><?= $unitName ?></td>
                <td style="text-align: center">
                    <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                </td>
                <td style="text-align: center"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
                <td style="text-align: center"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td colspan="6" class="text-right font-size-8">Итого</td>
            <td class="text-center bordered">x</td>
            <td class="text-center bordered"><?= TextHelper::invoiceMoneyFormat($amount, 2); ?></td>
        </tr>
        </tbody>
    </table>
    <table class="table no-border">
        <tr>
            <td class="font-size-8" width="41%">Сведения об использовании запасов, полученных о</td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip text-center font-size-8">наименование, количество, стоимость</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="font-size-8">Приложение: Перечень документации, в том числе отчет(ы) о маркетинговых, научных исследованиях, консультационных и прочих услугах (обязательны при его (их) наличии на ____________ с _________________________________________________</td>
        </tr>
    </table>

    <div style="height: 200px;<?= $printLinkStyle ?>">
        <div style="height: 200px;<?= $signatureLinkStyle ?>">
            <table class="table no-border" style="width: 100%;">
                <tr>
                    <td class="font-size-8 no-border ver-bottom">Сдал (Исполнитель)</td>
                    <td style="width: 37%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            / <?= $model->signed_by_name ? $model->signed_by_name : $invoice->getCompanyChiefFio(true) ?> /
                        <?php else: ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php endif; ?>
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td class="font-size-8 no-border ver-bottom">Принял (Заказчик)</td>
                    <td style="width: 37%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php else: ?>
                            / <?= $model->signed_by_name ? $model->signed_by_name : $invoice->getCompanyChiefFio(true) ?> /
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="font-bold" style="padding-left:20px">М.П.</td>
                    <td></td>
                    <td></td>
                    <td colspan="2" class="m-l">
                        <br/>
                        <table class="table no-border">
                            <tr>
                                <td class="font-size-8">Дата подписания (принятия) работ (услуг)</td>
                                <td class="border-bottom" width="55%"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td class="font-bold" style="padding-left:20px">М.П.</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>
