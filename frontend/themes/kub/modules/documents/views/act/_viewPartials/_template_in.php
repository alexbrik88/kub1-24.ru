<?php
use common\components\TextHelper;
use common\components\helpers\Html;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;

$company = \Yii::$app->user->identity->company;
?>
<?php if (isset($order)): ?>
    <?php
    $hasNds = $model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT;
    $ioType = $model->type;
    $order_id = $order->order_id;
    $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
    $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
    $isNdsExclude = $model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
    $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
    ?>
    <tr role="row" class="odd order">
        <?php $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?>
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td>
            <?= $order->order->product_title; ?>
            <input type="hidden" class="selected-product-id" value="<?= $order->product_id ?>"/>
        </td>
        <td>
            <?= Html::hiddenInput("OrderAct[{$order_id}][order_id]", $order_id, [
                'data-value' => 1,
            ]) ?>
            <?php if ($unitName == Product::DEFAULT_VALUE) : ?>
                <?= Html::hiddenInput('OrderAct[' . $order_id . '][quantity]', 1, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
                <span><?= Product::DEFAULT_VALUE ?></span>
            <?php else : ?>
                <?= Html::input('number', "OrderAct[{$order_id}][quantity]", $order->quantity, [
                    'min' => 1,
                    'class' => 'input-editable-field quantity form-control-number product-count',
                    'available' => (float)$order::availableQuantity($order->order, $order->act_id),
                    'price' => (float)$orderPrice,
                    'tax-rate' => ($ioType == Documents::IO_TYPE_OUT) ? $order->order->saleTaxRate->rate : $order->order->purchaseTaxRate->rate
                ]) ?>
            <?php endif ?>
        </td>
        <td><?= $unitName ?></td>
        <?php if ($hasNds): ?>
            <td><?= $order->order->purchaseTaxRate->name; ?></td>
        <?php endif; ?>
        <?php if ($isNdsExclude) : ?>
            <td class="product-price text-right"><?= TextHelper::invoiceMoneyFormat($order->priceNoNds, $precision); ?></td>
            <td class="product-amount text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
        <?php else : ?>
            <td class="product-price text-right"><?= TextHelper::invoiceMoneyFormat($order->priceWithNds, $precision); ?></td>
            <td class="product-amount text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
        <?php endif; ?>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td><?= Html::dropDownList('test', null, $result, ['id' => 'choose-product-in-table', 'class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <?php if ($hasNds) : ?>
            <td></td>
        <?php endif ?>
    </tr>
<?php endif; ?>