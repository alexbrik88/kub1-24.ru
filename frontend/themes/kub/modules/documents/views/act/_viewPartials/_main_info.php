<?php

use common\models\project\Project;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message frontend\modules\documents\components\Message; */

$hasProject = Yii::$app->user->identity->menuItem->project_item;
$project = Project::find()->where(['id' => $model->invoice->project_id])->one();
$invoice = $model->invoice;
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
?>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($model->invoice->contractor_name_short, [
            '/contractor/view',
            'type' => $model->invoice->contractor->type,
            'id' => $model->invoice->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <?php if ($hasProject && $project) : ?>
        <div class="about-card-item">
            <span class="text-grey">Проект:</span>
            <span>
                <?= Html::a($project->name, ['/project/view', 'id' => $project->id], ['class' => 'link']); ?>
            </span>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">Основание:</span>
        <span>
            <?= $model->getBasisName(); ?>
        </span>
    </div>
    <?php if ($invoice && $invoice->industry) : ?>
        <div class="about-card-item">
            <span class="text-grey">Направление:</span>
            <span>
                <?= $invoice->industry->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($invoice && $invoice->salePoint) : ?>
        <div class="about-card-item">
            <span class="text-grey">Точка продаж:</span>
            <span>
                <?= $invoice->salePoint->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($invoice && $invoice->responsible) : ?>
        <div class="about-card-item">
            <span class="text-grey">Ответственный:</span>
            <?php if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) : ?>
                <?= frontend\widgets\ChangeResponsibleWidget::widget([
                    'formAction' => [
                        'invoice/set-responsible',
                        'type' => $invoice->type,
                        'id' => $invoice->id,
                    ],
                    'toggleButton' => [
                        'label' => Html::encode($invoice->responsible->getFio(true)),
                        'class' => 'link',
                    ],
                    'inputValue' => $invoice->responsible_employee_id,
                ]) ?>
            <?php else : ?>
                <span>
                    <?= Html::encode($invoice->responsible->getFio(true)) ?>
                </span>
            <?php endif ?>
        </div>
    <?php endif ?>
    <div class="about-card-item">
        <?= \frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>

<div class="about-card" style="margin-bottom: 12px">
    <div class="about-card-item">
        <span style="font-weight: bold;">Комментарий</span>
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
            'id' => 'comment_internal_update',
            'style' => 'cursor: pointer;',
        ]); ?>
        <div id="comment_internal_view" style="margin-top:5px" class="">
            <?= Html::encode($model->comment_internal) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
                'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_internal_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
            ]); ?>
            <?= Html::textarea('comment_internal', $model->comment_internal, [
                'id' => 'comment_internal_input',
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
            ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
            $(this).toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
                $("#comment_internal_update").toggleClass("hidden");
            })
        });
        $(document).on("change", "#activate_contractor_signature", function () {
            $.post($(this).data("url"), {status: +$(this).is(":checked")}, function (data) {});
        });
    ');
}
?>