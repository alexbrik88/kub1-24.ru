<?php
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\Html;

/**
 * @var $order \common\models\document\OrderAct
 */

?>

<?php if (isset($order)): ?>
    <?php
    $ioType = $model->type;
    $order_id = $order->order_id;
    $invoice = $model->invoice;
    $hasNds = $invoice->hasNds;
    $isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
    $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
    $orderAmount = $model->getPrintOrderAmount($order->order_id, $isNdsExclude);
    ?>
    <tr role="row" class="odd order">
        <?php $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?>
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td>
            <?= $order->order->product_title; ?>
            <input type="hidden" class="selected-product-id" value="<?= $order->product_id ?>"/>
        </td>
        <td>
            <?= Html::hiddenInput("OrderAct[{$order_id}][order_id]", $order_id, [
                'data-value' => 1,
            ]) ?>
            <?php if ($unitName == Product::DEFAULT_VALUE) : ?>
                <?= Html::hiddenInput("OrderAct[{$order_id}][quantity]", 1, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
                <span><?= Product::DEFAULT_VALUE ?></span>
            <?php else : ?>
                <?= Html::input('number', "OrderAct[{$order_id}][quantity]", $order->quantity, [
                    'min' => 1,
                    'class' => 'input-editable-field quantity form-control-number product-count',
                    'available' => (float)$order::availableQuantity($order->order, $order->act_id),
                    'price' => (float)$orderPrice,
                    'tax-rate' => ($ioType == Documents::IO_TYPE_OUT) ? $order->order->saleTaxRate->rate : $order->order->purchaseTaxRate->rate
                ]) ?>
            <?php endif ?>
        </td>
        <td><?= $unitName ?></td>
        <td class="product-price text-right"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
        <td class="product-amount text-right"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td class="text-center">
            <button class="remove-product-from-invoice delete-row hide button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td><?= Html::dropDownList('test', null, $result, ['id' => 'choose-product-in-table', 'class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
    </tr>
<?php endif; ?>