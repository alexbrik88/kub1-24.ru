<?php

use yii\helpers\Html;
use common\models\document\Act;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\web\JsExpression;
use frontend\rbac\permissions;
use common\models\document\Invoice;

/**
 * @var $model Act
 */
$hasNds = $model->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT;
?>

<table id="product-table-act" class="table table-style table-count in-act_table">
    <thead>
    <tr class="heading" role="row">
        <th width="1%"></th>
        <th width="10%">Наименование</th>
        <th width="6%">Количество</th>
        <th width="5%">Ед.измерения</th>
        <?php if ($hasNds): ?>
            <th width="5%">Ставка</th>
        <?php endif; ?>
        <th width="5%">Цена</th>
        <th width="5%">Сумма</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php foreach ($model->orderActs as $key => $order): ?>
        <?php echo
        $this->render('_template_in', [
            'key' => $key,
            'order' => $order,
            'model' => $model,
            'precision' => $precision,
        ]);
        ?>
    <?php endforeach; ?>

    <tr id="from-new-add-row" role="row" class="from-new-add" style="display: none;">
        <td></td>
        <td>
            <div class="add-exists-product">
                <?php echo Select2::widget([
                    'id' => 'order-add-select',
                    'name' => 'addOrder',
                    'initValueText' => '',
                    'options' => [
                        'placeholder' => '',
                        'class' => 'form-control ',
                        'data-doctype' => $ioType,
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'width' => '100%',
                        'minimumInputLength' => 1,
                        'dropdownCssClass' => 'product-search-dropdown',
                        'ajax' => [
                            'url' => "/product/search",
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                            'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(product) { return product.text; }'),
                        'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                    ],
                ]); ?>
                <div id="order-add-static-items" style="display: none;">
                    <ul class="order-add-static-items select2-results__options">
                        <?php if ($canAddProduct) : ?>
                            <li class="select2-results__option add-modal-new-service red" aria-selected="false" data-type="<?= $model->type ?>" data-document="act">
                                <?= Icon::PLUS . " Добавить {$addProductTypesText}" ?>
                            </li>
                        <?php endif ?>
                        <li class="select2-results__option add-modal-services<?= $onlyProducts || $serviceCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                        </li>
                        <?php /*
                        <li class="select2-results__option add-modal-products<?= $onlyServices || $goodsCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                            Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                        </li>*/ ?>
                    </ul>
                </div>
                <script type="text/javascript">
                    $('#order-add-select').on('select2:open', function (evt) {
                        var prodItems = $('#select2-order-add-select-results').parent().children('.order-add-static-items');
                        if (prodItems.length) {
                            prodItems.remove();
                        }
                        $('.order-add-static-items').clone().insertAfter('#select2-order-add-select-results');
                    });
                    $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function() {
                        $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                        $(this).addClass('select2-results__option--highlighted');
                    });
                </script>
            </div>
        </td>
        <td>
        </td>
        <td></td>
        <?php if ($hasNds): ?>
            <td></td>
        <?php endif; ?>
        <td></td>
        <td></td>
    </tr>

    <tr role="row" class="template" style="display: none;">
        <td class="text-center">
            <input type="hidden" class="selected-product-id" value="-1"/>
            <button class="remove-product-from-invoice delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
        <td class="product-title"></td>
        <td class="product-quantity">
            <span class="product-no-count">
                <?= Product::DEFAULT_VALUE ?>
                <?= Html::hiddenInput("_surchargeProduct[productID][quantity]", 1, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
            </span>
            <span class="product-count">
                <?= Html::input('number', "_surchargeProduct[productID][quantity]", 1, [
                    'min' => 1,
                    'class' => 'input-editable-field quantity form-control-number',
                    'style' => 'min-width: 85px',
                    'available' => 0,
                    'price' => 0,
                    'tax-rate' => 0,

                ]) ?>
            </span>
        </td>
        <td class="product-unit-name"></td>
        <?php if ($hasNds): ?>
            <td class="product-tax_rate"></td>
        <?php endif; ?>
        <td class="product-price text-right">
            <?= Html::input('text', "_surchargeProduct[productID][price]", '0.00', [
                'class' => 'input-editable-field price form-control-number text-right',
                'style' => 'min-width: 120px'
            ]) ?>
        </td>
        <td class="product-amount text-right"></td>
    </tr>

    </tbody>
</table>
