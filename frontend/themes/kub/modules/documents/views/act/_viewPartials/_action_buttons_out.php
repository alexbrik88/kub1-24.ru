<?php

use common\models\document\status\ActStatus;
use frontend\rbac\permissions;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this View */
/* @var $model \common\models\document\Act */
/* @var $useContractor boolean */

$contractorId = $useContractor ? $model->invoice->contractor_id : null;

$canStatus = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);

$canDelete = Yii::$app->user->can(permissions\document\Document::DELETE, [
    'model' => $model,
]);

$hasSignature = $model->employeeSignature || $model->getImage('chiefSignatureImage') || $model->getImage('chiefAccountantSignatureImage');
$hasStamp = $model->getImage('printImage');

$loadAsPdfButtons = [[
    'label' => '<span style="display: inline-block;">PDF</span> файл',
    'encode' => false,
    'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'addStamp' => 0, 'filename' => $model->getPdfFileName()],
    'linkOptions' => ['target' => '_blank']
]];

if ($hasSignature || $hasStamp) {
    $loadAsPdfButtons[] = [
        'label' => '<span style="display: inline-block;">PDF</span> файл с ' . ($hasSignature ? 'подписью' : 'печатью'),
        'encode' => false,
        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'addStamp' => 1, 'filename' => $model->getPdfFileName()],
        'linkOptions' => ['target' => '_blank']
    ];
}

?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if (true || $canStatus) : ?>
                <?=Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $model->type,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-hover-transparent w-full',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => array_merge($loadAsPdfButtons, [
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">XML</span> для ЭДО',
                            'encode' => false,
                            'url' => ['/edm/diadoc/act', 'id' => $model->id],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                    ])
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canStatus) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'check-2']).' <span>Передан</span>', [
                    'update-status',
                    'type' => $model->type,
                    'id' => $model->id,
                    'contractorId' => $contractorId,
                ], [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => ActStatus::STATUS_SEND,
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canStatus) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'check-double']).' <span>Подписан</span>', [
                    'update-status',
                    'type' => $model->type,
                    'id' => $model->id,
                    'contractorId' => $contractorId,
                ], [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => ActStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            &nbsp;
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canDelete) : ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'type' => $model->type,
                        'id' => $model->id,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить этот акт?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($canStatus) : ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>