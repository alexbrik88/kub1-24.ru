<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $useContractor string */

?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: #26cd58;
            border-color: #26cd58;
            color: #ffffff;
        ">
            <?php if ($model->is_original) : ?>
                <?= $this->render('//svg-sprite', ['ico' => 'new-doc']) ?>
                <span class="ml-3">Оригинал</span>
            <?php else : ?>
                <i class="icon pull-left fa fa-copy"></i>
                <span class="ml-3">Скан</span>
            <?php endif; ?>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->is_original_updated_at ? : $model->created_at) ?></span>
        </div>
    </div>
</div>

<?= $this->render('@frontend/modules/documents/views/default/status_row_invoices', [
    'model' => $model
]) ?>