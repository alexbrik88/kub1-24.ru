<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Act;
use common\models\document\DocumentImportType;
use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\themes\kub\helpers\KubHelper;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\models\document\OrderAct;
use yii\web\View;
use yii\widgets\ActiveForm;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\project\Project;

/* @var $this yii\web\View */
/* @var $searchModel ActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$company = Yii::$app->user->identity->company;
$period = StatisticPeriod::getSessionName();

/** @var Config $userConfig */
$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');


$exists = Invoice::find()->joinWith('acts', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет актов. Измените период, чтобы увидеть имеющиеся акты.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного акта.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
        'ioType' => $ioType,
    ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
        'ioType' => $ioType,
    ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus) {
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) {
    $dropItems[] = [
        'label' => 'Ответственный',
        'url' => '#mass_change_responsible',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeResponsibleWidget::widget([
        'formAction' => [
            'invoice/mass-set-responsible',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
    ]);
}
if ($ioType == \frontend\models\Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
}
if ($canPrint) {
    if ($company->pdf_act_signed) {
        $manyPrintButton =
            Html::tag('div',
                Html::button($this->render('//svg-sprite', ['ico' => 'print']) . '<span>Печать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
                    'data-toggle' => 'dropdown',
                ]) . Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span>Печать без подписи</span>',
                            'encode' => false,
                            'url' => ['many-document-print', 'actionType' => 'pdf', 'type' => $ioType, 'addStamp' => 0, 'multiple' => ''],
                            'linkOptions' => ['class' => 'multiple-print', 'target' => '_blank']
                        ],
                        [
                            'label' => '<span>Печать с подписью</span>',
                            'encode' => false,
                            'url' => ['many-document-print', 'actionType' => 'pdf', 'type' => $ioType, 'addStamp' => 1, 'multiple' => ''],
                            'linkOptions' => ['class' => 'multiple-print-with-stamp', 'target' => '_blank']
                        ]
                    ],
                ]), ['class' => 'dropup']);
    } else {
        $manyPrintButton = Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]);
    }
}

$showAutoActModal = Yii::$app->request->get('autoActModal');

$isTaxTypeOsno = $company->companyTaxationType->osno;
$isTaxTypeUsn = $company->companyTaxationType->usn;

$isDocumentOut = (bool)($ioType == Documents::IO_TYPE_OUT);

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_act' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

if ($userConfig->act_stat_by_payment || !$isDocumentOut) {
    $stat1 = [
        'sum' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                '`invoice`.`total_amount_with_nds` as `total_amount_with_nds`',
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_with_nds`'),
        'count' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->andWhere(['and',
                [Act::tableName() . '.type' => $ioType],
            ])
            ->count(),
        'title' => 'Итого',
        'title2' => 'Количество актов:',
    ];
    $stat2 = [
        'sum' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                'IF(`invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds` - `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
            ])
            ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_with_nds`'),
        'count' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Неоплачено',
        'title2' => 'Количество актов:',
    ];
    $stat3 = [
        'sum' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                'IF(`invoice`.`payment_partial_amount`, `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
            ])
            ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_with_nds`'),
        'count' => $searchModel->getQuery()
            ->distinct()
            ->joinWith('invoice.payment')
            ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Оплачено',
        'title2' => 'Количество актов:',
    ];
} else if ($userConfig->act_stat_by_status) {
    $stat1 = [
        'sum' => OrderAct::find()
            ->select(['`order`.`amount_sales_with_vat`', '`order_act`.`quantity`'])
            ->andWhere(['act_id' => $searchModel->getQuery()
                ->select('`act`.`id`')
                ->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])
                ->column()])
            ->leftJoin('order', 'order.id = order_act.order_id')
            ->sum('`order`.`amount_sales_with_vat`'),
        'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])->count(),
        'title' => 'Распечатано',
        'title2' => 'Количество актов:',
    ];
    $stat2 = [
        'sum' => OrderAct::find()
            ->select(['`order`.`amount_sales_with_vat`', '`order_act`.`quantity`'])
            ->andWhere(['act_id' => $searchModel->getQuery()
                ->select('`act`.`id`')
                ->andWhere(['status_out_id' => ActStatus::STATUS_SEND])
                ->column()])
            ->leftJoin('order', 'order.id = order_act.order_id')
            ->sum('`order`.`amount_sales_with_vat`'),
        'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_SEND])->count(),
        'title' => 'Передано',
        'title2' => 'Количество актов:',
    ];
    $stat3 = [
        'sum' => OrderAct::find()
            ->select(['`order`.`amount_sales_with_vat`', '`order_act`.`quantity`'])
            ->andWhere(['act_id' => $searchModel->getQuery()
                ->select('`act`.`id`')
                ->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])
                ->column()])
            ->leftJoin('order', 'order.id = order_act.order_id')
            ->sum('`order`.`amount_sales_with_vat`'),
        'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])->count(),
        'title' => 'Подписано',
        'title2' => 'Количество актов:',
    ];
} else if ($userConfig->act_stat_by_calculate_nds) {
    $stat1 = [
        'sum' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                Invoice::tableName() . '.total_amount_with_nds as total_amount_with_nds',
//                Payment::tableName().'.sum as sum',
            ])
//            ->andWhere(['IS NOT', Payment::tableName().'.id', new Expression('NULL')])
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_with_nds`'),
        'count' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
//            ->andWhere(['IS NOT', Payment::tableName().'.is_confirmed', new Expression('NULL')])
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Итого',
        'title2' => 'Количество актов:',
    ];
    $stat2 = [
        'sum' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                Invoice::tableName() . '.total_amount_nds as total_amount_nds',
            ])
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_nds`'),
        'count' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Сумма НДС',
        'title2' => 'Количество актов:',
    ];
    $stat3 = [
        'sum' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->addSelect([
                Act::tableName() . '.*',
                Invoice::tableName() . '.total_amount_no_nds as total_amount_no_nds',
            ])
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->sum('`total_amount_no_nds`'),
        'count' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->andWhere(['or',
                ['IS', 'service_payment_id', new Expression('NULL')],
                ['IS NOT', 'service_payment_id', new Expression('NULL')],
                [Payment::tableName() . '.is_confirmed' => 0],
                [Payment::tableName() . '.is_confirmed' => 1]
            ])
            ->andWhere([Act::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Сумма без НДС',
        'title2' => 'Количество актов:',
    ];
}

$this->registerCss(<<<CSS
    .sorting_block {
        cursor: pointer;
    }
CSS);

$this->registerJs('
    $(".sorting_block").on("click", function(e){
        e.preventDefault();
        var url = $(this).data("url");

        if (url) {
            window.location = url;
        }
    });
    $(document).on("change", ".autoact-rule-radio", function() {
        var rule = $(".autoact-rule-radio:checked").val();
        $("#autoact-form .collapse.show").collapse("hide");
        if (rule) {
            $("#autoact-form .rule" + rule + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $("#autoact-form .rule" + rule + " .has-error").removeClass("has-error");
            $("#autoact-form .rule" + rule + " .help-block").html("");
            $("#autoact-form .rule" + rule).collapse("show");
        }
    });
    $(document).on("change", ".autoact-status-radio", function() {
        var $checked = $(".autoact-status-radio:checked");
        console.log($checked.val());
        $("#autoact-form .autoact-status-content.collapse.show").collapse("hide");
        if ($checked.val() == "1") {
            $checked.data("target")
            $($checked.data("target") + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $($checked.data("target") + " .has-error").removeClass("has-error");
            $($checked.data("target") + " .help-block").html("");
            $($checked.data("target")).collapse("show");
        }
    });
    $(document).on("hidden.bs.collapse", ".autoact-rule-box", function () {
        $("#autoact-form .autoact-rule-box.collapse:not(.show) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.collapse", ".autoact-status-content", function () {
        $("#autoact-form .autoact-status-content.collapse:not(.show) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.modal", "#autoact-form-modal", function () {
        $.pjax.reload("#autoact-form-pjax");
    });
    $(document).on("pjax:success", "#autoact-form-pjax", function () {
        $("input", this).uniform("refresh");
    });
');

KubHelper::setStatisticsFontSize(max($stat1['sum'], $stat2['sum'], $stat3['sum']));

$this->registerJs(<<<JS
    $('.action_by').on('click', function(e){
        e.preventDefault();
        var url = $(this).data('url');

        $.post(url, {company_id: $company->id}, function(response){
            if ('true' === response.success) {
                location.href = 'index?type=$ioType';
            }
        }, 'json');
    });
JS, View::POS_READY);

?>

    <div class="stop-zone-for-fixed-elems">
        <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()) : ?>
            <?php Modal::begin([
                'clientOptions' => ['show' => true],
                'closeButton' => false,
            ]); ?>
            <h4 class="modal-title text-center mb-4">Перед тем как выставить Акт, нужно создать Счет.</h4>
            <div class="text-center">
                <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                    'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
                ]); ?>
            </div>
            <?php Modal::end(); ?>
            <div class="alert-success alert fade in">
                <button id="contractor_alert_close" type="button" class="close"
                        data-dismiss="alert" aria-hidden="true">×
                </button>
                Перед тем как выставить Акт,
                нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
                .
            </div>
        <?php endif; ?>
        <div class="page-head d-flex flex-wrap align-items-center">
            <h4><?= Html::encode($this->title) ?></h4>
            <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                <?php if ($existsInvoicesToAdd) : ?>
                    <button class="button-regular button-regular_red button-width ml-auto add-document">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span>Добавить</span>
                    </button>
                <?php else : ?>
                    <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
                        '/documents/invoice/create',
                        'type' => $ioType,
                        'document' => Documents::SLUG_ACT,
                    ]) ?>">
                        <svg class="svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span>Добавить</span>
                    </a>
                <?php endif; ?>
            <?php else : ?>
                <button class="button-regular button-regular_red button-width ml-auto action-is-limited">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
            <?php endif ?>
        </div>
        <div class="wrap wrap_count">
            <div class="row">
                <div class="col-6 col-xl-3">
                    <div class="sorting_block count-card <?= $userConfig->act_stat_by_status ? 'count-card' : 'count-card_green'; ?> wrap"
                        <?php if ($userConfig->act_stat_by_payment): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[payment_status]' => 'all'
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_status): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[status_out_id]' => 2
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_calculate_nds): ?>
                            data-url=""
                        <?php endif; ?>
                    >
                        <?php if ($userConfig->act_stat_by_payment || $userConfig->act_stat_by_calculate_nds): ?>
                            <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                            <div class="count-card-title"><?= $stat3['title']; ?></div>
                            <hr>
                            <div class="count-card-foot">
                                <?= $stat3['title2']; ?>
                                <?= $stat3['count'] ?>
                            </div>
                        <?php elseif ($userConfig->act_stat_by_status): ?>
                            <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                            <div class="count-card-title"><?= $stat1['title']; ?></div>
                            <hr>
                            <div class="count-card-foot">
                                <?= $stat1['title2']; ?>
                                <?= $stat1['count'] ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-6 col-xl-3">
                    <div class="sorting_block count-card count-card_red wrap"
                        <?php if ($userConfig->act_stat_by_payment): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[payment_status]' => 'not_payment'
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_status): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[status_out_id]' => 3
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_calculate_nds): ?>
                            data-url=""
                        <?php endif; ?>
                    >
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                        <div class="count-card-title"><?= $stat2['title']; ?></div>
                        <hr>
                        <div class="count-card-foot">
                            <?= $stat2['title2']; ?>
                            <?= $stat2['count'] ?>
                        </div>
                    </div>
                </div>
                <div class="sorting_block col-6 col-xl-3">
                    <div class="sorting_block count-card <?= $userConfig->act_stat_by_status ? 'count-card_green' : 'count-card'; ?> wrap"
                        <?php if ($userConfig->act_stat_by_payment): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[payment_status]' => 'payment'
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_status): ?>
                            data-url=<?= Url::to([
                                '/documents/act/index',
                                'type' => $ioType,
                                'ActSearch[status_out_id]' => 4
                            ]); ?>
                        <?php elseif ($userConfig->act_stat_by_calculate_nds): ?>
                            data-url=""
                        <?php endif; ?>
                    >
                        <?php if ($userConfig->act_stat_by_payment || $userConfig->act_stat_by_calculate_nds): ?>
                            <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                            <div class="count-card-title"><?= $stat1['title']; ?></div>
                            <hr>
                            <div class="count-card-foot">
                                <?= $stat1['title2']; ?>
                                <?= $stat1['count'] ?>
                            </div>
                        <?php elseif ($userConfig->act_stat_by_status): ?>
                            <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                            <div class="count-card-title"><?= $stat3['title']; ?></div>
                            <hr>
                            <div class="count-card-foot">
                                <?= $stat3['title2']; ?>
                                <?= $stat3['count'] ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                    <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                    <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                        <?php Modal::begin([
                            'id' => 'autoact-form-modal',
                            'closeButton' => false,
                            'toggleButton' => [
                                'tag' => 'button',
                                'label' => 'Настройка АвтоАктов',
                                'class' => 'button-regular w-100 button-hover-content-red',
                            ],
                        ]); ?>

                        <?= $this->render('autoact') ?>

                        <?php Modal::end(); ?>
                    <?php endif ?>
                    <?php if ($isDocumentOut): ?>
                        <div class="dropdown popup-dropdown popup-dropdown_position-shevron">
                            <a class="button-regular w-100 button-hover-content-red"
                               id="actTypeDisplayToggleButton"
                               href="#"
                               role="button"
                               title='Кнопка переключает рассчет итого в блоках слева. "По статусу акта" и <?= $userConfig->act_stat_by_calculate_nds ? "\"Подсчет НДС\"" : "\"По оплате\""; ?>.'
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="true">
                                <span><?= $userConfig->act_stat_by_status ? 'По статусам актов' : ''; ?>
                                        <?= $userConfig->act_stat_by_payment ? 'По оплатам' : ''; ?>
                                        <?= $userConfig->act_stat_by_calculate_nds ? 'Подсчет НДС' : ''; ?></span>
                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                </svg>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="actTypeDisplayToggleButton">
                                <div class="popup-dropdown-in overflow-hidden overflow-hidden">
                                    <ul class="form-filter-list list-clr">
                                        <li>
                                            <?= Html::a('По статусам', '#', [
                                                'class' => 'nowrap action_by',
                                                'title' => 'По статусам актов',
                                                'data-url' => 'act-by-status',
                                            ]); ?>
                                        </li>
                                        <?php if ($isTaxTypeUsn): ?>
                                        <li>
                                            <?= Html::a('По оплатам', '#', [
                                                'class' => 'nowrap action_by',
                                                'title' => 'По оплатам',
                                                'data-url' => 'act-by-payment',
                                            ]) ?>
                                        </li>
                                        <?php endif; ?>
                                        <?php if ($isTaxTypeOsno): ?>
                                            <li>
                                                <?= Html::a('Подсчет НДС', '#', [
                                                    'class' => 'nowrap action_by',
                                                    'title' => 'Подсчет НДС',
                                                    'data-url' => 'act-by-calculate-nds',
                                                ]) ?>
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="table-settings row row_indents_s">
            <div class="col-6">
                <?= TableConfigWidget::widget([
                    'items' => [
                        [
                            'attribute' => 'act_scan',
                        ],
                        [
                            'attribute' => 'act_order_sum_without_nds',
                            'visible' => (bool)$isTaxTypeOsno,
                        ],
                        [
                            'attribute' => 'act_order_sum_nds',
                            'visible' => (bool)$isTaxTypeOsno,
                        ],
                        [
                            'attribute' => 'act_contractor_inn',
                        ],
                        [
                            'attribute' => 'act_industry',
                        ],
                        [
                            'attribute' => 'act_sale_point',
                        ],
                        [
                            'attribute' => 'act_project',
                        ],
                        [
                            'attribute' => 'act_payment_date',
                        ],
                        [
                            'attribute' => 'act_comment',
                        ],
                        [
                            'attribute' => 'act_responsible',
                        ],
                        [
                            'attribute' => 'act_source',
                        ],
                    ],
                ]); ?>
                <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                    'title' => 'Скачать в Excel',
                ]); ?>
                <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
            </div>
            <div class="col-6">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'options' => [
                        'class' => 'd-flex flex-nowrap align-items-center',
                    ],
                ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер акта, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => $emptyMessage,
            'tableOptions' => [
                'class' => 'table table-style table-count-list' . $tabViewClass,
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

            'columns' => [
                [
                    'header' => Html::checkbox('', false, [
                        'class' => 'joint-operation-main-checkbox',
                    ]),
                    'headerOptions' => [
                        'class' => 'text-center',
                        'width' => '1%',
                    ],
                    'contentOptions' => [
                        'class' => 'text-center',
                    ],
                    'format' => 'raw',
                    'value' => function (Act $model) {
                        return Html::checkbox('Act[' . $model->id . '][checked]', false, [
                            'class' => 'joint-operation-checkbox',
                            'data-sum' => $model->totalAmountWithNds,
                            'data-contractor' => $model->invoice->contractor_id,
                            'data-responsible' => $model->invoice->responsible_employee_id,
                            'data-invoice-id' => $model->invoice->id,
                        ]);
                    },
                ],
                [
                    'attribute' => 'document_date',
                    'label' => 'Дата акта',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '10%',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view',
                    ],
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],

                [
                    'attribute' => 'document_number',
                    'label' => '№ акта',
                    'headerOptions' => [
                        'class' => 'sorting nowrap',
                        'width' => '5%',
                    ],
                    'contentOptions' => [
                        'class' => 'document_number link-view',
                    ],
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                            ? Html::a($data->fullNumber, ['/documents/act/view', 'type' => $data->type, 'id' => $data->id])
                            : $data->fullNumber);
                    },
                ],
                [
                    'label' => 'Скан',
                    'headerOptions' => [
                        'class' => 'sorting col_act_scan' . ($userConfig->act_scan ? '' : ' hidden'),
                        'width' => '5%',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view col_act_scan' . ($userConfig->act_scan ? '' : ' hidden'),
                    ],
                    'attribute' => 'has_file',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return DocumentFileWidget::widget(['model' => $model]);
                    },
                ],
                [
                    'attribute' => 'order_sum',
                    'label' => 'Сумма',
                    'headerOptions' => [
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view',
                    ],
                    'format' => 'raw',
                    'value' => function ($data) {
                        $price = TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                        return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                    },
                ],
                [
                    'attribute' => 'order_sum_without_nds',
                    'label' => 'Сумма без НДС',
                    'headerOptions' => [
                        'class' => 'col_act_order_sum_without_nds ' . ($userConfig->act_order_sum_without_nds ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view col_act_order_sum_without_nds ' . ($userConfig->act_order_sum_without_nds ? '' : ' hidden'),
                    ],
                    'format' => 'raw',
                    'value' => function ($data) {
                        $price = TextHelper::invoiceMoneyFormat($data->totalAmountNoNds, 2);
                        return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                    },
                    'visible' => (bool)$isTaxTypeOsno,
                ],
                [
                    'attribute' => 'order_sum_nds',
                    'label' => 'Сумма НДС',
                    'headerOptions' => [
                        'class' => 'col_act_order_sum_nds ' . ($userConfig->act_order_sum_nds ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'link-view col_act_order_sum_nds ' . ($userConfig->act_order_sum_nds ? '' : ' hidden'),
                    ],
                    'format' => 'raw',
                    'value' => function ($data) {
                        $price = TextHelper::invoiceMoneyFormat($data->getTotalActNds(), 2);
                        return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                    },
                    'visible' => (bool)$isTaxTypeOsno,
                ],
                [
                    'attribute' => 'contractor_id',
                    'label' => 'Контрагент',
                    'headerOptions' => [
                        'width' => '30%',
                        'class' => 'dropdown-filter',
                    ],
                    'contentOptions' => [
                        'class' => 'contractor-cell',
                    ],
                    'filter' => FilterHelper::getContractorList($searchModel->type, Act::tableName(), true, false, false),
                    'hideSearch' => false,
                    's2width' => '300px',
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return Html::tag('span', Html::encode($data->invoice->contractor_name_short), [
                            'title' => $data->invoice->contractor_name_short,
                        ]);
                    },
                ],
                [
                    'attribute' => 'contractor_inn',
                    'label' => 'ИНН контрагента',
                    'headerOptions' => [
                        'class' => 'dropdown-filter col_act_contractor_inn' . ($userConfig->act_contractor_inn ? '' : ' hidden'),
                        'width' => '30%',
                    ],
                    'contentOptions' => [
                        'class' => 'contractor-cell col_act_contractor_inn' . ($userConfig->act_contractor_inn ? '' : ' hidden'),
                    ],
                    'hideSearch' => false,
                    's2width' => '300px',
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return Html::tag('span', Html::encode($data->invoice->contractor_inn), [
                            'title' => $data->invoice->contractor_inn,
                        ]);
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'label' => 'Статус',
                    'headerOptions' => [
                        'width' => '15%',
                    ],
                    'filter' => $searchModel->getStatusArray($searchModel->type),
                    's2width' => '120px',
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return $data->statusOut->name ?? '';
                    },
                    'visible' => $ioType == Documents::IO_TYPE_OUT,
                ],
                [
                    'attribute' => 'industry_id',
                    'label' => 'Направление',
                    'headerOptions' => [
                        'class' => 'col_act_industry' . ($userConfig->act_industry ? '' : ' hidden'),
                        'width' => '10%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_industry' . ($userConfig->act_industry ? '' : ' hidden'),
                    ],
                    'filter' => ['' => 'Все'] + CompanyIndustry::getSelect2Data(),
                    'hideSearch' => false,
                    's2width' => '300px',
                    'format' => 'raw',
                    'value' => function (Act $model) {
                        return ($model->invoice->industry_id && $model->invoice->industry) ? Html::encode($model->invoice->industry->name) : '';
                    },
                ],
                [
                    'attribute' => 'sale_point_id',
                    'label' => 'Точка продаж',
                    'headerOptions' => [
                        'class' => 'col_act_sale_point' . ($userConfig->act_sale_point ? '' : ' hidden'),
                        'width' => '10%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_sale_point' . ($userConfig->act_sale_point ? '' : ' hidden'),
                    ],
                    'filter' => ['' => 'Все'] + SalePoint::getSelect2Data(),
                    'hideSearch' => false,
                    's2width' => '300px',
                    'format' => 'raw',
                    'value' => function (Act $model) {
                        return ($model->invoice->sale_point_id && $model->invoice->salePoint) ? Html::encode($model->invoice->salePoint->name) : '';
                    },
                ],
                [
                    'attribute' => 'project_id',
                    'label' => 'Проект',
                    'headerOptions' => [
                        'class' => 'col_act_project' . ($userConfig->act_industry ? '' : ' hidden'),
                        'width' => '10%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_project' . ($userConfig->act_project ? '' : ' hidden'),
                    ],
                    'filter' => ['' => 'Все'] + Project::getSelect2Data(),
                    'hideSearch' => false,
                    's2width' => '300px',
                    'format' => 'raw',
                    'value' => function (Act $model) {
                        return ($model->invoice->project_id && $model->invoice->project) ? Html::encode($model->invoice->project->name) : '';
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'label' => 'Счёт №',
                    'headerOptions' => [
                        'width' => '15%',
                    ],
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        $invoice = $data->invoice;
                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice,])) {
                            return Html::a($invoice->fullNumber, [
                                    '/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $data->invoice->id,
                                ]) . DocumentFileWidget::widget(['model' => $invoice, 'cssClass' => 'pull-right']);
                        } else {
                            return $data->invoice->fullNumber;
                        }
                    },
                ],
                [
                    'attribute' => 'act_payment_date',
                    'label' => 'Дата оплаты',
                    'headerOptions' => [
                        'class' => 'col_act_payment_date ' . ($userConfig->act_payment_date ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_payment_date ' . ($userConfig->act_payment_date ? '' : ' hidden'),
                    ],
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        $date = $data->invoice->payment_partial_amount ? $data->invoice->getLastPaymentDate() : 0;
                        return !$date ? "-" : date(DateHelper::FORMAT_USER_DATE, $date);
                    },
                ],
                [
                    'attribute' => 'act_comment',
                    'label' => 'Комментарий',
                    'headerOptions' => [
                        'class' => 'col_act_comment ' . ($userConfig->act_comment ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_comment ' . ($userConfig->act_comment ? '' : ' hidden'),
                    ],
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return htmlspecialchars($data->comment_internal);
                    },
                ],
                [
                    'attribute' => 'act_responsible',
                    'label' => 'Ответственный',
                    'headerOptions' => [
                        'class' => 'col_act_responsible' . ($userConfig->act_responsible ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_responsible' . ($userConfig->act_responsible ? '' : ' hidden'),
                    ],
                    's2width' => '200px',
                    'filter' => $searchModel->filterActResponsible(),
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        $responsible = \common\models\EmployeeCompany::item($data->invoice->responsible_employee_id, $data->invoice->company_id);

                        return $responsible ? $responsible->getFio(true) : '';
                    },
                ],
                [
                    'attribute' => 'act_source',
                    'label' => 'Источник',
                    'headerOptions' => [
                        'class' => 'col_act_source' . ($userConfig->act_source ? '' : ' hidden'),
                        'width' => '15%',
                    ],
                    'contentOptions' => [
                        'class' => 'col_act_source' . ($userConfig->act_source ? '' : ' hidden'),
                    ],
                    'filter' => ['' => 'Все'] + DocumentImportType::getList(),
                    'format' => 'raw',
                    'value' => function (Act $data) {
                        return DocumentImportType::getName($data->one_c_imported);
                    },
                ],
            ],
        ]); ?>
    </div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($showAutoActModal) : ?>
    <?php $this->registerJs('
    $(document).ready(function () {
        $("#autoact-form-modal").modal();
    });
'); ?>
<?php endif; ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT) : ?>
    <?php /*
    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'title' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
 */ ?>
<?php endif; ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? $manyPrintButton : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']) . ' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>  ' . $this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
                'data-toggle' => 'dropdown',
                'style' => 'width: 90px; padding: 12px 15px;',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'class' => 'dropdown-menu-right form-filter-list list-clr',
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

    <!-- Скачать в Excel -->
<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType' => $ioType,
    'documentType' => Documents::SLUG_ACT,
    'documentTypeName' => 'Акт'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_ACT
    ]); ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные акты?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button"
                data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>