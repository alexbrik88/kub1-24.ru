<?php

use common\models\document\Autoact;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Autoact */

if (empty($model)) {
    $model = Yii::$app->user->identity->company->autoact;
}
?>

<h4 class="modal-title mb-4">Правила выставления АвтоАктов</h4>

<?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
    'class' => 'modal-close close',
    'data-dismiss' => 'modal',
    'aria-label' => 'Close',
]) ?>

<?php Pjax::begin([
    'id' => 'autoact-form-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'autoact-form',
    'action' => ['autoact'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'class' => 'autoact-form',
        'data-pjax' => '',
    ],
]); ?>

    <?= $form->field($model, 'rule', ['options' => ['class' => '']])->label(false)->radioList(Autoact::$ruleLabels, [
        'encode' => false,
        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
            $comment = isset(Autoact::$ruleComments[$value]) ? ' (' . Autoact::$ruleComments[$value] . ')' : '';
            $input = Html::radio($name, $checked, [
                'value' => $value,
                'class' => 'autoact-rule-radio',
                'label' => Html::tag('span', $label, [
                    'style' => 'font-weight: bold;',
                ]) . $comment,
                'labelOptions' => [
                    'class' => 'autoact-rule-label'
                ],
            ]);

            $input .= $this->render("autoactPartials/_rule{$value}", [
                'form' => $form,
                'model' => $model,
                'value' => $value,
                'checked' => $checked,
            ]);

            return Html::tag('div', $input, [
                'class' => 'form-group',
            ]);
        }
    ]); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>