<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\NdsViewType;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\themes\kub\helpers\Icon;
use frontend\widgets\TableConfigWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $company common\models\Company */
/* @var $invoiceContractEssence \common\models\document\InvoiceContractEssence */
/* @var $document string */
/* @var $projectEstimateId integer */

SortableAsset::register($this);

$userConfig = Yii::$app->user->identity->config;

    $ndsCellClass = 'with-nds-item';
    if (!$model->hasNds &&
        (
            $model->type == Documents::IO_TYPE_IN ||
            $model->company->nds_view_type_id == NdsViewType::NDS_VIEW_WITHOUT
        )
    ) {
        $ndsCellClass .= ' hidden';
    }

$priceOneCss = ($model->has_discount ? ' has_discount' : '') . ($model->has_markup ? ' has_markup' : '');

$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}

if (!isset($model->discount_type)) {
    $model->discount_type = Invoice::DISCOUNT_TYPE_PERCENT;
}
if (!isset($model->is_hidden_discount)) {
    $model->is_hidden_discount = 0;
}

$js = <<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: ".sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS;
$this->registerJs($js);
?>

<?= Html::hiddenInput('documentType', $ioType, [
    'id' => 'documentType',
]); ?>
<?php /*
<div class="row">
    <div class="col-sm-12 table-icons" style="margin-top: -12px;">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'invoice_form_article',
                ],
            ],
        ]); ?>
    </div>
</div> */ ?>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
        'style' => 'margin-bottom: 0;',
    ],
]); ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.prise_modify_toggle',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true,
        'repositionOnScroll' => true,
        'side' => ['bottom', 'left', 'top', 'right'],
        'functionReady' => new \yii\web\JsExpression('function (origin, tooltip) {
            $("[title]", tooltip.tooltip).tooltipster({theme: ["tooltipster-kub"]});
        }'),
    ],
]); ?>

<div class="hidden">
    <!-- Discount popup -->
    <div id="invoice_price_conf" style="width: 140px; overflow: hidden; text-align: center;">
        <label class="label" style="text-align: center;">
            Количество знаков
            <br>
            после запятой
        </label>
        <div class="btn-group pb-1" role="group" style="margin: 10px 0 0;">
            <?= Html::button('2 знака', [
                'class' => 'button-regular '.($model->price_precision == 2 ? 'button-regular_red' : 'button-hover-content-red'),
                'value' => '2',
            ]) ?>
            <?= Html::button('4 знака', [
                'class' => 'button-regular '.($model->price_precision == 4 ? 'button-regular_red' : 'button-hover-content-red'),
                'value' => '4',
            ]) ?>
        </div>
    </div>
    <!-- Discount popup -->
    <div id="invoice_all_discount" style="display: inline-block; width: 490px; overflow: hidden;">
        <div class="mb-3 d-flex">
            <div class="mr-auto">
                <label class="label" for="checkbox-discount-rub">
                    Использовать формат скидки
                </label>
                <?= Html::activeRadioList($model, 'discount_type', [
                    Invoice::DISCOUNT_TYPE_PERCENT => 'В процентах',
                    Invoice::DISCOUNT_TYPE_RUBLE => 'В рублях',
                ], [
                    'id' => 'config-discount_type',
                    'data-rub' => Invoice::DISCOUNT_TYPE_RUBLE,
                    'itemOptions' => [
                        'labelOptions' => [
                            'class' => 'label mr-3',
                        ],
                    ],
                ]) ?>
            </div>
        </div>

        <div class="mb-3 d-flex">
            <div class="mr-2">
                <label class="label">Скидка для всех позиций счета, %</label>
                <?= Html::input('number', 'all-discount', 0, [
                    'id' => 'all-discount',
                    'min' => 0,
                    'max' => 99.9999,
                    'step' => 'any',
                    'style' => '',
                    'class' => 'form-control form-control-number',
                    'disabled' => $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE,
                    'data-old-value' => 0,
                ]); ?>
            </div>
            <div class="mr-2">
                <label class="label">Округление</label>
                <?= Html::activeDropDownList($model, 'price_round_rule', Invoice::$roundRules, [
                    'id' => 'config-price_round_rule',
                    'class' => 'form-control price_round_config',
                    'disabled' => !$model->has_discount || $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE,
                    'data-old-value' => $model->price_round_rule ?: Invoice::ROUND_RULE_DEFAULT,
                ]) ?>
            </div>
            <div>
                <label class="label">Округлять до</label>
                <?= Html::activeDropDownList($model, 'price_round_precision', Invoice::$roundPrecisions, [
                    'id' => 'config-price_round_precision',
                    'class' => 'form-control price_round_config',
                    'disabled' => !$model->has_discount || $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE,
                    'data-old-value' => $model->price_round_precision ?: Invoice::ROUND_PRECISION_DEFAULT,
                ]) ?>
            </div>
        </div>

        <div class="form-group mb-3">
            <label class="label">
                <?= Html::checkbox('discount_hidden', $model->is_hidden_discount, [
                    'id' => 'config-is_hidden_discount',
                    'class' => 'discount-hidden',
                ]); ?>
                Скидка видна только в режиме редактирования
                <?= Icon::get('question', [
                    'title' => 'Используйте, когда нужно посчитать скидку от текущей цены, но клиент не должен знать, что есть скидка. В счете для клиента столбцов со скидкой не будет, но цена будет указана со скидкой.',
                    'class' => 'tooltip-question-icon ml-2',
                ]) ?>
            </label>
        </div>

        <div class="row justify-content-between">
            <div class="column">
                <?= Html::button('Применить', [
                    'id' => 'discount_submit',
                    'class' => 'button-regular button-hover-grey button-width invoice',
                ]) ?>
            </div>
        </div>
    </div>
    <!-- Markup popup -->
    <div id="invoice_all_markup" style="display: inline-block; width: 220px; overflow: hidden;">
        <div class="form-group mb-3">
            <div class="label">Наценка для всех позиций счета, %</div><br/>
            <div class="row">
                <div class="col-6">
                    <?= Html::input('number', 'all-markup', 0, [
                        'id' => 'all-markup',
                        'min' => 0,
                        'max' => 99.9999,
                        'step' => 'any',
                        'style' => '',
                        'class' => 'form-control',
                        'data-old-value' => 0,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-between">
            <div class="column">
                <?= Html::button('Применить', [
                    'id' => 'markup_submit',
                    'class' => 'button-regular button-hover-grey button-width invoice',
                ]) ?>
            </div>
        </div>
    </div>
    <?= Html::activeHiddenInput($model, 'discount_type'); ?>
    <?= Html::activeHiddenInput($model, 'is_hidden_discount'); ?>
    <?= Html::activeHiddenInput($model, 'price_round_rule'); ?>
    <?= Html::activeHiddenInput($model, 'price_round_precision'); ?>
</div>

<?php if ($model->is_surcharge): ?><div class="mb-2 w-100 text-right" style="color:red">Позиции в счете доплаты редактировать нельзя.</div><?php endif; ?>
<div class="wrap" style="position: relative">
    <div class="table-wrap table-responsive">
        <table id="table-for-invoice" class="table table-style table-count" style="position: relative;">
            <thead>
            <tr class="heading" role="row">
                <th class="delete-column-left" style="min-width:45px; width:1%">
                </th>
                <th style="min-width:80px;  width: 1%"
                    class="col_invoice_form_article<?= $userConfig->invoice_form_article ? '' : ' hidden'; ?>">
                    Артикул
                </th>
                <th style="min-width:300px; width: 30%;">
                    Наименование

                    <?php if (!$model->company->getInvoices()->where(['type' => $ioType, 'is_deleted' => false])->exists()) : ?>
                        <!-- help -->
                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => '#tooltip_add_product',
                        ]) ?>
                    <?php endif; ?>
                </th>
                <th style="min-width:100px; width: 10%;" class="">
                    Количество
                </th>
                <th style="min-width:75px; width: 7%" class="">
                    Ед. измерения
                </th>
                <th style="min-width:75px;  width: 7%;" class="<?= $ndsCellClass ?>">
                    НДС
                </th>
                <th style="min-width:100px; width: 10%; position: relative;" class="">
                    <?= Html::tag('span', 'Цена', [
                        'id' => 'price_conf_toggle',
                        'class' => 'prise_modify_toggle',
                        'data-tooltip-content' => '#invoice_price_conf',
                        'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                    ]) ?>
                    <?= Html::activeHiddenInput($model, 'price_precision') ?>
                </th>
                <th style="min-width:100px; width: 10%; position: relative;"
                    class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                    rowspan="1" colspan="1" style="position: relative;">
                    <?php $discountText = (YII_ENV_PROD && $company->id == 9888 ?
                        'Агентское вознаграждение' :
                        ('Скидка' . ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'))); ?>
                    <?= Html::tag('span', $discountText, [
                        'id' => 'all_discount_toggle',
                        'class' => 'prise_modify_toggle',
                        'data-tooltip-content' => '#invoice_all_discount',
                        'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                    ]) ?>
                </th>
                <th style="min-width:100px; width: 10%;"
                    class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>" tabindex="0"
                    rowspan="1" colspan="1" style="position: relative;">
                    <?= Html::tag('span', 'Наценка %', [
                        'id' => 'all_markup_toggle',
                        'class' => 'prise_modify_toggle',
                        'data-tooltip-content' => '#invoice_all_markup',
                        'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                    ]) ?>
                </th>
                <th style="min-width:100px; width: 10%;"
                    class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                    <?php if (YII_ENV_PROD && $company->id == 9888): ?>
                        Цена с агентским вознаграждением
                    <?php else: ?>
                        Цена со скидкой
                    <?php endif; ?>
                </th>
                <th style="min-width:100px; width: 10%;"
                    class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
                    Цена с наценкой
                </th>
                <th style="min-width:100px; width: 10%; position:relative;"
                    class="weight_column<?= $model->has_weight ? '' : ' hidden'; ?>">
                    Вес, кг
                </th>
                <th style="min-width:100px; width: 10%; position:relative;"
                    class="volume_column<?= $model->has_volume ? '' : ' hidden'; ?>">
                    Объем, м2
                </th>
                <th style="min-width:100px; width: 10%;" class="">
                    Сумма
                </th>
            </tr>
            </thead>
            <tbody id="table-product-list-body" data-number="<?= count($model->orders) ?>">
            <?php if (count($model->orders) > 0) : ?>
                <?php
                foreach ($model->orders as $key => $order) {
                    echo $this->render('_form_order_row' . ($model->is_surcharge ? '_disabled' : ''), [
                        'order' => $order,
                        'ioType' => $ioType,
                        'number' => $key,
                        'model' => $model,
                        'ndsCellClass' => $ndsCellClass,
                        'precision' => $model->price_precision,
                        'userConfig' => $userConfig,
                    ]);
                }
                ?>
            <?php endif; ?>
            <?= $this->render('_form_add_order_row', [
                'ioType' => $ioType,
                'hasOrders' => count($model->orders) > 0,
                'hasDiscount' => (boolean)$model->has_discount,
                'hasMarkup' => (boolean)$model->has_markup,
                'hasWeight' => (boolean)$model->has_weight,
                'hasVolume' => (boolean)$model->has_volume,
                'ndsCellClass' => $ndsCellClass,
                'userConfig' => $userConfig,
                'hasProjectEstimate' => $projectEstimateId > 0,
            ]) ?>
            </tbody>
            <tfoot>
            <tr class="template disabled-row" role="row" style="display:none">
                <td class="product-delete delete-column-left" style="white-space: nowrap;">
                    <div style="max-width: 45px">
                        <button class="remove-product-from-invoice button-clr" type="button">
                            <svg class="table-count-icon svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                            </svg>
                        </button>
                        <button class="sortable-row-icon button-clr" type="button">
                            <svg class="table-count-icon table-count-icon_small svg-icon">
                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                            </svg>
                        </button>
                    </div>
                </td>
                <td class="col_invoice_form_article<?= $userConfig->invoice_form_article ? '' : ' hidden'; ?>">
                    <div style="min-width: 100px" class="order-param-value product-article">&nbsp;</div>
                </td>
                <td>
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="max-width: 100%">
                        <input disabled="disabled" type="text" class="product-title form-control tooltip-product"
                               name="orderArray[][title]">
                    </div>
                </td>
                <td>
                    <input disabled="disabled" type="hidden" class="tax-rate" value="0" />
                    <input disabled="disabled" type="hidden" class="order-id" name="orderArray[][id]" value="" />
                    <input disabled="disabled" type="hidden" class="product-id" name="orderArray[][product_id]" value="" />
                    <input disabled="disabled" type="hidden" class="max-product-count" name="orderArray[][max_product_count]" value="" />
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 100px">
                        <input disabled="disabled" type="number" min="0" step="any" lang="en"
                               class="form-control-number product-count" name="orderArray[][count]" value="1"/>
                    </div>

                    <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
                </td>
                <td class="">
                    <div style="min-width: 90px" class="order-param-value product-unit-name">
                        <?= Html::dropDownList('orderArray[][unit_id]',
                            null,
                            ['' => Product::DEFAULT_VALUE] + $unitItems,
                            [
                                'class' => '',
                                'disabled' => 'disabled'
                            ]
                        ); ?>
                    </div>
                </td>
                <td class="<?= $ndsCellClass ?>">
                    <div style="min-width: 90px" class="order-param-value price-for-sell-nds-name">
                        <?= Html::dropDownList(
                            ($ioType == Documents::IO_TYPE_OUT ? 'orderArray[][sale_tax_rate_id]' : 'orderArray[][purchase_tax_rate_id]'),
                            null,
                            $taxItems,
                            [
                                'class' => 'order_product_tax_rate',
                                'options' => $taxOptions,
                                'disabled' => 'disabled'
                            ]
                        ); ?>
                    </div>
                </td>
                <td class="price-one">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][price]', 0, [
                            'class' => 'form-control-number price-input',
                            'disabled' => 'disabled',
                            'min' => 0,
                            'step' => 'any',
                        ]); ?>
                    </div>
                </td>
                <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][discount]', 0, [
                            'class' => 'form-control-number discount-input',
                            'disabled' => 'disabled',
                            'min' => 0,
                            'max' => 99.9999,
                            'step' => 'any',
                        ]); ?>
                    </div>
                </td>
                <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                    <div style="min-width:120px" class="order-param-value">
                        <span class="price-one-with-nds">0</span>
                    </div>
                </td>
                <td class="markup markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][markup]', 0, [
                            'class' => 'form-control-number markup-input',
                            'disabled' => 'disabled',
                            'min' => 0,
                            'max' => 9999.9999,
                            'step' => 'any',
                        ]); ?>
                    </div>
                </td>
                <td class="markup markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
                    <div style="min-width:100px" class="order-param-value">
                        <span class="price-one-with-nds">0</span>
                    </div>
                </td>
                <td class="weight weight_column<?= $model->has_weight ? '' : ' hidden'; ?>">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][weight]', 0, [
                            'class' => 'form-control-number weight-input',
                            'disabled' => 'disabled',
                            'min' => 0,
                            'step' => 'any',
                        ]); ?>
                    </div>
                </td>
                <td class="volume volume_column<?= $model->has_volume ? '' : ' hidden'; ?>">
                    <!-- FLOAT WIDTH INPUT -->
                    <div style="min-width: 120px">
                        <?= Html::input('number', 'orderArray[][volume]', 0, [
                            'class' => 'form-control-number volume-input',
                            'disabled' => 'disabled',
                            'min' => 0,
                            'step' => 'any',
                        ]); ?>
                    </div>
                </td>
                <td class="" style="text-align: right;">
                    <div class="order-param-value price-with-nds">0</div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

    <div class="row align-flex-start justify-content-between mt-3">
        <div class="column button-add-line">
            <?php if (!$model->is_surcharge): ?>
            <span class="btn-add-line-table button-regular button-hover-content-red">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </span>
            <?php endif; ?>

            <table class="total-txt text-right table-resume table-resume-weight" style="margin-top: 5px;">
                <tbody>
                <tr role="row" class="weight_column<?= $model->has_weight ? '': ' hidden'; ?>">
                    <td>Общий вес:</td>
                    <td class="val">
                            <strong class="total_weight">
                                <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalWeight($model), $model->weightPrecision) ?>
                            </strong> <b style="padding-right:4px;color:#333">кг</b>
                    </td>
                </tr>
                <tr role="row" class="volume_column<?= $model->has_volume ? '': ' hidden'; ?>">
                    <td>Общий объем:</td>
                    <td class="val">
                            <strong class="total_volume">
                               <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalVolume($model), $model->volumePrecision) ?>
                            </strong> <b style="color:#333">м2</b>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <div class="column">
            <?= $this->render('_invoice_total_block', [
                'model' => $model,
                'projectEstimateId' => $projectEstimateId,
            ]) ?>
        </div>
    </div>
</div>
<?php Pjax::end(); ?>

<?php if (isset($invoiceContractEssence) && $document == null): ?>
    <div class="field-invoice_contract-essence <?= $model->is_invoice_contract ? null : 'hidden'; ?>">
        <div class="wrap">
            <div class="label mb-3 block">Предмет договора</div>
            <div id="invoice-contract_essence_template">
                <div class="mr-3 mb-2 inline-block ">
                    <input id="typeInput1" type="radio" name="Invoice[contract_essence_template]" value="<?=($model::CONTRACT_ESSENCE_TEMPLATE_GOODS) ?>"
                        <?=($model->contract_essence_template == $model::CONTRACT_ESSENCE_TEMPLATE_GOODS) ? 'checked=""' : '' ?> >
                    <label class="radio-label" for="typeInput1">
                        <span class="radio-txt-bold">Поставка товара</span>
                    </label>
                </div>
                <div class="mr-3 mb-2 inline-block">
                    <input id="typeInput2" type="radio" name="Invoice[contract_essence_template]" value="<?=($model::CONTRACT_ESSENCE_TEMPLATE_SERVICES) ?>"
                        <?=($model->contract_essence_template == $model::CONTRACT_ESSENCE_TEMPLATE_SERVICES) ? 'checked=""' : '' ?> >
                    <label class="radio-label" for="typeInput2">
                        <span class="radio-txt-bold">Оказание услуг</span>
                    </label>
                </div>
                <div class="mr-3 mb-2 inline-block">
                    <input id="typeInput3" type="radio" name="Invoice[contract_essence_template]" value="<?=($model::CONTRACT_ESSENCE_TEMPLATE_EMPTY) ?>"
                        <?=($model->contract_essence_template == $model::CONTRACT_ESSENCE_TEMPLATE_EMPTY) ? 'checked=""' : '' ?> >
                    <label class="radio-label" for="typeInput3">
                        <span class="radio-txt-bold">Пустой бланк</span>
                    </label>
                </div>
            </div>
            <div class="txt-grey mb-2">
                Данные шаблоны договоров являются типовыми. Вы можете вносить любые правки согласно <br>
                вашим условиям работы с покупателями
            </div>
            <?= Html::activeTextarea($model, 'contract_essence', [
                'maxlength' => true,
                'class' => 'form-control mb-2',
                'rows' => 8,
                'value' => $model->getContractEssenceText(),
                'data' => [
                    'paylimit' => 'Счет действителен для оплаты до {{date}}.',
                ]
            ]); ?>

            <div class="checkbox">
                <?= Html::activeCheckbox($invoiceContractEssence, 'is_checked', [
                    'label' => false,
                ]); ?>
                <label class="checkbox-label" for="invoicecontractessence-is_checked">
                    <span class="checkbox-txt-bold">Сохранить текст для всех счет-договоров</span>
                </label>
            </div>
        </div>
    </div>

<?php endif; ?>
<script type="text/javascript">

    $(document).on('change', 'select.order_product_tax_rate', function() {

        var $row = $(this).parents('tr');
        var $totalNds = $('.nds-view-item');
        var $invoiceNdsViewType = $('#invoice-nds_view_type_id');
        var showNdsType = $invoiceNdsViewType.attr('data-id') != 2 ?
            $invoiceNdsViewType.attr('data-id') : $('#company-nds_view_type_id').val();

        $row.find('.tax-rate').val($(this).find('option:selected').data('rate'));
        $totalNds.addClass('hidden').filter('.type-' + showNdsType).removeClass('hidden');
        $invoiceNdsViewType.val(showNdsType);

        INVOICE.recalculateInvoiceTable();
    });

    var contractorEssenceTemplateText = <?= json_encode($model->getContractEssenceTemplates()); ?>;
    $(document).on('change', '#config-discount_type input', function () {
        var box = $(this).closest('#invoice_all_discount');
        var value = $('#config-discount_type input:checked', box).val();
        var allDiscount = $('#all-discount', box);
        if (value == $('#config-discount_type', box).data('rub')) {
            allDiscount.prop('disabled', true).val('');
            $('select.price_round_config', box).prop('disabled', true);
        } else {
            allDiscount.prop('disabled', false).val(allDiscount.data('old-value'));
            $('select.price_round_config', box).prop('disabled', false);
        }
    });
    $(document).on('input change', '#invoice_all_discount input, #invoice_all_discount select', function () {
        var changed = false;
        var box = $(this).closest('#invoice_all_discount');
        var allDiscountValue = parseFloat($('#all-discount', box).val()).toString();
        var discountTypeValue = $('#config-discount_type input[type=radio]:checked', box).val();
        var hiddenDiscountValue = $('#config-is_hidden_discount', box).is(":checked") ? '1' : '0';
        if (allDiscountValue != $('#all-discount', box).data('old-value')) {
            changed = true;
        } else if (discountTypeValue != $('#invoice-discount_type').val()) {
            changed = true;
        } else if (hiddenDiscountValue != $('#invoice-is_hidden_discount').val()) {
            changed = true;
        } else if ($('#config-price_round_rule').val() != $('#invoice-price_round_rule').val()) {
            changed = true;
        } else if ($('#config-price_round_precision').val() != $('#invoice-price_round_precision').val()) {
            changed = true;
        }

        $('#discount_submit', box).toggleClass('button-hover-grey', !changed).toggleClass('button-regular_red', changed);
    });
    $(document).on("input change", "input.discount-input", function(e) {
        var p = (parseFloat(this.value) * 1).precision();
        if (p > 2 && $('#config-discount_type input[type=radio]:checked').val() == '1') {
            $("#invoice_price_conf button[value=4]").click();
        }
    });
    $(document).on('input change', '#invoice_all_markup input', function () {
        var changed = false;
        var box = $(this).closest('#invoice_all_markup');
        var allMarkupValue = parseFloat($('#all-markup', box).val()).toString();
        if (allMarkupValue != $('#all-markup', box).data('old-value')) {
            changed = true;
        }

        $('#markup_submit', box).toggleClass('button-hover-grey', !changed).toggleClass('button-regular_red', changed);
    });
    $(document).on('change', '#invoice-contract_essence_template', function (e) {
        var $templateType = $(this).find('input:checked').val();
        var $template = contractorEssenceTemplateText[$templateType].trim();
        var $productRows = $('#table-for-invoice #table-product-list-body tr.product-row');
        var $services = [];
        if ($templateType == 1) {
            if ($productRows.length > 0) {
                $productRows.each(function (e) {
                    $services.push($(this).find('.product-title').val());
                });
                $template = $template.replace('{Наименование услуги}', $services.join(', '));
            }
        }
        $('#invoice-contract_essence').val($template);
    });

    function checkContractEssenceTemplate() {
        var $templateType = $('#invoice-contract_essence_template input:checked').val();
        if ($templateType == 1) {
            var $template = contractorEssenceTemplateText[$templateType].trim();
            var $productRows = $('#table-for-invoice #table-product-list-body tr.product-row');
            var $services = [];
            if ($productRows.length > 0) {
                $productRows.each(function (e) {
                    $services.push($(this).find('.product-title').val());
                });
                $template = $template.replace('{Наименование услуги}', $services.join(', '));
            }
            $('#invoice-contract_essence').val($template);
        }
    }

    $('#moreDetails').find('[type="checkbox"]').on('change' , function() {
        setTimeout("$('.products-scroll-table').mCustomScrollbar('update')", 250);
    });

</script>
