<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceEssence;
use common\models\document\InvoiceExpenditureGroup;
use common\models\employee\Employee;
use common\models\NdsOsno;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\AutoinvoiceForm;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\models\Bank;
use common\components\ImageHelper;

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;

use common\models\employee\EmployeeRole;
use frontend\rbac\permissions;
use \frontend\themes\kub\helpers\Icon;
use yii\web\View;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $fixedContractor boolean */
/* @var $message Message */
/* @var $ioType integer */
/* @var $isAuto boolean */
/* @var $invoiceEssence InvoiceEssence */
/* @var $document string */
/* @var $user Employee */
/* @var $hasProjectEstimate integer */
/* @var $projectEstimateId integer */

$user = Yii::$app->user->identity;
/** @var Company $company */
$company = $user->company;
$userConfig = $user->config;
$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');
$contractorType = ($ioType == Documents::IO_TYPE_IN) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;
$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => !$model->getIsContractorEditable(),
];

$hasContractor = $company->getContractors()
    ->byContractor($contractorType)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->exists();

if (empty($model->contractor_id)) {
    $contractorDropDownConfig['prompt'] = '';
}
$autoDisplay = $model->isAutoinvoice ? 'block' : 'none';
$canEditRs = Yii::$app->getUser()->can(permissions\Company::UPDATE);
$canAddContractor = Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
    'type' => ArrayHelper::getValue($model, 'type'),
]);
$isFirstCreate = $this->context->action->id === 'first-create';
$currentRs = $model->company_rs ? [$model->company_rs => $model->company_bank_name] : [];
$companyRs = $model->company->getRubleAccounts()
    ->select(['name', 'rs', 'id', 'bik'])
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->indexBy('rs')
    ->asArray()->all();

foreach ($companyRs as $key => $rs) {
    $rsData[$rs['rs']] = $rs['name'];
    $companyRs[$key]['bank_logo'] = null;
    $companyRs[$key]['bank_link'] = null;
    /*if ($alias = Banking::aliasByBik($rs['bik'])) {
        $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
        $banking = new $bankingClass($company, [
            'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
        ]);
        if ($banking->getHasAutoload() && !$banking->isValidToken()) {
            $companyRs[$key]['bank_link'] = Html::a("Загрузка выписки из банка автоматически", [
                "/cash/banking/{$alias}/default/index",
            ], [
                'class' => 'bank-link',
                'style' => 'font-size: 13px;',
            ]);

            if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
                $companyRs[$key]['bank_logo'] = ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32]);
            }
        }
    }*/
}
//$rsData = array_merge($currentRs, $companyRs);
if ($canEditRs) {
    $rsData["add-modal-rs"] = Icon::PLUS . ' Добавить рублевый счет ';
}
$hasAdditionals = $model->has_discount || $model->has_markup ||
    $model->show_paylimit_info || $model->comment ||
    $model->currency_name != Currency::DEFAULT_NAME;

$hasPaymentOptions = !$hasAdditionals && (in_array($model->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_MIXED, Invoice::PAYMENT_LIMIT_RULE_DOCUMENT]));

$this->registerJs('$.pjax.defaults.scrollTo = false;', View::POS_LOAD);
?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltipProjectInfo',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'side' => ['top'],
    ],
]); ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltipPaymentRule',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'side' => ['top'],
    ],
]); ?>

<?php
$this->registerCss(<<<CSS
    .hight-line {
        line-height: 24px;
        vertical-align: middle;
    }
CSS)
?>

<?= \yii\helpers\Html::activeHiddenInput($model, 'store_id'); ?>

<?= Html::hiddenInput('is_first_invoice', (int)$isFirstCreate, ['id' => 'is_first_invoice']); ?>

<?= Html::activeHiddenInput($model, 'isAutoinvoice'); ?>

<?= Html::activeHiddenInput($model, 'production_type', [
    'class' => 'invoice-production-type-hidden',
    'value' => '',
]); ?>

<?= Html::hiddenInput('company-nds_view_type_id', $model->company->nds_view_type_id, ['id' => 'company-nds_view_type_id']) ?>

<?php
// Set Agreement data
$agreementBasis = null;
if ($model->isNewRecord && $fromAgreement = \Yii::$app->request->get('fromAgreement')) {
    if ($agreementBasis = Agreement::findOne(['id' => $fromAgreement, 'company_id' => $user->company->id])) {
        $model->company_rs = $agreementBasis->company_rs;
        $model->contractor_id = $agreementBasis->contractor_id;
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+{$agreementBasis->payment_delay} day"));
        $contractorDropDownConfig['disabled'] = true;
    }
}

// Project
$projectId = Yii::$app->request->get('project_id', $model->project_id);
$hasProject = $hasProjectEstimate = Yii::$app->user->identity->menuItem->project_item;
if ($hasProject) {
    $projects = ArrayHelper::map(Project::find()->joinWith('customers')
        ->select(['project.id', 'project.name'])
        ->andWhere(['project.company_id' => $company->id])
        ->andWhere('project.status = ' . intval(Project::STATUS_INPROGRESS))
        ->andWhere('project.start_date <= ' . (new Expression('current_date()')))
        ->andWhere(['or',
            ['>=', 'project.end_date', new Expression('current_date()')],
            ['project.end_date' => null]
        ])
        ->andFilterWhere(['project_customer.customer_id' => ($ioType == Documents::IO_TYPE_OUT ? $model->contractor_id : null)])
        ->all(), 'id', 'name');

}
if ($hasProjectEstimate) {
    $estimates = ProjectEstimate::find()
        ->select(ProjectEstimate::tableName() . '.name')
        ->joinWith('project')
        ->andWhere([Project::tableName() . '.id' => $projectId])
        ->andWhere([Project::tableName() . '.company_id' => $company->id])
        ->andWhere(Project::tableName() . '.start_date <= ' . (new Expression('current_date()')))
        ->andWhere(['or',
            ['>=', 'project.end_date', new Expression('current_date()')],
            ['project.end_date' => null]
        ])
        ->indexBy('id')
        ->column();
}

// SalePoint
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
if ($hasSalePoint) {
    $salePoints = SalePoint::getSelect2Data(true);
}

// Industry
$hasCompanyIndustry = CompanyIndustry::find()->where(['company_id' => $company->id])->exists();
if ($hasCompanyIndustry) {
    $companyIndustries = CompanyIndustry::getSelect2Data();
}

$hasExtraFields =
    $hasProject ||
    $hasProjectEstimate ||
    $hasSalePoint ||
    $hasCompanyIndustry;

$contractorDropDownConfig['options'] = Contractor::getAllContractorSelect2Options($contractorType, Yii::$app->user->identity->company);
?>
<?php if ($canEditRs): ?>
    <style>
        #select2-invoice-company_rs-results .select2-results__option:last-child {
            color: #4679AE;
            font-weight: bold;
        }
    </style>
<?php endif; ?>
<?php if ($canAddContractor): ?>
    <style>
        #select2-invoice-contractor_id-results .select2-results__option:first-child {
            color: #4679AE;
            font-weight: bold;
        }
    </style>
<?php endif; ?>

    <div class="wrap">
        <div class="row d-block">

            <?php if ($isFirstCreate) : ?>
                <!-- FIRST ADD INVOICE -->
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <label class="label">
                                <?= $ioType == Documents::IO_TYPE_IN ? 'Поставщик' : 'Покупатель'; ?>
                                <span class="important" aria-required="true">*</span>
                                <!-- help -->
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_contractor',
                                ]) ?>
                            </label>
                            <div id="invoice-contractor_id-container">
                                <div id="add-first-contractor" class="form-group required details row" style="display: <?= $hasContractor ? 'none' : 'block'; ?>;">
                                    <div class="col-12">
                                        <?= \yii\helpers\Html::textInput('new_contractor_inn', '', [
                                            'id' => 'new_contractor_inn',
                                            'data-required' => 1,
                                            'class' => 'form-control',
                                            'placeholder' => 'Автозаполнение по ИНН ' . ($ioType == Documents::IO_TYPE_OUT ? 'покупателя' : 'продавца')
                                        ]); ?>
                                        <?= $this->render('_new_contractor_inn_api') ?>
                                    </div>
                                </div>
                                <div id="select-existing-contractor" class="form-group required details row" style="display: <?= $hasContractor ? 'block' : 'none'; ?>;">
                                    <div class="col-12">
                                        <?= $form->field($model, 'contractor_id', [
                                            'template' => "{input}",
                                            'options' => [
                                                'class' => 'show-contractor-type-in-fields',
                                            ],
                                        ])->widget(ContractorDropdown::class, [
                                            'company' => $company,
                                            'contractorType' => $contractorType,
                                            'staticData' => [
                                                "add-modal-contractor" => Icon::PLUS . ' Добавить ' . (
                                                    $ioType == Documents::IO_TYPE_IN ? 'поставщика' : 'покупателя'
                                                ),
                                            ],
                                            'options' => $contractorDropDownConfig,
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <?php if ($company->strict_mode) : ?>
                            <div class="form-group required your-company row">
                                <div class="col-12">
                                    <label class="label">
                                        Ваша компания<span class="important" aria-required="true">*</span>
                                        <?php if ($isFirstCreate) : ?>
                                            <span style="color: #ccc; font-weight: normal;">
                                                (<?= $ioType == Documents::IO_TYPE_IN ? 'Покупатель' : 'Поставщик'; ?>)
                                            </span>
                                        <?php endif; ?>
                                        <!-- help -->
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_company',
                                        ]) ?>
                                    </label>
                                    <?= \yii\helpers\Html::textInput('new_company_inn', '', [
                                        'id' => 'new_company_inn',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                        'placeholder' => 'Автозаполнение по ИНН'
                                    ]); ?>
                                    <?= $this->render('_new_company_inn_api') ?>
                                </div>
                                <?= Html::hiddenInput('Company[strict_mode]'); ?>
                            </div>
                            <br>
                            <?php endif ?>
                            <div class="form-group required row <?= ($invoiceBlock) ?>">
                                <div class="col-12">
                                    <label class="label" for="input3">Оплатить до<span class="important">*</span></label>
                                    <div class="date-picker-wrap" style="width: 136px;">
                                        <?php $paymentDelayDays = $model->payment_limit_days ?: 10 ?>
                                        <?= Html::activeTextInput($model, 'payment_limit_date', [
                                            'class' => 'form-control date-picker',
                                            'data-date-viewmode' => 'years',
                                            'data-delay' => $model->contractor ? (
                                                $model->type == Documents::IO_TYPE_IN ?
                                                $model->contractor->seller_payment_delay :
                                                $model->contractor->customer_payment_delay
                                            ) : 10,
                                            'value' => DateHelper::format(!$model->isNewRecord ? $model->payment_limit_date : date(DateHelper::FORMAT_DATE, strtotime('+10 day')), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                        ]); ?>
                                        <svg class="date-picker-icon svg-icon input-toggle">
                                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mt-element-step">

                            <div class="row step-default">
                                <div id="invoise-step-1"
                                     class="col-4 mt-step-col first-invoice-contractor invoice-step<?= $model->contractor ? ' done' : ''; ?>"
                                     data-invoice-ready="35">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">1</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">
                                            <?= ($ioType == \frontend\models\Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?>
                                        </div>
                                        <div class="mt-step-content font-grey-cascade">Заполнить реквизиты</div>
                                    </div>
                                </div>
                                <div id="invoise-step-2"
                                     class="col-4 mt-step-col first-invoice-company invoice-step<?= !$company->strict_mode ? ' done' : ''; ?>"
                                     data-invoice-ready="35">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">2</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">
                                            Ваше <?= $model->company->companyType->name_short ?></div>
                                        <div class="mt-step-content font-grey-cascade">Заполнить реквизиты</div>
                                    </div>
                                </div>
                                <div id="invoise-step-3"
                                     class="col-4 mt-step-col first-invoice-product invoice-step<?= $model->orders ? ' done' : ''; ?>"
                                     data-invoice-ready="30">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">3</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">Товар/Услуга
                                        </div>
                                        <div class="mt-step-content font-grey-cascade">Ввести
                                            название,
                                            количество и цену
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php else: ?>
                <!-- REGULAR INVOICE -->
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Мой расч/счет в<span class="important" aria-required="true">*</span>
                        </label>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'company_rs', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->widget(Select2::class, [
                                    'data' => $rsData,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'templateResult' => new JsExpression('formatCompanyRsTemplateResult'),
                                        'width' => '100%'
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                        'data-update-url' => Url::to(['update-checking-accountant']),
                                        'data-create-url' => Url::to(['create-checking-accountant']),
                                        // 'data-company-rs' => $companyRs,
                                    ]
                                ])->label(false); ?>
                            </div>
                            <div class="col-6 d-flex align-items-center">
                                <?php if (1 || !$model->isNewRecord && $model->company->requisites_updated_at > $model->updated_at) : ?>
                                    <?= Html::a(Html::img('/images/icon-refresh.png', ['class' => 'refresh-contractor-invoice']), '#refresh-company-invoice', [
                                        'class' => 'tooltip3 refresh-contractor-invoice mr-3',
                                        'data-tooltip-content' => '#tooltip_refresh_company',
                                        'data-toggle' => 'modal',
                                    ]) ?>
                                    <img src="" class="little_logo_bank mr-3">
                                    <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                                        'options' => [
                                            'id' => 'refresh-company-invoice',
                                        ],
                                        'toggleButton' => false,
                                        'confirmUrl' => Url::to(['update-company-fields', 'id' => $model->id, 'type' => $model->type, 'isAuto' => $model->isAutoinvoice]),
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите обновить ваши реквизиты в данном счете?',
                                    ]); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label" for="client">
                            <?= ($ioType == Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?><span class="important">*</span>
                        </label>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'contractor_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => 'show-contractor-type-in-fields',
                                    ]
                                ])->widget(ContractorDropdown::class, [
                                    'company' => $company,
                                    'contractorType' => $contractorType,
                                    'staticData' => [
                                        "add-modal-contractor" => Icon::PLUS . ' Добавить ' . (
                                            $ioType == Documents::IO_TYPE_IN ? 'поставщика' : 'покупателя'
                                        ),
                                    ],
                                    'options' => $contractorDropDownConfig,
                                ]); ?>
                            </div>

                            <span class="hidden contractor-invoice-debt small-txt col-3">
                                Есть неоплаченные счета: <span class="count"></span><br>
                                на сумму <span class="amount"></span> Р
                            </span>
                            <?php if (!$model->isNewRecord && $model->checkContractor()): ?>
                                <?= Html::a(Html::img('/images/icon-refresh.png', ['class' => 'refresh-contractor-invoice']), '#refresh-contractor-invoice', [
                                    'class' => 'tooltip3 refresh-contractor-invoice',
                                    'data-tooltip-content' => '#tooltip_refresh_contractor',
                                    'style' => 'padding-top:10px; margin-left: 13px',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#',
                                ]) ?>
                                <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                                    'options' => [
                                        'id' => 'refresh-contractor-invoice',
                                    ],
                                    'toggleButton' => false,
                                    //'toggleButton' => [
                                    //    'label' => Icon::REPEAT,
                                    //    'class' => 'tooltip3 refresh-contractor-invoice',
                                    //    'data-tooltip-content' => '#tooltip_refresh_contractor',
                                    //    'tag' => 'a',
                                    //    'style' => 'padding-top:10px'
                                    //],
                                    'confirmUrl' => Url::to(['update-contractor-fields', 'id' => $model->id, 'type' => $model->type, 'isAuto' => $model->isAutoinvoice]),
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите обновить информацию по ' . ($model->type == Documents::IO_TYPE_OUT ? 'покупателю' : 'продавцу') . ' в данном счете?',
                                ]); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-6 form-group">
                            <div class="form-filter">
                                <label class="label" for="cause">Основание</label>
                            </div>
                            <?= $this->render('_basis_document', [
                                'model' => $model,
                                'agreementBasis' => $agreementBasis,
                            ]) ?>
                        </div>
                        <div class="col-6 <?= ($invoiceBlock) ?>">
                            <div class="">
                                <label class="label" for="input3">Оплатить до<span class="important">*</span></label>
                            </div>
                            <div class="row align-items-center">
                                <div class="form-group col-4 col-xl-4">
                                    <div class="date-picker-wrap">
                                        <?php if (in_array($model->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_DOCUMENT, Invoice::PAYMENT_LIMIT_RULE_MIXED])) { $model->payment_limit_date = null; } ?>
                                        <?= Html::activeTextInput($model, 'payment_limit_date', [
                                            'class' => 'form-control date-picker',
                                            'data-date-viewmode' => 'years',
                                            'data-delay' => $model->contractor ? ($model->type == Documents::IO_TYPE_IN ? $model->contractor->seller_payment_delay : $model->contractor->customer_payment_delay) : 10,
                                            'value' => ($model->payment_limit_date)
                                                ? DateHelper::format(!$model->isNewRecord ? $model->payment_limit_date : date(DateHelper::FORMAT_DATE, strtotime('+10 day')), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                                                : null,
                                            'disabled' => in_array($model->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_DOCUMENT, Invoice::PAYMENT_LIMIT_RULE_MIXED])
                                        ]); ?>
                                        <svg class="date-picker-icon svg-icon input-toggle">
                                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                        </svg>
                                    </div>
                                </div>
                                <?php if (!$isFirstCreate && $ioType == Documents::IO_TYPE_OUT) : ?>
                                    <div class="form-group column">
                                        <div class="d-flex align-items-center">
                                            <label class="checkbox-label checkbox_label_left" for="invoice-remind_contractor">
                                                <span class="checkbox-txt-bold">Отправить напоминание</span>
                                            </label>
                                            <?= Html::activeCheckbox($model, 'remind_contractor', [
                                                'class' => '',
                                                'label' => false,
                                            ]); ?>
                                            <div class="tooltip-box mr-2">
                                                <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                                        data-original-title="Если счет не будет оплачен, то будет автоматически отправлено письмо вашему покупателю с напоминанием об оплате счета">
                                                    <svg class="tooltip-question-icon svg-icon">
                                                        <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                                    </svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label" for="client">Статья расходов<span class="important">*</span></label>
                        <div class="row">
                            <div class="col-6">
                                <?php
                                echo $form->field($model, 'invoice_expenditure_item_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                    ]
                                ])->widget(ExpenditureDropdownWidget::class, [
                                    'loadAssets' => false,
                                    'exclude' => [
                                        'group' => InvoiceExpenditureGroup::TAXES,
                                    ],
                                    'options' => [
                                        'prompt' => '',
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%'
                                    ]
                                ]);
                                echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                                    'inputId' => 'invoice-invoice_expenditure_item_id',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($autoinvoice) : ?>
                <div class="form-group col-xl-4 <?= $autoinvoiceBlock ?>">
                    <div class="mb-0">
                        <div class="inline-block">
                            <label class="label" for="autoinvoice-payment_delay">Отсрочка оплаты<span class="important">*</span></label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'payment_delay',
                                'data' => Autoinvoice::monthDays(),
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                        <div class="inline-block ml-2">
                            <label for="autoinvoice-payment_delay" class="label">
                                дней.
                            </label>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($hasExtraFields): ?>
            <div class="col-12">
                <div class="row">
                    <?php if ($hasCompanyIndustry): ?>
                        <div class="col-3 mt-2">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Направление</label>
                            </div>
                            <?= Select2::widget([
                                'hideSearch' => count($companyIndustries) === 1,
                                'model' => $model,
                                'attribute' => 'industry_id',
                                'data' => $companyIndustries,
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'placeholder' => 'Без направления',
                                    'width' => '100%',
                                    'allowClear' => true,
                                ]
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($hasSalePoint): ?>
                        <div class="col-3 mt-2">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Точка продаж</label>
                            </div>
                            <?= Select2::widget([
                                'model' => $model,
                                'attribute' => 'sale_point_id',
                                'data' => $salePoints,
                                'hideSearch' => count($salePoints) === 1,
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'placeholder' => 'Без точки продаж',
                                    'width' => '100%',
                                    'allowClear' => true,
                                ]
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($hasProject): ?>
                        <div class="<?= $hasProjectEstimate ? 'col-3' : 'col-6' ?> mt-2">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Проект
                                    <span class="tooltipProjectInfo" data-tooltip-content='#project-info'>
                                        <?= Icon::QUESTION; ?>
                                    </span>
                                </label>
                            </div>
                            <?= $this->render('_project_selecting', [
                                'model' => $model,
                                'projects' => $projects,
                                'projectId' => $projectId,
                                'contractorId' => (bool)$model->contractor_id,
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($hasProjectEstimate): ?>
                        <div class="col-3 mt-2">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Смета</label>
                            </div>
                            <?= $this->render('_project_estimate_selecting', [
                                'model' => $model,
                                'estimates' => $estimates,
                                'projectEstimateId' => $projectEstimateId,
                                'contractorId' => (bool)$model->contractor_id,
                            ]) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($isFirstCreate) : ?>
                <?php if ($company->companyTaxationType->osno) : ?>
                    <br/>
                    <div class="form-group col-6">

                        <label for="invoice-payment_limit_date" class="label">
                            Тип
                            <?php if ($document == null): ?>
                                счета
                            <?php endif; ?>
                            <span class="important" aria-required="true">*</span>:
                        </label>

                        <?php
                        if ($company->nds === null) {
                            $company->nds = NdsOsno::WITH_NDS;
                        }
                        echo \yii\helpers\Html::activeRadioList($company, 'nds', ArrayHelper::map(NdsOsno::find()->all(), 'id', 'name'), [
                            'class' => 'inp_one_line_company checkbox-width',
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return '<div class="col-xs-12 pad0">' .
                                    Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                        'class' => 'pr-3',
                                    ]) . Html::a('Пример счета', [
                                        Url::to('/company/view-example-invoice'),
                                        'nds' => $value == 1 ? true : false,
                                    ], [
                                        'class' => 'marg radio-inline radio-padding',
                                        'target' => '_blank',
                                    ]) .
                                    "</div>";
                            },
                        ]);
                        ?>
                    </div>

                <?php endif; ?>
            <?php endif; ?>

            <?php if ($autoinvoice) : ?>
                <?php
                $isMonthly = $autoinvoice->period == Autoinvoice::MONTHLY;
                $isQuarterly = $autoinvoice->period == Autoinvoice::QUARTERLY;
                $isHalfyear = $autoinvoice->period == Autoinvoice::HALF_YEAR;
                $isAnnually = $autoinvoice->period == Autoinvoice::ANNUALLY;
                $this->registerJs('
                    $(document).on("change", "#autoinvoiceform-period", function () {
                        var period = $(this).val();
                        var monthly = "' . Autoinvoice::MONTHLY . '";
                        var quarterly = "' . Autoinvoice::QUARTERLY . '";
                        var halfyear = "' . Autoinvoice::HALF_YEAR . '";
                        var annually = "' . Autoinvoice::ANNUALLY . '";
                        $(".period-dependent").toggleClass("hidden", true);
                        $(".period-dependent select").prop("disabled", true);
                        switch (period) {
                            case monthly:
                                $(".period-dependent.period-monthly").toggleClass("hidden", false);
                                $(".period-dependent.period-monthly select").prop("disabled", false);
                                break;
                            case quarterly:
                                $(".period-dependent.period-quarterly").toggleClass("hidden", false);
                                $(".period-dependent.period-quarterly select").prop("disabled", false);
                                break;
                            case halfyear:
                                $(".period-dependent.period-halfyear").toggleClass("hidden", false);
                                $(".period-dependent.period-halfyear select").prop("disabled", false);
                                break;
                            case annually:
                                $(".period-dependent.period-annually").toggleClass("hidden", false);
                                $(".period-dependent.period-annually select").prop("disabled", false);
                                break;
                        }
                    });
                ');
                ?>

                <div id="autoinvoice-body" class="form-group col-12 <?= $autoinvoiceBlock ?>">
                    <br>
                    <div class="row form-group">
                        <div class="col-3">
                            <label class="label" for="cause">Периодичность<span class="important">*</span></label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'period',
                                'data' => Autoinvoice::$PERIOD,
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                        <div class="col-3 period-dependent period-quarterly<?= $isQuarterly ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">Месяц квартала<span class="important">*</span></label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'month',
                                'data' => Autoinvoice::quarterMonths(),
                                'options' => [
                                    'id' => 'autoinvoice-day-quarterly',
                                    'class' => 'form-control',
                                    'disabled' => !$isQuarterly,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                        <div class="col-3 period-dependent period-halfyear<?= $isHalfyear ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">Месяц полугодия<span class="important">*</span></label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'month',
                                'data' => Autoinvoice::halfyearMonths(),
                                'options' => [
                                    'id' => 'autoinvoice-day-halfyear',
                                    'class' => 'form-control',
                                    'disabled' => !$isHalfyear,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                        <div class="col-3 period-dependent period-annually<?= $isAnnually ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">Месяц года<span class="important">*</span></label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'month',
                                'data' => Autoinvoice::annuallyMonths(),
                                'options' => [
                                    'id' => 'autoinvoice-day-annually',
                                    'class' => 'form-control',
                                    'disabled' => !$isAnnually,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                        <div class="col-3">
                            <label class="label" for="cause">
                                Число месяца<span class="important">*</span>
                                <?= Icon::get('question', [
                                    'class' => 'tooltip-question-icon ml-2',
                                    'title' => 'Если число выпадает на субботу или воскресенье, то счет уходит в понедельник. '.
                                        'Если число выпадает на субботу или воскресенье, а понедельник это уже следующий месяц, то счет уйдет в пятницу.',
                                ]) ?>
                            </label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'day',
                                'data' => Autoinvoice::monthDays(),
                                'options' => [
                                    'id' => 'autoinvoice-day-monthly',
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <br>
                    <div class="row form-group">
                        <div class="form-group col-3">
                            <label class="label" for="cause">Начало<span class="important">*</span></label>
                            <div class="date-picker-wrap">
                                <?= Html::activeTextInput($autoinvoice, 'dateFrom', [
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                ]); ?>
                                <svg class="date-picker-icon svg-icon input-toggle">
                                    <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="form-group col-3">
                            <label class="label" for="cause">Окончание<span class="important">*</span></label>
                            <div class="date-picker-wrap">
                                <?= Html::activeTextInput($autoinvoice, 'dateTo', [
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                ]); ?>
                                <svg class="date-picker-icon svg-icon input-toggle">
                                    <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6 period-dependent period-monthly<?= $isMonthly ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">В названии услуг, автоматически добавлять</label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'add_month_and_year',
                                'data' => Autoinvoice::$addToTitle1,
                                'options' => [
                                    'id' => 'autoinvoice-add-monthly',
                                    'class' => 'form-control',
                                    'value' => $autoinvoice->add_month_and_year,
                                    'disabled' => !$isMonthly,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                            <?= Html::tag('span', '', [
                                'class' => 'tooltip3 ico-question valign-middle',
                                'data-tooltip-content' => '#product_services_title',
                                'style' => 'position: absolute; top: 2px; right: -5px;',
                            ]) ?>
                        </div>
                        <div class="col-6 period-dependent period-quarterly<?= $isQuarterly ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">В названии услуг, автоматически добавлять</label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'add_month_and_year',
                                'data' => Autoinvoice::$addToTitle2,
                                'options' => [
                                    'id' => 'autoinvoice-add-quarterly',
                                    'class' => 'form-control',
                                    'value' => $autoinvoice->add_month_and_year,
                                    'disabled' => !$isQuarterly,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                            <?= Html::tag('span', '', [
                                'class' => 'tooltip3 ico-question valign-middle',
                                'data-tooltip-content' => '#product_services_title',
                                'style' => 'position: absolute; top: 2px; right: -5px;',
                            ]) ?>
                        </div>
                        <div class="col-6 period-dependent period-halfyear<?= $isHalfyear ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">В названии услуг, автоматически добавлять</label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'add_month_and_year',
                                'data' => Autoinvoice::$addToTitle3,
                                'options' => [
                                    'id' => 'autoinvoice-add-halfyear',
                                    'class' => 'form-control',
                                    'value' => $autoinvoice->add_month_and_year,
                                    'disabled' => !$isHalfyear,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                            <?= Html::tag('span', '', [
                                'class' => 'tooltip3 ico-question valign-middle',
                                'data-tooltip-content' => '#product_services_title',
                                'style' => 'position: absolute; top: 2px; right: -5px;',
                            ]) ?>
                        </div>
                        <div class="col-6 period-dependent period-annually<?= $isAnnually ? '' : ' hidden'; ?>">
                            <label class="label" for="cause">В названии услуг, автоматически добавлять</label>
                            <?= Select2::widget([
                                'model' => $autoinvoice,
                                'attribute' => 'add_month_and_year',
                                'data' => Autoinvoice::$addToTitle4,
                                'options' => [
                                    'id' => 'autoinvoice-add-annually',
                                    'class' => 'form-control',
                                    'value' => $autoinvoice->add_month_and_year,
                                    'disabled' => !$isAnnually,
                                ],
                                'pluginOptions' => [
                                    'width' => '100%',
                                    'minimumResultsForSearch' => -1
                                ]
                            ]) ?>
                            <?= Html::tag('span', '', [
                                'class' => 'tooltip3 ico-question valign-middle',
                                'data-tooltip-content' => '#product_services_title',
                                'style' => 'position: absolute; top: 2px; right: -5px;',
                            ]) ?>
                        </div>
                    </div>

                    <div class="autoinvoice_send_to collapse show <?php /*<?= $model->contractor ? 'in' : '' ?>*/ ?>">
                        <div class="form-group row">
                            <div class="col-6">
                                <label for="under-date" class="label">
                                    <?= $autoinvoice->getAttributeLabel('send_to') ?>
                                    <span class="important" aria-required="true">*</span>:
                                </label>
                                <?= Html::activeCheckboxList($autoinvoice, 'send_to', $autoinvoice->sendItems, [
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model, $autoinvoice) {
                                        $isDirector = $value == AutoinvoiceForm::TO_DIRECTOR ? null : (
                                        isset($model->contractor->{$value . '_is_director'}) ?
                                            $model->contractor->{$value . '_is_director'} :
                                            1
                                        );
                                        return Html::checkbox($name, $checked, [
                                            'value' => $value,
                                            'class' => 'item-send-to',
                                            'label' => $this->render('_form_send_to_item', [
                                                'model' => $model,
                                                'autoinvoice' => $autoinvoice,
                                                'index' => $index,
                                                'label' => $label,
                                                'name' => $name,
                                                'checked' => $checked,
                                                'value' => $value,
                                                'isDirector' => $isDirector,
                                            ]),
                                            'data-is-director' => $isDirector,
                                            'labelOptions' => [
                                                'class' => 'send_to',
                                            ],
                                        ]);
                                    }
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <br>
            <div class="form-group col-12">
                <button class="link link_collapse link_bold button-clr  <?=($model->is_surcharge) ? 'hide':'' ?> <?= $hasAdditionals ? '' : ' collapsed'; ?>" type="button" data-toggle="collapse" data-target="#moreDetails" aria-expanded="false" aria-controls="moreDetails">
                    <span class="link-txt">Дополнительные параметры</span>
                    <svg class="link-shevron svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                    </svg>
                </button>

                <?php if ($ioType == Documents::IO_TYPE_OUT && !$isAuto): ?>
                    <button class="link link_collapse link_bold button-clr <?= !$model->contractor ? 'tooltipPaymentRule no-empty-contractor-collapsable' : '' ?> <?= $hasPaymentOptions ? '' : ' collapsed'; ?> <?=($model->is_surcharge) ? 'hide':'' ?>"
                            type="button"
                            data-toggle="collapse"
                            data-target="#morePaymentRules"
                            aria-expanded="false"
                            aria-controls="morePaymentRules"
                            style="position: absolute; right: 15px;" data-tooltip-content="#payment-rule-empty">
                        <span class="link-txt">Настройка отсрочки платежа</span>
                        <svg class="link-shevron svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>
                <?php endif; ?>

            </div>
            <div class="col-12">
                <div class="collapse<?= $hasAdditionals ? ' show' : ''; ?>" id="moreDetails">
                    <br>
                    <div class="row mt-3">
                        <div class="col-6 col-xl-4 addition-params">
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_discount">
                                        <span class="checkbox-txt">Указать скидку</span>
                                        <?= Html::activeCheckbox($model, 'has_discount', [
                                            'id' => 'invoice-has_discount',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-has_markup">
                                            <span class="checkbox-txt">Указать наценку</span>
                                            <?= Html::activeCheckbox($model, 'has_markup', [
                                                'id' => 'invoice-has_markup',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($ioType == Documents::IO_TYPE_OUT && $company->companyTaxationType->usn) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-hasnds">
                                    <span class="checkbox-txt">
                                        <?php if ($document == Documents::SLUG_ACT): ?>
                                            Акт
                                        <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                            ТН
                                        <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                            СФ
                                        <?php elseif ($document == Documents::SLUG_UPD): ?>
                                            Д
                                        <?php else: ?>
                                            Счет
                                        <?php endif; ?>
                                        с НДС
                                    </span>
                                            <?= Html::activeCheckbox($model, 'hasNds', [
                                                'id' => 'invoice-hasnds',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($ioType == Documents::IO_TYPE_OUT && $document == null) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-show_paylimit_info">
                                            <span class="checkbox-txt">Указать срок оплаты</span>
                                            <?= Html::activeCheckbox($model, 'show_paylimit_info', [
                                                'id' => 'invoice-show_paylimit_info',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                    <div class="tooltip-box">
                                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                                data-original-title='Срок оплаты подставляется автоматически в комментарий к счету из поля "Оплатить до"'>
                                            <svg class="tooltip-question-icon svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_weight">
                                        <span class="checkbox-txt">Указать вес</span>
                                        <?= Html::activeCheckbox($model, 'has_weight', [
                                            'id' => 'invoice-has_weight',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_volume">
                                        <span class="checkbox-txt">Указать объем</span>
                                        <?= Html::activeCheckbox($model, 'has_volume', [
                                            'id' => 'invoice-has_volume',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <div id="config_items_box">
                                        <label class="checkbox-label" for="config-invoice_form_article">
                                            <span class="checkbox-txt">Указать артикул</span>
                                            <input class="checkbox-input input-hidden"
                                                   id="config-invoice_form_article"
                                                   type="checkbox"
                                                   data-target="col_invoice_form_article"
                                                <?= ($userConfig->invoice_form_article) ? ' checked="" ' : '' ?>>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-not_for_bookkeeping">
                                            <?= $form->field($model, 'not_for_bookkeeping')
                                                ->checkbox([
                                                    'id' => 'invoice-not_for_bookkeeping',
                                                    'class' => 'checkbox-input input-hidden',
                                                    'label' => 'Счет не для бухгалтерии',
                                                ]); ?>
                                        </label>
                                    </div>
                                    <div class="tooltip-box">
                                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                                data-original-title='По данному счету можно будет проставить только по кассе.
                                                По данному счету нельзя будет выставить акт, товарную накладную, УПД, счет-фактуру.
                                                Для списания товара со склада появится документ "Расходная накладная", которую вы также можете отдать
                                                клиенту.'>
                                            <svg class="tooltip-question-icon svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?= $this->render('_form_currency', [
                            'form' => $form,
                            'model' => $model,
                        ]) ?>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="checkbox-txt" for="input2">
                                <span class="pb-2">Комментарий в счете для покупателя</span>
                                <div class="tooltip-box tooltip-box_side-margins" style="margin-top:-5px;">
                                    <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                            data-original-title="Добавьте к счету комментарий для покупателя. Покупатель получит дополнительную информацию о деталях оплаты, условиях договора и тд.">
                                        <svg class="tooltip-question-icon svg-icon">
                                            <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                        </svg>
                                    </button>
                                </div>
                            </label>
                            <div class="form-group mb-2">
                                <?= Html::activeTextarea($model, 'comment', [
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                    'value' => $invoiceEssence->is_checked && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT ?
                                        $invoiceEssence->text : $model->comment,
                                    'data' => [
                                        'currency' => 'Оплата счета производится в российских рублях по курсу ЦБ РФ на день оплаты.',
                                        'paylimit' => 'Счет действителен для оплаты до {{date}}.',
                                    ]
                                ]); ?>
                            </div>
                            <div class="form-group mb-0">
                                <div class="checkbox">
                                    <label class="checkbox-label" for="invoiceessence-is_checked">
                                        <?= Html::activeCheckbox($invoiceEssence, 'is_checked', [
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                        <span class="checkbox-txt">Сохранить текст для всех счетов</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php if ($ioType == Documents::IO_TYPE_OUT && !$isAuto): ?>
                <?= $this->render('_form_payment_rule', [
                    'model' => $model,
                    'defaultShow' => $hasPaymentOptions
                ]) ?>
            <?php endif; ?>

        </div>
    </div>


    <div style="display:none">
        <div id="project-info">
            Если у вас есть проекты, то для учета приходов по проектам, выберите проект.
        </div>
        <div id="contractor-empty">
            Укажите покупателя, чтобы добавить основание
        </div>
        <div id="payment-rule-empty">
            Укажите покупателя, чтобы открыть настройку
        </div>
    </div>

<?php $this->registerJs('
    $companyRs = ' . json_encode($companyRs) . ';

    function htmlEscape(str) {
        return str
            .replace(/&/g, \'&amp;\')
            .replace(/"/g, \'&quot;\')
            .replace(/\'/g, \'&#39;\')
            .replace(/</g, \'&lt;\')
            .replace(/>/g, \'&gt;\');
    }

    function formatCompanyRsTemplateResult(data, container) {
        if (data.id !== "add-modal-rs" && data.disabled !== true) {
            var input = $("#invoice-company_rs");
            var content = \'<div class="company_rs-item-name-label">\';
            if (' . (int)$canEditRs . ') {
                content += \'<i class="icon icon-pencil pull-right edit-company_rs-item ajax-modal-btn" data-id="\' + data.id + \'" data-url="\' + input.data("update-url") + \'?id=\' + $companyRs[data.id].id + \'" title="Обновить"></i>\';
            }
            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            container.innerHTML = content;
        } else {
            var input = $("#invoice-company_rs");
            $(container).addClass("ajax-modal-btn").attr("title", "Добавить рублевый счет").attr("data-url", input.data("create-url"));
            container.innerHTML = data.text;
        }

        return container;
    };
', \yii\web\View::POS_HEAD);

$this->registerJs('
    function loadBankLogo(bankRs) {
        if (+$("#is_first_invoice").val() !== 1 && +$("#invoiceauto-isautoinvoice").val() !== 1 && $companyRs[bankRs] !== undefined) {
            var $logo = $companyRs[bankRs].bank_logo;
            var $link = $companyRs[bankRs].bank_link;

            if ($logo !== null) {
                $(".little_logo_bank").attr("src", $logo);
            } else {
                $(".little_logo_bank").attr("src", "");
            }
            $(".little_logo_bank").siblings(".bank-link").remove();
            if ($link) {
                $(".little_logo_bank").after($link);
            }
        }
    };

    $(document).ready(function () {
        loadBankLogo($("#invoice-company_rs").val());
    });

    $(document).on("change", "#invoice-company_rs", function () {
        loadBankLogo($(this).val());
    });

    $(document).on("submit", "form.form-checking-accountant", function () {
        $form = $(this);
        $.post($form.attr("action"), $form.serialize(), function (data) {
            if (data.result == true) {
                var select = $("#invoice-company_rs");
                if (data.model !== null) {
                    $companyRs = data.companyRs;
                    select.prepend(new Option(data.model.name, data.model.rs)).val(data.model.rs).trigger("change");
                }
                $form.closest(".modal").modal("hide");
            } else {
                $form.html($(data.html).find("form").html());
            }
        });

        return false;
    });

    // sync fields
    $("#autoinvoice-add-monthly").on("change", function() {
        if ($("#autoinvoice-add-quarterly").val() !== $(this).val())
            $("#autoinvoice-add-quarterly").val($(this).val()).trigger("change");
    });
    $("#autoinvoice-add-quarterly").on("change", function() {
        if ($("#autoinvoice-add-monthly").val() !== $(this).val())
            $("#autoinvoice-add-monthly").val($(this).val()).trigger("change");
    });

    $(document).on("select2:selecting", "#invoice-agreement", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-agreement-item")) {
            e.preventDefault();

            $("#invoice-agreement").select2("close");

            $.pjax({
                url: "/documents/agreement/update" +
                    "?id=" + $(target).data("id") +
                    "&contractor_id=" + $("#invoice-contractor_id").val() +
                    "&type=" + $("#documentType").val() +
                    "&returnTo=invoice" +
                    "&container=agreement-select-container" +
                    "&old_record=1" +
                    "&hide_template_fields=1",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });

            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));

            });
            $("#agreement-modal-container").modal("show");
            $("#invoice-agreement").val("").trigger("change");

        }
    });
    $(document).on("select2:open", "#invoice-agreement", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });

    $(document).on("select2:open", "#invoice-company_rs", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });
    $(document).on("select2:selecting", "#invoice-company_rs", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-company_rs-item") || target.classList.contains("ajax-modal-btn")) {
            e.preventDefault();
        }
    });
    $(document).on("keyup", "input.select2-search__field", function (e) {
        $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
            .attr("aria-selected", "false")
            .addClass("selected");
    });

    $(document).on("shown.bs.modal", ".modal#ajax-modal-box", function () {
        $("#invoice-company_rs").select2("close");
    });

    $(document).on("click", "#contractor_update_button", function() {
            var url  = "add-modal-contractor";
            var form = {
                documentType: INVOICE.documentIoType,
                contractorId: $(this).data("contractor-id") || 0,
                invoiceId: $(this).data("invoice-id") || 0,
        };
        INVOICE.addNewContractor("add-modal-contractor", form);
    });

    $("#config-invoice_form_article").on("change", function(e) {
        var input = this;
        var attr = input.id.replace("config-", "");
        $.post("/site/config", {"Config[invoice_form_article]": $("#config-invoice_form_article").prop("checked") ? 1 : 0}, function(data) {
            if (typeof data[attr] !== "undefined") {
                if (data[attr]) {
                    $("." + $(input).data("target")).removeClass("hidden");
                } else {
                    $("." + $(input).data("target")).addClass("hidden");
                }
            }
        })
    });

    $(document).on("shown.bs.modal", "#add-new", function() {
        refreshUniform();
    });

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });
    
    // PAYMENT LIMIT RULES
    
    $(document).on("change", ".radio_payment_limit_rule", function() {
        const block = $("#morePaymentRules");
        const subBlock = $(this).closest(".radio-label");
        const inputPaymentLimitDate = $("#invoice-payment_limit_date");
        
        $(block).find(".rule-inner").addClass("hidden");
        $(subBlock).find(".rule-inner").removeClass("hidden");
        
        // by docs only
        if (this.value == 3 || this.value == 2) {
            inputPaymentLimitDate.val(null).trigger("change").prop("disabled", true);
        } else {
            inputPaymentLimitDate.prop("disabled", false);
            $("#under-date").trigger("change");        
        }
    });
    
    $(document).on("show.bs.collapse", "#moreDetails", function() {
        $("#morePaymentRules").collapse("hide");
    });
    $(document).on("show.bs.collapse", "#morePaymentRules", function() {
        $("#moreDetails").collapse("hide");
    });
    $(document).on("change", "#input_payment_limit_percent", function() {
        let percent = Math.round(Number(this.value)) || 100;
        
        if (percent < 0) 
            percent = 100;
        else if (percent > 100) 
            percent = 100;
            
        $(this).val(percent);
        $("#input_payment_limit_percent_related").val(100 - percent); 
    });
    $(document).on("change", ".input-change-date-delay", function() {
        const delay = Math.round(Number(this.value));
        if (delay > 0) {
            $("#invoice-agreement").data("delay", delay);
            $("#invoice-payment_limit_date").data("delay", delay);
            $("#under-date").trigger("change");
        }
    });
    $(".no-empty-contractor-collapsable").on("click", function (e) {
        const contractorId = Number($("select#invoice-contractor_id").val());
        if (!contractorId) 
            e.stopPropagation();
    });
    $(document).on("change", "select#invoice-contractor_id", function() {
        if (Number(this.value) > 0) {
            $(".tooltipPaymentRule.no-empty-contractor-collapsable")
                .first()
                .tooltipster("disable");
        } else {
            $(".tooltipPaymentRule.no-empty-contractor-collapsable")
                .first()
                .tooltipster("enable");
                
            $("#morePaymentRules").collapse("hide");
        }
    });
');

