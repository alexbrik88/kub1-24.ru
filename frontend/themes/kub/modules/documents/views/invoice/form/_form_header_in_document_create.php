<?php
use common\components\date\DateHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap4\Dropdown;
?>
<style>
    #account-number, #invoice-account-number {width:80px!important;}
    #under-date, #invoice-under-date {width:120px!important;}
</style>
<span class="<?= $invoiceBlock ?>">
    <?= Html::hiddenInput('', (int)$model->isNewRecord, [
        'id' => 'isNewRecord',
    ]) ?>
    <?= Html::activeHiddenInput($model, 'is_invoice_contract'); ?>
    <?php if ($document == Documents::SLUG_ACT): ?>
        <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий акт' : 'Входящий акт' ?>
    <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
        <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая ТН' : 'Входящая ТН' ?>
    <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
        <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая СФ' : 'Входящая СФ' ?>
    <?php elseif ($document == Documents::SLUG_UPD): ?>
        <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий УПД' : 'Входящий УПД' ?>
    <?php else: ?>
        <?= die('Unknown IN Document Type.') ?>
    <?php endif; ?>
    <?= Html::textInput('NewDoc[document_number]', $newDoc['document_number'], [
        'id' => 'account-number',
        'data-required' => 1,
        'class' => 'form-control',
    ]); ?>

    <br class="box-br">
</span>
<span class="box-margin-top-t">от</span>
<div class="input-icon box-input-icon-top"
     style="display: inline-block; vertical-align: top; margin-left: 6px">
    <i class="fa fa-calendar"></i>
    <?= Html::textInput('NewDoc[document_date]', DateHelper::format($newDoc['document_date'],
        DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE), [
        'id' => 'under-date',
        'class' => 'form-control date-picker invoice_document_date',
        'size' => 16,
        'data-date' => '12-02-2012',
        'data-date-viewmode' => 'years',
    ]); ?>
</div>
<div class="new-document-invoice">
    <span class="<?= $invoiceBlock ?>">
        <span style="padding-left:5px;">к счету</span>
        <?= Html::activeTextInput($model, 'document_number', [
            'id' => 'invoice-account-number',
            'data-required' => 1,
            'class' => 'form-control',
        ]); ?>

        <br class="box-br">
    </span>
    <span class="box-margin-top-t">от</span>
    <div class="input-icon box-input-icon-top"
         style="display: inline-block; vertical-align: top; margin-left: 6px">
        <i class="fa fa-calendar"></i>
        <?= Html::activeTextInput($model, 'document_date', [
            'id' => 'invoice-under-date',
            'class' => 'form-control date-picker invoice_document_date',
            'size' => 16,
            'data-date' => '12-02-2012',
            'data-date-viewmode' => 'years',
            'value' => DateHelper::format($model->document_date,
                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>
    </div>
</div>
