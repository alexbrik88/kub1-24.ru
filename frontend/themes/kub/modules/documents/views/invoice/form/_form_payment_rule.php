<?php

use yii\helpers\Html;
use common\models\document\Invoice;
use frontend\components\BusinessAnalyticsAccess;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/** @var Invoice $model */

$checkedRuleInvoice = ($model->payment_limit_rule == Invoice::PAYMENT_LIMIT_RULE_INVOICE);
$checkedRuleDocument = ($model->payment_limit_rule == Invoice::PAYMENT_LIMIT_RULE_DOCUMENT);
$checkedRuleMixed = ($model->payment_limit_rule == Invoice::PAYMENT_LIMIT_RULE_MIXED);

$defaultShow = $defaultShow ?? false;
$show = ($defaultShow) ? 'show' : '';
$canAccess = BusinessAnalyticsAccess::matchSubscribe(BusinessAnalyticsAccess::SECTION_INVOICE_PAYMENT_LIMIT);
?>

<div class="col-12">
    <div class="collapse <?= $show ?>" id="morePaymentRules">
        <br>
        <div class="row mt-3">
            <div class="col-6 pl-0">
            </div>
            <div class="col-6 pr-0">

                <!-- INVOICE -->
                <div class="mr-0">
                    <label class="radio-label w-100" for="radio_payment_limit_rule_invoice">
                        <input id="radio_payment_limit_rule_invoice"
                               class="radio-input input-hidden radio_payment_limit_rule"
                               type="radio"
                               name="Invoice[payment_limit_rule]"
                               value="<?=(Invoice::PAYMENT_LIMIT_RULE_INVOICE)?>"
                               <?= ($checkedRuleInvoice) ? 'checked=""' : '' ?>>

                            <span class="radio-txt">
                                <span class="d-block mb-2 mt-1">Отсрочка только ПО СЧЕТУ</span>
                                <div class="rule-inner row m-0 <?=($checkedRuleInvoice ? '' : 'hidden')?>">
                                    <div class="col-12 p-0">
                                        <span class="d-block label">Отсрочка оплаты в днях от даты счета</span>
                                        <?= Html::textInput('paymentLimitRule[1][payment_limit_days]',
                                            $model->payment_limit_days ?: 10, [
                                            'class' => 'form-control input-change-date-delay',
                                            'style' => 'width: 120px; height: 40px;',
                                        ]) ?>
                                    </div>
                                </div>
                            </span>
                    </label>
                </div>

                <!-- DOCUMENT -->
                <div class="mr-0">
                    <label class="radio-label w-100 tooltip-disallow" for="radio_payment_limit_rule_document" data-tooltip-content="#payment_limit_rule_tooltip_2">
                        <input id="radio_payment_limit_rule_document"
                               class="radio-input input-hidden radio_payment_limit_rule"
                               type="radio"
                               name="Invoice[payment_limit_rule]"
                               value="<?=(Invoice::PAYMENT_LIMIT_RULE_DOCUMENT)?>"
                               <?= (!$canAccess) ? 'disabled="disabled"' : '' ?>
                               <?= ($checkedRuleDocument) ? 'checked=""' : '' ?>>

                            <span class="radio-txt">
                                <span class="d-block mb-2 mt-1">Отсрочка только ПО Акту, ТН или УПД</span>
                                <div class="rule-inner row m-0 <?=($checkedRuleDocument ? '' : 'hidden')?>">
                                    <div class="col-12 p-0">                                
                                        <span class="d-block label">Отсрочка оплаты в днях от даты документа</span>
                                        <?= Html::textInput('paymentLimitRule[2][related_document_payment_limit_days]',
                                            $model->related_document_payment_limit_days, [
                                            'class' => 'form-control',
                                            'style' => 'width: 120px; height: 40px;',
                                        ]) ?>
                                    </div>
                                </div>
                            </span>
                    </label>
                </div>

                <!-- INVOICE + DOCUMENT -->
                <div class="mr-0">
                    <label class="radio-label w-100 tooltip-disallow" for="radio_payment_limit_rule_mixed" data-tooltip-content="#payment_limit_rule_tooltip_3">
                        <input id="radio_payment_limit_rule_mixed"
                               class="radio-input input-hidden radio_payment_limit_rule"
                               type="radio"
                               name="Invoice[payment_limit_rule]"
                               value="<?=(Invoice::PAYMENT_LIMIT_RULE_MIXED)?>"
                               <?= (!$canAccess) ? 'disabled=""' : '' ?>
                               <?= ($checkedRuleMixed) ? 'checked=""' : '' ?>>

                            <span class="radio-txt" style="width: 90%">
                                <span class="d-block mb-2 mt-1">Отсрочка и ПО СЧЕТУ и ПО Акту, ТН или УПД</span>

                                <div class="rule-inner row m-0 <?=($checkedRuleMixed ? '' : 'hidden')?>">
                                    <div class="col-6 p-0">
                                        <span class="d-block label">Отсрочка оплаты в днях<br>от даты счета</span>
                                    </div>
                                    <div class="col-6 p-0">
                                        <span class="d-block label">% суммы аванса от суммы счета</span>
                                    </div>
                                </div>

                                <div class="rule-inner row m-0 <?=($checkedRuleMixed ? '' : 'hidden')?>">
                                    <div class="col-6 p-0">
                                        <?= Html::textInput('paymentLimitRule[3][payment_limit_days]',
                                            $model->payment_limit_days, [
                                            'class' => 'form-control input-change-date-delay',
                                            'style' => 'width: 120px; height: 40px;',
                                        ]) ?>
                                    </div>
                                    <div class="col-6 p-0">
                                        <?= Html::textInput('paymentLimitRule[3][payment_limit_percent]',
                                        $model->payment_limit_percent ?: 100, [
                                            'id' => 'input_payment_limit_percent',
                                            'class' => 'form-control',
                                            'style' => 'width: 120px; height: 40px;',
                                        ]) ?>
                                    </div>
                                </div>

                                <div class="rule-inner row m-0 <?=($checkedRuleMixed ? '' : 'hidden')?>">
                                    <div class="col-6 p-0">
                                        <span class="d-block label">Отсрочка оплаты в днях<br>от Акта, ТН или УПД</span>
                                    </div>
                                    <div class="col-6 p-0">
                                        <span class="d-block label">% оставшейся суммы оплаты после выставления Акта, ТН или УПД</span>
                                    </div>
                                </div>

                                <div class="rule-inner row m-0 <?=($checkedRuleMixed ? '' : 'hidden')?>">
                                    <div class="col-6 p-0">
                                        <?= Html::textInput('paymentLimitRule[3][related_document_payment_limit_days]',
                                        $model->related_document_payment_limit_days, [
                                            'class' => 'form-control',
                                            'style' => 'width: 120px; height: 40px;',
                                        ]) ?>
                                    </div>
                                    <div class="col-6 p-0">
                                        <?= Html::textInput('paymentLimitRule[3][related_document_payment_limit_percent]',
                                        $model->related_document_payment_limit_percent ?: 0, [
                                            'id' => 'input_payment_limit_percent_related',
                                            'class' => 'form-control',
                                            'style' => 'width: 120px; height: 40px;',
                                            'readonly' => true
                                        ]) ?>
                                    </div>
                                </div>
                            </span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!$canAccess): ?>

    <?= yii2tooltipster::widget([
        'options' => [
            'class' => '.tooltip-disallow',
        ],
        'clientOptions' => [
            'theme' => ['tooltipster-kub t-doc-date-select'],
            'contentAsHTML' => true,
            'interactive' => true,
            'trigger' => 'click',
        ],
    ]) ?>

    <div style="display: none">
        <div id="payment_limit_rule_tooltip_2">
            <div class="p-2">
                Доступно только на тарифах «ФинДиректор», <br/>
                «ФинДиректор PRO» и «ФинДиректор Всё включено»<br/>
                <a class="link" href="/subscribe/default/index" target="_blank">Перейти к оплате</a>
            </div>
        </div>
        <div id="payment_limit_rule_tooltip_3">
            <div class="p-2">
                Доступно только на тарифах «ФинДиректор», <br/>
                «ФинДиректор PRO» и «ФинДиректор Всё включено»<br/>
                <a class="link" href="/subscribe/default/index" target="_blank">Перейти к оплате</a>
            </div>
        </div>
    </div>

<?php endif; ?>