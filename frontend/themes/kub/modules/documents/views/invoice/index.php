<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use frontend\components\Icon;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $prompt backend\models\Prompt */
/* @var $user Employee */

$this->title = $message->get(Message::TITLE_PLURAL);

$user = Yii::$app->user->identity;
$company = $user->company;

$countInvoice = Invoice::find()->where([
    'and',
    ['invoice_status_author_id' => $user->id],
    ['type' => $ioType],
])->count();

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
        'ioType' => $ioType,
    ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
        'ioType' => $ioType,
    ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canPay = Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW);
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['ioType' => $ioType]);
$hasProject = $user->menuItem->project_item;

$dropItems = [];
if ($canCreate) {
    $dropItems[] = [
        'label' => 'Создать Акты',
        'url' => '#many-create-act',
        'linkOptions' => [
            'class' => 'create-acts',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один Акт',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-act',
            'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать ТН',
        'url' => '#many-create-packing-list',
        'linkOptions' => [
            'class' => 'create-packing-list',
            'data-toggle' => 'modal',
        ],
    ];
    if ($company->companyTaxationType->osno) {
        $dropItems[] = [
            'label' => 'Создать СФ',
            'url' => '#many-create-invoice-facture',
            'linkOptions' => [
                'class' => 'create-invoice-factures',
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать одну СФ',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-invoice-facture',
                'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Создать УПД',
        'url' => '#many-create-upd',
        'linkOptions' => [
            'class' => 'create-upd',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-upd',
            'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
}
if (!$canCreate && $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_MANAGER) {
    $dropItems[] = [
        'label' => 'Создать один Акт',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-act',
            'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-upd',
            'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    if ($company->companyTaxationType->osno) {
        $dropItems[] = [
            'label' => 'Создать одну СФ',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-invoice-facture',
                'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
}

if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
    $dropItems[] = [
        'label' => 'Без Акта',
        'url' => '#not-need-document-modal',
        'linkOptions' => [
            'class' => 'act-not-need',
            'data' => [
                'toggle' => 'modal',
                'name' => 'Акт',
                'attribute_text' => 'Без Акта',
                'param' => Invoice::NEED_DOCUMENT_TYPE_ACT,
            ],
        ],
    ];
    $dropItems[] = [
        'label' => 'Без ТН',
        'url' => '#not-need-document-modal',
        'linkOptions' => [
            'class' => 'packing-list-not-need',
            'data' => [
                'toggle' => 'modal',
                'name' => 'ТН',
                'attribute_text' => 'Без ТН',
                'param' => Invoice::NEED_DOCUMENT_TYPE_PACKING_LIST,
            ],
        ],
    ];
    $dropItems[] = [
        'label' => 'Без УПД',
        'url' => '#not-need-document-modal',
        'linkOptions' => [
            'class' => 'upd-not-need',
            'data' => [
                'toggle' => 'modal',
                'name' => 'УПД',
                'attribute_text' => 'Без УПД',
                'param' => Invoice::NEED_DOCUMENT_TYPE_UPD,
            ],
        ],
    ];
}

$sendDropItems = [];
if ($canSend) {
    $sendDropItems[] = [
        'label' => 'Счета',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ],
    ];
    $sendDropItems[] = [
        'label' => 'Счета + Акт/ТН/СФ/УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs send-invoices-with-documents',
            'data-url' => Url::to(['many-send', 'type' => $ioType, 'send_with_documents' => true]),
        ],
    ];
}

$submenuChangeItems = [];
if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) {
    $submenuChangeItems[] = [
        'label' => 'Ответственный',
        'url' => '#mass_change_responsible',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeResponsibleWidget::widget([
        'formAction' => [
            'invoice/mass-set-responsible',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
    ]);
}
if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) {
    $submenuChangeItems[] = [
        'label' => 'Направление',
        'url' => '#mass_change_industry',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeIndustryWidget::widget([
        'formAction' => [
            'invoice/mass-set-industry',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
    ]);
    if ($hasProject) {
        $submenuChangeItems[] = [
            'label' => 'Проект',
            'url' => '#mass_change_project',
            'linkOptions' => [
                'data' => [
                    'toggle' => 'modal',
                ],
            ],
        ];
        echo frontend\widgets\MassChangeProjectWidget::widget([
            'formAction' => [
                'invoice/mass-set-project',
                'type' => $searchModel->type,
            ],
            'toggleButton' => false,
        ]);
    }
    $submenuChangeItems[] = [
        'label' => $searchModel->type == Documents::IO_TYPE_OUT ?
            'Статью приходов' :
            'Статью расходов',
        'url' => '#mass_change_invoice_item_modal',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeItemWidget::widget([
        'formAction' => [
            'invoice/mass-invoice-item',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
        'data' => $searchModel->type == Documents::IO_TYPE_OUT ?
            InvoiceIncomeItem::getSelect2Data($company->id)['list'] :
            InvoiceExpenditureItem::getSelect2Data($company->id)['list'],
        'modalTitle' => $searchModel->type == Documents::IO_TYPE_OUT ?
            'Изменить статью приходов' :
            'Изменить статью расходов',
    ]);
}

if ($submenuChangeItems) {
    $dropItems[] = [
        'label' => 'Изменить',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'dropdown-header dropdown-subitems-toggle',
        ],
        'submenuOptions' => [
            'id' => 'invoice-index-submenu-change',
            'class' => 'pt-0 pb-0'
        ],
        'items' => $submenuChangeItems
    ];
}


$submenuRequisitesItems = [
    [
        'encode' => false,
        'label' => Html::tag('span', 'Моей компании'),
        'url' => '#many-update-company-fields',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ],
    [
        'encode' => false,
        'label' => Html::tag('span', 'Контрагентов'),
        'url' => '#many-update-contractor-fields',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ]
];
if ($submenuRequisitesItems) {
    $dropItems[] = [
        'label' => 'Обновить реквизиты',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'dropdown-header dropdown-subitems-toggle',
        ],
        'submenuOptions' => [
            'id' => 'invoice-index-submenu-requisites',
            'class' => 'pt-0 pb-0'
        ],
        'items' => $submenuRequisitesItems
    ];
}

$dropItems[] = [
    'label' => 'Скачать в Excel',
    'url' => 'javascript:;',
    'linkOptions' => [
        'class' => 'get-xls-link generate-xls-many_actions',
    ],
];

if ($canPrint) {
    if ($company->pdf_signed) {
        $manyPrintButton =
            Html::tag('div',
                Html::button($this->render('//svg-sprite', ['ico' => 'print']) . '<span>Печать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
                    'data-toggle' => 'dropdown',
                ]) . Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span>Печать без подписи</span>',
                            'encode' => false,
                            'url' => ['many-document-print', 'actionType' => 'pdf', 'type' => $ioType, 'addStamp' => 0, 'multiple' => ''],
                            'linkOptions' => ['class' => 'multiple-print', 'target' => '_blank']
                        ],
                        [
                            'label' => '<span>Печать с подписью</span>',
                            'encode' => false,
                            'url' => ['many-document-print', 'actionType' => 'pdf', 'type' => $ioType, 'addStamp' => 1, 'multiple' => ''],
                            'linkOptions' => ['class' => 'multiple-print-with-stamp', 'target' => '_blank']
                        ]
                    ],
                ]), ['class' => 'dropup']);
    } else {
        $manyPrintButton = Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]);
    }
}

$invCount = $company->getInvoiceLeft($ioType);
$dateRange = StatisticPeriod::getSessionPeriod();
$invoiceStatusId = array_filter(is_array($searchModel->invoice_status_id) ?
    $searchModel->invoice_status_id :
    explode(',', $searchModel->invoice_status_id));
$stat1 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel,
    null,
        $searchModel->project_id,
        $searchModel->responsible_employee_id,
        $searchModel->industry_id,
        $searchModel->sale_point_id
);
$stat2 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::NOT_PAID_IN_TIME,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel,
    null,
    $searchModel->project_id,
    $searchModel->responsible_employee_id,
    $searchModel->industry_id,
    $searchModel->sale_point_id
);
$stat3 = InvoiceStatistic::getStatisticInfo(
    $ioType,
    $company->id,
    InvoiceStatistic::PAID,
    $dateRange,
    $searchModel->contractor_id,
    null,
    $invoiceStatusId,
    $searchModel,
    null,
    $searchModel->project_id,
    $searchModel->responsible_employee_id,
    $searchModel->industry_id,
    $searchModel->sale_point_id
);

$url1 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][$ioType]
    ),
]);
$url2 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID_IN_TIME][$ioType]
    ),
]);
$url3 = Url::to([
    '/documents/invoice/index',
    'type' => $ioType,
    'InvoiceSearch[invoice_status_id]' => implode(
        ',',
        InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::PAID][$ioType]
    ),
]);

\frontend\themes\kub\helpers\KubHelper::setStatisticsFontSize(max($stat1['sum'], $stat2['sum'], $stat3['sum']));
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <div class="ml-auto">
            <?php if ($ioType == Documents::IO_TYPE_OUT && Yii::$app->user->can(UserRole::ROLE_SYSADMIN)) : ?>
                <?= Html::button(Icon::get('add-icon').'<span>Добавить системный счет</span>', [
                    'class' => 'button-regular button-regular_red text-nowrap ml-2 ajax-modal-btn',
                    'data-url' => Url::to(['/system/selling/select']),
                    'data-title' => 'Выберите компанию, на которую выставить счет',
                ]) ?>
            <?php endif ?>
            <a class="button-regular button-regular_red button-width ml-2" href="<?= Url::to([
                '/documents/invoice/create',
                'type' => $ioType,
            ]) ?>">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </a>
        </div>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <a href="<?= $url1 ?>" style="text-decoration: none;" class="count-card wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено ВСЕГО</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat1['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3">
                <a href="<?= $url2 ?>" style="text-decoration: none;" class="count-card count-card_red wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Не оплачено в срок</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat2['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3">
                <a href="<?= $url3 ?>" style="text-decoration: none;" class="count-card count-card_green wrap">
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title">Оплачено</div>
                    <hr>
                    <div class="count-card-foot">Количество счетов: <?= $stat3['count'] ?></div>
                </a>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) &&
                    in_array($user->currentEmployeeCompany->employee_role_id, [
                        EmployeeRole::ROLE_CHIEF,
                        EmployeeRole::ROLE_SUPERVISOR
                    ])
                ) : ?>
                    <?php /* todo
                    <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                        'class' => 'button-regular w-100 button-hover-content-red'
                    ]); ?> */ ?>
                <?php endif; ?>
                <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
                    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php elseif ($ioType == Documents::IO_TYPE_OUT) : ?>
                    <?= Html::a('АвтоСчета', ['index-auto'], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php endif ?>
                <?php if ($ioType == Documents::IO_TYPE_OUT) { ?>
                    <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) && in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])) { ?>
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                            'class' => 'button-regular w-100 button-hover-content-red',
                        ]); ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'invoice_scan',
                    ],
                    [
                        'attribute' => 'invoice_paylimit',
                    ],
                    [
                        'attribute' => 'invoice_paydate',
                    ],
                    [
                        'attribute' => $searchModel->type == Documents::IO_TYPE_OUT ? 'invoice_income_item' : 'invoice_expenditure_item',
                    ],
                    [
                        'attribute' => 'invoice_act',
                    ],
                    [
                        'attribute' => 'invoice_paclist',
                    ],
                    [
                        'attribute' => 'invoice_waybill',
                    ],
                    [
                        'attribute' => 'invoice_invfacture',
                    ],
                    [
                        'attribute' => 'invoice_upd',
                    ],
                    [
                        'attribute' => 'invoice_industry',
                    ],
                    [
                        'attribute' => 'invoice_sale_point',
                    ],
                    [
                        'attribute' => 'invoice_project',
                    ],
                    [
                        'attribute' => 'invoice_author',
                        'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
                    ],
                    [
                        'attribute' => 'invoice_comment',
                    ],
                ],
                'showColorSelector' => true
            ]); ?>
            <?= Html::a(
                '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                array_merge(['get-xls'], Yii::$app->request->queryParams),
                [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                    'title' => 'Скачать в Excel',
                ]
            ); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер счета, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php ActiveForm::begin([
        'method' => 'POST',
        'action' => ['/documents/invoice/generate-xls'],
        'id' => 'generate-xls-form',
    ]); ?>
    <?php ActiveForm::end(); ?>

    <?= $this->render('_invoices_table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'type' => $ioType,
        'company' => $company,
    ]); ?>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? $manyPrintButton : null,
        $canSend ? (Html::a(Icon::get('envelope').' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ])  . ($sendDropItems ? Html::tag('div', Html::a(Icon::get('envelope').' <span>Отправить</span>', '#', [
            'id' => 'dropdownMenu3',
            'class' => 'button-regular button-hover-transparent button-width',
            'data-toggle' => 'dropdown',
        ]) . Dropdown::widget([
            'items' => $sendDropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), [
            'class' => 'dropup document-many-send-dropdown hidden',
        ]) : null)) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<span>Платежка</span>', '#many-charge', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a($this->render('//svg-sprite', ['ico' => 'check-2']) . ' <span>Оплачены</span>', '#many-paid-modal', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . ' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>' . Icon::get('shevron'), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
                'data-toggle' => 'dropdown',
                'style' => 'width: 90px; padding: 12px 15px;',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'class' => 'dropdown-menu-right form-filter-list list-clr',
                ],
            ]), [
            'class' => 'dropup',
            'style' => 'position: relative;',
        ]) : null,
    ],
]); ?>

<?php if ($ioType == Documents::IO_TYPE_IN): ?>
    <div id="many-charge" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12 text-left many-charge-text main-block" style="font-size: 15px;">
                                <h4 class="modal-title">Вы уверены, что хотите подготовить платежки в банк:</h4>
                                <span class="template mb-2" style="display: none;">
                                    <span class="number"></span>. <span class="contractor-title"></span>:
                                    счета <span class="invoice-count"></span> шт., сумма <span class="summary"></span> ₽
                                </span>
                                <span class="total-row mb-2" style="display: block;font-weight: bold;">
                                    Итого на оплату: Счета <span class="total-invoice-count"></span> шт., Сумма
                                    <span class="total-summary"></span> ₽
                                </span>
                            </div>
                            <div class="col-12 text-left many-charge-text payment-invoices-block mb-2" style="font-size: 15px;display: none;">
                                <span style="display: block;">
                                    Эти счета уже оплачены:
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions row mt-3">
                        <div class="payment-invoices-block" style="display: none;">
                            <div class="col-12 text-center">
                                <button type="button" class="button-clr button-regular button-hover-transparent button-width-medium" data-dismiss="modal">
                                    ОК
                                </button>
                            </div>
                        </div>
                        <div class="col-12 main-block text-center mt-3">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'modal-many-charge button-clr button-regular button-hover-transparent button-width-medium mr-2',
                                'data-url' => Url::to(['/documents/payment-order/many-create',]),
                                'data-style' => 'expand-right',
                            ]); ?>
                            <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($canPay) : ?>
    <?php Modal::begin([
        'id' => 'many-paid-modal',
        'closeButton' => false,
        'options' => [
            'data-url' => Url::to(['add-flow-form', 'type' => $ioType]),
        ],
    ]); ?>
    <div id="many-paid-content"></div>
    <?php Modal::end(); ?>

    <?php $this->registerJs('
    $(document).on("shown.bs.modal", "#many-paid-modal", function() {
        var modal = this;
        var idArray = $(".joint-operation-checkbox:checked").map(function () {
            return $(this).closest("tr").data("key");
        }).get();
        var url = this.dataset.url + "&id=" + idArray.join();
        $.post(url, function(data) {
            $("#many-paid-content", modal).html(data);
            $(".date-picker", modal).datepicker(kubDatepickerConfig);
            $("input:checkbox", modal).uniform();
            $("input:radio", modal).uniform();
        })
    });
    $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
        $("#many-paid-content", this).html("");
    });
') ?>
<?php endif ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные счета?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>

<?php if ($canCreate) : ?>
    <?php Modal::begin([
        'id' => 'many-create-act',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
        <div class="row form-group">
            <div class="col-6">
                <div class="form-group">
                    <label class="label">Акты создать от даты:</label>
                    <div>
                        <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker ico modal-document-date',
                            'data-date-viewmode' => 'years',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act ladda-button',
                'data-url' => Url::to(['/documents/act/many-create', 'type' => $ioType]),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-packing-list',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Товарные Накладные создать от даты:</label>
                <div>
                    <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Создать', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act ladda-button',
                'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => $ioType]),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-invoice-facture',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Счета-Фактуры создать от даты:</label>
                <div>
                    <?= Html::textInput('InvoiceFacture[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act ladda-button',
                'data-url' => Url::to(['/documents/invoice-facture/many-create', 'type' => $ioType]),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
    <?php Modal::begin([
        'id' => 'many-create-upd',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">УПД создать от даты:</label>
                <div>
                    <?= Html::textInput('Upd[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act ladda-button',
                'data-url' => Url::to(['/documents/upd/many-create', 'type' => $ioType]),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Modal::end(); ?>
<?php endif ?>

<?php if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF): ?>
    <?php Modal::begin([
        'id' => 'not-need-document-modal',
        'closeButton' => [
            'label' => $this->render('//svg-sprite', ['ico' => 'close']),
            'class' => 'modal-close close',
        ],
    ]); ?>
        <div class="form-group">
            <div class="bold">
                Для выбранных счетов в столбце <span class="document-name"></span> проставить
                «<span class="document-attribute_text"></span>».
            </div>
            Это можно будет отменить, кликнув на «<span class="document-attribute_text"></span>»
        </div>
        <div class="form-group">
            <div class="bold uppercase">Внимание!</div>
            Это делается для того, чтобы в разделе финансы у вас правильно строился Управленческий
            Баланс. Это позволит убрать суммы из строки “Предоплата покупателей (авансы)”<br>
            Ситуации, когда <b>НУЖНО</b> проставить, что «<span class="document-attribute_text"></span>»:
            <ul style="list-style: decimal;padding-inline-start: 15px;">
                <li>
                    <b><i>Счет покупатель оплатил наличными</i></b>. Вы эти деньги проводить по
                    бухгалтерии НЕ будете. Соответственно покупателю выставлять
                    <span class="document-name"></span> не будете.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил через Банк</i></b>. Вы проводить эти деньги по
                    бухгалтерии обязаны. Но <span class="document-name"></span> выставляете не в КУБ,
                    а в КУБ хотите получить правильный Управленческий Баланс.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил наличными, и вы пробили кассовый чек</i></b>. Вы
                    проводить эти деньги по бухгалтерии обязаны. Но <span class="document-name"></span>
                    выставляете не в КУБ, а в КУБ хотите получить правильный Управленческий
                    Баланс.
                </li>
            </ul>
        </div>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Применить', [
                'class' => 'button-regular button-width button-regular_red button-clr modal-not-need-document-submit ladda-button',
                'data-url' => Url::to(['not-need-document', 'type' => $ioType, 'param' => '_param_']),
                'style' => 'width: 130px!important;',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    </div>
    <?php Modal::end(); ?>
<?php endif; ?>

<?php if ($canSend) : ?>
    <?php Modal::begin([
        'id' => 'many-send-error',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Ошибка при отправке счетов
    </h4>
    <div class="modal-body">
        <div class="form-body">
        </div>
    </div>
    <div class="text-center">
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Ок
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif; ?>

<?php if ($canSend): ?>
    <?= $this->render('view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE
    ]); ?>
<?php endif; ?>

<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#many-charge", function () {
        var countChecked = 0;
        var totalSummary = 0;
        var data = {};
        var payedInvoices = {};
        var mainBlock = $(".main-block");
        var paymentInvoiceBlock = $(".payment-invoices-block");

        $(this).find(".generated-row").remove();
        mainBlock.show();
        paymentInvoiceBlock.hide();

        $(".joint-operation-checkbox:checked").each(function(){
            var price = $(this).closest("tr").find(".price").data("remaining-amount");
            var contractor = $(this).closest("tr").find(".contractor-cell a");
            var contractorID = contractor.data("id");
            var contractorTitle = contractor.text();
            var invoiceNumber = $(this).closest("tr").find(".document_number span").data("full-name");

            price = parseFloat(price.replace(",", "."));
            if (price > 0) {
                if (data[contractorID] == undefined) {
                    data[contractorID] = {
                        "id": contractorID,
                        "title": contractorTitle,
                        "invoiceCount": 1,
                        "summary": price,
                    };
                } else {
                    data[contractorID]["summary"] = data[contractorID]["summary"] + price;
                    data[contractorID]["invoiceCount"] = data[contractorID]["invoiceCount"] + 1;
                }
                totalSummary += price;
                countChecked++;
            } else {
                payedInvoices[invoiceNumber] = invoiceNumber;
            }
        });

        var number = 1;
        for (var key in data) {
            var $template = $("#many-charge .many-charge-text .template").clone();
            $template.find(".number").text(number);
            $template.find(".contractor-title").text(data[key]["title"]);
            $template.find(".invoice-count").text(data[key]["invoiceCount"]);
            $template.find(".summary").text(number_format(data[key]["summary"], 2, ",", " "));
            $template.css("display", "block");
            $template.removeClass("template");
            $template.addClass("generated-row");
            $("#many-charge .many-charge-text.main-block").append($template);
            number++;
        };
        var $totalRow = $("#many-charge .many-charge-text.main-block .total-row").clone();
        $("#many-charge .many-charge-text.main-block .total-row").remove();
        $totalRow.find(".total-invoice-count").text(countChecked);
        $totalRow.find(".total-summary").text(number_format(totalSummary, 2, ",", " "));
        $("#many-charge .many-charge-text.main-block").append($totalRow);

        if (countChecked == 0) {
            mainBlock.hide();
            if (Object.keys(payedInvoices).length > 0) {
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.payment-invoices-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
            paymentInvoiceBlock.show();
        } else {
            if (Object.keys(payedInvoices).length > 0) {
               $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;margin-top:15px;>Эти счета уже оплачены:</span>");
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
        }
    });

    $(".act-not-need, .packing-list-not-need, .upd-not-need").click(function (e) {
        var url = $("#not-need-document-modal .modal-not-need-document-submit").data("url").replace("_param_", $(this).data("param"));
        console.log(url);
        $("#not-need-document-modal span.document-name").text($(this).data("name"));
        $("#not-need-document-modal span.document-attribute_text").text($(this).data("attribute_text"));
        $("#not-need-document-modal .modal-not-need-document-submit").data("url", url);
    });

    $(".change-table-view").on("click", function() {
        var $table = $(".table.table-count-list").first();
        $table.toggleClass("table-compact");
    });
'); ?>
