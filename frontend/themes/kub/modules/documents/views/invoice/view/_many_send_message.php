<?php
use common\components\helpers\ArrayHelper;
use frontend\models\Documents;

/** @var $typeDocument integer */
/** @var $useContractor boolean */
/** @var $showSendPopup boolean */
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'document-many-send-message',
    'timeout' => 5000,
    'linkSelector' => false,
]); ?>
<?= $this->render('_many_send_message_form', [
    'models' => [],
    'useContractor' => $useContractor,
    'showSendPopup' => $showSendPopup,
    'typeDocument' => $typeDocument
]); ?>
<?php \yii\widgets\Pjax::end(); ?>

<?php $documentSlug = ArrayHelper::getValue(Documents::$typeToSlug, $typeDocument); ?>
<?php $useContractor = (int)$useContractor; ?>

<?php $this->registerJs(<<<JS

    $(document).on('click', '.send-panel-modal', function() {

        var idArray = $('.joint-operation-checkbox:checked').map(function() {
          return $(this).closest('tr').data('key');
        }).get();
        $.pjax({
            type: 'POST',
            push: false,
            url: '/documents/{$documentSlug}/get-many-send-message-panel',
            container: "#document-many-send-message",
            data: {
                ioType: 2, // send only out documents
                typeDocument: {$typeDocument},
                ids: idArray,
                sendWithDocs: ($(this).data('url').indexOf('send_with_documents') !== -1) ? 1:0,
                useContractor: {$useContractor}
            },
        });

        $(document).on('pjax:success', '#document-many-send-message', function() {
            $('[data-id="invoice"]').toggleClass('visible show');
            $('#document-many-send-message [data-toggle="toggleVisible"]').click(function(e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('[data-id="'+target+'"]').toggleClass('visible show');
            });
        });
    });

    $(document).on('change', '.joint-operation-checkbox', function () {
        var contractors = [];
        $('.joint-operation-checkbox:checked').each(function (i) {
            let contractorId = $(this).data('contractor');
            if (contractors.indexOf(contractorId) === -1) {
                contractors.push(contractorId);
            }
        });
        if (contractors.length === 1) {
            $('.document-many-send').addClass('no-ajax-loading').addClass('send-panel-modal');
            $('.document-many-send-with-docs').addClass('no-ajax-loading').addClass('send-panel-modal');
        } else {
            $('.document-many-send').removeClass('no-ajax-loading').removeClass('send-panel-modal');
            $('.document-many-send-with-docs').removeClass('no-ajax-loading').removeClass('send-panel-modal');
        }
    });

JS
) ?>