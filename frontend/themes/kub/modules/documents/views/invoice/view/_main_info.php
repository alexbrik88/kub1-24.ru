<?php

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\bank\BankingParams;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\project\Project;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\components\TextHelper;
use common\models\companyStructure\SalePoint;
use common\models\company\CompanyIndustry;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string */
/* @var $newCompanyTemplate bool */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */
/* @var $company \common\models\Company */
/* @var $user \common\models\employee\Employee */

$cashFlowData = [];
foreach (array_merge(
        $model->getCashBankFlows()->andWhere(['parent_id' => NULL])->all(),
        $model->cashOrderFlows,
        $model->cashEmoneyFlows) as $i => $flow) {

    $cashFlowData[$i] = [
        'id' => $flow->id,
        'date' => $flow->date,
        'amount' => $flow->amount,
        'number' => $flow->number ?? null,
        'self_amount' => $flow->getFlowInvoices()->andWhere(['invoice_id' => $model->id])->sum('amount'),
        'sort' => DateHelper::format($flow->date, 'Ymd', 'd.m.Y')
    ];

    switch (get_class($flow)) {
        case CashBankFlows::class:
            $cashFlowData[$i]['url'] = '/cash/bank/index';
            $cashFlowData[$i]['text'] = ' по Банку';
            break;
        case CashOrderFlows::class:
            $cashFlowData[$i]['url'] = '/cash/order/index';
            $cashFlowData[$i]['text'] = ' по Кассе';
            break;
        case CashEmoneyFlows::class:
            $cashFlowData[$i]['url'] = '/cash/e-money/index';
            $cashFlowData[$i]['text'] = ' по E-money';
            break;
        default:
            $cashFlowData[$i]['url'] = '#';
            $cashFlowData[$i]['text'] = ' ---';
            break;
    };
}

usort($cashFlowData, function ($a, $b) {
    return $a['sort'] <=> $b['sort'];
});

$user = Yii::$app->user->identity;
$company = $user->company;
$alias = null;
$link = null;
$image = null;
if ($alias = Banking::aliasByBik($model->company_bik)) {
    $hasData = BankingParams::find()->where([
        'company_id' => $model->company_id,
        'bank_alias' => $alias,
    ])->exists();
    if (!$hasData) {
        $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
        $bankName = $bankingClass::NAME;
        $banking = new $bankingClass($company, [
            'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
        ]);
        if ($banking->getHasAutoload() && !$banking->isValidToken()) {
            $link = Html::a("Автоматическая загрузка выписки из банка с авто проставлением оплат у счетов", Url::to([
                "/cash/banking/{$alias}/default/index",
            ]), [
                'style' => 'font-size: 13px;',
            ]);

            if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
                $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                    'class' => 'little_logo_bank_2',
                    'style' => 'display: inline-block; ',
                ]);
            }
        }
    }
}
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->contractor->getInvoicesAuto()->exists();

//// SalePoint
//$hasSalePoint = (bool)$model->sale_point_id;
//if ($hasSalePoint) {
//    $salePoint = SalePoint::find()->andWhere(['and',
//        ['id' => $model->sale_point_id],
//        ['company_id' => $model->company_id],
//    ])->one();
//}
//
//// Industry
//$hasCompanyIndustry = $model->industry_id;
//if ($hasCompanyIndustry) {
//    $companyIndustry = CompanyIndustry::find()->andWhere(['and',
//        ['id' => $model->industry_id],
//        ['company_id' => $model->company_id],
//    ])->one();
//}
//
//if ($hasProject = Yii::$app->user->identity->menuItem->project_item) {
//    $project = Project::find()->andWhere(['and',
//        ['id' => $model->project_id],
//        ['company_id' => $model->company_id],
//    ])->one();
//}

?>

<style type="text/css">
    button.link {
        border: none;
        background-color: transparent;
        font: inherit;
    }
</style>

<div class="about-card" style="margin-bottom: 12px">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($model->contractor_name_short, [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <?php if ($model->industry) : ?>
        <div class="about-card-item">
            <span class="text-grey">Направление:</span>
            <span>
                <?= $model->industry->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($model->salePoint) : ?>
        <div class="about-card-item">
            <span class="text-grey">Точка продаж:</span>
            <span>
                <?= $model->salePoint->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($model->project) : ?>
        <div class="about-card-item">
            <span class="text-grey">Проект:</span>
            <span>
                <?= Html::a($model->project->name, ['/project/view', 'id' => $model->project->id], ['class' => 'link']); ?>
            </span>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">Оплатить до:</span>
        <span>
            <?php $paymentLimitDate = InvoiceHelper::getFloatPaymentLimitDate($model);
            echo ($paymentLimitDate) ? DateHelper::format($paymentLimitDate, 'd.m.Y', 'Y-m-d') : ''; ?>
        </span>
    </div>
    <?php if (YII_ENV_DEV && $ioType == Documents::IO_TYPE_OUT) : ?>
    <div class="about-card-item">
        <span class="text-grey">Статья приходов:</span>
        <span>
                <?= $model->invoiceIncomeItem !== null ? $model->invoiceIncomeItem->fullName : 'не указано'; ?>
            </span>
    </div>
    <?php endif; ?>
    <?php if ($ioType == Documents::IO_TYPE_IN) : ?>
        <div class="about-card-item">
            <span class="text-grey">Статья расходов:</span>
            <span>
                <?= $model->invoiceExpenditureItem !== null ? $model->invoiceExpenditureItem->fullName : 'не указано'; ?>
            </span>
        </div>
    <?php else : ?>
        <?php if (isset($model->agreement_new_id) && $model->agreement_new_id > 0) : ?>
            <div class="about-card-item">
                <span class="text-grey">Договор №</span>
                <?php /* todo: agreements for KUB
                <?= Html::a(
                    Html::encode($model->basis_document_number) .
                    ' от ' .
                    DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    [
                        '/documents/agreement/view',
                        'id' => $model->agreement_new_id,
                    ],
                    [
                        'class' => 'link',
                    ]
                ); ?>*/ ?>
                <?= Html::encode($model->basis_document_number) . ' от ' .
                DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
            </div>
        <?php elseif ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
            <div class="about-card-item">
                <span class="text-grey">
                    <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>:
                </span>
                <span>
                    № <?= Html::encode($model->basis_document_number) ?>
                    от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </span>
            </div>
        <?php endif ?>
    <?php endif; ?>
    <?php if ($cashFlowData) : ?>
        <div class="about-card-item">
            <span class="text-grey">Оплата:</span>
            <span>
                <?php
                if (count($cashFlowData) > 1) echo '<br/>';
                foreach ($cashFlowData as $flow) {
                    $text = '№ ' . $flow['number'] .' от '. $flow['date'] . $flow['text'];
                    $amountText = ($flow['self_amount'] != $flow['amount'])
                        ? (TextHelper::invoiceMoneyFormat($flow['self_amount']) .' из '. TextHelper::invoiceMoneyFormat($flow['amount']))
                        : TextHelper::invoiceMoneyFormat($flow['amount']);
                    if (count($cashFlowData) > 1)
                        $text .= ': ' . $amountText;
                    echo Html::beginForm($flow['url'], 'post', ['style' => 'display: inline-block;']) .
                         Html::hiddenInput('flow_id[]', $flow['id']) .
                         Html::submitButton($text, ['class' => 'link pl-0']) .
                         Html::endForm() .
                         '<br/>';
                } ?>
            </span>
        </div>
    <?php endif ?>
    <?php if ($model->responsible) : ?>
        <div class="about-card-item">
            <span class="text-grey">Ответственный:</span>
            <?php if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) : ?>
                <?= frontend\widgets\ChangeResponsibleWidget::widget([
                    'formAction' => [
                        'set-responsible',
                        'type' => $model->type,
                        'id' => $model->id,
                    ],
                    'toggleButton' => [
                        'label' => Html::encode($model->responsible->getFio(true)),
                        'class' => 'link',
                    ],
                    'inputValue' => $model->responsible_employee_id,
                ]) ?>
            <?php else : ?>
                <span>
                    <?= Html::encode($model->responsible->getFio(true)) ?>
                </span>
            <?php endif ?>
        </div>
    <?php endif ?>
    <?php if ($model->currency_name != Currency::DEFAULT_NAME) : ?>
        <div id="invoice_currency_rate_box" class="about-card-item">
            <?= $this->render('_currency_info', ['model' => $model]) ?>
        </div>
    <?php endif ?>
    <div class="about-card-item">
        <?= \frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>
<div class="about-card" style="margin-bottom: 12px">
    <div class="about-card-item">
        <span style="font-weight: bold;">Комментарий</span>
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
            'id' => 'comment_internal_update',
            'style' => 'cursor: pointer;',
        ]); ?>
        <div id="comment_internal_view" style="margin-top:5px" class="">
            <?= Html::encode($model->comment_internal) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
                'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_internal_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
            ]); ?>
            <?= Html::textarea('comment_internal', $model->comment_internal, [
                'id' => 'comment_internal_input',
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
            ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>

<?php if ($model->type == Documents::IO_TYPE_OUT && !$hasAutoinvoice) : ?>
    <div style="margin-bottom: 12px">
        <div class="d-flex flex-nowrap align-items-center">
            <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'clock']) . Html::tag('span', 'АвтоСчет'), [
                    'create',
                    'type' => Documents::IO_TYPE_OUT,
                    'clone' => $model->id,
                    'auto' => 1,
                    'returnUrl' => Url::to([
                        '/contractor/view',
                        'type' => $model->contractor->type,
                        'id' => $model->contractor->id,
                        'tab' => 'autoinvoice',
                    ]),
                ], [
                    'class' => 'button-regular button-hover-content-red text-left flex-grow-1 mb-0',
                ]) ?>
            <?php else : ?>
                <button class="button-regular button-hover-content-red text-left flex-grow-1 mb-0 action-is-limited">
                    <?= $this->render('//svg-sprite', ['ico' => 'clock']) ?>
                    <span>
                        АвтоСчет
                    </span>
                </button>
            <?php endif ?>
            <div class="tooltip-box ml-3">
                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip-hover',
                    'data-tooltip-content' => '#tooltip-autoinvoice-create',
                ]) ?>
            </div>
        </div>
        <div class="hidden">
            <div id="tooltip-autoinvoice-create" class="box-tooltip-templates">
                Создайте АвтоСчёт – счета будут сами <br>создаваться и отправляться клиенту<br>в указанный вами день.
            </div>
        </div>
    </div>
<?php endif; ?>

<?php /*if (
    $model->contractor_id
    && $model->contractor->face_type != Contractor::TYPE_PHYSICAL_PERSON
    && $model->contractor->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON
) { ?>
    <div class="">
        <?php
        if ($model->contractor->verified) { ?>
            <a class="button-clr button-regular button-hover-transparent widthe-100" href="<?= Url::to(['/dossier', 'id' => $model->contractor_id]) ?>" target="_blank">
                <?= $model->contractor->verified ? 'Досье' : 'Проверить' ?> <?= ($model->type == \common\models\Contractor::TYPE_CUSTOMER) ? 'покупателя' : 'поставщика' ?>
            </a>
        <?php } else {
            echo $this->render('../../../../../views/dossier/_popup-confirm', [
                'link' => Url::to([
                    '/dossier',
                    'type' => $model->type,
                    'id' => $model->contractor_id
                ]),
                'button' => [
                    'label' => 'Проверить покупателя',
                    'class' => 'btn button-regular darkblue-invert contractor-invoice-dossier',
                ],
                'newTab' => true
            ]);
        } ?>
    </div>
<?php }*/ ?>

<?php if ($alias && $link): ?>
    <div class="main-bank_logo" style="margin-bottom: 12px; margin-top:3px;">
        <?= $image; ?>
        <span class="bank-name" style="margin-left: 5px;">
            <?= $bankName; ?>
        </span>
        <div style="margin-top:8px; line-height: 1">
            <?= $link; ?>
        </div>
    </div>
<?php endif; ?>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
            $(this).toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
                $("#comment_internal_update").toggleClass("hidden");
            })
        });
        $(document).on("change", "#activate_contractor_signature", function () {
            $.post($(this).data("url"), {status: +$(this).is(":checked")}, function (data) {});
        });
    ');
}
?>
