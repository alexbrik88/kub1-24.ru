<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\BoolleanSwitchWidget;
use common\models\employee\EmployeeRole;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $invoiceFlowCalc common\models\document\InvoiceFlowCalculator */
/* @var $useContractor string */
/* @var $user \common\models\employee\Employee */

$status = $model->invoiceStatus;
$styleClass = $model->isOverdue() ? 'red' : $status->getStyleClass();
$user = Yii::$app->user->identity;

$createdInvoicesCount = $model->company->getInvoices()->where(['from_demo_out_invoice' => 0])->count();

$statusIcon = [
    InvoiceStatus::STATUS_CREATED => 'new-doc',
    InvoiceStatus::STATUS_SEND => 'envelope',
    InvoiceStatus::STATUS_PAYED => 'check-2',
    InvoiceStatus::STATUS_VIEWED => 'check-2',
    InvoiceStatus::STATUS_APPROVED => 'check-double',
    InvoiceStatus::STATUS_PAYED_PARTIAL => 'check-2',
    InvoiceStatus::STATUS_OVERDUE => 'calendar',
    InvoiceStatus::STATUS_REJECTED => 'stop',
];
$statusBackground = [
    InvoiceStatus::STATUS_CREATED => '#4679AE',
    InvoiceStatus::STATUS_SEND => '#FAC031',
    InvoiceStatus::STATUS_PAYED => '#26cd58',
    InvoiceStatus::STATUS_VIEWED => '#FAC031',
    InvoiceStatus::STATUS_APPROVED => '#FAC031',
    InvoiceStatus::STATUS_PAYED_PARTIAL => '#26cd58',
    InvoiceStatus::STATUS_OVERDUE => '#e30611',
    InvoiceStatus::STATUS_REJECTED => '#f2f3f7',
];
$icon = ArrayHelper::getValue($statusIcon, $status->id);
$background = ArrayHelper::getValue($statusBackground, $status->id);
$color = $status->id == InvoiceStatus::STATUS_REJECTED ? '#001424' : '#ffffff';
$border = $status->id == InvoiceStatus::STATUS_REJECTED ? '#e2e5eb' : $background;
?>

<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <strong class="column mb-2 mb-xl-3 doc-status-price" style="padding: 0 5px;">
        <?= TextHelper::invoiceMoneyFormat($model->getPaidAmount(), 2); ?> ₽
    </strong>
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
            background-color: <?=$background?>;
            border-color: <?=$border?>;
            color: <?=$color?>;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span><?= $status->name; ?></span>
        </div>
    </div>
</div>

<?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
    <?php $_copyLinkLabel = '<table>'.'<tr><td class="pr-2">'.$this->render('//svg-sprite', ['ico' => 'copy-link']) .'</td><td>'. Html::tag('span', 'Ссылка на счет для клиента') . '</td></tr></table>' ?>
    <?= Html::tag('div', $_copyLinkLabel, [
        'onclick' => 'copyTextToClipboard(document.getElementById("invoice-pdf-link").value)',
        'class' => 'button-regular button-hover-content-red text-left flex-grow-1 w-100',
        'title' => 'Нажмите на кнопку и скопируется<br/>ссылка на счет, которую можно<br/> отправить клиенту в мессенджере',
        'title-as-html' => 1
    ]) ?>
    <input type="hidden" id="invoice-pdf-link" value="<?= Url::to(['/documents/invoice/download', 'type' => 'pdf', 'uid' => $model->uid], true) ?>">
<?php endif; ?>

<?php if (!$model->has_upd): ?>
    <!-- ACT -->
    <?php if ($model->has_services): ?>
        <?php if (!$model->need_act): ?>
            <div class="clearfix">
                <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                    BoolleanSwitchWidget::widget([
                        'id' => 'add-act',
                        'action' => [
                            '/documents/act/create',
                            'type' => $model->type,
                            'invoiceId' => $model->id,
                        ],
                        'inputName' => 'add_act',
                        'inputValue' => 1,
                        'label' => 'Выставить Акт?',
                        'addFalseForm' => false,
                        'closeByFalseLabel' => true,
                        'options' => [
                            'class' => 'dropdown-popup-in text-center',
                        ],
                        'labelOptions' => [
                            'class' => 'dropdown-price-title pt-3 pb-3',
                        ],
                        'buttonBoxOptions' => [
                            'class' => 'd-flex flex-nowrap justify-content-center',
                        ],
                        'buttonOptions' => [
                            'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                        ],
                        'toggleButton' => [
                            'tag' => 'span',
                            'label' => 'БЕЗ АКТа',
                            'class' => 'button-regular button-hover-content-red w-100 text-left disabled',
                        ],
                    ]) : '<span class="button-regular button-hover-content-red w-100 text-left disabled">БЕЗ АКТа</span>'; ?>
            </div>
        <?php else: ?>
            <?php if (!$model->acts): ?>
                <?= $this->render('status_rows/_first_row', [
                    'document' => 'act',
                    'title' => 'Акт',
                    'model' => $model,
                    'action' => 'create',
                    'id' => 0,
                ]);
                ?>
            <?php elseif (count($model->acts) == 1 && !$model->canAddAct): ?>
                <?= $this->render('status_rows/_first_row', [
                    'document' => 'act',
                    'title' => 'Акт №' . $model->acts[0]->fullNumber . ' от ' . DateHelper::format($model->acts[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    'model' => $model,
                    'action' => 'view',
                    'id' => $model->acts[0]->id,
                ]);
                ?>
            <?php else: ?>
                <?= $this->render('status_rows/_many_rows', [
                    'document' => 'act',
                    'title' => 'Акт',
                    'model' => $model,
                    'addAvailable' => $model->canAddAct,
                ]); ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
    <!-- PACKING-LIST -->
    <?php if ($model->has_goods && !$model->not_for_bookkeeping): ?>
        <?php if (!$model->need_packing_list): ?>
            <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                BoolleanSwitchWidget::widget([
                    'id' => 'add-packing-list',
                    'action' => [
                        '/documents/packing-list/create',
                        'type' => $model->type,
                        'invoiceId' => $model->id,
                    ],
                    'inputName' => 'add_packing_list',
                    'inputValue' => 1,
                    'label' => 'Выставить ТН?',
                    'addFalseForm' => false,
                    'options' => [
                        'class' => 'dropdown-popup-in text-center',
                    ],
                    'labelOptions' => [
                        'class' => 'dropdown-price-title pt-3 pb-3',
                    ],
                    'buttonBoxOptions' => [
                        'class' => 'd-flex flex-nowrap justify-content-center',
                    ],
                    'buttonOptions' => [
                        'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                    ],
                    'toggleButton' => [
                        'tag' => 'span',
                        'label' => 'Без ТН',
                        'class' => 'button-regular button-hover-content-red w-100 text-left disabled',
                    ],
                ]) : '<span class="btn yellow full_w disabled" style="height: 34px;margin-left: 0;background-color: #a2a2a2;">БЕЗ ТН</span>'; ?>
        <?php else: ?>
            <?php if (!$model->packingLists): ?>
                <?= $this->render('status_rows/_first_row', [
                    'document' => 'packing-list',
                    'title' => 'Товарная накладная',
                    'model' => $model,
                    'action' => 'create',
                    'id' => 0,
                ]);
                ?>
            <?php elseif (count($model->packingLists) == 1 && !$model->canAddPackingList): ?>
                <?= $this->render('status_rows/_first_row', [
                    'document' => 'packing-list',
                    'title' => 'ТН №' . $model->packingLists[0]->fullNumber . ' от ' . DateHelper::format($model->packingLists[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    'model' => $model,
                    'action' => 'view',
                    'id' => $model->packingLists[0]->id,
                ]);
                ?>
            <?php else: ?>
                <?php $sum = array_sum(ArrayHelper::getColumn($model->packingLists, 'totalAmountWithNds')) ?>
                <?= $this->render('status_rows/_many_rows', [
                    'document' => 'packing-list',
                    'title' => $sum >= 10000000 ? 'ТН' : 'Тов. накладная',
                    'model' => $model,
                    'addAvailable' => $model->canAddPackingList,
                ]);
                ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
    <!-- SALE INVOICE -->
    <?php if ($model->has_goods && $model->not_for_bookkeeping && $ioType == Documents::IO_TYPE_OUT): ?>
        <?php if (!$model->salesInvoices): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'sales-invoice',
                'title' => 'Расходная накладная',
                'model' => $model,
                'action' => 'create',
                'id' => 0,
            ]);
            ?>
        <?php elseif (count($model->salesInvoices) == 1 && !$model->canAddSalesInvoice): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'sales-invoice',
                'title' => 'РН' . $model->salesInvoices[0]->fullNumber . ' от ' . DateHelper::format($model->salesInvoices[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                'model' => $model,
                'action' => 'view',
                'id' => $model->salesInvoices[0]->id,
            ]);
            ?>
        <?php else: ?>
            <?= $this->render('status_rows/_many_rows', [
                'document' => 'sales-invoice',
                'title' => 'Расходная накладная',
                'model' => $model,
                'addAvailable' => $model->canAddSalesInvoice,
            ]);
            ?>
        <?php endif; ?>
    <?php endif; ?>
    <!-- INVOICE FACTURE -->
    <?php if ($model->hasNds && !$model->not_for_bookkeeping): ?>
        <?php if (!$model->invoiceFactures): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'invoice-facture',
                'title' => 'Счет-фактура',
                'model' => $model,
                'action' => 'create',
                'id' => 0,
            ]);
            ?>
        <?php elseif (count($model->invoiceFactures) == 1 && !$model->canAddInvoiceFacture): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'invoice-facture',
                'title' => 'Счет-фактура №' . $model->invoiceFactures[0]->fullNumber . ' от ' . DateHelper::format($model->invoiceFactures[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                'model' => $model,
                'action' => 'view',
                'id' => $model->invoiceFactures[0]->id,
            ]);
            ?>
        <?php else: ?>
            <?= $this->render('status_rows/_many_rows', [
                'document' => 'invoice-facture',
                'title' => 'Счет-фактура',
                'model' => $model,
                'addAvailable' => $model->canAddInvoiceFacture,
            ]);
            ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
<!-- UPD -->
<?php if (!$model->hasDocs && !$model->not_for_bookkeeping) : ?>
    <?php if (!$model->need_upd): ?>
        <div class="clearfix">
            <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                BoolleanSwitchWidget::widget([
                    'id' => 'add-upd',
                    'action' => [
                        '/documents/upd/create',
                        'type' => $model->type,
                        'invoiceId' => $model->id,
                    ],
                    'inputName' => 'add_upd',
                    'inputValue' => 1,
                    'label' => 'Выставить УПД?',
                    'addFalseForm' => false,
                    'options' => [
                        'class' => 'dropdown-popup-in text-center',
                    ],
                    'labelOptions' => [
                        'class' => 'dropdown-price-title pt-3 pb-3',
                    ],
                    'buttonBoxOptions' => [
                        'class' => 'd-flex flex-nowrap justify-content-center',
                    ],
                    'buttonOptions' => [
                        'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                    ],
                    'toggleButton' => [
                        'tag' => 'span',
                        'label' => 'Без УПД',
                        'class' => 'button-regular button-hover-content-red w-100 text-left disabled',
                    ],
                ]) : '<span class="btn yellow full_w disabled" style="height: 34px;margin-left: 0;background-color: #a2a2a2;">БЕЗ УПД</span>'; ?>
        </div>
    <?php else: ?>
        <?php if (!$model->upds): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'upd',
                'title' => 'УПД',
                'model' => $model,
                'action' => 'create',
                'id' => 0,
            ]);
            ?>
        <?php elseif (count($model->upds) == 1 && !$model->canAddUpd): ?>
            <?= $this->render('status_rows/_first_row', [
                'document' => 'upd',
                'title' => 'УПД №' . $model->upds[0]->fullNumber . ' от ' . DateHelper::format($model->upds[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                'model' => $model,
                'action' => 'view',
                'id' => $model->upds[0]->id,
            ]);
            ?>
        <?php else: ?>
            <?= $this->render('status_rows/_many_rows', [
                'document' => 'upd',
                'title' => 'УПД',
                'model' => $model,
                'addAvailable' => $model->canAddUpd,
            ]);
            ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif ?>
<!-- CANCELLATION -->
<?php if ($ioType == Documents::IO_TYPE_OUT && ($cancellations = $model->goodsCancellations)): ?>
    <?php $title = 'Списание материалов'; ?>
    <?php if (count($cancellations) == 1): ?>
        <?= Html::a(Icon::get('new-doc') . Html::tag('span', $title), [
            '/documents/goods-cancellation/view',
            'id' => $cancellations[0]->id,
        ], [
            'class' => 'button-regular button-hover-content-red w-100 text-left',
            'title' => $title,
        ]) ?>
    <?php else: ?>
        <div class="dropdown w-100 pr-2">
            <?= Html::a(Icon::get('new-doc') . Html::tag('span', $title), 'javascript:;', [
                'class' => 'button-regular button-hover-content-red w-100 text-left dropdown-toggle',
                'title' => $title,
                'data-toggle' => 'dropdown'
            ]) ?>
            <ul class="dropdown-menu documents-dropdown" style="right: 0; left: auto;">
                <?php foreach ($cancellations as $item) : ?>
                    <li>
                        <?= Html::a($item->title, [
                            '/documents/goods-cancellation/view',
                            'id' => $item->id,
                        ], [
                            'class' => 'dropdown-item',
                        ]) ?>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php endif ?>
<?php endif ?>
<!-- WAYBILL -->
<?php if ($model->has_goods && $ioType == Documents::IO_TYPE_OUT && $model->company->show_waybill_button_in_invoice): ?>
    <?php if (!$model->waybills): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'waybill',
            'title' => 'Трансп. накладная',
            'model' => $model,
            'action' => 'create',
            'id' => 0,
        ]);
        ?>
    <?php elseif (count($model->waybills) == 1 && !$model->canAddWaybill): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'waybill',
            'title' => 'ТТН №' . $model->waybills[0]->fullNumber . ' от ' . DateHelper::format($model->waybills[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'model' => $model,
            'action' => 'view',
            'id' => $model->waybills[0]->id,
        ]);
        ?>
    <?php else: ?>
        <?= $this->render('status_rows/_many_rows', [
            'document' => 'waybill',
            'title' => 'Трансп. накладная',
            'model' => $model,
            'addAvailable' => $model->canAddWaybill,
        ]);
        ?>
    <?php endif ?>
<?php endif ?>
<!-- PROXY -->
<?php if ($model->has_goods && $ioType == Documents::IO_TYPE_IN): ?>
    <?php if (!$model->proxies): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'proxy',
            'title' => 'Доверенность',
            'model' => $model,
            'action' => 'create',
            'id' => 0,
        ]);
        ?>
    <?php elseif (count($model->proxies) == 1 && !$model->canAddProxy): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'proxy',
            'title' => 'Доверенность №' . $model->proxies[0]->document_number . ' от ' . DateHelper::format($model->proxies[0]->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'model' => $model,
            'action' => 'view',
            'id' => $model->proxies[0]->id,
        ]);
        ?>
    <?php else: ?>
        <?= $this->render('status_rows/_many_rows', [
            'document' => 'proxy',
            'title' => 'Доверенность',
            'model' => $model,
            'addAvailable' => $model->canAddProxy,
        ]);
        ?>
    <?php endif; ?>
<?php endif; ?>
<!-- ORDER -->
<?php if ($model->orderDocument): ?>
    <?= $this->render('status_rows/_first_row', [
        'document' => 'order-document',
        'title' => 'Заказ №' . $model->orderDocument->document_number . ' от ' . DateHelper::format($model->orderDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        'model' => $model,
        'action' => 'view',
        'id' => $model->orderDocument->id,
    ]); ?>
<?php endif; ?>
<!-- AGENT REPORT -->
<?php if ($model->agentReport): ?>
    <?= $this->render('status_rows/_first_row', [
        'document' => 'agent-report',
        'title' => 'Отчет Агента',
        'model' => $model,
        'action' => 'view',
        'id' => $model->agentReport->id,
    ]); ?>
<?php endif; ?>
<!-- PAYMENT ORDER -->
<?php if ($ioType == Documents::IO_TYPE_IN && $model->paymentOrderInvoices) : ?>
    <?php if (count($model->paymentOrderInvoices) == 1): ?>
        <?= Html::a('<i class="pull-left icon icon-doc"></i> ПЛАТЁЖНОЕ ПОРУЧЕНИЕ', [
            '/documents/payment-order/view',
            'id' => $model->paymentOrderInvoices[0]->payment_order_id,
        ], [
            'class' => 'btn yellow width317 tooltipstered' . ($model->isRejected ? ' disabled' : ''),
            'style' => 'width: 100%!important;margin-bottom: 12px;' . ($model->isRejected ? ' background-color: #a2a2a2;' : ''),
            'title' => 'Платежное поручение',
        ]) ?>
    <?php else: ?>
        <div class="clearfix" style="display: block; position: relative;">
            <div class="col-xs-12 pad0">
                <a data-toggle="dropdown"
                   class="btn yellow dropdown-toggle tooltipstered<?= $model->isRejected ? ' disabled' : ''; ?>"
                    <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
                   style="text-transform: uppercase;width: 100%; margin-left: 0;margin-bottom: 12px; padding-right: 14px; padding-left: 14px;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
                   title="ПЛАТЁЖНОЕ ПОРУЧЕНИЕ">
                    <i class="pull-left icon icon-doc"></i>
                    ПЛАТЁЖНОЕ ПОРУЧЕНИЕ
                </a>
                <ul class="dropdown-menu" style="width: 317px;">
                    <?php foreach ($model->paymentOrderInvoices as $paymentOrderInvoice): ?>
                        <li>
                            <a class="dropdown-item" style="position: relative;"
                               href="<?= Url::to(['payment-order/view', 'id' => $paymentOrderInvoice->payment_order_id]); ?>">
                        <span class="dropdown-span-left">
                            № <?= $paymentOrderInvoice->paymentOrder->document_number ?>
                            от <?= Yii::$app->formatter->asDate($paymentOrderInvoice->paymentOrder->document_date); ?>
                        </span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<!-- Help (pulsate button) -->
<?php if ($createdInvoicesCount <= 3) {
    $this->registerJs('
        // Pulsate block
        (function($) {
            jQuery.fn.weOffset = function () {
                var de = document.documentElement;
                var box = $(this).get(0).getBoundingClientRect();
                var top = box.top + window.pageYOffset - de.clientTop;
                var left = box.left + window.pageXOffset - de.clientLeft;
                var width = $(this).width();
                var height = $(this).height();
                return { top: top, left: left, width: width, height: height };
            };
        }(jQuery));

        $(document).ready(function() {

            var elementOffset = $("#document-panel-pulsate").weOffset();

            jQuery("<div/>", {
                class: "pulsate-block",
            }).css({
                "position": "absolute",
                "top": elementOffset.top + 10,
                "left": elementOffset.left,
                "width": elementOffset.width,
                "height": elementOffset.height - 10
            }).appendTo("body").pulsate({
                color: "#bf1c56",
                reach: 20,
                repeat: 2
            });

            setTimeout(\'jQuery(".pulsate-block").remove()\', 2500);
        });

        $(window).resize(function() {
            if (jQuery(".pulsate-block").length) {
                var elementOffset = $("#document-panel-pulsate").weOffset();

                jQuery(".pulsate-block").css({
                    "position": "absolute",
                    "top": elementOffset.top + 10,
                    "left": elementOffset.left,
                    "width": elementOffset.width,
                    "height": elementOffset.height - 10
                }).appendTo("body");
            }
        });
    ');
}
?>