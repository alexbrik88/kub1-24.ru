<?php

use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\WaybillStatus;
use common\models\document\status\InvoiceFactureStatus;
use frontend\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\status\ProxyStatus;

/**
 * @var $model \common\models\document\Invoice
 * @var $id integer
 * @var $document string
 * @var $title string
 * @var $action string
 */

$url = $action == 'create' ?
    [$document . '/' . $action, 'type' => $model->type, 'invoiceId' => $model->id] :
    [$document . '/' . $action, 'type' => $model->type, 'id' => $id];

$iconClass = 'add-icon';

$isCreateProxy = ($document == 'proxy' && $action == 'create');

if ($document == 'waybill') {
    if ($model->getWaybill()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'new-doc' : 'new-doc';
            //WaybillStatus::getStatusIcon($model->getWaybill()->min('status_out_id'));
    }
} elseif ($document == 'packing-list') {
    if ($model->getPackingList()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'new-doc' : 'new-doc';
            //PackingListStatus::getStatusIcon($model->getPackingList()->min('status_out_id'));
    }
} elseif ($document == 'sales-invoice' && $model->getSalesInvoice()->exists()) {
    $iconClass = 'new-doc';
} elseif ($document == 'proxy') {
    if ($model->getProxies()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'new-doc' : 'new-doc';
            //ProxyStatus::getStatusIcon($model->getProxy()->min('status_out_id'));
    }
} elseif ($document == 'act') {
    if ($model->getAct()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'new-doc' : 'new-doc';
            //ActStatus::getStatusIcon($model->getAct()->min('status_out_id'));
    }
} elseif ($document == 'invoice-facture') {
    if ($model->getInvoiceFactures()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'new-doc' : 'new-doc';
            //InvoiceFactureStatus::getStatusIcon($model->getInvoiceFactures()->min('status_out_id'));
    }
} elseif ($document == 'upd') {
    if ($model->getUpds()->exists()) {
        $iconClass = 'new-doc';
    }
} elseif ($document == 'order-document') {
    $iconClass = 'new-doc';
} elseif ($document == 'agent-report') {
    $iconClass = 'new-doc';
    $url = [$document . '/' . $action, 'type' => $model->agentReport->type, 'id' => $id];
}
?>

<?= Html::a($this->render('//svg-sprite', ['ico' => $iconClass]).Html::tag('span', $title), $url, [
    'class' => 'button-regular button-hover-content-red w-100 text-left'.
        ($model->isRejected ? ' disabled' : '').
        ($isCreateProxy ? ' add-proxy' : ''),
    'title' => ($action == 'create' ? 'Добавить ' : '') . $title,
]) ?>

<?php /*if ($isCreateProxy): ?>
    <?= $this->render('@frontend/modules/documents/views/proxy/_viewPartials/_invoices_modal', ['fromInvoice' => $model->id]) ?>
<?php endif;*/ ?>