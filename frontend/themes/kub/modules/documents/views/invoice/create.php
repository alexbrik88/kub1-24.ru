<?php

use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceContractEssence;
use common\models\document\InvoiceEssence;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\themes\kub\components\Icon;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/** @var View $this */
/** @var Invoice $model */
/** @var Company $company */
/** @var boolean $fixedContractor */
/** @var Message $message */
/** @var integer $ioType */
/** @var ActiveForm $form */
/** @var InvoiceContractEssence $invoiceContractEssence */
/** @var InvoiceEssence $invoiceEssence */
/** @var string|null $create */
/** @var string $document */
/** @var integer $projectId */
/** @var integer $projectEstimateId */
/** @var integer $hasProjectEstimate */

$this->title = 'Создать ' . mb_strtolower($message->get(Message::TITLE_SINGLE));
$this->context->layoutWrapperCssClass = 'create-out-invoice';

$projectEstimateId = empty($projectEstimateId) ? $model->project_estimate_id : $projectEstimateId;
$hasProjectEstimate = $projectEstimateId > 0;

if (!isset($returnUrl)) {
    $returnUrl = '';
}

$errorModelArray = [$model];
if (empty($autoinvoice)) {
    $autoinvoice = null;
} else {
    $errorModelArray[] = $autoinvoice;
}
$errorModelArray = array_merge($errorModelArray, $model->orders);

$isFirstCreate = \Yii::$app->controller->action->id === 'first-create';
$isAuto = $model->isAutoinvoice && $autoinvoice != null;
if (!isset($create)) {
    $create = null;
}
if (!isset($document)) {
    $document = null;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$fileButton = '';
if ($model->type == Documents::IO_TYPE_OUT) {
    if (!$company->getImage('logoImage')) {
        $fileButton = [0, 'ЛОГОТИП'];
    } elseif ($company->company_type_id != CompanyType::TYPE_IP && !$company->getImage('printImage')) {
        $fileButton = [1, 'ПЕЧАТЬ'];
    } elseif (!$company->getImage('chiefSignatureImage')) {
        $fileButton = [2, 'ПОДПИСЬ'];
    }
}

if (0) {
    $fileButton = Html::a(
        Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-plus',
            'style' => 'font-size: 10px; vertical-align: 1px;',
            'aria-hidden' => 'true',
        ]) . ' ' . Html::tag('span', $fileButton[1]),
        '#',
        [
            'id' => 'companyImages-button',
            'class' => 'btn yellow companyImages-button',
            'data-toggle' => 'modal',
            'data-target' => '#modal-companyImages',
            'data-tab' => $fileButton[0],
        ]
    );
}

if ($document == null) {
    $logoTooltipLine = '/img/tips/arrow/2.png';
    $logoPositionTooltip = 'top: -20px;left: 325px;';
} else {
    $logoTooltipLine = '/img/tips/arrow/3.png';
    $logoPositionTooltip = 'top: 19px;left: 345px;';
}

$this->registerJs('
    $(document).on("click", ".companyImages-button", function() {
        var tabId = parseInt($(this).data("tab"));
        $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
    });
    $(document).on("hidden.bs.modal", ".modal-companyImages", function() {
        $.pjax.reload("#companyImages-button-pjax", {timeOut: 5000});
    });
    ');
?>

<div>
    <div class="tooltip_help_templates" style="display:none">
        <div id="tooltip_requisites">
            <div class="hidden1024 pad0" style="width: 578px; position: absolute; color: white">
                <div style="display: block; width: 35px">
                    <img style="width: 55px;height: 37px;" src="/img/tips/arrow/1.png">
                </div>
                <div style="display: block;padding-left: 29px;width: 341px;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 1</div>
                    </div>
                    <div style="float: left;width: 221px;">
                        <h4 style="margin: 0px; padding-top:3px"><strong> Автозаполнение </strong></h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Заполните реквизиты клиента и профиль
                            Вашей компании, что бы данные автоматически подтягивались в акты и накладные</p>
                        <a href="javascript:void(0);"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>
        <div id="tooltip_help_add_product">
            <div class="hidden1024 pad0" style="width: 578px;position: absolute;color: white;margin-top: -11px;">
                <div style="display: block;width: 92px;float: left;margin-top: -10px !important;">
                    <img style="width: 88px;height: 39px;" src="/img/tips/arrow/2.png">
                </div>
                <div style="width: 304px;float: left;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 2</div>
                    </div>
                    <div style="float: left;width: 221px;">
                        <h4 style="margin: 0px; padding-top:3px"><strong> Товары и услуги </strong></h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Добавьте Ваш товар или услугу. Далее вы
                            сможе выставлять <br> счета выбрав данное <br> наименование из списка </p>
                        <a href="javascript:void(0);"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="tooltip_help_logo">
            <div class="hidden1024 pad0" style="width: 471px;position: absolute;color: white;">
                <div style="display: block;width: 92px;float: left;">
                    <img style="width: 85px;height: 35px;" src="<?= $logoTooltipLine; ?>">
                </div>
                <div style="width: 304px;float: left;padding-top: 13px;">
                    <div style="float: left;width: 45px">
                        <img style="height:25px;" src="/img/tips/arrow/circle.png">
                        <div style="margin-top:-22px; text-align: center;width: inherit;"> 3</div>
                    </div>
                    <div style="float: left;width: 250px;">
                        <h4 style="margin: 0px; padding-top:3px">
                            <strong> Профессиональный вид
                                <?php if ($document == Documents::SLUG_ACT): ?>
                                    акта
                                <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                    ТН
                                <?php elseif ($document == Documents::SLUG_WAYBILL): ?>
                                    ТТН
                                <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                    СФ
                                <?php elseif ($document == Documents::SLUG_UPD): ?>
                                    УПД
                                <?php else: ?>
                                    счета
                                <?php endif; ?>
                            </strong>
                        </h4>
                        <p style="padding-top: 10px;padding-bottom: 10px;"> Загрузите логотип, печать и подпись
                            <br> и они будут подгружаться <br> автоматически в счета </p>
                        <a href="javascript:void(0);" class="companyImages-button"
                           style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                            Понятно </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- remove product -->
<div id="modal-remove-one-product" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить эту позицию из счета?
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- helpers tooltip -->
<?php echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-first-r',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-light'],
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]); ?>

<?php // KUB
// form
/** for itself */
$formDefaultConfig = [
    'layout' => 'default',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => '',
        ],
        'hintOptions' => [
            'tag' => 'small',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => '',
            'hint' => '',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
]; ?>

<div class="form">
    <?php $form = ActiveForm::begin(array_merge($formDefaultConfig, [
        'id' => 'invoice-form',
        'options' => [
            'enctype' => 'multipart/form-data',
            'class' => 'add-avtoschet',
            'data' => [
                'iotype' => $ioType,
                'new' => $model->isNewRecord ? 1 : 0
            ]
        ],
        'enableClientValidation' => false,
    ])); ?>

    <?= $form->errorSummary($errorModelArray); ?>

    <?= Html::hiddenInput('returnUrl', $returnUrl) ?>

    <?= Html::hiddenInput('document', $document, [
        'id' => 'create-document',
    ]); ?>

    <?= $this->render('form/_form_header', [
        'model' => $model,
        'message' => $message,
        'form' => $form,
        'isAuto' => $isAuto,
        'autoinvoice' => $autoinvoice,
        'create' => $create,
        'fileButton' => false,
        'document' => $document,
        'ioType' => $ioType,
        'newDoc' => isset($newDoc) ? $newDoc : null
    ]); ?>

    <?= $this->render('form/_form_body', [
        'model' => $model,
        'message' => $message,
        'fixedContractor' => $fixedContractor,
        'ioType' => $ioType,
        'company' => $company,
        'form' => $form,
        'isAuto' => $isAuto,
        'autoinvoice' => $autoinvoice,
        'invoiceEssence' => $invoiceEssence,
        'document' => $document,
        'hasProjectEstimate' => $hasProjectEstimate,
        'projectEstimateId' => $projectEstimateId,
    ]); ?>

    <?= $this->render('form/_product_table_invoice', [
        'model' => $model,
        'ioType' => $ioType,
        'company' => $company,
        'invoiceContractEssence' => $invoiceContractEssence,
        'document' => $document,
        'projectEstimateId' => $projectEstimateId,
    ]); ?>

    <?php if ($company->strict_mode == Company::ON_STRICT_MODE) : ?>
        <?php $docName = $create ? Documents::$slugToName[$create] : ($document ? Documents::$slugToName[$document] : 'Счет'); ?>
        <div class="wrap" style="margin-top: 20px">
            <h4>Добавить в счет логотип, подпись и печать</h4>
            <div class="row">
                <div class="col-4">
                    <div class="addLogoWrap">
                        <span data-toggle="modal" data-target="#modal-companyImages" class="addLogo link">Выберите файлы</span>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?= $this->render('form/_form_buttons', [
        'model' => $model,
        'isAuto' => $isAuto,
        'ioType' => $ioType,
        'fixedContractor' => $fixedContractor,
        'autoinvoice' => empty($autoinvoice) ? null : $autoinvoice,
        'document' => $document,
    ]); ?>
    <?php ActiveForm::end(); ?>
</div>


<?= $this->render('form/_form_product_new'); ?>

<?= $this->render('form/_form_product_existed', [
    'model' => $model,
]); ?>

<?php if ($fileButton || $company->strict_mode == Company::ON_STRICT_MODE) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'title' => 'Загрузить логотип, печать и подпись',
        'titleOptions' => [
            'class' => 'mb-0',
        ],
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>

    <div class="tooltip_templates" style="display: none;">
    <span id="tooltip_contractor"
          style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите по вашему покупателю ИНН,<br>
        остальные реквизиты заполнятся автоматически.<br>
        Данные по покупателю сохранятся, в дальнейшем вы<br>
        будите выбирать его из списка.
    </span>
        <span id="tooltip_company"
              style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите ИНН, БИК и расч. счет<br>
        по вашей компании или ИП,<br>
        остальные реквизиты заполнятся автоматически.
    </span>
        <span id="tooltip_add_product"
              style="display: inline-block; text-align: center;">
        В наименовании нажмите «Добавить новый товар/услугу»,<br>
        после заполните информацию по вашему товару или услуге.<br>
        Данные сохранятся, в дальнейшем вы<br>
        будите выбирать товар или услугу  из списка.
    </span>
        <span id="tooltip_upload"
              style="display: inline-block; text-align: center;">
        Что бы счет выглядел профессионально и красиво<br>
        добавьте ваш логотип, а так же подпись и печать
    </span>
        <span id="tooltip_service_no_delete"
              style="display: inline-block; text-align: center;">
        Нельзя удалить услугу,<br>
        т.к. она используется в акте<br>
        к данному счету
    </span>
        <span id="tooltip_goods_no_delete"
              style="display: inline-block; text-align: center;">
        Нельзя удалить товар, который уже<br>
        используется в товарной накладной
    </span>
        <span id="tooltip_service_no_edit"
              style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одном Акте
    </span>
        <span id="tooltip_goods_no_edit"
              style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одной ТН
    </span>
        <span id="tooltip_remind"
              style="display: inline-block; text-align: center;">
        Если счет не будет оплачен, то<br>
        <span id="when_remind"></span>
        будет автоматически<br>
        отправлено письмо вашему покупателю<br>
        с напоминанием об оплате счета
    </span>
    <span id="tooltip_refresh_company" style="display: inline-block; text-align: center;">
        Реквизиты вашей компании были изменены.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
        <span id="tooltip_refresh_contractor" style="display: inline-block; text-align: center;">
        Информация по покупателю была изменена.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
        <span id="product_services_title" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий месяц,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за февраль 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Месяц и год»</span>.<br>
        Если вы выставляете счета за следующий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за март 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий месяц и год»</span>.<br>
        Если вы выставляете счета за предыдущий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за январь 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий месяц и год»</span>.
    </span>
        <span id="product_services_title2" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий кватрал,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Кватрал и год»</span>.<br>
        Если вы выставляете счета за следующий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 2-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий кватрал и год»</span>.<br>
        Если вы выставляете счета за предыдущий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 01.04.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий кватрал и год»</span>.
    </span>
        <span id="tooltip_paydate_limit"
              style="display: inline-block;">
        Срок оплаты подставляется автоматически в
        <br>
        комментарий к счету из поля "Оплатить до"
    </span>
        <span id="tooltip_comment_client"
              style="display: inline-block;">
        Добавьте к счету комментарий для покупателя.
        <br>
        Покупатель получит дополнительную информацию
        <br>
        о деталях оплаты, условиях договора и тд.
    </span>
        <span id="tooltip_currency_select"
              style="display: inline-block; text-align: center;">
        Выставляйте счет в любой валюте.
        <br>
        Выберите валюту из списка.
    </span>
        <span id="invoice-contract-tooltip">
            <?= Html::img('/images/invoice-contract-kub.png', [370, 210]); ?>
        </span>
        <span id="tooltip_hidden-discount" class="text-center" style="display: inline-block;">
            Используйте, когда нужно посчитать скидку от текущей цены,<br>
            но клиент не должен знать, что есть скидка.<br>
            В счете для клиента столбцов со скидкой не будет,<br>
            но цена будет указана со скидкой.
        </span>
    </div>
<?php
$videoModalArray = [
    'video_modal_contractor' => 'https://player.vimeo.com/video/199833243?title=0&byline=0&portrait=0',
    'video_modal_company' => 'https://player.vimeo.com/video/199827240?title=0&byline=0&portrait=0',
    'video_modal_add_product' => 'https://player.vimeo.com/video/199833591?title=0&byline=0&portrait=0',
    'video_modal_upload' => 'https://player.vimeo.com/video/199833809?title=0&byline=0&portrait=0',
];
foreach ($videoModalArray as $id => $src) {
    Modal::begin([
    'id' => $id,
    'toggleButton' => false,
    'size' => 'modal-lg',
    'closeButton' => false
    ]);
?>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div style="text-align: center">
        <iframe width="480" height="370" allowfullscreen="true" src="<?= $src ?>" class="video-container"></iframe>
    </div>
<?php
     Html::tag('div', Html::tag('iframe', '', [
        'width' => 480,
        'height' => 370,
        'allowfullscreen' => true,
        'src' => $src,
    ]), ['class' => 'video-container']);
    Modal::end();
}
?>
<?php /*
<?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
    <?= $this->render('//xls/_import_xls', [
        'header' => 'Загрузка товаров из Excel',
        'text' => '<p>Для загрузки списка товаров из Excel,
                                    <br>
                                    заполните шаблон таблицы и загрузите ее тут.
                                    </p>',
        'formData' => Html::hiddenInput('className', 'Product') .
            Html::hiddenInput('addToInvoice', 1) .
            Html::hiddenInput('Product[production_type]', Product::PRODUCTION_TYPE_GOODS),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => XlsHelper::PRODUCT_GOODS]),
    ]); ?>
<?php endif; ?> */ ?>
<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'title' => '',
    'closeButton' => false
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();
?>

<?php /* BS3
<?= $this->render('_modal_zero_submit', ['form' => 'invoice-form']); ?>
*/ ?>
<?php

$this->registerJs('

$(document).ready(function() {
    // fix Chrome chosen select
    var fixedContractor = '.(int)$fixedContractor.';
    var agreementListCount = $("#invoice-agreement").find("option").length;
    if (!fixedContractor && agreementListCount == 0 && $("#invoice-contractor_id").val())
        $("#invoice-contractor_id").trigger("change");
})

$(document).on("change", "#invoice-agreement, #invoiceauto-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({
            url: "/documents/agreement/create?contractor_id=" + $("#invoice-contractor_id, #invoiceauto-contractor_id").val() + "&type=" + $("#documentType").val() + "&returnTo=invoice" + "&container=agreement-select-container",
            container: "#agreement-form-container",
            push: false,
            timeout: 5000,
        });

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
        });
        $("#agreement-modal-container").modal("show");
        $(this).val("").trigger("change");
    }
});
$(document).on("change", "select#invoice-project_id", function() {

    var pjaxProjectEstimate = $("#project-estimate-pjax-container");
    var contractorId = $("#invoice-contractor_id").val();
    var documentType = '. (int)$model->type .'
    
    if (pjaxProjectEstimate.length) {
        var projectId = $("#invoice-project_id").val();
        $.pjax({
            url: pjaxProjectEstimate.data("url"),
            data: {"contractorId": contractorId, "projectId": projectId},
            container: "#project-estimate-pjax-container",
            push: false,
            timeout: 10000
        });
    }
}); 

$(document).on("change", "select#invoice-contractor_id", function() {
    const opt = $(this).find("option:selected").first();
    if (opt.data("industry")) {
        $("#invoice-industry_id").val(opt.data("industry")).trigger("change");
    } else {
        $("#invoice-industry_id").val(null).trigger("change");
    }
    if (opt.data("sale_point")) {
        $("#invoice-sale_point_id").val(opt.data("sale_point")).trigger("change");
    } else {
        $("#invoice-sale_point_id").val(null).trigger("change");
    }
});

$(document).on("change", "select#invoice-contractor_id, #invoiceauto-contractor_id", function() {
    var form = this.form;
    var pjax = $("#agreement-pjax-container", form);
    var pjaxProject = $("#project-pjax-container", form);
    var pjaxProjectEstimate = $("#project-estimate-pjax-container", form);    
    
    var contractorId = $(this).val();
    var projectId = ' . (isset($projectId) ? (int)$projectId : 0) .';
    var documentType = '. (int)$model->type .'

    if (this.value != "add-modal-contractor") {
        if (pjax.length) {
            $.pjax({
                url: pjax.data("url"),
                data: {contractorId: contractorId},
                container: "#agreement-pjax-container",
                push: false,
                timeout: 10000
            }).done(function(){
                if (pjaxProject.length && documentType == 2) {
                    $.pjax({
                        url: pjaxProject.data("url"),
                        data: {"contractorId": contractorId, "projectId": projectId},
                        container: "#project-pjax-container",
                        push: false,
                        timeout: 10000
                    }).done(function() {
                        if (pjaxProjectEstimate.length) {
                            var projectId = $("#invoice-project_id").val();
                            $.pjax({
                                url: pjaxProjectEstimate.data("url"),
                                data: {"contractorId": contractorId, "projectId": projectId},
                                container: "#project-estimate-pjax-container",
                                push: false,
                                timeout: 10000
                            });
                        }
                    });
                }
            });
        }

        if (!!$("#invoice-form").data("new")) {
            const paymentRules = $("#morePaymentRules");
            const getWithPaymentLimitRules = paymentRules.length;
            $.post("get-contractor-info", {contractor_id: this.value, with_payment_rules: getWithPaymentLimitRules}, function (data) {
                if (data.discount > 0) {
    
                    $("#all-discount").val(data.discount);
                    $(".discount-input").val(data.discount);
    
                    $("#invoice-has_discount").prop("checked", true);
                    $("#invoice-has_markup").prop("checked", false);
                    $("#invoice-has_markup:not(.md-check)");
                    $(".markup_column").addClass("hidden");
                    $("#invoice_all_markup input").val(0);
                    $(".markup-input").val(0);
                    $(".discount_column").removeClass("hidden");
                } else {
    
                    $("#all-discount").val(0);
                    $(".discount-input").val(0);
    
                    $("#invoice-has_discount").prop("checked", false);
                    $(".discount_column").addClass("hidden");
                }
    
                $("table .discount-input").val(data.discount).each(function() {
                    INVOICE.recalculateItemPrice($(this));
                });
                INVOICE.recalculateInvoiceTable();
                
                if (getWithPaymentLimitRules && data.paymentLimitRules) {
                    // 1) payment_limit_days
                    $(paymentRules).find("input[name=\"paymentLimitRule[1][payment_limit_days]\"").val(data.paymentLimitRules.payment_limit_days);
                    $(paymentRules).find("input[name=\"paymentLimitRule[3][payment_limit_days]\"").val(data.paymentLimitRules.payment_limit_days); 
                    // 2) related_document_payment_limit_days
                    $(paymentRules).find("input[name=\"paymentLimitRule[2][related_document_payment_limit_days]\"").val(data.paymentLimitRules.related_document_payment_limit_days);
                    $(paymentRules).find("input[name=\"paymentLimitRule[3][related_document_payment_limit_days]\"").val(data.paymentLimitRules.related_document_payment_limit_days);
                    // 3) payment_limit_percent / related_document_payment_limit_percent 
                    $(paymentRules).find("input[name=\"paymentLimitRule[3][payment_limit_percent]\"").val(data.paymentLimitRules.payment_limit_percent);                          
                    $(paymentRules).find("input[name=\"paymentLimitRule[3][related_document_payment_limit_percent]\"").val(data.paymentLimitRules.related_document_payment_limit_percent);

                    // set rule
                    $("input.radio_payment_limit_rule")
                        .filter("[value=" + data.paymentLimitRules.payment_limit_rule + "]")
                        .prop("checked", true)
                        .trigger("change")
                        .end()
                        .uniform("refresh");
                }
            });
        }
    }

    $(".contractor-invoice-dossier").addClass("hidden");
    $(".verified,#popup-confirm a.button-clr").attr("href", "/dossier?id=" + this.value);
    if (this.value && this.value != "add-modal-contractor") {
        let option=$(this.options[this.selectedIndex]);
        let faceType=option.data("face_type");
        if(faceType!='.Contractor::TYPE_PHYSICAL_PERSON.' && faceType!='.Contractor::TYPE_FOREIGN_LEGAL_PERSON.') {
            if(option.data("verified") == 0) {
                $(".contractor-invoice-dossier.not-verified").removeClass("hidden");
            }
        }
    }

});
$(document).on("pjax:success", "#agreement-pjax-container", function() {
    if (window.AgreementValueLong) {
        $("#invoice-agreement").val(window.AgreementValueLong).trigger("change");
    }
    var newDocumentDate = $("#under-date").val().split(".").reverse().join("-");
    var delay = parseInt($("#invoice-agreement").data("delay"));
    var payTo = moment(newDocumentDate).add(delay,"days").format("DD.MM.YYYY");
    var contacts = $("#invoice-agreement").data("contacts");
    var contactItems = [
        "director",
        "chief_accountant",
        "contact",
    ]
    if (!!$("#invoice-form").data("new")) {       
        $("#autoinvoice-payment_delay").val(delay).trigger("change");
        
        //$("#invoice-payment_limit_date").val(payTo).trigger("change");
        $("#invoice-payment_limit_date").data("delay", delay);
        $("#invoice-payment_limit_date").data("datepicker").selectDate(moment(newDocumentDate).add(delay,"days"));
        $("#under-date").trigger("change");
    }
    if ($("#invoice-hasnds").length) {
     //   $("#invoice-hasnds").prop("checked", $("#invoice-agreement").data("outnds") == "1" ? true : false).trigger("change");
    }
    if ($("#invoice-agreement").data("refresh")) {
        $("#title_conf_toggle").attr("style", "cursor: pointer; border-bottom: 1px dashed #333333;");
        $("#title_conf_toggle").addClass("title_conf-toggle");
    } else {
        $("#title_conf_toggle").removeAttr("style", "cursor: pointer; border-bottom: 1px dashed #333333;");
        $("#title_conf_toggle").removeClass("title_conf-toggle");
    }
    if (+$("#invoice-agreement").data("invoice_count") > 0) {

        let select=document.getElementById("invoice-contractor_id");
        let option=$(select.options[select.selectedIndex]);
        let faceType=option.data("face_type");
        if(faceType!='.Contractor::TYPE_PHYSICAL_PERSON.' && faceType!='.Contractor::TYPE_FOREIGN_LEGAL_PERSON.') {
            if(option.data("verified") == 1) {
                $(".contractor-invoice-dossier.verified").removeClass("hidden");
            }
        }

        $(".contractor-invoice-debt").removeClass("hidden");
        $(".contractor-invoice-debt .count").text($("#invoice-agreement").data("invoice_count"));
        $(".contractor-invoice-debt .amount").text($("#invoice-agreement").data("invoice_sum"));
    } else {
        $(".contractor-invoice-debt").addClass("hidden");
        $(".contractor-invoice-debt .count").text("");
        $(".contractor-invoice-debt .amount").text("");
    }
    var sendTo = $("#autoinvoiceform-send_to");
    $(".autoinvoice_send_to:not(.in)").collapse("show");
    contactItems.forEach(function(contact) {
        var name = "",
            email = "";
        
        if (contacts) {
            name = contacts[contact + "_name"] || "";
            email = contacts[contact + "_email"] || "";
        }
        
        // mxfi: break first reload _basis_document
        if ($("." + contact + "_name span", sendTo).html() == name)
            return false;

        var input = $("input.item-send-to[type=checkbox][value="+contact+"]");
        if (contact != "director") {
            var isDirector = contacts[contact + "_is_director"];
            $(".send_to_" + contact + " .toggle-is_director.yes", sendTo).toggleClass("hidden", isDirector == 0);
            $(".send_to_" + contact + " .toggle-is_director.no", sendTo).toggleClass("hidden", isDirector == 1);
            input.data("is-director", isDirector);
        }
        $("." + contact + "_name span", sendTo).html(name).toggleClass("hidden", name == "");
        $("." + contact + "_name input", sendTo).val("").toggleClass("hidden", name != "").prop("disabled", name != "");
        $("." + contact + "_email span", sendTo).html(email).toggleClass("hidden", email == "");
        $("." + contact + "_email input", sendTo).val("").toggleClass("hidden", email != "").prop("disabled", email != "");
        input.prop("checked", input.val() == "director").trigger("change");
    });
    $("#invoice-invoice_expenditure_item_id").val($("#invoice-agreement").data("expenditure")).trigger("change");
});
$(document).on("click", ".toggle-is_director-btn", function (e) {
    e.preventDefault();
    $($(this).data("target")).toggleClass("hidden");
    $("input", $($(this).data("target")+".no")).prop("disabled", $($(this).data("target")+".no").hasClass("hidden"));
});
$(document).on("change", "input.item-send-to[data-is-director]", function (e) {
    var isDirector = $(this).data("is-director");
    var label = $(this).closest("label");
    if (this.checked) {
        $(".toggle-is_director.yes", label).addClass("hidden");
        $(".toggle-is_director.no", label).removeClass("hidden");
        $("input[type=text]", label).prop("disabled", false);
    } else {
        $(".toggle-is_director.yes", label).toggleClass("hidden", isDirector != 1);
        $(".toggle-is_director.no", label).toggleClass("hidden", isDirector == 1);
        $("input[type=text]", label).prop("disabled", true);
    }
});
$(document).on("click", ".title_conf-toggle", function () {
    $("#title_conf").toggleClass("hidden");
});
$(document).on("click", "#refresh-product-table", function(e) {
    e.preventDefault();
    $.pjax({
        url: $("#product-table-invoice").data("url"),
        data: {
            type: $("#documentType").val(),
            id: $("#select-existing-contractor select").val(),
            nds: ($(".with-nds-item").hasClass("hidden") ? 0 : 1)
        },
        container: "#product-table-invoice",
        push: false,
        timeout: 5000,
    });
    $(document).on("pjax:success", "#product-table-invoice", function() {
        $("#invoice-has_discount").trigger("change");
    })
});
');

if (Yii::$app->session->remove('show_example_popup') || Yii::$app->request->get('show_example_popup')) {
    if ($document == Documents::SLUG_ACT) {
        $header = ' Так будет выглядеть ваш акт';
    } elseif ($document == Documents::SLUG_PACKING_LIST) {
        $header = ' Так будет выглядеть ваша ТН';
    } elseif ($document == Documents::SLUG_INVOICE_FACTURE) {
        $header = ' Так будет выглядеть ваша СФ';
    } elseif ($document == Documents::SLUG_UPD) {
        $header = ' Так будет выглядеть ваш УПД';
    } else {
        $header = ' Так будет выглядеть ваш счёт';
    }
    Modal::begin([
        'footerOptions' => ['style' => 'text-align: center;'],
        'id' => 'show_example_popup',
        'title' => false,
        'clientOptions' => ['show' => true],

    ]);
    $company = $model->company;
    switch ($document) {
        case Documents::SLUG_ACT:
            $src = '/img/examples/act.jpg';
            break;
        case Documents::SLUG_PACKING_LIST:
            $src = '/img/examples/packing_list.jpg';
            break;
        case Documents::SLUG_UPD:
            $src = '/img/examples/act.jpg';
            break;
        case Documents::SLUG_INVOICE_FACTURE:
            $src = '/img/examples/invoice_facture.jpg';
            break;
        default:
            if ($model->is_invoice_contract) {
                $src = '/img/examples/invoice_contract.jpg';
            } else {
                if ($company->company_type_id == CompanyType::TYPE_IP) {
                    $src = '/img/examples/ip_na_usn.jpg';
                } elseif ($company->companyTaxationType->usn) {
                    $src = '/img/examples/ooo_na_usn.jpg';
                } else {
                    $src = '/img/examples/ooo_na_osno_with-nds.jpg';
                }
            }
            break;
    }
?>
    <h4 class="modal-title"><?= $header ?></h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>
<?php
    echo Html::beginTag('div', ['class' => 'text-center']);
    echo Html::img($src, ['style' => 'max-width: 100%;']);
    echo '<br>';
    echo Html::button('Ок', [
        'class' => 'button-clr button-regular button-width button-hover-transparent mt-3',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]);
    echo Html::endTag('div');
    Modal::end();
}
if (\Yii::$app->controller->action->id == 'first-create') {
    $this->registerJs('INVOICE.recalculateInvoiceReady();');
}
if (\Yii::$app->request->get('contractorId') && !\Yii::$app->request->get('fromAgreement')) {
    $this->registerJs('$("select#invoice-contractor_id").trigger("change")');
}
?>
<?php
$urlAdd = \yii\helpers\Url::to(['invoice/autoinvoice']);
$urlRedirect = \yii\helpers\Url::to(['invoice/index', 'type' => 2]);
$this->registerJs(<<<JS
    var checkRemindTooltip = function() {
        if (parseInt($('#invoice-isautoinvoice').val()) == 1) {
            var remindDay = parseInt($('#autoinvoice-payment_delay option:selected').val()) - 1;
            $('#tooltip_remind #when_remind').text('на ' + remindDay + ' день');
        } else {
            var remindDate = moment($('#invoice-payment_limit_date').val(), 'DD.MM.YYYY').subtract(1, 'days');
            if (remindDate.isValid()) {
                $('#tooltip_remind #when_remind').text(remindDate.format('DD.MM.YYYY'));
            }
        }
        $('#tooltip_remind_ico').tooltipster('content', $('#tooltip_remind')[0].outerHTML);
    };
    checkRemindTooltip();
    $(document).on('change','#invoice-payment_limit_date, #autoinvoice-payment_delay',function() {
        checkRemindTooltip();
    });
    $(document).on('click','.autoinvoice',function() {
        $('.invoice-block').addClass('hidden');
        $('.autoinvoice-block').removeClass('hidden');
        $('#invoice-isautoinvoice').val('1');
        checkRemindTooltip();
    });
    $(document).on('click','.autoinvoice-cancel',function() {
        $('.invoice-block').removeClass('hidden');
        $('.autoinvoice-block').addClass('hidden');
        $('#invoice-isautoinvoice').val('0');
        checkRemindTooltip();
    });
    $(document).on('click','#autoinvoice-submit',function() {
        $.ajax({
            url: '$urlAdd',
            type: 'POST',
            data: $('.add-avtoschet').serialize(),
            success : function(data) {
                if(data == 1){
                    window.location.href = "$urlRedirect";
                } else{
                    $('#autoinvoice-flash').text(data);
                }
            },
        });
    });
    $(document).on('blur', '.product-count', function(){
        if ($(this).val() == 0 || $(this).val() == ''){
            $(this).val($(this).attr('data-value'));
            $(this).closest('tr').find('.price-with-nds').text(parseFloat($(this).attr('data-value') * $(this).closest('tr').find('.price-one-with-nds-input').val()).toFixed(2));
        } else {
           $(this).attr('data-value', $(this).val());
        }
    });

    $(document).on('blur','.product-title', function(){
        var name = $(this).val().replace(/\s+/g, '').length;
        if (name == 0) {
            $(this).closest('tr').remove();
            $('#from-new-add-row').show();
            INVOICE.recalculateInvoiceTable();
        }

    });
    $(document).on('click', '.product-title-clear', function(){
        $(this).closest('td').find('.product-title').val('');
        $(this).closest('td').find('.product-title').focus();
    });

    // REFRESH_UNIFORMS
    $(document).on("click", ".refreshDatepicker", function () {
        refreshDatepicker();
    });

    $(document).on("pjax:complete", "#agreement-form-container", function() {
        refreshDatepicker();
        refreshUniform();
    });
    $(document).on("pjax:complete", "#pjax-product-grid", function() {
        refreshUniform();
    });
JS
    , $this::POS_READY);


// Autofill fields from ScanRecognizeDocument
if ('previewScan' == Yii::$app->request->get('mode')) {
    echo $this->render('@frontend/modules/documents/views/upload-manager/_autofill/autofill', [
        'model' => $model,
        'company' => $company,
        'ioType' => $ioType
    ]);
}

if ($hasProjectEstimate) {
    $this->registerJs(<<<JS
        $.post('/project/get-project-estimate-order', {estimate: $projectEstimateId, type: $ioType}, function (data) {
            let row = $('.product-row');
            if ('true' === data.success) {
                $('#from-new-add-row').hide();
                $('#add-one-more-position').hide();
                row.remove();
                INVOICE.addProductToTable(data.data);
                row.show();
            }
        }, 'json');
JS, View::POS_LOAD);
}