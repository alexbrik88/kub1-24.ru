<?php

use common\components\date\DateHelper;
use common\models\document\Autoinvoice;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $projectList array */
/* @var $industryList array */

$this->title = 'Шаблоны АвтоСчетов';
$changeModalTitle = 'Изменить для выбранных автосчетов';
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => Documents::IO_TYPE_OUT,
        ])
        ): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'create',
                    'type' => Documents::IO_TYPE_OUT,
                    'auto' => true,
                ], [
                    'class' => 'button-regular button-regular_red button-width ml-auto',
                ]) ?>
            <?php else : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'class' => 'button-regular button-regular_red button-width ml-auto action-is-limited',
                ]) ?>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-9">
                <div class="count-card count-card_green wrap mb-0">
                    Если вы ежемесячно выставляете счета за одни и те же услуги или товары и в назначении меняется только месяц и год, то настройте шаблон АвтоСчета.
                    Счета автоматически будут формироваться и отправляться вашим клиентам в день, который вы зададите.
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
                    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php else : ?>
                    <?= Html::a('АвтоСчета', ['index-auto'], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_autoinvoice']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Номер счета или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= $this->render('_template_table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]); ?>
</div>

<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input id="Allcheck" type="checkbox" name="count-list-table">
            <span class="checkbox-txt total-txt-foot">
                Выбрано:
                <strong class="pl-2 total-count">0</strong>
            </span>
        </div>
        <div class="column ml-auto">
            <?= Html::tag('div', Html::a(Icon::get('repeat', [
                'style' => 'font-size: 16px; color: #001424!important;'
            ]).'<span class="mr-2">Обновить</span>' . Icon::get('shevron'), '#', [
                'class' => 'button-regular button-hover-transparent button-regular-more space-between dropdown-toggle',
                'data-toggle' => 'dropdown',
                'title' => 'Обновить реквизиты клиента или компании в автосчетах',
            ]) . Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Реквизиты моей компании',
                        'url' => '#many-update-company-fields',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Реквизиты контрагентов',
                        'url' => '#many-update-contractor-fields',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'dropdown-menu-right form-filter-list list-clr',
                ],
            ]), [
                'class' => 'dropup',
            ]) ?>
        </div>
        <div class="column">
            <?= Html::tag('div', Html::a('<span>Изменить</span>' . Icon::get('shevron'), '#', [
                'class' => 'button-regular button-hover-transparent button-width button-regular-more space-between dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => [
                    [
                        'label' => 'Число месяца отправки',
                        'url' => '#many-update-day',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Окончание',
                        'url' => '#many-update-dateto',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Направление',
                        'url' => '#many-update-industry',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                    [
                        'label' => 'Проект',
                        'url' => '#many-update-project',
                        'linkOptions' => [
                            'data-toggle' => 'modal',
                        ],
                    ],
                ],
                'options' => [
                    'class' => 'dropdown-menu-right form-filter-list list-clr',
                ],
            ]), [
                'class' => 'dropup',
            ]) ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'many-update-company-fields',
    'title' => 'Вы уверены, что хотите обновить реквизиты своей компании у выбранных шаблонов?',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'class' => 'button-regular button-hover-transparent button-width-medium mr-1 confirm',
            'data-selector' => 'input.invoice-id-check',
            'data-url' => Url::to([
                'invoice/many-update-company-fields',
                'type' => Documents::IO_TYPE_OUT,
                'isAuto' => 1,
            ]),
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>

<?php Modal::begin([
    'id' => 'many-update-contractor-fields',
    'title' => 'Вы уверены, что хотите обновить реквизиты контрагента у выбранных шаблонов?',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="text-center">
        <?= Html::button('Да', [
            'class' => 'button-regular button-hover-transparent button-width-medium mr-1 confirm',
            'data-selector' => 'input.invoice-id-check',
            'data-url' => Url::to([
                'invoice/many-update-contractor-fields',
                'type' => Documents::IO_TYPE_OUT,
                'isAuto' => 1,
            ]),
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>

<?php Modal::begin([
    'id' => 'many-update-day',
    'title' => $changeModalTitle,
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="form-group">
        <label>Число месяца отправки</label>
        <div class="row">
            <div class="col-3">
                <?= Select2::widget([
                    'name' => 'value',
                    'data' => Autoinvoice::monthDays(),
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'minimumResultsForSearch' => -1
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-regular_red button-width mr-1 confirm',
            'data-selector' => 'input.invoice-id-check, #many-update-day select',
            'data-url' => Url::to([
                'autoinvoice/many-update-day',
            ]),
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>

<?php Modal::begin([
    'id' => 'many-update-dateto',
    'title' => $changeModalTitle,
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="form-group" style="position: relative;">
        <label>Дата окончания</label>
        <div class="row">
            <div class="col-3">
                <?= Html::textInput('value', date('d.m.Y'), [
                    'class' => 'form-control date-picker ico',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-regular_red button-width mr-1 confirm',
            'data-selector' => 'input.invoice-id-check, #many-update-dateto input',
            'data-url' => Url::to([
                'autoinvoice/many-update-dateto',
            ]),
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>

<?php if ($industryList) : ?>
<?php Modal::begin([
    'id' => 'many-update-industry',
    'title' => $changeModalTitle,
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="form-group">
        <label>Направление</label>
        <div class="row">
            <div class="col-6">
                <?= Select2::widget([
                    'name' => 'value',
                    'data' => $industryList,
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'minimumResultsForSearch' => -1
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-regular_red button-width mr-1 confirm',
            'data-selector' => 'input.invoice-id-check, #many-update-industry select',
            'data-url' => Url::to([
                'autoinvoice/many-update-industry',
            ]),
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>
<?php endif ?>

<?php if ($projectList) : ?>
<?php Modal::begin([
    'id' => 'many-update-project',
    'title' => $changeModalTitle,
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="form-group">
        <label>Проект</label>
        <div class="row">
            <div class="col-6">
                <?= Select2::widget([
                    'name' => 'value',
                    'data' => $projectList,
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'minimumResultsForSearch' => -1
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => 'button-regular button-regular_red button-width mr-1 confirm',
            'data-selector' => 'input.invoice-id-check, #many-update-project select',
            'data-url' => Url::to([
                'autoinvoice/many-update-project',
            ]),
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'button-regular button-hover-transparent button-width ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end() ?>
<?php endif ?>

<?php
$this->registerJs(<<<JS
    $(document).on('change', 'input[type=checkbox][name="ids[]"]', function(e) {
        let length = $('input[type=checkbox][name="ids[]"]:checked').length;
        $('#summary-container .total-count').text(length);
        $('#Allcheck').prop('checked', $('input[type=checkbox][name="ids[]"]:not(:checked)').length == 0).uniform('refresh');
        if (length > 0) {
            $('#summary-container').toggleClass('visible check-true', true);
        } else {
            $('#summary-container').toggleClass('visible check-true', false);
        }
    });
    $(document).on('click', 'button.confirm', function(e) {
        l = Ladda.create(this);
        l.start();
        $.ajax({
            type: 'post',
            url: $(this).data('url'),
            data: $($(this).data('selector')).serialize()
        })
    });
JS);
?>
