<?php
use common\models\address;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use common\components\widgets\AddressTypeahead;
use yii\helpers\Url;
use common\models\address\AddressHouseType;
use yii\helpers\ArrayHelper;
use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $checkingAccountant CheckingAccountant */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

$inputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigFIO = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$remoteUrl = Url::to(['/dictionary/address']);
?>


    <?= $form->field($model, 'inn', array_merge($inputConfig, [
        'options' => [
            'class' => 'legal form-group col-6',
        ],
    ]))->label('ИНН:')->textInput([
        'placeholder' => 'Автозаполнение по ИНН',
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
        ],
    ]); ?>

    <?= $form->field($model, 'egrip', $inputConfig)->textInput([
        'maxlength' => true,
    ]); ?>

    <div class="col-12">
        <?= \common\components\helpers\Html::label('ФИО Руководителя', null, [
            'class' => 'label',
        ]); ?>
    </div>
    <div class="col-12">
        <div class="row required">
            <div class="col-3">
                <?= $form->field($model, 'ip_lastname', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Фамилия',
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_firstname', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Имя',
                        'class' => 'form-control'
                    ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_patronymic', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Отчество',
                        'class' => 'form-control',
                        'readonly' => (boolean) $model->has_chief_patronymic,
                    ]); ?>
            </div>
            <div class="col-3 pt-2">
                <?= Html::activeCheckbox($model, 'has_chief_patronymic') ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'address_legal', array_merge($inputConfig, [
        'options' => [
            'class' => 'form-group col-12',
        ],
    ]))->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '9{6}, ~{1,245}',
        'definitions' => [
            '~' => [
                'validator' => '[ 0-9A-zА-я.,/-]',
                'cardinality' => 1,
            ],
        ],
        'options' => [
            'class' => 'form-control',
            'placeholder' => 'Индекс, Адрес',
        ],
    ]); ?>

<?php

/*
echo $form->field($model, 'phone', array_merge($inputConfig, [
    'options'=>['class'=>'form-group required']
]))
    ->widget(\yii\widgets\MaskedInput::className(), [

        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control field-width field-w inp_one_line_company',
            'placeholder' => '+7(XXX) XXX-XX-XX (Нужен для восстановления пароля)',
        ],
    ]);
*/
echo $form->field($checkingAccountant, 'rs', $inputConfig)
    ->textInput([
        'maxlength' => true,
        'class' => 'form-control company_rs',
    ]);
echo $form->field($checkingAccountant, 'bik', $inputConfig)
    ->widget(\common\components\widgets\BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
            '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
            '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
        ],
        'options' => [
            'placeholder' => 'Автозаполнение по БИК',
            'class' => 'form-control company_bik',
            'autocomplete' => 'off'
        ],
    ]);
echo $form->field($checkingAccountant, 'bank_name', $inputConfig)
    ->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]);
echo $form->field($checkingAccountant, 'ks', $inputConfig)->textInput([
    'maxlength' => true,
    'readonly' => true,
]);
echo $form->field($checkingAccountant, 'bank_city', $inputConfig)
    ->textInput([
        'maxlength' => true,
        'readonly' => true,
]);
echo $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
    'options' => [
        'class' => 'form-control',
        'placeholder' => '+7(XXX) XXX-XX-XX (Для восстановления пароля)',
    ],
]);
?>

<?php
$this->registerJs(<<<JS
$('#new-company-invoice-form #company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-inn').val(suggestion.data.inn);

            var nameFullArray = suggestion.data.name.full.split(' ');
            $('#new-company-invoice-form #company-ip_lastname').val(nameFullArray[0]);
            $('#new-company-invoice-form #company-ip_firstname').val(nameFullArray[1]);
            $('#new-company-invoice-form #company-ip_patronymic').val(nameFullArray[2]);

            $('#new-company-invoice-form #company-egrip').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);
        }
    });
JS
);