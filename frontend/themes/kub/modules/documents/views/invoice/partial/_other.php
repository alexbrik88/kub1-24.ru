<?php
use common\models\address;
use common\models\company\CompanyType;
use common\components\helpers\Html;
use common\components\widgets\AddressTypeahead;
use common\models\dictionary\address\AddressDictionary;
use common\components\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\widgets\BikTypeahead;
use common\models\company\CheckingAccountant;

/* @var $checkingAccountant CheckingAccountant */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

$companyTypes = CompanyType::getCompanyTypeVariants($model->company_type_id);

$inputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'autocomplete' => 'off'
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigFIO = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$remoteUrl = Url::to(['/dictionary/address']);

echo $form->field($model, 'inn', array_merge($inputConfig, [
    'options' => [
        'class' => 'legal form-group col-6',
    ],
]))->label('ИНН:')->textInput([
    'placeholder' => 'Автозаполнение по ИНН',
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
    ],
]);

echo $form->field($model, 'kpp', $inputConfig)->textInput([
    'maxlength' => true,
]);

echo $form->field($model, 'name_short')
    ->label()
    ->textInput([
        'placeholder' => 'Без ООО/ИП',
        'maxlength' => true,
        'disabled' => $model->company_type_id == CompanyType::TYPE_IP,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
        ],
    ]);

echo $form->field($model, 'company_type_id', [
    'options' => [
        'id' => 'contractor_company_type',
        'class' => 'form-group col-6',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
])->label('Форма')->widget(\kartik\select2\Select2::class, [
    'data' => \yii\helpers\ArrayHelper::map($companyTypes, 'id', 'name_short'),
    'disabled' => count($companyTypes) === 1,
]);

?>

    <div class="col-12">
        <?= Html::label('ФИО Руководителя', null, [
            'class' => 'label',
        ]); ?>
    </div>
    <div class="col-12">
        <div class="row required">
            <div class="col-3">
                <?= $form->field($model, 'chief_lastname', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Фамилия',
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'chief_firstname', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Имя',
                        'class' => 'form-control'
                    ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'chief_patronymic', $inputConfigFIO)
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Отчество',
                        'class' => 'form-control',
                        'readonly' => (boolean) $model->has_chief_patronymic,
                    ]); ?>
            </div>
            <div class="col-3 pt-2">
                <?= Html::activeCheckbox($model, 'has_chief_patronymic') ?>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
    <?= $form->field($checkingAccountant, 'rs', $inputConfig)->textInput([
        'maxlength' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'bik', $inputConfig)->widget(BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
            '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
            '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
        ],
        'options' => [
            'placeholder' => 'Автозаполнение по БИК',
        ],
    ]); ?>
    <?= $form->field($checkingAccountant, 'bank_name', $inputConfig)->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'ks', $inputConfig)->textInput([
        'maxlength' => true,
        'readonly' => true,
    ]); ?>
    <?= $form->field($checkingAccountant, 'bank_city', $inputConfig)->textInput([
    'maxlength' => true,
    'readonly' => true,
    ]); ?>

    <div class="form-group col-6"></div>

    <?= $form->field($model, 'address_legal', array_merge($inputConfig, [
        'options' => [
            'class' => 'form-group col-12',
        ],
    ]))->widget(\yii\widgets\MaskedInput::class, [
        'mask' => '9{6}, ~{1,245}',
        'definitions' => [
            '~' => [
                'validator' => '[ 0-9A-zА-я.,/-]',
                'cardinality' => 1,
            ],
        ],
        'options' => [
            'class' => 'form-control',
            'placeholder' => 'Индекс, Адрес',
        ],
    ]); ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+7(XXX) XXX-XX-XX (Для восстановления пароля)',
        ],
    ]); ?>
    <?= $form->field($model, 'ogrn', $inputConfig)->textInput([
        'maxlength' => true,
    ]); ?>

<?php
$this->registerJs(<<<JS
    $('[id="company-inn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-name_short').val(suggestion.data.name.short);
            $('#company-inn').val(suggestion.data.inn);
            $('#company-kpp').val(suggestion.data.kpp);
            $('#company-ogrn').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);

            var nameFull = suggestion.data.management.name;
                var lastName = '';
                var firstName = '';
                var patronymic = '';
                var endLastNamePlace = nameFull.indexOf(' ');
                for (var i = 0; i < endLastNamePlace; i++) {
                    lastName += nameFull[i];
                }
                var endNamePlace = nameFull.indexOf(' ', endLastNamePlace+1);
                for (var i = endLastNamePlace+1; i < endNamePlace; i++) {
                    firstName += nameFull[i];
                }
                for (var i = endNamePlace+1; i < nameFull.length; i++) {
                    patronymic += nameFull[i];
                }
                $('#company-chief_lastname').val(lastName);
                $('#company-chief_firstname').val(firstName);
                $('#company-chief_patronymic').val(patronymic);
        }
    });
JS
);