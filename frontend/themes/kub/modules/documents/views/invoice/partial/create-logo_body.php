<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
    <div class="company-update">
        <div class="company-form">
            <div class="form-body form-horizontal">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'new-company-logo-invoice-form',
                    'options' => [
                        'enctype' => 'multipart/form-data',
                    ],
                ]);
                echo $this->render('_logo', [
                    'company' => $model,
                    'form' => $form,
                ]);
                ?>

                <div class="form-actions">
                    <div class="row action-buttons">
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            <?= Html::submitButton('Сохранить', [
                                'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs',
                            ]); ?>
                            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                'class' => 'btn darkblue text-white widthe-100 hidden-lg',
                                'title' => 'Сохранить',
                            ]); ?>
                        </div>
                        <div
                            class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
                        <div
                            class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
                        <div
                            class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
                        <div
                            class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
                        <div
                            class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
                        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            <?php $cancelUrl = isset($cancelUrl) ? $cancelUrl : 'javascript:;'; ?>

                            <?=
                            Html::a('Отменить', $cancelUrl, [
                                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                            ]);
                            ?>
                            <?=
                            Html::a('<i class="fa fa-reply fa-2x"></i>', $cancelUrl, [
                                'class' => 'btn darkblue widthe-100 hidden-lg',
                                'title' => 'Отменить',
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>