<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this \yii\web\View */
/* @var $model \common\models\document\ForeignCurrencyInvoice */
/* @var $useContractor string */

Modal::begin([
    'options' => [
        'id' => 'paid',
        'class' => 'fade',
    ],
    'closeButton' => false,
    'toggleButton' => false,
]);

echo $this->render('_add_flow_form', [
    'model' => $model,
    'useContractor' => $useContractor,
]);

Modal::end();
