<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2017
 * Time: 6:26
 */

use yii\bootstrap4\Modal;
use devgroup\dropzone\DropZone;
use yii\helpers\Url;
use common\models\Company;

/* @var $company Company */

\devgroup\dropzone\DropZoneAsset::register($this)->jsOptions = ['position' => \yii\web\View::POS_HEAD];

$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';

Modal::begin([
    'id' => 'modal-documents-upload',
    'header' => '<h4>Загрузить документы</h4>',
    'toggleButton' => false,
]); ?>
    <div id="uploaded_documents_img" class="dropzone"
         style="position: relative; padding-bottom: 30px;">
         <span class="col-md-12 progress-text"
               style="bottom: 26px;position: absolute;padding-left: 0;left: 10px;display: none;">
                        Идет распознавание документов. Распознано <span
                 class="count-uploaded-files">0</span> из <span
                 class="count-added-files"></span>
         </span>

        <div id="progress" class="progress progress-striped active"
             style="position: absolute; bottom: 5px; left: 10px; right: 10px; margin: 0; background-color: #fff;display: none;">
            <div id="progress_bar" class="progress-bar"
                 role="progressbar" style="width: 0;"></div>
        </div>
    </div>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var $progressLength = 0;
        var $sentFilesCount = 0;
        var $oneFilePercent = 0;
        var dropzone_uploaded_documents = new Dropzone("#uploaded_documents_img", {
            'url': '<?= Url::toRoute(['/documents/upload-documents/upload', 'id' => $company->id]); ?>',
            'dictDefaultMessage': 'Перетащите файлы изображений документов сюда или <br>' +
            'кликните тут, чтобы выбрать его с вашего компьютера. <br> Допустимый формат: jpg, jpeg, png.',
            'uploadMultiple': true,
            'parallelUploads': 1,
            'acceptedFiles': "image/jpeg,image/png,application/pdf",
            'params': {'<?= \Yii::$app->request->csrfParam; ?>': '<?= \Yii::$app->request->getCsrfToken(); ?>'},
            'previewTemplate': "<?= $DZpreviewTemplate; ?>"
        });
        dropzone_uploaded_documents.on('totaluploadprogress', function (progress) {
            var $progressBar = $('#progress_bar');
            $progressBar.closest('#progress').show();
            $('.progress-text').show();
        });
        dropzone_uploaded_documents.on('addedfiles', function (files) {
            $('#progress_bar').attr('files-count', files.length);
            $('.count-added-files').text(files.length);
            $oneFilePercent = 1 / files.length * 100;
        });
        dropzone_uploaded_documents.on('queuecomplete', function () {
            $('.count-uploaded-files').text(+$('#progress_bar').attr('files-count'));
            location.href = location.href;
        });
        dropzone_uploaded_documents.on('successmultiple', function () {
            var $progressBar = $('#progress_bar');
            $sentFilesCount++;
            $progressBar.width($sentFilesCount * $oneFilePercent + '%');
            $('.count-uploaded-files').text($sentFilesCount);
        });
    </script>
<?php Modal::end(); ?>