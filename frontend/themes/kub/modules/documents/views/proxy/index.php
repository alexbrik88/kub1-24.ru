<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\models\document\Invoice;
use common\models\document\Proxy;
use kartik\select2\Select2;
use \yii\bootstrap4\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\TableViewWidget;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\ProxySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */

frontend\assets\ProxyAsset::register($this);

$this->title = $message->get(Message::TITLE_PLURAL);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = Invoice::find()->joinWith('proxies', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет доверенностей. Измените период, чтобы увидеть имеющиеся доверенности.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одной доверенности.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);

$dropItems = [];
if ($ioType == Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems = [
        [
            'label' => 'Скачать в Excel',
            'url' => ['generate-xls'],
            'linkOptions' => [
                'class' => 'get-xls-link',
            ],
        ],
    ];
}
?>
<?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>
    <?php
    Modal::begin([
        'clientOptions' => ['show' => true],
        'closeButton' => false,
    ]); ?>
    <h4 class="modal-title text-center mb-4">Перед тем как подготовить доверенность, нужно создать Счет.</h4>
    <div class="text-center">
        <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
            'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
        ]); ?>
    </div>
    <?php
    Modal::end();

    ?>
    <div class="alert-success alert fade in">
        <button id="contractor_alert_close" type="button" class="close"
                data-dismiss="alert" aria-hidden="true">×
        </button>
        Перед тем как подготовить доверенность,
        нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
        .
    </div>
<?php endif; ?>

<div class="stop-zone-for-fixed-elems page-proxy">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => $ioType,
        ])): ?>
            <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'class' => 'button-regular button-regular_red button-width ml-auto add-proxy',
                ]) ?>
            <?php else : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'class' => 'button-regular button-regular_red button-width ml-auto action-is-limited',
                ]) ?>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-9" style="font-size: 18px;">
                Добавьте доверенность к входящему счету, чтобы ваши сотрудники или курьеры смогли забрать товар у продавца
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер платежного поручения или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list ' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                ],
                'format' => 'raw',
                'value' => function (Proxy $model) {
                    return Html::checkbox('Proxy[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                    ]);

                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => 'Номер',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $data,
                    ])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber;
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '70px',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'invoice.total_amount_with_nds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],

            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '30%',
                    'class' => 'dropdown-filter',
                ],
                'contentOptions' => [
                    'style' => 'max-width:300px'
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, Proxy::tableName(), true, false, false),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
                's2width' => '300px'
            ],

            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'class' => DropDownDataColumn::className(),
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    return $data->status->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],

            [
                'attribute' => 'invoice_document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (Proxy $data) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])
                    ) {
                        if ($data->invoice->file !== null) {
                            $invoiceFileLinkClass = null;
                            $tooltipId = null;
                            $contentPreview = null;
                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                if ($thumb) {
                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                    $contentPreview .= Html::endTag('span');
                                    $contentPreview .= Html::endTag('div');
                                }
                            }

                            return Html::a($data->invoice->fullNumber,
                                    ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id])
                                . Html::a('<span class="pull-right icon icon-paper-clip"></span>',
                                    ['/documents/invoice/file-get', 'type' => $data->type, 'id' => $data->invoice->id, 'file-id' => $data->invoice->file->id,],
                                    [
                                        'class' => $invoiceFileLinkClass,
                                        'target' => '_blank',
                                        'data-tooltip-content' => '#' . $tooltipId,
                                        'style' => 'float: right',
                                    ]) . $contentPreview;
                        } else {
                            return Html::a($data->invoice->fullNumber,
                                ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id]);
                        }
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],
            [
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'proxy_person_id',
                'label' => 'Выдана, ФИО',
                'encodeLabel' => false,
                'value' => function ($data) {
                    $employee = \common\models\employee\Employee::findOne([
                        'id' => $data['proxy_person_id']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getProxyPersonsArray(),
                's2width' => '200px'
            ],
        ],
    ]); ?>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'button-clr button-regular button-width button-hover-transparent dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'class' => 'dropdown-menu-right form-filter-list list-clr'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('_viewPartials/_invoices_modal', [
    'company' => $company,
]) ?>

<?php Modal::begin([
    'id' => 'many-delete',
    'closeButton' => false,
]); ?>

<?= \yii\bootstrap4\Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
    'class' => 'modal-close close',
    'data-dismiss' => 'modal',
    'aria-label' => 'Close',
]) ?>

<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите удалить выбранные доверенности?
</h4>
<div class="text-center">
    <?= Html::button('ДА', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
        'data-url' => Url::to(['many-delete', 'type' => $ioType]),
    ]); ?>
    <?= Html::button('НЕТ', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<div style="display: none">
    <?php /* preload styles */ ?>
    <?= Select2::widget([
        'name' => 'empty',
        'data' => []
    ]); ?>
</div>
