<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use common\models\document\OrderProxy;
/* @var $this yii\web\View */
/* @var $model common\models\document\Proxy */
/* @var $message Message */
/* @var $ioType integer */
/* @var $contractorId integer|null */

//$plus = OrderProxy::getAvailable($model->id) ? 1 : 0;

frontend\assets\ProxyAsset::register($this);

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = \common\models\document\OrderProxy::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = ['index', 'type' => $model->type,];
$precision = $model->invoice->price_precision;

?>

<?= $this->render('_viewPartials/_style') ?>

<div class="page-content-in">
    <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-proxy',
        'class' => 'form-horizontal',
        'enableClientValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => true,
        'enctype' => 'multipart/form-data',
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group col-6'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => '',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ],
    ]); ?>

    <?php echo $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
        'model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
        'ioType' => $ioType
    ]);
    ?>

    <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
        'model' => $model,
        'precision' => $precision,
    ]); ?>

    <?= $this->render('_viewPartials/_form_buttons', [
        'model' => $model,
        'ioType' => $ioType
    ]); ?>
    <?php echo Html::endForm(); ?>
</div>

<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
<?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить доверенность?',
]); ?>

<?php
Modal::begin([
    'id' => 'proxy-modal-container',
]);

Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => '5000'
]);

Pjax::end();

Modal::end();

$this->registerJs('

var currentProxyPersonId = "'.(int)$model->proxy_person_id.'";

$(document).on("change", "#proxy-proxy_person_id", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-employee") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        'create-employee',
        'container' => 'proxy-select-container',
    ]) . '", container: "#proxy-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#proxy-modal-header").html($("[data-header]").data("header"));
            // $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            $(".radio-inline").click(function () {
                var radio = $(this).find("input:radio");

                if (radio.length > 0) {
                    radio = radio[0];
                    if (radio.name == "EmployeeCompany[passport_isRf]") {
                        if (radio.value == 0) {
                            $(".field-physical-passport-country").removeClass("hide").find("input").val("");
                            Passport_isRf.hideInputsMask();
                        }
                        else {
                            $(".field-physical-passport-country").addClass("hide");
                            Passport_isRf.showInputsMask();
                        }
                    }
                }
            });



        });
        $("#proxy-modal-container").modal("show");
        $("#proxy-proxy_person_id").val(currentProxyPersonId).trigger("change");
    }

    var Passport_isRf = (function () {
        var inputs = {
            "employeecompany-passport_series": {"mask": "9{2} 9{2}"},
            "employeecompany-passport_department": {"mask": "9{3}-9{3}"},
            "employeecompany-passport_number": {"mask": "9{6}"}
        };
        var placeholders = {
            "employeecompany-passport_series": "XX XX",
            "employeecompany-passport_department": "XXX-XXX",
            "employeecompany-passport_number": "XXXXXX"
        };
        var inputsFonNotRf = {
            "employeecompany-passport_series": {"mask": "[9|a| ]{1,25}"},
            "employeecompany-passport_department": {"mask": "[9|a]{1,255}"},
            "employeecompany-passport_number": {"mask": "[9|a]{1,25}"}
        };

        var init = function(){

        };

        var hideInputsMask = function () {
            $.each(inputsFonNotRf, function (o, val) {
                if ($("#" + o).inputmask) {
                    $("#" + o).inputmask(val);
                    $("#" + o).attr("placeholder","");
                }
            });
        };

        var showInputsMask = function () {
            $.each(inputs, function (o, val) {
                var inp = document.getElementById(o);
                if (inp) {
                    $(inp).inputmask(val);
                    $(inp).attr("placeholder",placeholders[o]);
                }
            });
        };

        return {hideInputsMask: hideInputsMask, showInputsMask: showInputsMask, init: init}
    })();

});
$("#proxy-pjax-container").on("pjax:complete", function() {
    if (window.ProxyValue) {
        $("#proxy-proxy_person_id").val(window.ProxyValue).trigger("change");
    }
})
');
?>
