<?php

use frontend\models\Documents;
use frontend\rbac\permissions\document\Document;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);

/** @var \common\models\document\Proxy $model */
?>


<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate): ?>
                <?=Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php echo Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>',
            ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'button-regular button-hover-transparent w-full',
            'target' => '_blank',
        ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>',
                ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()], [
                'class' => 'button-regular button-hover-transparent w-full',
                'target' => '_blank'
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
                <button type="button" class="button-regular button-hover-transparent w-full" data-toggle="modal" href="#delete-confirm">
                    <?= $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>' ?>
                </button>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($canUpdate): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => null,
        'header' => 'Отправить',
    ]); ?>
<?php endif; ?>