<?php
use common\components\date\DateHelper;
use common\models\document\Proxy;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use common\models\product\Product;
use \yii\helpers\ArrayHelper;
use \common\models\employee\Employee;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var \yii\web\View $this */
/* @var Proxy $model */
/* @var $message Message */
/* @var string $dateFormatted */

$productionType = explode(', ', $model->invoice->production_type);

$employeeArray = ['add-modal-employee' => \frontend\themes\kub\helpers\Icon::PLUS . ' Добавить сотрудника ', '' => '---'] + ArrayHelper::map(
        \common\models\EmployeeCompany::find()
            ->where(['company_id' => $model->invoice->company->id])
            ->indexBy('lastname')
            ->all(), 'employee_id', function ($model) {
        return $model->getShortFio(true) ?: '(не задан)';
    });

$cancelUrl = Url::to(['view',
    'type' => $ioType,
    'id' => $model->id]);

?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Доверенность №
                        </span>
                    </span>
                </div>
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date_input', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-6">
            <div class="form-filter">
                <label class="label" for="input3">Действительна по<span class="important">*</span></label>
            </div>
            <div class="row align-items-center">
                <div class="form-group col-4">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'limit_date', [
                            'class' => 'form-control date-picker',
                            'value' => DateHelper::format($model->limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-6">
            <div class="form-filter">
                <label class="label" for="input3">Выдана<span class="important">*</span></label>
            </div>
            <?php Pjax::begin([
                'id' => 'proxy-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>
            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'proxy_person_id',
                'data' => $employeeArray,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                ]
            ]); ?>
            <?php Pjax::end() ?>
        </div>
        <br>
        <div class="col-12">
            <div class="row">
                <div class="form-group col-6">
                    <div class="form-filter">
                        <label class="label" for="input3">Поставщик<span class="important">*</span></label>
                    </div>
                    <?= Html::textInput('contractor', $model->invoice->contractor_name_short, [
                        'class' => 'form-control',
                        'disabled' => true
                    ]); ?>
                </div>
                <div class="form-group col-4">
                    <div class="form-filter">
                        <label class="label" for="input3">Счет № и Дата</label>
                    </div>
                    <?= Html::textInput('invoice_number_date', '№' .
                        $model->invoice->getFullNumber() .
                        ' от ' .
                        DateHelper::format($model->limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE), [
                            'class' => 'form-control',
                            'disabled' => true
                        ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>