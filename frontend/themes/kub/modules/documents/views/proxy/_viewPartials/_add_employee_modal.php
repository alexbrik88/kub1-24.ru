<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

Modal::begin([
    'id' => 'proxy-modal-container',
]);
?>

<h4 id="proxy-modal-header" class="modal-title">Добавить сотрудника</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php
Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => '5000'
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#proxy-proxy_person_id", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-employee") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/documents/proxy/create-employee',
        'container' => 'proxy-select-container',
    ]) . '", container: "#proxy-form-container", push: false});

        $(document).on("pjax:success", "#proxy-form-container", function() {
            $("#proxy-modal-header").html($("[data-header]").data("header"));
            // $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            $(".radio-inline").click(function () {
                var radio = $(this).find("input:radio");

                if (radio.length > 0) {
                    radio = radio[0];
                    if (radio.name == "EmployeeCompany[passport_isRf]") {
                        if (radio.value == 0) {
                            $(".field-physical-passport-country").removeClass("hide");
                            Passport_isRf.hideInputsMask();
                        }
                        else {
                            $(".field-physical-passport-country").addClass("hide");
                            Passport_isRf.showInputsMask();
                        }
                    }
                }
            });

        });
        $("#proxy-modal-container").modal("show");
        $("#proxy-proxy_person_id").val("").trigger("change");
    }
});

');
?>