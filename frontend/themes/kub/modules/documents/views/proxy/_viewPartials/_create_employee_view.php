<?php

use common\models\file\widgets\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */
/* @var $isNewRecord boolean */

Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
]) ?>
<div class="proxy-form" data-header="Сотрудник">

    Сотрудник <strong><?= $model->getFio(true) ?></strong>сохранен.

</div>
<script type="text/javascript">
    window.ProxyValue = "<?= $model->employee_id ?>";

    if ($('#proxy-pjax-container').length) {
        var options = {
            container: "#proxy-pjax-container",
            push: false,
        };
        $.pjax(options);
    } else {
        var newOption = new Option('<?= htmlspecialchars($model->getShortFio()) ?>', <?= $model->employee_id ?>, true, true);
        // Append it to the select
        $('#proxy-proxy_person_id').append(newOption).trigger('change');
    }

    $("#proxy-modal-container").modal("hide");
    $(document).ready(function () {
        $(".page-content").prepend("<div id='w2-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Сотрудник добавлен</div>");
    });
</script>
<?php Pjax::end() ?>
