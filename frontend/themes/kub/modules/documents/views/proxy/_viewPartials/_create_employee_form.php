<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use common\components\date\DateHelper;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */

$header = 'Добавить сотрудника';

if ($model->passport_isRf === null) {
    $model->passport_isRf = 1;
    $model->passport_country = 'Россия';
    $model->position = 'Курьер';
}

$checkboxConfig = [
    'options' => [
        'class' => 'form-group col-6',
        'style' => 'padding-top:35px'
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

Pjax::begin([
    'id' => 'proxy-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style type="text/css">
.proxy-form .form-control {width: 100%;}
form .form-group p.help-block {padding:0}
</style>
<div class="proxy-form" data-header="<?= $header ?>">

    <?php $form = ActiveForm::begin([
        'id' => 'proxy-add-employee-form',
        'enableClientValidation' => false,
        'action' => ['create-employee'],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group col-6',
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => '',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
            'data-employee' => $model->isNewRecord ? null : [
                'id' => $model->id,
                'name' => $model->getFio(true),
            ],
        ],
    ]); ?>

    <div class="row">

        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'patronymic')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'has_no_patronymic', $checkboxConfig)
            ->label('Нет отчества')->checkbox([], true); ?>

        <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    </div>

    <?= $this->render('_form_passport', [
            'model' => $model,
            'form'  => $form ]) ?>


    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'style' => 'width: 130px!important;',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php Pjax::end() ?>
