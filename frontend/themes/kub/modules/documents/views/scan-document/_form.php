<?php

use frontend\models\Documents;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\documents\models\ScanDocument */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="scan-document-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'options' => [
            'id' => 'scan-document-form',
            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'col-md-4 control-label',
            ],
        ],
    ])); ?>

        <?= $form->field($model, 'name', ['wrapperOptions' => ['class' => 'col-md-8']])
            ->textInput(['class' => 'form-control width100']); ?>

        <?= $form->field($model, 'description', ['wrapperOptions' => ['class' => 'col-md-8']])
            ->textarea(['class' => 'form-control width100']); ?>

        <?= $form->field($model, 'contractor_id', [
            'wrapperOptions' => ['class' => 'col-md-8'],
        ])->widget('kartik\select2\Select2', [
            'data' => Yii::$app->user->identity->company->sortedContractorList(),
            'options' => [
                'placeholder' => '',
                'disabled' => (boolean) $model->owner_id,
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

        <?= $form->field($model, 'document_type_id', [
            'wrapperOptions' => ['class' => 'col-md-8'],
        ])->widget('kartik\select2\Select2', [
            'data' => Documents::mapIdName(),
            'options' => [
                'placeholder' => '',
                'disabled' => (boolean) $model->owner_id,
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]); ?>

        <?php if ($model->isNewRecord) : ?>
            <div class="row">
                <?= $form->field($model, 'upload', [
                    'template' => "{input}\n{hint}\n{error}",
                    'options' => ['class' => 'col-sm-8'],
                ])->fileInput() ?>
                <div class="col-md-4">
                    <?= Html::submitButton('<i class="upload-button glyphicon glyphicon-download-alt"></i> Загрузить', [
                        'class' => 'btn darkblue pull-right text-white',
                    ]) ?>
                </div>
            </div>
        <?php else : ?>
            <div class="row">
                <?= $form->field($model, 'upload', [
                    'template' => "{input}\n{hint}\n{error}",
                    'options' => ['class' => 'col-sm-12'],
                ])->fileInput(['data' => ['filename' => $model->file? $model->file->filename_full: '']]) ?>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-6">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'btn darkblue pull-left text-white',
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= Html::button('Отменить', [
                            'class' => 'btn darkblue pull-right text-white',
                            'data-dismiss' => 'modal',
                        ]) ?>
                    </div>
                </div>
            </div>
        <?php endif ?>

    <?php $form->end(); ?>
</div>

<?php
$js = <<<JS
$(function () {
    $('#scandocument-upload').styler({
        'fileBrowse' : '<span class="glyphicon glyphicon-plus-sign"></span> Выбрать файл',
        'onFormStyled' : function() {
            $('#scandocument-upload-styler .jq-file__browse')
                .prependTo('#scandocument-upload-styler')
                .css({'float' : 'none', 'display' : 'inline-block'});
            $('#scandocument-upload-styler .jq-file__name')
                .css({'padding' : 0});
        }
    });
    if ($('#scandocument-upload').data('filename')) {
        $(".jq-file__name").text($('#scandocument-upload').data('filename')).show();
    }
});
JS;
$this->registerJs($js);
?>
