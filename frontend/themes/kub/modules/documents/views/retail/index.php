<?php

use common\components\grid\GridView;
use common\models\evotor\ReceiptItem;
use faryshta\widgets\JqueryTagsInput;
use frontend\assets\IntegrationAsset;
use frontend\assets\InterfaceCustomAsset;
use frontend\modules\documents\models\RetailDataProvider;
use frontend\modules\documents\models\RetailSendForm;
use frontend\modules\documents\widgets\SummarySelectWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var                    $this              View
 * @var string             $date              Диапазон дат
 * @var RetailDataProvider $productDataProvider
 * @var int                $totalCount        Общее количество позиций
 * @var float              $totalAmount       Общая сумма по всем позициям
 * @var float              $totalTax          Общая сумма налога
 * @var float              $totalCard         Общая сумма оплат банковской картой
 * @var float              $totalCash         Общая сумма оплат наличными
 * @var int                $totalPaymentCount Общее количество платежей
 * @var int                $offset
 * @var RetailSendForm     $retailSendForm
 */
InterfaceCustomAsset::register($this);
IntegrationAsset::register($this);
?>

<?= $this->render('_partial/navbar') ?>

<div class="row align-items-center my-3 mx-0">
    <h5 class="col-6">
        Список отчётов
    </h5>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'method' => 'GET',
        'options' => [
            'class' => 'col-6 d-flex flex-nowrap align-items-center',
        ],
    ]); ?>
        <div class="flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'keyword', [
                'type' => 'search',
                'placeholder' => 'Найти по артикулу или названию',
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
    <?php $form->end(); ?>
</div>
<div class="wrap">
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $productDataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'Нет данных, удовлетворяющих условию',
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $productDataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center pad0',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-center pad0-l pad0-r',
                ],
                'format' => 'raw',
                'value' => function (ReceiptItem $model) {
                    return Html::checkbox('Retail[' . $model->quantity . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                    ]);
                },
            ],
            [
                'label' => '№',
                'value' => function () use (&$offset) {
                    return ++$offset;
                },
            ],
            'product.article_number',
            [
                'attribute' => 'name',
                'value' => function (ReceiptItem $model) {
                    return Html::a($model->name, Url::to(['/documents/retail/sell', 'uuid' => $model->uuid]));
                },
                'format' => 'raw',
            ],
            'quantity',
            'measure_name',
            'price',
            [
                'label' => 'Цена со скидкой',
                'value' => function (ReceiptItem $model) {
                    return $model->price - $model->discount;
                },
            ],
            'sum_price',
            [
                'label' => 'Сумма со скидкой',
                'value' => function (ReceiptItem $model) {
                    return $model->sum_price - ($model->discount * $model->quantity);
                },
                'contentOptions' => [
                    'class' => 'price',
                ],
            ],
        ],
    ]) ?>
    <p class="text-right">Всего отпущено <strong><?= $productDataProvider->totalCount ?></strong> наименований на сумму <strong><?= number_format($totalAmount, 0, '.', ' ') ?></strong> руб.
    </p>
    <?php if ($totalTax > 0) { ?>
        <p class="text-right">В том числе НДС: <?= number_format($totalTax, 0, '.', ' ') ?> руб.</p>
    <?php } ?>
</div>

<div class="wrap">
    <h5>
        Платежи <?= $date ?>
    </h5>

    <table class="table table-style table-count-list overal-result-table table-hover compact-disallow" aria-describedby="datatable_ajax_info" role="grid">
        <thead>
            <tr id="w2-filters" class="heading">
                <th>№</th>
                <th>Вид оплаты</th>
                <th>Сумма</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Оплата платёжной картой</td>
            <td><?= number_format($totalCard, 0, '.', ' ') ?></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Наличные</td>
            <td><?= number_format($totalCash, 0, '.', ' ') ?></td>
        </tr>
        </tbody>
    </table>

    <p class="text-right">Всего <strong><?= $totalPaymentCount ?></strong> платежей.</p>
</div>

<div id="sendPanel" style="display:none;">
    <span class="header">Отправить отчёт</span>
    <?php $form = ActiveForm::begin([
        'action' => Url::to('/documents/retail/send'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'options' => ['name' => 'retailSend'],
    ]); ?>
    <input type="hidden" name="ids" value=""/>
    <?= $form->errorSummary($retailSendForm); ?>

    <div class="form-block" data-tooltip-content="#send-from-tooltip">
        <div class="email-label">От кого:</div>
        <?= $form->field($retailSendForm, 'from')->textInput([
            'class' => 'form-control input-sm',
            'id' => 'message-from',
            'aria-required' => true,
            'readonly' => true,
        ])->label(false) ?>
    </div>

    <div class="block-to-email">
        <div class="email-label">Кому:</div>
        <div class="dropdown-email">
            <?= $form->field($retailSendForm, 'to', [
                'options' => [
                    'class' => 'form-group form-md-line-input form-md-floating-label field-message-to',
                    'style' => 'width: 96%;',
                ],
            ])->widget(JqueryTagsInput::className(), [
                'clientOptions' => [
                    'width' => '100%',
                    'defaultText' => '',
                    'removeWithBackspace' => true,
                    'onAddTag' => new JsExpression("function (val) {
                    var tagInput = $('#emailletterform-to');
                    var reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                    if (reg.test(val) == false) {
                        tagInput.removeTag(val);
                    } else {
                        $('.who-send-container .container-who-send-label').each(function () {
                            var sendToCheckBox = $(this).find('.checker input');
                            if (sendToCheckBox.data('value') == val) {
                                if (!sendToCheckBox.is(':checked')) {
                                    sendToCheckBox.click();
                                }
                            }
                        });
                    }
                    if ($('#emailletterform-to').val() == '') {
                        $('#emailletterform-to_tagsinput #emailletterform-to_tag')
                        .attr('placeholder', 'Укажите e-mail')
                        .addClass('visible');
                    } else {
                        $('#emailletterform-to_tagsinput #emailletterform-to_tag')
                        .removeAttr('placeholder')
                        .removeClass('visible');
                    }
                }"),
                    'onRemoveTag' => new JsExpression("function (val) {
                    $('.who-send-container .container-who-send-label').each(function () {
                        sendToCheckBox = $(this).find('.checker input');
                        if (sendToCheckBox.data('value') == val) {
                            if (sendToCheckBox.is(':checked')) {
                                sendToCheckBox.click();
                            }
                        }
                    });
                    if ($('#emailletterform-to').val() == '') {
                        $('#emailletterform-to_tagsinput #emailletterform-to_tag')
                        .attr('placeholder', 'Укажите e-mail')
                        .addClass('visible');
                    } else {
                        $('#emailletterform-to_tagsinputemailletterform-to_tagsinput #emailletterform-to_tag')
                        .removeAttr('placeholder')
                        .removeClass('visible');
                    }
                }"),
                ],
            ])->label(false); ?>
        </div>
    </div>

    <div class="form-block block-subject-email">
        <div class="email-label">Тема:</div>
        <?= $form->field($retailSendForm, 'subject', [
            'options' => [
                'class' => 'form-group form-md-line-input form-md-floating-label field-message-subject',
                'style' => 'width: 90%;',
            ],
        ])->textInput([
            'class' => 'form-control input-sm edited',
            'id' => 'message-subject',
            'aria-required' => true,
            'placeholder' => 'Тема письма',

        ])->label(false); ?>
    </div>

    <div class="email_text_input">
        <?= $form->field($retailSendForm, 'body')->textarea([
            'rows' => 7,
            'style' => 'padding: 10px 0; width: 100%; border: 0;border-bottom:1px solid #e5e5e5;overflow-y: auto;',
        ])->label(false) ?>
    </div>

    <div class="row email-uploaded-files" style="margin-top:25px;">
        <div class="one-file col-md-6" data-id="12">
            <img src="/img/email/emailPdf.png" class="preview-img" alt="">
            <span class="file-name" title="report.pdf">report.pdf</span>
        </div>
    </div>

    <div class="form-actions" style="z-index: 999;">
        <div class="row action-buttons">
            <div class="col-sm-5 col-xs-5" style="width: 14%;">
                <div class="btn btn-group dropup">
                    <?= Html::submitButton('<span class="ladda-label">Отправить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue text-white mt-ladda-btn ladda-button btn-danger',
                        'data-style' => 'expand-right',
                        'data-action' => 'send',
                    ]); ?>
                </div>

                <div>
                </div>
            </div>
            <div class="col-sm-2 col-xs-2" style="width: 12.3%;"></div>
            <div class="col-sm-5 col-xs-5" style="width: 9.9%;">
                <?= Html::button('Отменить', ['class' => 'btn darkblue text-white pull-right widthe-100 side-panel-close-button',]); ?>
            </div>
        </div>
    </div>
    <?php $form->end(); ?>
</div>

<script>
    function retailSendEmail(form) {
        var idArray = $(".joint-operation-checkbox:checked").map(function () {
            return $(this).closest("tr").data("key");
        }).get();
        form.ids.value = idArray.join();
        return rightPanel.fromContainer(document.getElementById("sendPanel"));
    }
</script>

<?= SummarySelectWidget::widget([
    'buttons' => [
        Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
            'data-url' => Url::to(['get-many-send-message-panel']),
            'onclick' => 'return retailSendEmail(document.forms.retailSend);',
        ]),
        Html::a('<i class="glyphicon glyphicon-envelope"></i>', null, [
            'class' => 'btn btn-default btn-sm hidden-lg',
            'data-url' => Url::to(['get-many-send-message-panel']),
            'onclick' => 'return rightPanel.fromContainer(document.getElementById("sendPanel"));',
        ]),
        Html::a('<i class="fa fa-print"></i> Печать', [
            'print',
            'multiple' => '',
        ], [
            'class' => 'btn btn-default btn-sm multiple-print hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]),
        Html::a('<i class="fa fa-download"></i> Скачать', [
            'download',
            'multiple' => '',
        ], [
            'class' => 'btn btn-default btn-sm multiple-download hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]),
    ],
]); ?>