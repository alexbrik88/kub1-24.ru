<?php

use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

?>

<div class="wrap py-3">
    <div class="row align-items-center">
        <div class="col-9">
            <h4 class="mb-2">Розничные продажи</h4>
        </div>
        <div class="col-3">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row']) ?>
        </div>
    </div>
</div>
<div class="nav-retail menu-nav-tabs nav-tabs-row mb-2 pb-1">
    <?= Nav::widget([
        'id' => 'debt-report-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            [
                'label' => 'Отчёты',
                'url' => ['/documents/retail/index'],
                'active' => Yii::$app->controller->action->id === 'index',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
            [
                'label' => 'Продажи',
                'url' => ['/documents/retail/sell'],
                'active' => Yii::$app->controller->action->id === 'sell',
                'options' => ['class' => 'nav-item'],
                'linkOptions' => ['class' => 'nav-link'],
            ],
        ],
    ]) ?>
</div>

<script>
    $(window).on('load', function () {
        $('#debt-report-menu').tabdrop();
    });
</script>