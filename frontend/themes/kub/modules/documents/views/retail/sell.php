<?php

use common\models\evotor\ReceiptItem;
use frontend\assets\InterfaceCustomAsset;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\RetailDataProvider;
use frontend\rbac\permissions\document\Invoice;
use frontend\widgets\RangeButtonWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var                    $this              View
 * @var string             $date              Диапазон дат
 * @var RetailDataProvider $productDataProvider
 * @var int                $totalCount        Общее количество позиций
 * @var float              $totalAmount       Общая сумма по всем позициям
 * @var float              $totalTax          Общая сумма налога
 * @var float              $totalCard         Общая сумма оплат банковской картой
 * @var float              $totalCash         Общая сумма оплат наличными
 * @var int                $totalPaymentCount Общее количество платежей
 * @var int                $offset
 */
InterfaceCustomAsset::register($this);
?>
<style type="text/css">
.rightPanel.send-message-panel {
    position: fixed;
    top: 0;
    right: 0;
    width: 40%;
    height: 100%;
    display: none;
    filter: alpha(opacity=85);
    background-color: #ffffff;
    border-left: 1px solid #e4e4e4;
    z-index: 10051;
}
.send-message-panel .side-panel-close {
    position: absolute;
    top: 0;
    right: 0;
    width: 40px;
    height: 45px;
    border-radius: 50% !important;
    cursor: pointer;
    transition: opacity .3s;
}
.send-message-panel .side-panel-close-inner {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -18px;
    margin-left: -18px;
    width: 32px;
    height: 32px;
    border: 2px solid #ffffff;
    border-radius: 50% !important;
    opacity: 1;
    transition: all 300ms ease;
    cursor: pointer;
}
.send-message-panel .side-panel-close-inner:before {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 16px;
    height: 2px;
    background-color: #000000;
    content: "";
}
.send-message-panel .side-panel-close-inner:before {
    -webkit-transform: translateX(-50%) translateY(-50%) rotate(-45deg);
    -moz-transform: translateX(-50%) translateY(-50%) rotate(-45deg);
    -ms-transform: translateX(-50%) translateY(-50%) rotate(-45deg);
    -o-transform: translateX(-50%) translateY(-50%) rotate(-45deg);
    transform: translateX(-50%) translateY(-50%) rotate(-45deg);
}
.send-message-panel .side-panel-close-inner:after {
    position: absolute;
    top: 50%;
    left: 50%;
    width: 16px;
    height: 2px;
    background-color: #000000;
    content: "";
}
.send-message-panel .side-panel-close-inner:after {
    -webkit-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    -moz-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    -ms-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    -o-transform: translateX(-50%) translateY(-50%) rotate(45deg);
    transform: translateX(-50%) translateY(-50%) rotate(45deg);
}
</style>

<?= $this->render('_partial/navbar') ?>

<div class="wrap">
    <div class="row align-items-center mb-4">
        <h5 class="col-8">
            Список чеков <?= $date ?>
        </h5>
        <div class="col-4 text-right">
            <?= Html::a(Icon::get('add-icon', ['class' => 'mr-2']).'Загрузка данных из Эвотор', [
                '/integration/evotor/setting',
                'layout' => 'no',
            ], [
                'class' => 'button-regular button-regular_red',
                'onclick' => 'return rightPanel.fromURL(this.href);',
            ]) ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $productDataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'Нет данных, удовлетворяющих условию',
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $productDataProvider->totalCount]),
        'columns' => [
            [
                'label' => 'Дата и время',
                'value' => function (ReceiptItem $model) {
                    return date('d.m.Y<\b\r/>H:i:s', $model->receipt->date_time);
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Розничная точка',
                'attribute' => 'receipt.store.name',
            ],
            [
                'label' => 'Касса',
                'attribute' => 'receipt.device.name',
            ],
            //№ смены
            [
                'label' => '№ чека',
                'attribute' => 'receipt.uuid',
            ],
            'name',
            'quantity',
            'measure_name',
            'price',
            [
                'label' => 'Цена со скидкой',
                'value' => function (ReceiptItem $model) {
                    return $model->price - $model->discount;
                },
            ],
            'sum_price',
            [
                'label' => 'Сумма со скидкой',
                'value' => function (ReceiptItem $model) {
                    return $model->sum_price - ($model->discount * $model->quantity);
                },
            ],
            'tax_percent',
            [
                'label' => 'Оплата',
                'value' => function (ReceiptItem $model) {
                    return $model->receipt->cash ? 'наличная' : 'безналичная';
                },
            ],
        ],
    ]) ?>

<style>
    .send-message-panel .col-md-6 {
        width: 100%;
        padding: 0;
    }
</style>
