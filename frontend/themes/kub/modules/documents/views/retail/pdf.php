<?php

use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\evotor\ReceiptItem;

/**
 * @var string        $date         Диапазон дат
 * @var ReceiptItem[] $allModels
 * @var int           $paymentCount Количество платежей
 * @var float         $paymentCard  Количество платежей банковской картой
 * @var float         $paymentCash  Количество платежей наличными
 */
$this->title='Отчёт о розничных продажах';

$tax = $amount = 0;
?>
<style>
    h3 {
        margin: 0;
    }

    hr {
        height: 2px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
    }

    tfoot td {
        text-align: right;
        border: 0;
        font-weight: bold;
    }

    td, th {
        border: 1px solid black !important;
    }

    p {
        margin: 0;
    }

</style>
<div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">

    <h3>Отчёт о розничных продажах <?= $date ?></h3>
    <hr/>
    <br/><br/>

    <table>
        <thead>
        <tr>
            <th>№</th>
            <th>Артикул</th>
            <th>Товар</th>
            <th>Кол-во</th>
            <th>Ед.</th>
            <th>Цена</th>
            <th>Сумма продажная</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($allModels as $i => $model) {
            $amount += $model->sum_price;
            $tax += $model->tax;
            ?>
            <tr>
                <td style="text-align:center;"><?= $i + 1 ?></td>
                <td><?=$model->product->article_number ?? ''?></td>
                <td><?= $model->name ?></td>
                <td style="text-align:right;"><?= $model->quantity ?></td>
                <td><?= $model->measure_name ?></td>
                <td style="text-align:right;"><?= $model->price ?></td>
                <td style="text-align:right;"><?= $model->sum_price ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6">Итого:</td>
            <td><?= number_format($amount, 2, ',', ' ') ?></td>
        </tr>
        <tr>
            <td colspan="6">В том числе НДС:</td>
            <td><?= number_format($tax, 2, ',', ' ') ?></td>
        </tr>
        </tfoot>
    </table>

    <br/><br/>
    <p>Всего отпущено <strong><?= count($allModels) ?></strong> наименований на сумму <strong><?= number_format($amount, 2, ',', ' ') ?> руб</strong>.</p>
    <p style="font-weight:bold;"><?= TextHelper::mb_ucfirst(Currency::textPrice($amount, 'RUB')) ?>.</p>
    <br/>
    <hr/>
    <br/><br/>

    <table>
        <thead>
        <tr>
            <th>№</th>
            <th>Вид оплаты</th>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="text-align:center;">1</td>
            <td>Оплата платёжной картой</td>
            <td><?= number_format($paymentCard, 2, ',', ' ') ?></td>
        </tr>
        <tr>
            <td style="text-align:center;">2</td>
            <td>Наличные</td>
            <td><?= number_format($paymentCash, 2, ',', ' ') ?></td>
        </tr>
        </tbody>
    </table>

    <br/><br/>
    <p>Всего платажей <strong><?= $paymentCount ?></strong> на сумму <strong><?= number_format($paymentCard + $paymentCash, 2, ',', ' ') ?> руб</strong>.</p>
    <p style="font-weight:bold;"><?= TextHelper::mb_ucfirst(Currency::textPrice($paymentCard + $paymentCash, 'RUB')) ?>.</p>
</div>
