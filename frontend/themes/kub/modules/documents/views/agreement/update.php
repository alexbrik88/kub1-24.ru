<?php

use common\models\Agreement;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */

$this->context->layout = 'agreements-update';
$this->title = 'Редактировать договор';
?>

<?= $this->render('_form', [
    'model' => $model,
    'type' => $type
]) ?>
