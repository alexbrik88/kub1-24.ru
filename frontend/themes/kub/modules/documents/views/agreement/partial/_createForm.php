<?php

use common\models\AgreementType;
use common\models\AgreementTemplate;
use common\models\AgreementTitleTemplate;
use frontend\themes\kub\helpers\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */
/* @var $form yii\widgets\ActiveForm */
/* @var $fixedContractor boolean */
/* @var $fixedType boolean */
/* @var $newRecord boolean */
/* @var $disableAllFields boolean */

$rsData = [];
$allTemplates = [];
$sellerArray = $customerArray = [];
$sellerTemplatesArray = $customerTemplatesArray = [];
$company = $company_id = null;
$contractorTypesArray = [];
$titleTemplateData = AgreementTitleTemplate::find()->select('name')->indexBy('id')->column();

if (Yii::$app->user->identity !== null
    && method_exists(Yii::$app->user->identity, 'hasProperty')
    && Yii::$app->user->identity->hasProperty('company')) {

    $company = Yii::$app->user->identity->company;
    $company_id = $company->id;
}


if ($company_id) {

    //$currentRs = $model->company_rs ? [$model->company_rs => $model->company_bank_name] : [];
    $companyRs = Company::findOne($company_id)->getCheckingAccountants()
        ->select(['name', 'rs', 'id'])
        ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
        ->orderBy(['type' => SORT_ASC])
        ->indexBy('rs')
        ->asArray()->all();

    $rsData = [];
    foreach ($companyRs as $rs) {
        $rsData[$rs['rs']] = $rs['name'];
    }

    $contractorDropDownConfig = [
        'class' => 'form-control contractor-select',
    ];

    $sellerTemplatesArray = ['' => '---', 'add-agreement-template' => Icon::PLUS . ' Добавить шаблон'] + AgreementTemplate::getAllTemplatesList($company_id, Contractor::TYPE_SELLER);
    $customerTemplatesArray = ['' => '---', 'add-agreement-template' => Icon::PLUS . ' Добавить шаблон'] + AgreementTemplate::getAllTemplatesList($company_id, Contractor::TYPE_CUSTOMER);

    $contractorTypesArray = [
        Contractor::TYPE_CUSTOMER => 'С покупателем',
        Contractor::TYPE_SELLER => 'С поставщиком'
    ];
}

if ($model->type == Contractor::TYPE_SELLER) {
    $contractorType = Contractor::TYPE_SELLER;
    $staticData = ["add-modal-contractor" => Icon::PLUS . ' Добавить поставщика'];
} else {
    $contractorType = Contractor::TYPE_CUSTOMER;
    $staticData = ["add-modal-contractor" => Icon::PLUS . ' Добавить покупателя'];
}

if ($fixedContractor && $contractor = Contractor::findOne($model->contractor_id)) {
    if ($model->type == Contractor::TYPE_SELLER) {
        $sellerArray = [$contractor->id => $contractor->getNameWithType()];
        $customerArray = [];
    } else {
        $customerArray = [$contractor->id => $contractor->getNameWithType()];
        $sellerArray = [];
    }
}

if ($fixedType) {
    if ($model->type == Contractor::TYPE_SELLER)
        $contractorTypesArray = [Contractor::TYPE_SELLER => 'С поставщиком'];
    else
        $contractorTypesArray = [Contractor::TYPE_CUSTOMER => 'С покупателем'];

    $hasTemplates = AgreementTemplate::find()->where(['company_id' => $company_id, 'type' => $model->type])->andWhere(['<>', 'status', AgreementTemplate::STATUS_DELETED])->exists();
} else {

    $hasTemplates = AgreementTemplate::find()->where(['company_id' => $company_id])->andWhere(['<>', 'status', AgreementTemplate::STATUS_DELETED])->exists();
}

if (!$newRecord) {
    $model->create_agreement_from = $model->agreement_template_id ? 1 : 0;
}

if ($model->type === null)
    $model->type = Contractor::TYPE_CUSTOMER;

$header = $newRecord ? 'Добавить договор' : 'Редактировать договор';
$firstCreate = stristr(\Yii::$app->request->referrer, 'invoice/first-create') !== false;
$model->document_name = $model->document_name ? $model->document_name : ($firstCreate ? 'Договор' : null);

// Create/Update from Invoice
$hideTemplateFields = Yii::$app->request->get('hide_template_fields');

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<div class="agreement-form" data-header="<?= $header ?>">

    <h4 class="modal-title"><?= $header ?></h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>

    <?php $form = ActiveForm::begin([
        'id' => 'contractor-agreement-form',
        'enableClientValidation' => false,
        'action' => ['update',
            'id' => $model->id,
            'contractor_id' => ($fixedContractor ? $model->contractor_id : null),
            'type' => ($fixedType ? $model->type : null),
            'from_template' => ($fixedTemplate ? $model->agreement_template_id : null),
            'old_record' => !$newRecord,
            'returnTo' => $returnTo
        ],
        'fieldConfig' => array_merge(Yii::$app->params['kubFieldConfig'], [
            'enableLabel' => false,
            'options' => [
                'class' => '',
            ],
            'inputOptions' => ['class' => 'form-control'],
            'template' => "{input}\n{hint}\n{error}"
        ]),
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]); ?>

    <?php // echo $form->errorSummary($model); ?>

    <?php if ($newRecord) : ?>
        <div class="row row-create-from-tpl">
            <div class="form-group col-6">
                <label class="label mb-3" for="agreement-create_agreement_from">Создать по шаблону</label>
                <br/>
                <div class="checkbox">
                    <?= Html::activeCheckbox($model, 'create_agreement_from', [
                        'label' => false,
                        'value' => \common\models\Agreement::CREATE_AGREEMENT_FROM_TEMPLATE,
                        'onchange' => "changeAgreementFrom(this)",
                        'disabled' => $fixedTemplate ? true : false
                    ]) ?>
                    <?php if (!$hasTemplates) : ?>
                        <label for="agreement-create_agreement_from">У вас нет ни одного шаблона. <?= Html::a('Создать?', '/documents/agreement-template') ?></label>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($model->create_agreement_from): ?>
                <div class="form-group col-6">
                    <label class="label">Тип документа</label>
                    <?php echo $form->field($model, 'document_type_id', ['template' => "{input}", 'options' => []])
                        ->widget(Select2::class, [
                            'data' => AgreementType::find()->select(['name', 'id'])->orderBy(['sort' => SORT_ASC])->indexBy('id')->column(),
                            'pluginOptions' => [
                                'width' => '100%',
                                'minimumResultsForSearch' => -1
                            ]
                        ])->label(false); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="form-group col-6 <?=($hideTemplateFields) ? 'hidden':'' ?>">
            <label class="label mb-0" for="Agreement[type]">Тип</label>
            <br><br>
            <div class="mr-3 inline-block <?=(($fixedContractor || $fixedType) && $model->type == Contractor::TYPE_SELLER ? 'hidden' : '')?>">
                <input id="radioA1" type="radio"
                       name="Agreement[type]"
                       value="<?=(Contractor::TYPE_CUSTOMER)?>"
                       <?=($model->type == Contractor::TYPE_CUSTOMER ? 'checked=""' : '')?>
                       onclick="changeDocumentType(this)">
                <label for="radioA1" class="label">С покупателем</label>
            </div>
            <div class="mr-3 inline-block <?=(($fixedContractor || $fixedType) && $model->type == Contractor::TYPE_CUSTOMER ? 'hidden' : '')?>">
                <input id="radioA2" type="radio"
                       name="Agreement[type]"
                       value="<?=(Contractor::TYPE_SELLER)?>"
                    <?=($model->type == Contractor::TYPE_SELLER ? 'checked=""' : '')?>
                       onclick="changeDocumentType(this)">
                <label for="radioA2" class="label">С поставщиком</label>
            </div>
        </div>

        <div class="form-group col-6" style="display: <?= $firstCreate ? 'none' : 'block' ?>;">
            <label class="label" id="contractor_label"><?= ($model->type == Contractor::TYPE_SELLER) ? 'Поставщик' : 'Покупатель' ?></label>
            <?= $form->field($model, 'contractor_id', [
                'template' => "{input}{hint}{error}",
                'options' => [],
            ])->widget(Select2::class, [
                'data' => $staticData + ArrayHelper::map(Contractor::getSorted()
                        ->byCompany($model->company_id)
                        ->byContractor($contractorType)
                        ->byIsDeleted(Contractor::NOT_DELETED)
                        ->byStatus(Contractor::ACTIVE)
                        ->all(), 'id', 'shortName'),
                'options' => [
                    'readonly' => ($fixedContractor) ? true : false,
                    'placeholder' => '',
                    'data-createurl' => Url::to(['add-modal-contractor', 'type' => $model->type]),
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                ],
            ])->label(false); ?>
        </div>

        <div class="row-create-from-file"></div>
        <?php if (!$model->create_agreement_from): ?>
        <div class="form-group col-6">
            <label class="label">Тип документа</label>
            <?php echo $form->field($model, 'document_type_id', ['template' => "{input}", 'options' => ['class' => '']])
                ->widget(Select2::class, [
                    'data' => AgreementType::find()->select(['name', 'id'])->orderBy(['sort' => SORT_ASC])->indexBy('id')->column(),
                    'pluginOptions' => [
                        'width' => '100%',
                        'minimumResultsForSearch' => -1
                    ]
                ])->label(false); ?>
        </div>
        <?php endif; ?>

        <?php if ($hideTemplateFields): ?>
            <div class="hidden">
        <?php endif; ?>

            <div class="form-group col-6 <?= ($fixedTemplate || !empty($templateDocumentTypeId) ? '' : 'hidden') ?>">
            <?php
            Pjax::begin([
                'id' => 'templates-pjax',
                'enablePushState' => false,
                'linkSelector' => false,
                'options' => [
                    'data-url' => Url::toRoute(['create', 'type' => $model->type])
                ]]) ?>

                <label class="label">Шаблон</label>
                <?php echo $form->field($model, 'agreement_template_id', ['template' => "{input}{hint}{error}", 'options' => []])
                    ->widget(Select2::class, [
                        'data' => $model->type == Contractor::TYPE_SELLER ? $sellerTemplatesArray : $customerTemplatesArray,
                        'pluginOptions' => [
                            'width' => '100%',
                            'minimumResultsForSearch' => -1,
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        ]
                    ])
                    ->label(false); ?>
                <?= Html::dropDownList('seller_templates_id', 0, $sellerTemplatesArray, ['id' => 'seller_templates_id', 'style' => 'display:none']); ?>
                <?= Html::dropDownList('customer_templates_id', 0, $customerTemplatesArray, ['id' => 'customer_templates_id', 'style' => 'display:none']); ?>
            <?php Pjax::end() ?>
            </div>

        <?php if ($hideTemplateFields): ?>
            </div>
        <?php endif; ?>

        <div class="form-group col-6">
            <label class="label">Наименование документа</label>
            <?= $form->field($model, 'document_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="form-group col-6">
            <label class="label">Номер документа</label>
            <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="form-group col-6">
            <label class="label">Дата документа</label>
            <div class="date-picker-wrap">
                <?= $form->field($model, 'document_date_input')->textInput([
                    'class' => 'form-control date-picker ico',
                    'autocomplete' => 'off',
                ]) ?>
            </div>
        </div>

        <div class="form-group col-6 <?= ($fixedTemplate ? '' : 'hidden') ?>">
            <label class="label">Мой расчетный счет</label>
            <?php echo $form->field($model, 'company_rs', ['template' => "{input}{hint}{error}", 'options' => []])
                ->widget(Select2::class, [
                    'data' => $rsData,
                    'pluginOptions' => [
                        'width' => '100%',
                        'minimumResultsForSearch' => -1
                    ]
                ])->label(false); ?>
        </div>

        <div class="form-group col-6">
            <label class="label">Дата окончания документа</label>
            <div class="date-picker-wrap">
                <?= $form->field($model, 'payment_limit_date_input')->textInput([
                    'class' => 'form-control date-picker ico',
                    'autocomplete' => 'off',
                ]) ?>
            </div>
        </div>

        <div class="form-group col-6">
            <label class="label">Выводить в документах</label>
            <?= $form->field($model, 'title_template_id')->widget(Select2::classname(), [
                'data' => $titleTemplateData,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'hideSearch' => true,
                ],
            ])->label(false); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label class="label" for="file">Загрузить скан/файл документа</label>
            <div id="agreement_upload_file" style="position:relative">
                <?= \frontend\themes\kub\widgets\file\FileUpload::widget([
                    'uploadUrl' => Url::to(['agreement-file-upload', 'id' => $model->id,]),
                    'deleteUrl' => Url::to(['agreement-file-delete', 'id' => $model->id,]),
                    'listUrl' => Url::to(['agreement-file-list', 'id' => $model->id,]),
                ]); ?>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-between" style="margin-top:18px">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-regular_red button-clr min-w-130 ladda-button',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    <?php
    if (is_file($file = \Yii::getAlias('@common/assets/web/scripts/upload.js'))) {
        echo $this->renderFile($file);
    }
    ?>
</script>

<?php $this->registerJs(<<<JS

templatesReload = function(doc_type_id) {
    var pjax = $("#templates-pjax");
    $.pjax({
        url: pjax.attr("data-url"),
        data: {templateDocumentTypeId: doc_type_id},
        container: "#templates-pjax",
        push: false,
        timeout: 10000,
    });
}

agreementToggleFields = function(type_agreement_from) {
    var type_file = 0;
    var type_template = 1;
    var enable_fields;
    var disable_fields;

    if (type_agreement_from != type_template) {
        enable_fields = []
        disable_fields = [
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    } else {
        disable_fields = []
        enable_fields = [
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    }

    $.each(disable_fields, function(i, the_id) {
        $('#agreement-'+the_id).prop("disabled", true).parents('.form-group').addClass('hidden');
    });
    $.each(enable_fields, function(i, the_id) {
        $('#agreement-'+the_id).prop("disabled", false).parents('.form-group').removeClass('hidden');
    });
}

agreementResetFields = function(type_agreement_from) {
    var type_file = 0;
    var type_template = 1;
    var reset_fields;

    if (type_agreement_from != type_template) {
        reset_fields = [
            'document_number',
            'document_name',
            'company_rs',
            'agreement_template_id',
            'payment_limit_date_input'
        ];
    } else {
        reset_fields = [];
    }

    $.each(reset_fields, function(i, the_id) {
        $('#agreement-'+the_id).val('');
        $('#agreement-'+the_id).siblings('.help-block').html('');
        $('#agreement-'+the_id).parents('.form-group').removeClass('has-error');
    });

}

changeAgreementTemplate = function(agreement_template_id) {
    if (agreement_template_id > 0) {
        $.post("/documents/agreement/get-next-number/?agreement_template_id=" + agreement_template_id, {}, function(data) {
            if (typeof data.document_number !== "undefined" && $('#agreement-document_number').val() == '')
                $('#agreement-document_number').val(data.document_number);
            if (typeof data.document_name !== "undefined")
                $('#agreement-document_name').val(data.document_name);
        });
    }
}

changeAgreementFrom = function(el) {
    var type_file = 0;
    var type_template = 1;
    var type_agreement_from = $(el).prop('checked') ? type_template : type_file;
    var doc_type_id = $("#agreement-document_type_id").val();

    if (type_agreement_from && doc_type_id > 0) {
        templatesReload(doc_type_id);
    }
    agreementToggleFields(type_agreement_from);
    agreementResetFields(type_agreement_from);

    var doc_type_field = $('#agreement-document_type_id').parents('.form-group');
    if (type_agreement_from === type_template) {
        $(doc_type_field).appendTo('.row-create-from-tpl');
    } else {
        $(doc_type_field).insertAfter('.row-create-from-file');
    }

    console.log(type_agreement_from);
}

changeDocumentType = function(el) {
    var type_seller = 1;
    var type_customer = 2;
    var type = $(el).val();
    if (type == type_seller) {
        $('#agreement-contractor_id').html($('#seller_id').html());
        $('#agreement-contractor_id').addClass('seller');
        $('#agreement-agreement_template_id').html($('#seller_templates_id').html());
        $('#contractor_label').html('Поставщик');
    } else {
        $('#agreement-contractor_id').html($('#customer_id').html());
        $('#agreement-contractor_id').addClass('customer');
        $('#agreement-agreement_template_id').html($('#customer_templates_id').html());
        $('#contractor_label').html('Покупатель');
    }
    console.log(type);
}

$(document).on("change", "#agreement-agreement_template_id", function (e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-agreement-template") {
        e.preventDefault();
        window.location.href = "/documents/agreement-template";
    } else {
        changeAgreementTemplate(value);
    }
});

$("#agreement-document_type_id").on("change", function() {
    if ($("#agreement-create_agreement_from").prop("checked")) {
        if ($(this).val() > 0) {
            templatesReload($(this).val());
            $("#agreement-document_name").val('');
        }
    }
});

JS
); ?>

<?php $this->registerJs(' $(document).ready(function(){
    agreementToggleFields(' . ($model->create_agreement_from) . '); });
    refreshDatepicker();
    // $("#agreement-document_type_id").trigger("change").val("' . $model->document_type_id . '");
'); ?>

<?php if ($fixedTemplate && $newRecord) {
    $this->registerJs('
    $(document).ready(function(){
        changeAgreementTemplate($("#agreement-agreement_template_id").val());
    });');
}
?>

<?php Pjax::end() ?>

