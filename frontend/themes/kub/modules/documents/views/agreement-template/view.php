<?php

use common\models\AgreementTemplate;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\permissions\Contractor;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

$this->context->layout = 'agreements-update';
$this->title = 'Просмотр шаблона договора';

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$canUpdate = Yii::$app->user->can(permissions\AgreementTemplate::UPDATE, [
    'model' => $model,
]);

?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', ['index', 'type' => $model->type], ['class' => 'link mb-2']); ?>
<?php endif ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\kub\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if ($model->status == AgreementTemplate::STATUS_ACTIVE && Yii::$app->user->can(frontend\rbac\permissions\AgreementTemplate::UPDATE, [
                    'model' => $model,
                ])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'id' => $model->id,
                        'type' => $model->type,
                    ], [
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                    ]) ?>
                <?php endif; ?>
                <div class="doc-container doc-agreement doc-preview">
                    <?= $this->render('partial/_previewText', [
                        'model' => $model,
                        'canUpdate' => $canUpdate
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('partial/_previewStatus', [
                'model' => $model,
                'canUpdate' => $canUpdate
            ]); ?>
        </div>
    </div>
</div>
<?= $this->render('partial/_previewActionsButtons', [
    'model' => $model,
    'canUpdate' => $canUpdate
]); ?>

<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h4 id="agreement-modal-header">Добавить договор</h4>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();
?>

<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<?php
if (Yii::$app->session->get('showAgreementCreatePopup')) {
    $this->registerJs('
        $(document).ready(function (e) {
            $(".agreement-modal-link").click();
        });
    ');
}
$this->registerJs('
$(document).on("click", ".agreement-modal-link", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).data("url"), container: "#agreement-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#agreement-modal-header").html($("[data-header]").data("header"));
        // $(".date-picker").datepicker({format:"dd.mm.yyyy",language:"ru",autoclose:true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
    })
    $("#agreement-modal-container").modal("show");
});
$(document).on("show.bs.modal", "#agreement-modal-container", function(event) {
        $(".alert-success").remove();
});

');

?>