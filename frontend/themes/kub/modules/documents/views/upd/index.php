<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\UpdStatus;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\service\Payment;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\document\DocumentImportType;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\UpdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;

$exists = Invoice::find()->joinWith('upd', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет УПД. Измените период, чтобы увидеть имеющиеся УПД.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного УПД.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => UpdStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => UpdStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) {
    $dropItems[] = [
        'label' => 'Ответственный',
        'url' => '#mass_change_responsible',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeResponsibleWidget::widget([
        'formAction' => [
            'invoice/mass-set-responsible',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
    ]);
}
if ($ioType == Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ],
    ];
}

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_upd' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');

$isTaxTypeOsno = $company->companyTaxationType->osno;
$isTaxTypeUsn = $company->companyTaxationType->usn;

$stat1 = [
    'sum' => $searchModel->getQuery()
        ->addSelect([
            Upd::tableName().'.*',
            '`invoice`.`total_amount_with_nds` as `total_amount_with_nds`',
        ])
        ->sum('total_amount_with_nds'),
    'count' => $searchModel->getQuery()
        ->count(),
    'title' => 'Итого',
    'title2' => 'Количество УПД:',
];
$stat2 = [
    'sum' => $searchModel->getQuery()
        ->joinWith('invoice.payment')
        ->addSelect([
            Upd::tableName().'.*',
            'IF(`invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds` - `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
        ])
        ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
        ->sum('total_amount_with_nds'),
    'count' => $searchModel->getQuery()
        ->joinWith('invoice.payment')
        ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
        ->count(),
    'title' => 'Неоплачено',
    'title2' => 'Количество УПД:',
];
$stat3 = [
    'sum' => $searchModel->getQuery()
        ->joinWith('invoice.payment')
        ->addSelect([
            Upd::tableName().'.*',
            'IF(`invoice`.`payment_partial_amount`, `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
        ])
        ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
        ->sum('total_amount_with_nds'),
    'count' => $searchModel->getQuery()
        ->joinWith('invoice.payment')
        ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
        ->count(),
    'title' => 'Оплачено',
    'title2' => 'Количество УПД:',
];

$this->registerCss(<<<CSS
    .sorting_block {
        cursor: pointer;
    }
CSS);

$this->registerJs(<<<JS
    $(".sorting_block").on("click", function(e){
        e.preventDefault();
        var url = $(this).data("url");

        if (url) {
            window.location = url;
        }
    });
JS, View::POS_READY);

?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()) : ?>
        <?php
        Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как подготовить УПД, нужно создать Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php
        Modal::end();

        ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как подготовить УПД,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
            .
        </div>
    <?php endif; ?>
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card count-card_green wrap"
                     data-url=<?= Url::to([
                         '/documents/upd/index',
                         'type' => $ioType,
                         'UpdSearch[payment_status]' => 'payment'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat3['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat3['title2']; ?>
                        <?= $stat3['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card count-card_red wrap"
                     data-url=<?= Url::to([
                         '/documents/upd/index',
                         'type' => $ioType,
                         'UpdSearch[payment_status]' => 'not_payment'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat2['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat2['title2']; ?>
                        <?= $stat2['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card wrap"
                     data-url=<?= Url::to([
                         '/documents/upd/index',
                         'type' => $ioType,
                         'UpdSearch[payment_status]' => 'all'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat1['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat1['title2']; ?>
                        <?= $stat1['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'upd_scan',
                    ],
                    [
                        'attribute' => 'upd_order_sum_without_nds',
                        'visible' => (bool)$isTaxTypeOsno,
                    ],
                    [
                        'attribute' => 'upd_order_sum_nds',
                        'visible' => (bool)$isTaxTypeOsno,
                    ],
                    [
                        'attribute' => 'upd_contractor_inn',
                    ],
                    [
                        'attribute' => 'upd_industry',
                    ],
                    [
                        'attribute' => 'upd_sale_point',
                    ],
                    [
                        'attribute' => 'upd_project',
                    ],
                    [
                        'attribute' => 'upd_payment_date',
                    ],
                    [
                        'attribute' => 'upd_responsible',
                    ],
                    [
                        'attribute' => 'upd_source',
                    ],
                ],
            ]); ?>
            <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]);?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер УПД, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('Upd[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $data->id,
                        'data-sum' => $data->totalAmountWithNds,
                        'data-contractor' => $data->invoice->contractor_id,
                        'data-responsible' => $data->invoice->responsible_employee_id,
                        'data-invoice-id' => $data->invoice->id,
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата УПД',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ УПД',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $data,
                    ])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber;
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting col_upd_scan ' . ($userConfig->upd_scan ? '' : ' hidden'),
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'link-view col_upd_scan ' . ($userConfig->upd_scan ? '' : ' hidden'),
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'totalAmountWithNds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'order_sum_without_nds',
                'label' => 'Сумма без НДС',
                'headerOptions' => [
                    'class' => 'col_upd_order_sum_without_nds ' . ($userConfig->upd_order_sum_without_nds ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view col_upd_order_sum_without_nds ' . ($userConfig->upd_order_sum_without_nds ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = TextHelper::invoiceMoneyFormat($data->totalAmountNoNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
                'visible' => (bool)$isTaxTypeOsno,
            ],
            [
                'attribute' => 'order_sum_nds',
                'label' => 'Сумма НДС',
                'headerOptions' => [
                    'class' => 'col_upd_order_sum_nds ' . ($userConfig->upd_order_sum_nds ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view col_upd_order_sum_nds ' . ($userConfig->upd_order_sum_nds ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = TextHelper::invoiceMoneyFormat($data->getTotalNds(), 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
                'visible' => (bool)$isTaxTypeOsno,
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, Upd::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],

            [
                'attribute' => 'contractor_inn',
                'label' => 'ИНН контрагента',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_upd_contractor_inn' . ($userConfig->upd_contractor_inn ? '' : ' hidden'),
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell col_upd_contractor_inn' . ($userConfig->upd_contractor_inn ? '' : ' hidden'),
                ],
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span title="' . Html::encode($data->invoice->contractor_inn) . '">' . Html::encode($data->invoice->contractor_inn) . '</span>';
                },
            ],

            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->statusOut->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],
            [
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => 'col_upd_industry' . ($userConfig->upd_industry ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_industry' . ($userConfig->upd_industry ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + CompanyIndustry::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Upd $model) {
                    return ($model->invoice->industry) ? Html::encode($model->invoice->industry->name) : '';
                },
            ],
            [
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => 'col_upd_sale_point' . ($userConfig->upd_sale_point ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_sale_point' . ($userConfig->upd_sale_point ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + SalePoint::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Upd $model) {
                    return ($model->invoice->salePoint) ? Html::encode($model->invoice->salePoint->name) : '';
                },
            ],
            [
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => 'col_upd_project' . ($userConfig->upd_project ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_project' . ($userConfig->upd_project ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + Project::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Upd $model) {
                    return ($model->invoice->project) ? Html::encode($model->invoice->project->name) : '';
                },
            ],
            [
                'attribute' => 'invoice.document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $invoiceArray = $data->invoices;
                    $items = [];
                    foreach ($invoiceArray as $invoice) {
                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice,])
                        ) {
                            if ($invoice->file !== null) {
                                $invoiceFileLinkClass = null;
                                $tooltipId = null;
                                $contentPreview = null;
                                if (in_array($invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                    $invoiceFileLinkClass = 'invoice-file-link-preview';
                                    $tooltipId = 'invoice-file-link-preview-' . $invoice->file->id;
                                    $thumb = $invoice->file->getImageThumb(400, 600);
                                    if ($thumb) {
                                        $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                        $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                        $contentPreview .= Html::img($thumb, ['alt' => '']);
                                        $contentPreview .= Html::endTag('span');
                                        $contentPreview .= Html::endTag('div');
                                    }
                                }

                                $items[] = Html::a($invoice->fullNumber, [
                                    '/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $invoice->id,
                                ]) . Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                    '/documents/invoice/file-get',
                                    'type' => $data->type,
                                    'id' => $invoice->id,
                                    'file-id' => $invoice->file->id,
                                ], [
                                    'class' => $invoiceFileLinkClass,
                                    'target' => '_blank',
                                    'data-tooltip-content' => '#' . $tooltipId,
                                    'style' => 'float: right',
                                ]) . $contentPreview;
                            } else {
                                $items[] = Html::a($invoice->fullNumber, [
                                    '/documents/invoice/view',
                                    'type' => $data->type,
                                    'id' => $invoice->id,
                                ]);
                            }
                        } else {
                            $items[] = $invoice->fullNumber;
                        }
                    }
                    return implode(', ', $items);
                },
            ],
            [
                'attribute' => 'upd_payment_date',
                'label' => 'Дата оплаты',
                'headerOptions' => [
                    'class' => 'col_upd_payment_date ' . ($userConfig->upd_payment_date ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_payment_date ' . ($userConfig->upd_payment_date ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (Upd $data) {
                    $date = $data->invoice->getLastPaymentDate();
                    return !$date ? "-" : date(DateHelper::FORMAT_USER_DATE, $date);
                },
            ],
            [
                'attribute' => 'upd_responsible',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'col_upd_responsible' . ($userConfig->upd_responsible ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_responsible' . ($userConfig->upd_responsible ? '' : ' hidden'),
                ],
                's2width' => '200px',
                'filter' => $searchModel->filterUpdResponsible(),
                'format' => 'raw',
                'value' => function (Upd $data) {
                    return $data->invoice->responsible ? $data->invoice->responsible->getFio(true) : '';
                },
            ],
            [
                'attribute' => 'upd_source',
                'label' => 'Источник',
                'headerOptions' => [
                    'class' => 'col_upd_source' . ($userConfig->upd_source ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_upd_source' . ($userConfig->upd_source ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + DocumentImportType::getList(),
                'format' => 'raw',
                'value' => function ($data) {
                    return DocumentImportType::getName($data->one_c_imported);
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>'. Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr'
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_UPD,
    'documentTypeName' => 'УПД'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_UPD
    ]); ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php \yii\bootstrap4\Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные товарные накладные?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php \yii\bootstrap4\Modal::end(); ?>
<?php endif ?>
