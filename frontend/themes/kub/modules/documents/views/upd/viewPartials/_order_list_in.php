<?php

use common\components\TextHelper;
use common\models\address\Country;
use yii\bootstrap4\Html;
use common\models\product\Product;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $model \common\models\document\Upd */
/* @var $company \common\models\Company */
/* @var $this \yii\web\View */

$tableClass = isset($tableClass) ? $tableClass : 'table table-style table-count in-upd_table';
$countryList = Country::find()->select('name_short')->indexBy('id')->column();
?>

<div class="custom-scroll-table scroll-table">
    <div class="table-wrap">
        <table id="product-table-upd" class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th width="10%">Наименование</th>
                <th width="5%">Код<br>вида<br>товара</th>
                <th width="6%">Ед.изм.</th>
                <th width="5%">Код</th>
                <th width="5%">Код по ОКЕИ</th>
                <th width="3%">Вид упаковки</th>
                <th width="3%">Кол-во в одном месте</th>
                <th width="3%">Кол-во мест, штук</th>
                <th width="3%">Масса брутто</th>
                <th width="5%">Кол-во (объем)</th>
                <th width="5%">Цена</th>
                <th width="5%">Сумма без НДС</th>
                <th width="5%">Ставка НДС</th>
                <th width="5%">Сумма НДС</th>
                <th width="8%">Сумма с учётом НДС</th>
                <th width="5%">Страна происх. то&shy;ва&shy;ра</th>
                <th width="5%">
                    Регистрацион&shy;ный но&shy;мер та&shy;мо&shy;жен&shy;ной де&shy;кла&shy;ра&shy;ции
                </th>
                <th width="3%"></th>
            </tr>
            </thead>
            <tbody id="tbody" class="document-orders-rows">
            <?php foreach ($model->ownOrders as $key => $order): ?>
                <?= $this->render('_order_list_in_row', [
                    'key' => $key,
                    'orderUpd' => $order,
                    'model' => $model,
                    'precision' => $precision,
                    'countryList' => $countryList,
                ]); ?>
            <?php endforeach; ?>

            <tr id="from-new-add-row" role="row" class="from-new-add" style="display: none;">
                <td>
                    <div class="add-exists-product">
                        <?php echo Select2::widget([
                            'id' => 'order-add-select',
                            'name' => 'addOrder',
                            'initValueText' => '',
                            'options' => [
                                'placeholder' => '',
                                'class' => 'form-control ',
                                'data-doctype' => $ioType,
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'width' => '100%',
                                'minimumInputLength' => 1,
                                'dropdownCssClass' => 'product-search-dropdown',
                                'ajax' => [
                                    'url' => "/product/search",
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                                    'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(product) { return product.text; }'),
                                'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                            ],
                        ]); ?>
                        <div id="order-add-static-items" style="display: none;">
                            <ul class="order-add-static-items select2-results__options">
                                <?php if ($canAddProduct) : ?>
                                    <li class="select2-results__option add-modal-produc-new red" aria-selected="false" data-type="<?= $model->type ?>" data-document="act">
                                        <?= Icon::PLUS . " Добавить {$addProductTypesText}" ?>
                                    </li>
                                <?php endif ?>
                                <li class="select2-results__option add-modal-services<?= $onlyProducts || $serviceCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                                    Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                                </li>
                                <li class="select2-results__option add-modal-products<?= $onlyServices || $goodsCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                                    Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                                </li>
                            </ul>
                        </div>
                        <script type="text/javascript">
                            $('#order-add-select').on('select2:open', function (evt) {
                                var prodItems = $('#select2-order-add-select-results').parent().children('.order-add-static-items');
                                if (prodItems.length) {
                                    prodItems.remove();
                                }
                                $('.order-add-static-items').clone().insertAfter('#select2-order-add-select-results');
                            });
                            $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function() {
                                $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                                $(this).addClass('select2-results__option--highlighted');
                            });
                        </script>
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr role="row" class="template" style="display: none;">
                <td class="product-title"></td>
                <td class="product-type_code"></td>
                <td class="product-unit-name"></td>
                <td class="product-code"></td>
                <td class="product-code_okei"></td>
                <td class="product-box_type"></td>
                <td class="product-count_in_place"></td>
                <td class="product-place_count"></td>
                <td class="product-mass_gross"></td>
                <td class="product-quantity">
                    <span class="product-no-count">
                        <?= Product::DEFAULT_VALUE ?>
                        <?= Html::hiddenInput("_surchargeProduct[productID][quantity]", 1, [
                            'class' => 'quantity',
                            'data-value' => 1,
                        ]) ?>
                    </span>
                    <span class="product-count">
                        <?= Html::input('number', "_surchargeProduct[productID][quantity]", 1, [
                            'min' => 1,
                            'class' => 'input-editable-field quantity form-control-number',
                            'style' => 'min-width: 85px',
                            'available' => 0,
                            'price' => 0,
                            'tax-rate' => 0
                        ]) ?>
                    </span>
                </td>
                <td class="product-price text-right">
                    <?= Html::input('text', "_surchargeProduct[productID][price]", '0.00', [
                        'class' => 'input-editable-field price form-control-number text-right',
                        'style' => 'min-width: 120px'
                    ]) ?>
                </td>
                <td class="product-amount_no_nds text-right"></td>
                <td class="product-tax_rate text-right"></td>
                <td class="product-tax_amount text-right"></td>
                <td class="product-amount text-right"></td>
                <td class="product-country">
                    <?= Html::dropDownList(
                        "_surchargeProduct[productID][country_id]",
                        null,
                        $countryList,
                        [
                            'class' => 'form-control simple-select-2',
                            'style' => 'width: 150px;',
                        ]
                    ); ?>
                </td>
                <td class="product-custom_declaration_number">
                    <?= Html::textInput("_surchargeProduct[productID][custom_declaration_number]",
                        null,
                        ['class' => 'form-control']
                    ); ?>
                </td>
                <td class="text-center">
                    <button class="delete-row button-clr" type="button">
                        <svg class="table-count-icon svg-icon">
                            <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                        </svg>
                    </button>
                    <input type="hidden" class="selected-product-id" value="-1"/>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>
