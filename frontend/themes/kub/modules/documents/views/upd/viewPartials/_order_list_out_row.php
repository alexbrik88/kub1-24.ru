<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\OrderUpd;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\Html;

/* @var \common\models\document\PackingList $model */
/* @var \common\models\document\orderUpd $orderUpd */
/* @var integer $key */

$ioType = $model->type;
$order = $orderUpd->order;
$invoice = $model->invoice;
$hasNds = $invoice->hasNds;
$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$orderPrice = $isNdsExclude ? $orderUpd->priceNoNds : $orderUpd->priceWithNds;

$product = $order->product;
$isGoods = ($product->production_type == Product::PRODUCTION_TYPE_GOODS);
$isService = $product->production_type == Product::PRODUCTION_TYPE_SERVICE;
$hideUnits = ($isService && !$model->show_service_units);
$maxQuantity = OrderUpd::availableQuantity($order, $model->id);
if ($maxQuantity != intval($maxQuantity)) {
    $maxQuantity = rtrim(number_format($maxQuantity, 10, '.', ''), 0);
}
// $quantityValue = ($isGoods || $orderUpd->quantity != 1) ? $orderUpd->quantity : Product::DEFAULT_VALUE;

$unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
$amountNoNds = $model->getPrintOrderAmount($orderUpd->order_id, $hasNds);
$amountWithNds = $model->getPrintOrderAmount($orderUpd->order_id);
?>
<tr role="row" class="odd order">
    <td>
        <?= $order->product_title ?>
        <input type="hidden" class="selected-product-id" value="<?= $order->product_id ?>"/>
    </td>
    <td><?= $product->item_type_code ?: Product::DEFAULT_VALUE; ?></td>
    <td>
        <?= Html::tag('span', $unitName, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td><?= $isGoods && $product->code ? $product->code : Product::DEFAULT_VALUE; ?></td>
    <td>
        <?php $value = $product->productUnit ? $product->productUnit->code_okei : Product::DEFAULT_VALUE; ?>
        <?= Html::tag('span', $value, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td><?= $isGoods && $order->box_type ? $order->box_type : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->count_in_place ? $order->count_in_place : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->place_count ? $order->place_count : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->mass_gross ? $order->mass_gross : Product::DEFAULT_VALUE; ?></td>
    <td class="product-quantity">
        <?= $isService ? Html::tag('span', Product::DEFAULT_VALUE, [
            'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
        ]) : null; ?>
        <?= Html::input('number', "orderParams[{$order->id}][quantity]", $orderUpd->quantity, [
            'style' => 'width: 75px;',
            'class' => 'form-control-number quantity product-count width-100' . (
                $isService ? ' service_units_visible' . ($hideUnits ? ' hide' : '') : ''
            ),
            'min' => 0,
            'available' => (float)$orderUpd::availableQuantity($orderUpd->order, $orderUpd->upd_id),
            'price' => (float)$orderUpd->priceNoNds,
            'price_with_nds' => (float)$orderUpd->priceWithNds,
            'tax-rate' => ($ioType == Documents::IO_TYPE_OUT) ? $orderUpd->order->saleTaxRate->rate : $orderUpd->order->purchaseTaxRate->rate
        ]); ?>
    </td>
    <td class="product-price text-right">
        <?= Html::tag('span', TextHelper::invoiceMoneyFormat($orderUpd->priceNoNds, $precision), [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td class="product-amount_no_nds text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
    <td class="product-tax_rate text-right"><?= $hasNds ? $order->saleTaxRate->name : 'Без НДС'; ?></td>
    <td class="product-tax_amount text-right"><?= $hasNds ? TextHelper::invoiceMoneyFormat($orderUpd->amountNds, $precision) : Product::DEFAULT_VALUE; ?></td>
    <td class="product-amount text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
    <td style="min-width: 220px">
        <?= \kartik\select2\Select2::widget([
                'name' => "orderParams[{$order->id}][country_id]",
                'value' => $orderUpd->country_id,
                'data' => $countryList,
                'options' => [
                    'class' => 'form-control',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%'
                ],
            ]
        ); ?>
    </td>
    <td>
        <?= Html::textInput(
            "orderParams[{$order->id}][custom_declaration_number]",
            $orderUpd->custom_declaration_number,
            [
                'class' => 'form-control',
            ]
        ); ?>
    </td>
    <td>
        <button class="delete-row button-clr" type="button">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
            </svg>
        </button>
    </td>
</tr>
