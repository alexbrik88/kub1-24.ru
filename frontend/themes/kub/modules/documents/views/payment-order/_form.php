<?php

use common\components\date\DateHelper;
use common\components\TaxRobotHelper;
use common\components\widgets\BikTypeahead;
use common\models\cash\CashBankFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\OperationType;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\employee\EmployeeRole;
use frontend\themes\kub\components\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */
/* @var $form yii\bootstrap4\ActiveForm */

$taxRobot = new TaxRobotHelper($company);
$companyIfns = Json::encode($model->company->ifns);

$companyStrictMode = $company->strict_mode == Company::ON_STRICT_MODE;

$rsItems = [];
$rsOptions = [];
$accountsArray = $model->company->getCheckingAccountants()
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->all();

foreach ($accountsArray as $account) {
    $rsItems[$account->rs] = $account->rs;
    $rsOptions[$account->rs] = [
        'data' => [
            'bank' => $account->bank_name,
            'city' => $account->bank_city,
            'bik' => $account->bik,
            'ks' => $account->ks,
        ]
    ];
}

$statusList = ArrayHelper::map(TaxpayersStatus::find()->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->all(), 'id', 'name');
$operationTypeList = ArrayHelper::map(OperationType::find()->all(), 'id', 'name');
$paymentDetailsList = ArrayHelper::map(PaymentDetails::find()->all(), 'id', 'code');
$paymentTypeList = ArrayHelper::map(PaymentType::find()->all(), 'id', 'code');

if (!isset($cancelUrl)) {
    $cancelUrl = ($model->isNewRecord) ? [
        '/documents/payment-order/index',
    ] : [
        '/documents/payment-order/view',
        'id' => $model->id,
    ];
}

$this->registerJs("
PaymentOrder.ifns = {$companyIfns};
");
?>
<?php $form = ActiveForm::begin([
    'id' => 'asd',
    'fieldConfig' => [
        'template' => "{input}",
        'options' => [
            'class' => '',
        ],
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]); ?>

    <div class="wrap">
        <?= Html::activeHiddenInput($model, 'invoiceIDs'); ?>

        <?= $form->errorSummary($model); ?>

        <div id="connection-to-account" class="no_mrg_bottom">
            <?= $form->field($model, 'relation_with_in_invoice')->label('Связано со счётом', [
                'class' => 'checkbox-inline'
            ])->checkbox([
                'id' => 'relation-with-invoice-checkbox',
                'data-toggle' => 'accounts-list',
                'class' => 'm-l-0',
                'data-contractor' => $model->relation_with_in_invoice && $model->contractor_id ?
                    $model->contractor_id : null,
            ]); ?>

            <span class="bold-text" id="relation-invoice-span"></span>
        </div>

        <?= $form->field($model, 'presence_status_budget_payment')->label('Бюджетный платёж', [
            'class' => 'checkbox-inline',
        ])->checkbox([
            'id' => 'budget-payment-checkbox',
            'class' => 'm-l-1'
        ]) ?>

        <table style="width: 100%; table-layout: fixed;">
            <tbody>
                <tr>
                    <td width="25%">
                        Платежное поручение №
                    </td>
                    <td width="15%" class="">
                        <?= $form->field($model, 'document_number')->textInput([
                            'maxlength' => 45,
                            'style' => 'width: 100%',
                        ])->label(false) ?>
                    </td>
                    <td width="5%"></td>
                    <td width="20%" style="text-align: center;">
                        <div style="position: relative;">
                            <?= $form->field($model, 'document_date')->textInput([
                                'class' => 'form-control date-picker ico',
                                'style' => 'font-weight: 400; font-size: 16px;',
                                'size' => 16,
                                'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label(false); ?>
                        </div>
                    </td>
                    <td width="5%"></td>
                    <td width="15%"></td>
                    <td width="15%">
                        <?= $form->field($model, 'taxpayers_status_id')->widget(Select2::class, [
                            'data' => $statusList,
                            'options' => [
                                'class' => 'form-control js_select pull-right',
                                'prompt' => '',
                                'data-default-value' => $model->company->company_type_id == CompanyType::TYPE_IP ?
                                    TaxpayersStatus::DEFAULT_IP :
                                    TaxpayersStatus::DEFAULT_OOO,
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                                'dropdownCssClass' => 'paymentorder-taxpayers_status_id-dropdown'
                            ]

                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td style="border-top: 1px solid black; text-align: center;">
                        дата
                    </td>
                    <td></td>
                    <td style="border-top: 1px solid black; text-align: center;">
                        вид платежа
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <table class="paymentorder-table" style="width: 100%; table-layout: fixed;">
            <tbody>
                <tr class="bor0-t">
                    <td width="10%" class="bor0-l">
                        Сумма прописью
                    </td>
                    <td width="90%" colspan="12" class="bor0-r va-t summery__sum-in-words"></td>
                </tr>
                <tr>
                    <td width="26%" colspan="3" class="bor0-l">
                        ИНН <span class="company_inn"><?= $company->inn ?></span>
                        <?php if ($companyStrictMode && !$company->inn) : ?>
                            <?= Html::button('Заполнить ваши реквизиты по ИНН', [
                                'class' => 'btn yellow first-invoice-company company_inn_button',
                                'style' => 'padding-top:4px;padding-bottom:4px;margin-top:-3px;',
                            ]) ?>
                        <?php endif ?>
                    </td>
                    <td width="28%" colspan="3">
                        КПП <span class="company_kpp"><?= $company->kpp; ?></span>
                    </td>
                    <td width="9%" colspan="2" rowspan="2" class="va-t">
                        Сумма
                    </td>
                    <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r">
                        <?= $form->field($model, 'sum')->textInput([
                            'class' => 'summery__sum-in-digits form-control js_input_to_money_format',
                            'value' => $model->sum / 100,
                            'data-default-value' => $taxRobot->getTaxPayableAmount() / 100,
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="60px">
                        <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                    </td>
                </tr>
                <tr>
                    <td width="9%" colspan="2" rowspan="2" class="va-t">
                        Сч. №
                    </td>
                    <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-b">
                        <?= $form->field($model, 'company_rs', [
                            'template' => "{input}",
                            'options' => [
                                'class' => '',
                            ],
                        ])->widget(Select2::classname(), [
                            'data' => $rsItems,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '100%',
                                'templateResult' => new JsExpression('function (data, container) {
                                    if (data.id && data.element) {
                                        container.innerHTML = $(data.element).data("bank") + "<br>" + data.id;
                                    }
                                    return container;
                                }'),
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'data-update-url' => Url::to(['update-checking-accountant']),
                                'data-create-url' => Url::to(['create-checking-accountant']),
                                'options' => $rsOptions,
                            ]
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" class="bor0-l bor0-t">
                        Плательщик
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="60px">
                        <?= $form->field($model, 'company_bank_name')->textInput([
                            'readonly' => true,
                        ])->label(false); ?>
                        <?= $form->field($model, 'company_bank_city')->hiddenInput()->label(false); ?>
                    </td>
                    <td width="9%" colspan="2">
                        БИК
                    </td>
                    <td width="37%" colspan="5" class="bor0-r bor0-t bor0-b">
                        <?php if ($companyStrictMode && !$model->company_bik) {
                            echo Html::button('Заполнить ваш банк по БИК', [
                                'class' => 'btn yellow first-invoice-company company_bank_button',
                                'style' => 'padding-top:4px;padding-bottom:4px;margin-top:-3px;']);
                        } ?>
                        <?= $form->field($model, 'company_bik')->textInput([
                            'readonly' => true,
                            'style' => ($companyStrictMode && !$model->company_bik) ? 'display:none' : ''
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="9%" colspan="2" rowspan="2" class="va-t">
                        Сч. №
                    </td>
                    <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-t">
                        <?= $form->field($model, 'company_ks')->textInput([
                            'readonly' => true,
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" class="bor0-l bor0-t">
                        Банк плательщика
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="60px">
                        <?= $form->field($model, 'contractor_bank_name')->textInput([
                            'readonly' => true,
                            'class' => 'form-control',
                        ])->label(false); ?>
                        <?= $form->field($model, 'contractor_bank_city')->hiddenInput()->label(false); ?>
                    </td>
                    <td width="9%" colspan="2">
                        БИК
                    </td>
                    <td width="37%" colspan="5" class="bor0-r bor0-b">
                        <?= $form->field($model, 'contractor_bik')->label(false)->widget(BikTypeahead::classname(), [
                            'remoteUrl' => Url::to(['/dictionary/bik']),
                            'related' => [
                                '#paymentorder-contractor_bank_name' => 'name',
                                '#paymentorder-contractor_bank_city' => 'city',
                                '#paymentorder-contractor_corresponding_account' => 'ks',
                            ],
                            'options' => [
                                'id' => 'paymentorder-contractor_bik',
                                'class' => 'form-control',
                                'autocomplete' => 'off',
                                'style' => ($companyStrictMode && !$model->contractor_bik) ? 'display:none':''
                            ],
                        ]); ?>
                        <?php if ($companyStrictMode && !$model->contractor_bik) : ?>
                            <?= Html::button('Заполнить банк получателя', [
                                'class' => 'btn yellow payment-order-add-contractor contractor_bank_button',
                                'style' => 'position:absolute;top:0;padding-top:4px;padding-bottom:4px;'
                            ]) ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr>
                    <td width="9%" colspan="2" rowspan="2" class="va-t">
                        Сч. №
                    </td>
                    <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-t">
                        <?= $form->field($model, 'contractor_corresponding_account')->textInput([
                            'readonly' => true,
                            'class' => 'form-control',
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" class="bor0-l bor0-t">
                        Банк получателя
                    </td>
                </tr>
                <tr>
                    <td width="26%" colspan="3" class="bor0-l">
                        <div class="d-flex align-items-center">
                            <span>ИНН</span>
                            <?= $form->field($model, 'contractor_inn', [
                                'options' => [
                                    'class' => 'flex-grow-1 ml-2'
                                ],
                            ])->label('ИНН', [
                                'class' => 'control-label',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'form-control field-width contractor_bank_config',
                                'style' => 'padding-bottom: 2px;',
                            ]) ?>
                        </div>
                        <?php if ($companyStrictMode && !$model->contractor_inn) : ?>
                            <?= Html::button('Заполнить реквизиты получателя', [
                                'class' => 'btn yellow payment-order-add-contractor contractor_inn_button',
                                'style' => 'position:absolute;top:0;padding-top:4px;padding-bottom:4px;',
                            ]) ?>
                        <?php endif ?>
                    </td>
                    <td width="28%" colspan="3">
                        <div class="d-flex align-items-center">
                            <span>КПП</span>
                            <?= $form->field($model, 'contractor_kpp', [
                                'options' => [
                                    'class' => 'flex-grow-1 ml-2'
                                ],
                            ])->label('ИНН', [
                                'class' => 'control-label',
                            ])->textInput([
                                'maxlength' => true,
                                'class' => 'form-control field-width contractor_bank_config',
                                'style' => 'padding-bottom: 2px;',
                            ]) ?>
                        </div>
                    </td>
                    <td width="9%" colspan="2" rowspan="2" class="va-t" height="60px">
                        Сч. №
                    </td>
                    <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r">
                        <?= $form->field($model, 'contractor_current_account')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control',
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" rowspan="3" class="va-t bor0-l bor0-b" height="60px">
                        <?= $form->field($model, 'contractor_name', [
                            'options' => [
                                'class' => 'form-group contractor-text-name' .
                                    ($model->relation_with_in_invoice ? ' hidden' : null),
                                'style' => 'max-width: 480px;',
                            ],
                        ])->textInput([
                            'class' => 'form-control',
                        ])->label(false); ?>

                        <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                            'class' => 'contractor-list-name' .
                                (!$model->relation_with_in_invoice ? ' hidden' : null),
                        ]])->widget(Select2::classname(), [
                            'data' => Contractor::getALLContractorList(Contractor::TYPE_SELLER, false),
                            'options' => [
                                'class' => 'form-control contractor-select',
                                'disabled' => !$model->relation_with_in_invoice,
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ]); ?>
                    </td>
                </tr>
                <tr>
                    <td width="9%" colspan="2">
                        Вид оп.
                    </td>
                    <td width="11%">
                        <?= $form->field($model, 'operation_type_id')->widget(Select2::class, [
                            'hideSearch' => true,
                            'data' => $operationTypeList,
                            'options' => [
                                'class' => 'form-control',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                                'dropdownCssClass' => 'paymentorder-operation_type_id-dropdown'
                            ]
                        ])->label(false); ?>
                    </td>
                    <td width="12%" colspan="2">
                        Срок пл.
                    </td>
                    <td width="14%" colspan="2" class="bor0-r">
                        <div style="position: relative;">
                            <?= $form->field($model, 'payment_limit_date')->textInput([
                                'class' => 'date-picker',
                                'style' => 'line-height: normal;',
                                'size' => 16,
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                                'autocomplete' => 'off',
                                'value' => DateHelper::format(
                                    $model->payment_limit_date,
                                    DateHelper::FORMAT_USER_DATE,
                                    DateHelper::FORMAT_DATE
                                ),
                            ])->label(false); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="9%" colspan="2">
                        Наз. пл.
                    </td>
                    <td width="11%"></td>
                    <td width="12%" colspan="2">
                        Очер. пл.
                    </td>
                    <td width="14%" colspan="2" class="bor0-r">
                        <?= $form->field($model, 'ranking_of_payment')->textInput([
                            'class' => 'form-control pad-n',
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr>
                    <td width="54%" colspan="6" class="bor0-l bor0-t">
                        Получатель
                    </td>
                    <td width="9%" colspan="2">
                        Код
                    </td>
                    <td width="11%">
                        <?= $form->field($model, 'uin_code')->textInput([
                            'class' => 'form-control budget-payment pad-n',
                            'maxlength' => true,
                            'data-default-value' => '0',
                        ])->label(false); ?>
                    </td>
                    <td width="12%" colspan="2">
                        Рез. поле
                    </td>
                    <td width="14%" colspan="2" class="bor0-r"></td>
                </tr>
                <tr>
                    <td width="23%" colspan="2" class="bor0-l">
                        <?= $form->field($model, 'kbk')->label(false)->textInput([
                            'maxlength' => true,
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'class' => 'form-control budget-payment',
                            'data-default-value' => '18210501011011000110',
                        ]); ?>
                    </td>
                    <td width="17%" colspan="2">
                        <?= $form->field($model, 'oktmo_code')->textInput([
                            'maxlength' => true,
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'class' => 'form-control budget-payment',
                            'data-default-value' => $model->company->oktmo ?: ($model->company->okato ?: 0),
                        ])->label(false); ?>
                    </td>
                    <td width="6%" style="padding-left: 2px; padding-right: 2px;">
                        <?= $form->field($model, 'payment_details_id')->dropDownList($paymentDetailsList, [
                            'prompt' => '',
                            'class' => 'form-control budget-payment',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => PaymentDetails::DETAILS_1,
                        ])->label(false); ?>
                    </td>
                    <td width="14%" colspan="2">
                        <?= $form->field($model, 'tax_period_code')->textInput([
                            'class' => 'form-control budget-payment pad-n',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => $taxRobot->getUsn6PeriodCode(),
                        ])->label(false); ?>
                    </td>
                    <td width="20%" colspan="3">
                        <?= $form->field($model, 'document_number_budget_payment')->textInput([
                            'class' => 'form-control budget-payment pad-n',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => '0',
                        ])->label(false); ?>
                    </td>
                    <td width="14%" colspan="2">
                        <div style="position: relative;">
                            <?= $form->field($model, 'dateBudgetPayment')->textInput([
                                'class' => 'form-control date-picker budget-payment',
                                'style' => 'line-height: normal;',
                                'size' => 16,
                                'data' => [
                                    'date-viewmode' => 'years',
                                ],
                                'disabled' => (boolean) $model->relation_with_in_invoice,
                                'data-default-value' => '0',
                            ])->label(false); ?>
                        </div>
                    </td>
                    <td width="6%" class="bor0-r">
                        <?= $form->field($model, 'payment_type_id')->dropDownList($paymentTypeList, [
                            'prompt' => '',
                            'class' => 'form-control budget-payment',
                            'disabled' => (boolean) $model->relation_with_in_invoice,
                            'data-default-value' => PaymentType::TYPE_0,
                        ])->label(false); ?>
                    </td>
                </tr>
                <tr class="">
                    <td width="10%" colspan="13" class="bor0-l bor0-r">
                        <?= $form->field($model, 'purpose_of_payment')->textarea([
                            'class' => 'form-control',
                            'data-default-value' => 'Налог, взимаемый с налогоплательщиков, выбравших в качестве объекта налогообложения доходы',
                        ])->label(false); ?>
                        <div>
                            Назначение платежа
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

        <?php if ($companyStrictMode) : ?>
            <?= Html::hiddenInput('payment_order_new_contractor_id', 0, [
                'id' => 'payment_order_new_contractor_id',
            ]) ?>
        <?php endif ?>
    </div>

    <div class="wrap wrap_btns visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'min-w-130 button-regular button-regular_red button-clr pl-4 pr-4 ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', isset($cancelUrl) ? $cancelUrl : ['index'], [
                    'class' => 'button-clr button-width button-regular button-hover-transparent pl-4 pr-4',
                ]); ?>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<?= $this->render('partial/_in_invoice_table', [
    'model' => $model,
    'company' => $company,
]) ?>

<div id="many-charge" class="confirm-modal fade modal" role="dialog"
     tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 text-left many-charge-text" style="font-size: 15px;">
                            <span style="display: block;">Вы уверены, что хотите подготовить платежки в банк:</span>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                            'class' => 'btn darkblue pull-right modal-many-charge ladda-button',
                            'data-url' => Url::to(['/documents/payment-order/many-create',]),
                            'data-style' => 'expand-right',
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue"
                                data-dismiss="modal">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $("#relation-with-invoice-checkbox").click(function () {
        var $contractorNameText = $(".contractor-text-name");
        var $contractorNameList = $(".contractor-list-name");
        if ($(this).is(":checked")) {
            $contractorNameText.addClass("hidden");
            $contractorNameList.removeClass("hidden");
            $contractorNameList.find("select").removeAttr("disabled");
        } else {
            $contractorNameText.removeClass("hidden");
            $contractorNameList.addClass("hidden");
            $contractorNameList.find("select").attr("disabled", true);
        }
    });

    $(".contractor-list-name select").change(function () {
        $.post("get-contractor-data", {
            id: $(this).val()
        }, function ($data) {
            if ($data.result == true) {
                for (var $i in $data.contractor) {
                    if ($i !== "id") {
                        $("#paymentorder-" + $i).val($data.contractor[$i]);
                    }
                }
            }
        });
    });
    $(document).on("change", "#paymentorder-company_rs", function () {
        var form = this.form;
        var rs = $(":selected", this);
        if ($(this).val() && rs) {
            $("#paymentorder-company_bank_name", form).val(rs.data("bank"));
            $("#paymentorder-company_bank_city", form).val(rs.data("city"));
            $("#paymentorder-company_bik", form).val(rs.data("bik"));
            $("#paymentorder-company_ks", form).val(rs.data("ks"));
        }
    });
'); ?>
