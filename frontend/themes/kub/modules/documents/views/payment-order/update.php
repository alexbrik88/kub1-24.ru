<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

//$this->title = 'Обновить платёжное поручение: ' . ' ' . $model->document_number;
$this->params['breadcrumbs'][] = ['label' => 'Payment Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]) ?>
</div>
