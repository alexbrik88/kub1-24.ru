<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\PaymentOrder;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\themes\kub\modules\documents\widgets\PaymentOrderPaid;
use frontend\rbac\permissions;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\PaymentOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платёжные поручения';
$this->params['breadcrumbs'][] = $this->title;

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_payment_order');
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
            'create',
        ], [
            'class' => 'button-regular button-regular_red button-width ml-auto',
        ]) ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3">
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= Html::a(Icon::get('exel'), Url::current(['xls' => 1]), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
                'data-pjax' => 0,
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_payment_order']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер платежного поручения или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?php yii\widgets\Pjax::begin(['id' => 'payment-order-search-pjax']); ?>
    <?= common\components\grid\GridView::widget([
        'id' => 'payment-order-search',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'Вы еще не создали ни одного платежного поручения.',
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (PaymentOrder $model) {
                    return Html::checkbox('PaymentOrder[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->sum,
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата ПП',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentOrder $data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => PaymentOrder::tableName() . '.`document_number` * 1',
                'label' => '№ ПП',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'format' => 'raw',
                'value' => function (PaymentOrder $data) {
                    return Html::a($data->document_number, ['payment-order/view', 'id' => $data->id], [
                        'data-pjax' => 0,
                    ]);
                },
            ],
            [
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '9%',
                ],
                'format' => 'raw',
                'attribute' => 'sum',
                'value' => function (PaymentOrder $data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->sum, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'contractor_name',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'contentOptions' => [
                    'style' => 'max-width:300px',
                ],
                'filter' => array_merge(['' => 'Все'], ArrayHelper::map($searchModel->contractorArray, 'contractor_name', 'contractor_name')),
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (PaymentOrder $model) {
                    return '<span title="' . htmlspecialchars($model->contractor_name) . '">' . $model->contractor_name . '</span>';
                },
            ],
            [
                'attribute' => 'payment_order_status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getStatusFilter(),
                's2width' => '200px',
                'format' => 'raw',
                'value' => function (PaymentOrder $model) {
                    return $model->paymentOrderStatus->name;
                },
            ],
            [
                'attribute' => 'id',
                'label' => 'Счета в ПП',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function (PaymentOrder $data) {
                    $result = null;
                    if ($data->paymentOrderInvoices) {
                        foreach ($data->getPaymentOrderInvoices()
                                     ->joinWith('invoice')
                                     ->orderBy([
                                         'cast(document_number as unsigned)' => SORT_ASC,
                                     ])
                                     ->all() as $paymentOrderInvoice) {
                            if ($paymentOrderInvoice->invoice !== null) {
                                $invoiceNumber = '№ ' . $paymentOrderInvoice->invoice->fullNumber;
                                $result .= '<div>'.(Yii::$app->user->can(permissions\document\Document::VIEW, [
                                    'model' => $paymentOrderInvoice->invoice,
                                ]) ? Html::a($invoiceNumber, [
                                    '/documents/invoice/view',
                                    'type' => $paymentOrderInvoice->invoice->type,
                                    'id' => $paymentOrderInvoice->invoice_id,
                                ], [
                                    'data-pjax' => 0,
                                ]) : $invoiceNumber).'</div>';
                            }
                        }
                    }

                    return $result ?? '--';
                },
            ],
            [
                'attribute' => 'document_author_id',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getAuthorFilter(),
                's2width' => '200px',
                'format' => 'raw',
                'value' => function (PaymentOrder $model) {
                    return $model->documentAuthor->getFio(true);
                },
            ],
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => Documents::IO_TYPE_IN,
            'multiple' => '',
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
            'data-pjax' => 0,
        ]) : null,
        $canPrint ? Html::a('Экспорт в банк', ['import'], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-import-link no-ajax-loading',
            'data-url' => Url::to(['import']),
            'data-pjax' => 0,
        ]) : null,
        $canStatus ? Html::a('Оплачено', '#many-paid', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0,
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0,
        ]) : null,
    ],
]); ?>

<?php if ($canStatus) : ?>
    <?= PaymentOrderPaid::widget([
        'id' => 'many-paid',
        'toggleButton' => false,
    ]) ?>
<?php endif ?>

<?php Modal::begin([
    'id' => 'many-delete',
    'closeButton' => false,
]); ?>

<?php if ($canDelete): ?>
    <?= \yii\bootstrap4\Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
        'aria-label' => 'Close',
    ]) ?>
    <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные платежные поручения?</h4>
    <div class="text-center">
        <?= Html::button('ДА', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
            'data-url' => Url::to(['many-delete']),
        ]); ?>
        <?= Html::button('НЕТ', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>
<?php endif; ?>