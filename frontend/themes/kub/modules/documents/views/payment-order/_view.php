<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use php_rutils\RUtils;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

?>
<div class="paymentorder-view">
    <div class="row"  style="margin-bottom: 20px;margin-top: 0;">
        <div class="column" style="margin-right: 7%;">
            <div class="data-block__data" style="height: 20px;width: 100%;font-weight: bold;">
            </div>
            <div class="data-block__title" style="font-weight: 400; border-top: 1px solid black; text-align: center; font-size: 13px">
                Поступ. в банк плат.
            </div>
        </div>
        <div class="column">
            <div class="data-block__data" style="height: 20px;width: 100%;font-weight: bold;">
            </div>
            <div class="data-block__title" style="font-weight: 400; border-top: 1px solid black; text-align: center; font-size: 13px">
                Списано со сч. плат.
            </div>
        </div>
        <div class="column ml-auto" style="margin: 0; font-size: 13px">
            <div style="font-weight: 400; padding: 3px 10px; border: 1px solid #000; min-width: 26px;">
                0401060
            </div>
        </div>
    </div>
    <table style="width: 100%; margin-bottom: 30px;">
        <tbody>
            <tr style="font-size: 18px; font-weight: bold;">
                <td width="50%" style="white-space: nowrap;">
                    Платежное поручение №
                    <?= $model->document_number ?>
                </td>
                <td width="18%" style="text-align: center;">
                    <?= DateHelper::format(
                        $model->document_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    ) ?>
                </td>
                <td width="4%"></td>
                <td width="18%"></td>
                <td width="10%" style="text-align: right;">
                    <span style="padding: 3px 6px; border: 1px solid black; font-weight: normal;">
                        <?= ArrayHelper::getValue($model, 'taxpayersStatus.code') ?: "&nbsp;&nbsp;&nbsp;&nbsp;" ?>
                    </span>
                </td>
            </tr>
            <tr>
                <td></td>
                <td style="border-top: 1px solid black; text-align: center;">
                    дата
                </td>
                <td></td>
                <td style="border-top: 1px solid black; text-align: center;">
                    вид платежа
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <table class="paymentorder-table">
        <tbody>
            <tr class="bor0-t">
                <td width="10%" class="bor0-l va-t" height="60px">
                    Сумма прописью
                </td>
                <td width="90%" colspan="12" class="bor0-r va-t" height="60px">
                    <?= $model->sum_in_words; ?>
                </td>
            </tr>
            <tr>
                <td width="26%" colspan="3" class="bor0-l">
                    ИНН <?= $model->company_inn; ?>
                </td>
                <td width="28%" colspan="3">
                    КПП <?= !empty($model->company_kpp) ? $model->company_kpp : ''; ?>
                </td>
                <td width="9%" colspan="2" rowspan="2" class="va-t">
                    Сумма
                </td>
                <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r">
                    <?= \common\components\TextHelper::invoiceMoneyFormat($model->sum, 2); ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="80px">
                    <?= $company->getTitle($company->company_type_id == CompanyType::TYPE_IP ? false : true); ?>
                </td>
            </tr>
            <tr>
                <td width="9%" colspan="2" rowspan="2" class="va-t">
                    Сч. №
                </td>
                <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-b break-word">
                    <?= $model->company_rs ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" class="bor0-l bor0-t bold">
                    Плательщик
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="40px">
                    <?= $model->company_bank_name ?>
                </td>
                <td width="9%" colspan="2">
                    БИК
                </td>
                <td width="37%" colspan="5" class="bor0-r bor0-t bor0-b">
                    <?= $model->company_bik ?>
                </td>
            </tr>
            <tr>
                <td width="9%" colspan="2" rowspan="2" class="va-t">
                    Сч. №
                </td>
                <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-t break-word">
                    <?= $model->company_ks ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" class="bor0-l bor0-t bold">
                    Банк плательщика
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" rowspan="2" class="va-t bor0-l bor0-b" height="40px">
                    <?= $model->contractor_bank_name ?>
                </td>
                <td width="9%" colspan="2">
                    БИК
                </td>
                <td width="37%" colspan="5" class="bor0-r bor0-b">
                    <?= $model->contractor_bik ?>
                </td>
            </tr>
            <tr>
                <td width="9%" colspan="2" rowspan="2" class="va-t">
                    Сч. №
                </td>
                <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r bor0-t break-word">
                    <?= $model->contractor_corresponding_account ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" class="bor0-l bor0-t bold">
                    Банк получателя
                </td>
            </tr>
            <tr>
                <td width="26%" colspan="3" class="bor0-l">
                    ИНН <?= $model->contractor_inn ?>
                </td>
                <td width="28%" colspan="3">
                    КПП <?= $model->contractor_kpp ?>
                </td>
                <td width="9%" colspan="2" rowspan="2" class="va-t break-word">
                    Сч. №
                </td>
                <td width="37%" colspan="5" rowspan="2" class="va-t bor0-r">
                    <?= $model->contractor_current_account ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" rowspan="3" class="va-t bor0-l bor0-b" height="80px">
                    <?= $model->contractor_name ?>
                </td>
            </tr>
            <tr>
                <td width="9%" colspan="2">
                    Вид оп.
                </td>
                <td width="11%">
                    <?= ArrayHelper::getValue($model, 'operationType.code') ?>
                </td>
                <td width="12%" colspan="2">
                    Срок пл.
                </td>
                <td width="14%" colspan="2" class="bor0-r">
                    <?= DateHelper::format(
                        $model->payment_limit_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    ) ?>
                </td>
            </tr>
            <tr>
                <td width="9%" colspan="2">
                    Наз. пл.
                </td>
                <td width="11%"></td>
                <td width="12%" colspan="2">
                    Очер. пл.
                </td>
                <td width="14%" colspan="2" class="bor0-r">
                    <?= $model->ranking_of_payment ?>
                </td>
            </tr>
            <tr>
                <td width="54%" colspan="6" class="bor0-l bor0-t bold">
                    Получатель
                </td>
                <td width="9%" colspan="2">
                    Код
                </td>
                <td width="11%">
                    <?= $model->uin_code ?>
                </td>
                <td width="12%" colspan="2">
                    Рез. поле
                </td>
                <td width="14%" colspan="2" class="bor0-r"></td>
            </tr>
            <tr>
                <td width="23%" colspan="2" class="bor0-l">
                    <?= $model->kbk ?>&nbsp;
                </td>
                <td width="17%" colspan="2">
                    <?= $model->oktmo_code ?>
                </td>
                <td width="6%" style="padding-left: 2px; padding-right: 2px;">
                    <?= ArrayHelper::getValue($model, 'paymentDetails.code') ?>
                </td>
                <td width="14%" colspan="2">
                    <?= $model->tax_period_code; ?>
                </td>
                <td class="hidden-1220" width="20%" colspan="3">
                    <?= $model->document_number_budget_payment ?>
                </td>
                <td class="showing-1220" width="10%" colspan="2">
                    <?= $model->document_number_budget_payment ?>
                </td>
                <td width="14%" colspan="2">
                    <?php if ($model->document_date_budget_payment !== null) : ?>
                        <?= RUtils::dt()->ruStrFTime([
                            'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                            'date' => $model->document_date_budget_payment,
                        ]) ?>
                    <?php elseif ($model->presence_status_budget_payment) : ?>
                        0
                    <?php endif ?>
                </td>
                <td width="6%" class="bor0-r">
                    <?= ArrayHelper::getValue($model, 'paymentType.code') ?>
                </td>
            </tr>
            <tr class="">
                <td width="10%" colspan="13" class="bor0-l bor0-r">
                    <div style="min-height: 60px;">
                        <?= $model->purpose_of_payment ?>
                    </div>
                    <div>
                        Назначение платежа
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="width: 100%;">
        <tbody>
            <tr>
                <td width="33%" height="60px"></td>
                <td width="33%" class="bor1-b" style="vertical-align: top; text-align: center; font-size: 13px;">
                    Подписи
                </td>
                <td width="33%" style="vertical-align: top; text-align: center; font-size: 13px;">
                    Отметки банка
                </td>
            </tr>
            <tr>
                <td width="33%" height="60px;" style="vertical-align: top; text-align: center;">
                    М.П.
                </td>
                <td width="33%" class="bor1-b"></td>
                <td width="33%"></td>
            </tr>
        </tbody>
    </table>
</div>
