<?php


use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use yii\helpers\Html;
use \common\components\helpers\Month;
use common\models\document\AgentReport;

/* @var $this yii\web\View */
/* @var $contractor \common\models\Contractor */
/* @var $model AgentReport */
/* @var $multiple [] */

$this->title = $model->getPdfPrintTitle();
$this->context->layoutWrapperCssClass = 'out-invoice out-document';


/* @var string $dateFormatted */
/* @var $this yii\web\View */
/* @var $model common\models\document\AgentReport */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $agent Contractor */
/* @var $company \common\models\Company */
/** @var $order \common\models\document\AgentReportOrder */

$company = $model->company;
$agent = $model->agent;

$isCompanyIP = $company->company_type_id == \common\models\company\CompanyType::TYPE_IP;
$isAgentIP = $agent->company_type_id == \common\models\company\CompanyType::TYPE_IP;
$agentAgreementTitle = $model->agreement ? '№ '.$model->agreement->fullNumber.' от '.DateHelper::format($model->agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
$startDate = DateHelper::format($model->document_date, '01.m.Y', DateHelper::FORMAT_DATE);
$endDate = DateHelper::format($model->document_date, 't.m.Y', DateHelper::FORMAT_DATE);

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$director_name = $agent->getDirectorFio();

$hasNds = $agent->taxation_system == Contractor::WITH_NDS;
$isNdsExclude = false;
$ndsName = ($hasNds) ? 'В том числе НДС' : 'Без налога (НДС)';
$ndsValue = $hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-';
$precision = 2;

$agent_director_name = '';
if (!empty($agent->director_name) && $agent->director_name != 'ФИО Руководителя') {
    $agent_director_name = TextHelper::nameShort($agent->director_name);
}

//$signatureLink = (!$contractor->company->chief_signature_link) ? null :
//    EasyThumbnailImage::thumbnailSrc($contractor->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
//
//$printLink = (!$contractor->company->print_link) ? null :
//    EasyThumbnailImage::thumbnailSrc($contractor->company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);
//
//$logoLink = !$contractor->company->logo_link ? null :
//    EasyThumbnailImage::thumbnailSrc($contractor->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);
//
//$images = [
//    'logo' => $logoLink,
//    'print' => $printLink,
//    'signature' => $signatureLink
//];
?>
    <style type="text/css">
        <?php if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            echo $this->renderFile($file);
        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            echo $this->renderFile($file);
        } ?>
    </style>

    <style media="print">
        @page {
            size: portrait;
            margin: 1cm auto;
        }
    </style>

    <div class="page-content-in p-center pad-pdf-p portrait" style="box-sizing: content-box;">
        <h4>Отчет агента № <?= $model->fullNumber; ?></h4>
        <?php if ($agentAgreementTitle): ?>
            <div style="text-align:center;font-size:9pt;font-weight:bold">
                По агентскому договору
                <?= $agentAgreementTitle ?>
            </div>
        <?php endif; ?>

        <div class="text-right" style="font-size:9pt;"><?=(DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE))?></div>

        <div style="margin:15px 0; font-size:9pt">
            <?=($company->getShortName()) ?>, именуемое далее «Принципал», в лице <?= $company->chief_post_name ?> <?= $company->getChiefFio() ?>,
            <?=(!$isCompanyIP ? 'действующего на основании Устава' : '')?>, с одной стороны и
            <?=($agent->getShortName())?>, именуемое далее «Агент», в лице <?= $agent->director_post_name ?> <?= $agent_director_name ?>
            <?=(!$isAgentIP ? 'действующего на основании Устава' : '')?>, с другой стороны, в дальнейшем именуемые Стороны, настоящим Отчетом удостоверяют, что
            <?=($agentAgreementTitle ? "в соответствии с условиями Договора {$agentAgreementTitle}, " : '')?>
            в период с <?=$startDate?> по <?=$endDate?>, были получены оплаты от клиентов Агента:
        </div>

        <table class="table table-bordered " style="margin-top:15px; width:100%;">
            <tr>
                <th class="text-center" width="5%"> №</th>
                <th class="text-center" width="40%"> Покупатель</th>
                <th class="text-center" width="25%"> Сумма оплат за месяц (руб.)</th>
                <th class="text-center" width="15%"> Комиссионное вознаграждение %</th>
                <th class="text-center" width="15%"> Комиссионное вознаграждение (руб.)</th>
            </tr>
            <?php foreach ($model->agentReportOrders as $key => $order): ?>
                <?php if ($order->is_exclude) continue; ?>
                <tr>
                    <td style="text-align: center;"><?= $key + 1; ?></td>
                    <td style="text-align: left;"><?= $order->contractor->getShortName(); ?></td>
                    <td style="text-align: right;"><?= TextHelper::invoiceMoneyFormat($order->payments_sum, $precision) ?></td>
                    <td style="text-align: right;"><?= TextHelper::numberFormat($order->agent_percent, $precision) ?></td>
                    <td style="text-align: right;"><?= TextHelper::invoiceMoneyFormat($order->agent_sum, $precision); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <table class="it-b no-border" style="width: 99.9%; margin-top:10px">
            <tbody>
            <tr>
                <td width="450px" style="border: none"></td>
                <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;">
                    Итого:
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></b>
                </td>
            </tr>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;"><?= $ndsName; ?>
                    :
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;"><?= $ndsValue; ?></td>
            </tr>
            <tr>
                <td width="450px" style="border: none"></td>
                <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;">
                    Всего к оплате:
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br/>
                    Общая сумма вознаграждения по данному отчету
                    <b><?= TextHelper::amountToWords($model->total_sum / 100) ?></b>
                    <?= ($hasNds) ? ', в том числе НДС 20%' : ', НДС не облагается, в связи с применением Упрощенной Системы Налогообложения' ?>.
                    <br/>Услуги оказаны полностью. Стороны не имеют претензий по оказанию услуг.
                </td>
            </tr>
            </tbody>
        </table>
        <div class="col-xs-12" style="margin-top:20px;">
            <div style="height: 200px;">
                <div style="position: relative">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                АГЕНТ
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                ПРИНЦИПАЛ
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $agent->getShortName(); ?></td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="position:relative; z-index:2; width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $company->getShortName(); ?></td>
                        </tr>
                        <tr>
                            <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                                <?= !empty($agent_director_name)? '/ ' . $agent_director_name . ' /': ''; ?>
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                                / <?= $agent->company->getChiefFio(true) ?> /
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>