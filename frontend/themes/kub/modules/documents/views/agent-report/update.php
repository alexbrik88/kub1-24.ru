<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\file\File;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\document\AgentReport */
/* @var $message Message */
/* @var $ioType integer */

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$canUpdate =
    Yii::$app->getUser()->can(permissions\document\Document::UPDATE)
    &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$isAddCan = false;

$agent = $model->agent;
$hasNds = $agent->taxation_system == Contractor::WITH_NDS;
$isNdsExclude = false;
$ndsName = ($hasNds) ? 'В том числе НДС' : 'Без налога (НДС)';
$ndsValue = $hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-';
$ndsPercent = $model->getNdsRate();
$precision = 2;

$this->registerJs(<<<JS
 $(function(){     
    $('.edit').addClass('hide');
    $('.view-action-buttons').addClass('hide');
    $('.editable-field').addClass('hide');
    $('.control-panel-pre-edit').addClass('hide');
    $('.info-button').addClass('hide');    
    $('.btn-save').removeClass('hide');
    $('.btn-cancel').removeClass('hide');
    $('.form-action-buttons').removeClass('hide');
    $('.input-editable-field').removeClass('hide');
})
JS
);
?>
    <div class="page-content-in">

        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

        <?= Html::beginForm('', 'post', [
            'id' => 'edit-agent-report',
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate',
            'enctype' => 'multipart/form-data',
        ]); ?>

        <div class="row">
            <div class="col-xs-12 col-lg-7 col-w-lg-8 col-md-7" style="max-width: 720px;">
                <?= $this->render('_viewPartials/_customer_info_out', [
                    'model' => $model,
                    'message' => $message,
                    'canUpdate' => $canUpdate,
                    'backUrl' => $backUrl
                ]); ?>
            </div>

        </div>
        <div class="portlet">
            <?= Html::hiddenInput("AgentReportOrder") ?>

            <?= $this->render('_viewPartials/_order_list_out', [
                'model' => $model,
                'precision' => $precision,
            ]); ?>
        </div>

        <div class="portlet pull-right">
            <table id="agent-table-resume" class="table table-resume">
                <tbody>
                <tr role="row" class="even">
                    <td><b>Итого:</b></td>
                    <td class="total_sum"><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></td>
                </tr>
                <tr role="row" class="odd">
                    <td>
                        <b>
                            <?php if ($hasNds) : ?>
                                <?php if ($isNdsExclude) : ?>
                                    НДС сверху:
                                <?php else : ?>
                                    В том числе НДС:
                                <?php endif ?>
                            <?php else : ?>
                                Без налога (НДС):
                            <?php endif ?>
                        </b>
                    </td>
                    <td class="total_nds"><?= $hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-'; ?></td>
                </tr>
                <tr role="row" class="even">
                    <td><b>Всего к оплате:</b></td>
                    <td class="total_to_pay"><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?= Html::endForm(); ?>
    </div>

    <?= $this->render('_viewPartials/_action_buttons_out', [
        'model' => $model,
        'canUpdate' => $canUpdate,
        'ioType'    => $ioType
    ]); ?>

    <!-- remove buyer -->
    <div id="modal-remove-one-buyer" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -45px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                        <div class="row">Вы уверены, что хотите удалить эту позицию из отчета?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                    style="width: 80px;color: white;">ДА
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue"
                                    style="width: 80px;color: white;">НЕТ
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

<script>
AGENT_REPORT = {removeBuyerFromReportId: null};

$('.remove-buyer-from-agent-report').on('click', function () {
    $('#modal-remove-one-buyer').modal("show");
    AGENT_REPORT.removeBuyerFromReportId = this;
});

$('#modal-remove-one-buyer .yes').on('click', function() {
    $(AGENT_REPORT.removeBuyerFromReportId).parents('tr').remove();
    recalcOrders();
});

$('.input_agent_percent').on('change', function() {
    recalcOrders();
});

function recalcOrders() {
    var precision = 2;
    var sum, total = 0;
    $('.agent_report_table').find('tr.order').each(function(i,v) {
        var payments_sum = $(v).find('.order_payments_sum').html().replace(',', '.').replace(/[^\d\.]/g, '').split('.', 2).join('.');
        var agent_percent = $(v).find('.input_agent_percent').val().replace(',', '.').replace(/[^\d\.]/g, '').split('.', 2).join('.');
        var sum = (payments_sum * agent_percent / 100);
        $(this).find('.order_agent_sum').html(sum.toFixed(precision));
        total += sum;
    });

    var $resume = $('#agent-table-resume');
    var hasNds = <?=(int)$hasNds?>;
    var ndsPercent = <?=(int)$ndsPercent?>;
    console.log(hasNds, ndsPercent)
    $resume.find('.total_sum').html(total.toFixed(precision));
    $resume.find('.total_nds').html((hasNds) ? ( total * ndsPercent/(100+ndsPercent)).toFixed(precision) : '-');
    $resume.find('.total_to_pay').html(total.toFixed(precision));
}
</script>
