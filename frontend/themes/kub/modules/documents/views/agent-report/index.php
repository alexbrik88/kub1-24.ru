<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\document\Invoice;
use common\models\document\AgentReport;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\AgentReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $agent \common\models\Contractor */
/* @var $ioType int */
/* @var $message Message */

$this->title = $message->get(Message::TITLE_PLURAL) . ' ' . $agent->getShortName();

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = true;

$isFilter = (boolean) ($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет отчетов. Измените период, чтобы увидеть имеющиеся отчеты.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного отчета.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);
?>

<div class="portlet box">
    <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button']); ?>
    <h4 class="page-title"><?= Html::encode($this->title) ?></h4>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список агентских отчетов
        </div>
        <div
            class="search-tools tools tools_button <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-4 col-sm-4' : 'col-md-6 col-sm-6'; ?>">
            <div class="form-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'type' => 'search',
                            'placeholder' => 'Номер отчета или название агента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('Найти', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div
            class="actions joint-operations <?= $ioType == Documents::IO_TYPE_OUT ? 'col-md-4 col-sm-4' : 'col-md-3 col-sm-3'; ?>"
            style="display:none;">

            <?php if ($ioType == \frontend\models\Documents::IO_TYPE_OUT): ?>
                <?php if ($canSend) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                        'class' => 'btn btn-default btn-sm document-many-send',
                        'data-url' => Url::to(['/documents/agent-report/many-send', 'type' => $ioType]),
                    ]); ?>
                    <div class="modal fade confirm-modal" id="many-send-error"
                         tabindex="-1"
                         role="modal"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h4 style="text-align: center; margin: 0">Ошибка
                                        при отправке отчетов</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canPrint) : ?>
                    <?= Html::a('<i class="fa fa-print"></i> Печать', Url::to([
                        '/documents/agent-report/many-document-print',
                        'actionType' => 'pdf',
                        'type' => $ioType,
                        'multiple' => ''
                    ]), [
                        'class' => 'btn btn-default btn-sm multiple-print',
                        'target' => '_blank',
                    ]); ?>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <div class="dataTables_wrapper dataTables_extended_wrapper">
                <div>
                    <?php Pjax::begin([
                        'id' => 'pjax-agent-report',
                        'enablePushState' => false,
                        'timeout' => 5000
                    ]) ?>
                    <?= common\components\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'emptyText' => $emptyMessage,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '1%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $model) {
                                    return Html::checkbox('AgentReport[' . $model->id . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox',
                                    ]);

                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата отчета',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'contentOptions' => [
                                    'class' => 'link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                                },
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ отчета',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'contentOptions' => [
                                    'class' => 'document_number link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                        'model' => $data,
                                    ])
                                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id], ['data-pjax' => 0])
                                        : $data->fullNumber;
                                },
                            ],
                            [
                                'attribute' => 'total_sum',
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'contentOptions' => [
                                    'class' => 'link-view',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->total_sum, 2);
                                    return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                                },
                            ],
                            [
                                //'attribute' => 'contractor_id',
                                'label' => 'Оплатить до',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '10%',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return '';
                                },
                            ],
                            [
                                'attribute' => 'status_out_id',
                                'label' => 'Статус',
                                'class' => DropDownDataColumn::className(),
                                'headerOptions' => [
                                    'class' => 'dropdown-filter',
                                    'width' => '5%',
                                ],
                                'filter' => $searchModel->getStatusArray(),
                                'format' => 'raw',
                                'value' => function (AgentReport $data) {
                                    return ($data->statusOut) ? $data->statusOut->name : '';
                                },
                            ],
                            [
                                'attribute' => 'has_invoice',
                                'label' => 'Счёт',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '5%',
                                ],
                                'format' => 'raw',
                                'value' => function (AgentReport $data) use ($canCreate) {
                                    $content = '';
                                    if ($data->invoice) {
                                        if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])) {
                                            $content .= Html::a($data->invoice->fullNumber, ['/documents/invoice/view', 'type' => Documents::IO_TYPE_IN, 'id' => $data->invoice->id], ['data-pjax' => 0]);
                                        } else {
                                            $content .= $data->invoice->fullNumber;
                                        }
                                    } else {
                                        $content .= Html::a('Добавить', [
                                            '/documents/agent-report/create-invoice',
                                            'reportId' => $data->id,
                                        ], [
                                            'class' => 'btn btn-sm yellow' . ($canCreate ? '' : ' no-rights-link'),
                                            'style' => ($content ? 'margin-top: 5px;' : ''),
                                            'data-pjax' => 0
                                        ]);
                                    }

                                    return $content;
                                },
                                'filter' => ['' => 'Все', 1 => 'Есть счет', 0 => 'Нет счета']
                            ],
                            [
                                'headerOptions' => [
                                    //'class' => 'col_agreement_responsible_employee_id',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'col_agreement_responsible_employee_id',
                                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                                ],
                                'attribute' => 'document_author_id',
                                'label' => 'Ответст&shy;венный',
                                'encodeLabel' => false,
                                'class' => DropDownSearchDataColumn::className(),
                                'value' => function ($data) {
                                    $employee = \common\models\employee\Employee::findOne([
                                        'id' => $data['document_author_id']
                                    ]);

                                    return (!empty($employee)) ? $employee->getShortFio() : '';
                                },
                                'format' => 'raw',
                                'filter' => $searchModel->getResponsibleEmployees()
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>

<?=\frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            '/documents/agent-report/many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['/documents/agent-report/many-send', 'type' => $ioType]),
        ]) : null,
    ],
]);?>
