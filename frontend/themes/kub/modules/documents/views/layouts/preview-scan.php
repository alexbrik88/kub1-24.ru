<?php

use frontend\rbac\UserRole;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;
use yii\helpers\Html;

$this->beginContent('@frontend/themes/kub/views/layouts/main.php');
?>

<?php echo $content ?>

<?php $this->endContent(); ?>
