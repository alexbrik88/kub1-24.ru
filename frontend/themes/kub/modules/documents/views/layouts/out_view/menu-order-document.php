<?php

use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$isDemo = ArrayHelper::getValue($this->params, 'isDemo', false);
/** @var \common\models\document\OrderDocument $orderDocument */
$orderDocument =  ArrayHelper::getValue($this->params, 'orderDocument', false);
$company = $orderDocument->company;
$path = $company->getImage('logoImage');

?>

<header class="header">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end" style="min-height: 40px;">
            <span class="header-logo logo column mr-auto" style="padding-right:0;">
                <?php if (is_file($path)) : ?>
                    <div style="margin-top:-10px; margin-bottom:-10px">
                        <?= ImageHelper::getThumb($path, [200, 54.4], [
                            'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                        ]); ?>
                    </div>
                <?php endif; ?>
            </span>

            <ul class="menu-nav list-clr">
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в PDF', $isDemo ? null :
                        ["/documents/{$controllerId}/download", 'type' => 'pdf', 'uid' => $uid],
                        ['class' => 'menu-nav-link', 'target' => '_blank']) ?>
                </li>
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в WORD', $isDemo ? null :
                        ["/documents/{$controllerId}/download", 'type' => 'docx', 'uid' => $uid],
                        ['class' => 'menu-nav-link', 'target' => '_blank']) ?>
                </li>
                <?php if (!$orderDocument->priceList || $orderDocument->priceList->can_checkout_invoice): ?>
                    <li class="menu-nav-item" style="padding-top:0; margin-top:-1px;">
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'article']) . '<span>Получить счет на оплату</span>', $isDemo ? null :
                            ["/documents/{$controllerId}/out-create-invoice", 'uid' => $uid],
                            ['class' => 'button-clr button-regular button-regular_red', 'target' => '_blank']) ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</header>