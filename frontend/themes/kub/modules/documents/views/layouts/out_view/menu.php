<?php

use common\models\Contractor;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$logoUrl  = Url::to(['/documents/invoice/index', 'type' => 2]);
$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$isDemo = ArrayHelper::getValue($this->params, 'isDemo', false);
?>

<header class="header">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end" style="min-height: 40px;">
            <a class="header-logo logo column d-flex align-items-center mr-auto" href="<?= $logoUrl ?>" style="padding-right:0;">
                <img class="logo-image" src="/img/fav.svg" alt="КУБ"
                    style="height: 40px; width: 40px; margin-right: 10px;" />
                <span class="logo-txt">
                    Выставление счетов
                </span>
            </a>

            <ul class="menu-nav list-clr">
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в PDF', $isDemo ? null : [
                        "/documents/{$controllerId}/download",
                        'type' => 'pdf',
                        'uid' => $uid
                    ], [
                        'class' => 'menu-nav-link',
                        'target' => '_blank',
                    ]) ?>
                </li>
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в WORD', $isDemo ? null : [
                        "/documents/{$controllerId}/download",
                        'type' => 'docx',
                        'uid' => $uid
                    ], [
                        'class' => 'menu-nav-link',
                        'target' => '_blank',
                    ]) ?>
                </li>
                <?php if ($this->params['canSave']) : ?>
                    <li class="menu-nav-item">
                        <?= Html::a('Сохранить в личном кабинете', $isDemo ? null : '#', [
                            'id' =>  $isDemo ? null : 'document-save-link',
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                <?php endif ?>
                <li class="menu-nav-item">
                    <?= Html::a('Оплатить счет', $isDemo ? null : '#', [
                        'id' =>  $isDemo ? null : 'document-pay-link',
                        'class' => 'menu-nav-link',
                    ]) ?>
                </li>
            </ul>
        </div>
    </div>
</header>