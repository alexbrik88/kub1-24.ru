<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->context->layoutWrapperCssClass = 'upload-manager';
$this->beginContent('@frontend/themes/kub/views/layouts/main.php');
$activePage = Yii::$app->controller->action->id;
?>

<?php echo $content ?>

<?php $this->endContent(); ?>
