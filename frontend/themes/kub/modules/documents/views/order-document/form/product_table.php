<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/** @var View $this */
/** @var OrderDocument $model */
/** @var Company $company */
/** @var Employee $user */
/** @var integer $ioType */

$ndsCellClass = 'with-nds-item';
if ($model->has_nds == OrderDocument::HAS_NO_NDS) {
    $ndsCellClass .= ' hidden';
}
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}
?>
<?php if (!($company->companyTaxationType->osno || $company->companyTaxationType->usn)) : ?>
    <style type="text/css">
        #table-for-order-document tr > th:nth-child(5),
        #table-for-order-document tr > td:nth-child(5) {
            display: none;
        }
    </style>
<?php endif ?>
<?php
SortableAsset::register($this);

$js = <<<JS
     $("#table-for-order-document").sortable({
        containerSelector: "table",
        handle: ".sortable-row-icon",
        itemPath: "> tbody",
        itemSelector: "tr",
        placeholder: "<tr class=\"placeholder\"/>",
        onDrag: function (item, position, _super, event) {
            position.left -= 45;
            position.top -= 25;
            item.css(position);
        }
    });
JS;
$this->registerJs($js);
?>

<?= Html::hiddenInput('documentType', $ioType, ['id' => 'documentType']); ?>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-order-document',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
    ],
]); ?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.prise_modify_toggle',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true,
        'repositionOnScroll' => true,
        'side' => ['bottom', 'left', 'top', 'right'],
    ],
]); ?>

    <div class="hidden">
        <!-- Discount popup -->
        <div id="order-document_all_discount" style="overflow: hidden;">
            <div class="form-group">
                <div class="label">Скидка для всех позиций счета, %</div><br/>
                <div class="row">
                    <div class="col-6">
                        <?= Html::input('number', 'all-discount', 0, [
                            'id' => 'all-discount',
                            'min' => 0,
                            'max' => 99.9999,
                            'step' => 'any',
                            'style' => '',
                            'class' => 'form-control form-control-number',
                            'data-old-value' => 0,
                        ]); ?>
                    </div>
                </div>
            </div>

            <?= Html::button('Применить', [
                'id' => 'discount_submit',
                'class' => 'button-regular button-hover-grey button-width mb-2 order-document',
            ]) ?>
        </div>
        <!-- Markup popup -->
        <div id="order-document_all_markup" style="overflow: hidden;">
            <div class="form-group">
                <div class="label">Наценка для всех позиций счета, %</div><br/>
                <div class="row">
                    <div class="col-6">
                        <?= Html::input('number', 'all-markup', 0, [
                            'id' => 'all-markup',
                            'min' => 0,
                            'max' => 99.9999,
                            'step' => 'any',
                            'style' => '',
                            'class' => 'form-control form-control-number',
                            'data-old-value' => 0,
                        ]); ?>
                    </div>
                </div>
            </div>

            <?= Html::button('Применить', [
                'id' => 'markup_submit',
                'class' => 'button-regular button-hover-grey button-width mb-2 order-document',
            ]) ?>
        </div>
    </div>

    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'order_document_product_article',],
                    ['attribute' => 'order_document_product_reserve',],
                    ['attribute' => 'order_document_product_quantity',],
                    ['attribute' => 'order_document_product_weigh',],
                    ['attribute' => 'order_document_product_volume',],
                ],
            ]); ?>
        </div>
    </div>

    <div class="wrap" style="position: relative;">
        <div class="table-wrap table-responsive">
            <table id="table-for-order-document" class="table table-style table-count account_table last-line-table" style="position: relative;">
                <thead>
                    <tr class="heading" role="row">
                        <th width="5%" tabindex="0" rowspan="1" colspan="1">
                        </th>
                        <th width="25%" tabindex="0" rowspan="1" colspan="1">
                            Наименование
                        </th>
                        <th width="15%" class="col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                            Артикул
                        </th>
                        <th width="10%" class="" tabindex="0" rowspan="1" colspan="1">
                            Количество
                        </th>
                        <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                            Ед.измерения
                        </th>
                        <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                            Доступно
                        </th>
                        <th width="5%" class="col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                            Резерв
                        </th>
                        <th width="5%" class="col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                            Остаток
                        </th>
                        <th width="5%" class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                            Вес (кг)
                        </th>
                        <th width="5%" class="col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                            Объем
                        </th>
                        <th width="10%" class="<?= $ndsCellClass ?>" tabindex="0" rowspan="1" colspan="1">
                            НДС
                        </th>
                        <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                            Цена
                        </th>
                        <th width="10%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                            rowspan="1" colspan="1">
                            <?= Html::tag('span', 'Скидка %', [
                                'id' => 'all_discount_toggle',
                                'class' => 'prise_modify_toggle',
                                'data-tooltip-content' => '#order-document_all_discount',
                                'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                            ]) ?>
                        </th>
                        <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                            rowspan="1" colspan="1">
                            Цена со скидкой
                        </th>
                        <th width="10%" class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>" tabindex="0"
                            rowspan="1" colspan="1">
                            <?= Html::tag('span', 'Наценка %', [
                                'id' => 'all_markup_toggle',
                                'class' => 'prise_modify_toggle',
                                'data-tooltip-content' => '#order-document_all_markup',
                                'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                            ]) ?>
                        </th>
                        <th width="15%" class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>" tabindex="0"
                            rowspan="1" colspan="1">
                            Цена с наценкой
                        </th>
                        <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                            Сумма
                        </th>
                    </tr>
                </thead>
                <tbody id="table-product-list-body">
                    <?php foreach ($model->orderDocumentProducts as $key => $orderDocumentProduct): ?>
                        <?= $this->render('order_document_product_row', [
                            'orderDocumentProduct' => $orderDocumentProduct,
                            'number' => $key,
                            'model' => $model,
                            'ndsCellClass' => $ndsCellClass,
                            'company' => $company,
                            'user' => $user,
                        ]); ?>
                    <?php endforeach; ?>
                    <?= $this->render('add_order_row', [
                        'hasOrders' => (boolean)$model->orderDocumentProducts,
                        'hasDiscount' => (boolean)$model->has_discount,
                        'hasMarkup' => (boolean)$model->has_markup,
                        'ndsCellClass' => $ndsCellClass,
                        'company' => $company,
                        'user' => $user,
                        'ioType' => $ioType,
                    ]) ?>
                    <tr class="template disabled-row" role="row">
                        <td class="product-delete">
                            <button class="remove-product-from-order-document button-clr" type="button">
                                <svg class="table-count-icon svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                                </svg>
                            </button>
                            <button class="sortable-row-icon button-clr" type="button">
                                <svg class="table-count-icon table-count-icon_small svg-icon">
                                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                </svg>
                            </button>
                        </td>
                        <td>
                            <input type="text" class="product-title form-control tooltip-product" name="orderArray[][title]"
                                   style="width: 100%;">
                        </td>
                        <td class="product-article col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>"></td>
                        <td>
                            <input disabled="disabled" type="hidden" class="tax-rate"
                                   value="0"/>

                            <input disabled="disabled" type="hidden" class="product-model"
                                   name="orderArray[][model]"
                                   value="<?= Product::tableName(); ?>"/>
                            <input disabled="disabled" type="hidden" class="product-id"
                                   name="orderArray[][id]" value="0"/>
                            <input disabled="disabled" type="number" min="0" step="any"
                                   class="form-control product-count"
                                   name="orderArray[][count]" value="1"/>
                            <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
                        </td>
                        <td class="product-unit-name"></td>
                        <td class="product-available"></td>
                        <td class="product-reserve col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>"></td>
                        <td class="product-quantity col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>"></td>
                        <td class="product-weight col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>"></td>
                        <td class="product-volume col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>"></td>
                        <td class="price-for-sell-nds-name <?= $ndsCellClass ?>">
                            <?= Html::dropDownList(
                                ($ioType == Documents::IO_TYPE_OUT ? 'orderArray[][sale_tax_rate_id]' : 'orderArray[][purchase_tax_rate_id]'),
                                null,
                                $taxItems,
                                [
                                    'class' => 'order_product_tax_rate',
                                    'options' => $taxOptions,
                                    'disabled' => 'disabled'
                                ]
                            ); ?>
                        </td>
                        <td class="price-one">
                            <?= Html::input('number', 'orderArray[][price]', 0, [
                                'class' => 'form-control price-input',
                                'disabled' => 'disabled',
                                'min' => 0,
                                'step' => 'any',
                            ]); ?>
                        </td>
                        <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                            <?= Html::input('number', 'orderArray[][discount]', 0, [
                                'class' => 'form-control discount-input',
                                'disabled' => 'disabled',
                                'min' => 0,
                                'max' => 99.9999,
                                'step' => 'any',
                            ]); ?>
                        </td>
                        <td class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                            <span class="price-one-with-nds">0</span>
                            <?= Html::hiddenInput('orderArray[][priceOneWithVat]', 0, [
                                'class' => 'form-control price-one-with-nds-input',
                                'disabled' => 'disabled',
                            ]); ?>
                        </td>
                        <td class="markup markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
                            <?= Html::input('number', 'orderArray[][markup]', 0, [
                                'class' => 'form-control markup-input',
                                'disabled' => 'disabled',
                                'min' => 0,
                                'max' => 99.9999,
                                'step' => 'any',
                            ]); ?>
                        </td>
                        <?php /*
                        <td class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
                            <span class="price-one-with-nds">0</span>
                            <?= Html::hiddenInput('orderArray[][priceOneWithVat]', 0, [
                                'class' => 'form-control price-one-with-nds-input',
                                'disabled' => 'disabled',
                            ]); ?>
                        </td>*/ ?>
                        <td class="price-with-nds" style="text-align: right;">0</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row align-flex-start justify-content-between mt-3">
            <div class="column button-add-line">
                <span class="btn-add-line-table button-regular button-hover-content-red button-width">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </span>
            </div>
            <div class="column">
                <?= $this->render('total_block', [
                    'model' => $model,
                    'user' => $user,
                ]); ?>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>

<script type="text/javascript">
    $(document).on('input change', '#order-document_all_discount input', function () {
        var changed = false;
        var box = $(this).closest('#order-document_all_discount');
        var allDiscountValue = parseFloat($('#all-discount', box).val()).toString();
        if (allDiscountValue != $('#all-discount', box).data('old-value')) {
            changed = true;
        }

        $('#discount_submit', box).toggleClass('button-hover-grey', !changed).toggleClass('button-regular_red', changed);
    });
    $(document).on('input change', '#order-document_all_markup input', function () {
        var changed = false;
        var box = $(this).closest('#order-document_all_markup');
        var allMarkupValue = parseFloat($('#all-markup', box).val()).toString();
        if (allMarkupValue != $('#all-markup', box).data('old-value')) {
            changed = true;
        }

        $('#markup_submit', box).toggleClass('button-hover-grey', !changed).toggleClass('button-regular_red', changed);
    });
    $(document).on('change', 'select.order_product_tax_rate', function () {
        var $row = $(this).parents('tr');
        var $totalNds = $('.nds-view-item');
        var $invoiceNdsViewType = $('#orderdocument-nds_view_type_id');
        var showNdsType = $invoiceNdsViewType.attr('data-id') != 2 ?
            $invoiceNdsViewType.attr('data-id') : $('#company-nds_view_type_id').val();

        $row.find('.tax-rate').val($(this).find('option:selected').data('rate'));
        $totalNds.addClass('hidden').filter('.type-' + showNdsType).removeClass('hidden');
        $invoiceNdsViewType.val(showNdsType);

        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });
</script>
