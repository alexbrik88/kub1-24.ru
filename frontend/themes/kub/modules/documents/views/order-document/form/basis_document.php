<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 17:51
 */

use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\components\date\DateHelper;

/* @var $model \common\models\document\OrderDocument */

$agreementDropDownList = ['' => '---'];
$outnds = 0;
if ($model->contractor) {
    $agreementArray = $model->contractor->getAgreements()->joinWith('agreementType')->orderBy([
        'agreement_type.name' => SORT_ASC,
        'agreement.document_date' => SORT_DESC,
    ])->all();
    $agreementDropDownList += ['' => '   '] + ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '];
    $agreementDropDownList[$model->agreement] = $model->getBasisDocString();
    /** @var $agreement \common\models\Agreement */
    foreach ($agreementArray as $agreement) {
        $agreement_date = DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $key = "{$agreement->agreementType->name}&{$agreement->getFullNumber()}&{$agreement->document_date}&{$agreement->agreementType->id}&{$agreement->id}";
        $value = "{$agreement->agreementType->name} № {$agreement->getFullNumber()} от {$agreement_date}";
        $agreementDropDownList[$key] = $value;
    }
    if ($model->contractor->isOutInvoiceHasNds) {
        $outnds = 1;
    }
}

Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['basis-document']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'orderdocument-agreement',
    'model' => $model,
    'attribute' => 'agreement',
    'data' => $agreementDropDownList,
    'options' => [
        'data' => [
            'delay' => !empty($delay) ? $delay : 10,
            'refresh' => empty($refresh) ? 'false' : 'true',
            'outnds' => $outnds,
        ]
    ],
    'pluginOptions' => [
        'width' => '100%',
        'escapeMarkup' => new JsExpression('function(text) {return text;}')
    ],
]);

Pjax::end();