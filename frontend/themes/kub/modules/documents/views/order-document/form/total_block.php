<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.02.2018
 * Time: 17:46
 */

use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use frontend\models\Documents;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

$sum = $model->total_amount;
$discount = ($model->has_discount) ? $model->getDiscountSum() : 0;
$markup = ($model->has_markup) ? $model->getMarkupSum() : 0;
?>

<table class="total-txt table-resume">
    <tr class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <td>
            Сумма скидки:
        </td>
        <td>
            <strong id="discount_sum">
                <?= TextHelper::moneyFormatFromIntToFloat($discount); ?>
            </strong>
        </td>
    </tr>
    <tr class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
        <td>
            Сумма наценки:
        </td>
        <td>
            <strong id="markup_sum">
                <?= TextHelper::moneyFormatFromIntToFloat($markup); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>
            Итого:
        </td>
        <td>
            <strong id="total_price">
                <?= TextHelper::moneyFormatFromIntToFloat($sum); ?>
            </strong>
        </td>
    </tr>
    <?php if ($model->type == Documents::IO_TYPE_IN): ?>
        <tr>
            <td>
                <?php Pjax::begin([
                    'id' => 'nds_view_type_id-pjax-container',
                    'enablePushState' => false,
                    'linkSelector' => false,
                    'options' => ['style' => 'display: inline-block;']
                ]); ?>
                <?= Select2::widget([
                    'hideSearch' => true,
                    'model' => $model,
                    'attribute' => 'nds_view_type_id',
                    'data' => OrderDocument::$ndsViewList,
                    'options' => [
                        'class' => 'form-control nds-select',
                        'data' => ['id' => $model->nds_view_type_id],
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]) ?>
                <?php Pjax::end(); ?>
            </td>
            <td>
                <strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong>
            </td>
        </tr>
    <?php else: ?>
        <tr style="display:none">
            <td><?= Html::activeHiddenInput($model, 'nds_view_type_id', [
                    'data' => ['id' => $model->nds_view_type_id],
                ]) ?>
            </td>
        </tr>
        <tr class="nds-view-item type-0 <?= $model->nds_view_type_id == 0 ? '' : 'hidden'; ?>">
            <td><span>В том числе НДС:</span></td>
            <td><strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong></td>
        </tr>
        <tr class="nds-view-item type-1 <?= $model->nds_view_type_id == 1 ? '' : 'hidden'; ?>">
            <td>НДС сверху:</td>
            <td><strong class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></strong></td>
        </tr>
        <tr class="nds-view-item type-2 <?= $model->nds_view_type_id == 2 ? '' : 'hidden'; ?>">
            <td>Без налога (НДС)</td>
            <td><strong>-</strong></td>
        </tr>
    <?php endif; ?>

    <tr class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>" role="row">
        <td>
            Вес (кг):
        </td>
        <td>
            <strong id="total_mass">
                <?= $model->getTotalWeight(); ?>
            </strong>
        </td>
    </tr>

    <tr class="" role="row">
        <td>
            Всего к оплате:
        </td>
        <td>
            <strong id="total_total_amount">
                <?= TextHelper::moneyFormatFromIntToFloat($model->total_amount); ?>
            </strong>
        </td>
    </tr>

</table>

<?php
if ($model->type == Documents::IO_TYPE_IN) {
    $this->registerJs('
    var checkInInvoiceNds = function() {
        if ($("#invoice-nds_view_type_id").val() == "2") {
            $(".with-nds-item").addClass("hidden");
            $(".without-nds-item").removeClass("hidden");
        } else {
            $(".with-nds-item").removeClass("hidden");
            $(".without-nds-item").addClass("hidden");
        }
    }
    $(document).on("change", "#invoice-nds_view_type_id", function() {
        checkInInvoiceNds();
    });
    $(document).on("change", "#invoice-contractor_id", function() {
        $.pjax.reload("#nds_view_type_id-pjax-container", {"type": "post", "data": $(this).closest("form").serialize()});
        $(document).on("pjax:complete", "#nds_view_type_id-pjax-container", function(){
            INVOICE.recalculateInvoiceTable();
            checkInInvoiceNds();
            
            $("select.nds-select").select2({
                theme: "krajee-bs4",
                width: "100%",
                minimumResultsForSearch: -1,
                escapeMarkup: function(markup) {
                    return markup;
                }
            });            
        });
    });
');
}
?>
