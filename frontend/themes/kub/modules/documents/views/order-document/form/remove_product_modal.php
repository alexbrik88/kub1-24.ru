<div id="modal-remove-one-product" class="confirm-modal fade modal"  role="dialog" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <h4 class="text-center mb-4" style="margin-bottom:30px;">
                        Вы уверены, что хотите удалить эту позицию из заказа?
                    </h4>
                    <div class="text-center">
                        <button class="button-clr button-regular button-hover-transparent button-width-medium mr-2 yes" type="button" data-dismiss="modal">Да</button>
                        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>