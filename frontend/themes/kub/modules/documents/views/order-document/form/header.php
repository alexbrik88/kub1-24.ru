<?php

use frontend\models\Documents;
use yii\bootstrap4\Html;
use common\components\date\DateHelper;
use common\models\document\OrderDocument;
use yii\bootstrap4\ActiveForm;
use common\models\document\status\OrderDocumentStatus;
use frontend\themes\kub\helpers\Icon;
use yii\web\View;

/** @var View $this */
/** @var OrderDocument $model */
/** @var ActiveForm $form */
/** @var integer $ioType */

/** @var OrderDocumentStatus[] $statusArray */
$statusArray = OrderDocumentStatus::find()->all();
?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>
<style type="text/css">
    <?php foreach ($statusArray as $status) : ?>
    .order-doc-status-set[data-status="<?= $status->id ?>"] .button-regular {
        background-color: <?= $status->color; ?>;
        border-color: <?= $status->color; ?>;
        color: #ffffff;
    }
    <?php endforeach ?>
</style>
<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column text-nowrap">
                    Заказ <?= $ioType == Documents::IO_TYPE_IN ? 'поставщику' : 'покупателя'; ?>  №
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>

                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                        'placeholder' => 'доп. номер',
                    ]); ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
                <div class="form-group d-inline-block mb-0 order-doc-status-set ml-auto" data-status="<?= $model->status->id ?>">
                    <div class="dropdown" style="width: 150px;">
                        <?= Html::button(Html::tag('span', $model->status->name, [
                                'class' => 'order-document-status-name w-100',
                            ]) . Icon::get('shevron', [
                                'class' => 'position-absolute',
                                'style' => 'right: 10px;',
                            ]), [
                            'class' => 'button-regular dropdown-toggle w-100 pr-4 position-relative',
                            'data-toggle' => 'dropdown',
                        ]) ?>
                        <div class="dropdown-menu w-100" aria-labelledby="documentStatusButton">
                            <?php foreach ($statusArray as $status): ?>
                                <?= Html::a($status->name, 'javascript:;', [
                                    'data-status' => $status->id,
                                    'class' => 'document-status-link d-block px-3 py-2',
                                ]) ?>
                            <?php endforeach; ?>
                        </div>
                        <?= Html::activeHiddenInput($model, 'status_id') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    $(document).on("click", ".document-status-link", function(e) {
        let status = $(this).data("status");

        $(".order-document-status-name").text($(this).text());
        $(this).closest(".order-doc-status-set").attr("data-status", status);
        $("#orderdocument-status_id").val(status);
    });
'); ?>
