<?php

use common\components\ImageHelper;
use common\models\bank\Bank;
use common\models\company\CompanyType;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\models\LoginForm;
use frontend\models\RegistrationForm;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $model frontend\modules\documents\forms\InvoiceOutViewForm */
/* @var $orderDocument common\models\document\Invoice */
/* @var $userExists boolean */

$isExists = Yii::$app->user->identity &&
    Yii::$app->user->identity->company &&
    Yii::$app->user->identity->company->hasOrderDocument($orderDocument);

$this->params['orderDocument'] = $orderDocument;
$this->params['canSave'] = !$isExists && $orderDocument->contractor->companyType && $orderDocument->contractor->companyType->in_company;

$example = $orderDocument->uid == (YII_ENV_PROD ? 'fGkg0' : '7ubrQ');

$script = <<<JS
var documentSaveOpen = function() {
    $("#out-view-shading").removeClass("hidden");
    $("#document-save-wrapper").animate({right: 0}, 200);
}
var documentSaveClose = function() {
    $("#document-save-wrapper").animate({right: "-400px"}, 200, function() {
        $("#out-view-shading").addClass("hidden");
        $.pjax.reload("#out-view-pjax", {"push":false,"replace":false,"timeout":5000,"scrollTo":false});
    });
}
var documentPayOpen = function() {
    $("#out-view-shading").removeClass("hidden");
    $("#document-pay-wrapper").animate({right: 0}, 200);
}
var documentPayClose = function() {
    $("#document-pay-wrapper").animate({right: "-400px"}, 200, function() {
        $("#out-view-shading").addClass("hidden");
    });
}
$(document).on("click", "#document-save-link", function (e) {
    e.preventDefault();
    documentSaveOpen();
});
$(document).on("click", "#document-save-close, .close-panel", function (e) {
    e.preventDefault();
    documentSaveClose();
});
$(document).on("click", "#document-pay-link", function (e) {
    e.preventDefault();
    documentPayOpen();
});
$(document).on("click", "#document-pay-close, .close-panel", function (e) {
    e.preventDefault();
    documentPayClose();
});
$(document).on("click", "#document-pay-content .pay-block.pay-link",function () {
    if ($(this).hasClass('target')) {
        window.open($(this).data('url'));
    } else {
        location.href = $(this).data('url');
    }
});
$(document).on("change", "#invoiceoutviewform-taxationtype input", function (e) {
    if (this.value == "osno") {
        if (this.checked == true) {
            $("#invoiceoutviewform-taxationtype input[value='usn']")
                .prop("checked", false)
                .prop("disabled", true)
                .uniform("refresh");
        } else {
            $("#invoiceoutviewform-taxationtype input[value='usn']")
                .prop("disabled", false)
                .uniform("refresh");
        }
    }
    if (this.value == "usn") {
        if (this.checked == true) {
            $("#invoiceoutviewform-taxationtype input[value='osno']")
                .prop("checked", false)
                .prop("disabled", true)
                .uniform("refresh");
        } else {
            $("#invoiceoutviewform-taxationtype input[value='osno']")
                .prop("disabled", false)
                .uniform("refresh");
        }
    }
});
$("form input:checkbox, form input:radio").uniform();
JS;

$this->registerJs($script);
?>
<?php if ($example): ?>
    <div id="background-example-block">
        <p id="example-text" style=>ОБРАЗЕЦ</p>
    </div>
<?php endif; ?>
<div>
    <?= $this->render('pdf-view', [
        'model' => $orderDocument,
        'ioType' => $orderDocument->type,
        'addStamp' => (boolean) $orderDocument->company->pdf_signed,
    ]); ?>
</div>