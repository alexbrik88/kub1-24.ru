<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.02.2018
 * Time: 17:17
 */

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\document\Invoice;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use frontend\modules\documents\forms\InvoiceSendForm;
use common\models\document\OrderDocument;
use frontend\modules\documents\forms\OrderDocumentSendForm;

/* @var OrderDocument $model */
/* @var $useContractor string */

$sendForm = new OrderDocumentSendForm(Yii::$app->user->identity->currentEmployeeCompany);
$modalHeader = '';
$contractor = $model->contractor;
$modalHeader = 'заказ';
$sendForm->textRequired = true;
$sendForm->emailText = $model->emailText;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
$company = Yii::$app->user->identity->company;
?>
<?php Modal::begin([
    'title' => '',
    'options' => [
        'id' => 'send',
    ],
    'toggleButton' => false,
]); ?>
<?php $form = ActiveForm::begin([
    'id' => 'send-document-form',
    'action' => Url::to(['send', 'id' => $model->id,]),
    'options' => [
        'class' => 'add-to-order-document',
    ],
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group mb-0'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

<h4 class="modal-title"><?= 'Выберите, кому отправить ' . $modalHeader ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="row">
    <div class="form-group column mb-3 pb-3 pt-2">
        <?= $form->field($sendForm, 'sendToChief', ['template' => "{label}\n{input}"])
            ->error(false)
            ->checkbox(); ?>
    </div>
    <div class="form-group column mb-3 pb-3">
        <div class="row">
            <?php if (empty($contractor->director_email)) : ?>
                <?php if (!empty($contractor->director_name)) : ?>
                    <div class="column pt-2 text-muted" style="padding-right: 15px;">
                        <?= $contractor->director_name; ?>
                    </div>
                <?php endif ?>
                <?= $form->field($sendForm, 'chiefEmail', ['options' => ['class' => '']])
                    ->label(false)
                    ->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]);?>
            <?php else : ?>
                <div class="column pt-2 text-muted">
                    <?= $contractor->director_name; ?>
                </div>
                <div class="column pt-2 text-muted">
                    <?= $contractor->director_email; ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group column mb-3 pb-3 pt-2">
        <?= $form->field($sendForm, 'sendToOther')->checkbox(); ?>
    </div>
    <div class="form-group column mb-3 pb-3">
        <?= $form->field($sendForm, 'otherEmail')->label(false)->textInput([
            'placeHolder' => 'Укажите e-mail',
            'class' => 'form-control width100' . (isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? ' tooltip-self-send' : ''),
            'data-tooltip-content' => isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? '#tooltip_send-example' : null,
        ]); ?>
    </div>
</div>

<?php if (!$contractor->chief_accountant_is_director): ?>
    <div class="row">
        <div class="form-group column mb-3 pb-3 pt-2">
            <?= $form->field($sendForm, 'sendToChiefAccountant')->checkbox(); ?>
        </div>
        <div class="form-group column mb-3 pb-3">
            <div class="row">
                <?php if (empty($contractor->chief_accountant_email)) : ?>
                    <?php if (!empty($contractor->chief_accountant_name)) : ?>
                        <div class="column pt-2 text-muted" style="padding-right: 15px;">
                            <?= $contractor->chief_accountant_name; ?>
                        </div>
                    <?php endif ?>
                    <?= $form->field($sendForm, 'chiefAccountantEmail', ['options' => ['class' => '']])
                        ->label(false)
                        ->textInput([
                            'placeHolder' => 'Укажите e-mail',
                            'class' => 'form-control width100',
                        ]); ?>
                <?php else : ?>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->chief_accountant_name; ?>
                    </div>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->chief_accountant_email; ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (!$contractor->contact_is_director): ?>
    <div class="row">
        <div class="form-group column mb-3 pb-3 pt-2">
            <?= $form->field($sendForm, 'sendToContact')->checkbox(); ?>
        </div>
        <div class="form-group column mb-3 pb-3">
            <div class="row">
                <?php if (empty($contractor->contact_email)) : ?>
                    <?php if (!empty($contractor->contact_name)) : ?>
                        <div class="column pt-2 text-muted" style="padding-right: 15px;">
                            <?= $contractor->contact_name; ?>
                        </div>
                    <?php endif ?>
                    <?= $form->field($sendForm, 'contactEmail', ['options' => ['class' => '']])
                        ->label(false)
                        ->textInput([
                            'placeHolder' => 'Укажите e-mail',
                            'class' => 'form-control width100',
                        ]); ?>
                <?php else : ?>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->contact_name; ?>
                    </div>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->contact_email; ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-12 mb-2">
        <?= $form->field($sendForm, 'sendToChief', ['template' => "{error}", 'options' => ['class' => '']])->error(); ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= Html::activeLabel($sendForm, 'emailText', [
            'class' => 'label',
        ]) ?>
        <div class="email_text_input" style="margin-top: -1px;">
            <?= $form->field($sendForm, 'emailText')->textArea([
                'rows' => 10,
                'onkeyup' => 'emailTextResize()',
                'style' => 'padding: 10px; width: 100%; resize: none; overflow: hidden; border-color: #e5e5e5 !important;',
            ])->label(false); ?>
            <?php $this->registerJs('
                function emailTextResize() {
                    var inputArea = document.getElementById("collateform-emailtext")
                    $(inputArea).height(1);
                    $(inputArea).height(inputArea.scrollHeight);
                }
                $("#send").on("shown.bs.modal", function() {
                    emailTextResize();
                })
            '); ?>
        </div>
    </div>
</div>

<?php /*
<div class="form-body">
    <div class="form-group row"
         style="<?= empty($contractor->director_email) ? 'margin-bottom: 0px;' : ''; ?>">
        <div class="container-who-send-label mar-top-8">
            <?= $form->field($sendForm, 'sendToChief', ['template' => "{label}\n{input}"])->checkbox([
                'label' => 'Руководитель:',
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
                'class' => 'add-chief-email',
            ]); ?>
        </div>
        <div class="container-who-send-input chief-email"
             style="margin-bottom: -8px;">

            <?php if (empty($contractor->director_email)) { ?>
                <?php if (!empty($contractor->director_name)) { ?>
                    <span class="text-muted inline-block col-lg-6 col-md-6 col-xs-6 mar-top-8 pad-left-none"
                          style="padding-right: 15px;"><?= $contractor->director_name; ?> </span>
                <?php } ?>
                <?= $form->field($sendForm, 'chiefEmail', ['options' => ['class' => 'col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none']])->label(false)->textInput([
                    'placeHolder' => 'Укажите e-mail',
                    'class' => 'form-control width100',
                ]); ?>
            <?php } else { ?>
                <span
                    class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-left-none"><?= $contractor->director_name; ?> </span>
                <span
                    class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none"><?= $contractor->director_email; ?></span>
            <?php } ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($sendForm, 'sendToChief', ['template' => "{error}"])->checkbox(); ?>
        </div>
    </div>

    <?php if (!$contractor->chief_accountant_is_director): ?>
        <div class="form-group row"
             style="<?= empty($contractor->chief_accountant_email) ? 'margin-bottom: 0px;' : ''; ?>">
            <div class="container-who-send-label mar-top-8">
                <?= $form->field($sendForm, 'sendToChiefAccountant')->checkbox([
                    'label' => 'Главный бухгалтер:',
                ]); ?>
            </div>
            <div class="container-who-send-input chief-email"
                 style="margin-bottom: -8px;">

                <?php if (empty($contractor->chief_accountant_email)) { ?>
                    <?php if (!empty($contractor->chief_accountant_name)) { ?>
                        <span class="text-muted inline-block col-lg-6 col-md-6 mar-top-8 pad-left-none"
                              style="padding-right: 15px;"><?= $contractor->chief_accountant_name; ?> </span>
                    <?php } ?>
                    <?= $form->field($sendForm, 'chiefAccountantEmail', ['options' => ['class' => 'col-lg-6 col-md-6 pad-right-none pad-left-none']])->label(false)->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php } else { ?>
                    <span
                        class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-left-none"><?= $contractor->chief_accountant_name; ?> </span>
                    <span
                        class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none"><?= $contractor->chief_accountant_email; ?></span>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!$contractor->contact_is_director): ?>
        <div class="form-group row" style="margin-bottom: 0px;">
            <div class="container-who-send-label mar-top-8">
                <?= $form->field($sendForm, 'sendToContact')->checkbox([
                    'label' => 'Контакт:',
                    'labelOptions' => [
                        'style' => 'width: ' . $labelWidth . 'px;',
                    ],
                ]); ?>
            </div>
            <div class="container-who-send-input chief-email"
                 style="margin-bottom: -8px;">

                <?php if (empty($contractor->contact_email)) { ?>
                    <?php if (!empty($contractor->contact_name)) { ?>
                        <span
                            class="text-muted inline-block col-lg-6 col-md-6 mar-top-8 pad-left-none"><?= $contractor->contact_name; ?> </span>
                    <?php } ?>
                    <?= $form->field($sendForm, 'contactAccountantEmail', ['options' => ['class' => 'col-lg-6 col-md-6 pad-right-none pad-left-none']])->label(false)->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php } else { ?>
                    <span class="text-muted"
                          style="padding-right: 15px;"><?= $contractor->contact_name; ?> </span>
                    <span class="text-muted"
                          style="padding-left: 15px;"><?= $contractor->contact_email; ?></span>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group row">
        <div class="container-who-send-label mar-top-8">
            <?= $form->field($sendForm, 'sendToOther')->checkbox([
                'label' => 'Еще:',
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
            ]); ?>
        </div>
        <div class="container-who-send-input">
            <?= $form->field($sendForm, 'otherEmail')->label(false)->textInput([
                'placeHolder' => 'Укажите e-mail',
                'class' => 'form-control width100',
            ]); ?>
        </div>
    </div>
    <?php if ($sendForm->textRequired): ?>
        <div class="form-group row">
            <div class="col-sm-12">
                <label id="invoicesendform-emailtext-label" class="control-label"
                       style="cursor: pointer; border-bottom: 1px dashed #e5e5e5; margin-bottom: 0;">
                    Текст письма <span class="caret"></span>
                </label>

                <div class="email_text_input hidden" style="margin-top: -1px;">
                    <?= $form->field($sendForm, 'emailText')->textArea([
                        'rows' => 4,
                        'onkeyup' => 'emailTextResize()',
                        'style' => 'padding: 10px; width: 100%; border-bottom: 0; resize: none; overflow: hidden; border-color: #e5e5e5 !important;',
                    ])->hint($this->render('email_text_hint'))->label(false); ?>
                </div>
            </div>
        </div>
        <?php $this->registerJs('
                function emailTextResize() {
                    var inputArea = document.getElementById("orderdocumentsendform-emailtext")
                    $(inputArea).height(1);
                    $(inputArea).height(inputArea.scrollHeight);
                }
                $("#send").on("shown.bs.modal", function() {
                    emailTextResize();
                })
                $(document).on("click", "#orderdocumentsendform-emailtext-label", function() {
                    $(".email_text_input").toggleClass("hidden");
                    emailTextResize();
                });
            '); ?>
    <?php endif ?>
</div>
*/ ?>
<div class="form-group">
    <div class="row">
        <div class="col-sm-12">
            Ответное письмо клиента на данное письмо с заказом,
            <br>
            придет на вашу почту – <span><?= Yii::$app->user->identity->email; ?></span>
        </div>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('<span class="ladda-label">Отправить</span><span class="ladda-spinner"></span>', [
        'class' => 'button-regular button-clr button-width button-hover-transparent ladda-button',
        'data-style' => 'expand-right',
        'style' => 'width: 130px!important;',
    ]); ?>
    <button type="button" class="button-regular button-clr button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php $form->end(); ?>

<div id="send-document-loading" class="ajax-loader-wrapper" style="display: none;">
    <img src="/img/loading.gif">
</div>

<?php Modal::end(); ?>

<?php $this->registerJs('
$(document).on("submit", "#send-document-form", function() {
    $("#send-document-loading").show();
});
$(document).on("hidden.bs.modal", "#send", function () {
    $("#send-document-form")[0].reset();
})
'); ?>
