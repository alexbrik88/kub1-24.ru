<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 6:12
 */

use common\models\document\OrderDocument;
use frontend\models\Documents;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use frontend\modules\documents\widgets\CreatedByWidget;
use common\models\Contractor;
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\components\TextHelper;
use common\models\employee\Employee;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $dateFormatted string
 * @var $user Employee
 */

$company = $model->company;
$contractor = $model->contractor;
$ndsName = (isset($model->ndsViewType->name)) ? $model->ndsViewType->name : '-';
$ndsValue = ($model->has_nds) ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';

?>
<style type="text/css">
    .col-xs-2, .col-xs-3, .col-xs-12 {
        float: left;
        display: block;
    }
    .col-xs-2 {
        width: 16.66666667%;
    }
    .col-xs-3 {
        width: 25%;
    }
    .col-xs-10 {
        width: 80%;
    }
    .col-xs-12 {
        width: 100%;
        float: none!important;
        clear: both;
    }
    .v_bottom {
        display: table;
        height: 99.8%;
    }
    .bord-dark-t {
        border-top: 1px solid #000000;
    }
    .pad3 {
        padding: 3px !important;
    }
    .v_bottom > div {
        width: 100%;
        height: 100%;
        display: table-cell;
        vertical-align: bottom;
    }

    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }

    .pre-view-table h4 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }

    .m-size-div div {
        font-size: 12px;
    }

    .m-size-div div span {
        font-size: 9px;
    }

    .no_min_h {
        min-height: 0 !important;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table pad0 no_min_h"
     style="max-width:735px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div style="font-size: 17px"> <?= $model->contractor->getTitle(true); ?> </div>
        <h4 style="">Заказ № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?></h4>
        <div class="col-xs-12 pad3" style="padding-top: 15px !important;">
            <div class="col-xs-12 pad0">
                <div class="col-xs-2 pad0"> Поставщик <br/> (Исполнитель):</div>
                <div class="col-xs-10 pad0 pull-right" style="padding-left:3px;font-weight: bold;">
                    <?php if ($model->type == 2): ?>
                        <?= $model->company->getTitle(true); ?>,
                        ИНН <?= $model->company->inn; ?><?= !empty($model->company->kpp) ? ', КПП' . $model->company->kpp : ''; ?>
                        , <?= $model->company->getAddressLegalFull(); ?>
                    <?php else: ?>
                        <?= $model->contractor_name_short; ?>,
                        <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                        <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                        <?= $model->contractor_address_legal_full; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-top: 5px !important;">
                <div class="col-xs-2 pad0"> Покупатель<br/>(Заказчик):</div>
                <div class="col-xs-10 pad0 pull-right" style="padding-left:3px;font-weight: bold;">
                    <?php if ($model->type != 2): ?>
                        <?= $model->company->getTitle(true); ?>,
                        ИНН <?= $model->company->inn; ?><?= !empty($model->company->kpp) ? ', КПП' . $model->company->kpp : ''; ?>
                        , <?= $model->company->getAddressLegalFull(); ?>
                    <?php else: ?>
                        <?= $model->contractor_name_short; ?>,
                        <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                        <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                        <?= $model->contractor_address_legal_full; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                <div class="col-xs-12 pad0"
                     style="padding-top: 5px !important;">
                    <div class="col-xs-2 pad0"> Основание:</div>
                    <div class="col-xs-10 pad3 pull-right" style="font-weight: bold;">
                        <?= $model->basis_document_name ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="col-xs-12 pad3" style="margin:10px 0;">
            <?= $this->render('orders_table', [
                'model' => $model,
                'user' => $user,
            ]); ?>
        </div>
        <div class="col-xs-12 pad3">
            <table class="w-100">
                <tbody>
                <?php if ($model->has_discount): ?>
                    <tr>
                        <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 85%;">Сумма скидки:</td>
                        <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 15%;">
                            <b><?= TextHelper::invoiceMoneyFormat($model->discountSum, 2); ?></b>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ($model->has_markup): ?>
                    <tr>
                        <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 85%;">Сумма наценки:</td>
                        <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 15%;">
                            <b><?= TextHelper::invoiceMoneyFormat($model->markupSum, 2); ?></b>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 85%;">Итого:</td>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 15%;">
                        <b><?= TextHelper::invoiceMoneyFormat($model->getViewTotalAmount(), 2); ?></b>
                    </td>
                </tr>
                <tr>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 85%;"><?= $ndsName ?>:</td>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 15%;">
                        <b><?= $ndsValue ?></b>
                    </td>
                </tr>
                <tr>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 85%;">Всего к оплате:</td>
                    <td class="text-right" style="vertical-align: middle;padding: 2px 4px;width: 15%;">
                        <b><?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?></b>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 pad3">
            <div class="col-xs-12 pad0 m-t-n" style="border-bottom: 2px solid #000000;">
                <div class="col-xs-12 pad3">
                    <div class="txt-9">
                        Всего наименований <?= count($model->orderDocumentProducts) ?>, на сумму
                        <?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?>
                        руб.
                    </div>
                    <div class="txt-9-b">
                        Всего к
                        оплате: <?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->total_amount / 100)); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-top: 10px !important; z-index: 2;height: 220px;">
                <div class="col-xs-12 pad0" style="height: 65px; text-align: center">
                    <div class="col-xs-3 pad3 v_bottom" style="font-weight: bold; text-align: left">
                        <div> Руководитель:</div>
                    </div>
                    <div class="col-xs-3 pad3 v_bottom">
                        <div>
                            <?= ($model->type != Documents::IO_TYPE_OUT) ? $model->contractor_director_post_name : $company->chief_post_name; ?>
                        </div>
                    </div>
                    <div class="col-xs-3 pad0 v_bottom"></div>
                    <div class="col-xs-3 pad3 v_bottom">
                        <div>
                            <?= ($model->type != Documents::IO_TYPE_OUT) ? $model->contractor_director_name : $company->getChiefFio(true); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 pad0"
                     style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                    <div class="col-xs-3 pad3"> &nbsp; </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> должность </span></div>
                    </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> подпись </span></div>
                    </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>