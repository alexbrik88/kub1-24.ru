<?php

use frontend\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\document\status\PackingListStatus;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceFactureStatus;

/**
 * @var $model OrderDocument
 * @var $id integer
 * @var $document string
 * @var $title string
 * @var $action string
 * @var $type int
 */

if ($action == 'create') {
    $url = Url::to([$document . '/' . $action, 'type' => $type, 'invoiceId' => $model->invoice->id]);
} elseif ($action == 'view') {
    $url = Url::to([$document . '/' . $action, 'type' => $type, 'id' => $id]);
} elseif ($action == 'create-from-order') {
    $url = Url::to([$document . '/' . $action, 'type' => $type, 'orderDocumentID' => $model->id]);
} elseif ($action == 'update-from-order') {
    $url = Url::to([$document . '/' . $action, 'type' => $type, 'orderDocumentID' => $model->id]);
} else {
    return null;
}
$hasInvoice = $model->getInvoice()->exists();
$iconClass = 'add-icon';

if ($document == 'invoice') {
    if ($action == 'update-from-order') {
        $iconClass = 'repeat';
    } elseif ($hasInvoice) {
        $iconClass = 'new-doc';
    }
} elseif ($document == 'packing-list') {
    if ($model->invoice->getPackingList()->exists()) {
        $iconClass = 'new-doc'; //$iconClass = PackingListStatus::getStatusIcon($model->invoice->getPackingList()->min('status_out_id'));
    }
} elseif ($document == 'act') {
    if ($model->invoice->getAct()->exists()) {
        $iconClass = 'new-doc';  //$iconClass = ActStatus::getStatusIcon($model->invoice->getAct()->min('status_out_id'));
    }
} elseif ($document == 'invoice-facture') {
    if ($model->invoice->getInvoiceFactures()->exists()) {
        $iconClass = 'new-doc';  //$iconClass = InvoiceFactureStatus::getStatusIcon($model->invoice->getInvoiceFactures()->min('status_out_id'));
    }
} elseif ($document == 'upd') {
    if ($model->invoice->getUpds()->exists()) {
        $iconClass = 'new-doc';  //$iconClass = 'icon-doc';
    }
}
?>

<?= Html::a($this->render('//svg-sprite', ['ico' => $iconClass]).Html::tag('span', $title), $url, [
    'class' => 'mb-3 button-regular button-hover-content-red w-100 text-left' . ($model->isRejected ? ' disabled' : ''),
    'title' => mb_convert_case(($action == 'create' ? 'Добавить ' : '') . $title, MB_CASE_TITLE)
]) ?>