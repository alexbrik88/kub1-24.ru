<?php

use common\models\document\OrderDocument;
use common\models\document\OrderDocumentInToOut;
use frontend\models\Documents;
use yii\helpers\Url;

/* @var $model OrderDocument */
/* @var $type integer */
/** @var OrderDocumentInToOut $i2o */

if ($model->type == Documents::IO_TYPE_IN):
    if ($in2out = OrderDocumentInToOut::find()->where(['in_order_document' => $model->id])->groupBy(['out_order_document'])->all()):
        if (count($in2out) === 1):
            foreach ($in2out as $i2o):
                if ($outModel = $i2o->outOrderDocument): ?>
                    <a href="<?php echo Url::toRoute(['view', 'type' => Documents::IO_TYPE_OUT, 'id' => $outModel->id]); ?>"
                       class="button-regular button-hover-content-red button-float-icon w-100 text-left">
                        <?= $this->render('//svg-sprite', ['ico' => 'new-doc', 'class' => 'float-icon']) ?>
                        <span class="ml-2">Заказ Покупателя № <?=($outModel->getFullNumber().' от '.date_format(date_create($outModel->document_date), 'd.m.Y'))?></span>
                    </a>
                <?php endif;
            endforeach;

        else: ?>

            <div class="clearfix" style="display: block; position: relative;">
                <div class="col-xs-7 pad0">
                    <a data-toggle="dropdown" class="mb-3 button-regular dropdown-toggle w-100 text-left" title="Заказы Покупателя">
                        <?= $this->render('//svg-sprite', ['ico' => 'new-doc']) . '<span class="ml-2">Заказы Покупателя</span>' ?>
                    </a>
                    <ul class="dropdown-menu documents-dropdown" style="width: 317px;">
                        <?php foreach ($in2out as $i2o):
                            if ($outModel = $i2o->outOrderDocument): ?>
                                <li>
                                    <a href="<?php echo Url::toRoute(['view', 'type' => Documents::IO_TYPE_IN, 'id' => $outModel->id]); ?>"
                                       class="dropdown-item">
                                        <?= $this->render('//svg-sprite', ['ico' => 'new-doc']).'<span class="ml-2">Заказ Покуп. №'.($outModel->getFullNumber().' от '.date_format(date_create($outModel->document_date), 'd.m.Y')).'</span>' ?>
                                    </a>
                                </li>
                            <?php endif;
                        endforeach; ?>
                    </ul>
                </div>
            </div>

        <?php endif;
    endif;
endif;

if ($model->type == Documents::IO_TYPE_OUT): ?>

    <?php if ($model->hasInOrderDocuments()): ?>
        <?php if ($in2out = OrderDocumentInToOut::find()->where(['out_order_document' => $model->id])->groupBy(['in_order_document'])->all()): ?>

            <?php if (count($in2out) === 1): ?>

                <?php foreach ($in2out as $i2o): ?>
                    <?php if ($inModel = $i2o->inOrderDocument): ?>
                        <a href="<?php echo Url::toRoute(['view', 'type' => Documents::IO_TYPE_IN, 'id' => $inModel->id]); ?>"
                           class="button-regular button-hover-content-red button-float-icon w-100 text-left">
                            <?= $this->render('//svg-sprite', ['ico' => 'new-doc', 'class' => 'float-icon']) ?>
                            <span class="ml-2">Заказ Поставщику № <?=($inModel->getFullNumber().' от '.date_format(date_create($inModel->document_date), 'd.m.Y')) ?> </span>
                        </a>
                    <?php endif; ?>
                <?php endforeach; ?>

            <?php else: ?>

                <div class="clearfix" style="display: block; position: relative;">
                    <div class="col-xs-7 pad0">
                        <a data-toggle="dropdown"
                           class="mb-3 button-regular dropdown-toggle tooltipstered text-left"
                           style="width: 100%; height: 44px;margin-left: 0;padding-right: 14px; padding-left: 14px;"
                           title="Заказы Поставщику">
                            <?= $this->render('//svg-sprite', ['ico' => 'new-doc']) . '<span class="ml-2">Заказы Поставщику</span>' ?>
                        </a>
                        <ul class="dropdown-menu documents-dropdown" style="width: 317px;">
                            <?php foreach ($in2out as $i2o): ?>
                                <?php if ($inModel = $i2o->inOrderDocument): ?>
                                    <li>
                                        <a href="<?php echo Url::toRoute(['view', 'type' => Documents::IO_TYPE_IN, 'id' => $inModel->id]); ?>"
                                           class="dropdown-item">
                                            <?= $this->render('//svg-sprite', ['ico' => 'new-doc']).'<span class="ml-2">Заказы Постав. №'.($inModel->getFullNumber().' от '.date_format(date_create($inModel->document_date), 'd.m.Y')).'</span>' ?>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

            <?php endif; ?>
        <?php endif; ?>
    <?php else: ?>
        <?php $out = [];
        foreach ($model->orderDocumentProducts as $orderProduct)
            $out[] = $model->id .'-'. $orderProduct->id;
        ?>
        <a href="<?php echo Url::toRoute(['create', 'type' => Documents::IO_TYPE_IN, 'out' => $out]); ?>"
           class="button-regular button-hover-content-red w-100 text-left pl-3 pr-3">
            <?= $this->render('//svg-sprite', ['ico' => 'add-icon']).'<span class="ml-2">Заказ Поставщику</span>' ?>
        </a>
    <?php endif; ?>
<?php endif; ?>
