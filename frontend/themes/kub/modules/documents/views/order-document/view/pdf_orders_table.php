<?php

use common\components\TextHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use common\models\document\OrderDocument;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$company = $model->company;
$contractor = $model->contractor;
$amount = $model->total_amount;
$discount = $model->getDiscountSum();
$markup = $model->getMarkupSum();
$totalAmount = $model->total_amount;
$ndsAmount = $model->getViewTotalNds();
$isNdsExclude = $model->nds_view_type_id == OrderDocument::NDS_VIEW_OUT;
// nds total row
$ndsName = (isset($model->ndsViewType->name)) ? $model->ndsViewType->name : '-';
$ndsValue = ($model->has_nds) ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';

if ($contractor->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON) {
    $exchangeRate = Currency::find()->where(['id' => $contractor->order_currency])->one()->current_value;
    $amount = round($amount / $exchangeRate);
    $discount = round($discount / $exchangeRate);
    $ndsAmount = round($ndsAmount / $exchangeRate);
    $totalAmount = round($totalAmount / $exchangeRate);
}

?>
<div style="margin-bottom: 10px;">
    <table style=" width: 100%; margin-bottom: 8px; border: 2px solid #000000">
        <thead>
        <tr>
            <th>№</th>
            <th>Товары (работы, услуги)</th>
            <th>Кол-во</th>
            <th>Ед.</th>
            <th>Цена</th>
            <?php if ($model->has_discount) : ?>
                <th>Скидка %</th>
                <th>Цена со скидкой</th>
            <?php endif ?>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->orderDocumentProducts as $order): ?>
            <?php $productTitle = $order->product_title;
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                //if ($model->has_nds && $model->company->isNdsExclude) {
                if ($isNdsExclude) {
                    $priceBase = $order->base_price_no_vat;
                    $priceOne = ($model->type == Documents::IO_TYPE_IN) ? $order->purchase_price_no_vat : $order->selling_price_no_vat;
                    $orderAmount = ($model->type == Documents::IO_TYPE_IN) ? $order->amount_purchase_no_vat : $order->amount_sales_no_vat;
                } else {
                    $priceBase = $order->base_price_with_vat;
                    $priceOne = ($model->type == Documents::IO_TYPE_IN) ? $order->purchase_price_with_vat : $order->selling_price_with_vat;
                    $orderAmount = ($model->type == Documents::IO_TYPE_IN) ? $order->amount_purchase_with_vat : $order->amount_sales_with_vat;
                }
            if ($contractor->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON) {
                $priceBase = round($priceBase / $exchangeRate);
                $priceOne = round($priceOne / $exchangeRate);
                $orderAmount = round($orderAmount / $exchangeRate);
            } ?>
            <tr>
                <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                <td style=" width: 0"><?= $productTitle; ?></td>
                <td style="text-align: right; width: 10%">
                    <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                </td>
                <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                <?php if ($model->has_discount) : ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($priceBase, 2); ?>
                    </td>
                    <td style="text-align: right; width: 13%">
                        <?= strtr($order->discount, ['.' => ',']) ?>
                    </td>
                <?php endif ?>
                <td style="text-align: right; width: 13%">
                    <?= TextHelper::invoiceMoneyFormat($priceOne, 2); ?>
                </td>
                <td style="text-align: right; width: 15%">
                    <?= TextHelper::invoiceMoneyFormat($orderAmount, 2); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <table class="it-b">
        <?php if ($model->has_discount) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Сумма скидки:</td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($discount, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
        <?php if ($model->has_markup) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Сумма наценки:</td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($markup, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;"><?= $ndsName ?>:</td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= $ndsValue ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Итого:</td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= TextHelper::invoiceMoneyFormat($model->getViewTotalAmount(), 2); ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Всего к оплате:</td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= TextHelper::invoiceMoneyFormat($amount, 2); ?></b>
            </td>
        </tr>
        <?php /*
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">Вес:</td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= $model->getOrderDocumentProducts()->joinWith('product')->sum('weight') * 1; ?></b>
            </td>
        </tr>*/ ?>
    </table>
</div>


