<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 7:48
 */

use common\models\product\Product;
use frontend\components\Icon;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\document\status\OrderDocumentStatus;
use common\components\helpers\ArrayHelper;
use frontend\models\Documents;
use common\models\document\OrderDocumentInToOut;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $type integer */

$productionTypeArray = explode(', ', $model->production_type);
$hasInvoice = $model->getInvoice()->exists();
if ($hasInvoice) {
    $serviceCountActs = $model->invoice->getActs()->joinWith('orderActs')->select('SUM({{order_act}}.[[quantity]])')->scalar();
    $serviceCountInvoice = $model->invoice->getOrders()->joinWith('product')->andWhere([
        'product.production_type' => Product::PRODUCTION_TYPE_SERVICE,
    ])->select('SUM({{order}}.[[quantity]])')->scalar();

    $productCountPL = $model->invoice->getPackingLists()->joinWith('orderPackingLists')->select('SUM({{order_packing_list}}.[[quantity]])')->scalar();
    $productCountInvoice = $model->invoice->getOrders()->joinWith('product')->andWhere([
        'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
    ])->select('SUM({{order}}.[[quantity]])')->scalar();
}

$statusArray = OrderDocumentStatus::find()->all();

?>
<style type="text/css">
<?php foreach ($statusArray as $status) : ?>
    .order-document-status-wrap[data-status="<?= $status->id ?>"] .button-regular {
        background-color: <?= $status->color; ?>;
        border-color: <?= $status->color; ?>;
        color: #ffffff;
    }
<?php endforeach ?>
</style>
<div class="order-document-status-wrap sidebar-title d-flex flex-wrap align-items-center"
    data-status="<?= $model->status->id ?>"
    style="margin-bottom: 4px;">
    <div class="column flex-grow-1 mt-1 mt-xl-0 pr-0 order-doc-status-set">
        <div class="dropdown w-100">
            <?= Html::button(Html::tag('span', $model->status->name, [
                'class' => 'order-document-status-name',
            ]) . Icon::get('shevron', [
                'class' => 'position-absolute',
                'style' => 'right: 10px;',
            ]), [
                'class' => 'button-regular dropdown-toggle w-100 pr-4 position-relative',
                'data-toggle' => 'dropdown',
            ]) ?>
            <div class="dropdown-menu w-100" aria-labelledby="documentStatusButton">
                <?php foreach ($statusArray as $status): ?>
                    <?= Html::a($status->name, ['update-status', 'id' => $model->id], [
                        'data-status' => $status->id,
                        'class' => 'document-status-link d-block px-3 py-2' . ($status->id ? ' active' : ''),
                    ]) ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="column mt-1 mt-xl-0">
        <div class="button-regular w-100 order-document_status_date pl-1 pr-1" title="Дата изменения статуса">
            <?= date("d.m.Y", $model->status_updated_at); ?>
        </div>
    </div>
</div>

<?= $this->render('status_rows/_in_to_out_button', [
    'model' => $model,
    'type' => $type
]) ?>

<?php if (!$model->invoice): ?>
    <?= $this->render('status_rows/_first_row', [
        'document' => 'invoice',
        'title' => 'Счет',
        'model' => $model,
        'action' => 'create-from-order',
        'type' => $type,
    ]); ?>
<?php else: ?>
    <?= $this->render('status_rows/_first_row', [
        'document' => 'invoice',
        'title' => $model->canUpdateInvoice() ? 'Обновить счет' : 'Счет',
        'model' => $model,
        'action' => $model->canUpdateInvoice() ? 'update-from-order' : 'view',
        'id' => $model->invoice->id,
        'type' => $type,
    ]); ?>
<?php endif; ?>
<?php if (in_array(Product::PRODUCTION_TYPE_SERVICE, $productionTypeArray) && $hasInvoice): ?>
    <?php if (!$model->invoice->acts): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'act',
            'title' => 'акт',
            'model' => $model,
            'action' => 'create',
            'id' => 0,
            'type' => $type,
        ]);
        ?>
    <?php elseif (count($model->invoice->acts) == 1 && $serviceCountActs == $serviceCountInvoice): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'act',
            'title' => 'акт',
            'model' => $model,
            'action' => 'view',
            'id' => $model->invoice->acts[0]->id,
            'type' => $type,
        ]);
        ?>
    <?php else: ?>
        <?= $this->render('status_rows/_many_rows', [
            'document' => 'act',
            'title' => 'акт',
            'model' => $model,
            'addAvailable' => $serviceCountInvoice != $serviceCountActs,
        ]); ?>
    <?php endif; ?>
<?php endif; ?>
<?php if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray) && $hasInvoice): ?>
    <?php if (!$model->invoice->packingLists): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'packing-list',
            'title' => 'Товарная накладная',
            'model' => $model,
            'action' => 'create',
            'id' => 0,
            'type' => $type,
        ]);
        ?>
    <?php elseif (count($model->invoice->packingLists) == 1 && $productCountPL == $productCountInvoice): ?>
        <?= $this->render('status_rows/_first_row', [
            'document' => 'packing-list',
            'title' => 'Товарная накладная',
            'model' => $model,
            'action' => 'view',
            'id' => $model->invoice->packingLists[0]->id,
            'type' => $type,
        ]);
        ?>
    <?php else: ?>
        <?= $this->render('status_rows/_many_rows', [
            'document' => 'packing-list',
            'title' => 'Товарная накладная',
            'model' => $model,
            'addAvailable' => $productCountPL != $productCountInvoice,
        ]);
        ?>
    <?php endif; ?>
<?php endif; ?>

<?php $this->registerJs('
    $(document).ready(function() {
        $(document).on("click", ".order-document-status-wrap .document-status-link", function (e) {
            e.preventDefault();
            $.post(this.href, {status_id: $(this).data("status")}, function (data) {
                console.log(data);
                if (data.result == true) {
                    $(".order-document-status-wrap").attr("data-status", data.status.id);
                    $(".order-document-status-name").text(data.status.name);
                    $(".order-document_status_date").text(data.date);
                }
            });
        })
    });
'); ?>
