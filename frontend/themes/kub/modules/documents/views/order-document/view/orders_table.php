<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 7:11
 */

use common\components\TextHelper;
use common\models\product\Product;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

//$isNdsExclude = $model->has_nds && $model->company->isNdsExclude;
$isNdsExclude = $model->nds_view_type_id == OrderDocument::NDS_VIEW_OUT;
$sum = $model->total_amount;
$discount = $model->getDiscountSum();
?>
<style>
    .bord-dark tr, .bord-dark td, .bord-dark th {
        border: 1px solid #000000;
    }
</style>
<div class="portlet">
    <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
        <thead>
        <tr style="text-align: center;">
            <th> №</th>
            <th> Товары (работы, услуги)</th>
            <?php if ($user->config->order_document_product_article): ?>
                <th> Артикул</th>
            <?php endif; ?>
            <th> Кол-во</th>
            <th> Ед.</th>
            <th> Цена</th>
            <?php if ($model->has_discount) : ?>
                <th> Скидка %</th>
                <th> Цена со скидкой</th>
            <?php endif ?>
            <th> Сумма</th>
        </tr>
        </thead>
        <tbody class="bord-dark">
        <?php foreach ($model->orderDocumentProducts as $order): ?>
            <?php if ($isNdsExclude) {
                $priceBase = $order->base_price_no_vat;
                $priceOne = ($model->type == Documents::IO_TYPE_IN) ? $order->purchase_price_no_vat : $order->selling_price_no_vat;
                $amount = ($model->type == Documents::IO_TYPE_IN) ? $order->amount_purchase_no_vat : $order->amount_sales_no_vat;
            } else {
                $priceBase = $order->base_price_with_vat;
                $priceOne = ($model->type == Documents::IO_TYPE_IN) ? $order->purchase_price_with_vat : $order->selling_price_with_vat;
                $amount = ($model->type == Documents::IO_TYPE_IN) ? $order->amount_purchase_with_vat : $order->amount_sales_with_vat;
            }
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE; ?>
            <tr>
                <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                <td style=" width: 0">
                    <?= $order->product_title; ?>
                </td>
                <?php if ($user->config->order_document_product_article): ?>
                <td style="width: 10%;text-align: center">
                    <?= $order->product->article ? $order->product->article : '---'; ?>
                </td>
                <?php endif; ?>
                <td style="text-align: right; width: 10%">
                    <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                </td>
                <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                <?php if ($model->has_discount) : ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($priceBase, 2); ?>
                    </td>
                    <td style="text-align: right; width: 13%">
                        <?= strtr($order->discount, ['.' => ',']) ?>
                    </td>
                <?php endif ?>
                <td style="text-align: right; width: 13%">
                    <?= TextHelper::invoiceMoneyFormat($priceOne, 2); ?>
                </td>
                <td style="text-align: right; width: 15%">
                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
