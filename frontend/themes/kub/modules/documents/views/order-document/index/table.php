<?php

use common\components\date\DateHelper;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\models\OrderDocumentSearch;
use frontend\rbac\permissions;
use frontend\themes\kub\components\Icon;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\web\View;

/** @var View $this */
/** @var array $dashBoardData */
/** @var OrderDocumentSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var Employee $user */
/** @var integer $type */

?>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', [
            'totalCount' => $dataProvider->totalCount,
            'scroll' => true,
        ]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return Html::checkbox('OrderDocument[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-id' => $model->id,
                        'data-sum' => $model->total_amount,
                        'data-contractor' => $model->contractor_id,
                    ]);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => '№ заказа',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) use ($type) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $model,
                    ]) ? Html::a($model->fullNumber, ['/documents/order-document/view', 'id' => $model->id, 'type' => $type])
                        : $model->fullNumber;
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата заказа',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контр&shy;агент',
                'encodeLabel' => false,
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'filter' => FilterHelper::getContractorList(Documents::IO_TYPE_OUT, OrderDocument::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return '<span title="' . htmlspecialchars($model->contractor_name_short) . '">' . $model->contractor_name_short . '</span>';
                },
            ],
            [
                'attribute' => 'total_amount',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $price = TextHelper::invoiceMoneyFormat($model->total_amount, 2);
                    return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                },
            ],
            [
                'label' => 'Оплачено',
                'attribute' => 'paymentSum',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    if ($model->invoice) {
                        return ($model->invoice->getPaidAmount()) ?
                            TextHelper::invoiceMoneyFormat($model->invoice->getPaidAmount(), 2) : '';
                    }
                    return '';
                },
            ],
            [
                'attribute' => 'shipped',
                'label' => 'Отгружено',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_shipped' . ($user->config->order_document_shipped ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_shipped' . ($user->config->order_document_shipped ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->getShippedCount();
                },
            ],
            [
                'attribute' => 'reserve',
                'label' => 'Резерв',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_reserve' . ($user->config->order_document_reserve ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'sorting col_order_document_reserve' . ($user->config->order_document_reserve ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->getOrderDocumentProducts()
                        ->sum('quantity') * 1 - $model->getShippedCount();
                },
            ],
            [
                'attribute' => 'status_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'style' => 'white-space: initial;',
                ],
                'filter' => $searchModel->getStatusFilter(),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->status->name;
                },
            ],
            [
                'attribute' => 'ship_up_to_date',
                'label' => 'План. дата отгрузки',
                'headerOptions' => [
                    'class' => 'sorting col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'store_id',
                'label' => 'Со склада',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getStoreFilter(),
                's2width' => '200px',
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    return $model->store ? $model->store->name : '---';
                },
            ],
            [
                'attribute' => 'invoiceState',
                'label' => 'Счет',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'filter' => $searchModel->getInvoiceFilter(),
                'value' => function (OrderDocument $model) use ($type) {
                    $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);

                    return $model->invoice ?
                        Html::a($model->invoice->fullNumber, [
                            '/documents/invoice/view',
                            'id' => $model->invoice_id,
                            'type' => $type,
                        ]) :
                        Html::a(Icon::get('add-icon') . '<span class="pl-1">Счёт</span>', [
                            '/documents/invoice/create-from-order',
                            'type' => $type,
                            'orderDocumentID' => $model->id,
                        ], [
                            'title' => 'Добавить счет',
                            'class' => 'button-regular button-hover-content-red' . ($canCreate ? null : ' disabled no-rights-link'),
                            'style' => '',
                        ]);
                },
            ],
            [
                'attribute' => 'actState',
                'label' => 'Акт',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                    'style' => '',
                ],
                'filter' => $searchModel->getActFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->invoice) {
                        foreach ($model->invoice->acts as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/act/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);
                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/act/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';
                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddAct()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model, 'type' => Documents::IO_TYPE_OUT]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">Добавить</span>', [
                                '/documents/act/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'class' => 'button-regular button-hover-content-red' . ($model->invoice->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null),
                                'title' => 'Добавить акт',
                            ]);
                        }
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'packingListState',
                'label' => 'ТН',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                    'style' => '',
                ],
                'filter' => $searchModel->getPackingListFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->invoice) {
                        foreach ($model->invoice->packingLists as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/packing-list/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);
                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/packing-list/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';
                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddPackingList()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">Добавить</span>', [
                                '/documents/packing-list/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'title' => 'Добавить ТН',
                                'class' => 'button-regular button-hover-content-red' .
                                    ($model->invoice->isRejected ? ' disabled' : '') .
                                    ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null),
                            ]);
                        }
                    }

                    return $content;
                }
            ],
            [
                'attribute' => 'updState',
                'label' => 'УПД',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_upd' . ($user->config->order_document_upd ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_upd' . ($user->config->order_document_upd ? '' : ' hidden'),
                    'style' => '',
                ],
                'filter' => $searchModel->getUpdFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->invoice) {
                        foreach ($model->invoice->upds as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/upd/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);
                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/upd/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';
                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddUpd()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">Добавить</span>', [
                                '/documents/upd/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'title' => 'Добавить УПД',
                                'class' => 'button-regular button-hover-content-red' .
                                    ($model->invoice->isRejected ? ' disabled' : '') .
                                    ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null),
                            ]);
                        }
                    }

                    return $content;
                }
            ],
            [
                'attribute' => 'invoiceFactureState',
                'label' => 'Счёт-фактура',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                    'style' => '',
                ],
                'filter' => $searchModel->getInvoiceFactureFilter(),
                'format' => 'raw',
                'value' => function (OrderDocument $model) {
                    $content = '';
                    if ($model->has_nds == OrderDocument::HAS_NO_NDS) {
                        return $content;
                    }
                    if ($model->invoice) {
                        foreach ($model->invoice->invoiceFactures as $doc) {
                            $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                            $docLink = Html::a($doc->fullNumber, [
                                '/documents/invoice-facture/view',
                                'type' => $doc->type,
                                'id' => $doc->id,
                            ], [
                                'class' => $canView ? '' : 'no-rights-link',
                            ]);

                            $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/invoice-facture/file-get',
                                'type' => $model->invoice->type,
                                'id' => $doc->id,
                                'file-id' => $doc->file->id,
                            ], [
                                'target' => '_blank',
                                'class' => $canView ? '' : 'no-rights-link',
                            ]) : '';

                            $content .= Html::tag('div', $docLink . $fileLink);
                        }
                        if ($model->invoice->getCanAddInvoiceFacture()) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                            $content .= Html::a(Icon::get('add-icon') . '<span class="pl-1">Добавить</span>', [
                                '/documents/invoice-facture/create',
                                'type' => $model->invoice->type,
                                'invoiceId' => $model->invoice->id,
                            ], [
                                'class' => 'button-regular button-hover-content-red' .
                                    ($model->invoice->isRejected ? ' disabled' : '') .
                                    ($canCreate ? '' : ' no-rights-link'),
                                'style' => ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : ''),
                                'title' => 'Добавить СФ',
                            ]);
                        }
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'author_id',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                    'style' => 'white-space: initial;',
                ],
                'format' => 'raw',
                'filter' => $searchModel->getAuthorFilter(),
                's2width' => '300px',
                'value' => function (OrderDocument $model) {
                    return $model->author ? $model->author->getFio(true) : $model->contractor_name_short;
                },
            ],
        ],
    ]); ?>
