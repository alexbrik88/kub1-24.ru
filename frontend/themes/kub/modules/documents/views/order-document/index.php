<?php

use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\models\OrderDocumentSearch;
use frontend\rbac\permissions;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use frontend\themes\kub\helpers\KubHelper;

/** @var View $this */
/** @var array $dashBoardData */
/** @var OrderDocumentSearch $searchModel */
/** @var ActiveDataProvider $dataProvider */
/** @var Employee $user */
/** @var integer $type */

$this->title = 'Заказы ' . ($type == Documents::IO_TYPE_OUT ? 'покупателей' : ' у поставщика');

KubHelper::setStatisticsFontSize(max($dashBoardData[0]['amount'], $dashBoardData[1]['amount'], $dashBoardData[2]['amount']) );
$period = StatisticPeriod::getSessionName();

$dropItems = [];

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');

$exists = OrderDocument::find()
    ->where(['company_id' => Yii::$app->user->identity->company->id])
    ->andWhere(['is_deleted' => false])->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет заказов. Измените период, чтобы увидеть имеющиеся заказы.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного заказа.';
}

$tableRenderParams = [
    'type' => $type,
    'user' => $user,
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    'emptyMessage' => $emptyMessage,
    'tabViewClass' => $tabViewClass,
];

$detailingBy = ($user->config->order_document_detailing)
    ? OrderDocumentSearch::DETAILING_BY_NOMENCLATURE
    : OrderDocumentSearch::DETAILING_BY_UNSET;

$isDetailingTable = ($type == Documents::IO_TYPE_OUT && $detailingBy === OrderDocumentSearch::DETAILING_BY_NOMENCLATURE);
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to(['create', 'type' => $type]) ?>">
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
            </svg>
            <span>Добавить</span>
        </a>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="count-card wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[0]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[0]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[0]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_red wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[1]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[1]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[1]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="count-card count-card_green wrap">
                    <div class="count-card-main">
                        <?= TextHelper::invoiceMoneyFormat($dashBoardData[2]['amount'], 2); ?> ₽
                    </div>
                    <div class="count-card-title">
                        <?=$dashBoardData[2]['name']?>
                    </div>
                    <hr>
                    <div class="count-card-foot">
                        Количество заказов: <?= $dashBoardData[2]['count']; ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(['zIndex' => 999]); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'order_document_payment_sum'],
                    ['attribute' => 'order_document_shipped'],
                    ['attribute' => 'order_document_reserve'],
                    ['attribute' => 'order_document_ship_up_to_date'],
                    ['attribute' => 'order_document_stock_id'],
                    ['attribute' => 'order_document_invoice'],
                    ['attribute' => 'order_document_act'],
                    ['attribute' => 'order_document_packing_list'],
                    ['attribute' => 'order_document_upd'],
                    ['attribute' => 'order_document_invoice_facture'],
                    ['attribute' => 'order_document_responsible_employee_id'],
                    ($isDetailingTable)
                        ? ['attribute' => 'order_document_supplier_document_date']
                        : []
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
            <?php if ($type == Documents::IO_TYPE_OUT): ?>
                <div class="ml-2 dropdown dropdown-settings d-inline-block">
                    <div class="mb-2">
                        <?= Html::button(Icon::get('pie-pal'), [
                            'class' => 'button-list button-hover-transparent button-clr',
                            'data-toggle' => 'dropdown',
                            'aria-haspopup' => 'true',
                            'aria-expanded' => 'false',
                            'title' => 'Детализация'
                        ]) ?>
                        <ul class="dropdown-popup dropdown-popup-settings dropdown-menu" role="menu" style="padding: 10px 15px;">
                            <li class="mb-2">
                                <label>Детализация</label>
                            </li>
                            <?= Html::radioList('_detailingTableBy', $detailingBy, OrderDocumentSearch::$detailingBy, [
                                'encode' => false,
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    $input = Html::radio($name, $checked, [
                                        'value' => $value,
                                        'label' => Html::tag('span', $label, [
                                            'class' => 'nowrap',
                                            'style' => 'font-weight: bold;',
                                        ]),
                                        'data-url' => Url::current(['PaymentOrderSearch[detailingTableBy]' => $index]),
                                        'onchange' => new \yii\web\JsExpression('location.href = this.dataset.url')
                                    ]);

                                    return Html::tag('li', $input);
                                },
                            ]); ?>
                        </ul>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер заказа или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php if ($isDetailingTable): ?>
        <?= $this->render('index/table_detailing', $tableRenderParams) ?>
    <?php else: ?>
        <?= $this->render('index/table', $tableRenderParams) ?>
    <?php endif; ?>


</div>

<?= $this->render('index/summary-select', [
    'type' => $type,
    'detailingBy' => $detailingBy
]); ?>

<?php

$this->registerJs(<<<JS
    function submitOperationCheckbox(url, button) {
        var inputs = $('.joint-operation-checkbox:checked');
        var l;
        
        if (button) {
            l = Ladda.create(button);
            l.start();
        }
        $.post(url, inputs.serialize(), function(data) {});
    }
    
    $(document).on('click', '.many-update-status', function(){
        var url = $(this).data('url');
        
        submitOperationCheckbox(url);
        
    });

    $(document).on('click', '.modal-many-create-invoice', function(){
        var url = $(this).data('url');
       
        submitOperationCheckbox(url, $(this)[0]);
    });

    $(document).on('click', '.modal-many-create-act', function(){
        var url = $(this).data('url');
       
        submitOperationCheckbox(url, $(this)[0]);
    });

    $(document).on('click', '.modal-many-create-packing-list', function(){
        var url = $(this).data('url');
       
        submitOperationCheckbox(url, $(this)[0]);
    });

    $(document).on('click', '.modal-many-create-upd', function(){
        var url = $(this).data('url');
       
        submitOperationCheckbox(url, $(this)[0]);
    });
    
    $( document ).ajaxComplete(function( event, xhr, settings ) {
        Ladda.stopAll();
    });
    
    $(document).on('click', '.create-in-from-out', function() {
        let url = $(this).data('url');
        
        $('.joint-operation-checkbox').each(function(i,v) {
            let documentId = $(v).data('id');
            let productId = $(v).data('product');
            
            if ($(v).prop('checked')) {
                if (documentId > 0 && productId > 0)
                    url += '&out[]=' + documentId + '-' + productId;
            }
        });
        
        location.href = url;
    });
    
JS, View::POS_READY
);