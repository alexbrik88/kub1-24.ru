<?php

use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\bank\Bank;
use common\models\Contractor;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $orderDocument common\models\document\Invoice */

$apiBank = null;
$apiBankUrl = null;
$userBank = $orderDocument->contractor_bik ? Bank::findOne([
    'bik' => $orderDocument->contractor_bik,
    'is_blocked' => false,
]) : null;

if ($orderDocument->contractor_bik && ($apiBank = Banking::getBankByBik($orderDocument->contractor_bik)) !== null) {
    if ($userBank === null) {
        $userBank = $apiBank;
    }
    $alias = Banking::aliasByBik($orderDocument->contractor_bik);
    $apiBankUrl = Url::to([
        "/cash/banking/{$alias}/default/pay-bill",
        'uid' => $orderDocument->uid,
    ]);
}

$payUrl = Url::to([
    "/documents/invoice/out-view",
    'uid' => $orderDocument->uid,
    'payment' => 1,
]);
$isPhysicalPerson = $orderDocument->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON;
?>

<?php if ($apiBank !== null && $apiBankUrl !== null) : ?>
    <div class="pay-block pay-link target border-bottom-e4" data-url="<?= $apiBankUrl; ?>">
        <table>
            <tr>
                <td style="width: 80px; padding: 0; padding-right: 10px; vertical-align: middle;">
                    <?= ImageHelper::getThumb($apiBank->getUploadDirectory() . $apiBank->little_logo_link, [70, 54], [
                        'cutType' => EasyThumbnailImage::THUMBNAIL_INSET,
                    ]); ?>
                </td>
                <td>
                    <span class="main-text text-bold"><?= $apiBank->bank_name; ?></span>
                </td>
                <td style="width: 20px;padding: 15px 0;">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 -4 31.49 31.49">
                        <path style="fill:#122c49;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                        C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                        c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                    </svg>
                </td>
            </tr>
        </table>
    </div>
    <div class="pay-block border-bottom-e4" style="padding: 10px 15px">
        <table>
            <tr>
                <td style="width: 100%; padding: 5px 0;">
                    <div class="main-text" style="margin-bottom: 5px;">
                        Платежка будет создана автоматически в вашем клиент-банке,
                        после нажатия на логотип банка.
                    </div>
                    <div class="additional-text">
                        Платежка создается за счет интеграции Банка и
                        <br>
                        Сервиса выставления счетов
                        <?= Html::a('КУБ', Yii::$app->params['serviceSite'], [
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
<?php else : ?>
    <?php if ($userBank !== null) : ?>
        <div class="pay-block pay-link target border-bottom-e4" data-url="<?= $userBank->url; ?>">
            <table>
                <tr>
                    <td style="width: 80px; padding: 0; padding-right: 10px; vertical-align: middle;">
                        <?= ImageHelper::getThumb($userBank->getUploadDirectory() . $userBank->little_logo_link, [70, 54], [
                        'cutType' => EasyThumbnailImage::THUMBNAIL_INSET,
                    ]); ?>
                    </td>
                    <td>
                        <span class="main-text text-bold"><?= $userBank->bank_name; ?></span>
                    </td>
                    <td style="width: 20px;padding: 15px 0;">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 -4 31.49 31.49">
                            <path style="fill:#122c49;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                            C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                            c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                        </svg>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif; ?>
    <?php if ($isPhysicalPerson) : ?>
        <?php if (YII_ENV == 'dev' || in_array($orderDocument->company_id, [1, 486, 30495])) : ?>
            <div style="padding: 15px;">
                <div class="form-group">
                    Для оплаты счета № <?= $orderDocument->getFullNumber() ?>
                    от <?= date_format(date_create($orderDocument->document_date), 'd.m.Y') ?>г,<br>
                    на сумму <?= number_format($orderDocument->total_amount_with_nds/100, 2, ',', ' ') ?> руб.,
                    укажите сумму счета<br>
                    и нажмите "Оплатить".
                </div>
                <div class="form-group">
                    Оплата будет произведена через платежную<br>
                    систему <span style="color: #F48723;">QIWI</span> на счет платежного агента ООО «КУБ»
                    <?php if ($orderDocument->company_id != 1) : ?>
                        <br>
                        с последующим зачислением <?= $orderDocument->company->getTitle(true) ?>
                    <?php endif ?>
                </div>

                <iframe width="300" height="300" src="https://widget.qiwi.com/widgets/middle-widget-300x300?publicKey=2tbp1WQvsgQeziGY9vTLe9vDZNg7tmCymb4Lh6STQokqKrpCC6qrUUKEDZAJ7orpdMagCqapdmb9DjbQkStALmcU2egNERsreZZYTavVTgndXJAbLyFYyAyH4bNmv" allowtransparency="true" scrolling="no" frameborder="0"></iframe>
            </div>
        <?php endif ?>
    <?php else : ?>
        <div class="pay-block pay-link border-bottom-e4" data-url="<?= $payUrl; ?>">
            <table>
                <tr>
                    <td style="width: 80px; padding: 15px 0; vertical-align: middle;">
                        <svg xmlns="http://www.w3.org/2000/svg" width="27" height="40" viewBox="0 0 27 40" style="margin-left: 13px;">
                          <defs>
                            <style>
                              .cls-1 {
                                fill: #122c49;
                                fill-rule: evenodd;
                              }
                            </style>
                          </defs>
                          <path class="cls-1" d="M100.868,43.889v3.556A3.586,3.586,0,0,0,104.463,51h3.594Zm3.595,8.889a5.378,5.378,0,0,1-5.391-5.333V43H86.493A4.482,4.482,0,0,0,82,47.445V78.556A4.482,4.482,0,0,0,86.493,83H91.03a4.434,4.434,0,0,0,4.447-4.4V74.156a4.5,4.5,0,0,1,4.538-4.489h4.537a4.435,4.435,0,0,0,4.448-4.4V52.778h-4.537Zm-17.97-6.222h8.985a0.889,0.889,0,1,1,0,1.778H86.493A0.889,0.889,0,1,1,86.493,46.556Zm17.97,19.555H86.493a0.889,0.889,0,1,1,0-1.778h17.97A0.889,0.889,0,1,1,104.463,66.111Zm0-4.444H86.493a0.889,0.889,0,1,1,0-1.778h17.97A0.889,0.889,0,1,1,104.463,61.667Zm0-4.444H86.493a0.889,0.889,0,1,1,0-1.778h17.97A0.889,0.889,0,1,1,104.463,57.222Zm2.7,16.267a0.875,0.875,0,0,0-1.258,0l-4.313,4.267-0.943-.933a0.875,0.875,0,0,0-1.258,0,0.853,0.853,0,0,0,0,1.245l1.572,1.556a0.875,0.875,0,0,0,1.258,0l4.942-4.889A0.854,0.854,0,0,0,107.158,73.489Z" transform="translate(-82 -43)"/>
                        </svg>
                    </td>
                    <td style="padding: 15px 0;">
                        <div class="main-text">Скачать платежку</div>
                        <div class="additional-text">Платежка в формате 1С,<br>для загрузки в клиент-банк</div>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif ?>
<?php endif; ?>
