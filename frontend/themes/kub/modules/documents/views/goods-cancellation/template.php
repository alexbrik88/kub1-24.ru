<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\InvoiceContractorSignature;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;

/* @var $this yii\web\View */
/* @var $model GoodsCancellation */
/* @var $addStamp boolean */
/* @var $showHeader boolean */
/* @var $_params_ array */

common\assets\DocumentTemplateAsset::register($this);
$this->params['asset'] = common\assets\DocumentTemplateAsset::class;

$company = $model->company;
$contractor = $model->contractor;

$signatureHeight = $accountantSignatureHeight = $defaultSignatureHeight = 50;

$showHeader = ArrayHelper::getValue($_params_, 'showHeader', true);
$addStamp = ArrayHelper::getValue($_params_, 'addStamp', false);
$accountantSignatureLink = $signatureLink = $printLink = $logoLink = null;

$logoLink = EasyThumbnailImage::thumbnailSrc(
    $company->getImage('logoImage'),
    300,
    100,
    EasyThumbnailImage::THUMBNAIL_INSET_BOX
);
if ($addStamp) {
    if ($model->signed_by_name) {
        if ($model->employeeSignature) {
            $accountantSignatureHeight = $signatureHeight = ImageHelper::getSignatureHeight($model->employeeSignature->file ?? null, 50, 100);
            $accountantSignatureLink = $signatureLink = $model->employeeSignature ? EasyThumbnailImage::thumbnailSrc(
                $model->employeeSignature->file,
                165,
                $accountantSignatureHeight,
                EasyThumbnailImage::THUMBNAIL_INSET_BOX
            ) : null;
        }
    } else {
        $signatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefSignatureImage'), 50, 100);
        $signatureLink = EasyThumbnailImage::thumbnailSrc(
            $model->getImage('chiefSignatureImage'),
            165,
            $signatureHeight,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );

        if (!$company->chief_is_chief_accountant) {
            $accountantSignatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefAccountantSignatureImage'), 50, 100);
            $accountantSignatureLink = EasyThumbnailImage::thumbnailSrc(
                $model->getImage('chiefAccountantSignatureImage'),
                165,
                $accountantSignatureHeight,
                EasyThumbnailImage::THUMBNAIL_INSET_BOX
            );
        } else {
            $accountantSignatureHeight = $signatureHeight;
            $accountantSignatureLink = $signatureLink;
        }
    }
    $printLink = EasyThumbnailImage::thumbnailSrc(
        $model->getImage('printImage'),
        150,
        150,
        EasyThumbnailImage::THUMBNAIL_INSET_BOX
    );
}

$invoiceContractorSignature = $company->invoiceContractorSignature;
if (!$invoiceContractorSignature) {
    $invoiceContractorSignature = new InvoiceContractorSignature();
    $invoiceContractorSignature->company_id = $model->company_id;
}

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$ndsName = $model->ndsViewType->name;
$ndsValue = $model->hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';
$precision = $model->price_precision;
$isArticle = $model->show_article ?? false;
$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";

?>

<div class="document-template invoice-template">
    <?PHP IF ($showHeader) : ?>
        <table>
            <tbody>
                <tr>
                    <td class="p0ad doc-title" style="width: 60%;">
                        <?= strpos($model->company->name_short, 'ИП ') === 0 ?
                            $model->company->name_full :
                            $model->company->getShortName(); ?>
                    </td>
                    <td class="p0ad text-r" style="width: 40%;">
                        <?php if ($logoLink) : ?>
                            <img src="<?= $logoLink ?>" alt="" style="max-height: 100px; max-width: 100%">
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?PHP endif ?>

    <div class="doc-main-title text-c" style="margin: 12px 0;">
        Списание товара № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?>
    </div>

    <div style="border-bottom: 2px solid #000000;"></div>

    <table style="margin: 5px 0;">
        <tr>
            <td class="v-al-t pad-t-5" style="width: 7%">
                Склад:
            </td>
            <td class="pad-t-5 font-b">
                <?= $model->store->name ?>
            </td>
        </tr>
    </table>

    <table class="" style="margin: 8px 0; border: 2px solid #000000;">
        <thead>
            <tr>
                <th class="bor">№</th>
                <?php if ($isArticle) : ?>
                    <th class="bor">Артикул</th>
                <?php endif ?>
                <th class="bor">Товары (работы, услуги)</th>
                <th class="bor">Кол-во</th>
                <th class="bor">Ед.</th>
                <?php if ($model->has_weight) : ?>
                    <th class="bor"> Вес, кг</th>
                <?php endif; ?>
                <?php if ($model->has_volume) : ?>
                    <th class="bor"> Объем, м2</th>
                <?php endif; ?>
                <th class="bor">Цена закупки</th>
                <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                    <th class="bor">
                        <?= 'Скидка' . ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'); ?>
                    </th>
                    <th class="bor">
                        Цена со скидкой
                    </th>
                <?php endif ?>
                <th class="bor">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->orders as $order) : ?>
                <?php
                $productTitle = $order->product_title;
                $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                if ((float)$order->quantity != (int)($order->quantity)) {
                    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
                } else {
                    $order->quantity = round($order->quantity);
                }
                ?>
                <tr>
                    <td class="bor" style="text-align: center; width: 5%">
                        <?= $order->number; ?>
                    </td>
                    <?php if ($isArticle) : ?>
                        <td class="bor" style="width: 15%">
                            <?= $order->article; ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="word-break: break-word;">
                        <?= $productTitle ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 10%">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 7%">
                        <?= $unitName ?>
                    </td>
                    <?php if ($model->has_weight) : ?>
                        <td class="bor" style="text-align: right; width: 7%">
                            <?= ($order->weight) ? TextHelper::invoiceMoneyFormat($order->weight * 100, $model->weightPrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_volume) : ?>
                        <td class="bor" style="text-align: right; width: 7%">
                            <?= ($order->volume) ? TextHelper::invoiceMoneyFormat($order->volume * 100, $model->volumePrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                        <td class="bor" style="text-align: right; width: 13%">
                            <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                        </td>
                        <td class="bor" style="text-align: right; width: 13%">
                            <?= $order->getDiscountViewValue() ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table>
        <tbody>
            <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                <tr>
                    <td class="text-r" style="width: 85%;">
                        <?= YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Сумма скидки'; ?><?= $currCode ?>:
                    </td>
                    <td class="text-r" style="width: 15%;">
                        <b><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></b>
                    </td>
                </tr>
            <?php endif ?>
            <tr>
                <td class="text-r" style="width: 85%;">Итого<?= $currCode ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></b>
                </td>
            </tr>
            <tr>
                <td class="text-r" style="width: 85%;"><?= $ndsName ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= $ndsValue ?></b>
                </td>
            </tr>
            <tr>
                <td class="text-r" style="width: 85%;">Всего к оплате<?= $currCode ?>:</td>
                <td class="text-r" style="width: 15%;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></b>
                </td>
            </tr>
        </tbody>
    </table>

    <div class="mar-t-10">
        Всего наименований <?= count($model->orders) ?>, на сумму
        <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
    </div>
    <div style="font-weight: bold;">
        <?= TextHelper::mb_ucfirst(Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name)); ?>
    </div>
    <?php if ($model->has_weight) : ?>
        <div>
            Общий вес:
            <?= TextHelper::invoiceMoneyFormat(100 * $model->getTotalWeight(), $model->weightPrecision) ?> кг
        </div>
    <?php endif; ?>
    <?php if ($model->has_volume) : ?>
        <div>
            Общий объем:
            <?= TextHelper::invoiceMoneyFormat(100 * $model->getTotalVolume(), $model->volumePrecision) ?> м2
        </div>
    <?php endif; ?>

    <?php if ($model->comment) : ?>
        <div style="padding-top: 10px;">
            <?= nl2br(Html::encode($model->comment)) ?>
        </div>
    <?php endif; ?>

    <div style="border-bottom: 2px solid #000000; margin: 5px 0;"></div>

    <div style="height: 200px;
                <?php if ($printLink) : ?>
                background-image: url('<?= $printLink ?>');
                background-repeat: no-repeat;
                background-position: 95% bottom;
                background-size: 22.2%;
                background-image-resize: 0;
                background-image-resolution: from-image;
                <?php endif ?>
                -webkit-print-color-adjust: exact;">
        <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
            <tr>
                <td class="font-b v-al-b" style="width: 16%;">
                    Руководитель:
                </td>
                <td style="width: 3%;"></td>
                <td class="text-c v-al-b" style="border-bottom: 1px solid #000000; width: 25%;">
                    <?= ($model->signed_by_employee_id && $model->signEmployeeCompany) ?
                        $model->signEmployeeCompany->position : $model->company->chief_post_name; ?>
                </td>
                <td style="width: 3%;">
                </td>
                <td class="text-c p0ad v-al-b"
                    style="border-bottom: 1px solid #000000; position: relative; width: 25%;">
                    <?php if ($signatureLink) : ?>
                        <img src="<?= $signatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px' ?>">
                    <?php endif; ?>
                </td>
                <td style="width: 3%;"></td>
                <td class="text-c v-al-b"
                    style="border-bottom: 1px solid #000000; width: 25%;">
                    <?= $model->signed_by_name ? $model->signed_by_name : $model->company->getChiefFio(true); ?>
                    <?php if ($model->signed_by_employee_id) : ?>
                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr class="small-txt va-top">
                <td colspan="2" style="width: 19%;"></td>
                <td class="p0ad v-al-t text-c" style="width: 25%;">
                    должность
                </td>
                <td style="width: 3%;"></td>
                <td class="p0ad v-al-t text-c" style="width: 25%;">
                    подпись
                </td>
                <td style="width: 3%;"></td>
                <td class="p0ad v-al-t text-c" style="width: 25%;">
                    расшифровка
                    подписи
                </td>
            </tr>
        </table>

        <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
            <tbody>
                <tr>
                    <td class="font-b v-al-b" style="width: 30%;">
                        Главный (старший) бухгалтер:
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="p0ad text-c v-al-b" style="border-bottom: 1px solid #000000; position: relative; width: 30%;">
                        <?php if ($accountantSignatureLink) : ?>
                            <img src="<?= $accountantSignatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $accountantSignatureHeight).'px' ?>">
                        <?php endif; ?>
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="text-c v-al-b"
                        style="border-bottom: 1px solid #000000; width: 30%;">
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->company->getChiefAccountantFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr class="small-txt">
                    <td style="width: 30%;"></td>
                    <td style="width: 5%;"></td>
                    <td class="text-c p0ad v-al-t" style="width: 30%;">
                        подпись
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="text-c p0ad v-al-t" style="width: 30%;">
                        расшифровка подписи
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
