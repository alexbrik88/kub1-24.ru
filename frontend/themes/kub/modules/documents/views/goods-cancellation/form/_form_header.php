<?php

use common\components\date\DateHelper;
use common\models\document\GoodsCancellation;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model GoodsCancellation */
/* @var $message Message */
/* @var $form ActiveForm */

$invoiceBlock = 'invoice-block';

if (!$model->document_date)
    $model->document_date = date('Y-m-d');
?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span class="<?= $invoiceBlock ?>">
                        <span>
                            <?= $message->get(Message::TITLE_SINGLE); ?>
                        </span>
                    </span>
                    <span>№</span>
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 <?= ($model->type == Documents::IO_TYPE_OUT) ? 'col-xl-2' : 'col-xl-4' ?> <?=($invoiceBlock)?>">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'data-required' => 1,
                        'class' => 'form-control',
                        'autocomplete' => 'off',
                    ]); ?>
                </div>

                <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2 <?=($invoiceBlock)?>">
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'data-required' => 1,
                        'class' => 'form-control',
                        'placeholder' => 'доп. номер',
                    ]); ?>
                </div>
                <?php endif ?>

                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'data-is-inited' => 0,
                            'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>