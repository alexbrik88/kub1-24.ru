<?php

use common\models\document\Invoice;
use common\models\project\Project;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/** @var Invoice $model */
/** @var array[] $projects */
/** @var integer $contractorId */
/** @var integer $projectId */
/** @var integer $actualProjectId */

$actualProjectId = $actualProjectId ?? $projectId;

if (!empty($actualProjectId)) {
    $model->project_id = $actualProjectId;
}

Pjax::begin([
    'id' => 'project-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['/documents/goods-cancellation/field-project']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'invoice-project',
    'model' => $model,
    'attribute' => 'project_id',
    'data' => [0 => 'Без проекта'] + $projects,
    'hideSearch' => true,
    'pluginOptions' => [
        'placeholder' => 'Без проекта',
        'width' => '100%',
        'allowClear' => true,
        'language' => [
            'noResults' =>  new \yii\web\JsExpression('function(){
                return $("<span />").attr("style", "color:red").html("Сначала выберите покупателя");
            }'),
        ],
        'escapeMarkup' => new JsExpression('function(text) {return text;}')
    ],
]);

Pjax::end();