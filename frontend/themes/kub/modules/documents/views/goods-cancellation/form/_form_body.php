<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;
use \frontend\themes\kub\helpers\Icon;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $fixedContractor boolean */
/* @var $message Message */
/* @var $ioType integer */
/* @var $document string */
/* @var $user Employee */
/* @var $hasProjectEstimate integer */
/* @var $projectEstimateId integer */

$user = Yii::$app->user->identity;
/** @var Company $company */
$company = $user->company;
$userConfig = $user->config;
$invoiceBlock = 'invoice-block';
$contractorType = ($ioType == Documents::IO_TYPE_IN) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;
$storeData = ArrayHelper::map($user->getStores()->orderBy(['is_main' => SORT_DESC])->all(), 'id', 'name');
$canEditStore = Yii::$app->getUser()->can(permissions\Company::UPDATE);
$canAddContractor = Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
    'type' => ArrayHelper::getValue($model, 'type'),
]);
// SalePoint
$hasSalePoint = SalePoint::find()->where(['company_id' => $company->id])->exists();
if ($hasSalePoint) {
    $salePoints = SalePoint::getSelect2Data(true);
}

// Industry
$hasCompanyIndustry = CompanyIndustry::find()->where(['company_id' => $company->id])->exists();
if ($hasCompanyIndustry) {
    $companyIndustries = CompanyIndustry::getSelect2Data();
}
?>

<?= yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'side' => ['top'],
    ],
]); ?>

<?php
$projectId = null;
$hasProject = Yii::$app->user->identity->menuItem->project_item;
$hasProjectEstimate = $hasProject;
if ($hasProject) {
    $current_date = date('Y-m-d');
    $projects = Project::find()->joinWith('customers')
        ->select(['project.name', 'project.id'])
        ->andWhere(['project.company_id' => $company->id])
        ->andWhere([
            'or',
            ['project.id' => $model->project_id],
            [
                'and',
                ['project.status' => Project::STATUS_INPROGRESS],
                ['project_customer.customer_id' => $model->contractor_id],
                ['<=', 'project.start_date', $current_date],
                [
                    'or',
                    ['>=', 'project.end_date', $current_date],
                    ['project.end_date' => null]
                ]
            ]
        ])
        ->groupBy('id')
        ->indexBy('id')
        ->column();

}
if ($hasProjectEstimate) {
    if (count($projects)) {
        $estimates = ProjectEstimate::find()
            ->select(ProjectEstimate::tableName() . '.name')
            ->joinWith('project')
            ->andWhere([Project::tableName() . '.id' => ArrayHelper::getColumn($projects, 'id')])
            ->andWhere([Project::tableName() . '.company_id' => $company->id])
            ->andWhere(Project::tableName() . '.start_date <= ' . (new Expression('current_date()')))
            ->andWhere(['or',
                ['>=', 'project.end_date', new Expression('current_date()')],
                ['project.end_date' => null]
            ])
            ->indexBy('id')
            ->column();
    } else {
        $estimates = [];
    }
}
?>

<?= Html::hiddenInput('company-nds_view_type_id', $model->company->nds_view_type_id, ['id' => 'company-nds_view_type_id']) ?>
<?= Html::hiddenInput('document', Documents::SLUG_GOODS_CANCELLATION, ['id' => 'create-document']); ?>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-12" style="margin-bottom: 35px">
            <div class="form-filter">
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'store_id', [
                            'options' => [
                                'class' => '',
                            ],
                        ])->widget(Select2::class, [
                            'data' => $storeData,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '100%'
                            ],
                            'options' => [
                                'class' => 'form-control',
                            ]
                        ])->label('Со склада' . '<span class="important">*</span>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group col-12" style="margin-bottom: 21px">
            <div class="form-filter">
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'contractor_id')->label('Покупатель')->widget(ContractorDropdown::class, [
                            'company' => $company,
                            'contractorType' => Contractor::TYPE_CUSTOMER,
                            'staticData' => ['add-modal-contractor' => Icon::get('add-icon', ['class' => 'link']) . ' Добавить покупателя'],
                            'options' => [
                                'id' => 'customer_contractor_id',
                                'placeholder' => '',
                                'data' => [
                                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                                    'createurl' => Url::to(['/documents/invoice/add-modal-contractor'])
                                ],
                            ],
                        ])->label('Под покупателя' . '<span class="important">*</span>'); ?>
                    </div>
                    <?php if ($hasProject): ?>
                        <div class="<?= $hasProjectEstimate ? 'col-3' : 'col-6' ?>">
                            <div class="form-filter">
                                <label class="label" for="cause" style="margin-bottom: 4px">Проект
                                    <span class="tooltip2" data-tooltip-content='#project-info'>
                                        <?= Icon::QUESTION; ?>
                                    </span>
                                </label>
                            </div>
                            <?= $this->render('_project_selecting', [
                                'model' => $model,
                                'projects' => $projects,
                                'projectId' => $projectId,
                                'contractorId' => (bool)$model->contractor_id,
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($hasProjectEstimate): ?>
                        <div class="col-3">
                            <div class="form-filter">
                                <label class="label" for="cause">Смета</label>
                            </div>
                            <?= $this->render('_project_estimate_selecting', [
                                'model' => $model,
                                'estimates' => $estimates,
                                'projectEstimateId' => $projectEstimateId,
                                'contractorId' => (bool)$model->contractor_id,
                            ]) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-group col-12" style="margin-bottom: 35px">
            <div class="form-filter">
                <div class="row">
                    <div class="col-3">
                        <?php
                        echo $form->field($model, 'profit_loss_type', [
                            'options' => [
                                'class' => '',
                            ]
                        ])->widget(Select2::class, [
                            'hideSearch' => true,
                            'data' => $model::$profitLossTypeItemList,
                            'options' => [
                                'prompt' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ])->label('Раздел ОПиУ' . '<span class="important">*</span>');
                        ?>
                    </div>
                    <div class="col-3">
                        <?php
                        echo $form->field($model, 'expenditure_item_id', [
                            'options' => [
                                'class' => '',
                            ]
                        ])->widget(Select2::class, [
                            'hideSearch' => true,
                            'data' => $model::$expenditureItemList,
                            'options' => [
                                'prompt' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ])->label('Статья расходов' . '<span class="important">*</span>');
                        ?>
                    </div>
                    <?php if ($hasCompanyIndustry): ?>
                        <div class="col-3">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Направление</label>
                            </div>
                            <?= Select2::widget([
                                'hideSearch' => count($companyIndustries) === 1,
                                'model' => $model,
                                'attribute' => 'industry_id',
                                'data' => $companyIndustries,
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'placeholder' => 'Без направления',
                                    'width' => '100%',
                                    'allowClear' => true,
                                ]
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($hasSalePoint): ?>
                        <div class="col-3">
                            <div class="form-filter hight-line">
                                <label class="label" for="cause">Точка продаж</label>
                            </div>
                            <?= Select2::widget([
                                'model' => $model,
                                'attribute' => 'sale_point_id',
                                'data' => $salePoints,
                                'hideSearch' => count($salePoints) === 1,
                                'options' => [
                                    'class' => 'form-control'
                                ],
                                'pluginOptions' => [
                                    'placeholder' => 'Без точки продаж',
                                    'width' => '100%',
                                    'allowClear' => true,
                                ]
                            ]) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-group col-12">
            <div class="form-filter">
                <?php
                echo $form->field($model, 'comment', [
                    'options' => [
                        'class' => '',
                    ]
                ])->textarea();
                ?>
            </div>
        </div>

    </div>
</div>


<div style="display:none">
    <div id="project-info">
        Сначала нужно выбрать Покупателя, тогда в списке отобразятся<br/>
        проекты, связанные с данным покупателем.

    </div>
    <div id="contractor-empty">
        Укажите покупателя, чтобы добавить основание
    </div>
    <div id="tooltip_add_product" class="text-center">
        В наименовании нажмите «Добавить новый товар/услугу»,<br>
        после заполните информацию по вашему товару или услуге.<br>
        Данные сохранятся, в дальнейшем вы<br>
        будите выбирать товар или услугу  из списка.
    </div>
</div>

<?php

$this->registerJs('

    $(document).on("keyup", "input.select2-search__field", function (e) {
        $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
            .attr("aria-selected", "false")
            .addClass("selected");
    });

    $(document).on("shown.bs.modal", ".modal#ajax-modal-box", function () {
        $("#invoice-company_rs").select2("close");
    });

    $(document).on("click", "#contractor_update_button", function() {
            var url  = "add-modal-contractor";
            var form = {
                documentType: INVOICE.documentIoType,
                contractorId: $(this).data("contractor-id") || 0,
                invoiceId: $(this).data("invoice-id") || 0,
        };
        INVOICE.addNewContractor("add-modal-contractor", form);
    });

    $("#config-invoice_form_article").on("change", function(e) {
        var input = this;
        var attr = input.id.replace("config-", "");
        $.post("/site/config", {"Config[invoice_form_article]": $("#config-invoice_form_article").prop("checked") ? 1 : 0}, function(data) {
            if (typeof data[attr] !== "undefined") {
                if (data[attr]) {
                    $("." + $(input).data("target")).removeClass("hidden");
                } else {
                    $("." + $(input).data("target")).addClass("hidden");
                }
            }
        })
    });

    $(document).on("shown.bs.modal", "#add-new", function() {
        refreshUniform();
    });

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });
');

