<?php

use common\models\document\Invoice;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/** @var Invoice $model */
/** @var ProjectEstimate[] $estimates */
/** @var integer $projectEstimateId */
/** @var integer $actualEstimateId */

$actualEstimateId = $actualEstimateId ?? $projectEstimateId;

Pjax::begin([
    'id' => 'project-estimate-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'scrollTo' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['/documents/goods-cancellation/field-project-estimate']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'invoice-project-estimate',
    'model' => $model,
    'attribute' => 'project_estimate_id',
    'data' => [0 => 'Без сметы'] + $estimates,
    'hideSearch' => true,
    'pluginOptions' => [
        'placeholder' => 'Без сметы',
        'width' => '100%',
        'allowClear' => true,
        'language' => [
            'noResults' =>  new \yii\web\JsExpression('function(){
                return $("<span />").attr("style", "color:red").html("Сначала выберите покупателя");
            }'),
        ],
        'escapeMarkup' => new JsExpression('function(text) {return text;}')
    ],
    'options' => [
        'value' => $actualEstimateId ?: null,
    ]
]);

Pjax::end();