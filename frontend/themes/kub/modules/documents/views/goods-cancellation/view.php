<?php

use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\GoodsCancellation */
/* @var $message Message */
/* @var $useContractor string */
/* @var $hasEstimate string */
/* @var $estimateId string */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = 'Списание товара' . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-document';

$contractorId = $useContractor ? $model->contractor_id : null;

$canUpdate = Yii::$app->user->can(permissions\Product::WRITE_OFF);
$canDelete = Yii::$app->user->can(permissions\Product::WRITE_OFF);
$canCreate = Yii::$app->user->can(permissions\Product::WRITE_OFF);
$canUpdateStatus = Yii::$app->user->can(permissions\Product::WRITE_OFF);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, ['class' => 'link mb-2']); ?>
<?php endif; ?>

<div class="wrap wrap_padding_small">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if ($canUpdate) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->contractor_id : null),
                    ], [
                        'title' => 'Редактировать',
                        'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container doc-invoice doc-preview">
                    <?= $this->render('template', [
                        'model' => $model,
                        'addStamp' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('view/_status_block', [
                'model' => $model,
                'useContractor' => $useContractor,
            ]); ?>

            <?= $this->render('view/_main_info', [
                'model' => $model,
                'dateFormatted' => $dateFormatted,
                'useContractor' => $useContractor,
                'canUpdate' => $canUpdate,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('view/_action_buttons', [
    'model' => $model,
    'useContractor' => $useContractor,
    'canCreate' => $canCreate,
    'canUpdate' => $canUpdate,
    'canUpdateStatus' => $canUpdateStatus,
    'canDelete' => $canDelete
]); ?>

<?= $this->render('/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>

<?php

$this->registerJs(<<<JS
$('.open-send-to').on('click', function() {
    $('.dropdown-email').addClass('open-users');
});

JS
);
