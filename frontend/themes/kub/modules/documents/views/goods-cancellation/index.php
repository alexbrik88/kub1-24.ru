<?php

use common\components\date\DateHelper;
use common\models\document\GoodsCancellation;
use frontend\widgets\TableConfigWidget;
use kartik\select2\Select2;
use \yii\bootstrap4\Modal;
use frontend\components\StatisticPeriod;
use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\TableViewWidget;
use common\models\employee\Employee;
use common\components\helpers\ArrayHelper;
use frontend\modules\documents\widgets\SummarySelectWidget;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\GoodsCancellationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */

$this->title = 'Списание товара';
$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');
$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = GoodsCancellation::find()->where(['company_id' => $company->id])->exists();

if ($exists) {
    $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
} else {
    $emptyMessage = 'Вы еще не создали ни одного списания.';
}

$canIndex = Yii::$app->getUser()->can(permissions\Product::WRITE_OFF);
$canCreate = Yii::$app->getUser()->can(permissions\Product::WRITE_OFF);
$canDelete = Yii::$app->getUser()->can(permissions\Product::WRITE_OFF);
$canSend = Yii::$app->getUser()->can(permissions\Product::WRITE_OFF);
$canPrint = Yii::$app->getUser()->can(permissions\Product::WRITE_OFF);
?>

<div class="stop-zone-for-fixed-elems page-goods-cancellation">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if ($canCreate): ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>',
                ['/documents/goods-cancellation/create'],
                ['class' => 'button-regular button-regular_red button-width ml-auto'
            ]) ?>
        <?php endif; ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3"></div>
            <div class="col-6 col-xl-3"></div>
            <div class="col-6 col-xl-3"></div>
            <div class="col-6 col-xl-3"><?= frontend\widgets\RangeButtonWidget::widget(); ?></div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'goods_cancellation_contractor'],
                    ['attribute' => 'goods_cancellation_project'],
                    ['attribute' => 'goods_cancellation_estimate'],
                    ['attribute' => 'goods_cancellation_pal_type'],
                    ['attribute' => 'goods_cancellation_responsible'],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер документа, ИНН или название покупателя',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list ' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '1%',
                ],
                'contentOptions' => [
                ],
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return Html::checkbox('GoodsCancellation[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-model_id' => $model->id,
                        'data-sum' => $model->totalAmountWithNds
                    ]);

                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата документа',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => 'Номер документа',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return Html::a($model->fullNumber, ['view', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'view_total_with_nds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return \common\components\TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
                },
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'class' => 'col_goods_cancellation_contractor' . ($userConfig->goods_cancellation_contractor ? '' : ' hidden'),
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'col_goods_cancellation_contractor' . ($userConfig->goods_cancellation_contractor ? '' : ' hidden'),
                    'style' => 'max-width:300px'
                ],
                'filter' => $searchModel->getContractorsArray(),
                'hideSearch' => false,
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    if ($model->contractor) {
                        return '<span title="' . htmlspecialchars($model->contractor->shortName) . '">' . $model->contractor->shortName . '</span>';
                    }

                    return '---';
                },
                's2width' => '300px'
            ],
            [
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => 'col_goods_cancellation_project' . ($userConfig->goods_cancellation_project ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_goods_cancellation_project' . ($userConfig->goods_cancellation_project ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getProjectsArray(),
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return ($model->project)
                        ? $model->project->getFullName(false)
                        : null;
                },
                's2width' => '200px'
            ],
            [
                'attribute' => 'project_estimate_id',
                'label' => 'Cмета',
                'headerOptions' => [
                    'class' => 'col_goods_cancellation_estimate' . ($userConfig->goods_cancellation_estimate ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_goods_cancellation_estimate' . ($userConfig->goods_cancellation_estimate ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getProjectsEstimatesArray(),
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return ($model->projectEstimate)
                        ? $model->projectEstimate->getFullName(false)
                        : null;
                },
                's2width' => '200px'
            ],
            [
                'attribute' => 'profit_loss_type',
                'label' => 'Раздел ОПиУ',
                'headerOptions' => [
                    'class' => 'col_goods_cancellation_pal_type' . ($userConfig->goods_cancellation_pal_type ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_goods_cancellation_pal_type' . ($userConfig->goods_cancellation_pal_type ? '' : ' hidden'),
                ],
                'filter' => $searchModel->getProfitAndLossTypeArray(),
                'format' => 'raw',
                'value' => function (GoodsCancellation $model) {
                    return ArrayHelper::getValue(GoodsCancellation::$profitLossTypeItemList, $model->profit_loss_type);
                },
                's2width' => '200px'
            ],
            [
                'headerOptions' => [
                    'class' => 'col_goods_cancellation_responsible' . ($userConfig->goods_cancellation_responsible ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_goods_cancellation_responsible' . ($userConfig->goods_cancellation_responsible ? '' : ' hidden'),
                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                ],
                'attribute' => 'responsible_employee_id',
                'label' => 'Ответственный',
                'encodeLabel' => false,
                'value' => function ($model) {
                    $employee = Employee::findOne([
                        'id' => $model['responsible_employee_id']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getEmployersArray(),
                's2width' => '200px'
            ],
        ],
    ]); ?>
</div>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canCreate ? Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', '#modal-clone-from-index', [
            'class' => 'button-clr button-regular button-width button-hover-transparent button-clone-from-index',
            'data-toggle' => 'modal',
            'data-url' => Url::to(['create']),
            'data-clone_id' => '' // fill by js
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php Modal::begin([
    'id' => 'many-delete',
    'closeButton' => false,
]); ?>

<?= \yii\bootstrap4\Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
    'class' => 'modal-close close',
    'data-dismiss' => 'modal',
    'aria-label' => 'Close',
]) ?>

<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите удалить выбранные списания?
</h4>
<div class="text-center">
    <?= Html::button('ДА', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
        'data-url' => Url::to(['many-delete', 'type' => $ioType]),
    ]); ?>
    <?= Html::button('НЕТ', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<?php Modal::begin(['id' => 'modal-clone-from-index', 'closeButton' => false]); ?>
<?= \yii\bootstrap4\Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
    'class' => 'modal-close close',
    'data-dismiss' => 'modal',
    'aria-label' => 'Close',
]) ?>
<h4 class="modal-title text-center mb-4">
    Вы уверены, что хотите скопировать это списание?
</h4>
<div class="text-center">
    <?= Html::button('ДА', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-clone-from-index-yes',
    ]); ?>
    <?= Html::button('НЕТ', [
        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
        'data-dismiss' => 'modal',
    ]); ?>
</div>
<?php Modal::end(); ?>

<script>
    $('.joint-operation-checkbox').on('change', function() {
        const selectedCheckboxes = $(this).closest('table').find('.joint-operation-checkbox:checked');
        const toggleButton = $('.actions-many-items').find('.button-clone-from-index');
        if (selectedCheckboxes.length === 1) {
            const modelId = $(selectedCheckboxes).first().data('model_id');
            toggleButton.data('clone_id', modelId);
            toggleButton.show();
        } else {
            toggleButton.data('clone_id', '');
            toggleButton.hide();
        }
    });

    $('.modal-clone-from-index-yes').on('click', function() {
        const toggleButton = $('.actions-many-items').find('.button-clone-from-index');
        const cloneUrl = toggleButton.data('url');
        const cloneId = toggleButton.data('clone_id');

        if (cloneId) {
            location.href = cloneUrl + '?clone=' + cloneId;
        } else {
            console.log('cloneId undefined');
        }
    });
</script>

<div style="display: none">
    <?php /* preload styles */ ?>
    <?= Select2::widget([
        'name' => 'empty',
        'data' => []
    ]); ?>
</div>