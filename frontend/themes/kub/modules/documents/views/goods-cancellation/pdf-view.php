<?php

/* @var $this yii\web\View */
/* @var $model common\models\document\GoodsCancellation */
/* @var $orders common\models\document\OrderGoodsCancellation[] */
/* @var $addStamp boolean */
/* @var $multiple [] */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document';
?>

<div style="
    width: 675px;
    margin: 0 auto;
    padding: 20px 60px;
    background: #fff;
    box-sizing: content-box;
    overflow: hidden;
    zoom: 1;
    ">
    <?= $this->render('template', [
        'model' => $model,
        'addStamp' => $addStamp,
        'orders' => $model->orders,
    ]); ?>
</div>

<?php /* if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
<pagebreak/>
<?php endif; */ ?>
