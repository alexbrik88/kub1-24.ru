<?php

use common\models\Company;
use common\models\Contractor;
use common\models\document\GoodsCancellation;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/** @var View $this */
/** @var GoodsCancellation $model */
/** @var Company $company */
/** @var boolean $fixedContractor */
/** @var Message $message */
/** @var integer $ioType */
/** @var ActiveForm $form */
/** @var integer $projectId */
/** @var integer $projectEstimateId */
/** @var integer $hasProjectEstimate */

$this->title = ($model->isNewRecord ? 'Создать ' : 'Изменить ') . mb_strtolower($message->get(Message::TITLE_SINGLE));
$this->context->layoutWrapperCssClass = 'create-out-document';

$projectEstimateId = empty($projectEstimateId) ? $model->project_estimate_id : $projectEstimateId;
$hasProjectEstimate = $projectEstimateId > 0;

$errorModelArray = array_merge([$model], $model->orders);
?>

<!-- remove product -->
<div id="modal-remove-one-product" class="confirm-modal modal fade" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4">
                    Вы уверены, что хотите удалить эту позицию?
                </h4>
                <div class="text-center">
                    <?= Html::button('ДА', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 yes',
                        'data-dismiss' => 'modal',
                    ]) ?>
                    <?= Html::button('НЕТ', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-2',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php // KUB
// form
/** for itself */
$formDefaultConfig = [
    'layout' => 'default',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => '',
        ],
        'hintOptions' => [
            'tag' => 'small',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => '',
            'hint' => '',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
]; ?>

<div class="form">
    <?php $form = ActiveForm::begin(array_merge($formDefaultConfig, [
        'id' => 'goods-cancellation-form',
        'options' => [
            'enctype' => 'multipart/form-data',
            'data' => [
                'iotype' => $ioType,
            ]
        ],
        'enableClientValidation' => false,
    ])); ?>

    <?= $form->errorSummary($errorModelArray); ?>

    <?= Html::hiddenInput('returnUrl', $returnUrl) ?>

    <?= $this->render('form/_form_header', [
        'model' => $model,
        'message' => $message,
        'form' => $form,
        'ioType' => $ioType,
    ]); ?>

    <?= $this->render('form/_form_body', [
        'model' => $model,
        'message' => $message,
        'fixedContractor' => $fixedContractor,
        'ioType' => $ioType,
        'company' => $company,
        'form' => $form,
        'hasProjectEstimate' => $hasProjectEstimate,
        'projectEstimateId' => $projectEstimateId,
    ]); ?>

    <?= $this->render('form/_product_table', [
        'model' => $model,
        'ioType' => $ioType,
        'company' => $company,
        'projectEstimateId' => $projectEstimateId,
    ]); ?>

    <?= $this->render('form/_form_buttons', [
        'model' => $model,
        'ioType' => $ioType,
        'fixedContractor' => $fixedContractor,
    ]); ?>
    <?php ActiveForm::end(); ?>
</div>


<?= $this->render('form/_form_product_new'); ?>

<?= $this->render('form/_form_product_existed', [
    'model' => $model,
]); ?>

<?php

$this->registerJs('


$(document).on("change", "select#goodscancellation-project_id", function() {

    var pjaxProjectEstimate = $("#project-estimate-pjax-container");
    var contractorId = $("#goodscancellation-contractor_id").val();
    var documentType = '. (int)$model->type .'
    
    if (pjaxProjectEstimate.length) {
        var projectId = $("#goodscancellation-project_id").val();
        $.pjax({
            url: pjaxProjectEstimate.data("url"),
            data: {"contractorId": contractorId, "projectId": projectId},
            container: "#project-estimate-pjax-container",
            push: false,
            timeout: 10000
        });
    }
}); 

$(document).on("change", "select#customer_contractor_id", function() {
    var form = this.form;
    var pjax = $("#agreement-pjax-container", form);
    var pjaxProject = $("#project-pjax-container", form);
    var pjaxProjectEstimate = $("#project-estimate-pjax-container", form);    
    
    var contractorId = $(this).val();
    var projectId = ' . (isset($projectId) ? (int)$projectId : 0) .';
    var documentType = '. (int)$model->type .'

    if (this.value != "add-modal-contractor") {

        if (pjaxProject.length) {
            $.pjax({
                url: pjaxProject.data("url"),
                data: {"contractorId": contractorId, "projectId": projectId},
                container: "#project-pjax-container",
                push: false,
                scrollTo: false,
                timeout: 10000
            }).done(function() {
                if (pjaxProjectEstimate.length) {
                    var projectId = $("#goodscancellation-project_id").val();
                    $.pjax({
                        url: pjaxProjectEstimate.data("url"),
                        data: {"contractorId": contractorId, "projectId": projectId},
                        container: "#project-estimate-pjax-container",
                        push: false,
                        scrollTo: false,
                        timeout: 10000
                    });
                }
            });
        }

    }

});

');

?>
<?php
$this->registerJs(<<<JS

    $(document).on('blur', '.product-count', function(){
        if ($(this).val() == 0 || $(this).val() == ''){
            $(this).val($(this).attr('data-value'));
            $(this).closest('tr').find('.price-with-nds').text(parseFloat($(this).attr('data-value') * $(this).closest('tr').find('.price-one-with-nds-input').val()).toFixed(2));
        } else {
           $(this).attr('data-value', $(this).val());
        }
    });

    $(document).on('blur','.product-title', function(){
        var name = $(this).val().replace(/\s+/g, '').length;
        if (name == 0) {
            $(this).closest('tr').remove();
            $('#from-new-add-row').show();
            INVOICE.recalculateInvoiceTable();
        }

    });
    $(document).on('click', '.product-title-clear', function(){
        $(this).closest('td').find('.product-title').val('');
        $(this).closest('td').find('.product-title').focus();
    });

    // REFRESH_UNIFORMS
    $(document).on("click", ".refreshDatepicker", function () {
        refreshDatepicker();
    });

    $(document).on("pjax:complete", "#pjax-product-grid", function() {
        refreshUniform();
    });
JS
    , $this::POS_READY);

if ($hasProjectEstimate) {
    $this->registerJs(<<<JS
        $.post('/project/get-project-estimate-order', {estimate: $projectEstimateId, type: $ioType}, function (data) {
            let row = $('.product-row');
            if ('true' === data.success) {
                $('#from-new-add-row').hide();
                $('#add-one-more-position').hide();
                row.remove();
                INVOICE.addProductToTable(data.data);
                row.show();
            }
        }, 'json');
JS, View::POS_LOAD);
}