<?php

use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $model common\models\document\GoodsCancellation */
/* @var $useContractor boolean */

$contractorId = $useContractor ? $model->contractor_id : null;
$hasSignature = $model->employeeSignature || $model->getImage('chiefSignatureImage') || $model->getImage('chiefAccountantSignatureImage');
$hasStamp = $model->getImage('printImage');

$loadAsPdfButtons = [[
    'label' => '<span style="display: inline-block;">PDF</span> файл',
    'encode' => false,
    'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'addStamp' => 0, 'filename' => $model->getPdfFileName()],
    'linkOptions' => ['target' => '_blank']
]];

if ($hasSignature || $hasStamp) {
    $loadAsPdfButtons[] = [
        'label' => '<span style="display: inline-block;">PDF</span> файл с ' . ($hasSignature ? 'подписью' : 'печатью'),
        'encode' => false,
        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'addStamp' => 1, 'filename' => $model->getPdfFileName()],
        'linkOptions' => ['target' => '_blank']
    ];
}
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdateStatus): ?>
                <?=Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent open-send-to',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php $printUrl = [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'filename' => $model->getPrintTitle(),
            ]; ?>
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', $printUrl, [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-width button-hover-transparent',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= yii\bootstrap4\Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => $loadAsPdfButtons,
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canCreate): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'copied']).'<span>Копировать</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent',
                    ],
                    'confirmUrl' => Url::to([
                        'create',
                        'clone' => $model->id,
                    ]),
                    'message' => 'Вы уверены, что хотите скопировать это списание?',
                ]);
                ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1 text-right">
            <?php if ($canDelete && !$model->getHasRelated()): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'id' => $model->id,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить это списание?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>