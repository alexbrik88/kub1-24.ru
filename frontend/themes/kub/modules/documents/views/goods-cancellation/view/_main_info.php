<?php

use common\models\currency\Currency;
use common\models\document\GoodsCancellation;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model GoodsCancellation */
/* @var $dateFormatted string */
/* @var $useContractor string */
/* @var $company common\models\Company */
/* @var $user common\models\employee\Employee */

$user = Yii::$app->user->identity;
$company = $user->company;
if ($hasProject = $model->project_id) {
    /** @var Project $project */
    $project = Project::find()->andWhere(['and',
        ['id' => $model->project_id],
        ['company_id' => $model->company_id],
    ])->one();
}
if ($hasProjectEstimate = $model->project_estimate_id) {
    /** @var ProjectEstimate $projectEstimate */
    $projectEstimate = ProjectEstimate::find()
        ->joinWith('project')
        ->andWhere([Project::tableName() . '.id' => $model->project_id])
        ->andWhere([Project::tableName() . '.company_id' => $model->company_id])
        ->one();
}
?>

<div class="about-card" style="margin-bottom: 12px">
    <?php if ($model->contractor): ?>
    <div class="about-card-item">
        <span class="text-grey">
            Покупатель:
        </span>
        <?= Html::a($model->contractor->shortName, [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <?php endif; ?>
    <?php if ($model->industry) : ?>
        <div class="about-card-item">
            <span class="text-grey">Направление:</span>
            <span>
                <?= $model->industry->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($model->salePoint) : ?>
        <div class="about-card-item">
            <span class="text-grey">Точка продаж:</span>
            <span>
                <?= $model->salePoint->name ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($hasProject && $project) : ?>
        <div class="about-card-item">
            <span class="text-grey">Проект:</span>
            <span>
                <?= Html::a($project->getFullName(), ['/project/view', 'id' => $project->id], ['class' => 'link']); ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($hasProjectEstimate && $projectEstimate) : ?>
        <div class="about-card-item">
            <span class="text-grey">Смета:</span>
            <span>
                <?= Html::a($projectEstimate->getFullName(), ['/project/estimate', 'type' => 'view', 'id' => $projectEstimate->id], ['class' => 'link']); ?>
            </span>
        </div>
    <?php endif; ?>
    <?php if ($model->profit_loss_type != GoodsCancellation::UNSET_EXPENSE_TYPE) : ?>
        <div class="about-card-item">
            <span class="text-grey">Раздел ОПиУ:</span>
            <span>
                <?= ArrayHelper::getValue(GoodsCancellation::$profitLossTypeItemList, $model->profit_loss_type) ?>
            </span>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">Статья расходов:</span>
        <span>
            <?= $model->expenditureItem !== null ? $model->expenditureItem->fullName : 'не указано'; ?>
        </span>
    </div>
    <?php if ($model->responsible) : ?>
        <div class="about-card-item">
            <span class="text-grey">Ответственный:</span>
            <span>
                <?= Html::encode($model->responsible->getFio(true)) ?>
            </span>
        </div>
    <?php endif ?>
    <?php if ($model->currency_name != Currency::DEFAULT_NAME) : ?>
        <div id="invoice_currency_rate_box" class="about-card-item">
            <?= $this->render('_currency_info', ['model' => $model]) ?>
        </div>
    <?php endif ?>
    <?php /*<div class="about-card-item">
        <?= \frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>*/ ?>
</div>
<div class="about-card" style="margin-bottom: 12px">
    <div class="about-card-item">
        <span style="font-weight: bold;">Комментарий</span>
        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
            'id' => 'comment_internal_update',
            'style' => 'cursor: pointer;',
        ]); ?>
        <div id="comment_internal_view" style="margin-top:5px" class="">
            <?= Html::encode($model->comment) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
                'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_internal_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: -15px; right: 0px; cursor: pointer; font-size: 16px;',
            ]); ?>
            <?= Html::textarea('comment', $model->comment, [
                'id' => 'comment_internal_input',
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd; margin-top: 4px;',
            ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
            $(this).toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
                $("#comment_internal_update").toggleClass("hidden");
            })
        });
    ');
}
?>
