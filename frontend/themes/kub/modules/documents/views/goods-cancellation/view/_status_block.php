<?php

use common\components\TextHelper;
use common\models\document\GoodsCancellation;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\BoolleanSwitchWidget;
use common\models\employee\EmployeeRole;
use common\components\date\DateHelper;
use common\models\document\GoodsCancellationStatus;

/* @var $this yii\web\View */
/* @var $model GoodsCancellation */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $user common\models\employee\Employee */

$status = $model->status;
$user = Yii::$app->user->identity;

$createdInvoicesCount = $model->company->getInvoices()->where(['from_demo_out_invoice' => 0])->count();

$statusIcon = GoodsCancellationStatus::$statusIcon;
$statusBackground = GoodsCancellationStatus::$statusStyleClass;
$icon = ArrayHelper::getValue($statusIcon, $status->id);
$background = ArrayHelper::getValue($statusBackground, $status->id);
$color = '#ffffff';
$border = $background;
?>

<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
            background-color: <?=$background?>;
            border-color: <?=$border?>;
            color: <?=$color?>;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span><?= $status->name; ?></span>
        </div>
    </div>
</div>