<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\Waybill;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\widgets\SummarySelectWidget;
use frontend\rbac\permissions;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\WaybillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */

$this->title = $message->get(Message::TITLE_PLURAL);

/** @var \common\models\employee\Employee $user */
$user = Yii::$app->user->identity;
$period = StatisticPeriod::getSessionName();
$company = $user->company;
$exists = Invoice::find()->joinWith('waybills', false, 'INNER JOIN')
    ->byCompany($user->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);
$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_waybill' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет товарно-транспортных накладных. Измените период, чтобы увидеть имеющиеся накладные.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одной товарно-транспортной накладной.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;

$tabViewClass = $user->config->getTableViewClass('table_view_document');
?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>
        <?php Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как подготовить товарно-транспортную накладную, нужно создать
            Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php Modal::end(); ?>

        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как подготовить товарно-транспортную накладную,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>.
        </div>
    <?php endif; ?>

    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if ($company->createInvoiceAllowed($ioType)) : ?>
            <?php if ($existsInvoicesToAdd) : ?>
                <button class="button-regular button-regular_red button-width ml-auto add-document">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
            <?php else : ?>
                <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
                    '/documents/invoice/create',
                    'type' => $ioType,
                    'document' => Documents::SLUG_WAYBILL,
                ]) ?>">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </a>
            <?php endif; ?>
        <?php else : ?>
            <button class="button-regular button-regular_red button-width ml-auto action-is-limited">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </button>
        <?php endif ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-9"></div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'byNumber', [
                    'type' => 'search',
                    'placeholder' => 'Номер накладной или название контрагента',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (Waybill $model) {
                    return Html::checkbox('Waybill[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата ТТН',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ ТТН',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $data,
                    ])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber;
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'invoice.total_amount_with_nds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    $price = TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],

            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, Waybill::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],
            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    return $data->status->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],
            [
                'attribute' => 'invoice_document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'format' => 'raw',
                'value' => function (Waybill $data) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])
                    ) {
                        if ($data->invoice->file !== null) {
                            $invoiceFileLinkClass = null;
                            $tooltipId = null;
                            $contentPreview = null;
                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                if ($thumb) {
                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                    $contentPreview .= Html::endTag('span');
                                    $contentPreview .= Html::endTag('div');
                                }
                            }

                            return Html::a($data->invoice->fullNumber,
                                    ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id])
                                . Html::a('<span class="pull-right icon icon-paper-clip"></span>',
                                    ['/documents/invoice/file-get', 'type' => $data->type, 'id' => $data->invoice->id, 'file-id' => $data->invoice->file->id,],
                                    [
                                        'class' => $invoiceFileLinkClass,
                                        'target' => '_blank',
                                        'data-tooltip-content' => '#' . $tooltipId,
                                        'style' => 'float: right',
                                    ]) . $contentPreview;
                        } else {
                            return Html::a($data->invoice->fullNumber,
                                ['/documents/invoice/view', 'type' => $data->type, 'id' => $data->invoice->id]);
                        }
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],
        ],
    ]) ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_WAYBILL,
    'documentTypeName' => 'накладную',
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_WAYBILL
    ]); ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные накладные?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>

<?php Modal::begin([
    'id' => 'many-send-error',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <h4 class="modal-title text-center mb-4">
        Ошибка при отправке накладных
    </h4>
    <div class="modal-body">
        <div class="form-body">
        </div>
    </div>
    <div class="text-center">
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Ок
        </button>
    </div>
<?php Modal::end(); ?>
