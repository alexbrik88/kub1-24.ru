<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\company\CompanyType;
use common\models\document\OrderWaybill;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\Waybill;
use common\models\product\ProductUnit;

/* @var $this yii\web\View */
/* @var $model common\models\document\Waybill */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$hasMass = $realMassGross && $realMassNet;
?>

<style>
    .ttn .table {
        margin: 0;
        width: 100%;
        font-family: Arial, sans-serif;
    }

    .ttn {
        padding-top: 1px
    }

    .ttn td {
        font-size: 7pt;
        padding: 1px;
        line-height: 8pt;
    }

    .ttn td.fs6 {
        font-size: 6pt;
    }

    .ttn td.fs7 {
        font-size: 7pt;
    }

    .ttn td.fs8 {
        font-size: 8pt;
    }

    .ttn td.fs8-b {
        font-size: 8pt;
        font-weight: bold;
    }

    .ttn td.m-l {
        padding: 0;
        margin: 0;
    }

    .ttn .border-2 {
        border: 2px solid #000
    }

    .ttn .bt {
        border-top: 1px solid #000;
    }

    .ttn .bl {
        border-left: 1px solid #000;
    }

    .ttn .br {
        border-right: 1px solid #000;
    }

    .ttn .bb {
        border-bottom: 1px solid #000;
    }

    .ttn .bt2 {
        border-top: 2px solid #000;
    }

    .ttn .bl2 {
        border-left: 2px solid #000;
    }

    .ttn .br2 {
        border-right: 2px solid #000;
    }

    .ttn .bb2 {
        border-bottom: 2px solid #000;
    }

    .ttn .bt2 {
        border-top: 2px solid #000;
    }

    .ttn .bl3 {
        border-left: 3px solid #000;
    }

    .ttn .br3 {
        border-right: 3px solid #000;
    }

    .ttn .bb3 {
        border-bottom: 3px solid #000;
    }

    .ttn .bt3 {
        border-top: 3px solid #000;
    }

    .ttn .tc {
        text-align: center;
    }

    .ttn .va-t {
        vertical-align: top;
    }

    .ttn .va-b {
        vertical-align: bottom;
    }

    .ttn .tr {
        text-align: right;
    }

    .ttn .tip {
        font-size: 6pt;
        margin: 0;
        padding: 1px 0;
        line-height: 6pt;
        text-align: center;
    }

    @media print {
        .ttn.page-break {
            page-break-after: always;
        }
    }

</style>

<div class="page-content-in p-center-album pad-pdf-p-album ttn page-break">
    <div class="text-right font-size-6 line-h-small">
        <p>Типовая межотраслевая форма № 1-Т</p>
        <p>Утверждена постановлением Госкомстата России от 28.11.97 № 78</p>
    </div>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td colspan="5">
            </td>
            <td width="8%" class="tc"
                style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">
                Коды
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td colspan="3" class="font-size-8 text-right" style="padding: 1px 4px;">Форма по ОКУД</td>
            <td class="font-size-8-bold tc"
                style="border-left: 2px solid black; border-right: 2px solid black;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">
                0345009
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-8-bold text-right" style="width:44%">ТОВАРНО-ТРАНСПОРТНАЯ НАКЛАДНАЯ</td>
            <td class="bb" style="padding-left:8pt">ТТН</td>
            <td class="font-size-8-bold text-right ver-bottom" colspan="2"
                style="padding: 1px 4px; border-bottom: 1px solid #ffffff">№
            </td>
            <td class="font-size-8-bold tc"
                style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= $model->getFullNumber(); ?>
            </td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td></td>
            <td colspan="3">
                <table class="no-border m-l" style="width:100%">
                    <tr>
                        <td class="fs6 tc ver-top m-l" width="75%">серия</td>
                        <td class="font-size-8 text-right m-l" style="padding: 1px 4px;" width="24%">Дата составления
                        </td>
                    </tr>
                </table>
            </td>
            <td style="border-right: 2px solid black;border-left: 2px solid black; border-bottom: 1px solid black; padding:0;margin:0;">
                <table class="table no-border" style="height: 16pt;width: 100%;">
                    <tr>
                        <td class="tc"
                            style="width: 30%;border-right: 1px solid #000;"><?= date('d', strtotime($model->document_date)) ?></td>
                        <td class="tc"
                            style="width: 30%;border-right: 1px solid #000;"><?= date('m', strtotime($model->document_date)) ?></td>
                        <td class="tc"><?= date('Y', strtotime($model->document_date)) ?></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td width="12%" class="tr ver-bottom m-l fs8">Грузоотправитель</td>
            <td class="bb va-b" colspan="2" style="padding-left:8pt">
                <?php if ($model->consignor) {
                    echo $model->consignor->getTitle(true);
                    echo ($model->consignor->legal_address) ? ", {$model->consignor->legal_address}" : "";
                    echo ($model->consignor->getRealContactPhone()) ? ", {$model->consignor->getRealContactPhone()}" : "";
                } else {
                    echo $model->invoice->company_name_short;
                    echo ($model->invoice->company_address_legal_full) ? ", {$model->invoice->company_address_legal_full}" : "";
                    echo ($model->invoice->company_phone) ? ", {$model->invoice->company_phone}" : "";
                }
                ?>
            </td>
            <td colspan="2" class="fs8 tr va-b" style="padding: 1px 4px; height:16pt;">по ОКПО</td>
            <td class="font-size-8-bold tc"
                style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= ($model->consignor) ? $model->consignor->okpo : $model->invoice->company->okpo; ?>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="fs6 tc ver-top m-l">полное наименование организации, адрес, номер телефона</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>

        <tr>
            <td width="12%" class="text-right ver-bottom m-l fs8">Грузополучатель</td>
            <td class="bb" colspan="2" style="padding-left: 8pt">
                <?php if ($model->consignee) {
                    if ($model->contractor_address == Waybill::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->consignee->legal_address;
                    } else {
                        $address = $model->consignee->actual_address;
                    }
                    echo $model->consignee->getTitle(true);
                    echo ($address) ? ", {$address}" : "";
                    echo ($model->consignee->getRealContactPhone()) ? ", {$model->consignee->getRealContactPhone()}" : "";
                } else {
                    if ($model->contractor_address == Waybill::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->invoice->contractor->legal_address;
                    } else {
                        $address = $model->invoice->contractor->actual_address;
                    }
                    echo $model->invoice->contractor_name_short;
                    echo ($address) ? ", {$address}" : "";
                    echo $model->invoice->contractor->getRealContactPhone() ? ", {$model->invoice->contractor->getRealContactPhone()}" : "";
                } ?>
            </td>
            <td colspan="2" class="fs8 tr va-b" style="padding: 1px 4px;">по ОКПО</td>
            <td class="fs8-b tc"
                style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= $model->invoice->contractor->okpo; ?>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="fs6 tc ver-top m-l">полное наименование организации, адрес, номер телефона</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l fs8">Плательщик</td>
            <td class="bb" colspan="2" style="padding-left:8pt">
                <?= $model->invoice->contractor_name_short,
                $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
            </td>
            <td colspan="2" class="fs8 tr va-b" style="border-bottom: 1px solid #fff;padding: 1px 4px">по ОКПО</td>
            <td class="fs8-b tc"
                style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 2px solid black;">
                <?= $model->invoice->contractor->okpo; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="fs6 tc ver-top m-l">полное наименование организации, адрес, номер телефона,
                банковские реквизиты
            </td>
            <td width="1%"></td>
            <td width="4.8%" style=""></td>
            <td style="padding: 1px 2px;"></td>
        </tr>

    </table>


    <table class="table no-border" style="margin-bottom: 0 !important; margin-top:10px;">
        <tr>
            <td width="100%" class="tc fs8">I. ТОВАРНЫЙ РАЗДЕЛ (заполняется грузоотправителем)</td>
        </tr>
    </table>

    <table class="table" style="margin-bottom: 0 !important; ">
        <tr>
            <td width="8%" class="tc bt2 bl2">Код продукции (номенклатурный номер)</td>
            <td width="8%" class="tc bt2">Номер прейскуранта и дополнения к нему</td>
            <td width="8%" class="tc bt2">Артикул или номер по прейскуранту</td>
            <td width="8%" class="tc bt2">Количество</td>
            <td width="8%" class="tc bt2">Цена, руб. коп</td>
            <td width="8%" class="tc bt2">Наименование продукции, товара (груза), ТУ, марка, размер, сорт</td>
            <td width="8%" class="tc bt2">Единица измерения</td>
            <td width="8%" class="tc bt2">Вид упаковки</td>
            <td width="8%" class="tc bt2">Количество мест</td>
            <td width="8%" class="tc bt2">Масса, т.</td>
            <td width="8%" class="tc bt2">Сумма, руб. коп.</td>
            <td width="8%" class="tc bt2 br2">Порядковый номер записи по складской картотеке (грузоотправителю,
                грузополучателю)
            </td>

        </tr>

        <tr class="border-2">
            <td class="tc  fs6">1</td>
            <td class="tc  fs6">2</td>
            <td class="tc  fs6">3</td>
            <td class="tc  fs6">4</td>
            <td class="tc  fs6">5</td>
            <td class="tc  fs6">6</td>
            <td class="tc  fs6">7</td>
            <td class="tc  fs6">8</td>
            <td class="tc  fs6">9</td>
            <td class="tc  fs6">10</td>
            <td class="tc  fs6">11</td>
            <td class="tc  fs6">12</td>

        </tr>
        <?php
        /** @var OrderWaybill $order */
        $orderQuery = OrderWaybill::find()->where(['waybill_id' => $model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order):
            $priceWithNds = $order->priceWithNds;
            $amountWithNds = $order->amountWithNds;
            if ($model->invoice->hasNds) {
                $priceNoNds = $order->priceNoNds;
                $amountNoNds = $order->amountNoNds;
                $TaxRateName = $order->order->saleTaxRate->name;
                $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
            } else {
                $priceNoNds = $order->priceWithNds;
                $amountNoNds = $order->amountWithNds;
                $TaxRateName = 'Без НДС';
                $ndsAmount = Product::DEFAULT_VALUE;
            }
            if ($hasMass) {
                $weight_in_tonn = ($order->order->unit_id == ProductUnit::UNIT_KG) ? $order->order->product->weight / 1000 : $order->order->product->weight;
            } else {
                $weight_in_tonn = '---';
            }
            ?>
            <tr>
                <td data-column="1" class="tc bl2"><?= $order->order->product_code; ?></td>
                <td data-column="2" class="tc"> -</td>
                <td data-column="3" class="tc"> -</td>
                <td data-column="4" class="tc"><?= $order->quantity; ?></td>
                <td data-column="5"
                    class="text-right"><?= TextHelper::invoiceMoneyFormat($priceWithNds, $precision); ?></td>
                <td data-column="6" class=""><?= $order->order->product_title; ?></td>
                <td data-column="7"
                    class="tc"><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                <td data-column="8" class="tc"><?= $order->order->box_type; ?></td>
                <td data-column="9"
                    class="tc"><?= ($realPlaceCount) ? $order->quantity * $order->order->place_count : '---'; ?></td>
                <td data-column="10"
                    class="tc"><?= ($hasMass) ? TextHelper::moneyFormat($order->quantity * $weight_in_tonn, 1) : '---' ?></td>
                <td data-column="11"
                    class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, $precision); ?></td>
                <td data-column="12" class="tc br2"></td>
            </tr>
        <?php endforeach; ?>

        <tr>
            <td colspan="9" rowspan="4" class="bt2 br2"
                style="vertical-align: top; border-left:none; border-bottom:none;">

                <table class="table no-border" style="margin: 0 !important;">
                    <tr>
                        <td width="27%" class="ver-bottom text-left" style="padding-top: 1px;padding-bottom: 1px">
                            Товарная накладная имеет продолжение на
                        </td>
                        <td width="20%" class="tc"
                            style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"> -
                        </td>
                        <td width="16%" class="ver-bottom text-left" style="padding-left: 5px">листах, на бланках за №
                        </td>
                        <td width="10%" class="tc"
                            style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"> -
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="table no-border" style="margin: 0 !important;">
                    <tr>
                        <td width="9%" class="ver-bottom text-left" style="padding-top: 1px;padding-bottom: 1px">и
                            содержит
                        </td>
                        <td width="28%" class="ver-bottom"
                            style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px">
                            <?= TextHelper::mb_ucfirst(RUtils::numeral()->getInWords(count($orderArray))); ?>
                        </td>
                        <td width="15%"
                            style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"></td>
                        <td width="25%" class="ver-bottom"
                            style="padding-top: 1px;padding-bottom: 1px; padding-left: 6px">порядковых номеров записей
                        </td>
                        <td></td>
                    </tr>
                </table>
                <table class="table no-border" style="margin: 0 !important;">
                    <tr>
                        <td width="7%"></td>
                        <td width="42%" class="fs6 tc vet-top">(прописью)</td>
                        <td></td>
                    </tr>
                </table>


                <table class="table no-border" style="margin-bottom: 0 !important;margin-top: -5px; width:95%;">
                    <tr>
                        <td class="">Всего наименований</td>
                        <td style="border-bottom: 1px solid black;padding-bottom: 1px; padding-top: 1px"
                            class="bb va-b">
                            <?= TextHelper::mb_ucfirst(RUtils::numeral()->getInWords(count($orderArray))); ?>
                        </td>
                        <td style="" class="va-b">Масса груза (нетто)</td>
                        <td style="border-bottom: 1px solid black;" class="va-b">
                            <?php if ($hasMass) {
                                echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassNet / 1000));
                            } ?>
                        </td>
                        <td></td>
                        <td style="border-bottom: 1px solid black;border-right: 2px solid black;border-top: 2px solid black;border-left: 2px solid black;"
                            class="tc">
                            <?php if ($hasMass) {
                                echo TextHelper::moneyFormat($realMassNet / 1000, 1);
                            } ?>
                        </td>
                        <td class="tl va-b" style="padding-left:5px">Т</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="fs6 tc vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                        <td></td>
                        <td class="fs6 tc vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                        <td></td>
                        <td style="border-left: 2px solid black; border-right: 2px solid black"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="10%" class="" style="padding: 1px 0 1px 0">Всего мест</td>
                        <td width="20%" style="border-bottom: 1px solid black;padding-bottom: 1px; padding-top: 1px"
                            class="">
                            <?php if ($realPlaceCount) {
                                echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                            } ?>
                        </td>
                        <td width="14%" class="va-b" style="">Масса груза (брутто)</td>
                        <td width="40%" style="border-bottom: 1px solid black" class="va-b">
                            <?php if ($hasMass) {
                                echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross / 1000));
                            } ?>
                        </td>
                        <td width="1%"></td>
                        <td width="7%"
                            style="border-left: 2px solid black; border-bottom: 2px solid black; border-right: 2px solid black;"
                            class="tc va-b">
                            <?php if ($hasMass) {
                                echo TextHelper::moneyFormat($realMassGross / 1000, 1);
                            } ?>
                        </td>

                        <td class="tl va-b" style="padding-left:5px">Т</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="fs6 tc vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                        <td></td>
                        <td class="fs6 tc vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>

            </td>
            <td class="bt2">Наценка, %</td>
            <td class="bt2 br2 tc"> -</td>
            <td class="bt2" style="border-right:none;border-bottom:none;border-left:none;"></td>
        </tr>
        <tr>
            <td class="">Складские или транспортные расходы</td>
            <td class="br2 tc"> -</td>
            <td style="border:none"></td>
        </tr>
        <tr>
            <td class="">&nbsp;</td>
            <td class="br2"></td>
            <td style="border:none"></td>
        </tr>
        <tr>
            <td class="bb2">Всего к оплате</td>
            <td class="br2 bb2 tr"><?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, $precision) ?></td>
            <td style="border:none"></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="22%" class="" style="padding-bottom: 1px; padding-top: 1px">Приложение (паспорт, сертификаты и
                т.п.) на
            </td>
            <td width="23%" class="tc" style="border-bottom: 1px solid #000;"> -</td>
            <td width="7%" class="" style="padding-bottom: 1px; padding-top: 1px">листах</td>
            <td width="12.5%" style="padding: 0 0 0 10px;border-left: 1px solid black" class="">По доверенности №</td>
            <td class="" style="padding:0">
                <table class="table no-border m-l">
                    <tr>
                        <td width="25%" class="" style="border-bottom: 1px solid #000">
                            <?= $model->proxy_number ?: '' ?>
                        </td>
                        <td width="9%" class="tc" style="padding-left:5px">
                            от
                            "&nbsp;<?= $model->proxy_date ? DateHelper::format($model->proxy_date, 'd', DateHelper::FORMAT_DATE) : '&nbsp;&nbsp;'; ?>
                            &nbsp;"
                        </td>
                        <td width="25%" class=" tc bb">
                            <?= ($model->proxy_date) ? \php_rutils\RUtils::dt()->ruStrFTime(['date' => $model->proxy_date, 'format' => 'F', 'monthInflected' => true]) : ''; ?>
                        </td>
                        <td width="25%" class="">
                            <?= ($model->proxy_date) ? DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE) : '20 &nbsp;&nbsp;&nbsp;&nbsp;' ?>
                            г.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <div id="print_layer">
        <div id="signature1_layer">
            <div id="signature2_layer">
                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td></td>
                        <td colspan="5" class="tc vet-top fs6" style="padding-bottom: 1px; padding-top: 1px">
                            (прописью)
                        </td>
                        <td class="" style="border-left: 1px solid black;padding: 0 0 0 10px;">выданной</td>
                        <td colspan="5" class="bb">
                            <?= ($model->consignor) ? $model->consignor->shortName : $model->invoice->company_name_short ?>
                            <?php /*
                            $model->consignee ? $model->consignee->getTitle(true) : $model->invoice->contractor_name_short,
                            $model->given_out_position ? ", {$model->given_out_position}" : null,
                            $model->given_out_fio ? ", {$model->given_out_fio}" : null; */ ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" class="va-b" style="border-bottom: none;">
                            Всего отпущено на сумму
                        </td>
                        <td colspan="5" class="va-b" style="border-bottom: 1px solid #000;">
                            <?= \common\components\TextHelper::amountToWords($model->totalAmountWithNds / 100); ?>
                        </td>
                        <td width="12.5%" class="va-b" style="border-left: 1px solid black;padding: 10px 0 0 10px;">
                            Груз к перевозке принял

                        </td>
                        <td width="10%" class="va-b"><?= $model->given_out_position ?></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td class="tc va-b"><?= $model->given_out_fio ?></td>
                    </tr>
                    <tr>
                        <td colspan="1" class="tc vet-top fs6" style="padding-bottom: 0; padding-top: 0"></td>
                        <td colspan="5" class="tc vet-top fs6"
                            style="border-top: 1px solid black;padding-bottom: 0; padding-top: 0">(прописью)
                        </td>
                        <td style="border-left: 1px solid black"></td>
                        <td class="tc vet-top fs6 bt">(должность)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(подпись)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(расшифровка подписи)</td>
                    </tr>
                    <tr>
                        <td width="12%" class="ver-bottom" style="padding-bottom: 1px; padding-top: 8px">
                            Отпуск груза разрешил

                        </td>
                        <td width="14%" class="va-b">
                            <?= $model->release_allowed_position ?>
                        </td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%" class="va-b tc">
                            <?= $model->release_allowed_fullname ?>
                        </td>
                        <td colspan="6" style="border-left: 1px solid black;padding: 0 0 0 10px;" class="">(При личном
                            приеме товара по количеству и ассортименту)
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tc vet-top fs6"
                            style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(должность)
                        </td>
                        <td></td>
                        <td class="tc vet-top fs6"
                            style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(подпись)
                        </td>
                        <td></td>
                        <td class="tc vet-top fs6"
                            style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(расшифровка
                            подписи)
                        </td>
                        <td style="border-left: 1px solid black"></td>
                        <td colspan="5" style=""></td>
                    </tr>
                </table>


                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td width="27%" class="" style="">Главный (старший) бухгалтер</td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%" class=" tc" style="">
                            <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP): ?>
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            <?php else : ?>
                                <?= $model->invoice->getCompanyChiefFio(true); ?>
                            <?php endif ?>
                        </td>
                        <td width="12.5%" class="bb" style="border-left: 1px solid black;padding: 0 0 0 10px;"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(подпись)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(расшифровка подписи)</td>
                        <td style="border-left: 1px solid black;"></td>
                        <td colspan="5" class="tc vet-top fs6 bt"></td>
                    </tr>
                </table>
                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td width="12%" class="va-b" style="">Отпуск груза произвел</td>
                        <td width="14%"><?= $model->release_produced_position ?></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%" class="tc va-b"><?= $model->release_produced_fullname ?></td>
                        <td width="10.5%" style="border-left: 1px solid black;padding: 0 0 0 10px;" class="">Груз
                            получил
                        </td>
                        <td width="10%" class="tc va-b"><?= $model->cargo_accepted_position ?></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td class="tc va-b"><?= $model->cargo_accepted_fullname ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(должность)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(подпись)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(расшифровка подписи)</td>
                        <td class="vet-top"
                            style="margin-top: 0 !important;border-left: 1px solid black;padding: 0 0 0 10px;">
                            грузополучатель
                        </td>
                        <td class="tc vet-top fs6 bt">(должность)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(подпись)</td>
                        <td></td>
                        <td class="tc vet-top fs6 bt">(расшифровка подписи)</td>
                    </tr>
                    <tr>
                        <td class="text-right">М.П.</td>
                        <td class="text-right">
                            "&nbsp;<?= DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE); ?>&nbsp;"
                        </td>
                        <td></td>
                        <td class="tc" style="border-bottom: 1px solid black">
                            <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                'date' => $model->document_date,
                                'format' => 'F', // month in word
                                'monthInflected' => true,
                            ]);; ?>
                        </td>
                        <td></td>
                        <td class=""><?= DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE); ?>г.
                        </td>
                        <td class="text-right" style="border-left: 1px solid black"></td>
                        <td class="text-right"></td>
                        <td></td>
                        <td style=""></td>
                        <td></td>
                        <td class=""></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>