<?php

use frontend\rbac\permissions\document\Document;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap4\Dropdown;
use yii\helpers\Html;
use common\models\document\status;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var \common\models\document\Waybill $model */
/* @var $useContractor boolean */

$contractorId = $useContractor ? $model->invoice->contractor_id : null;

$canUpdate = $model->status_out_id != status\WaybillStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$canDelete = Yii::$app->user->can(Document::DELETE, [
    'model' => $model,
]);
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if (true || $canUpdate) : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'envelope']).'<span>Отправить</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a($this->render('//svg-sprite', ['ico' => 'print']).'<span>Печать</span>', [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $model->type,
                'filename' => $model->getPrintTitle(),
            ], [
                'target' => '_blank',
                'class' => 'button-clr button-regular button-hover-transparent w-full',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <div class="dropup">
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'download']).'<span>Скачать</span>', [
                    'class' => 'button-clr button-regular button-hover-transparent w-full no-after',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr',
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'check-2']).' <span>Передана</span>', [
                    'update-status',
                    'type' => $model->type,
                    'id' => $model->id,
                    'contractorId' => $contractorId,
                ], [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\WaybillStatus::STATUS_SEND,
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'check-double']).' <span>Подписана</span>', [
                    'update-status',
                    'type' => $model->type,
                    'id' => $model->id,
                    'contractorId' => $contractorId,
                ], [
                    'class' => 'button-clr button-regular button-hover-transparent w-full',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\WaybillStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($canDelete): ?>
                <?= ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::toRoute([
                        'delete',
                        'type' => $model->type,
                        'id' => $model->id,
                        'contractorId' => $contractorId,
                    ]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить эту накладную?",
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($canUpdate): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>
