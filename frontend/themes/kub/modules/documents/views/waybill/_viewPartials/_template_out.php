<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\components\Icon;

/** @var \common\models\document\OrderWaybill $order */
/** @var [] $product */

$plus = 0;

?>
<?php if (isset($order)): ?>

    <?php
    $amountWithNds = $order->amountWithNds;
    $priceWithNds  = $order->priceWithNds;
    if ($order->waybill->invoice->hasNds) {
        $priceNoNds = $order->priceNoNds;
        $amountNoNds = $order->amountNoNds;
        $TaxRateName = $order->order->saleTaxRate->name;
        $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
    } else {
        $priceNoNds = $order->priceWithNds;
        $amountNoNds = $order->amountWithNds;
        $TaxRateName = 'Без НДС';
        $ndsAmount = Product::DEFAULT_VALUE;
    }
	if ($order->order->product->weight > 0) {
		$weight_in_tonn = ($order->order->unit_id != ProductUnit::UNIT_TON) ? $order->order->product->weight / 1000 : $order->order->product->weight;
	} else {
		$weight_in_tonn = '---';
	}
    ?>
    <tr>
        <td><?= $order->order->product_code; ?></td>
        <td> --- </td>
        <td> --- </td>
        <td>
            <input
                    class="input-editable-field quantity  form-control" type="number"
                    value="<?= $order->quantity ?>" min="1"
                    max="<?= $order ? $order->getAvailableQuantity() : 1 ?>"
                    id="<?= $order->order_id ?>"
                    name="<?= $order ? 'OrderWaybill[' . $order->order_id . '][quantity]' : '' ?>"
                    style="padding: 6px 6px;">
        </td>
        <input class="status" type="hidden"
               name="<?= 'OrderWaybill[' . $order->order_id . '][status]' ?>"
               value="active">
        <td><?= TextHelper::invoiceMoneyFormat($priceWithNds, $precision); ?></td>
        <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->box_type; ?></td>
        <td><?= $order->order->place_count; ?></td>
        <td><?= $weight_in_tonn > 0 ? TextHelper::numberFormat($order->order->quantity * $weight_in_tonn, 4) : '---'; ?></td>
        <td><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
        <td> --- </td>
        <td><span class="input-editable-field delete-row"><?= Icon::get('circle-close') ?></span></td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?=  \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td> </td>
        <input class="status" type="hidden"
               name=""
               value="active">
        <td></td>
        <td</td>
        <td></td>
        <td class="text-right"></td>
        <td><span class="editable-field"><?= $key + 1; ?></span><span
                    class="input-editable-field delete-row hide"><?= Icon::get('circle-close') ?></span>
        </td>
    </tr>
<?php endif; ?>
