<?php

/* @var $this \yii\web\View */
/* @var \common\models\document\Waybill $model */

use common\models\document\OrderWaybill;
use kartik\widgets\Select2;
use yii\bootstrap4\Html;
use yii\widgets\MaskedInput;

/** @var OrderWaybill[] $orderArray */
$orderArray = OrderWaybill::find()->where(['waybill_id' => $model->id])->all();
$tableClass = isset($tableClass) ? $tableClass : 'table table-style table-count out-packinglist_table';
$plus = 0;

// for MaskedInput
$maskDistance = '9{0,4}';
$maskTimeDefinition = [
    'h'=>[
        'cardinality'=>2,
        'prevalidator' => [
            ['validator'=>'^([0-2])$', 'cardinality'=>1],
            ['validator'=>'^([0-9]|0[0-9]|1[0-9]|2[0-3])$', 'cardinality'=>2],
        ],
        'validator'=>'^([0-9]|0[0-9]|1[0-9]|2[0-3])$'
    ],
    'm'=>[
        'cardinality'=>2,
        'prevalidator' => [
            ['validator'=>'^(0|[0-5])$', 'cardinality'=>1],
            ['validator'=>'^([0-5]?\d)$', 'cardinality'=>2],
        ]
    ]
];

$maskTimeFormat = function($param) { return $param; }
?>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th colspan="9">
                    С В Е Д Е Н И Я &nbsp; О &nbsp; Г Р У З Е
                </th>
            </tr>
            <tr class="heading" role="row">
                <th>Краткое описание груза</th>
                <th>С грузом следуют документы</th>
                <th>Вид упаковки</th>
                <th>Количество мест</th>
                <th>Способ определения массы</th>
                <th>Код груза</th>
                <th>Номер контейнера</th>
                <th>Класс груза</th>
                <th>Масса брутто, т</th>
            </tr>
            <tr class="heading heading-small" role="row">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th>5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orderArray as $order): ?>
                <tr>
                    <td>
                        <?= $order->order->product_title ?>
                    </td>
                    <td>
                        <div style="width:100%"><?= Html::activeCheckbox($order, 'with_packing_list', [
                            'label' => 'Транспортная накладная',
                            'name' => "Waybill[updatedOrders][{$order->id}][with_packing_list]",
                            'id' => "orderwaybill-{$order->id}-with_packing_list",
                        ]) ?></div>
                        <div style="width:100%"><?= Html::activeCheckbox($order, 'with_invoice', [
                            'label' => 'Счет',
                            'name' => "Waybill[updatedOrders][{$order->id}][with_invoice]",
                            'id' => "orderwaybill-{$order->id}-with_invoice",
                        ]) ?></div>
                        <div style="width:100%"><?= Html::activeCheckbox($order, 'with_invoice_facture', [
                            'label' => 'Счет-фактура',
                            'name' => "Waybill[updatedOrders][{$order->id}][with_invoice_facture]",
                            'id' => "orderwaybill-{$order->id}-with_invoice_facture",
                        ]) ?></div>
                    </td>
                    <td>
                        <?= Html::activeTextInput($order, 'consignment_packaging_type', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_packaging_type]",
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::activeTextInput($order, 'consignment_places_count', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_places_count]",
                        ]) ?>
                    </td>
                    <td>
                        <?= Select2::widget([
                            'model' => $order,
                            'attribute' => 'weight_determining_method',
                            'data' => OrderWaybill::$WEIGHT_DETERMINING_METHOD,
                            'pluginOptions' => [
                                'width' => '170px',
                            ],
                            'options' => [
                                'name' => "Waybill[updatedOrders][{$order->id}][weight_determining_method]",
                                'id' => "orderwaybill-{$order->id}-weight_determining_method",
                            ],
                        ]) ?>

                    </td>
                    <td>
                        <?= Html::activeTextInput($order, 'consignment_code', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_code]",
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::activeTextInput($order, 'consignment_container_number', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_container_number]",
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::activeTextInput($order, 'consignment_classification', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_classification]",
                        ]) ?>
                    </td>
                    <td>
                        <?= Html::activeInput('number', $order, 'consignment_gross_weight', [
                            'class' => 'form-control',
                            'name' => "Waybill[updatedOrders][{$order->id}][consignment_gross_weight]",
                            'min' => 0,
                            'step' => 1,
                        ]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th colspan="10">
                    П О Г Р У З О Ч Н О  -  Р А З Г Р У З О Ч Н Ы Е &nbsp;  О П Е Р А Ц И И
                </th>
            </tr>
            <tr class="heading" role="row">
                <th rowspan="2">операция</th>
                <th rowspan="2">исполнитель (автовладелец, получатель, отправитель)</th>
                <th rowspan="2">дополнительные операции (наименование, количество)</th>
                <th rowspan="2">механизм, грузоподъемность, емкость ковша</th>
                <th colspan="2" style="border-bottom: 1px solid #f2f3f7;">способ</th>
                <th colspan="2" style="border-bottom: 1px solid #f2f3f7;">дата (число, месяц), время, ч. мин.</th>
                <th rowspan="2">время дополнительных операций, мин.</th>
                <th rowspan="2">подпись ответственного лица</th>
            </tr>
            <tr class="heading" role="row">
                <th>ручной, механизированный, наливом, самосвалом</th>
                <th>код</th>
                <th>прибытия</th>
                <th>убытия</th>
            </tr>
            <tr class="heading heading-small" role="row">
                <th>10</th>
                <th>11</th>
                <th>12</th>
                <th>13</th>
                <th>14</th>
                <th>15</th>
                <th>16</th>
                <th>17</th>
                <th>18</th>
                <th>19</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Погрузка</td>
                <td>
                    <?= Html::activeTextInput($model, 'loading_contractor', [
                        'class' => 'form-control',
                    ]) ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'loading_operations', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td></td>
                <td>
                    <?= Html::activeInput('text', $model, 'loading_method_name', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'loading_method_code', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                    'name' => 'Waybill[loading_time_come]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->loading_time_come,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[loading_time_gone]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->loading_time_gone,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[loading_time_operations]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->loading_time_operations,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Разрузка</td>
                <td>
                    <?= Html::activeTextInput($model, 'unloading_contractor', [
                        'class' => 'form-control',
                    ]) ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'unloading_operations', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td></td>
                <td>
                    <?= Html::activeInput('text', $model, 'unloading_method_name', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'unloading_method_code', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[unloading_time_come]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->unloading_time_come,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[unloading_time_gone]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->unloading_time_gone,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[unloading_time_operations]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->unloading_time_operations,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th colspan="13">
                    П Р О Ч И Е &nbsp; С В Е Д Е Н И Я &nbsp; ( з а п о л н я е т с я &nbsp;  о р г а н и з а ц и е й , &nbsp; в л а д е л ь ц е м &nbsp; а в т о т р а н с п о р т а)
                </th>
            </tr>
            <tr class="heading" role="row">
                <th colspan="5">расстояние перевозки по группам дорог, км</th>
                <th rowspan="2">код экспедирования груза</th>
                <th colspan="2">за транспортные услуги</th>
                <th rowspan="2">сумма штрафа за неправильное оформление документов, руб. коп.</th>
                <th colspan="2">поправочный коэффициент</th>
                <th colspan="2">время простоя, ч. мин.</th>
            </tr>
            <tr class="heading" role="row">
                <th>всего</th>
                <th>в гор.</th>
                <th>I гр.</th>
                <th>II гр.</th>
                <th>III гр.</th>
                <th>с клиента</th>
                <th>причитается водителю</th>
                <th>расценка водителю</th>
                <th>основной тариф</th>
                <th>под погрузкой</th>
                <th>под разгрузкой</th>
            </tr>
            <tr class="heading heading-small" role="row">
                <th width="8%">20</th>
                <th width="8%">21</th>
                <th width="8%">22</th>
                <th width="8%">23</th>
                <th width="8%">24</th>
                <th>25</th>
                <th width="10%">26</th>
                <th>27</th>
                <th>28</th>
                <th>29</th>
                <th>30</th>
                <th>31</th>
                <th>32</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_distance]',
                        'mask' => $maskDistance,
                        'value' => $model->transportation_distance,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'км'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_in_city]',
                        'mask' => $maskDistance,
                        'value' => $model->transportation_in_city,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'км'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_group_1]',
                        'mask' => $maskDistance,
                        'value' => $model->transportation_group_1,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'км'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_group_2]',
                        'mask' => $maskDistance,
                        'value' => $model->transportation_group_2,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'км'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_group_3]',
                        'mask' => $maskDistance,
                        'value' => $model->transportation_group_3,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'км'
                        ]
                    ]); ?>
                </td>
                <td></td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_cost]',
                        'mask' => '9{0,9}',
                        'value' => $model->transportation_cost,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'руб.'
                        ]
                    ]); ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_downtime_loading]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->transportation_downtime_loading,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
                <td>
                    <?= MaskedInput::widget([
                        'name' => 'Waybill[transportation_downtime_unloading]',
                        'mask' => 'h:m',
                        'definitions' => $maskTimeDefinition,
                        'value' => $model->transportation_downtime_unloading,
                        'options' => [
                            'class' => 'form-control',
                            'placeholder' => 'чч:мм'
                        ]
                    ]); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table style="margin-bottom: 25px; width:100%">
            <tr>
                <td width="10%" style="padding-right:25px;">Таксировка</td>
                <td>
                    <?= Html::activeInput('text', $model, 'taxation', [
                        'placeholder' => 'Таксировка',
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th rowspan="3">Расчет стоимости</th>
                <th rowspan="2">За тонны</th>
                <th rowspan="2">За тонно-км</th>
                <th rowspan="2">Погрузочно- разгрузочные работы, тонн</th>
                <th rowspan="2">Недогрузка автомобиля и прицепа</th>
                <th rowspan="2">Экспеди- рование</th>
                <th colspan="2">Сверхнормативный простой, ч. мин. при</th>
                <th rowspan="2">За сроч- ность заказа</th>
                <th rowspan="2">За специ- альный транспорт</th>
                <th rowspan="2">Прочие доплаты</th>
                <th rowspan="2">Всего</th>
            </tr>
            <tr class="heading" role="row">
                <th>погрузке</th>
                <th>разгрузке</th>
            </tr>
            <tr class="heading heading-small" role="row">
                <th>33</th>
                <th>34</th>
                <th>35</th>
                <th>36</th>
                <th>37</th>
                <th>38</th>
                <th>39</th>
                <th>40</th>
                <th>41</th>
                <th>42</th>
                <th>43</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Выполнено</td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_per_ton', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_per_ton_km', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_loading_and_unloading_works', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_underload', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_forwarding', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_excess_simple_loading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_excess_simple_unloading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_urgency', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_special_transport', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_another_surcharge', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'completed_total', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
            </tr>
            <tr>
                <td>Расценка, руб.коп</td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_per_ton', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_per_ton_km', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_loading_and_unloading_works', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_underload', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_forwarding', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_excess_simple_loading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_excess_simple_unloading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_urgency', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_special_transport', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_another_surcharge', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'rate_total', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
            </tr>
            <tr>
                <td width="10%">К оплате, руб.коп</td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_per_ton', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_per_ton_km', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_loading_and_unloading_works', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_underload', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_forwarding', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_excess_simple_loading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_excess_simple_unloading', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_urgency', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_special_transport', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_another_surcharge', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
                <td>
                    <?= Html::activeInput('text', $model, 'to_pay_total', [
                        'class' => 'form-control',
                        'style' => 'width: 100%;',
                    ]); ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table style="width:100%">
            <tr>
                <td width="10%" style="padding-right:25px;">Таксировщик</td>
                <td width="20%">
                <?= Html::activeInput('text', $model, 'taximaster', [
                    'placeholder' => 'ФИО',
                    'class' => 'form-control',
                    'style' => 'width: 100%; margin-left:0',
                ]); ?>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</div>