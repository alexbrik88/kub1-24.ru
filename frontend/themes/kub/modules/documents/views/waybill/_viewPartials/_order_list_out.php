<?php

/* @var $this \yii\web\View */
/* @var \common\models\document\Waybill $model */

use common\models\document\OrderWaybill;
use frontend\components\Icon;

$plus = 0;
$tableClass = isset($tableClass) ? $tableClass : 'table table-style table-count out-packinglist_table';

?>

<div class="scroll-table custom-scroll-table">
    <div class="table-wrap">
        <table class="<?= $tableClass; ?>">
            <thead>
            <tr class="heading" role="row">
                <th>Код продукции (номенклатурный номер)</th>
                <th>Номер прейскуранта и дополнения к нему</th>
                <th>Артикул или номер по прейскуранту</th>
                <th>Количество</th>
                <th>Цена, руб. коп.</th>
                <th>Наименование продукции, товара (груза), ТУ, марка, размер, сорт</th>
                <th>Единица измерения</th>
                <th>Вид упаковки</th>
                <th>Количество мест</th>
                <th>Масса, т</th>
                <th>Сумма, руб. коп.</th>
                <th>Порядковый номер записи по складской картотеке (грузоотправителю, грузополучателю)</th>
                <th></th>
            </tr>
            <tr class="heading heading-small" role="row">
                <th>1</th>
                <th>2</th>
                <th>3</th>
                <th>4</th>
                <th width="8%">5</th>
                <th>6</th>
                <th>7</th>
                <th>8</th>
                <th>9</th>
                <th>10</th>
                <th width="8%">11</th>
                <th>12</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="tbody">
            <?php
            foreach (OrderWaybill::findAll(['waybill_id' => $model->id]) as $key => $order): ?>
                <?php echo $this->render('_template_out', [
                    'key' => $key,
                    'order' => $order,
                    'precision' => $precision,
                ]);
                ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<span id="plusbtn" class="btn-add-line-table button-regular button-hover-content-red hide">
    <?= Icon::get('add-icon') ?>
    <span>Добавить</span>
</span>

<?php

$count = count(OrderWaybill::findAll(['waybill_id' => $model->id]));
$urlAdd = \yii\helpers\Url::to('/documents/waybill-ajax/add-new-row');
$urlDel = \yii\helpers\Url::to('/documents/waybill-ajax/delete-row');
$urlSub = \yii\helpers\Url::to('/documents/waybill-ajax/subsitution');
$urlClose = \yii\helpers\Url::to('/documents/waybill-ajax/close');
$urlEdit = \yii\helpers\Url::to('/documents/waybill-ajax/edit');
$script = <<< JS
var count = parseInt('$count');

$('#plusbtn').click(function() {
    var active = [];
    $('#plusbtn').addClass('hide');
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlAdd',
        data : {key : count, invoice_id: '$model->invoice_id', waybill_id: '$model->id', active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
        }
    });
    count++;
});

$(document).on('click', '.delete-row', function() {
    $(this).closest('tr').remove();
});

/*$(document).on('click', '.delete-row', function() {
    $(this).closest('tr').find('.status').val('deleted');
    var active = [];
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlDel',
        data : {waybill_id : '$model->id',active : active},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
        }
    });

    if ($(this).closest('tr').find('.status').val() == ''){
        $(this).closest('tr').remove();
    } else {
        $(this).closest('tr').hide();
    }

    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
});*/

$(document).on('change','.dropdownlist',function() {
    var active = [];
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlSub',
        data : {order_id: $(this).val(), waybill_id : '$model->id',active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
            if( $('.order:visible').length == 1){
                $('.delete-row').hide();
            }
            if( $('.order:visible').length > 1){
                $('.delete-row').show();
            }
        }
    });
    $(this).closest('tr').remove();
});

$(document).on('click','.btn-cancel', function() {
    $('.edit-in').hide();
    $('#plusbtn').addClass('hide');
    jQuery.get({
        url : '$urlClose',
        data : {waybill_id : '$model->id',ioType : 2} ,
        success : function(data) {
            jQuery('.customers_table').replaceWith(data);
            $('.edit-in').show();
        }
    })
});

$(document).ready(function () {
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {waybill_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});

$(document).on('click','.edit-in', function() {
    $('#plusbtn').addClass('hide');
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {waybill_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});
JS;
$this->registerJs($script, $this::POS_READY);
?>


