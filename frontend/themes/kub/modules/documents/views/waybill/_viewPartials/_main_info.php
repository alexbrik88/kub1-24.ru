<?php

use common\models\project\Project;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\document\Waybill */
/* @var $message frontend\modules\documents\components\Message; */

$invoice = $model->invoice;
$hasProject = Yii::$app->user->identity->menuItem->project_item;
$project = Project::find()->where(['id' => $invoice->project_id])->one();
$basis = '';
if ($invoice->basis_document_name &&
    $invoice->basis_document_number &&
    $invoice->basis_document_date
) {
    $basis = $invoice->basis_document_name;
    $basis .= ' № '. Html::encode($invoice->basis_document_number);
    $basis .= ' от ' . date('d.m.Y', strtotime($invoice->basis_document_date));
} else {
    $basis = 'Счет № '. $invoice->fullNumber;
    $basis .= ' от ' . date('d.m.Y', strtotime($invoice->document_date));
}
?>
<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($invoice->contractor_name_short, [
            '/contractor/view',
            'type' => $invoice->contractor->type,
            'id' => $invoice->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <?php if ($hasProject && $project) : ?>
        <div class="about-card-item">
            <span class="text-grey">Проект:</span>
            <span>
            <?= Html::a($project->name, ['/project/view', 'id' => $project->id], ['class' => 'link']); ?>
        </span>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">Основание:</span>
        <span>
        <?= $basis ?>
    </span>
    </div>
    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
        <div class="about-card-item">
            <label class="label" for="add_stamp">Печать и подпись:</label>
            <div style="display: flex;">
                <?= Html::checkbox('add_stamp', $model->add_stamp, [
                    'id' => 'add_stamp',
                    'class' => 'kub-switch',
                ]); ?>
                <button type="button" class="button-clr ml-2" data-toggle="tooltip" data-placement="bottom" title="Добавить в ТН печать и подпись при отправке по e-mail и при скачивании в PDF">
                    <svg class="tooltip-question-icon svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                    </svg>
                </button>
            </div>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <?= DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>

<?php
$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#add_stamp", function() {
        let printLayer = $("#print_layer");        
        let signatureFirstLayer = $("#signature1_layer");
        let signatureSecondLayer = $("#signature2_layer");

        $.post("' . $jsUrl . '", $(this).serialize());
        if ($(this).is(":checked")) {
            printLayer.css("background-image", "url(" + printLayer.data("image") + ")");
            signatureFirstLayer.css("background-image", "url(" + signatureFirstLayer.data("image") + ")");
            signatureSecondLayer.css("background-image", "url(" + signatureSecondLayer.data("image") + ")");

            return;
        }
        
        printLayer.css("background-image", "url(\'\')");
        signatureFirstLayer.css("background-image", "url(\'\')");
        signatureSecondLayer.css("background-image", "url(\'\')");
    });
');
