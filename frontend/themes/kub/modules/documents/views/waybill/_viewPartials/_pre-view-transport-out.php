<?php

/**
 * @var $this yii\web\View
 * @var $model Waybill
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\OrderWaybill;
use common\models\document\Waybill;
use common\models\product\Product;
use common\models\product\ProductUnit;
use php_rutils\RUtils;

$company = $model->invoice->company;
$precision = $model->invoice->price_precision;
/** @var OrderWaybill[] $orderArray */
$orderArray = OrderWaybill::find()->where(['waybill_id' => $model->id])->all();
$realMassGross = $model->getRealMassGross($orderArray);
$realMassNet = $model->getRealMassNet($orderArray);
$realPlaceCount = $model->getRealPlaceCount($orderArray);
$invoiceItemName = '';
$invoiceFactureItemName = '';
$packingListItemName = '';

$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if ($model->invoice->invoiceFacture)
    $invoiceFactureItemName = 'Счет-фактура № ' . $model->invoice->invoiceFacture->fullNumber . ' от ' .
        DateHelper::format($model->invoice->invoiceFacture->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
if ($model->invoice->packingList)
    $packingListItemName = 'ТН № ' . $model->invoice->packingList->fullNumber . ' от ' .
        DateHelper::format($model->invoice->packingList->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$hasMass = $realMassGross && $realMassNet;
?>
<div class="over-hidden m-size-div container-first-account-table no_min_h pad0"
     style="min-width: 520px; margin-top: 95px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3">
            <div class="no-border">
                <div class="pad0" style="position: relative;">
                    <div class="caption">
                        <div>
                            <table class="table no-border" style="margin-bottom: 0!important;">
                                <tr>
                                    <td width="83%" class="font-size-8 text-center" style="padding-left: 12%">II. ТРАНСПОРТНЫЙ РАЗДЕЛ</td>
                                    <td colspan="2" class="font-size-7 pr-0 text-right">Оборотная сторона формы № 1-Т</td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tbody>
                                            <tr>
                                                <td width="12%" class="text-left font-size-7" style="padding-left: 0;">
                                                    Срок доставки груза
                                                </td>
                                                <td width="3%" class="font-size-7">
                                                    "&nbsp;<?= $model->delivery_time ? DateHelper::format($model->delivery_time, 'd', DateHelper::FORMAT_DATE) : '&nbsp;&nbsp;'; ?>&nbsp;"
                                                </td>
                                                <td width="12%" class="font-size-7 text-center" style="border-bottom: 1px solid black;">
                                                    <?= ($model->delivery_time) ? RUtils::dt()->ruStrFTime(['date' => $model->delivery_time, 'format' => 'F', 'monthInflected' => true]) : ''; ?>
                                                </td>
                                                <td width="20%" class="font-size-7">
                                                    <?= ($model->delivery_time) ? DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE) : '20 &nbsp;&nbsp;&nbsp;&nbsp;' ?> г.
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="9%" class="font-size-8 text-right" style="padding: 1px 4px;vertical-align:middle;">ТТН №</td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">
                                        №<?=$model->getFullNumber()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="8%" class="text-left font-size-7" style="padding-left: 0;">Организация</td>
                                                <td width="46%" class="font-size-7">
                                                    <?php if ($model->organization) : ?>
                                                        <?= $model->organization ?>
                                                    <?php else : ?>
                                                        <?php if ($model->consignor) {
                                                            echo $model->consignor->getRequisitesFull();
                                                        } else {
                                                            echo $model->invoice->company_name_short,
                                                            ", {$model->invoice->company_address_legal_full}",
                                                            ', ИНН ', $model->invoice->company_inn,
                                                            ", БИК {$model->invoice->company_bik}",
                                                            $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';
                                                        }  ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td width="8%" class="font-size-7">Автомобиль</td>
                                                <td width="10%" class="font-size-7"><?= $model->car ?></td>
                                                <td width="18%" class="font-size-7 pl-0 pr-0">Государственный номерной знак</td>
                                                <td class="font-size-7"><?= $model->car_number ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px 1px 25px;">К путевому листу №</td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black ; border-bottom: 1px solid black;padding: 1px 2px;">
                                        <?= ($model->waybill_number && $model->waybill_date) ?
                                            $model->waybill_number .' от '. DateHelper::format($model->waybill_date, 'd.m.y', DateHelper::FORMAT_DATE) :
                                            ''
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="8%" class="pb-0"></td>
                                                <td width="46%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    наименование, адрес, номер телефона, банковские реквизиты
                                                </td>
                                                <td width="8%" class="pb-0"></td>
                                                <td width="10%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">марка</td>
                                                <td width="20%" class="pb-0"></td>
                                                <td class="pb-0"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="pb-0"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="13%" class="text-left font-size-7" style="padding-left: 0;">Заказчик(плательщик)</td>
                                                <td class="font-size-7">
                                                    <?= $model->invoice->contractor_name_short,
                                                    $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                                                    $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                                                    $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                                                    $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                                                    $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                                                    $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                                                    $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="13%" class="pb-0"></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    наименование, адрес, номер телефона, банковские реквизиты
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="pb-0"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="7%" class="text-left font-size-7" style="padding-left: 0;">Водитель</td>
                                                <td width="24%" class="font-size-7">
                                                    <?= $model->given_out_fio ?>
                                                </td>
                                                <td width="10%" class="font-size-7 pl-0 pr-0">Удостоверение №</td>
                                                <td width="24%" class="font-size-7">
                                                    <?= $model->driver_license ?>
                                                </td>
                                                <td class="font-size-7"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="7%" class="pb-0"></td>
                                                <td width="24%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    фамилия, имя, отчество
                                                </td>
                                                <td width="10%" class="pb-0"></td>
                                                <td width="24%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                                <td class="pb-0"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="pb-0"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="14%" class="text-left font-size-7" style="padding-left: 0;">Лицензионная карточка</td>
                                                <td width="18%" class="font-size-7">
                                                    <?php if ($model->license_card == Waybill::LICENSE_LIMITED) : ?>
                                                        <del>стандартная</del>, ограниченная
                                                    <?php else : ?>
                                                        стандартная, <del>ограниченная</del>
                                                    <?php endif; ?>
                                                </td>
                                                <td width="10%" class="font-size-7 pr-0">Вид перевозки</td>
                                                <td class="font-size-7">
                                                    <?= $model->transportation_type ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="14%" class="pb-0"></td>
                                                <td width="18%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    ненужное зачеркнуть
                                                </td>
                                                <td width="10%" class="pb-0"></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;">Код</td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="12%" class="text-left font-size-7" style="padding-left: 0;">Регистрационный №</td>
                                                <td width="8%" class="font-size-7"></td>
                                                <td width="5%" class="font-size-7">серия</td>
                                                <td width="5%" class="font-size-7"></td>
                                                <td width="2%" class="font-size-7">№</td>
                                                <td width="7%" class="font-size-7"></td>
                                                <td class="font-size-7"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="12%" class="pb-0"></td>
                                                <td width="8%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                                <td width="5%" class="pb-0"></td>
                                                <td width="5%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                                <td width="2%" class="pb-0"></td>
                                                <td width="7%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                                <td class="pb-0"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="text-left font-size-7" style="padding-left: 0;">
                                                    Пункт погрузки
                                                </td>
                                                <td width="30%" class="font-size-7"></td>
                                                <td width="15%" class="font-size-7 text-center">
                                                    Пункт разгрузки
                                                </td>
                                                <td class="font-size-7"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="pb-0"></td>
                                                <td width="30%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    адрес, номер телефона
                                                </td>
                                                <td width="15%" class="pb-0"></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;">
                                        Маршрут
                                    </td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px;">
                                        <?= $model->transportation_route ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="text-left font-size-7" style="padding-left: 0;">
                                                    Переадресовка
                                                </td>
                                                <td width="30%" class="font-size-7"></td>
                                                <td width="15%" class="font-size-7"></td>
                                                <td width="8%" class="font-size-7 text-center">1. Прицеп</td>
                                                <td width="10%" class="font-size-7">
                                                    <?= $model->trailer1_brand ?>
                                                </td>
                                                <td width="20%" class="font-size-7 text-center">Государственный номерной знак</td>
                                                <td  class="font-size-7">
                                                    <?= $model->trailer1_number ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;">
                                        Гаражный номер
                                    </td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;">
                                        <?= $model->trailer1_garage_number ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="pb-0"></td>
                                                <td width="30%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    наименование и адрес нового грузополучателя
                                                </td>
                                                <td width="15%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    номер распоряжения
                                                </td>
                                                <td width="8%" class="pb-0"></td>
                                                <td width="10%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    марка
                                                </td>
                                                <td width="20%" class="pb-0"></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px;"></td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="text-left font-size-7" style="padding-left: 0;"></td>
                                                <td width="30%" class="font-size-7"></td>
                                                <td width="15%" class="font-size-7"></td>
                                                <td width="8%" class="font-size-7 text-center">2. Прицеп</td>
                                                <td width="10%" class="font-size-7">
                                                    <?= $model->trailer2_brand ?>
                                                </td>
                                                <td width="20%" class="font-size-7 text-center">Государственный номерной знак</td>
                                                <td class="font-size-7">
                                                    <?= $model->trailer2_number ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;">
                                        Гаражный номер
                                    </td>
                                    <td class="font-size-8-bold text-center" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;padding: 1px 2px;">
                                        <?= $model->trailer2_garage_number ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-0 pb-0">
                                        <table class="table no-border" style="margin-bottom: 0!important;">
                                            <tr>
                                                <td width="10%" class="pb-0"></td>
                                                <td width="30%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    подпись ответственного лица
                                                </td>
                                                <td width="15%" class="pb-0 text-center vet-top font-size-8" style="padding-top: 1px;">СВЕДЕНИЯ О ГРУЗЕ</td>
                                                <td width="7%" class="pb-0"></td>
                                                <td width="10%" class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    марка
                                                </td>
                                                <td width="20%" class="pb-0"></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="font-size-8 text-right" style="vertical-align:middle;padding: 1px 4px;"></td>
                                    <td class="font-size-8-bold text-center pb-0" style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black;border-bottom: 2px solid black;padding: 1px 2px;"></td>
                                </tr>
                            </table>

                            <!-- 1. Сведения о грузе -->
                            <table class="table font-size-7" style="margin-top: 10px;margin-bottom: 0px;">
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Краткое наименование</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">С грузом следуют документы</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Вид упаковки</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Количество мест</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Способ определения массы</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Код груза</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Номер контейнера</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Класс груза</td>
                                    <td class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">Масса брутто, т</td>
                                </tr>
                                <tr class="order-packing">
                                    <td width="20%" class="text-center font-size-6">1</td>
                                    <td width="20%" class="text-center font-size-6">2</td>
                                    <td width="10%" class="text-center font-size-6">3</td>
                                    <td width="6%"  class="text-center font-size-6">4</td>
                                    <td width="20%" class="text-center font-size-6">5</td>
                                    <td width="5%"  class="text-center font-size-6">6</td>
                                    <td             class="text-center font-size-6">7</td>
                                    <td             class="text-center font-size-6">8</td>
                                    <td width="5%"  class="text-center font-size-6">9</td>
                                </tr>

                                <?php foreach ($orderArray as $key => $order): ?>
                                    <tr class="order-packing">
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= ($key+1) . '. ' . $order->order->product_title; ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?php
                                            $result = [];
                                            if ($order->with_invoice) {
                                                $result[] = 'Транспортная накладная';
                                            }

                                            if ($order->with_invoice_facture) {
                                                $result[] = 'Счет';
                                            }

                                            if ($order->with_packing_list) {
                                                $result[] = 'Счет-фактура';
                                            }

                                            if (empty($result)) {
                                                echo '---';
                                            }

                                            echo implode(', ', $result);
                                            ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_packaging_type ?? '---' ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_places_count ?? '---' ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= OrderWaybill::$WEIGHT_DETERMINING_METHOD[$order->weight_determining_method] ?? '---' ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_code ?? '---'; ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_code ?? '---'; ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_code ?? '---'; ?>
                                        </td>
                                        <td class="text-center font-size-7" style="padding: 1px 2px">
                                            <?= $order->consignment_gross_weight ?? '---'; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr class="">
                                    <td colspan="5" style="border:none;padding: 1px 2px;"></td>
                                    <td class="text-center font-size-7" style="padding: 1px 2px;border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000;">1</td>
                                    <td colspan="2" style="border:none;padding: 1px 2px;"></td>
                                    <td class="text-center font-size-7" style="padding: 1px 2px;border-left: 1px solid #000;border-right: 1px solid #000;border-bottom: 1px solid #000;"><?=($realMassGross) ?  TextHelper::moneyFormat($realMassGross/1000, 1) : '---'?></td>
                                </tr>
                            </table>
                            <table class="table no-border" style="margin-bottom: 0;position: relative;bottom: 15px;">
                                <tr>
                                    <td width="33%" class="p-0" style="border-right: 1px solid #000;">
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td class="text-left font-size-7" style="padding: 2px!important;">Указанный груз с исправной</td>
                                                <td class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" colspan="2" style="padding: 2px!important;">Количество</td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="text-left font-size-7" style="padding: 2px!important;">пломбой, тарой и упаковкой</td>
                                                <td width="15%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="9%" class="font-size-7" style="padding: 2px!important;">мест</td>
                                                <td class="font-size-7" style="padding: 2px!important;">
                                                    <?php if ($realPlaceCount) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                                                    } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">оттиск</td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">прописью</td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="23%" class="text-left font-size-7" style="padding: 2px!important;">Массой брутто</td>
                                                <td width="50%" class="font-size-7" style="padding: 2px!important;">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross/1000));
                                                    } ?>
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important;">т к перевозке</td>
                                                <td class="font-size-7" style="padding: 2px!important;"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    прописью
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="10%" class="text-left font-size-7" style="padding: 2px!important;">Сдал</td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;">
                                                    <?= $model->release_produced_position ?>
                                                </td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" style="padding: 2px!important;"><?= $model->release_produced_fullname ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    должность
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    подпись
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    расшифровка подписи
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="30%" class="text-left font-size-7" style="padding: 2px!important;">
                                                    Принял <?= mb_strtolower($model->given_out_position) ?>
                                                </td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="30%" class="font-size-7" style="padding: 2px!important;">
                                                    <?= $model->given_out_fio ?>
                                                </td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" style="padding: 2px!important;">место для</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    подпись
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    расшифровка подписи
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="padding-bottom: 1px;padding-top: 1px">
                                                    штампа
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="33%" class="p-0" style="border-right: 1px solid #000;">
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td class="text-left font-size-7" style="padding: 2px!important;">
                                                    Указанный груз с исправной
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" style="padding: 2px!important;" colspan="2">
                                                    Количество
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="40%" class="text-left font-size-7" style="padding: 2px!important;">пломбой, тарой и упаковкой</td>
                                                <td width="15%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="9%" class="font-size-7" style="padding: 2px!important;">мест</td>
                                                <td class="font-size-7" style="padding: 2px!important;">
                                                    <?php if ($realPlaceCount) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                                                    } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    оттиск
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    прописью
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="23%" class="text-left font-size-7" style="padding: 2px!important;">Массой брутто</td>
                                                <td class="font-size-7" style="padding: 2px!important;">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross/1000));
                                                    } ?>
                                                </td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;">т к перевозке</td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    прописью
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td class="text-left font-size-7" style="padding: 2px!important;">
                                                    Сдал <?= $model->given_out_position ?>
                                                </td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" style="padding: 2px!important;"><?= $model->given_out_fio ?></td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    подпись
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    расшифровка подписи
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="12%" class="text-left font-size-7" style="padding: 2px!important;">
                                                    Принял
                                                </td>
                                                <td width="20%"class="font-size-7" style="padding: 2px!important;">
                                                    <?= $model->cargo_accepted_position ?>
                                                </td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="20%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="1%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td class="font-size-7" style="padding: 2px!important;">
                                                    <?= $model->cargo_accepted_fullname ?>
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important;">
                                                    место для
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    должность
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    подпись
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    расшифровка подписи
                                                </td>
                                                <td class="text-center vet-top font-size-6 pb-0" style="padding-bottom: 1px;padding-top: 1px">
                                                    штампа
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="33%" class="p-0">
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td width="25%" class="text-left font-size-7" style="padding: 2px!important;">
                                                    Количество ездок, заездов
                                                </td>
                                                <td width="23%" class="font-size-7" style="padding: 2px!important;"></td>
                                                <td width="34%" class="font-size-7" style="padding: 2px!important;">
                                                    Итого: масса брутто, т
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important;"></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td class="font-size-7" style="padding: 2px!important;">&nbsp;</td>
                                                <td class="font-size-7" style="padding: 2px!important;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="43%" class="text-left font-size-7" style="padding: 2px!important;">
                                                    Отметки о составленных актах
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important; border-bottom: 1px solid #000"></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td class="font-size-7" style="padding: 2px!important;">&nbsp;</td>
                                                <td class="font-size-7" style="padding: 2px!important;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="32%" class="text-left font-size-7" style="padding: 2px!important;">
                                                    Транспортные услуги
                                                </td>
                                                <td class="font-size-7" style="padding: 2px!important; border-bottom: 1px solid #000"></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0;">
                                            <tr>
                                                <td style="padding: 5px">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="100%" style="padding: 2px!important; border-bottom: 1px solid #000;"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table class="table font-size-7" style="margin-bottom: 0;position: relative;bottom: 15px;">
                                <tr class="order-packing">
                                    <td colspan="10" class="text-center font-size-7" style="border-top:1px solid #000;padding: 1px 2px">
                                        Погрузочно-разгрузочные операции
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        операция
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        исполнитель (автовладелец, получатель, отправитель)
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        дополнительные операции (наименование, количество)
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        механизм, грузоподъемность, емкость ковша
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        способ
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        дата (число, месяц), время, ч. мин.
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        время дополнительных операций, мин.
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        подпись ответственного лица
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        ручной, механизированный, наливом, самосвалом
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        код
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        прибытия
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;border-top:1px solid #000;padding: 1px 2px">
                                        убытия
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">10</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">11</td>
                                    <td width="10%" class="text-center font-size-7" style="vertical-align: middle;">12</td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">13</td>
                                    <td width="10%" class="text-center font-size-7" style="vertical-align: middle;">14</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">15</td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">16</td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">17</td>
                                    <td width="9%" class="text-center font-size-7" style="vertical-align: middle;">18</td>
                                    <td width="9%" class="text-center font-size-7" style="vertical-align: middle;">19</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        погрузка
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($model->consignor) ? $model->consignor->getShortName() : $model->invoice->company_name_short ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_operations ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_method_name ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_method_code ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_time_come ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_time_gone ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->loading_time_operations ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;"></td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        разгрузка
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($model->consignee) ? $model->consignee->getShortName() : $model->invoice->contractor->getShortName() ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_operations ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_method_name ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_method_code ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_time_come ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_time_gone ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->unloading_time_operations ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;"></td>
                                </tr>
                            </table>
                            <!-- 3. Прочие сведения -->
                            <table class="table font-size-7" style="position: relative;bottom: 13px;margin-bottom: 0;">
                                <tr class="order-packing">
                                    <td colspan="13" class="text-center font-size-7" style="vertical-align: middle;">
                                        прочие сведения <span style="font-size:7pt">(заполняется организацией, владельцев автотранспорта)</span>
                                    </td>

                                    <td width="15%" rowspan="5" class="va-t" style="position:relative;border: none;">
                                        <?php if ($model->taxation) : ?>
                                            <table class="table no-border" style="margin-bottom: 0;">
                                                <tr>
                                                    <td style="padding-left: 4px">
                                                        Таксировка: <span style="border-bottom:1px solid #000;"><?= $model->taxation ?></span>
                                                    </td>
                                            </table>
                                        <?php else : ?>
                                            <table class="table no-border" style="margin-bottom: 0;">
                                                <tr>
                                                    <td width="40%" style="border:none; padding-left: 4px">Таксировка</td>
                                                    <td style="border-bottom: 1px solid #000;"></td>
                                                </tr>
                                                <tr><td colspan="2" style="padding-top: 5px!important;border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                                <tr><td colspan="2" style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                                <tr><td colspan="2" style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                                <tr><td colspan="2" style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                            </table>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td colspan="5" class="text-center font-size-7" style="vertical-align: middle;">
                                        расстояние перевозки по группам дорог, км
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        код экспедирования груза
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        за транспортные услуги
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        сумма штрафа за неправильное оформление документов, руб. коп.
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        поправочный коэффициент
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        время простоя, ч. мин.
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">всего</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">в гор.</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">I гр.</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">II гр.</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">III гр.</td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">с клиента</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">причитается водителю</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">расценка водителю</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">основной тариф</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">под погрузкой</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">под разгрузкой</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">20</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">21</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">22</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">23</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">24</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">25</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">26</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">27</td>
                                    <td width="10%" class="text-center font-size-7" style="vertical-align: middle;">28</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">29</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">30</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">31</td>
                                    <td width="7%" class="text-center font-size-7" style="vertical-align: middle;">32</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_distance ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_in_city ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_group_1 ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_group_2 ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_group_3 ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_cost ?> руб.
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        -
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_downtime_loading ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= $model->transportation_downtime_unloading ?>
                                    </td>
                                </tr>
                            </table>
                            <!-- 4. Расчёт стоимости -->
                            <table class="table font-size-7" style="position: relative;bottom: 11px;margin-bottom: 0;">
                                <tr class="order-packing">
                                    <td width="10%" rowspan="3" class="text-center font-size-7" style="vertical-align: middle;">
                                        Расчет стоимости
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        За тонны
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        За тонно-км
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Погрузочно- разгрузочные работы, тонн
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Недогрузка автомобиля и прицепа
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Экспеди- рование
                                    </td>
                                    <td colspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Сверхнормативный простой, ч. мин. при
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        За сроч- ность заказа
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        За специ- альный транспорт
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Прочие доплаты
                                    </td>
                                    <td rowspan="2" class="text-center font-size-7" style="vertical-align: middle;">
                                        Всего
                                    </td>
                                    <td width="20%" rowspan="6" style="border: none;">
                                        <table class="table no-border" style="margin-bottom: 0;margin-top: 10px;">
                                            <?php if ($model->taxation) : ?>
                                                <tr><td colspan="4">&nbsp;</td></tr>
                                                <tr><td colspan="4" style="padding-top: 6pt">&nbsp;</td></tr>
                                                <tr><td colspan="4" style="padding-top: 6pt">&nbsp;</td></tr>
                                            <?php else : ?>
                                                <tr><td colspan="4" style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                                <tr><td colspan="4" style="border-bottom: 1px solid #000;">&nbsp;</td></tr>
                                                <tr><td colspan="4" style="border-bottom: 1px solid #000;t">&nbsp;</td></tr>
                                            <?php endif; ?>
                                            <tr>
                                                <td width="36%" style="padding-top: 10px!important;">Таксировщик</td>
                                                <td width="24%" style="border-bottom: 1px solid #000;"></td>
                                                <td width="1%"></td>
                                                <td style="border-bottom: 1px solid #000;"><?= $model->taximaster ?></td>
                                            </tr>
                                            <tr>
                                                <td width="35%"></td>
                                                <td width="25%" class="tip">подпись</td>
                                                <td width="1%"></td>
                                                <td class="tip">расшифровка подписи</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">погрузке</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">разгрузке</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">33</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">34</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">35</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">36</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">37</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">38</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">39</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">40</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">41</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">42</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">43</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">Выполнено</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($hasMass) ? TextHelper::moneyFormat($realMassNet/1000, 1) : '-' ?>
                                    </td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($hasMass && $model->transportation_distance > 0) ? TextHelper::moneyFormat(floor($realMassNet/1000 * $model->transportation_distance), 0) : '-' ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($hasMass) ? TextHelper::moneyFormat($realMassNet/1000, 1) : '-' ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td width="8%" class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        Расценка, руб.коп
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($hasMass && $model->transportation_cost > 0) ? TextHelper::moneyFormat(floor($model->transportation_cost / $realMassNet/1000 * $model->transportation_distance), 2) : '-' ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        К оплате, руб.коп
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($model->transportation_cost > 0) ? TextHelper::moneyFormat($model->transportation_cost, 2) : '-' ?>
                                    </td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">-</td>
                                    <td class="text-center font-size-7" style="vertical-align: middle;">
                                        <?= ($model->transportation_cost > 0) ? TextHelper::moneyFormat($model->transportation_cost, 2) : '-' ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
