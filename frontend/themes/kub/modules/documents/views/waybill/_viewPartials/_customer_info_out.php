<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Waybill;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\themes\kub\helpers\Icon;
use frontend\themes\kub\modules\documents\widgets\DocumentFileScanWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use php_rutils\RUtils;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var \yii\web\View $this */
/* @var Waybill $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$proxyDateFormatted = RUtils::dt()->ruStrFTime([
    'date' => $model->proxy_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$basisDocumentDate = RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$productionType = explode(', ', $model->invoice->production_type);

$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$model->invoice->fullNumber}&{$model->invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#waybill-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}
$model->consignor_id .= '';
$model->consignee_id .= '';
if (empty($model->delivery_time)) {
    $model->delivery_time = date('d.m.Y');
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                <span>
                    <span>
                        <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                    </span>
                </span>
                    <span>№</span>
                </div>
                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= Html::activeTextInput($model, 'document_additional_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                            'placeholder' => 'доп. номер',
                            'style' => 'display: inline;',
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <!-- document_date -->
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-6">
            <label class="label">Грузоотправитель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignor_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $model->consignorArray,
                'options' => [
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Доверенность</label>
                    <?= Html::activeTextInput($model, 'proxy_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'номер',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">От</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'proxy_date', [
                            'class' => 'form-control date-picker',
                            'value' => DateHelper::format($model->proxy_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Груз к перевозке принял</label>
                    <?= Html::activeTextInput($model, 'given_out_fio', [
                        'maxlength' => true,
                        'class' => 'form-control input-editable-field ',
                        'placeholder' => '',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">Должность</label>
                    <?= Html::activeTextInput($model, 'given_out_position', [
                        'maxlength' => true,
                        'class' => 'form-control input-editable-field ',
                        'placeholder' => '',
                    ]); ?>
                </div>
                <div class="col-6">
                    <label class="label">Удостоверение</label>
                    <?= Html::activeTextInput($model, 'driver_license', [
                        'maxlength' => true,
                        'class' => 'form-control input-editable-field ',
                        'placeholder' => 'Удостоверение',
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Отпуск груза разрешил</label>
                    <?= Html::activeTextInput($model, 'release_allowed_position', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Должность',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'release_allowed_fullname', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'ФИО',
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Отпуск груза произвёл</label>
                    <?= Html::activeTextInput($model, 'release_produced_position', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Должность',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'release_produced_fullname', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'ФИО',
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label"><?= $message->get(Message::CONTRACTOR) ?></label>
            <div>
                <?= Html::a($model->invoice->contractor_name_short, [
                    '/contractor/view',
                    'type' => $model->invoice->contractor->type,
                    'id' => $model->invoice->contractor->id,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Грузополучатель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignee_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $model->consignorArray,
                'options' => [
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Груз получил</label>
                    <?= Html::activeTextInput($model, 'cargo_accepted_position', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Должность',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'cargo_accepted_fullname', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'ФИО',
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Срок доставки груза</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'delivery_time', [
                            'class' => 'form-control date-picker',
                            'value' => DateHelper::format($model->delivery_time, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Путевой лист №</label>
                    <?= Html::activeTextInput($model, 'waybill_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'номер',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">От</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'waybill_date', [
                            'class' => 'form-control date-picker',
                            'value' => DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Организация-перевозчик</label>
            <?= Html::activeTextInput($model, 'organization', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => 'Наименование, реквизиты и банковские реквизиты',
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-6">
                    <label class="label">Автомобиль</label>
                    <?= Html::activeTextInput($model, 'car', [
                        'maxlength' => true,
                        'class' => 'form-control input-editable-field ',
                        'placeholder' => 'Марка и модель автомобиля',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">Гос. номерной знак</label>
                    <?= Html::activeTextInput($model, 'car_number', [
                        'maxlength' => true,
                        'class' => 'form-control input-editable-field ',
                        'placeholder' => 'Гос. регистрационный знак автомобиля',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">Лицензионная карточка</label>
                    <?= Select2::widget([
                        'model' => $model,
                        'attribute' => 'license_card',
                        'data' => Waybill::$LICENSE_CARD_TYPES,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Вид перевозки</label>
            <?= Html::activeTextInput($model, 'transportation_type', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => 'Вид перевозки'
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Маршрут</label>
            <?= Html::activeTextInput($model, 'transportation_route', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => 'Маршрут'
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-2">
                    <label class="label">Прицеп 1</label>
                    <?= Html::activeTextInput($model, 'trailer1_brand', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Марка',
                    ]); ?>
                </div>
                <div class="col-2">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'trailer1_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Гос. номер',
                    ]); ?>
                </div>
                <div class="col-2">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'trailer1_garage_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Гаражный номер',
                    ]); ?>
                </div>
                <div class="col-2">
                    <label class="label">Прицеп 2</label>
                    <?= Html::activeTextInput($model, 'trailer2_brand', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Марка',
                    ]); ?>
                </div>
                <div class="col-2">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'trailer2_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Гос. номер',
                    ]); ?>
                </div>
                <div class="col-2">
                    <label class="label" style="height: 14px;"></label>
                    <?= Html::activeTextInput($model, 'trailer2_garage_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'Гаражный номер',
                    ]); ?>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-4">
            <?= DocumentFileScanWidget::widget([
                'model' => $model,
                'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
            ]); ?>
        </div>
    </div>
</div>
