<?php

use \yii\helpers\Url;
use \common\components\date\DateHelper;
use \common\models\document\OrderWaybill;
use \common\components\TextHelper;
use \common\models\product\Product;
use \php_rutils\RUtils;
use \common\models\company\CompanyType;
use common\models\document\Waybill;
use \common\models\product\ProductUnit;
use \frontend\modules\documents\components\Message;

/* @var $model \common\models\document\Waybill */
/* @var $order \common\models\document\OrderWaybill */

$company = $model->invoice->company;
$precision = $model->invoice->price_precision;
$orderQuery = OrderWaybill::find()->where(['waybill_id' => $model->id]);
$orderArray = $orderQuery->all();
$realMassGross = $model->getRealMassGross($orderArray);
$realMassNet = $model->getRealMassNet($orderArray);
$realPlaceCount = $model->getRealPlaceCount($orderArray);
$hasMass = $realMassGross && $realMassNet;
$addStamp = (boolean) $model->add_stamp;
?>
<style>

    .ver-top {
        vertical-align: top;
    }

    .font-size-6 {
        font-size: 9px
    }

    .font-size-7 {
        font-size: 10px
    }

    .font-size-8 {
        font-size: 11.5px
    }

    .font-size-8-bold {
        font-size: 11.5px;
        font-weight: bold
    }

    .table.no-border, .customer-info.no-border {
        border: none !important;
    }

    .table.no-border td {
        border: none;
    }

    .order-packing .page-number {
        font-family: Arial, sans-serif;
        font-size: 8pt;
        font-style: italic;
        text-align: right;
    }

    .order-packing td {
        border: 1px solid #000000;
        padding: 2px 2px !important;
        border-color: #000 !important
    }

    .ver-bottom {
        vertical-align: bottom;
    !important;
    }

    #print_layer {
        height: 230px;
        background-image: url('<?= $addStamp ? $printLink : null ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }

    #signature1_layer {
        background-image: url('<?= $addStamp ? $signatureLink : null ?>');
        background-position: 30%<?= $model->signed_by_employee_id ? '41px' : '56px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }

    #signature2_layer {
        background-image: url('<?= $addStamp ? $signatureLink : null ?>');
        background-position: 30%<?= $model->signed_by_employee_id ? '85px' : '93px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
</style>

<div class="over-hidden m-size-div container-first-account-table no_min_h pad0"
     style="min-width: 520px; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3">
            <div class="no-border">
                <div class="pad0" style="position: relative;">
                    <div class="caption">
                        <div>
                            <div class="portlet customer-info no-border" style="margin-bottom:0px">
                                <div class="portlet-title pad0" style="position: relative;">
                                    <div class="caption">
                                        <div style="padding-right: 60px;">
                                            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                                            № <span class="editable-field"><?= $model->fullNumber; ?></span>
                                            от
                                            <span id="document_date_item" class="editable-field"
                                                  data-tooltip-content="#tooltip_document_date"><?= $dateFormatted; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table class="table no-border" style="margin-bottom: 0 !important;">
                                <tr>
                                    <td colspan="5">
                                    </td>
                                    <td width="6%" class="font-size-7 text-center"
                                        style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">
                                        Коды
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                    <td colspan="3" class="font-size-8 text-right">Форма по ОКУД</td>
                                    <td class="font-size-8-bold text-center"
                                        style="vertical-align:middle;border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">
                                        0345009
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="font-size-8-bold text-right" style="width:44%">ТОВАРНО-ТРАНСПОРТНАЯ
                                        НАКЛАДНАЯ
                                    </td>
                                    <td style="border-bottom: 1px solid black;"></td>
                                    <td class="font-size-8-bold text-right" colspan="2"
                                        style="vertical-align:middle; padding: 1px 8px; border-bottom: 1px solid #ffffff">
                                        №
                                    </td>
                                    <td class="font-size-8-bold text-center"
                                        style="vertical-align:middle; padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                                        <?= $model->getFullNumber(); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1"></td>
                                    <td></td>
                                    <td colspan="3">
                                        <table class="no-border" style="width:100%;margin:0;padding:0;">
                                            <tr>
                                                <td class="font-size-6 text-center ver-top" width="75%">серия</td>
                                                <td class="font-size-8 text-right" width="24%">Дата составления</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="border-right: 2px solid black;border-left: 2px solid black; border-bottom: 1px solid black; padding:0">
                                        <table style="height: 32px;width: 100%;">
                                            <tr>
                                                <td class="text-center"
                                                    style="width: 33%;border-right: 1px solid #000;padding:0"><?= date('d', strtotime($model->document_date)) ?></td>
                                                <td class="text-center"
                                                    style="width: 33%;border-right: 1px solid #000;padding:0"><?= date('m', strtotime($model->document_date)) ?></td>
                                                <td class="text-center"><?= date('y', strtotime($model->document_date)) ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="12%" class="text-right ver-bottom m-l font-size-8">Грузоотправитель</td>
                                    <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                                        <?php if ($model->consignor) {
                                            echo $model->consignor->getTitle(true);
                                            echo ($model->consignor->legal_address) ? ", {$model->consignor->legal_address}" : "";
                                            echo ($model->consignor->getRealContactPhone()) ? ", {$model->consignor->getRealContactPhone()}" : "";
                                        } else {
                                            echo $model->invoice->company_name_short;
                                            echo ($model->invoice->company_address_legal_full) ? ", {$model->invoice->company_address_legal_full}" : "";
                                            echo ($model->invoice->company_phone) ? ", {$model->invoice->company_phone}" : "";
                                        }
                                        ?>
                                    </td>
                                    <td colspan="2" class="font-size-8 text-right"
                                        style="vertical-align:middle; padding: 1px 4px;">по ОКПО
                                    </td>
                                    <td class="font-size-8-bold text-center"
                                        style="vertical-align:middle; padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                                        <?= ($model->consignor) ? $model->consignor->okpo : $model->invoice->company->okpo; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="font-size-6 text-center ver-top m-l"
                                        style="padding: 1px 2px">полное наименование организации, адрес, номер телефона
                                    </td>
                                    <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
                                </tr>

                                <tr>
                                    <td width="12%" class="text-right ver-bottom m-l font-size-8">Грузополучатель</td>
                                    <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                                        <?php if ($model->consignee) {
                                            if ($model->contractor_address == Waybill::CONTRACTOR_ADDRESS_LEGAL) {
                                                $address = $model->consignee->legal_address;
                                            } else {
                                                $address = $model->consignee->actual_address;
                                            }
                                            echo $model->consignee->getTitle(true);
                                            echo ($address) ? ", {$address}" : "";
                                            echo ($model->consignee->getRealContactPhone()) ? ", {$model->consignee->getRealContactPhone()}" : "";
                                        } else {
                                            if ($model->contractor_address == Waybill::CONTRACTOR_ADDRESS_LEGAL) {
                                                $address = $model->invoice->contractor->legal_address;
                                            } else {
                                                $address = $model->invoice->contractor->actual_address;
                                            }
                                            echo $model->invoice->contractor_name_short;
                                            echo ($address) ? ", {$address}" : "";
                                            echo $model->invoice->contractor->getRealContactPhone() ? ", {$model->invoice->contractor->getRealContactPhone()}" : "";
                                        } ?>
                                    </td>
                                    <td colspan="2" class="font-size-8 text-right"
                                        style="vertical-align:middle; padding: 1px 4px;">по ОКПО
                                    </td>
                                    <td class="font-size-8-bold text-center"
                                        style="vertical-align:middle; padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                                        <?= ($model->consignee) ? $model->consignee->okpo : $model->invoice->contractor->okpo; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="font-size-6 text-center ver-top m-l"
                                        style="padding: 1px 2px">полное наименование организации, адрес, номер телефона
                                    </td>
                                    <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
                                </tr>
                                <tr>
                                    <td width="12%" class="text-right ver-bottom m-l font-size-8">Плательщик</td>
                                    <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                                        <?= $model->invoice->contractor_name_short,
                                        $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                                    </td>
                                    <td colspan="2" class="font-size-8 text-right "
                                        style="vertical-align:middle; border-bottom: 1px solid #fff;padding: 1px 4px">по
                                        ОКПО
                                    </td>
                                    <td class="font-size-8-bold text-center"
                                        style="vertical-align:middle; padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 2px solid black;">
                                        <?= $model->invoice->contractor->okpo; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="font-size-6 text-center ver-top m-l"
                                        style="padding: 1px 2px">полное наименование организации, адрес, номер телефона,
                                        банковские реквизиты
                                    </td>
                                    <td width="1%"></td>
                                    <td width="4.8%" style=""></td>
                                    <td style="padding: 1px 2px;"></td>
                                </tr>

                            </table>
                            <table class="table no-border" style="margin-bottom: 0 !important; margin-top:10px;">
                                <tr>
                                    <td width="100%" class="text-center font-size-8">I. ТОВАРНЫЙ РАЗДЕЛ (заполняется
                                        грузоотправителем)
                                    </td>
                                </tr>
                            </table>
                            <table class="table font-size-7" style="margin-bottom: 0 !important;">
                                <tr class="order-packing">
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Код продукции (номенклатурный
                                        номер)
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Номер прейскуранта и
                                        дополнения к нему
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Артикул или номер по
                                        прейскуранту
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Количество
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Цена, руб. коп
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Наименование продукции,
                                        товара (груза), ТУ, марка, размер, сорт
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Единица измерения
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Вид упаковки
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Количество мест
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Масса, т.
                                    </td>
                                    <td width="8%" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Сумма, руб. коп.
                                    </td>
                                    <td width="" class="text-center font-size-7"
                                        style="border-top:1px solid #000;padding: 1px 2px">Порядковый номер записи по
                                        складской картотеке (грузоотправителю, грузополучателю)
                                    </td>
                                </tr>
                                <tr class="order-packing">
                                    <td class="text-center  font-size-6">1</td>
                                    <td class="text-center  font-size-6">2</td>
                                    <td class="text-center  font-size-6">3</td>
                                    <td class="text-center  font-size-6">4</td>
                                    <td class="text-center  font-size-6">5</td>
                                    <td class="text-center  font-size-6">6</td>
                                    <td class="text-center  font-size-6">7</td>
                                    <td class="text-center  font-size-6">8</td>
                                    <td class="text-center  font-size-6">9</td>
                                    <td class="text-center  font-size-6">10</td>
                                    <td class="text-center  font-size-6">11</td>
                                    <td class="text-center  font-size-6">12</td>
                                </tr>
                                <?php
                                /** @var OrderWaybill $order */
                                $orderQuery = OrderWaybill::find()->where(['waybill_id' => $model->id]);
                                $orderArray = $orderQuery->all();
                                foreach ($orderArray as $key => $order):
                                    $priceWithNds = $order->priceWithNds;
                                    $amountWithNds = $order->amountWithNds;
                                    if ($model->invoice->hasNds) {
                                        $priceNoNds = $order->priceNoNds;
                                        $amountNoNds = $order->amountNoNds;
                                        $TaxRateName = $order->order->saleTaxRate->name;
                                        $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
                                    } else {
                                        $priceNoNds = $order->priceWithNds;
                                        $amountNoNds = $order->amountWithNds;
                                        $TaxRateName = 'Без НДС';
                                        $ndsAmount = Product::DEFAULT_VALUE;
                                    }
                                    if ($hasMass) {
                                        $weight_in_tonn = ($order->order->unit_id == ProductUnit::UNIT_KG) ? $order->order->product->weight / 1000 : $order->order->product->weight;
                                    } else {
                                        $weight_in_tonn = '---';
                                    }
                                    ?>
                                    <tr class="order-packing">
                                        <td data-column="1" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= $order->order->product_code; ?></td>
                                        <td data-column="2" class="text-center font-size-7" style="padding: 1px 2px">
                                            -
                                        </td>
                                        <td data-column="3" class="text-center font-size-7" style="padding: 1px 2px">
                                            -
                                        </td>
                                        <td data-column="4" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= $order->quantity; ?></td>
                                        <td data-column="5" class="text-right  font-size-7"
                                            style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($priceWithNds, $precision); ?></td>
                                        <td data-column="6" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= $order->order->product_title; ?></td>
                                        <td data-column="7" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                                        <td data-column="8" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= $order->order->box_type; ?></td>
                                        <td data-column="9" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= ($realPlaceCount) ? $order->quantity * $order->order->place_count : '---'; ?></td>
                                        <td data-column="10" class="text-center font-size-7"
                                            style="padding: 1px 2px"><?= ($hasMass) ? TextHelper::moneyFormat($order->quantity * $weight_in_tonn, 1) : '---' ?></td>
                                        <td data-column="11" class="text-right  font-size-7"
                                            style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, $precision); ?></td>
                                        <td data-column="12" class="text-center font-size-7"
                                            style="padding: 1px 2px"></td>
                                    </tr>
                                <?php endforeach; ?>

                                <tr>
                                    <td colspan="9" rowspan="4"
                                        style="vertical-align: top; border-left:none; border-bottom:none;">

                                        <table class="table no-border" style="margin: 0 !important;">
                                            <tr>
                                                <td width="24%" class="ver-bottom text-left font-size-7"
                                                    style="padding-left: 0;">Товарная накладная имеет продолжение на
                                                </td>
                                                <td width="20%" class="text-center"
                                                    style="border-bottom: 1px solid black;"> -
                                                </td>
                                                <td width="13%" class="ver-bottom text-left font-size-7; " style="">
                                                    листах, на бланках №
                                                </td>
                                                <td width="10%" class="text-center"
                                                    style="border-bottom: 1px solid black;"> -
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin: 0 !important;">
                                            <tr>
                                                <td width="8%" class="ver-bottom text-left font-size-7"
                                                    style=" padding-left: 0;">и содержит
                                                </td>
                                                <td width="28%" class="ver-bottom font-size-7"
                                                    style="border-bottom: 1px solid black;">
                                                    <?= TextHelper::mb_ucfirst(RUtils::numeral()->getInWords(count($orderArray))); ?>
                                                </td>
                                                <td width="15%" style="border-bottom: 1px solid black;"></td>
                                                <td width="25%" class="ver-bottom font-size-7"
                                                    style=" padding-left: 6px">порядковых номеров записей
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin: 0 !important;">
                                            <tr>
                                                <td width="7%"></td>
                                                <td width="42%" class="font-size-6 text-center vet-top"
                                                    style="padding-bottom: 1px; padding-top: 1px">(прописью)
                                                </td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border"
                                               style="margin-bottom: 0 !important;margin-top: -5px; width:95%;">
                                            <tr>
                                                <td class="font-size-7" style="padding-left:0">Всего наименований</td>
                                                <td style="border-bottom: 1px solid black;" class="font-size-7">
                                                    <?= TextHelper::mb_ucfirst(RUtils::numeral()->getInWords(count($orderArray))); ?>
                                                </td>
                                                <td style="" class="font-size-7 text-right">Масса груза (нетто)</td>
                                                <td style="border-bottom: 1px solid black;" class="font-size-7">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassNet / 1000));
                                                    } ?></td>
                                                <td width="1%"></td>
                                                <td style="border-bottom: 1px solid black;border-right: 2px solid black;border-top: 2px solid black;border-left: 2px solid black;"
                                                    class="font-size-7 text-center">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::moneyFormat($realMassNet / 1000, 1);
                                                    } ?></td>
                                                <td style="border-bottom: 1px solid black;" class="font-size-7"></td>
                                                <td width="1%">Т</td>
                                                <td style="border-bottom: 1px solid black;padding-bottom: 1px; padding-top: 1px"
                                                    class="font-size-7"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="font-size-6 text-center vet-top m-l"
                                                    style="padding-bottom: 1px; padding-top: 1px">(прописью)
                                                </td>
                                                <td></td>
                                                <td class="font-size-6 text-center vet-top m-l"
                                                    style="padding-bottom: 1px; padding-top: 1px">(прописью)
                                                </td>
                                                <td></td>
                                                <td style="border-left: 2px solid black; border-right: 2px solid black"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td width="14%" class="font-size-7" style="padding-left:0">Всего мест
                                                </td>
                                                <td width="23%" style="border-bottom: 1px solid black;"
                                                    class="font-size-7">
                                                    <?php if ($realPlaceCount) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realPlaceCount, RUtils::NEUTER));
                                                    } ?>
                                                </td>
                                                <td width="17%" class="font-size-7 text-right" style="">Масса груза
                                                    (брутто)
                                                </td>
                                                <td width="27%" style="border-bottom: 1px solid black;"
                                                    class="font-size-7">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross / 1000));
                                                    } ?>
                                                </td>
                                                <td width="1%" style=""></td>
                                                <td width="7.5%"
                                                    style="border-left: 2px solid black; border-bottom: 2px solid black; border-right: 2px solid black;"
                                                    class="font-size-7 text-center">
                                                    <?php if ($hasMass) {
                                                        echo TextHelper::numberFormat($realMassGross / 1000, 2);
                                                    } ?>
                                                </td>
                                                <td width="3%" style="border-bottom: 1px solid black;"
                                                    class="font-size-7"></td>
                                                <td width="1%" style="">Т</td>
                                                <td width="3%" style="border-bottom: 1px solid black;"
                                                    class="font-size-7"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="font-size-6 text-center vet-top m-l"
                                                    style="padding-bottom: 1px; padding-top: 1px">(прописью)
                                                </td>
                                                <td></td>
                                                <td class="font-size-6 text-center vet-top m-l"
                                                    style="padding-bottom: 1px; padding-top: 1px">(прописью)
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </table>

                                    </td>
                                    <td class="font-size-7 text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;">
                                        Наценка, %
                                    </td>
                                    <td class="text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;"> -
                                    </td>
                                    <td style="border:none; padding:2px!important;"></td>
                                </tr>
                                <tr>
                                    <td class="font-size-7 text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;">
                                        Складские или транспортные расходы
                                    </td>
                                    <td class="text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;"> -
                                    </td>
                                    <td style="border:none; padding:2px!important;"></td>
                                </tr>
                                <tr>
                                    <td class="font-size-7 text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;">
                                        &nbsp;
                                    </td>
                                    <td style="border:1px solid #000; padding:2px!important; vertical-align: middle;"></td>
                                    <td style="border:none; padding:2px!important;"></td>
                                </tr>
                                <tr>
                                    <td class="font-size-7 text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;">
                                        Всего к оплате
                                    </td>
                                    <td class="text-center"
                                        style="border:1px solid #000; padding:2px!important; vertical-align: middle;"> <?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, $precision) ?></td>
                                    <td style="border:none; padding:2px!important;"></td>
                                </tr>
                            </table>

                            <table class="table no-border" style="margin-bottom: 0 !important;position: relative;top: 1px;">
                                <tr>

                                </tr>
                                <tr>

                                    <td width="20%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px;">
                                        Приложение (паспорт, сертификаты и т.п.) на
                                    </td>
                                    <td width="1%" style="border-bottom: 1px solid #000;"></td>
                                    <td width="16%" class="font-size-7 text-center"
                                        style="padding-bottom: 1px; padding-top: 1px;border-bottom: 1px solid #000;"> -
                                    </td>
                                    <td width="1%"></td>
                                    <td width="4%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px">
                                        листах
                                    </td>
                                    <td width="4%" class="font-size-7 text-center"
                                        style="padding-bottom: 1px; padding-top: 1px;border-bottom: 1px solid #000;"> -
                                    </td>
                                    <td width="5.2%" class="font-size-7"
                                        style="padding-bottom: 1px; padding-top: 1px;"></td>

                                    <td width="9%" style="padding: 0 0 0 10px;border-left: 1px solid black"
                                        class="font-size-7">По доверенности №
                                    </td>
                                    <td class="font-size-7" style="padding:0">
                                        <table class="m-l" style="width: 100%;">
                                            <tr>
                                                <td width="25%" class="font-size-7"
                                                    style="border-bottom: 1px solid #000">
                                                    <?= $model->proxy_number ?: '' ?>
                                                </td>
                                                <td width="8%" class="font-size-7" style="padding-left:5px">
                                                    от
                                                    "&nbsp;<?= $model->proxy_date ? DateHelper::format($model->proxy_date, 'd', DateHelper::FORMAT_DATE) : '&nbsp;&nbsp;'; ?>
                                                    &nbsp;"
                                                </td>
                                                <td width="25%" class="font-size-7 text-center"
                                                    style="border-bottom: 1px solid #000">
                                                    <?= ($model->proxy_date) ? \php_rutils\RUtils::dt()->ruStrFTime(['date' => $model->proxy_date, 'format' => 'F', 'monthInflected' => true]) : ''; ?>
                                                </td>
                                                <td width="25%" class="font-size-7">
                                                    <?= ($model->proxy_date) ? DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE) : '20 &nbsp;&nbsp;&nbsp;&nbsp;' ?>
                                                    г.
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                            <div id="print_layer">
                                <div id="signature1_layer">
                                    <div id="signature2_layer">
                                        <table class="table no-border" style="margin-bottom: 0 !important;">
                                            <tr>
                                                <td></td>
                                                <td colspan="4" class="text-center vet-top font-size-6"
                                                    style="padding-left:5%; padding-bottom: 1px; padding-top: 1px">
                                                    (прописью)
                                                </td>
                                                <td style="padding-bottom: 1px; padding-top: 1px"></td>
                                                <td width="8%" class="font-size-7"
                                                    style="border-left: 1px solid black;padding: 0 0 0 10px;vertical-align: middle;">
                                                    выданной
                                                </td>
                                                <td colspan="5" style="border-bottom: 1px solid #000;"
                                                    class="font-size-7">
                                                    <?= ($model->consignor) ? $model->consignor->shortName : $model->invoice->company_name_short ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" class="font-size-7" style="border-bottom: none;">
                                                    Всего отпущено на сумму
                                                </td>
                                                <td colspan="5" class="font-size-7"
                                                    style="border-bottom: 1px solid #000;">
                                                    <?= TextHelper::amountToWords($model->totalAmountWithNds / 100); ?>
                                                </td>
                                                <td width="12.5%" class="font-size-7"
                                                    style="border-left: 1px solid black;padding: 0 0 0 10px; vertical-align: bottom;">
                                                    Груз к перевозке принял
                                                </td>
                                                <td width="10%"
                                                    class="font-size-7 text-center"><?= $model->given_out_position ?></td>
                                                <td width="1%"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td class="font-size-7 text-center"><?= $model->given_out_fio ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="1" class="text-center vet-top font-size-6"
                                                    style="padding-bottom: 0; padding-top: 0"></td>
                                                <td colspan="5" class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 0; padding-top: 0">
                                                    (прописью)
                                                </td>
                                                <td style="border-left: 1px solid black"></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (должность)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (подпись)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (расшифровка подписи)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="14%" class="ver-bottom font-size-7"
                                                    style="padding-bottom: 1px; padding-top: 8px">
                                                    Отпуск груза разрешил

                                                </td>
                                                <td width="12%" class="font-size-7"
                                                    style="padding-bottom: 1px; padding-top: 1px ">
                                                    <?= $model->release_allowed_position ?>
                                                </td>

                                                <td width="1%" style="padding-bottom: 1px;padding-top: 1px"></td>
                                                <td width="10%" style="padding-bottom: 1px;padding-top: 1px"></td>
                                                <td width="1%" style="padding-bottom: 1px;padding-top: 1px"></td>
                                                <td width="14%" class="ver-bottom font-size-7 text-center"
                                                    style="padding-bottom: 1px; padding-top: 1px">
                                                    <?= $model->release_allowed_fullname ?>
                                                </td>
                                                <td width="12.5%"
                                                    style="border-left: 1px solid black;padding: 0 0 0 10px;"
                                                    class="font-size-7">(При личном приеме товара по количеству и
                                                    ассортименту)
                                                </td>
                                                <td colspan="5"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">
                                                    (должность)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">
                                                    (подпись)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">
                                                    (расшифровка подписи)
                                                </td>
                                                <td style="border-left: 1px solid black"></td>
                                                <td colspan="5" style=""></td>
                                            </tr>
                                        </table>

                                        <table class="table no-border" style="margin-bottom: 0 !important;">
                                            <tr>
                                                <td width="14%" class="font-size-7"
                                                    style="padding-bottom: 1px;padding-top: 1px">Главный (старший)
                                                    бухгалтер
                                                </td>
                                                <td width="12%"></td>
                                                <td width="1%"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td width="14%" class="font-size-7 text-center"
                                                    style="padding-bottom: 1px;padding-top: 1px">
                                                    <?php if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP): ?>
                                                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                                                        <?php if ($model->signed_by_employee_id) : ?>
                                                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                                        <?php endif ?>
                                                    <?php endif ?>
                                                </td>
                                                <td width="12.5%" class="font-size-7"
                                                    style="border-left: 1px solid black;padding: 0 0 0 10px;"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (подпись)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (расшифровка подписи)
                                                </td>
                                                <td style="border-left: 1px solid black;"></td>
                                                <td colspan="5" class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px"></td>
                                            </tr>
                                        </table>
                                        <table class="table no-border" style="margin-bottom: 0 !important;">
                                            <tr>
                                                <td width="14%" class="font-size-7"
                                                    style="padding-bottom: 1px;padding-top: 1px">Отпуск груза произвел
                                                </td>
                                                <td width="12%"
                                                    class="font-size-7 text-center"><?= $model->release_produced_position ?></td>
                                                <td width="1%"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td width="14%"
                                                    class="font-size-7 text-center"><?= $model->release_produced_fullname ?></td>
                                                <td width="12.5%"
                                                    style="border-left: 1px solid black;padding: 0 0 0 10px;"
                                                    class="font-size-7">Груз получил
                                                </td>
                                                <td width="10%"
                                                    class="font-size-7 text-center"><?= $model->cargo_accepted_position ?></td>
                                                <td width="1%"></td>
                                                <td width="10%"></td>
                                                <td width="1%"></td>
                                                <td class="font-size-7 text-center"><?= $model->cargo_accepted_fullname ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (должность)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (подпись)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (расшифровка подписи)
                                                </td>
                                                <td class="vet-top font-size-7"
                                                    style="margin-top: 0 !important;border-left: 1px solid black;padding: 0 0 0 10px;">
                                                    грузополучатель
                                                </td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (должность)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (подпись)
                                                </td>
                                                <td></td>
                                                <td class="text-center vet-top font-size-6"
                                                    style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                                    (расшифровка подписи)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right font-size-7">М.П.</td>
                                                <td class="text-right font-size-7">
                                                    "&nbsp;<?= DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE); ?>
                                                    &nbsp;"
                                                </td>
                                                <td></td>
                                                <td class="text-center font-size-7"
                                                    style="border-bottom: 1px solid black">
                                                    <?= RUtils::dt()->ruStrFTime([
                                                        'date' => $model->document_date,
                                                        'format' => 'F', // month in word
                                                        'monthInflected' => true,
                                                    ]);; ?>
                                                </td>
                                                <td></td>
                                                <td class="text-left font-size-7"><?= DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE); ?>
                                                    г.
                                                </td>
                                                <td class="text-right font-size-7"
                                                    style="border-left: 1px solid black"></td>
                                                <td class="text-right font-size-7"></td>
                                                <td></td>
                                                <td style=""></td>
                                                <td></td>
                                                <td class="font-size-7"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
