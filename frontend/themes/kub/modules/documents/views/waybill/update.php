<?php

use common\models\document\OrderWaybill;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\DocConverter;
use yii\bootstrap4\Nav;

/* @var $this yii\web\View */
/* @var $model common\models\document\Waybill */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$plus = 0;
$dateFormatted = RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = OrderWaybill::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$precision = $model->invoice->price_precision;
$cancelUrl = Url::to(['view', 'type' => $ioType, 'id' => $model->id]);

$script = <<< JS

if('$plus' === '1'){
  $('.testclass').hide();
}
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, $this::POS_READY);
?>

<div class="form">
    <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-waybill',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>

    <?= $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
        'model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
    ]); ?>

    <?= Nav::widget([
        'id' => 'contractor-menu',
        'items' => [
            [
                'label' => 'I. Товарный раздел',
                'url' => '#tab1',
                'options' => [
                    'class' => 'col-sm-2',
                ],
                'linkOptions' => [
                    'data-toggle' => 'tab',
                    'class' => 'active',
                ],
            ],
            [
                'label' => 'II. Транспортный раздел',
                'url' => '#tab2',
                'options' => [
                    'class' => 'col-sm-3',
                    'style' => 'max-width: 20%;',
                ],
                'linkOptions' => [
                    'data-toggle' => 'tab',
                ],
            ]
        ],
        'options' => ['class' => 'nav-form-tabs nav nav-tabs'],
    ]); ?>
    <div class="wrap">
        <div class="tab-content" style="margin-top: 25px">
            <div id="tab1" class="tab-pane products-tab active">
                <div class="portlet wide_table overflow-x">
                    <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                        'model' => $model,
                        'precision' => $precision,
                    ]); ?>
                </div>
            </div>
            <div id="tab2" class="tab-pane transport-tab">
                <div class="portlet wide_table overflow-x">
                    <?= $this->render('_viewPartials/_transport_' . Documents::$ioTypeToUrl[$ioType], [
                        'model' => $model,
                        'precision' => $precision,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                    'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', $cancelUrl ? : ['index', 'type' => $model->type], [
                    'class' => 'button-width button-clr button-regular button-hover-grey',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
