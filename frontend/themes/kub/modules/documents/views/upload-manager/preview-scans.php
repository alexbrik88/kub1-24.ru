<?php $this->title = 'Просмотр сканов'; ?>
<?= $this->render('@frontend/themes/kub/views/layouts/_widget_side_menu_preview_scan', [
        'isFullScreen' => true,
        'filesIds' => $filesIds,
        'startFileId' => $startFileId
    ]);
?>