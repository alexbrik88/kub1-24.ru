<?php
use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use devgroup\dropzone\DropZone;

/** @var \frontend\modules\documents\models\ScanUploadEmail $scanUploadEmail */

$this->title = 'Распознавание';
DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="page-head d-flex flex-wrap align-items-center">
    <div class="column pr-0 pl-0">
        <h4 class="mb-2"><?= $this->title ?></h4>
    </div>
    <div class="column pr-2">
        <button class="button-list button-hover-transparent button-clr mb-2 collapsed" type="button" data-toggle="collapse" href="#helpCollapse" aria-expanded="false" aria-controls="helpCollapse">
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#book"></use>
            </svg>
        </button>
    </div>
    <button class="button-regular button-regular_red button-width ml-auto create-scan-btn" data-url="<?= Url::to(['create-scan']) ?>">
        <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
        <span>Загрузить</span>
    </button>
</div>

<div class="collapse" id="helpCollapse">
    <?= $this->render('_partial/_help_panel_uploads') ?>
</div>

<div class="upload-header">
    <div class="row row-pad-5">

        <div class="col-4 col-pad-5">
            <div class="wrap upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>
                    по Email
                </div>
                <div class="ub-body">
                    <img width="143" src="/img/bg/upload-manager-1.png"/>
                </div>
                <div class="ub-footer">
                    <?php if ($scanUploadEmail->enabled): ?>
                        <div>Ваши документы будут загружены на эту страницу, при отправке их по адресу:</div>
                        <div style="margin-top: 3px;"><a href="mailto:<?= $scanUploadEmail->email ?>"><b><?= $scanUploadEmail->email ?></b></a></div>
                    <?php else: ?>
                        У вас есть способ избежать ручной загрузки документов. <br/>
                        <a href="javascript:" class="show-email-upload-modal">Подробнее...</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="col-4 col-pad-5">
            <div class="wrap upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>
                    через мобильное приложение
                </div>
                <div class="ub-body">
                    <div>
                        <img width="150" src="/img/bg/upload-manager-2.png"/>
                    </div>
                    <div class="mt-3">
                        <?= Html::a('<img width="101" src="/img/bg/upload-manager-3.png"/>',
                            Yii::$app->params['googlePlayLink'], [
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </div>
                <div class="ub-footer">
                </div>
            </div>
        </div>

        <div class="col-4 col-pad-5">
            <div class="wrap upload-block">
                <div class="ub-header">
                    Загрузка документов<br/>с вашего компьютера
                </div>
                <div class="ub-body">

                    <div class="upload-pic">
                        <div id="dz-upload-scan" class="dz-upload-product-image dz-upload-first-step-image">
                            <div class="dz-help"><span class="link">Выберите файл</span><br>или перетащите сюда</div>
                            <div class="dz-drag" style="display: none">Перетащите файл сюда</div>
                        </div>
                        <div class="dz-upload-product-tip">Форматы: PDF, JPG, JPEG, PNG</div>
                    </div>
                </div>
                <div class="ub-footer"></div>
            </div>
        </div>

    </div>
</div>

<?php Modal::begin([
    'id' => 'scan-document-modal',
    'header' => Html::tag('h1', 'Фото документа'),
]); ?>

<?php Modal::end(); ?>

<?= $this->render('_partial/_email_upload_modal') ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.create-scan-btn', function() {
            $('#dz-upload-scan').click();
        });
        $(document).on("hidden.bs.modal", "#scan-document-modal", function () {
            $('#scan-document-modal .modal-body').html('')
        });
        $(document).on('click', '.show-email-upload-modal', function(e) {
            e.preventDefault();
            $('#email-upload-modal').modal();
            $.pjax({
                url: '/documents/upload-manager/get-email-upload-modal',
                container: '#email-upload-pjax',
                data: {},
                push: false,
                timeout: 5000
            });
        });
    });

    // DZ
    Dropzone.autoDiscover = false;
    var dropzone_scan_upload = new Dropzone("#dz-upload-scan", {
        'paramName': 'ScanDocument[upload]',
        'url': '/documents/upload-manager/create-scan',
        'dictDefaultMessage':  '',
        'dictInvalidFileType': 'Недопустимый формат файла',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png,application/pdf",
        'params': {'dz': true, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
    });
    dropzone_scan_upload.on('thumbnail', function (f, d) {
    });
    dropzone_scan_upload.on('success', function (f, d) {
        dragEnd();
        location.href = location.href;
    });
    dropzone_scan_upload.on('error', function (f, d) {
        dragEnd();
        alert(d);
    });
    dropzone_scan_upload.on('totaluploadprogress', function (progress) {
    });
    dropzone_scan_upload.on("addedfile", function(f) {
        dragEnd();
        $('.dz-upload-first-step-image .dz-help').remove();
    });
    dropzone_scan_upload.on("dragover", function(e) {
        dragStart();
    });
    dropzone_scan_upload.on("dragleave", function(e) {
        dragEnd();
    });

    /* DRAG */
    function dragStart() {
        var blocks = $(".dz-upload-product-image");
        $(blocks).css({'background-color': '#e8eaf7'});
        $(blocks).find('.dz-help').hide();
        $(blocks).find('.dz-drag').show();
    }
    function dragEnd() {
        var blocks = $(".dz-upload-product-image");
        $(blocks).css({'background-color': '#fff'});
        $(blocks).find('.dz-help').show();
        $(blocks).find('.dz-drag').hide();
    }

    $(document).on("dragstart, dragover", function (e) {
        dragStart();
    });
    $(document).on("dragend, dragleave", function (e) {
        dragEnd();
    });

</script>