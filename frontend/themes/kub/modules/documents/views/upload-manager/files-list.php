<?php

use common\models\Company;
use common\components\grid\GridView;
use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use frontend\models\Documents;
use frontend\modules\documents\models\ScanUploadEmail;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\UploadManagerHelper;
use common\models\employee\Employee;
use yii\widgets\Pjax;
use frontend\rbac\permissions;
use common\models\file\FileDir;
use yii\bootstrap\Dropdown;
use kartik\select2\Select2;
use frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\file\FileUploadType;
use yii\helpers\ArrayHelper;
use frontend\rbac\UserRole;
use common\models\document\ScanDocument;
use \common\models\Contractor;

/* @var $company Company */
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\UploadManagerFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $directory FileDir or null */
/** @var \frontend\modules\documents\models\ScanUploadEmail $scanUploadEmail */

$this->title = $title;

if (!isset($directory)) $directory = null;
if (!isset($isUploads)) $isUploads = false;
if (!isset($isTrash)) $isTrash = false;

$canIndex = $canCreate = $canDelete = $canUpdate = $canRestore = $canRecognize =
    Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
    Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
    Yii::$app->user->can(UserRole::ROLE_SUPERVISOR);

$dropDirs = [];
$dropDirs[] = [
    'label' => 'В папку',
    'url' => 'javascript:;',
    'linkOptions' => [
        'class' => 'group-attach-files-to',
    ],
];

if (!$isUploads) {
    $dropDirs[] = [
        'label' => 'Распознавание',
        'url' => '#move-files-to',
        'linkOptions' => [
            'class' => 'move-files-to',
            'data-directory_id' => 0
        ],
    ];
}

if ($dirs = FileDir::getEmployeeAllowedDirs()) {
    /** @var FileDir $dir */
    foreach ($dirs as $dir) {

        if ($directory && $directory->id == $dir->id)
            continue;

        $dropDirs[] = [
            'label' => $dir->name,
            'url' => '#move-files-to',
            'linkOptions' => [
                'class' => 'move-files-to',
                'data-directory_id' => $dir->id
            ],
        ];
    }
}

$docTypesList = UploadManagerHelper::getDocumentTypesList($company);

$dropDocs = [];
foreach ($docTypesList as $label => $docTypes) {
    $dropDocs[] = [
        'label' => $label,
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'group-attach-files-to',
        ],
    ];
    foreach ($docTypes as $key => $value) {
        $dropDocs[] = [
            'label' => $value,
            'url' => '#attach-files-to',
            'linkOptions' => [
                'class' => 'attach-files-to',
                'data-doc_type' => $key
            ],
        ];
    }
}

if ($isTrash)
    $emptyText = 'Корзина пуста.';
elseif ($isUploads)
    $emptyText = 'Нет новых сканов.';
else
    $emptyText = 'Вы еще не добавили ни одного фото документа.';

$hasFilters = (bool)$searchModel->filter_contractor ||
    (bool)$searchModel->date_from ||
    (bool)$searchModel->filter_type ||
    (bool)$searchModel->filter_doc_type;

$dateFrom = $searchModel->date_from;
$dateTo = $searchModel->date_to;

/** @var ScanUploadEmail $scanUploadEmail */
$scanUploadEmail = ScanUploadEmail::findOne([
    'company_id' => $company->id,
    'enabled' => true
]) ?: new ScanUploadEmail();

$filterDocTypeItems = [
    '' => 'Все',
    Documents::DOCUMENT_INVOICE => 'Счет',
    Documents::DOCUMENT_ACT => 'Акт',
    Documents::DOCUMENT_PACKING_LIST => 'ТН',
    Documents::DOCUMENT_INVOICE_FACTURE => 'СФ',
    Documents::DOCUMENT_UPD => 'УПД',
    Documents::DOCUMENT_AGREEMENT => 'Договор',
];
$filterContractorItems = ['' => 'Все'] + $searchModel->getFilterContractors();
$filterTypeItems = [
    '' => 'Все',
    Documents::IO_TYPE_IN => 'От поставщиков',
    Documents::IO_TYPE_OUT => 'От покупателей',
];

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="stop-zone-for-fixed-elems upload-manager-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <div class="column pr-0 pl-0">
            <h4 class="mb-2"><?= $this->title ?></h4>
        </div>
        <?php if ($isUploads): ?>
            <div class="column pr-2">
                <button class="button-list button-hover-transparent button-clr mb-2 collapsed" type="button" data-toggle="collapse" href="#helpCollapse" aria-expanded="false" aria-controls="helpCollapse">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#book"></use>
                    </svg>
                </button>
            </div>
        <?php endif; ?>
        <button class="button-regular button-regular_red button-width ml-auto create-scan-btn" data-url="<?= Url::to(['create-scan']) ?>">
            <svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>
            <span>Загрузить</span>
        </button>
    </div>

    <?php if ($isUploads): ?>
        <div class="collapse" id="helpCollapse">
            <?= $this->render('_partial/_help_panel_uploads') ?>
        </div>
    <?php endif; ?>

    <div class="table-settings row row_indents_s" style="margin-top:0;">
        <div class="col-5">
            <?php if (!$isTrash): ?>
                <?php // todo: Filter ?>
                <?php if ($scanUploadEmail->enabled): ?>
                    <div class="scan-upload-user-email">
                        <span>Почта для приема сканов</span><br/>
                        <a class="link" href="mailto:<?= $scanUploadEmail->email ?>"><?= $scanUploadEmail->email ?></a>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-7">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'id' => 'form_product_filters',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>

            <div class="mr-2">
                <!-- Filters -->
                <div class="dropdown popup-dropdown popup-dropdown_filter products-filter <?= $hasFilters ? 'itemsSelected' : '' ?>">
                    <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="button-txt">Фильтр</span>
                        <svg class="svg-icon svg-icon-shevron">
                            <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                        </svg>
                    </button>
                    <div class="dropdown-menu keep-open" aria-labelledby="filter">
                        <div class="popup-dropdown-in p-3">
                            <div class="p-1">
                                <div class="row">
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown1">
                                            <div class="label">Период</div>

                                            <?= Html::hiddenInput('UploadManagerFileSearch[date_from]', $dateFrom, ['data-id' => 'date_from']) ?>
                                            <?= Html::hiddenInput('UploadManagerFileSearch[date_to]', $dateTo, ['data-id' => 'date_to']) ?>

                                            <div class="dropdown popup-dropdown popup-dropdown_calendar popup-dropdown_calendar2" data-id="calendar" style="position:relative;">
                                                <button class="button-regular button-hover-content-red button-clr w-100" type="button" data-toggle="toggleVisible" data-target="calendar">
                                                    <?= $this->render('//svg-sprite', ['ico' => 'calendar']) ?>
                                                    <span class="calendar-2-title"><?= ($searchModel->date_from) ? "{$dateFrom} - {$dateTo}" : 'Указать диапазон' ?></span>
                                                </button>
                                                <div class="dropdown-menu" data-id="calendar">
                                                    <div class="popup-dropdown-in">
                                                        <div class="row items-container" >
                                                            <div class="row align-items-end">
                                                                <div class="col-12">
                                                                    <div class="row range-box-datepicker">
                                                                        <div class="col-6 pt-0 pb-0">
                                                                            <div class="text-grey text_size_14 mb-1">От</div>
                                                                            <div class="date-picker-wrap date-picker-wrap_inline date-picker-wrap_left date-picker-wrap_left_max mb-0">
                                                                                <div class="position-relative">
                                                                                    <?= Html::textInput('date_from', $dateFrom ?: date('d.m.Y'), [
                                                                                        'class' => 'form-control date-picker-calendar date-from',
                                                                                        'data-date-format' => 'dd.mm.yyyy',
                                                                                        'data-maxdate' => date('d.m.Y'),
                                                                                        'pattern' => '\d{2}\.\d{2}\.\d{4}',
                                                                                        'readonly' => true,
                                                                                    ]) ?>
                                                                                    <svg class="date-picker-icon svg-icon input-toggle" style="color: #335A82;">
                                                                                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6 pt-0 pb-0">
                                                                            <div class="text-grey text_size_14 mb-1">До</div>
                                                                            <div class="date-picker-wrap date-picker-wrap_inline date-picker-wrap_left mb-0">
                                                                                <div class="position-relative">
                                                                                    <?= Html::textInput('date_to', $dateTo ?: date('d.m.Y'), [
                                                                                        'class' => 'form-control date-picker-calendar date-to',
                                                                                        'data-date-format' => 'dd.mm.yyyy',
                                                                                        //'data-mindate' => date('d.m.Y'),
                                                                                        'pattern' => '\d{2}\.\d{2}\.\d{4}',
                                                                                        'readonly' => true,
                                                                                    ]) ?>
                                                                                    <svg class="date-picker-icon svg-icon input-toggle" style="color: #335A82;">
                                                                                        <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        <div class="col-6 pt-0 pb-0">
                                                                            <a class="text-white button-regular button-regular_red w-100 accept-calendar-2" href="#" onclick="acceptCalendar2()">Применить</a>
                                                                        </div>
                                                                        <div class="col-6 pt-0 pb-0">
                                                                            <a class="button-regular button-hover-grey w-100" href="#" data-toggle="toggleVisible" data-target="calendar">Отменить</a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <script>
                                                                    function acceptCalendar2()
                                                                    {
                                                                        var date_from = $('.date-picker-calendar.date-from').datepicker().val();
                                                                        var date_to = $('.date-picker-calendar.date-to').datepicker().val();
                                                                        if (date_from && date_to) {
                                                                            $('[data-id="date_from"]').val(date_from.split(".").reverse().join("-"));
                                                                            $('[data-id="date_to"]').val(date_to.split(".").reverse().join("-"));

                                                                            $('.popup-dropdown_calendar2').find('.dropdown-menu').removeClass('visible').removeClass('show');
                                                                            $('.calendar-2-title').html(date_from + ' - ' + date_to);
                                                                        }
                                                                    }
                                                                </script>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown2">
                                            <div class="label">Вид документа</div>
                                            <?= Html::activeHiddenInput($searchModel, 'filter_doc_type') ?>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                            <span class="drop-title">
                                                <?= ArrayHelper::getValue($filterDocTypeItems, $searchModel->filter_doc_type) ?>
                                            </span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown2">
                                                <div class="dropdown-drop-in">
                                                    <ul class="form-filter-list list-clr">
                                                        <?php foreach ($filterDocTypeItems as $key=>$item): ?>
                                                            <li>
                                                                <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filter_doc_type) ?>"><?= $item ?></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown3">
                                            <div class="label">Контрагент</div>
                                            <?= Html::activeHiddenInput($searchModel, 'filter_contractor') ?>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                <span class="drop-title">
                                                    <?= ArrayHelper::getValue($filterContractorItems, $searchModel->filter_contractor) ?>
                                                </span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown3">
                                                <div class="dropdown-drop-in">
                                                    <ul class="form-filter-list list-clr">
                                                        <?php foreach ($filterContractorItems as $key=>$item): ?>
                                                            <li>
                                                                <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filter_contractor) ?>"><?= $item ?></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown4">
                                            <div class="label">Тип</div>
                                            <?= Html::activeHiddenInput($searchModel, 'filter_type') ?>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown4">
                                                <span class="drop-title">
                                                    <?= ArrayHelper::getValue($filterTypeItems, $searchModel->filter_type) ?>
                                                </span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown4">
                                                <div class="dropdown-drop-in">
                                                    <ul class="form-filter-list list-clr">
                                                        <?php foreach ($filterTypeItems as $key=>$item): ?>
                                                            <li>
                                                                <a href="#" class="filter-item" data-id="<?= $key ?>" data-default="<?= (int)($key == $searchModel->filter_type) ?>"><?= $item ?></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <div class="row justify-content-between">
                                            <div class="form-group column">
                                                <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                    <span>Применить</span>
                                                </button>
                                            </div>
                                            <div class="form-group column">
                                                <button id="product_filters_reset" class="button-regular button-hover-content-red button-width-medium button-clr" data-clear="dropdown" type="button">
                                                    <span>Сбросить</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'find_by', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
            <?php $form->end(); ?>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'pjax-uploaded-files',
        'enablePushState' => false,
        'timeout' => 5000
    ]) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-style table-count-list uploaded_files_table',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
            $this->render('//layouts/grid/layout_no_scroll_modal', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
        'emptyText' => $emptyText,
        'rowOptions'=>function($data){
            return ['data-file_id' => $data['id']];
        },
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                    'style' => 'max-width:36px!important'
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('File[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'style' => 'margin-right:0!important',
                        'value' => $data['id']
                    ]);
                },
            ],
            [
                'label' => '',
                'headerOptions' => [
                    'width' => '1%',
                    'style' => 'min-width:40px'
                ],
                'format' => 'raw',
                'value' => function($data) {
                    $url = UploadManagerHelper::getImgPreviewUrl($data);
                    $icon = UploadManagerHelper::getFileIcon($data);

                    if ($data['ext'] == 'pdf') {
                        $img = Html::tag('embed', '', [
                            'src' => Url::to($url),
                            'width' => '250px',
                            'height' => '400px',
                            'name' => 'plugin',
                            'type' => 'application/pdf',
                        ]);
                    } else {
                        $img = Html::img($url, [
                            'style' => 'max-height:300px;max-width:300px;',
                            'alt' => '',
                        ]);
                    }
                    $content = Html::a($icon, $url, [
                        'class' => 'scan_link text-center',
                        'download' => 'download',
                        'data' => [
                            'pjax' => 0,
                            'tooltip-content' => '#scan-tooltip-' . $data['id'],
                        ]
                    ]);
                    $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $data['id']]);
                    $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);

                    return $content;
                }
            ],
            [
                'label' => 'Файл',
                'attribute' => 'filename_full',
                'headerOptions' => [
                    'width' => '35%',
                ],
                'contentOptions' => [
                    'style' => 'min-width: 150px',
                ],
                'format' => 'raw',
                'value' => function($data) use ($isTrash, $isUploads) {

                    if ($isTrash)
                        $content = Html::tag('span', $data['filename_full'], ['title' => $data['filename_full']]);
                    elseif ($isUploads)
                        $content = Html::a($data['filename_full'], ['preview-scans',
                            'dir_id' => 'upload',
                            'file_id' => $data['id']], ['data-pjax' => 0]);
                    else
                        $content = Html::a($data['filename_full'], ['preview-scans',
                            'dir_id' => $data['directory_id'],
                            'file_id' => $data['id']], ['data-pjax' => 0]);

                    return Html::tag('div', $content, [
                        'title' => $data['filename_full'],
                        'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                    ]);
                }
            ],
            [
                'label' => 'Дата загрузки',
                'attribute' => 'created_at',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'label' => 'Размер',
                'attribute' => 'filesize',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-right'
                ],
                'format' => 'raw',
                'value' => function($data) {

                    return UploadManagerHelper::formatSizeUnits($data['filesize']);
                }
            ],
            [
                'label' => 'Прикреплен',
                'headerOptions' => [
                    'width' => '35%',
                    'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                ],
                'contentOptions' => [
                    'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                ],
                'format' => 'raw',
                'value' => function($data) {
                    if ($owner = UploadManagerHelper::getAttachedDocument($data)) {

                        $invoice = (isset($owner->invoice)) ? $owner->invoice : $owner;
                        if ($owner instanceof \frontend\modules\documents\models\SpecificDocument) {
                            return '';
                        }
                        $doc = ($owner->type == Documents::IO_TYPE_IN ? 'Вх. ' : 'Исх. ') .
                            (isset($owner->shortPrefix) ? $owner->shortPrefix : $owner->printablePrefix) .
                            Html::a(' №'.$owner->getFullNumber(), $owner->viewUrl, [
                                'data-pjax' => 0,
                                'target' => '_blank'
                            ]).' от '.date_format(date_create($owner->document_date), 'd.m.Y');

                        $contractor = Html::a(Html::encode($invoice->contractor->getShortName()), [
                            '/contractor/view',
                            'type' => $invoice->type,
                            'id' => $invoice->contractor_id,
                        ], [
                            'data-pjax' => 0,
                            'target' => '_blank'
                        ]);

                        $content = Html::tag('div', $doc, ['class' => 'attached-document document']);

                        if (isset($owner->totalAmountWithNds)) {
                            $content .= Html::tag('div',
                                'сумма ' . TextHelper::invoiceMoneyFormat($owner->totalAmountWithNds, 2),
                                ['class' => 'attached-document amount']);
                        }

                        $content .= Html::tag('div', $contractor, ['class' => 'attached-document contractor']);

                        return $content;
                    }

                    return '';
                }
            ],
            [
                'label' => 'Детали',
                'headerOptions' => [
                    'class' => ((!$isUploads) ? 'hidden' : ''),
                    'width' => '35%'
                ],
                'contentOptions' => [
                    'class' => ((!$isUploads) ? 'hidden' : ''),
                ],
                'format' => 'raw',
                'value' => function($data) use ($company) {
                    $content = '';
                    $contractors = $products = '';

                    $scan = UploadManagerHelper::getScanDocument($data);
                    if ($scan instanceof ScanDocument) {
                        if ($scan->recognize) {
                            if ($scan->recognize->is_recognized && $scan->recognize->scanRecognizeDocuments) {
                                $rDocument = $scan->recognize->scanRecognizeDocuments[0];

                                //$content .= 'GUID: ' . $rDocument->guid . '<br/>';

                                if ($rDocument->document_type == 'UNKNOWN') {
                                    $content .= $rDocument->documentTypeName;
                                    return $content;
                                }

                                foreach ($rDocument->scanRecognizeContractors as $rContractor) {

                                    if ($rContractor->type == Contractor::TYPE_SELLER && $rDocument->io_type != Documents::IO_TYPE_OUT
                                        ||  $rContractor->type == Contractor::TYPE_CUSTOMER && $rDocument->io_type != Documents::IO_TYPE_IN) {

                                        $contractor = ($rContractor->contractor_id) ?
                                            $rContractor->short_name :
                                            Html::a($rContractor->short_name, 'javascript:;', [
                                                'class' => 'add-modal-contractor',
                                                'data-pjax' => 0,
                                                'data-inn' => $rContractor->inn,
                                                'data-type' => $rContractor->type,
                                                'style' => 'color:red',
                                                'title' => 'Добавить поставщика'
                                            ]);

                                        $contractors .= ($rContractor->type == 1 ? 'Поставщик: ' : 'Покупатель: ') . $contractor . '<br/>';
                                    }
                                }

                                $needSpecify = false;
                                foreach ($rDocument->scanRecognizeProducts as $rProduct) {
                                    if (!$rProduct->product_id) {
                                        $needSpecify = true;
                                        break;
                                    }
                                }

                                $productsLink = Html::a($needSpecify ? 'Уточнить' : 'Посмотреть', 'javascript:;', [
                                    'class' => 'create-doc-from-products-link link',
                                    'style' => ($needSpecify ? 'color:red' : ''),
                                    'data' => [
                                        'pjax' => 0,
                                        'type' => $rDocument->io_type ?: 1,
                                        'doc_type' => $rDocument->getDocumentTypeId(),
                                        'files' => [$data['id']]
                                    ]
                                ]);

                                $content .= ($rDocument->io_type ? ($rDocument->io_type == 1 ? 'Вх. ' : 'Исх. ') : '') .
                                    $rDocument->documentTypeName .
                                    ' №' . $rDocument->document_number .
                                    ' от ' . DateHelper::format($rDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
                                    '<br/>';
                                $content .= $contractors;
                                $content .= $productsLink;

                            } else {
                                $content = Html::tag('span', 'Распознается...', ['class' => 'scan-recognize-task', 'data-scan_id' => $scan->id]);
                            }
                        } else {
                            $content = Html::a('Распознать', '#one-recognize', [
                                'class' => 'btn-recognize btn btn-sm darkblue',
                                'data-toggle' => 'modal',
                                'data-id' => $data['owner_id']
                            ]);
                        }
                    }

                    return Html::tag('div', $content, [
                        'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                    ]);
                }
            ],

            [
                'label' => 'Папка',
                'attribute' => 'directory_id',
                'headerOptions' => [
                    'width' => '10%',
                    'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                ],
                'contentOptions' => [
                    'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                ],
                'format' => 'raw',
                'value' => function($data) use ($isUploads, $directory, $isTrash) {

                    if ($isUploads || $directory || $isTrash)
                        return '';

                    $directory = FileDir::findOne($data['directory_id']);
                    if ($directory) {
                        return Html::a($directory->name, '/documents/upload-manager/directory/' . $directory->id, [
                            'data-pjax' => 0
                        ]);
                    }

                    // no owner
                    if ($data['owner_model'] == ScanDocument::className() && !UploadManagerHelper::getAttachedDocument($data)) {
                        return Html::a('Распознавание', '/documents/upload-manager/upload', [
                            'data-pjax' => 0
                        ]);
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'created_at_author_id',
                'label' => 'Загрузил',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                    'style' => 'line-height:1.2; max-width: 200px',
                ],
                'value' => function ($data) {

                    $scan = UploadManagerHelper::getScanDocument($data);
                    if ($scan instanceof ScanDocument) {
                        if ($scan->is_from_employee) {
                            $content = ($scan->employee) ? $scan->employee->getShortFio() : '';
                        } elseif ($scan->is_from_contractor) {
                            $content = ($scan->contractor) ? $scan->contractor->getShortName() : '';
                        } elseif ($scan->from_email) {
                            $content = $scan->from_email;
                        } else {
                            $content = ($scan->employee) ? $scan->employee->getShortFio() : '';
                        }
                        $uploadType = ArrayHelper::getValue(FileUploadType::findOne($data['upload_type_id']), 'name', 'С компьютера');

                    } else {
                        $uploadType = 'К документу';

                        $employee = Employee::findOne(['id' => $data['created_at_author_id']]);
                        $content = $employee ? $employee->getShortFio() : '';
                    }

                    return Html::tag('span', $content.'<br/><span class="small">'.$uploadType.'</span>', ['title' =>$content]);
                },
                'format' => 'raw',
                'filter' => $searchModel->getCreators($dataProvider->query)
            ],
            [
                'label' => '',
                'headerOptions' => [
                    'width' => '5%',
                    'style' => 'min-width:165px!important;'
                ],
                'format' => 'raw',
                'value' => function($data) use ($docTypesList, $isTrash, $isUploads) {

                    if ($isTrash || UploadManagerHelper::getAttachedDocument($data))
                        return false;

                    $content = Select2::widget([
                        'name' => 'btn-attach-image',
                        'data' =>  $docTypesList,
                        'options' => [
                            'placeholder' => 'Прикрепить',
                            'class' => 'form-control btn-attach-image',
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'minimumResultsForSearch' => -1,
                            'dropdownCssClass' => 'dropdown-attach-image',
                        ],
                    ]);

                    return $content;
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end() ?>

</div>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        (!$isTrash && $canRecognize) ?
            Html::a($this->render('//svg-sprite', ['ico' => 'recognize']) . '<span>Распознать</span>', '#many-recognize', [
                'class' => 'btn-many-recognize button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal']) : null,
        ($isTrash && $canRestore) ?
            Html::a($this->render('//svg-sprite', ['ico' => 'search']) . '<span>Восстановить</span>', '#many-restore', [
                'class' => 'btn-many-recognize button-clr button-regular button-width button-hover-transparent',
                'data-toggle' => 'modal']) : null,
        (!$isTrash && $canUpdate && $dropDocs) ? Html::tag('div', Html::button(
                $this->render('//svg-sprite', ['ico' => 'clip']) . '<span class="pr-2">Прикрепить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'btn-many-attach button-regular button-regular-more button-more-2-icons button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropDocs,
                'options' => [
                    'class' => 'form-filter-list list-clr dropdown-menu-right',
                ],
            ]), ['class' => 'dropup']) : null,
        (!$isTrash && $canUpdate && $dropDirs) ? Html::tag('div', Html::button(
                $this->render('//svg-sprite', ['ico' => 'folder']) . '<span class="pr-2">Переместить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-more-2-icons button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropDirs,
                'options' => [
                    'class' => 'form-filter-list list-clr dropdown-menu-right',
                ]]), ['class' => 'dropup']) : null,
        (!$isTrash && $canDelete) ?
            Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal']) : null,
    ]]) ?>

<?php Modal::begin([
    'id' => 'scan-document-modal',
    'header' => Html::tag('h1', 'Фото документа'),
]); ?>

<?php Modal::end(); ?>

<?= $this->render('_partial/_invoices_modal', [
    'company' => $company,
]) ?>

<?= $this->render('_partial/_joint_operations_modals', [
    'canUpdate' => $canUpdate,
    'canRestore' => $canRestore,
    'canDelete' => $canDelete,
    'canRecognize' => $canRecognize
]) ?>

<?= $this->render('_partial/_kub_doc_js') ?>

<div style="display:none" id="dz-upload-scan" class="dz-upload-scan dropzone"></div>

<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<div id="hellopreloader" style="display: none;">
    <div id="hellopreloader_preload"></div>
</div>
<div id="visible-right-menu" style="display: none;">
    <div id="visible-right-menu-wrapper"></div>
</div>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzone_scan_upload = new Dropzone("#dz-upload-scan", {
        'paramName': 'ScanDocument[upload]',
        'url': '<?= Url::to(['create-scan', 'dir_id' => ($directory) ? $directory->id : null]) ?>',
        'dictDefaultMessage':  '<div class="icon"><?=ImageHelper::getThumb('img/upload-files-pc.png', [100, 100])?></div>',
        'dictInvalidFileType': 'Недопустимый формат файла',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png,application/pdf",
        'params': {'dz': true, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
    });
    dropzone_scan_upload.on('thumbnail', function (f, d) {
    });
    dropzone_scan_upload.on('success', function (f, d) {
        location.href = location.href;
    });
    dropzone_scan_upload.on('error', function (f, d) {
        alert(d);
    });
    dropzone_scan_upload.on('totaluploadprogress', function (progress) {
    });
</script>

<script>

    $(document).on("change", ".btn-attach-image", function (e) {
        e.preventDefault();
        var addToDoc = $(this).val();
        var files = [$(this).parents('tr').data('file_id')];

        if (!addToDoc)
            return false;

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
            $(this).val('').trigger('change');
        }
    });

    $(document).on('click', '.group-attach-files-to', function(e) {
        return false;
    });

    $(document).on("click", ".attach-files-to", function (e) {
        e.preventDefault();
        var addToDoc = $(this).data('doc_type');
        var files = [];
        $('.uploaded_files_table').find('.joint-operation-checkbox').filter(':checked').each(function() {
            files.push($(this).val());
        });

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
        }

    });

    $('.modal-attach-image-yes').on('click', function () {
        $('#invoices-list').modal();
        $('#modal-attach-image').modal('hide');
        $.pjax({
            url: '/documents/upload-manager/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            },
            push: false,
            timeout: 5000
        });
    });

    $(document).on('click', '.create-doc-from-products-link', function(e) {
        e.preventDefault();
        $.post('/documents/upload-manager/redirect-to-create-document', {
            type: $(this).data('type'),
            doc_type: $(this).data('doc_type'),
            files: $(this).data('files')
        }, function(data) {
            $('#modal-attach-image').modal('hide');
            if (data['redirect_url']) {
                location.href = data['redirect_url'];
            }
        });
    });

    $('.modal-attach-image-no').on('click', function () {
        $.post('/documents/upload-manager/redirect-to-create-document', {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            }, function(data) {
                $('#modal-attach-image').modal('hide');
                if (data['redirect_url']) {
                    location.href = data['redirect_url'];
                }
        });
    });

    $('.move-files-to').on('click', function(e) {
        e.preventDefault();
        $('#move-files-to').find('.modal-move-files-to').attr('data-url', '/documents/upload-manager/move-files-to?directory_id=' + $(this).data('directory_id'));
        $('#move-files-to').modal('show');
    });

    $(document).on('click', '.create-scan-btn', function() {
        $('.dz-upload-scan').click();
        //$("#scan-document-modal").modal('show');
        //$('#scan-document-modal .modal-body').load($(this).data('url'));
    });
    $(document).on("hidden.bs.modal", "#scan-document-modal", function () {
        $('#scan-document-modal .modal-body').html('')
    });

    $(document).ready(function() {
        $('.scan_link').tooltipster({
            theme: ['tooltipster-kub'],
            contentCloning: true,
            side: ['right', 'left', 'top', 'bottom'],
        });

        ScanRecognize.init();
    });

    // Recognize
    ScanRecognize = {
        timerId: null,
        timerPeriod: 60000,
        taskIdx: null,
        init: function()
        {
            if ($('.scan-recognize-task').length) {
                this.timerId = setInterval(this.checkRecognizeStatus, this.timerPeriod);
                this.checkRecognizeStatus();
            }
        },
        checkRecognizeStatus: function()
        {
            var els = $('.uploaded_files_table').find('.scan-recognize-task');
            var el;

            if (els.length === 0) {
                clearInterval(this.timerId);
                return;
            }

            if (this.taskIdx === null) {
                el = $(els).first();
            } else {
                el = $(els).eq(this.taskIdx + 1);
                if (!el.length)
                    el = $(els).first();
            }
            this.taskIdx = $(el).index();

            $.ajax({
                url: 'check-recognize-status',
                data: {scan_id: $(el).data('scan_id')},
                success: function(data) {
                    if (data.result) {
                        $.pjax.reload("#pjax-uploaded-files");
                        ScanRecognize.checkRecognizeStatus();
                    }
                }
            });
        }
    };
</script>

<div style="display:none">
    <!-- Contractor -->
    <select id="invoice-contractor_id"><option value="0"></option></select>
    <input type="hidden" id="invoice-contractor_inn"/>
    <input type="hidden" id="invoice-contractor_type"/>
    <!-- Product -->
    <input type="hidden" id="new_product_id"/>
    <input type="hidden" id="new_product_name"/>
    <input type="hidden" id="new_product_code"/>
    <br id="has_self_add_modal_product_method" data-url="add-modal-product"/>
</div>

<script>
    function beforeShowAddNewModal() {
        $('#add-new .modal-header').html("");
        $('#block-modal-new-product-form').html("");
    }

    $(document).on("click", ".add-modal-contractor", function() {
        var url  = "add-modal-contractor";
        var form = {
            documentType: $(this).data('type'),
            inn: $(this).data('inn')
        };

        $("#invoice-contractor_id").val(0);
        $("#invoice-contractor_inn").val($(this).data('inn'));
        $("#invoice-contractor_type").val($(this).data('type'));

        beforeShowAddNewModal();
        INVOICE.addNewContractor(url, form);

        return false;
    });

    $(document).on("click", ".add-modal-product", function() {
        var url = "add-modal-product";
        var form = {
            Product: {
                name: $(this).data('name'),
                production_type: 1,
                flag: 1,
                documentType: $(this).data('document-type'),
                price: $(this).data('price')
            }
        };

        $("#new_product_id").val(0);
        $("#new_product_name").val($(this).data('name'));
        $("#new_product_code").val($(this).data('code'));

        beforeShowAddNewModal();
        INVOICE.addNewProduct(url, form);

        return false;
    });

    // after adding new Contractor
    $(document).on("change", "select#invoice-contractor_id", function(e) {

        if ($(this).val() == 0)
            return;

        var url = "set-recognized-contractor-id";
        var form = {
            id: $("#invoice-contractor_id").val(),
            inn: $("#invoice-contractor_inn").val(),
            type: $("#invoice-contractor_type").val()
        };
        $.get(url, form, function (data) {
            $.pjax.reload("#pjax-uploaded-files");
        });
    });

    // after adding new Product
    $(document).on("change", "#new_product_id", function(e) {

        if ($(this).val() == 0)
            return;

        var url = "set-recognized-product-id";
        var form = {
            id: $("#new_product_id").val(),
            name: $("#new_product_name").val(),
            code: $("#new_product_code").val()
        };
        $.get(url, form, function (data) {
            $.pjax.reload("#pjax-uploaded-files");
        });
    });

    // show/hide recognize button
    $(document).on('change', '.joint-operation-checkbox', function () {
        var showRecognizeBtn = false;
        var hideAttachBtn = false;
        if ($('.joint-operation-checkbox:checked').length) {
            $('.joint-operation-checkbox:checked').each(function (i) {
                if (!showRecognizeBtn) showRecognizeBtn = $(this).closest('tr').find('.btn-recognize').length > 0;
                if (!hideAttachBtn) hideAttachBtn = $(this).closest('tr').find('.attached-document').length > 0;
            });
            if (showRecognizeBtn) {
                $('.btn-many-recognize').show();
            } else {
                $('.btn-many-recognize').hide();
            }
            if (hideAttachBtn) {
                $('.btn-many-attach').hide();
                $('.move-files-to').filter('[data-directory_id="0"]').hide();
            } else {
                $('.btn-many-attach').show();
                $('.move-files-to').filter('[data-directory_id="0"]').show();
            }
        }
    });

    // FILTER BUTTON TOGGLE COLOR
    function refreshProductFilters() {
        var pop = $(".products-filter");
        var submit = $(pop).find("[type=submit]");
        var filter_on = false;
        $(pop).find(".dropdown-drop").each(function() {
            var a_val = $(this).find("a.filter-item").filter("[data-default=1]").attr("data-id");
            var i_val = $(this).find("input").val();
            if (i_val === undefined) {
                i_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val === undefined) {
                a_val = $(this).find("a.filter-item").first().find("input").attr("data-id");
            }
            if (a_val != i_val) {
                filter_on = true;
                return false;
            }
        });

        if (filter_on)
            $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
        else
            $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
    }


    $("#form_product_filters").find(".filter-item").on("click", function(e) {
        e.preventDefault();
        var pop =  $(this).parents(".popup-dropdown_filter");
        var drop = $(this).parents(".dropdown-drop");
        var value = $(this).data("id");
        $(drop).find("input").val(value);
        $(drop).find(".drop-title").html($(this).text());
        $(drop).find(".dropdown-drop-menu").removeClass("visible show");

        refreshProductFilters();
    });

    $("#product_filters_reset").on("click", function() {
        var pop =  $(this).parents(".popup-dropdown_filter");
        $(pop).find(".drop-title").each(function() {
            var drop = $(this).parents(".dropdown-drop");
            var a_first = $(drop).find("li").first().find("a");
            $(this).html($(a_first).text());
            $(drop).find("input").val($(a_first).data("id"));
        });

        $(pop).find(".dropdown-drop-menu").removeClass("visible show");

        $('[data-id="date_from"]').val('');
        $('[data-id="date_to"]').val('');
        $('.calendar-2-title').html('Указать диапазон');


        refreshProductFilters();
    });

    // SEARCH
    $("input#uploadmanagerfilesearch-find_by").on("keydown", function(e) {
        if(e.keyCode == 13) {
            e.preventDefault();
            $(this).closest("form").first().submit();
        }
    });

</script>