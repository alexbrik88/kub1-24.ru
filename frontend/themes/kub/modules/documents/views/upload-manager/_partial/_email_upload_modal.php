<?php
use yii\widgets\Pjax;
?>
<div class="modal fade t-p-f" id="email-upload-modal" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'email-upload-pjax',
                'linkSelector' => false,
                'formSelector' => '#email-upload-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <h4 class="modal-title">Загрузка документов по e-mail</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body"></div>
            <div class="row action-buttons pad-10">
                <div class="col-sm-12">
                    <button class="button-regular button-width button-regular_red button-clr" data-dismiss="modal">Включить</button>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJs("

    $(document).on('hidden.bs.modal', '#email-upload-modal', function () {
        $('#email-upload-modal .modal-body').html('')
    });

"); ?>

