<?php
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\models\Documents;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\product\Store;

$productIdArray = Yii::$app->request->post('product', []);
?>

<?php Pjax::begin([
    'id' => 'pjax-product-grid',
    'linkSelector' => false,
    'formSelector' => '#products_in_order',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<div class="modal-body">

    <h4 class="modal-title">Выбрать товар/услугу из списка</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/img/svg/svgSprite.svg#close"></use>
        </svg>
    </button>

    <?= Html::beginForm(['/documents/upload-manager/get-products'], 'get', [
        'id' => 'products_in_order',
        'class' => 'add-to-invoice',
        'data' => [
            'pjax' => true,
        ]
    ]); ?>

    <?= Html::hiddenInput('documentType', $documentType, [
        'id' => 'documentTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('searchTitle', $searchModel->title, [
        'id' => 'searchTitleHidden',
    ]); ?>

    <?php foreach ((array) $searchModel->exclude as $value) {
        echo Html::hiddenInput('exists[]', $value);
    } ?>

    <div class="form-body">

        <div class="search-form-default">
            <div class="pull-right" style="width: 100%">
                <div class="input-group">
                    <div class="input-cont inp_pad" style="width: calc(100% - 95px)">
                        <?= Html::input('search', 'title', $searchModel->title, [
                            'placeholder' => 'Поиск...',
                            'class' => 'form-control',
                            'id' => 'product-title-search',
                            'autocomplete' => 'off'
                        ]) ?>
                    </div>
                    <span class="input-group-btn">
                        <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                            'class' => 'mb-3 ml-1 button-clr button-regular button-regular_padding_bigger button-regular_red',
                        ]); ?>
                    </span>
                </div>
            </div>
        </div>

        <div class="portlet m-b add-to-invoice-table" id="add-to-invoice-tbody">
            <div class="portlet-body accounts-list">
                <div class="table-container" style="">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list uploaded_files_table',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
                            $this->render('//layouts/grid/layout_no_scroll_modal', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'emptyText' => 'Ничего не найдено.',
                        'columns' => [
                            [
                                'format' => 'raw',
                                'value' => function (Product $model) use ($productIdArray) {
                                    return Html::checkbox('in_order[]', in_array($model->id, $productIdArray), [
                                        'id' => 'product-' . $model->id,
                                        'value' => $model->id,
                                        'class' =>'product_selected_one',
                                    ]);
                                },
                                'headerOptions' => [
                                    'width' => '3%'
                                ],
                                'header' => Html::checkbox('in_order_all', false, [
                                    'id' => 'product_selected-all',
                                ]),
                            ],
                            [
                                'attribute' => 'title',
                                'label' => 'Продукция',
                                'headerOptions' => [
                                    'width' => '65%'
                                ],
                                'format' => 'text',
                            ],
                            [
                                'attribute' => 'article',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'format' => 'text',
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                            ],
                            //Группа
                            [
                                'attribute' => 'group_id',
                                'label' => 'Группа',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'filter' => [null => 'Все'] + ArrayHelper::map(ProductGroup::getGroups(), 'id', 'title'),
                                'format' => 'raw',
                                'value' => 'group.title',
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                            ],
                            //Количество на складе
                            [
                                'attribute' => 'quantity',
                                'label' => 'Количество на складе',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return $data->quantity * 1;
                                },
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS &&
                                             $documentType == Documents::IO_TYPE_OUT,
                            ],
                            [
                                'attribute' => 'price_for_sell_with_nds',
                                'label' => 'Цена продажи',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds);
                                },
                                'visible' => $documentType == Documents::IO_TYPE_OUT,
                            ],
                            [
                                'attribute' => 'price_for_buy_with_nds',
                                'label' => 'Цена покупки',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds);
                                },
                                'visible' => $documentType == Documents::IO_TYPE_IN,
                            ],
                        ],
                    ]); ?>
                </div>
                <span class="empty-choose-error" style="color:red; display: none">Необходимо выбрать</span>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= Html::button('Выбрать', [
                    'class' => 'mt-3 button-regular button-width button-regular_red button-clr pull-right btn-w button-add-marked',
                    //'data-dismiss' => 'modal',
                    'id' => 'add-to-invoice-button',
                    'style' => 'margin-left: 15px;',
                    'data-type' => $documentType,
                ]); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).on("change", "#product_selected-all", function() {
    $("input.product_selected").prop("checked", $(this).prop("checked")).trigger("change").uniform("refresh");
});
</script>

<?php Pjax::end(); ?>