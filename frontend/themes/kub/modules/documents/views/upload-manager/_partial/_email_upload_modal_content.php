<?php
use common\components\ImageHelper;
use yii\helpers\Html;

$emailArr = explode('.', $model->email);
$customEmailPart = array_shift($emailArr);
$basisEmailPart = implode('.', $emailArr);

/** @var $model \frontend\modules\documents\models\ScanUploadEmail */
?>

<h4 class="modal-title">Загрузка документов по e-mail</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row">
        <div class="col-12">
            <table class="content-table">
                <tr>
                    <td>
                        <div>Присылайте счета, акты товарные накладные и другие документы
                            на e-mail, который мы создали для вас: <a href="mailto:<?= $oldEmail ?>"><?= $oldEmail ?></a></div>
                        <div class="mt-2">Документы будут сохранены на этой странице.
                        Мы сделаем распознавание сканов, чтобы вы могли проверить  и преобразовать их в счета и другие документы,
                            для дальнейшей работы.</div>
                        <div class="email-upload-text-wrap mt-2" style="<?= !$model->enabled ? '':'display:none' ?>">
                            Не нравится e-mail, который мы вам создали?
                            <a href="javascript:" class="btn-enable-email-upload">Выберите сами!</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="email-upload-text-wrap w-100"  style="<?= !$model->enabled ? '':'display:none' ?>">
            <div class="mt-3 text-center">
                <button class="btn-enable-email-upload button-regular button-width button-regular_red button-clr">Включить</button>
            </div>
        </div>
    </div>

    <div class="email-upload-form-wrap" style="<?= $model->enabled ? '':'display:none' ?>">
        <?php
        $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
            'action' => \yii\helpers\Url::to("/documents/upload-manager/get-email-upload-modal"),
            'method' => 'POST',

            'options' => [
                'id' => 'email-upload-form',
                'enctype' => 'multipart/form-data',
                'data-pjax' => true
            ],

            'fieldConfig' => [
                'labelOptions' => [
                    'class' => 'col-md-4 control-label',
                ],
            ],
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'validateOnSubmit' => false,
            'validateOnBlur' => false,
        ]));
        ?>

        <?= Html::activeHiddenInput($model, 'enabled', ['value' => '1']); ?>

        <div class="row">
            <div class="col-12">
                <label class="control-label" style="font-weight: bold" for="scanuploademail-customemailpart">
                    Подтвердите ваш e-mail, на которые вы будите пересылать документы<span class="important" aria-required="true">*</span>:</label>
            </div>
            <div class="col-12 mt-2">
                <input style="width:25%;display: inline-block" type="text" id="scanuploademail-customemailpart" class="form-control" name="ScanUploadEmail[customEmailPart]" value="<?=$customEmailPart?>" aria-required="true">
                <input style="width:73%;display: inline-block" type="text" id="scanuploademail-basisemailpart" class="form-control" name="ScanUploadEmail[basisEmailPart]" value="<?=$basisEmailPart?>" aria-required="true" readonly>
                <p class="help-block help-block-error ">
                    <?= $model->getFirstError('customEmailPart') ?>
                    <?= $model->getFirstError('email') ?>
                </p>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-width button-regular_red button-clr',
                'style' => 'width: 130px!important;',
            ]); ?>
            <button type="button" class="button-clr button-regular button-widthbutton-hover-transparent" data-dismiss="modal">Отменить</button>
        </div>

        <?php
        $form->end();
        ?>
    </div>

</div>

<?php
$this->registerJs("
    $(document).on('click', '.btn-enable-email-upload', function(e) {
        $('.email-upload-text-wrap').hide();
        $('.email-upload-form-wrap').fadeIn(200);
    });
"); ?>

