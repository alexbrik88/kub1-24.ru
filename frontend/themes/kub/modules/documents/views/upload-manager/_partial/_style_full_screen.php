<style>
    /* MAIN LAYOUT */
    .page-content.preview-scan.full-screen {
        max-width: 100%;
        width: 100%;
        flex: unset;
    }

    /* SLIDER */
    .page-scan-preview {
        position: relative;
        width: 100%;
    }
    .slider-body {
        position: relative;
        margin-top: 12px;
    }
    .issuu-img-container {
        width:100%;
        text-align: center;
    }
    .issuu-embed-container {
        position: relative;
        padding-bottom: 100%; /* set the aspect ratio here as (height / width) * 100% */
        height: 0;
        overflow: hidden;
        max-width: 100%;
    }
    .issuu-embed-container embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    .issuu-img-container img {
        width: auto;
        background-color: #fff;
    }
    .page-scan-preview .zoomIn,
    .page-scan-preview .zoomOut {
        position: absolute;
        right:45px;
        width:50px;
        height:25px;
        text-align: center;
        background-color: #ddd;
        border: 1px solid #ccc;
        cursor: pointer;
        opacity: .75;
    }
    .page-scan-preview .zoomIn:hover,
    .page-scan-preview .zoomOut:hover {
        background-color: #eee;
    }
    .page-scan-preview .zoomIn i,
    .page-scan-preview .zoomOut i {
        font-size:18px;
        color:#aaa;
        padding-top:2px;
    }
    .page-scan-preview .zoomIn {
        bottom:80px;
        border-top-left-radius: 8px!important;
        border-top-right-radius: 8px!important;
    }
    .page-scan-preview .zoomOut {
        bottom:50px;
        border-bottom-left-radius: 8px!important;
        border-bottom-right-radius: 8px!important;
    }

    .button-list-width {
        width: 44px;
    }
    .wrap_btns_100 > .row > .column {
        padding-left: 6px;
        padding-right: 6px;
    }
    .page-scan-preview .file-name {
        font-weight: bold;
        margin: 0 10px;
        max-width: 300px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .page-scan-preview .file-name > span {
        font-size: 13px;
        color: #9198a0;
        padding-left: 10px;
    }
    .scan-info-column {
        position: relative;
    }
    .scan-info-column .prevPage {
        position: absolute;
        top:0;
        left: -20px;
        cursor: pointer;
    }
    .scan-info-column .nextPage {
        position: absolute;
        top:0;
        right: -20px;
        cursor: pointer;
    }
    .scan-info-column .prevPage > a {
        transform:rotate(90deg);
        font-size: 22px;
        margin-top: 10px;
    }
    .scan-info-column .nextPage > a {
        transform:rotate(270deg);
        font-size: 22px;
        margin-top: 10px;
    }
    .scan-info-column .num-page-wrap {
        padding-top: 5px;
    }
    .preview-scan.full-screen .button-more-2-icons .svg-icon:last-child {
        padding-top: 7px;
    }
</style>