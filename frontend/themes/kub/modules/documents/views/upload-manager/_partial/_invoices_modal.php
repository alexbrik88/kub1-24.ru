<?php
use yii\widgets\Pjax;
?>
<div class="modal fade t-p-f" id="invoices-list" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'get-vacant-invoices-pjax',
                'formSelector' => '#search-invoice-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <div class="invoice-list"></div>
            <div class="row action-buttons pad-10">
                <div class="col-sm-12">
                    <button type="button" class="pull-right button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJs("

    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"label_name\"]').val(picker.chosenLabel);
        $('#search-invoice-form').submit();
    });   

    $(document).on('click', '#add-to-out-document', function () {
        var checkboxes = $('.checkbox-invoice-id');
        $.post('/documents/upload-manager/attach-file', $('#search-invoice-form').serialize(), function(data) {
            $('#invoices-list').modal('hide');
            location.href = location.href;
        });
    });
       
    $(document).on('click', '.attach-out-document', function(e) {
        e.preventDefault();
        $(this).parents('tr').find('.checkbox-invoice-id').prop('checked', true);
        $.post('/documents/upload-manager/attach-file', $('#search-invoice-form').serialize(), function(data) {
            $('#invoices-list').modal('hide');
            location.href = location.href;
        });
    });
    
    $(document).on('click', '.checkbox-invoice-id', function() {
        var checkboxes = $('.checkbox-invoice-id');
        $('#add-to-out-document').removeAttr('disabled');
    });
        
    $(document).on('hidden.bs.modal', '#invoices-list', function () {
        $('#invoices-list .modal-body').html('')
    });

"); ?>

