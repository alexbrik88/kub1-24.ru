<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
/* @var array $files */

$user = Yii::$app->user->identity;
?>
<h4 class="modal-title">Прикрепить файл к <?= $documentTypeName ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="invoice-list">
    <?= Html::beginForm(["/documents/upload-manager/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>
    <?= Html::hiddenInput('doc_type', $docType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod): ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>
            <?php foreach ($files as $file)
                echo Html::hiddenInput('files[]', $file) ?>

            <div class="search-form-default">
                <div class="pull-right">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'byNumber', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'mb-3 ml-1 button-clr button-regular button-regular_padding_bigger button-regular_red',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?php \yii\widgets\Pjax::begin([
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list uploaded_files_table',
                        ],
                        'options' => [
                            'id' => 'invoice-payment-order',
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout_no_scroll_modal', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
                        'columns' => [
                            [
                                'label' => '',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'style' => 'display:none!important',
                                ],
                                'contentOptions' => [
                                    'style' => 'display:none!important',
                                ],
                                'value' => function ($data) use ($docType) {

                                    if ($docType == Documents::DOCUMENT_ACT && isset($data->acts[0]))
                                        $id = $data->acts[0]->id;
                                    elseif ($docType == Documents::DOCUMENT_PACKING_LIST && isset($data->packingLists[0]))
                                        $id = $data->packingLists[0]->id;
                                    elseif ($docType == Documents::DOCUMENT_INVOICE_FACTURE && isset($data->invoiceFactures[0]))
                                        $id = $data->invoiceFactures[0]->id;
                                    elseif ($docType == Documents::DOCUMENT_UPD && isset($data->upds[0]))
                                        $id = $data->upds[0]->id;
                                    elseif ($docType == Documents::DOCUMENT_INVOICE)
                                        $id = $data->id;
                                    else
                                        return '';

                                    return Html::radio('attached_doc_id', false, [
                                        'value' => $id,
                                        'class' => 'checkbox-invoice-id',
                                        'style' => 'margin-left: -9px;',
                                    ]);
                                },
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_act',
                                'label' => 'Акт',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_ACT ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_ACT ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($user, $files) {
                                    $content = '';
                                    foreach ($data->acts as $doc) {
                                        $docLink = Html::a($doc->fullNumber, 'javascript:;', [
                                            'data-pjax' => 0,
                                            'class' => 'attach-out-document',
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }

                                    return $content;
                                },
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_packing_list',
                                'label' => 'ТН',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_PACKING_LIST ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_PACKING_LIST ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($user, $files) {
                                    $content = '';
                                        foreach ($data->packingLists as $doc) {
                                            $docLink = Html::a($doc->fullNumber, 'javascript:;', [
                                                'data-pjax' => 0,
                                                'class' => 'attach-out-document',
                                            ]);

                                            $fileLink = DocumentFileWidget::widget([
                                                'model' => $doc,
                                                'cssClass' => 'pull-right',
                                            ]);

                                            $content .= Html::tag('div', $docLink . $fileLink);
                                        }

                                    return $content;
                                }
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_invoice_facture',
                                'label' => 'СФ',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_INVOICE_FACTURE ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_INVOICE_FACTURE ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($files) {
                                    $content = '';

                                    if (!$data->hasNds) {
                                        return $content;
                                    }
                                    /* @var $doc \common\models\document\InvoiceFacture */
                                    foreach ($data->invoiceFactures as $doc) {
                                        $docLink = Html::a($doc->fullNumber, 'javascript:;', [
                                            'class' => 'attach-out-document',
                                            'data-pjax' => 0
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }

                                    return $content;
                                },
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'has_upd',
                                'label' => 'УПД',
                                'headerOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_UPD ? '' : ' hidden'),
                                    'width' => '100px',
                                ],
                                'contentOptions' => [
                                    'class' => ($docType == Documents::DOCUMENT_UPD ? '' : ' hidden'),
                                ],
                                'format' => 'raw',
                                'value' => function (Invoice $data) use ($files) {
                                    $content = '';

                                    /* @var $doc \common\models\document\Upd */
                                    foreach ($data->upds as $doc) {
                                        $docLink = Html::a($doc->fullNumber, 'javascript:;', [
                                            'class' => 'attach-out-document',
                                            'data-pjax' => 0
                                        ]);

                                        $fileLink = DocumentFileWidget::widget([
                                            'model' => $doc,
                                            'cssClass' => 'pull-right',
                                        ]);

                                        $content .= Html::tag('div', $docLink . $fileLink);
                                    }

                                    return $content;
                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) use ($files, $docType) {
                                    /** @var Invoice $data */
                                    if ($docType == Documents::DOCUMENT_INVOICE) {
                                        return Html::a($data->fullNumber, 'javascript:;', [
                                                'class' => 'attach-out-document',
                                                'data-pjax' => '0',
                                            ]);
                                    }

                                    return $data->fullNumber;
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'attribute' => 'total_amount_with_nds',
                                'value' => function (Invoice $model) {
                                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                },
                            ],
                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'headerOptions' => [
                                    'width' => '24%',
                                ],
                                'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                                'format' => 'raw',
                                'value' => 'contractor_name_short',
                                's2width' => '300px'
                            ],
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12">
        <button type="button" class="pull-right button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
</div>