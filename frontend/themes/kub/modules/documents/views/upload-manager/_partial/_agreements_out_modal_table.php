<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
/* @var array $files */

$user = Yii::$app->user->identity;
?>
<h4 class="modal-title">Прикрепить файл к <?= $documentTypeName ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="invoice-list">
    <?= Html::beginForm(["/documents/upload-manager/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>
    <?= Html::hiddenInput('doc_type', $docType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod): ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>
            <?php foreach ($files as $file)
                echo Html::hiddenInput('files[]', $file) ?>

            <div class="search-form-default">
                <div class="pull-right">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'find_by', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'mb-3 ml-1 button-clr button-regular button-regular_padding_bigger button-regular_red',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?php \yii\widgets\Pjax::begin([
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list uploaded_files_table',
                        ],
                        'options' => [
                            'id' => 'invoice-payment-order',
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout_no_scroll_modal', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
                        'columns' => [
                            [
                                'label' => '',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'style' => 'display:none!important',
                                ],
                                'contentOptions' => [
                                    'style' => 'display:none!important',
                                ],
                                'value' => function ($data) use ($docType) {

                                    if ($docType == Documents::DOCUMENT_AGREEMENT)
                                        $id = $data['id'];
                                    else
                                        return '';

                                    return Html::radio('attached_doc_id', false, [
                                        'value' => $id,
                                        'class' => 'checkbox-invoice-id',
                                        'style' => 'margin-left: -9px;',
                                    ]);
                                },
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ договора',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) use ($files, $docType) {
                                    if ($docType == Documents::DOCUMENT_AGREEMENT) {
                                        $agreement = \common\models\Agreement::findOne($data['id']);
                                        return Html::a($agreement->fullNumber, 'javascript:;', [
                                            'class' => 'attach-out-document',
                                            'data-pjax' => '0',
                                        ]);
                                    }

                                    return '';
                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата договора',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '21%',
                                    'class' => 'dropdown-filter',
                                ],
                                'contentOptions' => [
                                    'class' => 'contractor-cell',
                                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                                ],
                                'attribute' => 'contractor_id',
                                'label' => 'Контр&shy;агент',
                                'encodeLabel' => false,
                                'filter' => $searchModel->getContractorFilterItems(),
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $contractor = \common\models\Contractor::findOne($data['cid']);
                                    $name = Html::encode($contractor->getShortName());
                                    return Html::a($name, [
                                        '/contractor/view',
                                        'type' => $data['type'],
                                        'id' => $data['cid'],
                                    ], [
                                        'target' => '_blank',
                                        'title' => html_entity_decode($name),
                                        'data-pjax' => 0,
                                        'data-id' => $data['cid'],
                                    ]);
                                },
                                's2width' => '300px'
                            ],
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12">
        <button type="button" class="pull-right button-clr button-regular button-width button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
</div>