<?php

$tariffList = [
    1 => ['months' => 4, 'count' => 50,  'price' => 1000, 'hit' => 0],
    2 => ['months' => 4, 'count' => 100, 'price' => 1700, 'hit' => 0],
    3 => ['months' => 4, 'count' => 200, 'price' => 3000, 'hit' => 1],
    4 => ['months' => 4, 'count' => 300, 'price' => 3600, 'hit' => 0],
    5 => ['months' => 4, 'count' => 400, 'price' => 4000, 'hit' => 0],
]

?>
<div class="wrap help-tables">

    <h4 class="mb-3">РАСПОЗНАВАНИЕ ДОКУМЕНТОВ</h4>

    <table class="no-border w-100 table-1">
        <tr>
            <td>
                <div class="img"><img src="/img/bg/help-table-1-1.png"/></div>
                <div class="txt"><span>Счет</span></div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-1-2.png"/></div>
                <div class="txt"><span>Акт</span></div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-1-3.png"/></div>
                <div class="txt"><span>Товарная накладная</span></div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-1-4.png"/></div>
                <div class="txt"><span>УПД</span></div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-1-5.png"/></div>
                <div class="txt"><span>Счет-фактуры</span></div>
            </td>
        </tr>
    </table>
    <div class="txt-gray">
        Загрузите сканы документов.<br/>
        КУБ распознает и создаст документ, товары, услуги и контрагентов.
    </div>

    <h4 class="mt-3 mb-3">ВАШИ ВЫГОДЫ</h4>

    <div style="margin-right: -10px; margin-left: -10px;">
    <table class="no-border w-100 table-2">
        <tr>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-1.svg"/></div>
                <div class="head">УЧЕТ ЗАТРАТ<br/>в управленческих отчетах</div>
                <div class="txt">Загружайте документы от поставщиков и расходы автоматически попадут в нужные отчеты</div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-2.svg"/></div>
                <div class="head">ВВОД ТОВАРОВ<br/>из товарных накладных</div>
                <div class="txt">Не нужно тратить время на ручной ввод данных. Загрузите ТН и весь поступивший товар от поставщика у вас в системе</div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-3.svg"/></div>
                <div class="head">СОЗДАНИЕ ПЛАТЕЖЕК<br/>на основе отчетов</div>
                <div class="txt">Из счета, в 1 клик формируйте платежку и отправляйте в клиент-банк</div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-4.svg"/></div>
                <div class="head">ЭКОНОМИЯ<br/>вашего времени и денег</div>
                <div class="txt">Сокращение затрат на ввод документов на 80%</div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-5.svg"/></div>
                <div class="head">УДОБНО<br/>загружать документы</div>
                <div class="txt">
                    <ul>
                        <li>На e-mail</li>
                        <li>Загрузка с компьютера</li>
                        <li>Фото из мобильного приложения</li>
                    </ul>
                </div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-2-6.png"/></div>
                <div class="head">ВЫГРУЗКА В 1С<br/>распознанных документов</div>
                <div class="txt">Чем меньше работы у бухгалтера, тем меньше вы ему платите</div>
            </td>
        </tr>
    </table>
    </div>

    <h4 class="mt-3 mb-3">КАК РАБОТАЕТ РАСПОЗНАВАНИЕ</h4>

    <table class="no-border w-100 table-3">
        <tr>
            <td>
                <div class="img"><img src="/img/bg/help-table-3-1.svg"/></div>
                <div class="txt">Загружайте сканы и фото документов в КУБ</div>
                <div class="arrow"><img src="/img/arrow-help.svg"/></div>
            </td>
            <td>
                <div class="img"><img src="/img/bg/help-table-3-2.svg"/></div>
                <div class="txt">КУБ распознает сканы и сформирует документы</div>
                <div class="arrow"><img src="/img/arrow-help.svg"/></div>
            </td>
            <td>
                <div class="img" style="padding-top:20px;"><img src="/img/bg/help-table-3-3.svg"/></div>
                <div class="txt">Данные из документов будут использованы в отчетах</div>
            </td>
        </tr>
    </table>

    <h4 class="mt-3 mb-3">Тарифы</h4>

    <div class="row row-5ths">
        <?php foreach ($tariffList as $tariff): ?>
            <div class="col-x5">
                <div class="tariff-block <?= $tariff['hit'] ? 'hit' : '' ?>">
                    <?php if ($tariff['hit']): ?>
                        <div class="hit">ХИТ</div>
                    <?php endif; ?>
                    <div class="head">
                        <?= \php_rutils\RUtils::numeral()->getPlural($tariff['count'], ['страница', 'страницы', 'страниц']) ?>
                    </div>
                    <div class="body">
                        <div class="average_price">
                            <span class="small">на <?= \php_rutils\RUtils::numeral()->getPlural($tariff['months'], ['месяц', 'месяца', 'месяцев']) ?></span>
                        </div>
                        <div class="price">
                            <?= \common\components\TextHelper::numberFormat($tariff['price']); ?> ₽
                        </div>
                        <div class="average_price">
                            <span class="small top-left">₽</span>
                            <?= \common\components\TextHelper::numberFormat(($tariff['count']) ? round($tariff['price'] / $tariff['count']) : 0)  ?>
                            <span class="small">/страница</span>
                        </div>
                    </div>
                    <div class="foot">
                        <button class="button-clr button-regular <?= ($tariff['hit']) ? 'button-hover-transparent' : 'button-regular_red' ?> w-100">Оплатить</button>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>

<style>
    .help-tables .txt-gray { font-size: 14px; color: #9198A0; }
    .help-tables .table-1 { margin: 25px 0; }
    .help-tables .table-1 td { text-align: center; width: 20%; }
    .help-tables .table-1 .img {}
    .help-tables .table-1 .txt { margin-top: 10px; color: #4679AE; }

    .help-tables .table-2 { margin: 0; border-collapse: separate; border-spacing:10px; }
    .help-tables .table-2 td { text-align: left; vertical-align: top; width: 33.333%; border: 1px solid #f2f3f7; padding: 15px; }
    .help-tables .table-2 .img {}
    .help-tables .table-2 .txt {}
    .help-tables .table-2 .head { font-size: 18px; color:#4679AE; font-weight: bold; margin: 15px 0 10px 0; }
    .help-tables .table-2 ul { padding-left: 0; }
    .help-tables .table-2 li { margin-left: 15px; }

    .help-tables .table-3 { margin: 0; border-collapse: separate; border-spacing:10px; }
    .help-tables .table-3 td { position: relative; text-align: center; vertical-align: top; width: 33.333%; border: 1px solid #f2f3f7; padding: 25px; }
    .help-tables .table-3 .img { height: 160px; }
    .help-tables .table-3 .txt { margin-top: 10px; }
    .help-tables .table-3 .arrow { position: absolute; top: 25px; right: -50px; z-index: 2; }

    .tariff-block { margin-top: 20px; min-height: 250px; border: 1px solid #ddd; margin-bottom: 10px; position: relative; border-radius: 4px; }
    .tariff-block .head { padding: 20px; background-color: #eee; color:#999; font-size: 18px; }
    .tariff-block .body .price { padding: 15px 20px 0 20px; font-size: 32px; font-weight: bold; }
    .tariff-block .body .average_price { position: relative; padding: 0 20px 15px 20px; color:#9299a1; font-size: 24px; }
    .tariff-block .body .average_price > .small { font-size: 13px; }
    .tariff-block .body .average_price > .top-left { position: absolute; top:0; left:13px; }
    .tariff-block .foot { padding: 20px; text-align: center; }
    .tariff-block.hit { margin-top: 0; }
    .tariff-block.hit .head { background-color: #7098c0; color: #fff; padding-top: 40px; }
    .tariff-block.hit .body { background-color: #4679ae;  color: #fff; }
    .tariff-block.hit .body .average_price { color: #d0d8e0; }
    .tariff-block.hit .foot { background-color: #4679ae; }
    .tariff-block.hit .hit {position: absolute; right: 5px; top: 5px; color:#fff; font-size: 18px; }

    .row-5ths { padding-left:10px; padding-right:10px; }
    .col-x5 { position: relative; min-height: 1px; padding-right: 5px; padding-left: 5px; width: 25%; float: left; }
    @media (min-width: 1316px) {
        .col-x5 { width: 20%; float: left; }
    }
</style>