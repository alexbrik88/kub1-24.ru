<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use yii\bootstrap4\ActiveForm;

/** @var  $model \common\models\file\FileDir */

$header = ($action == 'create-dir') ? 'Создать папку' : 'Обновить папку';

$form = ActiveForm::begin([
    'action' => \yii\helpers\Url::to("/documents/upload-manager/{$action}/?id={$model->id}"),
    'method' => 'POST',
    'options' => [
        'id' => 'add-directory-form',
        'enctype' => 'multipart/form-data',
        'data-pjax' => true
    ],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'validateOnSubmit' => false,
    'validateOnBlur' => false,
]);

$employeeArray = ArrayHelper::map(
        Employee::find()
            ->where(['company_id' => $model->company_id])
            ->indexBy('lastname')
            ->all(), 'id', function ($employee) {
        return $employee->getShortFio(true) ?: '(не задан)';
    });
$dropItems = [
    [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ],
    ],
];
if ($action == 'create-dir') {
    $selectedEmployers = $model->hasEmployers;
} else {
    $selectedEmployers = ArrayHelper::getColumn(
        $model->employers,
        'id'
    );
}

?>

<h4 class="modal-title"><?= ($action == 'update-dir') ? 'Редактировать папку' : 'Новая папка' ?></h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

    <?php echo $form->field($model, 'name')
        ->textInput(['class' => 'form-control']);
    ?>

    <?php echo $form->field($model, 'hasEmployers')
    ->widget(Select2::classname(),[
        'data' => $employeeArray,
        'class' => 'form-control',
        'options' => [
            'id' => 'directory-employers',
            'placeholder' => 'Выберите сотрудников...',
            'multiple' => true,
            'value' => $selectedEmployers,
        ],
        'pluginOptions' => [
            'closeOnSelect' => false,
            'width' => '100%'
        ],
        'toggleAllSettings' => [
            'selectLabel' => '<i></i>Все',
            'unselectLabel' => '<i></i>Все',
        ]
    ]); ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'style' => 'width: 130px!important;',
    ]); ?>
    <button type="button" class="button-clr button-regular button-widthbutton-hover-transparent" data-dismiss="modal" style="width: 130px!important;">Отменить</button>
</div>

<?php
$form->end();
?>