<?php

use common\models\document\OrderSalesInvoice;
use common\models\document\SalesInvoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use php_rutils\RUtils;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model SalesInvoice */
/** @var $message Message */
/** @var $ioType integer */
/** @var $useContractor boolean */
/** @var $contractorId integer|null */

$dateFormatted = RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$products = OrderSalesInvoice::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = Url::previous('sales-invoice_last_index_page') ?: ['index', 'type' => $model->type];
}
$precision = $model->invoice->price_precision;

$isFullCustomerInfo = $ioType == Documents::IO_TYPE_OUT_URL;
$fullInfoPrefix = '';
if ($isFullCustomerInfo) {
    $fullInfoPrefix = 'full';
}

?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, [
        'class' => 'link mb-2',
    ]); ?>
<?php endif ?>

<div class="wrap wrap_padding_small position-relative">
    <div class="page-in row">
        <div class="col-12 column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, [
                    'model' => $model,
                ])
                ) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'type' => $ioType,
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->invoice->contractor_id : null),
                    ], [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                        'title' => 'Редактировать',
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container">
                    <div class="custom-scroll-table" style="overflow-x: auto;">
                        <div style="width: 1200px; position: relative;<?= $model->add_stamp ? null : 'padding-bottom: 30px;' ?>">
                            <?= $this->render('_viewPartials/_pre-view-out', [
                                'model' => $model,
                                'message' => $message,
                                'dateFormatted' => $dateFormatted,
                                'ioType' => $ioType,
                                'useContractor' => $useContractor,
                                'pageView' => true,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar page-in-sidebar_absolute pl-3 column pl-3">
            <?= $this->render('_viewPartials/_status_block_out', [
                'model' => $model,
                'ioType' => $model->type,
                'useContractor' => $useContractor,
            ]); ?>
            <?= $this->render('_viewPartials/_main_info', [
                'model' => $model,
                'message' => $message,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('_viewPartials/_action_buttons_out', [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>
