<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;

/* @var $this yii\web\View */
/* @var $model common\models\document\SalesInvoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

\frontend\assets\AppAsset::register($this);
\frontend\assets\PrintAsset::register($this);

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->invoice->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-sales-invoice';
?>

<?= $this->render('_viewPartials/_pre-view-out', [
    'model' => $model,
    'message' => $message,
    'dateFormatted' => $dateFormatted,
    'ioType' => Documents::IO_TYPE_OUT,
    'useContractor' => true,
]); ?>