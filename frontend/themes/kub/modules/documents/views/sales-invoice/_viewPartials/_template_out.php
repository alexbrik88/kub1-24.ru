<?php

use common\components\TextHelper;
use common\models\product\Product;

/** @var \common\models\document\OrderSalesInvoice $order */
/** @var [] $product */

$plus = 0;

?>
<?php if (isset($order)): ?>
    <?php
    if (!isset($model)) {
        $model = $order->salesInvoice;
    }
    if ($order->salesInvoice->invoice->hasNds) {
        $priceNoNds = $order->priceNoNds;
        $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
        $amountWithNds = $model->getPrintOrderAmount($order->order_id);
        $TaxRateName = $order->order->saleTaxRate->name;
        $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
    } else {
        $priceNoNds = $order->priceWithNds;
        $amountNoNds = $model->getPrintOrderAmount($order->order_id);
        $amountWithNds = $model->getPrintOrderAmount($order->order_id);
        $TaxRateName = 'Без НДС';
        $ndsAmount = Product::DEFAULT_VALUE;
    }
    $quantity = $order ? $order->getAvailableQuantity() : 1;
    if ($quantity != intval($quantity)) {
        $quantity = rtrim(number_format($quantity, 10, '.', ''), 0);
    }
    ?>
    <tr role="row" class="odd order">
        <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->product_code; ?></td>
        <td><?= $order->order->unit ? $order->order->unit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->box_type; ?></td>
        <td><?= $order->order->count_in_place; ?></td>
        <td><?= $order->order->place_count; ?></td>
        <td><?= $order->order->mass_gross; ?></td>
        <td style="padding: 1px">
            <input class="input-editable-field quantity form-control-number"
                    type="number"
                    value="<?= $quantity ?>"
                    data-value="<?= $quantity ?>"
                    min="1"
                    max="<?= $quantity ?>"
                    step="any"
                    id="<?= $order->order_id ?>"
                    name="<?= $order ? 'OrderSalesInvoice[' . $order->order_id . '][quantity]' : '' ?>"
                    style="width: 100px;">
        </td>
        <input class="status" type="hidden"
               name="<?= 'OrderSalesInvoice[' . $order->order_id . '][status]' ?>"
               value="active">
        <td><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
        <td><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
        <td>
            <?= $TaxRateName ?>
        </td>
        <td>
            <?= $ndsAmount ?>
        </td>
        <td><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
        <td>
            <button class="delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td><?= \common\components\helpers\Html::dropDownList('test', null, $result, ['id' => 'choose-product-in-table', 'class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <input class="status" type="hidden"
               name=""
               value="active">
        <td></td>
        <td
        </td>
        <td>
        </td>
        <td>
        </td>
        <td></td>
        <td class="text-right"></td>
        <td>
            <button class="delete-row button-clr" type="button">
                <svg class="table-count-icon svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
                </svg>
            </button>
        </td>
    </tr>
<?php endif; ?>
