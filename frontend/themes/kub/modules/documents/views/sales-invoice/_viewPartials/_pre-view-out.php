<?php

use common\models\document\SalesInvoice;
use frontend\modules\documents\components\Message;
use common\components\image\EasyThumbnailImage;
use \common\models\document\OrderSalesInvoice;
use \common\components\TextHelper;
use \common\models\product\Product;

/* @var $model SalesInvoice
 * @var $addStamp bool
 * @var $signatureLink string
 * @var $pageView bool
 */

$actionType = Yii::$app->request->get('actionType', 'pdf');

$pageView = isset($pageView) ? $pageView : false;

$precision = $model->invoice->price_precision;
$realMassNet = $model->invoice->getRealMassNet();
$realMassGross = $model->invoice->getRealMassGross();
$company = $model->invoice->company;
$addStamp = (boolean)$model->add_stamp;
if ($model->signed_by_name) {
    $signatureLink = !$model->employeeSignature
        ? null
        : EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = !$company->chief_signature_link
        ? null
        : EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
}

$printLink = !$company->print_link
    ? null
    : EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$orderQuery = OrderSalesInvoice::find()->where(['sales_invoice_id' => $model->id]);
$orderArray = $orderQuery->all();

?>
<style>
    .ver-top {
        vertical-align: top;
    }

    .font-size-6 {
        font-size: 9px
    }

    .font-size-7 {
        font-size: 10px
    }

    .font-size-8 {
        font-size: 11.5px
    }

    .font-size-8-bold {
        font-size: 11.5px;
        font-weight: bold
    }

    .table.no-border, .customer-info.no-border {
        border: none !important;
    }

    .table.no-border td {
        border: none;
    }

    .order-packing .page-number {
        font-family: Arial, sans-serif;
        font-size: 8pt;
        font-style: italic;
        text-align: right;
    }

    .order-packing td {
        border: 1px solid #000000;
        padding: 2px 2px !important;
        border-color: #000 !important
    }

    .ver-bottom {
        vertical-align: bottom;
    !important;
    }

    #print_layer {
        height: 230px;
        background-image: url('<?= $addStamp ? $printLink : null ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }

    #signature1_layer {
        background-image: url('<?= $addStamp ? $signatureLink : null ?>');
        background-position: 30%<?= $model->signed_by_employee_id ? '41px' : '56px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }

    #signature2_layer {
        background-image: url('<?= $addStamp ? $signatureLink : null ?>');
        background-position: 30%<?= $model->signed_by_employee_id ? '85px' : '93px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }

    .caption {
        font-size: 1.5em;
    }
</style>
<div class="over-hidden m-size-div container-first-account-table no_min_h <?= $pageView ? 'p-0' : 'p-5'; ?> pt-1 pb-1"
     style="min-width: 520px; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="no-border">
            <div class="m-0 p-0">
                <div class="no-border m-0 p-0">
                    <div class="caption text-bold weight-300 pl-0 ml-0">
                        <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                        № <span class="editable-field"><?= $model->fullNumber; ?></span>
                        от
                        <span id="document_date_item" class="editable-field"
                              data-tooltip-content="#tooltip_document_date"><?= $dateFormatted; ?></span>
                    </div>
                </div>
                <table class="table no-border p-0 m-0 mt-3">
                    <tr>
                        <td class="text-left ver-top ml-0 pl-0 font-size-8">Поставщик</td>
                        <td class="font-size-7" colspan="2">
                            <?= $model->invoice->company_name_short; ?>,
                            ИНН <?= $model->invoice->company_inn; ?>
                            <?php if (!empty($model->invoice->company_kpp)): ?>
                                , КПП <?= $model->invoice->company_kpp; ?>
                            <?php endif; ?>
                            , <?= $model->invoice->company_address_legal_full; ?>,
                            р/с <?= $model->invoice->company_rs; ?>, в
                            банке <?= $model->invoice->company_bank_name; ?>,
                            БИК <?= $model->invoice->company_bik; ?>,
                            к/с <?= $model->invoice->company_ks; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-left ver-top ml-0 pl-0 font-size-8">Покупатель</td>
                        <td class="font-size-7" colspan="2">
                            <?= $model->invoice->contractor_name_short,
                            $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                            $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                            $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                            $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                            $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                            $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                            $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : '';
                            ?>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="order-packing m-0 p-0 <?= $actionType == 'print' ? 'mt-4' : ''; ?>">
                <table class="table font-size-7 pt-0 pb-0 mt-0 mb-0">
                    <tr>
                        <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">
                            Но-мер по по-рядку
                        </td>
                        <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Товар</td>
                        <td width="10%" colspan="2" class="text-center font-size-7" style="padding: 1px 2px">
                            Единица измерения
                        </td>
                        <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Вид
                            упаков-ки
                        </td>
                        <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Количество</td>
                        <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Масса брутто
                        </td>
                        <td width="7%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">
                            Количество (масса нетто)
                        </td>
                        <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Цена, руб.
                            коп.
                        </td>
                        <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма без учета
                            НДС, руб. коп.
                        </td>
                        <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">НДС</td>
                        <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма с учётом
                            НДС, руб. коп.
                        </td>
                    </tr>
                    <tr>
                        <td width="22%" class="text-center font-size-7 font-size-7" style="padding: 1px 2px">
                            наименование, характеристика, сорт, артикул товара
                        </td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">код</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">наиме-нование</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">код по ОКЕИ</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">в одном месте</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">мест, штук</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">ставка, %</td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">сумма, руб. коп.</td>
                    </tr>
                    <tr>
                        <td class="text-center  font-size-6">1</td>
                        <td class="text-center  font-size-6">2</td>
                        <td class="text-center  font-size-6">3</td>
                        <td class="text-center  font-size-6">4</td>
                        <td class="text-center  font-size-6">5</td>
                        <td class="text-center  font-size-6">6</td>
                        <td class="text-center  font-size-6">7</td>
                        <td class="text-center  font-size-6">8</td>
                        <td class="text-center  font-size-6">9</td>
                        <td class="text-center  font-size-6">10</td>
                        <td class="text-center  font-size-6">11</td>
                        <td class="text-center  font-size-6">12</td>
                        <td class="text-center  font-size-6">13</td>
                        <td class="text-center  font-size-6">14</td>
                        <td class="text-center  font-size-6">15</td>
                    </tr>
                    <?php

                    foreach ($orderArray as $key => $order):
                        if ($model->invoice->hasNds) {
                            $priceNoNds = $order->priceNoNds;
                            $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
                            $amountWithNds = $model->getPrintOrderAmount($order->order_id);
                            $TaxRateName = $order->order->saleTaxRate->name;
                            $ndsAmount = \common\components\TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
                        } else {
                            $priceNoNds = $order->priceWithNds;
                            $amountNoNds = $model->getPrintOrderAmount($order->order_id);
                            $amountWithNds = $model->getPrintOrderAmount($order->order_id);
                            $TaxRateName = 'Без НДС';
                            $ndsAmount = Product::DEFAULT_VALUE;
                        }
                        if ($order->quantity != intval($order->quantity)) {
                            $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
                        } ?>
                        <tr>
                            <td class="text-center font-size-7" style="padding: 1px 2px"
                                style="padding: 1px 2px"><?= $key + 1; ?></td>
                            <td class="font-size-7"><?= $order->order->product_title; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->article; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->box_type; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->count_in_place; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->place_count; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->order->mass_gross; ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $order->quantity; ?></td>
                            <td class="text-right font-size-7"
                                style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
                            <td class="text-right font-size-7"
                                style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
                            <td class="text-center font-size-7"
                                style="padding: 1px 2px"><?= $TaxRateName; ?></td>
                            <td class="text-right font-size-7" style="padding: 1px 2px">
                                <?= $ndsAmount; ?>
                            </td>
                            <td class="text-right font-size-7"
                                style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <?php $totalQuantity = $orderQuery->sum('quantity') * 1;
                    if ($totalQuantity != intval($totalQuantity)) {
                        $totalQuantity = rtrim(number_format($totalQuantity, 10, '.', ''), 0);
                    } ?>
                    <tr>
                        <td class="text-right font-size-7" colspan="7"
                            style="border: none !important; padding: 1px 2px">Итого
                        </td>
                        <td class="text-center font-size-7"
                            style="padding: 1px 2px"><?= $model->invoice->total_place_count; ?></td>
                        <td class="text-center font-size-7"
                            style="padding: 1px 2px"><?= $model->invoice->total_mass_gross; ?></td>
                        <td class="text-center font-size-7" style="padding: 1px 2px"><?= $totalQuantity; ?></td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
                        <td class="text-right font-size-7" style="padding: 1px 2px">
                            <?= TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2); ?>
                        </td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
                        <td class="text-right font-size-7" style="padding: 1px 2px">
                            <?php if ($model->invoice->hasNds) : ?>
                                <?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-right font-size-7"
                            style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                    </tr>
                    <tr>
                        <td class="text-right font-size-7" colspan="7"
                            style="border: none !important;padding: 1px 2px">Всего по накладной
                        </td>
                        <td class="text-center font-size-7"
                            style="padding: 1px 2px"><?= $model->invoice->total_place_count; ?></td>
                        <td class="text-center font-size-7"
                            style="padding: 1px 2px"><?= $model->invoice->total_mass_gross; ?></td>
                        <td class="text-center font-size-7" style="padding: 1px 2px"><?= $totalQuantity; ?></td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
                        <td class="text-right font-size-7" style="padding: 1px 2px">
                            <?= TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2); ?>
                        </td>
                        <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
                        <td class="text-right font-size-7" style="padding: 1px 2px">
                            <?php if ($model->invoice->hasNds) : ?>
                                <?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?>
                            <?php endif; ?>
                        </td>
                        <td class="text-right font-size-7"
                            style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                    </tr>
                </table>

                <div id="print_layer" data-image="<?= $printLink ?>">
                    <div id="signature1_layer" data-image="<?= $signatureLink ?>">
                        <div id="signature2_layer" data-image="<?= $signatureLink ?>">
                            <table class="table no-border mt-4" style="margin-bottom: 0 !important;">
                                <tr>
                                    <td colspan="8" class="font-size-8-bold"
                                        style="padding-bottom: 1px; padding-top: 1px">Всего
                                        наименований <?= count($orderArray); ?>, на
                                        сумму <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="font-size-8-bold"
                                        style="padding-bottom: 1px; padding-top: 1px">
                                        <?= TextHelper::amountToWords($model->getPrintAmountWithNds() / 100); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8" class="text-center vet-top font-size-6"
                                        style="border-top: 1px solid black;padding-bottom: 0; padding-top: 0">
                                        (прописью)
                                    </td>
                                </tr>
                            </table>
                            <table class="table no-border mt-4" style="margin-bottom: 0 !important;">
                                <tr>
                                    <td width="14%" class="font-size-7"
                                        style="padding-bottom: 1px;padding-top: 1px">Отпустил
                                    </td>
                                    <td width="10%"></td>
                                    <td width="12%"></td>
                                    <td width="3%"></td>
                                    <td width="12.5%" style="padding: 0 0 0 10px;" class="font-size-6">Получил
                                    </td>
                                    <td width="10%"></td>
                                    <td width="12%"></td>
                                    <td width="3%"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-center vet-top font-size-6"
                                        style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                        (подпись)
                                    </td>
                                    <td colspan="2"></td>
                                    <td></td>
                                    <td class="text-center vet-top font-size-6"
                                        style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">
                                        (подпись)
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
