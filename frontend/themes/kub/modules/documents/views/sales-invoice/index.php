<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\SalesInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\SalesInvoiceStatus;
use common\models\employee\Employee;
use common\models\service\Payment;
use common\widgets\Modal;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\SalesInvoiceSearch;
use frontend\rbac\permissions;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use yii\web\View;

/* @var $this View */
/* @var $searchModel SalesInvoiceSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = Invoice::find()->joinWith('salesInvoices', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет расходных накладных. Измените период, чтобы увидеть имеющиеся товарные накладные.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одной товарной накладной.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передана',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => SalesInvoiceStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписана',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => SalesInvoiceStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if ($ioType == Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ],
    ];
}

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_sales_invoice' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_document');

$isTaxTypeOsno = $company->companyTaxationType->osno;
$isTaxTypeUsn = $company->companyTaxationType->usn;

if ($isTaxTypeUsn || $isTaxTypeOsno) {
    $stat1 = [
        'sum' => $searchModel->getQuery()
            ->addSelect([
                SalesInvoice::tableName().'.*',
                '`invoice`.`total_amount_with_nds` as `total_amount_with_nds`',
            ])
            ->sum('total_amount_with_nds'),
        'count' => $searchModel->getQuery()
            ->count(),
        'title' => 'Итого',
        'title2' => 'Количество расходных накладных:',
    ];
    $stat2 = [
        'sum' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->addSelect([
                SalesInvoice::tableName().'.*',
                'IF(`invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds` - `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
            ])
            ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
            ->andWhere([SalesInvoice::tableName() . '.type' => $ioType])
            ->sum('total_amount_with_nds'),
        'count' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->andWhere(['NOT IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED]])
            ->andWhere([SalesInvoice::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Неоплачено',
        'title2' => 'Количество расходных накладных:',
    ];
    $stat3 = [
        'sum' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->addSelect([
                SalesInvoice::tableName().'.*',
                'IF(`invoice`.`payment_partial_amount`, `invoice`.`payment_partial_amount`, `invoice`.`total_amount_with_nds`) as `total_amount_with_nds`',
            ])
            ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
            ->andWhere([SalesInvoice::tableName() . '.type' => $ioType])
            ->sum('total_amount_with_nds'),
        'count' => $searchModel->getQuery()
            ->joinWith('invoice.payment')
            ->andWhere(['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
            ->andWhere([SalesInvoice::tableName() . '.type' => $ioType])
            ->count(),
        'title' => 'Оплачено',
        'title2' => 'Количество расходных накладных:',
    ];
}


$this->registerCss(<<<CSS
    .sorting_block {
        cursor: pointer;
    }
CSS);

$this->registerJs(<<<JS
    $(".sorting_block").on("click", function(e){
        e.preventDefault();
        var url = $(this).data("url");

        if (url) {
            window.location = url;
        }
    });
JS, View::POS_READY);

?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()) : ?>
        <?php
        Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как подготовить расходную накладную, нужно создать Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php
        Modal::end();

        ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как подготовить расходную накладную,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
            .
        </div>
    <?php endif; ?>
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if ($company->createInvoiceAllowed($ioType)) : ?>
            <?php if ($existsInvoicesToAdd) : ?>
                <button class="button-regular button-regular_red button-width ml-auto add-document">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
            <?php else : ?>
                <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
                    '/documents/invoice/create',
                    'type' => $ioType,
                    'document' => Documents::SLUG_SALES_INVOICE,
                ]) ?>">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </a>
            <?php endif; ?>
        <?php else : ?>
            <button class="button-regular button-regular_red button-width ml-auto action-is-limited">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </button>
        <?php endif ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card wrap"
                     data-url=<?= Url::to([
                         '/documents/sales-invoice/index',
                         'type' => $ioType,
                         'SalesInvoiceSearch[payment_status]' => 'all'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat1['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat1['title2']; ?>
                        <?= $stat1['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card count-card_red wrap"
                     data-url=<?= Url::to([
                         '/documents/sales-invoice/index',
                         'type' => $ioType,
                         'SalesInvoiceSearch[payment_status]' => 'not_payment'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat2['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat2['title2']; ?>
                        <?= $stat2['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3">
                <div class="sorting_block count-card count-card_green wrap"
                     data-url=<?= Url::to([
                         '/documents/sales-invoice/index',
                         'type' => $ioType,
                         'SalesInvoiceSearch[payment_status]' => 'payment'
                     ]); ?>
                >
                    <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                    <div class="count-card-title"><?= $stat3['title']; ?></div>
                    <hr>
                    <div class="count-card-foot">
                        <?= $stat3['title2']; ?>
                        <?= $stat3['count'] ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]); ?>
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'sales_invoice_order_sum_without_nds',
                        'visible' => (bool)$isTaxTypeOsno,
                    ],
                    [
                        'attribute' => 'sales_invoice_order_sum_nds',
                        'visible' => (bool)$isTaxTypeOsno,
                    ],
                    [
                        'attribute' => 'sales_invoice_contractor_inn',
                    ],
                    [
                        'attribute' => 'sales_invoice_payment_date',
                    ],
                    [
                        'attribute' => 'sales_invoice_responsible',
                    ],
                    [
                        'attribute' => 'sales_invoice_source',
                    ],
                ],
            ]); ?>
            <?= TableViewWidget::widget(['attribute' => 'table_view_document']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер расходной накладной, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $model) {
                    return Html::checkbox('SalesInvoice[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->totalAmountWithNds,
                        'data-contractor' => $model->invoice->contractor_id,
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата Р/Н',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ Р/Н',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                        'model' => $data,
                    ])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber;
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'invoice.total_amount_with_nds',
                'label' => 'Сумма',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'order_sum_without_nds',
                'label' => 'Сумма без НДС',
                'headerOptions' => [
                    'class' => 'col_sales_invoice_order_sum_without_nds ' . ($userConfig->sales_invoice_order_sum_without_nds ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view col_sales_invoice_order_sum_without_nds ' . ($userConfig->sales_invoice_order_sum_without_nds ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = TextHelper::invoiceMoneyFormat($data->totalAmountNoNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
                'visible' => (bool)$isTaxTypeOsno,
            ],
            [
                'attribute' => 'order_sum_nds',
                'label' => 'Сумма НДС',
                'headerOptions' => [
                    'class' => 'col_sales_invoice_order_sum_nds ' . ($userConfig->sales_invoice_order_sum_nds ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view col_sales_invoice_order_sum_nds ' . ($userConfig->sales_invoice_order_sum_nds ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = TextHelper::invoiceMoneyFormat($data->getTotalPLNds(), 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
                'visible' => (bool)$isTaxTypeOsno,
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, SalesInvoice::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],
            [
                'attribute' => 'contractor_inn',
                'label' => 'ИНН контрагента',
                'headerOptions' => [
                    'class' => 'dropdown-filter col_sales_invoice_contractor_inn' . ($userConfig->sales_invoice_contractor_inn ? '' : ' hidden'),
                    'width' => '30%',
                ],
                'contentOptions' => [
                    'class' => 'contractor-cell col_sales_invoice_contractor_inn' . ($userConfig->sales_invoice_contractor_inn ? '' : ' hidden'),
                ],
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span title="' . Html::encode($data->invoice->contractor_inn) . '">' . Html::encode($data->invoice->contractor_inn) . '</span>';
                },
            ],
            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type, Yii::$app->request->queryParams),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    return $data->status->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],

            [
                'attribute' => 'invoice_document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data->invoice,])
                    ) {
                        if ($data->invoice->file !== null) {
                            $invoiceFileLinkClass = null;
                            $tooltipId = null;
                            $contentPreview = null;
                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                if ($thumb) {
                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                    $contentPreview .= Html::endTag('span');
                                    $contentPreview .= Html::endTag('div');
                                }
                            }

                            return Html::a($data->invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                            ]) . Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                '/documents/invoice/file-get',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                                'file-id' => $data->invoice->file->id,
                            ], [
                                'class' => $invoiceFileLinkClass,
                                'target' => '_blank',
                                'data-tooltip-content' => '#' . $tooltipId,
                                'style' => 'float: right',
                            ]) . $contentPreview;
                        } else {
                            return Html::a($data->invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                            ]);
                        }
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],

            [
                'attribute' => 'sales_invoice_payment_date',
                'label' => 'Дата оплаты',
                'headerOptions' => [
                    'class' => 'col_sales_invoice_payment_date ' . ($userConfig->sales_invoice_payment_date ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_sales_invoice_payment_date ' . ($userConfig->sales_invoice_payment_date ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    $date = $data->invoice->getLastPaymentDate();
                    return !$date ? "-" : date(DateHelper::FORMAT_USER_DATE, $date);
                },
            ],
            [
                'attribute' => 'sales_invoice_responsible',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'class' => 'col_sales_invoice_responsible' . ($userConfig->sales_invoice_responsible ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_sales_invoice_responsible' . ($userConfig->sales_invoice_responsible ? '' : ' hidden'),
                ],
                's2width' => '200px',
                'filter' => $searchModel->filterSalesInvoiceResponsible(),
                'format' => 'raw',
                'value' => function (SalesInvoice $data) {
                    $employee = Employee::findOne(['id' => $data->document_author_id]);
                    return $employee->getShortFio();
                },
            ],
            [
                'attribute' => 'sales_invoice_source',
                'label' => 'Источник',
                'headerOptions' => [
                    'class' => 'col_sales_invoice_source' . ($userConfig->sales_invoice_source ? '' : ' hidden'),
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'col_sales_invoice_source' . ($userConfig->sales_invoice_source ? '' : ' hidden'),
                ],
                'filter' => $searchModel->filterSalesInvoiceSource(),
                'format' => 'raw',
                'value' => function ($data) {
                    return 'КУБ24';
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT) : ?>
<?php /*
    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items',
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
*/ ?>
<?php endif; ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>'. Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr'
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_SALES_INVOICE,
    'documentTypeName' => 'расходную накладную'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_SALES_INVOICE
    ]); ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php \yii\bootstrap4\Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные расходные накладные?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php \yii\bootstrap4\Modal::end(); ?>
<?php endif ?>

<?php \yii\bootstrap4\Modal::begin([
    'id' => 'many-send-error',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
<h4 class="modal-title text-center mb-4">
    Ошибка при отправке расходных накладных
</h4>
<div class="modal-body">
    <div class="form-body">
    </div>
</div>
<div class="text-center">
    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
        Ок
    </button>
</div>
<?php \yii\bootstrap4\Modal::end(); ?>
