<?php
use frontend\rbac\permissions\document\Document;
use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\components\Icon;
use yii\helpers\Html;
use yii\helpers\Url;

if (Yii::$app->user->can(Document::VIEW, ['model' => $model->invoice])) :

    if (count($model->invoices) == 1): ?>

        <?php $invoiceTitle = 'Счет № ' . $model->invoice->fullNumber . ' от ' . DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>

        <?= Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']) . Html::tag('span', $invoiceTitle, ['class' => 'ml-3 mr-1']), [
            '/documents/invoice/view',
            'type' => $model->type,
            'id' => $model->invoice->id,
        ], [
            'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3',
        ]) ?>

    <?php else: ?>

        <div style="display: flex; justify-content: space-between;">
            <div class="dropdown w-100 pr-2">
                <?= \yii\bootstrap4\Html::a(Icon::get('new-doc').' ' . Html::tag('span', 'Счет', ['class' => 'ml-3 mr-1']), 'javascript:;', [
                    'class' => 'button-regular button-hover-content-red w-100 text-left dropdown-toggle pl-3',
                    'title' => 'Счет',
                    'data-toggle' => 'dropdown'
                ]) ?>
                <ul class="dropdown-menu documents-dropdown" style="right: -110px; left: auto;">
                    <?php $totalSum = 0; ?>
                    <?php foreach ($model->invoices as $item) : ?>
                        <?php
                        $url = [
                            'invoice/view',
                            'type' => $model->type,
                            'id' => $item->id,
                        ];
                        $date = DateHelper::format($item->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                        $totalSum += $item->totalAmountWithNds;
                        ?>
                        <li>
                            <a class="dropdown-item pl-3" style="position: relative;" href="<?= Url::to($url) ?>">
                                <span class="dropdown-span-left">
                                    № <?= $item->document_number ?> от <?= $date; ?>
                                </span>
                                <span class="dropdown-span-right">
                                    <?= TextHelper::invoiceMoneyFormat($item->totalAmountWithNds, 2) ?> ₽
                                </span>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <?= Html::tag('div', TextHelper::invoiceMoneyFormat($totalSum, 2).' ₽', [
                'class' => 'button-regular button-hover-content-red nowrap',
                'title' => "Сумма счетов",
            ]) ?>
        </div>

    <?php endif; ?>

<?php endif; ?>