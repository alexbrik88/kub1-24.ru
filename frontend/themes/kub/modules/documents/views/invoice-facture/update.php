<?php

use common\components\TextHelper;
use common\models\Company;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\document\Document;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;
use common\models\document\Order;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
/* @var $packing_lists \common\models\document\OrderPackingList */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$invoiceOrderCount = Order::find()
    ->andWhere(['in', 'invoice_id', ArrayHelper::getColumn($model->invoices, 'id')])
    ->count();
$hideAddBtn = $invoiceOrderCount === count($model->ownOrders);
$precision = $model->invoice->price_precision;
$cancelUrl = Url::previous('lastPage');
?>


<div class="form">
    <?= Html::beginForm('', 'post', [
        'id' => 'edit-invoice-facture',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>

    <?= $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
        'model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
    ]); ?>

    <div class="wrap">

        <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $precision,
        ]); ?>

        <div class="row align-flex-start justify-content-between mt-3">
            <div class="column">
                <span id="plusbtn"
                      class="btn-add-line-table button-regular button-hover-content-red <?= $hideAddBtn ? 'hide' : '' ?>"
                      data-maxrows="<?= $invoiceOrderCount ?>"
                      data-url="<?= Url::to(['select-product', 'type' => $model->type, 'id' => $model->id])?>">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </span>
            </div>
            <div class="column">
                <table class="total-txt">
                    <tr>
                        <td class="text-right">
                            <strong>Всего к оплате:</strong>
                        </td>
                        <td class="text-right">
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            Стоимость без налога:
                        </td>
                        <td class="text-right">
                            <strong>
                                <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2) ?>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            Сумма налога:
                        </td>
                        <td class="text-right">
                            <strong>
                                <?= TextHelper::invoiceMoneyFormat($model->totalNds, 2) ?>
                            </strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            Стоимость с налогом:
                        </td>
                        <td class="text-right">
                            <strong>
                                <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2) ?>
                            </strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                    'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', $cancelUrl, [
                    'class' => 'button-width button-clr button-regular button-hover-grey',
                ]); ?>
            </div>
        </div>
    </div>

    <?= Html::endForm(); ?>
</div>

<div id="add-products-to-list" class="modal fade t-p-f modal_scroll_center mobile-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Выбрать позицию из списка</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <div id="available-product-list"></div>
            </div>
        </div>
    </div>
</div>

<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs('
var checkPlusbtn = function() {
    if ($("tbody.document-orders-rows tr").length < $("#plusbtn").data("maxrows")) {
        $("#plusbtn").removeClass("hide");
    } else {
        $("#plusbtn").addClass("hide");
    }
};
var checkRowNumbers = function() {
    $(".document-orders-rows").children("tr").each(function(i, row) {
        $(row).find("span.row-number").text(++i);
    });
};
var checkServiceUnits = function() {
    if ($("#invoicefacture-show_service_units").is(":checked")) {
        $(".service_units_visible").removeClass("hide");
        $(".service_units_hidden").addClass("hide");
    } else {
        $(".service_units_visible").addClass("hide");
        $(".service_units_hidden").removeClass("hide");
    }
};
var toggleFieldsToForm = function() {
    $(".input-editable-field").removeClass("hide");
    $(".editable-field").addClass("hide");
};
checkPlusbtn();
checkRowNumbers();

$(document).on("change", "#invoicefacture-show_service_units", function() {
    checkServiceUnits();
});
$(document).on("click", "tr .delete-row", function() {
    var row = $(this).closest("tr");
    var table = row.parent();
    row.remove();
    checkRowNumbers();
    checkPlusbtn();
});
$(document).on("click", "#plusbtn", function() {
    $.post($(this).data("url"), $("#edit-invoice-facture").serialize(), function(data) {
        $("#available-product-list").html(data);
        $("#add-products-to-list").modal("show");
    });
});
$(document).on("click", "#add-selected-button", function() {
    $.post($(this).data("url"), $("#select-invoice-product-form").serialize(), function(data) {
        console.log(data);
        if (data.rows.length) {
            $.each(data.rows, function(i, item) {
                $(".document-orders-rows").append(item);
            });
        }
        toggleFieldsToForm();
        checkPlusbtn();
        checkRowNumbers();
        $("#available-product-list").html("");
        $(".document-orders-rows input:not(.md-check, .md-radiobtn)").uniform();
    });
});
');
?>
