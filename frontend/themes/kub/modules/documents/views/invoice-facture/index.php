<?php

use common\components\date\DateHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\status\InvoiceFactureStatus;
use common\models\project\Project;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\kub\components\Icon;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\documents\models\InvoiceFactureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$period = StatisticPeriod::getSessionName();
$company = Yii::$app->user->identity->company;
$exists = Invoice::find()->joinWith('invoiceFacture', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted(false)->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет счетов-фактур. Измените период, чтобы увидеть имеющиеся счета-фактур.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного счёта фактуры.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => InvoiceFactureStatus::STATUS_DELIVERED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if (Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)) {
    $dropItems[] = [
        'label' => 'Ответственный',
        'url' => '#mass_change_responsible',
        'linkOptions' => [
            'data' => [
                'toggle' => 'modal',
            ],
        ],
    ];
    echo frontend\widgets\MassChangeResponsibleWidget::widget([
        'formAction' => [
            'invoice/mass-set-responsible',
            'type' => $searchModel->type,
        ],
        'toggleButton' => false,
    ]);
}
if ($ioType == \frontend\models\Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => ['generate-xls'],
        'linkOptions' => [
            'class' => 'get-xls-link',
        ]
    ];
}

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_invoice_facture' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_invoice_facture');

$this->registerJs('
    $(document).on("change", ".autoinvoicefacture-rule-radio", function() {
        var additional = $(".autoinvoicefacture-rule-radio:checked").data("additional");
        $(".additional-rule.collapse.show").collapse("hide");
        $(additional).collapse("show");
        $("input[type=checkbox]", $(additional)).prop({disabled: false}).uniform("refresh");
    });
    $(document).on("hidden.bs.collapse", ".additional-rule.collapse", function () {
        $("input[type=checkbox]", this).prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.modal", "#autoinvoicefacture-form-modal", function () {
        $.pjax.reload("#autoinvoicefacture-form-pjax");
    });
    $(document).on("pjax:success", "#autoinvoicefacture-form-pjax", function () {
        $("input", this).uniform("refresh");
    });
');
?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()): ?>
        <?php
        Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как подготовить счета-фактуры, нужно создать Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php
        Modal::end();

        ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как подготовить счета-фактуры,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
            .
        </div>
    <?php endif; ?>
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="wrap wrap_count">
        <div class="row" style="">
            <div class="col-6 col-xl-9">
                Счета-фактуры отображают НДС и создаются по компаниям на НДС (ОСНО).<br/>
                Для ООО и ИП на УСН счета-фактуры не создаются.
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php Modal::begin([
                    'id' => 'autoinvoicefacture-form-modal',
                    'closeButton' => false,
                    'toggleButton' => [
                        'tag' => 'div',
                        'label' => 'АвтоСчетФактура',
                        'class' => 'button-regular w-100 button-hover-content-red mb-0',
                    ],
                ]); ?>

                <?= $this->render('autoinvoicefacture') ?>

                <?php Modal::end(); ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'invoice_facture_industry',
                    ],
                    [
                        'attribute' => 'invoice_facture_sale_point',
                    ],
                    [
                        'attribute' => 'invoice_facture_project',
                    ],
                ],
            ]); ?>
            <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]); ?>

            <?= TableViewWidget::widget(['attribute' => 'table_view_invoice_facture']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер счет-фактуры, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list'.$tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center',
                    'width' => '1%',
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'format' => 'raw',
                'value' => function (InvoiceFacture $model) {
                    return Html::checkbox('InvoiceFacture[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->totalAmountWithNds,
                        'data-contractor' => $model->invoice->contractor_id,
                        'data-responsible' => $model->invoice->responsible_employee_id,
                        'data-invoice-id' => $model->invoice->id,
                    ]);

                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата СФ',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'value' => function ($data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],
            [
                'attribute' => 'document_number',
                'label' => '№ СФ',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (InvoiceFacture $data) {
                    return (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                        ? Html::a($data->fullNumber, ['view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber);
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '70px',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->view->render('@documents/views/layouts/_doc-file-link', [
                        'model' => $model,
                    ]);
                },
            ],
            [
                'attribute' => 'totalAmountWithNds',
                'label' => 'Сумма',
                'format' => 'raw',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'value' => function (InvoiceFacture $data) {
                    $price = \common\components\TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '30%',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, InvoiceFacture::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (InvoiceFacture $data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],

            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                's2width' => '120px',
                'value' => function (InvoiceFacture $data) {
                    return $data->statusOut->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],
            [
                'attribute' => 'industry_id',
                'label' => 'Направление',
                'headerOptions' => [
                    'class' => 'col_invoice_facture_industry' . ($userConfig->invoice_facture_industry ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_facture_industry' . ($userConfig->invoice_facture_industry ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + CompanyIndustry::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (InvoiceFacture $model) {
                    return ($model->invoice->industry) ? Html::encode($model->invoice->industry->name) : '';
                },
            ],
            [
                'attribute' => 'sale_point_id',
                'label' => 'Точка продаж',
                'headerOptions' => [
                    'class' => 'col_invoice_facture_sale_point' . ($userConfig->invoice_facture_sale_point ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_facture_sale_point' . ($userConfig->invoice_facture_sale_point ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + SalePoint::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (InvoiceFacture $model) {
                    return ($model->invoice->salePoint) ? Html::encode($model->invoice->salePoint->name) : '';
                },
            ],
            [
                'attribute' => 'project_id',
                'label' => 'Проект',
                'headerOptions' => [
                    'class' => 'col_invoice_facture_project' . ($userConfig->invoice_facture_industry ? '' : ' hidden'),
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'col_invoice_facture_project' . ($userConfig->invoice_facture_project ? '' : ' hidden'),
                ],
                'filter' => ['' => 'Все'] + Project::getSelect2Data(),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (InvoiceFacture $model) {
                    return ($model->invoice->project) ? Html::encode($model->invoice->project->name) : '';
                },
            ],
            [
                'attribute' => 'invoice_document_number',
                'label' => 'Счёт №',
                'format' => 'raw',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'value' => function (InvoiceFacture $data) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                    ) {
                        if ($data->invoice->file !== null) {
                            $invoiceFileLinkClass = null;
                            $tooltipId = null;
                            $contentPreview = null;
                            if (in_array($data->invoice->file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
                                $invoiceFileLinkClass = 'invoice-file-link-preview';
                                $tooltipId = 'invoice-file-link-preview-' . $data->invoice->file->id;
                                $thumb = $data->invoice->file->getImageThumb(400, 600);
                                if ($thumb) {
                                    $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                                    $contentPreview .= Html::img($thumb, ['alt' => '']);
                                    $contentPreview .= Html::endTag('span');
                                    $contentPreview .= Html::endTag('div');
                                }
                            }

                            return Html::a($data->invoice->fullNumber,
                                    ['invoice/view', 'type' => $data->type, 'id' => $data->invoice_id])
                                . Html::a('<span class="pull-right icon icon-paper-clip"></span>',
                                    ['/documents/invoice/file-get', 'type' => $data->type, 'id' => $data->invoice->id, 'file-id' => $data->invoice->file->id,],
                                    [
                                        'class' => $invoiceFileLinkClass,
                                        'target' => '_blank',
                                        'data-tooltip-content' => '#' . $tooltipId,
                                        'style' => 'float: right',
                                    ]) . $contentPreview;
                        } else {
                            return Html::a($data->invoice->fullNumber,
                                ['invoice/view', 'type' => $data->type, 'id' => $data->invoice_id]);
                        }
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT): ?>
<?php /*
    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
 */ ?>
<?php endif; ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>'. Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr'
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_INVOICE_FACTURE,
    'documentTypeName' => 'Счет-фактуру'
]) ?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE_FACTURE
    ]); ?>
<?php endif; ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные счет фактуры?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>