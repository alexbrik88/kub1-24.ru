<?php
/** @var \common\models\document\InvoiceFacture $model */
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\OrderInvoiceFacture;
use common\models\product\Product;
use yii\helpers\Html;

$invoiceOrder = $order->order;
$product = $invoiceOrder->product;
$unitName = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
if ($order->quantity != intval($order->quantity)) {
    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
}
$amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
$amountWithNds = $model->getPrintOrderAmount($order->order_id);
?>

<tr role="row" class="odd">
    <td><?= $invoiceOrder->product_title; ?></td>
    <td>
        <?php if ($unitName == Product::DEFAULT_VALUE) : ?>
            <span><?= $unitName ?></span>
            <?= Html::hiddenInput("orderParams[{$invoiceOrder->id}][quantity]", 1) ?>
        <?php else : ?>

            <span class="input-editable-field">
                <?= Html::input('number', "orderParams[{$invoiceOrder->id}][quantity]", $order->quantity, [
                    'style' => 'width: 75px;',
                    'class' => 'form-control-number product-count width-100',
                    'max' => OrderInvoiceFacture::availableQuantity($invoiceOrder, $model->id),
                    'min' => 0,
                ]) ?>
            </span>
        <?php endif ?>
    </td>
    <td><?= $unitName ?></td>
    <td><?= $invoiceOrder->purchaseTaxRate->name; ?></td>
    <?php if ($model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) : ?>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($invoiceOrder->purchase_price_no_vat, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
    <?php else : ?>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($invoiceOrder->purchase_price_with_vat, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
    <?php endif; ?>
    <td>
        <?= Html::dropDownList(
            "orderParams[{$invoiceOrder->id}][country_id]",
            $order->country_id,
            $countryList,
            [
                'class' => 'form-control',
                'style' => 'width: 150px;',
            ]
        ); ?>
    </td>
    <td>
        <?= Html::textInput(
            "orderParams[{$invoiceOrder->id}][custom_declaration_number]",
            $order->custom_declaration_number,
            [
                'class' => 'form-control',
            ]
        ); ?>
    </td>
    <td class="text-center">
        <button class="delete-row button-clr" type="button">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#circle-close"></use>
            </svg>
        </button>
    </td>
</tr>