<?php
use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\InvoiceFacturePaymentDocument;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use \frontend\themes\kub\helpers\Icon;

/** @var \common\models\document\InvoiceFacture $model */
/* @var $message Message */
/** @var string $dateFormatted */
$paymentDocumentsArray = $model->paymentDocuments;
$paymentDocuments = [];
if ($paymentDocumentsArray) {
    foreach ($paymentDocumentsArray as $doc) {
        $date = date('d.m.Y', strtotime($doc->payment_document_date));
        $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
    }
} else {
    $paymentDocumentsArray[] = new InvoiceFacturePaymentDocument;
}

TooltipAsset::register($this);
$tooltipJs = '$("#document_date_item").tooltipster({"theme":["tooltipster-noir","tooltipster-noir-customized"],"trigger":"hover","side":"bottom"})';
if (Yii::$app->session->remove('show_document_date_tooltip')) {
    $tooltipJs .= '.tooltipster("open")';
    $tooltipJs .= "\n";
    $tooltipJs .= '$(document).on("click", function() {$("#document_date_item").tooltipster("close");});';
}
$tooltipJs .= ';';
$this->registerJs($tooltipJs);

$hasService = $model->getOrders()->joinWith('product product')
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])->exists();

$consignorList = ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($model->getConsignors()->all(), 'id', 'shortName'));
$model->consignor_id .= '';
$model->consignee_id .= '';

$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}
?>

    <div class="wrap">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="column">
                <div class="row row_indents_s flex-nowrap align-items-center">
                    <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Входящая СФ
                        </span>
                    </span>
                        <span>№</span>
                    </div>
                    <!-- document_number -->
                    <div class="form-group d-inline-block mb-0 col-xl-2">
                        <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                    <div class="form-txt d-inline-block mb-0 column">от</div>
                    <!-- document_date -->
                    <div class="form-group d-inline-block mb-0">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'document_date', [
                                'id' => 'under-date',
                                'class' => 'form-control form-control_small date-picker invoice_document_date',
                                'size' => 16,
                                'data-date' => '12-02-2012',
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->document_date,
                                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <span style="font-size:13px">По законодательству РФ, дата счет-фактуры не должна превышать 5 дней от даты акта или товарной накладной.</span>
    </div>

    <div class="wrap">
        <div class="row d-block">

            <!-- к платежно расч -->

            <div class="form-group col-6">
                <label class="label">Гос.контракт, договор (соглашение)</label>
                <?= Html::activeTextInput($model, 'state_contract', [
                    'maxlength' => true,
                    'class' => 'form-control input-editable-field ',
                    'placeholder' => '',
                ]); ?>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузоотправитель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignor_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $consignorList,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <br>
            <div class="form-group col-12">
                <div class="form-filter">
                    <label class="label" for="client">Поставщик<span class="important">*</span></label>
                    <div class="row">
                        <div class="col-6">
                            <?php echo Select2::widget([
                                'name' => 'contractor_id',
                                'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                                'disabled' => true,
                                'pluginOptions' => [
                                    'width' => '100%'
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузополучатель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignee_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $consignorList,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>

        </div>
    </div>

<?php /*
<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-9 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field ',
                'placeholder' => 'номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от
            <div class="input-icon input-calendar input-editable-field">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'id' => 'under-date',
                    'class' => 'form-control date-picker',
                    'size' => 16,
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">

            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm" style="color: #FFF;" title="Сохранить">
                    <span class="ico-Save-smart-pls"></span>
                </button>
                <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', ['view', 'type' => $model->type, 'id' => $model->id], [
                    'class' => 'btn darkblue btn-sm invoice-facture-reload input-editable-field',
                    'style' => "color: #FFF;",
                    'title' => "Отменить"
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                    <span><?= Html::a($model->invoice->contractor_name_short, [
                        '/contractor/view',
                        'type' => $model->invoice->contractor->type,
                        'id' => $model->invoice->contractor->id,
                    ]) ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    <div style="margin-bottom: 5px;">
                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
 */ ?>