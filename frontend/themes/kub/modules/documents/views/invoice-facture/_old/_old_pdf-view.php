<?php

/**
 * see: frontend/modules/documents/views/invoice-facture/pdf-view.php
 */

if ($model->document_date < '2021-07-01') {
    echo $this->render('pdf-view-2020', $_params_);
} else {
    echo $this->render('pdf-view-2021', $_params_);
}