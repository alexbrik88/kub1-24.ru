<?php

use common\models\document\Autoinvoicefacture;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Autoinvoicefacture */

if (empty($model)) {
    $company = Yii::$app->user->identity->company;
    $model = $company->autoinvoicefacture ? : new Autoinvoicefacture([
        'company_id' => $company->id,
        'rule' => Autoinvoicefacture::MANUAL,
        'send_auto' => false,
    ]);
}
?>

<h4 class="modal-title mb-4">Правила выставления АвтоСчетФактур</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<?php Pjax::begin([
    'id' => 'autoinvoicefacture-form-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'autoinvoicefacture-form',
    'action' => ['autoinvoicefacture'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'options' => [
        'class' => 'autoinvoicefacture-form',
        'data-pjax' => '',
    ],
]); ?>

    <?= $form->field($model, 'rule', ['options' => ['class' => '']])->label(false)->radioList(Autoinvoicefacture::$ruleLabels, [
        'encode' => false,
        'item' => function ($index, $label, $name, $checked, $value) use ($form, $model) {
            $ruleAdditional = 'additional-rule-' . $value;
            $comment = isset(Autoinvoicefacture::$ruleComments[$value]) ? Autoinvoicefacture::$ruleComments[$value] : '';
            $comment = Html::tag('div', $comment, [
                'style' => 'padding-left: 25px;',
            ]);
            $input = Html::radio($name, $checked, [
                'value' => $value,
                'class' => 'autoinvoicefacture-rule-radio',
                'data-additional' => '.' . $ruleAdditional,
                'label' => Html::tag('span', $label, [
                    'style' => 'font-weight: bold;',
                ]) . $comment,
            ]);
            if ($value == Autoinvoicefacture::BY_AUTO_DOC) {
                $input .= Html::tag('div', Html::activeCheckbox($model, 'send_auto'), [
                    'class' => 'additional-rule collapse ' . $ruleAdditional . ($checked ? ' show' : ''),
                    'style' => 'padding-left: 25px;',
                ]);
            }

            return Html::tag('div', $input, [
                'class' => 'form-group',
            ]);
        }
    ]); ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>