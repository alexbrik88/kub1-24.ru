<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use common\models\company;
use common\components\date\DateHelper;
use common\models\document\InvoiceFacture;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';
?>

<div class="page-content-in p-center width-1500 p-t">
    <div class="text-right font-size-10">
        <div>Приложение №1</div>
        <div>к постановлению Правительства Российской Федерации</div>
        <div>от 26 декабря 2011 г. № 1137</div>
    </div>
    <h4 class="font-bold">Счет-фактура № <?php echo $model->fullNumber; ?>
        от <?php echo $dateFormatted; ?></h4>

    <h4 class="font-bold m-t-10 m-b-0">Исправление № -- от --</h4>

    <div>Продавец: <?php echo $model->invoice->company_name_short; ?></div>
    <div>Адрес: <?php echo $model->invoice->company_address_legal_full; ?></div>
    <div>ИНН/КПП продавца: <?php echo $model->invoice->company_inn; ?>/<?php echo $model->invoice->company_kpp; ?></div>
    <p>Грузоотправитель и его адрес: <?= $model->invoice->production_type ? 'он же' : ''?></p>
    <p>Грузополучатель и его адрес:
        <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
            $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
        } else {
            $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
        }
        echo $model->invoice->production_type ? ($model->consignee ?
            $model->consignee->getRequisitesFull($address, false) :
            $model->invoice->contractor->getRequisitesFull($address, false)
        ) : \common\models\product\Product::DEFAULT_VALUE; ?>
    </p>
    <div>К платежно-расчетному документу № <?= $model->to_payment_document; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?></div>
    <div>Покупатель: <?php echo $model->invoice->contractor_name_short; ?></div>
    <div>Адрес: <?php echo $model->invoice->contractor_address_legal_full; ?></div>
    <div>ИНН/КПП покупателя: <?php echo $model->invoice->contractor_inn; ?>
        /<?php echo $model->invoice->contractor_kpp; ?></div>
    <div>Валюта: наименование, код Российский рубль, 643</div>

    <div class="portlet wide_table m-t-sm">
        <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType] . '_out-view', [
            'model' => $model,
        ]); ?>
    </div>

    <div class="row m-t-n-lg">
        <div class="col-md-12">
            <span class="m-r-lg text-top inline-block pull-left">Руководитель организации<br>или иное уполномоченное лицо</span>
            <div class="col-xs-1 b-b img-sign m-t-xl sign-pseudo m-r-sm"></div>
             <div class="col-xs-2 b-b fio-pseudo m-t-md">
                 <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                     <?= $model->invoice->getCompanyChiefFio(true); ?>
                 <?php endif; ?>
             </div>

            <span class="m-r-lg text-top inline-block m-l-80 pull-left">Главный бухгалтер<br>или иное уполномоченное лицо</span>
             <div class="col-xs-1 b-b img-sign sign-pseudo m-t-xl m-r-sm"></div>
             <div class="col-xs-2 b-b fio-pseudo m-t-md">
                <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                    <?= $model->invoice->getCompanyChiefAccountantFio(true); ?>
                <?php endif; ?>
             </div>
        </div>
    </div>

    <div class="row m-t-lg">
        <div class="col-md-12">
            <span class="m-r-lg text-top inline-block pull-left">Индивидуальный предприниматель</span>
             <div class="col-xs-1 b-b img-sign sign-pseudo m-r-sm"></div>
             <div class="col-xs-2 b-b fio-pseudo m-r-sm">
                 <?php if ($model->invoice->company->company_type_id == company\CompanyType::TYPE_IP): ?>
                     <?= $model->invoice->getCompanyChiefFio(true); ?>
                 <?php endif; ?>
             </div>
             <div class="col-xs-3 b-b img-sign rekv-pseudo text-center"></div>
        </div>
    </div>


</div>
