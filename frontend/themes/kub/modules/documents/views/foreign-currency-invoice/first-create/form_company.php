<?php

use common\models\company\CompanyType;
use common\components\widgets\BikTypeahead;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $account common\models\company\CheckingAccountant */
/* @var $form yii\widgets\ActiveForm */

$companyTypes = CompanyType::getCompanyTypeVariants($company->company_type_id);
$isIP = $company->company_type_id == CompanyType::TYPE_IP;
$account->isNewRecord = true;

if ($isIP) {
    $setData = <<<JS
        $('#company-chief_fio', form).val(suggestion.data.name.full);
        $('#company-egrip', form).val(suggestion.data.ogrn);
JS;
} else {
    $setData = <<<JS
        $('#company-kpp', form).val(suggestion.data.kpp);
        $('#company-ogrn', form).val(suggestion.data.ogrn);
        $('#company-name_short', form).val(suggestion.data.name.short);
        $('#company-chief_fio', form).val(suggestion.data.management.name);
JS;
}
?>

<div class="row">
    <div class="col-sm-5">
        <?= $form->field($company, 'inn')->textInput([
            'maxlength' => true,
        ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
    </div>
    <div class="col-sm-5 <?= $isIP ? 'hidden' : ''; ?>">
        <?= $form->field($company, 'kpp')->textInput([
            'maxlength' => true,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        <?= $form->field($company, 'company_type_id')->textInput([
            'disabled' => true,
            'value' => $company->companyType->name_short,
        ])->label('Форма') ?>
    </div>
    <div class="col-sm-10">
        <?php if ($isIP) : ?>
            <?= $form->field($company, 'chief_fio')->textInput([
                'maxlength' => true,
            ])->label('Наименование') ?>
        <?php else : ?>
            <?= $form->field($company, 'name_full')->textInput([
                'maxlength' => true,
            ])->label('Наименование') ?>
        <?php endif ?>
    </div>
</div>

<?php if (!$isIP) : ?>
    <div class="row">
        <div class="col-sm-5">
            <?= $form->field($company, 'chief_post_name')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
        <div class="col-sm-7">
            <?= $form->field($company, 'chief_fio')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
    </div>
<?php endif ?>

<?= $form->field($company, 'address_legal')->textInput([
    'maxlength' => true,
]) ?>

<?= $form->field($company, 'phone')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
    'options' => [
        'class' => 'form-control input-sm' . ($company->phone ? ' edited' : ''),
    ],
    'clientOptions' => [
        'showMaskOnHover' => false,
    ]
]) ?>

<div class="row">
    <div class="col-sm-7">
        <?= $form->field($account, 'rs')->textInput([
            'maxlength' => true,
        ])->label('Расчётный счёт') ?>
    </div>
    <div class="col-sm-5">
        <?= $form->field($account, 'bik')->textInput([
            'maxlength' => true,
            'class' => 'form-control input-sm dictionary-bik' . ($account->bik ? ' edited' : ''),
            'autocomplete' => 'off',
            'data' => [
                'url' => Url::to(['/dictionary/bik']),
                'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                'target-ks' => '#' . Html::getInputId($account, 'ks'),
                'target-collapse' => '#company-bank-block',
            ]
        ])->label('БИК вашего банка') ?>
    </div>
</div>

<div id="company-bank-block" class="collapse<?= $account->bik ? ' in' : ''; ?>">
    <div class="row">
        <div class="col-sm-7">
            <?= $form->field($account, 'bank_name')->textInput([
                'maxlength' => true,
                'readonly' => true,
            ]) ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($account, 'bank_city')->textInput([
                'maxlength' => true,
                'readonly' => true,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <?= $form->field($account, 'ks')->textInput([
                'maxlength' => true,
                'readonly' => true,
            ]) ?>
        </div>
    </div>
</div>

<?= Html::activeHiddenInput($company, 'ogrn'); ?>
<?= Html::activeHiddenInput($company, 'egrip'); ?>

<?php
$this->registerJs(<<<JS
    $('#first-invoice-form #company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            var form = $('#first-invoice-form');
            $('#company-inn', form).val(suggestion.data.inn);
            $('#company-okpo', form).val(suggestion.data.okpo);
            $('#company-okved', form).val(suggestion.data.okved);
            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal', form).val(address);
            {$setData}

            $('#first-invoice-form .invoice-form-company .form-control').each(function(){
                $(this).toggleClass('edited', $(this).val().length > 0);
            });
        }
    });
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            $($(this).data('target-collapse')).collapse('show');

            $('#first-invoice-form .invoice-form-company .form-control').each(function(){
                $(this).toggleClass('edited', $(this).val().length > 0);
            });
        }
    });
JS
);
