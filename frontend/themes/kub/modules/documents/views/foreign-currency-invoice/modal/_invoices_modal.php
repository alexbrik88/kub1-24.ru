<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>

<?php Modal::begin([
    'id' => 'accounts-list',
    'closeButton' => false,
    'toggleButton' => false,
]); ?>

    <?php Pjax::begin([
        'id' => 'get-vacant-invoices-pjax',
        'formSelector' => '#search-invoice-form',
        'timeout' => 5000,
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

        <h4 class="modal-title mb-4">Добавить <?= $documentTypeName ?></h4>
        <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
            <svg class="svg-icon">
                <use xlink:href="/img/svg/svgSprite.svg#close"></use>
            </svg>
        </button>

        <div class="form-group invoice-list"></div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Добавить', [
                'id' => 'add-act-to-invoice',
                'class' => 'button-regular button-width button-regular_red button-clr',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'style' => 'width: 130px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    <?php Pjax::end(); ?>

<?php Modal::end(); ?>

<?php
$this->registerJs("
    $('.add-document').on('click', function () {
        $('#accounts-list').modal();
        $.pjax({
            url: '/documents/{$documentType}/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {type: {$ioType}},
            push: false,
            timeout: 5000
        });
    });
    $(document).on('pjax:success', '#get-vacant-invoices-pjax', function () {
        $('input', this).uniform('refresh');
    });
    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"label_name\"]').val(picker.chosenLabel);
        $('#search-invoice-form').submit();
    });

    $(document).on('click', '.checkbox-invoice-id, #add-new-invoice', function() {
        var checkboxes = $('.checkbox-invoice-id');
        var button = $('#add-act-to-invoice');
        var addNew = $('#add-new-invoice');
        if ($(checkboxes).filter(':checked').length || $(addNew).prop('checked')) {
            $(button).removeAttr('disabled');
            if ($(checkboxes).filter(':checked').length) {
                $(addNew).prop('checked', false).attr('disabled', true).uniform();
            }
        } else {
            $(button).attr('disabled', 'disabled');
            $(addNew).removeAttr('disabled').uniform();
        }
        if ($(checkboxes).filter(':checked').length > 1) {
            $(checkboxes).filter(':checked').prop('checked', false);
            $(this).prop('checked', true);
            $(checkboxes).uniform();
        }
    });

    $(document).on('click', '#add-act-to-invoice', function () {
        var checkboxes = $('.checkbox-invoice-id');
        var isAddNew = $('#add-new-invoice').filter(':checked').length;

        if (isAddNew) {
            location.href = $(this).data('url-new-invoice')
        } else {
            var invoiceId = $(checkboxes).filter(':checked').first().val();
            location.href = $(this).data('url') + '&invoiceId=' + invoiceId;
        }
    });

"); ?>

