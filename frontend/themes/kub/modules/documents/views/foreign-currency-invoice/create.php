<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ForeignCurrencyInvoice */
/* @var $rsArray CheckingAccountant[] */
/* @var $company Company */
/* @var $employee Employee */
/* @var $ioType integer */
/* @var $fixedContractor boolean */

$this->title = 'Создать ' . $model->printablePrefix;

$isFirstCreate = \Yii::$app->controller->action->id === 'first-create';
?>

<div class="documents_foreign-currency-invoice_create">
    <?= $this->render('_form', [
        'model' => $model,
        'rsArray' => $rsArray,
        'invoiceEssence' => $invoiceEssence,
        'company' => $company,
        'employee' => $employee,
        'ioType' => $ioType,
        'fixedContractor' => $fixedContractor,
        'isFirstCreate' => $isFirstCreate,
    ]); ?>
</div>