<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */

?>

<button class="link link_collapse link_bold button-clr mb-4 collapsed" type="button" data-toggle="collapse" data-target="#dopColumns2" aria-expanded="false" aria-controls="dopColumns2">
    <span class="link-txt">Указать дополнительные данные и банковские реквизиты</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
    </svg>
</button>

<div id="dopColumns2" class="collapse dopColumns">
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'director_post_name')->label('Должность руководителя'); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'director_name')->label('ФИО руководителя'); ?>
        </div>
    </div>
    <?= $form->field($model, 'legal_address')->textInput([
        'maxlength' => true,
        'placeholder' => 'Пример: USA, NY, street, etc',
    ]); ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'current_account')->textInput([
                'maxlength' => true,
                'placeholder' => '0123456789',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'BIC')->textInput([
                'placeholder' => 'ALFABANK',
                'autocomplete' => 'off',
            ])->label('SWIFT'); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'bank_name')->textInput([
                'maxlength' => true,
                'placeholder' => 'ALFABANK',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'bank_address')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: USA, NY, street, etc',
            ]); ?>
        </div>
    </div>
</div>