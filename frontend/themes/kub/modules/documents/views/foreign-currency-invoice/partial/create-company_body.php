<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;

$this->registerJs('
$("#company-has_chief_patronymic:not(.md-check)");
$(document).on("change", "#company-has_chief_patronymic", function () {
    if ($(this).prop("checked")) {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .val("")
            .attr("readonly", true)
            .trigger("change");
        $(".field-company-chief_patronymic, .field-company-ip_patronymic")
            .removeClass("has-error")
            .find(".help-block").html("");
    } else {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .attr("readonly", false);
    }
});
');

$form = ActiveForm::begin([
    'id' => 'new-company-invoice-form',
    'validationUrl' => ['validate-company-modal'],
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
    'options' => [
        'data-ruble' => 0,
        'data-foreign' => 0,
    ],
]);
?>

<?= Html::activeHiddenInput($model, 'self_employed') ?>

<?php /*if ($model->self_employed) : ?>
    <?= $this->render('_self', [
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php elseif ($model->company_type_id == CompanyType::TYPE_IP) : ?>
    <?= $this->render('_ip', [
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php else : ?>
    <?= $this->render('_other', [
        'model' => $model,
        'form' => $form,
    ]) ?>
<?php endif;*/ ?>

<div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'name_short_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: KUB',
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'form_legal_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: LLC',
            ]); ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'name_full_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Limited Liability Company «KUB»',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'address_legal_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Russia, Moscow, street, etc.',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'address_actual_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Russia, Moscow, street, etc.',
            ]); ?>
        </div>
    </div>
    <div class="my-2">
        Данные руководителя на английском
    </div>
    <div class="row">
        <div class="col-md-3 left-column">
            <?= $form->field($model, 'chief_post_name_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Chief Executive Officer',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'lastname_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petrov',
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'firstname_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petr',
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'patronymic_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petrovich',
            ]); ?>
        </div>
        <div class="col-md-3">
            <label class="label">&nbsp;</label>
            <div class="mt-2">
                <?= $form->field($model, 'not_has_patronymic_en')->checkbox([
                    'class' => '',
                    'labelOptions' => [
                        'class' => 'label mb-0',
                    ],
                ], true); ?>
            </div>
        </div>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= \yii\bootstrap4\Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr form-submit-btn',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>

<?php ActiveForm::end(); ?>
