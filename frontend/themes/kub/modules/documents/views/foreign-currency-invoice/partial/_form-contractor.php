<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use frontend\models\Documents;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */

$typeLabel = $model->type == Contractor::TYPE_SELLER ? 'Тип поставщика' : 'Тип покупателя';
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => '',
        'data-find-duplicate' => Url::to([
            '/contractor/find-duplicate',
            'id' => $model->id,
            'type' => $documentType,
        ]),
    ],
    'id' => 'new-contractor-invoice-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

<?= Html::hiddenInput('documentType', $documentType); ?>

<?php // echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'face_type')->label($typeLabel)->radioList([
            Contractor::TYPE_FOREIGN_LEGAL_PERSON => 'Иностранное юр.лицо',
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'ITN')->label('ИНН/ITN')->textInput([
            'placeholder' => 'Пример: 0123456789',
            'maxlength' => true,
        ]); ?>
    </div>
</div>

<?= $this->render('_partial_main_info', [
    'model' => $model,
    'form' => $form,
]) ?>

<?= $this->render('_partial_details', [
    'model' => $model,
    'form' => $form,
]) ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    function contractorIsIp() {
        return false;
    }
    function contractorIsNotIp() {
        return false;
    }

    function contractorIsPhysical() {
        return false;
    }
    function contractorIsLegal() {
        return false;
    }
    function contractorIsForeignLegal() {
        return true;
    }
    function contractorIsAgent() {
        return false;
    }
</script>
