<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'inn')->label('ИНН:')->textInput([
            'id' => 'self_employed-inn',
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-12">
        <?= \common\components\helpers\Html::label('ФИО', null, [
            'class' => 'label',
        ]); ?>
    </div>
    <div class="col-12">
        <div class="row required">
            <div class="col-3">
                <?= $form->field($model, 'ip_lastname')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Фамилия',
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_firstname')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Имя',
                        'class' => 'form-control'
                    ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_patronymic')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Отчество',
                        'class' => 'form-control',
                        'readonly' => (boolean) $model->has_chief_patronymic,
                    ]); ?>
            </div>
            <div class="col-3 pt-2">
                <?= Html::activeCheckbox($model, 'has_chief_patronymic') ?>
            </div>
        </div>
    </div>
</div>

<?= $form->field($model, 'address_legal')->widget(\yii\widgets\MaskedInput::class, [
    'mask' => '9{6}, ~{1,245}',
    'definitions' => [
        '~' => [
            'validator' => '[ 0-9A-zА-я.,/-]',
            'cardinality' => 1,
        ],
    ],
    'options' => [
        'class' => 'form-control',
        'placeholder' => 'Индекс, Адрес',
    ],
]); ?>
