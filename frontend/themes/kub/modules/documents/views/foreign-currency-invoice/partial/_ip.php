<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'inn')->label('ИНН:')->textInput([
            'placeholder' => 'Автозаполнение по ИНН',
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'egrip')->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-12">
        <?= \common\components\helpers\Html::label('ФИО Руководителя', null, [
            'class' => 'label',
        ]); ?>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ip_lastname')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Фамилия',
                    ]);
                ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_firstname')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Имя',
                    ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'ip_patronymic')
                    ->label(false)
                    ->textInput([
                        'placeholder' => 'Отчество',
                        'readonly' => (boolean) $model->has_chief_patronymic,
                    ]); ?>
            </div>
            <div class="col-3 pt-2">
                <?= Html::activeCheckbox($model, 'has_chief_patronymic') ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <?= $form->field($model, 'address_legal')->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '9{6}, ~{1,245}',
            'definitions' => [
                '~' => [
                    'validator' => '[ 0-9A-zА-я.,/-]',
                    'cardinality' => 1,
                ],
            ],
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Индекс, Адрес',
            ],
        ]); ?>
    </div>
</div>

<?php
$this->registerJs(<<<JS
$('#new-company-invoice-form #company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-inn').val(suggestion.data.inn);

            var nameFullArray = suggestion.data.name.full.split(' ');
            $('#new-company-invoice-form #company-ip_lastname').val(nameFullArray[0]);
            $('#new-company-invoice-form #company-ip_firstname').val(nameFullArray[1]);
            $('#new-company-invoice-form #company-ip_patronymic').val(nameFullArray[2]);

            $('#new-company-invoice-form #company-egrip').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);
        }
    });
JS
);