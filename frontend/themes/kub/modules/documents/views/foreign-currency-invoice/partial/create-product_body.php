<?php
use common\models\product\Product;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $document string */

$document = isset($document) ? $document : null;

$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;

?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'new-product-invoice-form',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => '',
        ]
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
])); ?>

    <div class="form-body">

        <?= Html::hiddenInput('documentType', $documentType); ?>

        <?= $this->render('_mainForm', [
            'model' => $model,
            'form' => $form,
            'documentType' => $documentType,
            'document' => $document,
        ]); ?>
        <br>
        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'id' => 'product-form-submit',
                'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
            ]) ?>
            <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
        </div>

    </div>

<?php ActiveForm::end();
