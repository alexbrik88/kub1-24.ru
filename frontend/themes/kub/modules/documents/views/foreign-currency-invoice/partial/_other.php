<?php

use common\models\company\CompanyType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

if (!$model->address_legal_house_type_id) {
    $model->address_legal_house_type_id = \common\models\address\AddressHouseType::TYPE_HOUSE;
}
if (!$model->address_legal_apartment_type_id) {
    $model->address_legal_apartment_type_id = \common\models\address\AddressApartmentType::TYPE_APARTMENT;
}

$companyTypes = CompanyType::getCompanyTypeVariants($model->company_type_id);

?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'inn')->label('ИНН:')->textInput([
            'placeholder' => 'Автозаполнение по ИНН',
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'kpp')->textInput([
            'maxlength' => true,
        ]) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'name_short')->textInput([
            'placeholder' => 'Без ООО/ИП',
            'maxlength' => true,
            'disabled' => $model->company_type_id == CompanyType::TYPE_IP,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
                'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
            ],
        ]) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'company_type_id', [
            'options' => [
                'id' => 'contractor_company_type',
            ],
        ])->label('Форма')->widget(\kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map($companyTypes, 'id', 'name_short'),
            'disabled' => count($companyTypes) === 1,
        ]) ?>
    </div>
    <div class="col-12">
        <?= Html::label('ФИО Руководителя', null, [
            'class' => 'label',
        ]); ?>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'chief_lastname')->label(false)->textInput([
                    'placeholder' => 'Фамилия',
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'chief_firstname')->label(false)->textInput([
                    'placeholder' => 'Имя',
                    'class' => 'form-control'
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'chief_patronymic')->label(false)->textInput([
                    'placeholder' => 'Отчество',
                    'class' => 'form-control',
                    'readonly' => (boolean) $model->has_chief_patronymic,
                ]) ?>
            </div>
            <div class="col-3 pt-2">
                <?= Html::activeCheckbox($model, 'has_chief_patronymic') ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <?= $form->field($model, 'address_legal')->widget(\yii\widgets\MaskedInput::class, [
            'mask' => '9{6}, ~{1,245}',
            'definitions' => [
                '~' => [
                    'validator' => '[ 0-9A-zА-я.,/-]',
                    'cardinality' => 1,
                ],
            ],
            'options' => [
                'class' => 'form-control',
                'placeholder' => 'Индекс, Адрес',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control',
                'placeholder' => '+7(XXX) XXX-XX-XX (Для восстановления пароля)',
            ],
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'ogrn')->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
</div>

<?php
$this->registerJs(<<<JS
    $('[id="company-inn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#new-company-invoice-form #company-name_short').val(suggestion.data.name.short);
            $('#company-inn').val(suggestion.data.inn);
            $('#company-kpp').val(suggestion.data.kpp);
            $('#company-ogrn').val(suggestion.data.ogrn);

            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#company-address_legal').val(address);

            var nameFull = suggestion.data.management.name;
                var lastName = '';
                var firstName = '';
                var patronymic = '';
                var endLastNamePlace = nameFull.indexOf(' ');
                for (var i = 0; i < endLastNamePlace; i++) {
                    lastName += nameFull[i];
                }
                var endNamePlace = nameFull.indexOf(' ', endLastNamePlace+1);
                for (var i = endLastNamePlace+1; i < endNamePlace; i++) {
                    firstName += nameFull[i];
                }
                for (var i = endNamePlace+1; i < nameFull.length; i++) {
                    patronymic += nameFull[i];
                }
                $('#company-chief_lastname').val(lastName);
                $('#company-chief_firstname').val(firstName);
                $('#company-chief_patronymic').val(patronymic);
        }
    });
JS
);