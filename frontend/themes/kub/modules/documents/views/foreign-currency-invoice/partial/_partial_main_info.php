<?php

use common\components\helpers\Html;
use common\models\Contractor;
use common\models\address\Country;
use frontend\models\Documents;
use kartik\select2\Select2;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$nameLabel = $model->type == Contractor::TYPE_SELLER ? 'Название поставщика' : 'Название покупателя';
$emailLabel = $model->type == Contractor::TYPE_SELLER ? 'Email поставщика' : 'Email покупателя';
$emailLabel .= ' (для отправки счетов)';

?>

<?= Html::activeHiddenInput($model, 'type'); ?>
<?= Html::hiddenInput(null, $model->isNewRecord, [
    'id' => 'contractor-is_new_record',
]); ?>

<div class="row">
    <div class="col-12">
        <?= $form->field($model, 'name')->label($nameLabel)->hint(false)->textInput([
            'placeholder' => 'Пример: Company name',
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-3">
        <?= $form->field($model, 'foreign_legal_form')->label('Форма')->textInput([
            'placeholder' => 'Пример: LCC',
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-9">
        <?= $form->field($model, 'decoding_legal_form')->label('Pacшифровка формы')->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'country_id')->widget(Select2::class, [
            'data' => Country::find()->select(['name_short', 'id'])->indexBy('id')->column(),
            //'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ]
        ])->label('Страна'); ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'director_email')->label($emailLabel)->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
</div>
