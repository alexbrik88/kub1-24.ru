<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model ForeignCurrencyInvoice */
/* @var $rsArray CheckingAccountant[] */
/* @var $company Company */
/* @var $employee Employee */
/* @var $ioType integer */
/* @var $fixedContractor boolean */

$this->title = 'Редактировать ' . $model->printablePrefix.' №'.$model->getFullNumber();

?>

<div class="documents_foreign-currency-invoice_update">
    <?= $this->render('_form', [
        'model' => $model,
        'rsArray' => $rsArray,
        'invoiceEssence' => $invoiceEssence,
        'company' => $company,
        'employee' => $employee,
        'ioType' => $ioType,
        'fixedContractor' => $fixedContractor,
        'isFirstCreate' => false,
    ]); ?>
</div>