<?php

use frontend\models\PasswordResetRequestForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\PasswordResetRequestForm */

?>

<?php Pjax::begin([
    'id' => 'out-view-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<div class="row">
    <div class="col-sm-12 form-header border-bottom-e4">
        Пожалуйста введите Ваш e-mail. На него будет отправлена ссылка для восстановления пароля.
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php $form = ActiveForm::begin([
            'id' => 'request-password-reset-form',
            'options' => [
                'data-pjax' => '',
            ],
        ]); ?>
            <?= $form->field($model, 'email') ?>
            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'button-regular button-hover-transparent']) ?>
            </div>
        <?php ActiveForm::end(); ?>

        <?= Html::a('Войти в систему', [
            'out-view-login',
            'uid' => $invoice->uid,
            'email' => $email,
        ]) ?>
    </div>
</div>

<?php Pjax::end(); ?>
