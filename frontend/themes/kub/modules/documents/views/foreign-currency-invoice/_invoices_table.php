<?php

use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\rbac\UserRole;
use frontend\rbac\permissions\document\Document as DocumentPermissions;
use frontend\widgets\BoolleanSwitchWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;


/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel InvoiceSearch */
/* @var $company \common\models\Company */
/* @var $useContractor boolean
 * @var $this \yii\web\View
 * @var $user \common\models\employee\Employee
 */

$period = StatisticPeriod::getSessionName();
$user = Yii::$app->user->identity;
if (empty($company)) {
    $company = $user->company;
}

$userConfig = $user->config;
$isContractor = $this->context->id == 'contractor';
$tabConfig = [
    'scan' => $isContractor ? $userConfig->contr_inv_scan : $userConfig->invoice_scan,
    'paydate' => $isContractor ? $userConfig->contr_inv_paydate : $userConfig->invoice_paydate,
    'paylimit' => $isContractor ? $userConfig->contr_inv_paylimit : $userConfig->invoice_paylimit,
    'author' => $isContractor ? $userConfig->contr_inv_author : $userConfig->invoice_author,
    'comment' => $isContractor ? $userConfig->contr_inv_comment : $userConfig->invoice_comment,
];
if (!isset($tabViewClass)) {
    $tabViewClass = $userConfig->getTableViewClass('table_view_document');
}

if ($type != Documents::IO_TYPE_OUT) // todo: ТТН для входящих счетов
    $tabConfig['waybill'] = false;

$existsQuery = Invoice::find()->byCompany($company->id)->byDeleted(false)->byIOType($type);
if (isset($id)) {
    $existsQuery->byContractorId($id);
}
$exists = $existsQuery->exists();

$isFilter = (boolean)($searchModel->search);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет счётов. Измените период, чтобы увидеть имеющиеся счета.";
    }
} else {
    if ($type == Documents::IO_TYPE_OUT) {
        $emptyMessage = 'Вы еще не создали ни одного счёта. ' . Html::a('Создать инвойс.', [
                '/documents/foreign-currency-invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } elseif ($type == Documents::IO_TYPE_IN) {
        $emptyMessage = 'Вы еще не загрузили ни одного счета от поставщика. ' . Html::a('Загрузить инвойс.', [
                '/documents/foreign-currency-invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$useContractor = isset($useContractor) ? $useContractor : false;

$statusFilterItems = [];
foreach ($searchModel->getStatusItemsByQuery($dataProvider->query) as $key => $value) {
    $statusFilterItems[$key] = $value;
    if ($key == InvoiceStatus::STATUS_PAYED) {
        foreach (Invoice::$paymentTypeData as $typeData) {
            $statusFilterItems["{$key},{$typeData['alias']}"] = Html::tag('span', $value, [
                    'style' => 'padding: 0 10px;'
                ]) . $typeData['icon'];
        }
    }
}

echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-dark'],
        'trigger' => 'click',
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-currency',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]); ?>
<style>
    .update-attribute-tooltip-content {
        text-align: center;
    }
</style>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table' . $tabViewClass,
    ],
    'rowOptions' => function ($model) {
        return $model->invoice_status_id == InvoiceStatus::STATUS_PAYED ? ['class' => 'invoice-payed'] : [];
    },
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '2%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function ($model) {
                return Html::checkbox('Invoice[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data-sum' => $model->total_amount_with_nds,
                    'data-contractor' => $model->contractor_id,
                    'data-responsible' => $model->responsible_employee_id,
                    'data-invoice-id' => $model->id,
                ]);
            },
        ],
        [
            'attribute' => 'document_date',
            'label' => 'Дата инвойса',
            'headerOptions' => [
                //'class' => 'sorting',
                'width' => '10%',
                'style' => 'min-width:50px'
            ],
            'contentOptions' => [
                'class' => 'link-view',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ инвойса',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'document_number link-view',
            ],
            'format' => 'raw',
            'value' => function ($model) use ($useContractor) {
                $canView = Yii::$app->user->can(DocumentPermissions::VIEW, [
                    'ioType' => $model->type,
                    'model' => $model,
                ]);
                if ($canView) {
                    $link = Html::a($model->fullNumber, ['/documents/foreign-currency-invoice/view',
                        'type' => $model->type,
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->contractor_id : null),
                    ], ['class' => 'link']);
                } else {
                    $link = $model->fullNumber;
                }
                $fullName = '№' . $model->document_number . ' от ' . DateHelper::format(
                        $model->document_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    );

                return Html::tag('span', $link, ['data-full-name' => $fullName]);
            },
        ],
        [
            'label' => 'Скан',
            'headerOptions' => [
                'class' => 'col_invoice_scan col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_scan link-view col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
            ],
            'attribute' => 'has_file',
            'format' => 'raw',
            'value' => function ($model) {
                return DocumentFileWidget::widget(['model' => $model]);
            },
        ],
        [
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'min-width:50px'
            ],
            'contentOptions' => [
                'class' => 'link-view',
                'style' => 'white-space: nowrap;',
            ],
            'attribute' => 'total_amount_with_nds',
            'format' => 'raw',
            'value' => function ($model) {
                return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2).' '.$model->currency_name;
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контр&shy;агент',
            'encodeLabel' => false,
            'contentOptions' => [
                'class' => 'contractor-cell',
            ],
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '30%',
            ],
            'filter' => $searchModel->getContractorItems(),
            'hideSearch' => false,
            's2width' => '300px',
            'format' => 'raw',
            'value' => function ($model) {
                $name = Html::encode($model->contractor_name_short);
                return Html::a($name, [
                    '/contractor/view',
                    'type' => $model->type,
                    'id' => $model->contractor_id,
                ], [
                    'class' => 'link',
                    'title' => html_entity_decode($name),
                    'data' => [
                        'id' => $model->contractor_id,
                    ]
                ]);
            },
            'visible' => ($useContractor ? false : true),
        ],
        [
            'attribute' => 'invoice_status_id',
            'label' => 'Статус',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '10%',
                'style' => 'min-width:50px'
            ],
            'contentOptions' => function ($model) {
                $options = [
                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap;',
                    'class' => '',
                ];
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                    $options['class'] .= ' tooltip2';
                    $options['data-tooltip-content'] = '#tooltip_invoice-sum-' . $model->id;
                }

                return $options;
            },
            'selectPluginOptions' => [
                'templateResult' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
                'templateSelection' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
            ],
            'filter' => $statusFilterItems,
            's2width' => '170px',
            'format' => 'raw',
            'value' => function ($model) {
                $tooltipAmount = '';
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                    $tooltipAmount = '<span class="tooltip-template"  style="display:none;"><span id="tooltip_invoice-sum-' . $model->id . '">
                    <span style="color: #45b6af;">Оплачено: ' . TextHelper::invoiceMoneyFormat($model->paid_amount, 2) . ' руб.</span></br>
                    <span style="color: #f3565d;">Задолженность: ' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . ' руб.</span>
                    </span></span>';
                }
                $icon = $model->invoice_status_id == InvoiceStatus::STATUS_PAYED ?
                    Html::tag('span', $model->getPaymentIcon(), ['style' => 'color: #0097FD; font-size: 14px;']) : '';

                return $model->invoiceStatus->name . " $icon" . $tooltipAmount;
            },
        ],
        [
            'attribute' => 'payment_limit_date',
            'format' => 'date',
            'headerOptions' => [
                'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:50px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
            ],
            'visible' => true,
        ],
        [
            'attribute' => 'payDate',
            'label' => 'Дата оплаты',
            'headerOptions' => [
                'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:50px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
            ],
            'value' => function ($model) {
                return $model->payDate ? date('d.m.Y', $model->payDate) : '';
            },
            'visible' => true,
        ],
        [
            'attribute' => 'responsible_employee_id',
            'label' => 'От&shy;вет&shy;ствен&shy;ный',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:150px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getResponsibleEmployeeByQuery($dataProvider->query),
            's2width' => '200px',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->responsible ? $model->responsible->getFio(true) : '';
            },
            'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
        ],
        [
            'attribute' => 'comment_internal',
            'label' => 'Ком&shy;мен&shy;та&shy;рий',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:150px'
            ],
            'contentOptions' => function ($model) use ($tabConfig) {
                $options = [
                    'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap; max-width:90px;',
                ];
                if ($model->comment_internal) {
                    $options['class'] .= ' tooltip2';
                    $options['data-tooltip-content'] = "#tooltip_invoice_comment_{$model->id}";
                }

                return $options;
            },
            'format' => 'raw',
            'value' => function ($model) {
                $tooltip = '';
                if ($model->comment_internal) {
                    $tooltip = '
                    <span class="tooltip-template" style="display:none;">
                        <span id="tooltip_invoice_comment_' . $model->id . '">
                        ' . $model->comment_internal . '
                        </span>
                    </span>';
                }

                return $model->comment_internal . $tooltip;
            },
        ],
    ],
]);

echo $this->render('../invoice-facture/_form_create', [
    'useContractor' => $useContractor,
]);

Modal::begin([
    'id' => 'no-rights-modal',
    'closeButton' => false,
    'toggleButton' => false,
]);
echo Html::button('×', ['class' => 'close', 'type' => 'button', 'data-dismiss' => 'modal', 'aria-hidden' => 'true']);
echo Html::tag('span', 'У Вас нет прав на данное действие.', ['class' => 'center-block text-center']);
Modal::end();

$contractorDuplicateLink = Url::to(['/contractor/index', 'type' => $searchModel->type, 'ContractorSearch' => [
    'duplicate' => 1,
]]);
$this->registerJs('
    $(document).on("click", "a.no-rights-link", function(e) {
        e.preventDefault();
        $("#no-rights-modal").modal("show");
    });
    $(".act-file-link-preview, .packing-list-file-link-preview, .invoice-facture-file-link-preview, .upd-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
    $(document).on("change", "#invoicesearch-contractor_id", function (e) {
        if ($(this).val() == "duplicate") {
            location.href = "' . $contractorDuplicateLink . '";
        }
    });

    $("input:checkbox:not(.md-check)").uniform(\'refresh\');
');
?>
