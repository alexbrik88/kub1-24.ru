<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\EmployeeRole;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\widgets\BoolleanSwitchWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $invoiceFlowCalc common\models\document\InvoiceFlowCalculator */
/* @var $useContractor string */
/* @var $user \common\models\employee\Employee */

$status = $model->invoiceStatus;
$styleClass = $model->isOverdue() ? 'red' : $status->getStyleClass();
$user = Yii::$app->user->identity;

$createdInvoicesCount = $model->company->getInvoices()->where(['from_demo_out_invoice' => 0])->count();

$statusIcon = [
    InvoiceStatus::STATUS_CREATED => 'new-doc',
    InvoiceStatus::STATUS_SEND => 'envelope',
    InvoiceStatus::STATUS_PAYED => 'check-2',
    InvoiceStatus::STATUS_VIEWED => 'check-2',
    InvoiceStatus::STATUS_APPROVED => 'check-double',
    InvoiceStatus::STATUS_PAYED_PARTIAL => 'check-2',
    InvoiceStatus::STATUS_OVERDUE => 'calendar',
    InvoiceStatus::STATUS_REJECTED => 'stop',
];
$statusBackground = [
    InvoiceStatus::STATUS_CREATED => '#4679AE',
    InvoiceStatus::STATUS_SEND => '#FAC031',
    InvoiceStatus::STATUS_PAYED => '#26cd58',
    InvoiceStatus::STATUS_VIEWED => '#FAC031',
    InvoiceStatus::STATUS_APPROVED => '#FAC031',
    InvoiceStatus::STATUS_PAYED_PARTIAL => '#26cd58',
    InvoiceStatus::STATUS_OVERDUE => '#e30611',
    InvoiceStatus::STATUS_REJECTED => '#f2f3f7',
];
$icon = ArrayHelper::getValue($statusIcon, $status->id);
$background = ArrayHelper::getValue($statusBackground, $status->id);
$color = $status->id == InvoiceStatus::STATUS_REJECTED ? '#001424' : '#ffffff';
$border = $status->id == InvoiceStatus::STATUS_REJECTED ? '#e2e5eb' : $background;
?>

<div class="sidebar-title d-flex flex-wrap align-items-center" style="margin: 0 -5px;">
    <strong class="column mb-2 mb-xl-3 doc-status-price" style="padding: 0 5px;">
        <?= TextHelper::invoiceMoneyFormat($model->paid_amount, 2); ?> ₽
    </strong>
    <div class="column flex-grow-1 mt-1 mt-xl-0" style="padding: 0 5px;">
        <div class="button-regular mb-3 pl-0 pr-0 w-100" style="
            background-color: <?=$background?>;
            border-color: <?=$border?>;
            color: <?=$color?>;
        ">
            <?= Icon::get($icon) ?>
            <span><?= $status->name; ?></span>
        </div>
    </div>
</div>

<?php if ($createdInvoicesCount <= 3) {
    $this->registerJs('
        // Pulsate block
        (function($) {
            jQuery.fn.weOffset = function () {
                var de = document.documentElement;
                var box = $(this).get(0).getBoundingClientRect();
                var top = box.top + window.pageYOffset - de.clientTop;
                var left = box.left + window.pageXOffset - de.clientLeft;
                var width = $(this).width();
                var height = $(this).height();
                return { top: top, left: left, width: width, height: height };
            };
        }(jQuery));

        $(document).ready(function() {

            var elementOffset = $("#document-panel-pulsate").weOffset();

            jQuery("<div/>", {
                class: "pulsate-block",
            }).css({
                "position": "absolute",
                "top": elementOffset.top + 10,
                "left": elementOffset.left,
                "width": elementOffset.width,
                "height": elementOffset.height - 10
            }).appendTo("body").pulsate({
                color: "#bf1c56",
                reach: 20,
                repeat: 2
            });

            setTimeout(\'jQuery(".pulsate-block").remove()\', 2500);
        });

        $(window).resize(function() {
            if (jQuery(".pulsate-block").length) {
                var elementOffset = $("#document-panel-pulsate").weOffset();

                jQuery(".pulsate-block").css({
                    "position": "absolute",
                    "top": elementOffset.top + 10,
                    "left": elementOffset.left,
                    "width": elementOffset.width,
                    "height": elementOffset.height - 10
                }).appendTo("body");
            }
        });
    ');
}
?>