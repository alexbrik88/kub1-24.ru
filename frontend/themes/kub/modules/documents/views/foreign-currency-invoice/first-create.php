<?php

use common\models\document\InvoiceEssence;
use common\models\document\InvoiceContractEssence;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;

$invoiceEssence = new InvoiceEssence();
$invoiceEssence->company_id = $company->id;

$invoiceContractEssence = new InvoiceContractEssence();
$invoiceContractEssence->company_id = $company->id;

echo $this->render('create', [
    'company' => $company,
    'account' => $account,
    'contractor' => $contractor,
    'ioType' => $ioType,
    'model' => $model,
    'message' => new Message(Documents::DOCUMENT_INVOICE, $model->type),
    'fixedContractor' => null,
    'contractorId' => null,
    'invoiceEssence' => $invoiceEssence,
    'invoiceContractEssence' => $invoiceContractEssence

]);