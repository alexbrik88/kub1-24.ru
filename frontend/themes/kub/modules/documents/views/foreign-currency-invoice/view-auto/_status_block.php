<?php

use common\models\document\Autoinvoice;
use common\models\document\InvoiceAuto;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceAuto */

$count = $model->auto->getInvoices()->andWhere(['not', ['email_messages' => null]])->count();

switch ($model->auto->status) {
    case Autoinvoice::ACTIVE:
        $icon = 'check-2';
        $color = '#26cd58';
        break;

    case Autoinvoice::CANCELED:
        $icon = 'stop';
        $color = '#FAC031';
        break;

    case Autoinvoice::DELETED:
        $icon = 'garbage';
        $color = '#4679AE';
        break;

    default:
        $icon = '';
        $color = '';
        break;
}
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: <?= $color ?>;
            border-color: <?= $color ?>;
            color: #ffffff;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span class="ml-3"><?= $model->auto->statusName ?></span>
            <span class="ml-auto mr-1"><?= date("d.m.Y", $model->invoice_status_updated_at); ?></span>
        </div>
    </div>
</div>

<div class="button-regular w-100 text-left pl-3 pr-3" style="cursor: default">
    <?= $this->render('//svg-sprite', ['ico' => 'envelope']) ?>
    <span class="ml-3">Отправлено</span>
    <span class="ml-auto mr-1"><?=$count?></span>
</div>
