<?php

use frontend\rbac\permissions\document\Invoice;
use yii\helpers\Html;
use common\models\document\Autoinvoice;
use yii\bootstrap4\Modal;

/* @var $model \common\models\document\InvoiceAuto */

$canActivate = date_create($model->auto->setNextDate()) < date_create($model->auto->date_to) &&
    $model->auto->status == Autoinvoice::CANCELED;
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE)) : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    'data-toggle' => 'modal',
                    'href' => '#copy-confirm'
                ]) ?>
            <?php endif ?>
        </div>
        <div class="column flex-xl-grow-1">

        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($model->auto->status == 1 && Yii::$app->user->can(\frontend\rbac\permissions\document\Autoinvoice::STOP)): ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'close']).' <span>Остановить</span>', '#stop-confirm', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    'data-toggle' => 'modal',
                ]); ?>
            <?php elseif ($model->auto->status != 1 && Yii::$app->user->can(frontend\rbac\permissions\document\Autoinvoice::START)): ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'repeat']).' <span>Запустить</span>',
                    $canActivate ? '#start-confirm' : '#cannot-confirm', [
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                        'data-toggle' => 'modal',
                    ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if ($model->auto->status != 0 && Yii::$app->user->can(\frontend\rbac\permissions\document\Autoinvoice::DELETE)): ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#delete-confirm', [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    'data-toggle' => 'modal',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (!$canActivate): ?>
    <?php Modal::begin([
        'id' => 'cannot-confirm',
        'closeButton' => false,
        'toggleButton' => false,
        'options' => ['class' => 'confirm-modal fade'],
    ]); ?>
    <div class="form-body">
        <h4 class="modal-title text-center mb-4">Чтобы запустить АвтоСчет, измените дату окончания данного шаблона</h4>
        <div class="text-center">
            <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Ок</button>
        </div>
    </div>
    <?php Modal::end(); ?>

<?php endif; ?>
