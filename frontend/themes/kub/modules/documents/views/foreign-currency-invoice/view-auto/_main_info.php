<?php
use common\components\date\DateHelper;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \common\models\document\Autoinvoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string */

$auto = $model->auto;
?>

<div class="about-card mb-3 mt-1 pl-3 pr-3">
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">
            Шаблон <?= $model->is_invoice_contract ? 'автосчёт-договора' : 'автосчёта'; ?>
            № <?= $auto->document_number; ?>
            <br>
            от <?= $dateFormatted; ?>
        </span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Покупатель:</span><br>
        <?= Html::a($model->contractor->getShortName(), [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], [
            'class' => 'link mt-1',
        ]) ?>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Отсрочка платежа:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->payment_delay; ?> дней</span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Периодичность:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->getPeriodText(); ?></span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700"><?= $auto->period == Autoinvoice::QUARTERLY ? 'День' : 'Дата'; ?>:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->day; ?></span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Начало:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->dateFrom ?></span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Окончание:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->dateTo ?></span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Указывать в наименовании:</span><br>
        <span class="d-inline-block mt-1"><?= $auto->addToTitle ?></span>
    </div>
    <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
        <span class="text-grey weight-700">Тип счёта:</span><br>
        <span class="d-inline-block mt-1"><?= $model->getProductionType() ?></span>
    </div>
    <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
        <div class="about-card-item mb-2 pb-1 pl-1 pr-1">
            <span class="text-grey weight-700">Договор:</span><br>
            <span class="d-inline-block mt-1">
                № <?= Html::encode($model->basis_document_number) ?>
                от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
            </span>
        </div>
    <?php endif ?>
</div>
