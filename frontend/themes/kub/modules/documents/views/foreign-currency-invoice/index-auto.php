<?php

use common\components\date\DateHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\widgets\TableViewWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Шаблоны АвтоСчетов';
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => Documents::IO_TYPE_OUT,
        ])
        ): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'create',
                    'type' => Documents::IO_TYPE_OUT,
                    'auto' => true,
                ], [
                    'class' => 'button-regular button-regular_red button-width ml-auto',
                ]) ?>
            <?php else : ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']).' <span>Добавить</span>', [
                    'class' => 'button-regular button-regular_red button-width ml-auto action-is-limited',
                ]) ?>
            <?php endif ?>
        <?php endif; ?>
    </div>
    <div class="wrap wrap_count">
        <div class="row">
            <div class="col-6 col-xl-9">
                <div class="count-card count-card_green wrap mb-0">
                    Если вы ежемесячно выставляете счета за одни и те же услуги или товары и в назначении меняется только месяц и год, то настройте шаблон АвтоСчета.
                    Счета автоматически будут формироваться и отправляться вашим клиентам в день, который вы зададите.
                </div>
            </div>
            <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">

                <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                <?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
                    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php else : ?>
                    <?= Html::a('АвтоСчета', ['index-auto'], [
                        'class' => 'button-regular w-100 button-hover-content-red',
                    ]) ?>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= TableViewWidget::widget(['attribute' => 'table_view_autoinvoice']) ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Номер счета или название контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= $this->render('_template_table', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]); ?>
</div>
