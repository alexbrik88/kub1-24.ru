<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureGroup;
use common\models\NdsOsno;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\AutoinvoiceForm;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\models\Bank;
use common\components\ImageHelper;

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;

use common\models\employee\EmployeeRole;
use frontend\rbac\permissions;
use \frontend\themes\kub\helpers\Icon;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $rsArray CheckingAccountant[] */
/* @var $fixedContractor boolean */
/* @var $message Message */
/* @var $ioType integer */
/* @var $isAuto boolean */
/* @var $invoiceEssence \common\models\document\InvoiceEssence */
/* @var $document string */
/* @var $user \common\models\employee\Employee */

$user = Yii::$app->user->identity;
$userConfig = Yii::$app->user->identity->config;
$invoiceBlock = 'invoice-block' . (!empty($isAuto) ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . (!empty($isAuto) ? '' : ' hidden');
$contractorType = ($ioType == Documents::IO_TYPE_OUT) ?
    Contractor::TYPE_CUSTOMER :
    Contractor::TYPE_SELLER;
$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => !$model->getIsContractorEditable(),
];

$hasContractor = $company->getContractors()
    ->byContractor($contractorType)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->exists();

if (empty($model->contractor_id)) {
    $contractorDropDownConfig['prompt'] = '';
}
$autoDisplay = 'none';
$canEditRs = Yii::$app->getUser()->can(permissions\Company::UPDATE);
$canAddContractor = Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
    'type' => ArrayHelper::getValue($model, 'type'),
]);

$rsData = [];
$currencyData = [];
$companyRs = $rsArray;
if (!$model->isNewRecord && ($rs = $model->foreignCurrencyAccount)) {
    $rsData[$rs->id] = $rs->name . ", {$rs->rs} ({$rs->currency->name})";
}
foreach ($companyRs as $rs) {
    $rsData[$rs->id] = $rs->name . ", {$rs->rs} ({$rs->currency->name})";
    $currencyData[$rs->id] = Currency::name($rs->currency_id);
}
$noRs = empty($rsData);
if ($canEditRs) {
    $rsData["add-modal-rs"] = Icon::PLUS . ' Добавить валютный счет';
}
$hasAdditionals = $model->has_discount || $model->has_markup || $model->hasNds ||
    $model->show_paylimit_info || $model->comment;
?>

<?= \yii\helpers\Html::activeHiddenInput($model, 'store_id'); ?>

<?= Html::hiddenInput('is_first_invoice', 0, ['id' => 'is_first_invoice']); ?>

<?php
$contractorDropDownConfig['options'] = Contractor::getAllContractorSelect2Options($contractorType, Yii::$app->user->identity->company);
?>
<?php if ($canEditRs): ?>
    <style>
        #select2-invoice-checking_accountant_id-results .select2-results__option:last-child {
            color: #4679AE;
            font-weight: bold;
        }
    </style>
<?php endif; ?>
<?php if ($canAddContractor): ?>
    <style>
        #select2-invoice-contractor_id-results .select2-results__option:first-child {
            color: #4679AE;
            font-weight: bold;
        }
    </style>
<?php endif; ?>

    <div class="wrap">
        <div class="row d-block">
            <?php if ($isFirstCreate) : ?>
                <!-- FIRST ADD INVOICE -->
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <label class="label">
                                Bank and Account № (Банк и номер счета)
                            </label>
                            <div class="">
                                <?= $form->field($model, 'checking_accountant_id', [
                                    'template' => "{input}",
                                ])->widget(Select2::class, [
                                    'data' => $rsData,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'placeholder' => $noRs ? '' : null,
                                        'templateResult' => new JsExpression('formatCompanyRsTemplateResult'),
                                        'width' => '100%'
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                        'data-update-url' => Url::to(['update-checking-accountant']),
                                        'data-create-url' => Url::to(['create-checking-accountant']),
                                        'data-currency' => $currencyData,
                                    ]
                                ])->label(false); ?>
                            </div>
                            <br>
                            <label class="label">
                                <?= $ioType == Documents::IO_TYPE_IN ? 'Поставщик' : 'Покупатель'; ?>
                                <span class="important" aria-required="true">*</span>
                                <!-- help -->
                                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                    'class' => 'tooltip2',
                                    'data-tooltip-content' => '#tooltip_contractor',
                                ]) ?>
                            </label>
                            <div id="invoice-contractor_id-container">
                                <div id="add-first-contractor" class="form-group required details row" style="display: <?= $hasContractor ? 'none' : 'block'; ?>;">
                                    <div class="col-12">
                                        <?= \yii\helpers\Html::textInput('new_contractor_inn', '', [
                                            'id' => 'new_contractor_inn',
                                            'data-required' => 1,
                                            'class' => 'form-control',
                                            'placeholder' => 'Автозаполнение по ИНН ' . ($ioType == Documents::IO_TYPE_OUT ? 'покупателя' : 'продавца')
                                        ]); ?>
                                        <?= $this->render('_new_contractor_inn_api') ?>
                                    </div>
                                </div>
                                <div id="select-existing-contractor" class="form-group required details row" style="display: <?= $hasContractor ? 'block' : 'none'; ?>;">
                                    <div class="col-12">
                                        <?= $form->field($model, 'contractor_id', [
                                            'template' => "{input}",
                                            'options' => [
                                                'class' => 'show-contractor-type-in-fields',
                                            ],
                                        ])->widget(ContractorDropdown::class, [
                                            'company' => $company,
                                            'contractorType' => $contractorType,
                                            'staticData' => [
                                                "add-modal-contractor" => Icon::PLUS . ' Добавить ' . (
                                                    $ioType == Documents::IO_TYPE_IN ? 'поставщика' : 'покупателя'
                                                ),
                                            ],
                                            'options' => $contractorDropDownConfig,
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <?php if ($company->getIsForeignAttributesRequired()) : ?>
                            <div class="form-group required your-company row">
                                <div class="col-12">
                                    <label class="label">
                                        Ваша компания на английском<span class="important" aria-required="true">*</span>
                                        <?php if ($isFirstCreate) : ?>
                                            <span style="color: #ccc; font-weight: normal;">
                                                (<?= $ioType == Documents::IO_TYPE_IN ? 'Покупатель' : 'Поставщик'; ?>)
                                            </span>
                                        <?php endif; ?>
                                        <!-- help -->
                                        <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                                            'class' => 'tooltip2',
                                            'data-tooltip-content' => '#tooltip_company',
                                        ]) ?>
                                    </label>
                                    <?= \yii\helpers\Html::textInput('new_company_inn', '', [
                                        'id' => 'new_company_inn',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                        //'placeholder' => 'Автозаполнение по ИНН'
                                    ]); ?>
                                    <?php // echo $this->render('_new_company_inn_api') ?>
                                </div>
                                <?= Html::hiddenInput('Company[strict_mode]'); ?>
                                <?php $this->registerJs('
                                    $(document).on("click", "#new_company_inn", function() {
                                        INVOICE.addDetailsAddress("add-modal-details-address", {});
                                    });
                                ') ?>
                            </div>
                            <br>
                            <?php endif ?>
                            <div class="form-group required row <?= ($invoiceBlock) ?>">
                                <div class="col-12">
                                    <label class="label" for="input3">Оплатить до<span class="important">*</span></label>
                                    <div class="date-picker-wrap" style="width: 136px;">
                                        <?= Html::activeTextInput($model, 'payment_limit_date', [
                                            'class' => 'form-control date-picker',
                                            'data-date-viewmode' => 'years',
                                            'data-delay' => $model->contractor ? (
                                                $model->type == Documents::IO_TYPE_IN ?
                                                $model->contractor->seller_payment_delay :
                                                $model->contractor->customer_payment_delay
                                            ) : 10,
                                            'value' => DateHelper::format(!$model->isNewRecord ? $model->payment_limit_date : date(DateHelper::FORMAT_DATE, strtotime('+10 day')), DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                        ]); ?>
                                        <svg class="date-picker-icon svg-icon input-toggle">
                                            <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 mt-element-step">

                            <div class="row step-default">
                                <div id="invoise-step-1"
                                     class="col-4 mt-step-col first-invoice-contractor invoice-step<?= $model->contractor ? ' done' : ''; ?>"
                                     data-invoice-ready="35">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">1</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">
                                            <?= ($ioType == \frontend\models\Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?>
                                        </div>
                                        <div class="mt-step-content font-grey-cascade">Заполнить реквизиты</div>
                                    </div>
                                </div>
                                <div id="invoise-step-2"
                                     class="col-4 mt-step-col first-invoice-company invoice-step<?= !$company->getIsForeignAttributesRequired() ? ' done' : ''; ?>"
                                     data-invoice-ready="35">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">2</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">
                                            Ваше <?= $model->company->companyType->name_short ?></div>
                                        <div class="mt-step-content font-grey-cascade">Заполнить реквизиты</div>
                                    </div>
                                </div>
                                <div id="invoise-step-3"
                                     class="col-4 mt-step-col first-invoice-product invoice-step<?= $model->orders ? ' done' : ''; ?>"
                                     data-invoice-ready="30">
                                    <div class="col-12">
                                        <div class="mt-step-number bg-white font-grey">3</div>
                                        <div class="mt-step-title uppercase font-grey-cascade">Товар/Услуга
                                        </div>
                                        <div class="mt-step-content font-grey-cascade">Ввести
                                            название,
                                            количество и цену
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            <?php else: ?>
                <!-- REGULAR INVOICE -->
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label">
                            Bank and Account № (Банк и номер счета)
                        </label>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'checking_accountant_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->widget(Select2::class, [
                                    'data' => $rsData,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'placeholder' => $noRs ? '' : null,
                                        'templateResult' => new JsExpression('formatCompanyRsTemplateResult'),
                                        'width' => '100%'
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                        'data-update-url' => Url::to(['update-checking-accountant']),
                                        'data-create-url' => Url::to(['create-checking-accountant']),
                                        'data-currency' => $currencyData,
                                    ]
                                ])->label(false); ?>
                            </div>
                            <div class="col-6 d-flex align-items-center">
                                <?php if (1 || !$model->isNewRecord && $model->company->requisites_updated_at > $model->updated_at) : ?>
                                    <?= Html::a(Html::img('/images/icon-refresh.png', ['class' => 'refresh-contractor-invoice']), '#refresh-company-invoice', [
                                        'class' => 'tooltip3 refresh-contractor-invoice mr-3',
                                        'data-tooltip-content' => '#tooltip_refresh_company',
                                        'data-toggle' => 'modal',
                                    ]) ?>
                                    <img src="" class="little_logo_bank mr-3">
                                    <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                                        'options' => [
                                            'id' => 'refresh-company-invoice',
                                        ],
                                        'toggleButton' => false,
                                        'confirmUrl' => Url::to([
                                            'update-company-fields',
                                            'id' => $model->id,
                                            'type' => $model->type,
                                        ]),
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите обновить ваши реквизиты в данном инвойсе?',
                                    ]); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label" for="client">
                            <?= ($ioType == Documents::IO_TYPE_IN) ? 'Bill from (Поставщик)' : 'Bill to (Покупатель)' ?>
                        </label>
                        <div class="row">
                            <div class="col-6">
                                <?= $form->field($model, 'contractor_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'id' => "invoice-contractor_id-container",
                                        'class' => 'show-contractor-type-in-fields',
                                    ]
                                ])->widget(ContractorDropdown::class, [
                                    'company' => $company,
                                    'contractorType' => $contractorType,
                                    'contractorFaceType' => Contractor::TYPE_FOREIGN_LEGAL_PERSON,
                                    'staticData' => [
                                        "add-modal-contractor" => Icon::PLUS . ' Добавить иностранного ' . (
                                            $ioType == Documents::IO_TYPE_IN ? 'поставщика' : 'покупателя'
                                        ),
                                    ],
                                    'options' => $contractorDropDownConfig,
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                    ],
                                ]); ?>
                            </div>
                            <span class="hidden contractor-invoice-debt small-txt col-3">
                                Есть неоплаченные инвойсы: <span class="count"></span><br>
                                на сумму <span class="amount"></span> Р
                            </span>
                            <?php if (!$model->isNewRecord && $model->checkContractor()): ?>
                                <?= Html::a(Html::img('/images/icon-refresh.png', ['class' => 'refresh-contractor-invoice']), '#refresh-contractor-invoice', [
                                    'class' => 'tooltip3 refresh-contractor-invoice',
                                    'data-tooltip-content' => '#tooltip_refresh_contractor',
                                    'style' => 'padding-top:10px; margin-left: 13px',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#',
                                ]) ?>
                                <?= \frontend\themes\kub\widgets\ConfirmModalWidget::widget([
                                    'options' => [
                                        'id' => 'refresh-contractor-invoice',
                                    ],
                                    'toggleButton' => false,
                                    'confirmUrl' => Url::to([
                                        'update-contractor-fields',
                                        'id' => $model->id,
                                        'type' => $model->type,
                                    ]),
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите обновить информацию по ' . ($model->type == Documents::IO_TYPE_OUT ? 'покупателю' : 'продавцу') . ' в данном инвойсе?',
                                ]); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-xl-6 <?= ($invoiceBlock) ?>">
                    <div class="form-group mb-0">
                        <label class="label" for="input3">
                            Due Date (Оплатить до)
                        </label>
                    </div>
                    <div class="row align-items-center">
                        <div class="form-group col-4 col-xl-4">
                            <div class="date-picker-wrap">
                                <?= Html::activeTextInput($model, 'payment_limit_date_input', [
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                    'data-delay' => $model->contractor ? (
                                        $model->type == Documents::IO_TYPE_IN ?
                                        $model->contractor->seller_payment_delay :
                                        $model->contractor->customer_payment_delay
                                    ) : 10,
                                ]); ?>
                                <svg class="date-picker-icon svg-icon input-toggle">
                                    <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                                </svg>
                            </div>
                        </div>
                        <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                            <div class="form-group column">
                                <div class="d-flex align-items-center">
                                    <label class="checkbox-label checkbox_label_left" for="invoice-remind_contractor">
                                        <span class="checkbox-txt-bold">Отправить напоминание</span>
                                    </label>
                                    <?= Html::activeCheckbox($model, 'remind_contractor', [
                                        'class' => '',
                                        'label' => false,
                                    ]); ?>
                                    <div class="tooltip-box mr-2">
                                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                                data-original-title="Если инвойс не будет оплачен, то будет автоматически отправлено письмо вашему покупателю с напоминанием об оплате инвойса">
                                            <svg class="tooltip-question-icon svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                <br>
                <div class="form-group col-12">
                    <div class="form-filter">
                        <label class="label" for="client">Статья расходов</label>
                        <div class="row">
                            <div class="col-6">
                                <?php
                                echo $form->field($model, 'invoice_expenditure_item_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                    ]
                                ])->widget(ExpenditureDropdownWidget::class, [
                                    'loadAssets' => false,
                                    'exclude' => [
                                        'group' => InvoiceExpenditureGroup::TAXES,
                                    ],
                                    'options' => [
                                        'prompt' => '',
                                    ],
                                    'pluginOptions' => [
                                        'width' => '100%'
                                    ]
                                ]);
                                echo $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                                    'inputId' => 'invoice-invoice_expenditure_item_id',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <br/>
            <div class="form-group col-6">
                <div class="form-filter">
                    <label class="label" for="cause">
                        Contract (Договор)
                    </label>
                </div>
                <?= $this->render('_basis_document', [
                    'model' => $model,
                ]) ?>
            </div>
            <?php if ($isFirstCreate) : ?>
                <?php if ($company->companyTaxationType->osno) : ?>
                    <br/>
                    <div class="form-group col-6">

                        <label for="invoice-payment_limit_date" class="label">
                            Тип
                            <?php if ($document == null): ?>
                                счета
                            <?php endif; ?>
                            <span class="important" aria-required="true">*</span>:
                        </label>

                        <?php
                        if ($company->nds === null) {
                            $company->nds = NdsOsno::WITH_NDS;
                        }
                        echo \yii\helpers\Html::activeRadioList($company, 'nds', ArrayHelper::map(NdsOsno::find()->all(), 'id', 'name'), [
                            'class' => 'inp_one_line_company checkbox-width',
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return '<div class="col-xs-12 pad0">' .
                                    Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                        'class' => 'pr-3',
                                    ]) . Html::a('Пример счета', [
                                        Url::to('/company/view-example-invoice'),
                                        'nds' => $value == 1 ? true : false,
                                    ], [
                                        'class' => 'marg radio-inline radio-padding',
                                        'target' => '_blank',
                                    ]) .
                                    "</div>";
                            },
                        ]);
                        ?>
                    </div>

                <?php endif; ?>
            <?php endif; ?>
            <br>
            <div class="form-group col-12">
                <button class="link link_collapse link_bold button-clr collapsed" type="button" data-toggle="collapse" data-target="#moreDetails" aria-expanded="false" aria-controls="moreDetails">
                    <span class="link-txt">Дополнительные параметры</span>
                    <svg class="link-shevron svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#shevron"></use>
                    </svg>
                </button>
            </div>
            <div class="col-12">
                <div class="collapse in" id="moreDetails">
                    <br>
                    <div class="row mt-3">
                        <div class="col-6 col-xl-3">
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_discount">
                                        <span class="checkbox-txt">Указать скидку</span>
                                        <?= Html::activeCheckbox($model, 'has_discount', [
                                            'id' => 'invoice-has_discount',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-has_markup">
                                            <span class="checkbox-txt">Указать наценку</span>
                                            <?= Html::activeCheckbox($model, 'has_markup', [
                                                'id' => 'invoice-has_markup',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($ioType == Documents::IO_TYPE_OUT && $company->companyTaxationType->usn) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-hasnds">
                                    <span class="checkbox-txt">
                                        Инвойс с НДС
                                    </span>
                                            <?= Html::activeCheckbox($model, 'hasNds', [
                                                'id' => 'invoice-hasnds',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($ioType == Documents::IO_TYPE_OUT && $document == null) : ?>
                                <div class="form-group row flex-nowrap">
                                    <div class="checkbox col-9 pr-0">
                                        <label class="checkbox-label" for="invoice-show_paylimit_info">
                                            <span class="checkbox-txt">Указать срок оплаты</span>
                                            <?= Html::activeCheckbox($model, 'show_paylimit_info', [
                                                'id' => 'invoice-show_paylimit_info',
                                                'class' => 'checkbox-input input-hidden',
                                                'label' => false,
                                            ]); ?>
                                        </label>
                                    </div>
                                    <div class="tooltip-box">
                                        <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title=""
                                                data-original-title='Срок оплаты подставляется автоматически в комментарий к инвойсу из поля "Оплатить до"'>
                                            <svg class="tooltip-question-icon svg-icon">
                                                <use xlink:href="/img/svg/svgSprite.svg#question"></use>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_weight">
                                        <span class="checkbox-txt">Указать вес</span>
                                        <?= Html::activeCheckbox($model, 'has_weight', [
                                            'id' => 'invoice-has_weight',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <label class="checkbox-label" for="invoice-has_volume">
                                        <span class="checkbox-txt">Указать объем</span>
                                        <?= Html::activeCheckbox($model, 'has_volume', [
                                            'id' => 'invoice-has_volume',
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row flex-nowrap">
                                <div class="checkbox col-9 pr-0">
                                    <div id="config_items_box">
                                        <label class="checkbox-label" for="config-invoice_form_article">
                                            <span class="checkbox-txt">Указать артикул</span>
                                            <input class="checkbox-input input-hidden"
                                                   id="config-invoice_form_article"
                                                   type="checkbox"
                                                   data-target="col_invoice_form_article"
                                                <?= ($userConfig->invoice_form_article) ? ' checked="" ' : '' ?>>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <label class="checkbox-txt">
                                <span class="pb-2">Комментарий в инвойсе для покупателя</span>
                                <div class="tooltip-box tooltip-box_side-margins" style="margin-top:-5px;">
                                    <?= Html::tag('span', Icon::get('question', [
                                        'class' => 'tooltip-question-icon',
                                    ]), [
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'bottom',
                                        'data-original-title' => 'Добавьте к инвойсу комментарий для покупателя. '.
                                            'Покупатель получит дополнительную информацию о деталях оплаты, условиях договора и тд.'
                                    ]) ?>
                                </div>
                            </label>
                            <div class="form-group mb-2">
                                <?= Html::activeTextarea($model, 'comment', [
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                    'value' => $invoiceEssence->is_checked_en && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT ?
                                        $invoiceEssence->text_en : $model->comment,
                                    'data' => [
                                        'paylimit' => 'Инвойс действителен для оплаты до {{date}}.',
                                    ]
                                ]); ?>
                            </div>
                            <div class="form-group mb-0">
                                <div class="checkbox">
                                    <label class="checkbox-label" for="invoiceessence-is_checked_en">
                                        <?= Html::activeCheckbox($invoiceEssence, 'is_checked_en', [
                                            'class' => 'checkbox-input input-hidden',
                                            'label' => false,
                                        ]); ?>
                                        <span class="checkbox-txt">Сохранить текст для всех инвойсов</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $this->registerJs('
    $companyRs = ' . json_encode($companyRs) . ';

    function htmlEscape(str) {
        return str
            .replace(/&/g, \'&amp;\')
            .replace(/"/g, \'&quot;\')
            .replace(/\'/g, \'&#39;\')
            .replace(/</g, \'&lt;\')
            .replace(/>/g, \'&gt;\');
    }

    function formatCompanyRsTemplateResult(data, container) {
        if (data.id !== "add-modal-rs" && data.disabled !== true) {
            var input = $("#invoice-checking_accountant_id");
            var content = \'<div class="checking_accountant_id-item-name-label">\';
            if (' . (int)$canEditRs . ') {
                content += \'<i class="icon icon-pencil pull-right edit-checking_accountant_id-item ajax-modal-btn" data-id="\' + data.id + \'" data-url="\' + input.data("update-url") + \'?id=\' + $companyRs[data.id].id + \'" title="Обновить"></i>\';
            }
            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            container.innerHTML = content;
        } else {
            var input = $("#invoice-checking_accountant_id");
            $(container).addClass("ajax-modal-btn").attr("title", "Добавить расчетный счет").attr("data-url", input.data("create-url"));
            container.innerHTML = data.text;
        }

        return container;
    };
', \yii\web\View::POS_HEAD);

$this->registerJs('
    $(document).ready(function () {
        loadBankLogo($("#invoice-checking_accountant_id").val());
    });

    $(document).on("change", "#invoice-checking_accountant_id", function () {
        loadBankLogo($(this).val());
    });

    function loadBankLogo(bankRs) {
        if (+$("#is_first_invoice").val() !== 1 && +$("#invoiceauto-isautoinvoice").val() !== 1 && $companyRs[bankRs] !== undefined) {
            var $logo = $companyRs[bankRs].bank_logo;
            var $link = $companyRs[bankRs].bank_link;

            if ($logo !== null) {
                $(".little_logo_bank").attr("src", $logo);
            } else {
                $(".little_logo_bank").attr("src", "");
            }
            $(".little_logo_bank").siblings(".bank-link").remove();
            if ($link) {
                $(".little_logo_bank").after($link);
            }
        }
    }

    $(document).on("submit", "form.form-checking-accountant", function (e) {
        e.preventDefault();
        form = $(this);
        container = form.closest("#ajax-modal-content");
        $.post(form.attr("action"), $(this).serialize(), function (data) {
            if (data.result == true) {
                var select = $("#invoice-checking_accountant_id");
                select.find("option").remove();
                for (var key in data.options) {
                    select.append($("<option></option>").attr("value", key).text(data.options[key]));
                }
                select.data("currency", data.currencyData);
                $companyRs = data.companyRs;
                if (data.val) {
                    select.val(data.val).trigger("change");
                }
                form.closest(".modal").modal("hide");

            } else {
                container.html(data);
            }
            Ladda.stopAll();
        });
    });

    // sync fields
    $("#autoinvoice-add-monthly").on("change", function() {
        if ($("#autoinvoice-add-quarterly").val() !== $(this).val())
            $("#autoinvoice-add-quarterly").val($(this).val()).trigger("change");
    });
    $("#autoinvoice-add-quarterly").on("change", function() {
        if ($("#autoinvoice-add-monthly").val() !== $(this).val())
            $("#autoinvoice-add-monthly").val($(this).val()).trigger("change");
    });

    $(document).on("select2:selecting", "#invoice-agreement", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-agreement-item")) {
            e.preventDefault();

            $("#invoice-agreement").select2("close");

            $.pjax({
                url: "/documents/agreement/update" +
                    "?id=" + $(target).data("id") +
                    "&contractor_id=" + $("#invoice-contractor_id").val() +
                    "&type=" + $("#documentType").val() +
                    "&returnTo=invoice" +
                    "&container=agreement-select-container" +
                    "&old_record=1" +
                    "&hide_template_fields=1",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });

            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));

            });
            $("#agreement-modal-container").modal("show");
            $("#invoice-agreement").val("").trigger("change");

        }
    });
    $(document).on("select2:open", "#invoice-agreement", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });

    $(document).on("select2:open", "#invoice-checking_accountant_id", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });
    $(document).on("select2:selecting", "#invoice-checking_accountant_id", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-checking_accountant_id-item") || target.classList.contains("ajax-modal-btn")) {
            e.preventDefault();
        }
    });
    $(document).on("change", "#invoice-checking_accountant_id", function(e) {
        let dataCurrency = $(this).data("currency");
        let currencyName = dataCurrency[this.value] || "";
        $(".currency_name_label").html(currencyName);
        let content = $("#all_discount_toggle").tooltipster("content");
        content.find(".currency_name_label").html(currencyName);
        $("#all_discount_toggle").tooltipster("content", content);
    });
    $(document).on("keyup", "input.select2-search__field", function (e) {
        $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
            .attr("aria-selected", "false")
            .addClass("selected");
    });

    $(document).on("shown.bs.modal", ".modal#ajax-modal-box", function () {
        $("#invoice-checking_accountant_id").select2("close");
    });

    $(document).on("click", "#contractor_update_button", function() {
            var url  = "add-modal-contractor";
            var form = {
                documentType: INVOICE.documentIoType,
                contractorId: $(this).data("contractor-id") || 0,
                invoiceId: $(this).data("invoice-id") || 0,
        };
        INVOICE.addNewContractor("add-modal-contractor", form);
    });

    $("#config-invoice_form_article").on("change", function(e) {
        var input = this;
        var attr = input.id.replace("config-", "");
        $.post("/site/config", {"Config[invoice_form_article]": $("#config-invoice_form_article").prop("checked") ? 1 : 0}, function(data) {
            if (typeof data[attr] !== "undefined") {
                if (data[attr]) {
                    $("." + $(input).data("target")).removeClass("hidden");
                } else {
                    $("." + $(input).data("target")).addClass("hidden");
                }
            }
        })
    });

    $(document).on("shown.bs.modal", "#add-new", function() {
        refreshUniform();
    });

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });
');

