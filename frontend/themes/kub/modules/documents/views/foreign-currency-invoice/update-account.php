<?php

use common\models\company\CheckingAccountant;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\company\CheckingAccountant */

?>

<?= $this->render('//company/form/modal_rs/_partial/_form', [
    'checkingAccountant' => $model,
    'company' => $company,
    'actionUrl' => Url::current(),
]) ?>
