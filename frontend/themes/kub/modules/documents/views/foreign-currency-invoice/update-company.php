<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/**
 * @var $form
 * @var $inputConfig
 * @var $model
 */
?>

<?php $form = ActiveForm::begin([
    'id' => 'company-en-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => false,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name_short_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: KUB',
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name_full_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Limited Liability Company «KUB»',
            ]); ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'form_legal_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: LLC',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'address_legal_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Russia, Moscow, street, etc.',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'address_actual_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Russia, Moscow, street, etc.',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 left-column">
            <?= $form->field($model, 'chief_post_name_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Chief Executive Officer',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'lastname_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petrov',
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'firstname_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petr',
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'patronymic_en')->textInput([
                'maxlength' => true,
                'placeholder' => 'Пример: Petrovich',
            ]); ?>
        </div>
        <div class="col-md-3">
            <label class="label">&nbsp;</label>
            <div class="mt-2">
                <?= $form->field($model, 'not_has_patronymic_en')->checkbox([
                    'class' => '',
                    'labelOptions' => [
                        'class' => 'label mb-0',
                    ],
                ], true); ?>
            </div>
        </div>
    </div>

    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::button('Отменить', [
                'class' => 'button-width button-clr button-regular button-hover-grey',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
