<?php

use common\components\date\DateHelper;
use common\models\cash\CashFactory;
use common\models\currency\Currency;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\models\bank\BankingParams;

/* @var $this \yii\web\View */
/* @var $model \common\models\document\Invoice */
/* @var $useContractor string */
/* @var $company \common\models\Company */

$employee = Yii::$app->user->identity;
$company = $employee->company;
$addFlowForm = new \frontend\modules\documents\forms\ForeignCurrencyInvoiceFlowForm([
    'invoice' => $model,
    'employee' => $employee,
    'date_pay' => date(DateHelper::FORMAT_USER_DATE),
]);
$addFlowForm->load(Yii::$app->request->get());
$link = null;
$image = null;
$idArray = $idArray ?? [];

$flowType = (int)\Yii::$app->request->get('flowType');
if (!in_array($flowType, CashFactory::$flowTypeArray)) {
    $flowType = CashFactory::TYPE_BANK;
}

$action = [
    'add-flow',
    'type' => $model->type,
    'id' => Yii::$app->request->get('id'),
];

if ($useContractor) {
    $action['contractorId'] = $model->contractor_id;
}
$validateAction = $action;
$validateAction[0] = 'add-flow-validate';
?>

<?php
$form = ActiveForm::begin([
    'action' => $action,
    'validationUrl' => count($idArray) > 1 ? null : $validateAction,
    'options' => [
        'id' => 'add-flow-form',
        'class' => 'add-to-invoice',
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,

]);

$this->registerJs('
$(document).on("click", "#flow-list-pjax a.flow_type", function(e) {
    e.preventDefault();
    $.pjax.reload("#flow-list-pjax", {
        "url":$(this).attr("href"),
        "data":$("#invoiceflowform-date_pay, #invoiceflowform-amount, #invoiceflowform-cashbox_id input:checked, #invoiceflowform-emoney_id input:checked").serialize(),
        "push":false,
        "replace":false,
        "timeout":10000,
        "scrollTo":false,
    });
});
$(document).on("change", ".flow_item_check", function() {
    var flowsSum = 0;
    $(".flow_item_check:checked").each(function(i, item) {
        flowsSum += parseFloat($(item).closest("tr").find("span.sum").text());
    });
    $("#selectedFlowsAmount").val(flowsSum);
    if (flowsSum == parseFloat($("#invoiceflowform-amount").val())) {
        $("#invoiceflowform-amount").css("color", "green");
    } else {
        $("#invoiceflowform-amount").css("color", "red");
    }
});
$(document).on("change", "#invoiceflowform-createnew", function() {
    if ($("#invoiceflowform-createnew").is(":checked")) {
        $(".flow_item_check").prop( "checked", false ).prop("disabled", true).uniform("refresh");
        $("#selectedFlowsAmount").val(0);
    } else {
        $(".flow_item_check").prop("disabled", false);
    }
});
');
?>

<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="form-body">
    <div style="float: left; width: 50%;">
        <div class="row">
            <?= Html::activeLabel($addFlowForm, 'date_pay', [
                'class' => 'col-4 control-label',
                'style' => 'margin-top: 12px; max-width: 120px;'
            ]); ?>
            <div class="col-8">
                <?= $form->field($addFlowForm, 'date_pay')->label(false)->textInput([
                    'class' => 'form-control date-picker ico width_date',
                    'data' => [
                        'date-viewmode' => 'years',
                        'model-id' => $model->id,
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'flow-list-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
        'timeout' => 10000,
    ]); ?>

    <div class="<?= $model->isNewRecord ? 'hidden' : ''; ?>" style="float: left; width: 50%;">
        <div class="row">
            <?= Html::activeLabel($addFlowForm, 'amount', [
                'class' => 'col-4 control-label',
                'style' => 'margin-top: 12px; text-align: right;'
            ]); ?>

            <div class="col-md-8">
                <?= $form->field($addFlowForm, 'amount')->textInput([
                    'class' => 'form-control js_input_to_money_format',
                    'value' => $addFlowForm->amount,
                    'data' => [
                        'amount' => $addFlowForm->amount,
                    ],
                ])->label(false); ?>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row form-group">
        <?php $flowTypeName = Html::getInputName($addFlowForm, 'flowType'); ?>
        <div class="col-4" style="max-width: 150px;">
            <a href="<?= Url::current(['flowType' => CashFactory::TYPE_BANK]) ?>"
               class="flow_type"
               style="text-decoration: none;color: #333333!important;">
                <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_BANK, [
                    'value' => CashFactory::TYPE_BANK,
                ]) ?>
                через банк
            </a>
        </div>
        <?php if ($addFlowForm->cashboxList) : ?>
            <div class="col-4" style="max-width: 150px;">
                <a href="<?= Url::current(['flowType' => CashFactory::TYPE_ORDER]) ?>"
                   class="flow_type"
                   style="text-decoration: none;color: #333333!important;">
                    <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_ORDER, [
                        'value' => CashFactory::TYPE_ORDER,
                    ]) ?>
                    через кассу
                </a>
            </div>
        <?php endif ?>
        <?php if ($addFlowForm->emoneyList) : ?>
            <div class="col-4" style="max-width: 175px;">
                <a href="<?= Url::current(['flowType' => CashFactory::TYPE_EMONEY]) ?>"
                   class="flow_type"
                   style="text-decoration: none;color: #333333!important;">
                    <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_EMONEY, [
                        'value' => CashFactory::TYPE_EMONEY,
                    ]) ?>
                    через e-money
                </a>
            </div>
        <?php endif ?>
    </div>

    <?php if ($flowType == CashFactory::TYPE_ORDER && $addFlowForm->cashboxList) : ?>
        <?php
        if (empty($addFlowForm->cashbox_id) && $addFlowForm->cashboxList) {
            $addFlowForm->cashbox_id = key($addFlowForm->cashboxList);
        }
        $cashboxIsAccounting = &$addFlowForm->cashboxListIsAccounting;
        ?>
        <?= $form->field($addFlowForm, 'cashbox_id', [
            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group' . (count($addFlowForm->cashboxList) > 1 ? '' : ' hidden'),
            ],
            'labelOptions' => [
                'class' => 'col-sm-4',
                'style' => 'display: block; max-width: 150px;',
            ]
        ])->radioList($addFlowForm->cashboxList, [
            'item' => function ($index, $label, $name, $checked, $value) use ($cashboxIsAccounting) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'data-is_accounting' => $cashboxIsAccounting[$value] ?? 1,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif ?>

    <?php if ($flowType == CashFactory::TYPE_EMONEY && $addFlowForm->emoneyList) : ?>
        <?php
        if (empty($addFlowForm->emoney_id) && $addFlowForm->emoneyList) {
            $addFlowForm->emoney_id = key($addFlowForm->emoneyList);
        }
        $emoneyIsAccounting = &$addFlowForm->emoneyListIsAccounting;
        ?>
        <?= $form->field($addFlowForm, 'emoney_id', [
            'template' => "{label}\n<div class=\"col-sm-4\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group' . (count($addFlowForm->emoneyList) > 1 ? '' : ' hidden'),
            ],
            'labelOptions' => [
                'class' => 'col-8',
                'style' => 'display: block; max-width: 300px;',
            ]
        ])->radioList($addFlowForm->emoneyList, [
            'item' => function ($index, $label, $name, $checked, $value) use ($emoneyIsAccounting) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'data-is_accounting' => $emoneyIsAccounting[$value] ?? 1,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif ?>

    <?php
    $isOutInvoice = ($model->type == Documents::IO_TYPE_OUT);

    switch ($flowType) {
        case CashFactory::TYPE_BANK:
            $query = $model->getPossibleBankFlows();
            break;
        case CashFactory::TYPE_ORDER:
            $query = $model->getPossibleOrderFlows();
            break;
        case CashFactory::TYPE_EMONEY:
            $query = $model->getPossibleEmoneyFlows();
            break;
        default:
            $query = null;
            break;
    }

    if ($query) {
        $flowProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $flowProvider->pagination->pageSize = 0;

        if ($flowProvider->totalCount) {
            echo \common\components\grid\GridView::widget([
                'dataProvider' => $flowProvider,
                'filterModel' => $model,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list form-group',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'cssClass' => 'flow_item_check',
                        'name' => Html::getInputName($addFlowForm, 'selectedFlows'),
                        'header' => false,
                        'headerOptions' => [
                            'width' => '40px',
                        ],
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'label' => $isOutInvoice ? 'Приход' : 'Расход',
                        'attribute' => 'amount',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'value' => function ($model) {
                            return '<span class="sum" style="display:none;">' . intval($model->availableAmount) / 100 . '</span>' .
                                \common\components\TextHelper::invoiceMoneyFormat($model->amount, 2);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '60%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($invoiceArray = $data->getInvoices()->all()) {
                                $linkArray = [];
                                foreach ($invoiceArray as $invoice) {
                                    $linkArray[] = Html::a($data->formattedDescription . $invoice->fullNumber, [
                                        '/documents/invoice/view',
                                        'type' => $invoice->type,
                                        'id' => $invoice->id,
                                    ]);
                                }
                                return join(', ', $linkArray);
                            } else {
                                $description = mb_substr($data->description, 0, 50) . '<br>' . mb_substr($data->description, 50, 50);
                                return Html::label(strlen($data->description) > 100 ? $description . '...' : $description, null, ['title' => $data->description]);
                            }
                        },
                    ],
                ],
            ]);
            echo $form->field($addFlowForm, 'selectedFlowsAmount', ['options' => ['class' => '', 'template' => '{input}']])->hiddenInput(['id' => 'selectedFlowsAmount'])->label(false);
            echo $form->field($addFlowForm, 'createNew', [
                'enableClientValidation' => false,
            ])->checkbox();
        } else {
            echo $form->field($addFlowForm, 'createNew')->hiddenInput(['value' => 1])->label(false);
        }
    } else {
        echo $form->field($addFlowForm, 'createNew')->hiddenInput(['value' => 1])->label(false);
    }
    ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>

<div class="clearfix"></div>
<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col">
            <?= Html::submitButton('<span class="ladda-label">Добавить</span>', [
                'class' => 'button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width: 130px!important;',
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="col">
            <?= Html::a('Отменить', '#',[
                'class' => 'button-regular button-width button-hover-transparent',
                'style' => 'width: 130px!important; float: right;',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>
</div>

<?php $form->end(); ?>
