<?php

use common\models\document\status\InvoiceStatus;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\rbac\permissions\document\Document;
use frontend\themes\kub\modules\documents\widgets\DocumentLogWidget;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->registerAssetBundle(TooltipAsset::className(), $this::POS_END);
$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(permissions\Contractor::VIEW)) {
    if (!isset($contractorTab)) $contractorTab = null;
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $model->contractor_id, 'tab' => $contractorTab];
} elseif (Yii::$app->user->can(permissions\document\Document::INDEX, ['ioType' => $ioType])) {
    $backUrl = (\Yii::$app->request->referrer && strpos(\Yii::$app->request->referrer, 'documents/invoice/index')) ?
        \Yii::$app->request->referrer :
        ['index', 'type' => $model->type,];
}
$showSendPopup = Yii::$app->session->remove('show_send_popup');

$contractorId = $useContractor ? $model->contractor_id : null;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);
?>

<?= Yii::$app->session->getFlash('invoiceFactureError'); ?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, ['class' => 'link mb-2']); ?>
<?php endif; ?>

<div class="wrap wrap_padding_small">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => Icon::get('info'),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if (Yii::$app->user->can(Document::UPDATE, ['model' => $model])) : ?>
                    <?php if ($model->updateAllowed()): ?>
                        <?= Html::a(Icon::get('pencil'), [
                            'update',
                            'type' => $ioType,
                            'id' => $model->id,
                            'contractorId' => ($useContractor ? $model->contractor_id : null),
                        ], [
                            'title' => 'Редактировать',
                            'class' => 'tooltip3 button-list button-hover-transparent button-clr mb-3 ml-1 '
                        ]) ?>
                    <?php else: ?>
                        <?= Html::a(Icon::get('pencil'), 'javascript:;', [
                            'data-tooltip-content' => '#unpaid-invoice',
                            'data-placement' => 'right',
                            'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 tooltip2-right'
                        ]) ?>
                    <?php endif ?>
                <?php endif; ?>

                <div class="doc-container doc-invoice doc-preview">
                    <?= $this->render('/foreign-currency-invoice/template', [
                        'model' => $model,
                        'addStamp' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('/foreign-currency-invoice/view/_status_block', [
                'model' => $model,
                'ioType' => $model->type,
                'useContractor' => $useContractor,
            ]); ?>

            <?= $this->render('/foreign-currency-invoice/view/_main_info', [
                'model' => $model,
                'ioType' => $model->type,
                'message' => $message,
                'dateFormatted' => $dateFormatted,
                'useContractor' => $useContractor,
                'newCompanyTemplate' => true,
                'invoiceContractorSignature' => $invoiceContractorSignature,
            ]); ?>
        </div>
    </div>
</div>

<div style="display:none">
    <span id="unpaid-invoice">
        Что бы отредактировать счет, вам нужно снять оплату.<br>
        Для этого нажмите внизу на кнопку "Снять оплату"
    </span>
</div>

<?= $this->render('view/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>

<?php if ($model->invoiceStatus->sendAllowed()): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
        'showSendPopup' => $showSendPopup,
    ]); ?>
<?php endif; ?>

<?php if ($model->invoiceStatus->paymentAllowed($model->type)
    && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
) {
    echo $this->render('_modal_add_flow', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]);
} ?>

<?php
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-bottom',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'bottom',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-left',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'side' => 'left',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'zIndex' => 10000,
    ],
]);
$urlActivateScanPopup = \yii\helpers\Url::to(['/documents/invoice/run-scan-popup']);
$canShowScan = false;
$this->registerJs(<<<JS
setTimeout(function() {
    $('.tooltipstered-main').tooltipster('open');
}, 1000);
$('#send').on('shown.bs.modal', function() {
    $('.tooltip-self-send').tooltipster({
        "theme":["tooltipster-noir","tooltipster-noir-customized"],
        "trigger":"custom",
        "side":"right",
        "zIndex":$.topZIndex('.modal-scrollable'),
    });
    setTimeout(function(){
        $('.tooltip-self-send').tooltipster('open');
    }, 500);
});
$(document).on('click','.btn-close-tooltip-templates',function() {
    var name = $(this).closest('.box-tooltip-templates').attr('id') + "_{$model->company_id}";
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $(this).closest('.tooltipster-base').hide();
});

$('.open-send-to').on('click', function() {
    $('.dropdown-email').addClass('open-users');
});

JS
);
