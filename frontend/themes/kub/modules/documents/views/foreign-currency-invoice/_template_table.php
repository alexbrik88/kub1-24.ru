<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\OrderPackingList;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use PhpOffice\PhpWord\IOFactory;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/*
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \frontend\modules\documents\models\AutoinvoiceSearch
 */

if (isset($id)) {
    $emptyMessage = 'Вы еще не создали ни одного шаблона. ' . Html::a('Создать шаблон.', ['/documents/invoice/create', 'type' => 2, 'auto' => 1, 'contractorId' => $id]);
} else {
    $emptyMessage = 'Вы еще не создали ни одного шаблон. ' . Html::a('Создать шаблон.', ['/documents/invoice/create', 'type' => 2, 'auto' => 1]);
}
$useContractor = isset($useContractor) ? $useContractor : false;

$layoutView = (isset($noScroll)) ? 'layout_no_scroll' : 'layout';
$tableOpt = (isset($noScroll)) ? 'table-fixed' : '';

if (!isset($tabViewClass)) {
    $tabViewClass = Yii::$app->user->identity->config->getTableViewClass('table_view_autoinvoice');
}
?>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list' . $tableOpt . $tabViewClass,
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('//layouts/grid/' . $layoutView, ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        [
            'attribute' => 'created_at',
            'label' => 'Дата создания',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '8%',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ шаблона',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '8%',
            ],
            'format' => 'raw',
            'value' => function (Autoinvoice $data) use ($useContractor) {
                return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $data->invoice,
                ]) ? Html::a($data->document_number, [
                    '/documents/invoice/view-auto',
                    'id' => $data->id,
                    'contractorId' => $useContractor ? $data->contractor_id : null,
                ]) : $data->document_number;
            },
        ],
        [
            'attribute' => 'period',
            's2containerCssClass' => 'period-filter-in-grid',
            'label' => 'Периодичность',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '13%',
            ],
            'filter' => ['' => 'Все'] + Autoinvoice::$PERIOD,
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return Autoinvoice::$PERIOD[$model->period];
            },
        ],
        [
            'attribute' => 'contractor_id',
            's2containerCssClass' => 'contractor-filter-in-grid',
            'label' => 'Покупатель',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-left text-ellipsis',
            ],
            'filter' => $searchModel->getContractorList(),
            'hideSearch' => false,
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                $name = $model->invoice->contractor->getShortName();
                return '<div style="max-width:250px;padding-right:10px;">'.
                    Html::a($name, [
                    '/contractor/view',
                    'type' => $model->type,
                    'id' => $model->contractor_id,
                ], [
                    'class' => 'link',
                    'title' => html_entity_decode($name),
                    'data' => [
                        'id' => $model->contractor_id,
                    ]
                ]).'</div>';
            },
            'visible' => $this->context->id != 'contractor',
        ],
        [
            'attribute' => 'amount',
            'label' => 'Сумма',
            'headerOptions' => [
                'width' => '8%',
            ],
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return TextHelper::invoiceMoneyFormat($model->amount, 2);
            },
        ],
        [
            'attribute' => 'product_id',
            'selectPluginOptions' => [
                'templateResult' => new JsExpression('
                    function(data, container) {
                        if (data.id != "") {
                            data.title = data.text;
                            container.setAttribute("title", data.text);
                        }
                        container.innerHTML = data.text;
                        return container;
                    }
                '),
            ],
            's2containerCssClass' => 'product-filter-in-grid',
            'options' => [
                'class' => 'text-left text-ellipsis',
            ],
            'label' => 'Наименование',
            'headerOptions' => [
                'width' => '20%',
            ],
            'filter' => $searchModel->getProductList(),
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                $list = '<ul style="list-style: none; margin: 0 0 0 -40px;">';
                foreach ($model->invoice->orders as $autoorder){
                    $list .= '<li class="bounded-string">'. $autoorder->product_title . '</li>';
                }
                $list .= '</ul>';

                return $list;
            },
        ],
        [
            'attribute' => 'last_invoice_date',
            'label' => 'Последний счёт',
            'headerOptions' => [
                'width' => '11%',
            ],
            'format' => 'raw',
            'value' => function(Autoinvoice $model){
                return $model->last_invoice_date != null ? Yii::$app->formatter->asDate($model->last_invoice_date) : '';
            },
        ],
        [
            'attribute' => 'next_invoice_date',
            'label' => 'Следующий счёт',
            'headerOptions' => [
                'width' => '11%',
            ],
            'format' => 'raw',
            'value' => function(Autoinvoice $model){
                if (!$model->next_invoice_date || $model->status !== Autoinvoice::ACTIVE || $model->next_invoice_date > $model->date_to) {
                    return '';
                }

                return Yii::$app->formatter->asDate($model->next_invoice_date);
            },
        ],
        [
            'attribute' => 'status',
            'label' => 'Статус',
            'value' => 'statusName',
            's2width' => '150px',
            'headerOptions' => [
                'width' => '8%',
            ],
            'contentOptions' => [
                'class' => 'text-left text-ellipsis',
            ],
            'filter' => [
                '' => 'Все',
                1 => 'Активные',
                2 => 'Остановленные',
            ]
        ],
    ],
]);

echo $this->render('../invoice-facture/_form_create', [
    'useContractor' => $useContractor,
]);

?>
