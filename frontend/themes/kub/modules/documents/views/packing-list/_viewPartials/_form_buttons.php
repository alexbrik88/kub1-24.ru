<?php

use common\models\Company;
use common\models\document\Invoice;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
/* @var $isAuto boolean */

$invoiceBlock = 'invoice-block';
$autoinvoiceBlock = 'autoinvoice-block';
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button'
                    . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg create-invoice'
                    . ((Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <div class="<?= $invoiceBlock ?>">
                <?php
                $cancelUrl = Url::previous('lastPage');
                $permission = $model->isNewRecord ?
                    frontend\rbac\permissions\document\Document::INDEX :
                    frontend\rbac\permissions\document\Document::VIEW;

                if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                    echo Html::a('Отменить', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]);
                    echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]);
                } ?>
            </div>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>
