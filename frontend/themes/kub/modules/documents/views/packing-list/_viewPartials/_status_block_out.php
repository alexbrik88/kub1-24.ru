<?php

use common\components\date\DateHelper;
use common\models\document\status\PackingListStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $useContractor string */

$status = $model->statusOut;

$statusIcon = [
    PackingListStatus::STATUS_CREATED => 'check-2',
    PackingListStatus::STATUS_PRINTED => 'check-2',
    PackingListStatus::STATUS_SEND => 'envelope',
    PackingListStatus::STATUS_RECEIVED => 'check-double',
    PackingListStatus::STATUS_REJECTED => 'check-2',
];
$statusColor = [
    PackingListStatus::STATUS_CREATED => '#26cd58',
    PackingListStatus::STATUS_PRINTED => '#FAC031',
    PackingListStatus::STATUS_SEND => '#FAC031',
    PackingListStatus::STATUS_RECEIVED => '#26cd58',
    PackingListStatus::STATUS_REJECTED => '#4679AE',
];
$icon = ArrayHelper::getValue($statusIcon, $status->id);
$color = ArrayHelper::getValue($statusColor, $status->id);
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: <?= $color ?>;
            border-color: <?= $color ?>;
            color: #ffffff;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->status_out_updated_at) ?></span>
        </div>
    </div>
</div>
<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
    'model' => $model->invoice,
])) : ?>

    <?php $invoiceTitle = 'Счет № ' . $model->invoice->fullNumber . ' от ' . DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>

    <?= Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']) . Html::tag('span', $invoiceTitle, ['class' => 'ml-3 mr-1']), [
        '/documents/invoice/view',
        'type' => $model->type,
        'id' => $model->invoice->id,
    ], [
        'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3',
    ]) ?>

<?php endif; ?>