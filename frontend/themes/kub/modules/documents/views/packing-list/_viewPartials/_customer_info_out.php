<?php

use common\components\date\DateHelper;
use common\models\document\PackingList;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\Contractor;
use \frontend\themes\kub\helpers\Icon;

/* @var \yii\web\View $this */
/* @var PackingList $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$proxyDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->proxy_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$basisDocumentDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$productionType = explode(', ', $model->invoice->production_type);

//$agreementDropDownList = $model->basisDropDownList([
//    '' => '   ',
//    'add-modal-agreement' => Icon::PLUS . ' Добавить договор',
//]);

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#packinglist-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}

$consignorArray = $model->consignorArray;
$model->consignor_id .= '';
$model->consignee_id .= '';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

    <div class="wrap">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="column">
                <div class="row row_indents_s flex-nowrap align-items-center">
                    <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Исходящая ТН
                        </span>
                    </span>
                        <span>№</span>
                    </div>
                    <!-- document_number -->
                    <div class="form-group d-inline-block mb-0 col-xl-2">
                        <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>
                    </div>
                    <!-- document_additional_number -->
                    <div class="form-group d-inline-block mb-0 col-xl-2">
                        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                            <?= Html::activeTextInput($model, 'document_additional_number', [
                                'maxlength' => true,
                                'id' => 'account-number',
                                'data-required' => 1,
                                'class' => 'form-control',
                                'placeholder' => 'доп. номер',
                                'style' => 'display: inline;',
                            ]); ?>
                        <?php endif ?>
                    </div>
                    <div class="form-txt d-inline-block mb-0 column">от</div>
                    <!-- document_date -->
                    <div class="form-group d-inline-block mb-0">
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'document_date', [
                                'id' => 'under-date',
                                'class' => 'form-control form-control_small date-picker invoice_document_date',
                                'size' => 16,
                                'data-date' => '12-02-2012',
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->document_date,
                                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap">
        <div class="row d-block">

            <div class="form-group col-12">
                <div class="row">
                    <div class="col-3">
                        <label class="label">Транспортная накладная №</label>
                        <?= Html::activeTextInput($model, 'waybill_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control  input-editable-field ',
                            'placeholder' => 'номер',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">От</label>
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'waybill_date', [
                                'class' => 'form-control date-picker']) ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузоотправитель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignor_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $consignorArray,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <br>
            <div class="form-group col-12">
                <div class="row">
                    <div class="col-3">
                        <label class="label">Доверенность</label>
                        <?= Html::activeTextInput($model, 'proxy_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'class' => 'form-control  input-editable-field ',
                            'placeholder' => 'номер',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">От</label>
                        <div class="date-picker-wrap">
                            <?= Html::activeTextInput($model, 'proxy_date', [
                                'class' => 'form-control date-picker']) ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/img/svg/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-12">
                <div class="row">
                    <div class="col-3">
                        <label class="label">Должность</label>
                        <?= Html::activeTextInput($model, 'given_out_position', [
                            'maxlength' => true,
                            'class' => 'form-control input-editable-field ',
                            'placeholder' => '',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">ФИО</label>
                        <?= Html::activeTextInput($model, 'given_out_fio', [
                            'maxlength' => true,
                            'class' => 'form-control input-editable-field ',
                            'placeholder' => '',
                        ]); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-6">
                <label class="label">Грузополучатель</label>

                <?= Select2::widget([
                    'model' => $model,
                    'attribute' => 'consignee_id',
                    'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $consignorArray,
                    'options' => [
                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                    ],
                    'pluginOptions' => [
                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                        'width' => '100%'
                    ],
                ]); ?>
            </div>
            <br>

            <div class="form-group col-12">
                <div class="row">
                    <div class="col-3">
                        <label class="label">Груз принял</label>
                        <?= Html::activeTextInput($model, 'cargo_accepted_position', [
                            'maxlength' => true,
                            'class' => 'form-control',
                            'placeholder' => 'Должность',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label" style="height: 14px;"></label>
                        <?= Html::activeTextInput($model, 'cargo_accepted_fullname', [
                            'maxlength' => true,
                            'class' => 'form-control',
                            'placeholder' => 'ФИО',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label">Груз получил</label>
                        <?= Html::activeTextInput($model, 'cargo_received_position', [
                            'maxlength' => true,
                            'class' => 'form-control',
                            'placeholder' => 'Должность',
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <label class="label" style="height: 14px;"></label>
                        <?= Html::activeTextInput($model, 'cargo_received_fullname', [
                            'maxlength' => true,
                            'class' => 'form-control',
                            'placeholder' => 'ФИО',
                        ]); ?>
                    </div>
                </div>
            </div>
            <br/>

            <div class="form-group col-12">
                <div class="form-filter">
                    <label class="label" for="client">Покупатель<span class="important">*</span></label>
                    <div class="row">
                        <div class="col-6">
                            <?php echo Select2::widget([
                                'name' => 'contractor_id',
                                'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                                'disabled' => true,
                                'pluginOptions' => [
                                    'width' => '100%'
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-12">
                <div class="form-filter">
                    <label class="label" for="client">Основание</label>
                    <div class="row">
                        <div class="col-6">
                            <?php Pjax::begin([
                                'id' => 'agreement-pjax-container',
                                'enablePushState' => false,
                                'linkSelector' => false,
                            ]);
                            echo \frontend\widgets\AgreementDropdownWidget::widget([
                                'model' => $model,
                                'attribute' => 'agreement',
                                'invoiceId' => $model->invoice->id,
                                'contractorId' => $model->invoice->contractor_id,
                                'documentType' => $model->invoice->type
                            ]);
                            //echo Select2::widget([
                            //    'model' => $model,
                            //    'attribute' => 'agreement',
                            //    'data' => $agreementDropDownList,
                            //    'hideSearch' => true,
                            //    'pluginOptions' => [
                            //        'width' => '100%',
                            //        'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            //    ],
                            //]);
                            Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group col-6 mb-0">
                <?= Html::activeCheckbox($model, 'add_stamp', [
                    'class' => 'kub-switch',
                    'label' => false,
                ]); ?>
                <label class="label ml-1" for="packinglist-add_stamp">Печать и подпись</label>
                <span class="tooltip-hover ico-question valign-middle"
                      data-tooltip-content="#tooltip_add_stamp"
                      style="vertical-align: top;">
                </span>
            </div>

        </div>
    </div>

    <div class="tooltip_templates" style="display: none;">
        <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
            Добавить в ТН печать и подпись
            <br>
            при отправке по e-mail и
            <br>
            при скачивании в PDF
        </span>
    </div>

<?php $this->registerJs('
    $("#packinglist-consignee_id").change(function () {
        var $contractorAddressBlock = $(".contractor-address-block");
        var $contractorID = $(this).val();
        if ($contractorID == "") {
            $contractorID = $contractorAddressBlock.data("current");
        }

        $.post("/contractor/is-different-address", {
                id: $contractorID
           }, function (data) {
                if (data.isDifferent == true) {
                    $contractorAddressBlock.show();
                } else {
                    $contractorAddressBlock.hide();
                }
                $contractorAddressBlock.find("#packinglist-contractor_address input[value=\"0\"]").prop("checked", true).click();
           });
    });
'); ?>