<?php
/** @var \common\models\document\PackingList $model */
use common\components\TextHelper;

?>

<div class="pade-number">Страница 1</div>
<table class="table table-bordered customers_table th-bg-none table-b">
    <thead class="th-p-t-b-0">
    <tr class="heading" role="row">
        <th rowspan="2" class="text-center" width="1%">Но-мер по по-рядку</th>
        <th colspan="2" class="text-center b-b-ddd">Товар</th>
        <th colspan="2" class="text-center b-b-ddd p-l-r-0">Единица измерения
        </th>
        <th rowspan="2" class="text-center" width="3%">Вид упаков-ки</th>
        <th colspan="2" class="text-center b-b-ddd">Количество</th>
        <th rowspan="2" class="text-center" width="3%">Масса брутто</th>
        <th rowspan="2" class="text-center" width="3%">Коли-чество (масса
            нетто)
        </th>
        <th rowspan="2" class="text-center" width="8%">Цена, руб. коп.</th>
        <th rowspan="2" class="text-center" width="8%">Сумма без учета НДС, руб.
            коп.
        </th>
        <th colspan="2" class="text-center b-b-ddd">НДС</th>
        <th rowspan="2" class="text-center" width="9%">Сумма с учётом НДС, руб.
            коп.
        </th>
    </tr>
    <tr class="heading" role="row">
        <th class="text-center" width="18%">наименование, характеристика, сорт,
            артикул товара
        </th>
        <th class="text-center" width="3%">код</th>
        <th class="text-center" width="5%">наиме-нование</th>
        <th class="text-center" width="5%">код по ОКЕИ</th>
        <th class="text-center" width="3%">в одном месте</th>
        <th class="text-center" width="3%">мест, штук</th>
        <th class="text-center" width="6%">ставка, %</th>
        <th class="text-center" width="8%">сумма, руб. коп.</th>
    </tr>
    </thead>
    <tbody>
    <tr class="td-p-0 text-center">
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
        <td>10</td>
        <td>11</td>
        <td>12</td>
        <td>13</td>
        <td>14</td>
        <td>15</td>
    </tr>
    <?php foreach ($model->invoice->getOrdersByProductType(\common\models\product\Product::PRODUCTION_TYPE_GOODS) as $key => $order): ?>
        <tr role="row" class="odd td-p-t-b-0">
            <td class="text-right"><?= $key + 1; ?></td>
            <td><?= $order->product_title; ?></td>
            <td><?= $order->article; ?></td>
            <td class="text-center"><?= $order->product->productUnit ? $order->product->productUnit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
            <td class="text-center"><?= $order->product->productUnit ? $order->product->productUnit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
            <td class="text-center"><?= $order->product->box_type; ?></td>
            <td class="text-right"><?= $order->count_in_place; ?></td>
            <td class="text-right"><?= $order->place_count; ?></td>
            <td class="text-right"><?= $order->mass_gross; ?></td>
            <td class="text-right"><?= $order->quantity; ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->selling_price_with_vat, 2); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amount_sales_no_vat, 2); ?></td>
            <td class="text-center">
                <?= $order->saleTaxRate->name; ?>
            </td>
            <td class="text-right">
                <?php if ($model->invoice->hasNds): ?>
                    <?= TextHelper::invoiceMoneyFormat($order->sale_tax, 2); ?>
                <?php endif; ?>
            </td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amount_sales_with_vat, 2); ?></td>
        </tr>
    <?php endforeach; ?>
    <tr role="row" class="odd td-p-t-b-0">
        <td class="text-right b-l-b-0" colspan="7">Итого</td>
        <td class="text-right"><?= $model->invoice->total_place_count; ?></td>
        <td class="text-right"><?= $model->invoice->total_mass_gross; ?></td>
        <td class="text-right"><?= $model->invoice->total_order_count; ?></td>
        <td class="text-center">X</td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_no_nds, 2); ?></td>
        <td class="text-center">X</td>
        <td class="text-right">
            <?php if ($model->invoice->hasNds): ?>
                <?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_nds, 2); ?>
            <?php endif; ?>
        </td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_with_nds, 2); ?></td>
    </tr>
    <tr role="row" class="odd td-p-t-b-0">
        <td class="text-right b-l-b-0" colspan="7">Всего по накладной</td>
        <td class="text-right"><?= $model->invoice->total_place_count; ?></td>
        <td class="text-right"><?= $model->invoice->total_mass_gross; ?></td>
        <td class="text-right"><?= $model->invoice->total_order_count; ?></td>
        <td class="text-center">X</td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_no_nds, 2); ?></td>
        <td class="text-center">X</td>
        <td class="text-right">
            <?php if ($model->invoice->hasNds): ?>
                <?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_nds, 2); ?>
            <?php endif; ?>
        </td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->invoice->total_amount_with_nds, 2); ?></td>
    </tr>
    </tbody>
</table>
