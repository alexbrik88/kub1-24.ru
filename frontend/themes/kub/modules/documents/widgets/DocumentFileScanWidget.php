<?php

namespace frontend\themes\kub\modules\documents\widgets;

use common\models\document\AbstractDocument;
use frontend\modules\documents\assets\DocumentFileScanAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class DocumentFileScanWidget extends Widget
{
    /**
     * @var AbstractDocument
     */
    public $_model;

    /**
     * @var boolean
     */
    public $hasFreeScan = false;

    /**
     * @var int
     */
    public $maxFileCount = 3;

    /**
     * @var string
     */
    public $uploadUrl;
    public $deleteUrl;
    public $listUrl;
    public $scanFreeUrl;
    public $scanBindUrl;
    public $scanListUrl;

    /**
     * @var string
     */
    public $uploadBtnClass = 'document-file-upload-btn';

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->uploadUrl === null) {
            throw new InvalidConfigException('Ссылка на загрузку файла не определена.');
        }
        if ($this->deleteUrl === null) {
            throw new InvalidConfigException('Ссылка на удаление файла не определена.');
        }
        if ($this->listUrl === null) {
            throw new InvalidConfigException('Ссылка на список файлов не определена.');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        ////DocumentFileScanAsset::register($this->view);

        return $this->render('documentFileScanWidget', ['maxFileCount' => $this->maxFileCount]);
    }

    /**
     * @inheritdoc
     */
    public function setModel($model)
    {
        $this->_model = $model;
    }

    /**
     * @return AbstractDocument
     */
    public function getModel()
    {
        return $this->_model;
    }
}
