<?php

namespace frontend\themes\kub\modules\documents\widgets;

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\status\AgreementStatus;
use common\models\document\Waybill;
use common\models\EmployeeCompany;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


/**
 * Class DocumentLogWidget
 * @package frontend\themes\kub\modules\documents\widgets
 */
class DocumentAutoivoiceLogWidget extends Widget
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $closeButton;
    /**
     * @var
     */
    public $toggleButton;
    /**
     * @var
     */
    public $footer;

    /**
     *
     */
    public function run()
    {
        if (!$this->model instanceof InvoiceAuto) {
            return 'unknown model';
        }
        $logArray = Log::find()->andWhere([
            'model_id' => $this->model->auto->id,
            'model_name' => Autoinvoice::class,
            'company_id' => Yii::$app->user->identity->company->id,
            'log_event_id' => LogEvent::LOG_EVENT_UPDATE_STATUS,
        ])->all();
        if ($invoicesIds = $this->model->auto->getInvoices()->select('id')->column()) {
            $sendsLogArray = Log::find()->andWhere([
                'model_id' => $invoicesIds,
                'model_name' => InvoiceAuto::class,
                'company_id' => Yii::$app->user->identity->company->id,
                'log_event_id' => LogEvent::LOG_EVENT_UPDATE_STATUS,
            ])->all();
        } else {
            $sendsLogArray = [];
        }

        Modal::begin([
            'options' => [
                'class' => 'doc-history-modal fade',
            ],
            'closeButton' => $this->closeButton !== null ? $this->closeButton : false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'class' => 'btn darkblue btn-sm info-button',
                'style' => 'width:33px; height: 27px;',
                'label' => '<i class="icon-info" style="color:white"></i>'
            ],
            'footer' => $this->footer !== null ? $this->footer : Html::button('OK', [
                'class' => 'button-list button-hover-transparent button-clr',
                'data-dismiss' => 'modal',
            ]),
        ]);

        echo '<h4 class="modal-title">Последние действия</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>';

        $tmp = [];
        foreach ($logArray as $log) {
            $statusId = ArrayHelper::getValue($log->attributesNewArray, 'status');
            $eventName = ($statusId == Autoinvoice::ACTIVE) ? 'запущен' : 'остановлен';
            $fio = $log->employeeCompany ? $log->employeeCompany->getShortFio() : ($log->employee ? $log->employee->getShortFio() : '');
            $tmp[] = ['time' => $log->created_at, 'msg' => date('d.m.Y H:i:s', $log->created_at) . " $eventName $fio"];
        }

        /** @var Log $log */
        foreach ($sendsLogArray as $log) {
            $eventName = 'отправлен';
            //$authorId = ArrayHelper::getValue($log->attributesNewArray, 'document_author_id');
            //if ($authorId && ($author = EmployeeCompany::findOne(['company_id' => $log->company_id, 'employee_id' => $authorId]))) {
            //    $fio = $author->getShortFio();
            //} else {
            //    $fio = $log->employeeCompany ? $log->employeeCompany->getShortFio() : ($log->employee ? $log->employee->getShortFio() : '');
            //}
            $invoiceId = ArrayHelper::getValue($log->attributesNewArray, 'id');
            if ($invoice = Invoice::findOne($invoiceId)) {
                $fio = ' Счет №' . $invoice->getFullNumber();
                $tmp[] = ['time' => $log->created_at, 'msg' => date('d.m.Y H:i:s', $log->created_at) . " $eventName $fio"];
            }
        }

        usort($tmp, function($first, $second) {
            return (intval($first['time']) - intval($second['time']));
        });

        $history = [];
        $history[] = date('d.m.Y', $this->model->created_at) . ' создан ' . $this->model->documentAuthor->getShortFio();
        foreach ($tmp as $t) $history[] = $t['msg'];
        //$history[] = DateHelper::format($this->model->auto->next_invoice_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . " дата следующей отправки";
        foreach ($history as $h)
            echo Html::tag('div', $h, [
                'class' => 'created-by',
                'style' => 'margin-top: 5px;',
            ]);

        Modal::end();
    }
}
