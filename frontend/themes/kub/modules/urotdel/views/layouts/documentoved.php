<?php

use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php')

?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2">
        <?= Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Внесение изменений в ООО',
                    'url' => ['/urotdel/default'],
                    'active' => Yii::$app->controller->action->id == 'index',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Трудовые документы',
                    'url' => ['/urotdel/default/labor-documents'],
                    'active' => Yii::$app->controller->action->id == 'labor-documents',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]); ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->registerJs("
    let visitId = null;
    let url = 'default/visit';
    let duration = 1;

    function visit(duration) {
        $.post(url, {
                duration: duration,
                visitId: visitId
            }, function(response) {
                if (response !== 0) {
                    visitId = response;
                }
        });
    }
    
    visit(1);
    setInterval(visit, 5000, 5);
") ?>
<?php $this->endContent(); ?>

