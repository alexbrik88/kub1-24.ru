<?php

/* @var $this yii\web\View
 * @var $model \frontend\modules\urotdel\models\LaborDocumentsFrom
 */

use frontend\modules\urotdel\models\LaborDocumentsFrom;
use yii\bootstrap\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Трудовые документы';
?>
<div class="wrap pt-3 pl-4 pr-4 pb-4">
    <div class="pt-3 pl-1 pr-1 pb-1">
        <?php $form = ActiveForm::begin(array_replace(Yii::$app->params['formDefaultConfig'], [
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'validateOnSubmit' => true,
            'id' => 'labor-documents-form',
            'fieldConfig' => [
                'labelOptions' => [
                    'class' => 'label',
                ],
                'inputOptions' => [
                    'class' => 'form-control'
                ],
                'wrapperOptions' => [
                    'class' => 'row col-md-6',
                ],
            ],
        ])); ?>
            <div style="margin-bottom: 20px;">Раздел в работе.</div>
            <div>
                <b>Нам важно знать ваше мнение</b> – ниже документы, которые планируем добавить.<br>
                Отметьте те, которые вы бы использовали в работе и в дополнительно укажите те, которые вам нужны, но их нет в списке.

                <?= $form->field($model, 'documents', [
                    'options' => [
                        'style' => 'margin-top: 10px;',
                    ],
                ])->checkboxList(LaborDocumentsFrom::$documentsMap)->label(false) ?>

                <?= $form->field($model, 'otherWishes')->textarea(['rows' => 6])->label('Ваши пожелания') ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить пожелания', [
                        'class' => 'button-regular button-regular_red button-clr mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
