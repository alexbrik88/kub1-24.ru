<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.03.2020
 * Time: 21:42
 */

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\widgets\TableViewWidget;
use php_rutils\RUtils;
use frontend\themes\kub\helpers\Icon;
use frontend\modules\reports\models\AbstractFinance;

/* @var $this yii\web\View
 * @var $user \common\models\employee\Employee
 * @var $searchModel ArticlesSearch
 * @var $data []
 * @var $type int
 */

$MAX_CONTRACTORS = $contractorsOnPage ?? 9E9;

$user = Yii::$app->user->identity;
$questionIco = '<svg class="tooltip-question-icon svg-icon"><use xlink:href="/img/svg/svgSprite.svg#question"></use></svg>';
$preloader = '<span class="loader pl-2" style="display: none"><img width="18" src="/img/ofd-preloader.gif"/></span>';
?>
<div class="table-settings row row_indents_s">
    <div class="col-6">
        <?php if ($type == ArticlesSearch::TYPE_BY_ACTIVITY): ?>
            <?= Html::a(
                '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#exel"></use></svg>',
                array_merge(['get-xls'], Yii::$app->request->queryParams),
                [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2 mb-2',
                    'title' => 'Скачать в Excel',
                ]
            ) ?>
        <?php endif; ?>
    </div>
    <div class="col-6">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'action' => Url::current([$searchModel->formName() => null]),
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
        <div class="form-group flex-grow-1 mr-2">
            <?= Html::activeTextInput($searchModel, 'search', [
                'type' => 'search',
                'placeholder' => 'Поиск по статьям и контрагентам',
                'class' => 'form-control',
            ]); ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Найти', [
                'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
            ]) ?>
        </div>
        <?php $form->end(); ?>
    </div>
</div>


<script>
    window.customersFullList = [];
    window.sellersFullList = [];
</script>


<div class="wrap wrap_padding_none_all" style="margin-top: 5px;">
    <ul class="articles-list articles-list-sorting-disabled" data-type="<?= $type ?>">




        <?php if ($type === ArticlesSearch::TYPE_BY_ACTIVITY): ?>

            <?php foreach ($data as $item): ?>

                <li class="main-block">
                    <?= $item['name'] . Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'data-tooltip-content' => $item['id'] === AbstractFinance::RECEIPT_FINANCING_TYPE_FIRST
                            ? '#tooltip_financial-operations-block'
                            : ($item['id'] === AbstractFinance::INCOME_OPERATING_ACTIVITIES
                                ? '#tooltip_operation-activities-block'
                                : ($item['id'] === ArticlesSearch::BLOCK_FREE_CONTRACTORS
                                    ? '#tooltip_free-contractors-block'
                                    : '#tooltip_investment-activities-block')),
                    ]) ?>
                </li>

                <?php foreach ($item['items'] as $key => $flowItem): ?>

                    <!-- MAIN INCOME/EXPENSE ROW -->
                    <li class="category flow-item-category <?= empty($searchModel->search) ? null : 'open' ?>"
                        id="<?= "flow-type-{$item['id']}-{$flowItem['id']}" ?>">
                        <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                            'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                        ]) ?>
                        <?= $flowItem['name'] ?>
                        <div class="actions">
                            <?= $this->render('_element/contractors_count', [
                                'flowType' => $flowItem['flowType'],
                                'count' => $flowItem['contractorCount']
                            ]) ?>
                            <div class="add-item-block">
                                <?php if (!isset($flowItem['contractors'])): ?>
                                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить СТАТЬЮ '
                                        /*. ($flowItem['flowType'] === ArticlesSearch::TYPE_INCOME ? 'прихода' : 'расхода')*/ . '</span>', '#', [
                                        'class' => 'button-regular button-hover-content-red hover-visible article-modal-link text-left',
                                        'data-url' => Url::to(['create', 'activeTab' => $type, 'type' => $flowItem['flowType']]),
                                        'style' => 'margin-left: 15px; padding-left: 15px;',
                                    ]) ?>
                                <?php endif; ?>
                            </div>
                            <div class="switch-item-block"></div>
                            <div class="update-item-block"></div>
                            <div class="delete-item-block"></div>
                        </div>
                    </li>

                    <?php if (isset($flowItem['items'])): ?>
                        <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>">
                            <?php foreach ($flowItem['items'] as $basicItem): ?>
                                <?php $model = $basicItem['flowType'] === ArticlesSearch::TYPE_INCOME
                                    ? InvoiceIncomeItem::findOne($basicItem['id'])
                                    : InvoiceExpenditureItem::findOne($basicItem['id']);
                                $showSwitch = $basicItem['canBeControlled'] && $basicItem['flowOfFundsItemId'] ?>

                                <!-- ARTICLE ROW LEVEL 1 -->
                                <li class="category item-<?= $basicItem['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                    id="<?= "flow-type-{$item['id']}-{$flowItem['id']}-item-{$basicItem['id']}" ?>"
                                    style="padding-left: 50px;">
                                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                    ]) ?>
                                    <div class="itemName"><?= $basicItem['name'] ?></div>
                                    <div class="actions">
                                        <div class="add-contractor-block">
                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                . ($basicItem['flowType'] === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', 'javascript:;', [
                                                'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                'data-url' => Url::to(['update', 'id' => $basicItem['id'], 'type' => $basicItem['flowType'], 'onlyContractor' => true, 'activeTab' => $type]),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <?= $this->render('_element/contractors_count', [
                                            'flowType' => $basicItem['flowType'],
                                            'count' => $basicItem['contractorCount']
                                        ]) ?>
                                        <div class="add-sub-article-block">
                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить подстатью' . '</span>', 'javascript:;', [
                                                'class' => 'button-regular button-hover-content-red article-modal-link hover-visible',
                                                'data-url' => Url::to(['create', 'activeTab' => $type, 'type' => $basicItem['flowType'], 'parent' => $basicItem['id']]),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <div class="switch-item-block">
                                            <?php if ($showSwitch): ?>
                                                <div class="switch hover-visible"
                                                     title="<?= $basicItem['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить' ?> статью для использования в работе"
                                                     style="margin-left: 15px;">
                                                    <?= Html::checkbox('is_visible', $basicItem['isVisibleFlowOfFundsItem'], [
                                                        'id' => "is-visible-{$basicItem['flowOfFundsItemId']}",
                                                        'class' => 'is-visible-switch md-check switch-input input-hidden hidden '
                                                            . ($basicItem['isVisibleFlowOfFundsItem'] && !$model->getCanToggleVisibility() ? 'cant-disable' : null),
                                                        'data-url' => Url::to(['toggle-is-visible', 'id' => $basicItem['id'], 'type' => $basicItem['flowType'], 'filterType' => $type]),
                                                        'data-message' => ($basicItem['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить') . " статью \"{$basicItem['name']}\" для использования в работе?",
                                                    ]) ?>
                                                    <label class="d-flex align-items-center switch-label pl-0"
                                                           for="<?= "is-visible-{$basicItem['flowOfFundsItemId']}" ?>"
                                                           style="cursor: pointer;">
                                                        <span class="switch-pseudo position-relative">&nbsp;</span>
                                                    </label>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="update-item-block">
                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                'class' => 'article-modal-link hover-visible',
                                                'data-url' => Url::to(['update', 'id' => $basicItem['id'], 'type' => $basicItem['flowType'], 'activeTab' => $type]),
                                                'title' => Yii::t('yii', 'Редактировать'),
                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <div class="delete-item-block">
                                            <?php if ($basicItem['companyId'] !== null): ?>
                                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                    'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                    'data' => [
                                                        'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                        'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                        'message' => "Вы уверены, что хотите удалить статью \"{$basicItem['name']}\"?",
                                                        'url' => Url::to(['delete', 'id' => $basicItem['id'], 'type' => $basicItem['flowType'], 'filterType' => $type]),
                                                    ],
                                                    'title' => Yii::t('yii', 'Удалить'),
                                                    'aria-label' => Yii::t('yii', 'Удалить'),
                                                    'style' => 'margin-left: 15px;',
                                                ]) ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </li>
                                <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                                    data-type="<?= $basicItem['flowType'] ?>" data-id="<?= $basicItem['id'] ?>">

                                    <!-- CHILDREN -->
                                    <?php if (!empty($basicItem['children'])): ?>
                                        <?php foreach ($basicItem['children'] as $subItem): ?>
                                                <?php $model = $basicItem['flowType'] === ArticlesSearch::TYPE_INCOME
                                                    ? InvoiceIncomeItem::findOne($subItem['id'])
                                                    : InvoiceExpenditureItem::findOne($subItem['id']);
                                                $showSwitch = $subItem['canBeControlled'] && $subItem['flowOfFundsItemId']; ?>
                                                <li class="category item-<?= $subItem['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                                    id="<?= "flow-type-{$item['id']}-{$flowItem['id']}-item-{$subItem['id']}" ?>"
                                                    style="padding-left: 80px;">
                                                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                                    ]) ?>
                                                    <div class="itemName"><?= $subItem['name'] ?></div>
                                                    <div class="actions">
                                                        <div class="add-contractor-block">
                                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                                . ($basicItem['flowType'] === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', 'javascript:;', [
                                                                'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                                'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $basicItem['flowType'], 'onlyContractor' => true, 'activeTab' => $type]),
                                                                'style' => 'margin-left: 15px;',
                                                            ]) ?>
                                                        </div>
                                                        <?= $this->render('_element/contractors_count', [
                                                            'flowType' => $basicItem['flowType'],
                                                            'count' => $subItem['contractorCount']
                                                        ]) ?>
                                                        <div class="empty-block" style="width: 249px;"></div>
                                                        <div class="update-item-block">
                                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                                'class' => 'article-modal-link hover-visible',
                                                                'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $basicItem['flowType'], 'activeTab' => $type]),
                                                                'title' => Yii::t('yii', 'Редактировать'),
                                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                'style' => 'margin-left: 15px;',
                                                            ]) ?>
                                                        </div>
                                                        <div class="delete-item-block">
                                                            <?php if ($subItem['companyId'] !== null): ?>
                                                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                    'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                                    'data' => [
                                                                        'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                                        'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                                        'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\"?",
                                                                        'url' => Url::to(['delete', 'id' => $subItem['id'], 'type' => $basicItem['flowType'], 'filterType' => $type]),
                                                                    ],
                                                                    'title' => Yii::t('yii', 'Удалить'),
                                                                    'aria-label' => Yii::t('yii', 'Удалить'),
                                                                    'style' => 'margin-left: 15px;',
                                                                ]) ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                                                    data-type="<?= $basicItem['flowType'] ?>" data-id="<?= $subItem['id'] ?>">
                                                    <?php $lim = $MAX_CONTRACTORS ?>
                                                    <?php foreach ($subItem['contractors'] as $contractor): ?>
                                                        <?php if (--$lim < 0) {
                                                            echo Html::tag('span', $contractor['name'], [
                                                                'class' => 'tpl-contractor',
                                                                'data-id' => $contractor['id'],
                                                                'data-type' => $type,
                                                                'data-tab' => $type,
                                                                'style' => 'display:none'
                                                            ]);
                                                            continue;
                                                        } ?>
                                                        <li class="item" data-type="<?= $basicItem['flowType'] ?>"
                                                            data-id="<?= $contractor['id'] ?>" style="padding-left: 115px;">
                                                            <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                                'class' => 'joint-operation-checkbox' . ($basicItem['flowType'] == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                                'data-type' => $type,
                                                            ]) ?>
                                                            <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                                            </svg>
                                                            <span class="name"><?= $contractor['name'] ?></span>
                                                            <div class="actions">
                                                                <div class="update-item-block">
                                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                                        'class' => 'article-modal-link add-contractor-button hover-visible',
                                                                        'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $basicItem['flowType'], 'activeTab' => $type]),
                                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                        'style' => 'margin-left: 15px; text-decoration: none;',
                                                                    ]) ?>
                                                                </div>
                                                                <div class="delete-item-block">
                                                                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                        'class' => 'hover-visible can-delete-article',
                                                                        'data' => [
                                                                            'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\" у этого контрагента?",
                                                                            'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $basicItem['flowType'], 'filterType' => $type]),
                                                                        ],
                                                                        'title' => Yii::t('yii', 'Удалить'),
                                                                        'aria-label' => Yii::t('yii', 'Удалить'),
                                                                        'style' => 'margin-left: 15px;',
                                                                    ]) ?>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; ?>
                                                    <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 115px']); } ?>
                                                </ul>
                                            <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- end of CHILDREN -->

                                    <?php $lim = $MAX_CONTRACTORS ?>
                                    <?php foreach ($basicItem['contractors'] as $contractor): ?>
                                        <?php if (--$lim < 0) {
                                            echo Html::tag('span', $contractor['name'], [
                                                'class' => 'tpl-contractor',
                                                'data-id' => $contractor['id'],
                                                'data-type' => $type,
                                                'data-tab' => $type,
                                                'style' => 'display:none'
                                            ]);
                                            continue;
                                        } ?>
                                        <li class="item" data-type="<?= $basicItem['flowType'] ?>" data-id="<?= $contractor['id'] ?>" style="padding-left: 85px;">
                                            <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                'class' => 'joint-operation-checkbox' . ($basicItem['flowType'] == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                'data-type' => $basicItem['flowType'],
                                            ]) ?>
                                            <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                            </svg>
                                            <span class="name"><?= $contractor['name'] ?></span>
                                            <div class="actions">
                                                <div class="update-item-block">
                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                        'class' => 'article-modal-link add-contractor-button hover-visible',
                                                        'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $basicItem['flowType'], 'activeTab' => $type]),
                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                        'style' => 'margin-left: 15px; text-decoration: none;',
                                                    ]) ?>
                                                </div>
                                                <div class="delete-item-block">
                                                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                        'class' => 'hover-visible can-delete-article',
                                                        'data' => [
                                                            'message' => "Вы уверены, что хотите удалить статью \"{$basicItem['name']}\" у этого контрагента?",
                                                            'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $basicItem['flowType'], 'filterType' => $type]),
                                                        ],
                                                        'title' => Yii::t('yii', 'Удалить'),
                                                        'aria-label' => Yii::t('yii', 'Удалить'),
                                                        'style' => 'margin-left: 15px;',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                    <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 85px']); } ?>
                                </ul>
                            <?php endforeach; ?>
                        </ul>
                    <?php elseif (isset($flowItem['contractors'])): ?>
                        <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                            data-id="empty">
                            <?php $lim = $MAX_CONTRACTORS ?>
                            <?php foreach ($flowItem['contractors'] as $contractor): ?>
                                <?php if (--$lim < 0) {
                                    echo Html::tag('span', $contractor['name'], [
                                        'class' => 'tpl-contractor',
                                        'data-id' => $contractor['id'],
                                        'data-type' => $type,
                                        'data-tab' => $type,
                                        'style' => 'display:none'
                                    ]);
                                    continue;
                                } ?>
                                <li class="item" data-type="<?= $flowItem['flowType'] ?>"
                                    data-id="<?= $contractor['id'] ?>" style="padding-left: 35px;">
                                    <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                        'class' => 'joint-operation-checkbox' . ($flowItem['flowType'] == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                        'data-type' => $flowItem['flowType'],
                                    ]) ?>
                                    <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                        <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                    </svg>
                                    <span class="name"><?= $contractor['name'] ?></span>
                                    <div class="actions">
                                        <div class="add-contractor-block">
                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Выбрать Статью</span>', 'javascript:;', [
                                                'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $flowItem['flowType'], 'activeTab' => $type]),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <div class="switch-item-block">
                                        </div>
                                        <div class="update-item-block">
                                        </div>
                                        <div class="delete-item-block">
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                            <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 35px']); } ?>
                        </ul>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>





        <?php else: // ArticlesSearch::TYPE_INCOME || ArticlesSearch::TYPE_EXPENSE ?>

            <?php foreach ($data as $category): ?>
                <li class="category" id="<?= "{$category['id']}" ?>">
                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                    ]) ?>
                    <?= $category['name'] ?>
                    <?php if ($category['id'] === 'visible-items'): ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $type === ArticlesSearch::TYPE_INCOME
                                ? '#tooltip_enabled-income-items-block'
                                : '#tooltip_enabled-expenditure-items-block',
                        ]) ?>
                    <?php elseif ($category['id'] === 'not-visible-items'): ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => $type === ArticlesSearch::TYPE_INCOME
                                ? '#tooltip_disabled-income-items-block'
                                : '#tooltip_disabled-expenditure-items-block',
                        ]) ?>
                    <?php elseif ($category['id'] === ArticlesSearch::BLOCK_FREE_CONTRACTORS): ?>
                        <?= Html::tag('span', Icon::QUESTION, [
                            'class' => 'tooltip2',
                            'data-tooltip-content' => '#tooltip_free-contractors-block',
                        ]) ?>
                    <?php endif; ?>
                    <div class="actions">
                        <?= $this->render('_element/contractors_count', [
                            'flowType' => $type,
                            'count' => $category['contractorsCount']
                        ]) ?>
                        <div class="add-item-block">
                            <?php if ($category['id'] === 'visible-items'): ?>
                                <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить СТАТЬЮ '
                                    /*. ($type === ArticlesSearch::TYPE_INCOME ? 'прихода' : 'расхода')*/ . '</span>', '#', [
                                    'class' => 'button-regular button-hover-content-red hover-visible article-modal-link',
                                    'data-url' => Url::to(['create', 'activeTab' => $type, 'type' => $type]),
                                    'style' => 'margin-left: 15px;',
                                ]) ?>
                            <?php endif; ?>
                        </div>
                        <div class="switch-item-block"></div>
                        <div class="update-item-block"></div>
                        <div class="delete-item-block"></div>
                    </div>
                </li>
                <?php if (isset($category['items'])): ?>
                    <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>">
                        <?php foreach ($category['items'] as $item): ?>
                            <?php if (isset($item['items'])): ?>
                                <li class="category bold <?= empty($searchModel->search) ? null : 'open' ?>"
                                    id="<?= "{$category['id']}-group-{$item['id']}" ?>"
                                    style="padding-left: 50px;">
                                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                    ]) ?>
                                    <?= $item['name'] ?>
                                </li>
                                <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>">
                                    <?php foreach ($item['items'] as $subItem): ?>
                                        <?php $model = $type === ArticlesSearch::TYPE_INCOME
                                            ? InvoiceIncomeItem::findOne($subItem['id'])
                                            : InvoiceExpenditureItem::findOne($subItem['id']);
                                        $showSwitch = $subItem['canBeControlled'] && $subItem['flowOfFundsItemId']; ?>
                                        <li class="category item-<?= $subItem['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                            id="<?= "{$category['id']}-group-{$item['id']}-item-{$subItem['id']}" ?>"
                                            style="padding-left: 80px;">
                                            <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                                'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                            ]) ?>
                                            <div class="itemName"><?= $subItem['name'] ?></div>
                                            <div class="actions">
                                                <div class="add-contractor-block">
                                                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                        . ($type === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', 'javascript:;', [
                                                        'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                        'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $type, 'onlyContractor' => true, 'activeTab' => $type]),
                                                        'style' => 'margin-left: 15px;',
                                                    ]) ?>
                                                </div>
                                                <?= $this->render('_element/contractors_count', [
                                                    'flowType' => $type,
                                                    'count' => $subItem['contractorCount']
                                                ]) ?>
                                                <div class="add-sub-article-block">
                                                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить подстатью' . '</span>', 'javascript:;', [
                                                        'class' => 'button-regular button-hover-content-red article-modal-link hover-visible',
                                                        'data-url' => Url::to(['create', 'activeTab' => $type, 'type' => $subItem['flowType'], 'parent' => $subItem['id']]),
                                                        'style' => 'margin-left: 15px;',
                                                    ]) ?>
                                                </div>
                                                <div class="switch-item-block">
                                                    <?php if ($showSwitch): ?>
                                                        <div class="switch hover-visible"
                                                             title="<?= $subItem['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить' ?> статью для использования в работе"
                                                             style="margin-left: 15px;">
                                                            <?= Html::checkbox('is_visible', $subItem['isVisibleFlowOfFundsItem'], [
                                                                'id' => "is-visible-{$subItem['flowOfFundsItemId']}",
                                                                'class' => 'is-visible-switch md-check switch-input input-hidden hidden '
                                                                    . ($subItem['isVisibleFlowOfFundsItem'] && !$model->getCanToggleVisibility() ? 'cant-disable' : null),
                                                                'data-url' => Url::to(['toggle-is-visible', 'id' => $subItem['id'], 'type' => $type, 'filterType' => $type]),
                                                                'data-message' => ($subItem['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить') . " статью \"{$subItem['name']}\" для использования в работе?",
                                                            ]) ?>
                                                            <label class="d-flex align-items-center switch-label pl-0"
                                                                   for="<?= "is-visible-{$subItem['flowOfFundsItemId']}" ?>"
                                                                   style="cursor: pointer;">
                                                                <span class="switch-pseudo position-relative">&nbsp;</span>
                                                            </label>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="update-item-block">
                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                        'class' => 'article-modal-link hover-visible',
                                                        'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $type, 'activeTab' => $type]),
                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                        'style' => 'margin-left: 15px;',
                                                    ]) ?>
                                                </div>
                                                <div class="delete-item-block">
                                                    <?php if ($subItem['companyId'] !== null): ?>
                                                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                            'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                            'data' => [
                                                                'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                                'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                                'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\"?",
                                                                'url' => Url::to(['delete', 'id' => $subItem['id'], 'type' => $type, 'filterType' => $type]),
                                                            ],
                                                            'title' => Yii::t('yii', 'Удалить'),
                                                            'aria-label' => Yii::t('yii', 'Удалить'),
                                                            'style' => 'margin-left: 15px;',
                                                        ]) ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </li>
                                        <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                                            data-type="<?= $type ?>" data-id="<?= $subItem['id'] ?>">

                                            <!-- CHILDREN -->
                                            <?php if (!empty($subItem['children'])): ?>
                                                <?php foreach ($subItem['children'] as $subItemChild): ?>
                                                        <?php $model = $type === ArticlesSearch::TYPE_INCOME
                                                            ? InvoiceIncomeItem::findOne($subItemChild['id'])
                                                            : InvoiceExpenditureItem::findOne($subItemChild['id']);
                                                        $showSwitch = $subItemChild['canBeControlled'] && $subItemChild['flowOfFundsItemId']; ?>
                                                        <li class="category item-<?= $subItemChild['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                                            id="<?= "{$category['id']}-group-{$item['id']}-item-{$subItemChild['id']}" ?>"
                                                            style="padding-left: 110px;">
                                                            <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                                                'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                                            ]) ?>
                                                            <div class="itemName"><?= $subItemChild['name'] ?></div>
                                                            <div class="actions">
                                                                <div class="add-contractor-block">
                                                                    <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                                        . ($type === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', 'javascript:;', [
                                                                        'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                                        'data-url' => Url::to(['update', 'id' => $subItemChild['id'], 'type' => $type, 'onlyContractor' => true, 'activeTab' => $type]),
                                                                        'style' => 'margin-left: 15px;',
                                                                    ]) ?>
                                                                </div>
                                                                <?= $this->render('_element/contractors_count', [
                                                                    'flowType' => $type,
                                                                    'count' => $subItemChild['contractorCount']
                                                                ]) ?>
                                                                <div class="empty-block" style="width: 249px;"></div>
                                                                <div class="update-item-block">
                                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                                        'class' => 'article-modal-link hover-visible',
                                                                        'data-url' => Url::to(['update', 'id' => $subItemChild['id'], 'type' => $type, 'activeTab' => $type]),
                                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                        'style' => 'margin-left: 15px;',
                                                                    ]) ?>
                                                                </div>
                                                                <div class="delete-item-block">
                                                                    <?php if ($subItemChild['companyId'] !== null): ?>
                                                                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                            'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                                            'data' => [
                                                                                'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                                                'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                                                'message' => "Вы уверены, что хотите удалить статью \"{$subItemChild['name']}\"?",
                                                                                'url' => Url::to(['delete', 'id' => $subItemChild['id'], 'type' => $type, 'filterType' => $type]),
                                                                            ],
                                                                            'title' => Yii::t('yii', 'Удалить'),
                                                                            'aria-label' => Yii::t('yii', 'Удалить'),
                                                                            'style' => 'margin-left: 15px;',
                                                                        ]) ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                                                            data-type="<?= $type ?>" data-id="<?= $subItemChild['id'] ?>">
                                                            <?php $lim = $MAX_CONTRACTORS ?>
                                                            <?php foreach ($subItemChild['contractors'] as $contractor): ?>
                                                                <?php if (--$lim < 0) {
                                                                    echo Html::tag('span', $contractor['name'], [
                                                                        'class' => 'tpl-contractor',
                                                                        'data-id' => $contractor['id'],
                                                                        'data-type' => $type,
                                                                        'data-tab' => $type,
                                                                        'style' => 'display:none'
                                                                    ]);
                                                                    continue;
                                                                } ?>
                                                                <li class="item" data-type="<?= $type ?>"
                                                                    data-id="<?= $contractor['id'] ?>" style="padding-left: 145px;">
                                                                    <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                                        'class' => 'joint-operation-checkbox' . ($type == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                                        'data-type' => $type,
                                                                    ]) ?>
                                                                    <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                                        <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                                                    </svg>
                                                                    <span class="name"><?= $contractor['name'] ?></span>
                                                                    <div class="actions">
                                                                        <div class="update-item-block">
                                                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                                                'class' => 'article-modal-link add-contractor-button hover-visible',
                                                                                'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $type, 'activeTab' => $type]),
                                                                                'title' => Yii::t('yii', 'Редактировать'),
                                                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                                'style' => 'margin-left: 15px; text-decoration: none;',
                                                                            ]) ?>
                                                                        </div>
                                                                        <div class="delete-item-block">
                                                                            <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                                'class' => 'hover-visible can-delete-article',
                                                                                'data' => [
                                                                                    'message' => "Вы уверены, что хотите удалить статью \"{$subItemChild['name']}\" у этого контрагента?",
                                                                                    'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $type, 'filterType' => $type]),
                                                                                ],
                                                                                'title' => Yii::t('yii', 'Удалить'),
                                                                                'aria-label' => Yii::t('yii', 'Удалить'),
                                                                                'style' => 'margin-left: 15px;',
                                                                            ]) ?>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php endforeach; ?>
                                                            <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 145px']); } ?>
                                                        </ul>
                                                    <?php endforeach; ?>
                                            <?php endif; ?>
                                            <!-- end of CHILDREN -->
                                            <?php $lim = $MAX_CONTRACTORS ?>
                                            <?php foreach ($subItem['contractors'] as $contractor): ?>
                                                <?php if (--$lim < 0) {
                                                    echo Html::tag('span', $contractor['name'], [
                                                        'class' => 'tpl-contractor',
                                                        'data-id' => $contractor['id'],
                                                        'data-type' => $type,
                                                        'data-tab' => $type,
                                                        'style' => 'display:none'
                                                    ]);
                                                    continue;
                                                } ?>
                                                <li class="item" data-type="<?= $type ?>"
                                                    data-id="<?= $contractor['id'] ?>" style="padding-left: 80px;">
                                                    <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                        'class' => 'joint-operation-checkbox' . ($type == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                        'data-type' => $type,
                                                    ]) ?>
                                                    <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                        <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                                    </svg>
                                                    <span class="name"><?= $contractor['name'] ?></span>
                                                    <div class="actions">
                                                        <div class="update-item-block">
                                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                                'class' => 'article-modal-link add-contractor-button hover-visible',
                                                                'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $type, 'activeTab' => $type]),
                                                                'title' => Yii::t('yii', 'Редактировать'),
                                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                'style' => 'margin-left: 15px; text-decoration: none;',
                                                            ]) ?>
                                                        </div>
                                                        <div class="delete-item-block">
                                                            <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                'class' => 'hover-visible can-delete-article',
                                                                'data' => [
                                                                    'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\" у этого контрагента?",
                                                                    'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $type, 'filterType' => $type]),
                                                                ],
                                                                'title' => Yii::t('yii', 'Удалить'),
                                                                'aria-label' => Yii::t('yii', 'Удалить'),
                                                                'style' => 'margin-left: 15px;',
                                                            ]) ?>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                            <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 80px']); } ?>
                                        </ul>
                                    <?php endforeach; ?>
                                </ul>
                            <?php else: ?>
                                <?php $model = $type === ArticlesSearch::TYPE_INCOME
                                    ? InvoiceIncomeItem::findOne($item['id'])
                                    : InvoiceExpenditureItem::findOne($item['id']);
                                $showSwitch = $item['canBeControlled'] && $item['flowOfFundsItemId']; ?>
                                <li class="category item-<?= $item['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                    id="<?= "{$category['id']}-item-{$item['id']}" ?>"
                                    style="padding-left: 50px;">
                                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                    ]) ?>
                                    <div class="itemName"><?= $item['name'] ?></div>
                                    <div class="actions">
                                        <div class="add-contractor-block">
                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                . ($type === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', '#', [
                                                'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                'data-url' => Url::to(['update', 'id' => $item['id'], 'type' => $type, 'onlyContractor' => true, 'activeTab' => $type]),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <?= $this->render('_element/contractors_count', [
                                            'flowType' => $type,
                                            'count' => $item['contractorCount']
                                        ]) ?>
                                        <div class="add-sub-article-block">
                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить подстатью' . '</span>', 'javascript:;', [
                                                'class' => 'button-regular button-hover-content-red article-modal-link hover-visible',
                                                'data-url' => Url::to(['create', 'activeTab' => $type, 'type' => $item['flowType'], 'parent' => $item['id']]),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <div class="switch-item-block">
                                            <?php if ($showSwitch): ?>
                                                <div class="switch hover-visible"
                                                     title="<?= $item['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить' ?> статью для использования в работе"
                                                     style="margin-left: 15px;">
                                                    <?= Html::checkbox('is_visible', $item['isVisibleFlowOfFundsItem'], [
                                                        'id' => "is-visible-{$item['flowOfFundsItemId']}",
                                                        'class' => 'is-visible-switch md-check switch-input input-hidden hidden '
                                                            . ($item['isVisibleFlowOfFundsItem'] && !$model->getCanToggleVisibility() ? 'cant-disable' : null),
                                                        'data-url' => Url::to(['toggle-is-visible', 'id' => $item['id'], 'type' => $type, 'filterType' => $type]),
                                                        'data-message' => ($item['isVisibleFlowOfFundsItem'] ? 'Отключить' : 'Включить') . " статью \"{$item['name']}\" для использования в работе?",
                                                    ]) ?>
                                                    <label class="d-flex align-items-center switch-label pl-0"
                                                           for="<?= "is-visible-{$item['flowOfFundsItemId']}" ?>"
                                                           style="cursor: pointer;">
                                                        <span class="switch-pseudo position-relative">&nbsp;</span>
                                                    </label>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="update-item-block">
                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                'class' => 'article-modal-link hover-visible',
                                                'data-url' => Url::to(['update', 'id' => $item['id'], 'type' => $type, 'activeTab' => $type]),
                                                'title' => Yii::t('yii', 'Редактировать'),
                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                'style' => 'margin-left: 15px;',
                                            ]) ?>
                                        </div>
                                        <div class="delete-item-block">
                                            <?php if ($item['companyId'] !== null): ?>
                                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                    'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                    'data' => [
                                                        'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                        'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                        'message' => "Вы уверены, что хотите удалить статью \"{$item['name']}\"?",
                                                        'url' => Url::to(['delete', 'id' => $item['id'], 'type' => $type, 'filterType' => $type]),
                                                    ],
                                                    'title' => Yii::t('yii', 'Удалить'),
                                                    'aria-label' => Yii::t('yii', 'Удалить'),
                                                    'style' => 'margin-left: 15px;',
                                                ]) ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </li>

                                <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>"
                                    data-type="<?= $type ?>" data-id="<?= $item['id'] ?>">

                                    <!-- CHILDREN -->
                                    <?php if (!empty($item['children'])): ?>
                                        <?php foreach ($item['children'] as $subItem): ?>
                                                <?php $model = $type === ArticlesSearch::TYPE_INCOME
                                                    ? InvoiceIncomeItem::findOne($subItem['id'])
                                                    : InvoiceExpenditureItem::findOne($subItem['id']);
                                                $showSwitch = $subItem['canBeControlled'] && $subItem['flowOfFundsItemId']; ?>
                                                <li class="category item-<?= $subItem['id'] ?> <?= empty($searchModel->search) ? null : 'open' ?>"
                                                    id="<?= "{$category['id']}-group-{$item['id']}-item-{$subItem['id']}" ?>"
                                                    style="padding-left: 80px;">
                                                    <?= Html::button('<span class="table-collapse-icon">&nbsp;</span>', [
                                                        'class' => 'table-collapse-btn button-clr ml-1 ' . (!empty($searchModel->search) ? 'active' : null),
                                                    ]) ?>
                                                    <div class="itemName"><?= $subItem['name'] ?></div>
                                                    <div class="actions">
                                                        <div class="add-contractor-block">
                                                            <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Добавить '
                                                                . ($type === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика') . '</span>', 'javascript:;', [
                                                                'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                                                'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $type, 'onlyContractor' => true, 'activeTab' => $type]),
                                                                'style' => 'margin-left: 15px;',
                                                            ]) ?>
                                                        </div>
                                                        <?= $this->render('_element/contractors_count', [
                                                            'flowType' => $type,
                                                            'count' => $subItem['contractorCount']
                                                        ]) ?>
                                                        <div class="empty-block" style="width: 249px;"></div>
                                                        <div class="update-item-block">
                                                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                                'class' => 'article-modal-link hover-visible',
                                                                'data-url' => Url::to(['update', 'id' => $subItem['id'], 'type' => $type, 'activeTab' => $type]),
                                                                'title' => Yii::t('yii', 'Редактировать'),
                                                                'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                'style' => 'margin-left: 15px;',
                                                            ]) ?>
                                                        </div>
                                                        <div class="delete-item-block">
                                                            <?php if ($subItem['companyId'] !== null): ?>
                                                                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                    'class' => 'hover-visible ' . ($model->getCanDelete() ? 'can-delete-article' : null),
                                                                    'data' => [
                                                                        'toggle' => !$model->getCanDelete() ? 'modal' : null,
                                                                        'target' => !$model->getCanDelete() ? '#article-is-used' : null,
                                                                        'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\"?",
                                                                        'url' => Url::to(['delete', 'id' => $subItem['id'], 'type' => $type, 'filterType' => $type]),
                                                                    ],
                                                                    'title' => Yii::t('yii', 'Удалить'),
                                                                    'aria-label' => Yii::t('yii', 'Удалить'),
                                                                    'style' => 'margin-left: 15px;',
                                                                ]) ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </li>
                                                <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>" data-type="<?= $type ?>" data-id="<?= $subItem['id'] ?>">
                                                    <?php $lim = $MAX_CONTRACTORS ?>
                                                    <?php foreach ($subItem['contractors'] as $contractor): ?>
                                                        <?php if (--$lim < 0) {
                                                            echo Html::tag('span', $contractor['name'], [
                                                                'class' => 'tpl-contractor',
                                                                'data-id' => $contractor['id'],
                                                                'data-type' => $type,
                                                                'data-tab' => $type,
                                                                'style' => 'display:none'
                                                            ]);
                                                            continue;
                                                        } ?>
                                                        <li class="item" data-type="<?= $type ?>"
                                                            data-id="<?= $contractor['id'] ?>" style="padding-left: 115px;">
                                                            <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                                'class' => 'joint-operation-checkbox' . ($type == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                                'data-type' => $type,
                                                            ]) ?>
                                                            <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                                            </svg>
                                                            <span class="name"><?= $contractor['name'] ?></span>
                                                            <div class="actions">
                                                                <div class="update-item-block">
                                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                                        'class' => 'article-modal-link add-contractor-button hover-visible',
                                                                        'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $type, 'activeTab' => $type]),
                                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                                        'style' => 'margin-left: 15px; text-decoration: none;',
                                                                    ]) ?>
                                                                </div>
                                                                <div class="delete-item-block">
                                                                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                                        'class' => 'hover-visible can-delete-article',
                                                                        'data' => [
                                                                            'message' => "Вы уверены, что хотите удалить статью \"{$subItem['name']}\" у этого контрагента?",
                                                                            'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $type, 'filterType' => $type]),
                                                                        ],
                                                                        'title' => Yii::t('yii', 'Удалить'),
                                                                        'aria-label' => Yii::t('yii', 'Удалить'),
                                                                        'style' => 'margin-left: 15px;',
                                                                    ]) ?>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php endforeach; ?>
                                                    <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 115px']); } ?>
                                                </ul>
                                            <?php endforeach; ?>
                                    <?php endif; ?>
                                    <!-- end of CHILDREN -->
                                    <?php $lim = $MAX_CONTRACTORS ?>
                                    <?php foreach ($item['contractors'] as $contractor): ?>
                                        <?php if (--$lim < 0) {
                                            echo Html::tag('span', $contractor['name'], [
                                                'class' => 'tpl-contractor',
                                                'data-id' => $contractor['id'],
                                                'data-type' => $type,
                                                'data-tab' => $type,
                                                'style' => 'display:none'
                                            ]);
                                            continue;
                                        } ?>
                                        <li class="item" data-type="<?= $type ?>" data-id="<?= $contractor['id'] ?>"
                                            style="padding-left: 85px;">
                                            <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                                'class' => 'joint-operation-checkbox' . ($type == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                                'data-type' => $type,
                                            ]) ?>
                                            <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                                <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                            </svg>
                                            <span class="name"><?= $contractor['name'] ?></span>
                                            <div class="actions">
                                                <div class="update-item-block">
                                                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', 'javascript:;', [
                                                        'class' => 'article-modal-link add-contractor-button hover-visible',
                                                        'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $type, 'activeTab' => $type]),
                                                        'title' => Yii::t('yii', 'Редактировать'),
                                                        'aria-label' => Yii::t('yii', 'Редактировать'),
                                                        'style' => 'margin-left: 15px; text-decoration: none;',
                                                    ]) ?>
                                                </div>
                                                <div class="delete-item-block">
                                                    <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, [
                                                        'class' => 'hover-visible can-delete-article',
                                                        'data' => [
                                                            'message' => "Вы уверены, что хотите удалить статью \"{$item['name']}\" у этого контрагента?",
                                                            'url' => Url::to(['delete-contractor', 'id' => $contractor['id'], 'type' => $type, 'filterType' => $type]),
                                                        ],
                                                        'title' => Yii::t('yii', 'Удалить'),
                                                        'aria-label' => Yii::t('yii', 'Удалить'),
                                                        'style' => 'margin-left: 15px;',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                    <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 85px']); } ?>
                                </ul>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                <?php elseif (isset($category['contractors'])): ?>
                    <ul class="subCategory <?= empty($searchModel->search) ? 'hidden' : null ?>" data-id="empty">
                        <?php $lim = $MAX_CONTRACTORS ?>
                        <?php foreach ($category['contractors'] as $contractor): ?>
                            <?php if (--$lim < 0) {
                                echo Html::tag('span', $contractor['name'], [
                                    'class' => 'tpl-contractor',
                                    'data-id' => $contractor['id'],
                                    'data-type' => $type,
                                    'data-tab' => $type,
                                    'style' => 'display:none'
                                ]);
                                continue;
                            } ?>
                            <li class="item" data-type="<?= $category['flowType'] ?>"
                                data-id="<?= $contractor['id'] ?>" style="padding-left: 35px;">
                                <?= Html::checkbox('contractors[' . $contractor['id'] . '][checked]', false, [
                                    'class' => 'joint-operation-checkbox' . ($type == ArticlesSearch::TYPE_INCOME ? ' income' : ' expense'),
                                    'data-type' => $type,
                                ]) ?>
                                <svg class="sortable-row-icon hidden svg-icon text_size-14 text-grey ml-1 mr-2 flex-shrink-0">
                                    <use xlink:href="/img/svg/svgSprite.svg#menu-small"></use>
                                </svg>
                                <span class="name"><?= $contractor['name'] ?></span>
                                <div class="actions">
                                    <div class="add-contractor-block">
                                        <?= Html::a('<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg><span>Выбрать Статью</span>', 'javascript:;', [
                                            'class' => 'button-regular button-hover-content-red article-modal-link add-contractor-button hover-visible',
                                            'data-url' => Url::to(['add-item-to-contractor', 'id' => $contractor['id'], 'type' => $type, 'activeTab' => $type]),
                                            'style' => 'margin-left: 15px;',
                                        ]) ?>
                                    </div>
                                    <div class="switch-item-block">
                                    </div>
                                    <div class="update-item-block">
                                    </div>
                                    <div class="delete-item-block">
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                        <?php if ($lim < 0) { echo Html::a('Еще' . $preloader, 'javascript:void(0)', ['class' => 'more-contractors', 'style' => 'padding-left: 35px']); } ?>
                    </ul>
                <?php endif; ?>
            <?php endforeach; ?>

        <?php endif; ?>

    </ul>
</div>
<div id="change-visible-item" class="confirm-modal modal fade show" role="dialog" tabindex="-1" aria-modal="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4"></h4>
                <div class="text-center">
                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 change-link ladda-button ladda-custom',
                        'data-style' => 'expand-right',
                    ]) ?>
                    <?= Html::button('Нет', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="error-change-visible-item" class="confirm-modal modal fade show" role="dialog" tabindex="-1"
     aria-modal="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4"></h4>
                <div class="text-center">
                    <?= Html::button('Ок', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="delete-article" class="confirm-modal modal fade show" role="dialog" tabindex="-1" aria-modal="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title text-center mb-4"></h4>
                <div class="text-center">
                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 delete-link ladda-button ladda-custom',
                        'data-style' => 'expand-right',
                    ]) ?>
                    <?= Html::button('Нет', [
                        'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                        'data-dismiss' => 'modal',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>