<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.03.2020
 * Time: 18:38
 */

use common\models\Contractor;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use frontend\modules\reference\controllers\ArticlesController;
use frontend\modules\reference\models\ArticlesSearch;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use frontend\themes\kub\modules\reference\widgets\ArticlesSummarySelectWidget;

/* @var $this yii\web\View
 * @var $activeTab int
 * @var $searchModel ArticlesSearch
 * @var $data []
 * @var $user \common\models\evotor\Employee
 * @var $setDefaultItemForm \frontend\modules\reference\models\SetDefaultContractorItem
 * @var $manyChangeItemsForm \frontend\modules\reference\models\ManyChangeItemsForm
 */

$type = null;
switch ($activeTab) {
    case ArticlesController::TAB_INCOME:
        $type = ArticlesSearch::TYPE_INCOME;
        break;
    case ArticlesController::TAB_EXPENSE:
        $type = ArticlesSearch::TYPE_EXPENSE;
        break;
    case ArticlesController::TAB_BY_ACTIVITY:
        $type = ArticlesSearch::TYPE_BY_ACTIVITY;
        break;
}

$addIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$this->title = 'Статьи операций';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
    <div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <h4 class="mb-2"><?= $this->title ?></h4>
                    Привяжите статьи Прихода и Расхода к контрагентам. Статьи можно менять в любой момент.<br/>
                    В КУБ24 есть предустановленные статьи, используйте их или добавьте свои. Не делайте отдельные статьи под разовые и мелкие операции, лучше их сгруппировать. Используйте «Настройки правил», что бы автоматически присваивать статью для новых Покупателей и Поставщиков.
                </div>
                <div class="col-3 column pl-0">
                    <div class="pb-2 mb-1">
                        <?= Html::a($addIcon . '<span>Добавить</span>', 'javascript:;', [
                            'class' => 'button-regular button-regular_red button-width ml-auto w-100 article-modal-link',
                            'data-url' => Url::to(['create', 'type' => $activeTab == ArticlesController::TAB_BY_ACTIVITY ? null : $type]),
                        ]) ?>
                    </div>
                    <div class="pb-2 mb-1">
                        <?= Html::a('Настройка правил', 'javascript:;', [
                            'class' => 'button-regular button-hover-content-red button-clr w-100',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#articles-rules',
                            ],
                        ]) ?>
                        <?= $this->render('_partial/_rules_settings_modal', [
                            'model' => $setDefaultItemForm,
                            'type' => $activeTab,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="nav-tabs-row articles-nav-tabs-row">
        <?= Nav::widget([
            'id' => 'articles-menu',
            'options' => [
                'class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3',
            ],
            'items' => [
                [
                    'label' => 'Статьи по видам деятельности',
                    'url' => Url::to(['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY]),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($activeTab === ArticlesController::TAB_BY_ACTIVITY ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Приходы',
                    'url' => Url::to(['/reference/articles/index', 'type' => ArticlesSearch::TYPE_INCOME]),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($activeTab === ArticlesController::TAB_INCOME ? ' active' : '')
                    ],
                ],
                [
                    'label' => 'Расходы',
                    'url' => Url::to(['/reference/articles/index', 'type' => ArticlesSearch::TYPE_EXPENSE]),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                    'linkOptions' => [
                        'class' => 'nav-link' . ($activeTab === ArticlesController::TAB_EXPENSE ? ' active' : '')
                    ],
                ],
            ],
        ]) ?>
    </div>
<?php if ($activeTab == ArticlesController::TAB_INCOME): ?>
    <?= $this->render('articles-list', [
        'searchModel' => $searchModel,
        'data' => $data,
        'type' => ArticlesSearch::TYPE_INCOME,
    ]) ?>
<?php elseif ($activeTab === ArticlesController::TAB_EXPENSE): ?>
    <?= $this->render('articles-list', [
        'searchModel' => $searchModel,
        'data' => $data,
        'type' => ArticlesSearch::TYPE_EXPENSE,
    ]) ?>
<?php elseif ($activeTab === ArticlesController::TAB_BY_ACTIVITY): ?>
    <?= $this->render('articles-list', [
        'searchModel' => $searchModel,
        'data' => $data,
        'type' => ArticlesSearch::TYPE_BY_ACTIVITY,
    ]) ?>
<?php endif; ?>
<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
    <div class="modal fade" id="article-modal-container" role="modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title">Добавить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body">
                    <?php Pjax::begin([
                        'id' => 'article-form-container',
                        'enablePushState' => false,
                        'linkSelector' => false,
                    ]) ?>
                    <?php Pjax::end() ?>

                    <div class="mt-3 d-flex">
                        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'form' => 'article-form',
                            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                            'data-style' => 'expand-right'
                        ]); ?>
                        <?= Html::submitButton('<div class="change-old-operations-button-block"></div><span class="ladda-label">Применить для имеющихся операций</span><span class="ladda-spinner"></span>', [
                            'form' => 'article-form',
                            'class' => 'button-clr button-regular button-hover-transparent ladda-button add-item-to-contractor-button change-old-operations-button',
                            'data-style' => 'expand-right',
                            'style' => 'margin-left: 20px;',
                            'data' => [
                                'method' => 'post',
                                'params' => [
                                    'UpdateArticleForm[changeOldOperations]' => 1,
                                ],
                            ],
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                                style="margin-left: auto;"
                                data-dismiss="modal">Отменить
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add-new" role="modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body" id="block-modal-new-product-form">
                </div>
            </div>
        </div>
    </div>
<?= ArticlesSummarySelectWidget::widget([
    'buttons' => [
        Html::a('<i class="glyphicon glyphicon-pencil" style="margin-right: 5px;"></i> Редактировать', '#many-change-items', [
            'class' => 'button-clr button-regular button-hover-transparent',
            'data-toggle' => 'modal',
            'style' => 'width: 135px;',
        ]),
        Html::a('<i class="glyphicon glyphicon-trash" style="margin-right: 5px;"></i> Удалить', '#many-delete-contractors', [
            'class' => 'button-clr button-regular button-hover-transparent',
            'data-toggle' => 'modal',
            'style' => 'width: 135px;',
        ]),
    ],
]); ?>
    <div id="many-delete-contractors" class="confirm-modal modal fade show" role="dialog" tabindex="-1"
         aria-modal="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить статьи у выбранных
                        контрагентов?</h4>
                    <div class="text-center">
                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 many-delete-contractors-link ladda-button ladda-custom',
                            'data-url' => Url::to(['many-delete-contractors', 'type' => $type]),
                            'data-style' => 'expand-right',
                        ]) ?>
                        <?= Html::button('Нет', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                            'data-dismiss' => 'modal',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->render('_partial/_modal_many_change_items', [
    'manyChangeItemsForm' => $manyChangeItemsForm,
    'type' => $type,
    'activeTab' => $activeTab,
]) ?>
    <div class="tooltip-template" style="display: none;">
    <span id="tooltip_financial-operations-block" style="display: inline-block; text-align: center;">
        Привлечение денег (кредиты, займы)<br>
        в компанию и их возврат. <br>
        Выплата дивидендов.<br>
        Предоставление займов и депозитов.
    </span>
        <span id="tooltip_operation-activities-block" style="display: inline-block; text-align: center;">
        Покупка и продажа оборудования и других основных средств. <br>
        Затраты на новые проекты и поступление выручки от них. <br>
    </span>
        <span id="tooltip_investment-activities-block" style="display: inline-block; text-align: center;">
        Движение денег, связанное с основной деятельностью компании <br>
        (оплата от покупателей, зарплата, аренда, покупка товаров и т.д.)
    </span>
        <span id="tooltip_free-contractors-block" style="display: inline-block; text-align: center;">
        Список контрагентов, которым еще не назначена <br>
        статья Прихода или Расхода. Перетащите контрагента <br>
        в нужную статью или в статье через кнопку "Добавить" <br>
        выберите его.
    </span>
        <span id="tooltip_enabled-income-items-block" style="display: inline-block;">
        Данные статьи Прихода вы используете в работе, <br>
        они отображаются в: <br>
        1) В выпадающих списках и разделе Деньги <br>
        2) Во всех выпадающих списках в других разделах <br>
        3) В финансовых отчетах <br>
    </span>
        <span id="tooltip_enabled-expenditure-items-block" style="display: inline-block;">
        Данные статьи Расхода вы используете в работе, <br>
        они отображаются в: <br>
        1) В выпадающих списках и разделе Деньги <br>
        2) Во всех выпадающих списках в других разделах <br>
        3) В финансовых отчетах <br>
    </span>
        <span id="tooltip_disabled-income-items-block" style="display: inline-block;">
        Данные статьи Прихода не используются вами. <br>
        Их можно включить в любое время. <br>
        Используйте только те статьи, которые вам действительно нужны, <br>
        чтобы в списках не было лишнего
    </span>
        <span id="tooltip_disabled-expenditure-items-block" style="display: inline-block;">
        Данные статьи Расхода не используются вами. <br>
        Их можно включить в любое время. <br>
        Используйте только те статьи, которые вам действительно нужны, <br>
        чтобы в списках не было лишнего.
    </span>
    </div>
<?php
$this->registerJs('$.pjax.defaults.scrollTo = false;', View::POS_LOAD);
$this->registerJs('

window.enabledSellers = [];
window.enabledCustomers = [];

$(document).on("click", ".article-modal-link", function(e) {
    e.preventDefault();
    
    if ($(this).hasClass("add-contractor-button")) {
        $("#article-modal-container").find(".change-old-operations-button").show();
    } else {
        $("#article-modal-container").find(".change-old-operations-button").hide();
    }
    
    $.pjax({url: $(this).data("url"), container: "#article-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#article-modal-container .modal-title").html($("[data-header]").data("header"));
        $("#article-modal-container").modal("show");
    });
});

$(document).on("click", ".field-articleform-seller ul.dropdown-menu, .field-articleform-customer ul.dropdown-menu", function(e) {
    e.stopPropagation();
});

$(document).on("keyup change", ".search-contractors input", function (e) {
    let searchValue = $(this).val().toLowerCase();
    let itemsBlock = $(this).closest("span").siblings(".select2-results").find("ul");
    if (searchValue !== "") {
        itemsBlock.find("li").each(function () {
            if ($(this).text().toLowerCase().search(searchValue) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    } else {
        itemsBlock.find("li").show();
    }
});

$(document).on("change", "#articleform-type", function(e) {
    if ($(this).find(":checked").val() == 0) {
        $(".field-articleform-customer").removeClass("hidden");
        $(".field-articleform-seller").addClass("hidden");
    } else {
        $(".field-articleform-seller").removeClass("hidden");
        $(".field-articleform-customer").addClass("hidden");
    }
});

$(document).on("select2:open", "#article-customers", function (e) {
    let dropDown = $(".select2-container--open .select2-dropdown");
    let dataValue = $(this).find("select").data("value");

    dropDown.find(".search-contractors").remove();
    dropDown.prepend("<span class=\'select2-search select2-search--dropdown search-contractors\'><input type=\'search\' class=\'select2-search__field\'></span>");
    $(".search-contractors input").val("");
    $("#article-customers option").each(function() {
        if (
            $(this).val() !== "add-modal-contractor"
            && $(this).val() !== "toggle-all"
            && +$(this).attr("data-new") !== 1
            && $.inArray(+$(this).val(), dataValue) === -1
            && $.inArray($(this).val(), enabledCustomers) === -1
        ) {
            $(this).attr("disabled", true);
        }
        
    });

    $("#article-customers").trigger("change");
});

$(document).on("select2:open", "#article-sellers", function (e) {
    let dropDown = $(".select2-container--open .select2-dropdown");
    let dataValue = $(this).find("select").data("value");

    dropDown.find(".search-contractors").remove();
    dropDown.prepend("<span class=\'select2-search select2-search--dropdown search-contractors\'><input type=\'search\' class=\'select2-search__field\'></span>");
    $(".search-contractors input").val("");
    $("#article-sellers option").each(function() {
        if (
            $(this).val() !== "add-modal-contractor"
            && $(this).val() !== "toggle-all"
            && +$(this).attr("data-new") !== 1
            && $.inArray(+$(this).val(), dataValue) === -1
            && $.inArray($(this).val(), enabledSellers) === -1
        ) {
            $(this).attr("disabled", true);
        }
    });

    $("#article-sellers").trigger("change");
});

$(document).on("click", ".clear-search", function(e) {
    $(this).siblings("#articlessearch-search").val(null);
    $(this).closest("form").submit();
});

$(document).on("change", "#addarticletocontractorform-itemid", function(e) {
    checkOperations($(this).data("check-operations-url"), $(this).val(), [$("#contractorId").val()]);
});

$(document).on("change", ".updatearticleform-contractors", function(e) {
    checkOperations($(this).data("check-operations-url"), $("#itemId").val(), $(this).val());
});

$(document).on("pjax:success", "#article-form-container", function() {
    let updateArticleContractors = $(".updatearticleform-contractors");
    let addItemToContractor = $("#contractorId");
    let contractors = null;
    let uri = null;

    if (updateArticleContractors.length > 0) {
        contractors = updateArticleContractors.val();
        uri = updateArticleContractors.data("check-operations-url")
    } else if (addItemToContractor.length > 0) {
        contractors = [addItemToContractor.val()];
        uri = $("#addarticletocontractorform-itemid").data("check-operations-url")
    }

    if (uri !== null && uri !== undefined && contractors !== null && $("#itemId").val()) {
        checkOperations(uri, $("#itemId").val(), contractors);
    }
});

function checkOperations(uri, itemId, contractors) {
    $.post(uri, {
        "itemId": itemId,
        "contractors": contractors
    }, function(data) {
        let changeOldOperationsButton = $(".change-old-operations-button:visible");
        let changeOldOperationsBlock = $(".change-old-operations-button-block:visible");
        if (data) {
            changeOldOperationsButton.removeAttr("disabled");
            if (changeOldOperationsBlock.hasClass("tooltipstered")) {
                changeOldOperationsBlock.tooltipster("destroy");
            }
        } else {
            changeOldOperationsButton.attr("disabled", true);
            changeOldOperationsBlock.tooltipster({
                theme: ["tooltipster-kub"],
                trigger: "hover",
                content: "Нет операций с другими статьями"
            });
        }
    });
}

$(document).on("click", ".many-delete-contractors-link", function(e) {
    $.post($(this).data("url"), $(".joint-operation-checkbox:checked").serialize(), function(){});
});

$(document).on("shown.bs.modal", "#many-change-items", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income:checked").length > 0;
    var $modal = $(this);
    var $header = $modal.find("h4.modal-title");
    var $additionalHeaderText = null;

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    if ($includeExpenditureItem && $includeIncomeItem) {
        $additionalHeaderText = " прихода / расхода";
    } else if ($includeExpenditureItem) {
        $additionalHeaderText = " расхода";
    } else if ($includeIncomeItem) {
        $additionalHeaderText = " прихода";
    }
    $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend("<input type=\"hidden\" class=\"change-items-contractors\" name=\"ManyChangeItemsForm[contractors][]\" value=\"" + $(this).attr("name").match(/\d+/) + "\">");
    });
});
$(document).on("change", "#manychangeitemsform-incomeitemidmanyitem, #manychangeitemsform-expenditureitemidmanyitem", function(e) {
    let contractors = [];
    $(".change-items-contractors").each(function() {
        contractors.push($(this).val());
    });
    checkOperations($(this).data("check-operations-url"), $(this).val(), contractors);
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
    $(".additional-header-text").remove();
    $(".modal#many-change-items form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    var $hasError = false;

    $(".field-manychangeitemsform-incomeitemidmanyitem:visible, .field-manychangeitemsform-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });

    if ($hasError) {
        return false;
    }
});

$(document).on("submit", "#js-cash_flow_update_item_form", function() {
    let modal = $(this).closest(".modal");
    $.post($(this).attr("action"), $(this).serialize(), function(response) {
        if (response.result) {
            let html = $($.parseHTML(response.html)).find("ul.articles-list");
            let openedCategories = [];

            $("ul.articles-list .category").each(function(i, e) {
                 if ($(this).hasClass("open")) {
                     openedCategories.push($(this).attr("id"));
                 }
            });
            $("ul.articles-list").replaceWith(html);

            $("ul.articles-list .category").each(function(i, e) {
                if ($.inArray($(this).attr("id"), openedCategories) !== -1) {
                    $(e).click();
                }
            });

            modal.modal("hide");
            sortableArticleList();
        }
    });

    return false;
});

$(document).on("click", "ul.articles-list .category", function(e) {
    if ($(e.target).hasClass("category") || $(e.target).hasClass("table-collapse-icon") || $(e.target).hasClass("actions")
       || $(e.target).hasClass("itemName")) {
        let subCategory = $(this).next(".subCategory");

        $(this).toggleClass("open");
        $(this).find(".table-collapse-btn").toggleClass("active");
        subCategory.toggleClass("hidden");
        subCategory.find(".category.open").click();
    }
});

$(document).on("change", ".is-visible-switch", function() {
    let isVisibleSwitch = $(this);
    let isChecked = isVisibleSwitch.prop("checked");
    let modal = $("#change-visible-item");
    let message = isVisibleSwitch.attr("data-message");
    let errorModal = $("#error-change-visible-item");

    if (isVisibleSwitch.hasClass("cant-disable")) {
        errorModal.find(".modal-title").text("Статью нельзя отключить, так как она используется минимум в одной операции.");
        errorModal.modal();
    } else {
        modal.attr("data-id", isVisibleSwitch.attr("id"));
        modal.find(".modal-title").text(message);
        modal.find(".change-link").attr("data-url", $(this).data("url"));
        modal.modal();
    }

    isVisibleSwitch.prop("checked", !isChecked);
});

$(document).on("click", "#change-visible-item .change-link", function(e) {
    let modal = $("#change-visible-item");
    let errorModal = $("#error-change-visible-item");
    let isVisibleSwitch = $("#" + modal.attr("data-id"));
    let isChecked = isVisibleSwitch.prop("checked");
    let laddaButton = Ladda.create(this);

    laddaButton.start();

    $("#blockScreen").show();
    $.post($(this).attr("data-url"), null, function(data) {
        if (data.result == true) {
            let html = $($.parseHTML(data.html)).find("ul.articles-list");
            let openedCategories = [];

            $("ul.articles-list .category").each(function(i, e) {
                 if ($(this).hasClass("open")) {
                     openedCategories.push($(this).attr("id"));
                 }
            });
            $("ul.articles-list").replaceWith(html);

            $("ul.articles-list .category").each(function(i, e) {
                if ($.inArray($(this).attr("id"), openedCategories) !== -1) {
                    $(e).click();
                }
            });

            laddaButton.stop();
            $("#blockScreen").hide();
            modal.modal("hide");
            sortableArticleList();
                        
            return;
        }

        $("#blockScreen").hide();

        modal.modal("hide");
        errorModal.find(".modal-title").text(data.message);
        errorModal.modal();
    });
});

$(document).on("click", ".can-delete-article", function(e) {
    let modal = $("#delete-article");

    modal.find(".modal-title").text($(this).data("message"));
    modal.find(".delete-link").attr("data-url", $(this).data("url"));
    modal.modal();
});

$(document).on("click", "#delete-article .delete-link", function(e) {
    let modal = $("#delete-article");
    let errorModal = $("#error-change-visible-item");
    let laddaButton = Ladda.create(this);

    laddaButton.start();
    $.post($(this).attr("data-url"), null, function(data) {
        if (data.result == true) {
            let html = $($.parseHTML(data.html)).find("ul.articles-list");
            let openedCategories = [];

            $("ul.articles-list .category").each(function(i, e) {
                 if ($(this).hasClass("open")) {
                     openedCategories.push($(this).attr("id"));
                 }
            });
            $("ul.articles-list").replaceWith(html);

            $("ul.articles-list .category").each(function(i, e) {
                if ($.inArray($(this).attr("id"), openedCategories) !== -1) {
                    $(e).click();
                }
            });

            laddaButton.stop();
            $("#blockScreen").hide();
            modal.modal("hide");
            sortableArticleList();
            
            window.toastr.success("Статья успешно удалена", "", {
                "closeButton": true,
                "showDuration": 1000,
                "hideDuration": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 1000,
                "escapeHtml": false
            }); 
            
            return;
        }

        $("#blockScreen").hide();

        modal.modal("hide");
        errorModal.find(".modal-title").text(data.message);
        errorModal.modal();
    });
});

$(document).on("change", "#addarticletocontractorform-activitytype", function(e) {
    getItems($(this).val());
});

let allStaticArticles = "";
$(document).on("pjax:success", "#article-form-container", function(event, data) {
    let form = $(this).find("form");
    if (form.data("update-list")) {
        updateList(form.data("update-list-url"), !form.data("change-old-operations"), $(this).closest(".modal"));
    }

    let addArticleToContractorType = $("#addarticletocontractorform-activitytype");
    if (addArticleToContractorType.length > 0) {
        allStaticArticles = $("#addarticletocontractorform-itemid").find("option, optgroup").clone();
        getItems($("#addarticletocontractorform-activitytype").val());
    }
});

function updateList(uri, closeModal, modal) {
    $.post(uri, {ajax:"article-form"}, function(response) {
        if (response.result) {
            let html = $($.parseHTML(response.html)).find("ul.articles-list");
            let openedCategories = [];

            $("ul.articles-list .category").each(function(i, e) {
                 if ($(this).hasClass("open")) {
                     openedCategories.push($(this).attr("id"));
                 }
            });
            $("ul.articles-list").replaceWith(html);

            $("ul.articles-list .category").each(function(i, e) {
                if ($.inArray($(this).attr("id"), openedCategories) !== -1) {
                    $(e).click();
                }
            });

            if (closeModal) {
                modal.modal("hide");
            }
            
            sortableArticleList();
        }
    });
}

function getItems(type) {
    let itemsInput = $("#addarticletocontractorform-itemid");
    let itemsByType = itemsInput.data("items");
    let items = itemsByType[type];
    let submitButton = $(".add-item-to-contractor-button");
    let allItems = $("#static-items");

    allItems.find("option, optgroup").remove();
    allItems.append(allStaticArticles);

    allItems.find("option").each(function(i) {
        let value = $(this).attr("value");
        if (value !== undefined) {
            if ($.inArray(value, items) == -1) {
                $(this).remove();
            }
        }
    });

    allItems.find("optgroup").each(function(i) {
        if ($(this).find("option").length == 0) {
             $(this).remove();
        }
    });

    itemsInput.removeAttr("disabled")
    submitButton.removeAttr("disabled")
    itemsInput.find("option, optgroup").remove();

    if (allItems.find("option, optgroup").length == 0) {
        itemsInput.attr("disabled", true);
        submitButton.attr("disabled", true);
    } else {
         itemsInput.append(allItems.find("option, optgroup").clone()).trigger("change");
    }
}
');

if (Yii::$app->request->get('show_add_modal')) {
    $this->registerJs('
        $(document).ready(function() {
            let url = $(".article-modal-link").data("url");
            $.pjax({url: url, container: "#article-form-container", push: false});
            $(document).on("pjax:success", function() {
                $("#article-modal-container .modal-title").html($("[data-header]").data("header"));
                $("#article-modal-container").modal("show");
            });
            window.history.pushState("object", document.title, location.href.replace("&show_add_modal=1", ""));
        });
    ');
}