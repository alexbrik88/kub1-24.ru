<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.07.2020
 * Time: 0:32
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\reference\models\ArticlesSearch;

/* @var $manyChangeItemsForm \frontend\modules\reference\models\ManyChangeItemsForm
 * @var $type int
 * @var $activeTab int
 */
?>
<div class="modal fade" id="many-change-items" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['many-change-items', 'type' => $type, 'activeTab' => $activeTab]),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12" style="margin:0 0 8px">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Приход',
                                            'labelOptions' => [
                                                'class' => 'radio-txt-bold font-14',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($manyChangeItemsForm, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => '',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-income_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::class, [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'data-check-operations-url' => Url::to(['is-has-operations-with-another-items', 'type' => ArticlesSearch::TYPE_INCOME]),
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Изменить статью прихода на:'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="row">
                            <label class="col-12" for="cashbankflowsform-flow_type">Для типа</label>

                            <div class="col-12" style="margin:0 0 8px">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Расход',
                                            'labelOptions' => [
                                                'class' => 'radio-txt-bold font-14',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($manyChangeItemsForm, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => '',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-expenditure_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::class, [
                            'options' => [
                                'prompt' => '',
                                'data-check-operations-url' => Url::to(['is-has-operations-with-another-items', 'type' => ArticlesSearch::TYPE_EXPENSE]),
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Изменить статью расхода на:'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex">
                        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'class' => 'button-regular button-width button-regular_red button-clr ladda-button',
                            'data-style' => 'expand-right',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <?= Html::submitButton('<div class="change-old-operations-button-block"></div><span class="ladda-label">Применить для имеющихся операций</span><span class="ladda-spinner"></span>', [
                            'class' => 'button-clr button-regular button-hover-transparent ladda-button add-item-to-contractor-button change-old-operations-button',
                            'data-style' => 'expand-right',
                            'style' => 'margin-left: 20px;',
                            'data' => [
                                'method' => 'post',
                                'params' => [
                                    'ManyChangeItemsForm[changeOldOperations]' => 1,
                                ],
                            ],
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent"
                                style="margin-left: auto;"
                                data-dismiss="modal">
                            Отменить
                        </button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>