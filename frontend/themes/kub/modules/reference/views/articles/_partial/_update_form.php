<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.12.2019
 * Time: 20:34
 */

use frontend\modules\reference\models\UpdateArticleForm;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use frontend\modules\reports\models\AbstractFinance;
use yii\bootstrap\Html;
use common\models\Contractor;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;
use frontend\themes\kub\helpers\Icon;
use yii\web\JsExpression;
use yii\helpers\Url;

/* @var $model UpdateArticleForm
 * @var $onlyContractor bool
 * @var $activeTab int
 * @var $updateList bool
 */

$parentModel = $model->model->parent ?? null;
$childrenModels = $model->model->getChildren()->all();
$attributeName = $model->getType() ? 'sellers' : 'customers';
$model->$attributeName = $model->getOldContractors();
if ($onlyContractor) {
    $header = 'Добавить ' . ($model->getType() ? 'Поставщика' : 'Покупателя') . ' для статьи';
} else {
    $header = 'Редактировать ' . ($parentModel ? 'подстатью' : 'статью') . ($model->getType() ? ' расхода' : ' прихода');
}

$enabledSellers = Contractor::find()
    ->select('id')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byContractor(Contractor::TYPE_SELLER)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->andWhere([
        'OR',
        ['invoice_expenditure_item_id' => null],
        ['invoice_expenditure_item_id' => ''],
    ])
    ->column();

$enabledCustomers = Contractor::find()
    ->select('id')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byContractor(Contractor::TYPE_CUSTOMER)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->andWhere([
        'OR',
        ['invoice_income_item_id' => null],
        ['invoice_income_item_id' => ''],
    ])
    ->column();

Pjax::begin([
    'id' => 'article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<script>
    enabledSellers = <?= json_encode($enabledSellers) ?>;
    enabledCustomers = <?= json_encode($enabledCustomers) ?>;
</script>

<style type="text/css">
    .article-form .control-label {
        font-weight: bold;
    }

    .article-form .form-control {
        width: 100%;
    }

    #updatearticleform-type > .radio {
        width: 140px
    }
</style>

<div class="article-form" data-header="<?= $header ?>">
    <?php $form = ActiveForm::begin([
        'id' => 'article-form',
        'enableClientValidation' => false,
        'action' => [
            'update',
            'id' => $model->getItem()->id,
            'type' => $model->getType(),
            'activeTab' => $activeTab,
            'onlyContractor' => $onlyContractor,
            'ajax' => 'article-form'
        ],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
            'data-update-list-url' => Url::to(['ajax-list', 'type' => $activeTab]),
            'data-change-old-operations' => (int)$model->changeOldOperations,
            'data-update-list' => (int)$updateList,
        ],
    ]); ?>
    <?= $form->field($model, 'type', [
        'options' => [
            'class' => 'form-group',
        ]])
        ->radioList([$model->getType() => $model->getType() ? 'Расход' : 'Приход'], [
            'value' => $model->getType(),
            'disabled' => true,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ],
                ]);
            },
        ]); ?>

    <?= $form->field($model, 'activityType')->widget(Select2::class, [
        'hideSearch' => true,
        'data' => [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
        ],
        'options' => [
            'placeholder' => '',
            'disabled' => $onlyContractor || $parentModel,
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]); ?>

    <?php if ($onlyContractor && $childrenModels): ?>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label class="label">Название статьи</label>
                    <div class="form-filter">
                        <input type="text" class="form-control" name="_parentName" value="<?= $model->model->name ?>" disabled>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label class="label">Название подстатьи</label>
                    <div class="form-filter">
                        <?= Select2::widget([
                            'hideSearch' => true,
                            'name' => 'articleChildrenId',
                            'data' => ArrayHelper::map($childrenModels, 'id', 'name'),
                            'options' => [
                                'placeholder' => '',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="col-6">

            </div>
        </div>
    <?php elseif ($parentModel): ?>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label class="label">Название статьи</label>
                    <div class="form-filter">
                        <input type="text" class="form-control" name="_parentName" value="<?= $parentModel->name ?>" disabled>
                        <input type="hidden" name="_parentId" value="<?= $parentModel->id ?>">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'name')->textInput([
                    'maxlength' => true,
                    'disabled' => $model->getItem()->company_id === null || $onlyContractor,
                ])->label('Название подстатьи') ?>
            </div>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'name')->textInput([
            'maxlength' => true,
            'disabled' => $model->getItem()->company_id === null || $onlyContractor,
        ]) ?>
    <?php endif; ?>

    <div class="form-group field-articleform-<?= $model->getType() ? 'seller' : 'customer' ?>">
        <label class="label">
            <?= $model->getType() ? 'Поставщики' : 'Покупатели' ?>
        </label>
        <div class="form-filter">
            <?= $form->field($model, $attributeName, [
                'template' => "{input}",
                'options' => [
                    'id' => "article-{$attributeName}",
                ],
            ])->widget(Select2::class, [
                'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить нового ' . ($model->getType() ? 'поставщика' : 'покупателя')]
                    + ['toggle-all' => 'Все']
                    + ArrayHelper::map(Contractor::getSorted()
                        ->byCompany($model->getUser()->currentEmployeeCompany->company_id)
                        ->byContractor($model->getType() ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER)
                        ->byIsDeleted(Contractor::NOT_DELETED)
                        ->byStatus(Contractor::ACTIVE)
                        ->all(), 'id', 'shortName'),
                'showToggleAll' => false,
                'value' => $model->getOldContractors(),
                'options' => [
                    'placeholder' => 'Выберите ' . ($model->getType() ? 'поставщиков' : 'покупателей') . ' для этой статьи',
                    'class' => 'form-control contractor-select updatearticleform-contractors',
                    'multiple' => true,
                    'data-value' => $model->getOldContractors(),
                    'data-check-operations-url' => Url::to(['is-has-operations-with-another-items', 'type' => $model->getType(), 'ajax' => 1]),
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'closeOnSelect' => false,
                    'dropdownCssClass' => 'contractor-items',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                ],
            ]); ?>
        </div>
    </div>

    <?= Html::hiddenInput('itemId', $model->getItem()->id, [
        'id' => 'itemId',
    ]) ?>
    <?php ActiveForm::end(); ?>

</div>

<?php if (!$updateList): ?>
    <script>
        Ladda.stopAll();
    </script>
<?php endif; ?>

<?php Pjax::end() ?>
