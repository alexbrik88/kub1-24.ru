<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.12.2017
 * Time: 17:09
 */

use common\models\Contractor;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use frontend\modules\reference\models\ArticleForm;
use frontend\modules\reports\models\AbstractFinance;
use yii\bootstrap\Html;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;
use frontend\themes\kub\helpers\Icon;
use yii\web\JsExpression;

/* @var $model ArticleForm
 * @var $type int|null
 * @var $parentModel
 * @var $updateList bool
 * @var $activeTab int
 */

//$updateList = $updateList ?? false;

if ($type !== null) {
    if ($type) {
        $types = [ArticleForm::TYPE_EXPENSE => 'Расход'];
    } else {
        $types = [ArticleForm::TYPE_INCOME => 'Приход'];
    }
} else {
    $types = [
        ArticleForm::TYPE_INCOME => 'Приход',
        ArticleForm::TYPE_EXPENSE => 'Расход',
    ];
}

$customersList = Contractor::getSorted()
    ->byCompany($model->getUser()->currentEmployeeCompany->company_id)
    ->byContractor(Contractor::TYPE_CUSTOMER)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->andWhere(['OR',
        ['invoice_income_item_id' => null],
        ['invoice_income_item_id' => ''],
    ])
    ->all();
$sellersList = Contractor::getSorted()
    ->byCompany($model->getUser()->currentEmployeeCompany->company_id)
    ->byContractor(Contractor::TYPE_SELLER)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->andWhere(['OR',
        ['invoice_expenditure_item_id' => null],
        ['invoice_expenditure_item_id' => ''],
    ])
    ->all();

Pjax::begin([
    'id' => 'article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'timeout' => 15000
]) ?>

<style type="text/css">
    .article-form .control-label {
        font-weight: bold;
    }

    .article-form .form-control {
        width: 100%;
    }

    #articleform-type > .radio {
        width: 140px
    }
</style>

<div class="article-form" data-header="<?= ($parentModel) ? 'Добавить подстатью для статьи' : 'Добавить статью' ?>">
    <?php $form = ActiveForm::begin([
        'id' => 'article-form',
        'enableClientValidation' => false,
        'action' => [
            'create',
            'type' => $type,
            'activeTab' => $activeTab,
            'parent' => $parentModel ? $parentModel->id : null,
            'ajax' => 'article-form'
        ],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
            'data-update-list-url' => \yii\helpers\Url::to(['ajax-list', 'type' => $activeTab]),
            'data-update-list' => (int)$updateList,
        ],
    ]); ?>
    <?= $form->field($model, 'type', [
        'options' => [
            'class' => 'form-group',
        ]])
        ->radioList($types, [
            'value' => $type !== null ? $type : $model->type ?? ArticleForm::TYPE_INCOME,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ],
                ]);
            },
        ]); ?>

    <?php if ($parentModel): ?>
        <?php $parentFlowOfFundsItem = $parentModel->getFlowOfFundsItemByCompany(Yii::$app->user->identity->currentEmployeeCompany->company_id)->one(); ?>
        <?php switch ($parentFlowOfFundsItem->flow_of_funds_block) {
            case AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK:
                $activityTypeName = 'Инвестиционная деятельность';
                break;
            case AbstractFinance::FINANCIAL_OPERATIONS_BLOCK:
                $activityTypeName = 'Финансовая деятельность';
                break;
            case AbstractFinance::OPERATING_ACTIVITIES_BLOCK:
                $activityTypeName = 'Операционная деятельность';
                break;
            default:
                $parentFlowOfFundsItem->flow_of_funds_block = AbstractFinance::OPERATING_ACTIVITIES_BLOCK;
                $activityTypeName = 'Операционная деятельность';
                break;
        } ?>
        <div class="form-group">
            <label class="label">Вид деятельности</label>
            <div class="form-filter">
                <input type="text" class="form-control" name="ArticleForm[parentActivityTypeName]" value="<?= $activityTypeName ?>" disabled>
                <input type="hidden" name="ArticleForm[activityType]" value="<?= $parentFlowOfFundsItem->flow_of_funds_block ?>">
            </div>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'activityType')->widget(Select2::class, [
            'hideSearch' => true,
            'data' => [
                AbstractFinance::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
                AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
                AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
            ],
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]); ?>
    <?php endif; ?>

    <?php if ($parentModel): ?>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label class="label">Название статьи</label>
                    <div class="form-filter">
                        <input type="text" class="form-control" name="ArticleForm[parentName]" value="<?= $parentModel->name ?>" disabled>
                        <input type="hidden" name="ArticleForm[parentId]" value="<?= $parentModel->id ?>">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название подстатьи') ?>
            </div>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <div class="form-group field-articleform-customer <?= ($type !== null && (int)$type === ArticleForm::TYPE_INCOME) ? null : 'hidden' ?>">
        <label class="label" for="article-customers">Покупатели</label>
        <div class="form-filter">
            <?= $form->field($model, 'customers', [
                'template' => "{input}",
                'options' => [
                    'id' => 'article-customers',
                ]
            ])->widget(Select2::class, [
                'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить нового покупателя']
                    + ['toggle-all' => 'Все']
                    + ArrayHelper::map($customersList, 'id', 'shortName'),
                'showToggleAll' => false,
                'options' => [
                    'placeholder' => 'Выберите покупателей для этой статьи',
                    'class' => 'form-control contractor-select',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'closeOnSelect' => false,
                    'dropdownCssClass' => 'contractor-items',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                ],
            ]); ?>
        </div>
    </div>
    <div class="form-group field-articleform-seller <?= ($type !== null && (int)$type === ArticleForm::TYPE_EXPENSE) ? null : 'hidden' ?>">
        <label class="label" for="article-sellers">Поставщики</label>
        <div class="form-filter">
            <?= $form->field($model, 'sellers', [
                'template' => "{input}",
                'options' => [
                    'id' => 'article-sellers',
                    'class' => 'show-contractor-type-in-fields',
                ]
            ])->widget(Select2::class, [
                'data' => ["add-modal-contractor" => Icon::PLUS . ' Добавить нового поставщика']
                    + ['toggle-all' => 'Все']
                    + ArrayHelper::map($sellersList, 'id', 'shortName'),
                'showToggleAll' => false,
                'options' => [
                    'placeholder' => 'Выберите поставщиков для этой статьи',
                    'class' => 'form-control contractor-select',
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'closeOnSelect' => false,
                    'dropdownCssClass' => 'contractor-items',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                ],
            ]); ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
//$enabledSellers = Contractor::find()
//    ->select('id')
//    ->byCompany(Yii::$app->user->identity->company->id)
//    ->byContractor(Contractor::TYPE_SELLER)
//    ->byIsDeleted(Contractor::NOT_DELETED)
//    ->byStatus(Contractor::ACTIVE)
//    ->andWhere([
//        'OR',
//        ['invoice_expenditure_item_id' => null],
//        ['invoice_expenditure_item_id' => ''],
//    ])
//    ->column();
//
//$enabledCustomers = Contractor::find()
//    ->select('id')
//    ->byCompany(Yii::$app->user->identity->company->id)
//    ->byContractor(Contractor::TYPE_CUSTOMER)
//    ->byIsDeleted(Contractor::NOT_DELETED)
//    ->byStatus(Contractor::ACTIVE)
//    ->andWhere([
//        'OR',
//        ['invoice_income_item_id' => null],
//        ['invoice_income_item_id' => ''],
//    ])
//    ->column();
$enabledCustomers = array_keys($customersList);
$enabledSellers = array_keys($sellersList);
?>

<script>
    enabledCustomers = <?= json_encode($enabledCustomers) ?>;
    enabledSellers = <?= json_encode($enabledSellers) ?>;
</script>

<?php if (!$updateList): ?>
    <script>
        Ladda.stopAll();
    </script>
<?php endif; ?>

<?php Pjax::end() ?>
