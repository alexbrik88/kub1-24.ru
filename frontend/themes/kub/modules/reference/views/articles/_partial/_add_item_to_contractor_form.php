<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2020
 * Time: 23:22
 */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;
use frontend\modules\reference\models\ArticlesSearch;
use kartik\select2\Select2;
use frontend\modules\reports\models\AbstractFinance;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Url;

/* @var $model \frontend\modules\reference\models\AddArticleToContractorForm
 * @var $type int|null
 * @var $activeTab int
 * @var $updateList bool
 */

$types = [];
if ($type !== null) {
    if ($type) {
        $types = [$type => 'Расход'];
    } else {
        $types = [$type => 'Приход'];
    }
}

Pjax::begin([
    'id' => 'article-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style type="text/css">
    .article-form .control-label {
        font-weight: bold;
    }

    .article-form .form-control {
        width: 100%;
    }

    #articleform-type > .radio {
        width: 140px
    }
</style>

<div class="article-form"
     data-header="Выбрать статью для <?= $type === ArticlesSearch::TYPE_INCOME ? 'Покупателя' : 'Поставщика' ?>">
    <?php $form = ActiveForm::begin([
        'id' => 'article-form',
        'enableClientValidation' => false,
        'action' => [
            'add-item-to-contractor',
            'id' => $model->getContractor()->id,
            'type' => $type,
            'activeTab' => $activeTab,
            'ajax' => 'article-form'
        ],
        'fieldConfig' => Yii::$app->params['kubFieldConfig'],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
            'data-update-list-url' => Url::to(['ajax-list', 'type' => $activeTab]),
            'data-change-old-operations' => (int)$model->changeOldOperations,
            'data-update-list' => (int)$updateList,
        ],
    ]); ?>

    <?= $form->field($model, 'type', [
        'options' => [
            'class' => 'form-group',
        ]])
        ->radioList($types, [
            'value' => $type,
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">' . $label . '</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ],
                ]);
            },
        ]); ?>

    <?= $form->field($model, 'activityType')->widget(Select2::class, [
        'hideSearch' => true,
        'data' => [
            AbstractFinance::OPERATING_ACTIVITIES_BLOCK => 'Операционная деятельность',
            AbstractFinance::FINANCIAL_OPERATIONS_BLOCK => 'Финансовая деятельность',
            AbstractFinance::INVESTMENT_ACTIVITIES_BLOCK => 'Инвестиционная деятельность',
        ],
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'width' => '100%',
        ],
    ]); ?>

    <div class="row">
        <div class="col-6">
            <?php if ($type === ArticlesSearch::TYPE_EXPENSE): ?>
                <?= $form->field($model, 'itemId')->widget(ExpenditureDropdownWidget::classname(), [
                    'loadAssets' => false,
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '',
                        'data-items' => json_encode($model->getArticlesByTypes()),
                        'data-check-operations-url' => Url::to(['is-has-operations-with-another-items', 'type' => $type, 'ajax' => 1]),
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                    ]
                ]) ?>
            <?php else: ?>
                <?= $form->field($model, 'itemId')->widget(ExpenditureDropdownWidget::classname(), [
                    'loadAssets' => false,
                    'income' => true,
                    'options' => [
                        'class' => 'flow-income-items',
                        'prompt' => '',
                        'data-items' => json_encode($model->getArticlesByTypes()),
                        'data-check-operations-url' => Url::to(['is-has-operations-with-another-items', 'type' => $type, 'ajax' => 1]),
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'placeholder' => '',
                    ]
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="col-6">
            <?= $form->field($model, 'subItemId')->widget(Select2::class, [
                'hideSearch' => true,
                'data' => $model->getSubarticles($type, $model->itemId),
                'options' => [
                    'prompt' => '',
                    'data-items' => $model->getSubarticles($type),
                ],
                'pluginOptions' => [
                    'width' => '100%',
                    'placeholder' => '',
                    'language' => [
                            'noResults' =>  new \yii\web\JsExpression('function(){
                                return $("<span />").html("Нет подстатей");
                            }'),
                    ],
                ],
            ]); ?>
        </div>
    </div>


    <?= Html::hiddenInput('contractorId', $model->getContractor()->id, [
        'id' => 'contractorId',
    ]) ?>

    <?= $form->field($model, 'contractorName')->textInput(['disabled' => true]) ?>

    <select id="static-items" class="hidden"></select>

    <?php ActiveForm::end(); ?>
</div>

<?php if (!$updateList): ?>
    <script>
        Ladda.stopAll();
    </script>
<?php endif; ?>

<?php Pjax::end() ?>
