<?php
use php_rutils\RUtils;
use frontend\modules\reference\models\ArticlesSearch;
?>
<div class="contractorsCount">
    <div class="row">
        <div class="col-5 pr-0 text-right">
            <?= $count ?: '' ?>
        </div>
        <div class="col-7 pl-1 text-left">
            <?= ($count) ? RUtils::numeral()->choosePlural(
                $count,
                $flowType === ArticlesSearch::TYPE_INCOME
                    ? ['Покупатель', 'Покупателя', 'Покупателей']
                    : ['Поставщик', 'Поставщика', 'Поставщиков']
            ) : '' ?>
        </div>
    </div>
</div>