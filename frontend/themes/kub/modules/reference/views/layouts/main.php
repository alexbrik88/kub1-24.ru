<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.03.2020
 * Time: 18:32
 */

use common\models\balance\BalanceArticle;
use frontend\modules\reference\models\ArticlesSearch;
use yii\bootstrap\Nav;

/* @var $content string */

$this->beginContent('@frontend/views/layouts/main.php');
?>
<div class="debt-report-content nav-finance">
    <div class="nav-tabs-row mb-2 pb-1">
        <?php echo Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Статьи',
                    'url' => ['/reference/articles/index', 'type' => ArticlesSearch::TYPE_BY_ACTIVITY],
                    'active' => Yii::$app->controller->id === 'articles',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                //[
                //    'label' => 'Основные средства',
                //    'url' => ['/reference/balance-articles/index', 'type' => BalanceArticle::TYPE_FIXED_ASSERTS],
                //    'active' => Yii::$app->controller->id === 'balance-articles'
                //        && Yii::$app->controller->action->id === 'index'
                //        && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_FIXED_ASSERTS,
                //    'options' => ['class' => 'nav-item'],
                //    'linkOptions' => ['class' => 'nav-link'],
                //],
                //[
                //    'label' => 'НМА',
                //    'url' => ['/reference/balance-articles/index', 'type' => BalanceArticle::TYPE_INTANGIBLE_ASSETS],
                //    'active' => Yii::$app->controller->id === 'balance-articles'
                //        && Yii::$app->controller->action->id === 'index'
                //        && (int)Yii::$app->request->get('type') === BalanceArticle::TYPE_INTANGIBLE_ASSETS,
                //    'options' => ['class' => 'nav-item'],
                //    'linkOptions' => ['class' => 'nav-link'],
                //],
            ],
        ]) ?>
    </div>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>
