<?php

/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2020
 * Time: 14:41
 */

use common\components\helpers\Month;
use common\models\balance\BalanceArticle;
use frontend\modules\reference\models\BalanceArticlesSearch;
use frontend\modules\reference\models\BalanceArticlesSubcategories;
use frontend\widgets\RangeButtonWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use common\components\helpers\Html;
use frontend\widgets\TableViewWidget;
use common\components\grid\GridView;
use yii\grid\SerialColumn;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use Carbon\Carbon;
use frontend\modules\reference\models\BalanceArticlesCategories;
use yii\grid\ActionColumn;
use common\components\TextHelper;
use frontend\themes\kub\widgets\ConfirmModalWidget;
use frontend\modules\reference\widgets\SummarySelectWidget;
use yii\bootstrap4\Modal;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $type int
 * @var $searchModel BalanceArticlesSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$currDate = Month::$monthFullRU[date('n')] . ' ' . date('Y');
$plusIcon = '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#add-icon"></use></svg>';
$this->title = $type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'Основные средства' : 'Нематериальные активы';
?>
<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <h4 class="mb-2"><?= $this->title ?></h4>
            </div>
            <div class="col-3 column pl-0">
                <div class="pb-2 mb-1">
                    <?= Html::a($plusIcon . '<span>Добавить</span>', 'javascript:;', [
                        'class' => 'button-regular button-regular_red button-width ml-auto w-100 balance-article-modal-link',
                        'data-url' => Url::to(['create', 'type' => $type]),
                    ]) ?>
                </div>
                <div class="pb-2 mb-1">
                    <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_no_right']); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="table-settings row row_indents_s" style="margin-top: 10px;">
    <div class="col-12">
        <?= TableViewWidget::widget(['attribute' => 'table_view_article']) ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table',
    ],
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return Html::checkbox('BalanceArticle[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],
        [
            'header' => '№№',
            'class' => SerialColumn::class,
        ],
        [
            'attribute' => 'purchased_at',
            'label' => 'Дата приобретения',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return DateHelper::format($model->purchased_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            },
        ],
        [
            'attribute' => 'name',
            'label' => 'Наименование',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
        ],
        [
            'attribute' => 'count',
            'label' => 'Кол-во',
            'headerOptions' => [
                'class' => 'sorting',
            ],
            'format' => 'raw',
        ],
        [
            'class' => DropDownSearchDataColumn::class,
            'attribute' => 'category',
            'label' => 'Вид',
            'filter' => $searchModel->getCategoryFilter(),
            'headerOptions' => [
                'class' => 'dropdown-filter tooltip2',
                'data-tooltip-content' => '#category-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return BalanceArticlesCategories::MAP[$model->category];
            },
        ],
        [
            'attribute' => 'amount',
            'label' => 'Стоимость приобретения',
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#amount-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return TextHelper::invoiceMoneyFormat($model->amount, 2);
            },
        ],
        [
            'attribute' => 'depreciation_for_today',
            'label' => "Амортизация по {$currDate}",
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#depreciation_for_today-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                $monthInUse = round((Carbon::now())->diffInDays(Carbon::parse($model->purchased_at)) / 30);

                return TextHelper::invoiceMoneyFormat($model->amount / $model->useful_life_in_month * $monthInUse, 2);
            },
        ],
        [
            'attribute' => 'cost_for_today',
            'label' => "Стоимость на {$currDate}",
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#cost_for_today-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                $monthInUse = round((Carbon::now())->diffInDays(Carbon::parse($model->purchased_at)) / 30);
                $costForToday = $model->amount - ($model->amount / $model->useful_life_in_month * $monthInUse);

                return '<span class="price" data-price="' . TextHelper::moneyFormatFromIntToFloat($costForToday) . '">'
                    . TextHelper::invoiceMoneyFormat($costForToday, 2)
                    . '</span>';
            },
        ],
        [
            'attribute' => 'useful_life_in_month',
            'label' => 'Срок полезного использования (мес.)',
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#useful_life_in_month-tooltip',
            ],
            'format' => 'raw',
        ],
        [
            'attribute' => 'month_in_use',
            'label' => 'Кол-во месяцев использования',
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#month_in_use-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return round((Carbon::now())->diffInDays(Carbon::parse($model->purchased_at)) / 30);
            },
        ],
        [
            'attribute' => 'month_left',
            'label' => 'Осталось месяцев',
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#month_left-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                $monthInUse = round((Carbon::now())->diffInDays(Carbon::parse($model->purchased_at)) / 30);

                return $model->useful_life_in_month - $monthInUse;
            },
        ],
        [
            'attribute' => 'write_off_date',
            'label' => 'Дата полного списания',
            'headerOptions' => [
                'class' => 'sorting tooltip2',
                'data-tooltip-content' => '#write_off_date-tooltip',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                $purchasedAt = Carbon::parse($model->purchased_at);

                return $purchasedAt->addDays($model->useful_life_in_month * 30)->format(DateHelper::FORMAT_USER_DATE);
            },
        ],
        [
            'class' => DropDownSearchDataColumn::class,
            'attribute' => 'status',
            'label' => 'Статус',
            'filter' => $searchModel->getStatusFilter(),
            'headerOptions' => [
                'class' => 'dropdown-filter',
            ],
            'format' => 'raw',
            'value' => function (BalanceArticle $model) {
                return BalanceArticle::STATUS_MAP[$model->status];
            },
        ],
        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}',
            'contentOptions' => [
                'class' => 'actions-grid',
            ],
            'buttons' => [
                'update' => function ($url, BalanceArticle $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                        'class' => 'balance-article-modal-link',
                        'style' => 'cursor: pointer;color: #0097fd;',
                        'data-url' => Url::to(['update', 'id' => $model->id, 'type' => $model->type]),
                        'title' => Yii::t('yii', 'Обновить'),
                        'aria-label' => Yii::t('yii', 'Обновить'),
                    ]);
                },
                'delete' => function ($url, $model) use ($type) {
                    return ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                            'class' => '',
                            'tag' => 'a',
                            'style' => 'color: #0097fd;margin-left: 10px;',
                        ],
                        'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить '
                            . ($type === BalanceArticle::TYPE_FIXED_ASSERTS
                                ? 'основное средство'
                                : 'нематериальный актив') . '?',
                    ]);
                },
            ],
        ],
    ]
]) ?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        Html::a('Копировать', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent copy-balance-article',
        ]),
        Html::a('Продано', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent sell-balance-articles',
            'data-url' => Url::to(['sell', 'type' => $type]),
        ]),
        Html::a('Списано', '#many-written-off', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
        Html::a('Удалить', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]),
    ],
]); ?>

<?php Modal::begin([
    'id' => 'many-written-off',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>
    <div class="row form-group">
        <div class="col-6">
            <div class="form-group">
                <label class="label">Дата списания:</label>
                <div>
                    <?= Html::textInput('written_off_at', date(DateHelper::FORMAT_USER_DATE), [
                        'id' => 'under-date',
                        'class' => 'form-control date-picker ico modal-document-date',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Создать', [
            'class' => 'button-regular button-width button-regular_red button-clr modal-many-create-act ladda-button',
            'data-url' => Url::to(['/reference/balance-articles/write-off', 'type' => $type]),
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr button-width button-regular button-hover-transparent button-clr',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'many-delete',
    'closeButton' => false,
    'options' => [
        'class' => 'fade confirm-modal',
    ],
]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные
        <?= $type === BalanceArticle::TYPE_FIXED_ASSERTS ? 'основные средства' : 'нематериальные активы' ?>?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $type]),
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Нет', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>
<?php Modal::end(); ?>

<?php Modal::begin([
    'id' => 'balance-article-modal-container',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>
    <h4 class="modal-title" id="balance-article-modal-header"></h4>
    <?php Pjax::begin([
        'id' => 'balance-article-form-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]) ?>
    <?php Pjax::end() ?>
<?php Modal::end() ?>

<?php Modal::begin([
    'id' => 'sell-balance-article-modal-container',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]) ?>
    <h4 class="modal-title" id="sell-balance-article-modal-header"></h4>
    <?php Pjax::begin([
        'id' => 'sell-balance-article-form-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]) ?>
    <?php Pjax::end() ?>
<?php Modal::end() ?>
<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/img/svg/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_useful_life_in_month_description" style="display: inline-block; text-align: center;">
        Данный срок нужен для расчета амортизации, <br>
        которая отражается в балансе. <br>
        Сроки определены ст. 258 НК РФ <br>
    </span>
    <span id="category-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Вид имущества, которое используется в бизнесе, <br>
            полезный срок эксплуатации которого больше <br>
            одного года и стоит больше 40 000 рублей
        <?php else: ?>
            Вид актива, который используется в бизнесе, <br>
            полезный срок эксплуатации которого больше <br>
            одного года и стоит больше 40 000 рублей
        <?php endif; ?>
    </span>
    <span id="amount-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Сколько заплатили (если была рассрочка, тогда суммируем все платежи)
        <?php else: ?>
            Сколько заплатили (если была рассрочка, тогда суммируем все платежи) и стоимости внедрения. <br>
            Либо только стоимость внедрения.
        <?php endif; ?>
    </span>
    <span id="depreciation_for_today-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Стоимость физического износа основного средства
        <?php else: ?>
            Сумма списанных расходов
        <?php endif; ?>
    </span>
    <span id="cost_for_today-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Текущая стоимость с учетом износа
        <?php else: ?>
            Текущая стоимость с учетом амортизации
        <?php endif; ?>
    </span>
    <span id="useful_life_in_month-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Рекомендуемый срок, в течение которого имущество приносит прибыль <br>
            (не обязательно равен сроку физического существования актива)
        <?php else: ?>
            Рекомендуемый срок, в течение которого актив приносит прибыль
        <?php endif; ?>
    </span>
    <span id="month_in_use-tooltip" style="display: inline-block; text-align: center">
        Период с момента приобретения до сегодняшнего дня
    </span>
    <span id="month_left-tooltip" style="display: inline-block; text-align: center">
        Период с сегодняшнего дня до окончания срока полезного использования
    </span>
    <span id="write_off_date-tooltip" style="display: inline-block; text-align: center">
        <?php if ($type === BalanceArticle::TYPE_FIXED_ASSERTS): ?>
            Дата, когда текущая стоимость с учетом износа станет равной нулю
        <?php else: ?>
            Дата, когда текущая стоимость с учетом амортизации станет равной нулю
        <?php endif; ?>
    </span>
</div>

<?php
$this->registerJs('$.pjax.defaults.scrollTo = false;', View::POS_LOAD);
$this->registerJs('
let balanceArticlesSubcategoriesMapByCategories = ' . json_encode(BalanceArticlesSubcategories::MAP_BY_CATEGORIES) . ';
let usefulLifeInMonthSubcategoriesMap = ' . json_encode(BalanceArticlesSubcategories::USEFUL_LIFE_IN_MONTH_MAP) . '

$(document).on("click", ".balance-article-modal-link", function(e) {
    e.preventDefault();
    $.pjax({
        url: $(this).data("url"),
        container: "#balance-article-form-container",
        push: false
    });
    $(document).on("pjax:success", "#balance-article-form-container", function() {
        $("#balance-article-modal-header").html($("[data-header]").data("header"));
        $("#balance-article-form-container .tooltip2").tooltipster({
            theme: ["tooltipster-kub"],
            trigger: "hover",
            side: "right",
        });

        $("#balance-article-modal-container").modal("show");
    });
});

$(document).on("click", ".sell-balance-articles", function(e) {
    e.preventDefault();
    $.pjax({
        type: "POST",
        url: $(this).data("url"),
        data: $(".joint-operation-checkbox").serialize(),
        container: "#sell-balance-article-form-container",
        push: false
    });
    $(document).on("pjax:success", "#sell-balance-article-form-container", function() {
        $("#sell-balance-article-modal-header").html($("[data-header]").data("header"));
        $("#sell-balance-article-form-container .date-picker").datepicker({
            "language": "ru",
            "autoclose": true
        })
        $("#sell-balance-article-modal-container").modal("show");
    });
});

$(document).on("change", ".balance-article-category", function(e) {
    let subcategories = balanceArticlesSubcategoriesMapByCategories[$(this).val()];
    let subcategoriesInput = $(".balance-article-subcategory");
    
    subcategoriesInput.find("option").remove();
    subcategoriesInput.append(new Option("", ""));
    for (let categoryId in subcategories) {
        subcategoriesInput.append(new Option(subcategories[categoryId], categoryId));
    }
    subcategoriesInput.trigger("change");
});

$(document).on("change", ".balance-article-subcategory", function(e) {
    let usefulLifeInMonth = usefulLifeInMonthSubcategoriesMap[$(this).val()];
    let usefulLifeInMonthInput = $("#balancearticleform-useful_life_in_month");
    
    if (usefulLifeInMonth === null) {
        usefulLifeInMonth = "Не амортизируется";
    }
    
    usefulLifeInMonthInput.val(usefulLifeInMonth);
});

$(document).on("click", ".copy-balance-article", function(e) {
    if ($(this).attr("disabled") !== "disabled") { 
        let balanceArticleId = $(".joint-operation-checkbox:checked").closest("tr").data("key");
        
        $(this).data("url", "/reference/balance-articles/create?id=" + balanceArticleId + "&type=' . $type . '");
        $(this).removeClass("copy-balance-article");
        $(this).addClass("balance-article-modal-link");
        $(this).click();
        $(this).removeClass("balance-article-modal-link");
        $(this).addClass("copy-balance-article");
    }
});

$(document).on("click", "#flow-list-pjax a", function(e) {
    e.preventDefault();
    $.pjax.reload("#flow-list-pjax", {
        "url": $(this).attr("href"),
        "data": {
            contractorId: $("#sellbalancearticleform-contractor_id").val()
        },
        "push": false,
        "replace": false,
        "timeout": 10000,
        "scrollTo": false,
        "container": "#flow-list-pjax"
    });
});
$(document).on("pjax:complete", "#flow-list-pjax", function() {
    $("#flow-list-pjax input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
    $("#flow-list-pjax input:radio:not(.md-radiobtn)").uniform();
});
$(document).on("change", "#sellbalancearticleform-contractor_id", function(e) {
    let flowTypeVal = $(".flow-type-field input:checked").val();
    
    $.pjax.reload("#flow-list-pjax", {
        "url": "/reference/balance-articles/sell",
        "data": {
            type: ' . $type . ',
            flowType: flowTypeVal,
            contractorId: $(this).val()
        },
        "push": false,
        "replace": false,
        "timeout": 10000,
        "scrollTo": false,
        "container": "#flow-list-pjax"
    });
});

$(document).on("change", "#sellbalancearticleform-create_new", function() {
    if ($(this).is(":checked")) {
        $(".flow_item_check").prop("checked", false).prop("disabled", true).parent().removeClass("checked");
    } else {
        $(".flow_item_check").prop("disabled", false);
    }
});
');
