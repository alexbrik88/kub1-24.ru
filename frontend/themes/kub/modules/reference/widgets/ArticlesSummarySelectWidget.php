<?php

namespace frontend\themes\kub\modules\reference\widgets;

use yii\base\Widget;

class ArticlesSummarySelectWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('articles_summary_select', ['widget' => $this]);
    }
}
