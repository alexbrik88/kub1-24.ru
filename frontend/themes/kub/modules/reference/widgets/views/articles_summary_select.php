<?php

use yii\helpers\Html;

/* @var $widget \frontend\themes\kub\modules\reference\widgets\ArticlesSummarySelectWidget */

$js = <<<SCRIPT
(function($){
    $(document).on('change', '.joint-operation-checkbox', function(){
        $('.selected-count').text($('.joint-operation-checkbox:checked').length);
    });

    if ($('.page-proxy').length) {
        $('.wrap_btns.check-condition').find('.total-cnt').parent().addClass('mr-auto');
    }
}(jQuery));
SCRIPT;

$this->registerJs($js);
?>
<div class="wrap wrap_btns check-condition" style="padding-right: 10px; padding-left: 10px;">
    <div class="row align-items-center justify-content-end actions-many-items">
        <div class="column flex-shrink-0">
            <input class="joint-operation-main-checkbox" id="Allcheck" type="checkbox" name="count-list-table" disabled="disabled">
            <span class="total-cnt total-txt-foot ml-3">
                Выбрано:
                <strong class="selected-count ml-1">0</strong>
            </span>
        </div>
        <div class="column flex-shrink-0 mr-auto ml-5"></div>
        <?php foreach (array_filter($widget->buttons) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
