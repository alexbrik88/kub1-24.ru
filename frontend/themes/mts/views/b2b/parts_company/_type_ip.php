<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

?>

<?= Html::activeHiddenInput($model, 'chief_post_name'); ?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'inn')->textInput([
                    'maxlength' => true,
                ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'egrip')->textInput([
                    'maxlength' => true,
                ]) ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'taxRegistrationDate')->textInput([
                    'maxlength' => true,
                    'class' => 'form-control date-picker ico',
                    'autocomplete' => 'off',
                    'value' => $model->taxRegistrationDate,
                    'disabled' => !empty($model->taxRegistrationDate),
                ])->label('Дата регистрации '.$model->companyType->name_short) ?>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-6 form-group mb-3">
                <?= $form->field($model, 'address_legal')->label('Юридический адрес')->textInput(); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
            </div>
            <div class="col-3 form-group mb-3 pt-4 pl-0">
                <div class="pt-1">
                    <div class="checkbox d-block mb-3 mt-2">
                        <?= $form->field($model, 'has_chief_patronymic')->checkbox([
                            'data-target' => '#company-ip_patronymic',
                        ], true) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-3 form-group mb-3">
                <?= $form->field($model, 'phone', [
                    'options' => [
                        'class' => 'form-group form-md-line-input form-md-floating-label'
                    ]
                ])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => '+7(XXX) XXX-XX-XX',
                    ],
                ])->label('Телефон', ['style' => 'top:0!important;font-size:13px!important']); ?>
            </div>
        </div>
    </div>
</div>
