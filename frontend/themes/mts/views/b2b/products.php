<?php

use common\components\grid\GridView;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\document\status\OrderDocumentStatus;
use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductSearch;
use common\models\product\ProductUnit;
use frontend\components\XlsHelper;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use frontend\widgets\ProductGroupDropdownWidget;
use frontend\widgets\StoreFilterWidget;
use frontend\widgets\TableConfigWidget;
use frontend\widgets\TableViewWidget;
use kartik\select2\Select2;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */
/* @var $defaultSortingAttr string */
/* @var $priceList PriceList */
/* @var $store */
/* @var $user \common\models\employee\Employee */

$this->title = Product::$productionTypes[$productionType];

$userConfig = Yii::$app->user->identity->config;
$tabViewClass = $userConfig->getTableViewClass('table_view_product');

$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();
$xlsMessageType = $productionType ? 'товаров' : 'услуг';

if ($productionType == 1) {
    $emptyMessage = 'Вы еще не добавили ни одного товара';
} elseif ($productionType == 0) {
    $emptyMessage = 'Вы еще не добавили ни одной услуги';
} else {
    $emptyMessage = 'Ничего не найдено';
}

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);

$actionItems = [];
if ($canUpdate) {
    if (count($storeList) > 1 && $productionType == Product::PRODUCTION_TYPE_GOODS) {
        $actionItems[] = [
            'label' => 'Переместить на склад',
            'url' => '#move_to_store_modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    $actionItems[] = [
        'label' => 'Изменить цену',
        'url' => '#many-price-modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    if ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE) {
        $actionItems[] = [
            'label' => 'Переместить в архив',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-archive',
                'data' => [
                    'url' => '/product/to-archive',
                ],
            ],
        ];
    } else {
        $actionItems[] = [
            'label' => 'Извлечь из архива',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-store-from-archive',
                'data' => [
                    'url' => '/product/to-store-from-archive',
                ],
            ],
        ];
    }
}
$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';
}
$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$hasFilters = !($searchModel->filterStatus == ProductSearch::IN_WORK) ||
    (bool)$searchModel->filterComment ||
    (bool)$searchModel->filterImage ||
    (bool)$searchModel->filterDate;

if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $tableConfigItems = [
        [
            'attribute' => 'product_article',
        ],
        [
            'attribute' => 'product_group',
            'label' => 'Группа товара',
        ],
        [
            'attribute' => 'product_image',
        ],
        [
            'attribute' => 'product_comment',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа товара',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
} else {
    $tableConfigItems = [
        [
            'attribute' => 'product_group',
            'label' => 'Группа услуги',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа услуги',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
}

$this->registerJs('
    var checkSelectedProduct = function(input) {
        var form = input.form;
        $("input.select-on-check-all", form)
            .attr("checked", $("input.product_checker:not(:checked)", form).length == 0)
            .uniform("refresh");
    }
    $(document).on("change", "#product_checker_form input.select-on-check-all", function(e) {
        $("#product_checker_form input.product_checker").attr("checked", this.checked).uniform("refresh");
        checkSelectedProduct(this);
    });
    $(document).on("change", "input.product_checker", function(e) {
        checkSelectedProduct(this);
    });
    $(document).on("click", "#confirm-many-delete", function(e) {
        if ($("input.product_checker:checked").length > 0) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
    $(".tooltip-help").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "side": "right",
        "contentAsHTML": true,
    });
');

$taxationType = $company->companyTaxationType;
?>

<div class="row align-items-center pb-2 mb-1">
    <div class="column">
        <h4><?= Product::$productionTypes[$productionType] ?></h4>
    </div>
    <div class="column ml-auto">
        <span class="button-regular button-regular_red mb-2 pl-3 pr-3 add-modal-new-product">
            <?= Icon::get('add-icon', ['class' => 'mr-2',]) ?>
            <span class="ml-1">Добавить</span>
        </span>
    </div>
    <div class="col-12">
        <div class="row mt-2 mb-3">
            <div class="col-9">
            </div>
            <div class="col-3">
                <?php if (Yii::$app->user->can(permissions\Product::CREATE)) : ?>
                    <?= Html::a(Icon::get('exel').Html::tag('span', 'Загрузить из Excel', [
                        'class' => 'ml-2',
                    ]), '#import-xls', [
                        'class' => 'button-regular w-100 button-hover-content-red',
                        'data' => [
                            'toggle' => 'modal',
                        ],
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="table-settings row row_indents_s">
    <div class="col-8">
        <div class="row align-items-center">
            <div class="sub-title column">Список товаров: <strong><?=$dataProvider->totalCount?></strong></div>
            <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                <?= Html::a(Icon::get('exel'), array_merge([
                    '/product/get-xls',
                ], [
                    'productionType' => $productionType,
                ] + Yii::$app->request->queryParams), [
                    'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                    'title' => 'Скачать в Excel',
                ]); ?>
                <?= TableConfigWidget::widget([
                    'items' => $tableConfigItems,
                    'sortingItems' => $sortingItems,
                ]); ?>
                <div class="mr-auto">
                    <?= TableViewWidget::widget(['attribute' => 'table_view_product']) ?>
                </div>
                <div class="dropdown popup-dropdown popup-dropdown_filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
                    <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="button-txt">Фильтр</span>
                        <?= Icon::get('shevron', ['class' => 'svg-icon-shevron']) ?>
                    </button>
                    <div class="dropdown-menu keep-open" aria-labelledby="filter">
                        <div class="popup-dropdown-in p-3">
                            <div class="filter-block p-1">
                                <div class="row">
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown1">
                                            <div class="label">Показывать</div>
                                            <?= Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'filterStatus',
                                                'data' => $filterStatusItems,
                                                'hideSearch' => true,
                                                'pluginOptions' => [
                                                    'width' => '100%',
                                                ],
                                                'options' => [
                                                    'class' => 'form-control filter-item-select',
                                                    'data-id' => 'filterStatus',
                                                    'data-old' => $searchModel->filterStatus,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-6 mb-3">
                                        <div class="dropdown-drop" data-id="dropdown2">
                                            <div class="label">По дате добавления</div>
                                            <?= Select2::widget([
                                                'model' => $searchModel,
                                                'attribute' => 'filterDate',
                                                'data' => [
                                                    ProductSearch::ALL => 'Все',
                                                    ProductSearch::FILTER_YESTERDAY => 'Вчера',
                                                    ProductSearch::FILTER_TODAY => 'Сегодня',
                                                    ProductSearch::FILTER_WEEK => 'Неделя',
                                                    ProductSearch::FILTER_MONTH => 'Месяц',
                                                ],
                                                'pluginOptions' => [
                                                    'width' => '100%',
                                                ],
                                                'hideSearch' => true,
                                                'options' => [
                                                    'class' => 'form-control filter-item-select',
                                                    'data-id' => 'filterDate',
                                                    'data-url' => Url::to(['filter-date', 'actionType' => 'set']),
                                                    'data-old' => $searchModel->filterDate,
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS) : ?>
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown3">
                                                <div class="label">Наличие картинки</div>
                                                <?= Select2::widget([
                                                    'model' => $searchModel,
                                                    'attribute' => 'filterImage',
                                                    'data' => [
                                                        ProductSearch::ALL => 'Все',
                                                        ProductSearch::HAS_IMAGE => 'Есть',
                                                        ProductSearch::NO_IMAGE => 'Нет',
                                                    ],
                                                    'pluginOptions' => [
                                                        'width' => '100%',
                                                    ],
                                                    'hideSearch' => true,
                                                    'options' => [
                                                        'class' => 'form-control filter-item-select',
                                                        'data-id' => 'filterImage',
                                                        'data-old' => $searchModel->filterImage,
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                        <div class="form-group col-6 mb-3">
                                            <div class="dropdown-drop" data-id="dropdown4">
                                                <div class="label">Наличие описания</div>
                                                <?= Select2::widget([
                                                    'model' => $searchModel,
                                                    'attribute' => 'filterComment',
                                                    'data' => [
                                                        ProductSearch::ALL => 'Все',
                                                        ProductSearch::HAS_COMMENT => 'Есть',
                                                        ProductSearch::NO_COMMENT => 'Нет',
                                                    ],
                                                    'pluginOptions' => [
                                                        'width' => '100%',
                                                    ],
                                                    'hideSearch' => true,
                                                    'options' => [
                                                        'class' => 'form-control filter-item-select',
                                                        'data-id' => 'filterComment',
                                                        'data-old' => $searchModel->filterComment,
                                                    ],
                                                ]); ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <div class="col-12 mt-3">
                                        <div class="row justify-content-between">
                                            <div class="form-group column">
                                                <?= Html::button('<span>Применить</span>', [
                                                    'class' => 'apply-filters button-regular button-hover-content-red button-width button-clr',
                                                ]); ?>
                                            </div>
                                            <div class="form-group column">
                                                <?= Html::button('<span>Сбросить</span>', [
                                                    'class' => 'apply-default-filters button-regular button-hover-content-red button-width button-clr',
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <?php $form = ActiveForm::begin([
            'method' => 'GET',
            'action' => Url::current([$searchModel->formName() => null]),
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'title', [
                    'type' => 'search',
                    'class' => 'form-control',
                    'placeholder' => 'Поиск...',
                ]) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        <?php $form->end(); ?>
    </div>
</div>

<input type="hidden" id="is_b2b" value="1">
<input type="hidden" id="production_type" value="<?= $productionType ?>">

<?php \yii\widgets\Pjax::begin([
    'id' => 'b2b_products_list',
]); ?>

<?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
    <?= GridView::widget([
        'id' => 'product-grid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
        'tableOptions' => [
            'class' => 'table table-style table-count-list' . $tabViewClass,
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'cssClass' => 'product_checker',
                'headerOptions' => [
                    'style' => 'width: 20px;',
                ],
            ],
            [
                'attribute' => 'article',
                'headerOptions' => [
                    'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                        'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                ],
                'contentOptions' => [
                    'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                        'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                ],
            ],
            [
                'attribute' => 'title',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'contentOptions' => [
                    'class' => 'col_product_name',
                ],
                'format' => 'raw',
                'value' => function (Product $data) use ($productionType) {
                    if (Yii::$app->user->can(frontend\rbac\permissions\Product::VIEW)) {
                        $content = Html::a(Html::encode($data->title), [
                            '/product/view',
                            'productionType' => $productionType,
                            'id' => $data->id,
                        ]);
                    } else {
                        $content = Html::encode($data->title);
                    }

                    return Html::tag('div', $content, ['class' => 'product-title-cell']);
                },
            ],
            [
                'attribute' => 'group_id',
                'enableSorting' => false,
                'label' => 'Группа ' . ($productionType ? 'товара' : 'услуги'),
                'headerOptions' => [
                    'class' => 'dropdown-filter col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                    'width' => '14%',
                ],
                'contentOptions' => [
                    'class' => 'col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                ],
                'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
                's2width' => '300px',
                'format' => 'raw',
                'value' => 'group.title',
            ],
            [
                'attribute' => 'available',
                'label' => 'Доступно',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->available * 1;
                },
                'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
            ],
            [
                'attribute' => 'reserve',
                'label' => 'Резерв',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $tooltipId = 'product-reserve-tooltip-' . $data->id;
                    $content = Html::a($data->reserve * 1, null, [
                        'class' => 'product-reserve-value',
                        'data' => [
                            'tooltip-content' => '#' . $tooltipId,
                            'url' => Url::to(['/product/reserve-documents', 'id' => $data->id]),
                        ],
                    ]);
                    $content .= Html::tag('span', Html::tag('span', '', ['id' => $tooltipId]), [
                        'class' => 'hidden',
                    ]);

                    return $content;
                },
                'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
            ],
            [
                'attribute' => 'quantity',
                'label' => 'Остаток',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->quantity * 1;
                },
                'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
            ],
            [
                'attribute' => 'product_unit_id',
                'enableSorting' => false,
                'label' => 'Ед. измерения',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'col_product_unit',
                ],
                'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
                'format' => 'raw',
                'value' => 'productUnit.name',
            ],
            [
                'label' => 'Картинка',
                'headerOptions' => [
                    'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                    'width' => '30px',
                ],
                'contentOptions' => [
                    'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                'value' => function (Product $data) {
                    $content = '';
                    if ($thumb = $data->getImageThumb(200, 300)) {
                        $tooltipId = 'tooltip-product-image-' . $data->id;
                        $content = Html::tag('span', '', [
                            'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                            'data-tooltip-content' => '#' . $tooltipId,
                            'style' => 'cursor: pointer;',
                        ]);
                        $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                        $content .= Html::beginTag('div', ['id' => $tooltipId]);
                        $content .= Html::img($thumb, ['alt' => '']);
                        $content .= Html::endTag('div');
                        $content .= Html::endTag('div');
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'comment_photo',
                'label' => 'Описание',
                'headerOptions' => [
                    'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                    'width' => '25%',
                ],
                'contentOptions' => [
                    'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                ],
                'format' => 'raw',
                'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                'value' => function ($data) {
                    $content = '';
                    if ($data->comment_photo) {
                        $tooltipId = 'tooltip-product-comment-' . $data->id;
                        $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                            'class' => 'product-comment-box',
                            'data-tooltip-content' => '#' . $tooltipId,
                            'style' => 'cursor: pointer;'
                        ]);
                        $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                        $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                        $content .= Html::encode($data->comment_photo);
                        $content .= Html::endTag('div');
                        $content .= Html::endTag('div');
                    }

                    return $content;
                },
            ],
            [
                'attribute' => 'price_for_buy_with_nds',
                'label' => 'Цена покупки',
                'headerOptions' => [
                    'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
                ],
                'format' => 'raw',
                'value' => function ($data) use ($user) {
                    $currentEmployeeCompany = $user->currentEmployeeCompany;
                    $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                        (!$currentEmployeeCompany->can_view_price_for_buy &&
                            $currentEmployeeCompany->is_product_admin &&
                            $data->creator_id == $currentEmployeeCompany->employee_id);

                    return $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($data->price_for_buy_with_nds, 2) :
                        Product::DEFAULT_VALUE;
                },
            ],
            [
                'attribute' => 'price_for_buy_nds_id',
                'label' => 'НДС покупки',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => 'priceForBuyNds.name',
                'visible' => Yii::$app->user->identity->company->hasNds(),
            ],
            [
                'attribute' => 'amountForBuy',
                'label' => 'Стоимость покупки',
                'headerOptions' => [
                    'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
                ],
                'format' => 'raw',
                'value' => function ($data) use ($user) {
                    $currentEmployeeCompany = $user->currentEmployeeCompany;
                    $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                        (!$currentEmployeeCompany->can_view_price_for_buy &&
                            $currentEmployeeCompany->is_product_admin &&
                            $data->creator_id == $currentEmployeeCompany->employee_id);

                    return $canViewPriceForBuy ?
                        TextHelper::invoiceMoneyFormat($data->amountForBuy, 2) :
                        Product::DEFAULT_VALUE;
                },
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
            ],
            [
                'attribute' => 'price_for_sell_with_nds',
                'label' => 'Цена продажи',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->not_for_sale ? 'Не для продажи' :
                        TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
                },
            ],
            [
                'attribute' => 'price_for_sell_nds_id',
                'label' => 'НДС продажи',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->not_for_sale ? 'Не для продажи' : ($data->priceForSellNds ? $data->priceForSellNds->name : '---');
                },
                'visible' => Yii::$app->user->identity->company->hasNds(),
            ],
            [
                'attribute' => 'amountForSell',
                'label' => 'Стоимость продажи',
                'headerOptions' => [
                    'class' => 'sorting',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return TextHelper::invoiceMoneyFormat($data->amountForSell, 2);
                },
                'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
            ],
        ],
    ]); ?>
<?= Html::endForm(); ?>

<?php \yii\widgets\Pjax::end(); ?>

<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', [
                $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'products' : 'description'
            ], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Далее', [(Yii::$app->controller->action->id == 'products') ? 'services' : 'module'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
            ]); ?>
        </div>
    </div>
</div>

<?= \frontend\themes\mts\widgets\SummarySelectProductWidget::widget([
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'folder']).' <span>В группу</span>', '#move_to_group_modal', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canUpdate ? Html::tag('div', Html::button('<span class="pr-2">Изменить</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
                'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . \yii\bootstrap4\Dropdown::widget([
                'items' => $actionItems,
                'options' => [
                    'class' => 'form-filter-list list-clr '
                ],
            ]), ['class' => 'dropup']) : null,
        ($canUpdate && ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE)) ? Html::a($this->render('//svg-sprite', ['ico' => 'archiev']).' <span>В архив</span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'id' => 'many-to-archive',
            'data' => [
                'url' => '/product/to-archive',
            ],
        ]) : null,
        ($canUpdate && ((int)$searchModel->filterStatus === ProductSearch::IN_ARCHIVE)) ? Html::a($this->render('//svg-sprite', ['ico' => 'archiev']).' <span>Из архива</span>', 'javascript:;', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'id' => 'many-to-store-from-archive',
            'data' => [
                'url' => '/product/to-store-from-archive',
            ],
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
    'productionType' => $productionType
]); ?>

<?php
if (Yii::$app->user->can(permissions\Product::CREATE)) {
    echo $this->render('/xls/_import_xls', [
        'header' => 'Загрузка ' . $xlsMessageType . ' из Excel',
        'text' => '<p>Для загрузки списка ' . $xlsMessageType . ' из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
        'formData' => Html::hiddenInput('className', 'Product') . Html::hiddenInput('Product[production_type]', $productionType),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => $productionType ? XlsHelper::PRODUCT_GOODS : XlsHelper::PRODUCT_SERVICES]),
    ]);

    if (Yii::$app->request->get('modal')) {
        $this->registerJs('
            $("#import-xls").modal();
            window.history.replaceState(null, null, window.location.pathname);
        ');
    }
}

$this->registerJs('
    $(".preview-product-photo").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "side": "right",
        "contentAsHTML": true
    });
    $(".product-comment-box").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "contentAsHTML": true
    });
    $(document).ready(function () {
        $("#product-grid tbody tr").each(function() {
            var $row = $(this);
            var height = $(".product-title-cell", $row).height();
            $(".product-comment-box", $row).css("height", height + "px");
            /*$(".product-comment-box", $row).pseudo(":before", "height", height + "px");*/
        });
    });
    $(document).on("click", ".product-reserve-value:not(.tooltipstered)", function() {
        var reserve = $(this);
        var tooltipId = reserve.data("tooltip-content")
        $.ajax({
            url: reserve.data("url"),
            success: function(data) {
                if (data.result) {
                    $(reserve.data("tooltip-content")).html(data.result);
                    reserve.tooltipster({
                        theme: "tooltipster-kub",
                        trigger: "click",
                        contentCloning: true
                    });
                    reserve.tooltipster("show");
                }
            }
        })
    });
    $("#config-product_group").change(function () {
        if (!$(this).is(":checked")) {
            $(".sorting-table-config-item#title").click().uniform();
        }
    });
    $(document).on("click", "a#many-to-archive, a#many-to-store-from-archive", function() {
        $.ajax({
            type: "post",
            url: $(this).data("url"),
            data: {
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    });
    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];
        $("select, input", $(this).closest(".filter-block")).each(function () {
            $filterData[this.name] = this.value;
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var filterBlock = $(this).closest(".filter-block");
        var $filterData = [];
        $("select", filterBlock).each(function () {
            $filterData[this.name] = this.firstElementChild.value;
        });
        $("input", filterBlock).each(function () {
            $filterData[this.name] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
        resetFiltersOldValue();
    }

    $(document).on("change", "#productsearch-filterdate", function (e) {
        changePeriod($(this).val(), $(this).data("url"));
    });

    function changePeriod(dateType, url) {
        var dateFrom = $("#productsearch-datefrom");
        var dateTo = $("#productsearch-dateto");
        var dateText = $(".block-date .date-text");
        var blockDate = $(".block-date");

        if (dateType == 0) {
            dateFrom.val("");
            dateTo.val("");
            dateText.text("");
            blockDate.hide();
        } else {
            $.post(url, {
                date_type: dateType,
                date_from: dateFrom.val(),
                date_to: dateTo.val()
            }, function (data) {
                dateFrom.val(data.dateFrom);
                dateTo.val(data.dateTo);
                dateText.text(data.dateText);
                blockDate.show();
            });
        }
    }

    $(document).on("click", "button.move_to_group_apply", function() {
        $.ajax({
            type: "post",
            url: $(this).data("url"),
            data: {
                group_id: $("#product_group_select").val(),
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    });

    // ADD NEW PRODUCT
    $(document).on("click", ".add-modal-new-product", function () {
        var ioType = $("#out-invoice-form").length ? INVOICE.InvoiceOut : INVOICE.documentIoType;
        var document = $("input#create-document").val();
        var form = {
            documentType: 2,
            Product: {production_type: $("#production_type").val(), flag: 1},
            document: document,
            isB2B: 1
        };
        $("#order-add-select, #product-add-select").select2("close");
        INVOICE.addNewProduct("/documents/invoice/add-modal-product", form);

        $("#add-new").on("hidden.bs.modal", function () {
            $.pjax.reload({container: "#b2b_products_list"});
        });

    });

    // FIX OVERFLOW BUG - otherwise input not focused
    $("#move_to_group_modal").on("select2:open", function (evt) {
        $(".modal.show").addClass("no-overflow").removeAttr("tabindex");
    });
    $("#move_to_group_modal").on("select2:close", function (evt) {
        $(".modal.show").removeClass("no-overflow");
    });

    $(document).on("change", ".filter-item-select", function () {
        checkFiltersCanged();
    });

    function resetFiltersOldValue() {
        $(".filter-item-select").each(function(i, el) {
            $(el).data("old", $(el).val());
        });
        $("button.apply-filters")
            .toggleClass("button-regular_red", false)
            .toggleClass("button-hover-content-red", true);
    }

    // FILTER BUTTON TOGGLE COLOR
    function checkFiltersCanged() {
        let changed = false;
        $(".filter-item-select").each(function(i, el) {
            let curVal = ""+$(el).val();
            let oldVal = $(el).data("old");
            console.log(oldVal != curVal);
            if (oldVal != curVal) {
                changed = true;
            }
        });
        if (changed) {
            $("button.apply-filters")
                .toggleClass("button-regular_red", true)
                .toggleClass("button-hover-content-red", false);
        } else {
            $("button.apply-filters")
                .toggleClass("button-regular_red", false)
                .toggleClass("button-hover-content-red", true);
        }
    }
'); ?>

<?php if ($canUpdate) : ?>
    <?= $this->render('parts_products/_many_price', [
        'company' => $company,
        'productionType' => $productionType,
    ]) ?>
    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
        <?= $this->render('/product/index/move_to_store', ['storeList' => $storeList, 'store' => $store]); ?>
    <?php endif; ?>
<?php endif ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'title' => 'Вы уверены, что хотите удалить выбранные ' . ($productionType ? 'товары?' : 'услуги?'),
        'id' => 'many-delete',
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'headerOptions' => [
            'class' => 'text-center'
        ],
        'options' => [
            'class' => 'confirm-modal fade'
        ],
    ]); ?>
        <div class="text-center">
            <?= \yii\bootstrap4\Html::a('Да', null, [
                'id' => 'confirm-many-delete',
                'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                'data-action' => Url::to(['/product/many-delete', 'productionType' => $productionType]),
            ]); ?>
            <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" data-dismiss="modal">Нет</button>
        </div>
    <?php Modal::end(); ?>
<?php endif; ?>

<?php Modal::begin([
    'title' => 'Переместить в группу',
    'id' => 'move_to_group_modal',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<div class="move_to_group">
    <div class="form-group">
        Выберите группу, в которую будут перемещены выбранные товары
    </div>

    <?= ProductGroupDropdownWidget::widget([
        'id' => 'product_group_select',
        'name' => 'product_id',
        'minimumResultsForSearch' => 5,
        'productionType' => $productionType,
        'pluginOptions' => [
            'width' => '100%',
        ]
    ]) ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr move_to_group_apply',
        'data' => [
            'url' => Url::to(['/product/to-group']),
        ],
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php Modal::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>