<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\out\OutInvoice */
/* @var $backUrl string */

$this->title = 'Редактирование ссылки на создание счета';
?>

<div class="wrap p-4">
    <h4 class="mb-2"><?= Html::encode($this->title) ?></h4>
    <?= $this->render('_form', [
        'model' => $model,
        'backUrl' => $backUrl,
    ]) ?>
</div>
