<?php

use common\components\grid\DropDownDataColumn;
use common\components\grid\GridView;
use common\models\out\OutInvoice;
use frontend\models\Documents;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OutInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $backTo string */
/* @var $company Company */

$backTo = isset($backTo) ? $backTo : 'company';
//$this->title = 'Внешние ссылки создания счета';
$company = Yii::$app->user->identity->company;

$isBlocked = !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT);
?>
<div class="out-invoice-index">
    <?= Alert::widget([
        'alertTypes' => [
            'out-error' => 'alert-danger',
            'out-danger' => 'alert-danger',
            'out-success' => 'alert-success',
            'out-info' => 'alert-info',
            'out-warning' => 'alert-warning'
        ],
    ]); ?>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-8">
            <div style="font-size: 23px;">Внешние ссылки создания счета</div>
        </div>
        <div class="col-sm-4">
            <?= Html::a('Создать ссылку', $company->canAddOutInvoice() ?
                ['/out-invoice/create', 'backTo' => $backTo] : 'javascript:;', [
                'class' => 'btn yellow pull-right' . ($company->canAddOutInvoice() ? null : ' payment-out-invoice-tab'),
            ]); ?>
        </div>
    </div>

    <?php $pjax = Pjax::begin([
        'id' => 'out-invoice-contractor-pjax',
        'timeout' => 10000,
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],

        'options' => [
            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => [
                    'width' => '20',
                ],
                'header' => '##',
            ],
            [
                'attribute' => 'outUrl',
                'format' => 'raw',
                'value' => function ($model) use ($isBlocked) {
                    return $model->is_outer_shopcart ? '' : Html::a($model->outUrl, $isBlocked ? null : $model->outUrl, [
                        'data' => [
                            'toggle' => $isBlocked ? 'modal': null,
                            'target' => $isBlocked ? '#freeTariffNotify': null,
                        ]
                    ]);
                }
            ],
            'additional_number',
            'note',
            'return_url:url',
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'class' => DropDownDataColumn::className(),
                'filter' => [null => 'Все'] + OutInvoice::$statusAvailable,
                'headerOptions' => [
                    'width' => '100',
                ],
                'value' => 'statusValue',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'out-invoice',
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '40',
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) use ($backTo) {
                        return Html::a("<span class='glyphicon glyphicon-pencil'></span>",
                            Url::to(['/out-invoice/update', 'id' => $model->id, 'backTo' => $backTo]), [
                                'title' => 'Изменить',
                                'aria-label' => 'Изменить',
                                'data-pjax' => '0',
                            ]);
                    },
                    'delete' => function ($url, $model, $key) use ($backTo) {
                        return \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'title' => 'Удалить',
                                'aria-label' => 'Удалить',
                                'data-pjax' => '0',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => Url::to(['/out-invoice/delete', 'id' => $model->id, 'backTo' => $backTo]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить ссылку?',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

    <?php $pjax->end(); ?>
</div>
