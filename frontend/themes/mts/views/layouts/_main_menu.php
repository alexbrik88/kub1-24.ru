<?php

use common\components\image\EasyThumbnailImage;
use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\notification\Notification;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\components\NotificationHelper;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use frontend\themes\mts\components\Icon;
use yii\base\Widget;
use yii\bootstrap4\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;
use kartik\checkbox\CheckboxX;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

/* @var $this \yii\web\View */
/* @var $isMTSbank bool */

$theme = $this->theme;
$company = null;
$companyItems = [];
$userItems = [];
$notifications = [];
if ($user) {
    $company = $user->company;
    $companyId = $user->company ? $user->company->id : null;
    $employeeCompanyArray = $user->getEmployeeCompany()
        ->joinWith('company.companyType')
        ->andWhere([
            'employee_company.is_working' => true,
            'company.blocked' => false,
        ])->orderBy([
            new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
            "company_type.name_short" => SORT_ASC,
            "company.name_short" => SORT_ASC,
        ])->all();
    $companyArray = ArrayHelper::getColumn($employeeCompanyArray, 'company');
    foreach ($companyArray as $item) {
        if ($item->id != $companyId) {
            $imgPath = $item->getImage('logoImage');
            if (is_file($imgPath)) {
                $logo = EasyThumbnailImage::thumbnailImg($imgPath, 40, 40, EasyThumbnailImage::THUMBNAIL_INSET);
            } else {
                $logo = $this->render('svg/company');
            }
            $companyItems[] = [
                'logo' => Html::tag('span', $logo, [
                    'class' => 'dropdown-popup-radius',
                ]),
                'label' => '<i class = "comp_1"></i> '.Html::encode($item->shortTitle),
                'encode' => false,
                'url' => [
                    '/site/change-company',
                    'id' => $item->id,
                ],
                'active' => $item->id == ArrayHelper::getValue($company, 'id'),
            ];
        }
    }

    $userItems = [
        [
            'label' => 'Профиль',
            'url' => ['/profile/index'],
        ],
        //[
        //    'label' => 'Мои документы',
        //    'url' => '#',
        //],
        [
            'label' => 'Помощь',
            'url' => '#',
        ],
    ];

    if ($company !== null) {
        $notifications = (new NotificationHelper([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->getNotifications($company);
    }
}

$module = Yii::$app->controller->module->id;
$controller = Yii::$app->controller->id;

if ($module == 'reports') {
    $logoText = 'ФинДиректор';
    $logoUrl  = Url::to(['/reports/finance/odds']);
} elseif ($module == 'tax' || ($module == 'app-frontend' && $controller == 'notification')) {
    $logoText = 'Бухгалтерия ИП';
    $logoUrl = Url::to(['/tax/robot/index']);
} elseif ($module == 'app-frontend' && $controller == 'b2b') {
    $logoText = 'Бизнес платежи';
    $logoUrl = Url::to(['/b2b/module']);
} else {
    $logoText = 'Выставление счетов';
    $logoUrl  = Url::to(['/documents/invoice/index', 'type' => 2]);
}

$canBuhIP = $company && $company->getCanTaxModule() && Yii::$app->user->can(UserRole::ROLE_CHIEF);
$notificationsUrl = Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]);
?>

<header class="header">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end" style="min-height: 40px;">
            <a class="header-logo logo column mr-auto" href="<?= $logoUrl ?>" style="padding-right:0;">
                <red-egg-svg>
                    <?php if ($isMTSbank) : ?>
                        <svg height="16" viewBox="0 0 151 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M0 14.5335C0 19.2468 2.64609 24 8.5715 24C14.4916 24 17.143 19.2468 17.143 14.5335C17.143 11.3138 16.0557 7.6064 14.2409 4.61182C12.476 1.72004 10.3555 0 8.5715 0C6.78241 0 4.66095 1.72004 2.91143 4.61182C1.08807 7.6064 0 11.3138 0 14.5335ZM60.8569 7.07121H54.8569V1.28564H73.2856V7.07121H67.2856V22.7141H60.8569V7.07121ZM86.9993 7.07146H96.4286V1.28564H87.0001C78.3366 1.28564 73.7114 5.59281 73.7114 12C73.7114 18.4072 78.3366 22.7144 87.0001 22.7144H96.4286V16.9286H86.9993C82.9908 16.9286 80.3542 15.4955 80.3542 12C80.3542 8.50449 82.9908 7.07146 86.9993 7.07146ZM38.355 14.0761L41.7822 1.28564H53.1424V22.7144H46.7136V5.2733L42.0405 22.7144H34.6695L29.9994 5.28438L29.9992 22.7144H23.5708V1.28564H34.9277L38.355 14.0761ZM112.438 7.49681H110.171V9.59307H112.438C113.012 9.59307 113.354 9.29799 113.354 8.75446V8.36641C113.354 7.80725 113.059 7.49681 112.438 7.49681ZM113.012 11.9998H107.143V1.28564H115.776V3.84782H110.171V5.24508H113.012C115.388 5.24508 116.537 6.42537 116.537 8.50601C116.537 10.6952 115.388 11.9998 113.012 11.9998ZM123.524 7.62104H121.132L122.328 3.66161L123.524 7.62104ZM128.12 12L124.812 1.28564H120.31L117.002 12H119.828L120.434 9.95038H124.223L124.843 12H128.12ZM135.775 12V7.85412H132.312V12H129.207V1.28564H132.312V5.21408H135.775V1.28564H138.881V12H135.775ZM144.391 5.21408H143.847V1.28564H140.742V12H143.847V7.85412H144.298L147.171 12H150.757L146.875 6.39438L150.447 1.28564H147.171L144.391 5.21408Z" fill="#EA212E"/>
                        </svg>
                    <?php else : ?>
                        <svg style="width: 4.0625rem; fill:#E30611;" class="red-egg-svg" viewBox="0 0 65 16" xmlns="http://www.w3.org/2000/svg">
                            <g transform="translate(0.000000, -25.000000)">
                                <path d="M5.714336,25 C6.903696,25 8.31736,26.146696 9.49392,28.074544 C10.70376,30.070936 11.42864,32.542568 11.42864,34.68896 C11.42864,37.8312 9.66104,41 5.714336,41 C1.764056,41 8.52651283e-14,37.8312 8.52651283e-14,34.68896 C8.52651283e-14,32.542568 0.7253792,30.070936 1.940952,28.074544 C3.107304,26.146696 4.521608,25 5.714336,25 Z M48.8568,25.857152 L48.8568,29.714192 L44.8568,29.714192 L44.8568,40.1428 L40.57104,40.1428 L40.57104,29.714192 L36.57104,29.714192 L36.57104,25.857152 L48.8568,25.857152 Z M64.28536,25.857152 L64.28536,29.71436 L57.9992,29.71436 C55.3268,29.71436 53.5692,30.66972 53.5692,33.00008 C53.5692,35.2597842 55.2218927,36.2265968 57.7587981,36.2831165 L57.9992,36.28576 L64.28536,36.28576 L64.28536,40.14296 L57.99976,40.14296 C52.22408,40.14296 49.14064,37.27152 49.14064,33.00008 C49.14064,28.809194 52.1088215,25.9660069 57.6757126,25.8602088 L57.99976,25.857152 L64.28536,25.857152 Z M23.28576,25.857152 L25.57064,34.38416 L27.85544,25.857152 L35.42888,25.857152 L35.42888,40.14296 L31.14296,40.14296 L31.14296,28.515592 L28.0276,40.14296 L23.1136,40.14296 L20.00024,28.522984 L20.00008,40.14296 L15.71448,40.14296 L15.71448,25.857152 L23.28576,25.857152 Z" id="Combined-Shape"></path>
                            </g>
                        </svg>
                    <?php endif ?>
                </red-egg-svg>
                <span class="logo-txt">
                    <?= $logoText ?>
                </span>
            </a>
            <?php if (!Yii::$app->user->isGuest) : ?>
                <ul class="menu-nav list-clr">
                    <li class="menu-nav-item">
                        <div class="dropdown">
                            <button class="button-clr dropdown-btn" type="button" id="dropdownMenuCompanys" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span class="dropdown-btn-txt">Мои компании</span>
                                <?= $this->render('svg/shevron') ?>
                            </button>
                            <div class="dropdown-popup dropdown-popup_companys dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
                                <div class="dropdown-popup-in">
                                    <ul class="dropdown-popup-list list-clr">
                                        <?php if ($company) : ?>
                                            <li>
                                                <a class="dropdown-popup-link active" href="<?= Url::to([
                                                    '/site/change-company',
                                                    'id' => $company->id,
                                                ]) ?>">
                                                    <?php
                                                    $imgPath = $company->getImage('logoImage');
                                                    if (is_file($imgPath)) {
                                                        $logo = EasyThumbnailImage::thumbnailImg(
                                                            $imgPath,
                                                            40,
                                                            40,
                                                            EasyThumbnailImage::THUMBNAIL_INSET
                                                        );
                                                    } else {
                                                        $logo = $this->render('svg/company');
                                                    }
                                                    ?>
                                                    <?= Html::tag('span', $logo, [
                                                        'class' => 'dropdown-popup-radius',
                                                    ]); ?>
                                                    <span style="display: block; margin-left: 12px; white-space: nowrap;">
                                                        <?= $company->shortTitle ?>
                                                    </span>
                                                </a>
                                            </li>
                                        <?php endif ?>
                                        <?php foreach ($companyItems as $item) : ?>
                                            <li>
                                                <a class="dropdown-popup-link" href="<?= Url::to($item['url']) ?>">
                                                    <?= $item['logo'] ?>
                                                    <span style="display: block; margin-left: 12px; white-space: nowrap;">
                                                        <?= $item['label'] ?>
                                                    </span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                        <li>
                                            <a class="dropdown-popup-link dropdown-popup-link_add" href="#create-company" data-toggle="modal">
                                                <span class="dropdown-popup-radius"><span>+</span></span>
                                                <span class="dropdown-popup-txt">Добавить компанию</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="menu-nav-item <?= ($active == 'documents') ? 'active' : '' ?>">
                        <?= Html::a('Документы', [
                            '/documents/invoice/index',
                            'type' => Documents::IO_TYPE_OUT,
                        ], [
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                    <li class="menu-nav-item <?= ($active == 'contractor') ? 'active' : '' ?>">
                        <?= Html::a('Контрагенты', [
                            '/contractor/index',
                            'type' => Contractor::TYPE_CUSTOMER,
                        ], [
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                    <li class="menu-nav-item <?= ($active == 'cash') ? 'active' : '' ?>">
                        <?= Html::a('Деньги', [
                            '/cash/bank/index',
                        ], [
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                    <li class="menu-nav-item <?= ($active == 'product') ? 'active' : '' ?>">
                        <?= Html::a('Товары и Услуги', [
                            '/product/index',
                            'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        ], [
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                    <li class="menu-nav-item">
                        <div class="dropdown">
                            <button class="button-clr dropdown-btn" type="button" id="dropdownMenuCompanys" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="dropdown-btn-txt">Ещё</span>
                                <?= $this->render('svg/shevron') ?>
                            </button>
                            <div class="dropdown-popup dropdown-popup_more dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 19px, 0px);">
                                <div class="dropdown-popup-in">
                                    <ul class="products-list list-clr">
                                        <li>
                                            <a class="products-list-link" href="<?= Url::to(['/documents/invoice/index', 'type' => 2]) ?>">
                                                <span class="notif-marker notif-marker_red">
                                                    <?= $this->render('svg/invoicing') ?>
                                                </span>
                                                <span class="products-link-txt">Выставление счетов</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="products-list-link" href="<?= Url::to(['/reports/options']) ?>">
                                                <span class="notif-marker notif-marker_red">
                                                    <?= $this->render('svg/analytics') ?>
                                                </span>
                                                <span class="products-link-txt">ФинДиректор</span>
                                            </a>
                                        </li>
                                        <?php if ($canBuhIP) : ?>
                                            <li>
                                                <a class="products-list-link" href="<?= Url::to(['/tax/robot/index']) ?>">
                                                    <span class="notif-marker notif-marker_red">
                                                        <?= $this->render('//svg-sprite', ['ico' => 'mix']) ?>
                                                    </span>
                                                    <span class="products-link-txt">
                                                        Налоги и Декларация ИП
                                                    </span>
                                                </a>
                                            </li>
                                        <?php endif ?>
                                        <li>
                                            <a class="products-list-link" href="<?= Url::to(['/b2b/module']) ?>">
                                                <span class="notif-marker notif-marker_red">
                                                    <?= $this->render('//svg-sprite', ['ico' => 'bank']) ?>
                                                </span>
                                                <span class="products-link-txt">Бизнес платежи</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="products-list-link" href="<?= Url::to(['/company/index']) ?>">
                                                <span class="notif-marker notif-marker_red">
                                                    <?= $this->render('//svg-sprite', ['ico' => 'cog']) ?>
                                                </span>
                                                <span class="products-link-txt">Настройки</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="header-others column">
                    <div class="header-others-row row align-items-center justify-content-between">
                        <div class="header-others-column column">
                            <div class="dropdown dropdown_products">
                                <button class="button-clr dropdown-btn" type="button" id="dropdownProducts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $this->render('svg/tiles') ?>
                                </button>
                                <div class="dropdown-popup dropdown-popup_products dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-popup-in">
                                        <div class="products-list-title">Доступные продукты</div>
                                        <ul class="products-list list-clr">
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <img class="products-link-img" src="/images/LogoMTS.svg" alt="">
                                                    <span class="products-link-txt">Сайт МТС</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_dark">&nbsp;</span>
                                                    <span class="products-link-txt">Виртуальный помошник</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_grey">&nbsp;</span>
                                                    <span class="products-link-txt">Маркетолог</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_dark">&nbsp;</span>
                                                    <span class="products-link-txt">Cashback</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_red">&nbsp;</span>
                                                    <span class="products-link-txt">Касса</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <br><br>
                                        <div class="products-list-title">Доступные продукты</div>
                                        <ul class="products-list list-clr">
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <img class="products-link-img" src="/images/LogoMTS.svg" alt="">
                                                    <span class="products-link-txt">Сайт МТС</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_dark">&nbsp;</span>
                                                    <span class="products-link-txt">Виртуальный помошник</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="products-list-link" href="#">
                                                    <span class="notif-marker notif-marker_grey">&nbsp;</span>
                                                    <span class="products-link-txt">Маркетолог</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-others-column column">
                            <div class="dropdown">
                                <button class="button-clr dropdown-btn" type="button" id="dropdownNotif" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?= $this->render('svg/bell') ?>
                                    <span class="dropdown-btn-indicator indicator">&nbsp;</span>
                                </button>
                                <div class="dropdown-popup dropdown-popup_notifications dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-popup-in">
                                        <ul class="notification-list list-clr">
                                            <?php foreach ($notifications as $notification) : ?>
                                                <li>
                                                    <a class="notification-link" href="<?= $notificationsUrl ?>">
                                                        <span class="notif-marker">
                                                            <?= Icon::get('attention') ?>
                                                        </span>
                                                        <span class="notif-details">
                                                            <span class="notification-message" title="<?= $notification->title; ?>">
                                                                <?= $notification->title; ?>
                                                            </span>
                                                            <br>
                                                            <span class="notification-time">
                                                                <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                                                    'format' => 'd F',
                                                                    'monthInflected' => true,
                                                                ]); ?>
                                                            </span>
                                                        </span>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <a class="all-notifications link link_bold" href="<?= $notificationsUrl ?>">Все уведмоления</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header-others-column column">
                            <div class="dropdown">
                                <button class="button-clr dropdown-btn" type="button" id="dropdownUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="dropdown-btn-img" src="/images/user-img.png" alt="">
                                </button>
                                <div class="dropdown-popup dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <?php if ($user) : ?>
                                        <div class="dropdown-popup-in">
                                            <div class="dropdown-popup-head">
                                                <div class="dropdown-user-name">
                                                    <?= $user->shortFio ?>
                                                </div>
                                                <a class="dropdown-user-mail" href="mailto:<?= $user->email ?>">
                                                    <?= $user->email ?>
                                                </a>
                                            </div>
                                            <ul class="dropdown-list list-clr">
                                                <?php foreach ($userItems as $item) : ?>
                                                    <li>
                                                        <?= Html::a($item['label'], $item['url'], [
                                                            'class' => 'dropdown-list-link',
                                                        ]) ?>
                                                    </li>
                                                <?php endforeach ?>
                                            </ul>
                                            <div class="dropdown-popup-foot">
                                                <?= Html::a('Выход', '#logout-confitm-modal', [
                                                    'class' => 'dropdown-list-link',
                                                    'data-toggle' => 'modal',
                                                ]) ?>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>

        <?= ConfirmModalWidget::widget([
            'toggleButton' => false,
            'options' => [
                'id' => 'logout-confitm-modal',
            ],
            'confirmUrl' => Url::to(['/site/logout']),
            'message' => 'Вы уверены, что хотите выйти?',
        ]) ?>
    </div>
</header>

<?php
if (!Yii::$app->user->isGuest) {
    Modal::begin([
        'id' => 'freeTariffNotify',
        'toggleButton' => false,
    ]);

    echo Yii::$app->user->identity->company->freeTariffNotified ?
        '<h4 class="modal-title">Текущая подписка - БЕСПЛАТНО</h4>' :
        '<h4 class="modal-title">Ваш аккаунт переведен на тариф БЕСПЛАТНО</h4>';
?>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>
    <div style="font-weight: 400; font-size: 16px; color: #333;">
        <p>
            Ограничения на тарифе БЕСПЛАТНО:
        </p>
        <ul>
            <li>5 счетов в месяц</li>
            <li>Только 1 организация</li>
            <li>Не больше 3-х сотрудников</li>
            <li>Место на диске - 1 ГБ</li>
        </ul>
        <p>
            Что бы работать без ограничений нужно <?= Html::a('перейти на платный тариф', ['/subscribe']) ?>.
        </p>
    </div>

<?php

    Modal::end();

    $this->registerJs('
        $(document).on("click", ".action-is-limited", function(e) {
            e.preventDefault();
            $("#freeTariffNotify").modal("show");
        });
    ');
}