<?php
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Nav;

$type = Yii::$app->request->get('type');
$id = Yii::$app->request->get('id');
$tab = Yii::$app->request->get('tab', 'index');
$contractor = $this->params['dossierContractor'];
$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <h4>Досье <?= ($contractor) ? $contractor->getShortName(true) : '' ?></h4>
    <?php
    echo Nav::widget([
        'items' => [
            ['label' => 'Главная', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id], 'active' => $tab == 'index'],
            ['label' => 'Учредители', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'founders'], 'active' => $tab == 'founders'],
            ['label' => 'Связи', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'connections'], 'active' => $tab == 'connections'],
            ['label' => 'Надежность', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'reliability'], 'active' => $tab == 'reliability'],
            ['label' => 'Финансы', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'finance'], 'active' => $tab == 'finance'],
            ['label' => 'Суды', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'arbitr'], 'active' => $tab == 'arbitr'],
            ['label' => 'Виды деят.', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'okved'], 'active' => $tab == 'okved'],
            ['label' => 'Отчетность', 'url' => ['/contractor/dossier', 'type' => $type, 'id' => $id, 'tab' => 'accounting'], 'active' => $tab == 'accounting']
        ],
        'options' => ['class' => 'nav nav-tabs w-100 mr-3'],
    ]);
    ?>

    <?php echo $content ?>

</div>

<?php $this->endContent(); ?>
