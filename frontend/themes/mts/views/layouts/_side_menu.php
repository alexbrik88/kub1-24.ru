<?php

use common\components\image\EasyThumbnailImage;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Menu;
use kartik\checkbox\CheckboxX;

$visibleOutInvoiceFacture = false;
$visibleInInvoiceFacture = false;
$isIp = false;
$logo = $this->render('svg/company');
if ($user) {
    if ($company) {
        $visibleByTaxationType = $company->companyTaxationType->osno;
        $visibleOutInvoiceFacture = $visibleByTaxationType ||
            InvoiceFacture::find()
                ->alias('if')
                ->joinWith('invoice invoice')
                ->where([
                    'invoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                    'if.type' => Documents::IO_TYPE_OUT
                ])
                ->exists();
        $visibleInInvoiceFacture = $visibleByTaxationType ||
            InvoiceFacture::find()
                ->alias('if')
                ->joinWith('invoice invoice')
                ->where([
                    'invoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                    'if.type' => Documents::IO_TYPE_IN
                ])
                ->exists();
        $isIp = $company->company_type_id == CompanyType::TYPE_IP;


        $imgPath = $company->getImage('logoImage');
        if (is_file($imgPath)) {
            $logo = EasyThumbnailImage::thumbnailImg($imgPath, 22, 22, EasyThumbnailImage::THUMBNAIL_INSET, [
                'style' => 'max-width: 100%; max-height: 100%;',
            ]);
        }
    }

    $items = [
        [
            'label' => 'Продажи',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 1, 'i' => 'sales']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 1]),
            'visible' => $active == 'documents' && Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_OUT,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'invoice' && !$paramDocument) && $paramType != Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Товарные накладные',
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_OUT,
                ],
                //[
                //    'label' => 'Товарно-транспортные<br>накладные',
                //    'encode' => false,
                //    'url' => ['/documents/waybill/index', 'type' => Documents::IO_TYPE_OUT],
                //    'active' => $controller === 'waybill' && $paramType == Documents::IO_TYPE_OUT,
                //],
                [
                    'label' => 'Счета-фактуры',
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => $visibleOutInvoiceFacture,
                ],
                [
                    'label' => 'УПД',
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_OUT,
                ],
                [
                    'label' => 'Заказы',
                    'url' => ['/documents/order-document/index'],
                    'active' => $controller === 'order-document',
                ],
                [
                    'label' => 'Договоры',
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_OUT,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]),
                ],
            ],
        ],
        [
            'label' => 'Покупки',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 2, 'i' => 'purchase']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 2]),
            'visible' => $active == 'documents' && Yii::$app->user->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_IN,
            ]),
            'items' => [
                [
                    'label' => 'Счета',
                    'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'invoice' && !$paramDocument) && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Акты',
                    'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'act' || $controller == 'invoice' && $paramDocument == 'act') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Товарные накладные',
                    'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'packing-list' || $controller == 'invoice' && $paramDocument == 'packing-list') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Счета-фактуры',
                    'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'invoice-facture' || $controller == 'invoice' && $paramDocument == 'invoice-facture') && $paramType == Documents::IO_TYPE_IN,
                    'visible' => $visibleInInvoiceFacture,
                ],
                [
                    'label' => 'УПД',
                    'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'upd' || $controller == 'invoice' && $paramDocument == 'upd') && $paramType == Documents::IO_TYPE_IN,
                ],
                [
                    'label' => 'Платёжные поручения',
                    'url' => ['/documents/payment-order/index'],
                    'active' => $controller === 'payment-order',
                    'visible' => Yii::$app->user->can(permissions\document\PaymentOrder::INDEX),
                ],
                [
                    'label' => 'Распознавание сканов',
                    'url' => ['/documents/upload-documents/index'],
                    'active' => $controller === 'upload-documents',
                    'visible' => false,
                ],
                [
                    'label' => 'Договоры',
                    'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_IN,
                    'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                        'type' => Contractor::TYPE_SELLER,
                    ])
                ],
                [
                    'label' => 'Доверенности',
                    'url' => ['/documents/proxy/index', 'type' => Documents::IO_TYPE_IN],
                    'active' => $controller === 'proxy' && $paramType == Documents::IO_TYPE_IN,
                ],
            ],
        ],
        [
            'label' => 'Выгрузка в 1С',
            'url' => ['/export/one-s/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => '1c']),
            'active' => $module == 'export' && $controller == 'one-s',
            'visible' => $active == 'documents',
        ],
        [
            'label' => 'Выгрузка документов',
            'url' => ['/export/files/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'doc-upload']),
            'active' => $module == 'export' && $controller == 'files',
            'visible' => $active == 'documents',
        ],
        [
            'label' => 'Покупатели',
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_CUSTOMER],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'basket']),
            'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_CUSTOMER,
            'visible' => $active == 'contractor' && Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_CUSTOMER,
            ]),
        ],
        [
            'label' => 'Поставщики',
            'url' => ['/contractor/index', 'type' => Contractor::TYPE_SELLER],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'bagagge']),
            'active' => $controller === 'contractor' && $paramType == Contractor::TYPE_SELLER,
            'visible' => $active == 'contractor' && Yii::$app->user->can(permissions\Contractor::INDEX, [
                'type' => Contractor::TYPE_SELLER,
            ]),
        ],
        [
            'label' => 'Банк',
            'url' => ['/cash/bank/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'bank-4']),
            'active' => $module == 'cash' && $controller == 'bank',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => 'Касса',
            'url' => ['/cash/order/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'bill']),
            'active' => $module == 'cash' && $controller == 'order',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\CashOrder::INDEX),
        ],
        [
            'label' => 'E-money',
            'url' => ['/cash/e-money/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'pouch']),
            'active' => $module == 'cash' && $controller == 'e-money',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => 'Итого',
            'url' => ['/cash/default/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'repeat']),
            'active' => $module == 'cash' && $controller == 'default',
            'visible' => $active == 'cash' && Yii::$app->user->can(permissions\Cash::INDEX),
        ],
        [
            'label' => 'Товары',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 3, 'i' => 'buildings']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 3]),
            'visible' => $active == 'product' && Yii::$app->user->can(permissions\Product::INDEX),
            'items' => [
                [
                    'label' => 'Склад',
                    'url' => [
                        '/product/index',
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                    ],
                    'active' => $controller == 'product' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        in_array($action, ['index', 'view', 'create', 'update']),
                ],
                [
                    'label' => 'Оборот товара',
                    'url' => [
                        '/product/turnover',
                        'per-page' => 20,
                        'productionType' => Product::PRODUCTION_TYPE_GOODS,
                        'turnoverType' => ProductSearch::TURNOVER_BY_COUNT,
                    ],
                    'active' => $controller == 'product' &&
                        $action == 'turnover' &&
                        Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS &&
                        Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_COUNT,
                ],
                [
                    'label' => 'Оборот ₽',
                    'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                        && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_AMOUNT,
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ]),
                ],
                //[
                //    'label' => 'XYZ Анализ',
                //    'url' => ['/product/xyz'],
                //    'active' => $controller == 'product' && $action == 'xyz',
                //    'visible' => (YII_ENV_DEV || $user->company_id == 486),// <-Удалить условие, когда задача будет принята
                //],
            ],
        ],
        [
            'label' => 'Услуги',
            'url' => [
                '/product/index',
                'productionType' => Product::PRODUCTION_TYPE_SERVICE,
                'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
            ],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'tools']),
            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_SERVICE,
            'visible' => $active == 'product' && Yii::$app->user->can(permissions\Product::INDEX),
        ],
        [
            'label' => 'Аналитика',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 4, 'i' => 'chart']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 4]),
            'visible' => $module == 'reports' &&
                Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                Yii::$app->user->can(UserRole::ROLE_MANAGER) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER),
            'items' => [
                [
                    'label' => 'Старт',
                    'url' => ['/reports/options'],
                    'active' => in_array($controller, ['options']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF))
                ],
                [
                    'label' => 'Дашборд',
                    'url' => ['/reports/dashboard/index'],
                    'active' => in_array($controller, ['dashboard']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF))
                ],
                [
                    'label' => 'Финансы',
                    'url' => ['/reports/finance/odds'],
                    'active' => in_array($controller, ['expenses-report', 'finance']) &&
                                in_array($action, ['odds', 'plan-fact', 'profit-and-loss', 'balance', 'expenses']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
                ],
                [
                    'label' => 'Платёжный календарь',
                    'url' => ['/reports/finance/payment-calendar'],
                    'active' => in_array($controller, ['expenses-report', 'finance']) &&
                                in_array($action, ['payment-calendar']),
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                        Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
                ],
                //[
                //    'label' => 'Сотрудники',
                //    'url' => ['/reports/employees/index'],
                //    'active' => $controller == 'employees',
                //    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                //            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)) &&
                //        (YII_ENV_DEV || $user->company_id == 486),// <-Удалить условие, когда задача будет принята
                //],
                //[
                //    'label' => 'По счетам',
                //    'url' => ['/reports/invoice-report/created'],
                //    'active' => $controller == 'invoice-report',
                //    'visible' => true,
                //],
                //[
                //    'label' => 'По клиентам',
                //    'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                //        ['/reports/analysis/index'] :
                //        ['/reports/debt-report/debtor'],
                //    'active' => in_array($controller, ['debt-report', 'analysis', 'discipline']),
                //    'visible' => true,
                //],
                //[
                //    'label' => 'По поставщикам',
                //    'url' => ['/reports/debt-report-seller/debtor'],
                //    'active' => in_array($controller, ['debt-report-seller',]),
                //    'visible' => true,
                //],
                //[
                //    'label' => 'По продажам',
                //    'url' => ['/reports/selling-report/index'],
                //    'active' => $controller == 'selling-report',
                //    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                //        Yii::$app->user->can(UserRole::ROLE_SUPERVISOR),
                //],
                //[
                //    'label' => 'По прибыли',
                //    'url' => ['/reports/profit/index'],
                //    'active' => $controller == 'profit',
                //    'visible' => YII_ENV_DEV || $user->company_id == 486 || $user->company_id == 628,
                //],
            ],
        ],
        [
            'label' => 'Настройки',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 5, 'i' => 'cog']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 5]),
            'visible' => in_array($controller, [
                'company',
                'employee',
            ]) || in_array($module, [
                'subscribe',
            ]),
            'items' => [
                [
                    'label' => 'Профиль компании',
                    'url' => ['/company/index'],
                    'active' => $controller == 'company',
                    'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
                ],
                [
                    'label' => 'Сотрудники',
                    'url' => ['/employee/index'],
                    'active' => $controller == 'employee',
                    'visible' => Yii::$app->user->can(permissions\Employee::INDEX),
                ],
                [
                    'label' => 'Оплатить КУБ',
                    // 'url' => ['/mts/subscribe/index'], // TEMP
                    'url' => ['/subscribe'],
                    'active' => $module == 'subscribe' && $controller == 'default',
                    'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
                ],
            ],
        ],
        [
            'label' => 'Бухгалтерия',
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_btn', ['id' => 6, 'i' => 'mix']),
            'submenuTemplate' => $this->render('_side_menu_items', ['id' => 6]),
            'visible' => $company &&
                Yii::$app->user->can(UserRole::ROLE_CHIEF) &&
                ($module == 'tax' || $controller == 'notification') &&
                ($company->getCanTaxModule() || $company->getCanOOOTaxModule()),
            'items' => [
                [
                    'label' => '<span class="title">Бухгалтерия для ИП</span>',
                    'encode' => false,
                    'url' => ['/tax/robot/index'],
                    'active' => $controller == 'robot',
                    'visible' => $company && $company->getCanTaxModule(),
                ],
                [
                    'label' => '<span class="title">Бухгалтерия для ООО</span>',
                    'encode' => false,
                    'url' => ['/tax/declaration-osno/index'],
                    'active' => $controller == 'declaration-osno' && \Yii::$app->controller->action->id != 'index-list',
                    'visible' => $company && $company->getCanOOOTaxModule(),
                ],
                [
                    'label' => '<span class="title">Налоговый календарь</span>',
                    'encode' => false,
                    'url' => ['/notification/index', 'month' => date('m'), 'year' => date('Y')],
                    'active' => $controller == 'notification',
                    'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()),
                ],
                [
                    'label' => '<span class="title">Отчетность</span>',
                    'encode' => false,
                    'url' => [($isIp) ? '/tax/declaration/index' : '/tax/declaration-osno/index-list'],
                    'active' => Yii::$app->controller->id == 'declaration' || Yii::$app->controller->id == 'kudir' ||
                        (Yii::$app->controller->id == 'declaration-osno' && \Yii::$app->controller->action->id == 'index-list'),
                    'visible' => true || $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) && (
                        $company->getHasActualSubscription(SubscribeTariffGroup::IP_USN_6) ||
                        $company->getHasActualSubscription(SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING)
                    ),
                ],
            ],
        ],
        [
            'label' => 'Ваш профиль',
            'url' => ['/profile/index'],
            'options' => ['class' => 'sideabar-menu-item'],
            'template' => $this->render('_side_menu_link', ['i' => 'man']),
            'active' => $controller == 'profile',
            'visible' => $controller == 'profile',
        ],
    ];
} else {
    $items = [];
}

$this->registerJs("
$(document).ready(function () {
    $('.sideabar-menu-item .collapse:has(.active)').collapse('show');
});
");
?>

<div class="page-sidebar column">
    <div class="sidebar sidebar_fixed">
        <?php if ($user) : ?>
            <div class="company-name">
                <?= Html::tag('span', $logo, [
                    'class' => 'side-menu-logo',
                ]); ?>
                <a class="company-name-text" href="<?= \yii\helpers\Url::to('/company/index') ?>"><?= Html::encode(ArrayHelper::getValue($company, 'shortTitle')) ?></a>
            </div>
            <?php /*<div class="sidebar-scroll custom-scroll">*/ ?>
                <?= Menu::widget([
                    //'activateParents' => true,
                    'items' => $items,
                    'options' => [
                        'id' => 'sidebarCollapse',
                        'class' => 'sidebar-menu list-clr',
                    ]
                ]) ?>
            <?php /*</div>*/ ?>
        <?php endif ?>
    </div>
</div>
