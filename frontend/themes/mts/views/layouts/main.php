<?php

use common\components\notification\NotificationFlash;
use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use frontend\models\Documents;
use frontend\themes\mts\assets\MtsAsset;
use frontend\themes\mts\widgets\Alert;
use frontend\widgets\AjaxModalWidget;
use frontend\widgets\InvoicePaymentModal;
use frontend\widgets\SubscribeReminderWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

MtsAsset::register($this);

$user = Yii::$app->user->identity;
$company = ArrayHelper::getValue($user, 'company');
$controller = Yii::$app->controller->id;
$module = Yii::$app->controller->module->id;
$action = Yii::$app->controller->action->id;
$route = Yii::$app->controller->getRoute();
$paramType = Yii::$app->request->getQueryParam('type');
$paramDocument = Yii::$app->request->getQueryParam('document');

if ($module == 'documents' || in_array($route, ['export/one-s/index', 'export/files/index'])) {
    $active = 'documents';
} elseif ($module == 'cash') {
    $active = 'cash';
} elseif ($controller == 'contractor') {
    $active = 'contractor';
} elseif ($controller == 'product') {
    $active = 'product';
} else {
    $active = null;
}

$isMTSbank = Yii::$app->params['isMTSbank'];
$footerText = $isMTSbank ?
    'ПАО «МТС-Банк» Лицензия Банка России №2268 от 17.12.2014 г.' :
    'ПАО «МТС»';
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="icon" href="/img/favicon_red.ico" type="image/x-icon">
        <?php if (YII_ENV === 'prod') : ?>
            <script src="https://cdn.optimizely.com/js/6093251051.js"></script>
        <?php endif; ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFPYASVmcsXKqaa1Z2m9tAQgXvdw0CdvM"></script>
        <?= (YII_ENV === 'prod') ? $this->render('_carrot') : null; ?>
        <?php if (YII_ENV === 'prod') : ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({
                        'gtm.start': new Date().getTime(), event: 'gtm.js'
                    });
                    var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-WBNX29');</script>
            <!-- End Google Tag Manager -->
        <?php endif; ?>
    </head>
    <body class="theme-mts">
        <div class="wrapper__in">
            <?php $this->beginBody() ?>

            <?= $this->render('_main_menu', [
                'user' => $user,
                'company' => $company,
                'controller' => $controller,
                'module' => $module,
                'action' => $action,
                'route' => $route,
                'paramType' => $paramType,
                'active' => $active,
                'isMTSbank' => $isMTSbank,
            ]) ?>

            <div class="wrapper__content">
                <div class="page-grid">
                    <div class="container-fluid">
                        <div class="row flex-nowrap">
                            <?= $this->render('_side_menu', [
                                'user' => $user,
                                'company' => $company,
                                'controller' => $controller,
                                'module' => $module,
                                'action' => $action,
                                'route' => $route,
                                'paramType' => $paramType,
                                'paramDocument' => $paramDocument,
                                'active' => $active,
                            ]) ?>

                            <div class="page-content column">
                                <?= $this->render('//layouts/_service_mts_subscribe'); ?>

                                <?= NotificationFlash::widget([
                                    'options' => [
                                        'closeButton' => true,
                                        'showDuration' => 1000,
                                        'hideDuration' => 1000,
                                        'timeOut' => 5000,
                                        'extendedTimeOut' => 1000,
                                        'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
                                        'escapeHtml' => false,
                                    ],
                                ]); ?>

                                <?= Alert::widget(); ?>

                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="copy column"> © <?= date('Y') ?> <?= $footerText ?> &nbsp;&nbsp;&nbsp; 18+</div>
                        <div class="column ml-5">
                        </div>
                        <div class="copy column ml-auto">Круглосуточная служба поддержки: 8 800 500 54 36. Бесплатно по России.</div>
                    </div>
                </div>
            </footer>
        </div>

        <?php /* if (Yii::$app->user->can(frontend\rbac\permissions\Subscribe::INDEX)) {
            echo SubscribeReminderWidget::widget(['company' => Yii::$app->user->identity->company]);
        } */ ?>

        <?= frontend\widgets\IfFreeTariffWidget::widget([
            'modalId' => 'if-free-tariff-xls',
            'linkSelector' => '.get-xls-link',
            'text' => 'Выгрузить данные в Excel можно только на платном тарифе.',
        ]); ?>

        <?= frontend\widgets\IfFreeTariffWidget::widget([
            'modalId' => 'if-free-tariff-word',
            'linkSelector' => '.get-word-link',
            'text' => 'Скачать документ в Word можно только на платном тарифе.',
        ]); ?>

        <?= $this->render('//layouts/modal/modal_after_registration'); ?>

        <?= $this->render('//layouts/modal/notify_modal'); ?>

        <?php // $this->render('//layouts/_service_video_modal'); ?>

        <?= !\Yii::$app->user->isGuest ? $this->render('//layouts/inquirer/_modal') : null; ?>

        <?= (false && YII_ENV === 'prod') ? $this->render('//layouts/script/jivosite') : null; ?>
        <?= AjaxModalWidget::widget(); ?>
        <?= InvoicePaymentModal::widget(); ?>

        <?= $this->render('@frontend/widgets/views/modal/menu_no_rules'); ?>

        <?= $this->render('@frontend/widgets/views/modal/_create_company', [
            'newCompany' => new Company([
                'company_type_id' => CompanyType::TYPE_OOO,
                'scenario' => Company::SCENARIO_CREATE_COMPANY,
            ]),
            'companyTaxation' => new CompanyTaxationType(),
        ]); ?>

        <?php $this->endBody(); ?>

        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                ChartsFlotcharts.init();
                ChartsFlotcharts.initCharts();
                ChartsFlotcharts.initBarCharts();
            });
        </script>

        <?= $this->render('//layouts/_metrics'); ?>
    </body>
    <?php if (YII_ENV === 'prod') : ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="" https:
            //www.googletagmanager.com/ns.html?id=GTM-WBNX29""
            height=""0"" width=""0""
            style=""display:none;visibility:hidden""></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="" page"" class=""hfeed site"">
    <?php endif; ?>
</html>
<?php $this->endPage(); ?>