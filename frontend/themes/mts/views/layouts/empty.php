<?php

use frontend\themes\mts\assets\MtsAsset;

/* @var $this \yii\web\View */
/* @var $content string */

MtsAsset::register($this);
?>
<?php $this->beginPage() ?>

<?php $this->beginBody() ?>

<?php echo $content ?>

<?php $this->endBody(); ?>

<?php $this->endPage(); ?>
