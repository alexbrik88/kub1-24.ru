<?php
use frontend\components\PageSize;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}
?>

<div class="wrap wrap_padding_none">
        <div class="table-wrap">
            {items}
        </div>
</div>
<div class="table-settings-view row align-items-center">
    <div class="col-9">
        <nav>
            {pager}
        </nav>
    </div>
    <div class="col-3">
        <?= $this->render('perPage', [
            'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => $pageSizeParam,
        ]) ?>
    </div>
</div>