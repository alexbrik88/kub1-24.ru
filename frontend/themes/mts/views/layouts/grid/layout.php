<?php

use frontend\components\PageSize;
use yii\helpers\ArrayHelper;

$pageSizeParam = ArrayHelper::getValue($_params_, 'pageSizeParam', 'per-page');
$showExtraLink = ArrayHelper::getValue($_params_, 'showExtraLink', false);
?>

<div class="wrap wrap_padding_none">
    <div class="custom-scroll-table">
        <div class="table-wrap">
            {items}
        </div>
    </div>
</div>
<div class="table-settings-view row align-items-center">
    <div class="col-9">
        <nav>
            {pager}
        </nav>
    </div>
    <div class="col-3">
        <?= $this->render('perPage', [
            'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
            'pageSizeParam' => $pageSizeParam,
        ]) ?>
    </div>
</div>