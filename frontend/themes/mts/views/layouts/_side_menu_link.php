<div class="sidebar-menu-head">
    <a class="sidebar-menu-btn button-clr" href="{url}">
        <svg class="sidebar-menu-icon svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#<?=$i?>"></use>
        </svg>
        <span class="sidebar-menu-btn-txt">{label}</span>
        <svg class="sidebar-menu-angle svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#arrow"></use>
        </svg>
    </a>
</div>