<?php

use yii\bootstrap4\Modal;
use yii\helpers\Html;

$message = Yii::$app->session->remove('notify_modal');
?>

<?php if ($message !== null) : ?>

<?php Modal::begin([
    'id' => 'notify_modal',
    'footer' => Html::button('Ok', [
        'class' => 'button-regular button-hover-transparent',
        'style' => 'width: 44px;',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]),
    'closeButton' => false,
    'clientOptions' => [
        'show' => true,
    ],
]); ?>
    <div class="form-group">
        <?= $message ?>
    </div>
<?php Modal::end(); ?>

<?php endif ?>
