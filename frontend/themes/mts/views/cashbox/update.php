<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$this->title = 'Изменить Кассу: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кассы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cashbox-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
