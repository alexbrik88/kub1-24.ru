<?php

use common\models\employee\Employee;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$employeeArray = $model->company->getEmployeeCompanies()->andWhere([
    'or',
    ['is_working' => true],
    ['employee_id' => $model->responsible_employee_id],
])->orderBy([
    'lastname' => SORT_ASC,
    'firstname' => SORT_ASC,
    'patronymic' => SORT_ASC,
])->all();
$accessibleList = ['all' => 'Всем'] + ArrayHelper::map($employeeArray, 'employee_id', 'fio');
$templateDateInput = "{label}<div class='date-picker-wrap' style='width: 130px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";
?>

<?php if (Yii::$app->request->isAjax && $model->saved) : ?>
    <?= Html::script('
        $.pjax.reload("#cashbox-pjax-container", {timeout: 5000});
        $(".modal.in").modal("hide");
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>


    <?php $form = ActiveForm::begin([
        'id' => 'cashbox-form',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
            'checkOptions' => [
                'class' => '',
                'labelOptions' => [
                    'class' => 'label'
                ],
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
        ],
    ]); ?>

        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>
            </div>

            <?php if (!$model->is_main) : ?>
                <div class="col-6">
                    <?= $form->field($model, 'accessible')->widget(Select2::class, [
                        'data' => $accessibleList,
                        'options' => [
                            'placeholder' => '',
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]) ?>
                </div>
            <?php endif ?>
        </div>

        <?= $form->field($model, 'is_accounting')->checkbox([], true) ?>

        <?php if (!$model->isNewRecord && !$model->is_main) : ?>
            <?= $form->field($model, 'is_closed')->checkbox([], true) ?>
        <?php endif ?>

        <?php if ($model->isNewRecord) : ?>
            <?= $form->field($model, 'createStartBalance')->checkbox([], true) ?>

            <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : null; ?>">
                <div class="col">
                    <?= $form->field($model, 'startBalanceAmount')->textInput([
                        'class' => 'form-control js_input_to_money_format',
                    ]) ?>
                </div>
                <div class="col">
                    <?= $form->field($model, 'startBalanceDate', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                    ])->label('на дату') ?>
                </div>
            </div>
        <?php endif ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('Сохранить', [
                'class' => 'button-regular button-regular_red button-width button-clr',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
                'data-dismiss' => 'modal'
            ]); ?>
        </div>

    <?php ActiveForm::end(); ?>


<?= Html::script('
$("#cashbox-form input:checkbox:not(.md-check)").uniform();

$("#cashbox-startbalancedate").datepicker({
    language: "ru",
    autoclose: true,
}).on("change.dp", dateChanged);

function dateChanged(ev) {
    if (ev.bubbles == undefined) {
        var $input = $("#" + ev.currentTarget.id);
        if (ev.currentTarget.value == "") {
            if ($input.data("last-value") == null) {
                $input.data("last-value", ev.currentTarget.defaultValue);
            }
            var $lastDate = $input.data("last-value");
            $input.datepicker("setDate", $lastDate);
        } else {
            $input.data("last-value", ev.currentTarget.value);
        }
    }
}

$("#cashbox-createstartbalance").on("change", function (e) {
    $(".start-balance-block").toggleClass("hidden");
});
', [
    'type' => 'text/javascript',
]); ?>
