<?php
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Запрос на сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <?= \frontend\widgets\Alert::widget(); ?>
    <h4><?= Html::encode($this->title) ?></h4>
    <p>Пожалуйста введите Ваш e-mail.<br>На него будет отправлена ссылка для восстановления пароля.</p>
    <div class="row">
        <div class="col-5">
            <?php \yii\widgets\Pjax::begin(['enablePushState' => false]); ?>
                <?php $form = ActiveForm::begin([
                    'id' => 'request-password-reset-form',
                    'enableAjaxValidation' => true,
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                    'validateOnSubmit' => true,
                    'options' => [
                        'data-pjax' => '',
                    ],
                    'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
                ]); ?>
                    <?= $form->field($model, 'email') ?>
                    <?= Html::submitButton('Отправить', ['class' => 'button-regular button-regular_red width-120']) ?>
                <?php ActiveForm::end(); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
