<?php

use frontend\models\AuthSignupForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuthSignupForm */

$this->title = 'Войти';

switch ($model->scenario) {
    case AuthSignupForm::SCENARIO_LOGIN:
        $view = '_login';
        break;
    case AuthSignupForm::SCENARIO_CREATE:
        $view = '_create';
        break;

    default:
        $view = '_email';
        break;
}
?>

<h4><?= Html::encode($this->title) ?></h4>

<div class="row">
    <div class="col-lg-5">
        <?php Pjax::begin([
            'id' => 'auth-signup-pjax',
            'formSelector' => '#auth-signup-form',
            'linkSelector' => false,
        ]) ?>

        <?php $form = ActiveForm::begin([
            'id' => 'auth-signup-form',
            'enableClientValidation' => false,
            'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
        ]); ?>

        <?= Html::hiddenInput('scenario', $model->scenario) ?>

        <?= Html::activeHiddenInput($model, 'registrationPageTypeId') ?>

        <?= $this->render($view, [
            'form' => $form,
            'model' => $model,
        ]) ?>

        <?php ActiveForm::end(); ?>

        <?php Pjax::end() ?>
    </div>
</div>
