<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\models\AuthSignupForm */

?>

<?= $form->field($model, 'email')->label('Ведите E-mail') ?>

<?= Html::submitButton('Далее', [
    'class' => 'button-regular button-regular_red width-120',
]) ?>
