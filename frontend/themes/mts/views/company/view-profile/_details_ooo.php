<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

?>

<div class="row">
    <div class="col-6 pb-3">
        <div class="wrap pt-3 pl-4 pr-4 pb-4 mb-1">
            <div class="pt-3 pl-1 pr-1">
                <div class="label weight-700 mb-3">Руководитель (для документов)</div>
                <div>
                    <?= join(', ', array_filter([
                        $company->chief_post_name,
                        $company->getChiefFio(),
                    ])); ?>
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="col-6 pb-3">
        <div class="wrap pt-3 pl-4 pr-4 pb-4 mb-1">
            <div class="pt-3 pl-1 pr-1">
                <div class="label weight-700 mb-3">Главный бухгалтер (для документов)</div>
                <div>
                    <?= $company->getChiefAccountantFio(); ?>
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <h4 class="mb-3 pb-3">Реквизиты</h4>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ИНН</div>
                <div><?= $company->inn; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">КПП</div>
                <div><?= $company->kpp; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОГРН</div>
                <div><?= $company->ogrn; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКВЭД</div>
                <div><?= $company->okved; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКПО</div>
                <div><?= $company->okpo; ?>&nbsp;</div>
            </div>
        </div>
        <div class="pb-3 mb-3">
            <div class="label weight-700 mb-3">Юридический адрес</div>
            <div><?= $company->getAddressLegalFull(); ?>&nbsp;</div>
        </div>
        <div class="pb-3 mb-3">
            <div class="label weight-700 mb-3">Фактический адрес</div>
            <div><?= $company->getAddressActualFull(); ?>&nbsp;</div>
        </div>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКАТО</div>
                <div><?= $company->okato; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКТМО</div>
                <div><?= $company->oktmo; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКОГУ</div>
                <div><?= $company->okogu; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКФС</div>
                <div><?= $company->okfs; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ОКОПФ</div>
                <div><?= $company->okopf; ?>&nbsp;</div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ПФР</div>
                <div><?= $company->pfr; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">ФСС</div>
                <div><?= $company->fss; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">Уставной капитал</div>
                <div><?= TextHelper::invoiceMoneyFormat($company->capital, 2); ?>&nbsp;</div>
            </div>
            <div class="column pb-3 mb-3 mr-4">
                <div class="label weight-700 mb-3">Дата постановки на учёт в налоговом органе</div>
                <div>
                    <?= DateHelper::format(
                        $company->tax_authority_registration_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    ); ?>
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>