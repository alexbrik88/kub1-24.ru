<?php

use common\components\date\DateHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <h4 class="mb-3 pb-3">Реквизиты</h4>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ИНН</div>
                <div><?= $company->inn; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ОГРНИП</div>
                <div><?= $company->egrip; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ОКВЭД</div>
                <div><?= $company->okved; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ОКПО</div>
                <div><?= $company->okpo; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ОКТМО</div>
                <div><?= $company->oktmo; ?>&nbsp;</div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-8 pb-3 mb-3">
                <div class="label weight-700 mb-3">Юридический адрес</div>
                <div><?= $company->getAddressLegalFull(); ?>&nbsp;</div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">ОКУД</div>
                <div><?= $company->okud; ?>&nbsp;</div>
            </div>
        </div>
        <div class="pb-3 mb-3">
            <div class="label weight-700 mb-3">Фактический адрес</div>
            <div><?= $company->getAddressActualFull(); ?>&nbsp;</div>
        </div>
        <div class="row align-items-center">
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ФСС</div>
                <div><?= $company->fss; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ПФР (как ИП)</div>
                <div><?= $company->pfr_ip; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">ПФР (как работодателя)</div>
                <div><?= $company->pfr_employer; ?>&nbsp;</div>
            </div>
            <div class="column pb-3 mb-3">
                <div class="label weight-700 mb-3">Дата постановки на учёт в налоговом органе</div>
                <div>
                    <?= DateHelper::format(
                        $company->tax_authority_registration_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    ); ?>
                    &nbsp;
                </div>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col-4 pb-3 mb-3">
                <div class="label weight-700 mb-3">Серия и номер свидетельства</div>
                <div><?= $company->ip_certificate_number; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">Дата выдачи</div>
                <div><?= $company->certificateDate; ?>&nbsp;</div>
            </div>
            <div class="col-2 pb-3 mb-3">
                <div class="label weight-700 mb-3">Кем выдано</div>
                <div><?= $company->ip_certificate_issued_by; ?>&nbsp;</div>
            </div>
        </div>
    </div>
</div>