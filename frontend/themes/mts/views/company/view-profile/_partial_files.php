<?php

use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

?>

<div class="row">
    <div class="col-4 d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1">
            <div class="p-3 d-flex flex-column flex-grow-1">
                <div class="label weight-700 mb-3">Логотип компании</div>
                <div class="flex-grow-1 d-flex flex-column justify-content-center">
                    <div class="flex-shrink-0 text-center">
                        <!-- <img src="images/kub-logo-mini.png" alt=""> -->
                        <?php
                        $imgPath = $company->getImage('logoImage');
                        if (is_file($imgPath)) {
                            echo EasyThumbnailImage::thumbnailImg($imgPath, 545, 186, EasyThumbnailImage::THUMBNAIL_INSET, [
                                'style' => 'max-width: 100%; max-height: 100%;'
                            ]);
                        } else {
                            echo '<img src="" class="signature_image">';
                        }; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4 d-flex flex-column">
        <div class="wrap p-3 pb-0 d-flex flex-column flex-grow-1">
            <div class="p-3 d-flex flex-column flex-grow-1">
                <div class="label weight-700 mb-3">Печать компании</div>
                <div class="flex-grow-1 d-flex flex-column justify-content-center">
                    <div class="flex-shrink-0 text-center">
                        <!-- <img src="images/kub-seal.png" alt=""> -->
                        <?php
                        $imgPath = $company->getImage('printImage');
                        if (is_file($imgPath)) {
                            echo EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET, [
                                'style' => 'max-width: 100%; max-height: 100%;'
                            ]);
                        } else {
                            echo '<img src="" class="signature_image">';
                        }; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4 d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1">
            <div class="p-3 d-flex flex-column flex-grow-1">
                <div class="label weight-700 mb-3">Подпись руководителя</div>
                <div class="flex-grow-1 d-flex flex-column justify-content-center">
                    <div class="flex-shrink-0 text-center">
                        <!-- <img src="images/kub-sign.png" alt=""> -->
                        <?php
                        $imgPath = $company->getImage('chiefSignatureImage');
                        if (is_file($imgPath)) {
                            echo EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET, [
                                'style' => 'max-width: 100%; max-height: 100%;'
                            ]);
                        } else {
                            echo '<img src="" class="signature_image">';
                        }; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
