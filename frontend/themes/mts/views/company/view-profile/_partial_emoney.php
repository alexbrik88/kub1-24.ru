<?php

use frontend\models\EmoneySearch;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$searchModel = new EmoneySearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <div class="table-container">
                <?php \yii\widgets\Pjax::begin([
                    'id' => 'emoney-pjax-container',
                    'enablePushState' => false,
                ]); ?>

                    <?= $this->render('@frontend/views/emoney/_table', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]) ?>

                <?php \yii\widgets\Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>