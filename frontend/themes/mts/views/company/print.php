<?php
use common\models\Company;
use common\models\company\CheckingAccountant;

/** @var $this yii\web\View */
/** @var Company $company */

$this->title = $company->getTitle(true) . ' - Визитка компании';

$checkingAccountant = new CheckingAccountant;
$mainCheckingAccountant = $company->mainCheckingAccountant;

$this->registerJs('window.print();');
?>

<div class="page-content-in p-center pad-pdf-p">
    <h4 class="page-title">Визитка</h4>

    <h4>
        <b><?php echo $company->getTitle(true, true); ?></b>
    </h4>

    <table class="b-none">
        <tbody>
        <tr>
            <th class="text-left"><?= $company->getAttributeLabel('taxation_type_id'); ?>:</th>
            <td><?= $company->companyTaxationType->name; ?></td>
        </tr>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left">Юридический адрес:</th>
            <td><?= $company->getAddressLegalFull(); ?></td>
        </tr>
        <tr>
            <th class="text-left">Фактический адрес:</th>
            <td><?= $company->getAddressActualFull(); ?></td>
        </tr>
        </tbody>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('egrip'); ?>:</th>
                <td><?= $company->egrip; ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('inn'); ?>:</th>
                <td><?= $company->inn; ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('okved'); ?>:</th>
                <td><?= $company->okved; ?></td>
            </tr>
        <?php else: ?>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('ogrn'); ?>:</th>
                <td><?= $company->ogrn; ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('inn'); ?>:</th>
                <td><?= $company->inn; ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('kpp'); ?>:</th>
                <td><?= $company->kpp; ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('okved'); ?>:</th>
                <td><?= $company->okved; ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('rs'); ?>:</th>
            <td><?= $mainCheckingAccountant? $mainCheckingAccountant->rs: ''; ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('bank_name'); ?>:</th>
            <td><?= $mainCheckingAccountant? $mainCheckingAccountant->bank_name: ''; ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('ks'); ?>:</th>
            <td><?= $mainCheckingAccountant? $mainCheckingAccountant->ks: ''; ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('bik'); ?>:</th>
            <td><?= $mainCheckingAccountant? $mainCheckingAccountant->bik: ''; ?></td>
        </tr>

        <?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
            <tr>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            <?php if ($company->chief_post_name) : ?>
                <tr>
                    <th class="text-left"><?= mb_convert_case($company->chief_post_name, MB_CASE_TITLE, 'UTF-8'); ?>:</th>
                    <td><?= $company->getChiefFio(); ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <th class="text-left">Главный бухгалтер:</th>
                <td><?= $company->getChiefAccountantFio(); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left"><?= $company->getAttributeLabel('phone'); ?>:</th>
            <td><?= $company->phone; ?></td>
        </tr>

    </table>
</div>

