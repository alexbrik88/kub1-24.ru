<?php

use backend\models\Bank;
use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $form ActiveForm */
/* @var $backUrl string */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $checkingAccountant CheckingAccountant */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $submitAction string */

$params = !isset($cancelUrl) ? [
    'model' => $model,
    'ifns' => $ifns,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'banks' => $banks,
] : [
    'model' => $model,
    'ifns' => $ifns,
];

$view = $model->self_employed ? '_type_self' : (
    $model->company_type_id == CompanyType::TYPE_IP ?
    Company::$companyFormView[CompanyType::TYPE_IP] :
    Company::$companyFormView[CompanyType::TYPE_OOO]
);

$this->registerJs(<<<JS
$(document).on("change", "#company-has_chief_patronymic", function () {
    if ($(this).prop("checked")) {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .val("")
            .attr("readonly", true)
            .trigger("change");
        $(".field-company-chief_patronymic, .field-company-ip_patronymic")
            .removeClass("has-error")
            .find(".help-block").html("");
    } else {
        $("#company-chief_patronymic, #company-ip_patronymic")
            .attr("readonly", false);
    }
});
$(document).on("change", "input.has_depend", function (e) {
    console.log(this.checked);
    var depend = $($(this).data("target"));
    console.log($("input", depend));
    if (depend.length) {
        if (this.checked) {
            depend.collapse("show");
        } else {
            depend.collapse("hide");
            $("input", depend).prop("checked", false).uniform("refresh");
        }
    }
})
JS
);
?>

<div class="company-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'action' => isset($submitAction) ? $submitAction : ['update', 'backUrl' => isset($backUrl) ? $backUrl : null],
        'options' => [
            'enctype' => 'multipart/form-data',
            'id' => 'form-update-company',
        ],
        'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
    ]); ?>
    <?= Html::activeHiddenInput($model, 'tmpId') ?>
    <?= Html::activeHiddenInput($model, 'self_employed') ?>
    <?= $form->field($model, 'hasAccountants')->label(false)->hiddenInput() ?>
    <?= $form->field($model, 'company_type_id')->label(false)->hiddenInput([
        'id' => 'company_form_input',
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $this->render($view, array_merge($params, [
        'form' => $form,
        'admin' => isset($admin) ? $admin : false,
        'creating' => isset($submitAction),
    ])); ?>

    <?php Modal::begin([
        'id' => 'modal-companyImages',
        'title' => 'Загрузить логотип, печать и подпись',
        'titleOptions' => [
            'class' => 'mb-0',
        ],
        'closeButton' => [
            'label' => Icon::get('close'),
            'class' => 'modal-close close',
        ],
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]) ?>

        <?= $this->render('_partial_files_tabs', [
            'model' => $model,
        ]) ?>

    <?php Modal::end() ?>

    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'button-width button-regular button-regular_red button-clr mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]) ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', isset($cancelUrl) ? $cancelUrl : ['index'], [
                    'class' => 'button-width button-regular button-regular button-hover-transparent undo-contractor',
                ]); ?>
            </div>
        </div>
    </div>

    <?php $form->end(); ?>
</div>

<?php if (!isset($cancelUrl)): ?>
    <?= $this->render('modal_rs/_create', [
        'checkingAccountant' => $checkingAccountant,
        'company' => isset($submitAction) ? $model : null,
    ]); ?>
    <div id="company_rs_update_form_list">
        <?= $this->render('_rs_update_form_list', [
            'model' => $model,
        ]); ?>
    </div>
    <?php foreach ($banks as $bank) {
        echo $this->render('modal_bank/_create', [
            'bank' => $bank,
            'applicationToBank' => $applicationToBank,
            'redirectUrl' => Url::to(''),
        ]);
    }; ?>
<?php endif; ?>

<?php $this->render('_company_inn_api'); ?>
