<?php
use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\Modal;

$fileTypeText = 'Форматы: JPG, JPEG, PNG.';
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Company */

$dataArray = Company::$imageDataArray;
?>

<div class="row" style="margin-bottom: 20px">
    <div class="col-4 d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1">
            <div class="p-3 d-flex flex-column flex-grow-1">
                <div class="label weight-700 mb-3">Логотип компании</div>
                <div class="flex-grow-1 d-flex flex-column justify-content-between">
                    <div class="flex-shrink-0">
                        <?php
                        $attr = 'logoImage';
                        $uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
                        $imgPath = $model->getImage('logoImage', true) ? : $model->getImage('logoImage');
                        ?>
                        <div id="logoImage-image-result" class="<?= !$imgPath ? 'hidden' : '' ?>">
                            <table class="company-image-preview">
                                <tr>
                                    <td>
                                        <?php
                                        if (is_file($imgPath)) : ?>
                                            <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                            <div class="del-link link del-company-image" data-attr="logoImage" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                        </div>
                        <div id="logoImage-image-upload" class="<?= $imgPath ? 'hidden' : '' ?>">
                            <div id="company_img_<?= $attr ?>" class="dz-upload-company-image dropzone mt-3 mb-1">
                                <div id="progress_<?= $attr ?>" class="progress progress-striped active" style="display: none">
                                    <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                                </div>
                            </div>
                            <div class="dz-upload-product-tip">
                                <?= $dataArray[$attr]['dummy_text'] ?>. <?= $fileTypeText ?>
                            </div>
                            <?= $this->render('_jsDropzone', [
                                'model' => $model,
                                'attr' => $attr,
                                'uploadUrl' => $uploadUrl
                            ])?>
                        </div>
                        <div style="display: none!important;">
                            <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteLogoImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                                'id' => 'logoImage-delete'
                            ]) : ''; ?>
                        </div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate): ?>
                            <div id="logoImage-modification-date" class="text-grey text-left">
                                Дата добавления<br>
                                <?= date(DateHelper::FORMAT_USER_DATE, $modificationDate); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$model->self_employed) : ?>
        <div class="col-4 d-flex flex-column">
            <div class="wrap p-3 pb-0 d-flex flex-column flex-grow-1">
                <div class="p-3 d-flex flex-column flex-grow-1">
                    <div class="label weight-700 mb-3">Печать компании</div>
                    <div class="flex-grow-1 d-flex flex-column justify-content-between">
                        <div class="flex-shrink-0">
                            <?php
                            $attr = 'printImage';
                            $uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
                            $imgPath = $model->getImage('printImage', true) ? : $model->getImage('printImage');
                            ?>
                            <div id="printImage-image-result" class="<?= !$imgPath ? 'hidden' : '' ?>">
                                <table class="company-image-preview">
                                    <tr>
                                        <td>
                                            <?php
                                            if (is_file($imgPath)) : ?>
                                                <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                                <div class="del-link link del-company-image" data-attr="printImage" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                            </div>
                            <div id="printImage-image-upload" class="<?= $imgPath ? 'hidden' : '' ?>">
                                <div id="company_img_<?= $attr ?>" class="dz-upload-company-image dropzone mt-3 mb-1">
                                    <div id="progress_<?= $attr ?>" class="progress progress-striped active" style="display: none">
                                        <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                                    </div>
                                </div>
                                <div class="dz-upload-product-tip">
                                    <?= $dataArray[$attr]['dummy_text'] ?>. <?= $fileTypeText ?>
                                </div>
                                <?= $this->render('_jsDropzone', [
                                    'model' => $model,
                                    'attr' => $attr,
                                    'uploadUrl' => $uploadUrl
                                ])?>
                            </div>
                            <div style="display: none!important;">
                                <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deletePrintImage', [
                                    'label' => 'Удалить',
                                    'class' => 'radio-inline p-o radio-padding',
                                    'id' => 'printImage-delete'
                                ]) : ''; ?>
                            </div>
                            <?php
                            $modificationDate = ImageHelper::getModificationDate($imgPath);
                            if ($modificationDate): ?>
                                <div id="printImage-modification-date" class="text-grey text-left">
                                    Дата добавления<br>
                                    <?= date(DateHelper::FORMAT_USER_DATE, $modificationDate); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
    <div class="col-4 d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1">
            <div class="p-3 d-flex flex-column flex-grow-1">
                <div class="label weight-700 mb-3">Подпись руководителя</div>
                <div class="flex-grow-1 d-flex flex-column justify-content-between">
                    <div class="flex-shrink-0">
                        <?php
                        $attr = 'chiefSignatureImage';
                        $uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
                        $imgPath = $model->getImage('chiefSignatureImage', true) ? : $model->getImage('chiefSignatureImage');
                        ?>
                        <div id="chiefSignatureImage-image-result" class="<?= !$imgPath ? 'hidden' : '' ?>">
                            <table class="company-image-preview">
                                <tr>
                                    <td>
                                        <?php
                                        if (is_file($imgPath)) : ?>
                                            <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                            <div class="del-link link del-company-image" data-attr="chiefSignatureImage" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                        </div>
                        <div id="chiefSignatureImage-image-upload" class="<?= $imgPath ? 'hidden' : '' ?>">
                            <div id="company_img_<?= $attr ?>" class="dz-upload-company-image dropzone mt-3 mb-1">
                                <div id="progress_<?= $attr ?>" class="progress progress-striped active" style="display: none">
                                    <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                                </div>
                            </div>
                            <div class="dz-upload-product-tip">
                                <?= $dataArray[$attr]['dummy_text'] ?>. <?= $fileTypeText ?>
                            </div>
                            <?= $this->render('_jsDropzone', [
                                'model' => $model,
                                'attr' => $attr,
                                'uploadUrl' => $uploadUrl
                            ])?>
                        </div>
                        <div style="display: none!important;">
                            <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteChiefSignatureImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                                'id' => 'chiefSignatureImage-delete'
                            ]) : ''; ?>
                        </div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate): ?>
                            <div id="chiefSignatureImage-modification-date" class="text-grey text-left">
                                Дата добавления<br>
                                <?= date(DateHelper::FORMAT_USER_DATE, $modificationDate); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-3">
    <div class="pl-1 pr-1">
        Не получается загрузить логотип, печать или подпись?<br>
        Пришлите нам ваши файлы на
        <a href="mailto:<?= \Yii::$app->params['emailList']['support'] ?>"><?= \Yii::$app->params['emailList']['support'] ?></a>
        и мы самостоятельно загрузим их.
    </div>
</div>

<?php Modal::begin(['id' => 'delete-company-image']); ?>
<h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить <span></span>?</h4>
<div class="text-center">
    <?= Html::button('Да', [
        'class' => 'del-company-image-ok button-clr button-regular button-hover-transparent button-width-medium mr-2',
        'data-attr' => ''
    ]); ?>
    <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
</div>
<?php Modal::end(); ?>

<?php $this->render('_company_update_files'); ?>

<script>
    var tempCompanyImages = [];
    function tempImagesAdd(attr, img) {
        var needToAdd = true;
        $.each(tempCompanyImages, function(i,v) {
            if (v.attr == attr) {
                needToAdd = false;
                return false;
            }
        });
        if (needToAdd) {
            tempCompanyImages.push({'attr': attr, 'img': img});
        }
    }
    function tempImagesClear() {
        tempCompanyImages = [];
    }
    function tempImagesDeleteMTS() {
        $.each(tempCompanyImages, function(i,v) {
            jQuery.post('/company/img-delete', {'id': '<?= $model->id ?>', 'attr': v.attr}, function (data) {});
        });
        $('#modal-companyImages, #modal-chiefAccountantSignatureImage').modal('hide');
    }
    function tempImagesAcceptMTS() {
        $.each(tempCompanyImages, function(i,v) {
            $('#' + v.attr + '-image-upload').addClass('hidden');
            $('#' + v.attr + '-image-result td').html(v.img);
            $('#' + v.attr + '-image-result').removeClass('hidden');
            if ($('#'+v.attr+'-delete').length) {
                $('#'+v.attr+'-delete').prop('checked', false);
            }
        });
        $('#modal-companyImages, #modal-chiefAccountantSignatureImage').modal('hide');
    }

    function pjaxImgFormMTS(pjaxUrl, companyId, attr, action, apply) {
        apply = apply || 0;
        jQuery.pjax({
            url: pjaxUrl,
            data: {'id': companyId, 'attr': attr, 'action': action, 'apply': apply},
            container: '#' + attr + '-pjax-container',
            timeout: 5000,
            push: false,
            scrollTo: false
        });

        if (action == 'view') {
            $(document).on('pjax:complete', function () {
                var img = $('#' + attr + '-image-edit').find('.company-image-preview td').html();
                tempImagesAdd(attr, img);
            });
        }
    }
    function jcropImageDeleteMTS(pjaxUrl, formUrl, companyId, attr) {
        jQuery.post(pjaxUrl, {'id': companyId, 'attr': attr}, function (data) {
            pjaxImgFormMTS(formUrl, companyId, attr, 'view');
        });
    }
    function jcrop_ajaxRequestMTS(pjaxUrl, formUrl, companyId, attr, el) {
        var hasApply = el !== undefined;
        var tab = hasApply ? $(el).closest('.jcrop-image-container') : null;
        var apply = tab !== null ? $('input.apply-image-radio:checked', tab).val() : '0';
        var ajaxData = {};
        ajaxData[attr + '_x'] = $('#' + attr + '_x').val();
        ajaxData[attr + '_x2'] = $('#' + attr + '_x2').val();
        ajaxData[attr + '_y'] = $('#' + attr + '_y').val();
        ajaxData[attr + '_y2'] = $('#' + attr + '_y2').val();
        ajaxData[attr + '_h'] = $('#' + attr + '_h').val();
        ajaxData[attr + '_w'] = $('#' + attr + '_w').val();
        ajaxData['id'] = companyId;
        ajaxData['attr'] = attr;
        ajaxData['width'] = $('#' + attr).width();
        ajaxData['apply'] = apply;
        if ($('#company-tmpid').lenght) {
            ajaxData['tmpId'] = $('#company-tmpid').val();
        }
        jQuery.ajax({
            type: 'post',
            url: pjaxUrl,
            data: ajaxData,
            success: function (data) {
                pjaxImgFormMTS(formUrl, companyId, attr, 'view', apply);
            }
        });
    }

    $(document).on('click', '.del-company-image', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        var itemName;
        if (attr == 'logoImage')
            itemName = 'логотип';
        else if (attr == 'printImage')
            itemName = 'печать компании';
        else if (attr == 'chiefSignatureImage')
            itemName = 'подпись руководителя';
        else if (attr == 'chiefAccountantSignatureImage')
            itemName = 'подпись бухгалтера';
        else
            itemName = 'изображение';

        delModal.find('.modal-title > span').html(itemName);
        delModal.find('.del-company-image-ok').data('attr', attr);
        delModal.modal('show');

        return false;
    });

    $(document).on('click', '.del-company-image-ok', function() {
        var attr = $(this).data('attr');
        var delModal = $('#delete-company-image');
        if (attr) {
            $('#' + attr + '-image-result').addClass('hidden');
            $('#' + attr + '-image-upload').removeClass('hidden');
            // form input
            if ($('#' + attr + '-delete').length) {
                $('#' + attr + '-delete').prop('checked', true);
            }
            $('#' + attr + '-modification-date').remove();
        }
        delModal.modal('hide');
    });

    $(document).on('hide.bs.modal', '#modal-companyImages, #modal-chiefAccountantSignatureImage', function() {
        $('.progress-bar').width(0);
        $('.progress-bar').parent().hide();
        tempImagesClear();
    });

    /* DRAG */
    function dragStart() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#e8eaf7', 'border-color': '#0048aa'});
        $(blocks).find('.dz-message').html('<span>Перетащите файл сюда.</span>');
    }
    function dragEnd() {
        var blocks = $(".dz-upload-company-image");
        $(blocks).css({'background-color': '#fff', 'border-color': '#ddd'});
        $(blocks).find('.dz-message').html('<span><span class="link">Выберите файлы</span><br> или перетащите сюда.</span>');
    }

    $(document).on("dragstart, dragover", function (e) {
        dragStart();
    });
    $(document).on("dragend, dragleave", function (e) {
        dragEnd();
    });

</script>