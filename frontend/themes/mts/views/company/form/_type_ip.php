<?php
use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $creating boolean */

$templateDateInput = "{label}<div class='date-picker-wrap' style=''>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-address_legal_city, #company-address_legal_street", function () {
            var city = $.trim($("#company-address_legal_city").val());
            var street = $.trim($("#company-address_legal_street").val());
            if (!empty(city) && !empty(street)) {
                $.post("/company/ifns", {"city": city, "street": street}, function(data) {
                    if (!empty(data.oktmo)) {
                        $("#company-oktmo").val(data.oktmo);
                    }
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>

<div class="wrap pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'inn')->textInput([
                    'placeholder' => 'Автозаполнение по ИНН',
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>

        <h4 class="mb-3" style="padding-bottom: 3px">Индивидуальный предприниматель</h4>

        <div class="row align-items-center">
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
            </div>
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
            </div>
            <div class="col-3 mb-3">
                <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
            </div>
            <div class="col-3 mb-3" style="margin-top:10px;">
                <div class="form-filter">
                    <?= Html::activeCheckbox($model, 'has_chief_patronymic', [
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>

        <?= $this->render('_partial_description', [
            'model' => $model,
            'form' => $form,
            'creating' => $creating,
        ]); ?>
    </div>
</div>

<?= $this->render('_partial_tabs', [
    'model' => $model,
    'form' => $form,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'banks' => $banks,
]) ?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <h4 class="">Реквизиты</h4>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'egrip')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okved')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okpo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'oktmo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_legal')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_actual')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'fss')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr_ip')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr_employer')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okud')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ip_certificate_number')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'certificateDate', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker',
                ])->label('Дата свидетельства') ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'ip_certificate_issued_by')->textInput([
                    'maxlength' => true,
                ])->label('Кем выдано свидетельство'); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?php if (empty($model->tax_authority_registration_date)) : ?>
                    <?= $form->field($model, 'tax_authority_registration_date', [
                        'template' => $templateDateInput,
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'value' => DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]) ?>
                <?php else : ?>
                    <?= $form->field($model, 'tax_authority_registration_date')->textInput([
                        'readonly' => true,
                        'value' => DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]) ?>
                <?php endif ?>
            </div>
        </div>
        <br>
    </div>
</div>

<?= $this->render('_partial_ifns', [
    'form' => $form,
    'model' => $model,
    'ifns' => $ifns,
]) ?>

<?= $this->render('_partial_files', [
    'model' => $model,
]); ?>
