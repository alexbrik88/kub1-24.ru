<?php
$fileTypeText = 'Форматы: JPG, JPEG, PNG.';
$dummyText = "<span class=\"link\">Выберите файлы</span><br/> или перетащите сюда.";
$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';
$tabs = [
    'logoImage' => 0,
    'printImage' => 1,
    'chiefSignatureImage' => 2
];
?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzone_<?= $attr ?> = new Dropzone("#company_img_<?= $attr ?>", {
        'paramName': '<?= $attr ?>',
        'url': '<?= $uploadUrl ?>',
        'dictDefaultMessage': '<?= $dummyText ?>',
        'dictInvalidFileType': '<?= $fileTypeText ?>',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png",
        'params': {'<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
        'previewTemplate': "<?= $DZpreviewTemplate ?>"
    });
    dropzone_<?= $attr ?>.on('thumbnail', function (f, d) {
        $('#progress_<?= $attr ?>').show();
    });
    dropzone_<?= $attr ?>.on('success', function (f, d) {

        <?php if ($attr == 'chiefAccountantSignatureImage'): ?>
            $('#modal-chiefAccountantSignatureImage').modal('show');
        <?php else: ?>
            $('#company-image-tabs a[href="#company-image-tabs-tab<?=$tabs[$attr]?>"]').tab('show');
            $('#modal-companyImages').modal('show');
        <?php endif; ?>

        pjaxImgFormMTS('/company/img-form', '<?= $model->id ?>', '<?= $attr ?>', 'crop');
        this.removeAllFiles(true);
        dragEnd();
    });
    dropzone_<?= $attr ?>.on('error', function (f, d) {
        alert(d);
        this.removeAllFiles(true);
        $('#progress_<?= $attr ?>').hide();
        $('#progress_bar_<?= $attr ?>').width(0);
        dragEnd();
    });
    dropzone_<?= $attr ?>.on('totaluploadprogress', function (progress) {
        $('#progress_bar_<?= $attr ?>').width(progress + '%');
    });
    dropzone_<?= $attr ?>.on("addedfile", function(f) {
        dragEnd();
    });
    dropzone_<?= $attr ?>.on("dragover", function(e) {
        dragStart();
    });
    dropzone_<?= $attr ?>.on("dragleave", function(e) {
        dragEnd();
    });
</script>