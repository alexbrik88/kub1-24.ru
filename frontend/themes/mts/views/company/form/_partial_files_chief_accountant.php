<?php

use common\assets\JCropAsset;
use common\models\Company;
use devgroup\dropzone\DropZoneAsset;
use yii\bootstrap\Tabs;
use yii\widgets\ActiveForm;

/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Company */

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
JCropAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

if (empty($action)) {
    $action = 'crop';
}

$fileTypeText = 'Форматы: JPG, JPEG, PNG.';
$dataArray = Company::$imageDataArray;
?>


<h4 class="modal-title">Загрузить подпись главного бухгалтера</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<?= $this->render('_partial_files_tabs_item', [
    'model' => $model,
    'attr' => 'chiefAccountantSignatureImage',
    'action' => $action,
]); ?>

