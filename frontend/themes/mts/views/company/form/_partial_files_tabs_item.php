<?php

use common\components\image\EasyThumbnailImage;
use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $attr styring */

$attrArray = Company::$imageAttrArray;
$dataArray = Company::$imageDataArray;

$imgPath = $model->getImage($attr, true);
if (!$imgPath) {
    $imgPath = $model->getImage($attr);
}
$fileTypeText = 'Форматы: JPG, JPEG, PNG.';
$uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
$width = $dataArray[$attr]['width'];
$height = $dataArray[$attr]['height'];
$dummyText = "<span class=\"link\">Выберите файлы</span><br> или перетащите сюда.";

$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';
?>

<?php $pjax = Pjax::begin([
    'id' => "{$attr}-pjax-container",
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'style' => 'position: relative;',
    ],
]); ?>
<?php if ($action === 'crop') : ?>
    <?= $this->render('_partial_files_crop', ['model' => $model, 'attr' => $attr]) ?>
<?php elseif ($action === 'upload' || !is_file($imgPath)) : ?>

    <div id="MODAL_company_img_<?= $attr ?>" class="dropzone dz-upload-company-image mt-3 mb-1" style="position: relative; padding-bottom: 30px;">
        <div id="MODAL_progress_<?= $attr ?>" class="progress progress-striped active"
             style="position: absolute; bottom: 10px; left: 10px; right: 10px; margin: 0; background-color: #fff; display: none;">
            <div id="MODAL_progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
        </div>
    </div>
    <div style="text-align: center;">
        <span class="label">
            <?= $dataArray[$attr]['dummy_text'] ?>. <?= $fileTypeText ?>
        </span>
    </div>
    <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var MODAL_dropzone_<?= $attr ?> = new Dropzone("#MODAL_company_img_<?= $attr ?>", {
            'paramName': '<?= $attr ?>',
            'url': '<?= $uploadUrl ?>',
            'dictDefaultMessage': '<?= $dummyText ?>',
            'dictInvalidFileType': '<?= $fileTypeText ?>',
            'maxFilesize': 5,
            'maxFiles': 1,
            'uploadMultiple': false,
            'acceptedFiles': "image/jpeg,image/png",
            'params': {'<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
            'previewTemplate': "<?= $DZpreviewTemplate ?>"
        });
        MODAL_dropzone_<?= $attr ?>.on('thumbnail', function (f, d) {
            $('#MODAL_progress_<?= $attr ?>').show();
        });
        MODAL_dropzone_<?= $attr ?>.on('success', function (f, d) {
            pjaxImgFormMTS('/company/img-form', '<?= $model->id ?>', '<?= $attr ?>', 'crop');
            dragEnd();
        });
        MODAL_dropzone_<?= $attr ?>.on('error', function (f, d) {
            alert(d);
            this.removeAllFiles(true);
            $('#MODAL_progress_<?= $attr ?>').hide();
            $('#MODAL_progress_bar_<?= $attr ?>').width(0);
            dragEnd();
        });
        MODAL_dropzone_<?= $attr ?>.on('totaluploadprogress', function (progress) {
            $('#MODAL_progress_bar_<?= $attr ?>').width(progress + '%');
        });
        MODAL_dropzone_<?= $attr ?>.on("dragover", function(e) {
            dragStart();
        });
        MODAL_dropzone_<?= $attr ?>.on("dragleave", function(e) {
            dragEnd();
        });
    </script>

<?php else : ?>
    <div id="<?= $attr ?>-image-edit" class="mt-3" style="position: relative">
        <span id="del_<?= $attr; ?>" class="button-list button-hover-transparent button-clr" title="Редактировать"
              onclick="pjaxImgFormMTS('/company/img-form', '<?= $model->id; ?>', '<?= $attr; ?>', 'upload')"
              style="position: absolute;right: 0; top: 0;z-index: 11;">
            <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
        </span>
        <table class="company-image-preview">
            <tr>
                <td style="padding: 15px; border: 1px solid #e7eafa; height: 300px;">
                    <?php if (is_file($imgPath)) : ?>
                        <?= EasyThumbnailImage::thumbnailImg($imgPath, $width, $height, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                    <?php else : ?>
                        <div class="col-12">
                            <div class="portlet align-center company_img_dummy"><?= $dataArray[$attr]['dummy_text'] ?></div>
                        </div>
                    <?php endif; ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="mt-4 d-flex justify-content-between">
        <?= Html::button('Сохранить', [
            'class' => "button-regular button-width button-regular_red",
            'onclick' => "tempImagesAcceptMTS()",
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => "button-regular button-width button-regular button-hover-transparent",
            'onclick' => "tempImagesDeleteMTS()",
        ]) ?>
    </div>
<?php endif; ?>
<?php $pjax->end(); ?>