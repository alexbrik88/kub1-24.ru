<?php

use common\models\company\CheckingAccountant;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $checkingAccountantForm yii\bootstrap\ActiveForm */
/* @var $company Company|null */

$checkingAccountantForm = ActiveForm::begin([
    'action' => $checkingAccountant->isNewRecord ? Url::to(['create-checking-accountant', 'companyId' => $company ? $company->id : null]) : Url::to(['update-checking-accountant', 'id' => $checkingAccountant->id, 'companyId' => $company ? $company->id : null]),
    'options' => [
        'class' => 'form-checking-accountant',
        'id' => 'form-checking-accountant-' . $checkingAccountant->id,
        'is_new_record' => $checkingAccountant->isNewRecord ? 1 : 0,
    ],
    'enableClientValidation' => true,
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
    ],
]);
?>
<div class="form-body">
    <div class="row">
        <div class="col-6">
            <?= $checkingAccountantForm->field($checkingAccountant, 'bik')
                ->widget(BikTypeahead::classname(), [
                    'remoteUrl' => Url::to(['/dictionary/bik']),
                    'related' => [
                        '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
                        '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
                        '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
                    ],
                ])->textInput(); ?>
        </div>
        <div class="col-6">
            <?= $checkingAccountantForm->field($checkingAccountant, 'bank_name')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $checkingAccountantForm->field($checkingAccountant, 'bank_city')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $checkingAccountantForm->field($checkingAccountant, 'ks')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $checkingAccountantForm->field($checkingAccountant, 'rs')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>

    <?php if ($checkingAccountant->type !== CheckingAccountant::TYPE_MAIN) : ?>
        <?= $checkingAccountantForm->field($checkingAccountant, 'isMain')->checkbox([], false); ?>

        <?php if (!$checkingAccountant->isNewRecord && count(Yii::$app->user->identity->company->checkingAccountants) !== 1) : ?>
            <?= $checkingAccountantForm->field($checkingAccountant, 'isClosed')->checkbox([], false) ?>
        <?php endif ?>
    <?php endif; ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'button-width button-regular button-regular_red button-clr',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-width button-clr button-regular button-hover-transparent back',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
</div>
<?php $checkingAccountantForm->end(); ?>
