<?php

use frontend\models\StoreSearch;
use frontend\themes\mts\components\Icon;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new StoreSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

$this->registerJs(<<<JS
$(document).on('submit', '#store-form', function(e){
    var ajaxModal = $('#ajax-modal-box');
    if (ajaxModal && $(this, ajaxModal).length) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#ajax-modal-content', ajaxModal).html(data);
        });
    }
});
JS
);
?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'store-pjax-container',
                'enablePushState' => false,
            ]); ?>

            <?= $this->render('//store/_table', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>

            <?php \yii\widgets\Pjax::end(); ?>

            <div class="form-group">
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить склад</span>', [
                    'class' => 'button-regular button-regular_red ajax-modal-btn',
                    'data-title' => 'Добавить склад',
                    'data-url' => Url::to(['/store/create']),
                ]); ?>
            </div>
        </div>
    </div>
</div>
