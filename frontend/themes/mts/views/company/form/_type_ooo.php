<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use common\components\TextHelper;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $admin boolean */
/* @var $creating boolean */

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-kpp", function () {
            var kpp = $.trim($(this).val());
            if (kpp.length == 9) {
                $.post("/company/ifns", {"kpp": $(this).val()}, function(data) {
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}

$templateDateInput = "{label}<div class='date-picker-wrap' style=''>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";
?>


<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-4">
                <?= $form->field($model, 'inn')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <?= $this->render('_partial_description', [
            'model' => $model,
            'form' => $form,
            'creating' => $creating,
        ]); ?>
    </div>
</div>

<?= $this->render('_partial_tabs', [
    'model' => $model,
    'form' => $form,
    'dataProvider' => $dataProvider,
    'searchModel' => $searchModel,
    'banks' => $banks,
]) ?>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-6">
                <h4 class="mb-3 pb-3">Руководитель (для документов)</h4>
                <?= $form->field($model, 'chief_post_name')->label('Должность')->textInput(); ?>
                <br>
                <?= $form->field($model, 'chief_lastname')->label('Фамилия')->textInput(); ?>
                <br>
                <?= $form->field($model, 'chief_firstname')->label('Имя')->textInput(); ?>
                <br>
                <?= $form->field($model, 'chief_patronymic')->label('Отчество')->textInput([
                    'readonly' => (boolean) $model->has_chief_patronymic,
                ]); ?>
                <br>
                <?= $form->field($model, 'has_chief_patronymic')->label('Нет отчества')->checkbox(); ?>
            </div>
            <div class="col-6">
                <h4 class="mb-3 pb-3">Главный бухгалтер (для документов)</h4>
                <div class="form-group" style="margin-top: 50px;margin-bottom: 42px;">
                    <label class="checkbox-inline match-with-leader">
                        <?= \yii\helpers\Html::activeCheckbox($model, 'chief_is_chief_accountant', [
                            'id' => 'chief_is_chief_accountant_input',
                            'label' => false,
                        ]); ?>
                        совпадает с руководителем
                    </label>
                </div>

                <div class="change-disabled-inputs">
                    <?= $form->field($model, 'chief_accountant_lastname')->label('Фамилия')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <br>
                    <?= $form->field($model, 'chief_accountant_firstname')->label('Имя')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <br>
                    <?= $form->field($model, 'chief_accountant_patronymic')->label('Отчество')->textInput([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <br>
                    <?= $form->field($model, 'has_chief_accountant_patronymic')->label('Нет отчества')->checkbox([
                        'disabled' => $model->chief_is_chief_accountant? true: false,
                    ]); ?>
                    <div class="clearfix"></div>
                </div>

                <div id="chiefAccountantSignature-container" class="row <?= $model->chief_is_chief_accountant ? 'hidden' : '' ?>">
                    <div class="col-6 label-width mb-3 pb-3">
                        <label class="label">
                            Подпись
                        </label>
                        <div class="flex-shrink-0">
                            <?php
                            $attr = 'chiefAccountantSignatureImage';
                            $uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
                            $imgPath = $model->getImage('chiefAccountantSignatureImage', true) ? : $model->getImage('chiefAccountantSignatureImage');
                            ?>
                            <div id="chiefAccountantSignatureImage-image-result" class="<?= !$imgPath ? 'hidden' : '' ?>">
                                <table class="company-image-preview">
                                    <tr>
                                        <td>
                                            <?php
                                            if (is_file($imgPath)) : ?>
                                                <?= EasyThumbnailImage::thumbnailImg($imgPath, 545, 185, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                                <div class="del-link link del-company-image" data-attr="chiefAccountantSignatureImage" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                            </div>
                            <div id="chiefAccountantSignatureImage-image-upload" class="<?= $imgPath ? 'hidden' : '' ?>">
                                <div id="company_img_<?= $attr ?>" class="dz-upload-company-image dropzone mb-1">
                                    <div id="progress_<?= $attr ?>" class="progress progress-striped active" style="display: none">
                                        <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                                    </div>
                                </div>
                                <div class="dz-upload-product-tip">
                                    <?= Company::$imageDataArray[$attr]['dummy_text'] ?>. <?= 'Форматы: JPG, JPEG, PNG.' ?>
                                </div>
                                <?= $this->render('_jsDropzone', [
                                    'model' => $model,
                                    'attr' => $attr,
                                    'uploadUrl' => $uploadUrl,
                                ])?>
                            </div>
                            <div style="display: none!important;">
                                <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteChiefAccountantSignatureImage', [
                                    'label' => 'Удалить',
                                    'class' => 'radio-inline p-o radio-padding',
                                    'id' => 'chiefAccountantSignatureImage-delete'
                                ]) : ''; ?>
                            </div>
                            <?php
                            $modificationDate = ImageHelper::getModificationDate($imgPath);
                            if ($modificationDate): ?>
                                <div id="chiefAccountantSignatureImage-modification-date" class="text-grey text-left">
                                    Дата добавления<br>
                                    <?= date(DateHelper::FORMAT_USER_DATE, $modificationDate); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        
                        <?php /*
                        <div>
                            <?= Html::button('Добавить подпись', [
                                'class' => 'button-regular button-regular_red',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-chiefAccountantSignatureImage',
                                'style' => 'margin-right: 20px;',
                            ]) ?>
                        </div>
                        */ ?>
                    </div>
                    <?php /*
                    <div class="col-6 field-width inp_one_line">
                        <?php $imgPath = $model->getImage('chiefAccountantSignatureImage'); ?>
                        <div id="chiefAccountantSignatureImage-image" class="<?= is_file($imgPath) ? '' : 'hidden'; ?>">
                            <div id="chiefAccountantSignatureImage-image-result">
                                <table class="company-image-preview">
                                    <tr>
                                        <td>
                                            <?php if (is_file($imgPath)) : ?>
                                                <?= EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <?= $form->field($model, 'deleteChiefAccountantSignatureImage')->checkbox([
                                    'class' => 'has_depend',
                                    'data-target' => '#deleteChiefAccountantSignatureImageAll_wrap',
                                ]); ?>
                            </div>
                            <div id="deleteChiefAccountantSignatureImageAll_wrap" class="collapse">
                                <?= $form->field($model, 'deleteChiefAccountantSignatureImageAll')->checkbox(); ?>
                            </div>
                        </div>
                    </div>
                    */ ?>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pl-1 pr-1">
        <h4 class="">Реквизиты</h4>
        <br>

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ogrn')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'kpp')->textInput([
                    'maxlength' => true,
                    'ifns-exist' => $model->getIfns()->exists() ? 'true' : 'false',
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okpo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okato')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_legal')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-9">
                <?= $form->field($model, 'address_actual')->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'fss')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'pfr')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okud')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okved')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'oktmo')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okogu')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okfs')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'okopf')->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-6">
                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', [
                    'template' => $templateDateInput,
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                ])->textInput([
                    'class' => 'form-control date-picker'
                ]) ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'capital')->textInput([
                    'maxlength' => true,
                    'value' => TextHelper::invoiceMoneyFormat($model->capital, 2, '.', ''),
                ]); ?>
            </div>
        </div>
        <br>

    </div>
</div>

<?= $this->render('_partial_ifns', [
    'form' => $form,
    'model' => $model,
    'ifns' => $ifns,
]) ?>

<?= $this->render('_partial_files', [
    'model' => $model,
]); ?>

<?php Modal::begin([
    'id' => 'modal-chiefAccountantSignatureImage',
    'title' => '',
    'toggleButton' => false,
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
    ],
]) ?>

    <?= $this->render('_partial_files_chief_accountant', [
        'model' => $model,
    ]) ?>

<?php Modal::end() ?>

<?php $this->registerJs('
    $(document).on("change", "#chief_is_chief_accountant_input", function () {
        $(".change-disabled-inputs input").prop("disabled", $(this).is(":checked"));
        $.uniform.update(".change-disabled-inputs input:checkbox");
        if ($(this).is(":checked")) {
            $("#chiefAccountantSignature-container").addClass("hidden");
        } else {
            $("#chiefAccountantSignature-container").removeClass("hidden");
        }
    });
') ?>
