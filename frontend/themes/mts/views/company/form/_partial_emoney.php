<?php

use frontend\models\EmoneySearch;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new EmoneySearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="row pb-2">
    <div class="pb-1 w-100">
        <div class="page-border page-border_grey p-0 pl-3 pr-3">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'emoney-pjax-container',
                'enablePushState' => false,
            ]); ?>

                <?= $this->render('//emoney/_table', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'actionVisible' => true,
                ]) ?>

            <?php \yii\widgets\Pjax::end(); ?>

            <div class="form-group">
                <?= Html::button(Icon::get('add-icon') . '<span>Добавить E-money</span>', [
                    'class' => 'button-regular button-regular_red ajax-modal-btn',
                    'data-title' => 'Добавить E-money',
                    'data-url' => Url::to(['/emoney/create']),
                ]); ?>
            </div>

            <div class="form-group">
                Для пользователей QIWI Касса
            </div>
            <?= $form->field($model, 'qiwi_public_key')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>
</div>
