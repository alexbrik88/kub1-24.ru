<style>
    .portlet-title {
        font-size: 20px;
        font-weight: bold;
        line-height: 24px;
        margin-bottom: 1rem;
    }
    .company-name {
        border-bottom: none;
    }

    .company-item {
        margin-bottom:10px;
    }
    .company-item__title {
        font-size: 16px;
        margin: 15px 0 5px 0;
    }
    .company-item-info {

    }
    .company-item-info dl {
        margin-bottom: 5px;
    }
    .tabs-line {
        margin-top: 15px;
        margin-bottom: 15px;
    }
    .tabs-line .btn-link,
    .sort .sort-item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        width: auto;
        padding: 3px 10px;
    }
    .tabs-line .btn-link:hover,
    .tabs-line .btn-link:focus,
    .tabs-line .btn-link:active {
        background-color: transparent;
    }
    .sort .sort-item-wrap {
        display: inline-block;
        vertical-align: top;
        position: relative;
        width: auto;
    }
    .sort-list, .sort__label {
        display: inline-block;
        vertical-align: middle;
    }
    .sort-list {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .rely-rating-badge {
        font-weight: bold;
    }
    .requisites-item__name {
        font-size: 16px;
        line-height: 19px;
    }
    .rely-short-negative {
        color: #c40000;
    }
    .rely-short-warning {
        color: #dca52a;
    }
    .rely-short-positive, .rely-rating-positive {
        color: #09c400;
    }
    .requisites-item {
        display: -webkit-flex;
        display: flex;
        padding: 5px 8px;
        margin: 0 0 0 -8px;
        -ms-align-items: flex-start;
        align-items: flex-start;
        line-height: 15px;
    }
    .requisites-item__name {
        flex: 0 0 364px;
        flex-basis: 364px;
        font-weight: 400;
        margin-right: 20px;
    }
    .requisites-item__value {
        flex: 1 1 auto;
        font-weight: 400;
    }
    .reliability-page .requisites-item {
        padding: 13px 8px 11px;
    }

    .finance-header {
        font-weight: 700;
        font-size: 16px;
        margin-bottom: 10px;
        margin-top: 15px;
    }
    .finance-list {
        display: -webkit-flex;
        display: flex;
        flex-wrap: wrap;
    }
    .fl-block {
        flex: 0 0 25%;
        padding: 0 8px 12px 0;
    }
    .fl-text .num {
        font-weight: 400;
        font-size: 18px;
    }
    .finance-changes {
        position: relative;
        color: #646e74;
        font-size: 11px;
        line-height: 16px;
        font-weight: 400;
    }
    .finance-changes.lt, .finance-changes.mt.reverted {
        color: #f91d0a;
        padding: 0 0 0 10px;
        font-size: 12px;
    }
    .finance-changes.lt.reverted, .finance-changes.mt {
        color: #61a517;
        padding: 0 0 0 10px;
        font-size: 12px;
    }

    .okved-list {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .okved-item {
        display: -webkit-flex;
        display: flex;
        padding: 5px 8px;
        margin: 0 0 0 -8px;
    }
    .okved-item__num {
        flex: 0 0 80px;
        min-width: 80px;
        max-width: 80px;
        font-weight: 400;
    }
    .okved-item__text {
        flex: 1 1 auto;
    }
    .okved-list + .okved-list-title {
        margin-top: 24px;
    }
    .okved-list-title {
        margin-bottom: 21px;
    }
    .tile-item__title {
        font-size: 18px;
        line-height: 24px;
        font-weight: 700;
        margin: 0 0 8px 0;
    }

    .filter-block__wrap {
        display: -webkit-flex;
        display: flex;
        align-items: center;
        -webkit-justify-content: space-between;
        justify-content: space-between;
        padding: 5px 0;
        flex-wrap: wrap;
    }
    .filter-block__frame {
        width: 946px;
        margin: 0 -36px 0 0;
    }
    .filter-block__item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        padding: 8px 0;
        margin: 0 36px 0 0;
    }
    .filter-block__label {
        line-height: 29px;
        margin: 2px 5px 0 0;
        vertical-align: top;
        display: inline-block;
    }
    .filter-block__item {
        display: inline-block;
        vertical-align: top;
        position: relative;
        padding: 8px 0;
        margin: 0 36px 0 0;
    }

    .company-item-info dl {
        display: inline-block;
        vertical-align: top;
        margin: 0;
        padding: 0;
        line-height: 15px;
    }
    .company-item-info dt {
        display: inline;
        font-size: 10px;
        margin: 0;
        padding: 0 4px 0 0;
        text-transform: uppercase;
        font-weight: 400;
    }
    .company-item-info dd {
        display: inline;
        font-size: 1rem;
        line-height: 15px;
        margin: 0 16px 0 0;
        padding: 0;
    }
    .license-name::before {
        content: "№ ";
        font-size: 1rem;
    }
    .license-name {
        font-size: 16px;
        line-height: 22px;
        font-weight: 400;
        margin: 0 0 7px;
    }

    .info-table--egrul {
        margin-top: 21px;
    }
    .info-table {
        width: 100%;
        table-layout: fixed;
        margin: 0;
        border-collapse: collapse;
        margin-top: 5px;
    }
    .info-table--egrul td:first-child {
        width: 1px;
        text-align: center;
    }
    .info-table td {
        padding: 4px 8px;
        vertical-align: top;
        line-height: 16px;
    }
    .info-table--egrul td:nth-child(2) {
        width: 113px;
        padding-left: 1rem;
    }

    .accounting-header__title {
        float: left;
    }
    .accounting-description {
        margin: 0 0 16px;
        font-weight: 400;
    }
    .accounting-table {
        margin: 0;
        border-collapse: collapse;
        table-layout: fixed;
        width: 100%;
    }
    .accounting-table td:first-child, .accounting-table th:first-child {
        width: 220px;
    }
    .accounting-table th {
        text-align: left;
        padding: 5px 8px;
        background: rgba(234,236,237,.6);
        vertical-align: top;
    }
    .accounting-table td:nth-child(2), .accounting-table th:nth-child(2) {
        width: 107px;
    }
    .accounting-table td.has-value.table-desktop, .accounting-table th.has-value.table-desktop {
        display: table-cell;
    }
    .accounting-table td.has-value, .accounting-table th.has-value {
        text-align: right;
        display: none;
    }
    .accounting-table-wrap + .tile-item__title {
        margin-top: 36px;
    }
    .accounting-table tr:nth-child(2n) {
        background: rgba(234,236,237,.6);
    }

    .tile-item {
        background: #fff;
        margin-bottom: 24px;
        overflow: hidden;
        position: relative;
    }
    .anketa-top {
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 16px;
    }
    .anketa-bottom {
        margin-top: 24px;
        padding-top: 16px;
        display: flex;
        justify-content: space-between;
        align-items: center;

    }
    .anketa-bottom .anketa-actual {
        flex: none;
        font-size: 1rem;
        font-weight: bold;
    }
    .anketa-bottom .flextext {
        flex: 1 1 auto;
        margin-left: 25px;
    }
    .clear::after {
        display: block;
        clear: both;
        content: "";
    }
    #anketa .leftcol {
        float: left;
        width: 58%;
    }
    #anketa .rightcol {
        float: right;
        width: 42%;
        padding-left: 24px;
        box-sizing: border-box;
    }
    .company-requisites .company-row:first-child, .rightcol .company-row:first-child {
        margin-top: 0;
    }
    .company-requisites .company-row {
        margin-top: 10px;
    }
    .company-row {
        margin: 16px 0 0;
    }
    .company-row::after {
        display: block;
        clear: both;
        content: "";
    }
    .company-col {
        width: calc(50% - 10px);
        float: left;
        margin: 0;
    }
    .company-info__text, .company-info__title {
        font-size: 1rem;
        display: block;
    }
    .company-info__title {
        font-weight: 400;
        margin: 0 0 5px;
        text-decoration: underline;
    }
    .company-info__text {
        padding: 0 11px;
        margin: 0 -11px;
        font-style: normal;
    }
    .company-col + .company-col {
        margin-left: 20px;
    }
    .rely-tile-badge {
        padding: 10px;
        border-radius: 4px;
        font-size: 20px;
        font-weight: 400;
        line-height: 20px;
        color: #fff;
        margin: 1px 0 17px;
        display: inline-block;
    }
    .rely-positive::before, .rely-rating-positive {
        background: #09c400;
    }
    .connexion > .connexion-col {
        min-width: 27%;
    }
    .connexion-col {
        display: inline-block;
        vertical-align: top;
        padding: 6px 30px 0 0;
        min-width: 156px;
        box-sizing: border-box;
    }
    .connexion-col__title {
        font-weight: 500;
        font-size: 1rem;
    }
    .connexion-col__num, .connexion-col__num.tosmall .num {
        font-weight: 600;
        font-size: 24px;
    }
    .see-details {
        display: inline-block;
        margin: 5px 0 4px;
        padding-right: 1rem;
        position: relative;
    }
    .text-dl dt {
        float: left;
        margin: 0 4px 0 0;
        padding: 0;
    }
    .text-dl dd {
        margin: 0;
        padding: 0;
    }
    .finance-columns {
        display: flex;
        margin: 0 -10px;
    }
    .finance-col {
        flex: 1 1 33.3%;
        box-sizing: border-box;
        padding: 0 10px 15px;
    }
    .finance-link {
        margin: 0 0 12px;
    }
    .btn-link.active, .fake-tab-opener.active, .tab-opener.active {
        border-color: #007bff;
        color: #007bff;
    }
    .tile-area .fake-tab-opener, .tile-area .tab-opener {
        padding: 3.5px 8px 2.5px;
    }
    .btn-link, .fake-tab-opener, .tab-opener {
        border: 1px solid transparent;
        line-height: 17px;
        font-size: 1rem;
        font-weight: 400;
        border-radius: 4px;
        padding: 2px 8px;
        color: #007bff;
        cursor: pointer;
        transition: border-color .2s;
        background: none;

    }
    .btn-link, .fake-tab-opener, .tab-control li, .tab-opener {
        display: inline-block;
        vertical-align: top;
    }
    .finance-data {
        margin: 0 0 7px;
    }
    .finance-data .num {
        font-size: 24px;
        font-weight: 500;
    }
</style>