<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Виды деятельности ОКВЭД (13)
        </div>
    </div>
    <div class="portlet-body accounts-list">


        <div class="tile-item__title okved-list-title">Основной</div>

        <ul class="okved-list">
            <li class="okved-item okved-item--main">
                <a href="/codes/690000" class="okved-item__num">69</a>
                <a href="/codes/690000" class="okved-item__text">Деятельность в области права и бухгалтерского учета</a>
            </li>
        </ul>

        <div class="tile-item__title okved-list-title">Дополнительные (12)</div>

        <ul class="okved-list">
            <li class="okved-item">
                <div class="okved-item__num">63.11</div>
                <div class="okved-item__text">Деятельность по обработке данных, предоставление услуг по размещению
                    информации и связанная с этим деятельность
                </div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">69.10</div>
                <div class="okved-item__text">Деятельность в области права</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">69.20</div>
                <div class="okved-item__text">Деятельность по оказанию услуг в области бухгалтерского учета, по
                    проведению финансового аудита, по налоговому консультированию
                </div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">69.20.1</div>
                <div class="okved-item__text">Деятельность по проведению финансового аудита</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">69.20.2</div>
                <div class="okved-item__text">Деятельность по оказанию услуг в области бухгалтерского учета</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">70.22</div>
                <div class="okved-item__text">Консультирование по вопросам коммерческой деятельности и управления</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">73.1</div>
                <div class="okved-item__text">Деятельность рекламная</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">73.20</div>
                <div class="okved-item__text">Исследование конъюнктуры рынка и изучение общественного мнения</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">74.30</div>
                <div class="okved-item__text">Деятельность по письменному и устному переводу</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">78.1</div>
                <div class="okved-item__text">Деятельность агентств по подбору персонала</div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">82.19</div>
                <div class="okved-item__text">Деятельность по фотокопированию и подготовке документов и прочая
                    специализированная вспомогательная деятельность по обеспечению деятельности офиса
                </div>
            </li>
            <li class="okved-item">
                <div class="okved-item__num">82.99</div>
                <div class="okved-item__text">Деятельность по предоставлению прочих вспомогательных услуг для бизнеса,
                    не включенная в другие группировки
                </div>
            </li>
        </ul>


    </div>
</div>
