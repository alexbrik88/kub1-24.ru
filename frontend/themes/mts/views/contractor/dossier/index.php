<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="row">
    <div class="col-sm-12">
        <div class="wrap">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СМАРТ+"
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <div class="company-info" id="anketa">
                    <div class="anketa-top">
                        <div class="leftcol">
                            <div class="company-status active-yes">Действующая организация</div>
                        </div>
                    </div>
                    <div class="clear">
                        <div class="leftcol">
                            <div class="company-requisites">
                                <div class="company-row">
                                    <dl class="company-col">
                                        <dt class="company-info__title">ОГРН</dt>
                                        <dd class="company-info__text has-copy">
                                            <span class="copy_target" id="clip_ogrn">1127747224549</span>
                                            <span class="copy_button clipper" data-clipboard-target="#clip_ogrn"
                                                  data-quetip="Скопировано" data-quetime="500"></span>
                                        </dd>
                                        <dd class="company-info__text"> от 7 декабря 2012 г.</dd>
                                    </dl>
                                    <dl class="company-col">
                                        <dt class="company-info__title">ИНН/КПП</dt>
                                        <dd class="company-info__text has-copy" itemprop="taxID">
                                            <span class="copy_target" id="clip_inn">7705527945</span>
                                            <span class="copy_button clipper" data-clipboard-target="#clip_inn"
                                                  data-quetip="Скопировано" data-quetime="500"></span>
                                        </dd>
                                        <dd class="company-info__text has-copy">
                                            <span class="copy_target" id="clip_kpp">770501001</span>
                                            <span class="copy_button clipper" data-clipboard-target="#clip_kpp"
                                                  data-quetip="Скопировано" data-quetime="500"></span>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="company-row">
                                    <dl class="company-col">
                                        <dt class="company-info__title">Дата регистрации</dt>
                                        <dd class="company-info__text" itemprop="foundingDate">07.12.2012</dd>
                                    </dl>
                                    <dl class="company-col">
                                        <dt class="company-info__title">Уставный капитал</dt>
                                        <dd class="company-info__text">
                                            <span class="copy_target">20 000 руб.</span>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="company-row company-info__all">
                                    <a href="#" class="gtm_main_requisites" rel="nofollow">Все
                                        реквизиты</a>
                                    <span class="small">(ФНС / ПФР / ФСС / РОССТАТ)</span>
                                </div>
                            </div>
                            <div class="company-row">
                                <span class="company-info__title">Юридический адрес</span>
                                <address class="company-info__text" itemprop="address" itemscope=""
                                         itemtype="https://schema.org/PostalAddress">
                                                            <span itemprop="postalCode">
                                        115093</span>, <span itemprop="addressRegion">
                                        город Москва</span>, <span itemprop="streetAddress">
                                        улица Павла Андреева, 4, 24</span>
                                    <meta itemprop="addressCountry" content="RU">
                                    <meta itemprop="addressLocality" content="город Москва">
                                </address>
                            </div>
                            <div class="company-row hidden-parent">
                                <span class="company-info__title">Руководитель</span>
                                <span class="chief-title">Генеральный директор</span>
                                <span class="company-info__text"><a href="/person/kushhenko-av-770304445862"
                                                                    class="link-arrow gtm_main_fl"><span>Кущенко Алексей Владимирович</span></a></span>
                                <span class="chief-title">с 7 декабря 2012 г.</span>
                            </div>
                            <dl class="company-row">
                                <dt class="company-info__title">Среднесписочная численность <span
                                            data-quetip="<b>Источник</b>: реестр &quot;Сведения о среднесписочной численности работников организации&quot; ФНС РФ<br>Данные за 2018 год"
                                            class="tooltip-opener quetip queimg"></span></dt>
                                <dd class="company-info__text">25 сотрудников (2018)</dd>
                            </dl>
                            <dl class="company-row">
                                <dt class="company-info__title">Специальный налоговый режим <span
                                            data-quetip="<b>Источник</b>: реестр &quot;Сведения о специальных налоговых режимах, применяемых налогоплательщиками&quot; ФНС РФ<br>Данные за 2018 год"
                                            class="tooltip-opener quetip queimg"></span></dt>
                                <dd class="company-info__text">
                                    УСН (2018) <span data-quetip="Упрощенная система налогообложения"
                                                     class="tooltip-opener quetip queimg"></span></dd>
                            </dl>
                            <div class="company-row">
                                <span class="company-info__title">Реестр МСП<span class="quetip queimg tooltip-opener"
                                                                                  data-quetip="<b>Источник</b>: Единый реестр субъектов малого и среднего предпринимательства ФНС РФ. Данные за 2019 год">&nbsp;</span></span>
                                <span class="company-info__text">Статус: малое предприятие</span>
                                <span class="chief-title">присвоен 1 августа 2016 г.</span>
                            </div>
                        </div>
                        <div class="rightcol">
                            <div class="company-row">
                                <span class="company-info__title">Основной вид деятельности</span>
                                <span class="company-info__text">
                                                        Деятельность в области права и бухгалтерского учета
                                                        <span class="bolder">(69)</span>
                                                    </span>
                                <a href="#" class="gtm_main_okved" rel="nofollow">Все виды деятельности (13)</a>
                            </div>
                            <div class="company-row">
                                <span class="company-info__title">Налоговый орган</span>
                                <span class="company-info__text">ИНСПЕКЦИЯ ФЕДЕРАЛЬНОЙ НАЛОГОВОЙ СЛУЖБЫ № 5 ПО Г. МОСКВЕ</span>
                                <span class="chief-title">с 7 декабря 2012 г.</span>
                            </div>
                            <dl class="company-row">
                                <dt class="company-info__title">Коды статистики</dt>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКПО</span>
                                    <span class="copy_target" id="clip_okpo">13163904</span>
                                    <span class="copy_button clipper" data-clipboard-target="#clip_okpo"
                                          data-quetip="Скопировано" data-quetime="500"></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКАТО</span>
                                    <span class="copy_target" id="clip_okato">45286560000</span>
                                    <span class="copy_button clipper" data-clipboard-target="#clip_okato"
                                          data-quetip="Скопировано" data-quetime="500"></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКОГУ</span>
                                    <span class="copy_target" id="clip_okogu">4210014</span>
                                    <span class="copy_button clipper" data-clipboard-target="#clip_okogu"
                                          data-quetip="Скопировано" data-quetime="500"></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКТМО</span>
                                    <span class="copy_target" id="clip_oktmo">45376000000</span>
                                    <span class="copy_button clipper" data-clipboard-target="#clip_oktmo"
                                          data-quetip="Скопировано" data-quetime="500"></span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div class="company-info__intelligence">
                        По организации доступны <a href="#" class="gtm_main_history" rel="nofollow">исторические
                            сведения</a> (10 изменений).
                    </div>
                    <div class="anketa-bottom">
                        <div class="anketa-actual">Актуально на 13.12.2019</div>
                        <div class="flextext cutted">
                            <p class="information-text">
                                ООО "Смарт+" ИНН 7705527945 ОГРН 1127747224549 зарегистрировано 07.12.2012 по
                                юридическому адресу <span>
                                    115093</span>, <span>
                                    город Москва</span>, <span>
                                    улица Павла Андреева, 4, 24</span>.
                                Статус организации: действующая.
                                Руководителем является
                                генеральный директор Кущенко Алексей Владимирович (ИНН 770304445862). Размер уставного
                                капитала - 20 000 рублей. <span class="anketa-clip-opener">Подробнее &gt;</span>
                            </p>
                            <div class="anketa-clip">
                                <p class="information-text">
                                    В выписке из ЕГРЮЛ в качестве учредителя указано 1 физическое лицо. Основной вид
                                    деятельности - Деятельность в области права и бухгалтерского учета, также указано 12
                                    дополнительных видов.
                                    Организация присутствует в реестре Малого и среднего бизнеса (МСП) как малое
                                    предприятие с 1 августа 2016 г..
                                    В исторических сведениях доступно 10 записей об изменениях, последнее изменение
                                    датировано 6 сентября 2017 г..
                                </p>
                                <p class="information-text">
                                    Организация состоит на учете в налоговом органе ИНСПЕКЦИЯ ФЕДЕРАЛЬНОЙ НАЛОГОВОЙ
                                    СЛУЖБЫ № 5 ПО Г. МОСКВЕ с 7 декабря 2012 г., присвоен КПП 770501001.
                                    Регистрационный номер в
                                    ПФР - 087105088875, ФСС - 773120053877201. </p>
                                <p class="information-text">
                                    Информации об участии ООО "Смарт+" в тендерах не найдено.
                                    Есть данные об участии организации в
                                    2 завершенных
                                    арбитражных делах.
                                    <span class="anketa-clip-closer">&lt; Свернуть</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Надежность
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <a href="#" rel="nofollow" class="rely-tile-badge rely-rating-positive">Высокая</a>
                <p class="tile-item__text">Выявлен 31 факт об организации:</p>
                <div class="connexion">
                    <div class="connexion-col">
                        <div class="connexion-col__title">Положительных</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow" href="#">21</a>
                        </div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">Отрицательных</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow" href="#">3</a>
                        </div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">Требующих внимания</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow" href="#">7</a>
                        </div>
                    </div>
                </div>
                <a href="#" class="see-details" rel="nofollow">Подробнее о рейтинге и фактах</a>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Связи
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">
                    Выявлено 8 связанных c ООО "Смарт+" организаций.
                </p>
                <div class="connexion">
                    <div class="connexion-col">
                        <div class="connexion-col__title">Всего</div>
                        <div class="connexion-col__num"><a class="num gtm_c_all" href="#"
                                                           rel="nofollow">8</a></div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">По адресу</div>
                        <div class="connexion-col__num"><a class="num gtm_c_1" href="#"
                                                           rel="nofollow">4</a></div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">По руководителю</div>
                        <div class="connexion-col__num"><a class="num gtm_c_2" href="#"
                                                           rel="nofollow">8</a></div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">По учредителю</div>
                        <div class="connexion-col__num"><a class="num gtm_c_3"
                                                           href="#"
                                                           rel="nofollow">8</a></div>
                    </div>
                </div>
                <a href="#" class="see-details gtm_c_more" rel="nofollow">Все связи</a>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Учредители
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">
                    Согласно данным ЕГРЮЛ учредителем ООО "Смарт+" является
                    1 физическое лицо: </p>
                <div class="founder-item">
                    <div class="founder-item__title">
                        <a href="/person/kushhenko-av-770304445862" class="link-arrow gtm_f_list"><span>Кущенко Алексей Владимирович</span></a>
                    </div>
                    <dl class="founder-item__dl">
                        <dt>Доля:</dt>
                        <dd>20 000 руб.<span class="percent">(100%)</span></dd>
                        <dt>ИНН:</dt>
                        <dd>770304445862</dd>
                    </dl>
                </div>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Судебные дела
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">Имеются данные о 2 завершенных судебных делах с участием ООО "Смарт+":</p>
                <dl class="text-dl">
                    <dt>В качестве истца:</dt>
                    <dd>2</dd>
                </dl>
                <a class="see-details gtm_ar_more" href="#" rel="nofollow">Все судебные дела</a>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Гос. закупки
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">Сведения об участии ООО "Смарт+" в госзакупках в качестве поставщика или
                    заказчика по 44-ФЗ, 94-ФЗ и 223-ФЗ отсутствуют.</p>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Проверки
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">Данных о проведении в отношении ООО "Смарт+" плановых и внеплановых проверок
                    нет.</p>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Филиалы и представительства
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">Сведения о филиалах и представительствах ООО "Смарт+" отсутствуют.</p>
            </div>
        </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Финансы
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <div style="width: 100%">
                    <img src="/img/contractor_dosier_temp_chart.png"/>
                </div>
                <div class="finance-columns tab-control">
                    <div class="finance-col">
                        <div class="finance-link">
                            <div class="tab-opener gtm_acc_1 active">Выручка</div>
                        </div>
                        <div class="finance-data"><span class="num">30</span> млн
                            руб.
                        </div>
                        <div class="finance-changes lt">
                            - <span class="num">8,1</span> млн
                            руб.
                        </div>
                    </div>
                    <div class="finance-col">
                        <div class="finance-link">
                            <div class="tab-opener gtm_acc_2">Прибыль</div>
                        </div>
                        <div class="finance-data"><span class="num">351</span> тыс.
                            руб.
                        </div>
                        <div class="finance-changes lt">
                            - <span class="num">7,4</span> млн
                            руб.
                        </div>
                    </div>
                    <div class="finance-col">
                        <div class="finance-link">
                            <div class="notab">Стоимость</div>
                        </div>
                        <div class="finance-data"><span class="num">0</span>
                            руб.
                        </div>
                    </div>
                </div>
                <p class="tile-item__text">Данные по финансовым показателям ООО "Смарт+" приведены на основании <span
                            class="bolder"><a href="#" class="gtm_acc_more" rel="nofollow">бухгалтерской отчетности за 2013 и 2015–2018 годы</a></span>.
                </p>
                <p class="company-name nabors">Согласно сведениям ФНС за 2018 год:</p>
                <div class="finance-columns">
                    <div class="finance-col">
                        <div class="finance-title">Налоги</div>
                        <div class="finance-data">
                            <a href="#" rel="nofollow" class="num">409</a> тыс. руб.
                        </div>
                    </div>
                    <div class="finance-col">
                        <div class="finance-title">Взносы</div>
                        <div class="finance-data">
                            <a href="#" rel="nofollow" class="num">3,0</a> млн руб.
                        </div>
                    </div>
                </div>
                <p class="tile-item__text"><span class="bolder"><a class="gtm_fs_more" href="#" rel="nofollow">Смотреть подробные сведения</a></span>
                    об уплаченных организацией в 2018 году налогах и сборах (по каждому налогу и сбору).
                </p>
            </div>
        </div>
        </div>
        <div class="wrap">
            <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption">
                    Краткая информация
                </div>
            </div>
            <div class="portlet-body accounts-list">

                <div class="enquiry" itemprop="description">

                    <p class="enquiry-text">ООО "Смарт+" зарегистрирована 7 декабря 2012 г. регистратором Межрайонная
                        инспекция Федеральной налоговой службы № 46 по г. Москве. Руководитель организации: генеральный
                        директор Кущенко Алексей Владимирович. Юридический адрес ООО "Смарт+" - 115093, город Москва,
                        улица Павла Андреева, 4, 24.</p>

                    <p class="enquiry-text"> Основным видом деятельности является <a href="/codes/690000">«Деятельность
                            в области права и бухгалтерского учета»</a>, зарегистрировано 12 дополнительных видов
                        деятельности. Организации ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СМАРТ+" присвоены ИНН
                        7705527945, ОГРН 1127747224549, ОКПО 13163904.</p>


                    <p class="enquiry-text">Телефон, адрес электронной почты, адрес официального сайта и другие
                        контактные данные ООО "Смарт+" отсутствуют в ЕГРЮЛ и могут быть добавлены представителем
                        организации.</p>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>