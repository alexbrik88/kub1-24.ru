<?php
use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Судебные дела (2)
        </div>
    </div>
    <div class="portlet-body accounts-list">


        <div class="content-frame__header">
            <div class="content-frame__description">
                Сведения об участии организации в судебных процессах: 2 завершенных дела.
            </div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content">



                <div class="filter-block">
                    <!--<span class="filter__label">Фильтрация <span class="filter__label--opener js-filter--opener"></span></span>-->
                    <div class="filter-block__wrap">
                        <div class="filter-block__frame">

                            <div class="filter-block__item">
                                <label class="filter-block__label">Период </label>
                                <div class="form-group">
                                    <input type="text" class="form-control input-item date-picker" placeholder="13.09.2016 — 13.12.2019" value="">
                                </div>
                            </div>

                            <div class="filter-block__item">
                                <label class="filter-block__label">Роль</label>
                                <div class="custom-select" data-selectprefix="common_type">
                                    <div class="custom-select__drop">
                                    </div>
                                </div>
                            </div>

                            <div class="filter-block__item">
                                <label class="filter-block__label">Статус</label>
                                <div class="custom-select" data-selectprefix="level">
                                    <div class="custom-select__drop">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sort" data-filterplus="">
                    <span class="sort__label">Сортировка <span class="sort__label--opener js-sort--opener"></span></span>

                    <div class="sort-list">
                        <div class="sort-item-wrap">
                            <a href="/arbitr/6583754?sort=case_start&amp;order=asc" class="sort-item sort-item--default sort-item--down"><span>Дата начала</span></a>
                        </div>

                        <div class="sort-item-wrap">
                            <a href="/arbitr/6583754?sort=claim_sum&amp;order=desc" class="sort-item"><span>Сумма требований</span></a>
                        </div>
                    </div>
                </div>

                <div class="company-item">
                    <div class="license-name">А40-133162/2017 <span class="license-name__date">от <span class="green-mark">19.07.2017</span></span> <a target="_blank" href="https://kad.arbitr.ru/Card/30db84c2-a4f3-415a-9045-84b81ba93038" class="out-link" rel="nofollow noopener"><span>на сайте КАД</span></a></div>

                    <div class="company-item__text">
                        Экономические споры по гражданским правоотношениям
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Сумма</dt>
                            <dd>307&nbsp;504,63 руб.</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Статус</dt>
                            <dd>Завершено</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Инстанция</dt>
                            <dd>Первая инстанция</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Последнее обновление:</dt>
                            <dd>06.09.2017</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Истец</dt>
                            <dd>
                                <a href="/id/6583754" class="link-arrow noarrow"><span>ООО "Смарт+"</span></a>                                                                                                    </dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Ответчик</dt>
                            <dd>
                                <a href="/id/7259268" class="link-arrow noarrow"><span>ООО "Волсан"</span></a>                                                                                                    </dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>ПОСЛЕДНИЙ ДОКУМЕНТ </dt>
                            <dd>
                                Резолютивная часть решения суда по делу, рассматриваемому в порядке упрощенного производства
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="company-item">
                    <div class="license-name">А40-189198/2016 <span class="license-name__date">от <span class="green-mark">13.09.2016</span></span> <a target="_blank" href="https://kad.arbitr.ru/Card/6ff51b3c-3e35-4ddd-9525-f4998e96a188" class="out-link" rel="nofollow noopener"><span>на сайте КАД</span></a></div>

                    <div class="company-item__text">
                        Экономические споры по гражданским правоотношениям
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Сумма</dt>
                            <dd>98&nbsp;213,12 руб.</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Статус</dt>
                            <dd>Завершено</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Инстанция</dt>
                            <dd>Первая инстанция</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Последнее обновление:</dt>
                            <dd>31.10.2016</dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>Истец</dt>
                            <dd>
                                <a href="/id/6583754" class="link-arrow noarrow"><span>ООО "Смарт+"</span></a>                                                                                                    </dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Ответчик</dt>
                            <dd>
                                <a href="/id/2233898" class="link-arrow noarrow"><span>ООО "Глобател"</span></a>                                                                                                    </dd>
                        </dl>
                    </div>

                    <div class="company-item-info">
                        <dl>
                            <dt>ПОСЛЕДНИЙ ДОКУМЕНТ </dt>
                            <dd>
                                Резолютивная часть решения суда по делу, рассматриваемому в порядке упрощенного производства
                            </dd>
                        </dl>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
