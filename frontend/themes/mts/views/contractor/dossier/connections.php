<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Связи (8)
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">Сведения об аффилированных организациях, полученные на основании анализа
                информации из ЕГРЮЛ<span data-quetip="Единый государственный реестр юридических лиц ФНС РФ"
                                         class="tooltip-opener quetip queimg"></span> и контактных данных.
            </div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content noyellow">

                <div class="doppel"></div>

                <div class="tabs-line">
                    <div class="tabs-line__wrap">
                        <span class="btn-link size-md active">Все</span>
                        <a href="#" class="btn-link size-md">По адресу (4)</a>
                        <a href="#" class="btn-link size-md">По руководителю (8)</a>
                        <a href="#" class="btn-link size-md">По учредителю (8)</a>
                    </div>
                </div>

                <div class="sort">
                    <span class="sort__label">Сортировка <span class="sort__label--opener js-sort--opener"></span></span>
                    <div class="sort-list">
                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=strength&amp;order=asc"
                               class="sort-item sort-item--default sort-item--down"><span>Аффилированность</span></a>
                        </div>

                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=capital&amp;order=desc"
                               class="sort-item"><span>Уставной капитал</span></a>
                        </div>

                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=date&amp;order=desc"
                               class="sort-item"><span>Дата регистрации</span></a>
                        </div>
                    </div>
                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/5445169"> ООО "Смарт+" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По адресу, по руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Алексей Владимирович</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        <span class="yellow-mark">                                    115093, город Москва, улица Павла Андреева, 4, 24                                            </span>
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7721704790</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1107746813811</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>6 октября 2010 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>10 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>69 Деятельность в области права и бухгалтерского учета</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/7724961"> ООО "Сокол Экспресс" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По адресу, по руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Алексей Владимирович</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        <span class="yellow-mark">                                    115093, город Москва, улица Павла Андреева, 4, 24                                            </span>
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7734346742</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1157746083990</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>5 февраля 2015 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>20 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>58.11 Издание книг</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/7903472"> ООО "КУБ" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По адресу, по руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Алексей Владимирович</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        <span class="yellow-mark">                                    115093, город Москва, улица Павла Андреева, 4, 24                                            </span>
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>9705052396</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>5157746024190</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>9 ноября 2015 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>444 444,45 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>63.11.1 Деятельность по созданию и использованию баз данных и информационных ресурсов</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/8186089"> ООО "Сити Групп" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По адресу, по руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Алексей Владимирович</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        <span class="yellow-mark">                                    115093, город Москва, улица Павла Андреева, 4, 24                                            </span>
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7705258869</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1147746866904</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>30 июля 2014 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>20 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>68.20 Аренда и управление собственным или арендованным недвижимым имуществом</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/1196113"> ООО "Аудит-Центр" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Наталья Владимировна</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        105094, город Москва, улица Госпитальный Вал, дом 5 строение 5, квартира 43
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>4632022310</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1024600949548</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>26 апреля 2002 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>19 608 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>69.20.1 Деятельность по проведению финансового аудита</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/2587771"> ООО "Спектр" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Суржиков Леонид Николаевич</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        101000, город Москва, Лубянский проезд, дом 15 строение 4, э 2 пом XIII ком 3
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7713701509</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1107746094576</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>15 февраля 2010 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>44 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>70.22 Консультирование по вопросам коммерческой деятельности и управления</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/5446360"> ООО "Техсерв" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Кущенко Наталья Владимировна</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        105094, город Москва, улица Госпитальный Вал, дом 5 строение 5, квартира 43
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7718823518</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1107746859725</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>20 октября 2010 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>10 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>45.20 Техническое обслуживание и ремонт автотранспортных средств</dd>
                        </dl>
                    </div>


                </div>


                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/id/11492910"> ООО "Комик Стрит" </a></div>


                    <div class="company-item__text">
                        <span class="green-mark">По руководителю, по учредителю</span></div>

                    <div class="company-item-info">
                        <dl>

                            <dt>Генеральный директор</dt>
                            <dd>Попов Владислав Игоревич</dd>
                        </dl>
                    </div>

                    <address class="company-item__text">
                        129110, город Москва, Орлово-Давыдовский переулок, дом 1, э 1 п III ком 3 оф 36
                    </address>

                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>7702434107</dd>
                        </dl>

                        <dl>
                            <dt>ОГРН</dt>
                            <dd>1187746637495</dd>
                        </dl>

                        <dl>
                            <dt>Дата регистрации</dt>
                            <dd>4 июля 2018 г.</dd>
                        </dl>

                        <dl>
                            <dt>Уставный капитал</dt>
                            <dd>20 000 руб.</dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>47.62 Торговля розничная газетами и канцелярскими товарами в специализированных магазинах</dd>
                        </dl>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>