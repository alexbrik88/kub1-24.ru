<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Бухгалтерская (финансовая) отчетность
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">Бухгалтерская отчетность публикуется Федеральной службой
                государственной статистики с задержкой, отчет за предыдущий (2018) год обычно становится доступен в
                сентябре-октябре текущего (2019).
            </div>
            <br/>
            <div class="statement-name">ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СМАРТ+"</div>
            <table class="info-table info-table--egrul">
                <tbody>
                <tr>
                    <td class="empty"></td>
                    <td>ОГРН</td>
                    <td>1127747224549</td>
                </tr>
                <tr>
                    <td class="empty"></td>
                    <td>ИНН / КПП</td>
                    <td>7705527945 / 770501001</td>
                </tr>
                </tbody>
            </table>
            <br/>
        </div>

        <div class="tile-item striped-table">
            <div class="accounting-header">
                <div class="accounting-header__title">
                    <div id="xblock_1" class="accounting-title tile-item__title">Бухгалтерский баланс</div>
                    <div class="accounting-description">Все суммы указаны в тысячах рублей</div>
                </div>
            </div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <th>
                            <div class="accounting-table__title">Форма № 1</div>
                        </th>
                        <th>
                            <div class="accounting-table__title">Код</div>
                        </th>
                        <th class="has-value table-desktop">
                            <div class="accounting-table__title">2013</div>

                            <p>
                                <span class="gray-text">нач.</span>
                                <span class="dark-text">/ кон.</span>
                            </p>
                        </th>
                        <th class="has-value table-desktop">
                            <div class="accounting-table__title">2015</div>
                            <p>
                                <span class="gray-text">нач.</span>
                                <span class="dark-text">/ кон.</span>
                            </p>
                        </th>
                        <th class="has-value table-desktop table-tablet">
                            <div class="accounting-table__title">2016</div>
                            <p>
                                <span class="gray-text">нач.</span>
                                <span class="dark-text">/ кон.</span>
                            </p>
                        </th>
                        <th class="has-value table-desktop table-tablet">
                            <div class="accounting-table__title">2017</div>
                            <p>
                                <span class="gray-text">нач.</span>
                                <span class="dark-text">/ кон.</span>
                            </p>
                        </th>
                        <th class="has-value table-desktop table-tablet table-mobile">
                            <div class="accounting-table__title">2018</div>
                            <p>
                                <span class="gray-text">нач.</span>
                                <span class="dark-text">/ кон.</span>
                            </p>
                        </th>
                    </tr>
                    </tbody>
                </table>
            </div>


            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            БАЛАНС (актив)
                        </td>

                        <td>
                            1600
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">1 115</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">3 399</p>
                            <p class="dark-text">3 982</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">3 982</p>
                            <p class="dark-text">7 761</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">7 761</p>
                            <p class="dark-text">16 428</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">16 428</p>
                            <p class="dark-text">15 899</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            БАЛАНС (пассив)
                        </td>

                        <td>
                            1700
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">1 115</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">3 399</p>
                            <p class="dark-text">3 982</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">3 982</p>
                            <p class="dark-text">7 761</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">7 761</p>
                            <p class="dark-text">16 428</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">16 428</p>
                            <p class="dark-text">15 899</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tile-item__title">Внеоборотные активы</div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            Нематериальные активы
                        </td>

                        <td>
                            1110
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Результаты исследований и разработок
                        </td>

                        <td>
                            1120
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Нематериальные поисковые активы
                        </td>

                        <td>
                            1130
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Материальные поисковые активы
                        </td>

                        <td>
                            1140
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Основные средства
                        </td>

                        <td>
                            1150
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">77</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Доходные вложения в материальные ценности
                        </td>

                        <td>
                            1160
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Финансовые вложения
                        </td>

                        <td>
                            1170
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Отложенные налоговые активы
                        </td>

                        <td>
                            1180
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Прочие внеоборотные активы
                        </td>

                        <td>
                            1190
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Итого внеоборотных активов
                        </td>

                        <td>
                            1100
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">77</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tile-item__title">Оборотные активы</div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            Запасы
                        </td>

                        <td>
                            1210
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">41</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">63</p>
                            <p class="dark-text">331</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">331</p>
                            <p class="dark-text">63</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">63</p>
                            <p class="dark-text">3</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">3</p>
                            <p class="dark-text">3</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Налог на добавленную стоимость по приобретенным ценностям
                        </td>

                        <td>
                            1220
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Дебиторская задолженность
                        </td>

                        <td>
                            1230
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">455</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">2 619</p>
                            <p class="dark-text">2 316</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">2 316</p>
                            <p class="dark-text">6 476</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">6 476</p>
                            <p class="dark-text">15 051</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">15 051</p>
                            <p class="dark-text">14 455</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Финансовые вложения (за исключением денежных эквивалентов)
                        </td>

                        <td>
                            1240
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Денежные средства и денежные эквиваленты
                        </td>

                        <td>
                            1250
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">619</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">716</p>
                            <p class="dark-text">1 335</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 335</p>
                            <p class="dark-text">1 222</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 222</p>
                            <p class="dark-text">1 374</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">1 374</p>
                            <p class="dark-text">1 364</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Прочие оборотные активы
                        </td>

                        <td>
                            1260
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Итого оборотных активов
                        </td>

                        <td>
                            1200
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">1 115</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">3 399</p>
                            <p class="dark-text">3 982</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">3 982</p>
                            <p class="dark-text">7 761</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">7 761</p>
                            <p class="dark-text">16 428</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">16 428</p>
                            <p class="dark-text">15 822</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tile-item__title">Капитал и резервы</div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            Уставный капитал (складочный капитал, уставный фонд, вклады товарищей)
                        </td>

                        <td>
                            1310
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">20</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">20</p>
                            <p class="dark-text">20</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">20</p>
                            <p class="dark-text">20</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">20</p>
                            <p class="dark-text">20</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">20</p>
                            <p class="dark-text">20</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Собственные акции, выкупленные у акционеров
                        </td>

                        <td>
                            1320
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Переоценка внеоборотных активов
                        </td>

                        <td>
                            1340
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Добавочный капитал (без переоценки)
                        </td>

                        <td>
                            1350
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Резервный капитал
                        </td>

                        <td>
                            1360
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Нераспределенная прибыль (непокрытый убыток)
                        </td>

                        <td>
                            1370
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">827</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">1 999</p>
                            <p class="dark-text">2 077</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">2 077</p>
                            <p class="dark-text">6 447</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">6 447</p>
                            <p class="dark-text">14 188</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">14 188</p>
                            <p class="dark-text">14 539</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ИТОГО капитал
                        </td>

                        <td>
                            1300
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">847</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">2 019</p>
                            <p class="dark-text">2 097</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">2 097</p>
                            <p class="dark-text">6 467</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">6 467</p>
                            <p class="dark-text">14 208</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">14 208</p>
                            <p class="dark-text">14 559</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tile-item__title">Долгосрочные обязательства</div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            Долгосрочные заемные средства
                        </td>

                        <td>
                            1410
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">325</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">325</p>
                            <p class="dark-text">70</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Отложенные налоговые обязательства
                        </td>

                        <td>
                            1420
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Оценочные обязательства
                        </td>

                        <td>
                            1430
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Прочие долгосрочные обязательства
                        </td>

                        <td>
                            1450
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ИТОГО долгосрочных обязательств
                        </td>

                        <td>
                            1400
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">325</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">325</p>
                            <p class="dark-text">70</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="tile-item__title">Краткосрочные обязательства</div>

            <div class="accounting-table-wrap">
                <table class="accounting-table">
                    <tbody>
                    <tr>
                        <td>
                            Краткосрочные заемные обязательства
                        </td>

                        <td>
                            1510
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">1 001</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Краткосрочная кредиторская задолженность
                        </td>

                        <td>
                            1520
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">268</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">379</p>
                            <p class="dark-text">1 885</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 885</p>
                            <p class="dark-text">1 294</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 294</p>
                            <p class="dark-text">1 895</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">1 895</p>
                            <p class="dark-text">1 270</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Доходы будущих периодов
                        </td>

                        <td>
                            1530
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Оценочные обязательства
                        </td>

                        <td>
                            1540
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Прочие краткосрочные обязательства
                        </td>

                        <td>
                            1550
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">0</p>
                            <p class="dark-text">0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ИТОГО краткосрочных обязательств
                        </td>

                        <td>
                            1500
                        </td>

                        <td class="has-value table-desktop">
                            <p class="gray-text">0</p>
                            <p class="dark-text">268</p>
                        </td>
                        <td class="has-value table-desktop">
                            <p class="gray-text">1 380</p>
                            <p class="dark-text">1 885</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 885</p>
                            <p class="dark-text">1 294</p>
                        </td>
                        <td class="has-value table-desktop table-tablet">
                            <p class="gray-text">1 294</p>
                            <p class="dark-text">1 895</p>
                        </td>
                        <td class="has-value table-desktop table-tablet table-mobile">
                            <p class="gray-text">1 895</p>
                            <p class="dark-text">1 270</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>