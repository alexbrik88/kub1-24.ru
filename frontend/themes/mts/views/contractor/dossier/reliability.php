<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Анализ надёжности организации
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="content-frame__header content-frame__header--rely">
            <div class="rely-rating-badge rely-rating-positive" style="background-color: transparent">
                Надёжность:
                <span>Высокая</span>
            </div>

            <div class="content-frame__title"><!--Анализ надёжности организации-->&nbsp;</div>
            <div class="content-frame__description">Перед началом сотрудничества с ООО "Смарт+" рекомендуем:</div>
            <div class="recommendations">
                <div class="company-item-hidden-info js-hidden-wrap">
                    <ul>
                        <li><strong>Изучить информацию о деятельности организации</strong>, представленную на портале;
                        </li>
                        <li><strong>Запросить у организации копии учредительных документов, лицензий и
                                свидетельств</strong>, необходимых для осуществления деятельности, а также копии
                            документов, подтверждающих полномочия руководителя (если подписант не является руководителем
                            - копию доверенности).
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tabs-line">
            <div class="tabs-line__wrap">
                <span class="btn-link size-md active">Все</span>
                <a href="#" class="btn-link size-md rely-negative">Отрицательные (3)</a>
                <a href="#" class="btn-link size-md rely-warning">Требуют внимания (7)</a>
                <a href="#" class="btn-link size-md rely-positive">Положительные (21)</a>
            </div>
        </div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h5>Отрицательные факты</h5></div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Динамика доходов/активов</div>
                    <div class="rely-short-negative">
                        Отрицательная динамика
                    </div>
                </div>
                <div class="requisites-item__value">Наблюдается отрицательная динамика доходов (по выручке) и/или
                    активов по итогам последнего доступного периода. Рекомендуется выяснить причину снижения показателей
                    и провести дополнительный анализ бухгалтерской отчетности контрагента.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Зависимость от дебиторов</div>
                    <div class="rely-short-negative">
                        Наблюдается зависимость
                    </div>
                </div>
                <div class="requisites-item__value">Доля дебиторской задолженности в активах составляет более 50%, что в
                    большинстве случаев отрицательно сказывается на платежеспособности организации.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Рентабельность собственного капитала (ROE)</div>
                    <div class="rely-short-negative">
                        2,41%
                    </div>
                </div>
                <div class="requisites-item__value">Для оценки эффективности инвестиций широко используется нормативное
                    значение ROE. Хорошим показателем считается значение превышающее 10%. Важно отметить, что слишком
                    большое значение показателя может негативно влиять на финансовую устойчивость предприятия. Если
                    показатель меньше 10% - это уже тревожный сигнал и стимул для того, чтобы нарастить доходность
                    собственного капитала.
                </div>
            </div>
        </div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h5>Требующие внимания факты</h5></div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Уставный капитал</div>
                    <div class="rely-short-warning">
                        Менее 100 тысяч рублей
                    </div>
                </div>
                <div class="requisites-item__value">Уставный капитал менее 100 тысяч рублей, это типично для большинства
                    средних организаций.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Судебные дела</div>
                    <div class="rely-short-warning">
                        Участвовала в завершенных делах
                    </div>
                </div>
                <div class="requisites-item__value">Перед началом сотрудничества с контрагентом рекомендуется произвести
                    анализ судебных дел с участием организации.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Организационно-правовая форма</div>
                    <div class="rely-short-warning">
                        ООО
                    </div>
                </div>
                <div class="requisites-item__value">Данные организации изначально обладают средним уровнем доверия в
                    силу непубличности и распространенности.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Реестр субъектов МСП</div>
                    <div class="rely-short-warning">
                        Есть в реестре, статус - малое предприятие
                    </div>
                </div>
                <div class="requisites-item__value">Организации присвоен статус малого предприятия. Статус может быть
                    изменен после сдачи отчетности в налоговую инспекцию.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Налоговый спецрежим</div>
                    <div class="rely-short-warning">
                        Есть отметка
                    </div>
                </div>
                <div class="requisites-item__value">Организации, применяющие специальные налоговые режимы, часто не
                    являются плательщиками НДС.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Коэффициент абсолютной ликвидности</div>
                    <div class="rely-short-warning">
                        1,07
                    </div>
                </div>
                <div class="requisites-item__value">Нормальное значение коэффициента - от 0,2 до 0,5. Это означает, что
                    от 20 до 50% краткосрочных долгов организация способна погасить в кратчайшие сроки по первому
                    требованию кредиторов. Превышение величины 0,5 указывает на неоправданные задержки в использовании
                    высоколиквидных активов.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Коэффициент соотношения заемного и собственного капитала</div>
                    <div class="rely-short-warning">
                        0,00
                    </div>
                </div>
                <div class="requisites-item__value">Значение коэффициента менее 0,5, будучи показателем устойчивого
                    финансового положения, одновременно указывает на неэффективность работы организации.
                </div>
            </div>
        </div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h5>Положительные факты</h5></div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Дата регистрации</div>
                    <div class="rely-short-positive">
                        Создана более 7 лет назад
                    </div>
                </div>
                <div class="requisites-item__value">Организация создана более трех лет назад, что говорит о стабильной
                    деятельности и поднадзорности государственным органам.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Руководитель</div>
                    <div class="rely-short-positive">
                        Не менялся
                    </div>
                </div>
                <div class="requisites-item__value">У организации нет изменений в данных о руководителе, что
                    свидетельствует о стабильности аппарата принятия решений контрагента.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Юридический адрес</div>
                    <div class="rely-short-positive">
                        Не менялся
                    </div>
                </div>
                <div class="requisites-item__value">Отсутствие изменений в юридическом адресе на протяжении нескольких
                    лет косвенно свидетельствует о стабильном функционировании организации.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Среднесписочная численность</div>
                    <div class="rely-short-positive">
                        25 сотрудников
                    </div>
                </div>
                <div class="requisites-item__value">Рекомендуется соотнести масштаб деятельности организации с
                    количеством ее сотрудников.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Налоговая нагрузка</div>
                    <div class="rely-short-positive">
                        Положительная нагрузка
                    </div>
                </div>
                <div class="requisites-item__value">По итогам последнего доступного периода наблюдается положительная
                    налоговая нагрузка. Это снижает налоговые риски при взаимодействии с контрагентом.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Наличие выплат персоналу</div>
                    <div class="rely-short-positive">
                        Есть выплаты
                    </div>
                </div>
                <div class="requisites-item__value">Наличие данных о выплатах персоналу свидетельствует о ведении
                    реальной деятельности организацией.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Судебные дела</div>
                    <div class="rely-short-positive">
                        Не участвовала в качестве ответчика
                    </div>
                </div>
                <div class="requisites-item__value">Отсутствие судебных дел в качестве ответчика свидетельствует о
                    надлежащем исполнении обязательств перед контрагентами.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Реестр массовых руководителей</div>
                    <div class="rely-short-positive">
                        Нет в реестре
                    </div>
                </div>
                <div class="requisites-item__value">Руководитель организации не значится в реестре массовых
                    руководителей ФНС.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Реестр массовых учредителей</div>
                    <div class="rely-short-positive">
                        Нет в реестре
                    </div>
                </div>
                <div class="requisites-item__value">Учредители организации не значатся в реестре массовых учредителей
                    ФНС.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Реестр массовых адресов</div>
                    <div class="rely-short-positive">
                        Нет в реестре
                    </div>
                </div>
                <div class="requisites-item__value">Адрес регистрации организации не состоит в реестре массовых адресов
                    регистрации ФНС.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Дисквалифицированные лица</div>
                    <div class="rely-short-positive">
                        Нет в реестре
                    </div>
                </div>
                <div class="requisites-item__value">В состав исполнительных органов организации не входят
                    дисквалифицированные лица.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Учредители</div>
                    <div class="rely-short-positive">
                        Нет изменений за последний год
                    </div>
                </div>
                <div class="requisites-item__value">У организации нет изменений в данных об учредителях за последний
                    год. Это свидетельствует о стабильности структуры капитала организации.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Статус организации</div>
                    <div class="rely-short-positive">
                        Действует
                    </div>
                </div>
                <div class="requisites-item__value">Информации о прекращении деятельности нет.</div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Фондовооруженность</div>
                    <div class="rely-short-positive">
                        Положительная фондовооруженность
                    </div>
                </div>
                <div class="requisites-item__value">Положительная фондовооруженность (остаточная стоимость собственных
                    основных средств) по итогам последнего доступного периода снижает риски в работе с организацией.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Зависимость от кредиторов</div>
                    <div class="rely-short-positive">
                        Нормальная зависимость
                    </div>
                </div>
                <div class="requisites-item__value">Выручка существенно превышает краткосрочную задолженность. Отношение
                    задолженности к годовой выручке по данным доступной бухгалтерской отчетности составляет менее 50%.
                    Это свидетельствует об отсутствии существенной зависимости от кредиторов.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Обеспеченность собственными средствами</div>
                    <div class="rely-short-positive">
                        91,53%
                    </div>
                </div>
                <div class="requisites-item__value">Нормативное значение коэффициента обеспеченности собственными
                    средствами составляет 10% и выше. Если на конец отчетного периода коэффициент имеет значение менее
                    10%, структура баланса организации признается неудовлетворительной.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Коэффициент текущей ликвидности</div>
                    <div class="rely-short-positive">
                        12,46
                    </div>
                </div>
                <div class="requisites-item__value">Чем выше значение коэффициента текущей ликвидности, тем выше
                    ликвидность активов организации. Нормальным, а часто и оптимальным, считается значение коэффициента
                    1,7 и более.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Рентабельность по активам</div>
                    <div class="rely-short-positive">
                        Положительная
                    </div>
                </div>
                <div class="requisites-item__value">Рентабельность активов - отношение чистой прибыли (убытка) к
                    совокупным активам, нормальным считается любое положительное значение.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Рентабельность по продажам</div>
                    <div class="rely-short-positive">
                        Положительная
                    </div>
                </div>
                <div class="requisites-item__value">Рентабельность продаж – отношение чистой прибыли (убытка) к
                    себестоимости продаж. Нормальным считается любое положительное значение.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Коэффициент финансовой автономии</div>
                    <div class="rely-short-positive">
                        91,57%
                    </div>
                </div>
                <div class="requisites-item__value">Уровень автономии - доля оборотных средств, обеспеченных
                    собственными средствами организации. Характеризует финансовую устойчивость организации
                    (финансирование текущих операций за счет собственных средств), нормальным считается значение более
                    10%, пограничным – от 5 до 10%, критическим – менее 5%.
                </div>
            </div>
            <div class="requisites-item">
                <div class="requisites-item__name">
                    <div class="rely-name">Коэффициент финансовой устойчивости</div>
                    <div class="rely-short-positive">
                        0,92
                    </div>
                </div>
                <div class="requisites-item__value">Коэффициент финансовой устойчивости находится в пределах от 0,75 до
                    0,95, что говорит о стабильной хозяйственной деятельности и высокой финансовой независимости
                    организации.
                </div>
            </div>
        </div>
        <div class="disclaimer">
            Значение рейтинга формируется в результате анализа открытых общедоступных данных РФ и фактов хозяйственной
            деятельности организации с использованием скоринговой модели, основанной на алгоритмах машинного обучения.
            Данная оценка является мнением портала Rusprofile и не дает каких-либо гарантий или заверений третьим лицам,
            а также не является рекомендацией для принятия коммерческих или иных решений. Значение рейтинга
            автоматически пересчитывается при изменении фактов хозяйственной деятельности и поступлении новой информации
            из источников открытых данных.
        </div>
    </div>
</div>
