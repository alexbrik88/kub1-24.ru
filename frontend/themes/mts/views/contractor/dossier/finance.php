<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Финансы за 2018 год
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">Сведения на основании данных Росстат и ФНС.</div>
        </div>

        <div class="finance-header">Основные показатели</div>

        <div class="finance-list">
            <div class="fl-block">
                <div class="fl-title">Выручка</div>

                <div class="fl-text"><span class="num">29 746 000</span> руб.</div>

                <div class="finance-changes lt">
                    - <span class="num">8,1</span> млн
                    руб.
                </div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Прибыль</div>

                <div class="fl-text"><span class="num">351 000</span> руб.</div>

                <div class="finance-changes lt">
                    - <span class="num">7,4</span> млн
                    руб.
                </div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Сумма расходов</div>

                <div class="fl-text"><span class="num">29 184 000</span> руб.</div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Стоимость</div>

                <div class="fl-text"><span class="num">0</span> руб.</div>

            </div>

            <div class="fl-block">
                <div class="fl-title">Уставный капитал</div>

                <div class="fl-text"><span class="num">20 000</span> руб.</div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Налоговая нагрузка</div>

                <div class="fl-text"><span class="num">1,4 %</span></div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Основные средства</div>

                <div class="fl-text"><span class="num">77 000</span> руб.</div>

                <div class="finance-changes mt">
                    + <span class="num">77</span> тыс.
                    руб.
                </div>
            </div>

            <div class="fl-block">
                <div class="fl-title">Дебиторская задолженность</div>

                <div class="fl-text"><span class="num">14 455 000</span> руб.</div>

                <div class="finance-changes reverted lt">
                    - <span class="num">596</span> тыс.
                    руб.
                </div>
            </div>
        </div>

        <a href="#" class="see-details arrowfix" rel="nofollow">Смотреть бухгалтерскую отчетность
            за <span>2013 и 2015–2018 годы</span></a>

        <div class="finance-header">Уплачено налогов</div>

        <div class="finance-list">
            <div class="fl-block">
                <div class="fl-title">Налог, взимаемый в связи с применением УСН</div>

                <div class="fl-text"><span class="num">408 838</span> руб.</div>
            </div>
            <div class="fl-block">
                <div class="fl-title">Налог на добавленную стоимость</div>

                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
            <div class="fl-block">
                <div class="fl-title">Налог на прибыль</div>

                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
            <div class="fl-block">
                <div class="fl-title">Неналоговые доходы, администрируемые налоговыми органами</div>

                <div class="fl-text"><span class="num">0</span> руб.</div>
            </div>
        </div>

        <div class="finance-header">Уплачено взносов</div>

        <div class="finance-list">
            <div class="fl-block">
                <div class="fl-title">Страховые и другие взносы в ПФР</div>
                <div class="fl-text"><span class="num">2 223 132</span> руб.</div>
            </div>
            <div class="fl-block">
                <div class="fl-title">Страховые взносы на ОМС</div>
                <div class="fl-text"><span class="num">515 362</span> руб.</div>
            </div>
            <div class="fl-block">
                <div class="fl-title">Взносы на соц. страхование</div>
                <div class="fl-text"><span class="num">228 787</span> руб.</div>
            </div>
        </div>

    </div>
</div>