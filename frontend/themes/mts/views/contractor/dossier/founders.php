<?php

use common\models\Company;
use common\models\Contractor;

/** @var Company $company */
/** @var Contractor $model */

$this->title = 'Досье ' . $model->getShortName(true);

?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="portlet-title row-fluid">
        <div class="caption">
            Учредители (1)
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">
                Согласно данным ЕГРЮЛ<span data-quetip="Единый государственный реестр юридических лиц ФНС РФ" class="tooltip-opener quetip queimg"></span> учредителем организации является 1 физическое лицо.
            </div>
            <div class="content-frame__description">Уставный капитал: 20 000 руб.</div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content">
                <div class="company-item">
                    <div class="company-item__title">
                        <a href="/person/kushhenko-av-770304445862"> Кущенко Алексей Владимирович </a></div>
                    <div class="company-item-info">
                        <dl>
                            <dt>Доля</dt>
                            <dd>20 000 руб. <span class="green-mark">(100%)</span></dd>
                        </dl>
                    </div>
                    <div class="company-item-info">
                        <dl>
                            <dt>ИНН</dt>
                            <dd>770304445862</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>