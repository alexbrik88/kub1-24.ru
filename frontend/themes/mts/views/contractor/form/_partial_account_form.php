<?php

use common\components\widgets\BikTypeahead;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\ContractorAccount;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorAccount */
/* @var $contractor common\models\Contractor */

$bikAuto = $contractor->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON;
?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'options' => [
        'class' => 'contractor-account-form',
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group col-6'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

<div class="row">
    <div class="col-12">
        <div class="row">
            <?= $form->field($model, 'rs')->textInput(['maxlength' => true]); ?>

            <?php if ($bikAuto) : ?>
                <?= $form->field($model, 'bik')->widget(\common\components\widgets\BikTypeahead::classname(), [
                    'remoteUrl' => Url::to(['/dictionary/bik']),
                    'related' => [
                        '#' . Html::getInputId($model, 'bank_name') => 'name',
                        '#' . Html::getInputId($model, 'bank_city') => 'city',
                        '#' . Html::getInputId($model, 'ks') => 'ks',
                    ],
                ])->textInput(['maxlength' => true]); ?>
            <?php else : ?>
                <?= $form->field($model, 'bik')->textInput(); ?>
            <?php endif ?>

            <?= $form->field($model, 'bank_name')->textInput([
                'maxlength' => $bikAuto,
                'readonly' => $bikAuto,
            ]); ?>
            <?= $form->field($model, 'bank_city')->textInput([
                'maxlength' => $bikAuto,
                'readonly' => $bikAuto,
            ]); ?>
            <?= $form->field($model, 'ks')->textInput([
                'maxlength' => $bikAuto,
                'readonly' => $bikAuto,
            ]); ?>
            <?= $form->field($model, 'is_main')->checkbox(['class' => 'uniform-input'], true); ?>
        </div>
    </div>
</div>
<div class="mt-3 d-flex justify-content-between">
    <?= \yii\bootstrap4\Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal'
    ]); ?>
    </div>

<?php ActiveForm::end(); ?>

<?= Html::script(
<<<JS
$('input.uniform-input', $('form.contractor-account-form')).uniform();
JS
) ?>