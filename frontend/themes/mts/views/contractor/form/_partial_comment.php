<?php

use common\models\Contractor;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$delayInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$textAreaConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$discountInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
];

$responsibleEmployers = ArrayHelper::map($model->company->getEmployeeCompanies()
    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');
?>

<div class="legal  pl-2 pr-2 mt-3 pt-3">
    <div class="pl-1 pr-1">
        <div class="row">
            <div class="col-6">
                <?=
                $form->field($model, 'responsible_employee_id', $textInputConfig
                )->widget(Select2::class, [
                    'data' => $responsibleEmployers,
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]);
                ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'payment_delay', $delayInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'source', $textInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                ]); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'discount', $discountInputConfig)->input('number', [
                    'min' => 0,
                    'max' => 99.99,
                    'step' => 'any',
                    'style' => 'width: 100%;',
                ])->label('Фиксированная скидка на всё, %'); ?>
            </div>
        </div>
        <?= $form->field($model, 'comment', $textAreaConfig)->textarea([
            'maxlength' => true,
            'style' => 'width: 100%;',
        ]); ?>
    </div>
</div>