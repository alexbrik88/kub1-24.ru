<?php

use yii\helpers\Html;
use common\models\Contractor;
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\models\contractor\ContractorAgentBuyer;

/** @var $agent Contractor */
/** @var $buyerInfo ContractorAgentBuyer */
$start_date = DateHelper::format($buyerInfo->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
?>

<tr class="buyer-row" role="row">
    <td class="delete-column-left" style="white-space: nowrap;">
        <span class="icon-close remove-buyer"></span>
        <?= ImageHelper::getThumb('img/menu-humburger.png', [20, 14], [
            'class' => 'sortable-row-icon',
            'style' => 'padding-bottom: 9px;',
        ]); ?>
    </td>
    <td class="buyer-name">
        <span class="form-control" style="width:100%"><?= $buyerInfo->buyer->getShortName() ?></span>
        <?php echo Html::hiddenInput('agentBuyer['.$number.'][contractor_id]', $buyerInfo->buyer->id) ?>
    </td>
    <td class="buyer-start-date">
        <?= Html::textInput('agentBuyer['.$number.'][start_date]', $start_date, [
            'id' => 'document_date',
            'class' => 'form-control date-picker ico agent-field',
            'style' => 'width:130px',
            'data-date-viewmode' => 'years',
        ]); ?>
    </td>
    <td class="delete-column-right">

    </td>
</tr>