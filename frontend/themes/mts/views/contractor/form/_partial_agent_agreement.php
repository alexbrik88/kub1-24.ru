<?php
use yii\bootstrap4\Modal;
use yii\widgets\Pjax;
use yii\helpers\Url;

Modal::begin([
    'id' => 'agreement-modal-container',
]); ?>



<?php Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#contractor-agent_agreement_id", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
            '/documents/agreement/create',
            'contractor_id' => $model->id,
            'type' => 1,
            'returnTo' => 'act',
            'container' => 'agreement-select-container',
        ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            refreshDatepicker();
        });
        $("#agreement-modal-container").modal("show");
        $("#contractor-agent_agreement_id").val("").trigger("change");
    }
});

$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#contractor-agent_agreement_id").val(window.AgreementValue).trigger("change");
    }
})

');