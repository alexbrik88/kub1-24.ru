<?php

use common\components\grid\GridView;
use common\models\Agreement;
use common\models\file\widgets\FileUpload;
use frontend\widgets\BtnConfirmModalWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\AgreementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<div class="portlet box darkblue">
    <div class="portlet-body">
        <?php if ($model->isNewRecord) : ?>
            <h4>Возможность добавить договор появится после сохранения контрагента</h4>
        <?php else : ?>
            <?php Pjax::begin([
                'id' => 'agreement-pjax-container',
                'enablePushState' => false,
            ]) ?>
            <?= GridView::widget([
                'dataProvider' => new ActiveDataProvider([
                    'query' => $model->getAgreements(),
                    'sort' => [
                        'defaultOrder' => ['document_number' => SORT_DESC],
                    ]
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap overfl_text_hid',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'attribute' => 'document_number',
                        'headerOptions' => ['style' => 'width:100px;'],
                    ],
                    [
                        'attribute' => 'document_date_input',
                        'headerOptions' => ['style' => 'width:100px;'],
                    ],
                    'agreementType.name',
                    'document_name',
                    [
                        'label' => 'Файл',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'width:50px;'],
                        'content' => function ($data) {
                            $file = $data->files ? $data->files[0] : null;
                            return $file ? Html::a('<span class="icon icon-paper-clip m-r-10"></span>', [
                                    'agreement-file-get',
                                    'id' => $data->id,
                                    'file-id' => $file->id,
                                ], [
                                    'class' => 'file-link',
                                    'target' => '_blank',
                                    'download' => '',
                                    'data-pjax' => 0,
                                ]) : '';
                        },
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, $data) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                    'data-url' => $url,
                                    'class' => 'agreement-modal-link',
                                    'title' => Yii::t('yii', 'Обновить'),
                                    'aria-label' => Yii::t('yii', 'Обновить'),
                                ]);
                            },
                            'delete' => function ($url, $data) {
                                return BtnConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                        'class' => '',
                                        'tag' => 'a',
                                    ],
                                    'confirmUrl' => $url,
                                    'message' => 'Вы уверены, что хотите удалить договор?',
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = 'agreement-update';
                                    break;
                                case 'delete':
                                    $url = 'agreement-delete';
                                    break;
                            }

                            return Url::to([$url, 'id' => $model->id]);
                        },
                    ],
                ],
            ]); ?>
            <?php Pjax::end() ?>

            <?= Html::a('<i class="fa icon fa-plus-circle" style="font-size: 24px; padding: 0;"></i>', '#', [
                'class' => 'btn yellow agreement-modal-link',
                'style' => 'padding: 4px 8px;',
                'data-url' => Url::to(['agreement-create', 'id' => $model->id])
            ]) ?>
        <?php endif ?>
    </div>
</div>
