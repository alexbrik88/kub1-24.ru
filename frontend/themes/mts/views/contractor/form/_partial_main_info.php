<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use frontend\themes\mts\helpers\Icon;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$checkboxConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'wrapperOptions' => [
        'class' => 'form-filter',
        'style' => 'padding-top: 8px;'
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$inputListConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];

$helpAccounting = Html::tag('span', '', [
    'class' => 'tooltip2 ico-question',
    'style' => 'display: inline-block; margin: -6px 5px; vertical-align: middle;',
    'data-tooltip-content' => '#tooltip_not_accounting',
]);

$isPhysicalPerson = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
?>

    <div class="wrap">
        <div class="row">
            <div class="legal col-6" style="<?= ($isPhysicalPerson) ? 'display:none' : '' ?>">
                <!-- INN LEGAL -->
                <?=
                $form->field($model, 'ITN', array_merge($textInputConfig, [
                    'options' => [
                        'class' => $model->face_type == 2 ? 'legal' : 'legal required',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}<p class='exists-contractor'></p>\n{endWrapper}",
                ]))->label('ИНН:')->textInput([
                    'placeholder' => $model->face_type == 2 ? '' : 'Автозаполнение по ИНН' ,
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                        'prod' => YII_ENV_PROD ? 1 : 0,
                    ],
                ]);
                ?>
            </div>
            <div class="physical col-6" style="<?= (!$isPhysicalPerson) ? 'display:none' : '' ?>">
                <!-- LastName Physical -->
                <?=
                $form->field($model, 'physical_lastname', array_merge($textInputConfig, [
                    'options' => [
                        'class' => 'physical required',
                    ],
                ]))->label('Фамилия:')->textInput([
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                    ],
                ]);
                ?>
            </div>
            <div class="col-6">
                <!-- face_type -->
                <?=
                $form->field($model, 'face_type', $textInputConfig
                )->widget(Select2::class, [
                    'options' => [
                        'id' => 'contractor_face_type',
                    ],
                    'data' => Contractor::$contractorFaceType,
                    'hideSearch' => true,
                    'disabled' => !empty($face_type_opt),
                    'pluginOptions' => [
                        'width' => '100%'
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="physical row" style="<?= (!$isPhysicalPerson) ? 'display:none' : '' ?>">
            <div class="col-4">
                <?=
                $form->field($model, 'physical_firstname', $textInputConfig)->label('Имя:')->textInput([
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                    ],
                ]);
                ?>
            </div>
            <div class="col-4">
                <?=
                $form->field($model, 'physical_patronymic', $textInputConfig)->label('Отчество:')->textInput([
                    'maxlength' => true,
                    'readonly' => (boolean) $model->physical_no_patronymic,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                    ],
                ]);
                ?>
            </div>
            <div class="col-4">
                <?= $form->field($model, 'physical_no_patronymic', $checkboxConfig)->label('Нет отчества:')->checkbox([], false); ?>
            </div>
        </div>

        <div class="row">
            <div class="legal <?= ($isPhysicalPerson) ? 'display:none' : '' ?> col-6">
                <?=
                $form->field($model, 'name', $textInputConfig)->label('Название контрагента:')->textInput([
                    'maxlength' => true,
                    'data' => [
                        'toggle' => 'popover',
                        'trigger' => 'focus',
                        'placement' => 'bottom',
                        'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
                    ],
                ]);
                ?>
            </div>
            <div class="legal <?= ($isPhysicalPerson) ? 'display:none' : '' ?> col-6">
                <?=
                $form->field($model, 'companyTypeId', $textInputConfig)->label('Форма')
                    ->widget(Select2::class, [
                        'data' => $model->getTypeArray(),
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                    ]);
                ?>
            </div>
            <div class="legal <?= ($isPhysicalPerson) ? 'display:none' : '' ?> col-6">
                <?= $form->field($model, 'taxation_system', $inputListConfig)->label()->radioList(
                    [
                        Contractor::WITH_NDS => 'С НДС',
                        Contractor::WITHOUT_NDS => 'Без НДС',
                    ],
                    [
                        'class' => 'radio-list inp_one_line-contractor',
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::tag('label', Html::radio($name, $checked, [
                                    'value' => $value
                                ]) . $label, [
                                'class' => 'mr-3',
                            ]);
                        },
                    ]
                ); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'status', $inputListConfig)->label()->radioList(
                    [
                        Contractor::ACTIVE => 'Активен',
                        Contractor::INACTIVE => 'Не активен',
                    ], [
                    'class' => 'radio-list inp_one_line-contractor',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, [
                                'value' => $value,
                            ]) . $label, [
                            'class' => 'radio-inline width120 marg_left mrg_lf_checkbox',
                        ]);
                    },
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <?php if ($model->getCanHasOpposite()) : ?>
                    <?php
                    $model->opposite = (int) $model->getHasOpposite();
                    $helpOpposite = Html::tag('span', Icon::QUESTION, [
                        'class' => 'tooltip2',
                        'style' => '',
                        'data-tooltip-content' => '#tooltip_has_opposite',
                    ]);
                    ?>
                    <?= $form->field($model, 'opposite', array_merge($checkboxConfig, [
                        'template' => "{label}\n{beginWrapper}\n{input}{$helpOpposite}\n{error}\n{endWrapper}",
                    ]))->checkbox([
                        'disabled' => $model->getHasOpposite(),
                    ], false); ?>
                    <div class="hidden">
                        <span id="tooltip_has_opposite">
                            Поставив галочку, данный контрагент будет отображаться и в Покупателях и Поставщиках
                        </span>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-6">
                <?php
                $helpAccounting = Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip2',
                    'style' => '',
                    'data-tooltip-content' => '#tooltip_not_accounting',
                ]);
                ?>
                <?= $form->field($model, 'not_accounting', array_merge($checkboxConfig, [
                    'options' => [
                        'class' => '',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}{$helpAccounting}\n{error}\n{endWrapper}",
                ]))->checkbox([], false); ?>
                <div class="hidden">
                    <span id="tooltip_not_accounting">
                        Документы по такому клиенту не попадают в выгрузку для 1с и видны только для руководителя
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <?=
                $form->field($model, 'order_currency', array_merge($textInputConfig, [
                    'options' => [
                        'id' => 'contractor_order_currency',
                        'class' => 'foreign_legal',
                        'style'=>'display:none'
                    ],
                    'labelOptions' => [
                        'class' => 'label',
                    ],
                    'inputOptions' => [
                        'class' => 'form-control',
                    ],
                    'wrapperOptions' => [
                        'class' => 'form-filter',
                    ],
                ]))->label('Выставлять счета в:')
                    ->widget(Select2::class, [
                        'data' => \common\models\currency\Currency::getAllCurrency(),
                        'hideSearch' => true,
                        'options' => [
                            'id' => 'order_currency'
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]]);
                ?>
                <div class="foreign_legal text-left contractor-currency-disclaimer" style="display: none">
                    Конвертация в RUB происходит автоматически, на день оплаты по курсу банка России.
                </div>
            </div>
        </div>

    </div>
