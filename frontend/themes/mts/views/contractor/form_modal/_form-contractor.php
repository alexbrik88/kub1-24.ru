<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use frontend\models\Documents;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */
$textInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$checkboxConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];

if ($model->isNewRecord) {
    $contractorTypeList = [
        Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
        Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
} else {
    if ($model->face_type == Contractor::TYPE_LEGAL_PERSON)
        $contractorTypeList = [Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо'];
    else
        $contractorTypeList = [Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
}

$showContractorType = Yii::$app->request->post('show_contractor_type');
$innPlaceholder = 'Автозаполнение по ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ? ' покупателя' : ' продавца') : '');
$innLabel = 'ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ?  ' покупателя' : ' продавца') : '');
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => '',
    ],
    'id' => 'new-contractor-invoice-form',
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]); ?>
<?= Html::hiddenInput('documentType', $documentType); ?>
<?= Html::hiddenInput('type', $model->type); ?>
<?php if (!$model->isNewRecord) {
    $invoiceId = Yii::$app->request->post('invoiceId');
    echo Html::hiddenInput('contractorId', $model->id);
    echo Html::hiddenInput('invoiceId', $invoiceId);
} ?>

<div class="row">
    <?php // echo $form->errorSummary($model); ?>
    <?php $model->face_type = isset($model->face_type) ? $model->face_type : 0; ?>

    <div id="contractor-face_type" class="form-group col-6">
        <label class="label mb-3"><?=($model->type == Contractor::TYPE_SELLER ? 'Тип поставщика ' : 'Тип покупателя ')?></label>
        <br>
        <div class="d-flex flex-wrap">
            <div class="mr-3">
                <input id="radio1"
                       type="radio"
                       name="Contractor[face_type]"
                       value="<?=(Contractor::TYPE_LEGAL_PERSON)?>"
                       <?=($model->face_type == Contractor::TYPE_LEGAL_PERSON ? 'checked=""' : '')?>>
                <label class="radio-label" for="radio1">
                    <span class="radio-txt-bold">Юридическое лицо</span>
                </label>
            </div>
            <div class="mr-3">
                <input id="radio2"
                       type="radio"
                       name="Contractor[face_type]"
                       value="<?=(Contractor::TYPE_PHYSICAL_PERSON)?>"
                    <?=($model->face_type == Contractor::TYPE_PHYSICAL_PERSON ? 'checked=""' : '')?>>
                <label class="radio-label" for="radio2">
                    <span class="radio-txt-bold">Физическое лицо</span>
                </label>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'ITN', [
        'options' => [
            'class' => 'form-group col-6 forContractor' . (!$model->face_type ? '' : ' selectedDopColumns'),
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'inputOptions' => [
            'class' => 'form-control form-control_placeholder_red',
        ],
        'template' => "{label}\n{input}\n{error}",
    ])->label($innLabel)->textInput([
        'placeholder' => $innPlaceholder,
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'prod' => YII_ENV_PROD ? 1 : 0,
        ],
        'disabled' => !$model->isNewRecord
    ]); ?>
</div>

<div class="forContractor <?= !$model->face_type ? '' : 'selectedDopColumns' ?>">
    <?= $this->render('_partial_main_info', [
        'model' => $model,
        'form' => $form,
        'class' => 'required',
        'textInputConfigDirector' => $partialDirectorTextInputConfig,
        'face_type_opt' => $face_type_opt,
        'textInputConfig' => $textInputConfig,
        'documentType' => $documentType,
    ]) ?>
    <div class="clearfix"></div>
    <?= $this->render('_partial_details', [
        'model' => $model,
        'form' => $form,
    ]) ?>
</div>
<div class="forContractor <?= $model->face_type ? '' : 'selectedDopColumns' ?>">
    <div class="row">
        <?= $form->field($model, 'physical_lastname', $textInputConfig)->label('Фамилия')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_firstname', $textInputConfig)->label('Имя')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_patronymic', $textInputConfig)->label('Отчество')->textInput([
            'maxlength' => true,
            'readonly' => (boolean)$model->physical_no_patronymic,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <div class="form-group col-6">
            <div class="checkbox checkbox_left" style="padding-top:30px">
                <label class="checkbox-label" for="contractor-physical_no_patronymic">
                    <?= Html::activeCheckbox($model, 'physical_no_patronymic', [
                        'id' => 'contractor-physical_no_patronymic',
                        'class' => 'checkbox-input input-hidden',
                        'label' => false,
                    ]); ?>
                    <span class="checkbox-txt">Нет отчества</span>
                </label>
            </div>
        </div>

        <?= $form->field($model, 'physical_address', $textInputConfig)->label('Адрес регистрации')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]);
        ?>
        <?= $form->field($model, 'contact_email', $textInputConfig)
            ->label('Email покупателя (для отправки счетов)</span>')
            ->textInput([
                'maxlength' => true,
            ]); ?>

        <div class="col-12">
            <button class="link link_collapse link_bold button-clr mb-4 collapsed refreshDatepicker" type="button" data-toggle="collapse" data-target="#dopColumns3" aria-expanded="false" aria-controls="dopColumns3">
                <span class="link-txt">Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры</span>
                <svg class="link-shevron svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                </svg>
            </button>
        </div>
    </div>

    <div id="dopColumns3" class="collapse dopColumns">
        <div class="row">
            <?= $form->field($model, 'physical_passport_series', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{2} 9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XX XX',
                ],
            ]); ?>
            <?= $form->field($model, 'physical_passport_number', $textInputConfig)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{6}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XXXXXX',
                ],
            ]); ?>
            <div class="form-group col-6">
                <label class="label">Дата выдачи</label>
                <div class="date-picker-wrap">
                    <?= Html::activeTextInput($model, 'physical_passport_date_output', [
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                        'value' => DateHelper::format($model->physical_passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]); ?>
                    <svg class="date-picker-icon svg-icon input-toggle">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                    </svg>
                </div>
            </div>
        </div>

    </div>

</div>
<div class="clearfix"></div>

<br>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr',
        'data-style' => 'expand-right',
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
    function contractorIsIp() {
        return $('#contractor-companytypeid').val() == "<?= CompanyType::TYPE_IP ?>";
    }
    function contractorIsNotIp() {
        return $('#contractor-companytypeid').val() && !contractorIsIp();
    }

    function contractorIsPhysical() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
    }
    function contractorIsLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
    }
    function contractorIsForeignLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
    }
    function contractorIsAgent() {
        return $("#contractor_is_agent_input:checked").length;
    }

    $('#contractor-companytypeid').change(function () {
        if ($(this).val() == "<?= CompanyType::TYPE_IP ?>") {
            $('.field-contractor-ppc').hide();
        } else {
            $('.field-contractor-ppc').show();
        }
        $('#new-contractor-invoice-form').yiiActiveForm('validateAttribute', 'contractor-itn');
    });
    $('#contractor-name').on('input', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('#contractor-companytypeid').on('change', function () {
        if ($('#contractor-companytypeid').val() == '1') {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
</script>
