<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */
?>

<div class="col-4 d-flex flex-column">
    <div class="wrap pl-4 pr-4 pt-4 pb-3 d-flex flex-column flex-grow-1">
        <div class="pl-2 pr-2 pt-2 d-flex flex-column flex-grow-1">
            <h4 class="mb-3 pb-3">Контакт</h4>
            <div class="pb-3" style="margin-bottom: 23px">
                <label class="checkbox-inline match-with-leader" style="height: 39px; pointer-events: none;">
                    <div class="checker">
                        <span class="<?php if ($model->contact_is_director) echo 'checked' ?>">
                            <input value="1" checked="" disabled="" type="checkbox">
                        </span>
                    </div>
                    Совпадает с руководителем
                </label>
            </div>
            <?php  if (!$model->contact_is_director): ?>
                <div class="pb-3 mb-3">
                    <div class="label weight-700 mb-3">ФИО</div>
                    <div><?php echo $model->contact_name ?: '—'; ?></div>
                </div>
                <div class="pb-3 mb-3">
                    <div class="label weight-700 mb-3">E-mail</div>
                    <div>
                        <?php if ($model->contact_email): ?>
                            <a class="link link_bold" href="mailto:<?php echo $model->contact_email; ?>"><?php echo $model->contact_email; ?></a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>
                <div class="pb-3">
                    <div class="label weight-700 mb-3">Телефон</div>
                    <div>
                        <?php if ($model->contact_phone): ?>
                            <a class="link link_dark" href="tel:<?php echo $model->contact_phone; ?>"><?php echo $model->contact_phone; ?></a>
                        <?php else: ?>
                            —
                        <?php endif; ?>
                    </div>
                </div>
                <div class="pb-3">
                    <label class="checkbox-inline match-with-leader" style="height: 39px; pointer-events: none;">
                        <div class="checker">
                        <span class="<?php if ($model->contact_in_act) echo 'checked' ?>">
                            <input value="1" checked="" disabled="" type="checkbox">
                        </span>
                        </div>
                        Указывать ФИО в Актах
                    </label>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>