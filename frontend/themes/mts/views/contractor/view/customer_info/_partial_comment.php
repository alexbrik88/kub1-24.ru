<?php

use yii\helpers\ArrayHelper;
use common\models\employee\Employee;

?>

<div class="pl-2 pr-2  mt-3 pt-3">
    <div class="row pl-1 pr-1">
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Ответственный сотрудник</div>
                    <div><?php echo $model->responsibleEmployee['lastname'] .' '. $model->responsibleEmployee['firstname'] .' '. $model->responsibleEmployee['patronymic']; ?></div>
                </div>
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Отсрочка оплаты в днях</div>
                    <div><?php echo $model->payment_delay; ?></div>
                </div>
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Фиксированная скидка на всё</div>
                    <div><?php echo ($model->discount > 0) ? "{$model->discount}%" : '0' ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Источник</div>
                    <div><?php echo $model->campaign->name ?? '—'; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="column mb-3 pb-3 mr-4">
                    <div class="label weight-700 mb-3">Комментарии</div>
                    <div><?php echo $model->comment ?: '—'; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>