<?php
/* @var $model common\models\Contractor */

use common\models\company\CompanyType;
use common\models\Contractor;

$isPhysicalPerson = ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON);
$additionalAccounts = $model->getContractorAccounts()->andWhere(['is_main' => 0])->all();
?>

<?php if ($isPhysicalPerson): ?>
    <div class="pl-2 pr-2 mt-3 pt-3">
        <div class="row pl-1 pr-1">
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Паспорт</div>
                        <div><?= ($model->physical_passport_isRf == 0) ? 'не РФ' : 'РФ'; ?></div>
                    </div>
                    <?php if ($model->physical_passport_isRf == 0) : ?>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Страна</div>
                        <div><?= $model->physical_passport_country ?: '—' ?></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Серия</div>
                        <div><?php echo $model->physical_passport_series ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Номер</div>
                        <div><?php echo $model->physical_passport_number ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Код подразделения</div>
                        <div><?php echo $model->physical_passport_department ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Кем выдан</div>
                        <div><?php echo $model->physical_passport_issued_by ?: '—'; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Дата выдачи</div>
                        <div><?php echo $model->physical_passport_date_output ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Адрес регистрации</div>
                        <div><?php echo $model->physical_address ?: '—'; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="pl-2 pr-2 mt-3 pt-3">
        <div class="row pl-1 pr-1">
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Наименование банка</div>
                        <div><?php echo $model->bank_name ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">БИК</div>
                        <div><?php echo $model->BIC ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">Р/с</div>
                        <div><?php echo $model->current_account ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">К/с</div>
                        <div><?php echo $model->corresp_account ?: '—'; ?></div>
                    </div>
                </div>
                <?php foreach ($additionalAccounts as $account): ?>
                    <div class="row" style="margin-top: -1rem;">
                        <div class="col-3 mb-3 pb-3">
                            <div><?php echo $account->bank_name ?: '—'; ?></div>
                        </div>
                        <div class="col-3 mb-3 pb-3">
                            <div><?php echo $account->bik ?: '—'; ?></div>
                        </div>
                        <div class="col-3 mb-3 pb-3">
                            <div><?php echo $account->rs ?: '—'; ?></div>
                        </div>
                        <div class="col-3 mb-3 pb-3">
                            <div><?php echo $account->ks ?: '—'; ?></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Юридический адрес</div>
                    <div><?php echo $model->legal_address ?: '—'; ?></div>
                </div>
            </div>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Фактический адрес</div>
                    <div><?php echo $model->actual_address ?: '—'; ?></div>
                </div>
            </div>
            <div class="col-12">
                <div class="mb-3 pb-3">
                    <div class="label weight-700 mb-3">Адрес для почты</div>
                    <div><?php echo $model->postal_address ?: '—'; ?></div>
                </div>
            </div>
            <div class="col-12">
                <div class="row">
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">КПП</div>
                        <div><?php echo $model->PPC ?: '—'; ?></div>
                    </div>
                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3"><?= ($model->company_type_id == CompanyType::TYPE_IP) ? 'ОГРНИП' : 'ОГРН' ?></div>
                        <div><?php echo $model->BIN ?: '—'; ?></div>
                    </div>

                    <div class="col-3 mb-3 pb-3">
                        <div class="label weight-700 mb-3">ОКПО</div>
                        <div><?php echo $model->okpo ?: '—'; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
