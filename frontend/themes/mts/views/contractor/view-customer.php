<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use frontend\models\Documents;
use frontend\rbac\permissions\document\Collate;
use frontend\rbac\permissions\document\Invoice;
use frontend\themes\mts\components\Icon;
use frontend\widgets\RangeButtonWidget;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */
/* @var $user Employee */
/* @var $activeTab integer */
/* @var $analyticsMonthNumber string */
/* @var $analyticsYearNumber string */
/* @var $sellingMonthNumber string */
/* @var $sellingYearNumber string */
/* @var $abcGroup integer */
/* @var $disciplineGroup integer */

$this->title = $model->nameWithType;
$this->context->layoutWrapperCssClass = 'customer';

$tabFile = ArrayHelper::remove($tabData, 'tabFile');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-store',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'side' => 'top'
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

$user = Yii::$app->user->identity;
$isStoreActive = $model->getStoreCompanyContractors()
    ->andWhere(['status' => StoreCompanyContractor::STATUS_ACTIVE])
    ->exists();
$hasStore = $model->getStoreCompanyContractors()->one();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$disabled = !$canAddStoreCabinet && !$isStoreActive;
$invCount = $company->getInvoiceLeft();

$contractorNavColumnClass = 'nav-item';
// todo
if ($model->type == Contractor::TYPE_CUSTOMER) {
    $tabInvoice = [
        'label' => ($tab == 'autoinvoice' ?
            '<span class="ia-tab pr-2" data-href="'.Url::current(['tab' => 'autoinvoice', 'edit' => null]).'">АвтоСчета</span>' :
            '<span class="ia-tab pr-2" data-href="'.Url::current(['tab' => '', 'edit' => null]).'">Счета</span>') .
            '<span class="ia-drop">'.Icon::get('shevron', ['class' => 'dropdown-btn-shevron']).'</span>' ,
        'dropDownOptions' => ['class' => 'form-filter-list list-clr'],
        'url' => Url::current(['tab' => $tab == 'autoinvoice' ? 'autoinvoice' : '', 'edit' => null]),
        //'active' => $tab == null || $tab == 'autoinvoice',
        'options' => [
            'class' => $contractorNavColumnClass . ' dropdown tab-label-dropdown ',
        ],
        'linkOptions' => [
            'class' => 'invoice-autoinvoice nav-link dropdown-btn' . ($tab == '' || $tab == 'autoinvoice' ? ' active' : ''),
            'data-toggle' => 'dropdown',
        ],
        'encode' => false,
        'items' => [
            ['label' => 'Счета', 'url' => Url::current(['tab' => '', 'edit' => null])],
            ['label' => 'АвтоСчета', 'url' => Url::current(['tab' => 'autoinvoice', 'edit' => null])],
        ],
    ];
} else {
    $tabInvoice = [
        'label' => 'Счета',
        'url' => Url::current(['tab' => null, 'edit' => null]),
        //'active' => $tab == null,
        'options' => [
            'class' => $contractorNavColumnClass,
        ],
        'linkOptions' => [
            'class' => 'nav-link' . ($tab == null ? ' active' : '')
        ],
    ];
}

$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->getInvoicesAuto()->exists();

$this->registerJs('
    $(document).on("change", "#activate_store_account:not(:checked)", function() {
        changeCabinet($("#activate_store_account").serialize());
    });
    $(document).on("click", "#add-store-account .yes", function() {
        changeCabinet({store_account: 1});
    });
        $(document).on("click", "#add-store-account .no", function() {
        $("#activate_store_account").prop("checked", false);
    });

    function changeCabinet(val) {
        var isChecked = $("#activate_store_account").prop("checked");
        $.post("' . Url::to(['store-account', 'id' => $model->id]) . '", val, function(data) {
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value != undefined) {
                if (data.value == false) {
                    $icon = "error";
                }
                $("#activate_store_account").prop("checked", data.value);
            } else {
                $("#activate_store_account").prop("checked", !isChecked);
            }
            var $value = $("#activate_store_account").is(":checked");
            if ($value) {
                $("#activate_store_account").removeAttr("data-toggle");
                $("#activate_store_account").removeAttr("data-target");
            } else if (data.canAddStoreCabinet == 1) {
                $("#activate_store_account").attr("data-toggle", "modal");
                $("#activate_store_account").attr("data-target", "#add-store-account");
            }

            if (data.label != undefined) {
                $(".contractor-store-label").text(data.label);
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    }

    var $customerBlockIn = $(".contractor-in-block");
    var $customerBlockOut = $(".contractor-out-block");
    $(".update-contractor").click(function (e) {
        if ($(".contractor-info-tab").hasClass("active")) {
            $customerBlockIn.show();
            $customerBlockOut.hide();
        } else {
            location.href = $(".contractor-info-tab > a").attr("href") + "&edit=1";
        }
    });

    $(".undo-contractor").click(function (e) {
        $customerBlockIn.hide();
        $customerBlockOut.show();
    });
');
?>
<a class="link mb-2" href="<?= Url::to(['/contractor/index', 'type' => $model->type]) ?>">
    Назад к списку
</a>

<!-- MTS -->
<div class="wrap wrap_padding_small pl-4 pr-3 pb-2 mb-2">
    <div class="pl-1 pb-1">
        <div class="page-in row">
            <div class="col-9 column pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between mb-3">
                        <h4 class="column mb-2"><?= Html::encode($this->title); ?></h4>
                        <div class="column">
                            <button class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                type="button"
                                data-toggle="modal"
                                title="Последние действия"
                                href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#info"></use>
                                </svg>
                            </button>
                            <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::UPDATE, [
                                'model' => $model,
                            ])
                            ): ?>
                                <a href="<?= Url::to(['/contractor/update', 'id' => $model->id, 'type' => $model->type, 'tab' => $tab]) ?>"
                                    class="button-regular button-regular_red button-clr w-44 mb-2 ml-1"
                                    title="Изменить">
                                    <svg class="svg-icon">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">Контакт</div>
                            <div><?= $model->getRealContactName() ?: '—' ?></div>
                        </div>
                        <div class="column mb-4 pb-2">
                            <div class="label weight-700 mb-3">Телефон</div>
                            <div><?= $model->getRealContactPhone() ?: '—' ?></div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="column pb-2">
                                    <div class="label weight-700 mb-3">E-mail</div>
                                    <div>
                                        <?php if ($model->getRealContactEmail()): ?>
                                            <a class="link" href="mailto:<?= $model->getRealContactEmail() ?>"><?= $model->getRealContactEmail() ?></a>
                                        <?php else: ?>
                                            —
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php /* if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                        <div class="col-12">
                            <div class="row">
                                <div class="column">
                                    <?= $hasStore ?
                                        ($isStoreActive ? 'Отключить кабинет' : 'Включить кабинет')
                                        : ($model->type == Contractor::TYPE_CUSTOMER ?
                                            Html::a('Создать кабинет покупателя', Url::to(['/contractor/sale-increase'], ['class' => 'link'])) :
                                            'Создать кабинет'); ?>
                                </div>
                                <div class="column">
                                    <div class="d-flex flex-nowrap align-items-center">

                                        <div class="switch <?= $disabled ? ' tooltip2-store' : ''; ?>" data-tooltip-content="<?= $disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : ''; ?>">
                                            <?= \yii\bootstrap4\Html::checkbox('store_account', $isStoreActive, [
                                                'id' => 'activate_store_account',
                                                'class' => 'switch switch-input input-hidden md-check',
                                                'disabled' => $disabled,
                                                'data-toggle' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                    (!$isStoreActive && !$disabled ?
                                                        (empty($model->director_email) ? 'modal' : 'modal') : null) :
                                                    'modal',
                                                'data-target' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                    !$isStoreActive && !$disabled ?
                                                        (empty($model->director_email) ?
                                                            '#empty-contractor-email' :
                                                            '#add-store-account') : null :
                                                    '#no-rights-store-account',
                                            ]) ?>
                                            <label class="switch-label" for="activate_store_account">
                                                <span class="switch-pseudo">&nbsp;</span>
                                            </label>
                                            <?php if ($disabled): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                    <span id="tooltip_pay-to-add-cabinet-<?= $model->id; ?>"
                                                          style="display: inline-block; text-align: center;">
                                                        Кабинет можно добавить после
                                                        <?= Html::a('оплаты', Url::to(['/contractor/sale-increase',
                                                            'activeTab' => Contractor::TAB_PAYMENT,
                                                        ])); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </div>

                                        <div class="tooltip-box ml-2 mr-2">
                                            <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title="Самый быстрый способ увеличить продажи товара – упростить процесс заказа товара. Для этого подключите покупателей к вашему складу!">
                                                <svg class="tooltip-question-icon svg-icon">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; */ ?>
                    </div>
                </div>
            </div>
            <div class="col-3 column pl-0">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, ['ioType' => $ioType]) && $model->company_id !== null): ?>
                    <div class="pb-2 mb-1">
                        <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                            <a class="button-regular button-clr button-regular_red w-100" href="<?= Url::to(['/documents/invoice/create', 'type' => $ioType, 'contractorId' => $model->id]); ?>">
                                <svg class="svg-icon mr-auto">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                                </svg>
                                <span class="ml-0 mr-auto">СЧЕТ</span>
                            </a>
                        <?php else : ?>
                            <button class="button-regular button-clr button-regular_red w-100 action-is-limited"
                                <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                                <svg class="svg-icon mr-auto">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                                </svg>
                                <span class="ml-0 mr-auto">СЧЕТ</span>
                            </button>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <div class="pb-2 mb-1">
                    <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                </div>
                <?php if ($model->type == Contractor::TYPE_CUSTOMER && $tab != 'autoinvoice') : ?>
                    <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT) && !$hasAutoinvoice) : ?>
                        <div class="pb-2 mb-1">
                            <?= Html::a('АвтоСчета', [
                                'create-autoinvoice',
                                'type' => Documents::IO_TYPE_OUT,
                                'id' => $model->id,
                            ], [
                                'class' => 'button-regular button-hover-content-red button-clr w-100',
                            ]) ?>
                        </div>
                    <?php endif ?>
                <?php endif; ?>
                <?php /* todo
                <?php if ($model->type == Contractor::TYPE_CUSTOMER && !in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_ACCOUNTANT, EmployeeRole::ROLE_ASSISTANT])): ?>
                <div class="pb-2 mb-1">
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index', 'PaymentReminderMessageContractorSearch' => [
                            'contractor' => $model->id,
                        ]]), [
                            'class' => 'button-regular button-hover-content-red button-clr w-100',
                        ]); ?>
                </div>
                <?php endif; ?>
                */ ?>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)): ?>
                <div class="pb-2 mb-1">
                    <?php
                        $url = Url::to(['/contractor/collate', 'step' => 'form', 'id' => $model->id]);
                        Modal::begin([
                            'id' => 'modal-collate-report',
                            'closeButton' => false,
                            'size' => 'modal-dialog-centered',
                            'toggleButton' => [
                                'tag' => 'button',
                                'id' => 'collate-report-button',
                                'label' => ' АКТ СВЕРКИ',
                                'class' => 'button-regular button-hover-content-red button-clr w-100',
                            ],
                        ]);

                        $pjax = Pjax::begin([
                            'id' => 'collate-pjax-container',
                            'enablePushState' => false,
                            'enableReplaceState' => false,
                            'linkSelector' => '.collate-pjax-link',
                            'timeout' => 5000,
                        ]);

                        $pjax->end();

                        Modal::end();

                        $this->registerJs('
                            $(document).on("click", "#collate-report-button", function() {
                                $.pjax.reload("#collate-pjax-container", {url: ' . json_encode($url) . ', push: false, replace: false, timeout: 5000})
                            });
                            $(document).on("pjax:complete", "#collate-pjax-container", function(event) {
                                refreshDatepicker();
                                refreshUniform();
                                $("#modal-collate-report").find(".dropup > a").first().click();
                            });
                            $(document).on("hidden.bs.modal", "#modal-collate-report", function () {
                                $("#collate-pjax-container").html("");
                            })
                            ');
                    ?>
                </div>
                <div>
                    <?= Html::a('Проверить ' . ($model->type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика'), [
                            '/dossier',
                            'id' => $model->id,
                        ], [
                            'class' => 'button-regular button-hover-content-red button-clr w-100',
                        ]) ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="nav-tabs-row">
    <?= Nav::widget([
        'id' => 'contractor-menu',
        'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
        'items' => [
            $tabInvoice,
            [
                'label' => 'Договоры',
                'url' => Url::current(['tab' => 'agreements', 'edit' => null]),
                //'active' => $tab == 'agreements',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($tab == 'agreements' ? ' active' : '')
                ]
            ],
            [
                'label' => 'Аналитика',
                'url' => Url::current(['tab' => 'analytics', 'edit' => null]),
                //'active' => $tab == 'analytics',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($tab == 'analytics' ? ' active' : '')
                ],
                'visible' => $ioType == Contractor::TYPE_CUSTOMER,
            ],
            [
                'label' => ($ioType == Contractor::TYPE_CUSTOMER) ? 'Продажи' : 'Закупки',
                'url' => Url::current(['tab' => 'selling', 'edit' => null]),
                //'active' => $tab == 'selling',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($tab == 'selling' ? ' active' : '')
                ],
                'visible' => ($ioType == Contractor::TYPE_CUSTOMER || $ioType == Contractor::TYPE_SELLER && !$model->is_agent)
            ],
            [
                'label' => 'Агентские отчеты',
                'url' => Url::current(['tab' => 'agent_report', 'edit' => null]),
                //'active' => $tab == 'agent_report',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($tab == 'agent_report' ? ' active' : '')
                ],
                'visible' => (FALSE && $ioType == Contractor::TYPE_SELLER && $model->is_agent)
            ],
            [
                'label' => 'Карточка',
                'url' => Url::current(['tab' => 'info', 'edit' => null]),
                //'active' => $tab == 'info',
                'options' => [
                    'class' => $contractorNavColumnClass . ' contractor-info-tab',
                ],
                'linkOptions' => [
                    'class' => 'nav-link' . ($tab == 'info' ? ' active' : '')
                ],
            ],
        ],
    ]); ?>
</div>

<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab active">
            <div class="pt-2">
                <div class="pt-1">
                    <?= $this->render("view/{$tabFile}", $tabData); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>

<div id="add-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            Вы уверены, что хотите создать личный кабинет для <br>
                            данного покупателя? После нажатия на кнопку ДА, <br>
                            покупателю будет отослано на e-mail письмо <br>
                            с приглашением в личный кабинет. <br>
                            В письме также будет логин и пароль для входа.
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <button type="button" class="yes button-regular button-width button-regular_red button-clr mr-3" data-dismiss="modal">Да</button>
                    <button type="button" class="no button-regular button-clr button-width button-hover-transparent" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="empty-contractor-email" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            У данного покупателя не заполнено поле с e-mail. <br>
                            Заполните в закладке "Карточка покупателя".
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="no-rights-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            У вас не достаточно прав для создания кабинета. Обратитесь к руководителю.
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

    <div class="created-by">
        <?= date('d.m.Y', $model->created_at) ?>
        Создал
        <?= $model->employee ? $model->employee->fio : ''; ?>
    </div>

<?php Modal::end(); ?>

<div style="display: none">
    <?php /* preload styles */ ?>
    <?= Select2::widget([
        'name' => 'empty',
        'data' => []
    ]); ?>
</div>

<?php $this->registerJs(<<<JS

$('.invoice-autoinvoice').on('click', function(e) {
    if ($(e.target).hasClass('ia-tab')) {
        $('#contractor-menu .dropdown-menu').remove();
        location.href = $(e.target).attr('data-href');
        return false;
    }
});

JS
);