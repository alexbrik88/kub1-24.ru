<?php

use frontend\models\CollateForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$company = $model->company;
$contractor = $model->contractor;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<h4 class="modal-title">Выберите, кому отправить акт сверки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<?php $form = ActiveForm::begin([
    'id' => 'send-collate-form',
    'action' => [
        '/contractor/collate',
        'step' => 'send',
        'id' => $contractor->id,
        'accounting' => $model->accounting,
        'document' => $model->document,
        'from' => $model->dateFrom,
        'till' => $model->dateTill,
    ],
    'options' => [
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group mb-0'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
]); ?>

<div class="row">
    <div class="form-group column mb-3 pb-3 pt-2">
        <?= $form->field($model, 'sendToChief', ['template' => "{label}\n{input}"])
            ->error(false)
            ->checkbox(); ?>
    </div>
    <div class="form-group column mb-3 pb-3">
        <div class="row">
        <?php if (empty($contractor->director_email)) : ?>
            <?php if (!empty($contractor->director_name)) : ?>
                <div class="column pt-2 text-muted" style="padding-right: 15px;">
                    <?= $contractor->director_name; ?>
                </div>
            <?php endif ?>
            <?= $form->field($model, 'chiefEmail', ['options' => ['class' => '']])
                ->label(false)
                ->textInput([
                    'placeHolder' => 'Укажите e-mail',
                    'class' => 'form-control width100',
                ]);?>
        <?php else : ?>
            <div class="column pt-2 text-muted">
                <?= $contractor->director_name; ?>
            </div>
            <div class="column pt-2 text-muted">
                <?= $contractor->director_email; ?>
            </div>
        <?php endif ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group column mb-3 pb-3 pt-2">
        <?= $form->field($model, 'sendToOther')->checkbox(); ?>
    </div>
    <div class="form-group column mb-3 pb-3">
        <?= $form->field($model, 'otherEmail')->label(false)->textInput([
            'placeHolder' => 'Укажите e-mail',
            'class' => 'form-control width100' . (isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? ' tooltip-self-send' : ''),
            'data-tooltip-content' => isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? '#tooltip_send-example' : null,
        ]); ?>
    </div>
</div>

<?php if (!$contractor->chief_accountant_is_director): ?>
    <div class="row">
        <div class="form-group column mb-3 pb-3 pt-2">
            <?= $form->field($model, 'sendToChiefAccountant')->checkbox(); ?>
        </div>
        <div class="form-group column mb-3 pb-3">
            <div class="row">
            <?php if (empty($contractor->chief_accountant_email)) : ?>
                <?php if (!empty($contractor->chief_accountant_name)) : ?>
                    <div class="column pt-2 text-muted" style="padding-right: 15px;">
                        <?= $contractor->chief_accountant_name; ?>
                    </div>
                <?php endif ?>
                <?= $form->field($model, 'chiefAccountantEmail', ['options' => ['class' => '']])
                    ->label(false)
                    ->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
            <?php else : ?>
                <div class="column pt-2 text-muted">
                    <?= $contractor->chief_accountant_name; ?>
                </div>
                <div class="column pt-2 text-muted">
                    <?= $contractor->chief_accountant_email; ?>
                </div>
            <?php endif ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (!$contractor->contact_is_director): ?>
    <div class="row">
        <div class="form-group column mb-3 pb-3 pt-2">
            <?= $form->field($model, 'sendToContact')->checkbox(); ?>
        </div>
        <div class="form-group column mb-3 pb-3">
            <div class="row">
                <?php if (empty($contractor->contact_email)) : ?>
                    <?php if (!empty($contractor->contact_name)) : ?>
                        <div class="column pt-2 text-muted" style="padding-right: 15px;">
                            <?= $contractor->contact_name; ?>
                        </div>
                    <?php endif ?>
                    <?= $form->field($model, 'contactEmail', ['options' => ['class' => '']])
                        ->label(false)
                        ->textInput([
                            'placeHolder' => 'Укажите e-mail',
                            'class' => 'form-control width100',
                        ]); ?>
                <?php else : ?>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->contact_name; ?>
                    </div>
                    <div class="column pt-2 text-muted">
                        <?= $contractor->contact_email; ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-12 mb-2">
        <?= $form->field($model, 'sendToChief', ['template' => "{error}", 'options' => ['class' => '']])->error(); ?>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <?= Html::activeLabel($model, 'emailText', [
            'class' => 'label',
        ]) ?>
        <div class="email_text_input" style="margin-top: -1px;">
            <?= $form->field($model, 'emailText')->textArea([
                'rows' => 10,
                'onkeyup' => 'emailTextResize()',
                'style' => 'padding: 10px; width: 100%; resize: none; overflow: hidden; border-color: #e5e5e5 !important;',
            ])->label(false); ?>
        </div>
    </div>
</div>

<div class="pt-3 d-flex justify-content-between">
    <div class="box-txt-come-your-mail">
        Ответное письмо клиента на данное письмо,
        <br>
        придет на вашу почту – <span><?= Yii::$app->user->identity->email; ?></span>
    </div>
    <?= Html::submitButton('ОК', [
        'class' => 'button-clr button-regular button-regular_red button-width-medium',
    ]); ?>
</div>

<?php $form->end(); ?>

<?php $pjax->end(); ?>
