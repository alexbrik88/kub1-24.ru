<?php

use common\models\employee\Employee;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\product\Store */
/* @var $form yii\widgets\ActiveForm */

$storeList = ['all' => 'Все'] + ArrayHelper::map($model->company->getEmployeeCompanies()->orderBy([
    'lastname' => SORT_ASC,
    'firstname' => SORT_ASC,
    'patronymic' => SORT_ASC,
])->all(), 'employee_id', 'fio');
?>

<?php if (Yii::$app->request->isAjax && $model->saved) : ?>
    <?= Html::script('
        $.pjax.reload("#store-pjax-container", {timeout: 5000});
        $(".modal.in").modal("hide");
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>

<?php $form = ActiveForm::begin([
    'id' => 'store-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
    ],
]); ?>

    <div class="row">
        <div class="col-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название склада') ?>
        </div>
        <?php if (!$model->is_main) : ?>
            <div class="col-6">
                <?= $form->field($model, 'responsible_employee_id')->widget(Select2::class, [
                    'data' => $storeList,
                    'options' => [
                        'placeholder' => '',
                        'value' => $model->responsible_employee_id ?: 'all'
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]) ?>
            </div>
        <?php endif ?>
    </div>

<?php if (!$model->is_main) : ?>
    <?= $form->field($model, 'is_main')->checkbox([], true) ?>

    <?php if (!$model->isNewRecord) : ?>
        <?= $form->field($model, 'is_closed')->checkbox([], true) ?>
    <?php endif ?>
<?php endif ?>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_red button-width button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end(); ?>

<?= Html::script('$("#store-form input:checkbox").uniform();', ['type' => 'text/javascript']) ?>