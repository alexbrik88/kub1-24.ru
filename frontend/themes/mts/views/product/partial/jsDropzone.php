<?php

use yii\helpers\Url;

/**
 * @var bool $immediately
 */
?>
<script>

    $(document).on('click', '.del-pic', function(e) {
        e.preventDefault();
        $(this).parents('.wrap-pic').find('.upload-pic, .view-pic').toggleClass('hidden');
        $(this).parents('.view-pic').find('input').val(1);
    });

    function createDropzone(number)
    {
        var block_id = "#dz-pic" + number;
        var attr = "pic" + number;

        var dropzone = new Dropzone(block_id, {
            'paramName': attr,
            'url': '<?= Url::to(['img-upload', 'id' => $model->id]) ?>',
            //'dictDefaultMessage':  '<div class="dz-help"><span class="link">Выберите файл</span><br>или перетащите сюда</div>',
            'dictInvalidFileType': 'Недопустимый формат файла',
            'maxFilesize': 5,
            'maxFiles': 1,
            'uploadMultiple': false,
            'acceptedFiles': "image/jpeg,image/png",
            'params': {'dz': true, 'immediately': <?= (int)$immediately ?>, 'attr': attr, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
        });
        dropzone.on('thumbnail', function (f, d) {
        });
        dropzone.on('success', function (f, d) {
            if (!d.status || d.status !== 'success') {
                alert(d.message);
                dropzone.removeAllFiles(true);
                dragEnd();
            } else {
                // ONLY IN VIEW MODE
                $('.product-pics').find('.upload-pic').filter('[data-number="'+number+'"]').addClass('hidden');
                $('.product-pics').find('.view-pic').filter('[data-number="'+number+'"]').removeClass('hidden').find('img').attr('src', d.src);
            }
            dropzone.removeAllFiles(true);
            dragEnd();
        });
        dropzone.on('error', function (f, d) {
            alert(d);
            dropzone.removeAllFiles(true);
            dragEnd();
        });
        dropzone.on('totaluploadprogress', function (progress) {
        });
        dropzone.on("addedfile", function(f) {
            dragEnd();
            $(block_id).find(".dz-help").hide();
        });
        dropzone.on("dragover", function(e) {
            dragStart();
        });
        dropzone.on("dragleave", function(e) {
            dragEnd();
        });
    }

    Dropzone.autoDiscover = false;
    for (number = 1; number <= 4; number++) {
        createDropzone(number);
    }

    function dragStart() {
        var blocks = $(".dz-upload-product-image");
        $(blocks).css({'background-color': '#e8eaf7'});
        $(blocks).find('.dz-help').hide();
        $(blocks).find('.dz-drag').show();
    }
    function dragEnd() {
        var blocks = $(".dz-upload-product-image");
        $(blocks).css({'background-color': '#fff'});
        $(blocks).find('.dz-help').show();
        $(blocks).find('.dz-drag').hide();
    }

    $(document).on("dragstart, dragover", function (e) {
        dragStart();
    });
    $(document).on("dragend, dragleave", function (e) {
        dragEnd();
    });

</script>