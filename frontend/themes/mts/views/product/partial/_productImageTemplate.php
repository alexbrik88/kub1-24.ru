<?php
use dosamigos\fileinput\FileInput;
use yii\helpers\Html; ?>

<?php if ($thumbnail) : ?>
    <style type="text/css">
        .fileinput:not(.fileinput-empty) .fileinput-cancel {
            display: none;
        }
        .fileinput.fileinput-empty .fileinput-clear,
        .fileinput.fileinput-empty .thumbnail-exists {
            display: none;
        }
    </style>
<?php endif ?>
<div class="fileinput fileinput-new" data-provides="fileinput">
    <div>
        <div class="btn-file" style="cursor: pointer;">
            <button class="fileinput-new button-list button-hover-transparent button-clr mr-2" style="margin-right: 10px;">
                <?= $this->render('//svg-sprite', ['ico' => 'download']) ?>
            </button>
            <button class="fileinput-exists btn yellow btn-sm" style="margin-right: 10px;">
                <?= $this->render('//svg-sprite', ['ico' => 'download']) ?>
            </button>
            <div style="width: 44px; height: 44px; position: absolute; left: 0; top: 0; opacity: 0;">
                <?=$field;?>
            </div>
            <?php if ($thumbnail) : ?>
                <button class="fileinput-new fileinput-clear button-list button-hover-transparent button-clr mr-2">
                    <?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?>
                </button>
                <button class="fileinput-cancel btn button-list button-hover-transparent button-clr mr-2">
                    <?= $this->render('//svg-sprite', ['ico' => 'repeat']) ?>
                </button>
            <?php endif ?>
            <button class="fileinput-exists btn button-list button-hover-transparent button-clr mr-2" data-dismiss="fileinput">
                <?= $this->render('//svg-sprite', ['ico' => 'repeat']) ?>
            </button>
        </div>
    </div>
    <div class="fileinput-new thumbnail" style="width: 200px; height: 200px; margin: 10px 0 0;">
        <div class="thumbnail-exists" style="width: 100%; height: 100%;">
            <?=$thumbnail;?>
        </div>
    </div>
    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 200px; margin: 10px 0 0;"></div>
</div>

<?php if ($thumbnail) : ?>
    <script type="text/javascript">
        $('.fileinput-clear').on('click', function (e) {
            e.preventDefault();
            $('#product-img_del').prop('checked', true);
            $(this).closest('.fileinput').addClass('fileinput-empty');
        });
        $('.fileinput-cancel').on('click', function (e) {
            e.preventDefault();
            $('#product-img_del').prop('checked', false);
            $(this).closest('.fileinput').removeClass('fileinput-empty');
        });
    </script>
<?php endif ?>
