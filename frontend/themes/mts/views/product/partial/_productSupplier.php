<?php

use common\components\TextHelper;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\themes\mts\helpers\Icon;
use kartik\select2\Select2;
use yii\bootstrap4\Html;
use \common\models\product\ProductToSupplier;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $pts ProductToSupplier */

$productToSupplier = ($model->loadedSuppliers) ?: ProductToSupplier::find()->where(['product_id' => $model->id])->orderBy('price')->all();
$allSuppliersIds = $model->company->getContractors()->andWhere(['type' => Contractor::TYPE_SELLER, 'is_deleted' => false])->select('id')->asArray()->column();
$supplierData = $model->getSuppliersData($allSuppliersIds, $model->production_type);

$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
$goodsClass = (!$isGoods) ? 'hidden' : '';

?>
<div class="pb-3">
    <?php $number = 0; ?>
    <?php foreach ($productToSupplier as $pts): ?>
        <?php $baseName = 'supplier[' . $number . ']'; ?>
        <div class="supplier-price-block row align-items-top mb-3 pr-3 supplier-<?= $pts->supplier_id ?>">
            <input type="hidden" name="<?= $baseName . '[id]' ?>" value="<?= $pts->supplier_id ?>">
            <div class="col-4 mb-4 pb-2 mr-auto">
                <div class="label weight-700 mb-3"><?= $pts->supplier->getShortName() ?></div>
                <div><?= $model->title ?></div>
            </div>
            <div class="col-2 column-price mb-4 pb-2">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Цена <?= ($isGoods) ? 'покупки' : 'покупки' ?></div>
                    <div class="pts-text">
                        <span><?= TextHelper::invoiceMoneyFormat($pts->price, 2) ?></span> ₽
                    </div>
                    <div class="pts-input hidden">
                        <input type="text" name="<?= $baseName . '[price]' ?>" value="<?= number_format($pts->price/100, 2, ',', '') ?>" class="form-control">
                        <p class="help-block-empty help-block help-block-error hidden">Необходимо заполнить.</p>
                        <p class="help-block-invalid help-block help-block-error hidden">Значение неверно.</p>
                    </div>
                </div>
            </div>
            <div class="col-2 column-article mb-4 pb-2 <?= $goodsClass ?>">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Артикул</div>
                    <div class="pts-text">
                        <span><?= $pts->article ?: '--' ?></span>
                    </div>
                    <div class="pts-input hidden">
                        <input type="text" name="<?= $baseName . '[article]' ?>" value="<?= $pts->article ?>" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-3 column-date mb-4 pb-2">
                <div class="pl-1 pr-1">
                    <div class="label weight-700 mb-3">Последнее поступление</div>
                    <div><span><?= isset($supplierData[$pts->supplier_id]) ? $supplierData[$pts->supplier_id]['date'] : '--' ?></span></div>
                </div>
            </div>
            <div class="col-1 view-buttons column mb-4 pb-2">
                <div class="">
                    <a href="javascript:;" class="edit-supplier link mb-2 mr-2" type="button">
                        <svg class="svg-icon svg-icon_size_">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                        </svg>
                    </a>
                    <a href="javascript:;" class="remove-supplier link mb-2 ml-1" type="button" data-supplier_id="<?= $pts->supplier_id ?>">
                        <svg class="svg-icon">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-1 edit-buttons column mb-4 pb-2 hidden">
                <div class="">
                    <a href="javascript:;" class="save-supplier link mb-2 mr-2" type="button">
                        <svg class="svg-icon svg-icon_size_">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#check"></use>
                        </svg>
                    </a>
                    <a href="javascript:;" class="cancel-supplier link mb-2 ml-1" type="button">
                        <svg class="svg-icon">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        <?php $number++; ?>
    <?php endforeach; ?>
    <div class="pt-3 pb-3 supplier-price-add">
        <button class="button-regular button-clr button-hover-content-red button-width collapsed" type="button" data-toggle="collapse" data-target="#addSomething" aria-expanded="false" aria-controls="addSomething">
            <svg class="svg-icon mr-2">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
            </svg>
            <span class="ml-1">Добавить</span>
        </button>
    </div>
    <div class="collapse" id="addSomething">
        <div class="pt-3 pb-3">
            <div class="page-border p-4">
                <div class="pt-2 pl-2 pr-2">
                    <div class="row">
                        <div class="form-group mb-0 col-12">
                            <div class="row">
                                <div class="form-filter col-6 mb-4 pb-2">
                                    <label for="testTabsInput" class="label">Поставщик <span class="important">*</span></label>
                                    <?php $exceptSupplierIds = ProductToSupplier::find()->where(['product_id' => $model->id])->select('supplier_id')->asArray()->column(); ?>
                                    <?php echo Select2::widget([
                                        'id' => 'product_supplier',
                                        'name' => 'product_supplier',
                                        'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить поставщика'] +
                                            Contractor::getALLContractorList(Contractor::TYPE_SELLER, false, $exceptSupplierIds),
                                        'options' => [
                                            'class' => 'form-control seller agent-field',
                                            'placeholder' => '',
                                        ],
                                        'pluginOptions' => [
                                            'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                                            'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                                            'width' => '100%'
                                        ]
                                    ]); ?>
                                    <p class="help-block-empty help-block help-block-error hidden">Необходимо заполнить.</p>
                                    <p class="help-block-present help-block help-block-error hidden">Поставщик уже есть в списке.</p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0 col-12">
                            <div class="row">
                                <div class="form-group column mr-5 mb-3 pb-3">
                                    <div class="label mb-3 pt-1">Наименование <?= ($isGoods) ? 'товара' : 'услуги' ?></div>
                                    <div><?= $model->title ?></div>
                                </div>
                                <div class="form-group column mb-3 pb-3">
                                    <label class="label" for="tabsInput2">Цена <?= ($isGoods) ? 'покупки' : 'покупки' ?> (₽)</label>
                                    <input class="form-control form-control_width_240" id="new_price" name="new_price" type="text">
                                    <p class="help-block-empty help-block help-block-error hidden">Необходимо заполнить.</p>
                                    <p class="help-block-invalid help-block help-block-error hidden">Значение неверно.</p>
                                </div>
                                <div class="form-group column mb-3 pb-3 <?= $goodsClass ?>">
                                    <label class="label" for="tabsInput3">Артикул</label>
                                    <input class="form-control form-control_width_240" id="new_article" name="new_article" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row justify-content-between">
                                <div class="form-group column mb-2">
                                    <button class="add-supplier button-clr button-regular button-regular_red button-width_123" type="button" data-number="<?= $number ?>">Сохранить</button>
                                </div>
                                <div class="form-group column mb-2">
                                    <button class="button-clr button-regular button-hover-grey button-width_123" type="button" data-toggle="collapse" data-target="#addSomething" aria-expanded="false" aria-controls="addSomething">Отменить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="supplier-template row align-items-top pr-3" style="display: none!important;">
    <input type="hidden" name="new-supplier-id" value="0" class="input-supplier-id">
    <div class="col-4 mb-4 pb-2 mr-auto">
        <div class="label weight-700 mb-3 supplier-name"></div>
        <div><?= $model->title ?></div>
    </div>
    <div class="col-2 column-price mb-4 pb-2">
        <div class="pl-1 pr-1">
            <div class="label weight-700 mb-3">Цена <?= ($isGoods) ? 'покупки' : 'покупки' ?></div>
            <div class="pts-text">
                <span>0</span> ₽
            </div>
            <div class="pts-input hidden">
                <input type="text" name="new-supplier-price" value="" class="form-control">
                <p class="help-block-empty help-block help-block-error hidden">Необходимо заполнить.</p>
                <p class="help-block-invalid help-block help-block-error hidden">Значение неверно.</p>
            </div>
        </div>
    </div>
    <div class="col-2 column-article mb-4 pb-2 <?= $goodsClass ?>">
        <div class="pl-1 pr-1">
            <div class="label weight-700 mb-3">Артикул</div>
            <div class="pts-text">
                <span></span>
            </div>
            <div class="pts-input hidden">
                <input type="text" name="new-supplier-article" value="" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-3 column-date mb-4 pb-2">
        <div class="pl-1 pr-1">
            <div class="label weight-700 mb-3">Последнее поступление</div>
            <div class="pts-text"><span></span></div>
        </div>
    </div>
    <div class="col-1 view-buttons column mb-4 pb-2">
        <div class="">
            <a href="javascript:;" class="edit-supplier link mb-2 mr-2" type="button">
                <svg class="svg-icon svg-icon_size_">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                </svg>
            </a>
            <a href="javascript:;" class="remove-supplier link mb-2 ml-1" type="button" data-supplier_id="0">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="col-1 edit-buttons column mb-4 pb-2 hidden">
        <div class="">
            <a href="javascript:;" class="save-supplier link mb-2 mr-2" type="button">
                <svg class="svg-icon svg-icon_size_">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#check"></use>
                </svg>
            </a>
            <a href="javascript:;" class="cancel-supplier link mb-2 ml-1" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </a>
        </div>
    </div>
</div>

<script>
    var jsSupplierData = <?=  json_encode($supplierData) ?>;
    $('#product_supplier').on('change', function() {
        var supplierId = $(this).val();
        if (jsSupplierData[supplierId]) {
            $('#new_price').val(jsSupplierData[supplierId].price);
        }
    });

    $(document).on('click', '.remove-supplier', function(e) {
        e.preventDefault();
        $('#delete-supplier').find('.modal-delete-supplier').data('supplier_id', $(this).data('supplier_id'));
        $('#delete-supplier').modal('show');
    });
    $(document).on('click', '.modal-delete-supplier', function(e) {
        e.preventDefault();
        $('.supplier-price-block').filter('.supplier-' + $(this).data('supplier_id')).remove();
        $('#delete-supplier').modal('hide');
    })
    $(document).on('click', '.edit-supplier', function(e) {
        e.preventDefault();
        var block = $(this).closest('.supplier-price-block');
        $(block).find('.pts-text, .pts-input').toggleClass('hidden');
        $(block).find('.edit-buttons, .view-buttons').toggleClass('hidden');
    });

    function validateSupplier(block)
    {
        $(block).find('.help-block').addClass('hidden');
        var price = $(block).find('.column-price').find('input');
        var article = $(block).find('.column-article').find('input');

        var isValidate = true;
        var check_price_val = Math.ceil($(price).val().replace(',', '.'));

        if (!$(price).val()) {
            $(price).siblings('.help-block-empty').removeClass('hidden');
            isValidate = false;
        }
        else if (isNaN(check_price_val) || check_price_val <= 0) {
            $(price).siblings('.help-block-invalid').removeClass('hidden');
            isValidate = false;
        }

        return isValidate;
    }

    $(document).on('click', '.save-supplier', function(e) {
        e.preventDefault();
        var block = $(this).closest('.supplier-price-block');

        if (validateSupplier(block)) {

            $(block).find('.column-price, .column-article').each(function () {
                var input = $(this).find('.pts-input > input');
                var text = $(this).find('.pts-text > span');
                $(text).html($(input).val().trim());
            });
            $(block).find('.pts-text, .pts-input').toggleClass('hidden');
            $(block).find('.edit-buttons, .view-buttons').toggleClass('hidden');
        }
    });
    $(document).on('click', '.cancel-supplier', function(e) {
        e.preventDefault();
        var block = $(this).closest('.supplier-price-block');
        $(block).find('.column-price, .column-article').each(function() {
            var input = $(this).find('.pts-input > input');
            var text  = $(this).find('.pts-text > span');
            $(input).val($(text).html());
        });
        $(block).find('.pts-text, .pts-input').toggleClass('hidden');
        $(block).find('.edit-buttons, .view-buttons').toggleClass('hidden');
    });

    function validateNewSupplier()
    {
        $('#addSomething').find('.help-block').addClass('hidden');
        var supplier = $('#product_supplier');
        var price = $('#new_price');
        var article = $('#new_article');

        var isValidate = true;
        var check_price_val = Math.ceil($(price).val().replace(',', '.'));

        if (!$(supplier).val()) {
            $(supplier).siblings('.help-block-empty').removeClass('hidden');
            isValidate = false;
        }
        if ($('.supplier-price-block.supplier-' + $(supplier).val()).length) {
            $(supplier).siblings('.help-block-present').removeClass('hidden');
            isValidate = false;
        }
        if (!$(price).val()) {
            $(price).siblings('.help-block-empty').removeClass('hidden');
            isValidate = false;
        }
        else if (isNaN(check_price_val) || check_price_val <= 0) {
            $(price).siblings('.help-block-invalid').removeClass('hidden');
            isValidate = false;
        }

        return isValidate;
    }

    $('#new_price, #new_article').on('click', function() {
        $(this).siblings('.help-block').addClass('hidden');
    });
    $('#product_supplier').on('change', function() {
        $(this).siblings('.help-block').addClass('hidden');
    });

    $(document).on('click', '.add-supplier', function(e) {
        if (validateNewSupplier()) {
            var button = $(this);
            var select = $('#product_supplier');
            var number = $(button).data('number') + 1;
            var basis_name = 'supplier[' + number + ']';
            var supplier_id = $(select).val();
            var supplier_name = $(select).find('option:selected').html();
            var price = $('#new_price').val();
            var article = $('#new_article').val();

            var template = $('.supplier-template').clone();
            $(template).addClass('supplier-price-block supplier-' + supplier_id).removeClass('supplier-template');
            $(template).find('.remove-supplier').data('supplier_id', supplier_id);
            $(template).find('.column-price').find('.pts-text > span').html(price);
            $(template).find('.column-article').find('.pts-text').html(article);
            $(template).find('.input-supplier-id').attr('name', basis_name + '[id]').val(supplier_id);
            $(template).find('.column-price').find('.pts-input > input').attr('name', basis_name + '[price]').val(price);
            $(template).find('.column-article').find('.pts-input > input').attr('name', basis_name + '[article]').val(article);
            $(template).find('.supplier-name').html(supplier_name);
            $('.supplier-price-add').before(template);
            $(template).show();

            if (jsSupplierData[supplier_id]) {
                $(template).find('.column-date').find('span').html(jsSupplierData[supplier_id].date);
            }

            $(select).val('').trigger('change');
            $('#new_price, #new_article').val('');
            $('#addSomething').collapse('hide');

            $(button).data('number', number);
        }
    });
</script>