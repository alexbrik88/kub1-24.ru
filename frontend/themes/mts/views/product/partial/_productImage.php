<?php

use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use dosamigos\fileinput\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $thumbnail string */
/* @var $field string */
/* @var $model \common\models\product\Product */

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

/*
echo $form->field($model, 'image', [
    'wrapperOptions' => [
        'class' => 'col-md-12',
        'style' => 'margin-top: 15px;'
    ],
    'hintOptions' => [
        'tag' => 'div',
        'class' => 'hidden',
    ],
    'horizontalCssClasses' => [
        'offset' => '',
        'hint' => 'col-sm-12',
    ],
])->widget(FileInput::className(), [
    'options' => ['accept' => 'image/*', 'multiple' => false],
    'thumbnail' => ($src = $model->getImageThumb()) ?
        Html::img($src, [
            'class' => 'product-photo-preview-form',
            'alt' => '',
        ]) :
        null,
    'customView' => '@frontend/views/product/partial/_productImageTemplate.php',
    'style' => FileInput::STYLE_CUSTOM,
])->hint(Html::activeCheckbox($model, 'img_del', ['label' => false]));

*/

?>

<div class="row pr-2 product-pics">
    <div class="row row_indents_s w-100 pr-3">
        <?php for ($number=1; $number<=4; $number++): ?>
            <?php $src = $model->getPictureThumb($number, 193, 136); ?>
            <div class="wrap-pic col-6 col-xl-3 d-flex flex-column mb-1 pb-2">
                <div data-number="<?= $number ?>" class="<?= !$src ? 'hidden' : '' ?> view-pic page-border page-border_grey page-border_min-height_234 d-flex flex-column justify-content-center align-items-center pt-4">
                    <div class="flex-shrink-0">
                        <?= Html::img($src, ['alt' => '']) ?>
                    </div>
                    <div class="del-pic link text-right" title="Удалить"><?= $this->render('//svg-sprite', ['ico' => 'garbage']) ?></div>
                    <input type="hidden" name="delete_pic[<?= $number ?>]" value="0"/>
                </div>
                <div data-number="<?= $number ?>" class="<?= $src ? 'hidden' : '' ?> upload-pic">
                    <div id="dz-pic<?= $number ?>" class="dz-upload-product-image">
                        <div class="dz-help"><span class="link">Выберите файл</span><br>или перетащите сюда</div>
                        <div class="dz-drag" style="display: none">Перетащите файл сюда</div>
                    </div>
                    <div class="dz-upload-product-tip">Рекомендуемый размер: 200x200.<br>Форматы: JPG, JPEG, PNG</div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>

<?= $this->render('@frontend/themes/mts/views/product/partial/jsDropzone', [
    'model' => $model,
    'immediately' => false
]) ?>
