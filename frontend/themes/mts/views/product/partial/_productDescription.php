<?php

use common\models\Contractor;
use common\models\product\Product;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\web\View;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

?>
<?= $form->field($model, 'comment_photo')->textarea([
    'maxlength' => true,
    'rows' => 4,
    'style' => 'width: 100%;'
]); ?>