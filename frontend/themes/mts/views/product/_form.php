<?php
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

if (!isset($canViewPriceForBuy)) {
    $canViewPriceForBuy = true;
}
?>
<div class="product-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'product-form',
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ],
        ],
    ])); ?>
    <div class="form-body update-product">
        <?= $this->render('partial/_mainForm', [
            'model' => $model,
            'form' => $form,
            'canViewPriceForBuy' => $canViewPriceForBuy,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<!-- Add new supplier -->
<div class="modal fade" id="add-new" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>