<?php

use kartik\select2\Select2;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\product\Product;

/* @var $this yii\web\View */

$JS = <<<JS
$(document).on("change", "#from_store_select, #to_store_select, #count", function() {
    $($(this).data("error")).addClass("hidden");
});
$(document).on("change", "#from_store_select", function() {
    var toValue = this.value;
    var fromInput = $($(this).data("from"));
    if (fromInput.val() == toValue) {
        $("option[value=\'"+toValue+"\']", fromInput).prop("selected", false);
    }
    $("option:disabled", fromInput).prop("disabled", false);
    $("option[value=\'"+toValue+"\']", fromInput).prop("disabled", true);
    if (fromInput.data("select2")) {
        fromInput.select2("destroy");
    }
    $.when(fromInput.select2(eval(fromInput.data("krajee-select2"))))
        .done(initS2Loading("cashorderflows-contractorinput", eval(fromInput.data("s2-options"))));
});
$(document).on("change", "#count", function() {
    var numberCountInput = $("input#number-count");
    if ($(this).find("input:checked").val() == 1) {
        numberCountInput.removeAttr("disabled");
    } else {
        numberCountInput.attr("disabled", true);
    }
});

$(document).on('click', 'button.move_to_store_apply', function() {
    var hasError = false;
    var toStore = $('#to_store_select').val();
    var fromStore = $('#from_store_select').val();
    var count = $("#count input:checked").val();
    var numberCount = $("#number-count").val();

    if (toStore == '') {
        hasError = true;
        $($('#to_store_select').data("error")).toggleClass("hidden", false);
    }
    if (fromStore == '') {
        hasError = true;
        $($('#from_store_select').data("error")).toggleClass("hidden", false);
    }
    if (count == 1 && numberCount == '') {
        hasError = true;
        $($("#count").data("error")).toggleClass("hidden", false);
    }

    if (hasError) return;

    $.ajax({
        type: 'post',
        url: $(this).data('url'),
        data: {
            to_store_id: toStore,
            from_store_id: fromStore,
            count_type: count,
            number_count: numberCount,
            product_id: $("input.product_checker:checkbox:checked").map(function(){
                return $(this).val();
            }).get(),
        },
        success: function() {
            location.reload();
        }
    });
})
JS;

$this->registerJs($JS);
?>

<?php Modal::begin([
    'id' => 'move_to_store_modal',
]); ?>

<h4 class="modal-title">Переместить на склад</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="move_to_group">
    <div class="form-group">
        Выберите склады, для перемещения выбранного товара
    </div>

    <div class="form-group">
        <label>
            Со склада
        </label>
        <?= Select2::widget([
            'id' => 'from_store_select',
            'name' => 'from_store_id',
            'data' => $storeList,
            'value' => ($store != 'all') ? $store : null,
            'options' => [
                'placeholder' => '',
                'data' => [
                    'from' => '#to_store_select',
                    'error' => '#from-store-error',
                ],
            ],
            'pluginOptions' => [
                'width' => '100%',
                'minimumResultsForSearch' => 5
            ]
        ]) ?>

        <div id="from-store-error" class="product-move-error hidden" style="color: #a94442;">
            Выберите, с какого склада переместить товар
        </div>
    </div>

    <div class="form-group">
        <label>
            На склад
        </label>
        <?= Select2::widget([
            'id' => 'to_store_select',
            'name' => 'to_store_id',
            'data' => $storeList,
            'options' => [
                'placeholder' => '',
                'data' => [
                    'from' => '#from_store_select',
                    'error' => '#to-store-error',
                ],
            ],
            'pluginOptions' => [
                'width' => '100%',
                'minimumResultsForSearch' => 5
            ]
        ]) ?>
        <div id="to-store-error" class="product-move-error hidden" style="color: #a94442;">
            Выберите, на какой склад переместить товар
        </div>
    </div>

    <div class="form-group">
        <label>
            Количество
        </label>
        <?= Html::radioList('count', Product::MOVE_ALL, Product::$moveTypes, [
            'id' => 'count',
            'data' => [
                'error' => '#count-error',
            ],
            'item' => function ($index, $label, $name, $checked, $value) {
                $radio = Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                            'style' => $value == Product::MOVE_PARTIAL ? 'margin-left: 110px;' : null,
                    ],
                ]);
                if ($value == Product::MOVE_PARTIAL) {
                    $radio .= Html::input('number', 'numberCount', 1, [
                        'class' => 'form-control',
                        'id' => 'number-count',
                        'min' => 1,
                        'max' => 10000000000,
                        'step' => 'any',
                        'disabled' => true,
                        'style' => 'display: inline-block;width: 30%;margin-left: 25px;margin-right: 5px;'
                    ]) . 'ед.';
                }

                return $radio;
            },
        ]); ?>
        <div id="count-error" class="product-move-error hidden" style="color: #a94442;">
            Необходимо указать количество товара для перемещения
        </div>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr move_to_store_apply',
        'data' => [
            'url' => Url::to(['/product/to-store']),
        ],
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php Modal::end(); ?>
