<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\themes\mts\components\Icon;
use frontend\widgets\ProductGroupDropdownWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $productionType integer
 */

$JS = <<<JS
$(document).on('click', 'button.move_to_group_apply', function() {
    $.ajax({
        type: 'post',
        url: $(this).data('url'),
        data: {
            group_id: $('#product_group_select').val(),
            product_id: $("input.product_checker:checkbox:checked").map(function(){
                return $(this).val();
            }).get(),
        },
        success: function() {
            location.reload();
        }
    });
})
JS;

$this->registerJs($JS);
?>

<?php Modal::begin([
    'title' => 'Переместить в группу',
    'id' => 'move_to_group_modal',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<div class="move_to_group">
    <div style="margin-bottom: 10px;">
        Выберите группу, в которую будут перемещены выбранные товары
    </div>

    <?= ProductGroupDropdownWidget::widget([
        'id' => 'product_group_select',
        'name' => 'product_id',
        'minimumResultsForSearch' => 5,
        'productionType' => $productionType,
        'pluginOptions' => [
            'width' => '100%',
        ]
    ]) ?>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr move_to_group_apply',
        'data' => [
            'url' => Url::to(['/product/to-group']),
        ],
    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]) ?>
</div>

<?php Modal::end(); ?>
