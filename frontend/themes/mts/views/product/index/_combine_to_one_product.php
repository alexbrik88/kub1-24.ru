<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<h4 class="modal-title">Объединить в один товар</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="move_to_group">
    <div class="quantity_fail hidden">
        Для объединения товаров в один, необходимо выбрать больше одного товара.
    </div>
    <div class="quantity_correct hidden">
        <div>
            Выберите товар, к которому будут объеденены другие товары.
            <div class="same-units-hint">
                Товары объединяются только с одинаковой единицей измерения.
            </div>
        </div>
        <table class="combine_product_table table table-style table-count-list" style="width: 100%;">
            <thead>
            <tr class="">
                <th></th>
                <th class="col_product_article<?= $articleCss ?>">Артикул</th>
                <th>Наименование</th>
                <th>Группа</th>
                <th>Единица измерения</th>
            </tr>
            </thead>
            <tbody class="combine_product_items">
            </tbody>
            <tfoot class="hidden">
            <tr class="template combine_target_row" style="cursor: pointer; border-top: 1px solid #ccc;">
                <td class="control"></td>
                <td class="col_product_article<?= $articleCss ?>"></td>
                <td class="col_product_name"></td>
                <td class="col_product_group"></td>
                <td class="col_product_unit"></td>
            </tr>
            </tfoot>
        </table>
    </div>

</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-width button-regular_red button-clr combine_to_one_apply quantity_correct',
        'data' => [
            'units' => '1',
            'url' => Url::to(['/product/combine']),
        ],

    ]) ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]) ?>
</div>