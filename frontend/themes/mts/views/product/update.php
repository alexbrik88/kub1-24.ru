<?php

use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$this->title = 'Редактировать ' . ($model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'товар' : 'услугу');

$this->context->layoutWrapperCssClass = 'page-good';

echo $this->render('_form', [
    'model' => $model,
    'canViewPriceForBuy' => $canViewPriceForBuy,
]);

?>