<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\product\Product;
use frontend\rbac\permissions;
use frontend\rbac\permissions\document\Invoice;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$this->title = Html::encode($model->title);

$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';

$isMoveable = $model->production_type == Product::PRODUCTION_TYPE_GOODS && max(0, $model->quantity) > 0;
switch ($model->status) {
    case Product::DELETED:
        $statusText = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товар удалён' : 'Услуга удалена';
        break;

    case Product::ARCHIVE:
        $statusText = 'Находится в архиве';
        break;

    default:
        $statusText = '';
        break;
}

$iconArchive = '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#archiev"></use></svg>';
$iconDelete = '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use></svg>';

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);

$backUrl = Url::previous('product_list');
?>

<div class="page-head row justify-content-between align-items-end pb-1">
    <?= Html::a('Назад к списку', $backUrl ?: [
        'index',
        'productionType' => $model->production_type,
    ], [
        'class' => 'link mb-2 column back-to-customers',
    ]) ?>
</div>

<?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS): ?>
    <?= $this->render('view/_goods', [
        'model' => $model,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]) ?>
<?php elseif ($model->production_type == Product::PRODUCTION_TYPE_SERVICE): ?>
    <?= $this->render('view/_service', [
        'model' => $model,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]) ?>
<?php endif; ?>


<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-end">
        <div class="column">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>',
                    Url::to(['create', 'productionType' => $model->production_type, 'fromProduct' => $model->id]), [
                    'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                ]) ?>
            <?php endif ?>
        </div>
        <div class="column">
            <button class="print-product button-clr button-regular button-width button-hover-transparent w-100" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#print"></use>
                </svg>
                <span>Печать</span>
            </button>
        </div>
        <?php if ($canUpdate): ?>
            <div class="column">
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'folder']).' <span>В группу</span>', '#move_to_group_modal', [
                'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                'data-toggle' => 'modal',
                ]) ?>
            </div>
        <?php endif; ?>
        <div class="column">
            <?php if (in_array($model->status, [Product::ACTIVE, Product::ARCHIVE])
                && Yii::$app->user->can(\frontend\rbac\permissions\Product::UPDATE)
            ) : ?>
                <?= \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $iconArchive . ($model->status == Product::ACTIVE ? '<span>В архив</span>' : '<span>Извлечь из архива</span>'),
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::to(['archive', 'productionType' => $model->production_type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => $model->status == Product::ACTIVE ? 'Перенести в архив?' : 'Извлечь из архива?',
                ]);
                ?>
            <?php endif; ?>

        </div>
        <div class="column">
            <?php if (!$model->is_deleted && Yii::$app->user->can(\frontend\rbac\permissions\Product::DELETE)): ?>
                <?= \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $iconDelete . '<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent w-100',
                    ],
                    'confirmUrl' => Url::to(['delete', 'productionType' => $model->production_type, 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
                ]);
                ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'basic',
    'title' => 'Последние действия',
    'options' => [
        'class' => 'doc-history-modal fade',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
    'footer' => Html::button('OK', [
        'class' => 'button-regular button-regular_red button-clr w-44',
        'data-dismiss' => 'modal',
    ]),
]) ?>

    <div class="created-by">
        <?= date('d.m.Y', $model->created_at) ?>
        Создал
        <?= $model->creator ? $model->creator->fio : ''; ?>
    </div>

<?php Modal::end(); ?>

<?= $this->render('index/move_to_group', [
    'productionType' => $model->production_type,
]); ?>
<div class="move_to_group_product_id" style="display: none!important;"><input type="checkbox" class="product_checker" checked value="<?= $model->id ?>"></div>

<style>
    @media print {
        .header, .footer, .page-sidebar, .page-head, .button-regular, .nav-tabs, .button-clr, .wrap_btns {
            display: none;
        }
        .tab-pane {
            display: block!important;
        }
        .wrap {
            margin:0 !important;
        }
    }
</style>

<script>
    $('.print-product').on('click', function(e) {
        e.preventDefault();
        window.print();
    })
</script>