<?php

use common\components\TextHelper;
use common\models\Contractor;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\product\ProductToSupplier;
use frontend\models\Documents;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions;

$company = $model->company;
$tabActive = Yii::$app->request->get('tab');

$suppliers = [];
$productToSupplier = ProductToSupplier::find()->where(['product_id' => $model->id])->orderBy('price')->all();
if ($productToSupplier) {
    foreach ($productToSupplier as $pts) {
        $suppliers[] = Html::a($pts->supplier->getShortName(),
            ['/contractor/view', 'type' => Contractor::TYPE_SELLER, 'id' => $pts->supplier->id],
            ['target' => '_blank', 'class' => 'nowrap']
        );
    }
}

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */
?>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-1">
    <div class="pl-1">
        <div class="page-in row">
            <div class="col-9 pr-4">
                <div class="pr-2">
                    <div class="row align-items-center justify-content-between pb-1">
                        <h4 class="column mb-2"><?= Html::encode($model->title); ?></h4>
                        <div class="column">
                            <a class="button-regular button-regular_red button-clr w-44 mb-2 mr-2"
                                title="Последние действия"
                                data-toggle="modal" href="#basic">
                                <svg class="svg-icon svg-icon_size_">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#info"></use>
                                </svg>
                            </a>
                            <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                                <a href="<?= Url::toRoute(['product/update', 'productionType' => $model->production_type, 'id' => $model->id,]); ?>"
                                   title="Редактировать" class="edit-product button-regular button-regular_red button-clr w-44 mb-2 ml-1">
                                    <svg class="svg-icon">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-11">
                            <div class="row">
                                <div class="col-3 mb-3">
                                    <div class="label weight-700 mb-2">Цена продажи</div>
                                    <div><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?> ₽</div>
                                </div>
                                <div class="col-3 mb-3">
                                    <div class="label weight-700 mb-2">НДС</div>
                                    <div><?= $model->priceForSellNds->name; ?></div>
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="label weight-700 mb-2">Группа услуги</div>
                                    <div><?= $model->group ? $model->group->title : Product::DEFAULT_VALUE; ?></div>
                                </div>
                                <div class="col-3 mb-3">
                                    <div class="label weight-700 mb-2">Ед. изм.</div>
                                    <div><?= $model->productUnit ? $model->productUnit->name : Product::DEFAULT_VALUE; ?></div>
                                </div>
                                <div class="col-4 mb-3">
                                    <div class="label weight-700 mb-2">Поставщик</div>
                                    <div>
                                        <?= ($suppliers) ? implode(', ', $suppliers) :  Product::DEFAULT_VALUE ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($model->status == Product::ARCHIVE): ?>
                <div class="col-3 pl-0">
                    <div class="button-regular mb-3 w-100 btn-status-archive">
                        <svg class="svg-icon">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#archiev"></use>
                        </svg>
                        <span>В архиве c <?= date('d.m.Y', $model->archived_at) ?></span>
                    </div>
                </div>
            <?php else: ?>
            <div class="col-3 pl-0">
                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)
                ) : ?>
                    <div class="pb-2 dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductSales" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span>Продажа</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductSales">
                            <div class="popup-dropdown-in overflow-hidden overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('Выставить счёт', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2]),
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Выставить счёт-договор', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 2, 'isContract' => 1]),
                                        ]) ?>
                                    </li>
                                    <li>
                                        <?= Html::a('Создать заказ', null, [
                                            'class' => 'nowrap add-to-order',
                                            'data-action' => Url::to(['documents/order-document/create',]),
                                        ]); ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">Накладная на возраст</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_IN)
                    && Yii::$app->user->can(permissions\document\Invoice::CREATE)
                ) : ?>
                    <div class="pb-2 dropdown popup-dropdown popup-dropdown_position-shevron">
                        <a class="button-regular w-100 button-hover-content-red" href="#" role="button" id="cardProductPurchase" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <span>Покупка</span>
                            <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="cardProductPurchase">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <!--<li><a class="nowrap" href="#">По накладной</a></li>-->
                                    <li>
                                        <?= Html::a('По счёту', null, [
                                            'class' => 'nowrap add-to-invoice',
                                            'data-action' => Url::to(['documents/invoice/create', 'type' => 1]),
                                        ]) ?>
                                    </li>
                                    <!--<li><a class="nowrap" href="#">По приходному ордеру</a></li>
                                    <li><a class="nowrap" href="#">Без документов</a></li>
                                    <li><a class="nowrap" href="#">Создать заказ</a></li>
                                    <li><a class="nowrap" href="#">Начальные остатки</a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="wrap wrap_padding_small pl-4 pr-3 pb-0">
    <div class="pl-1">
        <div class="nav-tabs-row row pb-3 mb-3">
            <?= Tabs::widget([
                'options' => [
                    'class' => 'nav nav-tabs w-100 mb-3 mr-3',
                ],
                'linkOptions' => [
                    'class' => 'nav-link',
                ],
                'tabContentOptions' => [
                    'class' => 'tab-pane pl-3 pt-3 pr-3',
                    'style' => 'width:100%'
                ],
                'headerOptions' => [
                    'class' => 'nav-item',
                ],
                'items' => [
                    [
                        'label' => 'Информация',
                        'content' => $this->render('_productInfo', [
                            'model' => $model,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . (!$tabActive || $tabActive == '_productInfo' ? ' active' : ''),
                            'data-tab' => '_productInfo'
                        ],
                        'active' => (!$tabActive || $tabActive == '_productInfo')
                    ],
                    [
                        'label' => 'Поставщики',
                        'content' => $this->render('_productSupplier', [
                            'model' => $model,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productSupplier' ? ' active' : ''),
                            'data-tab' => '_productSupplier'
                        ],
                        'active' => ($tabActive == '_productSupplier')
                    ],
                    [
                        'label' => 'Цены',
                        'content' => $this->render('_productPrice', [
                            'model' => $model,
                            'canViewPriceForBuy' => $canViewPriceForBuy,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productPrice' ? ' active' : ''),
                            'data-tab' => '_productPrice'
                        ],
                        'active' => ($tabActive == '_productPrice')
                    ],
                    [
                        'label' => 'Описание',
                        'content' => $this->render('_productDescription', [
                            'model' => $model,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productDescription' ? ' active' : ''),
                            'data-tab' => '_productDescription'
                        ],
                        'active' => ($tabActive == '_productDescription')
                    ],
                    [
                        'label' => 'Оборот',
                        'content' => $this->render('_productTurnover', [
                            'model' => $model,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productTurnover' ? ' active' : ''),
                            'data-tab' => '_productTurnover'
                        ],
                        'active' => ($tabActive == '_productTurnover')
                    ],
                    [
                        'label' => 'Аналитика',
                        'content' => $this->render('_productStocks', [
                            'model' => $model,
                        ]),
                        'linkOptions' => [
                            'class' => 'nav-link' . ($tabActive == '_productStocks' ? ' active' : ''),
                            'data-tab' => '_productStocks'
                        ],
                        'active' => ($tabActive == '_productStocks')
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>

<!-- Create Invoice With Product -->
<?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
<?= Html::hiddenInput('selection[]', $model->id); ?>
<?= Html::endForm(); ?>
<script>
    $(document).on("click", ".add-to-invoice, .add-to-order", function(e) {
        e.preventDefault();
        $("#product_checker_form").attr("action", this.dataset.action).submit();
    });
    $(document).on("click", ".edit-product", function(e) {
        e.preventDefault();
        var activeTab = $(".nav-link.active").data('tab');
        var url = $(this).attr("href");
        if (activeTab)
            url += "&tab=" + activeTab;

        location.href = url;

        return false;
    });
</script>
