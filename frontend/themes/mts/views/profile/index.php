<?php
/** @var $this yii\web\View */

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\TimeZone;

/** @var $employee \common\models\employee\Employee */

$changePasswordModel = new \frontend\models\ChangePasswordForm();
$changeEmailModel = new \frontend\models\ChangeEmailForm($employee);
$changeNotifyModel = new \frontend\models\ChangeNotifyForm();
$changeTimeZone = new \frontend\models\ChangeTimeZoneForm();
$changeNotifyModel->notifyNearlyReport = $employee->notify_nearly_report;
$changeNotifyModel->notifyNewFeatures = $employee->notify_new_features;

$this->title = 'Настройки профиля';

$this->context->layoutWrapperCssClass = 'profile-setup';
?>

<?php $flash = Yii::$app->session->getFlash('change_profile'); ?>
<?php if (!empty($flash)) {
    echo \yii\bootstrap\Alert::widget([
        'body' => $flash,
    ]);
} ?>

<div class="wrap pt-3 pl-4 pr-4 pb-2">
    <h4>Профиль: <?= $employee->currentEmployeeCompany->getFio(); ?></h4>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changepassword']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <div class="row align-items-top">
            <div class="col-12 mb-3">
                <h5>Изменение пароля</h5>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changePasswordModel, 'oldPassword')->label(null, [
                    'class' => 'label',
                ])->passwordInput([
                    'class' => 'form-control field-width inp_one_line_prof',
                ]); ?>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changePasswordModel, 'newPassword')->label(null, [
                    'class' => 'label',
                ])->passwordInput([
                    'class' => 'form-control field-width inp_one_line_prof',
                ]); ?>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changePasswordModel, 'newPasswordRepeat')->label(null, [
                    'class' => 'label',
                ])->passwordInput([
                    'class' => 'form-control field-width inp_one_line_prof',
                ]); ?>
            </div>
            <div class="col-12 pb-3 mb-3">
                <?php echo Html::submitButton('Изменить', [
                    'class' => 'button-clr button-regular button-regular_red',
                ]); ?>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changetimezone']),
            'class' => 'form-horizontal',
        ]); ?>
        <?php $changeTimeZone->newTimeZone = TimeZone::DEFAULT_TIME_ZONE; ?>
        <div class="row align-items-top">
            <div class="col-12 mb-3">
                <h5>Изменение часового пояса</h5>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label">Текущий часовой пояс</div>
                <div class="mt-2 pt-1"><?php $timeZone = TimeZone::findOne($employee->time_zone_id);
                    echo $timeZone->out_time_zone ?></div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changeTimeZone, 'newTimeZone')
                    ->widget(Select2::class, [
                        'data' => ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone'),
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]) ?>
            </div>
            <div class="col-4 pb-3 mb-3">

            </div>
            <div class="col-12 pb-3 mb-3">
                <?php echo Html::submitButton('Изменить', [
                    'class' => 'button-clr button-regular button-regular_red',
                ]); ?>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changeemail']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <div class="row align-items-top">
            <div class="col-12 mb-3">
                <h5>Изменение адреса электронной почты</h5>
            </div>
            <div class="col-4 pb-3 mb-3">
                <div class="label">Текущая электронная почта</div>
                <div class="mt-2 pt-1"><?php echo $employee->email; ?></div>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changeEmailModel, 'email')->label(null, [
                    'class' => 'label',
                ])->textInput([
                    'class' => 'form-control field-width inp_one_line_prof',
                ]); ?>
            </div>
            <div class="col-4 pb-3 mb-3">
                <?= $form->field($changeEmailModel, 'password')->label(null, [
                    'class' => 'label',
                ])->passwordInput([
                    'class' => 'form-control field-width inp_one_line_prof',
                ]); ?>
            </div>
            <div class="col-12 pb-3 mb-3">
                <?php echo Html::submitButton('Изменить', [
                    'class' => 'button-clr button-regular button-regular_red',
                ]); ?>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="wrap pt-3 pl-4 pr-4 pb-0">
    <div class="pt-3 pl-1 pr-1">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changenotify']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
        ]); ?>
        <div class="row align-items-top">
            <div class="col-12 mb-3">
                <h5 class="mb-3">Получение уведомлений</h5>
            </div>
            <div class="col-12">
                <?= $form->field($changeNotifyModel, 'notifyNewFeatures')->label(null, [
                    'class' => '',
                ])->checkbox(); ?>
            </div>
            <div class="col-12 mb-3">
                <?= $form->field($changeNotifyModel, 'notifyNearlyReport')->label(null, [
                    'class' => '',
                ])->checkbox(); ?>
            </div>
            <div class="col-12 pb-3 mb-3">
                <?php echo Html::submitButton('Изменить', [
                    'class' => 'button-clr button-regular button-regular_red',
                ]); ?>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>