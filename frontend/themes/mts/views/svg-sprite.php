<?php

/* @var $this yii\web\View */
/* @var $_params_ array */

$d = DIRECTORY_SEPARATOR;
$path = Yii::getAlias('@frontend')."{$d}views{$d}svg-sprite.php";

echo $this->renderPhpFile($path, $_params_);
