<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */

$this->title = 'Изменить сотрудника';
?>
<div class="employee-create">

    <?= $this->render('_form_employee_company', [
        'model' => $model,
        'salaryModel' => null
    ]); ?>

    <div class="wrap wrap_btns visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label ml-1 mr-1">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'button-clr button-regular button-regular_red pl-4 pr-4 ladda-button',
                    'form' => 'employee-form',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="column">
                <a href="<?= Url::to(['index']) ?>" class="button-clr button-regular button-hover-transparent pl-4 pr-4" type="button">
                    <span class="ml-1 mr-1">Отменить</span>
                </a>
            </div>
        </div>
    </div>

</div>
