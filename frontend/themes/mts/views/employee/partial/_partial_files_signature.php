<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 25.01.2019
 * Time: 15:11
 */

use common\assets\JCropAsset;
use devgroup\dropzone\DropZoneAsset;
use yii\web\View;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\EmployeeCompany;
use common\components\helpers\Html;
use common\components\image\EasyThumbnailImage;

/* @var $model EmployeeCompany
 * @var $attr string
 */

DropZoneAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];
JCropAsset::register($this)->jsOptions = ['position' => View::POS_HEAD];
if (empty($action)) {
    $action = 'view';
}
$dataArray = EmployeeCompany::$imageDataArray;
$imgPath = $model->getTmpImage($attr);
if (!$imgPath && $model->employeeSignature) {
    $imgPath = $model->employeeSignature->getImage();
}
$fileTypeText = 'Допустимый формат: jpg, jpeg, png.';
$uploadUrl = Url::toRoute(['/employee/img-upload', 'id' => $model->employee_id, 'attr' => $attr]);
$width = $dataArray[$attr]['width'];
$height = $dataArray[$attr]['height'];
$dummyText = "Перетащите файл изображения сюда или<br>кликните тут, чтобы выбрать его с вашего компьютера.";
$dummyText .= "<br>{$dataArray[$attr]['dummy_text']}.<br>{$fileTypeText}";
$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';
?>



<h4 class="modal-title">Загрузить подпись</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="portlet box darkblue" style="margin-bottom: 0;">
    <div class="portlet-body">
        <?php $pjax = Pjax::begin([
            'id' => "{$attr}-pjax-container",
            'enablePushState' => false,
            'timeout' => 5000,
        ]); ?>
        <?php if ($action === 'crop') : ?>
            <?= $this->render('_partial_files_crop', ['model' => $model, 'attr' => $attr]); ?>
        <?php elseif ($action === 'upload' || !is_file($imgPath)) : ?>
            <div id="employee_company_img_<?= $attr ?>" class="dropzone" style="position: relative; padding-bottom: 30px;">
                <div id="progress_<?= $attr ?>" class="progress progress-striped active"
                     style="position: absolute; bottom: 10px; left: 10px; right: 10px; margin: 0; background-color: #fff; display: none;">
                    <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                </div>
            </div>
            <script type="text/javascript">
                Dropzone.autoDiscover = false;
                var MODAL_dropzone_<?= $attr ?> = new Dropzone("#employee_company_img_<?= $attr ?>", {
                    'paramName': '<?= $attr ?>',
                    'url': '<?= $uploadUrl ?>',
                    'dictDefaultMessage': '<?= $dummyText ?>',
                    'dictInvalidFileType': '<?= $fileTypeText ?>',
                    'maxFilesize': 5,
                    'maxFiles': 1,
                    'uploadMultiple': false,
                    'acceptedFiles': "image/jpeg,image/png",
                    'params': {'<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
                    'previewTemplate': "<?= $DZpreviewTemplate ?>"
                });
                MODAL_dropzone_<?= $attr ?>.on('thumbnail', function (f, d) {
                    $('#progress_<?= $attr ?>').show();
                });
                MODAL_dropzone_<?= $attr ?>.on('success', function (f, d) {
                    pjaxImgFormMTS('/employee/img-form', '<?= $model->employee_id ?>', '<?= $attr ?>', 'crop');
                });
                MODAL_dropzone_<?= $attr ?>.on('error', function (f, d) {
                    alert(d);
                    this.removeAllFiles(true);
                    $('#progress_<?= $attr ?>').hide();
                    $('#progress_bar_<?= $attr ?>').width(0);
                });
                MODAL_dropzone_<?= $attr ?>.on('totaluploadprogress', function (progress) {
                    $('#progress_bar_<?= $attr ?>').width(progress + '%');
                });
            </script>
        <?php else : ?>
            <span id="del_<?= $attr; ?>" class="button-list button-hover-transparent button-clr mr-2" title="Редактировать"
                  onclick="pjaxImgForm('/employee/img-form', '<?= $model->employee_id; ?>', '<?= $attr; ?>', 'upload')"
                  style="position: absolute;right: 16px;top: 16px;;z-index: 11;">
                    <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
            </span>
            <div id="<?= $attr ?>-image-edit">
                <table class="company-image-preview">
                    <tr>
                        <td>
                            <?php if (is_file($imgPath)) : ?>
                                <?= EasyThumbnailImage::thumbnailImg($imgPath, $width, $height, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                            <?php else : ?>
                                <div class="col-md-12">
                                    <div class="portlet align-center company_img_dummy"><?= $dataArray[$attr]['dummy_text'] ?></div>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="mt-4 d-flex justify-content-between">
                <?= Html::button('Сохранить', [
                    'class' => "button-regular button-width button-regular_red",
                    'onclick' => "jcropImageAcceptedMTS('{$attr}')",
                ]) ?>
                <?= Html::button('Отменить', [
                    'class' => "button-regular button-width button-regular button-hover-transparent",
                    'onclick' => "jcropImageDeleteMTS('/employee/img-delete', '/employee/img-form', '{$model->employee_id}', '{$attr}')",
                ]) ?>
            </div>
        <?php endif; ?>
        <?php $pjax->end(); ?>
    </div>
</div>
