<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\TimeZone;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\EmployeeCompany;

/* @var $this yii\web\View */
/* @var $model EmployeeCompany */
/* @var $salaryModel \frontend\models\SalaryConfigForm */

$this->title = $model->fio;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
$editMode = \Yii::$app->request->isPost;
?>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\Employee::INDEX)): ?>
    <a class="link mb-2" href="<?= Url::to(['index']) ?>">Назад к списку</a>
<?php endif; ?>

<div class="employee-view mb-3">

    <div class="wrap pl-4 pr-3 pb-2">
        <div class="pl-1 pb-1">
            <div class="page-in row">
                <div class="col-9 column pr-4">
                    <div class="pr-2">
                        <div class="row align-items-center justify-content-between mb-3">
                            <h4 class="column mb-2"><?= Html::encode($model->fio ?: '(не задано)'); ?></h4>
                            <div class="column">
                                <button class="button-list button-hover-transparent button-clr mb-2 mr-2" type="button" data-toggle="modal" href="#basic">
                                    <svg class="svg-icon svg-icon_size_">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#info"></use>
                                    </svg>
                                </button>
                                <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Employee::UPDATE)): ?>
                                    <a href="<?= Url::to(['update', 'id' => $model->employee_id]) ?>" class="button-list button-hover-transparent button-clr mb-2 ml-1" title="Редактировать">
                                        <svg class="svg-icon">
                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                        </svg>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="column mb-4 pb-2 col-4">
                                <div class="label weight-700 mb-3">Должность</div>
                                <div><?= Html::encode($model->position ?: '—'); ?></div>
                            </div>
                            <div class="column mb-4 pb-2 col-4">
                                <div class="label weight-700 mb-3">Роль</div>
                                <div><?= $model->employeeRole->name ?: '—'; ?></div>
                            </div>
                            <div class="column mb-4 pb-2 col-4">
                                <div class="label weight-700 mb-3">Дата приема на работу</div>
                                <div><?= DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?: '—'; ?></div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="column mb-4 pb-2 col-4">
                                        <div class="label weight-700 mb-3">E-mail</div>
                                        <?php if ($model->email): ?>
                                            <div><a class="link" href="mailto:<?= $model->email; ?>"><?= $model->email; ?></a></div>
                                        <?php else: ?>
                                            <div>—</div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="column mb-4 pb-2 col-4">
                                        <div class="label weight-700 mb-3">Телефон</div>
                                        <div><?= $model->phone; ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="column col-4 mb-3">
                                        <div class="label weight-700 mb-3">Право подписи на документах</div>
                                        <div>
                                            <?php if ($model->can_sign && $model->signBasisDocument) : ?>
                                                ДА
                                                <?php // $signDocument = Html::encode($model->signBasisDocument->name) . ' №' . Html::encode($model->sign_document_number) . ' от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                            <?php else: ?>
                                                НЕТ
                                                <?php // $signDocument = '' ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3 column pl-0 text-center">
                    <?php if (!$model->is_working): ?>
                        <h4 style="color: #e30611">Уволен</h4>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap pl-2 pr-2 pb-2 pt-2">
        <div class="pl-1 pr-1 pt-1">
            <div class="nav-tabs-row">
                <?= Tabs::widget([
                    'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
                    'headerOptions' => ['class' => 'nav-item'],
                    'items' => [
                        [
                            'label' => 'Общая информация',
                            'linkOptions' => ['class' => 'nav-link active'],
                            'content' => $this->render('view/_tab_general_info', [
                                'model' => $model,
                            ]),
                        ],
                        [
                            'label' => 'Паспортные данные',
                            'linkOptions' => ['class' => 'nav-link'],
                            'content' => $this->render('view/_tab_passport', [
                                'model' => $model,
                            ]),
                        ],
                        [
                            'label' => 'Для отчета С3В-М',
                            'linkOptions' => ['class' => 'nav-link'],
                            'content' => $this->render('view/_tab_c3b', [
                                'model' => $model,
                            ]),
                        ],
                        //[
                        //    'label' => 'Расчет зарплаты',
                        //    'linkOptions' => ['class' => 'nav-link'],
                        //    'content' => $this->render('view/_tab_salary', [
                        //        'model' => $model,
                        //        'salaryModel' => $salaryModel
                        //    ]),
                        //]
                    ],

                ]); ?>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="uneditable created-by"
                     style="text-align: center">Создан
                    <span><?= date(DateHelper::FORMAT_USER_DATE, $model->created_at); ?></span>
                </div>
                <div class="text-center mt-3">
                    <button type="button" class="button-regular button-hover-transparent button-clr button-width-medium" data-dismiss="modal">Ок</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->id != $model->employee_id && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE)): ?>
    <div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
        <div class="row align-items-center">
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
            </div>
            <div class="column flex-xl-grow-1">
                <?php
                echo \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'garbage']).'<span>Удалить</span>',
                        'class' => 'button-clr button-regular button-width button-hover-transparent',
                    ],
                    'confirmUrl' => Url::to(['delete', 'id' => $model->employee_id, 'companyId' => Yii::$app->user->identity->company->id]),
                    'message' => 'Вы уверены, что хотите удалить сотрудника?',
                ]);
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>