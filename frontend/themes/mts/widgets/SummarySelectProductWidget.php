<?php

namespace frontend\themes\mts\widgets;

use yii\base\Widget;

class SummarySelectProductWidget extends Widget
{
    public $buttons = [];
    public $productionType;

    public function run()
    {
        return $this->render('summary_select_product', ['widget' => $this]);
    }
}
