<?php

use frontend\components\StatisticPeriod;
use common\models\employee\Employee;
use common\models\TimeZone;
use yii\helpers\Html;
use yii\helpers\Url;

/*  @var $cssClass string */
/*  @var $pjaxSelector string */

$url = Url::to('/site/statistic-range');

$this->registerJs("
$(document).on('submit', 'form.statistic-period-form', function (e) {
    e.preventDefault();
    console.log(this);
    $.post(
        this.action,
        $(this).serialize(),
        function (data) {
            if ($(this).data('pjax-container')) {
                $.pjax.reload($(this).data('pjax-container'), {timeout: 10000});
            } else if (data) {
                location.href = location.href;
            }
        }
    );
    return false;
});
");

$curFrom = StatisticPeriod::dateRange('from');
$curTill = StatisticPeriod::dateRange('to');

$from = [];
$till = [];

$from[1] = $till[1] = date_create('-1 days')->format('Y-m-d');
$from[2] = $till[2] = date('Y-m-d');
$day = date('N');
$from[3] = date_create('-'.($day+6).' days')->format('Y-m-d');
$till[3] = date_create('-'.$day.' days')->format('Y-m-d');
$from[4] = date_create('-'.($day-1).' days')->format('Y-m-d');
$till[4] = date_create('+'.(7-$day).' days')->format('Y-m-d');
$from[5] = date_create('first day of last month')->format('Y-m-d');
$till[5] = date_create('last day of last month')->format('Y-m-d');
$from[6] = date_create('first day of this month')->format('Y-m-d');
$till[6] = date_create('last day of this month')->format('Y-m-d');
$m = (date('n') - 1) % 3 + 1;
$from[7] = date_create('first day of -'.($m+2).' month')->format('Y-m-d');
$till[7] = date_create('last day of -'.($m).' month')->format('Y-m-d');
$from[8] = date_create('first day of -'.($m-1).' month')->format('Y-m-d');
$till[8] = date_create('last day of +'.(3-$m).' month')->format('Y-m-d');
$from[9] = (date('Y')-1) . '-01-01';
$till[9] = (date('Y')-1) . '-12-31';
$from[10] = date('Y') . '-01-01';
$till[10] = date('Y') . '-12-31';

$curName = StatisticPeriod::getSessionName();

$name = [
    1 => 'Вчера',
    2 => 'Сегодня',
    3 => 'Предыдущая неделя',
    4 => 'Эта неделя',
    5 => 'Предыдущий месяц',
    6 => 'Этот месяц',
    7 => 'Предыдущий квартал',
    8 => 'Этот квартал',
    9 => 'Предыдущий год',
    10 => 'Этот год',
];
?>

<div class="dropdown popup-dropdown popup-dropdown_calendar" data-id="calendar">
    <button class="button-regular button-hover-dark button-clr w-100" type="button" data-toggle="toggleVisible" data-target="calendar">
        <?= $this->render('//svg-sprite', ['ico' => 'calendar']) ?>
        <span><?= StatisticPeriod::getSessionName() ?></span>
    </button>
    <div class="dropdown-menu" data-id="calendar">
        <div class="popup-dropdown-in">
            <div class="row items-container" >
                <?php foreach (range(1, 10) as $i) : ?>
                    <?= Html::beginForm(['/site/statistic-range'], 'post', [
                        'class' => 'col-6 statistic-period-form',
                        'data-pjax-container' => $pjaxSelector,
                    ]) ?>
                        <?= Html::hiddenInput('name', $name[$i]) ?>
                        <?= Html::hiddenInput('date_from', $from[$i]) ?>
                        <?= Html::hiddenInput('date_to', $till[$i]) ?>
                        <?= Html::submitButton($name[$i], [
                            'class' => 'button-regular button-hover-red w-100' .
                                        ($name[$i] == $curName ? ' active' : ''),
                        ]) ?>
                    <?= Html::endForm() ?>
                <?php endforeach ?>
                <?= Html::beginForm(['/site/statistic-range'], 'post', [
                    'class' => 'col-12 statistic-period-form',
                    'data-pjax-container' => $pjaxSelector,
                ]) ?>
                    <input type="hidden" name="name" value="Указать диапазон">
                    <div class="row align-items-end">
                        <div class="col-6">
                            <div class="row range-box-datepicker">
                                <div class="col-6 pt-0 pb-0">
                                    <div class="text-grey text_size_14 mb-1">От</div>
                                    <div class="date-picker-wrap date-picker-wrap_inline date-picker-wrap_left date-picker-wrap_left_max mb-0">
                                        <div class="position-relative">
                                            <?= Html::textInput('date_from', $curFrom->format('d.m.Y'), [
                                                'class' => 'form-control date-picker-calendar date-from',
                                                'data-date-format' => 'dd.mm.yyyy',
                                                'data-maxdate' => $curTill->format('d.m.Y'),
                                                'pattern' => '\d{2}\.\d{2}\.\d{4}',
                                                'readonly' => true,
                                            ]) ?>
                                            <svg class="date-picker-icon svg-icon input-toggle" style="color: #c90000;">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6 pt-0 pb-0">
                                    <div class="text-grey text_size_14 mb-1">До</div>
                                    <div class="date-picker-wrap date-picker-wrap_inline date-picker-wrap_left mb-0">
                                        <div class="position-relative">
                                            <?= Html::textInput('date_to', $curTill->format('d.m.Y'), [
                                                'class' => 'form-control date-picker-calendar date-till',
                                                'data-date-format' => 'dd.mm.yyyy',
                                                'data-mindate' => $curFrom->format('d.m.Y'),
                                                'pattern' => '\d{2}\.\d{2}\.\d{4}',
                                                'readonly' => true,
                                            ]) ?>
                                            <svg class="date-picker-icon svg-icon input-toggle" style="color: #c90000;">
                                                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-6 pt-0 pb-0">
                                    <button class="button-regular button-regular_red w-100" type="submit">Применить</button>
                                </div>
                                <div class="col-6 pt-0 pb-0">
                                    <a class="button-regular button-hover-grey w-100" href="#" data-toggle="toggleVisible" data-target="calendar">Отменить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>
