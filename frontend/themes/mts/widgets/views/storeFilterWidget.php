<?php
use yii\helpers\Url;
/** @var string $currentName */
/** @var array $storeItems */
/** @var array $banking */
?>

<h4>
<div class="dropdown popup-dropdown popup-dropdown_storage">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="store-label"><?=($currentName)?></span>
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
        </svg>
    </a>
    <div class="dropdown-menu" aria-labelledby="cardProductTitle">
        <div class="popup-dropdown-in" id="user-bank-dropdown">
            <?php foreach ($storeItems as $storeItem): ?>
                <div class="form-edit form-edit_alternative" data-id="<?=$storeItem['id'] ?>">
                    <div class="form-edit-result pt-3 hide show" data-id="form-edit">
                        <div class="weight-400">
                            <a href="<?= Url::to($storeItem['url']) ?>" class="goto-store black-link">
                                <?= $storeItem['label'] ?>
                            </a>
                        </div>
                        <?php if ($storeItem['id'] > 0): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <button class="update-store ajax-modal-btn button-clr link" title="Редактировать" data-title="Редактировать склад" data-url="<?= Url::to(['/store/update', 'id' => $storeItem['id']]) ?>">
                                    <svg class="svg-icon">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                    </svg>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="store-template" style="display: none!important;" data-id="0">
                <div class="form-edit-result pt-3 hide show" data-id="form-edit">
                    <div class="weight-400">
                        <a href="<?= Url::to(['/product/index', 'productionType' => 1, 'store' => 0]) ?>" class="goto-store black-link">
                            Новый
                        </a>
                    </div>
                    <div class="form-edit-btns form-edit-btns_position_right_center">
                        <a class="update-store ajax-modal-btn button-clr link" title="Редактировать" data-title="Редактировать склад" href="<?= Url::to(['/store/update', 'id' => 0]) ?>">
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <hr class="popup-devider">
            <div class="popup-content pt-3 pb-3">
                <a class="add-store ajax-modal-btn black-link link_bold link_font-14 link_decoration_none nowrap button-clr" title="Добавить" data-title="Добавить склад" href="<?= Url::to(['/store/create']) ?>" data-url="<?= Url::to(['/store/create']) ?>">
                    <svg class="add-button-icon svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-2">Добавить склад</span>
                </a>
            </div>
        </div>
    </div>
</div>
</h4>

