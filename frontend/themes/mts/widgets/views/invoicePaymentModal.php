<?php

use common\components\helpers\Html;
use common\models\service\ServiceInvoiceTariff;
use frontend\models\PaymentInvoiceForm;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

$paymentInvoiceForm = new PaymentInvoiceForm($company);

$vkUrl = 'https://vk.com/share.php?url='.urlencode('https://kub-24.ru');

Modal::begin([
    'id' => 'modal-invoice-payment',
    'title' => 'Добавить счет',
    'size' => 'modal-dialog_width-535',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ]
]); ?>

    <?php $form = ActiveForm::begin([
        'id' => 'invoice-payment-form',
        'action' => Url::to(['/invoice-payment/online-payment']),
        'options' => [
            'enctype' => 'multipart/form-data',
            'target' => '_blank',
        ],
    ]); ?>

        <?= Html::activeHiddenInput($paymentInvoiceForm, 'tariffId'); ?>

    <?php ActiveForm::end(); ?>

    <div class="row blocks">
        <div class="col-6">
            <div class="one-block">
                <div class="tariff-name">
                    1 счет за <br> 150 руб.
                </div>
                <div class="tariff-info">
                    Вместе со счетом вы сможете создать акт, накладную и счет-фактуру
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="one-block">
                <div class="tariff-name">
                    3 счета за <br> 300 руб.
                </div>
                <div class="tariff-info">
                    Вместе со счетом вы сможете создать акт, накладную и счет-фактуру
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-6">
            <?= Html::button('Оплатить', [
                'class' => 'button-regular button-regular_red w-100 pay',
                'data-tariff' => ServiceInvoiceTariff::ONE_INVOICE,
                'data-href' => Url::to(['/invoice-payment/online-payment']),
            ]); ?>
        </div>
        <div class="col-6">
            <?= Html::button('Оплатить', [
                'class' => 'button-regular button-regular_red w-100 pay',
                'data-tariff' => ServiceInvoiceTariff::THREE_INVOICES,
                'data-href' => Url::to(['/invoice-payment/online-payment']),
            ]); ?>
        </div>
    </div>

<?php Modal::end(); ?>
<?php $this->registerJs('
    $(document).on("click", "#modal-invoice-payment button.pay", function () {
        $("#paymentinvoiceform-tariffid").val($(this).data("tariff"));
        $("form#invoice-payment-form").submit();
    });
    $("#modal-invoice-payment button.share-vk").click(function () {
        $("#vk-link-block").collapse("show");
        var win = window.open($(this).data("url"), "_blank");
        win.focus();
    });
    $(".submit-shared-post-link").click(function () {
        var $postLink = $(".shared-post-link");
        if ($postLink.val() !== "") {
            $.post($(this).data("url"), $postLink.serialize(), function (data) {
                if (data.result) {
                    alert(data.msg);
                    location.href = location.href;
                } else {
                    alert(data.msg);
                }
            });
        }
    });
'); ?>
