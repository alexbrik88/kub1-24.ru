<?php
use yii\helpers\Url;
/** @var string $currentName */
/** @var array $emoneyItems */
/** @var array $banking */
?>

<h4>
<div class="dropdown popup-dropdown popup-dropdown_storage">
    <a class="link link_title" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="emoney-label"><?=($currentName)?></span>
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
        </svg>
    </a>
    <div class="dropdown-menu" aria-labelledby="cardProductTitle">
        <div class="popup-dropdown-in" id="user-bank-dropdown">
            <?php foreach ($emoneyItems as $emoneyItem): ?>
                <div class="form-edit form-edit_alternative" data-id="<?= $emoneyItem['id'] ?>">
                    <div class="form-edit-result pt-3 hide show" data-id="form-edit">
                        <div class="weight-400">
                            <a href="<?= Url::to($emoneyItem['url']) ?>" class="goto-cashbox black-link">
                                <?= $emoneyItem['label'] ?>
                            </a>
                        </div>
                        <?php if ($emoneyItem['id'] > 0): ?>
                            <div class="form-edit-btns form-edit-btns_position_right_center">
                                <a class="update-cashbox ajax-modal-btn button-clr link" title="Редактировать E-money" data-title="Редактировать E-money" href="<?= Url::to(['/cash/e-money/update-emoney', 'id' => $emoneyItem['id']]) ?>">
                                    <svg class="svg-icon">
                                        <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                                    </svg>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="cash-template" style="display: none!important;" data-id="0">
                <div class="form-edit-result pt-3 hide show" data-id="form-edit">
                    <div class="weight-400">
                        <a href="<?= Url::to(['/cash/e-money/index', 'emoney' => 0]) ?>" class="goto-cashbox black-link">
                            Новый
                        </a>
                    </div>
                    <div class="form-edit-btns form-edit-btns_position_right_center">
                        <a class="update-cashbox ajax-modal-btn button-clr link" title="Редактировать E-money" data-title="Редактировать E-money" href="<?= Url::to(['/cash/e-money/update-emoney', 'id' => 0]) ?>">
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <hr class="popup-devider">
            <div class="popup-content pt-3 pb-3">
                <a class="ajax-modal-btn black-link link_bold link_font-14 link_decoration_none nowrap button-clr" title="Добавить E-money" data-title="Добавить E-money" href="<?= Url::to(['/cash/e-money/create-emoney']) ?>" data-url="<?= Url::to(['/cash/e-money/create-emoney']) ?>">
                    <svg class="add-button-icon svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span class="ml-2">Добавить E-money</span>
                </a>
            </div>
        </div>
    </div>
</div>
</h4>

