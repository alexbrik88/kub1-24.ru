<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.09.2018
 * Time: 14:43
 */

namespace frontend\themes\mts\widgets;


use backend\models\Bank;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\bank\BankingParams;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use common\models\employee\Employee;
use frontend\modules\cash\modules\banking\components\Banking;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\base\Widget;
use yii\bootstrap4\Dropdown;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class CheckingAccountantFilterWidget
 * @package frontend\widgets
 */
class CheckingAccountantFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $rs;
    /* @var \common\models\Company */
    public $company;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $result = null;
        $result .= $this->getAccountantList();
        $result .= $this->getAccountantModals();
        $result .= yii2tooltipster::widget([
            'options' => [
                'class' => '.tooltip_rs',
            ],
            'clientOptions' => [
                'theme' => ['tooltipster-kub'],
                'trigger' => 'hover',
                'position' => 'right',
            ],
        ]);

        Yii::$app->view->registerJs('
            $(".add-checking-accountant").on("click", function(e) {
                e.preventDefault();
                $("#add-company-rs").modal("show");
            });
            $("#user-bank-dropdown .link").on("click", function(e) {
                var target = $(this).attr("data-target");
                if (target)
                    $(target).modal("show");
            });
            $(document).on("beforeSubmit", "form.form-checking-accountant", function () {
                $this = $(this);
                $modal = $(this).closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"rs\" value=\"' . $this->rs . '\">");

                var serializedObj = {rs: "' . $this->rs . '"};
                $this.find("input:checkbox").each(function(){
                    serializedObj[this.name] = this.checked ? 1 : 0;
                });
                $this.find("input:text").each(function(){
                    serializedObj[this.name] = $(this).val();
                });

                $.post($(this).attr("action"), serializedObj, function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        $(".modal-backdrop").remove();
                        if ($this.attr("is_new_record") == 1) {
                            $this.find("input").val("");
                        }
                        $("#company_rs_update_form_list").remove();
                        $(".page-content").append("<div id=\"company_rs_update_form_list\"></div>");
                        $("#company_rs_update_form_list").html($(data.updateModals).html());
                        //$("ul#user-bank-dropdown").replaceWith(data.html);
                        $(".tooltip_rs").tooltipster({
                            "theme":["tooltipster-kub"],
                            "trigger":"hover",
                            "position":"right"
                        });
                        $("#company_rs_update_form_list input[type=\"checkbox\"]").uniform();
                        Ladda.stopAll();
                        
                        var block = $("#user-bank-dropdown");
                        var update_block = $(block).find(".form-edit").filter("[data-id="+data.id+"]");
                        
                        if ($(update_block).length) {
                            $(update_block).find("a.goto-cashbox").html(data.name);                        
                        } else {
                            var template = $(block).find(".cash-template").clone();
                            var href_main = $(template).find(".goto-cashbox").attr("href");
                            var href_update = $(template).find(".update-cashbox").attr("data-target");                        
                            $(template).addClass("form-edit form-edit_alternative").removeClass("cash-template").attr("data-id", data.id);
                            $(template).find(".goto-cashbox").attr("href", href_main.replace("rs=0", "rs=" + data.rs)).html(data.name);
                            $(template).find(".update-cashbox").attr("data-target", href_update.replace("rs-0", "rs-" + data.id));
                            $(".form-edit").last().before(template);
                            $(template).show();                        
                        }  
                        
                    } else {
                        $this.html($(data.html).find("form").html());
                    }
                });
                return false;
            });
        ');

        return $result;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    private function getAccountantList()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $list = null;
        $activeAccount = null;
        $bankItems = [];
        /* @var $bankArray CheckingAccountant[] */
        $bankArray = $this->company->getCheckingAccountants()
            ->groupBy('rs')
            ->indexBy('rs')
            ->orderBy('type')
            ->all();
        if ($bankArray) {
            foreach ($bankArray as $account) {
                $bankItems[] = [
                    'id' => $account->id,
                    'label' => Html::tag('span', $account->bank_name, [
                            'class' => 'tooltip_rs',
                            'title' => "р/с: {$account->rs}",
                        ]),
                    'url' => ['index', 'rs' => $account->rs],
                    'linkOptions' => [
                        'class' => $this->rs == $account->rs ? 'active' : '',
                    ],
                ];
                if ($this->rs == $account->rs) {
                    $activeAccount = $account;
                }
            }
            $banking = [
                'logo' => '',
                'text' => ''
            ];
            if ($activeAccount !== null && ($bankingClass = Banking::classByBik($activeAccount->bik)) !== null) {
                if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
                    $banking['logo'] = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [29, 29], [
                        'class' => 'mr-1',
                    ]);
                }
                if ($activeAccount->autoload_mode_id === null && $bankingClass::$hasAutoload) {
                    $route = ['/cash/bank/index'];
                    if (Yii::$app->getRequest()->get('rs') == $activeAccount->rs) {
                        $route['rs'] = $activeAccount->rs;
                    }
                    $banking['text'] = Html::a("Подгружать выписку автоматически", [
                        "/cash/banking/{$bankingClass::$alias}/default/index",
                        'account_id' => $activeAccount->id,
                        'p' => Banking::routeEncode($route),
                    ], [
                        'class' => 'banking-module-open-link',
                    ]);
                }
                if ($lastStatementUpload = CashBankStatementUpload::find()->where([
                        'rs' => $activeAccount->rs,
                        'company_id' => $this->company->id,
                        'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
                    ])->orderBy(['created_at' => SORT_DESC])->one()
                ) {
                    $banking['text'] = Html::tag('span', 'Выписка загружена ' . date_timestamp_set(new \DateTime, $lastStatementUpload->created_at)->format('d.m.Y в H:i'), [
                        'class' => 'tooltip3',
                        'title' => 'Последняя ' . ($lastStatementUpload->source == CashBankStatementUpload::SOURCE_BANK ? 'выгрузка' : 'автовыгрузка') . ' выписки из Вашего клиент-банка',
                    ]);
                }
            }
            $bankCount = count($bankItems);
            if ($bankCount > 0) {
                $currentBankName = $this->rs == 'all' ? ' Все счета' : Html::tag('span', $bankArray[$this->rs]->bank_name, [
                    'class' => 'tooltip_rs',
                    'title' => "р/с: {$bankArray[$this->rs]->rs}",
                ]);
                if ($bankCount > 1) {
                    $pageRoute['rs'] = $this->rs;
                    $bankItems[] = [
                        'id' => null,
                        'label' => 'Все счета',
                        'url' => ['index', 'rs' => 'all'],
                        'linkOptions' => [
                            'class' => $this->rs == 'all' ? 'active' : '',
                        ],
                    ];
                }
            }

            if ($bankCount > 0) {

                return $this->render('checkingAccountantFilterWidget', [
                    'currentBankName' => $currentBankName,
                    'bankItems' => $bankItems,
                    'banking' => $banking
                ]);
            }
        }

        return "<h4>".Html::encode($this->pageTitle)."</h4>";
    }

    /**
     * @return string
     */
    private function getAccountantModals()
    {
        $checkingAccountant = new CheckingAccountant();

        $modals = $this->render('@frontend/views/company/form/modal_rs/_create', [
            'checkingAccountant' => $checkingAccountant,
            'company' => null,
        ]);
        $modals .= '<div id="company_rs_update_form_list">' .
            $this->render('@frontend/views/company/form/_rs_update_form_list', [
                'model' => $this->company,
            ]) .
            '</div>';

        return $modals;
    }
}