<?php

namespace frontend\themes\mts\widgets;

use yii\base\Widget;

class SummarySelectContractorWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('summary_select_contractor', ['widget' => $this]);
    }
}
