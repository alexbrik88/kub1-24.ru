<?php

namespace frontend\themes\mts\helpers;

class Icon {

    const PLUS = '<svg class="add-button-icon svg-icon"><path fill-rule="evenodd" d="M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zm0-1.486c3.866 0 6.505-3.295 6.505-6.514 0-3.219-2.639-6.534-6.505-6.534s-6.534 3.2-6.534 6.524c0 3.323 2.668 6.524 6.534 6.524zm.667-9.847v2.666h2.666v1.334H8.667v2.666H7.333V8.667H4.667V7.333h2.666V4.667h1.334z"></path></svg>';
    const QUESTION = '<svg class="tooltip-question-icon svg-icon ml-2"><use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use></svg>';
    const INFO = '<svg class="tooltip-question-icon svg-icon ml-2"><use xlink:href="/images/svg-sprite/svgSprite.svg#info"></use></svg>';
    const REPEAT = '<svg class="tooltip-question-icon svg-icon ml-2"><use xlink:href="/images/svg-sprite/svgSprite.svg#repeat"></use></svg>';

    public static function get($id, $options = [])
    {
        return \Yii::$app->getView()->render('//svg-sprite', array_merge(['ico' => $id], $options));
    }
}
