<?php
namespace frontend\themes\mts\helpers;

class MtsHelper {

    const DECREASE_FROM_SUM = 9999999;

    /**
     * @param int $size
     */
    static public function setStatisticsFontSize($sum, $invoiceMoneyFormat = true)
    {
        if ($invoiceMoneyFormat)
            $sum = bcdiv($sum, 100);

        if ($sum <= self::DECREASE_FROM_SUM)
            return;

        echo <<<STYLE
<style>
.count-card-main {
    font-size: 28px!important;
}
.count-card-title {
    padding-bottom: 5px!important;
}    
</style>
STYLE;

    }
}
