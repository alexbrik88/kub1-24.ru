<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.08.2016
 * Time: 9:53
 */

use common\components\helpers\Html;

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */
/* @var \common\models\document\Invoice $invoice */
/* @var \yii\mail\BaseMessage $message */

?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff;border-bottom-width:4px;border-color:#0077A7;border-style:solid; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;border-collapse: initial;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign">
                        <?= Html::a(Html::img($message->embed(Yii::getAlias(Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . 'logo.png')), [
                            'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                        ]), Yii::$app->params['serviceSite'] . Yii::$app->params['uniSender']['utm']['logo'], [
                            'target' => '_blank',
                        ]); ?>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign"><?= Html::a('Вход', Yii::$app->params['serviceSite'] . Yii::$app->params['uniSender']['utm']['entrance'], [
                            'style' => 'font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;',
                            'target' => '_blank',
                            'id' => 'watch_offline',
                        ]); ?></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 20pt; padding-left: 30px; margin-top: 20px; line-height: 24px; margin-bottom: 20px; color: #000;">
                                        Платеж на сумму <?= $data['sum'] ?> руб. успешно зачислен!
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 0px;">
                                        Ваша подписка активирована.<br>
                                        Для использования сервиса КУБ, пройдите в личный кабинет.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" height="80"
                                    class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 25px; font-weight: bold;">
                                        С уважением,<br> Команда КУБ</p></td>
                            </tr>
                        </table>

                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <?= Html::a('kub-24.ru', Yii::$app->params['serviceSite'], [
                                            'target' => '_blank',
                                            'style' => 'color: #055EC3;',
                                        ]); ?>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <?= Html::a('личный кабинет', Yii::$app->params['serviceSite'] . '/site/login', [
                                            'target' => '_blank',
                                            'style' => 'color: #055EC3;',
                                        ]); ?>, в раздел настройки и
                                        убрать галочки в блоке «Получение
                                        уведомлений».</p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
