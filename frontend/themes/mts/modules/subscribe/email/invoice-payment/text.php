<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/18/15
 * Time: 1:17 PM
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */
/* @var \common\models\document\Invoice $invoice */

$urlManager = clone \Yii::$app->urlManager;
$urlManager->baseUrl = '/';

?>
Здравствуйте <?= $company->chief_firstname; ?>!

Счет на оплату сервиса kub-24.ru

Ссылка на счет <?= $urlManager->createAbsoluteUrl([
    '/bill/invoice/' . $invoice->uid,
]); ?>

С уважением,
Команда КУБ


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет, в раздел
настройки и убрать галочки в блоке «Получение уведомлений».
