<?php

use common\components\date\DateHelper;
use common\models\service\SubscribeTariffGroup;
use frontend\themes\mts\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */
/* @var \common\models\service\SubscribeTariffGroup $group */

$tariffArray = $group->actualTariffs;
$tariffData = [];
$dropdownData = [];
$dropdownItems = [];
if (in_array($group->id, SubscribeTariffGroup::$hasLimit)) {
    foreach ($tariffArray as $tariff) {
        $dropdownLabel = $group->limitLabel($tariff->tariff_limit);
        $key = yii\helpers\Inflector::slug($dropdownLabel);
        $tariffData[$key][] = $tariff;
        if (!isset($dropdownData[$key])) {
            $dropdownData[$key] = $dropdownLabel;
        }
    }
} else {
    $tariffData['tariff_tab_'.$group->id] = $tariffArray;
}

foreach ($dropdownData as $key => $label) {
    $dropdownItems[] = [
        'label' => $label,
        'url' => '#',
        'linkOptions' => [
            'class' => 'tariff-toggle-link',
            'data-target' => '#'.$key,
        ]
    ];
}
$isTabActive = true;
?>

<?php if ($tariffArray && $group->activeByCompany($company)) : ?>
    <div class="col-3 d-flex flex-column">
        <div class="wrap p-3 d-flex flex-column flex-grow-1">
            <div class="subscribe-tariff-box">
                <div class="bordered" style="height: 150px;">
                    <div class="row">
                        <div class="col-12 d-flex">
                            <div class="tariff-icon-wrap">
                                <div class="tariff-icon">
                                    <?= Icon::get($group->icon) ?>
                                </div>
                            </div>
                            <div class="tariff-group-name">
                                <?= implode('<br>ИП', explode(' ИП', $group->name)) ?>
                                <?php if ($group->hint) : ?>
                                    <div style="font-size: 13px; font-weight: normal; text-transform: none; line-height: normal;">
                                        <?= $group->hint ?>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="active-subscribe">
                                <?php if (isset($expiresArray[$group->id])): ?>
                                    <div style="color:#9198a0">
                                        Подписка активна до
                                        <?= date('d.m.Y', $expiresArray[$group->id]['date']) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bordered" style="height: 118px;">
                    <div class="tariff-rules">
                        <?php if ($dropdownItems) : ?>
                            <div class="dropdown mb-2">
                                <?= Html::tag('div', $dropdownItems[0]['label'], [
                                    'class' => 'dropdown-toggle form-title-link',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; cursor: pointer;',
                                ])?>
                                <?= \yii\bootstrap4\Dropdown::widget([
                                    'id' => 'invoices-count-dropdown',
                                    'class' => 'dropdown-menu form-filter-list list-clr',
                                    'items' => $dropdownItems,
                                ])?>
                            </div>
                        <?php elseif ($group->has_base) : ?>
                            Тариф "Выставление счетов"<?= $limit ? "<br/>до $limit счетов в месяц" : '';?><br/>
                            <strong>ВКЛЮЧЕН</strong> в стоимость<br/>
                            этого тарифа
                        <?php else : ?>
                            Тариф "Выставление счетов"<br/>
                            <strong>НЕ включен</strong> в стоимость<br/>
                            этого тарифа
                        <?php endif; ?>
                    </div>
                </div>

                <div class="subscribe-tariff-content">
                    <?php foreach ($tariffData as $tab => $tariffArray) : ?>
                        <?php
                        $tariff = reset($tariffArray);
                        $discount = $company->getDiscount($tariff->id);
                        $price = round($tariff->price - $tariff->price * $discount / 100);
                        $tariffDropdown = [];
                        foreach ($tariffArray as $t) {
                            $tariffDropdown[] = [
                                'label' => $t->getReadableDuration(),
                                'url' => '#',
                                'linkOptions' => [
                                    'class' => 'choose-tariff',
                                    'data' => [
                                        'tariff-id' => $t->id,
                                    ],
                                ],
                            ];
                        }
                        ?>
                        <?= Html::beginTag('div', [
                            'id' => $tab,
                            'class' => 'subscribe-tariff' . ($isTabActive ? '': ' hidden'),
                            'data' => [
                                'tariff-id' => $tariff->id,
                                'tariff-group-id' => $group->id,
                                'tariff-price' => $tariff->price,
                                'tariff-period' => $tariff->getReadableDuration(),
                                'companies' => '',
                            ],
                        ]) ?>

                        <div class="bordered">
                            <div class="tariff-options">
                                <div style="display: inline-block">
                                    <div class="tariff-price" data-tariff-price="<?= $price ?>" data-tariff-id="<?= $tariff->id ?>">
                                        <?php if ($discount) : ?>
                                            <span style="text-decoration: line-through;">
                                        <?= $tariff->price ?>
                                        ₽
                                    </span>
                                            <br>
                                            <span class="current-tariff-price">
                                        <?= $price ?>
                                    </span>
                                        <?php else : ?>
                                            <span class="current-tariff-price">
                                        <?= $tariff->price ?>
                                    </span>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div style="display: inline-block;">₽ /</div>
                                <div style="display: inline-block;">
                                    <?php if (count($tariffDropdown) == 1) : ?>
                                        <?= $tariffDropdown[0]['label'] ?>
                                    <?php else : ?>
                                        <div class="dropdown">
                                            <?= Html::tag('div', $tariffDropdown[0]['label'], [
                                                'class' => 'dropdown-toggle form-title-link',
                                                'data-toggle' => 'dropdown',
                                                'style' => 'display: inline-block; cursor: pointer;',
                                            ])?>
                                            <?= \yii\bootstrap4\Dropdown::widget([
                                                'id' => 'employee-rating-dropdown',
                                                'class' => 'dropdown-menu form-filter-list list-clr',
                                                'encodeLabels' => false,
                                                'items' => $tariffDropdown,
                                            ])?>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="tariff-price-per-month mt-2" style="">
                                <span class="current-tariff-price-per-month">
                                    <?= round($price / $tariff->duration_month) ?>
                                </span>
                                ₽ / <?= $group->perOneLabel() ?>
                            </div>
                        </div>
                        <div class="bordered">
                            <div class="row">
                                <div class="col-12">
                                    <?php foreach ($tariffArray as $key=>$t): ?>
                                        <div class="tariff-discount-proposition" data-tariff-id="<?= $t->id ?>" <?php if ($key>0) echo ' style="display:none"' ?>>
                                            <?= $t->proposition ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="bordered">
                            <div style="text-align: center">
                                <?php /*<div class="tariff-read-more" data-toggle="modal" data-target="#tariff-info-<?= $group->id ?>">Подробнее</div>*/ ?>
                                <?= Html::tag('button', 'Оплатить', [
                                    'class' => 'tariff button-clr button-regular button-regular_red tooltip2-pay tariff-pay-btn w-100',
                                    'data-tooltip-content' => '#tooltip_pay_'.$group->id,
                                ]) ?>
                            </div>
                        </div>

                        <?= Html::endTag('div') ?> <!-- .subscribe-tariff -->

                        <?php $isTabActive = false ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
