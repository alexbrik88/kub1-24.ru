<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.07.2017
 * Time: 15:54
 */

use common\models\Company;
use common\components\ImageHelper;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $company Company */
$company = Yii::$app->user->identity->company;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
?>
<div>
    Вами заработано по партнерской программе "Заработай вместе с КУБом" - <?= $company->affiliate_sum; ?> руб.
    <?= ImageHelper::getThumb('img/information-web-circular-button-symbol.png', [15, 15], [
        'class' => 'tooltip2',
        'style' => 'margin-left: 10px; cursor:pointer;',
        'data-tooltip-content' => '#tooltip_affiliate',
    ]); ?>
    <div class="tooltip_templates" style="display: none;">
        <span id="tooltip_affiliate" style="display: inline-block; text-align: left;">
            Привлечено пользователей: <?= $company->getInvitedCompanies()->count(); ?>
            <br>
            Пользователей оплатило: <?= $company->getPayedInvitedCompaniesCount(); ?>
        </span>
    </div>
</div>
