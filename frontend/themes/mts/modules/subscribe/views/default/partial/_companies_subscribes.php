<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\models\PaymentSearch;
use yii\db\Expression;
use common\models\company\ActiveSubscribe;
use common\components\helpers\ArrayHelper;

/** @var Company $company */
/** @var Company $cmp */

function getExpiresArray($company, $groupArray)
{
    $expiresArray = [];
    foreach ($groupArray as $group) {
        $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
        if ($subscribes) {
            $expiresArray[$group->id] = [
                'name' => $group->name,
                'date' => $date = SubscribeHelper::getExpireDate($subscribes),
                'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', ['n' => SubscribeHelper::getExpireLeftDays($date)]),
                'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
            ];
            $expiresArray[$group->id]['is_free'] = $group->id == SubscribeTariffGroup::STANDART && $company->isFreeTariff;
        } elseif ($group->id == SubscribeTariffGroup::STANDART) {
            $expiresArray[$group->id] = [
                'name' => 'Бесплатно',
                'date' => 0,
                'days' => '',
                'is_trial' => false,
                'is_free' => true
            ];
        }
    }

    return $expiresArray;
}

$ids = \Yii::$app->user->identity->getCompanies()->select('id')->column();

$paidCompanies = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->leftJoin(['companyActiveSubscribe' => ActiveSubscribe::tableName()], "{{company}}.[[id]] = {{companyActiveSubscribe}}.[[company_id]]")
    ->leftJoin(['subscribe' => Subscribe::tableName()], "{{companyActiveSubscribe}}.[[subscribe_id]] = {{subscribe}}.[[id]]")
    ->where(['{{companyActiveSubscribe}}.[[tariff_group_id]]' => ArrayHelper::getColumn($groupArray,'id')])
    ->andWhere(['NOT', ['{{subscribe}}.[[tariff_id]]' => SubscribeTariff::TARIFF_TRIAL]])
    ->isBlocked(false)
    ->all();

$paidCompanyArray = [];
foreach ($paidCompanies as $cmp) {

    $expiresArray = getExpiresArray($cmp, $groupArray);

    if ($expiresArray[1]['days'] || count($expiresArray) > 1) {

        $paidCompanyArray[] = [
            'id' => $cmp->id,
            'name' => $cmp->getShortName(),
            'expires' => $expiresArray,
            'days_left' => $expiresArray[1]['days'] !== '' ? $expiresArray[1]['days'] : 9999,
            'invoices_limit' => $cmp->getInvoiceLeft(),
        ];
    }
}

$unpaidCompanies = \Yii::$app->user->identity->getCompanies()
    ->alias('company')
    ->leftJoin(['companyType' => CompanyType::tableName()], "{{company}}.[[company_type_id]] = {{companyType}}.[[id]]")
    ->isBlocked(false)
    ->where(['NOT IN', '{{company}}.[[id]]', ArrayHelper::getColumn($paidCompanyArray, 'id')])
    ->all();

$unpaidCompanyArray = [];
foreach ($unpaidCompanies as $cmp) {
    $expiresArray = getExpiresArray($cmp, $groupArray);

    $unpaidCompanyArray[] = [
        'name' => $cmp->getShortName(),
        'expires' => $expiresArray,
        'days_left' => $expiresArray[1]['days'],
        'invoices_limit' => $cmp->getInvoiceLeft(),
    ];
}

usort($paidCompanyArray, function ($item1, $item2) {
    return $item1['days_left'] <=> $item2['days_left'];
});

usort($unpaidCompanyArray, function ($item1, $item2) {
    return $item1['invoices_limit'] <=> $item2['invoices_limit'];
});

$companies = array_merge($paidCompanyArray, $unpaidCompanyArray);

?>

<div class="wrap">
    <h4>Текущая подписка</h4>

    <table class="table table-style table-count-list">
        <thead>
            <tr>
                <th>Наименование</th>
                <th>Действующий тариф</th>
                <th>Осталось дней</th>
                <th>Активен до</th>
                <th>Осталось (счетов)</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach ($companies as $cmp): ?>
                <?php $expiresArray = $cmp['expires']; ?>
                <tr>
                    <td><?= $cmp['name'] ?></td>
                    <td>
                        <?php foreach ($cmp['expires'] as $group_id=>$exp): ?>
                            <?php if ($exp['is_free']): ?>
                                Бесплатно
                            <?php elseif ($exp['is_trial']): ?>
                                Пробный период
                            <?php else: ?>
                                <?= $exp['name'] ?>
                            <?php endif; ?>
                            <br/>
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?php foreach ($cmp['expires'] as $exp): ?>
                            <?= ($exp['days'] > 0) ? $exp['days'] : ''  ?>
                            <br/>
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?php foreach ($cmp['expires'] as $exp): ?>
                            <?= (!$exp['is_free']) ? date('d.m.Y', $exp['date']) : '' ?>
                            <br/>
                        <?php endforeach; ?>
                    </td>
                    <td>
                        <?= ($cmp['invoices_limit'] !== null) ? $cmp['invoices_limit'] : '' ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>