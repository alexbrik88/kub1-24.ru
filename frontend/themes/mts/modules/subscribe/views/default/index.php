<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/26/15
 * Time: 4:27 PM
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\service;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\models\AffiliateProgramForm;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\ActiveForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap4\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var \yii\web\View $this */
/* @var \yii\data\ActiveDataProvider $paymentDataProvider */
/* @var \frontend\modules\subscribe\models\PaymentSearch $paymentSearchModel */
/* @var service\Subscribe[] $actualSubscribes */
/* @var common\models\Company $company */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $tariffId integer */

$this->title = 'Оплата сервиса';

$paymentDataProvider->pagination->setPageSize(Yii::$app->request->get('per-page', 10));

$model = new PaymentForm($company);
$formData = $model->getFormData();

$session = Yii::$app->session;
$pdfUid = $session->remove('paymentDocument.uid');
$done = (Yii::$app->request->get('status') == 'done');
$paymentType = Yii::$app->request->get('payment-type');
$left = $company->getInvoiceLeft();

if ($done && $pdfUid) {
    $this->registerJs('
        var link = document.createElement("a");
        link.href = "' . Url::to(['document-print', 'uid' => $pdfUid]) . '"
        link.target = "_blank";
        document.body.appendChild(link);
        link.click();
    ');
}

if ($done && $paymentType == 3) {
    $this->registerJs('
       $(document).ready(function() {
        $("#info-after-pay").modal("show");
    })
    ');
}

$tariffArray = ArrayHelper::getValue($formData, 'tariffArray', []);
$tariffPrices = ArrayHelper::getValue($formData, 'tariffPrices', []);
$discountByCount = ArrayHelper::getValue($formData, 'discountByCount', []);
$groupArray = ArrayHelper::getValue($formData, 'groupArray', []);
$tariffDurations = ArrayHelper::getValue($formData, 'tariffDurations', []);
$tariffPerQuantity = ArrayHelper::getValue($formData, 'tariffPerQuantity', []);
$limit = SubscribeTariff::find()->select('tariff_limit')->actual()->where([
    'tariff_group_id' => SubscribeTariffGroup::STANDART,
])->orderBy(['price' => SORT_ASC])->scalar();

$expiresArray = [];
foreach ($groupArray as $group) {
    $subscribes = service\SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
    if ($subscribes) {
        $expiresArray[$group->id] = [
            'name' => $group->name,
            'date' => $date = service\SubscribeHelper::getExpireDate($subscribes),
            'days' => Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => service\SubscribeHelper::getExpireLeftDays($date),
            ]),
            'is_trial' => $group->id == SubscribeTariffGroup::STANDART && end($subscribes)->tariff_id == SubscribeTariff::TARIFF_TRIAL,
        ];
    }
}
//if (count($expiresArray) > 1 && $expiresArray[0]['is_trial']) {
//    $exp = array_shift($expiresArray);
//    $expiresArray[] = $exp;
//}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);
?>

<?= Yii::$app->session->getFlash('emptyCompany'); ?>

<?= $this->render('_style') ?>

<div class="wrap">
    <div class="row">
        <div class="column">
            <h4>Оплатить подписку</h4>
        </div>
        <div class="column ml-auto">
            <?= $this->render('_promoCodeForm', [
                'promoCodeForm' => new \frontend\modules\subscribe\forms\PromoCodeForm(),
            ]); ?>
        </div>
    </div>
    <div class="row accounts-list">
        <?= Html::beginTag('div', [
            'id' => 'payment-box-1',
            'class' => 'col-12',
            'data' => [
                'discounts' => $discountByCount,
                'prices' => $tariffPrices,
                'durations' => $tariffDurations,
                'quantity' => $tariffPerQuantity,
            ],
        ]) ?>

        <div id="subscribe-tariff-block" class="subscr-tariff-blk sum_payment_amount" style="">
            <?= $this->render('_payment_form_2', [
                'company' => $company,
                'tariffArray' => $tariffArray,
                'tariffId' => 1,
            ]); ?>
        </div>

        <?= Html::endTag('div') ?> <!-- #payment-box-1 -->

        <div style="display: none;">
            <?= Html::a('', null, [
                'id' => 'document-print-link',
                'target' => '_blank',
                'data-url' => Url::to(['document-print', 'uid' => 'pdfUid']),
            ]) ?>
        </div>
    </div>
</div>

<div id="subscribe-tariff-items" class="row">
    <?php foreach ($groupArray as $group) : ?>
        <?= $this->render('_tariff_block', [
            'company' => $company,
            'group' => $group,
            'discountByCount' => $discountByCount,
            'expiresArray' => $expiresArray,
            'limit' => $limit,
        ]) ?>
    <?php endforeach; ?>
    <div class="clearfix"></div>
</div>

<?= $this->render('partial/_companies_subscribes', [
    'company' => $company,
    'groupArray' => $groupArray
]); ?>

<?= $this->render('partial/_affiliate_program', [
    'company' => $company,
    'tariffArray' => $tariffArray,
]); ?>

<div class="wrap">
    <div class="row" style="padding-bottom:15px;border-bottom:1px solid #f2f3f7;">
        <div class="column">
            <h4 style="margin-bottom:0;">Тариф БЕСПЛАТНО</h4>
        </div>
        <div class="column ml-auto" style="color:#9198a0; font-size: 13px;">
            (подключается автоматически после окончания оплаты)
        </div>
    </div>
    <div class="row" style="padding-top:15px;">
        <div class="col-12">
            <div class="bold" style="padding-bottom:10px">Ограничения на тарифе:</div>
            <ul class="free-tariff-list">
                <li>Не более 5 счетов в месяц</li>
                <li>
                    Только 1 организация.
                    <span style="color:#9198a0; font-size: 13px;">
                        Т.е. если вы владеете более 1-й компании,
                        то вы не сможете добавить еще компании.
                    </span>
                </li>
                <li>Не более 3-х сотрудников</li>
                <li>Место на диске - 1 ГБ</li>
            </ul>
        </div>
    </div>
</div>

<?= $this->render('_payment_history', [
    'paymentDataProvider' => $paymentDataProvider,
    'paymentSearchModel' => $paymentSearchModel,
]); ?>

    <!-- Small modal -->
    <div class="modal fade" id="payment-box-2" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none; padding: 0">
                    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                        <svg class="svg-icon">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                        </svg>
                    </button>
                </div>
                <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                    <div class="text-center pad5">
                        <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                        <div>
                            <p>
                                Счет отправлен на
                                <br>
                                <strong id="send-to-email"></strong>.
                            </p>
                            <p>
                                Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                                <br>
                                <span>support@kub-24.ru</span>
                            </p>
                        </div>

                        <a class="button-list button-clr button-regular button-hover-transparent" data-dismiss="modal" style="color: #FFF;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="bill-invoice-remuneration" tabindex="-1"
         role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <h4 class="modal-title">Получить вознаграждение на счет</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <div class="modal-body">
                    <?php $affiliateForm = ActiveForm::begin([
                        'action' => Url::to(['/company/withdraw-money', 'page' => 'subscribe']),
                        'options' => [
                            'class' => 'form-horizontal form-checking-accountant',
                            'id' => 'form-bill-invoice-remuneration',
                        ],
                        'enableClientValidation' => true,
                        'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
                    ]); ?>

                    <div class="form-group">
                        Доступно к выводу: <b><?= $company->affiliate_sum; ?> руб.</b>
                    </div>

                    <?= $affiliateForm->field($affiliateProgramForm, 'withdrawal_amount', [
                        'options' => [
                            'class' => 'form-group',
                        ],
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                        'wrapperOptions' => [
                            'class' => 'form-filter',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])->textInput([
                        'placeholder' => 'Укажите сумму для вывода вознаграждения',
                    ])->label('Вывести:'); ?>

                    <?= $affiliateForm->field($affiliateProgramForm, 'type', [
                        'options' => [
                            'class' => 'form-group',
                        ],
                        'labelOptions' => [
                            'class' => 'label',
                        ],
                        'wrapperOptions' => [
                            'class' => 'form-filter',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])->radioList([
                        AffiliateProgramForm::RS_TYPE => 'на расчетный счет компании',
                        AffiliateProgramForm::INDIVIDUAL_ACCOUNT_TYPE => 'на счет физического лица',
                    ])->label(false); ?>

                    <div class="mt-3 d-flex justify-content-between">
                                <?= Html::submitButton('Оформить заказ', [
                                    'class' => 'button-clr button-regular button-regular_red',
                                ]); ?>
                                <?= Html::a('Отменить', null, [
                                    'class' => 'button-width button-clr button-regular button-hover-transparent',
                                    'data-dismiss' => 'modal'
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <span>После оформления заказа, наш сотрудник свяжется с вами и обсудит детали.</span>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="tooltip-template hidden">
        <?php foreach ($groupArray as $group) : ?>
            <span id="tooltip_pay_<?= $group->id ?>" class="tooltip_pay_type" style="display: inline-block; text-align: center;">
                <span class="pay-tariff-wrap">
                    <span class="bold">Тариф:</span>
                    <br>
                    "<?= $group->name ?>"
                </span>
                <span class="pay-tariff-wrap">
                    <span class="bold">Период:</span>
                    <br>
                    <span class="pay-tariff-period"></span>
                </span>
                <span class="pay-tariff-wrap">
                    <span class="bold">
                        За <span class="count-one">компанию</span><span class="count-many hidden">компании</span>:&nbsp;
                    </span>
                    <br>
                    <span class="pay-tariff-companies"></span>
                </span>
                <span class="bold pay-tariff-wrap-title">Способ оплаты</span>
                <?= Html::beginForm(['payment'], 'post', [
                    'class' => 'tariff-group-payment-form',
                ]) ?>
                    <?= Html::activeHiddenInput($model, 'tariffId', [
                        'class' => 'tariff-id-input',
                    ]) ?>
                    <?= Html::activeHiddenInput($model, 'companyList[]', [
                        'class' => 'company-list-input template',
                        'disabled' => true,
                    ]) ?>
                    <?= Html::tag('span', 'Картой', [
                        'class' => 'form-submit-button button-clr button-regular button-hover-transparent w-100 ladda-button',
                        'data-name' => Html::getInputName($model, 'paymentTypeId'),
                        'data-value' => PaymentType::TYPE_ONLINE,
                        'style' => 'margin-bottom:10px;'
                    ]); ?>
                    <?= Html::tag('span', 'Выставить счет', [
                        'class' => 'form-submit-button button-clr button-regular button-hover-transparent w-100 ladda-button',
                        'data-name' => Html::getInputName($model, 'paymentTypeId'),
                        'data-value' => PaymentType::TYPE_INVOICE,
                    ]); ?>
                <?= Html::endForm() ?>
            </span>
        <?php endforeach; ?>
    </div>

<?php
$js = <<<JS
$(document).ready(function() {
    var paymentBox = $('#payment-box-1');
    var discountArray = eval(paymentBox.data('discounts'));
    var pricesArray = eval(paymentBox.data('prices'));
    var durationsArray = eval(paymentBox.data('durations'));
    var quantityArray = eval(paymentBox.data('quantity'));

    var tariffItemsBlock = $('#subscribe-tariff-items');
    var promocodeBlock = $('#promo-code-block');
    var currentTariffPrice = 0;

    var pracesByDiscount = function(price, discount) {
        return (price - (price * discount / 100));
    }

    var recalculatePraces = function() {
        var checkedCompany = $('input.company-id-checker:checked');

        $('.subscribe-tariff:not(.hidden)', tariffItemsBlock).each(function () {
            var tariffGroupBlock = $(this);
            var tariffCompanies = [];
            var tariffCompaniesList = '';
            var tariffPrice = 0;
            var tariffGroupId = parseInt(tariffGroupBlock.data('tariff-group-id'));
            var tariffId = parseInt(tariffGroupBlock.data('tariff-id'));
            var tariffBasePrice = pricesArray[tariffId];
            var tariffDuration = durationsArray[tariffId];
            var tariffQuantity= quantityArray[tariffId];
            var tooltip = $('#tooltip_pay_' + tariffGroupId);
            var companyListInput = $('input.company-list-input:disabled', tooltip);

            $('input.tariff-id-input', tooltip).val(tariffId);
            $('input.company-list-input:enabled', tooltip).remove();

            checkedCompany.each(function () {
                var company = $(this);
                var tariffIds = eval(company.data('tariffs'))
                if (tariffIds.indexOf(tariffId) != -1) {
                    tariffCompanies.push(company);
                }
            });
            var itemCount = tariffCompanies.length;

            if (itemCount > 0) {
                var discounts = discountArray[tariffId];
                tariffCompanies.forEach(function (company) {
                    var discountByTariff = eval(company.data('discounts'))[tariffId];
                    var discountByCount = 0;
                    Object.keys(discounts).forEach(function (key) {
                        if (key <= itemCount) {
                            discountByCount = discounts[key];
                        }
                    });
                    tariffCompaniesList += company.closest('label').text() + '<br/>';
                    tariffPrice += pracesByDiscount(tariffBasePrice, Math.max(discountByTariff, discountByCount));
                    companyListInput.clone().val(company.val()).prop('disabled', false).insertAfter(companyListInput);
                });
            }

            tariffPrice = Math.round(tariffPrice);

            tariffGroupBlock.data('tariff-price', tariffPrice);
            $('.current-tariff-price', this).text(tariffPrice);
            $('.current-tariff-price-per-month', this).text(Math.round(tariffPrice / tariffQuantity));
            $('.pay-tariff-period', tooltip).html(tariffGroupBlock.data('tariff-period'));
            $('.pay-tariff-companies', tooltip).html(tariffCompaniesList);
            $('.count-one', tooltip).toggleClass('hidden', itemCount > 1 ? true : false);
            $('.count-many', tooltip).toggleClass('hidden', itemCount > 1 ? false : true);
        });
    }
    var processingRequestData = function(data) {
        Ladda.stopAll();
        if (data.content) {
            $('#payment-box-2 .modal-body').html(data.content);
        } else if ($('#payment-box-2').is(':visible')) {
            $('#payment-box-2').modal('hide');
        }
        if (data.alert && typeof data.alert === 'object') {
            for (alertType in data.alert) {
                var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                $.alert('#js-alert', data.alert[alertType], {type: alertType, topOffset: offset});
            }
        }
        if (data.done) {
            if (data.email && (data.type == '2' || data.type == '3')) {
                $("#send-to-email").html(data.email);
                $("#info-after-pay").modal("show");
            }
            if (data.link) {
                $('#document-print-link').attr('href', data.link);
                $('#document-print-link')[0].click();
            }
        }
    }

    $(document).on("click", ".choose-tariff", function(e) {
        e.preventDefault();
        var tariff = $(this);
        var tariff_id = tariff.data('tariff-id');
        var subscribeTariff = tariff.closest('.subscribe-tariff');
        subscribeTariff.data('tariff-id', tariff_id);
        subscribeTariff.data('tariff-period', tariff.html());

        tariff.closest('.dropdown').removeClass('open').find('.dropdown-toggle').html(tariff.html());

        $('.tariff-discount-proposition', subscribeTariff).hide();
        $('.tariff-discount-proposition[data-tariff-id="'+tariff_id+'"]', subscribeTariff).show();

        recalculatePraces();
    });

    $(document).on("change", "input.company-id-checker", function(e) {
        recalculatePraces();
    });

    $(document).on('click', '.subscribe-tariff .tariff', function (e) {
        var subscribeTariff = $(this).closest('.subscribe-tariff');

        $('.subscribe-tariff').removeClass('active');
        $(subscribeTariff).addClass('active');
    });

    $(document).on('submit', 'form.tariff-group-payment-form', function (e) {
        e.preventDefault();
    });

    $(document).on('click', 'form.tariff-group-payment-form .form-submit-button', function (e) {
        e.preventDefault();
        var button = $(this);
        var form = button.closest('form');
        var formData = form.serializeArray();
        formData.push({ name: button.data('name'), value: button.data('value') });
        $.post(form.attr('action'), formData, function(data) {
            processingRequestData(data);
        });
    });

    $(document).on('click', 'a.tariff-toggle-link', function (e) {
        e.preventDefault();
        var toggleBox = $(this).closest('.dropdown');
        toggleBox.toggleClass('open', false);
        toggleBox.find('.dropdown-toggle').text($(this).text());
        $('li', toggleBox).toggleClass('active', false);
        $(this).closest('li').toggleClass('active', true);
        var target = $($(this).data('target'));
        var targetWrap = target.closest('.subscribe-tariff-content');
        $('.subscribe-tariff', targetWrap).toggleClass('hidden', true);
        target.toggleClass('hidden', false);
        recalculatePraces();
    });

    recalculatePraces();
});
JS;
$this->registerJs($js);
?>