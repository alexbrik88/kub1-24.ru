<?php
/**
 * @var $this  yii\web\View
 */

use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

$ofdArray = Ofd::$modelClassArray;
?>

<style>
    .bank-logo-preloader {
        position: absolute;
        top: 0;
        left: 0;
        display: block;
        padding: 40px 0 0 65px;
    }
</style>

<h4 class="modal-title">Загрузка выписки из ОФД</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body">

    <div class="statement-service-content" style="position: relative; min-height: 110px; font-size:14px">
        <span style="font-size:16px; font-weight: bold">Выберите оператора ОФД:</span>
        <div style="min-height: 100px;position:relative">
            <?php foreach ($ofdArray as $ofd) : ?>
                <?= Html::beginTag('a', [
                    'href' => Url::to([
                        "/cash/ofd/{$ofd::$alias}/default/index",
                        'p' => Yii::$app->request->get('p')
                    ]),
                    'class' => 'ofd-module-link',
                ]) ?>
                    <div class="cont-img_bank-logo">
                        <img class="bank-logo" style='padding-right: 15px; cursor: pointer;' src="/img/icons/<?=$ofd::$alias?>.png" width="150" height="100"/>
                    </div>
                    <div class="bank-logo-preloader" style="display: none">
                        <img width="32" height="32" src="/img/ofd-preloader.gif"/>
                    </div>
                <?= Html::endTag('a') ?>

            <?php endforeach; ?>

        </div>
        <?= Html::a('Отменить', [
            '/cash/ofd/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'button-clr button-width button-regular button-hover-transparent ofd-module-link',
            'style' => 'float:right;',
        ]); ?>
        <div class="statement-loader"></div>
    </div>
</div>

<script>
    $('.ofd-module-link').on('click', function() {
        $(this).css({'opacity':'0.5'});
        $(this).find('.bank-logo-preloader').show();
    });
</script>