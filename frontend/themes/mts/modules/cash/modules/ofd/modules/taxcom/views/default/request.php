<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdModel
 */

use common\components\date\DateHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\ofd\components\AccountSelectWidget;

$outlets = \yii\helpers\ArrayHelper::map((array)$model->outlet_list, 'id', 'name');
$kkts = \yii\helpers\ArrayHelper::index((array)$model->kkt_list, 'kktRegNumber');
$fns  = \yii\helpers\ArrayHelper::map((array)$model->fn_list, 'fn', 'fn', 'kktRegNumber');

$choose_kkt = [];
foreach ($kkts as $value) {
    $outlet_name = isset($outlets[$value['outlet_id']]) ? $outlets[$value['outlet_id']] : '';
    $choose_kkt[$value['kktRegNumber']] =  $value['name'] . ' ('.$outlet_name.')';
}

$getOfdRows = [];
foreach ((array)$model->shift_list as $fn=>$shifts) {
    foreach ((array)$shifts as $shift) {
        $getOfdRows[] = [
            'fn' => (string)$fn,
            'shift' => (string)$shift
        ];
    }
}

$templateDateInput = "<div class='date-picker-wrap' style='width:130px'>{input}<svg class='date-picker-icon svg-icon input-toggle'><use xlink:href='/images/svg-sprite/svgSprite.svg#calendar'></use></svg></div>\n{hint}\n{error}";
?>
<style>
    .progress-status {
        width:100%;
        margin-left:0;
    }
</style>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'get-shift-list',
            'cashbox_id' => Yii::$app->request->get('cashbox_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <?= Html::hiddenInput('isSuccessLoaded', 0, ['id' => 'is_success_loaded']) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'kktRegNumber', [
                'labelOptions' => [
                    'class' => 'control-label width-label bold-text',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-5',
                ],
            ])->dropDownList($choose_kkt, [
                    'id' => 'kktRegNumber',
                    'placeholder' => '',
                    'class' => 'form-control ',
                ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="form-group column">
                    <label class="label" for="bankPeriod1">Начало периода</label>
                    <div class="date-picker-wrap w-130">
                        <?= \yii\bootstrap4\Html::activeTextInput($model, 'start_date', [
                            'class' => 'form-control date-picker',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
                <div class="form-group column">
                    <label class="label" for="bankPeriod1">Конец периода</label>
                    <div class="date-picker-wrap w-130">
                        <?= \yii\bootstrap4\Html::activeTextInput($model, 'end_date', [
                            'class' => 'form-control date-picker',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="progress-status process-shift-info" style="display:none">
        <label><span class="export-progress-action"
                     data-in-progress="получение данных из ОФД... "
                     data-completed="загрузка завершена"></span><span
                    class="progress-value"></span></label>

            <div class="progress progress-striped active export-progressbar">
                <div class="progress-bar" role="progressbar"
                     aria-valuenow="0" aria-valuemin="0"
                     aria-valuemax="100" style="width:0%;">
                </div>
            </div>
    </div>

    <div class="row pb-3">
        <div class="col-3">
            <?= Html::submitButton('Отправить запрос', [
                'class' => 'button-clr button-regular button-regular_red w-100',
                'style' => '',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', [
                '/cash/ofd/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'button-regular button-hover-transparent button-clr button-width',
                'style' => 'width: 120px!important;',
            ]); ?>
        </div>
    </div>

    <?php $form->end() ?>

</div>

<?= $this->render('@ofd/views/default/delete_ask') ?>

<script>

    var Taxcom = {};

    Taxcom.processShiftInfo = <?= (int)$model->processShiftInfo ?>;
    Taxcom.currentRow = 0;
    Taxcom.ofdRows = <?= (!empty($getOfdRows)) ? json_encode($getOfdRows) : '{}' ?>;

    console.log(Taxcom.ofdRows);

    Taxcom.getShiftInfo = function() {

        var process = $('#statement-request-form').find('.progress-status');
        var processAction = $(process).find('.export-progress-action');
        var processWidth = $(process).find('.progress-bar');

        if ("undefined" !== typeof Taxcom.ofdRows[Taxcom.currentRow]) {

            $(processAction).html($(processAction).attr('data-in-progress'));

            $.post("/cash/ofd/taxcom/default/get-shift-info", {
                fn: Taxcom.ofdRows[Taxcom.currentRow].fn,
                shift: Taxcom.ofdRows[Taxcom.currentRow].shift,
                kkt: $('#kktRegNumber').val()
            }, function (data) {
                if (data.result == true) {

                    Taxcom.currentRow++;

                    $(processWidth).css('width', Math.round(100 * Taxcom.currentRow / Taxcom.ofdRows.length) + '%');

                    Taxcom.getShiftInfo(); // RECURSIVE

                } else {

                    alert('Error shift info: ' + Taxcom.ofdRows[Taxcom.currentRow].shift);
                }
            });

        } else {

            $(processAction).html($(processAction).attr('data-completed'));

            $('#is_success_loaded').val(1);
            $('#statement-request-form').submit();
        }

    }

    if (Taxcom.processShiftInfo) {
        $('.process-shift-info').show();
        Taxcom.getShiftInfo();
    }

    //$(".date-picker").datepicker({
    //    keyboardNavigation: false,
    //    forceParse: false,
    //    language: "ru",
    //    autoclose: true
    //});

</script>