<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$rs = ArrayHelper::getValue(json_decode(base64_decode(Yii::$app->request->get('p')), true), 'rs');
$bankingAccount = null;
$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;

if ($rs && $rs == 'all') {
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
}
elseif ($rs && $bankingAccountsArray) {
    foreach ((array)$bankingAccountsArray as $account) {
        if ($account->rs == $rs) {
            $bankingAccount = $account;
            break;
        }
    }
}

$bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$module}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
        'show_no_bank' => true
    ]);

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'is_special_offer' => 1,
    'bik' => Banking::bikList(),
])->all();
?>

<h4 class="modal-title">Загрузка выписки</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <div class="row pb-1">
        <div class="form-group col-6 mb-0">
            <button class="add-vidimus-file button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#1c"></use>
                </svg>
                <span>Загрузить файл 1С</span>
            </button>
            <br><br>
            <div class="upload-1C-hide">
                Выгрузите из Клиент-Банка файл формата 1С <br>
                и загрузите его сюда.
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-hide">
            <a href="<?= $bankingUrl ?>" class="banking-module-link button-hover-content-red button-regular button-regular_padding_bigger button-clr" type="button">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#bank-3"></use>
                </svg>
                <span>Загрузить из банка</span>
            </a>
            <br><br>
            <div>
                Мы уже интегрировались с несколькими банками. <br>
                Если у вас есть счет в этом банке, то выписка будет <br>
                загружаться напрямую из банка.
            </div>
        </div>
        <div class="form-group col-6 mb-0 upload-1C-show">
            <div class="upload-button text-right">
                <a href="#" class="button-clr button-regular button-regular_red button-width disabled">
                    Загрузить
                </a>
            </div>
        </div>

        <?php /*
        <div class="col-12 mt-3">
            Выгрузите из Клиент-Банка выписку в формате 1С и загрузите этот файл с помощью левой кнопки.
            <strong class="d-block mb-3">Мы уже интегрировались и автоматически загружаем выписки из банков партнеров:</strong>
        </div>*/ ?>

    </div>
    <?php /*
    <div class="row pt-2" style="margin-bottom: -15px;">
        <?php
        foreach ($bankArray as $bank) {
            echo $this->render('_select_account_item', ['model' => $bank]);
        }
        ?>
    </div>
    */ ?>

    <?= $this->render('_vidimus_upload') ?>
</div>