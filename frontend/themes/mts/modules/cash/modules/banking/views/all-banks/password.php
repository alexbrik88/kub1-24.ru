<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\models\BankRegistrationForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Регистрация';
?>

<div id='statement-request-form-container' style="overflow-y: auto; overflow-x: hidden; max-width: 600px;">
    <?php $form = ActiveForm::begin([
        'id' => 'password-form',
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <h4><?= $this->title ?></h4>

    <p>
        Аккаунт с электронной почтой <?= Html::mailto($email) ?> уже существует.
        Если это Ваш аккаунт, введите пароль к нему и нажмите "Подтвердить".
    </p>

    <?= $form->field($model, 'password')->passwordInput(); ?>

    <?= Html::submitButton('Подтвердить', ['class' => 'button-clr button-regular button-regular_red w-100']) ?>

    <?php $form->end() ?>
</div>
