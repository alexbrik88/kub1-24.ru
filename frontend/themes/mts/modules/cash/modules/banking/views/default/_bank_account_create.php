<?php
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $bank backend\models\Bank */
/* @var $model common\models\company\ApplicationToBank */

?>

<h4 class="modal-title">Заявка на открытие расчетного счета</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>
<div class="modal-body">

    <?php $form = ActiveForm::begin([
        'action' => [
            'account',
            'bankId' => $bank->id,
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'class' => 'form-horizontal',
            'id' => 'form-apply-bank-' . $bank->id,
            'data' => [
                'pjax' => true,
                'pjax-container' => 'banking-module-pjax'
            ]
        ],
        'fieldConfig' => [
            'options' => [
                'class' => 'form-group col-6'
            ],
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control'
            ]
        ],
    ]); ?>

        <div class="form-body">
            <div class="row">
                <?php if ($bank->description): ?>
                    <div class="col-12 description-bank mb-3">
                        <?= nl2br($bank->description); ?>
                    </div>
                <?php endif; ?>

                <div class="col-12">
                    <?= Alert::widget(); ?>
                </div>

                <?= $form->field($model, 'company_name'); ?>

                <?= $form->field($model, 'inn'); ?>

                <?= $form->field($model, 'legal_address'); ?>

                <?= $form->field($model, 'fio'); ?>

                <?= $form->field($model, 'contact_phone')->textInput(['data-inputmask' => "'mask': '+7(9{3}) 9{3}-9{2}-9{2}'"]); ?>

                <?= $form->field($model, 'contact_email'); ?>
                <div class="col-12 license-bank mb-3">
                    <span>
                    <?= 'Нажимая кнопку «Открыть счет», вы даете свое согласие на ',
                        Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank']),
                        " и отправку этих данных в банк «{$bank->bank_name}»."; ?>
                    </span>
                </div>
            </div>

            <div class="d-flex justify-content-between">
                <?= Html::submitButton('Открыть счет', [
                    'class' => 'button-regular button-width button-regular_red button-clr',
                ]); ?>
                <?= Html::a('Отменить', [
                    '/cash/banking/default/index',
                    'p' => Yii::$app->request->get('p'),
                ], [
                    'class' => 'banking-module-link button-clr button-width button-regular button-hover-transparent',
                ]); ?>
            </div>

        </div>

    <?php $form->end(); ?>

</div>