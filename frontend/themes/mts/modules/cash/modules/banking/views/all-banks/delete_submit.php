<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$submitText = ArrayHelper::getValue($_params_, 'submitText', 'Подтвердить');
$cancelUrl = ArrayHelper::getValue($_params_, 'cancelUrl', ['index', 'p' => Yii::$app->request->get('p')]);
?>

<div class="row pb-3">
    <div class="col-4">
        <?= Html::submitButton($submitText, ['class' => 'button-clr button-regular button-regular_red w-100']) ?>
    </div>
    <div class="column ml-auto">
        <?= Html::a('Отменить', $cancelUrl, [
            'class' => 'button-clr button-width button-regular button-hover-transparent',
        ]) ?>
    </div>
</div>
