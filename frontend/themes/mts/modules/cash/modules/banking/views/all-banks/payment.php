<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\modules\bank044525999\models\BankModel
 */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;

?>

<div id='statement-request-form-container' style="overflow-y: auto; overflow-x: hidden;">
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'payment',
            'account_id' => Yii::$app->request->get('account_id'),
            'po_id' => Yii::$app->request->get('po_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row">
        <div class="col-6">
            <?= AccountSelectWidget::widget([
                'action' => 'payment',
                'form' => $form,
                'bankModel' => $model,
                'accounts' => $model->company->bankingPaymentAccountants,
                'urlParams' => $model->paymentOrder ? ['po_id' => $model->paymentOrder->id] : [],
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
    </div>

    <?php if ($model->scenario == $model::SCENARIO_DEFAULT) : ?>
        <?= $form->field($model, 'auth_redirect', ['template' => "{input}"])->hiddenInput(['value' => 1]); ?>
        <?= Html::submitButton('Авторизация', ['class' => 'button-clr button-regular button-regular_red dis-none-bank']) ?>
    <?php else : ?>
        <?php if ($model->paymentOrder) : ?>
            <div class="form-group">
                <div style="font-weight: bold;">
                    Платежное поручение
                    №<?= $model->paymentOrder->document_number ?>
                </div>
                <div>
                    <span style="width: 100px; display: inline-block; font-weight: bold;">
                        Сумма
                    </span>
                    <?= $model->paymentOrder->amountFormated ?> руб.
                </div>
                <div>
                    <span style="width: 100px; display: inline-block; font-weight: bold;">
                        Получатель
                    </span>
                    <?= $model->paymentOrder->contractor_name ?>
                </div>
                <div>
                    <span style="width: 100px; display: inline-block; font-weight: bold;">
                        Счет
                    </span>
                    <?= $model->paymentOrder->contractor_current_account ?>
                </div>
                <div>
                    <span style="width: 100px; display: inline-block; font-weight: bold;">
                        БИК
                    </span>
                    <?= $model->paymentOrder->contractor_bik ?>
                </div>
                <div>
                    <span style="width: 100px; display: inline-block; font-weight: bold;">
                        Банк
                    </span>
                    <?= $model->paymentOrder->contractor_bank_name ?>
                </div>
            </div>

            <?= Html::submitButton('Отправить', ['class' => 'button-clr button-regular button-regular_red']) ?>
        <?php else : ?>
            <div  class="form-group" style="color: red;">Платежное поручение не найдено</div>
        <?php endif; ?>
    <?php endif; ?>

    <?= Html::button('Отменить', [
        'data-dismiss' => 'modal',
        'class' => 'button-clr button-width button-regular button-hover-transparent banking-module-link dis-none-bank',
        'style' => 'float:right;',
    ]) ?>

    <?php $form->end() ?>
</div>
