<?php
/**
 * @var $this  yii\web\View
 */

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$bankArray = BikDictionary::find()
    ->select(['bik', 'name'])
    ->byBik(Banking::bikList())
    ->byActive()->all();

$bankModel = ArrayHelper::getValue($this->params, 'bankingModel');

$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;
$bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
$bankingUrl = $bankingAccount && ($alias = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$alias}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);

$this->beginContent('@frontend/modules/cash/modules/banking/views/layouts/main.php');
?>

<h4 class="modal-title">Запрос выписки из банка</h4>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="modal-body">

    <div class="bank-form">
        <div class="row pb-3">
            <?php if ($bankModel && ($bankPartner = $bankModel->bankPartner)) : ?>
                <div class="col-5 d-flex flex-column align-items-center justify-content-center">
                    <?= ImageHelper::getThumb($bankPartner->getUploadDirectory() . $bankPartner->logo_link, [250, 150], [
                        'class' => 'flex-shrink-0',
                    ]); ?>
                </div>
                <?php if ($bankModel->showSecureText) : ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3">
                            <div class="p-1">
                                Для обеспечения безопасности данных используется <br>
                                протокол зашифрованного соединения. <br>
                                SSL - надежный протокол для передачи конфиденциальной <br>
                                банковской информации и соблюдаются требования <br>
                                международного стандарта PCI DSS по хранению и передаче <br>
                                конфиденциальной информации в банковской сфере.
                            </div>
                        </div>
                    </div>
                <?php elseif ($bankModel->showBankBatton): ?>
                    <div class="col-7">
                        <div class="bank-form-notify p-3">
                            <div class="p-1">
                                <a href="<?= $bankingUrl ?>" class="banking-module-link button-clr button-regular button-regular_red w-100">
                                    Загрузить из банка
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>

    </div>

</div>

<?php $this->endContent(); ?>
