<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\dictionary\bik\BikDictionary
 */

use common\components\ImageHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-3 d-flex flex-column mb-4 pb-1">
    <div class="flex-grow-1 d-flex flex-column justify-content-center">
        <div class="mb-2">
            <?= ImageHelper::getThumb(
                $model->getUploadDirectory() . $model->logo_link,
                [150, 90],
                [
                    'class' => 'bank-logo',
                    'style' => 'padding-right: 15px; cursor: pointer;',
                ]
            ); ?>
        </div>
    </div>
    <?php if ($model->is_special_offer): ?>
    <?= Html::a('Открыть счет по акции?', [
        '/cash/banking/default/account',
        'bankId' => $model->id,
        'p' => Yii::$app->request->get('p'),
    ], [
        'class' => 'banking-module-link link pt-1',
    ]) ?>
    <?php endif; ?>
</div>