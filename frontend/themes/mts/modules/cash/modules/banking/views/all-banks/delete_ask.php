<?php

use common\models\bank\BankingParams;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this  yii\web\View */
/* @var $model frontend\modules\cash\modules\banking\models\AbstractBankModel */
/* @var $delete bool */
/* @var $message string */
/* @var $bankName string */
/* @var $backUrl string|array */

$hasData = BankingParams::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'bank_alias' => Yii::$app->controller->bankModelClass::$alias,
])->exists()
?>

<?php if ($hasData) : ?>
    <?php $bankName = Yii::$app->controller->bankModelClass::NAME; ?>
    <div class="clearfix" style="margin-top: 20px;">
        <?php
        echo \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
            'options' => [
                'id' => 'delete-confirm',
            ],
            'toggleButton' => [
                'tag' => 'a',
                'label' => 'Удалить интеграцию с банком ' . $bankName,
                'class' => 'link'
            ],
            'confirmUrl' => Url::toRoute([
                'delete',
                'p' => Yii::$app->request->get('p'),
            ]),
            'confirmParams' => [],
            'message' => "Вы уверены, что хотите удалить интеграцию с {$bankName}?",
        ]);
        ?>
    </div>
<?php endif ?>
