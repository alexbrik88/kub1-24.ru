<?php

use yii\helpers\Html;
use yii\helpers\Url;

$selected = $model->currentAccount->autoload_mode_id ? : '';
?>

<?= Html::beginForm(['set-autoload', 'accountId' => $model->currentAccount->id], 'post', [
    'id' => 'autoload-mode-form',
]) ?>
    <?= Html::radioList('autoload', $selected, $model->autoloadItems, [
        'id' => 'autoload-mode-selector',
        'class' => 'd-flex flex-wrap',
        'uncheck' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => '<span class="radio-txt">'.$label.'</span>',
                'labelOptions' => [
                    'class' => 'radio d-block mb-3',
                    'style' => 'width:100%'
                ],
            ]);
        },
    ]) ?>

    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_padding_large button-hover-content-red',
        'data-tooltip-content' => '#tooltip_mode-selector',
        'style' => 'margin-right: 10px'
    ]) ?>
    <span id="autoload_save_report" style="display: none; color: green;">
        Настройки сохранены
    </span>
<?= Html::endForm() ?>
