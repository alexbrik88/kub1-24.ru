<?php

use frontend\modules\cash\modules\banking\modules\alfabank\models\BankModel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\alfabank\models\BankModel */

?>

<?php $form = ActiveForm::begin([
    'id' => 'statement-result-form',
    'enableClientValidation' => false,
    'enableClientScript' => false,
    'action' => [
        'result',
        'account_id' => Yii::$app->request->get('account_id'),
        'p' => Yii::$app->request->get('p'),
    ],
    'options' => [
        'data' => [
            'send-url' => Url::to([
                'send',
                'p' => Yii::$app->request->get('p'),
            ]),
            'status-url' => Url::to([
                'status',
                'p' => Yii::$app->request->get('p'),
            ]),
            'pjax' => true,
        ]
    ]
]); ?>

    <div class="form-inputs">
        <?php if ($model->scenario == BankModel::SCENARIO_RESULT) : ?>
            <?= Html::activeHiddenInput($model, 'account_id'); ?>
            <?= Html::activeHiddenInput($model, 'request_id'); ?>
        <?php else : ?>
            <?= Html::activeHiddenInput($model, 'account_id'); ?>
            <?= Html::activeHiddenInput($model, 'start_date'); ?>
            <?= Html::activeHiddenInput($model, 'end_date'); ?>
        <?php endif ?>
    </div>

<?php $form->end(); ?>

<div class="progress-container">
    <div class="formation-status form-group">
        <label class="control-label">
            <span>идет формирование выписки </span>
            <span class="progress-value"></span>
        </label>
        <div class="progress progress-striped active">
            <div class="progress-bar"
                 role="progressbar"
                 aria-valuenow="0" aria-valuemax="100" style="width: 0%;"></div>
        </div>
    </div>
    <?= Html::a('Отменить', [
        '/cash/banking/default/index',
        'p' => Yii::$app->request->get('p'),
    ], [
        'class' => 'btn btn-primary banking-module-link',
        'onclick' => 'BANKING.cancelRequest()',
        'style' => 'float:right;',
    ]); ?>
</div>

<script type="text/javascript">
(function( $ ) {
    if (typeof window.BANKING !== 'undefined') {
        BANKING.clearTimers();
    }
    window.BANKING = {};
    $.extend(BANKING, {
        xhr: null,
        progressTimerId: null,
        progressValue: 0,
        statusTimerId: null,
        form: $('#statement-result-form'),
        sendUrl: $('#statement-result-form').data('send-url'),
        statusUrl: $('#statement-result-form').data('status-url'),
        progressUpdate: function() {
            if (BANKING.progressValue < 100) {
                BANKING.progressValue++;
                $(".progress-container .progress-bar").css("width", BANKING.progressValue + "%");
                $(".progress-container .progress-value").text(BANKING.progressValue + "%");
            } else {
                clearTimeout(BANKING.progressTimerId);
            }
        },
        progressStart: function() {
            if (BANKING.progressTimerId !== null) {
                clearTimeout(BANKING.progressTimerId);
            }
            BANKING.progressValue = 0;
            BANKING.progressTimerId = setTimeout(function tick() {
                BANKING.progressUpdate();
                if (BANKING.progressValue < 100) {
                    BANKING.progressTimerId = setTimeout(tick, 2000);
                } else {
                    $('#statement-result-form').submit();
                }
            }, 2000);
        },
        progressStop: function() {
            clearTimeout(BANKING.progressTimerId);
            BANKING.progressValue = 0;
        },
        clearTimers: function() {
            if (BANKING.progressTimerId !== null) {
                clearTimeout(BANKING.progressTimerId);
            }
            if (BANKING.statusTimerId !== null) {
                clearTimeout(BANKING.statusTimerId);
            }
        },
        sendRequest: function() {
            BANKING.xhr = $.ajax({
                url: BANKING.sendUrl,
                type: 'post',
                data: BANKING.form.serialize(),
                success: function(data) {
                    console.log(data);
                    if (data.inputs) {
                        $('.form-inputs', BANKING.form).html(data.inputs);
                        BANKING.statusTimerId = setTimeout(BANKING.statusRequest, 1000);
                    } else if (data.redirect) {
                        clearTimeout(BANKING.progressTimerId);
                        $.pjax({url: data.redirect, container: "#banking-module-pjax"});
                    }
                }
            });
        },
        statusRequest: function() {
            BANKING.xhr = $.ajax({
                url: BANKING.statusUrl,
                type: 'post',
                data: BANKING.form.serialize(),
                success: function(data) {
                    console.log(data);
                    if (data.status == 1) {
                        BANKING.progressValue = Math.max(BANKING.progressValue, 80);
                        BANKING.progressUpdate();
                        $('#statement-result-form').submit();
                    } else if (data.status == 0) {
                        BANKING.statusTimerId = setTimeout(BANKING.statusRequest, 1000);
                    } else if (data.redirect) {
                        clearTimeout(BANKING.progressTimerId);
                        $.pjax({url: data.redirect, container: "#banking-module-pjax"});
                    }
                }
            });
        },
        cancelRequest: function() {
            console.log('cancelRequest');
            BANKING.clearTimers();
            if (BANKING.xhr !== null) {
                BANKING.xhr.abort();
                BANKING.xhr = null;
            }
        }
    });
    BANKING.progressStart();
    <?php if ($model->scenario == BankModel::SCENARIO_RESULT) : ?>
        BANKING.statusTimerId = setTimeout(BANKING.statusRequest, 1000);
    <?php else : ?>
        BANKING.sendRequest();
    <?php endif ?>
})( jQuery );
</script>
