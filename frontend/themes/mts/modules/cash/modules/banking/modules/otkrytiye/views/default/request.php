<?php

use common\components\date\DateHelper;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;

/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\modules\otkrytiye\models\BankModel
 */

?>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'request',
            'account_id' => Yii::$app->request->get('account_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row pt-3">
        <div class="col-6">
            <?= AccountSelectWidget::widget([
                'form' => $form,
                'bankModel' => $model,
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="form-group column">
                    <label class="label" for="bankPeriod1">Начало периода</label>
                    <div class="date-picker-wrap w-130">
                        <?= \yii\bootstrap4\Html::activeTextInput($model, 'start_date', [
                            'class' => 'form-control date-picker',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
                <div class="form-group column">
                    <label class="label" for="bankPeriod1">Конец периода</label>
                    <div class="date-picker-wrap w-130">
                        <?= \yii\bootstrap4\Html::activeTextInput($model, 'end_date', [
                            'class' => 'form-control date-picker',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pb-3">
        <div class="col-3">
            <?= Html::submitButton('Отправить запрос', [
                'class' => 'button-clr button-regular button-regular_red w-100',
                'style' => '',
            ]); ?>
        </div>
        <div class="column ml-auto">
            <?= Html::a('Отменить', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'button-regular button-hover-transparent button-clr button-width',
                'style' => 'width: 120px!important;',
            ]); ?>
        </div>
    </div>
    <?php $form->end() ?>

    <?= \frontend\modules\cash\modules\banking\widgets\AutoloadWidget::widget([
        'model' => $model,
    ]); ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>
