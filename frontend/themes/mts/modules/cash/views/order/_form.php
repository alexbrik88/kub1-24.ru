<?php
/**
 * @var yii\web\View $this
 * @var CashOrderFlows $model
 * @var yii\bootstrap\ActiveForm $form
 */

use common\components\date\DateHelper;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\themes\mts\helpers\Icon;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\document\InvoiceExpenditureItem;

$company = Yii::$app->user->identity->company;
if ($model->amount) {
    $model->amount /= 100;
}

$flowTypeItems = CashOrderFlows::getFlowTypes();
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
    if (!$model->number) {
        $model->number = $model::getNextNumber($company->id, $model->flow_type, $model->date);
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}


$rsArray = $company->getCheckingAccountants()
    ->select('rs')
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' =>SORT_ASC])
    ->indexBy('id')
    ->column();

$userCashboxList = Yii::$app->user->identity->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$cashboxList = $company->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$selfFlowQuery = CashContractorType::find()
    ->andWhere(['not', ['name'=> CashContractorType::COMPANY_TEXT]])
    ->andWhere(['not', ['name'=> CashContractorType::ORDER_TEXT]]);
if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $selfFlowQuery->andWhere(['not', ['name'=> CashContractorType::BALANCE_TEXT]]);
}
$selfFlowResult = $selfFlowQuery->select(['text', 'name'])->indexBy('name')->column();

$contractorListOptions = [];
$contractorData = [];
if (true || Yii::$app->request->get('canAddContractor')) {
    $contractorData += ['add-modal-contractor' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) ?
        (Icon::PLUS . ' Добавить поставщика ') :
        (Icon::PLUS . ' Добавить покупателя ') ];
}

if (count($cashboxList) > 1) {
    $contractorData += array_slice($selfFlowResult, 0, 1, true);
    foreach ($cashboxList as $key => $value) {
        $optionKey = CashContractorType::ORDER_TEXT . '.' . $key;
        $contractorData[$optionKey] = $value;
        if ($key == $model->cashbox_id) {
            $contractorListOptions[$optionKey] = ['disabled' => true];
        }
    }
    $contractorData += array_slice($selfFlowResult, 1, null, true);
}  else {
    $contractorData += $selfFlowResult;
}

switch ($model->flow_type) {
    case CashOrderFlows::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_INCOME);
        $contractorData += $company->sortedContractorList(Contractor::TYPE_CUSTOMER);
        $contractorListOptions += Contractor::getAllContractorSelect2Options(Contractor::TYPE_CUSTOMER);
        break;
    case CashOrderFlows::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_EXPENSE);
        $contractorData += $company->sortedContractorList(Contractor::TYPE_SELLER);
        $contractorListOptions += Contractor::getAllContractorSelect2Options(Contractor::TYPE_SELLER);
        break;

    default:
        $contractor = 'Контрагент';
        $reasonTypeArray = [];
        $contractorData = [];
        break;
}

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($contractorData as $key=>$seller) {

        if ('add-modal-contractor' == $key)
            continue;

        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($contractorData[$key]);
        else
            $model->contractor_id = $key;
    }
}

$income = 'income' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = (isset($isClone) && $isClone ? 'Копировать' : ($model->isNewRecord ? 'Добавить' : 'Изменить')) . ' движение по кассе';

$inputCalendarTemplate = '<div class="date-picker-wrap">{input}<svg class="date-picker-icon svg-icon input-toggle"><use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use></svg></div>';
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'cash-order-form',
    'action' => empty($action) ? '' : $action,
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'form-horizontal',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group col-6'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ]
    ],
])); ?>

    <div class="form-body row">
        <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
            <?= Html::hiddenInput('redirect', $redirect) ?>
        <?php endif ?>
        <?= $form->field($model, 'flow_type')->radioList($typeItems, [
            'class' => 'd-flex flex-wrap',
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) use ($model) {

                $input = Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => '<span class="radio-txt-bold">'.$label.'</span>',
                    'labelOptions' => [
                        'class' => 'label mb-3 mr-3 mt-2',
                    ]
                ]);

                if ($model->isNewRecord) {
                    return Html::a($input, ['create', 'type' => $value, 'id' => $model->cashbox_id], [
                        'class' => 'col-xs-5 m-l-n ajax-modal-btn',
                        'data-pjax' => '0',
                        'style' => 'padding: 0; color: #333333; text-decoration: none;'
                    ]);
                } else {
                    return Html::tag('div', $input, [
                        'class' => 'col-xs-5 m-l-n',
                        'style' => 'padding: 0;'
                    ]);
                }

            },
        ])->label('Тип'); ?>

        <?php if (count($userCashboxList) == 1) : ?>
            <?= $form->field($model, 'cashbox_id')->hiddenInput(['value' => key($userCashboxList)])->label(false) ?>
        <?php else : ?>
            <?= $form->field($model, 'cashbox_id')->widget(Select2::class, [
                'data' => $userCashboxList,
                'options' => [
                    'placeholder' => '',
                    'data' => [
                        'prefix' => CashContractorType::ORDER_TEXT . '.',
                        'target' => '#' . Html::getInputId($model, 'contractorInput'),
                    ],
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ],
            ]); ?>
        <?php endif; ?>

        <?= $form->field($model, 'contractorInput')->label($contractor)->widget(Select2::class, [
            'data' => $contractorData,
            'options' => [
                'placeholder' => '',
                'options' => $contractorListOptions,
                'class' => 'contractor-items-depend ' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? 'seller' : 'customer'),
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
            'pluginOptions' => [
                'width' => '100%',
                'templateResult' => new JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 43); }'),
                'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
            ],
        ]); ?>

        <?php $isOpen = $model->contractor_id == CashContractorType::BANK_TEXT; ?>
        <div class="col-6 internal_flows flow-rs-selector collapse <?= $isOpen ? 'show' : '' ?>"
             aria-expanded="<?= $isOpen ? 'true' : 'false'; ?>">
            <?= $form->field($model, 'other_rs_id', ['options' => [
                'class' => 'form-group'
            ]])->widget(Select2::classname(), [
                'data' => $rsArray,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'minimumResultsForSearch' => 10,
                    'width' => '100%'
                ],
            ]); ?>
        </div>

        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
                echo $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group col-6',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(Select2::class, [
                    'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                                ->where(['id' => [28, 46, 48, 53]])
                                ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                                ->all(), 'id', 'name'),
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]);
            } ?>
        <?php else: ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
                echo $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group col-6 required',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(ExpenditureDropdownWidget::class, [
                    'loadAssets' => false,
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                        'width' => '100%'
                    ]
                ]);
            } ?>

            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashorderflows-expenditure_item_id',
            ]); ?>
        <?php endif; ?>

        <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) {
            echo $form->field($model, 'income_item_id', [
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form-group col-6 required',
                    'jsValue' => $model->income_item_id
                ],
            ])->widget(ExpenditureDropdownWidget::class, [
                'loadAssets' => false,
                'income' => true,
                'options' => [
                    'class' => 'flow-income-items',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]);
        } ?>

        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashorderflows-income_item_id',
            'type' => 'income',
        ]); ?>

            <?= $form->field($model, 'amount')->textInput([
                // mxfi (not enought zeros!): 'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
                'class' => 'form-control js_input_to_money_format',
            ]); ?>

            <div class="col-12 mb-0">
                <div class="row">
                    <div class="col-3">
                        <?= $form->field($model, 'date', [
                            'inputTemplate' => $inputCalendarTemplate,
                            'options' => ['class' => 'form-group'],
                        ])->textInput([
                            'class' => 'form-control date-picker',
                            'data' => [
                                'date-viewmode' => 'years',
                            ],
                        ]); ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'number', [
                            'options' => ['class' => 'form-group'],
                        ])->textInput(); ?>
                    </div>
                </div>
            </div>

        <?= $form->field($model, 'description', [
            'options' => [
                'class' => 'col-12 form-group'
            ]])->textarea([
            'style' => 'resize: none;',
            'rows' => '2',
        ]); ?>

        <?= $form->field($model, 'invoices_list', [
            'options' => [
                'class' => 'col-12',
            ],
            'wrapperOptions' => [
                'class' => 'row'
            ]
        ])->widget(\frontend\themes\mts\modules\cash\widgets\InvoiceListInputWidget::class, [
            'contractor_attribute' => 'contractorInput',
            'invoicesListUrl' => ['/cash/order/un-payed-invoices'],
        ])->label(false); ?>


        <?= $form->field($model, 'reason_id')->widget(Select2::class, [
            'data' => ArrayHelper::map($reasonTypeArray, 'id', 'name'),
            'class' => 'form-control col-6',
            'options' => [
                'placeholder' => 'Необходимо для печатного документа',
                'jsValue' => $model->reason_id,
            ],
            'pluginOptions' => [
                'width' => '100%'
            ]
        ]); ?>

        <?= $form->field($model, 'other', [
            'options' => [
                'class' => 'form-group other-reason-box',
                'data-other' => CashOrdersReasonsTypes::VALUE_OTHER,
                'style' => 'display:' . ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER ? 'block;' : 'none;'),
            ]
        ])->textInput(['style' => 'width: 100%;',]); ?>

        <?= $form->field($model, 'application')->textInput([
            'placeholder' => 'Необходимо для печатного документа',
            'style' => 'width: 100%;',
        ]); ?>

        <?= $form->field($model, 'is_accounting', ['labelOptions' => ['style' => 'margin-bottom:0; margin-top:6px;']])->checkbox(); ?>

        <div class="col-12 mb-0">
            <div class="row">
                <div class="col-3">
                    <?= $form->field($model, 'recognitionDateInput', [
                        'inputTemplate' => $inputCalendarTemplate,
                        'options' => ['class' => 'form-group'],
                        'labelOptions' => [
                            'class' => 'label bold-text',
                            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                                    'class' => 'flow-type-toggle ' . $income,
                                ]) . Html::tag('span', 'расхода', [
                                    'class' => 'flow-type-toggle ' . $expense,
                                ]),
                        ],
                    ])->textInput([
                        'class' => 'form-control date-picker',
                        'data' => [
                            'date-viewmode' => 'years',
                        ],
                        'disabled' => (bool)$model->is_prepaid_expense
                    ]); ?>

                </div>
                <div class="col-3">
                    <?= $form->field($model, 'is_prepaid_expense', [
                        'options' => [
                            'class' => 'form-group',
                            'style' => 'padding-top: 36px'
                        ],
                    ])->checkbox(); ?>
                </div>
            </div>
        </div>

    </div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular button-width button-regular_red button-clr',
            'data-style' => 'expand-right',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">
    <?php if (Yii::$app->request->isAjax) : ?>
        var modalContent = $("#update-movement-pjax").closest(".modal-content");
        if (modalContent) {
            modalContent.find(".modal-title").html("<?= $header ?>");
            // REFRESH_UNIFORMS
            refreshUniform();
            refreshDatepicker();
        }
    <?php endif ?>
    $(document).on("change", "#cashorderflows-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashorderflows-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });
</script>

<?php \yii\widgets\Pjax::end(); ?>
