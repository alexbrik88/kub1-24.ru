<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\cash\CashOrderFlows
 */

use yii\bootstrap\Html;

$this->title = 'Создание кассового ордера';
$this->params['breadcrumbs'][] = ['label' => 'Касса', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create-out-invoice create-cash-order">
    <?php if (!Yii::$app->request->isAjax):?>
        <h4><?= Html::encode($this->title) ?></h4>
    <?php endif;?>

    <?= $this->render('_form', [
        'model' => $model,
        'isClone' => isset($isClone)
    ]) ?>
</div>
