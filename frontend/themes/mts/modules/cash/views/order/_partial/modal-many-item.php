<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2018
 * Time: 17:09
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\models\cash\CashOrderFlows;
use frontend\widgets\ExpenditureDropdownWidget;

/* @var $model CashOrderFlows
 */
?>
<div class="modal fade" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['many-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="row">
                            <label class="col-12" for="cashorderflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12" style="margin:0 0 8px">
                                <div id="cashorderflowsform-flow_type" aria-required="true">
                                    <div class="">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Приход',
                                            'labelOptions' => [
                                                'class' => 'radio-txt-bold font-14',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => '',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-income_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Изменить статью прихода на:'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="row">
                            <label class="col-12" for="cashorderflowsform-flow_type">Для типа</label>

                            <div class="col-12" style="margin:0 0 8px">
                                <div id="cashorderflowsform-flow_type" aria-required="true">
                                    <div class="">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Расход',
                                            'labelOptions' => [
                                                'class' => 'radio-txt-bold font-14',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => '',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-expenditure_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Изменить статью расхода на:'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>