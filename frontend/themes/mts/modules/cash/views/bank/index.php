<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\Company;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\product\ProductSearch;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\rbac\permissions;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Modal;
use yii\bootstrap\Dropdown;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashBankFlows
 * @var $company Company
 */

$this->title = 'Банк';
$this->params['breadcrumbs'][] = $this->title;

$code = Yii::$app->request->get('code');
$state = Yii::$app->request->get('state');
$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$pageRoute = ['/cash/bank/index', 'rs' => $rs];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]);

// fix filter defaults
if ($model->flow_type === null)
    $model->flow_type = -1;

$hasFilters =
    (bool)($model->flow_type > -1) ||
    (bool)$model->contractor_id ||
    (bool)($model->reason_id != null);

?>

<div class="stop-zone-for-fixed-elems  cash-bank-flows-index">

    <div class="page-head d-flex flex-wrap align-items-center">
        <?= \frontend\themes\mts\widgets\CheckingAccountantFilterWidget::widget([
            'pageTitle' => $this->title,
            'rs' => $rs,
            'company' => $company,
        ]); ?>
        <?php if ($canCreate) : ?>
            <?php if (Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE) : ?>
                <a href="<?= Url::to(['create']); ?>" class="button-regular button-regular_padding_medium button-regular_red ml-auto button-clr">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </a>
            <?php else : ?>
                <button class="button-regular button-regular_padding_medium button-regular_red ml-auto button-clr"
                        data-toggle="modal" data-target="#add-movement"
                        href="<?= Url::to(['create']) ?>">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <?php /* \yii\widgets\Pjax::begin([
        'id' => 'cash-bank-flows-pjax-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]); */ ?>
    <div class="wrap wrap_count">
        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model,
            ]); ?>
            <div class="count-card-column col-6 d-flex flex-column">
                <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                <div class="dropdown popup-dropdown popup-dropdown_position-shevron">
                    <?= Html::a('<span>Загрузить выписку</span>', [
                        '/cash/banking/default/index',
                        'p' => Banking::routeEncode($pageRoute),
                    ], [
                        'class' => 'banking-module-open-link  button-regular w-100 button-hover-content-red',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'cash_bank_filters',
        'enableClientValidation' => false,
        'method' => 'GET',
    ]); ?>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <div class="row align-items-center">
                <div class="column flex-grow-1 d-flex flex-wrap justify-content-end">
                    <!-- buttons -->
                    <?php if ($canUpdate) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'article']) . '<span>Статья</span>',
                            '#many-item', [
                            'class' => 'button-regular button-hover-transparent button-clr mr-2 check-condition',
                            'data-toggle' => 'modal',
                        ]); ?>
                    <?php endif; ?>
                    <?php if ($canDelete) : ?>
                        <?= Html::a($this->render('//svg-sprite', ['ico' => 'garbage']) . '<span>Удалить</span>',
                            '#many-delete', [
                                'class' => 'button-regular button-hover-transparent button-clr mr-2 check-condition',
                                'data-toggle' => 'modal',
                            ]); ?>
                    <?php endif ?>
                    <!-- filter -->
                    <div class="dropdown popup-dropdown popup-dropdown_filter popup-dropdown_filter_small cash-filter <?= $hasFilters ? 'itemsSelected' : '' ?>" data-check-items="dropdown">
                        <button class="button-regular button-regular-more button-hover-transparent button-clr" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="button-txt">Фильтр</span>
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                            </svg>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="filter">
                            <div class="popup-dropdown-in p-3">
                                <div class="p-1">
                                    <div class="form-group mb-3">
                                        <div class="dropdown-drop" data-id="dropdown1">
                                            <div class="label">Приход/Расход</div>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown1">
                                                <span>
                                                    <?= $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? 'Приход' :
                                                            ($model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расход' : 'Все') ?>
                                                </span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown1">
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($model, 'flow_type', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->radioList([
                                                        -1 => 'Все',
                                                        CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
                                                        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
                                                    ], [
                                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                            return Html::radio($name, $checked, [
                                                                'value' => $value,
                                                                'label' => $label,
                                                                'labelOptions' => [
                                                                    'class' => 'p-2 no-border',
                                                                ],
                                                                'data-default' => ($checked) ? 1:0
                                                            ]);
                                                        },
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <div class="dropdown-drop" data-id="dropdown2">
                                            <div class="label">Контрагент</div>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown2">
                                                <span><?= ($model->contractor) ? $model->contractor->shortName : 'Все контрагенты' ?></span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown2">
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($model, 'contractor_id', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->radioList($model->getContractorFilterItems(), [
                                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                            return Html::radio($name, $checked, [
                                                                'value' => $value,
                                                                'label' => $label,
                                                                'labelOptions' => [
                                                                    'class' => 'p-2 no-border',
                                                                ],
                                                                'data-default' => ($checked) ? 1:0
                                                            ]);
                                                        },
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $reasonFilterItems = $model->getReasonFilterItems(); ?>
                                    <div class="form-group mb-3">
                                        <div class="dropdown-drop" data-id="dropdown3">
                                            <div class="label">Статья</div>
                                            <a class="button-regular w-100 button-hover-content-red text-left pl-3 pr-5" href="#" data-toggle="toggleVisible" data-target="dropdown3">
                                                <span><?= ($model->reason_id && isset($reasonFilterItems[$model->reason_id])) ? $reasonFilterItems[$model->reason_id] : ($model->reason_id == 'empty' ? '-' : 'Все статьи') ?></span>
                                                <svg class="svg-icon svg-icon_grey svg-icon_shevron svg-icon_absolute">
                                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                                                </svg>
                                            </a>
                                            <div class="dropdown-drop-menu" data-id="dropdown3">
                                                <div class="dropdown-drop-in">
                                                    <?= $form->field($model, 'reason_id', [
                                                        'options' => ['class' => 'form-filter-list list-clr pt-3'],
                                                    ])->label(false)->radioList(array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems), [
                                                        'item' => function ($index, $label, $name, $checked, $value) {
                                                            return Html::radio($name, $checked, [
                                                                'value' => $value,
                                                                'label' => $label,
                                                                'labelOptions' => [
                                                                    'class' => 'p-2 no-border',
                                                                ],
                                                                'data-default' => ($checked) ? 1:0
                                                            ]);
                                                        },
                                                    ]); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-between">
                                        <div class="col-6 pr-0">
                                            <button class="button-regular button-hover-content-red button-width-medium button-clr" type="submit">
                                                <span>Применить</span>
                                            </button>
                                        </div>
                                        <div class="col-6 pl-0 text-right">
                                            <button class="cash_filters_reset button-regular button-hover-content-red button-width-medium button-clr" type="button">
                                                <span>Сбросить</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">

            <div class="d-flex flex-nowrap align-items-center">
                <div class="form-group flex-grow-1 mr-2">
                    <?= $form->field($model, 'contractor_query')->label(false)->textInput(['type' => 'search', 'class' => 'form-control', 'placeholder' => 'Поиск по контрагенту...']); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', ['class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red']) ?>
                </div>
            </div>

        </div>
    </div>
    <?php ActiveForm::end(); ?>

    <?= common\components\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $model,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    //'options' => [
                    //    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                    //],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout_no_scroll', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '45',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                if ($data->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $typeCss = 'income-item';
                                    $income = bcdiv($data->amount, 100, 2);
                                    $expense = 0;
                                } else {
                                    $typeCss = 'expense-item';
                                    $income = 0;
                                    $expense = bcdiv($data->amount, 100, 2);
                                }

                                return Html::checkbox('flowId[]', false, [
                                    'class' => 'joint-operation-checkbox ' . $typeCss,
                                    'value' => $data->id,
                                    'data' => [
                                        'income' => $income,
                                        'expense' => $expense,
                                    ],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Дата',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                        ],
                        [
                            'attribute' => 'amountIncome',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function (CashBankFlows $bankFlows) {
                                return is_numeric($bankFlows->amountIncome)
                                    ? \common\components\TextHelper::invoiceMoneyFormat($bankFlows->amountIncome, 2)
                                    : $bankFlows->amountIncome;
                            },
                        ],
                        [
                            'attribute' => 'amountExpense',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function (CashBankFlows $bankFlows) {
                                return is_numeric($bankFlows->amountExpense)
                                    ? \common\components\TextHelper::invoiceMoneyFormat($bankFlows->amountExpense, 2)
                                    : $bankFlows->amountExpense;
                            },
                        ],
                        [
                            //'class' => DropDownSearchDataColumn::class,
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                                'class' => 'nowrap-normal max10list',
                            ],
                            'format' => 'raw',
                            'value' => function (CashBankFlows $model) {
                                return $model->contractor !== null ?

                                    Html::a(Html::encode($model->contractor->nameWithType), [
                                        '/contractor/view',
                                        'type' => $model->contractor->type,
                                        'id' => $model->contractor->id,
                                    ], ['target' => '_blank'])
                                    : (
                                    $model->cashContractor ?
                                        ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                    );
                            },
                            'filter' => $model->getContractorFilterItems(),
                            'hideSearch' => false,
                            's2width' => '250px'
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Назначение',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '30%',
                            ],
                            'format' => 'raw',
                            'value' => function (CashFlowsBase $data) {
                                if ($data->description) {
                                    $description = mb_substr($data->description, 0, 50) . '<br>' . mb_substr($data->description, 50, 50);

                                    return Html::label(strlen($data->description) > 100 ? $description . '...' : $description, null, ['title' => $data->description]);
                                } elseif ($invoiceArray = $data->getInvoices()->all()) {
                                    $linkArray = [];
                                    foreach ($invoiceArray as $invoice) {
                                        $linkArray[] = Html::a($data->formattedDescription . $invoice->fullNumber, [
                                            '/documents/invoice/view',
                                            'type' => $invoice->type,
                                            'id' => $invoice->id,
                                        ]);
                                    }

                                    return join(', ', $linkArray);
                                }
                            },
                        ],
                        [
                            'attribute' => 'billPaying',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                        ],
                        [
                            //'class' => DropDownSearchDataColumn::class,
                            'attribute' => 'reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                            'hideSearch' => false,
                            'format' => 'raw',
                            'value' => function (\frontend\modules\cash\models\CashBankSearch $model) {
                                $reason = ($model->flow_type == $model::FLOW_TYPE_INCOME) ? $model->incomeReason : $model->expenditureReason;

                                return $reason ? $reason->name : '-';
                            },
                            's2width' => '200px'
                        ],
                        [
                            'class' => ActionColumn::class,
                            'template' => '<div class="table-count-cell" style="min-width:60px">{update}{delete}</div>',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                            'buttons' => [
                                'update' => function ($url, CashBankFlows $model, $key) {
                                    $options = [
                                        'class' => 'button-clr link mr-1',
                                        'title' => 'Изменить',
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#update-movement',
                                        ],
                                    ];

                                    return Html::a('<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use></svg>', $url, $options);
                                },
                                'delete' => function ($url) {
                                    return \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                                        'theme' => 'gray',
                                        'toggleButton' => [
                                            'label' => '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use></svg>',
                                            'class' => 'button-clr link',
                                            'tag' => 'button',
                                        ],
                                        'confirmUrl' => $url,
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить операцию?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
    <?php // \yii\widgets\Pjax::end(); ?>
</div>

<div class="modal fade" id="add-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Добавить движение по банку</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="update-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Изменить движение по банку</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?= BankingModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => Url::to($pageRoute),
]) ?>

<?= $this->render('_partial/modal-add-contractor'); ?>
<?= $this->render('_partial/modal-many-delete'); ?>
<?= $this->render('_partial/modal-many-item', [
    'model' => $model,
]); ?>

<?php
$successFlashMessageCreate = "<div class='alert-success alert fade in'><button id='contractor_alert_close' type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Операция по банку добавлена</div>";
$successFlashMessageUpdate = "<div class='alert-success alert fade in'><button id='contractor_alert_close' type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Операция по банку изменена</div>";

$this->registerJs('
$.pjax.defaults.timeout = 10000;

$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "update-movement") {
        $(this).find(".modal-body").empty();
        $(this).find("#js-modal_update_title").empty();
    }
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(document).on("show.bs.modal", "#add-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "add-movement") {
        $(this).find(".modal-body").empty();
    }
});

$(document).on("hide.bs.modal", "#add-movement", function(event) {
    if (event.target.id === "add-movement") {
        $("#add-movement .modal-body").empty();
    }
});

$(document).on("submit", "#js-cash_flow_update_form", function(event) {
    var $isNewRecord = $(this).attr("is_new_record");
    var $successFlashMessage = $isNewRecord == 1 ? "' . $successFlashMessageCreate . '" : "' . $successFlashMessageUpdate . '";
    $.ajax({
        "type": "post",
        "url": $(this).attr("action"),
        "data": $(this).serialize(),
        "success": function(data) {
                $("#add-movement, #update-movement").modal("hide");

                $(".cash-bank-flows-index").prepend($successFlashMessage);
                if ($("#cash-bank-flows-pjax-container").length)
                    $.pjax.reload({"container":"#cash-bank-flows-pjax-container", "timeout": 5000});
                else
                    location.href = location.href;
        }
    });

    return false;
});

$(document).on("pjax:complete", function(event) {
    // REFRESH_UNIFORMS
    refreshUniform();
    refreshDatepicker();
});

$(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);
    var $header = $modal.find("h4.modal-title");
    var $additionalHeaderText = null;

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    if ($includeExpenditureItem && $includeIncomeItem) {
        $additionalHeaderText = " прихода / расхода";
    } else if ($includeExpenditureItem) {
        $additionalHeaderText = " расхода";
    } else if ($includeIncomeItem) {
        $additionalHeaderText = " прихода";
    }
    $header.append("<span class=additional-header-text>" + $additionalHeaderText + "</span>")
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
    $(".additional-header-text").remove();
    $(".modal#many-item form#js-cash_flow_update_item_form .joint-operation-checkbox").remove();
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    // var l = Ladda.create($(this).find(".btn-save")[0]);
    var $hasError = false;

    // l.start();
    $(".field-cashbanksearch-incomeitemidmanyitem:visible, .field-cashbanksearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        return false;
    }
});

// SHOW_AJAX_MODAL
$("#add-movement").on("show.bs.modal", function (e) {

    var button = $(e.relatedTarget);
    var modal = $(this);
    if ($(button).attr("href"))
        modal.find(".modal-body").load(button.attr("href"));
});

$("#update-movement").on("show.bs.modal", function (e) {

    var button = $(e.relatedTarget);
    var modal = $(this);
    if ($(button).attr("href"))
        modal.find(".modal-body").load(button.attr("href"));
});


// REFRESH_UNIFORMS
$(document).on("pjax:complete", "#cash-bank-flows-pjax-container", function() {
    refreshUniform();
});


// FILTERS
$("form#cash_bank_filters").find("input:radio").on("change", function() {
        $(this).parents(".dropdown-drop").find("a > span").html($(this).parents("label").text());
        $(this).parents(".dropdown-drop-menu, .dropdown-drop").removeClass("visible show");
});
$(".cash_filters_reset").on("click", function() {
    $(this).parents(".dropdown").find(".dropdown-drop-in").each(function() {
        $(this).find("input:radio").first().prop("checked", true);
        $(this).find("input:radio").uniform();
        $(this).parents(".dropdown-drop").find("a > span").html($(this).find("input:radio").first().parents("label").text());
    });
    refreshProductFilters();
});

// SEARCH
$("input#cashbanksearch-contractor_query").on("keydown", function(e) {
  if(e.keyCode == 13) {
    e.preventDefault();
    $("#cash_bank_filters").submit();
  }
});

/* SELECTS */
$(document).on("select2:open", "#cashbankflowsform-rs", function() {});

/* FILTERS DROPDOWN */
$(".cash-filter .popup-dropdown-in").on("click", function(e) {
    e.stopPropagation();
    $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop.visible.show").removeClass("visible show");
    $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop-menu.visible.show").removeClass("visible show");
});

/* VIDIMUS BUTTON */
$(document).on(\'mouseover\', \'input[name="uploadfile"]\', function() {
    $(\'.add-vidimus-file\').css({\'color\': \'#c90000\', \'cursor\':\'pointer\'});
});
$(document).on(\'mouseout\', \'input[name="uploadfile"]\', function() {
    $(\'.add-vidimus-file\').css(\'color\', \'#001424\');
});

// FILTER BUTTON TOGGLE COLOR
function refreshProductFilters() {
    var pop = $(".cash-filter");
    var submit = $(pop).find("[type=submit]");
    var filter_on = false;
    $(pop).find(".form-filter-list").each(function() {
        var a_val = $(this).find("input").filter("[data-default=1]").val();
        var i_val = $(this).find("input:checked").val();
        if (i_val === undefined) {
            i_val = $(this).find("label:first-child").find("input").val();
        }
        if (a_val === undefined) {
            a_val = $(this).find("label:first-child").find("input").val();
        }
        if (a_val != i_val) {
            filter_on = true;
            return false;
        }
    });

    if (filter_on)
        $(submit).addClass("button-regular_red").removeClass("button-hover-content-red");
    else
        $(submit).removeClass("button-regular_red").addClass("button-hover-content-red");
}
$(".cash-filter").find("input").on("change", function(e) {
    e.preventDefault();
    refreshProductFilters();
});

'); ?>
