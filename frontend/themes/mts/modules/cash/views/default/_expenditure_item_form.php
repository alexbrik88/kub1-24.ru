<?php

use frontend\rbac\UserRole;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var string $inputId */

if (empty($type) || $type !== 'income') {
	$type = 'expenditure';
}
?>

<?php if (Yii::$app->getUser()->can(UserRole::ROLE_CHIEF)) : ?>

<div style="display: none;">
    <span class="add-new-<?= $type ?>-item-form"
        style="display: inline-block; position: relative; width: 100%; padding: 15px 52px 15px 15px; border-top: 2px solid #e2e5eb; z-index: 999999">

        <button class="new-<?= $type ?>-item-submit button-regular button-regular_padding_medium button-regular_red ml-auto button-clr" type="button"
            style="position: absolute; right: 12px; top: 15px; padding: 10px; color:#fff;">
            <svg class="svg-icon" style="font-size: 16px;">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
            </svg>
        </button>

        <?= Html::textInput('name', null, [
            'class' => 'form-control',
            'maxlength' => 45,
            'placeholder' => $type == 'income' ? 'Добавить тип прихода' : 'Добавить статью расходов',
            'style' => 'display: inline-block; width: 295px;',
        ]) ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().find('.add-new-<?= $type ?>-item-form').length) {
            $('.add-new-<?= $type ?>-item-form').first().clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
        // fix bug - otherwise input not focused
        $('.modal.show').addClass('no-overflow').removeAttr('tabindex');
    });
    $('#<?= $inputId ?>').on('select2:close', function (evt) {
        // fix bug - otherwise input not focused
        $('.modal.show').removeClass('no-overflow');
    });
    $(document).on('click', '.new-<?= $type ?>-item-submit', function() {
        var input = $(this).parent().children('input');
        $.post('/site/add-<?= $type ?>-item', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                var newOption = new Option(data.itemName, data.itemId, true, true);
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
                input.val('');
            }
        });
    });
</script>

<?php endif ?>