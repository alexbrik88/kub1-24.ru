<?php
/**
 * @var CashFlowsBase $model
 */

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use frontend\components\StatisticPeriod;

$date = StatisticPeriod::getSessionPeriod();

$dateStart = $model->periodStartDate;
$dateEnd = $model->periodEndDate;
$balanceStart = $model->getBalanceAtBegin(date_create_from_format(DateHelper::FORMAT_DATE, $date['from']));
$balanceEnd = $model->getBalanceAtEnd(date_create_from_format(DateHelper::FORMAT_DATE, $date['to']));
$totalIncome = $model->periodIncome;
$totalExpense = $model->periodExpense;

echo frontend\modules\cash\widgets\Statistic::widget([
    'items' => [
        [
            'class' => 'count-card_yellow',
            'text' => 'Баланс на начало',
            'amount' => $balanceStart,
            'statistic_text' => $dateStart,
            'statistic_text_class' => '_yellow_statistic',
        ],

        [
            'class' => 'count-card_turquoise',
            'text' => 'Приход',
            'amount' => $totalIncome,
            'statistic_text' => 'Всего поступлений: ' . $model->countIncome,
            'statistic_text_class' => '_green_statistic',
        ],

        [
            'class' => 'count-card_red',
            'text' => 'Расход',
            'amount' => $totalExpense,
            'statistic_text' => 'Всего списаний: ' . $model->countExpense,
            'statistic_text_class' => '_red_statistic',
        ],

        [
            'class' => 'count-card_yellow',
            'text' => 'Баланс на конец',
            'amount' => $balanceEnd,
            'statistic_text' => $dateEnd,
            'statistic_text_class' => '_yellow_statistic',
        ],
    ],

    'model' => $model,
]);