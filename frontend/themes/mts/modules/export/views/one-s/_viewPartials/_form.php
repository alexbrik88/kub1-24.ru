<?php

use frontend\modules\export\models\export\export;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Documents;

$outDocumentsType = Export::$documentTypes;
$inDocumentsType = Export::$documentTypes;
unset($inDocumentsType[Documents::DOCUMENT_UPD]);

$form = \yii\widgets\ActiveForm::begin([
    'id' => 'one-s-export-form',
    'action' => ['create'],
    'method' => 'post',
//    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
//    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => '',
        ],
    ],
    'options' => [
        'data' => [
            'progress-url' => Url::to(['/export/file/progress']),
        ],
    ],
]); ?>

    <div class="col-12">
        <div class="form-group">
            <div class="label mb-3 bold">Параметры выгрузки</div>
            <?= $form->field($exportModel, 'only_new')->checkbox(); ?>
        </div>
        <div class="form-group">
            <div class="label mb-3 bold">Документы для выгрузки</div>
            <?= $form->field($exportModel, 'io_type_out')->checkbox(['id' => 'export-type-out']); ?>
            <div class="pb-2 pt-2" style="padding-left: 27px">
                <?= $form->field($exportModel, 'io_type_out_items')->label(false)->checkboxList($outDocumentsType, ['class' => 'export-type-out']); ?>
            </div>
            <?= $form->field($exportModel, 'io_type_in')->checkbox(['id' => 'export-type-in']); ?>
            <div class="pb-2 pt-2" style="padding-left: 27px">
                <?= $form->field($exportModel, 'io_type_in_items')->label(false)->checkboxList($inDocumentsType, ['class' => 'export-type-in']); ?>
            </div>
            <div class="pb-2">
            <?= $form->field($exportModel, 'product_and_service')->checkbox(); ?>
            </div>
            <div class="pt-2">
                <?= $form->field($exportModel, 'contractor')->checkbox(['id' => 'export-contractor']); ?>
            </div>
            <div class="pb-2 pt-2" style="padding-left: 27px">
                <?= $form->field($exportModel, 'contractor_items')->label(false)->checkboxList(Export::$contractorTypes, ['class' => 'export-contractor']); ?>
            </div>
            <?= $form->field($exportModel, 'period_start_date')->label(false)->hiddenInput(); ?>
            <?= $form->field($exportModel, 'period_end_date')->label(false)->hiddenInput(); ?>
            <?= Html::hiddenInput('user_id', \Yii::$app->user->id, ['id' => 'user_id']); ?>

            <div class="load_button mt-3 mb-3">
                <?= Html::submitButton('Сформировать архив', ['class' => 'button-regular button-hover-content-red button-clr']); ?>
                <div class="progress-status mt-3 files" style="display: none;">
                    <label style="font-size: 14px"><span class="export-progress-action"
                                                         data-in-progress="идет формирование архива... "
                                                         data-completed="архив сформирован"></span><span
                                class="progress-value"></span></label>

                    <div
                            class="progress progress-striped active export-progressbar">
                        <div class="progress-bar" role="progressbar"
                             aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width:0%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $form->end(); ?>