<?php foreach($contractors as $contractor) { ?>
    <Справочник.Контрагенты>
        <КлючевыеСвойства>
            <ИНН><?= $contractor->ITN;?></ИНН>
            <КПП><?= $contractor->PPC;?><</КПП>
            <НаименованиеПолное><?= $contractor->name;?></НаименованиеПолное>
            <Ссылка></Ссылка>
            <ЮридическоеФизическоеЛицо><?= $contractor->director_name; ?></ЮридическоеФизическоеЛицо>
        </КлючевыеСвойства>
        <ГоловнойКонтрагент>
            <ИНН></ИНН>
            <КПП></КПП>
            <НаименованиеПолное></НаименованиеПолное>
            <Ссылка></Ссылка>
            <ЮридическоеФизическоеЛицо></ЮридическоеФизическоеЛицо>
        </ГоловнойКонтрагент>
        <Группа>
            <Группа></Группа>
            <Наименование></Наименование>
            <Ссылка></Ссылка>
        </Группа>
        <ДокументУдЛичность></ДокументУдЛичность>
        <ДополнительнаяИнформация></ДополнительнаяИнформация>
        <Наименование></Наименование>
        <НалоговыйНомерНерезидента></НалоговыйНомерНерезидента>
        <ОбособленноеПодразделение></ОбособленноеПодразделение>
        <ОКПО></ОКПО>
        <РегистрационныйНомерНерезидента></РегистрационныйНомерНерезидента>
        <СтранаРегистрации>
            <Код></Код>
            <Наименование></Наименование>
            <Ссылка></Ссылка>
        </СтранаРегистрации>
    </Справочник.Контрагенты>
<?php } ?>