<?php
/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */

use frontend\modules\reports\models\ExpensesSearch;
use frontend\widgets\RangeButtonWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Выгрузка в 1С';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stop-zone-for-fixed-elems">
    <div class="wrap pt-2 pb-1 pl-4 pr-3">
        <div class="pt-1 pl-2 pr-2">
            <div class="row align-items-center">
                <div class="col-9 mr-auto">
                    <h4 class="mb-2">
                        <?= $this->title ?>
                        <svg class="svg-icon link" data-toggle="toggleVisible" data-target="invoice" style="cursor: pointer; margin-left:10px; margin-top:-3px; width:20px; height: 20px;" viewBox="0 0 16 15">
                             <path d="M9.25 13.17H16v1.2H9.25a.636.636 0 0 0-.625.63h-1.25a.636.636 0 0 0-.625-.63H0v-1.2h6.75c.465 0 .904.168 1.25.473a1.88 1.88 0 0 1 1.25-.472zM16 0v11.804H9.25a.63.63 0 0 0-.625.635h-1.25a.63.63 0 0 0-.625-.635H0V0h6.75c.48 0 .918.184 1.25.486C8.332.184 8.77 0 9.25 0H16zM7.273 10.61V1.742a.638.638 0 0 0-.631-.644H1.09v9.401h5.55c.222 0 .434.04.632.11zm7.636-9.512H9.36a.638.638 0 0 0-.632.643v8.869a1.84 1.84 0 0 1 .631-.111h5.551V1.098z"></path>
                        </svg>
                    </h4>
                </div>
                <div class="col-3 pl-1 pr-0" style="margin-top:-9px">
                    <?= RangeButtonWidget::widget(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="wrap mt-2">
        <div class="row">
            <?= $this->render('_viewPartials/_form', [
                'exportModel' => $exportModel,
            ]); ?>
        </div>
    </div>

    <div class="wrap wrap_count mt-2">
        <div class="col-12">
            <div class="mt-1 mb-2"><strong>История выгрузок</strong></div>

            <?php Pjax::begin([
                'id' => 'export-list',
            ]) ?>
            <?= $this->render('_viewPartials/_list', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php
echo $this->render('_viewPartials/export-help-panel')
?>