<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\widgets\AddressTypeahead;
use common\components\widgets\BikTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\dictionary\address\AddressDictionary;
use common\models\employee\EmployeeRole;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use frontend\themes\mts\helpers\Icon;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $account CheckingAccountant */

$this->title = 'Заполните реквизиты';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');
$companyTypes = CompanyType::find()->andWhere(['in_company' => true])->all();
$typeList = ArrayHelper::map($companyTypes, 'id', 'name_short');

$acquiringAccountsList = ArrayHelper::map(
    CheckingAccountant::find()
        ->where(['company_id' => $model->id])
        ->distinct('bank_name')
        ->orderBy('bank_name')
        ->all(), 'id', 'bank_name');
$ofdTypes = ArrayHelper::map(
    \common\models\companyStructure\OfdType::find()
        ->where(['or',
            ['company_id' => null],
            ['company_id' => $model->id]])
        ->orderBy('sort')
        ->all(), 'id', 'name');
$onlinePaymentTypes = ArrayHelper::map(
        \common\models\companyStructure\OnlinePaymentType::find()
            ->where(['or',
                ['company_id' => null],
                ['company_id' => $model->id]])
            ->orderBy('sort')
            ->all(), 'id', 'name');
$accountingSystemTypes = ArrayHelper::map(
        \common\models\companyStructure\AccountingSystemType::find()
            ->where(['or',
                ['company_id' => null],
                ['company_id' => $model->id]])
            ->orderBy('sort')
            ->all(), 'id', 'name');

$this->registerJsFile('@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);

$view = $model->company_type_id == CompanyType::TYPE_IP ?
    Company::$companyFormView[CompanyType::TYPE_IP] :
    Company::$companyFormView[CompanyType::TYPE_OOO];
?>

<?php $form = ActiveForm::begin([
    'id' => 'company-update-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
]); ?>

<div class="wrap p-4">
    <div class="p-2">
        <div class="pb-4"><h4>Данные по вашей компании</h4></div>

        <?= $this->render("parts_company/{$view}", [
            'form' => $form,
            'model' => $model,
        ]); ?>

        <?= $this->render('parts_company/_accounts', [
            'form' => $form,
            'model' => $model,
            'accounts' => $accounts,
        ]) ?>

        <div>
            <div class="weight-700 mt-3 mb-3">Система налогообложения</div>

            <?= $form->field($taxation, 'osno', [
                'parts' => [
                    '{input}' => Html::beginTag('div', [
                            'class' => 'field-width inp_one_line_company',
                            'style' => 'padding-top: 7px; display: inline-block;',
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'osno', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => (boolean) $taxation->usn,
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'usn', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => (boolean) $taxation->osno,
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'envd', [
                            'labelOptions' => ['class' => 'label mr-4'],
                        ]) . "\n" .
                        Html::activeCheckbox($taxation, 'psn', [
                            'labelOptions' => ['class' => 'label mr-4'],
                            'disabled' => $model->company_type_id != CompanyType::TYPE_IP,
                        ]) . "\n" .
                        Html::beginTag('div', ['class' => 'tax-usn-config collapse' . ($taxation->usn ? ' show' : '')]) . "\n" .
                        $form->field($taxation, 'usn_type', [
                            'options' => [
                                'class' => 'row',
                                'style' => 'max-width: 300px; margin-left: 76px;',
                            ],
                        ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                            'class' => 'column mb-2',
                            'style' => 'margin-top: 10px;',
                            'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                                $radio = Html::radio($name, $checked, [
                                    'id' => 'usn_radio_'.$index,
                                    'class' => 'usn_type_radio',
                                    'value' => $value,
                                    'label' => $label,
                                    'labelOptions' => [
                                        'wrapInput' => true,
                                        'class' => 'label',
                                    ],
                                ]);
                                $itemRadio = Html::tag('div', $radio, [
                                    'class' => 'column mb-2',
                                ]);
                                $percent = Html::activeTextInput($taxation, 'usn_percent', [
                                        'class' => 'form-control d-inline-block text-right usn-percent usn-percent-' . $value,
                                        'style' => 'width: 75px;',
                                        'disabled' => !$checked,
                                        'value' => $checked ? $taxation->usn_percent : $taxation->defaultUsnPercent($value),
                                        'data-default' => $taxation->defaultUsnPercent($value),
                                    ]) . ' <span class="ml-1">%</span>';
                                $itemPercent = Html::tag('div', $percent, [
                                    'class' => 'column mb-2 pl-0',
                                ]);

                                return Html::tag('div', "$itemRadio\n$itemPercent", [
                                    'class' => 'row align-items-center justify-content-between usn_type_row',
                                ]);
                            },
                        ])->label(false) . "\n" .

                        Html::endTag('div') . "\n" . // .tax-usn-config

                        Html::endTag('div'), // .inp_one_line_company
                ],
            ])->label(false); ?>

            <div class="tax-patent-config collapse <?=($taxation->psn ? ' show' : '')?>">
                <div class="row">
                    <div class="col-6">
                        <?= $form->field($model, 'ip_patent_city')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3" style="position: relative;">
                        <?= $form->field($model, 'patentDate')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control date-picker ico',
                            'autocomplete' => 'off',
                            'value' => ($taxation->psn) ? $model->patentDate : null,
                        ])->label('Дата начала действия патента') ?>
                    </div>
                    <div class="col-3">
                        <?= $form->field($model, 'patentDateEnd')->textInput([
                            'maxlength' => true,
                            'class' => 'form-control input-sm date-picker ico',
                            'autocomplete' => 'off',
                            'value' => ($taxation->psn) ? $model->patentDateEnd : null,
                        ])->label('Дата окончания действия патента') ?>
                    </div>
                </div>
            </div>

            <div class="nds-view-osno collapse <?= $taxation->osno ? ' show' : ''; ?>">
                <?= $form->field($model, 'nds')->radioList($ndsViewOsno, [
                    'class' => 'inp_one_line_company',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, [
                                    'value' => $value,
                                ]) . $label, [
                                'class' => 'label',
                                'style' => 'margin-right: 10px;',
                            ]) . Html::a('Пример счета', [
                                '/company/view-example-invoice',
                                'nds' => $value == 1 ? true : false,
                            ], [
                                'class' => 'link',
                                'target' => '_blank',
                                'style' => 'margin-right: 20px;',
                            ]);
                    },
                ])->label('В счетах цену за товар/услугу указывать') ?>
            </div>

            <div class="company-data-collection-information" style="margin:40px 0">
                <div class="weight-700 mt-3 mb-3">Информация для сбора данных</div>

                <div class="row">
                    <?= $form->field($model, 'ofdTypes', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => $ofdTypes,
                        'hideSearch' => true,

                        'options' => [
                            'prompt' => 'Выберите ваше ОФД'
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ])->label('ОФД' . \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'title' => 'Если у вас есть касса, то выберите ваше ОФД'
                        ])); ?>

                    <?= $form->field($model, 'acquiringAccounts', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => $acquiringAccountsList,
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => 'Выберите ваш банк'
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ])->label('Эквайринг' . \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'title' => 'Если у вас есть эквайринг, то укажите банк'
                        ])); ?>

                    <?= $form->field($model, 'onlinePaymentTypes', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => ['add-modal' => Icon::PLUS . ' Добавить'] + $onlinePaymentTypes,
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => 'Выберите ваш сервис приема платежей'
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                        ]
                    ])->label('Прием платежей на сайте' . \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'title' => 'Если вы принимаете оплату на сайте, то укажите сервис. Данные будут подтягиваться из этого сервиса'
                        ])); ?>

                    <?= $form->field($model, 'accountingSystemTypes', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => ['add-modal' => Icon::PLUS . ' Добавить'] + $accountingSystemTypes,
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => 'Выберите вашу учетную систему'
                        ],
                        'pluginOptions' => [
                            'width' => '100%',
                            'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                        ]
                    ])->label('Учетная система' . \yii\helpers\Html::tag('span', Icon::QUESTION, [
                            'title' => 'Укажите, вашу учетную систему. Если у нас уже есть интеграция с ней, то вы сможете загружать данные из неё'
                        ])); ?>
                </div>
            </div>

            <hr class="devider mt-4 mb-4">
            <button class="link link_collapse link_bold button-clr mb-4 collapsed" type="button" data-toggle="collapse" data-target="#moreDetails2" aria-expanded="false" aria-controls="moreDetails2">
                <span class="link-txt">Данные, которые помогут настроить ваши отчеты</span>
                <svg class="link-shevron svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
                </svg>
            </button>
            <div class="collapse" id="moreDetails2">
                <div class="row">
                    <?= $form->field($model, 'info_industry_id', [
                        'options' => [
                            'class' => 'form-group col-6 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => ArrayHelper::map(\common\models\company\CompanyInfoIndustry::find()->all(), 'id', 'name'),
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => ''
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                    <div class="form-group col-6 mb-3 pb-3">
                        <div class="form-filter">
                            <label class="label">Ваша роль в компании</label>
                            <?= Select2::widget([
                                'name' => 'Employee[employee_role_id]',
                                'data' => [EmployeeRole::ROLE_CHIEF => 'Руководитель'],
                                'hideSearch' => true,
                                'options' => [
                                ],
                                'pluginOptions' => [
                                    'width' => '100%'
                                ]
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?= $form->field($model, 'info_employers_count', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->textInput(['maxlength' => true])->label('Кол-во сотрудников в компании'); ?>
                    <?= $form->field($model, 'info_sellers_count', [
                        'options' => [
                            'class' => 'form-group col-3 mb-3 pb-3'
                        ]
                    ])->textInput(['maxlength' => true])->label('Кол-во продавцов в компании'); ?>
                    <?= $form->field($model, 'info_site_id', [
                        'options' => [
                            'class' => 'form-group col-6 mb-3 pb-3'
                        ]
                    ])->widget(Select2::class, [
                        'data' => ArrayHelper::map(\common\models\company\CompanyInfoSite::find()->all(), 'id', 'name'),
                        'hideSearch' => true,
                        'options' => [
                            'prompt' => ''
                        ],
                        'pluginOptions' => [
                            'width' => '100%'
                        ]
                    ]); ?>
                </div>
                <div class="pt-4">
                    <div class="checkbox">
                        <label class="checkbox-label">
                            <?= $form->field($model, 'info_has_shop', [
                                'options' => [
                                    'style' => 'display:inline-block'
                                ]])->checkbox([], true)->label(false) ?>
                            <span class="ml-2">Наличие торговых точек <span class="weight-300">(розничных магазинов)</span></span>
                        </label>
                    </div>
                    <div class="pt-2 mt-1" style="padding-left: 36px">
                        <?= $form->field($model, 'info_shops_count', [
                            'options' => [
                                'style' => 'width: 79px;'
                            ]
                        ])->textInput(['maxlength' => true])->label('Кол-во'); ?>
                    </div>
                </div>
                <div class="pt-4 mt-1">
                    <div class="checkbox">
                        <label class="checkbox-label">
                            <?= $form->field($model, 'info_has_storage', [
                                'options' => [
                                    'style' => 'display:inline-block'
                                ]])->checkbox([], true)->label(false) ?>
                            <span class="ml-2">Наличие склада</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'company-structure',
    'submit_button' => true,
    'skip_button' => true,
]); ?>

<?php $form->end(); ?>


<?= $this->render('parts_company/_company_inn_api') ?>

<?php
$this->registerJs(<<<JS

    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
        }
    });

    $('#company-has_chief_patronymic').change(function() {
        var checked = $(this).is(':checked');
        var target = $(this).data('target');
        if (checked) {
            $(target).val('');
        }
        $(target).prop('readonly', checked).removeClass('is-invalid').parent().find('.invalid-feedback').html('');
    });

    // Система налогообложения
    $(document).ready(function() {
        if (0 === $(".nds-view-osno").find('input[type="radio"]:checked').length)
            $(".nds-view-osno").find('input[type="radio"]').first().prop('checked', 'checked').uniform();
    });

    function resetDisabledCheckboxes() {
        $('.taxation-system').find('.form-group').removeClass('has-success').removeClass('has-error');
    }

    $(document).on("change", "#companytaxationtype-osno", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true);

            $(".nds-view-osno", form).collapse("show");
        } else {
            $("#companytaxationtype-usn", form).prop("disabled", false);
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-usn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true);
            $(".tax-usn-config", form).collapse("show");
            $("input.usn-percent", form).each(function (i, el) {
                $(el).val($(el).data('default'));
            });
        } else {
            $("#companytaxationtype-osno", form).prop("disabled", false);
            $(".tax-usn-config", form).collapse("hide");
            $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#companytaxationtype-usn_percent", form).val("");
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "input.usn_type_radio", function() {
        $("input.usn_type_radio", this.form).each(function (i, el) {
            $(el).closest('.usn_type_row').find('input.usn-percent').prop('disabled', !$(this).is(':checked'));
        });
    });
    $(document).on("change", "#companytaxationtype-psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true);
            $(".tax-patent-config", form).collapse("show");
        } else {
            $("#companytaxationtype-envd", form).prop("disabled", false);
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes();
    });

JS
);

$this->registerJs(<<<JS

function repaintAccountNumbers() {
    $('.company-account').each(function(i,v) {
       $(v).find('.small-title > span').html(1+i);
    });
}

$(document).on("click", ".btn-confirm-yes", function() {
    var account_id = $(this).data('model_id');
    var account_block = $('.company-account').filter('[data-id="' + account_id + '"]');

    if (account_block.length) {

        if (!$(account_block).find('.dictionary-bik').val().trim())
           $('#add-new-account').removeAttr('disabled');

        if (!$(account_block).hasClass('new-account')) {
            $('#company-update-form').append('<input type="hidden" name="delete_rs[]" value="' + account_id + '" />');
        }

        $(account_block).remove();
        repaintAccountNumbers();
    }

    return false;
});

JS
);

Modal::begin([
    'id' => 'accounting-system-type-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>

    <h4 class="modal-title">Добавить учетную систему</h4>

<?php
Pjax::begin([
    'id' => 'accounting-system-type-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();

Modal::begin([
    'id' => 'online-payment-type-modal',
    'closeButton' => [
        'label' => $this->render('//svg-sprite', ['ico' => 'close']),
        'class' => 'modal-close close',
    ],
]); ?>

<h4 class="modal-title">Добавить сервис приема платежей на сайте</h4>

<?php
Pjax::begin([
    'id' => 'online-payment-type-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();

$this->registerJs(<<<JS
$(document).on("change", "#company-onlinepaymenttypes", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal") {
        e.preventDefault();

        $.pjax({
            url: "create-online-payment-type",
            container: "#online-payment-type-pjax",
            push: false,
            timeout: 5000,
        });

        $(document).on("pjax:success", function() {
        });
        $("#online-payment-type-modal").modal("show");
        $("#company-onlinepaymenttypes").val("").trigger("change");
    }
});
$(document).on("change", "#company-accountingsystemtypes", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal") {
        e.preventDefault();

        $.pjax({
            url: "create-accounting-system-type",
            container: "#accounting-system-type-pjax",
            push: false,
            timeout: 5000,
        });

        $(document).on("pjax:success", function() {
        });
        $("#accounting-system-type-modal").modal("show");
        $("#company-accountingsystemtypes").val("").trigger("change");
    }
});

JS
);