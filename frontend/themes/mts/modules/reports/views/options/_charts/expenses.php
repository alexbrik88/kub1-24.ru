<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 16:18
 */

use \miloschuman\highcharts\Highcharts;
use common\components\helpers\Month;
use frontend\modules\reports\models\ExpensesSearch;

/* @var $searchModel ExpensesSearch
 * @var $activeTab integer
 */

$categories = ['Себест-ть товара', 'Зарплата', 'Аренда', 'Транспорт', 'Прочее'];
$fact = [40000, 35000, 25000, 20000, 15000];
$plan = [45000, 45000, 30000, 30000, 30000];

?>
<div class="col-4 mb-3 expenses-chart">
    <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">ПРИХОД-РАСХОД</div>
    <?= Highcharts::widget([
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'spacing' => [10,10,10,10],
                'height' => '200px',
                'events' => [
                    'load' => new \yii\web\JsExpression('function () {
                        var titleX = +$(".expenses-chart").width() - +$(".expenses-chart .highcharts-plot-border").width() - 12;
                        
                        $(".expenses-chart .highcharts-title").attr("x", titleX);
                    }'),
                ],
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'tooltip' => [
                'shared' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
            ],
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => true,
                'style' => [
                    'font-size' => '12px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'],
                'minorGridLineWidth' => 0,
            ],
            'series' => [
                [
                    'name' => 'Факт',
                    'data' => [15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000],
                    'color' => 'rgba(250,192,49,1)',
                    'pointWidth' => 15,
                    'borderRadius' => 3
                ],
                [
                    'name' => 'План',
                    'data' => array_reverse([15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000]),
                    'marker' => [
                        'symbol' => 'c-rect',
                        'lineWidth' => 3,
                        'lineColor' => 'rgba(3,20,30,1)',
                        'radius' => 7
                    ],
                    'type' => 'spline',
                    'lineWidth' => 0,
                    'states' => [
                        'hover' => [
                            'lineWidthPlus' => 0,
                        ],
                    ],
                ]
            ],
        ],
    ]); ?>
</div>
<div class="col-4 mb-3 structure-chart">
    <div class="label text-uppercase weight-700 mb-0 mr-3 mb-1">Структура расходов</div>
    <?= Highcharts::widget([
        'scripts' => [
            //'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'credits' => [
                'enabled' => false
            ],
            'chart' => [
                'type' => 'column',
                'spacing' => [10,10,10,10],
                'height' => '200px',
                'inverted' => true,
                'events' => [
                    'load' => new \yii\web\JsExpression('function () {
                        var titleX = +$(".structure-chart").width() - +$(".structure-chart .highcharts-plot-border").width() - 12;
                        
                        $(".structure-chart .highcharts-title").attr("x", titleX);
                    }'),
                ],
            ],
            'legend' => [
                'layout' => 'horizontal',
                'align' => 'right',
                'verticalAlign' => 'top',
                'backgroundColor' => '#fff',
                'itemStyle' => [
                    'fontSize' => '11px',
                    'color' => '#9198a0'
                ],
                'symbolRadius' => 2
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0
                ]
            ],
            'tooltip' => [
                'shared' => true,
                'backgroundColor' => "rgba(255,255,255,1)",
                'borderColor' => '#ddd',
                'borderWidth' => '1',
            ],
            'title' => [
                'text' => '',
                'align' => 'left',
                'floating' => true,
                'style' => [
                    'font-size' => '12px',
                    'color' => '#9198a0',
                ],
                'x' => 0,
            ],
            'yAxis' => [
                'min' => 0,
                'index' => 0,
                'title' => '',
                'minorGridLineWidth' => 0,
            ],
            'xAxis' => [
                'categories' => $categories,
                'minorGridLineWidth' => 0,

            ],
            'series' => [
                [
                    'name' => 'План',
                    'pointPadding' => 0,
                    'data' => $plan,
                    'color' => 'rgba(226,229,236,.9)',
                    'pointWidth' => 15,
                    'borderRadius' => 3
                ],
                [
                    'name' => 'Факт',
                    'pointPadding' => 0,
                    'data' => $fact,
                    'color' => 'rgba(99,132,225,1)',
                    'pointWidth' => 15,
                    'borderRadius' => 3
                ],
            ]
        ],
    ]); ?>
</div>

<script>
    $(document).ready(function () {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };
    });
</script>