<?php

use backend\models\Bank;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;

$this->title = 'Загрузка данных';

/** @var \common\models\Company $company */
$company = Yii::$app->user->identity->company;

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'bik' => Banking::bikList(),
])->indexBy('bik')->all();

$accountByBikArray = [];
foreach ($company->bankingAccountants as $account) {
    $accountByBikArray[$account->bik] = $account->id;
}

$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();

$currentPageRoute = ['/reports/options/statements'];
?>

<div class="wrap p-4">
    <div class="mb-4">
        <h4>Подключение источников, для автоматической загрузки данных</h4>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'statements-update-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
    ]); ?>
    <div class="scroll-table-other">
        <div class="table-wrap">
            <table class="table table-style mb-0 text-dark">
                <tbody>
                <?php if ($bankArray): ?>
                <tr role="button" data-toggle="collapse" data-target="#tableMore" aria-expanded="true" aria-controls="tableMore">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#bank-2"></use>
                            </svg>
                            <span class="ml-2 pl-1">Банки</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Финансам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5">Стоимость</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Подарок</div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <?php foreach (Banking::$modelClassArray as $banking): ?>
                                <?php $bank = ArrayHelper::getValue($bankArray, $banking::BIK); ?>
                                <?php $userHasBik = array_intersect(array_keys($accountByBikArray), $banking::$bikList); ?>
                                <?php if (!$bank) continue; ?>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="<?= \common\components\ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link) ?>" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div><?= $bank->bank_name ?></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">

                                        <?php if ($userHasBik): ?>
                                            <?php if ($userHasIntegration = BankingParams::getValue($company, $banking::ALIAS, 'access_token')): ?>
                                                <span class="button-clr button-regular integration-success">Подключен</span>
                                            <?php else: ?>
                                            <?= \yii\bootstrap4\Html::a('<span>Подключить</span>', [
                                                '/cash/banking/'.$banking::ALIAS.'/default/index',
                                                'account_id' => $accountByBikArray[reset($userHasBik)],
                                                'bankingAfterLoginRedirectUrl' => '/reports/options/statements',
                                                'p' => Banking::routeEncode($currentPageRoute),
                                            ], [
                                                'class' => 'banking-module-open-link button-clr button-regular button-hover-transparent pr-3 pl-3',
                                                'data' => [
                                                    'pjax' => '0',
                                                ]
                                            ]); ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <button class="button-clr button-regular button-disabled pr-3 pl-3" type="button">Подключить</button>
                                        <?php endif; ?>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <?php endif; ?>
                <tr role="button" data-toggle="collapse" data-target="#tableMore2" aria-expanded="true" aria-controls="tableMore2">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#bank-2"></use>
                            </svg>
                            <span class="ml-2 pl-1">ОФД</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Финансам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore2">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <?php foreach (Ofd::$modelClassArray as $ofd): ?>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/img/icons/<?=$ofd::ALIAS?>.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div><?= $ofd::NAME ?></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <?php if ($cashbox): ?>
                                            <?php if ($userHasIntegration = OfdParams::getValue($company, $ofd::ALIAS, 'sessionToken')): ?>
                                                <span class="button-clr button-regular integration-success">Подключен</span>
                                            <?php else: ?>
                                                <?= \yii\bootstrap4\Html::a('<span>Подключить</span>', [
                                                        '/cash/ofd/'.$ofd::ALIAS.'/default/index',
                                                        'cashbox_id' => $cashbox->id,
                                                        'ofdAfterLoginRedirectUrl' => '/reports/options/statements',
                                                        'p' => Ofd::routeEncode($currentPageRoute),
                                                    ], [
                                                    'class' => 'ofd-module-open-link button-clr button-regular button-hover-transparent pr-3 pl-3',
                                                    'data' => [
                                                        'pjax' => '0',
                                                    ]
                                                ]); ?>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <button class="button-clr button-regular button-disabled pr-3 pl-3" type="button">Подключить</button>
                                        <?php endif; ?>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr role="button" data-toggle="collapse" data-target="#tableMore3" aria-expanded="true" aria-controls="tableMore3">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#1c"></use>
                            </svg>
                            <span class="ml-2 pl-1">1c</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Финансам, Продажам, Товарам </div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore3">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="ml-1">
                                                <div>Бухгалтерия</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="ml-1">
                                                <div>УТ</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr role="button" data-toggle="collapse" data-target="#tableMore4" aria-expanded="true" aria-controls="tableMore4">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#adds"></use>
                            </svg>
                            <span class="ml-2 pl-1">Рекламные каналы</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Маркетингу и Продажам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore4">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/YMLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Яндекс.Деньги</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/GAdsLogo.png" width="41" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Google Ads</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/FBLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Facebook</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/InstLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>instagram</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/VKLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>ВКонтакте</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr role="button" data-toggle="collapse" data-target="#tableMore5" aria-expanded="true" aria-controls="tableMore5">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#crm"></use>
                            </svg>
                            <span class="ml-2 pl-1">СРМ</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Продажам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore5">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/amo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>AmoCRM</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/bitrix.png" width="41" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Битрикс24</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr role="button" data-toggle="collapse" data-target="#tableMore5" aria-expanded="true" aria-controls="tableMore5">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#pc-shop"></use>
                            </svg>
                            <span class="ml-2 pl-1">Интернет магазины</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Маркетингу и Продажам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore5">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/openCartLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>OpenCart</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/inSaleLogo.png" width="41" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>InSale</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/bitrix1.png" width="41" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Битрикс 1С</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <span class="pr-2">10.10.2019</span>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr role="button" data-toggle="collapse" data-target="#tableMore5" aria-expanded="true" aria-controls="tableMore5">
                    <td class="pb-3 pl-0 pr-3 pt-3 no-border w-25">
                        <div class="text_size_16 weight-700 d-flex flex-nowrap align-items-center">
                            <svg class="svg-icon text-grey-light">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#styled-list"></use>
                            </svg>
                            <span class="ml-2 pl-1">Другое</span>
                        </div>
                    </td>
                    <td class="pb-3 pl-3 pr-3 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700">Для отчётов по Продажам</div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25"><div class="text_size_16 weight-700 pl-5"></div></td>
                    <td class="pb-3 pl-3 pr-4 pt-3 no-border w-25 text-right"><div class="text_size_16 weight-700"></div></td>
                </tr>
                <tr>
                    <td class="p-0 no-border border-bottom" colspan="4">
                        <div class="collapse show" id="tableMore5">
                            <table class="table table-style text-dark mb-0">
                                <tbody>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/excelLogo.png" width="37" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>Excel</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">Бесплатно</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <div class="pr-2">10.10.2019</div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="pt-2 pb-2 pr-3 pl-4 no-border-right w-25">
                                        <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                            <div class="mr-2">
                                                <img src="/images/uniLogo.png" width="41" alt="">
                                            </div>
                                            <div class="ml-1">
                                                <div>UniSender</div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <span style="color:#e30611">Скоро</span>
                                        <!--<button class="button-clr button-regular button-hover-transparent pr-3 pl-3" type="button">Подключить</button>-->
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25">
                                        <div class="pl-5">от  1000 ₽/мес</div>
                                    </td>
                                    <td class="pt-2 pb-2 pl-3 pr-3 no-border-right w-25 text-right">
                                        <div class="d-flex flex-nowrap align-items-center justify-content-between">
                                            <div class="d-flex flex-nowrap align-items-center">
                                                <div class="mr-2 nowrap">500 ₽</div>
                                                <div class="text_size_12">при загрузке <br> данных до</div>
                                            </div>
                                            <div class="pr-2">10.10.2019</div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php $form->end(); ?>

    <?= BankingModalWidget::widget([
        'pageTitle' => $this->title,
        'pageUrl' => Url::to($currentPageRoute),
    ]) ?>
    <?= OfdModalWidget::widget([
        'pageTitle' => $this->title,
        'pageUrl' => Url::to($currentPageRoute),
    ]) ?>
</div>

<?= $this->render('actions-buttons', [
    'next_step' => 'notifications',
    'submit_button' => true,
    'skip_button' => true,
]); ?>