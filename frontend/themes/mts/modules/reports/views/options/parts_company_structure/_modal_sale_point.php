<?php

use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use \frontend\themes\mts\helpers\Icon;
use common\models\product\Store;
use common\models\cash\Cashbox;
use common\models\companyStructure\OfdType;
use common\models\Company;
use common\models\companyStructure\SalePoint;
use common\components\helpers\ArrayHelper;
use yii\web\JsExpression;
use common\models\employee\Employee;

/** @var $company Company */
/** @var $model SalePoint */
/** @var boolean $isSaved */
/** @var Cashbox[] $newCashboxes */
/** @var Employee[] $newEmployers */

$questionIcon = [
    'companyName' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_companyName']),
    'name' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_name']),
    'store' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_store']),
    'city' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_city']),
    'address' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_address']),
    'cashbox' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_cashbox']),
    'employee' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_employee']),
    'site' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_site']),
    'online_payment_type' => Html::tag('span', Icon::QUESTION, ['class' => 'tooltip3', 'data-tooltip-content' => '#tooltip3_online_payment_type']),
];

$storeList = ['add-modal' => Icon::PLUS . 'Добавить склад'] + ArrayHelper::map($company->stores, 'id', 'name');

$employeeArray = ['add-modal' => Icon::PLUS . 'Добавить сотрудника'] + \yii\helpers\ArrayHelper::map(
        \common\models\EmployeeCompany::find()
            ->where(['company_id' => $company->id])
            ->indexBy('lastname')
            ->all(), 'employee_id', function ($model) {
        return $model->getShortFio(true) ?: '(не задан)';
    });

$onlinePaymentTypesArray = \yii\helpers\ArrayHelper::map(
    \common\models\companyStructure\OnlinePaymentType::find()
        ->where(['or',
            ['company_id' => null],
            ['company_id' => $company->id]])
        ->orderBy('sort')
        ->all(), 'id', 'name');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-mts'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$needShowFields = [
    'site' => [SalePoint::TYPE_INTERNET_SHOP],
    'online_payment_type' => [SalePoint::TYPE_INTERNET_SHOP],
    'employers' => [SalePoint::TYPE_DEPARTMENT, SalePoint::TYPE_FILIAL]
];
$needHideFields = [
    'city' => [SalePoint::TYPE_INTERNET_SHOP],
    'cashboxes' => [SalePoint::TYPE_INTERNET_SHOP],

];

?>

<h4 class="modal-title">
    <?= ($model->isNewRecord) ? 'Добавить' : 'Редактировать' ?> <?= $model->type->name ?>
</h4>

<?php

if (Yii::$app->request->isAjax && $isSaved) {

    echo Html::script('
        
        window.toastr.success("'.htmlentities($model->name).' добавлен", "", {
            "closeButton": true,
            "showDuration": 1000,
            "hideDuration": 1000,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            "escapeHtml": false,
        });        
        
        $(".modal.show").modal("hide");',
        ['type' => 'text/javascript']);

}

$form = ActiveForm::begin([
    'id' => 'sale-point-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
        'data-pjax' => true,
    ],
]); ?>

<?= Html::activeHiddenInput($model, 'type_id') ?>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label class="label">
                Ваше юридическое лицо
                <?= $questionIcon['companyName'] ?>
            </label>
            <input disabled type="text" class="form-control" name="companyName" value="<?= htmlentities($model->company->getShortName()) ?>">
        </div>
    </div>
    <div class="col-6">
        
    </div>
</div>
<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Например: Магазин №1'])->label('Название магазина' . $questionIcon['name']) ?>
    </div>
    <div class="col-6">
        <?= $form->field($model, 'store_id', [
            'options' => [
                'class' => 'form-group'
            ]
        ])->widget(Select2::class, [
            'data' => $storeList,
            'hideSearch' => true,
            'options' => [
                'prompt' => 'Например: Склад магазина №1'
            ],
            'pluginOptions' => [
                'width' => '100%',
                'escapeMarkup' => new JsExpression('function(text) {return text;}')
            ]
        ])->label('Склад' . $questionIcon['store']); ?>
    </div>
</div>
<div class="row">

    <?php if (!in_array($model->type_id, $needHideFields['city'])): ?>
        <div class="col-6">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true])->label('Город' . $questionIcon['city']) ?>
        </div>
    <?php endif; ?>

    <div class="col-6">
        <?= $form->field($model, 'address')->textInput(['maxlength' => true])->label('Адрес' . $questionIcon['address']) ?>
    </div>
</div>
<div class="row">

    <?php if (!in_array($model->type_id, $needHideFields['cashboxes'])): ?>
    <div class="col-6">
        <div class="form-group required">
            <label class="label">&nbsp;</label><br/>
            <?php $pointCashboxes = ($model->isNewRecord) ? $newCashboxes : $model->cashboxes ?>
            <div class="cashbox-list-modal">
                <?php foreach ($pointCashboxes as $cashbox): ?>
                    <div class="cashbox-item-modal" data-id="<?= $cashbox->id ?>">
                        <?= Html::hiddenInput('SalePoint[cashboxes][]', $cashbox->id) ?>
                        <span class="cashbox-name"><?= $cashbox->name ?></span>
                        <?= \yii\helpers\Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                            'class' => 'edit-cashbox pl-1 pr-1 btn-link',
                            'title' => 'Редактировать',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                        <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                            'class' => 'delete-cashbox pl-1 pr-1 btn-link',
                            'title' => 'Удалить',
                            'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                        ]); ?>
                    </div>
                <?php endforeach; ?>
                <div class="cashbox-item-modal template" data-id="" style="display: none">
                    <span class="cashbox-name"></span>
                    <?= \yii\helpers\Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'class' => 'edit-cashbox pl-1 pr-1 btn-link',
                        'title' => 'Редактировать',
                        'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                    ]); ?>
                    <?= Html::tag('span', $this->render('//svg-sprite', ['ico' => 'garbage']), [
                        'class' => 'delete-cashbox pl-1 pr-1 btn-link',
                        'title' => 'Удалить',
                        'style' => 'display: inline-block; margin-right:0px; cursor: pointer',
                    ]); ?>
                </div>
            </div>
            <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']) . 'Добавить кассу', [
                'class' => 'add-cashbox button-regular button-hover-transparent button-clr',
            ]); ?>
            <?= $questionIcon['cashbox'] ?>
            <?php if ($cashboxErrorMessage = $model->getFirstError('cashboxes')): ?>
                <div class="invalid-feedback" style="display: block!important;"><?= $cashboxErrorMessage ?></div>
            <?php endif; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php if (in_array($model->type_id, $needShowFields['employers'])): ?>
    <div class="col-6">
        <?php $pointEmployers = ArrayHelper::getColumn(($model->isNewRecord) ? $newEmployers : $model->employers, 'id'); ?>
        <?php echo $form->field($model, 'employers')
            ->widget(Select2::class,[
                'data' => $employeeArray,
                'hideSearch' => true,
                'options' => [
                    'placeholder' => '',
                    'multiple' => true,
                    'value' => $pointEmployers,
                ],
                'pluginOptions' => [
                    'closeOnSelect' => false,
                    'width' => '100%',
                    'escapeMarkup' => new JsExpression('function(text) {return text;}')
                ],
                'toggleAllSettings' => [
                    'selectLabel' => false,
                    'unselectLabel' => false,
                ]
            ])->label('Сотрудники' . $questionIcon['employee']); ?>
    </div>
    <?php endif; ?>

</div>

    <div class="row">

        <?php if (in_array($model->type_id, $needShowFields['site'])): ?>
            <div class="col-6">
                <?= $form->field($model, 'site')->textInput(['maxlength' => true])->label('Сайт' . $questionIcon['site']) ?>
            </div>
        <?php endif; ?>

        <?php if (in_array($model->type_id, $needShowFields['online_payment_type'])): ?>
            <div class="col-6">
                <?= $form->field($model, 'online_payment_type_id')->widget(Select2::class, [
                    'data' => $onlinePaymentTypesArray,
                    'hideSearch' => true,
                    'options' => [
                        'prompt' => ''
                    ],
                    'pluginOptions' => [
                        'width' => '100%',
                        'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                    ]
                ])->label('Прием платежей на сайте' . $questionIcon['online_payment_type']) ?>
            </div>
        <?php endif; ?>
    </div>

<div class="mt-3 pt-1 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_red button-width button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?= $this->render('_modal_sale_point_help', ['type_id' => $model->type_id]) ?>

<?php ActiveForm::end();