<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

if (Yii::$app->request->isAjax && $model->saved) {

    echo Html::script('
        var newState = new Option("'.$model->name.'", '.$model->id.', true, true);
        $("#company-accountingsystemtypes").append(newState).trigger("change");
        
        window.toastr.success("Учетная система добавлена", "", {
            "closeButton": true,
            "showDuration": 1000,
            "hideDuration": 1000,
            "timeOut": 1000,
            "extendedTimeOut": 1000,
            "escapeHtml": false,
        });
    
        $(".modal.show").modal("hide");',
        ['type' => 'text/javascript']);

}

$form = ActiveForm::begin([
    'id' => 'company-accounting-system-type-form',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'options' => [
            'class' => 'form-group'
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control'
        ],
        'checkOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label'
            ],
        ],
    ],
    'options' => [
        'data-isNewRecord' => (int)$model->isNewRecord,
        'data-modelid' => $model->id,
        'data-pjax' => true,
    ],
]); ?>

<div class="row">
    <div class="col-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', [
        'class' => 'button-regular button-regular_red button-width button-clr',
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-regular button-regular button-hover-transparent button-width button-clr',
        'data-dismiss' => 'modal'
    ]); ?>
</div>

<?php ActiveForm::end();