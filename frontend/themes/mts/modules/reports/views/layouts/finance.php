<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');

$year = isset(Yii::$app->controller->year) ? Yii::$app->controller->year : null;
?>
<div class="debt-report-content nav-finance">
    <?php if (Yii::$app->controller->action->id != 'payment-calendar'): ?>
    <div class="nav-tabs-row pb-3 mb-3">
        <?php
        echo Nav::widget([
            'id' => 'debt-report-menu',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'ОДДС',
                    'url' => ['/reports/finance/odds', 'FlowOfFundsReportSearch' => ['year' => $year,]],
                    'active' => Yii::$app->controller->action->id == 'odds',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'План-факт',
                    'url' => ['/reports/finance/plan-fact', 'PlanFactSearch' => ['year' => $year,]],
                    'active' => Yii::$app->controller->action->id == 'plan-fact',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'ОПиУ (P&L)',
                    'url' => ['/reports/finance/profit-and-loss', 'ProfitAndLossSearchModel' => ['year' => $year,]],
                    'active' => Yii::$app->controller->action->id == 'profit-and-loss',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Баланс',
                    'url' => ['/reports/finance/balance', 'year' => $year,],
                    'active' => Yii::$app->controller->action->id == 'balance',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
                [
                    'label' => 'Расходы',
                    'url' => ['/reports/finance/expenses', 'ExpensesSearch' => ['year' => $year,]],
                    'active' => Yii::$app->controller->action->id == 'expenses',
                    'options' => ['class' => 'nav-item'],
                    'linkOptions' => ['class' => 'nav-link'],
                ],
            ],
        ]);
        ?>
    </div>
    <?php endif; ?>
    <div class="finance-index">
        <?= $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>