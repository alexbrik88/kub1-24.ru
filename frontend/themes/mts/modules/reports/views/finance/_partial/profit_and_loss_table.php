<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 15.04.2019
 * Time: 16:06
 */

use common\components\helpers\ArrayHelper;use common\components\helpers\Html;
use common\components\TextHelper;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\ProfitAndLossSearchModel;
use frontend\themes\mts\helpers\Icon;
use kartik\checkbox\CheckboxX;
use yii\web\View;

/* @var $this yii\web\View
 * @var $searchModel ProfitAndLossSearchModel
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $data array
 */

$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];

?>

<div class="custom-scroll-table">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style table-count-list table-count-list_collapse_none mb-0">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
            </tr>
            <tr>
                <?php foreach (ProfitAndLossSearchModel::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            </thead>

            <tbody>

            <?php $floorID = 0; ?>
            <?php foreach ($data as $row): ?>
                <?php
                $currentQuarter = (int)ceil(date('m') / 3);
                $suffix = ArrayHelper::remove($row, 'suffix');
                $options = ArrayHelper::remove($row, 'options', []);
                $includedRow = (isset($options['class']) && strpos($options['class'], 'hidden') !== false);
                $classBold = (!$includedRow) ? 'weight-700' : '';
                ?>

                <?php if (isset($row['addCheckboxX']) && $row['addCheckboxX']):
                    $floorID++; ?>

                    <tr class="not-drag expenditure_type sub-block">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-row-trigger data-target="first-floor-<?= $floorID ?>">
                                <span class="table-collapse-icon">&nbsp;</span>
                                <span class="weight-700 text_size_14 ml-1"><?= $row['label']; ?></span>
                            </button>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($row['amount'][$monthNumber]) ? $row['amount'][$monthNumber] : 0;
                            ?>
                            <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                <div class="pl-1 pr-1">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                </div>
                            </td>
                            <?php $quarterSum = isset($row['amount']['quarter-' . $quarter]) ? $row['amount']['quarter-' . $quarter] : 0; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>

                <?php else: ?>

                    <tr class="item-block <?= ($includedRow) ? 'd-none' : '' ?>" <?= ($includedRow) ? 'data-id="first-floor-'.$floorID.'"' : '' ?>>
                        <td class="pl-3 pr-3 pt-3 pb-3">
                            <span class="<?= $classBold ?: 'text-grey' ?> text_size_14 ml-1 mr-5"><?= $row['label']; ?></span>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (ProfitAndLossSearchModel::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $amount = isset($row['amount'][$monthNumber]) ? $row['amount'][$monthNumber] : 0;
                            ?>
                            <td class="can-hover pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-collapse-cell data-id="<?= $cell_id[$quarter] ?>">
                                <div class="pl-1 pr-1 <?= $classBold ?>">
                                    <?= TextHelper::invoiceMoneyFormat($amount, 2) . $suffix; ?>
                                </div>
                            </td>
                            <?php $quarterSum = isset($row['amount']['quarter-' . $quarter]) ? $row['amount']['quarter-' . $quarter] : 0; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="can-hover quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-collapse-cell-total data-id="<?= $cell_id[$quarter] ?>">
                                    <div class="pl-1 pr-1 <?= $classBold ?>">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2) . $suffix; ?>
                                    </div>
                                </td>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </tr>

                <?php endif; ?>



            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>