<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2019
 * Time: 1:01
 */

use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\modules\reports\models\PlanFactSearch;
use common\components\helpers\Html;
use frontend\themes\mts\helpers\Icon;
use yii\helpers\Url;
use kartik\checkbox\CheckboxX;
use common\components\TextHelper;
use frontend\modules\reports\models\PaymentCalendarSearch;

/* @var $this yii\web\View
 * @var $activeTab integer
 * @var $searchModel PlanFactSearch
 * @var $data array
 * @var $currentMonthNumber string
 * @var $currentQuarter integer
 * @var $checkMonth boolean
 */
$cell_id = [1 => 'first-cell', 2 => 'second-cell', 3 => 'third-cell', 4 => 'fourth-cell'];

$isCurrentYear = $searchModel->isCurrentYear;

$d_monthes = [
    1 => $currentMonthNumber < 4 && $isCurrentYear,
    2 => $currentMonthNumber > 3 && $currentMonthNumber < 7,
    3 => $currentMonthNumber > 6 && $currentMonthNumber < 10,
    4 => $currentMonthNumber > 9 && $isCurrentYear
];

?>

<div class="custom-scroll-table">
    <div class="table-wrap">
        <table class="flow-of-funds table table-style table-count-list table-count-list_collapse_none mb-0 by_purse">
            <thead>
            <tr class="quarters-flow-of-funds">
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" rowspan="2">
                    <button class="table-collapse-btn button-clr ml-1 active" type="button" data-collapse-all-trigger>
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1">Статьи</span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[1] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="first-cell" data-quarter="1">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[1] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="first-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">1 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[2] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="second-cell" data-quarter="2">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[2] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="second-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">2 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[3] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="third-cell" data-quarter="3">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[3] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="third-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">3 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
                <th class="pl-2 pr-2 pt-3 pb-3 align-top" <?= $d_monthes[4] ? 'colspan="3"' : '' ?> data-collapse-cell-title data-id="fourth-cell" data-quarter="4">
                    <button class="table-collapse-btn button-clr ml-1 <?= $d_monthes[4] ? 'active' : '' ?>" type="button" data-collapse-trigger data-target="fourth-cell">
                        <span class="table-collapse-icon">&nbsp;</span>
                        <span class="text-grey weight-700 ml-1 nowrap">4 кв <?= $searchModel->year; ?></span>
                    </button>
                </th>
            </tr>
            <tr>
                <?php foreach (PlanFactSearch::$month as $key => $month): ?>
                    <?php $quarter = (int)ceil($key / 3); ?>
                    <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                        data-collapse-cell
                        data-id="<?= $cell_id[$quarter] ?>"
                        data-month="<?= $key; ?>"
                    >
                        <div class="pl-1 pr-1"><?= $month; ?></div>
                    </th>
                    <?php if ($key % 3 == 0): ?>
                        <th class="pl-2 pr-2 <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                            data-collapse-cell-total
                            data-id="<?= $cell_id[$quarter] ?>"
                        >
                            <div class="pl-1 pr-1">Итого</div>
                        </th>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            </thead>

            <tbody>
            <?php foreach (PlanFactSearch::$purseTypes as $typeID => $typeName): ?>
                <?php $class = in_array($typeID, [
                    PlanFactSearch::INCOME_CASH_BANK,
                    PlanFactSearch::INCOME_CASH_ORDER,
                    PlanFactSearch::INCOME_CASH_EMONEY]) ?
                    'income' : 'expense'; ?>

                <?php if ($class == 'income'): ?>
                    <tr class="main-block"
                        data-flow_type="<?= PlanFactSearch::$purseBlockByType[$typeID]; ?>">
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= PlanFactSearch::$purseBlocks[PlanFactSearch::$purseBlockByType[$typeID]]; ?>
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $totalFlowSum = isset($data['blocks'][PlanFactSearch::$purseBlockByType[$typeID]]['totalFlowSum']['flowSum']) ?
                                $data['blocks'][PlanFactSearch::$purseBlockByType[$typeID]]['totalFlowSum']['flowSum'] : 0;
                            $flowSum = isset($data['blocks'][PlanFactSearch::$purseBlockByType[$typeID]][$monthNumber]) ?
                                $data['blocks'][PlanFactSearch::$purseBlockByType[$typeID]][$monthNumber]['flowSum'] : 0;
                            if ($isCurrentYear && $monthNumber > date('m')) {
                                $totalFlowSum -= $flowSum;
                                $flowSum = 0;
                            }; ?>

                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($flowSum, 2) ?>
                                </div>
                            </td>
                            <?php $quarterSum += $flowSum; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </tr>
                <?php endif; ?>

                <tr class="not-drag expenditure_type sub-block <?= $class; ?>" id="<?= $typeID; ?>"
                    data-flow_type="<?= PlanFactSearch::$purseBlockByType[$typeID]; ?>">
                    <td class="pl-2 pr-2 pt-3 pb-3">
                        <button class="table-collapse-btn button-clr ml-1" type="button" data-collapse-row-trigger data-target="first-floor-<?= $typeID ?>">
                            <span class="table-collapse-icon">&nbsp;</span>
                            <span class="text-grey text_size_14 ml-1"><?= $typeName; ?></span>
                        </button>
                    </td>
                    <?php $key = $quarterSum = 0; ?>
                    <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                        <?php $key++;
                        $quarter = (int)ceil($key / 3);
                        $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                        $totalFlowSum = isset($data['types'][$typeID]['totalFlowSum']) ?
                            $data['types'][$typeID]['totalFlowSum'] : 0;
                        $flowSum = isset($data['types'][$typeID][$monthNumber]) ?
                            $data['types'][$typeID][$monthNumber]['flowSum'] : 0;
                        if ($isFutureDate) {
                            $totalFlowSum -= $flowSum;
                            $flowSum = 0;
                        };
                        $tooltipData = $searchModel->getTooltipData(
                            PlanFactSearch::$flowTypeByPurseType[$typeID],
                            $searchModel->getItemsByFlowType(PlanFactSearch::$flowTypeByPurseType[$typeID]),
                            $monthNumber,
                            $monthNumber,
                            $flowSum,
                            PaymentCalendarSearch::$paymentTypeByType[$typeID]
                        ); ?>
                        <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                            data-collapse-cell
                            data-id="<?= $cell_id[$quarter] ?>"
                            data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                            data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                            data-diff-amount="<?= $tooltipData['diff']; ?>"
                            data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                            style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation']); ?>;"
                        >
                            <div class="pl-1 pr-1">
                                <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                            </div>
                        </td>
                        <?php $quarterSum += $flowSum; ?>
                        <?php if ($key % 3 == 0): ?>
                            <?php $monthStart = ($monthNumber - 2) < 10 ? ('0' . ($monthNumber - 2)) : $monthNumber;
                            $tooltipData = $searchModel->getTooltipData(
                                PlanFactSearch::$flowTypeByPurseType[$typeID],
                                $searchModel->getItemsByFlowType(PlanFactSearch::$flowTypeByPurseType[$typeID]),
                                $monthStart,
                                $monthNumber,
                                $quarterSum,
                                PaymentCalendarSearch::$paymentTypeByType[$typeID]
                            ); ?>
                            <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                data-collapse-cell-total
                                data-id="<?= $cell_id[$quarter] ?>"
                                data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                data-diff-amount="<?= $tooltipData['diff']; ?>"
                                data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation']); ?>;"
                            >
                                <div class="pl-1 pr-1">
                                    <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                    $quarterSum = 0; ?>
                                </div>
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>

                <?php if (isset($data[$typeID])): ?>
                    <?php foreach ($data['itemName'][$typeID] as $expenditureItemID => $expenditureItemName): ?>
                        <?php if (isset($data[$typeID][$expenditureItemID])): ?>
                            <tr class="item-block d-none <?=  $class; ?>" 
                                data-id="first-floor-<?= $typeID ?>" 
                                data-type_id="<?= $typeID; ?>" 
                                data-item_id="<?= $expenditureItemID; ?>"
                                data-flow_type="<?= PlanFactSearch::$purseBlockByType[$typeID]; ?>">
                                <td class="pl-3 pr-3 pt-3 pb-3">
                                    <span class="text-grey text_size_14 ml-1 mr-5"><?= $expenditureItemName; ?></span>
                                    <!--<button class="table-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center" type="button" data-collapse-row-trigger data-target="second-floor">
                                        <svg class="svg-icon text_size-14 text-grey mr-2 flex-shrink-0">
                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#menu-small"></use>
                                        </svg>
                                        <span class="text-grey text_size_14 ml-1 mr-5">АйТи</span>
                                        <svg class="svg-icon-triangle svg-icon text_size_14 text-grey-light ml-auto flex-shrink-0">
                                            <use xlink:href="/images/svg-sprite/svgSprite.svg#triangle"></use>
                                        </svg>
                                    </button>-->
                                </td>
                                <?php $key = $quarterSum = 0; ?>
                                <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                                    <?php $key++;
                                    $quarter = (int)ceil($key / 3);
                                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                                    $totalFlowSum = isset($data[$typeID][$expenditureItemID]['totalFlowSum']) ?
                                        $data[$typeID][$expenditureItemID]['totalFlowSum'] : 0;
                                    $flowSum = isset($data[$typeID][$expenditureItemID][$monthNumber]) ?
                                        $data[$typeID][$expenditureItemID][$monthNumber]['flowSum'] : 0;
                                    if ($isFutureDate) {
                                        $totalFlowSum -= $flowSum;
                                        $flowSum = 0;
                                    };
                                    $tooltipData = $searchModel->getTooltipData(
                                        PlanFactSearch::$flowTypeByPurseType[$typeID],
                                        [$expenditureItemID],
                                        $monthNumber,
                                        $monthNumber,
                                        $flowSum,
                                        PaymentCalendarSearch::$paymentTypeByType[$typeID]
                                    ); ?>
                                    <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>"
                                        data-collapse-cell
                                        data-id="<?= $cell_id[$quarter] ?>"
                                        data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                        data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                        data-diff-amount="<?= $tooltipData['diff']; ?>"
                                        data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                        style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation']); ?>;"
                                    >
                                        <div class="pl-1 pr-1">
                                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2) ?>
                                        </div>
                                    </td>
                                    <?php $quarterSum += $flowSum; ?>
                                    <?php if ($key % 3 == 0): ?>
                                        <?php $monthStart = ($monthNumber - 2) < 10 ? ('0' . ($monthNumber - 2)) : $monthNumber;
                                        $tooltipData = $searchModel->getTooltipData(
                                            PlanFactSearch::$flowTypeByPurseType[$typeID],
                                            [$expenditureItemID],
                                            $monthStart,
                                            $monthNumber,
                                            $quarterSum,
                                            PaymentCalendarSearch::$paymentTypeByType[$typeID]
                                        ); ?>
                                        <td class="<?= $isFutureDate ? null : ' tooltip2-hover '; ?> quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>"
                                            data-collapse-cell-total
                                            data-id="<?= $cell_id[$quarter] ?>"
                                            data-fact-amount="<?= $tooltipData['factAmount']; ?>"
                                            data-plan-amount="<?= $tooltipData['planAmount']; ?>"
                                            data-diff-amount="<?= $tooltipData['diff']; ?>"
                                            data-deviation="<?= $tooltipData['deviation'] > 0 ? "+{$tooltipData['deviation']}" : $tooltipData['deviation']; ?>"
                                            style="background-color: <?= $isFutureDate ? '#ffffff' : $searchModel->getItemColor($tooltipData['deviation']); ?>;"
                                        >
                                            <div class="pl-1 pr-1">
                                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                                $quarterSum = 0; ?>
                                            </div>
                                        </td>
                                    <?php endif; ?>
                                <?php endforeach ?>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if ($class == 'expence'): ?>
                    <tr>
                        <td class="pl-2 pr-2 pt-3 pb-3">
                            <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                                <div class="text_size_14 weight-700 mr-2 nowrap">
                                    <?= PlanFactSearch::$growingBalanceLabelByType[$typeID]; ?>
                                </div>
                            </div>
                        </td>
                        <?php $key = $quarterSum = 0; ?>
                        <?php foreach (PlanFactSearch::$month as $monthNumber => $monthText): ?>
                            <?php $key++;
                            $quarter = (int)ceil($key / 3);
                            $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                            $flowSum = isset($data['growingBalanceByBlock'][PlanFactSearch::$purseBlockByType[$typeID]][$monthNumber]) ?
                                $data['growingBalanceByBlock'][PlanFactSearch::$purseBlockByType[$typeID]][$monthNumber]['flowSum'] : 0;
                            if ($isFutureDate) {
                                $flowSum = 0;
                            } ?>

                            <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                                <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                    <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                                </div>
                            </td>
                            <?php $quarterSum += $flowSum; ?>
                            <?php if ($key % 3 == 0): ?>
                                <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                                    <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                        <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                        $quarterSum = 0; ?>
                                    </div>
                                </td>
                            <?php endif; ?>

                        <?php endforeach; ?>

                    </tr>
                <?php endif; ?>

            <?php endforeach; ?>
            <!-- TOTALS -->
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Результат по месяцу
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                    $totalFlowSum = isset($data['balance']['totalFlowSum']) ? $data['balance']['totalFlowSum'] : 0;
                    $flowSum = isset($data['balance'][$monthNumber]) ? $data['balance'][$monthNumber] : 0;
                    if ($isFutureDate) {
                        $totalFlowSum -= $flowSum;
                        $flowSum = 0;
                    }
                    ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                        </div>
                    </td>
                    <?php $quarterSum += $flowSum; ?>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?php echo TextHelper::invoiceMoneyFormat($quarterSum, 2);
                                $quarterSum = 0; ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            <tr>
                <td class="pl-2 pr-2 pt-3 pb-3">
                    <div class="d-flex flex-nowrap align-items-center pl-1 pr-1">
                        <div class="text_size_14 weight-700 mr-2 nowrap">
                            Остаток на конец месяца
                        </div>
                    </div>
                </td>
                <?php $key = $quarterSum = 0; ?>
                <?php foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthText): ?>
                    <?php $key++;
                    $quarter = (int)ceil($key / 3);
                    $isFutureDate = $isCurrentYear && $monthNumber > date('m');
                    $flowSum = isset($data['growingBalance'][$monthNumber]) ?
                        $data['growingBalance'][$monthNumber] : 0;
                    if ($isFutureDate) {
                        $flowSum = 0;
                    } ?>

                    <td class="pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? '' : 'd-none' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell>
                        <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                            <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                        </div>
                    </td>
                    <?php if ($key % 3 == 0): ?>
                        <td class="quarter-block pl-2 pr-2 pt-3 pb-3 nowrap <?= $d_monthes[$quarter] ? 'd-none' : '' ?>" data-id="<?= $cell_id[$quarter] ?>" data-collapse-cell-total>
                            <div class="pl-1 pr-1 weight-700 text-dark-alternative">
                                <?= TextHelper::invoiceMoneyFormat($flowSum, 2); ?>
                            </div>
                        </td>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tr>
            </tbody>
        </table>
    </div>
</div>