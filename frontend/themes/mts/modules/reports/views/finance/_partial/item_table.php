<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.01.2019
 * Time: 2:19
 */

use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\components\grid\DropDownSearchDataColumn;
use common\components\helpers\Html;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\components\grid\GridView;
use frontend\rbac\permissions\Cash;
use yii\widgets\Pjax;
use common\models\company\CheckingAccountant;
use common\components\ImageHelper;
use common\models\cash\CashFlowsBase;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\rbac\permissions;

$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);


/* @var $this yii\web\View
 * @var $searchModel FlowOfFundsReportSearch
 * @var $itemsDataProvider \yii\data\ActiveDataProvider
 */
?>
<div class="items-table-block  pt-1 pb-1 hidden">
<div class="row align-items-center flex-nowrap">
    <div class="column">
        <h4 class="caption mb-2">Детализация</h4>
    </div>
    <div class="column ml-auto">
        <div class="form-group d-flex flex-nowrap align-items-center justify-content-end mb-0">
            <!--<label class="label mr-3 mb-2 weight-700 text_size_14 text-dark" for="view">Суммировать</label>
            <div class="dropdown dropdown-view mb-2 mr-1 width-160">
                <input class="form-filter-control" value="Не сумировать" id="view" type="text" autocomplete="off" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="form-control-shevron svg-icon input-toggle text-grey-light">
                    <use xlink:href="images/svg-sprite/svgSprite.svg#shevron"></use>
                </svg>
                <div class="form-filter-drop dropdown-menu" aria-labelledby="view">
                    <ul class="form-filter-list list-clr">
                        <li><a href="#">Не сумировать</a></li>
                        <li><a href="#">Cумировать</a></li>
                    </ul>
                </div>
            </div>-->
            <a class="button-regular button-regular_red ml-2 mb-2 close-item-table" href="javascript:;">Закрыть</a>
        </div>
    </div>
</div>

<?php $pjax = Pjax::begin([
    'id' => 'odds-items_pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>
<?php if (isset($itemsDataProvider)): ?>
    <div class="wrap wrap_padding_none">
        <div class="custom-scroll-table">
            <div class="table-wrap">
            <?= GridView::widget([
                'dataProvider' => $itemsDataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'nav-pagination list-clr',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center joint-operation-checkbox-td',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) {
                            if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                $typeCss = 'income-item';
                                $income = bcdiv($flows['amount'], 100, 2);
                                $expense = 0;
                            } else {
                                $typeCss = 'expense-item';
                                $income = 0;
                                $expense = bcdiv($flows['amount'], 100, 2);
                            }

                            return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                'class' => 'joint-operation-checkbox ' . $typeCss,
                                'value' => $flows['id'],
                                'data' => [
                                    'income' => $income,
                                    'expense' => $expense,
                                ],
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'value' => function ($flows) use ($searchModel) {
                            return !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE]) ?
                                DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
                        },
                    ],
                    [
                        'attribute' => 'amountIncome',
                        'label' => 'Приход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {
                            return is_numeric($flows['amountIncome'])
                                ? \common\components\TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2)
                                : $flows['amountIncome'];
                        },
                    ],
                    [
                        'attribute' => 'amountExpense',
                        'label' => 'Расход',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'contentOptions' => [
                            'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                        ],
                        'value' => function ($flows) {
                            return is_numeric($flows['amountExpense'])
                                ? \common\components\TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2)
                                : $flows['amountExpense'];
                        },
                    ],
                    [
                        'attribute' => 'payment_type',
                        'label' => 'Тип оплаты',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_DATE])) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        /* @var $checkingAccountant CheckingAccountant */
                                        $checkingAccountant = CheckingAccountant::find()->andWhere([
                                            'rs' => $model->rs,
                                            'company_id' => $model->company->id,
                                        ])->orderBy(['type' => SORT_ASC])->one();
                                        if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                            return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                    'class' => 'little_logo_bank',
                                                    'style' => 'display: inline-block;',
                                                ]);
                                        }
                                        return 'Банк <i class="fa fa-bank m-r-sm" style="color: #4276a4;"></i>';
                                    case CashOrderFlows::tableName():
                                        return 'Касса <i class="fa fa-money m-r-sm" style="color: #4276a4;"></i>';
                                    case CashEmoneyFlows::tableName():
                                        return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #4276a4;"></i>';
                                    default:
                                        return '';
                                }
                            }
                            return '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'contractor_id',
                        'label' => 'Контрагент',
                        'headerOptions' => [
                            'width' => '30%',
                            'class' => 'nowrap-normal max10list',
                        ],
                        'format' => 'raw',
                        'filter' => !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE]) ?
                            $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
                        'value' => function ($flows) use ($searchModel) {
                            if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE])) {
                                /* @var $contractor Contractor */
                                $contractor = Contractor::findOne($flows['contractor_id']);
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $model = CashEmoneyFlows::findOne($flows['id']);
                                        break;
                                    default:
                                        return '';
                                }
                                return $contractor !== null ?
                                    ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                    ($model->cashContractor ?
                                        ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                    );
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '30%',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                if ($flows['description']) {
                                    $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                    return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                } else {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        default:
                                            return '';
                                    }
                                    if ($invoiceArray = $model->getInvoices()->all()) {
                                        $linkArray = [];
                                        foreach ($invoiceArray as $invoice) {
                                            $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                '/documents/invoice/view',
                                                'type' => $invoice->type,
                                                'id' => $invoice->id,
                                            ]);
                                        }

                                        return join(', ', $linkArray);
                                    }
                                }
                            }

                            return '';
                        },
                    ],
                    [
                        'attribute' => 'billPaying',
                        'label' => 'Опл. счета',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                switch ($flows['tb']) {
                                    case CashBankFlows::tableName():
                                        $model = CashBankFlows::findOne($flows['id']);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $model = CashOrderFlows::findOne($flows['id']);
                                        $billInvoices = $model->getBillInvoices();
                                        if ($billInvoices && $model->getAvailableAmount() > 0) {
                                            return $billInvoices . '<br />' .
                                                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', '/cash/order/update?id=' . $model->id, [
                                                    'title' => 'Уточнить',
                                                    'data' => [
                                                        'toggle' => 'modal',
                                                        'target' => '#update-movement',
                                                    ],
                                                ]);
                                        }

                                        return $billInvoices;
                                    case CashEmoneyFlows::tableName():
                                        return '';
                                    default:
                                        return '';
                                }

                                return $model->billPaying;
                            }

                            return '';
                        },
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'reason_id',
                        'label' => 'Статья',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'filter' => empty($searchModel->group) ?
                            array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                            ['' => 'Все статьи'],
                        'format' => 'raw',
                        'value' => function ($flows) use ($searchModel) {
                            if (empty($searchModel->group)) {
                                $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                                return $reason ?: '-';
                            }

                            return '';
                        },
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update}<br>{delete}',
                        'headerOptions' => [
                            'width' => '3%',
                        ],
                        'contentOptions' => [
                            'class' => 'action-line',
                        ],
                        'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                        'buttons' => [
                            'update' => function ($url, $model, $key) use ($searchModel, $activeTab) {
                                $url = 'javascript:;';
                                switch ($model['tb']) {
                                    case CashBankFlows::tableName():
                                        $url = Url::to(['/cash/bank/update', 'id' => $model['id']]);
                                        break;
                                    case CashOrderFlows::tableName():
                                        $url = Url::to(['/cash/order/update', 'id' => $model['id']]);
                                        break;
                                    case CashEmoneyFlows::tableName():
                                        $url = Url::to(['/cash/e-money/update', 'id' => $model['id']]);
                                        break;
                                }
                                return Html::a('<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use></svg>', $url, [
                                    'title' => 'Изменить',
                                    'class' => 'update-flow-item button-clr link mr-1',
                                    'data' => [
                                        'toggle' => 'modal',
                                        'target' => '#update-movement',
                                    ],
                                ]);
                            },
                            'delete' => function ($url, $model) use ($searchModel, $activeTab) {
                                return \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use></svg>',
                                        'class' => 'delete-flow-item button-clr link',
                                        'tag' => 'a',
                                    ],
                                    'options' => [
                                        'id' => 'delete-flow-item-' . $model['id'],
                                    ],
                                    'confirmUrl' => Url::to(['delete-flow-item', 'id' => $model['id'], 'tb' => $model['tb']]),
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите удалить операцию?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php Pjax::end(); ?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? \yii\helpers\Html::a($this->render('//svg-sprite', ['ico' => 'article']).' <span>Статья</span>', '#many-item', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
        $canDelete ? \yii\bootstrap\Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
            'data-pjax' => 0
        ]) : null,
    ],
]); ?>

<?php
$js = <<<SCRIPT

// SummarySelect

$(document).on('change', '.joint-operation-checkbox', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    $('.joint-operation-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').addClass('visible check-true');
        $('#summary-container .total-count').text(countChecked);
        $('#summary-container .total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container .total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container .total-difference').closest("td").hide();
        } else {
            $('#summary-container .total-difference').closest("td").show();
        }
        $('#summary-container .total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').removeClass('visible check-true');
    }
});

$(document).on("pjax:complete", "#odds-items_pjax", function(e) {
    $('#summary-container').removeClass('visible check-true');
});


SCRIPT;

$this->registerJs($js);
?>

<?php if (false): ?>
<div class="portlet box darkblue items-table-block hidden">
    <div class="portlet-title">
        <div class="caption" style="padding-top: 10px;width: 45%;"></div>
        <div class="filter">
            <?= Html::a('Дата', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_DATE,
            ]); ?>
            <?= Html::a('Контрагент', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_CONTRACTOR,
            ]); ?>
            <?= Html::a('Тип оплат', 'javascript:;', [
                'class' => 'btn',
                'data-attr' => FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE,
            ]); ?>
            <?= Html::a('Заркыть', 'javascript:;', [
                'class' => 'btn close-odds',
            ]); ?>
        </div>
        <div class="actions joint-operations pull-right"
             style="display:none; width: 195px;padding: 7px 0 0 0!important;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
                'style' => 'margin-right: 10px;',
            ]); ?>
            <?= Html::a('<i class="fa fa-list" style="padding-right: 3px;"></i>Статья', '#many-item', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?php $pjax = Pjax::begin([
                'id' => 'odds-items_pjax',
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <?php if (isset($itemsDataProvider)): ?>
                <?= GridView::widget([
                    'dataProvider' => $itemsDataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                        'id' => 'items-table',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $itemsDataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center joint-operation-checkbox-td',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) {
                                if ($flows['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                    $typeCss = 'income-item';
                                } else {
                                    $typeCss = 'expense-item';
                                }

                                return Html::checkbox("flowId[{$flows['tb']}][]", false, [
                                    'class' => 'joint-operation-checkbox ' . $typeCss,
                                    'value' => $flows['id'],
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'date',
                            'label' => 'Дата',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'value' => function ($flows) use ($searchModel) {
                                return !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE]) ?
                                    DateHelper::format($flows['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
                            },
                        ],
                        [
                            'attribute' => 'amountIncome',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'style' => 'display: ' . (!empty($searchModel->income_item_id) ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                return is_numeric($flows['amountIncome'])
                                    ? \common\components\TextHelper::invoiceMoneyFormat($flows['amountIncome'], 2)
                                    : $flows['amountIncome'];
                            },
                        ],
                        [
                            'attribute' => 'amountExpense',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                            ],
                            'contentOptions' => [
                                'style' => 'display: ' . (!empty($searchModel->expenditure_item_id) ? 'table-cell;' : 'none;'),
                            ],
                            'value' => function ($flows) {
                                return is_numeric($flows['amountExpense'])
                                    ? \common\components\TextHelper::invoiceMoneyFormat($flows['amountExpense'], 2)
                                    : $flows['amountExpense'];
                            },
                        ],
                        [
                            'attribute' => 'payment_type',
                            'label' => 'Тип оплаты',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_CONTRACTOR, FlowOfFundsReportSearch::GROUP_DATE])) {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            /* @var $checkingAccountant CheckingAccountant */
                                            $checkingAccountant = CheckingAccountant::find()->andWhere([
                                                'rs' => $model->rs,
                                                'company_id' => $model->company->id,
                                            ])->orderBy(['type' => SORT_ASC])->one();
                                            if ($checkingAccountant && $checkingAccountant->sysBank && $checkingAccountant->sysBank->little_logo_link) {
                                                return $image = 'Банк ' . ImageHelper::getThumb($checkingAccountant->sysBank->getUploadDirectory() . $checkingAccountant->sysBank->little_logo_link, [32, 32], [
                                                        'class' => 'little_logo_bank',
                                                        'style' => 'display: inline-block;',
                                                    ]);
                                            }
                                            return 'Банк <i class="fa fa-bank m-r-sm" style="color: #4276a4;"></i>';
                                        case CashOrderFlows::tableName():
                                            return 'Касса <i class="fa fa-money m-r-sm" style="color: #4276a4;"></i>';
                                        case CashEmoneyFlows::tableName():
                                            return 'E-money <i class="flaticon-wallet31 m-r-sm m-l-n-xs" style="color: #4276a4;"></i>';
                                        default:
                                            return '';
                                    }
                                }
                                return '';
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'contractor_id',
                            'label' => 'Контрагент',
                            'headerOptions' => [
                                'width' => '30%',
                                'class' => 'nowrap-normal max10list',
                            ],
                            'format' => 'raw',
                            'filter' => !in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE]) ?
                                $searchModel->getContractorFilterItems() : ['' => 'Все контрагенты'],
                            'value' => function ($flows) use ($searchModel) {
                                if (!in_array($searchModel->group, [FlowOfFundsReportSearch::GROUP_PAYMENT_TYPE, FlowOfFundsReportSearch::GROUP_DATE])) {
                                    /* @var $contractor Contractor */
                                    $contractor = Contractor::findOne($flows['contractor_id']);
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $model = CashEmoneyFlows::findOne($flows['id']);
                                            break;
                                        default:
                                            return '';
                                    }
                                    return $contractor !== null ?
                                        ('<span title="' . htmlspecialchars($contractor->nameWithType) . '">' . $contractor->nameWithType . '</span>') :
                                        ($model->cashContractor ?
                                            ('<span title="' . htmlspecialchars($model->cashContractor->text) . '">' . $model->cashContractor->text . '</span>') : ''
                                        );
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'description',
                            'label' => 'Назначение',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '30%',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (empty($searchModel->group)) {
                                    if ($flows['description']) {
                                        $description = mb_substr($flows['description'], 0, 50) . '<br>' . mb_substr($flows['description'], 50, 50);

                                        return Html::label(strlen($flows['description']) > 100 ? $description . '...' : $description, null, ['title' => $flows['description']]);
                                    } else {
                                        switch ($flows['tb']) {
                                            case CashBankFlows::tableName():
                                                $model = CashBankFlows::findOne($flows['id']);
                                                break;
                                            case CashOrderFlows::tableName():
                                                $model = CashOrderFlows::findOne($flows['id']);
                                                break;
                                            case CashEmoneyFlows::tableName():
                                                $model = CashEmoneyFlows::findOne($flows['id']);
                                                break;
                                            default:
                                                return '';
                                        }
                                        if ($invoiceArray = $model->getInvoices()->all()) {
                                            $linkArray = [];
                                            foreach ($invoiceArray as $invoice) {
                                                $linkArray[] = Html::a($model->formattedDescription . $invoice->fullNumber, [
                                                    '/documents/invoice/view',
                                                    'type' => $invoice->type,
                                                    'id' => $invoice->id,
                                                ]);
                                            }

                                            return join(', ', $linkArray);
                                        }
                                    }
                                }

                                return '';
                            },
                        ],
                        [
                            'attribute' => 'billPaying',
                            'label' => 'Опл. счета',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (empty($searchModel->group)) {
                                    switch ($flows['tb']) {
                                        case CashBankFlows::tableName():
                                            $model = CashBankFlows::findOne($flows['id']);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($flows['id']);
                                            $billInvoices = $model->getBillInvoices();
                                            if ($billInvoices && $model->getAvailableAmount() > 0) {
                                                return $billInvoices . '<br />' .
                                                    Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', '/cash/order/update?id=' . $model->id, [
                                                        'title' => 'Уточнить',
                                                        'data' => [
                                                            'toggle' => 'modal',
                                                            'target' => '#update-movement',
                                                        ],
                                                    ]);
                                            }

                                            return $billInvoices;
                                        case CashEmoneyFlows::tableName():
                                            return '';
                                        default:
                                            return '';
                                    }

                                    return $model->billPaying;
                                }

                                return '';
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'reason_id',
                            'label' => 'Статья',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'filter' => empty($searchModel->group) ?
                                array_merge(['' => 'Все статьи', 'empty' => '-'], $searchModel->getReasonFilterItems()) :
                                ['' => 'Все статьи'],
                            'format' => 'raw',
                            'value' => function ($flows) use ($searchModel) {
                                if (empty($searchModel->group)) {
                                    $reason = ($flows['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) ? $flows['incomeItemName'] : $flows['expenseItemName'];

                                    return $reason ?: '-';
                                }

                                return '';
                            },
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update}<br>{delete}',
                            'headerOptions' => [
                                'width' => '3%',
                            ],
                            'contentOptions' => [
                                'class' => 'action-line',
                            ],
                            'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                            'buttons' => [
                                'update' => function ($url, $model, $key) use ($searchModel, $activeTab) {
                                    $url = 'javascript:;';
                                    switch ($model['tb']) {
                                        case CashBankFlows::tableName():
                                            $url = Url::to(['/cash/bank/update', 'id' => $model['id']]);
                                            break;
                                        case CashOrderFlows::tableName():
                                            $url = Url::to(['/cash/order/update', 'id' => $model['id']]);
                                            break;
                                        case CashEmoneyFlows::tableName():
                                            $url = Url::to(['/cash/e-money/update', 'id' => $model['id']]);
                                            break;
                                    }
                                    return Html::a("<span aria-hidden='true' class='icon-pencil'></span>", $url, [
                                        'title' => 'Изменить',
                                        'class' => 'update-flow-item',
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#update-movement',
                                        ],
                                    ]);
                                },
                                'delete' => function ($url, $model) use ($searchModel, $activeTab) {
                                    return ConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                            'class' => 'delete-flow-item',
                                            'tag' => 'a',
                                        ],
                                        'options' => [
                                            'id' => 'delete-flow-item-' . $model['id'],
                                        ],
                                        'confirmUrl' => Url::to(['delete-flow-item', 'id' => $model['id'], 'tb' => $model['tb']]),
                                        'confirmParams' => [],
                                        'message' => 'Вы уверены, что хотите удалить операцию?',
                                    ]);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            <?php endif; ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
<?php endif; ?>
</div>

<div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <h4 class="modal-title text-center mb-4">Вы уверены, что хотите удалить выбранные операции?</h4>
            <div class="text-center">
                <?= \yii\bootstrap\Html::a('Да', null, [
                    'class' => 'modal-many-delete button-clr button-regular button-hover-transparent button-width-medium mr-2',
                    'data-url' => Url::to(['many-delete-flow-item']),
                ]); ?>
                <button class="button-clr button-regular button-hover-transparent button-width-medium ml-1" type="button" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade odds-model-movement" id="update-movement" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 id="js-modal_update_title" class="modal-title">Изменить движение</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h4 class="modal-title mb-4">Изменить статью</h4>
                <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                    </svg>
                </button>
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['many-flow-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">
                                Для типа
                            </label>
                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= \yii\bootstrap\Html::radio(null, true, [
                                            'label' => 'Приход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-12 label',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-income_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group row">
                            <label class="col-12" for="cashbankflowsform-flow_type">Для типа</label>

                            <div class="col-12">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-12">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Расход изменить на:',
                                            'labelOptions' => [
                                                'class' => '',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-3',
                            ],
                            'wrapperOptions' => [
                                'class' => '',
                            ],
                            'options' => [
                                'id' => 'js-expenditure_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '100%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="mt-3 d-flex justify-content-between">
                        <?= Html::submitButton('Сохранить', [
                            'class' => 'button-regular button-width button-regular_red button-clr',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
                    </div>

                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php /* todo: many actions modals
<div id="many-delete" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        Вы уверены, что хотите удалить выбранные операции?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('ДА', null, [
                            'class' => 'btn darkblue pull-right modal-many-delete',
                            'data-url' => Url::to(['many-delete-flow-item']),
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue" data-dismiss="modal">
                            НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h1>Изменить статью</h1>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['many-flow-item']),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group">
                            <label class="col-xs-12 col-md-3 control-label width-label bold-text"
                                   for="cashbankflowsform-flow_type">
                                Для типа
                            </label>

                            <div class="col-xs-10 col-md-8 margin-left-15">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Приход <span style="padding-left: 30px;">изменить на:</span>',
                                            'labelOptions' => [
                                                'class' => 'radio-inline m-l-n-md',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label width-label font-bold',
                            ],
                            'wrapperOptions' => [
                                'class' => 'col-md-9',
                            ],
                            'options' => [
                                'id' => 'js-income_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '97%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group">
                            <label class="col-xs-12 col-md-3 control-label width-label bold-text"
                                   for="cashbankflowsform-flow_type">
                                Для типа
                            </label>

                            <div class="col-xs-10 col-md-8 margin-left-15">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Расход <span style="padding-left: 30px;">изменить на:</span>',
                                            'labelOptions' => [
                                                'class' => 'radio-inline m-l-n-md',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($searchModel, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label width-label font-bold',
                            ],
                            'wrapperOptions' => [
                                'class' => 'col-md-9',
                            ],
                            'options' => [
                                'id' => 'js-expenditure_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '97%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="form-actions">
                        <div class="row action-buttons" id="buttons-fixed">
                            <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                                    'data-style' => 'expand-right',
                                    'style' => 'width: 130px!important;',
                                ]); ?>
                                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                                    'title' => 'Сохранить',
                                ]); ?>
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%;">
                                <?php if (Yii::$app->request->isAjax): ?>
                                    <?= Html::button('Отменить', [
                                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                                        'data-dismiss' => 'modal',
                                        'style' => 'width: 130px!important;',
                                    ]) ?>
                                    <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                                        'data-dismiss' => 'modal',
                                    ]) ?>
                                <?php else: ?>
                                    <?= Html::a('Отменить', ['index'], [
                                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                                        'data-dismiss' => 'modal',
                                        'style' => 'width: 130px!important;',
                                    ]); ?>
                                    <?= Html::a('<i class="fa fa-reply fa-2x"></i></button>', ['index'], [
                                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                                        'data-dismiss' => 'modal',
                                    ]); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>
*/ ?>

<?php $this->registerJs('
$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();

    var button = $(event.relatedTarget);
    var modal = $(this);

    modal.find(".modal-body").load(button.attr("href"));

});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(".close-item-table").on("click", function() {
    var $block = $(".items-table-block");
    $block.find(".caption").html("Детализация");
    $block.find(".wrap_padding_none").html("");
    $block.addClass("hidden");
});
'); ?>