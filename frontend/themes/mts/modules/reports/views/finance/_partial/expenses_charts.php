<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2019
 * Time: 16:18
 */

use \miloschuman\highcharts\Highcharts;
use common\components\helpers\Month;
use frontend\modules\reports\models\ExpensesSearch;

/* @var $searchModel ExpensesSearch
 * @var $activeTab integer
 */
?>
<div class="charts-expenses row">
    <div class="col-md-7 expenses-chart" style="height: 250px;">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart' => [
                    'type' => 'column',
                    'events' => [
                        'load' => new \yii\web\JsExpression('function () {
                            var titleX = +$(".expenses-chart").width() - +$(".expenses-chart .highcharts-plot-border").width() - 12;
                            
                            $(".expenses-chart .highcharts-title").attr("x", titleX);
                        }'),
                    ],
                ],
                'legend' => [
                    'layout' => 'vertical',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'y' => 33,
                    'floating' => true,
                    'borderWidth' => 0,
                    'backgroundColor' => '#FFFFFF',
                    'shadow' => true
                ],
                'tooltip' => [
                    'shared' => true,
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => [
                    'text' => 'Расходы по оплате',
                    'align' => 'left',
                ],
                'yAxis' => [
                    ['min' => 0, 'index' => 0, 'title' => ''],
                ],
                'xAxis' => [
                    'categories' => array_values(Month::$monthShort),
                ],
                'series' => [
                    [
                        'name' => 'Факт',
                        'data' => $searchModel->getFlowAmountByPeriod(),
                        'color' => 'rgba(255,213,139,1)'
                    ],
                    [
                        'name' => 'План',
                        'data' => $searchModel->getFlowAmountByPeriod(true),
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' => 'rgba(50,50,50,1)',
                            'radius' => 10
                        ],
                        'type' => 'spline',
                        'lineWidth' => 0,
                        'states' => [
                            'hover' => [
                                'lineWidthPlus' => 0,
                            ],
                        ],
                    ]
                ],
            ],
        ]); ?>
    </div>
    <div class="structure-chart col-md-5">
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'chart' => [
                    'type' => 'column',
                    'inverted' => true,
                    'events' => [
                        'load' => new \yii\web\JsExpression('function () {
                            var titleX = +$(".structure-chart").width() - +$(".structure-chart .highcharts-plot-border").width() - 12;
                            
                            $(".structure-chart .highcharts-title").attr("x", titleX);
                        }'),
                    ],
                ],
                'legend' => [
                    'layout' => 'vertical',
                    'align' => 'right',
                    'verticalAlign' => 'bottom',
                    'y' => -30,
                    'floating' => true,
                    'borderWidth' => 0,
                    'backgroundColor' => '#FFFFFF',
                    'shadow' => true
                ],
                'plotOptions' => [
                    'column' => [
                        'dataLabels' => [
                            'enabled' => true,
                        ],
                        'grouping' => false,
                        'shadow' => false,
                        'borderWidth' => 0
                    ]
                ],
                'tooltip' => [
                    'shared' => true,
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => [
                    'text' => 'Структура расходов',
                    'align' => 'left',
                ],
                'yAxis' => [
                    ['min' => 0, 'index' => 0, 'title' => ''],
                ],
                'xAxis' => [
                    ['categories' => array_values($searchModel->getMaxItemsFlow(true))],

                ],
                'series' => [
                    [
                        'name' => 'Факт',
                        'pointPadding' => 0,
                        'data' => $searchModel->getFlowAmountByItems(),
                        'color' => 'rgba(195,195,195,1)',
                    ],
                    [
                        'name' => 'План',
                        'pointPadding' => 0.25,
                        'data' => $searchModel->getFlowAmountByItems(true),
                        'color' => 'rgba(247,163,92,.9)',
                    ]
                ]
            ],
        ]); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };
    });
</script>