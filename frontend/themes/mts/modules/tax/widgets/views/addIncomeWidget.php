<?php

use common\components\TaxRobotHelper;
use common\models\cash\CashFlowsBase;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;
use frontend\components\Icon;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $company common\models\Company */
/* @var $accountArray common\models\company\CheckingAccountant[] */
/* @var $cashbox common\models\cash\Cashbox */
/* @var $hasIncomes boolean */
/* @var $hasPeriodIncomes boolean */

$p = Banking::currentRouteEncode();
$bankUrl = [
    '/cash/banking/default/select',
    'p' => $p,
];
foreach ($accountArray as $account) {
    if (($class = Banking::classByBik($account->bik)) !== null) {
        $bankUrl = [
            '/cash/banking/'.$class::ALIAS.'/default/index',
            'account_id' => $account->id,
            'p' => Banking::currentRouteEncode(),
        ];
        break;
    }
}
$redirectUrl = Url::to(Banking::currentRoute());
?>

<?php Modal::begin([
    'id' => 'add-income-modal',
    'title' => 'Добавить ДОХОД'.Html::tag('div', 'для расчета налогов и заполнения декларации', [
        'style' => 'font-weight: normal; font-size: 16px;',
    ]),
    'options' => [
        'class' => 'fade small-modal',
    ],
    'size' => 'modal-sm',
]) ?>

    <?= Html::a(Icon::get('bank-3', [
        'class' => 'mr-3',
    ]).'Загрузить выписку из банка', $bankUrl, [
        'class' => 'button-regular button-hover-content-red w-100 mb-3 banking-module-open-link',
    ]) ?>

    <?= Html::a(Icon::get('1c', [
        'class' => 'mr-3',
    ]).'Загрузить выписку файлом', [
        "/cash/banking/default/index",
        'p' => $p,
        's' => 'f',
    ], [
        'class' => 'button-regular button-hover-content-red w-100 mb-3 banking-module-open-link',
    ]) ?>

    <?= Html::a('Загрузить выписку из ОФД', [
        '/cash/ofd/default/index',
        'p' => $p,
    ], [
        'class' => 'button-regular button-hover-content-red w-100 mb-3 ofd-module-open-link',
    ]) ?>

    <div class="dropdown">
        <?= Html::button('Добавить вручную', [
            'class' => 'button-regular button-hover-content-red w-100',
            'data-toggle' => 'dropdown',
            'type' => 'button',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false',
        ]) ?>
        <ul class="dropdown-menu p-0 border" style="right: 0;">
            <li>
                <?= Html::a('Добавить по банку', [
                    'create-movement',
                    'type' => TaxRobotHelper::TYPE_BANK,
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'redirect' => $redirectUrl,
                    'skipDate' => 1,
                    'canAddContractor' => 1,
                    'canAddAccount' => 1
                ], [
                    'class' => 'update-movement-link update-bank-movement-link text-center p-2 w-100 inline-block link',
                    'data' => [
                        'pjax' => '0',
                    ],
                ]) ?>
            </li>
            <li class="border-top">
                <?= Html::a('Добавить по кассе', [
                    'create-movement',
                    'type' => TaxRobotHelper::TYPE_ORDER,
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'redirect' => $redirectUrl,
                    'skipDate' => 1,
                    'canAddContractor' => 1,
                    'id' => $cashbox->id,
                ], [
                    'class' => 'update-movement-link update-order-movement-link text-center p-2 w-100 inline-block link',
                    'data' => [
                        'pjax' => '0',
                    ],
                ]) ?>
            </li>
        </ul>
    </div>

    <div class="hidden mt-3 next-step-note text-danger text-center">
        Для перехода к РАСЧЕТУ НАЛОГОВ,
        <br>
        необходимо добавить доход за
        <?= $taxRobot->period->label ?>
    </div>

<?php Modal::end(); ?>

<?= BankingModalWidget::widget() ?>

<?= OfdModalWidget::widget([
    'pageTitle' => $this->title,
    'pageUrl' => $redirectUrl,
]) ?>
