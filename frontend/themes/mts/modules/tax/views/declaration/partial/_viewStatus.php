<?php

use frontend\themes\mts\components\Icon;
use common\models\document\status\TaxDeclarationStatus;
use common\components\date\DateHelper;
use common\models\Agreement;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\TaxDeclaration
 * @var $isIp boolean
 * @var $declarationOsnoHelper \frontend\modules\tax\models\DeclarationOsnoHelper
 */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass = $status->getIcon();

$company = $model->company;
$ifns = $company->ifns;

$statusIcon = [
    TaxDeclarationStatus::STATUS_CREATED => 'check-2',
    TaxDeclarationStatus::STATUS_PRINTED => 'print',
    TaxDeclarationStatus::STATUS_SEND => 'envelope',
    TaxDeclarationStatus::STATUS_ACCEPTED => 'check-double',
    TaxDeclarationStatus::STATUS_CORRECTED => 'new-doc',
];
$statusCssClass = [
    TaxDeclarationStatus::STATUS_CREATED => 'paid',
    TaxDeclarationStatus::STATUS_PRINTED => 'paid',
    TaxDeclarationStatus::STATUS_SEND => 'paid',
    TaxDeclarationStatus::STATUS_ACCEPTED => 'paid',
    TaxDeclarationStatus::STATUS_CORRECTED => 'paid',
];
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="pl-2 pr-2 w-100">
        <div class="button-regular <?= $statusCssClass[$status->id] ?> mb-3 pl-3 pr-3 w-100">
            <?= Icon::get($statusIcon[$status->id], ['class' => 'mr-1']) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto mr-1">
                <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ? : $model->created_at) ?>
            </span>
        </div>
    </div>
</div>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?php if ($isIp): ?>
                Налоговая декларация за
            <?php elseif ($model->nds): ?>
                Декларация на добавленную стоимость за
            <?php elseif ($model->org): ?>
                Декларация на прибыль организации за
            <?php elseif ($model->balance): ?>
                Бухгалтерский баланс за
            <?php elseif ($model->szvm): ?>
                Сведения о застрахованных лицах за
            <?php else: ?>
                Единая (упрощенная) налоговая декларация ОСНО за
            <?php endif; ?>
        </span>
        <span>
            <?php if ($isIp): ?>
                <?= $model->tax_year ?> год
            <?php else: ?>
                <?= date('Y') == $model->tax_year ? $declarationOsnoHelper->period->label : "{$model->tax_year} год"; ?>
            <?php endif; ?>
        </span>
    </div>
    <?php if ($isIp): ?>
        <div class="about-card-item">
            <span class="text-grey">
                Сумма налогов за год:
            </span>
            <span>
                <?= \common\components\TextHelper::invoiceMoneyFormat($model->taxDeclarationQuarters[3]['tax_rate'] *
                    $model->taxDeclarationQuarters[3]['income_amount']) ?> руб.
            </span>
        </div>
    <?php endif; ?>
    <div class="about-card-item">
        <span class="text-grey">
            Дата декларации:
        </span>
        <span>
            <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            В инспекцию:
        </span>
        <span>
            <?= $company->ifns_ga . ', ' . $ifns->gb ?>
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Адрес инспекции:
        </span>
        <span>
            <?= $ifns->g1 ?>
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Ответственный:
        </span>
        <span>
            <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
        </span>
    </div>
</div>
<?php if (!$model->nds): ?>
    <div class="title-small mb-3 nowrap">
        <strong class="mr-1">Способы сдать декларацию УСН</strong>
    </div>
    <div class="about-card mb-3 mt-1">
        <div class="about-card-item">
            <span class="d-inline-block mb-1 text-grey">1. Отрпавить по почте</span>
            <div>
                Для этого
                <?= Html::a('распечатайте декларацию', [
                    '/tax/'.($isIp ? 'robot' : 'declaration-osno').'declaration-print',
                    'actionType' => 'pdf',
                    'id' => $model->id,
                    'filename' => $model->getPrintTitle().'.pdf',
                    'end' => !$isIp,
                ], [
                    'class' => 'link',
                    'target' => '_blank',
                ]) ?>
                в 1-м экземпляре и
                <?= Html::a('опись вложения', [
                    '/tax/'.($isIp ? 'robot' : 'declaration-osno').'/inventory-print',
                    'actionType' => 'pdf',
                    'id' => $model->id,
                    'filename' => 'Опись_'.$model->getPrintTitle().'.pdf',
                ], [
                    'class' => 'link',
                    'target' => '_blank',
                ]) ?>
                в 2-х экземплярах, подпишите их, поставьте на титульном листе декларации печать (если она у вас есть).
                Отправьте отчёт ценным письмом с описью вложения на адрес инспекции.
                У вас на руках должен остаться экземпляр описи с отметкой работника почты, который подтвердит сдачу отчёта.
            </div>
        </div>
        <div class="about-card-item">
            <span class="d-inline-block mb-1 text-grey">2. Отправьте через электронную систему сдачи отчётност</span>
            <div>
                Для этого
                <?= Html::a('скачайте декларацию', [
                    '/tax/declaration/xml',
                    'id' => $model->id,
                ], [
                    'class' => 'link',
                    'target' => '_blank',
                    'download' => true,
                ]) ?>
                в формате для электронной отчетности, загрузите её в вашу систему и подпишите вашей ЭЦП.
            </div>
        </div>
        <div class="about-card-item">
            <span class="d-inline-block mb-1 text-grey">3. Отнести отчёт в инспекцию самостоятельно</span>
            <div>
                Для этого
                <?= Html::a('распечатайте декларацию', [
                    '/tax/'.($isIp ? 'robot' : 'declaration-osno').'/declaration-print',
                    'actionType' => 'pdf',
                    'id' => $model->id,
                    'filename' => $model->getPrintTitle().'.pdf',
                    'end' => !$isIp,
                ], [
                    'class' => 'link',
                    'target' => '_blank',
                ]) ?>
                в 2-х экземплярах, подпишите каждый, на титульном листе поставьте печать (если она у вас есть), и отнесите в инспекцию.
            </div>
        </div>
    </div>
<?php endif; ?>
