<?php

use common\models\document\status\TaxDeclarationStatus;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model frontend\modules\tax\models\TaxDeclaration
 * @var $isIp boolean
 */

$pageRoute = ['/tax/declaration/view', 'id' => $model->id];

?>

<div class="wrap wrap_btns fixed">
    <div class="row justify-content-between align-items-center">
        <?php if (FALSE && $canUpdate): ?>
            <div class="column">
                <button class="button-regular button-hover-transparent width-160 send-message-panel-trigger">
                    Отправить
                </button>
            </div>
        <?php endif; ?>
        <div class="column">
            <?php Html::a('Печать', [
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'filename' => $model->getPrintTitle(),
                'end' => !$isIp && !$model->nds && !$model->org && !$model->balance && !$model->szvm,
                'nds' => $model->nds,
                'org' => $model->org,
                'balance' => $model->balance,
                'szvm' => $model->szvm,
            ], [
                'class' => 'button-regular button-hover-transparent width-160',
                'target' => '_blank',
            ]); ?>
        </div>
        <div class="column">
            <span class="dropup">
                <?= Html::a(Icon::get('download', ['class' => 'mr-1']) . '<span class="ml-2">Скачать</span>', '#', [
                    'class' => 'button-regular button-hover-transparent width-160',
                    'data-toggle' => 'dropdown',
                ]); ?>
                <?= \yii\bootstrap4\Dropdown::widget([
                    'items' => [
                        [
                            'label' => 'Скачать PDF файл',
                            'encode' => false,
                            'url' => [
                                'declaration-print',
                                'actionType' => 'pdf',
                                'id' => $model->id,
                                'filename' => $model->getPdfFileName(),
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                                'data-pjax' => 0
                            ]
                        ],
                        [
                            'label' => 'Скачать файл для ИФНС',
                            'encode' => false,
                            'url' => [
                                'document-print',
                                'actionType' => 'pdf',
                                'id' => $model->id,
                                'filename' => $model->getPdfFileName(),
                                'end' => !$isIp && !$model->nds && !$model->org && !$model->balance && !$model->szvm,
                                'nds' => $model->nds,
                                'org' => $model->org,
                                'balance' => $model->balance,
                                'szvm' => $model->szvm,
                            ],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                    ],
                ]); ?>
            </span>
        </div>
        <div class="column">
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'button-regular button-hover-transparent width-160',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать эту декларацию?',
            ]); ?>
        </div>
        <?php if (Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS)): ?>
            <?php if (!$model->status_id == TaxDeclarationStatus::STATUS_ACCEPTED) : ?>
                <div class="column">
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Подписана',
                            'class' => 'button-regular button-hover-transparent width-160',
                        ],
                        'confirmUrl' => Url::to([
                            'update-status',
                            'id' => $model->id,
                            'status' => 'accepted'
                        ]),
                        'message' => 'Вы уверены, что хотите установить для декларации статус "Подписана"?'
                    ]); ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS)): ?>
            <div class="column">
                <?= Html::a('Отправить', [
                    '/cash/ofd/default/import',
                    'p' => \frontend\modules\cash\modules\ofd\components\Ofd::routeEncode($pageRoute),
                ], [
                    'class' => 'button-regular button-hover-transparent width-160 ofd-module-open-link',
                ]); ?>
                <?= \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget::widget([
                    'pageTitle' => $this->title,
                    'pageUrl' => Url::to($pageRoute),
                ]) ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
            <div class="column ml-auto">
                <?=ConfirmModalWidget::widget([
                    'options' => [
                        'id' => 'delete-confirm',
                    ],
                    'toggleButton' => [
                        'label' => 'Удалить',
                        'class' => 'button-regular button-hover-transparent width-160',
                    ],
                    'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => "Вы уверены, что хотите удалить эту декларацию?",
                ]) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
