<?php

use yii\helpers\Html;
use frontend\modules\tax\models\DeclarationBarcodeHelper;

if ($page == '001')
    $barcode_left = 'barcode-03012017.png';
elseif ($page == '002')
    $barcode_left = 'barcode-03012024.png';
elseif ($page == '003')
    $barcode_left = 'barcode-03012048.png';
else
    $barcode_left = '';

if (is_file(Yii::getAlias('@webroot')."/img/pdf417/barcode-top-{$model->id}-{$page}.png"))
    $barcode_right = "barcode-top-{$model->id}-{$page}.png";
else {
    $barcode_right = $guid = '';
}

$guid = DeclarationBarcodeHelper::printGuid($model, $page);

?>
<table class="table no-border">
    <tr>
        <td width="25%" rowspan="2" style="vertical-align:top;">
            <?= ($barcode_left) ? Html::img('@web/img/'.$barcode_left, ['width'=>'165px', 'class'=>'barcode']) : '';?>
        </td>
        <td class="font-size-11 text-right pad-r-5">ИНН</td>
        <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
        <td rowspan="2" class="text-center ver-top" style="padding-top:3px">
            <?= ($barcode_right) ? Html::img('@web/img/pdf417/'.$barcode_right, ['width'=>'194px', 'height' => '46px', 'class'=>'barcode']) : '';?>
        </td>
    </tr>
    <tr>
        <!--<td width="25%"></td>-->
        <td width="10%" class="font-size-11 text-right pad-r-5">КПП</td>
        <td width="18%" class="font-main"><?= str_repeat("&mdash;", 5) ?></td>
        <td width="6%" class="font-size-11 text-right pad-r-5">Стр.</td>
        <td width="12%" class="font-main"><?= $page ?></td>
        <!--<td></td>-->
    </tr>
    <tr>
        <td colspan="5"></td>
        <td class="text-center font-bold" style="padding:0;font-size:8px"><?= $guid ?></td>
    </tr>
</table>