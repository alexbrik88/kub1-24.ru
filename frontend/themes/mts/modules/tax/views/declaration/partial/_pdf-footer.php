<?php use yii\helpers\Html;

if (is_file(Yii::getAlias('@webroot')."/img/pdf417/barcode-bottom-{$model->id}-{$page}.png"))
    $barcode_bottom = "barcode-bottom-{$model->id}-{$page}.png";
else {
    $barcode_bottom = '';
}

?>

<?php if ($barcode_bottom): ?>
<table class="table no-border">
    <tr>
        <td width="30%"></td>
        <td width="5%" class="ver-mid" style="font-size:35px">+</td>
        <td width="43%" class="ver-top" style="padding-top:3px">
            <?= Html::img('@web/img/pdf417/'.$barcode_bottom, [ 'height' => '90px', 'class'=>'barcode']);?>
        </td>
        <td width="5%" class="ver-mid" style="font-size:35px">+</td>
        <td></td>
    </tr>
</table>
<?php endif; ?>