<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 27.03.2019
 * Time: 14:47
 */

use common\components\helpers\Html;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use frontend\modules\tax\models\DeclarationOsnoHelper;

/* @var DeclarationOsnoHelper $declarationHelper */

$declarationHelper = isset($declarationHelper) ? $declarationHelper : new DeclarationOsnoHelper();
?>
<table class="table-preview no-border">
    <tr>
        <td width="25%" rowspan="2" style="vertical-align:top;">
            <?= Html::img('@web/img/barcode-5108-5108.png', ['width' => '165px', 'class' => 'barcode']); ?>
        </td>
        <td class="font-size-11 text-right">ИНН</td>
        <td colspan="3" class="font-main">00<?= $model->company->inn ?></td>
        <td rowspan="2"></td>
    </tr>
    <tr>
        <td width="40px" class="font-size-11 text-right">КПП</td>
        <td width="18%" class="font-main"><?= $model->company->kpp ?: str_repeat("&mdash;", 5) ?></td>
        <td width="6%" class="font-size-11 text-right">Стр.</td>
        <td width="9%" class="font-main">001</td>
    </tr>
</table>
<table class="table-preview no-border" style="margin-top:8pt">
    <tr>
        <td class="font-small" style="font-size: 6pt !important;">
            Приложение №1 к приказу Министерства финансов Российской Федерации от 10.07.2007 № 62н
        </td>
        <td></td>
    </tr>
    <tr>
        <td width="75%" class="text-center font-bold font-size-11">
            Единая (упрощённая) налоговая декларация
        </td>
        <td class="text-right font-bold">
            Форма по КНД 1151085
        </td>
    </tr>
    <tr>
        <td class="font-small" style="font-size: 6pt !important;">
            Вид документа: 1 - первичный, 3 - корректирующий (через дробь номер корректировки)
        </td>
        <td></td>
    </tr>
</table>
<table class="table-preview no-border">
    <tr>
        <td width="9%" class="text-right">Вид документа</td>
        <td width="7%" class="text-center font-main"><?= ($model->document_correction_number) ? "3" : "1" ?></td>
        <td width="2%" class="text-center font-main">/</td>
        <td width="8%" class="text-center font-main"><?= ($model->document_correction_number) ?: "&mdash;" ?></td>
        <td class="text-right">Отчетный год</td>
        <td width="10%" class="text-right font-main"><?= $model->tax_year ?></td>
    </tr>
</table>
<table class="table-preview no-border">
    <tr>
        <td width="22%" class="font-bold">Предоставляется в</td>
        <td class="font-size-11 text-center text-uppercase"><?= $model->company->ifns->gb ?></td>
        <td width="5%" class="font-bold text-right">Код</td>
        <td width="10%" class="text-right font-main"><?= $model->company->ifns_ga ?></td>
    </tr>
    <tr>
        <td></td>
        <td class="tip">(наименование налогового органа)</td>
        <td></td>
        <td></td>
    </tr>
</table>
<table class="table-preview no-border" style="margin-top:5px;">
    <tr>
        <td class="font-main border-bottom text-uppercase">
            <?= $model->company->companyType->name_full . ' ' . $model->company->name_full ?>
        </td>
    </tr>
    <tr>
        <td class="tip">
            (полное наименование организации/фамилия, имя, отчество физического лица)
        </td>
    </tr>
</table>
<table class="table-preview no-border">
    <tr>
        <td width="75%" style="font-size: 8pt;">
            Код объекта административно-территориального деления согласно Общероссийскому
            классификатору объектов административно-территориального деления (Код по ОКТМО)
        </td>
        <td width="4%"></td>
        <td class="font-main">
            <?= $model->company->oktmo ?: "&nbsp;" ?>
        </td>
    </tr>
    <tr>
        <td style="font-size: 8pt;">
            Код вида экономической деятельности согласно Общероссийскому классификатору видов
            экономической деятельности (ОКВЭД)
        </td>
        <td></td>
        <td class="font-main">
            <?= $model->company->okved ?: "&nbsp;" ?>
        </td>
    </tr>
    <tr>
        <td style="font-size: 8pt;">
            Код строки 10
        </td>
        <td></td>
        <td class="font-main"></td>
    </tr>
</table>
<table class="table" style="margin-top:5pt">
    <tr class="middle">
        <td class="text-center" style="font-size: 8pt;border: 1px solid #000000;">
            Налоги, по которым представляется декларация налогоплательщиком, не осуществляющим
            операции, в результате которых происходит движение денежных средств на его счетах в банках
            (в кассе организации), и не имеющим по этим налогам объектов налогообложения
        </td>
        <td width="20%" class="text-center" style="font-size: 8pt;border: 1px solid #000000;">
            Номер главы части второй Налогового кодекса Российской Федерации
        </td>
        <td width="10%" class="text-center" style="font-size: 8pt;border: 1px solid #000000;">
            Налоговый (отчетный) период
        </td>
        <td width="10%" class="text-center" style="font-size: 8pt;border: 1px solid #000000;">
            Номер квартала
        </td>
    </tr>
    <tr>
        <td class="text-center" style="font-size: 8pt;border: 1px solid #000000;">1</td>
        <td class="text-center" style="font-size: 8pt;border: 1px solid #000000;">2</td>
        <td class="text-center" style="font-size: 8pt;border: 1px solid #000000;">3</td>
        <td class="text-center" style="font-size: 8pt;border: 1px solid #000000;">4</td>
    </tr>
    <tr class="no-border bold">
        <td class="pad-5 no-border font-main text-uppercase no-border">Налог на добавленную стоимость</td>
        <td class="pad-5 no-border font-main" style="padding-left:10pt;">21</td>
        <td class="pad-5 no-border font-main text-center">3</td>
        <td class="pad-5 no-border font-main text-center">
            <?= substr($declarationHelper->period->id, 0, 4) == $model->tax_year ? $declarationHelper->getQuarterNumber() : '04'; ?>
        </td>
    </tr>
    <tr class="no-border bold">
        <td class="pad-5 no-border font-main text-uppercase no-border">Налог на прибыль организации</td>
        <td class="pad-5 no-border font-main" style="padding-left:10pt;">25</td>
        <td class="pad-5 no-border font-main text-center">
            <?= substr($declarationHelper->period->id, 0, 4) == $model->tax_year ? $declarationHelper->getTaxPeriod() : 0; ?>
        </td>
        <td class="pad-5 no-border font-main text-center">---</td>
    </tr>
    <?php for ($num = 0; $num <= 6; $num++): ?>
        <tr class="no-border bold">
            <td class="pad-5 no-border font-main text-uppercase no-border"><?= str_repeat("&mdash;", 19) ?></td>
            <td class="pad-5 no-border font-main" style="padding-left:10pt;"><?= str_repeat("&mdash;", 4) ?></td>
            <td class="pad-5 no-border font-main text-center"><?= str_repeat("&mdash;", 1) ?></td>
            <td class="pad-5 no-border font-main text-center"><?= str_repeat("&mdash;", 2) ?></td>
        </tr>
    <?php endfor; ?>
</table>
<table class="table-preview no-border">
    <tr>
        <td width="40%" style="font-size: 8pt;">
            Номер контактного телефона налогоплательщика
        </td>
        <td class="border-bottom font-size-11">
            <?= $model->company->phone ?>
        </td>
    </tr>
</table>
<table class="table-preview no-border" style="margin-top:5pt;font-size: 8pt;">
    <tr>
        <td width="22%">Декларация составлена на</td>
        <td width="10%" class="font-main text-center">1</td>
        <td width="13%">страницах</td>
        <td width="37%" class="text-right" style="padding-right:5pt">с приложением подтверждающих документов или их
            копий на
        </td>
        <?= getEmptyDottedBoxes(3) ?>
        <td width="10%" class="middle" style="padding-left:5pt">листах</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="8" class="border-bottom" style="padding-top:5pt"></td>
    </tr>
</table>
<table class="table-preview no-border" style="font-size: 8pt;">
    <tr>
        <td width="50%" style="border-right:1px solid #000;padding-right:10pt;vertical-align: top;">
            <table class="table-preview no-border">
                <tr>
                    <td class="font-bold">
                        Достоверность и полноту сведений, <br/>
                        указанных в настоящей декларации, подтверждаю:
                    </td>
                </tr>
                <tr>
                    <td class="font-bold">
                        Для организации
                    </td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td width="20%">Руководитель</td>
                    <td class="border-bottom font-size-11 text-uppercase">
                        <?= ($model->company->company_type_id == CompanyType::TYPE_IP) ?
                            $model->company->getIpFio() :
                            $model->company->getChiefFio() ?>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="tip">(фамилия, имя, отчество)</td>
                </tr>
            </table>
            <table class="table-preview no-border view-date-line">
                <tr>
                    <td width="15%">Подпись</td>
                    <td width="25%" class="border-bottom"></td>
                    <td width="10%">Дата</td>
                    <?= $declarationHelper->getDottedBoxes(DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE), 'days'); ?>
                    <td width="5%" class="text-center">=</td>
                    <?= $declarationHelper->getDottedBoxes(DateHelper::format($model->document_date, 'm', DateHelper::FORMAT_DATE), 'month'); ?>
                    <td width="5%" class="text-center">=</td>
                    <?= $declarationHelper->getDottedBoxes(DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE), 'year'); ?>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="font-bold text-center">М.П.</td>
                    <td colspan="11"></td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td class="font-bold">
                        Для физического лица
                    </td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td width="15%">Подпись</td>
                    <td width="25%" class="border-bottom"></td>
                    <td width="10%">Дата</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(4) ?>
                    <td></td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:5pt">
                <tr>
                    <td width="28%" class="font-bold">
                        Представитель
                    </td>
                    <td width="69%"><?= str_repeat("&mdash;", 20) ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="3" class="tip">
                        (полное наименование организации / фамилия, имя отчество физического лица)
                    </td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td width="15%">Подпись</td>
                    <td width="25%" class="border-bottom"></td>
                    <td width="10%">Дата</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(4) ?>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="font-bold text-center">М.П.</td>
                    <td colspan="11"></td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td width="100%" class="border-bottom"></td>
                </tr>
                <tr>
                    <td class="tip" style="padding-top:5pt;padding-bottom:3pt">
                        (наименование документа, подтверждающего полномочия представителя)
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="border-bottom"></td>
                </tr>
            </table>
        </td>
        <td width="50%" style="padding-left:10pt;vertical-align: top;">
            <table class="table-preview no-border">
                <tr>
                    <td class="font-bold">
                        <br/> Заполняется работником налогового органа
                    </td>
                </tr>
                <tr>
                    <td>
                        Сведения о представлении налоговой декларации
                    </td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td>
                        Данная декларация представлена <span style="font-size:6pt">(нужное отметить знаком V)</span>
                    </td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td width="15%">лично</td>
                    <?= getEmptyDottedBoxes(1) ?>
                    <td width="25%" class="text-center">по почте</td>
                    <?= getEmptyDottedBoxes(1) ?>
                    <td width="40%" class="text-center">через представителя</td>
                    <?= getEmptyDottedBoxes(1) ?>
                    <td></td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td width="10%">на</td>
                    <?= getEmptyDottedBoxes(3) ?>
                    <td style="padding-left:10pt">страницах</td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td width="60%">с приложением подтверждающих документов или их копий на</td>
                    <?= getEmptyDottedBoxes(3) ?>
                    <td width="15%" style="padding-left:5pt">листах</td>
                    <td></td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td width="40%">Дата представления декларации</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(2) ?>
                    <td width="5%" class="text-center">=</td>
                    <?= getEmptyDottedBoxes(4) ?>
                    <td></td>
                </tr>
            </table>
            <table class="table-preview no-border" style="margin-top:6pt;">
                <tr>
                    <td width="30%">
                        Зарегистрирован<br/>
                        за №
                    </td>
                    <?= getEmptyDottedBoxes(13) ?>
                    <td></td>
                </tr>
            </table>
            <table class="table-preview no-border">
                <tr>
                    <td width="50%" class="border-bottom">&nbsp;</td>
                    <td width="2%"></td>
                    <td class="border-bottom"></td>
                </tr>
                <tr>
                    <td class="tip">(Фамилия, И. О.)</td>
                    <td></td>
                    <td class="tip">(Подпись)</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
