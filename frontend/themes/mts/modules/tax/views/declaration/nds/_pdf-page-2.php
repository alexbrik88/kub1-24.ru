<?php
use frontend\modules\tax\models\TaxDeclaration;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value, $repeat = 10)
    {
        return ($value > 0) ? round($value) : str_repeat("&mdash;", $repeat);
    }
}

/** @var TaxDeclaration $model */
?>

<div class="page-content-in p-center pad-pdf-p">

    <table class="table no-border">
        <tr>
            <td width="25%" rowspan="2" style="vertical-align:top;">
                <?= Html::img('@web/img/barcode_00310024.png', ['width'=>'165px']);?>
            </td>
            <td class="font-size-11 text-right">ИНН</td>
            <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <!--<td width="25%"></td>-->
            <td width="40px" class="font-size-11 text-right">КПП</td>
            <td width="18%" class="font-main"><?= $model->company->kpp ?: str_repeat("&mdash;", 5) ?></td>
            <td width="6%" class="font-size-11 text-right">Стр.</td>
            <td width="9%" class="font-main">002</td>
            <!--<td></td>-->
        </tr>
    </table>
    <br/>
    <br/>
    <table class="table no-border">
        <tr>
            <td class="font-size-11 font-bold text-center">
                Раздел 1. Сумма налога <br/>
                подлежащая уплате в бюджет (возмещению из бюджета), <br/>
                по данным налогоплательщика
            </td>
        </tr>
    </table>
    <br/>
    <br/>

    <table class="table no-border">
        <tr>
            <td width="42%" class="small-italic">Показатели</td>
            <td width="15%" class="small-italic">Код строки</td>
            <td class="small-italic">Значения показателей</td>
        </tr>
        <tr style="padding-bottom:20px;">
            <td class="small-italic">1</td>
            <td class="small-italic">2</td>
            <td class="small-italic">3</td>
        </tr>
        <!--010-->
        <tr class="middle pp">
            <td>Код по ОКТМО</td>
            <td class="text-center">010</td>
            <td class="font-main text-right"><?= $columnData['010'] ?></td>
        </tr>
        <!--020-->
        <tr class="middle pp">
            <td>Код бюджетной классификации</td>
            <td class="text-center">020</td>
            <td class="font-main text-right"><?= $columnData['020'] ?></td>
        </tr>
        <!--030-->
        <tr class="middle pp">
            <td>Сумма налога, подлежащая уплате в бюджет в соответствии с пунктом 5 статьи 173 Налогового кодекса Российской Федерации <br/>
                <span style="font-size:7pt">(величина разницы суммы строк 200 раздела 3, 130 раздела 4, 160 раздела 6 и суммы строк 210 раздела 3,
                    120 раздела 4, 080 раздела 5, 090 раздела 5, 170 раздела 6 >= 0)</span></td>
            <td class="text-center">030</td>
            <td class="font-main text-right"><?= printOrEmpty($columnData['030']) ?></td>
        </tr>
        <!--040-->
        <tr class="middle pp">
            <td>Сумма налога, подлежащая уплате в бюджет в соответствии с пунктом 1 статьи 173 Налогового кодекса Российской Федерации <br/>
                <span style="font-size:7pt">(величина разницы суммы строк 200 раздела 3, 130 раздела 4, 160 раздела 6 и суммы строк 210 раздела 3,
                    120 раздела 4, 080 раздела 5, 090 раздела 5, 170 раздела 6 < 0)</span></td>
            <td class="text-center">040</td>
            <td class="font-main text-right"><?= $columnData['040'] ?></td>
        </tr>
        <!--050-->
        <tr class="middle pp">
            <td>Сумма налога, исчисленная к возмещению из бюджета в соответствии с пунктом 2 статьи 173 Налогового кодекса Российской Федерации</td>
            <td class="text-center">050</td>
            <td class="font-main text-right"><?= printOrEmpty($columnData['050']) ?></td>
        </tr>
        <!--060-->
        <tr class="middle pp">
            <td>Регистрационный номер договора инвестиционного товарищества</td>
            <td class="text-center">060</td>
            <td class="font-main text-right"><?= printOrEmpty($columnData['060']) ?></td>
        </tr>
        <!--070-->
        <tr class="middle pp">
            <td>Дата начала действия договора инвестиционного товарищества</td>
            <td class="text-center">070</td>
            <td class="font-main text-right">
                &mdash;&mdash;.&mdash;&mdash;.&mdash;&mdash;&mdash;&mdash;
            </td>
        </tr>
        <!--080-->
        <tr class="middle pp">
            <td>Дата окончания действия договора инвестиционного товарищества</td>
            <td class="text-center">080</td>
            <td class="font-main text-right">
                &mdash;&mdash;.&mdash;&mdash;.&mdash;&mdash;&mdash;&mdash;
            </td>
        </tr>

    </table>

    <br/><br/><br/><br/><br/><br/><br/><br/>

    <table class="table no-border">
        <tr>
            <td class="text-center font-bold">
                Достоверность и полноту сведений, указанных на данной странице, подтверждаю:
            </td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td width="25%"></td>
            <td width="15%" class="text-center border-bottom">
            </td>
            <td>(подпись)</td>
            <td width="15%" class="text-center border-bottom">
                <?= $formattedDate ?>
            </td>
            <td>(дата)</td>
            <td></td>
        </tr>
    </table>
</div>