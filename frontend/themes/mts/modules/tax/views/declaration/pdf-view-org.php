<?php

$declarationHelper = isset($declarationHelper) ? $declarationHelper : new \frontend\modules\tax\models\DeclarationOsnoHelper();
if (!function_exists('getEmptyDottedBoxes')) {
    function getEmptyDottedBoxes($count)
    {
        $str = '';
        for ($i = 0; $i < $count; $i++) {
            $str .= '<td class="dotbox">&nbsp;&nbsp;</td>';
        }

        return $str;
    }
}
if (!function_exists('printOrEmpty')) {
    function printOrEmpty($value, $repeat = 11)
    {
        return ($value > 0) ? round($value) : str_repeat("&mdash;", $repeat);
    }
}


echo $this->render('org/_pdf-page-1', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
]);

echo $this->render('org/_pdf-page-2', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'columnData' => [
        '010' => $company->oktmo,
        '030' => '18210101011011000110',
        '040' => 0,
        '050' => 0,
        '060' => '18210101011011000110',
        '070' => 0,
        '080' => 0
    ]
]);

echo $this->render('org/_pdf-page-3', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '010' => 0,
        '020' => 0,
        '030' => 0,
        '040' => 0,
        '050' => 0,
        '060' => 0,
        '070' => 0,
        '080' => 0,
        '100' => 0,
        '110' => 0,
        '120' => 0,
        '130' => 0,
        '140' => '20.000',
        '150' => '3.00',
        '160' => '17.000',
        '170' => 0,
        '180' => 0,
        '190' => 0,
        '200' => 0,
    ]
]);

echo $this->render('org/_pdf-page-4', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '210' => 0,
        '220' => 0,
        '230' => 0,
        '240' => 0,
        '250' => 0,
        '260' => 0,
        '265' => 0,
        '266' => 0,
        '267' => 0,
        '270' => 0,
        '271' => 0,
        '280' => 0,
        '281' => 0,
        '290' => 0,
        '300' => 0,
        '310' => 0,
        '320' => 0,
        '330' => 0,
        '340' => 0,
        '350' => 0,
        '351' => 0,
    ]
]);

echo $this->render('org/_pdf-page-5', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '010' => 0,
        '011' => 0,
        '012' => 0,
        '013' => 0,
        '014' => 0,
        '020' => 0,
        '021' => 0,
        '022' => 0,
        '023' => 0,
        '024' => 0,
        '027' => 0,
        '030' => 0,
        '040' => 0,
        '100' => 0,
        '101' => 0,
        '102' => 0,
        '103' => 0,
        '104' => 0,
        '105' => 0,
        '106' => 0,
    ]
]);

echo $this->render('org/_pdf-page-6', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '010' => 0,
        '020' => 0,
        '030' => 0,
        '040' => 0,
        '041' => 0,
        '042' => 0,
        '043' => 0,
        '045' => 0,
        '046' => 0,
        '047' => 0,
        '048' => 0,
        '049' => 0,
        '050' => 0,
        '051' => 0,
        '052' => 0,
        '053' => 0,
        '054' => 0,
        '055' => 0,
        '059' => 0,
        '060' => 0,
        '061' => 0,
    ]
]);

echo $this->render('org/_pdf-page-7', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '070' => 0,
        '071' => 0,
        '072' => 0,
        '073' => 0,
        '080' => 0,
        '090' => 0,
        '100' => 0,
        '110' => 0,
        '120' => 0,
        '130' => 0,
        '131' => 0,
        '132' => 0,
        '133' => 0,
        '134' => 0,
        '135' => '1',
        '200' => 0,
        '201' => 0,
        '202' => 0,
        '204' => 0,
        '205' => 0,
        '206' => 0,
    ]
]);

echo $this->render('org/_pdf-page-8', [
    'model' => $model,
    'company' => $company,
    'declarationHelper' => $declarationHelper,
    'taxpayerOrganizationCode' => 1,
    'columnData' => [
        '300' => 0,
        '301' => 0,
        '302' => 0,
        '400' => 0,
        '401' => ['year' => '', 'sum' => 0],
        '402' => ['year' => '', 'sum' => 0],
        '403' => ['year' => '', 'sum' => 0],
    ]
]);