<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 14:33
 */

use frontend\modules\tax\models\TaxDeclaration;

/* @var TaxDeclaration $model
 * @var $taxpayerOrganizationCode string
 */
?>
<?= $this->render('_viewText-pages-head', [
    'model' => $model,
    'title' => 'Доходы от реализации и внереализационные доходы',
    'subtitle' => 'Приложение №1 к Листу 02',
    'pageNum'  => '005',
    'barcode' => 'barcode_00213073.png',
    'taxpayerOrganizationCode' => $taxpayerOrganizationCode
]); ?>
<table class="table-preview no-border">
    <tr>
        <td width="55%" class="small-italic">Показатели</td>
        <td width="10%" class="small-italic">Код строки</td>
        <td class="small-italic">Сумма в рублях</td>
    </tr>
    <tr style="padding-bottom:20px;">
        <td class="small-italic">1</td>
        <td class="small-italic">2</td>
        <td class="small-italic">3</td>
    </tr>
    <!--010-->
    <tr class="middle spp small">
        <td>Выручка от реализации - всего</td>
        <td class="text-center">010</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['010']) ?></td>
    </tr>
    <tr class="middle small">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--011-->
    <tr class="middle spp small">
        <td class="lp">выручка от реализации товаров (работ, услуг) собственного производства</td>
        <td class="text-center">011</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['011']) ?></td>
    </tr>
    <!--012-->
    <tr class="middle spp small">
        <td class="lp">выручка от реализации покупных товаров</td>
        <td class="text-center">012</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['012']) ?></td>
    </tr>
    <!--013-->
    <tr class="middle spp small">
        <td class="lp">выручка от реализации имущественных прав, за исключением доходов от реализации прав
            требований долга, указанных в Приложении № 3 к Листу 02</td>
        <td class="text-center">013</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['013']) ?></td>
    </tr>
    <!--014-->
    <tr class="middle spp small">
        <td class="lp">выручка от реализации прочего имущества</td>
        <td class="text-center">014</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['014']) ?></td>
    </tr>
    <!--020-->
    <tr class="middle spp small">
        <td>Выручка от реализации (выбытия, в т.ч. доход от погашения) ценных бумаг
            профессиональных участников рынка ценных бумаг - всего</td>
        <td class="text-center">020</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['020']) ?></td>
    </tr>
    <tr class="middle small">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <tr class="middle small">
        <td class="lp">сумма отклонения фактической выручки от реализации (выбытия)
            ценных бумаг, обращающихся и не обращающихся на организованном рынке ценных бумаг, ниже:</td>
        <td></td>
        <td></td>
    </tr>
    <!--021-->
    <tr class="middle spp small">
        <td class="lp2">минимальной (расчетной) цены по обращающимся ценным бумагам</td>
        <td class="text-center">021</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['021']) ?></td>
    </tr>
    <!--022-->
    <tr class="middle spp small">
        <td class="lp2">минимальной (расчетной) цены по необращающимся ценным бумагам</td>
        <td class="text-center">022</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['022']) ?></td>
    </tr>
    <!--023-->
    <tr class="middle spp small">
        <td>Выручка от реализации (выбытия, в том числе доход от погашения) ценных бумаг,
            обращающихся на организованном рынке ценных бумаг - всего</td>
        <td class="text-center">023</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['023']) ?></td>
    </tr>
    <!--024-->
    <tr class="middle spp small">
        <td class="lp">в том числе сумма отклонения от минимальной (расчетной) цены</td>
        <td class="text-center">024</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['024']) ?></td>
    </tr>
    <!--027-->
    <tr class="middle spp small">
        <td>Выручка от реализации предприятия как имущественного комплекса</td>
        <td class="text-center">027</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['027']) ?></td>
    </tr>
    <!--030-->
    <tr class="middle spp small">
        <td>Выручка от реализации по операциям, отраженным в Приложении № 3 к Листу 02
            (стр. 340 Приложения № 3 к Листу 02)</td>
        <td class="text-center">030</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['030']) ?></td>
    </tr>
    <!--040-->
    <tr class="middle spp small">
        <td>Итого сумма доходов от реализации<br/> (стр. 010 + стр. 020 + стр. 023 + стр. 027 + стр. 030)</td>
        <td class="text-center">040</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['040']) ?></td>
    </tr>
    <!--100-->
    <tr class="middle spp small">
        <td>Внереализационные доходы - всего</td>
        <td class="text-center">100</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['100']) ?></td>
    </tr>
    <tr class="middle small">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--101-->
    <tr class="middle spp small">
        <td class="lp">в виде дохода прошлых лет, выявленного в отчетном (налоговом) периоде</td>
        <td class="text-center">101</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['101']) ?></td>
    </tr>
    <!--102-->
    <tr class="middle spp small">
        <td class="lp">в виде стоимости полученных материалов или иного имущества при демонтаже или
            разборке при ликвидации выводимых из эксплуатации основных средств, при ремонте, модернизации,
            реконструкции, техническом перевооружении, частичной ликвидации основных средств</td>
        <td class="text-center">102</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['102']) ?></td>
    </tr>
    <!--103-->
    <tr class="middle spp small">
        <td class="lp">в виде безвозмездно полученного имущества (работ, услуг) или имущественных прав
            (кроме указанных в статье 251 Налогового кодекса Российской Федерации)</td>
        <td class="text-center">103</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['103']) ?></td>
    </tr>
    <!--104-->
    <tr class="middle spp small">
        <td class="lp">в виде стоимости излишков материально-производственных запасов и прочего имущества,
            которые выявлены в результате инвентаризации</td>
        <td class="text-center">104</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['104']) ?></td>
    </tr>
    <!--105-->
    <tr class="middle spp small">
        <td class="lp">сумма восстановленных расходов на капитальные вложения в соответствии с абзацем
            четвертым пункта 9 статьи 258 Налогового кодекса Российской Федерации</td>
        <td class="text-center">105</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['105']) ?></td>
    </tr>
    <!--106-->
    <tr class="middle spp small">
        <td class="lp">доходы, полученные профессиональными участниками рынка ценных бумаг,
            осуществляющими дилерскую деятельность, включая банки, по операциям с финансовыми инструментами
            срочных сделок, не обращающимися на организованном рынке</td>
        <td class="text-center">106</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['106']) ?></td>
    </tr>
</table>

