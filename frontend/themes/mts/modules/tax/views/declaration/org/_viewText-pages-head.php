<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 14:20
 */

use yii\helpers\Html;

/* @var $barcode string
 * @var $pageNum string
 * @var $subtitle string
 * @var $title string
 * @var $taxpayerOrganizationCode string
 */
?>
<table class="table-preview no-border">
    <tr>
        <td width="25%" rowspan="2" style="vertical-align:top;">
            <?= Html::img("@web/img/{$barcode}", ['width' => '165px']); ?>
        </td>
        <td class="font-size-11 text-right">ИНН</td>
        <td colspan="3" class="font-main" style="padding-left: 10px;"><?= $model->company->inn ?></td>
        <td rowspan="2"></td>
    </tr>
    <tr>
        <!--<td width="25%"></td>-->
        <td width="40px" class="font-size-11 text-right">КПП</td>
        <td width="18%" class="font-main" style="padding-left: 10px;"><?= $model->company->kpp ?></td>
        <td width="6%" class="font-size-11 text-right">Стр.</td>
        <td width="9%" class="font-main"><?= $pageNum ?></td>
        <!--<td></td>-->
    </tr>
</table>
<br/>
<?php if (isset($subtitle)): ?>
    <table class="table-preview no-border">
        <tr>
            <td class="font-bold text-right" style="font-size:7pt">
                <?= $subtitle ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<?php if (isset($title)): ?>
    <table class="table-preview no-border">
        <tr>
            <td class="font-bold text-center" style="padding:5pt;">
                <?= $title ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<table class="table-preview no-border">
    <tr>
        <td width="30%" class="ver-top">Признак налогоплательщика (код)</td>
        <td width="5%" class="ver-top text-center font-main"><?= $taxpayerOrganizationCode ?></td>
        <td colspan="4" style="font-size:6pt">
            1 - организация, не относящаяся к указанным по кодам 2, 3, 4 и 6 <br/>
            2 - сельскохозяйственный товаропроизводитель <br/>
            3 - резидент (участник) особой (свободной) экономической зоны <br/>
            4 - организация, осуществляющая деятельность на новом морском месторождении углеводородного сырья <br/>
            6 - резидент территории опережающего экономического развития <br/><br/><br/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td width="10%" style="font-size:6pt">Лицензия:</td>
        <td width="18%" style="font-size:6pt">серия &nbsp;&nbsp;<?= str_repeat("&mdash;", 8) ?></td>
        <td width="18%" style="font-size:6pt">номер &nbsp;&nbsp;<?= str_repeat("&mdash;", 8) ?></td>
        <td style="font-size:6pt">вид &nbsp;&nbsp;<?= str_repeat("&mdash;", 4) ?>
            &nbsp;&nbsp; / &nbsp;&nbsp;<?= str_repeat("&mdash;", 4) ?></td>
    </tr>
</table>
<br/>
