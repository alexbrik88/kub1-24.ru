<?php
use frontend\modules\tax\models\TaxDeclaration;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

/** @var TaxDeclaration $model */
?>

<div class="page-content-in p-center pad-pdf-p page-break">

    <?= $this->render('_pdf-pages-head', [
        'model' => $model,
        'subtitle' => 'Приложение №2 к Листу 02 (продолжение)',
        'pageNum'  => '007',
        'barcode' => 'barcode_00213097.png',
        'taxpayerOrganizationCode' => $taxpayerOrganizationCode
    ]) ?>

    <table class="table no-border">
        <tr>
            <td width="55%" class="small-italic">Показатели</td>
            <td width="10%" class="small-italic">Код строки</td>
            <td class="small-italic">Сумма в рублях</td>
        </tr>
        <tr style="padding-bottom:20px;">
            <td class="small-italic">1</td>
            <td class="small-italic">2</td>
            <td class="small-italic">3</td>
        </tr>

        <!--070-->
        <tr class="middle spp small">
            <td>Расходы, связанные с приобретением и реализацией (выбытием, в том числе погашением)
                ценных бумаг профессиональными участниками рынка ценных бумаг</td>
            <td class="text-center">070</td>
            <td class="font-main"><?= printOrEmpty($columnData['070']) ?></td>
        </tr>
        <!--071-->
        <tr class="middle spp small">
            <td class="lp">сумма отклонения от максимальной (расчетной) цены</td>
            <td class="text-center">071</td>
            <td class="font-main"><?= printOrEmpty($columnData['071']) ?></td>
        </tr>
        <!--072-->
        <tr class="middle spp small">
            <td>Расходы, связанные с приобретением и реализацией (выбытием, в том числе погашением)
                ценных бумаг, обращающихся на организованном рынке ценных бумаг</td>
            <td class="text-center">072</td>
            <td class="font-main"><?= printOrEmpty($columnData['072']) ?></td>
        </tr>
        <!--073-->
        <tr class="middle spp small">
            <td class="lp">сумма отклонения от максимальной (расчетной) цены</td>
            <td class="text-center">073</td>
            <td class="font-main"><?= printOrEmpty($columnData['073']) ?></td>
        </tr>

        <!--080-->
        <tr class="middle spp small">
            <td>Расходы по операциям, отраженным в Приложении № 3 к Листу 02 <br/>
                (стр. 350 Приложения № 3 к Листу 02)</td>
            <td class="text-center">080</td>
            <td class="font-main"><?= printOrEmpty($columnData['080']) ?></td>
        </tr>
        <!--090-->
        <tr class="middle spp small">
            <td>Суммы убытков прошлых лет по объектам обслуживающих производств и хозяйств,
                включая объекты жилищно-коммунальной и социально-культурной сферы, учитываемые в уменьшение
                прибыли текущего отчетного (налогового) периода, полученной по указанным видам деятельности</td>
            <td class="text-center">090</td>
            <td class="font-main"><?= printOrEmpty($columnData['090']) ?></td>
        </tr>
        <!--100-->
        <tr class="middle spp small">
            <td>Сумма убытка от реализации амортизируемого имущества,
                относящаяся к расходам текущего отчетного (налогового) периода</td>
            <td class="text-center">100</td>
            <td class="font-main"><?= printOrEmpty($columnData['100']) ?></td>
        </tr>
        <!--110-->
        <tr class="middle spp small">
            <td>Сумма убытка от реализации права на земельный участок,
                относящаяся к расходам текущего отчетного (налогового) периода</td>
            <td class="text-center">110</td>
            <td class="font-main"><?= printOrEmpty($columnData['110']) ?></td>
        </tr>
        <!--120-->
        <tr class="middle spp small">
            <td>Сумма надбавки, уплачиваемая покупателем предприятия как имущественного комплекса,
                относящаяся к расходам текущего отчетного (налогового) периода</td>
            <td class="text-center">120</td>
            <td class="font-main"><?= printOrEmpty($columnData['120']) ?></td>
        </tr>
        <!--130-->
        <tr class="middle spp small">
            <td>Итого признанных расходов (сумма строк 010, 020, 040, 059 - 070, 072, 080 - 120)</td>
            <td class="text-center">130</td>
            <td class="font-main"><?= printOrEmpty($columnData['130']) ?></td>
        </tr>
        <tr class="middle spp small">
            <td>Сумма амортизации за отчетный (налоговый) период, начисленная:</td>
            <td></td>
            <td></td>
        </tr>
        <!--131-->
        <tr class="middle spp small">
            <td class="lp">линейным методом</td>
            <td class="text-center">131</td>
            <td class="font-main"><?= printOrEmpty($columnData['131']) ?></td>
        </tr>
        <!--132-->
        <tr class="middle spp small">
            <td class="lp2">в том числе по нематериальным активам</td>
            <td class="text-center">132</td>
            <td class="font-main"><?= printOrEmpty($columnData['132']) ?></td>
        </tr>
        <!--133-->
        <tr class="middle spp small">
            <td class="lp">нелинейным методом</td>
            <td class="text-center">133</td>
            <td class="font-main"><?= printOrEmpty($columnData['133']) ?></td>
        </tr>
        <!--134-->
        <tr class="middle spp small">
            <td class="lp2">в том числе по нематериальным активам</td>
            <td class="text-center">134</td>
            <td class="font-main"><?= printOrEmpty($columnData['134']) ?></td>
        </tr>
        <!--135-->
        <tr class="middle spp small">
            <td>Метод начисления амортизации, отраженный в учетной политике (код)</td>
            <td class="text-center">135</td>
            <td>
                <table class="table no-border">
                    <tr>
                        <td width="13%" class="font-main">
                            <?= $columnData['135'] ?>
                        </td>
                        <td style="font-size:6pt">
                            1 - линейный <br/>
                            2 - нелинейный
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <!--200-->
        <tr class="middle spp small">
            <td>Внереализационные расходы - всего</td>
            <td class="text-center">200</td>
            <td class="font-main"><?= printOrEmpty($columnData['200']) ?></td>
        </tr>
        <tr class="middle small">
            <td class="lp">в том числе:</td>
            <td></td>
            <td></td>
        </tr>
        <!--201-->
        <tr class="middle spp small">
            <td class="lp">расходы в виде процентов по долговым обязательствам любого вида, в том числе процентов,
                начисленных по ценным бумагам и иным обязательствам, выпущенным (эмитированным) налогоплательщиком</td>
            <td class="text-center">201</td>
            <td class="font-main"><?= printOrEmpty($columnData['201']) ?></td>
        </tr>
        <!--202-->
        <tr class="middle spp small">
            <td class="lp">расходы по созданию резерва предстоящих расходов, направляемых на цели,
                обеспечивающие социальную защиту инвалидов</td>
            <td class="text-center">202</td>
            <td class="font-main"><?= printOrEmpty($columnData['202']) ?></td>
        </tr>
        <!--204-->
        <tr class="middle spp small">
            <td class="lp">расходы на ликвидацию выводимых из эксплуатации основных средств,
                на списание нематериальных активов, на ликвидацию объектов незавершенного строительства
                и иного имущества, охрану недр и другие аналогичные работы</td>
            <td class="text-center">204</td>
            <td class="font-main"><?= printOrEmpty($columnData['204']) ?></td>
        </tr>
        <!--205-->
        <tr class="middle spp small">
            <td class="lp">штрафы, пени и (или) иные санкции за нарушение договорных или долговых обязательств,
                возмещение причиненного ущерба</td>
            <td class="text-center">205</td>
            <td class="font-main"><?= printOrEmpty($columnData['205']) ?></td>
        </tr>
        <!--206-->
        <tr class="middle spp small">
            <td class="lp">расходы профессиональных участников рынка ценных бумаг, осуществляющих
                дилерскую деятельность, включая банки, по операциям с финансовыми инструментами
                срочных сделок, не обращающимися на организованном рынке</td>
            <td class="text-center">206</td>
            <td class="font-main"><?= printOrEmpty($columnData['206']) ?></td>
        </tr>
        
    </table>

</div>