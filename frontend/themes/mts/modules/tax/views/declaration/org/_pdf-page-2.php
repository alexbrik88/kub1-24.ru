<?php
use frontend\modules\tax\models\TaxDeclaration;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\helpers\Html;
use common\components\date\DateHelper;

$formattedDate = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

/** @var TaxDeclaration $model */
?>

<div class="page-content-in p-center pad-pdf-p page-break">

    <table class="table no-border">
        <tr>
            <td width="25%" rowspan="2" style="vertical-align:top;">
                <?= Html::img('@web/img/barcode_00213028.png', ['width'=>'165px']);?>
            </td>
            <td class="font-size-11 text-right">ИНН</td>
            <td colspan="3" class="font-main"><?= $model->company->inn ?></td>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <!--<td width="25%"></td>-->
            <td width="40px" class="font-size-11 text-right">КПП</td>
            <td width="18%" class="font-main"><?= $model->company->kpp ?></td>
            <td width="6%" class="font-size-11 text-right">Стр.</td>
            <td width="9%" class="font-main">002</td>
            <!--<td></td>-->
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td class="font-size-10 font-bold text-center">
                Раздел 1. Сумма налога, подлежащая уплате в бюджет, <br/>
                по данным налогоплательщика (налогового агента)
            </td>
        </tr>
        <tr>
            <td class="font-bold text-center" style="padding-top:5pt">
                Подраздел 1.1. Для организаций, уплачивающих авансовые платежи и налог на прибыль опганизаций
            </td>
        </tr>
    </table>
    <br/>

    <table class="table no-border">
        <tr>
            <td width="45%" class="small-italic">Показатели</td>
            <td width="10%" class="small-italic">Код строки</td>
            <td class="small-italic">Значения показателей</td>
        </tr>
        <tr style="padding-bottom:20px;">
            <td class="small-italic">1</td>
            <td class="small-italic">2</td>
            <td class="small-italic">3</td>
        </tr>
        <!--010-->
        <tr class="middle pp">
            <td>Код по ОКТМО</td>
            <td class="text-center">010</td>
            <td class="font-main text-right"><?= $columnData['010'] ?></td>
        </tr>
        <!--TITLE-->
        <tr class="middle pp">
            <td class="font-bold font-size-10">В федеральный бюджет</td>
            <td></td>
            <td></td>
        </tr>
        <!--030-->
        <tr class="middle pp">
            <td>Код бюджетной классификации</td>
            <td class="text-center">030</td>
            <td class="font-main text-right"><?= $columnData['030'] ?></td>
        </tr>
        <!--040-->
        <tr class="middle pp">
            <td>Сумма налога к доплате в рублях</td>
            <td class="text-center">040</td>
            <td class="font-main text-right"><?= $columnData['040'] ?></td>
        </tr>
        <!--050-->
        <tr class="middle pp">
            <td>Сумма налога к уменьшению в рублях</td>
            <td class="text-center">050</td>
            <td class="font-main text-right"><?= printOrEmpty($columnData['050']) ?></td>
        </tr>
        <!--TITLE-->
        <tr class="middle pp">
            <td class="font-bold font-size-10">В бюджет субъекта Российской Федерации</td>
            <td></td>
            <td></td>
        </tr>
        <!--060-->
        <tr class="middle pp">
            <td>Код бюджетной классификации</td>
            <td class="text-center">060</td>
            <td class="font-main text-right"><?= $columnData['060'] ?></td>
        </tr>
        <!--070-->
        <tr class="middle pp">
            <td>Сумма налога к доплате в рублях</td>
            <td class="text-center">070</td>
            <td class="font-main text-right"><?= $columnData['070'] ?></td>
        </tr>
        <!--080-->
        <tr class="middle pp">
            <td>Сумма налога к уменьшению в рублях</td>
            <td class="text-center">080</td>
            <td class="font-main text-right"><?= printOrEmpty($columnData['080']) ?></td>
        </tr>

    </table>

    <br/><br/><br/><br/><br/><br/><br/><br/>

    <table class="table no-border">
        <tr>
            <td class="text-center">
                Достоверность и полноту сведений, указанных на данной странице, подтверждаю:
            </td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td width="25%"></td>
            <td width="15%" class="text-center border-bottom">
            </td>
            <td>(подпись)</td>
            <td width="15%" class="text-center border-bottom">
                <?= $formattedDate ?>
            </td>
            <td>(дата)</td>
            <td></td>
        </tr>
    </table>
</div>