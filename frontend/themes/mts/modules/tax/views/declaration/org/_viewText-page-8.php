<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 14:48
 */

use frontend\modules\tax\models\TaxDeclaration;

/* @var TaxDeclaration $model
 * @var $taxpayerOrganizationCode string
 */
?>
<?= $this->render('_viewText-pages-head', [
    'model' => $model,
    'subtitle' => 'Приложение №2 к Листу 02 (продолжение)',
    'pageNum'  => '008',
    'barcode' => 'barcode_00213103.png',
    'taxpayerOrganizationCode' => $taxpayerOrganizationCode,
]); ?>
<table class="table-preview no-border">
    <tr>
        <td width="50%" class="small-italic">Показатели</td>
        <td width="10%" class="small-italic">Код строки</td>
        <td class="small-italic">Значения показателей</td>
    </tr>
    <tr style="padding-bottom:20px;">
        <td class="small-italic">1</td>
        <td class="small-italic">2</td>
        <td class="small-italic">3</td>
    </tr>
    <!--300-->
    <tr class="middle pp">
        <td>Убытки, приравниваемые к внереализационным расходам - всего</td>
        <td class="text-center">300</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['300']) ?></td>
    </tr>
    <tr class="middle pp">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--301-->
    <tr class="middle pp">
        <td class="lp">убытки прошлых налоговых периодов, выявленные в текущем отчетном (налоговом) периоде</td>
        <td class="text-center">301</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['301']) ?></td>
    </tr>
    <!--302-->
    <tr class="middle pp">
        <td class="lp">суммы безнадежных долгов, а в случае, если налогоплательщик принял решение о создании
            резерва по сомнительным долгам, суммы безнадежных долгов, не покрытые за счет средств резерва</td>
        <td class="text-center">302</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['302']) ?></td>
    </tr>
    <!--400-->
    <tr class="middle pp">
        <td>Корректировка налоговой базы на выявленные ошибки (искажения),
            относящиеся к прошлым налоговым периодам, приведшие к излишней уплате налога, всего</td>
        <td class="text-center">400</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['400']) ?></td>
    </tr>
    <tr class="middle pp">
        <td class="lp">в том числе за:</td>
        <td></td>
        <td></td>
    </tr>
    <!--401-->
    <tr class="middle pp">
        <td class="text-center"><?= printOrEmpty($columnData['401']['year'], 4) ?> год</td>
        <td class="text-center">401</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['401']['sum']) ?></td>
    </tr>
    <!--402-->
    <tr class="middle pp">
        <td class="text-center"><?= printOrEmpty($columnData['402']['year'], 4) ?> год</td>
        <td class="text-center">402</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['402']['sum']) ?></td>
    </tr>
    <!--403-->
    <tr class="middle pp">
        <td class="text-center"><?= printOrEmpty($columnData['403']['year'], 4) ?> год</td>
        <td class="text-center">403</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['403']['sum']) ?></td>
    </tr>
</table>

