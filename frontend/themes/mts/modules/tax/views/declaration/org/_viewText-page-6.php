<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 14:39
 */

use frontend\modules\tax\models\TaxDeclaration;

/* @var TaxDeclaration $model
 * @var $taxpayerOrganizationCode string
 */
?>
<?= $this->render('_viewText-pages-head', [
    'model' => $model,
    'title' => 'Расходы, связанные с производством и реализацией, внереализационные расходы <br/> и убытки, приравниваемые к внереализационным расходам',
    'subtitle' => 'Приложение №2 к Листу 02',
    'pageNum'  => '006',
    'barcode' => 'barcode_00213080.png',
    'taxpayerOrganizationCode' => $taxpayerOrganizationCode
]); ?>
<table class="table-preview no-border">
    <tr>
        <td width="55%" class="small-italic">Показатели</td>
        <td width="10%" class="small-italic">Код строки</td>
        <td class="small-italic">Сумма в рублях</td>
    </tr>
    <tr style="padding-bottom:20px;">
        <td class="small-italic">1</td>
        <td class="small-italic">2</td>
        <td class="small-italic">3</td>
    </tr>
    <!--010-->
    <tr class="middle spp small">
        <td>Прямые расходы, относящиеся к реализованным товарам (работам, услугам)</td>
        <td class="text-center">010</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['010']) ?></td>
    </tr>
    <!--020-->
    <tr class="middle spp small">
        <td>Прямые расходы налогоплательщиков, осуществляющих оптовую, мелкооптовую и розничную торговлю в
            текущем отчетном (налоговом) периоде, относящиеся к реализованным товарам</td>
        <td class="text-center">020</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['020']) ?></td>
    </tr>
    <!--030-->
    <tr class="middle spp small">
        <td class="lp">в том числе стоимость реализованных покупных товаров</td>
        <td class="text-center">030</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['030']) ?></td>
    </tr>
    <!--040-->
    <tr class="middle spp small">
        <td>Косвенные расходы - всего</td>
        <td class="text-center">040</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['040']) ?></td>
    </tr>
    <tr class="middle small">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--041-->
    <tr class="middle spp small">
        <td class="lp">суммы налогов и сборов, начисленные в порядке, установленном законодательством ]
            Российской Федерации о налогах и сборах, за исключением налогов, перечисленных в статье 270
            Налогового кодекса Российской Федерации</td>
        <td class="text-center">041</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['041']) ?></td>
    </tr>
    <tr class="middle small">
        <td class="lp">расходы на капитальные вложения в соответствии с абзацем вторым пункта
            9 статьи 258 Налогового кодекса Российской Федерации в размере:</td>
        <td></td>
        <td></td>
    </tr>
    <!--042-->
    <tr class="middle spp small">
        <td class="lp2">не более 10%</td>
        <td class="text-center">042</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['042']) ?></td>
    </tr>
    <!--043-->
    <tr class="middle spp small">
        <td class="lp2">не более 30%</td>
        <td class="text-center">043</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['043']) ?></td>
    </tr>
    <!--045-->
    <tr class="middle spp small">
        <td class="lp">расходы, осуществленные налогоплательщиком-организацией, использующим труд инвалидов,
            согласно подпункту 38 пункта 1 статьи 264 Налогового кодекса Российской Федерации</td>
        <td class="text-center">045</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['045']) ?></td>
    </tr>
    <!--046-->
    <tr class="middle spp small">
        <td class="lp">расходы налогоплательщиков - общественных организаций инвалидов, а также
            налогоплательщиков-учреждений, единственными собственниками имущества которых являются
            общественные организации инвалидов, согласно подпункту 39 пункта 1 статьи 264
            Налогового кодекса Российской Федерации</td>
        <td class="text-center">046</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['046']) ?></td>
    </tr>
    <!--047-->
    <tr class="middle spp small">
        <td class="lp">расходы на приобретение права на земельные участки, указанные в пунктах 1 и 2 статьи 264.1
            Налогового кодекса Российской Федерации, признаваемые расходами отчетного (налогового) периода - всего</td>
        <td class="text-center">047</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['047']) ?></td>
    </tr>
    <tr class="middle spp small">
        <td class="lp2">из них при признании расходов:</td>
        <td></td>
        <td></td>
    </tr>
    <!--048-->
    <tr class="middle spp small">
        <td class="lp2">в течение срока, определенного налогоплательщиком (не менее 5 лет)</td>
        <td class="text-center">048</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['048']) ?></td>
    </tr>
    <!--049-->
    <tr class="middle spp small">
        <td class="lp2">в размере, не превышающем 30% налоговой базы предыдущего налогового периода</td>
        <td class="text-center">049</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['049']) ?></td>
    </tr>
    <!--050-->
    <tr class="middle spp small">
        <td class="lp2">в течение срока, установленного договором при приобретении
            земельного участка на условиях рассрочки</td>
        <td class="text-center">050</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['050']) ?></td>
    </tr>
    <!--051-->
    <tr class="middle spp small">
        <td class="lp2">в течение срока действия договора аренды земельного участка, не подлежащего государственной регистрации</td>
        <td class="text-center">051</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['051']) ?></td>
    </tr>
    <!--052-->
    <tr class="middle spp small">
        <td class="lp">расходы на НИОКР</td>
        <td class="text-center">052</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['052']) ?></td>
    </tr>
    <!--053-->
    <tr class="middle spp small">
        <td class="lp2">в том числе не давшие положительного результата</td>
        <td class="text-center">053</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['053']) ?></td>
    </tr>
    <!--054-->
    <tr class="middle spp small">
        <td class="lp">из суммы по стр. 052 расходы на НИОКР по перечню, установленному Правительством Российской Федерации</td>
        <td class="text-center">054</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['054']) ?></td>
    </tr>
    <!--055-->
    <tr class="middle spp small">
        <td class="lp2">в том числе не давшие положительного результата</td>
        <td class="text-center">055</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['055']) ?></td>
    </tr>
    <!--059-->
    <tr class="middle spp small">
        <td>Стоимость реализованных имущественных прав (кроме прав требований долга, указанных в Приложении № 3 к Листу 02)</td>
        <td class="text-center">059</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['059']) ?></td>
    </tr>
    <!--060-->
    <tr class="middle spp small">
        <td>Цена приобретения реализованного прочего имущества и расходы, связанные с его реализацией</td>
        <td class="text-center">060</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['060']) ?></td>
    </tr>
    <!--061-->
    <tr class="middle spp small">
        <td>Стоимость чистых активов предприятия, реализованного как имущественный комплекс</td>
        <td class="text-center">061</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['061']) ?></td>
    </tr>
</table>