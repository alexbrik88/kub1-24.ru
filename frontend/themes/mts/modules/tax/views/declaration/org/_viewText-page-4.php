<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2019
 * Time: 14:30
 */

use frontend\modules\tax\models\TaxDeclaration;

/* @var TaxDeclaration $model
 * @var $taxpayerOrganizationCode string
 */
?>
<?= $this->render('_viewText-pages-head', [
    'model' => $model,
    'subtitle' => 'Лист 02 (Продолжение)',
    'pageNum' => '004',
    'barcode' => 'barcode_00213066.png',
    'taxpayerOrganizationCode' => $taxpayerOrganizationCode
]); ?>
<table class="table-preview no-border">
    <tr>
        <td width="53%" class="small-italic">Показатели</td>
        <td width="13%" class="small-italic">Код строки</td>
        <td class="small-italic">Значения показателей</td>
    </tr>
    <tr style="padding-bottom:20px;">
        <td class="small-italic">1</td>
        <td class="small-italic">2</td>
        <td class="small-italic">3</td>
    </tr>
    <!--210-->
    <tr class="middle spp">
        <td>Сумма начисленных авансовых платежей за отчетный (налоговый) период - всего</td>
        <td class="text-center">210</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['210']) ?></td>
    </tr>
    <tr class="middle">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--220-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет</td>
        <td class="text-center">220</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['220']) ?></td>
    </tr>
    <!--230-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации</td>
        <td class="text-center">230</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['230']) ?></td>
    </tr>
    <!--240-->
    <tr class="middle spp">
        <td>Сумма налога, выплаченная за пределами Российской Федерации и засчитываемая в уплату налога
            согласно порядку, установленному статьей 311 Налогового кодекса Российской Федерации</td>
        <td class="text-center">240</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['240']) ?></td>
    </tr>
    <tr class="middle">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--250-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет</td>
        <td class="text-center">250</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['250']) ?></td>
    </tr>
    <!--260-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации</td>
        <td class="text-center">260</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['260']) ?></td>
    </tr>
    <!--265-->
    <tr class="middle spp">
        <td>Сумма торгового сбора, фактически уплаченная в бюджет субъекта
            Российской Федерации с начала налогового периода</td>
        <td class="text-center">265</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['265']) ?></td>
    </tr>
    <!--266-->
    <tr class="middle spp">
        <td>Сумма торгового сбора, на которую уменьшены авансовые платежи в
            бюджет субъекта Российской Федерации за предыдущий отчетный период</td>
        <td class="text-center">266</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['266']) ?></td>
    </tr>
    <!--267-->
    <tr class="middle spp">
        <td>Сумма торгового сбора, на которую уменьшены исчисленные авансовые платежи (налог) в
            бюджет субъекта Российской Федерации за отчетный (налоговый) период</td>
        <td class="text-center">267</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['267']) ?></td>
    </tr>
    <tr class="middle spp">
        <td>Сумма налога к доплате</td>
        <td></td>
        <td></td>
    </tr>
    <!--270-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет (стр. 190 - стр. 220 - стр. 250)</td>
        <td class="text-center">270</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['270']) ?></td>
    </tr>
    <!--271-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации (стр. 200 - стр. 230 - стр. 260)</td>
        <td class="text-center">271</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['271']) ?></td>
    </tr>
    <tr class="middle spp">
        <td>Сумма налога к уменьшению</td>
        <td></td>
        <td></td>
    </tr>
    <!--280-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет (стр. 220 + стр. 250 - стр. 190)</td>
        <td class="text-center">280</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['280']) ?></td>
    </tr>
    <!--281-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации (стр. 230 + стр. 260 - стр. 200)</td>
        <td class="text-center">281</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['281']) ?></td>
    </tr>
    <!--290-->
    <tr class="middle spp">
        <td>Сумма ежемесячных авансовых платежей, подлежащих уплате в квартале,
            следующем за текущим отчетным периодом</td>
        <td class="text-center">290</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['290']) ?></td>
    </tr>
    <tr class="middle">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--300-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет</td>
        <td class="text-center">300</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['300']) ?></td>
    </tr>
    <!--310-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации</td>
        <td class="text-center">310</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['310']) ?></td>
    </tr>
    <!--320-->
    <tr class="middle spp">
        <td>Сумма ежемесячных авансовых платежей, подлежащих уплате в
            первом квартале следующего налогового периода</td>
        <td class="text-center">320</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['320']) ?></td>
    </tr>
    <tr class="middle">
        <td class="lp">в том числе:</td>
        <td></td>
        <td></td>
    </tr>
    <!--330-->
    <tr class="middle spp">
        <td class="lp">в федеральный бюджет</td>
        <td class="text-center">330</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['330']) ?></td>
    </tr>
    <!--340-->
    <tr class="middle spp">
        <td class="lp">в бюджет субъекта Российской Федерации</td>
        <td class="text-center">340</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['340']) ?></td>
    </tr>
    <!--350-->
    <tr class="middle spp">
        <td>Объем капитальных вложений, осуществленных в целях реализации
            инвестиционного проекта</td>
        <td class="text-center">350</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['350']) ?></td>
    </tr>
    <!--351-->
    <tr class="middle spp">
        <td>Разница между суммой налога, рассчитанной по налоговой ставке 20%,
            и суммой налога, исчисленной с применением пониженных налоговых ставок</td>
        <td class="text-center">351</td>
        <td class="font-main text-right"><?= printOrEmpty($columnData['351']) ?></td>
    </tr>
</table>
