<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Редактировать книгу';
?>

<?= $this->render('form', [
    'model' => $model,
]) ?>
