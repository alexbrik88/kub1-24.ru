<?php
use \common\components\date\DateHelper;

$bankCount = 0;
$bankItems = [];
$bankArray = $company->getCheckingAccountants()
    ->groupBy('bik')
    ->indexBy('bik')
    ->orderBy('type')
    ->all();

if ($bankArray) {
    foreach ($bankArray as $account) {
        $bankItems[] = "№ {$account->rs} в {$account->bank_name}";
    }
    $bankCount = count($bankItems);
}

$start_date = "{$model->tax_year}-01-01";
if ($tax_registration_date = \DateTime::createFromFormat(DateHelper::FORMAT_USER_DATE, $company->gettaxRegistrationDate())) {
    if ($model->tax_year == $tax_registration_date->format('Y')) {
        $start_date = $tax_registration_date->modify("+ 3 days")->format('Y-m-d');
    }
}
?>
<style media="print">
    @page {
        size: auto;
        margin: 0;
    }
    .page-break  {
        page-break-after: always;
    }
</style>

<style>
    td {font-size:8pt;vertical-align: bottom}
    .table {margin:0; padding:0;}
    .table td.border-bottom {border-bottom:1px solid #000;}
    .table td.tip {padding:0 0 3px 0; font-size:7pt; text-align: center}
    .table td.ver-top {vertical-align:top;}
    .table tr.middle td {vertical-align:middle;padding:4px 0;}
    .table td.codes-h {border:2px solid #000; border-bottom:1px solid #000;}
    .table td.codes-m {border-right:2px solid #000; border-left:2px solid #000; border-bottom:1px solid #000;}
    .table td.inner-table {margin:0;padding:0;vertical-align:top;}
    <?php if (@$_GET['red']) { ?>.table td {border:1px solid red} <?php } ?>
</style>

<div class="page-content-in p-center pad-pdf-p page-break">
    <table class="table no-border">
        <tr>
            <td></td>
            <td width="25%" class="font-size-6 text-right">
                Приложение №1<br/>
                к Приказу Министерства Финансов<br/>
                Российской Федерации<br/>
                от 22.10.2012 N 135н
            </td>
        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td class="font-size-8-bold text-center">
                КНИГА <br/>
                УЧЕТА ДОХОДОВ И РАСХОДОВ ОРГАНИЗАЦИЙ И <br/>
                ИНДИВИДУАЛЬНЫХ ПРЕДПРИНИМАТЕЛЕЙ, ПРИМЕНЯЮЩИХ <br/>
                УПРОЩЕННУЮ СИСТЕМУ НАЛОГООБЛОЖЕНИЯ
            </td>
        </tr>
    </table>
    <br/>
    <br/>
    <table class="table no-border">
        <tr>

            <td class="inner-table" style="padding-top:3px;">
                <table class="table no-border">
                    <tr>
                        <td colspan="3" class="text-right"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-right">Форма по ОКУД</td>
                    </tr>
                    <tr>
                        <td width="45%"></td>
                        <td width="20%" class="border-bottom text-center">на <?= $model->tax_year ?> год</td>
                        <td class="text-right">Дата (год, месяц, число)</td>
                    </tr>
                </table>
                <table class="table no-border" style="margin-right:20px">
                    <tr>
                        <td width="40%">Налогоплательщик (наименование</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>организации/фамилия, имя, отчество</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            индивидуального предпринимателя)
                        </td>
                        <td class="border-bottom ver-bottom">
                            <?= $company->getIpFio() ?>
                        </td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td class="text-right">
                            по ОКПО
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Идентификационный номер налогоплательщика-организации/ код
                        </td>
                    </tr>
                    <tr>
                        <td>
                            причины постановки на учет в налоговом органе (ИНН/КПП)
                        </td>
                    </tr>
                </table>
                <table class="table no-border" style="margin:9px 2px">
                    <tr>
                        <?php for ($i=0; $i<22; $i++) : ?>
                            <td style="border: 1px solid #000; line-height:12pt" class="text-center">
                                <?= $i==13 ? '/' : '&nbsp;' ?>
                            </td>
                        <?php endfor; ?>
                        <td style="width:20px"></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td>
                            Идентификационный номер налогоплательщика - индивидуального
                        </td>
                    </tr>
                    <tr>
                        <td>
                            предпринимателя (ИНН)
                        </td>
                    </tr>
                </table>
                <table class="table no-border" style="margin:9px 2px">
                    <tr>
                        <?php for ($i=0; $i<22; $i++) : ?>
                            <td style="<?= ($i<12) ? 'border: 1px solid #000;':'' ?> line-height:12pt" class="text-center">
                                <?= ($company->inn && isset($company->inn[$i])) ? $company->inn[$i] : '&nbsp;' ?>
                            </td>
                        <?php endfor; ?>
                        <td style="width:20px"></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td width="27%">
                            Объект налогообложения
                        </td>
                        <td class="border-bottom ver-bottom">
                            Доходы
                        </td>
                        <td width="20px"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="tip">(наименование выбранного объекта налогообложения</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="border-bottom"> <br/> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="tip">в соответствии со статьей 346.14 Налогового кодекса Российской Федерации)</td>
                        <td></td>
                    </tr>
                </table>
                <table class="table no-border">
                    <tr>
                        <td width="21%">Единица измерения:</td>
                        <td>руб.</td>
                        <td class="text-right">по ОКЕИ</td>
                    </tr>
                </table>
            </td>

            <td width="20%" class="inner-table">
                <table class="table no-border">
                    <tr>
                        <td colspan="3" class="text-center font-bold codes-h">
                            Коды
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m">
                            <?= $company->okud ?: '<br/>' ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center border-bottom" style="border-left:2px solid #000">
                            <?= date('Y', strtotime($start_date)) ?>
                        </td>
                        <td class="text-center border-bottom" style="border-left:1px solid #000;border-right:1px solid #000;">
                            <?= date('m', strtotime($start_date)) ?>
                        </td>
                        <td class="text-center border-bottom" style="border-right:2px solid #000">
                            <?= date('d', strtotime($start_date)) ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="border-bottom:none"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m">
                            <?= $company->okpo ?: '<br/>'; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m" style="line-height: 155pt"> <br/> </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center codes-m"> 383 </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <br/>
    <table class="table no-border">
        <tr>
            <td colspan="2">Адрес места нахождения организации</td>
        </tr>
        <tr>
            <td colspan="2">(места жительства индивидуального</td>
        </tr>
        <tr>
            <td width="16%">предпринимателя)</td>
            <td class="border-bottom"><?= $company->getAddressActualFull() ?></td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom"> <br/> </td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="60%">Номера расчетных и иных счетов, открытых в учреждениях банков</td>
            <td class="border-bottom"></td>
        </tr>
        <tr>
            <td></td>
            <td class="tip">(номера расчетных</td>
        </tr>
        <tr>
            <td colspan="2" class="border-bottom"> <?php if ($bankItems > 0) echo $bankItems[0] ?> </td>
        </tr>
        <tr>
            <td colspan="2" class="tip">и иных счетов и наименование соответствующих банков)</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
        <?php if ($bankItems > 1) foreach ($bankItems as $key=>$item) : if ($key == 0) continue; ?>
            <tr>
                <td colspan="2" class="border-bottom"> <?= $item ?> </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>