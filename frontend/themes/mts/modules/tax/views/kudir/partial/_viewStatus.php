<?php

use frontend\themes\mts\components\Icon;
use common\models\document\status\KudirStatus;
use common\components\date\DateHelper;
use common\models\Agreement;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tax\models\Kudir; */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

$status = $model->status;
$styleClass = $status->getStyleClass();
$iconClass  = $status->getIcon();
$statusIcon = [
    KudirStatus::STATUS_CREATED => 'check-2',
    KudirStatus::STATUS_PRINTED => 'print',
    KudirStatus::STATUS_SEND => 'envelope',
    KudirStatus::STATUS_ACCEPTED => 'check-double',
    KudirStatus::STATUS_SIGNED => 'check-double',
];
$statusCssClass = [
    KudirStatus::STATUS_CREATED => 'paid',
    KudirStatus::STATUS_PRINTED => 'paid',
    KudirStatus::STATUS_SEND => 'paid',
    KudirStatus::STATUS_ACCEPTED => 'paid',
    KudirStatus::STATUS_SIGNED => 'paid',
];
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="pl-2 pr-2 w-100">
        <div class="button-regular <?= $statusCssClass[$status->id] ?> mb-3 pl-3 pr-3 w-100">
            <?= Icon::get($statusIcon[$status->id], ['class' => 'mr-1']) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto mr-1">
                <?= date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at ? : $model->created_at) ?>
            </span>
        </div>
    </div>
</div>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            КУДиР на
        </span>
        <span>
            <?= $model->tax_year ?> год
        </span>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Ответственный:
        </span>
        <span>
            <?= \common\models\employee\Employee::findOne(Yii::$app->user->id)->getShortFio() ?>
        </span>
    </div>
</div>
