<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\InvoiceExpenditureItem;

function my_roman($num) {
    if ($num == 1) return 'I';
    if ($num == 2) return 'II';
    if ($num == 3) return 'III';
    if ($num == 4) return 'IV';

    return $num;
}
?>

<style>
    td {font-size:8pt;vertical-align: bottom}
    .table {margin:0; padding:0;}
    .table td.border-bottom {border-bottom:1px solid #000;}
    .table td.tip {padding:0 0 3px 0; font-size:7pt; text-align: center}
    .table td.ver-top {vertical-align:top;}
    .table tr.middle td {vertical-align:middle;padding:4px 0;}
    .table td.codes-h {border:2px solid #000; border-bottom:1px solid #000;}
    .table td.codes-m {border-right:2px solid #000; border-left:2px solid #000; border-bottom:1px solid #000;}
    .table td.inner-table {margin:0;padding:0;vertical-align:top;}
</style>

<style type="text/css" media="print">
    @page {
        size: auto;
        margin: 0;
    }
</style>

<div class="page-content-in p-center-album pad-pdf-p-album landscape">
    <br/>
    <table class="table no-border">
        <tr>
            <td class="text-center font-size-9">
                IV. Расходы, предусмотренные пунктом 3.1 статьи 346.21 Налогового кодекса Российской Федерации, уменьшающие сумму <br/>
                налога, уплачиваемого в связи с применением упрощенной системы налогообложения (авансовых платежей по налогу)<br/>
                <span style="text-decoration: underline">за <?= $model->tax_year ?> г.</span>
            </td>
        </tr>
        <tr>
            <td class="text-center">отчетный (налоговый) период</td>
        </tr>
    </table>
    <br/>
    <table class="table">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="6" class="text-center">Сумма</td>
            <td></td>
        </tr>
        <tr>
            <td class="ver-top text-center" width="5%">№<br/>п/п</td>
            <td class="ver-top text-center" width="75px">Дата и номер первичного документа</td>
            <td class="ver-top text-center" width="75px">Период, за который произведена уплата страховых взносов, выплата пособия по временной нетрудоспособности, предусмотренных в графах 4-9</td>
            <td class="ver-top text-center" width="75px">Страховые взносы на обязательное пенсионное страхование <br/>(руб.)</td>
            <td class="ver-top text-center" width="75px">Страховые взносы на обязательное социальное страхование на случай временной нетрудоспособности в связи с материнством <br/>(руб.)</td>
            <td class="ver-top text-center" width="75px">Страховые взносы на обязательное медицинское страхование <br/>(руб.)</td>
            <td class="ver-top text-center" width="75px">Страховые взносы на обязательное социальное страхование от несчастных случаев на производстве и профессиональных заболеваний <br/>(руб.)</td>
            <td class="ver-top text-center" width="75px">Расходы по выплате пособия по временной нетрудоспособности <br/>(руб.)</td>
            <td class="ver-top text-center" width="75px">Платежи (взносы) по договорам добровольного личного страхования <br/>(руб.)</td>
            <td class="ver-top text-center">Итого <br/>(руб.)</td>
        </tr>
        <tr>
            <td width="5%" class="text-center">1</td>
            <td width="10%" class="text-center">2</td>
            <td width="10%" class="text-center">3</td>
            <td width="10%" class="text-center">4</td>
            <td width="10%" class="text-center">5</td>
            <td width="10%" class="text-center">6</td>
            <td width="10%" class="text-center">7</td>
            <td width="10%" class="text-center">8</td>
            <td width="10%" class="text-center">9</td>
            <td class="text-center">10</td>
        </tr>
        <?php $npp = 1; ?>
        <?php $period_total = 0; ?>
        <?php $pfr_total = 0; ?>
        <?php $osm_total = 0; ?>
        <?php for ($qnum=1; $qnum<=4; $qnum++) : ?>
            <?php $quarter_total = 0; ?>
            <?php $quarter_pfr = 0; ?>
            <?php $quarter_osm = 0; ?>
            <?php /** @var \common\models\cash\CashBankFlows || \common\models\cash\CashOrderFlows $flows */ ?>
            <?php foreach ($quartersBankFlowsOutcome[$qnum] as $flows) : ?>
                <?php $row_total = 0; ?>
                <?php if ($flows instanceof \common\models\cash\CashBankFlows) {
                        $doc_number = $flows->payment_order_number;
                        $doc_amount = $flows->amountExpense;
                    }  elseif ($flows instanceof \common\models\cash\CashOrderFlows) {
                        $doc_number = $flows->number;
                        $doc_amount = $flows->amount;
                    }  else {
                        continue;
                } ?>
                <tr>
                    <td>
                        <?=$npp?>
                    </td>
                    <td>
                        <?= '№ ' . $doc_number .  ' от ' .
                        DateHelper::format($flows->recognition_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)  ?>
                    </td>
                    <td>
                        <?= DateHelper::format($flows->recognition_date, 'Y', DateHelper::FORMAT_DATE) . ' г.'  ?>
                    </td>
                    <td class="text-right">
                        <?php if ($flows->expenditure_item_id == InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT && is_numeric($doc_amount)):  // ПФР ?>
                            <?= TextHelper::invoiceMoneyFormat($doc_amount, 2) ?>
                            <?php $row_total = $doc_amount ?>
                            <?php $quarter_total += $doc_amount ?>
                            <?php $quarter_pfr += $doc_amount; ?>
                        <?php endif; ?>
                    </td>
                    <td></td>
                    <td class="text-right">
                        <?php if (($flows->expenditure_item_id == InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP || $flows->expenditure_item_id == InvoiceExpenditureItem::ITEM_OMS_FIXED_PAYMENT) && is_numeric($doc_amount)):  // ОМС ?>
                            <?= TextHelper::invoiceMoneyFormat($doc_amount, 2) ?>
                            <?php $row_total = $doc_amount ?>
                            <?php $quarter_total += $doc_amount ?>
                            <?php $quarter_osm += $doc_amount; ?>
                        <?php endif; ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <?= ($row_total > 0) ? TextHelper::invoiceMoneyFormat($row_total, 2) : '' ?>
                    </td>
                </tr>
                <?php $npp++; ?>
            <?php endforeach; ?>
            <tr>
                <td colspan="3">Итого за <?= my_roman($qnum) ?> квартал</td>
                <td class="text-right">
                    <?= ($quarter_pfr > 0) ? TextHelper::invoiceMoneyFormat($quarter_pfr, 2) : '' ?>
                </td>
                <td></td>
                <td class="text-right">
                    <?= ($quarter_osm > 0) ? TextHelper::invoiceMoneyFormat($quarter_osm, 2) : '' ?>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right">
                    <?= ($quarter_total > 0) ? TextHelper::invoiceMoneyFormat($quarter_total, 2) : '' ?>
                </td>
            </tr>

            <?php $period_total += $quarter_total; ?>
            <?php $pfr_total += $quarter_pfr ?>
            <?php $osm_total += $quarter_osm ?>

            <?php if ($qnum > 1): ?>
                <tr>
                    <td colspan="3">
                        <?=($qnum == 2) ? 'Итого за полугодие' : '' ?>
                        <?=($qnum == 3) ? 'Итого за 9 месяцев' : '' ?>
                        <?=($qnum == 4) ? 'Итого за год' : '' ?>
                    </td>
                    <td class="text-right">
                        <?= ($pfr_total > 0) ? TextHelper::invoiceMoneyFormat($pfr_total, 2) : '' ?>
                    </td>
                    <td></td>
                    <td class="text-right">
                        <?= ($osm_total > 0) ? TextHelper::invoiceMoneyFormat($osm_total, 2) : '' ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="text-right">
                        <?= ($period_total > 0) ? TextHelper::invoiceMoneyFormat($period_total, 2) : '' ?>
                    </td>
                </tr>
            <?php endif; ?>

        <?php endfor; ?>

    </table>
</div>