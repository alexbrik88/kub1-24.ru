<?php
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $model frontend\modules\tax\models\Kudir */
?>

<div class="wrap">
    <div class="row justify-content-between">
        <div class="column">
            <?php $label = $model->isNewRecord ? 'Создать' : 'Сохранить' ?>
            <?= Html::submitButton('<span class="ladda-label">'.$label.'</span>', [
                'class' => 'svg-btn button-regular button-width button-hover-grey width-160',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?php $cancelUrl = $model->isNewRecord ? ['index'] : ['view', 'id' => $model->id,] ?>
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-regular button-width button-hover-grey width-160 mr-4',
                'data-style' => 'expand-right',
            ]) ?>
            <?= frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Удалить',
                    'class' => 'button-regular button-width button-hover-grey width-160',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить налоговую декларацию?',
            ]) ?>
        </div>
    </div>
</div>
