<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.03.2019
 * Time: 19:21
 */

use common\models\Company;
use common\models\company\CheckingAccountant;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\components\helpers\ArrayHelper;
use common\models\company\CompanyType;
use yii\bootstrap\ActiveForm;
use common\components\helpers\Html;
use yii\helpers\Url;
use common\widgets\Modal;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Заполните реквизиты';

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');
$view = Company::$companyFormView[CompanyType::TYPE_OOO];

$this->registerJsFile('@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset']);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<?= $this->render('_style'); ?>
<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 1, 'company' => $model]); ?>
        <div class="col-xs-12 col-lg-12 pad0">
            <?php $form = ActiveForm::begin([
                'id' => 'company-update-form',
                'enableClientValidation' => false,
                'enableAjaxValidation' => true,
                'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
                'fieldConfig' => [
                    'template' => "{input}\n{label}\n{error}\n",
                ],
                'options' => [
                    'class' => 'form-md-underline'
                ],
            ]); ?>
            <div class="col-xs-12 pad0 step-by-step-form">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 main-caption bord-light-b pad0">
                            <span>Шаг 1: Заполните данные по вашему <?= $model->companyType->name_short ?></span>
                        </div>
                    </div>
                    <div class="col-xs-12 caption">
                        <span>Реквизиты <?= $model->companyType->name_short ?></span>
                    </div>
                    <?= $this->render("parts_company/{$view}", [
                        'form' => $form,
                        'model' => $model,
                    ]); ?>
                    <div class="col-xs-12 caption">
                        <span>Расчетный счет <span class="show-first-account-number">№1</span></span>
                    </div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <?= $form->field($account, 'bik')->textInput([
                                    'maxlength' => true,
                                    'class' => 'form-control input-sm dictionary-bik' . ($account->bik ? ' edited' : ''),
                                    'data' => [
                                        'url' => Url::to(['/dictionary/bik']),
                                        'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                                        'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                                        'target-ks' => '#' . Html::getInputId($account, 'ks'),
                                        'target-collapse' => '#company-bank-block',
                                    ]
                                ])->label('БИК вашего банка') ?>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <?= $form->field($account, 'rs')->textInput(['maxlength' => true]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-6">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <?= $form->field($account, 'bank_name')->textInput([
                                    'maxlength' => true,
                                    'readonly' => true,
                                ]); ?>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <?= $form->field($account, 'ks')->textInput([
                                    'maxlength' => true,
                                    'readonly' => true,
                                ]); ?>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <?= $form->field($account, 'bank_city')->textInput([
                                    'maxlength' => true,
                                    'readonly' => true,
                                ]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->render('parts_company/_additional_accounts', [
                        'form' => $form,
                        'additionalAccounts' => $additionalAccounts,
                        'newAccounts' => $newAccounts,
                    ]) ?>
                    <div class="clearfix"></div>
                    <div id="buttons-bar-fixed">
                        <div class="col-xs-12 pad0 buttons-block">
                            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                                'data-style' => 'expand-right',
                                'style' => 'width: 130px!important; color: #fff;',
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
            <?php $form->end(); ?>
        </div>
    </div>
</div>
<?= $this->render('//company/form/_company_inn_api'); ?>
<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_what_is_oktmo" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен код ОКТМО? </strong></p>
        <p> ОКТМО — это общероссийский классификатор территорий муниципальных образований. <br/> Коды ОКТМО необходимо указывать в платежных поручениях на уплату налогов и сборов, а так же в налоговых декларациях."</p>
    </span>
    <span id="tooltip_what_is_ifns" style="display: inline-block; text-align: center;">
        <p> <strong> Зачем нужен номер ИФНС? </strong></p>
        <p> Номер ИФНС - это номер Инспекционной Федеральной Налоговой Службы в которой вы регистрировали свое ИП. <br/> Данный номер необходим для заполнения реквизитов налоговой в платежке на уплату налогов.</p>
    </span
</div>
<?php $this->registerJs('
    window.isTaxRobot = true;

    $(document).ready(function() {
       if (!$(\'#company-inn\').val()) {
           $(\'#company-inn\').parents(\'.form-group\').find(\'label\').css({\'color\':\'red\'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       }
    });
    $(\'#company-inn\').on(\'change\', function() {
       if (!$(\'#company-inn\').val()) {
           $(\'#company-inn\').parents(\'.form-group\').find(\'label\').css({\'color\':\'red\'});
           $("#company-inn").pulsate({
               color: "#f00",
               reach: 20,
               repeat: 3
           });
       } else {
           $(\'#company-inn\').parents(\'.form-group\').find(\'label\').css({\'color\':\'#999\'});
       }
    });
    window.ifns_error = 0;
    window.oktmo_error = 0;
    window.show_pulsate = 0;
    $(\'#company-update-form\').on(\'afterValidateAttribute\', function (event, attribute, message) {
        if (attribute.container == \'.field-checkingaccountant-rs\' && message.length
            || attribute.container == \'.field-checkingaccountant-bik\' && message.length) {
            $(\'#not_use_account\').show();
        }
        if (attribute.container == \'.field-company-ifns_ga\') {
            window.ifns_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }
        if (attribute.container == \'.field-company-oktmo\') {
            window.oktmo_error = (message.length) ? 1 : 0;
            window.show_pulsate = 1;
        }
        if (window.ifns_error || window.oktmo_error) {
            if (window.show_pulsate) {
                window.show_pulsate = 0;
                $("#get-fias-ittem").addClass(\'error\').parent().pulsate({
                   color: "#f00",
                   reach: 20,
                   repeat: 3
                });
            }
        } else {
            $("#get-fias-ittem").removeClass(\'error\');
        }
    });
');
