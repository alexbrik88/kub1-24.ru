<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\grid\GridView;
use common\models\employee\Employee;
use common\models\company\CompanyType;
use common\components\TextHelper;
use \frontend\modules\tax\models\DeclarationOsnoHelper;
use frontend\rbac\UserRole;

/* @var $company \common\models\Company */

$this->title = 'Нулевая отчетность в налоговую и ПФР';
$emptyMessage = 'У вас пока не создано ни одной отчетности.';

$canDelete = Yii::$app->user->can(UserRole::ROLE_CHIEF);
$canPrint = Yii::$app->user->can(UserRole::ROLE_CHIEF);
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center mb-3">
        <h4><?= Html::encode($this->title) ?></h4>
        <?= Html::a(Icon::get('add-icon').' <span>Добавить</span>', [
            '/tax/declaration-osno/index'
        ], [
            'class' => 'button-regular button-regular_red button-width ml-auto'
        ]) ?>
    </div>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <h6 class="mt-2">
                Список отчетности: <?= $dataProvider->totalCount ?>
            </h6>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'action' => ['index-list'],
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'find_by', [
                        'type' => 'search',
                        'placeholder' => 'Отчетный год',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'rowOptions'   => function ($data, $key, $index, $grid) {
            return [
                'data' => ['key' => $data['id']],
            ];
        },
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => [
            [

                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => 'text-center pad0',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'text-center pad0-l pad0-r',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('TaxDeclaration[' . $data['id'] . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                    ]);
                },
            ],
            [
                'attribute' => 'period_name_id',
                'label' => 'Отчетный период',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '12%',
                ],
                'contentOptions' => [
                    'class' => 'text-right'
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $monthName = \yii\helpers\ArrayHelper::getValue(\common\components\helpers\Month::$monthFullRU, $data['tax_month']);
                    $period =
                        ($data['szvm']) ? ($monthName.' '.$data['tax_year'].' г.') :
                            ($data['balance'] ? ($data['tax_year'].' г.') :
                                ($data['tax_quarter'].'-й квартал '.$data['tax_year'].' г.'));

                    return $period;
                },
                'filter' => $searchModel->getPeriodFilterNames()
            ],
            [
                'attribute' => 'declaration_name_id',
                'label' => 'Название отчета',
                'headerOptions' => [
                    'class' => 'dropdown-filter',
                    'width' => '20%',
                ],
                'value' => function ($data) use ($company) {
                    $name = ($data['nds'] ? 'Декларация на добавленную стоимость' :
                              ($data['org'] ? 'Декларация на прибыль организации' :
                                ($data['balance'] ? 'Бухгалтерский баланс' :
                                  ($data['szvm'] ? 'Сведения о застрахованных лицах' :
                                    'Единая упрощенная декларация'))));

                    return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data]) ?
                        Html::a($name, ['/tax/declaration/view', 'id' => $data['id']]) :
                        $name;
                },
                'format' => 'raw',
                'filter' => [
                    0 => 'Все',
                    DeclarationOsnoHelper::TYPE_SINGLE => 'Единая упрощенная декларация',
                    DeclarationOsnoHelper::TYPE_NDS => 'Декларация на добавленную стоимость',
                    DeclarationOsnoHelper::TYPE_ORG => 'Декларация на прибыль организации',
                    DeclarationOsnoHelper::TYPE_BALANCE => 'Бухгалтерский баланс',
                    DeclarationOsnoHelper::TYPE_SZVM => 'Сведения о застрахованных лицах'
                ]
            ],
            [
                'attribute' => 'document_correction_number',
                'label' => '№ корректировки',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '8%',
                ],
            ],
            [
                'attribute' => 'created_at',
                'label' => 'Дата создания',
                'headerOptions' => ['style' => 'width:10%;'],
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    return \common\models\document\status\TaxDeclarationStatus::findOne(['id'=>$data['status_id']])->name;
                },
                'format' => 'raw',
                'filter' => [
                    '' => 'Все',
                    1 => 'Создана',
                    3 => 'Принята',
                    4 => 'Передана',
                    5 => 'Скорректирована'
                ]
            ],
            [
                'attribute' => 'document_author_id',
                'label' => 'Ответственный',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'text-left text-ellipsis',
                ],
                'value' => function ($data) {
                    $employee = \common\models\EmployeeCompany::findOne([
                        'employee_id' => $data['document_author_id'],
                        'company_id' => $data['company_id']
                    ]);

                    return (!empty($employee)) ? $employee->getShortFio() : '';
                },
                'format' => 'raw',
                'filter' => $searchModel->getCreators()
            ],
        ],
    ]); ?>
</div>
