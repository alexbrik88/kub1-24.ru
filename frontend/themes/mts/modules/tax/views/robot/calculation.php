<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\TaxRobotHelper;
use common\components\widgets\BikTypeahead;
use common\components\widgets\IfnsTypeahead;
use common\models\address\AddressDictionary;
use common\models\cash\CashFlowsBase;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\document\InvoiceExpenditureItem;
use frontend\modules\tax\models\TaxrobotCashBankSearch;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use php_rutils\RUtils;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $company Company */

$this->title = 'Расчет налога за ' . str_replace("&nbsp;", " ", $taxRobot->period->label);
$this->params['step'] = 4;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$accountArray = $company->getCheckingAccountants()->orderBy([
    'type' => SORT_ASC,
    'rs' => SORT_ASC,
])->all();
$account = end($accountArray);
$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();
$contractorList = $taxRobot->getContractorList($taxRobot->getExpenseSearchQuery());
$taxItems = ArrayHelper::map($taxRobot->getTaxItems(), 'id', 'name');

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub',],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$currentPeriod = $taxRobot->period->label;
$quarter = $taxRobot->getQuarter();

$flowTypeItems = [CashFlowsBase::FLOW_TYPE_EXPENSE => CashFlowsBase::getFlowTypes()[CashFlowsBase::FLOW_TYPE_EXPENSE]];

$taxableTotalAmount = $taxRobot->getTaxableTotalAmount(); // налогооблагаемый приход
$sumPFR = $taxRobot->getUsn6ReductionByPFR();
$sumOMS = $taxRobot->getUsn6ReductionByOMS();
$sumOver300 = $taxRobot->getUsn6ReductionByOver300();
$taxTotalAmount = $taxRobot->getUsn6TotalAmount(); // начислено
$taxPayableAmount = $taxRobot->getUsn6needPayAmount($quarter); // к уплате
$taxAdvanceAmount = $taxRobot->getUsn6PreviousTotalAmount($quarter); // налоговый аванс

$over300taxable = $taxRobot->getOver300ThisTaxableAmount();
$over300unlimit = $taxRobot->getOver300ThisUnlimitAmount();
$over300limit = $taxRobot->getOver300ThisLimit();
$over300amount = $taxRobot->getOver300ThisAmount();
$over300paid = $taxRobot->getOver300ThisPaidAmount();
$over300remaining = $taxRobot->getOver300ThisRemainingAmount();

function moneyFormat($val) {
    return number_format($val / 100, 2, '.', ' ');
}

$usn6PaidArray = $taxRobot->getUsn6Paid();
$usn6paidTotal = 0;
$usn6paidData = [];
foreach (range(1, 4) as $q) {
    if ($usn6PaidArray[$q] > 0 || $q < $quarter) {
        $usn6paidTotal += $usn6PaidArray[$q];
        $p = "{$taxRobot->getYear()}_{$q}";
        $usn6paidData[] = [
            'periodLabel' => $taxRobot->getPeriodArray()[$p]['label'],
            'sum' => $usn6PaidArray[$q],
        ];
    }
}
?>

<div class="tax-robot-calculation">

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-calculation-pjax',
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">
                    <span>Расчет налога за</span>
                    <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
                        <?= Html::a(Html::tag('span', $taxRobot->period->label) . Icon::get('shevron'), '#', [
                            'id' > 'cardProductTitle',
                            'class' => 'link link_title',
                            'role' => 'button',
                            'data-toggle' => 'dropdown',
                        ])?>
                        <div class="dropdown-menu keep-open" aria-labelledby="cardProductTitle">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <?php foreach ($taxRobot->getPeriodDropdownItems('calculation') as $key => $item) : ?>
                                        <li style="white-space: nowrap;">
                                            <?= Html::a($item['label'], $item['url']) ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </h4>
            </div>
        </div>
    </div>
</div>
<div class="wrap p-4 mb-2">
    <div class="p-2">
        <div class="row">
            <div class="col column-61">
                <h4 class="text_size_20 mb-4">
                    <span class="mr-1">Уменьшение налога УСН</span>
                    <svg class="svg-icon link" data-toggle="toggleVisible" data-target="invoice" style="cursor: pointer; margin-left:10px; margin-top:-3px; width:20px; height: 20px;" viewBox="0 0 16 15">
                        <path d="M9.25 13.17H16v1.2H9.25a.636.636 0 0 0-.625.63h-1.25a.636.636 0 0 0-.625-.63H0v-1.2h6.75c.465 0 .904.168 1.25.473a1.88 1.88 0 0 1 1.25-.472zM16 0v11.804H9.25a.63.63 0 0 0-.625.635h-1.25a.63.63 0 0 0-.625-.635H0V0h6.75c.48 0 .918.184 1.25.486C8.332.184 8.77 0 9.25 0H16zM7.273 10.61V1.742a.638.638 0 0 0-.631-.644H1.09v9.401h5.55c.222 0 .434.04.632.11zm7.636-9.512H9.36a.638.638 0 0 0-.632.643v8.869a1.84 1.84 0 0 1 .631-.111h5.551V1.098z"></path>
                    </svg>
                </h4>
                <div class="mb-4">Проверьте все ли платежи, на которые можно уменьшить налог попали в расчёт. <br> Ниже, вы можете добавить неучётнный платежи, чтобы уменьшить сумму налога.</div>
                <div class="accordion" id="accordion">
                    <button class="tax-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center justify-content-between mb-2" type="button" data-toggle="collapse" data-target="#collapseItem-1" aria-expanded="true" aria-controls="collapseItem-1">
                        <span class="title-small mr-3 d-block">Страховые взносы за себя</span>
                        <strong><?= moneyFormat($sumOMS + $sumPFR + $sumOver300) ?> ₽</strong>
                    </button>
                    <div id="collapseItem-1" class="collapse show" data-parent="#accordion">
                        <div class="tax-collapse pt-2 pb-1">
                            <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                <div class="mr-3">1% от дохода свыше 300 000 ₽</div>
                                <div><?= moneyFormat($sumOver300) ?> ₽</div>
                            </div>
                            <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                <div class="mr-3">Пенсионное страхование</div>
                                <div><?= moneyFormat($sumPFR) ?> ₽</div>
                            </div>
                            <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                <div class="mr-3">Медицинское страхование</div>
                                <div><?= moneyFormat($sumOMS) ?> ₽</div>
                            </div>
                        </div>
                    </div>
                    <button class="tax-collapse-btn button-clr w-100 d-flex flex-nowrap align-items-center justify-content-between mb-2 collapsed" type="button" data-toggle="collapse" data-target="#collapseItem-2" aria-expanded="true" aria-controls="collapseItem-2">
                        <div class="title-small mr-3 d-block">Платежи по налогу УСН Доходы</div>
                        <strong><?= moneyFormat($usn6paidTotal) ?> ₽</strong>
                    </button>
                    <?php if ($quarter > 1) : ?>
                        <div id="collapseItem-2" class="collapse" data-parent="#accordion">
                            <div class="tax-collapse pt-2 pb-1">
                                <?php foreach ($usn6paidData as $data) : ?>
                                    <div class="d-flex flex-nowrap align-items-center justify-content-between mb-2">
                                        <div class="mr-3">Налог УСН за <?= $data['periodLabel'] ?></div>
                                        <div><?= moneyFormat($data['sum']) ?> ₽</div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div id="tax-calculation-wrap" class="col column-40 pl-4">
                <div class="card" style="padding: 5px 10px; border-radius: 4px; border: solid 2px #e2e5eb;">
                    <strong class="small-title mb-2 d-block">
                        <div class="text_size_16 mb-2">Сумма налога УСН к уплате:</div>
                        <div class="text_size_20"><?= moneyFormat($taxPayableAmount) ?> ₽</div>
                    </strong>
                    <?= Html::a('Расчет суммы налога УСН'.Icon::get('shevron', [
                        'class' => 'link-shevron ml-2',
                    ]), '#calc-sum-tax', [
                        'class' => 'link link_collapse mb-2',
                        'data-toggle' => 'collapse',
                        'aria-expanded' => true,
                    ]) ?>
                    <div id="calc-sum-tax" class="collapse show" data-parent="#tax-calculation-wrap">
                        <div class="mb-2">Доход за <?=$currentPeriod?>: <?= moneyFormat($taxableTotalAmount) ?> ₽</div>
                        <div class="mb-2">
                            <div class="mb-1">Расчёт налога:</div>
                            <strong>
                                <?= moneyFormat($taxableTotalAmount) ?> *
                                <?=$company->companyTaxationType->usn_percent ?>% =
                                <?= moneyFormat($taxTotalAmount) ?> ₽
                            </strong>
                        </div>
                        <div class="mb-2">
                            <div class="mb-1">Из налога вычитаем уплаченные страховые взносы:</div>
                            <strong>- <?= moneyFormat($taxRobot->getUsn6ReductionAmount()) ?> ₽</strong>
                        </div>
                        <?php if ($quarter > 1) : ?>
                            <?php
                            $q = $quarter-1;
                            $p = "{$taxRobot->getYear()}_{$q}";
                            ?>
                            <div class="mb-2">
                                <div class="mb-1">
                                    Вычитаем налог УСН, который уплачен за
                                    <?= $taxRobot->getPeriodArray()[$p]['label'] ?>:
                                </div>
                                <strong>- <?= moneyFormat($usn6paidTotal) ?> ₽</strong>
                            </div>
                        <?php endif ?>
                        <div class="mb-2">
                            <div class="mb-1">Сумма налога, который осталось заплатить:</div>
                            <strong><?= moneyFormat(max(0, $taxPayableAmount)) ?> ₽</strong>
                            <?php if ($taxPayableAmount < 0) : ?>
                                <div>
                                    Налог не может быть отрицательным
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="card mt-3" style="padding: 5px 10px; border-radius: 4px; border: solid 2px #e2e5eb;">
                    <strong class="small-title d-block mb-2">
                        <div class="text_size_16 mb-2">1% с дохода свыше 300 000 ₽ к уплате:</div>
                        <div class="text_size_20"><?= moneyFormat($taxRobot->getOver300ThisRemainingAmount()) ?> ₽</div>
                    </strong>
                    <?= Html::a('Расчет суммы взносов'.Icon::get('shevron', [
                        'class' => 'link-shevron ml-2',
                    ]), '#calc-over-sum-tax', [
                        'class' => 'link link_collapse mb-2 collapsed',
                        'data-toggle' => 'collapse',
                        'aria-expanded' => false,
                    ]) ?>
                    <div id="calc-over-sum-tax" class="collapse" data-parent="#tax-calculation-wrap">
                        <div class="mb-2">
                            <div class="mb-1">Доход за <?=$taxRobot->getYear()?> год:</div>
                            <strong><?= moneyFormat($over300taxable) ?> ₽</strong>
                        </div>
                        <div class="mb-2">
                            <div class="mb-1">Расчет страхового взноса:</div>
                            <strong>
                                (<?= moneyFormat($over300taxable) ?> -
                                <?= moneyFormat(TaxRobotHelper::$onePercentOver) ?>) * 1% =
                                <?= moneyFormat($over300unlimit) ?> ₽
                            </strong>
                        </div>
                        <div class="mb-2">
                            <div class="mb-1">
                                Из полученной суммы вычитаем уплаченные страховые взносы с дохода свыше
                                <?= moneyFormat(TaxRobotHelper::$onePercentOver) ?> ₽
                                за предыдущие кварталы этого года:
                            </div>
                            <strong>− <?= moneyFormat(min($over300paid, $over300limit)) ?> ₽</strong>
                        </div>
                        <div class="mb-1">
                            <div class="mb-1">Сумма страхового взноса, которую осталось заплатить:</div>
                            <strong><?= moneyFormat($over300remaining) ?> ₽</strong>
                            <?php if ($over300amount < $over300unlimit) : ?>
                                <div>
                                    (Лимит страховых взносов на ОПС в 2019 году - <?= moneyFormat($over300limit) ?> руб.)
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pt-1 pb-1 mb-2 d-flex flex-wrap">
    <div class="text_size_20" style="align-self: center;">Расходы, уменьшающие налог на УСН</div>
    <div class="dropdown form-filter ml-auto">
        <button class="button-clr button-regular button-regular_red pl-3 pr-3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= Icon::get('add-icon', ['class' => 'mr-2']) ?>
            <span class="ml-1">Добавить расход, уменьшающий налог</span>
        </button>
        <div class="form-filter-drop dropdown-menu">
            <ul class="form-filter-list list-clr">
                <li>
                    <?= Html::a('Добавить по банку', [
                        '/cash/bank/create',
                        'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                        'redirect' => Url::to(['/tax/robot/calculation', 'period' => $taxRobot->getUrlPeriodId()]),
                        'skipDate' => 1,
                        'canAddContractor' => 1,
                        'onlyFNS' => 1
                    ], [
                        'class' => 'update-movement-link',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>
                </li>
                <li>
                    <?= Html::a('Добавить по кассе', [
                        '/cash/order/create',
                        'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                        'redirect' => Url::to(['/tax/robot/calculation', 'period' => $taxRobot->getUrlPeriodId()]),
                        'skipDate' => 1,
                        'id' => $cashbox->id,
                        'canAddContractor' => 1,
                        'onlyFNS' => 1
                    ], [
                        'class' => 'update-movement-link',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="wrap wrap_padding_none">
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $taxRobot,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'rowOptions' => function ($data, $key, $index, $grid) {
            return [
                'class' => 'flow-row',
                'data' => [
                    'update-url' => Url::to(['update-movement', 'type' => $data['payment_type'], 'id' => $data['id']]),
                ]
            ];
        },
        'layout' => '{items}',
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'no-update-modal',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('flowId[]', false, [
                        'class' => 'joint-operation-checkbox',
                        'value' => $data['payment_type'] . '_' . $data['id'],
                        'data' => [
                            'income' => 0,
                            'expense' => bcdiv($data['amount'], 100, 2),
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amount',
                'label' => 'Расход',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'value' => function ($data) {
                    return moneyFormat($data['amount']);
                },
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '20%',
                    'class' => 'nowrap-normal max10list',
                ],
                'format' => 'raw',
                'filter' => ['' => 'Все'] + $contractorList,
                'hideSearch' => false,
                's2width' => '300px',
                'value' => function ($data) use ($contractorList) {
                    return ArrayHelper::getValue($contractorList, $data['contractor_id'], $data['contractor_id']);
                }
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '30%',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data['description']) {
                        $description = mb_substr($data['description'], 0, 50) . '<br>';
                        $description .= mb_substr($data['description'], 50, 50);

                        return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                    }

                    return '';
                },
            ],
            [
                'attribute' => 'expenditure_item_id',
                'label' => 'Статья',
                'headerOptions' => [
                    'width' => '20%',
                ],
                'filter' => ['' => 'Все статьи'] + $taxItems,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function ($data) use ($taxItems) {
                    return ArrayHelper::getValue($taxItems, $data['expenditure_item_id'], '-');
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'cash/bank',
                'template' => '{update-movement} {delete}',
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'no-update-modal nowrap',
                ],
                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                'buttons' => [
                    'update-movement' => function ($url, $model) {
                        $options = [
                            'title' => 'Изменить',
                            'class' => 'update-movement-link link mr-2',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#update-movement-modal',
                            ],
                        ];

                        return Html::a(Icon::get('pencil'), $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        return \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => Icon::get('garbage'),
                                'class' => 'link',
                                'type' => 'link',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => ['flowId' => $model['payment_type'] . '_' . $model['id']],
                            'message' => 'Вы уверены, что хотите удалить операцию?',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    return Url::to(['/tax/robot/' . $action, 'type' => $model['payment_type'], 'id' => $model['id']]);
                }
            ],
        ],
    ]); ?>
</div>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('/layouts/grid/pager', ['totalCount' => $dataProvider->totalCount]),
    'columns' => ['id'],
]); ?>

<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', [
                'bank',
                'period' => $taxRobot->getUrlPeriodId(),
            ], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        </div>
        <div class="column">
            <?= Html::a('Далее', [
                'payment',
                'period' => $taxRobot->getUrlPeriodId(),
            ], [
                'id' => 'submit',
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
    </div>
</div>

<?= \frontend\modules\cash\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canDelete ? Html::a(Icon::get('garbage') . ' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php if ($canDelete) : ?>
    <?= $this->render('part_calculation/modal-many-delete'); ?>
<?php endif ?>

<?= $this->render('part_calculation/modal-usn-payment-rules', [
    'sumPFR' => $sumPFR,
    'sumOMS' => $sumOMS,
    'sumOver300' => $sumOver300,
    'year' => $taxRobot->getYear(),
]); ?>

<?php \yii\widgets\Pjax::end(); ?>

<!-- ADD/UPDATE FLOW -->
<?php Modal::begin([
    'id' => 'update-movement-modal',
    'title' => '',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'update-movement-pjax',
        'enablePushState' => false,
        'linkSelector' => '.update-movement-link',
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

<?php Modal::end(); ?>

<!-- ADD NEW -->
<?php Modal::begin([
    'id' => 'add-new',
    'bodyOptions' => [
        'id' => 'block-modal-new-product-form',
    ],
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

<?php Modal::end(); ?>
</div>

<?php $this->registerJs(<<<JS
$.pjax.defaults.timeout = 10000;
var updateMoneyMovement = function(url) {
    $('#update-movement-modal').modal('show');
    $.pjax({url: url, push: false, container: '#update-movement-pjax'});
}
$(document).on("click", ".update-movement-link", function(e) {
    e.preventDefault();
    updateMoneyMovement(this.href);
});
$(document).on("click", "tr.flow-row > td:not(.no-update-modal)", function(e) {
    var row = $(this).closest('tr.flow-row');
    updateMoneyMovement(row.data("update-url"));
});
$(document).on('hidden.bs.modal', '#update-movement-modal', function (e) {
    $('.modal-header h1', this).html('');
    $('#update-movement-pjax', this).html('');
})

$(document).on("show.bs.modal", "#update-movement-modal", function(event) {
    $(".alert-success").remove();
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});

$(document).on('click', '.my-dropdown', function() {
    var inner = $(this).find('.my-dropdown-body');
    if ($(inner).is(':hidden'))
        $(inner).show(250);
    else
        $(inner).hide(250);
});
JS
)?>

<?php $this->registerJs('
$(document).ready(function(){
    $(document).on("click", ".odds-panel-trigger", function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $(document).on("click", ".odds-panel .side-panel-close", function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
    });
});
'); ?>