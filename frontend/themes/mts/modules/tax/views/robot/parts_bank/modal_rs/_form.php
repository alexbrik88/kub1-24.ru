<?php

use common\models\company\CheckingAccountant;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap4\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $company Company|null */

?>

<?php $form = ActiveForm::begin([
    'action' => $checkingAccountant->isNewRecord ? [
        'create-checking-accountant',
        'companyId' => $company ? $company->id : null,
    ] : [
        'update-checking-accountant',
        'id' => $checkingAccountant->id,
        'companyId' => $company ? $company->id : null,
    ],
    'options' => [
        'class' => 'form-horizontal form-checking-accountant',
        'id' => 'form-checking-accountant-' . $checkingAccountant->id,
        'is_new_record' => $checkingAccountant->isNewRecord ? 1 : 0,
    ],
    'enableClientValidation' => true,
    'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
]); ?>
    <div class="row">
        <div class="col-6">
            <?= $form->field($checkingAccountant, 'bik')->widget(BikTypeahead::classname(), [
                'remoteUrl' => Url::to(['/dictionary/bik']),
                'related' => [
                    '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
                    '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
                    '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
                ],
            ])->textInput(); ?>
        </div>
        <div class="col-6">
            <?= $form->field($checkingAccountant, 'bank_name')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($checkingAccountant, 'bank_city')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
        <div class="col-6">
            <?= $form->field($checkingAccountant, 'ks')->textInput([
                'disabled' => '',
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <?= $form->field($checkingAccountant, 'rs')->textInput([
                'maxlength' => true,
            ]); ?>
        </div>
    </div>

    <?php if ($checkingAccountant->type !== CheckingAccountant::TYPE_MAIN) : ?>
        <?= $form->field($checkingAccountant, 'isMain')->checkbox([], false); ?>

        <?php if (!$checkingAccountant->isNewRecord && count(Yii::$app->user->identity->company->checkingAccountants) !== 1) : ?>
            <?= $form->field($checkingAccountant, 'isClosed')->checkbox([], false) ?>
        <?php endif ?>
    <?php endif; ?>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::submitButton('Сохранить', [
            'class' => 'button-regular width-130 button-regular_red button-clr',
            'style' => 'width: 130px!important;',
        ]); ?>
        <?= Html::button('Отменить', [
            'class' => 'button-clr width-130 button-regular button-hover-transparent',
            'style' => 'width: 130px!important;',
            'data-dismiss' => 'modal',
            'onclick' => new \yii\web\JsExpression('$(this).closest(".modal").modal("hide")'),
        ]); ?>
    </div>

<?php $form->end(); ?>
