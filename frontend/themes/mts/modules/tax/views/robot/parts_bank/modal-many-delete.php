<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
?>

<?php Modal::begin([
    'id' => 'many-delete',
    'closeButton' => false,
]) ?>

    <?= Html::button($this->render('//svg-sprite', ['ico' => 'close']), [
        'class' => 'modal-close close',
        'data-dismiss' => 'modal',
        'aria-label' => 'Close',
    ]) ?>

    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные элементы?
    </h4>
    <div class="text-center">
        <?= Html::button('ДА', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
            'data-url' => Url::to(['delete']),
        ]); ?>
        <?= Html::button('НЕТ', [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>

<?php Modal::end() ?>
