<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\themes\mts\components\Icon;
use yii\widgets\ActiveForm;
use yii\bootstrap4\Html;
use frontend\modules\cash\modules\banking\components\Banking;

$company = $taxRobot->company;
$tariffItems = [];
$tariffArray = SubscribeTariff::find()->actual()->andWhere([
    'tariff_group_id' => SubscribeTariffGroup::IP_USN_6,
])->orderBy(['price' => SORT_ASC])->all();
foreach ($tariffArray as $key => $tariff) {
    $tariffItems[] = [
        'label' => $tariff->getTariffName(),
        'url' => '#',
        'linkOptions' => [
            'class' => 'choose-tariff',
            'data' => [
                'tariff-id' => $tariff->id,
            ],
        ],
    ];
}
$baseTariff = reset($tariffArray);
$model = new PaymentForm($company, ['tariffId' => $baseTariff->id]);

$bankName = ($account && $account->bank) ? $account->bank->name : null;
$bankUrl = ($account && $account->sysBank) ? $account->sysBank->url : null;

$clientBankHtml = 'клиент-банк';
if ($bankUrl) {
    $clientBankHtml = Html::a('клиент-банк', $bankUrl, ['target' => '_blank']);
}
?>

<style>
    .taxrobot-pay-panel {
        position: fixed;
        top: 0;
        right: 0;
        display: none;
        -moz-border-radius-topright: 20px;
        -webkit-border-top-right-radius: 20px;
        -moz-border-radius-bottomright: 20px;
        -webkit-border-bottom-right-radius: 20px;
        width: 400px;
        height: 100%;
        filter: alpha(opacity=85);
        background-color: #ffffff;
    }
    .taxrobot-pay-panel {
        z-index: 10051;
    }
    .taxrobot-pay-panel {
        border-left: 1px solid #e4e4e4;
    }
    .taxrobot-pay-panel .main-block {
        padding: 20px 30px 10px 30px;
        position: absolute;
        width: 100%;
        height: 95%;
        overflow-y: scroll;
    }
    .taxrobot-pay-panel .header {
        text-align: left;
        font-size: 21px;
        font-weight: 700;
    }
    .taxrobot-pay-panel .side-panel-close {
        position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        opacity: 1;
    }
    .tariff-price > del {
        font-size:24px;
        color: red;
    }
</style>

<div class="page-shading-panel taxrobot-pay-panel-close hidden"></div>
<div class="taxrobot-pay-panel">
    <div class="main-block" style="height: 100%;">
        <div style="text-align: left; font-size: 21px; font-weight: 700;">
            <button type="button" class="close side-panel-close taxrobot-pay-panel-close" aria-hidden="true">&times;</button>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 60px; vertical-align: middle; text-align: center;">
                        <?= Html::tag('div', Icon::get('mix', ['style' => 'font-size: 36px;']), [
                            'style' => '
                                display: inline-flex;
                                align-items: center;
                                justify-content: center;
                                width: 60px;
                                height: 60px;
                                background-color: #e30611;
                                color: #fff;
                                border-radius: 30px;
                            ',
                        ]) ?>
                    </td>
                    <td style="padding-left: 10px; font-size: 18px;">
                        Бухгалтерия ИП на УСН 6%
                    </td>
                </tr>
            </table>
        </div>

        <div class="part-pay <?=($taxSum > 0) ? '':'hidden' ?>">
            <div style="padding-top: 30px;">
                <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                    'class' => 'tariff-group-payment-form',
                ]) ?>
                    <?= Html::activeHiddenInput($model, 'tariffId', [
                        'class' => 'tariff-id-input',
                    ]) ?>
                    <div class="text-bold">
                        <div style="display: inline-block; font-size: 18px;">Оплата за </div>
                        <div style="display: inline-block; width: 120px; font-size: 18px;">
                            <div class="dropdown">
                                <?= Html::tag('div', $tariffItems[0]['label'], [
                                    'class' => 'dropdown-toggle link',
                                    'data-toggle' => 'dropdown',
                                    'style' => 'display: inline-block; border-bottom: 1px dashed #000; cursor: pointer;',
                                ])?>
                                <?= \yii\bootstrap4\Dropdown::widget([
                                    'id' => 'tax-robot-tariff-dropdown',
                                    'encodeLabels' => false,
                                    'items' => $tariffItems,
                                ])?>
                            </div>
                        </div>
                        <?php foreach ($tariffArray as $tariff) : ?>
                            <?php
                            $discount = $company->getMaxDiscount($tariff->id);
                            $discVal = $discount ? $discount->value : 0;
                            $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                            ?>
                            <?= Html::beginTag('div', [
                                'class' => "tariff-price tariff-price-{$tariff->id}" .
                                   ($baseTariff->id != $tariff->id ? ' hidden' : ''),
                            ]) ?>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                            <?= Html::endTag('div') ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="mar-t-20">
                        <p style="font-weight:bold;font-size:18px">Вы получите:</p>
                        <p>Автоматический расчёт налогов.</p>
                        <p style="font-weight:bold">Уменьшение налога в соответствии с законом.</p>
                        <p>Подготовка налоговых платёжек.</p>
                        <p>Автоматическая подготовка налоговой декларации.</p>
                        <p>Вся отчётность в налоговую, кроме отчётов за сотрудников.</p>
                        <p>Автоматическая подготовка КУДиР.</p>
                        <p style="font-weight:bold;">Сервис "Выставление счетов" не входит в стоимость тарифа "Бухгалтерия ИП на УСН 6%".</p>
                    </div>

                    <div id="js-form-alert" class="mar-t-20"></div>

                    <div class="row mar-t-20">
                        <div class="col-6">
                            <?= Html::submitButton('Картой', [
                                'class' => 'button-regular button-regular_red submit',
                                'name' => Html::getInputName($model, 'paymentTypeId'),
                                'value' => PaymentType::TYPE_ONLINE,
                                'style' => 'width: 100%;',
                                'data-style' => 'zoom-in',
                            ]); ?>
                        </div>
                        <div class="col-6">
                            <?= Html::submitButton('Выставить счет', [
                                'class' => 'button-regular button-regular_red submit',
                                'name' => Html::getInputName($model, 'paymentTypeId'),
                                'value' => PaymentType::TYPE_INVOICE,
                                'style' => 'width: 100%;',
                                'data-style' => 'zoom-in',
                            ]); ?>
                        </div>
                    </div>
                    <div class="form-submit-result hidden"></div>
                <?= Html::endForm() ?>
            </div>
        </div>
        <div class="part-help <?=($taxSum <= 0) ? '':'hidden' ?>">
            <div class="text-bold mar-t-20">
                <div style="display: inline-block; font-size: 16px;">
                    Нет данных для расчета налога<br/>
                    по вашему <?= $company->getShortName() ?>
                </div>
            </div>
            <div class="mar-t-20">

                <?php if ($hasBankIntegration): ?>
                    <p class="text-bold">Для расчета налога необходимо:</p>
                    <p>
                        <?php foreach ($bankData as $data) : ?>
                            <?php if ($data['active']) : ?>
                                <div class="row pad-t-5 pad-b-5">
                                    <div class="col-xs-12 ">
                                        <a href="/cash/banking/<?=Banking::aliasByBik($account->bik)?>/default/index?account_id=<?=$account->id?>&p=<?=Banking::routeEncode($bankingRoute)?>" data-pjax="0" class="modal-bank-load-statement">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="width: 50px; vertical-align: middle;">
                                                        <?= $data['logo'] ?>
                                                    </td>
                                                    <td style="vertical-align: middle;">
                                                        <div>
                                                            <?= $data['bank_name'] ?>
                                                        </div>
                                                        <div style="font-size: 10px;">
                                                            <?= $data['flow_name'] ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </a>
                                    </div>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </p>
                    <p>
                        1. Загрузить
                        <a href="/cash/banking/<?=Banking::aliasByBik($account->bik)?>/default/index?account_id=<?=$account->id?>&p=<?=Banking::routeEncode($bankingRoute)?>" data-pjax="0" class="modal-bank-load-statement">
                            выписку из банка
                        </a>
                        или загрузить вручную
                        <a class="modal-bank-load-statement">
                            приходы по банку
                        </a>
                    </p>
                <?php else: ?>
                    <p class="text-bold">Для расчета налога необходимо:</p>
                    <p>
                        <?php foreach ($bankData as $data) : ?>
                            <?php if ($data['active']) : ?>
                                <div class="pad-t-5 pad-b-5">
                                    <a class="modal-bank-load-statement">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td style="width: 50px; vertical-align: middle;">
                                                    <?= $data['logo'] ?>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <div>
                                                        <?= $data['bank_name'] ?>
                                                    </div>
                                                    <div style="font-size: 10px;">
                                                        <?= $data['flow_name'] ?>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </a>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>
                    </p>
                    <p>
                        1. Загрузить
                        <a class="modal-bank-load-statement">
                            выписку из банка
                        </a>
                        или загрузить вручную
                        <a class="modal-bank-add-statement">
                            приходы по банку
                        </a>
                    </p>
                <?php endif; ?>

                <?php if ($hasOfdIntegration): ?>
                    <p>
                        2. Загрузить выписку
                        <a class="modal-ofd-load-statement">
                            по онлайн кассе
                        </a>
                        или загрузить вручную
                        <a class="modal-order-load-statement">
                            приходы по кассе
                        </a>.
                    </p>
                <?php else: ?>
                    <p>
                        2. Загрузить вручную
                        <a class="modal-order-load-statement">
                            приходы по кассе
                        </a>.
                    </p>
                <?php endif; ?>

                <p style="font-weight:bold">Далее программа:</p>
                <p>1) Рассчитает налог по вашему ИП</p>
                <p>2) Уменьшит налог на суммы разрешенные законом</p>
                <p>3) Подготовит платежку по налогу</p>
                <p>4) Подготовит налоговую декларацию</p>
                <p style="font-weight:bold">Если у вашего ИП не было дохода, то вам нужна нулевая налоговая декларация</p>
                <p style="font-weight: bold">
                    Скачать
                    <?= Html::a('НУЛЕВАЯ налоговая декларация', ['declaration',
                        'empty' => 1,
                        'period' => ($taxRobot->period) ? $taxRobot->period->id : ''
                    ], [
                        'style' => 'min-width: 180px; margin-top:0',
                        'data' => [
                            'pjax' => '0',
                        ],
                    ]) ?>
                </p>

            </div>
        </div>
        <div class="part-declaration">
            <div class="text-bold mar-t-20">
                <div style="display: inline-block; font-size: 18px;">Нет данных для налоговой декларации.</div>
            </div>
            <div class="mar-t-20">
                Нужен только шаблон налоговой декларации?<br/>
                <a href="/img/xls/Deklaratsiya_ip_usn.xlsx" download="Deklaratsiya_USN_6.xlsx">Скачать бланк декларации ИП в формате Excel</a>
            </div>
        </div>
    </div>

</div>


<?php

$this->registerJs(<<<JS
    var taxrobotPayPanelOpen = function() {

        if (parseInt($("#totalTaxSum").attr('data-sum')) > 0) {
            $(".taxrobot-pay-panel .part-pay").removeClass('hidden');
            $(".taxrobot-pay-panel .part-help").addClass('hidden');
        } else {
            $(".taxrobot-pay-panel .part-pay").addClass('hidden');
            $(".taxrobot-pay-panel .part-help").removeClass('hidden');
        }
        $(".taxrobot-pay-panel .part-declaration").addClass('hidden');

        $(".taxrobot-pay-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".taxrobot-pay-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".taxrobot-pay-panel .main-block").scrollTop(0);
    }

    var taxrobotPayPanelOpenDeclaration = function() {

        $(".taxrobot-pay-panel .part-help").addClass('hidden');
        $(".taxrobot-pay-panel .part-pay").addClass('hidden');
        $(".taxrobot-pay-panel .part-declaration").removeClass('hidden');

        $(".taxrobot-pay-panel-trigger").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".taxrobot-pay-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".taxrobot-pay-panel .main-block").scrollTop(0);
    }

    var taxrobotPayPanelClose = function() {
        $(".taxrobot-pay-panel-trigger").removeClass("active");
        $(".taxrobot-pay-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
    }

    $(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
        e.preventDefault();
        taxrobotPayPanelOpen();
    });

    $(document).on("click", ".taxrobot-pay-panel-close", function () {
        taxrobotPayPanelClose();
    });

    $(document).on("click", ".modal-order-load-statement", function(e) {
        $('.update-order-movement-link').click();
    });

    $(document).on("click", ".modal-bank-load-statement-1c", function(e) {
        $('.banking-module-open-link').click();
    });

    $(document).on("click", ".modal-bank-load-statement", function(e) {
        $('.banking-module-open-link').click();
    });

    $(document).on("click", ".modal-ofd-load-statement", function(e) {
        $('.ofd-module-open-link').click();
    });

    $(document).on("click", ".modal-bank-add-statement", function(e) {
        $('.update-bank-movement-link').click();
    });

    $(document).on("click", ".tariff-group-payment-form .choose-tariff", function(e) {
        e.preventDefault();
        var tariff = $(this);
        var form = tariff.closest('form');
        var tariffId = tariff.data('tariff-id');
        $('#paymentform-tariffid', form).val(tariffId);
        tariff.closest('.dropdown').removeClass('open').find('.dropdown-toggle').html(tariff.html());
        $('.tariff-price', form).addClass('hidden');
        $('.tariff-price-'+tariffId, form).removeClass('hidden');

        var per_month = $('.tariff-price-per-month');
        $('.tariff-price', per_month).addClass('hidden');
        $('.tariff-price-'+tariffId, per_month).removeClass('hidden');
    });

    $(document).on('click', '.tariff-group-payment-form .submit', function (e) {
        e.preventDefault();
        var button = this;
        var form = $(this).closest('form');
        var formData = form.serializeArray();
        formData.push({ name: button.name, value: button.value });
        $(button).addClass('ladda-button');
        var l = Ladda.create(button);
        l.start();
        $.post($(form).attr('action'), formData, function(data) {
            Ladda.stopAll();
            l.remove();
            $(button).removeClass('ladda-button');
            if (data.content) {
                $('.form-submit-result', form).html(data.content);
            }
            if (data.alert && typeof data.alert === 'object') {
                for (alertType in data.alert) {
                    var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                    $.alert('#js-form-alert', data.alert[alertType], {type: alertType, topOffset: offset});
                }
            }
            if (data.done) {
                if (data.link) {
                    var link=document.createElement("a");
                    link.id = 'document-print-link';
                    link.href = data.link;
                    link.target = '_blank';
                    $('.form-submit-result', form).html(link);
                    document.getElementById('document-print-link').click();
                }
            }
        });
    });
JS
);

if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    taxrobotPayPanelOpen();
');
}