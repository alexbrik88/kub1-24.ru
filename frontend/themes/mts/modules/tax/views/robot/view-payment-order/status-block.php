<?php

use common\components\date\DateHelper;
use frontend\themes\mts\components\Icon;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="pl-2 pr-2 w-100">
        <div class="button-regular paid mb-3 pl-3 pr-3 w-100">
            <?= Icon::get('check-2', ['class' => 'mr-1']) ?>
            <span class="ml-3"><?= ArrayHelper::getValue($model, 'paymentOrderStatus.name') ?></span>
            <span class="ml-auto mr-1">
                <?= date('d.m.Y', $model->payment_order_status_updated_at) ?>
            </span>
        </div>
    </div>
</div>