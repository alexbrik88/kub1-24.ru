<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\TaxRobotHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\modules\tax\widgets\AddIncomeWidget;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use frontend\widgets\RobotCheckingAccountantFilterWidget;
use frontend\widgets\BoolleanSwitchWidget;
use kartik\switchinput\SwitchInput;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use \frontend\modules\cash\modules\ofd\components\Ofd;
use \common\models\company\RegistrationPageType;
use \frontend\modules\cash\modules\ofd\widgets\OfdModalWidget;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\SqlDataProvider
 * @var $company Company
 */

$this->title = 'Доходы за ' . $taxRobot->getPeriodLabel();
$this->params['step'] = 3;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->user->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$accountArray = $company->getCheckingAccountants()->orderBy([
    'type' => SORT_ASC,
    'rs' => SORT_ASC,
])->all();
$account = end($accountArray);

// Для НУЛЕВОЙ декларации
if (empty($account)) {
    $account = new CheckingAccountant(['company_id' => $company->id, 'type' => CheckingAccountant::TYPE_MAIN]);
}

$cashbox = $company->getCashboxes()->orderBy(['is_main' => SORT_DESC])->one();

$contractorList = $taxRobot->getContractorList($taxRobot->getIncomeSearchQuery());
$statistic = ArrayHelper::index($taxRobot->getIncomeStatistic(), 'flow_name', 'payment_type');
$bankStatistic = ArrayHelper::getValue($statistic, TaxRobotHelper::TYPE_BANK, []);
$orderStatistic = ArrayHelper::getValue($statistic, TaxRobotHelper::TYPE_ORDER, []);
$orderTotal = array_sum(ArrayHelper::getColumn($orderStatistic, 'total_amount', false));
$orderTaxable = array_sum(ArrayHelper::getColumn($orderStatistic, 'taxable_amount', false));

$bankLogo = Html::img('/img/icons/bank1.png', [
    'style' => 'width:32px; max-width: none;'
]);

$bankData = [];
foreach ($accountArray as $account) {
    $bank = Bank::find()->andWhere(['bik' => $account->bik])->byStatus(!Bank::BLOCKED)->one();
    if ($bank !== null && $bank->little_logo_link) {
        $logo = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32]);
    } else {
        $logo = $bankLogo;
    }

    $bankData[$account->rs] = [
        'active' => $account->type != CheckingAccountant::TYPE_CLOSED,
        'logo' => $logo,
        'bank_name' => $account->bank_name,
        'flow_name' => $account->rs,
        'total_amount' => 0,
        'taxable_amount' => 0,
    ];
}
foreach ($bankStatistic as $account_rs => $data) {
    if (isset($bankData[$data['flow_name']])) {
        $bankData[$data['flow_name']]['active'] = true;
        $bankData[$data['flow_name']]['total_amount'] = $data['total_amount'];
        $bankData[$data['flow_name']]['taxable_amount'] = $data['taxable_amount'];
    } else {
        $bankData[$data['flow_name']] = [
            'active' => true,
            'logo' => $bankLogo,
            'bank_name' => '',
            'flow_name' => $data['flow_name'],
            'total_amount' => 0,
            'taxable_amount' => 0,
            'account_id' => null
        ];
    }
}

$bankingRoute = ['/tax/robot/bank'];
if (($urlPeriod = $taxRobot->getUrlPeriodId()) !== null) {
    $bankingRoute['period'] = $urlPeriod;
}
$bankUploadRoute = [
    '/cash/banking/default/index',
    'p' => Banking::routeEncode($bankingRoute),
];

function moneyFormat($val)
{
    return number_format($val / 100, 2, '.', ' ');
}

$cashboxes = \common\models\cash\Cashbox::find()->where(['company_id' => $company->id])->all();
$cashboxRegNumbers = ArrayHelper::map((array)$cashboxes, 'name', 'reg_number');

$isTaxcomUser = Yii::$app->request->cookies['taxcom_user'] || $company->registration_page_type_id == RegistrationPageType::PAGE_TYPE_TAX_COM_OFD;
$isPulsateNullDeclaration = ($company->mainCheckingAccountant === null || $company->registration_page_type_id == RegistrationPageType::PAGE_TYPE_NULL_DECLARATION_TEMPLATE_IP);
$isPulsateBank = !$isPulsateNullDeclaration;

$bankTotal = array_sum(ArrayHelper::getColumn($bankStatistic, 'total_amount', false));
$bankTaxable = array_sum(ArrayHelper::getColumn($bankStatistic, 'taxable_amount', false));

$js = <<<JS
var updateMoneyMovement = function(url) {
    $('#update-movement-modal').modal('show');
    $.pjax({url: url, push: false, container: '#update-movement-pjax'});
}
$(document).on("click", ".update-attribute-tooltip", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
    $(this).tooltipster("open");
});
$(document).on("submit", "form.update-attribute-tooltip-form", function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: this.action,
        method: this.method,
        data: form.serialize(),
        success: function() {
            $(".update-attribute-tooltip").tooltipster("close");
            $.pjax.reload("#tax-robot-pjax");
        }
    })
});
$(document).on("click", "a.update-movement-link", function(e) {
    e.preventDefault();
    updateMoneyMovement(this.href);
});
$(document).on("click", "tr.flow-row > td:not(.no-update-modal)", function(e) {
    var row = $(this).closest('tr.flow-row');
    updateMoneyMovement(row.data("update-url"));
});
$(document).on('hidden.bs.modal', '#update-movement-modal', function (e) {
    $('.modal-title', this).html('');
    $('#update-movement-pjax', this).html('');
})
$(document).on("pjax:complete", "#tax-robot-pjax", function(e) {
});
$('.collapse').on('show.bs.collapse hidden.bs.collapse', function (e) {
    $($(e.target).data('chevron')).toggleClass('fa-chevron-down fa-chevron-left')
})
$(document).on("click", "#taxable-group .link", function() {
    var taxable = $(this).attr('data-taxable');
    var flow_ids = [];
    $('.joint-operation-checkbox:checked').each(function() {
        flow_ids.push($(this).val());
    });

    $('.tooltip-group-taxable').tooltipster('close');
    $.get("/tax/robot/taxable-group", {
        is_taxable: taxable,
        flow_ids: flow_ids,
    }, function (data) {
        $.pjax.reload("#tax-robot-pjax");
    });
    $('#summary-container').removeClass('visible check-true');
});
JS;
$this->registerJs($js);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-group-taxable',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub', 'tooltipster-switch-widget'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'interactive' => true,
    ],
]);
?>

<div class="tax-robot-bank">

<?= AddIncomeWidget::widget([
    'company' => $company,
    'accountArray' => $accountArray,
    'cashbox' => $cashbox,
    'taxRobot' => $taxRobot,
]) ?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'tax-robot-pjax',
]); ?>

<div class="wrap pt-3 pb-2 pl-4 pr-3">
    <div class="pt-1 pb-1 pl-2">
        <div class="row align-items-center">
            <div class="column">
                <h4 class="mb-2">
                    <span>ДОХОДЫ за</span>
                    <div class="dropdown d-inline-block popup-dropdown popup-dropdown_right">
                        <?= Html::a(Html::tag('span', $taxRobot->period->label) . Icon::get('shevron'), '#', [
                            'id' > 'cardProductTitle',
                            'class' => 'link link_title',
                            'role' => 'button',
                            'data-toggle' => 'dropdown',
                        ])?>
                        <div class="dropdown-menu keep-open" aria-labelledby="cardProductTitle">
                            <div class="popup-dropdown-in overflow-hidden">
                                <ul class="form-filter-list list-clr">
                                    <?php foreach ($taxRobot->getPeriodDropdownItems() as $key => $item) : ?>
                                        <li style="white-space: nowrap;">
                                            <?= Html::a($item['label'], $item['url']) ?>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </h4>
            </div>
            <div class="column ml-auto">
                <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']).'Добавить доход', [
                    'class' => 'button-regular button-regular_red width-160 toggle-add-income-modal',
                    'data' => [
                        'pulsate' => 1,
                        'toggle' => 'modal',
                        'target' => '#add-income-modal',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
<div class="wrap p-4 mb-2">
    <div class="p-2">
        <table class="table table-style table-collapse mb-0">
            <thead>
                <tr>
                    <th class="pb-3 pl-0 pr-0 pt-0 no-border">
                        <div class="th-title d-block text_size_20 text-dark weight-400">
                            <span>Какие приходы не нужно учитывать в УСН</span>
                            <svg class="svg-icon link" data-toggle="toggleVisible" data-target="invoice" style="cursor: pointer; margin-left:10px; margin-top:-3px; width:20px; height: 20px;" viewBox="0 0 16 15">
                                <path d="M9.25 13.17H16v1.2H9.25a.636.636 0 0 0-.625.63h-1.25a.636.636 0 0 0-.625-.63H0v-1.2h6.75c.465 0 .904.168 1.25.473a1.88 1.88 0 0 1 1.25-.472zM16 0v11.804H9.25a.63.63 0 0 0-.625.635h-1.25a.63.63 0 0 0-.625-.635H0V0h6.75c.48 0 .918.184 1.25.486C8.332.184 8.77 0 9.25 0H16zM7.273 10.61V1.742a.638.638 0 0 0-.631-.644H1.09v9.401h5.55c.222 0 .434.04.632.11zm7.636-9.512H9.36a.638.638 0 0 0-.632.643v8.869a1.84 1.84 0 0 1 .631-.111h5.551V1.098z"></path>
                            </svg>
                        </div>
                    </th>
                    <th class="pb-3 pl-3 pr-3 pt-0 no-border text-right"><div class="th-title d-block text_size_20 text-dark weight-400">Всего приходов</div></th>
                    <th class="pb-3 pl-3 pr-4 pt-0 no-border text-right">
                        <div class="th-title d-block text_size_20 text-dark weight-400" style="padding-right: 30px;">
                            Доходы для расчета налогов
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr role="button" data-toggle="collapse" data-target="#tableMore" aria-expanded="true" aria-controls="tableMore">
                    <td class="pt-2 pb-2 pr-3 pl-0 no-border-right">
                        <div class="weight-700 color-dark line-height-1 d-flex flex-nowrap align-items-center">
                            <?= Html::img('/img/icons/bank1.png', [
                                'style' => 'width:20px;'
                            ]) ?>
                            <span class="ml-1">РУБЛЕВЫЕ СЧЕТА</span>
                        </div>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <?= moneyFormat($bankTotal); ?>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <div class="d-flex flex-nowrap align-items-center justify-content-end">
                            <span class="mr-4">
                                <?= moneyFormat($bankTaxable); ?>
                            </span>
                            <div class="button-clr p-2 ml-2">
                                <?= Icon::get('shevron', ['class' => 'svg-icon-collapse text_size_14 text-grey-light']) ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="p-0 no-border" colspan="3">
                        <div class="collapse show" id="tableMore">
                            <table class="table table-style mb-0">
                                <tbody>
                                    <?php foreach ($bankData as $data) : ?>
                                        <?php if ($data['active']) : ?>
                                            <tr>
                                                <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
                                                    <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1"
                                                        onclick="$('#bank-upload-open-link').click()">
                                                        <div class="mr-2" style="position: relative;">
                                                            <?= $data['logo'] ?>
                                                        </div>
                                                        <div class="ml-1">
                                                            <div>
                                                                <?= $data['bank_name'] ?>
                                                            </div>
                                                            <div class="text_size_14 text-grey-light mt-1">
                                                                <?= $data['flow_name'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                                                    <?= moneyFormat($data['total_amount']) ?>
                                                </td>
                                                <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                                                    <span class="mr-5 pr-2"><?= moneyFormat($data['taxable_amount']) ?></span>
                                                </td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr class="collapsed" role="button" data-toggle="collapse" data-target="#tableMore2" aria-expanded="false" aria-controls="tableMore2">
                    <td class="pt-2 pb-2 pr-3 pl-0 no-border-right">
                        <div class="weight-700 color-dark line-height-1 d-flex flex-nowrap align-items-center">
                            <i class="fa fa-money" style="font-size: 19px;"></i>
                            <span class="ml-1">КАССА</span>
                            <?php if ($isTaxcomUser): ?>
                                <?= Html::a('<img width="30" height="45" src="/img/icons/taxcom.png"/> Загрузить из ОФД TaxCOM', [
                                    '/cash/ofd/taxcom/default/index',
                                    'p' => Ofd::routeEncode($bankingRoute),
                                ], [
                                    'class' => 'ofd-module-open-link',
                                    'style' => 'font-size: 13px;font-weight: 400;letter-spacing: normal; text-transform: none; margin-left:15px;',
                                    'data' => [
                                        'pjax' => '0',
                                    ],
                                ]) ?>
                            <?php endif; ?>
                        </div>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <?= moneyFormat($orderTotal); ?>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <div class="d-flex flex-nowrap align-items-center justify-content-end">
                            <span class="mr-4">
                                <?= moneyFormat($orderTaxable); ?>
                            </span>
                            <div class="button-clr p-2 ml-2">
                                <?= Icon::get('shevron', ['class' => 'svg-icon-collapse text_size_14 text-grey-light']) ?>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php if ($orderStatistic) : ?>
                <tr>
                    <td class="p-0 no-border" colspan="3">
                        <div class="collapse" id="tableMore2">
                            <table class="table table-style mb-0">
                                <tbody>
                                <?php foreach ($orderStatistic as $data) : ?>
                                    <?php $isOfdCashbox = (!empty($cashboxRegNumbers[$data['flow_name']])) ?>
                                    <tr>
                                        <td class="pt-2 pb-2 pr-3 pl-4 no-border-right">
                                            <div class="color-dark line-height-1 d-flex flex-nowrap align-items-center pt-1 pb-1">
                                                <div class="mr-2">
                                                    <?php if ($isOfdCashbox): ?>
                                                        <img width="27" height="38" src="/img/icons/taxcom.png"/>
                                                    <?php else: ?>
                                                        <i class="fa fa-money"></i>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="ml-1">
                                                    <?php if ($isOfdCashbox): ?>
                                                        <div>
                                                            ОФД TaxCOM
                                                        </div>
                                                        <div class="text_size_14 text-grey-light mt-1">
                                                            <?= $data['flow_name'] ?>
                                                        </div>
                                                    <?php else: ?>
                                                        <div>
                                                            <?= $data['flow_name'] ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                                            <?= moneyFormat($data['total_amount']) ?>
                                        </td>
                                        <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                                            <span class="mr-5 pr-2"><?= moneyFormat($data['taxable_amount']) ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <?php endif ?>

                <tr>
                    <td class="pt-2 pb-2 pr-3 pl-0 no-border-right">
                        <div class="weight-700 color-dark line-height-1 d-flex flex-nowrap align-items-center">
                            <img src="/img/icons/equality-sign.png" style="width:20px">
                            <span class="ml-1">ИТОГО</span>
                        </div>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <div class="weight-700">
                            <?= moneyFormat($bankTotal + $orderTotal) ?>
                        </div>
                    </td>
                    <td class="pt-2 pb-2 pl-3 pr-3 text-right no-border-right">
                        <span id="totalTaxSum" class="weight-700 mr-5 pr-1" data-sum="<?=($bankTaxable + $orderTaxable)?>">
                            <?= moneyFormat($bankTaxable + $orderTaxable) ?>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="pt-1 pb-1 mb-2 d-flex flex-wrap">
    <div class="text_size_20" style="align-self: center;">Приход</div>
</div>
<div class="wrap wrap_padding_none">
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $taxRobot,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'rowOptions' => function ($data, $key, $index, $grid) {
            return [
                'class' => 'flow-row',
                'data' => [
                    'update-url' => Url::to([
                        'update-movement',
                        'type' => $data['payment_type'],
                        'id' => $data['id']
                    ]),
                ]
            ];
        },
        'layout' => '{items}',
        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'class' => '',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'no-update-modal',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::checkbox('flowId[]', false, [
                        'class' => 'joint-operation-checkbox no-update-modal',
                        'value' => $data['payment_type'] . '_' . $data['id'],
                        'data' => [
                            'income' => bcdiv($data['amount'], 100, 2),
                            'expense' => 0,
                        ],
                    ]);
                },
            ],
            [
                'attribute' => 'date',
                'label' => 'Дата',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'amount',
                'label' => 'Приход',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($data) {
                    return moneyFormat($data['amount']);
                }
            ],
            [
                'attribute' => 'payment_type',
                'label' => 'Тип оплаты',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'format' => 'raw',
                'filter' => ['' => 'Все'] + TaxRobotHelper::$typeList,
                'value' => function ($data) {
                    return TaxRobotHelper::typeName($data['payment_type']) .
                            TaxRobotHelper::icon($data['payment_type'], [
                                'class' => 'link link_hover_blue ml-2',
                            ]);
                }
            ],
            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '12%',
                    'class' => 'nowrap-normal max10list',
                ],
                'format' => 'raw',
                'filter' => ['' => 'Все'] + $contractorList,
                'hideSearch' => false,
                's2width' => '300px',
                'hideSearch' => false,
                'value' => function ($data) use ($contractorList) {
                    return ArrayHelper::getValue($contractorList, $data['contractor_id'], '');
                }
            ],
            [
                'attribute' => 'description',
                'label' => 'Назначение',
                'headerOptions' => [
                    'width' => '30%',
                ],
            ],
            [
                'attribute' => 'is_taxable',
                'label' => 'Учитывать в налогах',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'no-update-modal',
                ],
                'filter' => ['' => 'Все', 1 => 'Да', 0 => 'Нет'],
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::tag('span', $data['is_taxable'] ? 'Да' : 'Нет', [
                        'class' => 'bool-switch-label',
                    ]) . "\n" . BoolleanSwitchWidget::widget([
                        'id' => 'change-taxable-' . $data['payment_type'] . '_' . $data['id'],
                        'action' => ['taxable', 'type' => $data['payment_type'], 'id' => $data['id']],
                        'inputName' => 'is_taxable',
                        'inputValue' => $data['is_taxable'],
                        'label' => 'Учитывать?',
                        'options' => [
                            'class' => 'dropdown-popup-in text-center',
                        ],
                        'labelOptions' => [
                            'class' => 'dropdown-price-title pt-3 pb-3',
                        ],
                        'buttonBoxOptions' => [
                            'class' => 'd-flex flex-nowrap justify-content-center',
                        ],
                        'buttonOptions' => [
                            'class' => 'dropdown-price-link link button-clr pt-3 pb-3 weight-400',
                        ],
                        'toggleButton' => [
                            'tag' => 'button',
                            'label' => Icon::get('pencil'),
                            'class' => 'button-regular button-hover-transparent button-clr ml-1 w-44',
                        ]
                    ]);
                },
            ],
        ],
    ]); ?>
</div>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => $this->render('/layouts/grid/pager', ['totalCount' => $dataProvider->totalCount]),
    'columns' => ['id'],
]); ?>

<div class="wrap wrap_btns fixed mb-0">
    <div class="row justify-content-between align-items-center">
        <div class="column">
            <?= Html::a('Назад', ['params'], [
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        </div>
        <div class="column ml-auto mr-auto">
            <?php if (!$taxRobot->getHasIncomeInThisYear()) : ?>
                <?= Html::a('Нулевая налоговая декларация', ['declaration',
                    'empty' => 1,
                    'period' => ($taxRobot->period) ? $taxRobot->period->id : ''
                ], [
                    'class' => 'link link_bold get-null-declaration',
                    'data' => [
                        'pjax' => '0',
                        'pulsate' => (int)$isPulsateNullDeclaration,
                    ],
                ]) ?>
            <?php endif ?>
        </div>
        <div class="column">
            <?= Html::a('Далее', ['calculation', 'period' => $urlPeriod], [
                'id' => 'taxrobot-next-step-btn',
                'class' => 'button-clr button-regular button-hover-transparent pr-3 pl-3 width-160' .
                            ($taxRobot->isPaid ? '' : ' taxrobot-pay-panel-trigger'),
                'data' => [
                    'pjax' => '0',
                ],
            ]); ?>
        </div>
    </div>
    <?= Html::hiddenInput('isPaid', (int)$taxRobot->isPaid, ['id' => 'isPaid']) ?>
</div>

<?php \yii\widgets\Pjax::end(); ?>

<?= $this->render('_select_tariff_modal', [
    'taxRobot' => $taxRobot,
    'groupIds' => [
        SubscribeTariffGroup::TAX_IP_USN_6,
        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
    ],
]); ?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? Html::tag('button', Icon::get('multi') . ' <span>Учитывать</span>', [
            'data-tooltip-content' => "#taxable-group",
            'class' => 'button-clr button-regular button-width button-hover-transparent tooltip-group-taxable',
        ]): null,
        $canDelete ? Html::a(Icon::get('garbage') . ' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php if ($canDelete) : ?>
    <?= $this->render('parts_bank/modal-many-delete'); ?>
<?php endif ?>

<?php Modal::begin([
    'id' => 'update-movement-modal',
    'title' => '',
    'closeButton' => [
        'label' => Icon::get('close'),
        'class' => 'modal-close close',
    ],
]); ?>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'update-movement-pjax',
        'enablePushState' => false,
        'linkSelector' => '.update-movement-link',
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>

<?php Modal::end(); ?>

<div class="tooltip_templates" style="display: none;">
    <div id="taxable-group" class="dropdown-popup-in text-center update-attribute-tooltip-content">
        <div class="dropdown-price-title pt-3 pb-3">Учитывать?</div>
        <div class="d-flex flex-nowrap justify-content-center">
            <button class="dropdown-price-link link button-clr pt-3 pb-3 weight-400" data-taxable="1">Да</button>
            <button class="dropdown-price-link link button-clr pt-3 pb-3 weight-400" data-taxable="0">Нет</button>
        </div>
    </div>
</div>

<div class="modal fade t-p-f modal_scroll_center" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="modal-close close" data-dismiss="modal" aria-hidden="true">
                    Icon::get('close')
                </button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<?= $this->render('parts_bank/modal-usn-payment-rules'); ?>

<?php
$newCheckingAccountant = new CheckingAccountant([
    'company_id' => $company->id,
    'type' => ($company->mainCheckingAccountant) ? CheckingAccountant::TYPE_ADDITIONAL : CheckingAccountant::TYPE_MAIN
]);
echo $this->render('parts_bank/modal_rs/_modal_form', [
    'checkingAccountant' => $newCheckingAccountant,
    'title' => 'Добавить расчетный счет',
    'id' => 'add-company-rs',
    'company' => $company,
]);
?>

</div>

<?php if (Yii::$app->session->getFlash('error')): ?>
    <?php $this->registerJs('
        $(document).ready(function(){
            $(".taxrobot-pay-panel-trigger").click();
        });
    ');
    ?>
<?php endif; ?>

<?php if (!$taxRobot->isPaid) {
    $this->registerJs('$(".sbs-step-4").addClass("taxrobot-pay-panel-trigger").removeAttr("href");');
} ?>

<?php $this->registerJs('
$(document).ready(function(){
    $(document).on("click", ".odds-panel-trigger", function(){
        $(".odds-panel").toggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $(document).on("click", ".odds-panel .side-panel-close", function () {
        $(".odds-panel").toggle("fast");
        $(".odds-panel-trigger").toggleClass("active");
    });

    $(".button-pulsate").find("a").each(function(i,v) {
        if ($(v).data("pulsate"))
            $(this).parents(".button-pulsate").pulsate({
            color: "#bf1c56",
            reach: 20,
            repeat: 3
        });
    });

});

$(document).on("click", ".sbs-step-4", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".sbs-step-5", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".sbs-step-6", function(e) {
    if (parseInt($("#totalTaxSum").attr("data-sum")) <= 0 || !$("#isPaid").val()) {
        taxrobotPayPanelOpen();
        return false;
    }
});
$(document).on("click", ".uppercase .ofd-module-open-link", function(e) {
    e.stopPropagation();
});
$(document).on("change", "#cashbankflowsform-rs", function (e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-checking-accountant") {
        e.preventDefault();
        $("#cashbankflowsform-rs").val("").trigger("change");
        $(".modal.new-company-rs").modal();
    }
});

$(document).on("submit", "form.form-checking-accountant", function () {
    $this = $(this);
    $.post($this.attr("action"), $(this).serialize(), function (data) {
        if (data.result == true) {
            var select = $("#cashbankflowsform-rs");
            select.find("option").remove();
            for (var key in data.options) {
                select.append($("<option></option>").attr("value", key).text(data.options[key]));
            }
            $companyRs = data.companyRs;
            if (data.val) {
                select.val(data.val);
            }
            $("#add-company-rs").find("form")[0].reset();
            $this.closest(".modal").modal("hide");
            $(".field-cashbankflowsform-rs").removeClass("has-error").find(".help-block").html("");
            Ladda.stopAll();

        } else {
            $this.html($(data.html).find("form").html());
        }
    });

    return false;
});

'); ?>