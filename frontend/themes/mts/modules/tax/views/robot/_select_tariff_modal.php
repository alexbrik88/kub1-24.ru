<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\mts\components\Icon;

$company = $taxRobot->company;

$needSelect = count($groupIds) > 1;
?>

<div class="page-shading-panel taxrobot-pay-panel-close hidden"></div>
<div class="taxrobot-pay-panel" style="padding: 20px 0; z-index: 100000;">
    <button type="button" class="close taxrobot-pay-panel-close" aria-hidden="true" style="    position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        line-height: 12px;
        opacity: 1;
    ">&times;</button>
    <div class="main-block" style="height: 100%; padding: 0 30px;">
        <?php if ($needSelect) : ?>
            <div class="main-content pt-0">
                <div class="tariff-block-wrapper mt-0" style="font-size: 18px;">
                    <strong>Выберите тариф</strong>
                </div>
                <?php foreach ($groupIds as $groupId) : ?>
                    <?php
                    $group = SubscribeTariffGroup::findOne($groupId);
                    $tariff = $group->getActualTariffs()->orderBy(['price' => SORT_ASC])->one();
                    $discount = $company->getMaxDiscount($tariff->id);
                    $discVal = $discount ? $discount->value : 0;
                    $price = round(max(0, $tariff->price - ($tariff->price * $discVal / 100)));
                    ?>
                    <div class="tariff-block-wrapper" data-group="<?= $groupId ?>">
                        <div class="header">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 60px; vertical-align: middle; text-align: center;">
                                        <?= Html::tag('div', Icon::get(Icon::forTariff($groupId), ['style' => 'font-size: 36px;']), [
                                            'style' => '
                                                display: inline-flex;
                                                align-items: center;
                                                justify-content: center;
                                                width: 60px;
                                                height: 60px;
                                                background-color: #e30611;
                                                color: #fff;
                                                border-radius: 30px;
                                            ',
                                        ]) ?>
                                    </td>
                                    <td style="padding-left: 10px; font-size: 18px;">
                                        <?= implode('<br>ИП', explode(' ИП', $group->name)) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding-top: 30px;">
                            <div class="text-bold">
                                <div style="display: inline-block; font-size: 18px;">
                                    Оплата за <?= $tariff->getTariffName() ?>
                                </div>
                                <table>
                                    <tr>
                                        <td style="font-size: 44px;"><?= $price ?>&nbsp₽</td>
                                        <td class="pad-l-10">
                                            <?php if ($discount) : ?>
                                                <div>
                                                    <del><?= $tariff->price ?> ₽</del>
                                                    <span style="color: red;">СКИДКА <?=round($discVal)?>%</span>
                                                </div>
                                                <div>
                                                    при оплате до <?= date('d.m.Y', $discount->active_to) ?>
                                                </div>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?= round($price / $tariff->duration_month) ?>
                                            ₽ / месяц
                                        </td>
                                    </tr>
                                </table>
                                <button  class="button-regular button-regular_red w-100 mt-3 tariff-block-button" data-group="<?= $groupId ?>">Выбрать</button>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endif ?>
        <?php foreach ($groupIds as $groupId) : ?>
            <?= $this->render('_select_tariff_form', [
                'taxRobot' => $taxRobot,
                'groupId' => $groupId,
                'needSelect' => $needSelect,
            ]) ?>
        <?php endforeach ?>
    </div>
</div>

<?php
if (Yii::$app->request->get('pay') && !$taxRobot->getIsPaid()) {
$this->registerJs('
    taxrobotPayPanelOpen();
');
}