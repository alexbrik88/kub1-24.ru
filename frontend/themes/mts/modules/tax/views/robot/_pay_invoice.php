<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var Invoice $invoice */

?>

<script type="text/javascript">
    var link = document.createElement("a");
    link.href = "<?= Url::to(['/documents/invoice/out-view', 'uid' => $invoice->uid]) ?>";
    link.target = "_blank";
    document.body.appendChild(link);
    link.click();
</script>