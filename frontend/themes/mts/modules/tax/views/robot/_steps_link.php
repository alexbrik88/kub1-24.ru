<?php

use frontend\themes\mts\components\Icon;

/* @var $this yii\web\View */
/* @var $i string */
/* @var $ok bool */
?>
<a href="{url}" class="nav-link d-flex flex-column flex-grow-1 align-items-center text-center pb-2 <?= $ok ? 'finish' : '' ?>">
    <div class="mb-2 pb-1">
        <?= Icon::get($i, ['class' => 'svg-icon svg-icon_font_size_40']) ?>
    </div>
    <div class="flex-grow-1 d-flex flex-column justify-content-center pb-1">{label}</div>
</a>