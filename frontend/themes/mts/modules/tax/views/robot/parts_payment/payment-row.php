<?php
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\rbac\UserRole;
use php_rutils\RUtils;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $taxRobot common\components\TaxRobotHelper */
/* @var $company common\models\Company */
/* @var $urlPeriod string */
/* @var $part string */
/* @var $key integer */

$img = Html::img([
    '/documents/payment-order/thumbnail',
    'id' => $model->id,
    'w' => 98,
    'h' => 98,
], [
    'style' => 'max-width: 100px; max-height: 100px; border: 1px solid #ddd;',
    'alt' => '',
]);
?>

<div class="<?= $key ? 'pad-l-40' : ''; ?>">
    <div class="bor-t-ddd"></div>
</div>
<div class="row pad-t-5 pad-b-5" style="cursor:pointer">
    <div class="col-xs-5">
        <div class="pad-l-40">
            <table style="width: 100%;">
                <tr>
                    <td style="width: 115px; vertical-align: middle;">
                        <?= Html::a($img, [
                            'view-payment-order',
                            'id' => $model->id,
                            'period' => $urlPeriod,
                            'part' => $part,
                        ], [
                            'data' => [
                                'pjax' => '0',
                            ],
                        ]) ?>
                    </td>
                    <td style="vertical-align: middle;">
                        <div>
                            <?= RUtils::dt()->ruStrFTime([
                                'format' => DateHelper::FORMAT_USER_DATE,
                                'date' => $model->document_date,
                            ]); ?>
                        </div>
                        <div>
                            <?= $model->purpose_of_payment ?>
                        </div>
                        <div>
                            <?= $model->contractor_name ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-xs-2 text-right" style="height: 34px; padding-top: 7px;">
        <?= TextHelper::invoiceMoneyFormat($model->sum, 2); ?>
    </div>
    <div class="col-xs-2 text-center" style="height: 34px; padding-top: 7px;">
        <?php if ($model->payment_order_status_id == PaymentOrderStatus::STATUS_PAID) : ?>
            <span style="color: #289b4a;">Оплачено</span>
        <?php else : ?>
            <span style="color: #b11d24;">Не оплачено</span>
        <?php endif ?>
    </div>
    <div class="col-xs-3" style="height: 34px; padding-top: 7px;">
        <?php if ($model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID &&
            Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::UPDATE)
        ) : ?>
            <?= Html::a(' <i class="fa fa-pencil fa-2x"></i> ', [
                'update-payment-order',
                'id' => $model->id,
                'period' => $urlPeriod,
            ], [
                'class' => 'btn darkblue btn-sm btn-ie',
                'title' => 'Редактировать',
                'data' => [
                    'pjax' => '0',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a('<i class="fa fa-print fa-2x"></i>', [
            '/documents/payment-order/document-print',
            'actionType' => 'print',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn btn-sm darkblue no-reload-status print',
            'data' => [
                'pjax' => '0',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', [
            '/documents/payment-order/document-print',
            'actionType' => 'pdf',
            'id' => $model->id,
            'type' => Documents::IO_TYPE_IN,
            'filename' => $model->getPrintTitle(),
        ], [
            'target' => '_blank',
            'title' => 'PDF',
            'class' => 'btn btn-sm darkblue',
            'data' => [
                'pjax' => '0',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-download fa-2x"></i>', [
            '/documents/payment-order/import',
            'id' => $model->id,
        ], [
            'target' => '_blank',
            'title' => 'Импорт в файл',
            'class' => 'btn btn-sm darkblue',
            'data' => [
                'pjax' => '0',
            ],
        ]) ?>
        <?php if ($model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID &&
            (Yii::$app->user->can(UserRole::ROLE_CHIEF) || Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)) &&
            ($paymentAccount = $company->getBankingPaymentAccountants()->one()) !== null &&
            ($alias = Banking::aliasByBik($paymentAccount->bik)) &&
            ($bank = Banking::getBankByBik($paymentAccount->bik))
        ) : ?>
            <?php $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                'class' => 'little_logo_bank',
                'style' => 'display: inline-block;',
            ]);?>
            <div class="mar-t-10">
                <?= Html::a($image.'<span>Отправить в ' . $bank->bank_name.'</span>', [
                    "/cash/banking/{$alias}/default/payment",
                    'account_id' => $paymentAccount->id,
                    'po_id' => $model->id,
                    'p' => Banking::routeEncode(array_filter(['/tax/robot/payment', 'period' => $taxRobot->getUrlPeriodId()])),
                ], [
                    'class' => 'banking-module-open-link',
                    'data' => [
                        'pjax' => '0',
                    ],
                    'style' => ''
                ]); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
