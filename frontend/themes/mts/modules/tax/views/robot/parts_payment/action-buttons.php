<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $urlPeriod string */

?>

<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Назад', ['calculation', 'period' => $urlPeriod], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-arrow-left fa-2x"></i>', ['calculation', 'period' => $urlPeriod], [
            'title' => 'Назад',
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg button-bottom-page-lg-double col-sm-2 col-xs-2">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Далее', ['declaration', 'period' => $urlPeriod], [
            'id' => 'submit',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
        <?= Html::a('<i class="fa fa-arrow-right fa-2x"></i>', ['declaration', 'period' => $urlPeriod], [
            'id' => 'submit',
            'title' => 'Далее',
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'data' => [
                'pjax' => '0',
            ],
        ]); ?>
    </div>
</div>
