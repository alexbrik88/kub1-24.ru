<?php

namespace frontend\themes\mts\modules\documents\widgets;

use common\models\Agreement;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\status\AgreementStatus;
use common\models\document\Waybill;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


/**
 * Class DocumentLogWidget
 * @package frontend\themes\mts\modules\documents\widgets
 */
class DocumentLogWidget extends Widget
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $closeButton;
    /**
     * @var
     */
    public $toggleButton;
    /**
     * @var
     */
    public $footer;

    public static $eventsM = [
        LogEvent::LOG_EVENT_CREATE => 'Создан',
        LogEvent::LOG_EVENT_DELETE => 'Удален',
        LogEvent::LOG_EVENT_UPDATE => 'Изменен',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];
    public static $eventsF = [
        LogEvent::LOG_EVENT_CREATE => 'Создана',
        LogEvent::LOG_EVENT_DELETE => 'Удалена',
        LogEvent::LOG_EVENT_UPDATE => 'Изменена',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];
    public static $eventsN = [
        LogEvent::LOG_EVENT_CREATE => 'Создано',
        LogEvent::LOG_EVENT_DELETE => 'Удалено',
        LogEvent::LOG_EVENT_UPDATE => 'Изменено',
        LogEvent::LOG_EVENT_UPDATE_STATUS => 'Статус изменён',
    ];

    /**
     * @return array
     */
    public function getEvents($model)
    {
        switch ($model->className()) {
            case InvoiceFacture::className():
            case PackingList::className():
            case Waybill::className():
            case TaxDeclaration::className():
                return self::$eventsF;
                break;

            case PaymentOrder::className():
                return self::$eventsN;
                break;

            default:
                return self::$eventsM;
                break;
        }
    }

    /**
     *
     */
    public function run()
    {
        $modelNameArray = $this->model->className();
        if ($this->model instanceof Invoice) {
            $modelNameArray = [$this->model->className(), InvoiceAuto::className()];
        }
        $logArray = Log::find()->andWhere([
            'model_id' => $this->model->id,
            'model_name' => $modelNameArray,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->orderBy(['id' => SORT_ASC])->all();
        $events = $this->getEvents($this->model);

        Modal::begin([
            'options' => [
                'class' => 'doc-history-modal fade',
            ],
            'closeButton' => $this->closeButton !== null ? $this->closeButton : false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'class' => 'btn darkblue btn-sm info-button',
                'style' => 'width:33px; height: 27px;',
                'label' => '<i class="icon-info" style="color:white"></i>',
                'title' => 'Последние действия',
            ],
            'footer' => $this->footer !== null ? $this->footer : Html::button('OK', [
                'class' => 'button-list button-hover-transparent button-clr',
                'data-dismiss' => 'modal',
            ]),
        ]);

        echo '<h4 class="modal-title">Последние действия</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>';

        $tableView = count($logArray) > 1;

        if ($tableView) {
            echo Html::beginTag('table', ['class' => 'table table-style']);
            echo Html::tag('thead', Html::tag('tr',
                Html::tag('th', 'Дата') .
                Html::tag('th', 'Тип') .
                Html::tag('th', 'Сотрудник')));
        }

        foreach ($logArray as $log) {
            $data = $log->attributesNewArray;
            $eventName = ArrayHelper::getValue($events, $log->log_event_id);
            if ($log->log_event_id == LogEvent::LOG_EVENT_UPDATE_STATUS) {
                $statusId = ArrayHelper::getValue($data, 'invoice_status_id') ? : (
                    ArrayHelper::getValue($data, 'status_out_id') ? : (
                        ArrayHelper::getValue($data, 'payment_order_status_id') ? :
                        ArrayHelper::getValue($data, 'status_id')
                    )
                );

                $statusClass = $this->model->getDocumentStatus()->modelClass;
                $status = $statusClass::find()->where(['id' => $statusId])->one();
                if ($status) {
                    $eventName = $status->name;
                    if ($modelNameArray == 'common\models\Agreement' && $status->id == AgreementStatus::STATUS_CREATED)
                        $eventName = 'В работе';
                }
            }
            $fio = $log->employeeCompany ? $log->employeeCompany->fio : ($log->employee ? $log->employee->fio : '');

            if ($tableView) {
                echo Html::tag('tr',
                    Html::tag('td', date('d.m.Y', $log->created_at)) .
                    Html::tag('td', $eventName) .
                    Html::tag('td', $fio));
            } else {
                echo Html::tag('div', date('d.m.Y', $log->created_at) . " $eventName $fio", [
                    'class' => 'created-by',
                    'style' => 'margin-top: 5px;',
                ]);
            }

        }

        if ($tableView) {
            echo Html::endTag('table');
        }

        Modal::end();
    }
}
