<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\document\OrderPackingList;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\PackingList;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$waybillDateFormatted = ($model->waybill_number) ?  $model->waybill_number . ' от ' . DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
$realMassNet = $model->invoice->getRealMassNet();
$realMassGross = $model->invoice->getRealMassGross();
$company = $model->invoice->company;

$addStamp = (boolean) $model->add_stamp;
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp || !$company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp || !$company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp || !$company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$precision = $model->invoice->price_precision;
?>
<style>
    @media print {
        thead {display: table-header-group;}
    }
<?php if (Yii::$app->request->get('actionType') == 'print') : ?>
    @page {
        size:A4 landscape;
    }
<?php endif ?>
<?php if ($addStamp) : ?>
    #print_layer {
        height: 230px;
        background-image: url('<?= $printLink ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature1_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '41px' : '35px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature2_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '85px' : '60px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
<?php endif ?>

    table td.va-middle {vertical-align: middle;}
    table td.bordered, table .bordered td {border:1px solid #000;}
    table td.border-bottom {border-bottom:1px solid #000;}
    table td.tip {padding:0 0 3px 0;font-style: italic; vertical-align: top;}
    td {font-size:8pt;}
</style>


<div class="page-content-in p-center-album pad-pdf-p-album">
    <table class="table no-border">
        <tr>
            <td></td>
            <td width="15%" class="font-size-7 text-center" style="font-style: italic">
                Приложение 26<br/>к приказу Министра финансов<br/>Республики Казахстан<br/> от 20 декабря 2012 года №562
            </td>
        </tr>
        <tr>
            <td></td>
            <td><br/></td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-7 text-right">Форма 3-2</td>
        </tr>
    </table>
    <table class="table no-border va-bottom">
        <tr>
            <td width="27%" class="font-size-8">Организация (индивидуальный предприниматель)</td>
            <td class="border-bottom">
                <?php if ($model->consignor): ?>
                    <?= $model->consignor->getRequisitesFull(true); ?>
                <?php elseif ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                    $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                    ", {$model->invoice->company_address_legal_full}",
                    ", р/с {$model->invoice->company_rs}",
                    ", в банке {$model->invoice->company_bank_name}",
                    ", БИК {$model->invoice->company_bik}",
                    $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : ''; ?>
                <?php else: ?>
                    <?= $model->invoice->contractor->getRequisitesFull(true); ?>
                <?php endif; ?>
            </td>
            <td width="1%"></td>
            <td width="8%" class="va-middle font-size-8" style="text-align:right;padding-right:3px;">ИИН/БИН</td>
            <td width="10%" class="bordered text-center va-middle">###########</td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="90%"></td>
            <td class="text-center bordered font-size-8">Номер</td>
            <td class="text-center bordered font-size-8">Дата</td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center bordered"><?= $model->fullNumber; ?></td>
            <td class="text-center bordered"><?= $dateFormatted ?></td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td class="font-size-8-bold text-center">НАКЛАДНАЯ НА ОТПУСК ЗАПАСОВ НА СТОРОНУ</td>
        </tr>
    </table>
    <table class="table" style="margin-bottom: 10px">
        <tr>
            <td class="font-size-8 text-center">Организация (индивидуальный предприниматель) - отправитель</td>
            <td class="font-size-8 text-center">Организация (индивидуальный предприниматель) - получатель</td>
            <td class="font-size-8 text-center">Ответственный за поставку (Ф.И.О)</td>
            <td class="font-size-8 text-center">Транспортная организация</td>
            <td class="font-size-8 text-center">Товарно-транспортная накладная</td>
        </tr>
        <tr>
            <td class="text-center">
                <?php if ($model->consignor): ?>
                    <?= $model->consignor->getTitle(true); ?>
                <?php elseif ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->company_name_short ?>
                <?php else: ?>
                    <?= $model->invoice->contractor->getTitle(true) ?>
                <?php endif; ?>
            </td>
            <td class="text-center">
                <?php if ($model->consignee): ?>
                    <?= $model->consignee->getTitle(true) ?>
                <?php elseif ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->contractor->getTitle(true) ?>
                <?php else: ?>
                    <?= $model->invoice->company_name_short ?>
                <?php endif; ?>
            </td>
            <td class="text-center"></td>
            <td class="text-center"></td>
            <td class="text-center"><?= $waybillDateFormatted ?></td>
        </tr>
    </table>

    <table class="table font-size-8">
        <tr>
            <td width="5%" rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Номер по порядку</td>
            <td rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Наименование, характеристика</td>
            <td rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Номенклатурный номер</td>
            <td rowspan="2" width="5%" class="text-center font-size-8 font-size-8" style="padding: 1px 2px">Единица измерения</td>
            <td colspan="2" class="text-center font-size-8" style="padding: 1px 2px">Количество</td>
            <td rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Цена за единицу, в KZT</td>
            <td rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Сумма с НДС, в KZT</td>
            <td rowspan="2" class="text-center font-size-8" style="padding: 1px 2px">Сумма НДС, в KZT</td>
        </tr>
        <tr>
            <td class="text-center font-size-8" style="padding: 1px 2px">Подлежит отпуску</td>
            <td class="text-center font-size-8" style="padding: 1px 2px">Отпущено</td>
        </tr>
        <tr>
            <td class="text-center  font-size-7">1</td>
            <td class="text-center  font-size-7">2</td>
            <td class="text-center  font-size-7">3</td>
            <td class="text-center  font-size-7">4</td>
            <td class="text-center  font-size-7">5</td>
            <td class="text-center  font-size-7">6</td>
            <td class="text-center  font-size-7">7</td>
            <td class="text-center  font-size-7">8</td>
            <td class="text-center  font-size-7">9</td>
        </tr>
        <?php
        $orderQuery = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order):
            if ($model->invoice->hasNds) {
                $priceNoNds = $order->priceNoNds;
                $amountNoNds = $order->amountNoNds;
                $amountWithNds = $order->amountWithNds;
                $TaxRateName = $order->order->saleTaxRate->name;
                $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
            } else {
                $priceNoNds = $order->priceWithNds;
                $amountNoNds = $order->amountWithNds;
                $amountWithNds = $order->amountWithNds;
                $TaxRateName = 'Без НДС';
                $ndsAmount = Product::DEFAULT_VALUE;
            } ?>
            <tr>
                <td class="text-center font-size-8" style="padding: 1px 2px" style="padding: 1px 2px"><?= $key + 1; ?></td>
                <td class="font-size-8"><?= $order->order->product_title; ?></td>
                <td class="text-center font-size-8" style="padding: 1px 2px"><?= $order->order->product_code; ?></td>
                <td class="text-center font-size-8" style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                <td class="text-center font-size-8" style="padding: 1px 2px"><?= $order->quantity; ?></td>
                <td class="text-center font-size-8" style="padding: 1px 2px"><?= $order->quantity; ?></td>
                <td class="text-right font-size-8" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
                <td class="text-right font-size-8" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, $precision); ?></td>
                <td class="text-right font-size-8" style="padding: 1px 2px"><?= $ndsAmount; ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td class="text-right font-size-8" colspan="4" style="border: none !important; padding: 1px 2px">Итого</td>
            <td class="text-center font-size-8" style="padding: 1px 2px"><?= $orderQuery->sum('quantity') * 1; ?></td>
            <td class="text-center font-size-8" style="padding: 1px 2px"><?= $orderQuery->sum('quantity') * 1; ?></td>
            <td class="text-center font-size-8" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-8" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?></td>
            <td class="text-right font-size-8" style="padding: 1px 2px">
                <?php if ($model->invoice->hasNds) : ?>
                    <?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>
    <table class="table no-border">
        <tr>
            <td width="25%" class="font-size-8">Всего отпущено количество запасов (прописью)</td>
            <td class="border-bottom">
                <?php if ($model->invoice->total_place_count > 0) {
                    echo iconv("UTF-8", "UTF-8//IGNORE", TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($orderQuery->sum('quantity') * 1, RUtils::NEUTER)));
                } ?>
            </td>
            <td width="15%" class="font-size-8 text-center">на сумму (прописью), в KZT</td>
            <td width="40%" class="border-bottom">
                <?= TextHelper::mb_ucfirst(common\components\TextHelper::amountToWordsKZ($model->totalAmountWithNds / 100)); ?>
            </td>
        </tr>
    </table>

    <table class="table-no-margin no-border">
        <tr>
            <!--row1-->
            <td width="10%" class="font-size-8">Отпуск разрешил</td>
            <td width="10%" class="text-center border-bottom"></td>
            <td width="1%"  class="text-center">/</td>
            <td width="10%" class="text-center border-bottom"></td>
            <td width="1%"  class="text-center">/</td>
            <td colspan="2" class="text-center border-bottom">
                <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true); ?>
                    <?php if ($model->signed_by_employee_id) : ?>
                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                    <?php endif ?>
                <?php else: ?>
                    <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                        <?=$model->invoice->contractor->director_name?>
                    <?php else: ?>
                        <?=$model->invoice->contractor->chief_accountant_name?>
                    <?php endif ?>
                <?php endif; ?>
            </td>
            <td width="1%" style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td></td>
            <td>По доверенности</td>
            <td colspan="3" class="border-bottom">
                <?= ($model->proxy_number && $model->proxy_date) ?
                    $model->proxy_number . ' от ' . Yii::$app->formatter->asDate($model->proxy_date, 'long') : '---'; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center font-size-7 tip">должность</td>
            <td></td>
            <td class="text-center font-size-7 tip">подпись</td>
            <td></td>
            <td colspan="2" class="text-center font-size-7 tip">расшифровка подписи</td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td width="1%"></td>
            <td width="10%"></td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="28%"></td>
        </tr>
        <tr>
            <td colspan="8" style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td></td>
            <td>выданной</td>
            <td colspan="3" class="border-bottom">
                <?= $model->consignee ? $model->consignee->getTitle(true) : $model->invoice->contractor_name_short,
                $model->given_out_position ? ", {$model->given_out_position}" : null,
                $model->given_out_fio ? ", {$model->given_out_fio}" : null; ?>
            </td>
        </tr>
        <tr><!--EMPTY-->
            <td colspan="8" style="border-right: 1px solid black;">&nbsp;</td>
            <!--row2-->
            <td colspan="5"></td>
        </tr>


        <tr>
            <td class="font-size-8">Главный бухгалтер</td>
            <td class="text-center border-bottom"></td>
            <td class="text-center">/</td>
            <td colspan="3" class="text-center border-bottom"></td>
            <td></td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td></td>
            <td colspan="4" class="border-bottom">&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center font-size-7 tip">подпись</td>
            <td></td>
            <td colspan="3" class="text-center font-size-7 tip">расшифровка подписи</td>
            <td></td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td colspan="5"></td>
        </tr>
        <tr>
            <td colspan="7" class="font-size-8-bold" style="padding-bottom:10px;">М.П.</td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td colspan="5"></td>
        </tr>
        <tr>
            <td class="font-size-8">Отпустил</td>
            <td class="text-center border-bottom"></td>
            <td class="text-center">/</td>
            <td colspan="3" class="text-center border-bottom"></td>
            <td></td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td></td>
            <td class="font-size-8">Запасы получил</td>
            <td class="text-center border-bottom"></td>
            <td class="text-center">/</td>
            <td class="text-center border-bottom"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center font-size-7 tip">подпись</td>
            <td></td>
            <td colspan="3" class="text-center font-size-7 tip">расшифровка подписи</td>
            <td></td>
            <td style="border-right: 1px solid black;"></td>
            <!--row2-->
            <td></td>
            <td></td>
            <td class="text-center font-size-7 tip">подпись</td>
            <td></td>
            <td class="text-center font-size-7 tip">расшифровка подписи</td>
        </tr>
    </table>

</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>








