<?php
use common\components\date\DateHelper;
use yii\helpers\Url;
use \common\components\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/** @var \common\models\document\InvoiceFacture $model */
/** @var $useContractor boolean */
/** @var $contractorId integer|null */

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#packinglist-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize());
    });
');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);


?>


<div class="response-controll-butt bg-white col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
    <div class="col-xs-12" style="padding-right:0px !important;">
        <div class="control-panel col-xs-12 pad0 pull-right">
            <div class="status-panel col-xs-12 pad0">
                <div class="col-xs-12 col-sm-3 pad3">
                    <div class="btn full_w marg darkblue no-margin-l box-status-act"
                         style="padding-left:0px; padding-right:0px;text-align: center; "
                         title="Дата изменения статуса">
                        <?=  date("d.m.Y", $model->is_original_updated_at ? $model->is_original_updated_at : $model->created_at); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9 pad0">
                    <div class="col-xs-12 pad3">
                        <div class="btn full_w marg btn-status full_w  darkblue" title="Статус">
                            <?php if ($model->is_original) : ?>
                                <i class="icon pull-left icon-original-document"></i>Оригинал
                            <?php else : ?>
                                <i class="icon pull-left fa fa-copy"></i>Копия
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
                <div class="col-xs-3"> &nbsp; </div>
                <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
                    <div class="document-panel widthe-i overflow-hide" style="width: 99.9% !important; margin-bottom: 0px;">
                        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                            'model' => $model->invoice,
                        ])
                        ): ?>
                            <a href="<?php echo Url::toRoute(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id, 'contractorId' => $contractorId,]); ?>"
                               class="btn btn-account yellow pull-right full_w" style="height: 34px;margin-left: 0; text-transform: uppercase;">
                                <?php $class = $model->invoice->invoiceStatus->className(); ?>
                                <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                                СЧЕТ
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
            <div class="portlet">
                <div class="customer-info" style="min-height:auto !important">
                    <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                        <table class="table no_mrg_bottom">
                            <tr>
                                <td>
                        <span
                                class="customer-characteristic color-black"><?= $message->get(\frontend\modules\documents\components\Message::CONTRACTOR); ?>
                            :</span>
                                    <span><?= Html::a($model->invoice->contractor_name_short, [
                                            '/contractor/view',
                                            'type' => $model->invoice->contractor->type,
                                            'id' => $model->invoice->contractor->id,
                                        ]) ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php if ($model->invoice->basis_document_name &&
                                        $model->invoice->basis_document_number &&
                                        $model->invoice->basis_document_date
                                    ) : ?>
                                        <span class="customer-characteristic color-black">Основание:</span>
                                        <span class="color-black">
                                            <?= $model->invoice->basis_document_name ?>
                                            № <?= Html::encode($model->invoice->basis_document_number) ?>
                                            от <?= DateHelper::format($model->invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </span>
                                    <?php else : ?>
                                        <span class="customer-characteristic color-black">Основание:</span>
                                        <span class="color-black">
                                            Счет № <?= $model->invoice->fullNumber; ?>
                                            от <?= DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        </span>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="margin-bottom: 5px;">
                                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                            'model' => $model,
                                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                                        ]); ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
