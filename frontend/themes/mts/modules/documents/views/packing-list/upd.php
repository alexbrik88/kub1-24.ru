<?php 
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
?>

<style>
@media print {
    thead {
        display: table-header-group;
    }
}
</style>
<div class="page-content-in p-center-album pad-pdf-p-album">
    <div class="text-right font-size-6 line-h-small">
        <p>Унифицированная форма № ТОРГ-12</p>
        <p>Утверждена постановлением Госкомстата России от 25.12.98 № 132</p>
    </div>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td colspan="5">
            </td>
            <td width="6%" class="font-size-7 text-center" style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">Коды</td>
        </tr>
                    <tr>
                <td class="font-size-7 text-left ver-bottom" colspan="2" rowspan="2" style="border-bottom: 1px solid black;padding: 1px 2px">
                    ООО "ЭВЕНТУАЛЬ", ИНН 7707751565                                            , КПП 770701001                                        , 127055, Москва, Угловой пер, Дом 2, р/с 40702810638000065087, в
                    банке ПАО СБЕРБАНК, БИК 044525225,
                    к/с 30101810400000000225                </td>
                <td colspan="3" class="font-size-8 text-right">Форма по ОКУД</td>
                <td class="font-size-8-bold text-center" style="border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">330212</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid black;"></td>
                <td class="font-size-8 text-right ver-bottom" colspan="2" style="padding: 1px 2px; border-bottom: 1px solid #ffffff">по ОКПО</td>
                <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
            </tr>
                <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 0">организация-грузоотправитель, адрес, телефон, факс, банковские реквизиты</td>

            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td colspan="3" style="border-bottom: 1px solid black;padding: 1px 0">
                <br/>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #ffffff"></td>
            <td class="font-size-8-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">структурное подразделение</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding-top: 1px; padding-bottom: 1px"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-8 text-right" style="padding: 1px 2px">Вид деятельности по ОКДП</td>
            <td class="font-size-8-bold text-center" style="padding-bottom:1px;padding-top: 1px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Грузополучатель</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                ООО "АЛЬБИОН"                , ИНН 5047157415                                , Юридический адрес                , р/с 40702810140020018858                , в банке ПАО СБЕРБАНК                , БИК 044525225                , к/с 30101810400000000225            </td>
            <td colspan="2" class="font-size-8 text-right" style="padding: 1px 2px;">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Поставщик</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                ООО "ЭВЕНТУАЛЬ", ИНН 7707751565                                    , КПП 770701001                                , 127055, Москва, Угловой пер, Дом 2, р/с 40702810638000065087, в
                банке ПАО СБЕРБАНК, БИК 044525225,
                к/с 30101810400000000225            </td>
            <td colspan="2" class="font-size-8 text-right ver-bottom" style="padding: 1px 2px">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Плательщик</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                ООО "АЛЬБИОН"                , ИНН 5047157415                                , Юридический адрес                , р/с 40702810140020018858                , в банке ПАО СБЕРБАНК                , БИК 044525225                , к/с 30101810400000000225            </td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="border-bottom: 1px solid black;padding: 1px 2px">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td width="1%"></td>
            <td width="4.8%" style="border-left: 1px solid black"></td>
            <td style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Основание</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">Договор            </td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="border-bottom: 1px solid black; border-left: 1px solid black;">номер</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                34            </td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">договор, заказ-наряд</td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">дата</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                28.03.2017            </td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-8 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px"> Транспортная накладная</td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">номер</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-8 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px"></td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">дата</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-9 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px">Вид</td>
            <td colspan="2" class="font-size-9 ver-bottom" style="padding: 1px 2px;">операции</td>
            <td class="font-size-9-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
    </table>

    <div style="margin-top: -2rem">
        <table class="table no-border" style="margin-right: 15%;margin-left: 8%;margin-bottom: 0">
            <tr>
                <td width=""></td>
                <td width="18%" class="font-size-7 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-left: 1px solid black;border-top: 1px solid black;">Номер документа</td>
                <td width="18%" class="font-size-7 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-top: 1px solid black;">Дата составления</td>
                <td></td>
            </tr>
            <tr>
                <td width="23%" class="font-size-8-bold text-center">ТОВАРНАЯ НАКЛАДНАЯ</td>
                <td class="font-size-7 text-center" style="padding: 1px 2px;border: 2px solid black;">2 </td>
                <td class="font-size-7 text-center" style="padding: 1px 2px;border-top: 2px solid black;border-right: 2px solid black;border-bottom: 2px solid black;">5 апреля 2017 г.</td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="page-number" style="padding: 0">
        Страница №1
    </div>

    <table class="table font-size-7" style="margin-bottom: 0 !important;">
        <tr>
            <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Но-мер по по-рядку</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Товар</td>
            <td width="10%" colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Единица измерения</td>
            <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Вид упаков-ки</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Количество</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Масса брутто</td>
            <td width="7%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Коли-чество (масса нетто)</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Цена, руб. коп.</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма без учета НДС, руб. коп.</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">НДС</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма с учётом НДС, руб. коп.</td>
        </tr>
        <tr>
            <td width="22%" class="text-center font-size-7 font-size-7" style="padding: 1px 2px">наименование, характеристика, сорт, артикул товара</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">код</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">наиме-нование</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">код по ОКЕИ</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">в одном месте</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">мест, штук</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">ставка, %</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">сумма, руб. коп.</td>
        </tr>
        <tr>
            <td class="text-center  font-size-6">1</td>
            <td class="text-center  font-size-6">2</td>
            <td class="text-center  font-size-6">3</td>
            <td class="text-center  font-size-6">4</td>
            <td class="text-center  font-size-6">5</td>
            <td class="text-center  font-size-6">6</td>
            <td class="text-center  font-size-6">7</td>
            <td class="text-center  font-size-6">8</td>
            <td class="text-center  font-size-6">9</td>
            <td class="text-center  font-size-6">10</td>
            <td class="text-center  font-size-6">11</td>
            <td class="text-center  font-size-6">12</td>
            <td class="text-center  font-size-6">13</td>
            <td class="text-center  font-size-6">14</td>
            <td class="text-center  font-size-6">15</td>
        </tr>
                <tr>
            <td class="text-center font-size-7" style="padding: 1px 2px" style="padding: 1px 2px">1</td>
            <td class="font-size-7">товар</td>
            <td class="text-center font-size-7" style="padding: 1px 2px"></td>
            <td class="text-center font-size-7" style="padding: 1px 2px">шт</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">796</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">1</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">100,00</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">100,00</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">Без НДС</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                                    0,00                            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px">100,00</td>
        </tr>
                <tr>
            <td class="text-center font-size-7" style="padding: 1px 2px" style="padding: 1px 2px">2</td>
            <td class="font-size-7">товар2</td>
            <td class="text-center font-size-7" style="padding: 1px 2px"></td>
            <td class="text-center font-size-7" style="padding: 1px 2px">шт</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">796</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">---</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">1</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">100,00</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">100,00</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">18%</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                                    18,00                            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px">118,00</td>
        </tr>
        
        <tr>
            <td class="text-right font-size-7" colspan="7" style="border: none !important; padding: 1px 2px">Итого</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">0</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">0</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">2</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">200,00</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                                    18,00                            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px">218,00</td>
        </tr>
        <tr>
            <td class="text-right font-size-7" colspan="7" style="border: none !important;padding: 1px 2px">Всего по накладной</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">0</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">0</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">2</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">200,00</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                                    18,00                            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px">218,00</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;margin-left: 10%">
        <tr>
            <td colspan="2" class="ver-bottom text-left font-size-7" style="padding-top: 1px;padding-bottom: 1px">Товарная накладная имеет приложение на</td>
            <td style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"></td>
            <td class="ver-bottom text-left font-size-7" style="padding-left: 6px">листах</td>
        </tr>
        <tr>
            <td width="9%" class="ver-bottom text-left font-size-7" style="padding-top: 1px;padding-bottom: 1px">и содержит</td>
            <td width="10%" class="ver-bottom font-size-7" style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px">два</td>
            <td width="40%" style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"></td>
            <td width="39%" class="ver-bottom font-size-7" style="padding-top: 1px;padding-bottom: 1px; padding-left: 6px">порядковых номеров записей</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;margin-left: 0">
        <tr>
            <td width="30%"></td>
            <td width="30%" class="font-size-6 text-center vet-top">(прописью)</td>
            <td></td>
        </tr>
    </table>

    <div style="margin-left: 8.5%">
        <table class="table no-border" style="margin-bottom: 0 !important;margin-top: -5px">
            <tr>
                <td></td>
                <td></td>
                <td style="padding-left: 48px;padding-top: 8px;" class="font-size-7">Масса груза (нетто)</td>
                <td style="border-bottom: 1px solid black;" class="font-size-7"></td>
                <td width="1%"></td>
                <td style="border-bottom: 1px solid black;border-right: 2px solid black;border-top: 2px solid black;border-left: 2px solid black;" class="font-size-7"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                <td></td>
                <td style="border-left: 2px solid black; border-right: 2px solid black"></td>
            </tr>
            <tr>
                <td width="7.5%" class="font-size-7" style="padding: 1px 0 1px 20px">Всего мест</td>
                <td width="23%" style="border-bottom: 1px solid black;padding-bottom: 1px; padding-top: 1px" class="font-size-7">
                                    </td>
                <td width="17%" class="font-size-7" style="padding-left: 47px;padding-top: 1px; padding-bottom: 1px">Масса груза (брутто)</td>
                <td width="27%" style="border-bottom: 1px solid black" class="font-size-7">                </td>
                <td width="1%"></td>
                <td width="24.5%" style="border-left: 2px solid black; border-bottom: 2px solid black; border-right: 2px solid black;" class="font-size-7">                </td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                <td></td>
                <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>



    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="21%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px">Приложение (паспорт, сертификаты и т.п.) на</td>
            <td width="24%" class="font-size-7"></td>
            <td width="7%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px">листах</td>
            <td width="12.5%" style="padding: 0 0 0 10px;border-left: 1px solid black" class="font-size-7">По доверенности №</td>
            <td width="15%" class="font-size-7"></td>
            <td width="2%" class="text-center font-size-7" style="padding-bottom: 1px; padding-top: 1px">от</td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center vet-top m-l font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(прописью)</td>
            <td></td>
            <td style="border-left: 1px solid black"></td>
            <td class="text-center vet-top m-l font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(номер, дата)</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td colspan="6" class="font-size-8-bold" style="padding-bottom: 1px; padding-top: 1px">Всего отпущено на сумму</td>
            <td class="font-size-7" style="border-left: 1px solid black;padding: 0 0 0 10px;">выданной</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="6" class="font-size-8-bold" style="padding-bottom: 1px; padding-top: 1px">двести восемнадцать рублей 00 копеек</td>
            <td style="border-left: 1px solid black;padding: 0 0 0 10px;border-left: 1px solid black"></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">кем, кому (организация, должность, фамилия, и.о.)</td>
        </tr>
        <tr>
            <td colspan="6" class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 0; padding-top: 0">(прописью)</td>
            <td style="border-left: 1px solid black"></td>
            <td style="border-bottom: 1px solid #000000"></td>
        </tr>
        <tr>
            <td width="14%" class="ver-bottom font-size-7" style="padding-bottom: 1px; padding-top: 8px">Отпуск груза разрешил</td>
            <td width="12%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px ">
                                    Директор                            </td>
            <td width="1%"></td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="14%" class="ver-bottom font-size-7" style="padding-bottom: 1px; padding-top: 1px">
                Якубова Е. В.            </td>
            <td width="12.5%" style="border-left: 1px solid black"></td>
            <td ></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(должность)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(подпись)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(расшифровка подписи)</td>
            <td style="border-left: 1px solid black"></td>
            <td  style="border-top: 1px solid black"></td>
        </tr>
    </table>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="27%" class="font-size-7" style="padding-bottom: 1px;padding-top: 1px">Главный (старший) бухгалтер</td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="14%" class="font-size-7" style="padding-bottom: 1px;padding-top: 1px">                    Якубова Е. В.                            </td>
            <td width="12.5%" class="font-size-7" style="border-left: 1px solid black;padding: 0 0 0 10px;">Груз принял</td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
            <td style="border-left: 1px solid black"></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
        </tr>
    </table>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="14%" class="font-size-7" style="padding-bottom: 1px;padding-top: 1px">Отпуск груза произвел</td>
            <td width="12%"></td>
            <td width="1%"></td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="14%"></td>
            <td width="12.5%" style="border-left: 1px solid black;padding: 0 0 0 10px;" class="font-size-6">Груз получил</td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td width="10%"></td>
            <td width="1%"></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
            <td class="vet-top font-size-7" style="margin-top: 0 !important;border-left: 1px solid black;padding: 0 0 0 10px;">грузополучатель</td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
            <td></td>
            <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
        </tr>
        <tr>
            <td class="text-right font-size-7">М.П.</td>
            <td class="text-right font-size-7">"&nbsp;05&nbsp;"</td>
            <td></td>
            <td class="text-center font-size-7" style="border-bottom: 1px solid black">
                апреля            </td>
            <td></td>
            <td class="text-center font-size-7">2017 года</td>
            <td class="text-right font-size-7" style="border-left: 1px solid black">М.П.</td>
            <td class="text-right font-size-7">"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"</td>
            <td></td>
            <td style="border-bottom: 1px solid black"></td>
            <td></td>
            <td class="font-size-7">20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; года</td>
        </tr>
    </table>
</div>