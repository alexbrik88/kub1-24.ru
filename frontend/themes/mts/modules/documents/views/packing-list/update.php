<?php

use common\components\TextHelper;
use common\models\Company;
use common\models\document\OrderPackingList;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\document\Document;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use frontend\modules\documents\components\DocConverter;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
$plus = 0;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = \common\models\document\OrderPackingList::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$precision = $model->invoice->price_precision;

$script = <<< JS

if('$plus' === '1'){
  $('.testclass').hide();
}
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, $this::POS_READY);

$order = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);

$cancelUrl = Url::previous('lastPage');
?>

<div class="form">

    <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-packing-list',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>

    <?php echo $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
        'model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
    ]);
    ?>


    <div class="wrap">

        <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $precision,
        ]); ?>

        <div class="row align-flex-start justify-content-between mt-3">
            <div class="column">
                <span id="plusbtn" class="btn-add-line-table button-regular button-hover-content-red hide">
                    <svg class="svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </span>
            </div>
            <div class="column">
                <table class="total-txt text-right">
                    <tr>
                        <td><strong>Итого:</strong></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Кол-во (масса нетто):</td>
                        <td><strong><?= $order->sum('quantity') * 1 ?></strong></td>
                    </tr>
                    <?php if ($model->invoice->hasNds): ?>
                        <tr>
                            <td>Сумма без учёта НДС:</td>
                            <td><strong><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2) ?></td>
                        </tr>
                        <tr>
                            <td>Сумма НДС:</td>
                            <td><strong><?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2) ?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td>Без налога (НДС):</td>
                            <td><strong>-</strong></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td>Сумма с учётом НДС:</td>
                        <td><strong><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2) ?></strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="wrap wrap_btns check-condition visible mb-0">
        <div class="row align-items-center justify-content-between">
            <div class="column">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                    'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
            </div>
            <div class="column">
                <?= Html::a('Отменить', $cancelUrl ? : ['index', 'type' => $model->type], [
                    'class' => 'button-width button-clr button-regular button-hover-grey',
                ]); ?>
            </div>
        </div>
    </div>

    <?php echo Html::endForm(); ?>
</div>

<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
<?= \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id, 'contractorId' => $contractorId,]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить товарную накладную?',
]); ?>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'title' => '<h4 id="agreement-modal-header">Добавить договор</h4>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#packinglist-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
            '/documents/agreement/create',
            'contractor_id' => $model->invoice->contractor_id,
            'type' => $model->type,
            'returnTo' => 'packing-list',
            'container' => 'agreement-select-container',
        ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            //$(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            //function dateChanged(ev) {
            //    if (ev.bubbles == undefined) {
            //        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            //        if (ev.currentTarget.value == "") {
            //            if ($input.data("last-value") == null) {
            //                $input.data("last-value", ev.currentTarget.defaultValue);
            //            }
            //            var $lastDate = $input.data("last-value");
            //            $input.datepicker("setDate", $lastDate);
            //        } else {
            //            $input.data("last-value", ev.currentTarget.value);
            //        }
            //    }
            //};
        });
        $("#agreement-modal-container").modal("show");
        $("#packinglist-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#packinglist-agreement").val(window.AgreementValue).trigger("change");
    }
})
');
?>
