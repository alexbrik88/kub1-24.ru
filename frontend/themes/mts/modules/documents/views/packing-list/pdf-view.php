<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\document\OrderPackingList;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\PackingList;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
$realMassNet = $model->invoice->getRealMassNet();
$realMassGross = $model->invoice->getRealMassGross();
$company = $model->invoice->company;

$addStamp = (boolean) $model->add_stamp;

if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp || !$company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp || !$company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp || !$company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$precision = $model->invoice->price_precision;
?>
<style>
    @media print {
        thead {display: table-header-group;}
    }
<?php if (Yii::$app->request->get('actionType') == 'print') : ?>
    @page {
        size:A4 landscape;
    }
<?php endif ?>
<?php if ($addStamp) : ?>
    #print_layer {
        height: 230px;
        background-image: url('<?= $printLink ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature1_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '41px' : '35px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature2_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '85px' : '60px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
<?php endif ?>
</style>
<div class="page-content-in p-center-album pad-pdf-p-album">
    <div class="text-right font-size-6 line-h-small">
        <p>Унифицированная форма № ТОРГ-12</p>
        <p>Утверждена постановлением Госкомстата России от 25.12.98 № 132</p>
    </div>
    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td colspan="5">
            </td>
            <td width="6%" class="font-size-7 text-center" style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">Коды</td>
        </tr>
        <tr>
            <td class="font-size-7 text-left ver-bottom" width="75%" colspan="2" rowspan="2" style="border-bottom: 1px solid black;padding: 1px 2px">
                <?php if ($model->consignor): ?>
                    <?= $model->consignor->getRequisitesFull(); ?>
                <?php elseif ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                        $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                        ", {$model->invoice->company_address_legal_full}",
                        ", р/с {$model->invoice->company_rs}",
                        ", в банке {$model->invoice->company_bank_name}",
                        ", БИК {$model->invoice->company_bik}",
                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : ''; ?>
                <?php else: ?>
                    <?= $model->invoice->contractor_name_short,
                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                        $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                <?php endif; ?>
            </td>
            <td colspan="3" class="font-size-8 text-right">Форма по ОКУД</td>
            <td class="font-size-8-bold text-center" style="border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">330212</td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black;"></td>
            <td class="font-size-8 text-right ver-bottom" colspan="2" style="padding: 1px 2px; border-bottom: 1px solid #ffffff">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= $model->consignor ? '' : $model->invoice->company_okpo; ?>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 0">организация-грузоотправитель, адрес, телефон, факс, банковские реквизиты</td>

            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td colspan="3" style="border-bottom: 1px solid black;padding: 1px 0">
                <br/>
            </td>
            <td colspan="2" style="border-bottom: 1px solid #ffffff"></td>
            <td class="font-size-8-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">структурное подразделение</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding-top: 1px; padding-bottom: 1px"></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-8 text-right" style="padding: 1px 2px">Вид деятельности по ОКДП</td>
            <td class="font-size-8-bold text-center" style="padding-bottom:1px;padding-top: 1px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Грузополучатель</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                <?php if ($model->consignee) {
                    if ($model->contractor_address == PackingList::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->consignee->legal_address;
                    } else {
                        $address = $model->consignee->actual_address;
                    }
                    echo $model->consignee->getRequisitesFull($address);
                } elseif ($model->type == Documents::IO_TYPE_OUT) {
                    if ($model->contractor_address == PackingList::CONTRACTOR_ADDRESS_LEGAL) {
                        $address = $model->invoice->contractor->legal_address;
                    } else {
                        $address = $model->invoice->contractor->actual_address;
                    }
                    echo $model->invoice->contractor_name_short,
                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                        $address ? ", {$address}" : '',
                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : '';
                } else {
                    echo $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                        $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                        ", {$model->invoice->company_address_legal_full}",
                        ", р/с {$model->invoice->company_rs}",
                        ", в банке {$model->invoice->company_bank_name}",
                        ", БИК {$model->invoice->company_bik}",
                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';
                } ?>
            </td>
            <td colspan="2" class="font-size-8 text-right" style="padding: 1px 2px;">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"><?= $model->invoice->contractor->okpo; ?></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Поставщик</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                        $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                        ", {$model->invoice->company_address_legal_full}",
                        ", р/с {$model->invoice->company_rs}",
                        ", в банке {$model->invoice->company_bank_name}",
                        ", БИК {$model->invoice->company_bik}",
                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : ''; ?>
                <?php else: ?>
                    <?= $model->invoice->contractor_name_short,
                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                        $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                <?php endif; ?>
            </td>
            <td colspan="2" class="font-size-8 text-right ver-bottom" style="padding: 1px 2px">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;padding: 1px 2px"><?= $model->invoice->company_okpo; ?></td>
        </tr>
        <tr>
            <td colspan="5" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Плательщик</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                    <?= $model->invoice->contractor_name_short,
                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                        $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '',
                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : ''; ?>
                <?php else: ?>
                    <?= $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                        $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                        ", {$model->invoice->company_address_legal_full}",
                        ", р/с {$model->invoice->company_rs}",
                        ", в банке {$model->invoice->company_bank_name}",
                        ", БИК {$model->invoice->company_bik}",
                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : ''; ?>
                <?php endif; ?>
            </td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="border-bottom: 1px solid black;padding: 1px 2px">по ОКПО</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"><?= $model->invoice->contractor->okpo; ?></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">организация, адрес, телефон, факс, банковские реквизиты</td>
            <td width="1%"></td>
            <td width="4.8%" style="border-left: 1px solid black"></td>
            <td style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;"></td>
        </tr>
        <tr>
            <td width="12%" class="text-right ver-bottom m-l font-size-8">Основание</td>
            <td class="font-size-7" colspan="2" style="border-bottom: 1px solid black">
                <?= $model->basis_name ?>
            </td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="border-bottom: 1px solid black; border-left: 1px solid black;">номер</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= $model->basis_document_number; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-6 text-center ver-top m-l" style="padding: 1px 2px">договор, заказ-наряд</td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">дата</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;">
                <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-8 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px"> Транспортная накладная</td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">номер</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"><?= $model->waybill_number; ?></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-8 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px"></td>
            <td></td>
            <td class="font-size-8 text-right ver-bottom" style="padding: 1px 2px;border-bottom: 1px solid black; border-left: 1px solid black;">дата</td>
            <td class="font-size-8-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"><?= DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
        </tr>
        <tr>
            <td colspan="3" class="font-size-9 text-right ver-bottom no-border-top" style="padding-top: 1px; padding-bottom: 1px">Вид</td>
            <td colspan="2" class="font-size-9 ver-bottom" style="padding: 1px 2px;">операции</td>
            <td class="font-size-9-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
        </tr>
    </table>

    <div style="margin-top: -2rem">
        <table class="table no-border" style="margin-right: 15%;margin-left: 8%;margin-bottom: 0">
            <tr>
                <td width=""></td>
                <td width="18%" class="font-size-7 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-left: 1px solid black;border-top: 1px solid black;">Номер документа</td>
                <td width="18%" class="font-size-7 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-top: 1px solid black;">Дата составления</td>
                <td></td>
            </tr>
            <tr>
                <td width="23%" class="font-size-8-bold text-center">ТОВАРНАЯ НАКЛАДНАЯ</td>
                <td class="font-size-7 text-center" style="padding: 1px 2px;border: 2px solid black;"><?= $model->fullNumber; ?></td>
                <td class="font-size-7 text-center" style="padding: 1px 2px;border-top: 2px solid black;border-right: 2px solid black;border-bottom: 2px solid black;"><?= $dateFormatted; ?></td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="page-number" style="padding: 0">
        Страница №1
    </div>

    <table class="table font-size-7" style="margin-bottom: 0 !important;">
        <tr>
            <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Но-мер по по-рядку</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Товар</td>
            <td width="10%" colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Единица измерения</td>
            <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Вид упаковки</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Количество</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Масса брутто</td>
            <td width="7%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Количество (масса нетто)</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Цена, руб. коп.</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма без учета НДС, руб. коп.</td>
            <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">НДС</td>
            <td rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма с учётом НДС, руб. коп.</td>
        </tr>
        <tr>
            <td width="22%" class="text-center font-size-7 font-size-7" style="padding: 1px 2px">наименование, характеристика, сорт, артикул товара</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">код</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">наименование</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">код по ОКЕИ</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">в одном месте</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">мест, штук</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">ставка, %</td>
            <td class="text-center font-size-7" style="padding: 1px 2px">сумма, руб. коп.</td>
        </tr>
        <tr>
            <td class="text-center  font-size-6">1</td>
            <td class="text-center  font-size-6">2</td>
            <td class="text-center  font-size-6">3</td>
            <td class="text-center  font-size-6">4</td>
            <td class="text-center  font-size-6">5</td>
            <td class="text-center  font-size-6">6</td>
            <td class="text-center  font-size-6">7</td>
            <td class="text-center  font-size-6">8</td>
            <td class="text-center  font-size-6">9</td>
            <td class="text-center  font-size-6">10</td>
            <td class="text-center  font-size-6">11</td>
            <td class="text-center  font-size-6">12</td>
            <td class="text-center  font-size-6">13</td>
            <td class="text-center  font-size-6">14</td>
            <td class="text-center  font-size-6">15</td>
        </tr>
        <?php
        $orderQuery = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order):
            if ($model->invoice->hasNds) {
                $priceNoNds = $order->priceNoNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id);
                $TaxRateName = $order->order->saleTaxRate->name;
                $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
            } else {
                $priceNoNds = $order->priceWithNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id);
                $TaxRateName = 'Без НДС';
                $ndsAmount = Product::DEFAULT_VALUE;
            }
            if ($order->quantity != intval($order->quantity)) {
                $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
            }
            ?>
            <tr>
                <td class="text-center font-size-7" style="padding: 1px 2px" style="padding: 1px 2px"><?= $key + 1; ?></td>
                <td class="font-size-7"><?= $order->order->product_title; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->article; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->unit ? $order->order->unit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->box_type; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->count_in_place; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->place_count; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->order->mass_gross; ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $order->quantity; ?></td>
                <td class="text-right font-size-7" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
                <td class="text-right font-size-7" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
                <td class="text-center font-size-7" style="padding: 1px 2px"><?= $TaxRateName; ?></td>
                <td class="text-right font-size-7" style="padding: 1px 2px">
                    <?= $ndsAmount; ?>
                </td>
                <td class="text-right font-size-7" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
            </tr>
        <?php endforeach; ?>
        <?php $totalQuantity = $orderQuery->sum('quantity') * 1;
        if ($totalQuantity != intval($totalQuantity)) {
            $totalQuantity = rtrim(number_format($totalQuantity, 10, '.', ''), 0);
        } ?>
        <tr>
            <td class="text-right font-size-7" colspan="7" style="border: none !important; padding: 1px 2px">Итого</td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $model->invoice->total_place_count; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $model->invoice->total_mass_gross; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $totalQuantity; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                <?= TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2); ?>
            </td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                <?php if ($model->invoice->hasNds) : ?>
                    <?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?>
                <?php endif; ?>
            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
        </tr>
        <tr>
            <td class="text-right font-size-7" colspan="7" style="border: none !important;padding: 1px 2px">Всего по накладной</td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $model->invoice->total_place_count; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $model->invoice->total_mass_gross; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px"><?= $totalQuantity; ?></td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                <?= TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2); ?>
            </td>
            <td class="text-center font-size-7" style="padding: 1px 2px">X</td>
            <td class="text-right font-size-7" style="padding: 1px 2px">
                <?php if ($model->invoice->hasNds) : ?>
                    <?= TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?>
                <?php endif; ?>
            </td>
            <td class="text-right font-size-7" style="padding: 1px 2px"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;margin-left: 10%">
        <tr>
            <td colspan="2" class="ver-bottom text-left font-size-7" style="padding-top: 1px;padding-bottom: 1px">Товарная накладная имеет приложение на</td>
            <td style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"></td>
            <td class="ver-bottom text-left font-size-7" style="padding-left: 6px">листах</td>
        </tr>
        <tr>
            <td width="9%" class="ver-bottom text-left font-size-7" style="padding-top: 1px;padding-bottom: 1px">и содержит</td>
            <td width="10%" class="ver-bottom font-size-7" style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"><?= RUtils::numeral()->getInWords(count($orderArray)); ?></td>
            <td width="40%" style="border-bottom: 1px solid black;padding-top: 1px;padding-bottom: 1px"></td>
            <td width="39%" class="ver-bottom font-size-7" style="padding-top: 1px;padding-bottom: 1px; padding-left: 6px">порядковых номеров записей</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;margin-left: 0">
        <tr>
            <td width="30%"></td>
            <td width="30%" class="font-size-6 text-center vet-top">(прописью)</td>
            <td></td>
        </tr>
    </table>

    <div style="margin-left: 8.5%">
    <table class="table no-border" style="margin-bottom: 0 !important;margin-top: -5px">
        <tr>
            <td></td>
            <td></td>
            <td style="padding-left: 48px;padding-top: 8px;" class="font-size-7">Масса груза (нетто)</td>
            <td style="border-bottom: 1px solid black;" class="font-size-7"><?php if ($realMassNet > 0) {
                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassNet))  . ' КГ';
                } ?></td>
            <td width="1%"></td>
            <td style="border-bottom: 1px solid black;border-right: 2px solid black;border-top: 2px solid black;border-left: 2px solid black;" class="font-size-7"><?php if ($realMassNet > 0) {
                    echo TextHelper::moneyFormat($realMassNet, 2) . ' КГ';
                } ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
            <td></td>
            <td style="border-left: 2px solid black; border-right: 2px solid black"></td>
        </tr>
        <tr>
            <td width="7.5%" class="font-size-7" style="padding: 1px 0 1px 20px">Всего мест</td>
            <td width="23%" style="border-bottom: 1px solid black;padding-bottom: 1px; padding-top: 1px" class="font-size-7">
                <?php if ($model->invoice->total_place_count > 0) {
                    echo iconv("UTF-8", "UTF-8//IGNORE", TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($model->invoice->total_place_count, RUtils::NEUTER)));
                } ?>
            </td>
            <td width="17%" class="font-size-7" style="padding-left: 47px;padding-top: 1px; padding-bottom: 1px">Масса груза (брутто)</td>
            <td width="27%" style="border-bottom: 1px solid black" class="font-size-7"><?php if ($realMassGross > 0) {
                    echo TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross)) . ' КГ';
                } ?>
            </td>
            <td width="1%"></td>
            <td width="24.5%" style="border-left: 2px solid black; border-bottom: 2px solid black; border-right: 2px solid black;" class="font-size-7"><?php if ($realMassGross > 0) {
                    echo TextHelper::moneyFormat($realMassGross, 2) . ' КГ';
                } ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
            <td></td>
            <td class="font-size-6 text-center vet-top m-l" style="padding-bottom: 1px; padding-top: 1px">(прописью)</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    </div>



    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="21%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px">Приложение (паспорт, сертификаты и т.п.) на</td>
            <td width="24%" class="font-size-7"></td>
            <td width="7%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px">листах</td>
            <td width="12.5%" style="padding: 0 0 0 10px;border-left: 1px solid black" class="font-size-7">По доверенности №</td>
            <td class="font-size-7">
                <?= ($model->proxy_number && $model->proxy_date) ?
                    $model->proxy_number . ' от ' . Yii::$app->formatter->asDate($model->proxy_date, 'long') : '---'; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center vet-top m-l font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(прописью)</td>
            <td></td>
            <td style="border-left: 1px solid black"></td>
            <td class="text-center vet-top m-l font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(номер, дата)</td>
        </tr>
    </table>
    <div id="print_layer">
        <div id="signature1_layer">
            <div id="signature2_layer">
                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td colspan="6" class="font-size-8-bold" style="padding-bottom: 1px; padding-top: 1px">Всего отпущено на сумму</td>
                        <td class="font-size-7" style="border-left: 1px solid black;padding: 0 0 0 10px;">выданной</td>
                        <td class="font-size-7">
                            <?= $model->consignee ? $model->consignee->getTitle(true) : $model->invoice->contractor_name_short,
                            $model->given_out_position ? ", {$model->given_out_position}" : null,
                            $model->given_out_fio ? ", {$model->given_out_fio}" : null; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="font-size-8-bold" style="padding-bottom: 1px; padding-top: 1px"><?= \common\components\TextHelper::amountToWords($model->getPrintAmountWithNds() / 100); ?></td>
                        <td style="border-left: 1px solid black;padding: 0 0 0 10px;border-left: 1px solid black"></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">кем, кому (организация, должность, фамилия, и.о.)</td>
                    </tr>
                    <tr>
                        <td colspan="6" class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 0; padding-top: 0">(прописью)</td>
                        <td style="border-left: 1px solid black"></td>
                        <td style="border-bottom: 1px solid #000000"></td>
                    </tr>
                    <tr>
                        <td width="14%" class="ver-bottom font-size-7" style="padding-bottom: 1px; padding-top: 8px">Отпуск груза разрешил</td>
                        <td width="12%" class="font-size-7" style="padding-bottom: 1px; padding-top: 1px ">
                            <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                                <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                                    <?= ($model->invoice->signEmployeeCompany) ? $model->invoice->signEmployeeCompany->position : $model->invoice->company_chief_post_name ?>
                                <?php else: ?>
                                    <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                                        <?=$model->invoice->contractor->director_name?>
                                    <?php else: ?>
                                        Главный бухгалтер
                                    <?php endif ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%" class="ver-bottom font-size-7 text-center" style="padding-bottom: 1px; padding-top: 1px">
                            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            <?php else: ?>
                                <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                                    <?=$model->invoice->contractor->director_name?>
                                <?php else: ?>
                                    <?=$model->invoice->contractor->chief_accountant_name?>
                                <?php endif ?>
                            <?php endif; ?>
                        </td>
                        <td width="12.5%" style="border-left: 1px solid black"></td>
                        <td ></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(должность)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(подпись)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px; padding-top: 1px">(расшифровка подписи)</td>
                        <td style="border-left: 1px solid black"></td>
                        <td  style="border-top: 1px solid black"></td>
                    </tr>
                </table>
                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td width="27%" class="font-size-7" style="padding-bottom: 1px;padding-top: 1px">Главный (старший) бухгалтер</td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%" class="font-size-7 text-center" style="padding-bottom: 1px;padding-top: 1px">
                            <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                                <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                <?php else: ?>
                                    <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                                        <?= $model->invoice->contractor->director_name; ?>
                                    <?php else: ?>
                                        <?= $model->invoice->contractor->chief_accountant_name; ?>
                                    <?php endif ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td width="12.5%" class="font-size-7" style="border-left: 1px solid black;padding: 0 0 0 10px;">Груз принял</td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
                        <td style="border-left: 1px solid black"></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
                    </tr>
                </table>
                <table class="table no-border" style="margin-bottom: 0 !important;">
                    <tr>
                        <td width="14%" class="font-size-7" style="padding-bottom: 1px;padding-top: 1px">Отпуск груза произвел</td>
                        <td width="12%"></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="14%"></td>
                        <td width="12.5%" style="border-left: 1px solid black;padding: 0 0 0 10px;" class="font-size-6">Груз получил</td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td width="10%"></td>
                        <td width="1%"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
                        <td class="vet-top font-size-7" style="margin-top: 0 !important;border-left: 1px solid black;padding: 0 0 0 10px;">грузополучатель</td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(должность)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(подпись)</td>
                        <td></td>
                        <td class="text-center vet-top font-size-6" style="border-top: 1px solid black;padding-bottom: 1px;padding-top: 1px">(расшифровка подписи)</td>
                    </tr>
                    <tr>
                        <td class="text-right font-size-7">М.П.</td>
                        <td class="text-right font-size-7">"&nbsp;<?= DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE); ?>&nbsp;"</td>
                        <td></td>
                        <td class="text-center font-size-7" style="border-bottom: 1px solid black">
                            <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                'date' => $model->document_date,
                                'format' => 'F', // month in word
                                'monthInflected' => true,
                            ]);; ?>
                        </td>
                        <td></td>
                        <td class="text-center font-size-7"><?= DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE); ?> года</td>
                        <td class="text-right font-size-7" style="border-left: 1px solid black">М.П.</td>
                        <td class="text-right font-size-7">"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"</td>
                        <td></td>
                        <td style="border-bottom: 1px solid black"></td>
                        <td></td>
                        <td class="font-size-7">20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; года</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>








