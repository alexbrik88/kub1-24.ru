<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use frontend\modules\documents\components\DocConverter;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
$plus = 0;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = \common\models\document\OrderPackingList::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$precision = $model->invoice->price_precision;

$script = <<< JS

if('$plus' === '1'){
  $('.testclass').hide();
}
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, $this::POS_READY);
?>

    <div class="page-content-in">

        <?php if ($backUrl !== null) {
            echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
                'class' => 'back-to-customers',
            ]);
        } ?>
        <?php
        echo Html::beginForm('', 'post', [
            'id' => 'edit-packing-list',
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate',
            'enctype' => 'multipart/form-data',
        ]); ?>
        <div class="row">
            <div
                class="customer_info_wrapper col-lg-7 col-w-lg-8 col-md-7 widthe-i_md" style="max-width: 720px;">
                <?php echo $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'message' => $message,
                    'dateFormatted' => $dateFormatted,
                ]);
                ?>
            </div>

            <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'useContractor' => $useContractor,
                'contractorId' => $contractorId,
            ]); ?>
        </div>
        <div class="portlet wide_table overflow-x">
            <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'precision' => $precision,
            ]); ?>
        </div>
            <span class="input-editable-field hide btn yellow btn-add-line-table" id="plusbtn">
                <i class="fa icon fa-plus-circle fa-2x" style="margin: 0; padding: 0;"></i>
            </span>


        <?php echo Html::endForm(); ?>
        <div class="portlet pull-right">
            <?= $this->render('_viewPartials/_summary_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
            ]); ?>
        </div>
    </div>

<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
    'contractorId' => $contractorId,
]); ?>
<?= \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id, 'contractorId' => $contractorId,]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить товарную накладную?',
]); ?>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h4 id="agreement-modal-header">Добавить договор</h4>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#packinglist-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
            '/contractor/agreement-create',
            'id' => $model->invoice->contractor_id,
            'container' => 'agreement-select-container',
        ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            //$(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);
            //
            //function dateChanged(ev) {
            //    if (ev.bubbles == undefined) {
            //        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            //        if (ev.currentTarget.value == "") {
            //            if ($input.data("last-value") == null) {
            //                $input.data("last-value", ev.currentTarget.defaultValue);
            //            }
            //            var $lastDate = $input.data("last-value");
            //            $input.datepicker("setDate", $lastDate);
            //        } else {
            //            $input.data("last-value", ev.currentTarget.value);
            //        }
            //    }
            //};
        });
        $("#agreement-modal-container").modal("show");
        $("#packinglist-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#packinglist-agreement").val(window.AgreementValue).trigger("change");
    }
})
');
?>
