<?php

use common\components\date\DateHelper;
use common\models\document\status\OrderDocumentStatus;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $orderDocumentStatus OrderDocumentStatus */

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => Documents::IO_TYPE_OUT]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);
$statusItems = [];
foreach (OrderDocumentStatus::find()->all() as $orderDocumentStatus) {
    $statusItems[] = [
        'label' => $orderDocumentStatus->name,
        'url' => '#',
        'linkOptions' => [
            'data-url' => Url::to(['many-update-status', 'statusID' => $orderDocumentStatus->id]),
        ],
    ];
}
$createItems = [
    [
        'label' => 'Счет',
        'url' => '#many-create-invoice',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ],
    [
        'label' => 'Акт',
        'url' => '#many-create-act',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ],
    [
        'label' => 'ТН',
        'url' => '#many-create-packing-list',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ],
    [
        'label' => 'УПД',
        'url' => '#many-create-upd',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ],
];
?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a(Icon::get('print').' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => Documents::IO_TYPE_OUT,
            'multiple' => '',
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a(Icon::get('envelope').' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send']),
        ]) : null,
        $canDelete ? Html::a(Icon::get('garbage').' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $canIndex || $canCreate ? Html::tag('div', Html::button('<span>Статус</span>  '.Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 100px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $statusItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), ['class' => 'dropup']) : null,
        $canIndex || $canCreate ? Html::tag('div', Html::button('<span>Создать</span>  '.Icon::get('shevron'), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 100px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $createItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<?php if ($canCreate) : ?>
    <?php Modal::begin([
        'id' => 'many-create-invoice',
        'title' => 'Создать счета',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>

        <div class="form-group">
            <label class="label">
                Счета создать от даты:
            </label>

            <div class="row">
                <div class="col-3">
                    <?= Html::textInput('Invoice[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Создать', [
                'class' => 'button-regular button-regular_red width-130 modal-many-create-invoice',
                'data-url' => Url::to(['/documents/invoice/many-create', 'type' => Documents::IO_TYPE_OUT]),
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-hover-transparent width-130',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => 'many-create-act',
        'title' => 'Создать акты',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>

        <div class="form-group">
            <label class="label">
                Акты создать от даты:
            </label>

            <div class="row">
                <div class="col-3">
                    <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Создать', [
                'class' => 'button-regular button-regular_red width-130 modal-many-create-act',
                'data-url' => Url::to(['/documents/act/many-create', 'type' => Documents::IO_TYPE_OUT]),
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-hover-transparent width-130',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => 'many-create-packing-list',
        'title' => 'Создать Товарные Накладные',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>

        <div class="form-group">
            <label class="label">
                Товарные Накладные создать от даты:
            </label>

            <div class="row">
                <div class="col-3">
                    <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Создать', [
                'class' => 'button-regular button-regular_red width-130 modal-many-create-act',
                'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => Documents::IO_TYPE_OUT]),
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-hover-transparent width-130',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Modal::end(); ?>

    <?php Modal::begin([
        'id' => 'many-create-upd',
        'title' => 'Создать УПД',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>

        <div class="form-group">
            <label class="label">
                УПД создать от даты:
            </label>

            <div class="row">
                <div class="col-3">
                    <?= Html::textInput('documentDate', date(DateHelper::FORMAT_USER_DATE), [
                        'class' => 'form-control date-picker ico modal-document-date',
                        'data-date-viewmode' => 'years',
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::button('Создать', [
                'class' => 'button-regular button-regular_red width-130 modal-many-create-act',
                'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => Documents::IO_TYPE_OUT]),
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-regular button-hover-transparent width-130',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Modal::end(); ?>
<?php endif ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'title' => 'Вы уверены, что хотите удалить выбранные заказы?',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>

        <div class="text-center">
            <?= Html::a('Да', null, [
                'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 modal-many-delete',
                'data-url' => Url::to(['many-delete']),
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Modal::end(); ?>
<?php endif ?>
