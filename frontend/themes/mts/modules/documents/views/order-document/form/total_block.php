<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.02.2018
 * Time: 17:46
 */

use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

$sum = $model->total_amount;
$discount = $model->getDiscountSum();
?>

<table class="total-txt table-resume">
    <tr class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <td>
            Сумма скидки:
        </td>
        <td>
            <strong id="discount_sum">
                <?= TextHelper::moneyFormatFromIntToFloat($discount); ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>
            Итого:
        </td>
        <td>
            <strong id="total_price">
                <?= TextHelper::moneyFormatFromIntToFloat($sum); ?>
            </strong>
        </td>
    </tr>
    <tr class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>" role="row">
        <td>
            Вес (кг):
        </td>
        <td>
            <strong id="total_mass">
                <?= $model->getTotalWeight(); ?>
            </strong>
        </td>
    </tr>
</table>