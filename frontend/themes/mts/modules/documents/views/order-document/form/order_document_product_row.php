<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 11:32
 */

use common\components\TextHelper;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\Html;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderDocument;
use common\models\Company;
use common\models\employee\Employee;

/* @var OrderDocumentProduct $orderDocumentProduct */
/* @var integer $number */
/* @var OrderDocument $model */
/* @var boolean $ndsCellClass */
/* @var $company Company */
/* @var $user Employee */

$baseName = 'orderArray[' . $number . ']';
$id = $orderDocumentProduct->isNewRecord ? $orderDocumentProduct->product_id : $orderDocumentProduct->id;
$prodType = $orderDocumentProduct->productionType;
if ($company->hasNds() && $company->isNdsExclude) {
    $price = $orderDocumentProduct->base_price_no_vat;
    $priceOne = $orderDocumentProduct->selling_price_no_vat;
    $amount = $orderDocumentProduct->amount_sales_no_vat;
} else {
    $price = $orderDocumentProduct->base_price_with_vat;
    $priceOne = $orderDocumentProduct->selling_price_with_vat;
    $amount = $orderDocumentProduct->amount_sales_with_vat;
}
?>

<tr id="model_<?= $orderDocumentProduct->product_id; ?>" class="product-row" role="row">
    <td class="product-delete">
        <button class="remove-product-from-order-document button-clr" type="button" data-id="<?= $orderDocumentProduct->product_id; ?>">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use>
            </svg>
        </button>
    </td>
    <td style="position: relative;">
        <input type="text" class="product-title form-control tooltip-product"
               style="padding-right: 25px; width: 100%;"
               name="<?= $baseName; ?>[title]"
               data-value="<?= $orderDocumentProduct->productTitle; ?>"
               value="<?= $orderDocumentProduct->productTitle; ?>">
        <span class="product-title-clear">×</span>
    </td>
    <td class="product-article col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>">
        <?= $orderDocumentProduct->product->article ? $orderDocumentProduct->product->article : '---'; ?>
    </td>
    <td>
        <input type="hidden" class="tax-rate" disabled="disabled"
               value="<?= $orderDocumentProduct->saleTaxRate->rate; ?>"/>

        <input type="hidden" class="product-model"
               name="<?= $baseName; ?>[model]"
               value="<?= $orderDocumentProduct->isNewRecord ? Product::tableName() : $orderDocumentProduct->tableName(); ?>"/>
        <input type="hidden" class="product-id"
               name="<?= $baseName; ?>[id]" value="<?= $id; ?>"/>

        <?php if (!$orderDocumentProduct->unit || $orderDocumentProduct->unit->name == Product::DEFAULT_VALUE) : ?>
            <?= Html::hiddenInput($baseName . '[count]', 1, [
                'class' => 'product-count',
                'data-value' => 1,
            ]) ?>
            <span><?= Product::DEFAULT_VALUE ?></span>
        <?php else : ?>
            <input type="number" min="0" step="any" name="<?= $baseName; ?>[count]"
                   value="<?= $orderDocumentProduct->quantity; ?>"
                   data-value="<?= $orderDocumentProduct->quantity ?>"
                   class="form-control-number product-count width-100"/>
        <?php endif ?>
    </td>
    <td class="product-unit-name"><?= $orderDocumentProduct->unit ? $orderDocumentProduct->unit->name : Product::DEFAULT_VALUE; ?></td>
    <td class="product-available"><?= $orderDocumentProduct->productAvailable; ?></td>
    <td class="product-reserve col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>">
        <?= $orderDocumentProduct->productReserve; ?>
    </td>
    <td class="product-quantity col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>">
        <?= $orderDocumentProduct->product->totalQuantity ? $orderDocumentProduct->product->totalQuantity : '---'; ?>
    </td>
    <td class="product-weight col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>">
        <?= $orderDocumentProduct->product->weight ? $orderDocumentProduct->product->weight : '---'; ?>
    </td>
    <td class="product-volume col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>">
        <?= $orderDocumentProduct->product->volume ? $orderDocumentProduct->product->volume : '---'; ?>
    </td>
    <td class="price-for-sell-nds-name <?= $ndsCellClass; ?>">
        <?= $orderDocumentProduct->saleTaxRate->name; ?>
    </td>
    <td class="price-one">
        <?= Html::input('number', $baseName . '[price]', TextHelper::moneyFormatFromIntToFloat($price), [
            'class' => 'form-control-number price-input',
            'data-value' => TextHelper::moneyFormatFromIntToFloat($price),
            'min' => 0,
            'step' => 'any',
        ]); ?>
    </td>
    <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <?= Html::input('number', $baseName . '[discount]', $orderDocumentProduct->discount, [
            'class' => 'form-control-number discount-input',
            'data-value' => $orderDocumentProduct->discount,
            'min' => 0,
            'max' => 99.9999,
            'step' => 'any',
        ]); ?>
    </td>
    <td class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <span class="price-one-with-nds"><?= TextHelper::moneyFormatFromIntToFloat($priceOne) ?></span>
        <?= Html::hiddenInput($baseName . '[priceOneWithVat]', TextHelper::moneyFormatFromIntToFloat($priceOne), [
            'class' => 'form-control price-one-with-nds-input',
            'data-price' => $priceOne,
            'min' => 0,
        ]); ?>
    </td>
    <td class="price-with-nds" style="text-align: right;">
        <?= TextHelper::moneyFormatFromIntToFloat($amount); ?>
    </td>
</tr>