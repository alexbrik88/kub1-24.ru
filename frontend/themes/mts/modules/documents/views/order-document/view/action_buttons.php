<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.02.2018
 * Time: 17:03
 */

use common\models\document\OrderDocument;
use common\models\document\status\OrderDocumentStatus;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\bootstrap4\Dropdown;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model OrderDocument
 */

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);
?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate && !$model->is_deleted): ?>
                <?= Html::button(Icon::get('envelope').'<span>Отправить</span>', [
                    'class' => 'button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice',
                    'title' => 'Отправить по e-mail',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT, 'filename' => $model->getPrintTitle(),];
            echo Html::a(Icon::get('print').'<span>Печать</span>', $printUrl, [
                'target' => '_blank',
                'class' => 'button-regular button-hover-transparent w-full',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <span class="dropup">
                <?= Html::a(Icon::get('download').'<span>Скачать</span>', '#', [
                    'class' => 'button-regular button-hover-transparent w-full dropdown-toggle',
                    'data-toggle' => 'dropdown'
                ]); ?>
                <?= Dropdown::widget([
                    'options' => [
                        'style' => '',
                        'class' => 'form-filter-list list-clr'
                    ],
                    'items' => [
                        [
                            'label' => '<span style="display: inline-block;">PDF</span> файл',
                            'encode' => false,
                            'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT, 'filename' => $model->getPdfFileName()],
                            'linkOptions' => [
                                'target' => '_blank',
                            ]
                        ],
                        [
                            'label' => '<span style="display: inline-block;">Word</span> файл',
                            'encode' => false,
                            'url' => ['docx', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT,],
                            'linkOptions' => [
                                'target' => '_blank',
                                'class' => 'get-word-link',
                            ]
                        ],
                    ],
                ]); ?>
            </span>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => Icon::get('copied').'<span>Копировать</span>',
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите скопировать этот заказ?',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && $canUpdate): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => Icon::get('close').'<span>Отменён</span>',
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['update-status', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите отклонить этот заказ?',
                    'confirmParams' => [
                        'status_id' => OrderDocumentStatus::STATUS_CANCELED,
                    ],
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
                <?= Html::button(Icon::get('garbage').'<span>Удалить</span>', [
                    'class' => 'button-regular button-hover-transparent w-full',
                    'data-toggle' => 'modal',
                    'href' => '#delete-confirm',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if ($canUpdate) : ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => false,
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот заказ?",
    ]);
}; ?>
