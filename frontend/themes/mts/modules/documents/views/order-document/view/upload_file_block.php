<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.02.2018
 * Time: 16:19
 */

use yii\bootstrap4\Html;
use common\models\document\OrderDocument;
use common\components\date\DateHelper;
use common\models\file\widgets\FileUpload;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\rbac\permissions;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
?>

<div class="about-card mb-3">
    <div class="about-card-item">
        <span class="text-grey">
            Покупатель:
        </span>
        <?= \yii\helpers\Html::a($model->contractor_name_short, [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Отгрузить до:
        </span>
        <?= DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
    </div>
    <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
    <div class="about-card-item">
        <span class="text-grey">
            <?= $model->agreementType ? $model->agreementType->name : 'Договор'; ?>:
        </span>
        № <?= Html::encode($model->basis_document_number); ?>
        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
    </div>
    <?php endif ?>
    <div class="about-card-item">
        <?= \frontend\themes\mts\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span style="font-weight: bold;">Комментарий</span>
        <?= \yii\helpers\Html::tag('span', $this->render('//svg-sprite', ['ico' => 'pencil']), [
            'id' => 'comment_internal_update',
            'style' => 'cursor: pointer;',
        ]); ?>
        <div id="comment_internal_view" class="">
            <?= Html::encode($model->comment_internal) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative;',
                'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
            ]) ?>
            <?= Html::tag('i', '', [
                'id' => 'comment_internal_save',
                'class' => 'fa fa-floppy-o',
                'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
            ]); ?>
            <?= Html::textarea('comment_internal', $model->comment_internal, [
                'id' => 'comment_internal_input',
                'rows' => 3,
                'maxlength' => true,
                'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
            ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>


<?php if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });
    ');
}; ?>
