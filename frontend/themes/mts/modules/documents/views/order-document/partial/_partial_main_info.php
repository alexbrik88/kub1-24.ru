<?php

use common\components\helpers\Html;
use \frontend\models\Documents;
use kartik\select2\Select2;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */


$inputListConfig = [
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control'
    ]
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];

?>
<?= Html::activeHiddenInput($model, 'type'); ?>
<?= Html::hiddenInput(null, $model->isNewRecord, [
    'id' => 'contractor-is_new_record',
]); ?>

<div class="row">
    <?= $form->field($model, 'name', $textInputConfig)->label('Название контрагента')->textInput([
        'placeholder' => 'Без ООО/ИП',
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
        ],
    ]); ?>

    <?= $form->field($model, 'companyTypeId', array_merge($textInputConfig, [
        'options' => [
            'id' => 'contractor_company_type',
            'class' => 'form-group legal col-6 required',
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
    ]))->label('Форма')->widget(Select2::class, [
        'data' => $model->getTypeArray(),
        'disabled' => !$model->isNewRecord,
        'pluginOptions' => [
            'width' => '100%'
        ]
    ]); ?>

    <?= $form->field($model, 'PPC', array_merge($textInputConfig, [
        'options' => [
            'class' => 'form-group col-6 field-contractor-ppc required',
        ],
    ]))->label('КПП:')->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'director_email', $textInputConfig)
        ->label('Email покупателя (для отправки счетов)</span>')
        ->textInput([
            'maxlength' => true,
        ]); ?>
</div>

<?php $this->registerJs(<<<JS
    var contractorIsNewRecord = $("#contractor-is_new_record").val();
    var companyType = {
        'ИП' : 1,
        'ООО' : 2,
        'ЗАО' : 3,
        'ПАО' : 4,
        'АО' : 5,
        'АНО' : 6,
        'ФГУП' : 7,
        'ОАО' : $('#contractor-itn').data('prod') == 1 ? 10 : 8
    };
    $('[id="contractor-itn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        beforeRender: function (container) {
            $(".field-contractor-itn p.exists-contractor").empty();
            if (contractorIsNewRecord) {
                $.post("/contractor/check-availability", {
                    inn: this.value,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
        },
        onSelect: function(suggestion) {
            var companyTypeId = '-1';

            if (contractorIsNewRecord) {
                $.post("/contractor/check-availability", {
                    inn: suggestion.data.inn,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId).trigger('change');
            if ($('#contractor-companytypeid').val() == companyType['ИП']) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
            } else {
                $('.field-contractor-ppc').show();
            }
            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
JS
);
