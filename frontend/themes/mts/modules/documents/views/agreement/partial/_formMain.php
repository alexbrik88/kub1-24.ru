<?php

use common\models\Agreement;
use common\models\AgreementTemplate;
use kartik\select2\Select2;
use yii\helpers\Html;
use common\components\date\DateHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\InvoiceStatus;
use common\models\NdsOsno;
use common\models\product\Product;
use common\models\TaxationType;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;
use common\models\AgreementType;

/* @var $form yii\bootstrap4\ActiveForm */
/* @var $this yii\web\View */
/* @var $model \common\models\Agreement */

$textAreaStyle = 'width: 100%; font-size:12px; text-align:justify; white-space: pre-line;';

$companyRs = [];
$rsData = [];
$allTemplates = [];
$contractorArray = [];

if (Yii::$app->user->identity !== null
    && method_exists(Yii::$app->user->identity, 'hasProperty')
    && Yii::$app->user->identity->hasProperty('company')) {

    $company = Yii::$app->user->identity->company;
    $company_id = $company->id;
} else {
    $company = $company_id = null;
}

$isNewRecord = $model->isNewRecord;

if ($company_id) {

    $companyRs = Company::findOne($company_id)->getCheckingAccountants()
        ->select(['bank_name', 'rs', 'id'])
        ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
        ->orderBy(['type' => SORT_ASC])
        ->indexBy('rs')
        ->asArray()->all();


    foreach ($companyRs as $rs) {
        $rsData[$rs['rs']] = $rs['bank_name'];
    }
    //$rsData["add-modal-rs"] = '[ + Добавить расчетный счет ]';

    $contractorType = Yii::$app->request->get('type', $model->type);
    $contractorDropDownConfig = [
        'class' => 'form-control contractor-select',
        //'disabled' => $fixedContractor,
    ];
    $contractorArray = Contractor::getALLContractorList($contractorType, false);
}

// get templates by type
$agreementType = Yii::$app->request->get('type', null);
$searchTemplatesCondition = ['company_id' => $company_id];
if ($agreementType)
    $searchTemplatesCondition['type'] = (int)$agreementType;

$templates = AgreementTemplate::find()->where($searchTemplatesCondition)->asArray()->all();


foreach ($templates as $t) {
    $allTemplates[$t['id']] = 'Шаблон №' . $t['document_number'] . ' от ' .
        DateHelper::format($t['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
}
?>

<style>
    <?php if ($model->document_type_id == AgreementType::TYPE_AGREEMENT) : ?>
        .agreement-template-form .show_agreement {display: inherit}
        .agreement-template-form .show_not_agreement {display: none}
    <?php else : ?>
        .agreement-template-form .show_agreement {display: none}
        .agreement-template-form .show_not_agreement {display: inherit}
    <?php endif; ?>
</style>

<?= $form->errorSummary($model); ?>
<?= $form->errorSummary($model->essence); ?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Договор №
                        </span>
                    </span>
                </div>
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                        'placeholder' => 'доп. номер',
                    ]); ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date_input', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-6">
            <label class="label">
                Мой расч/счет в<span class="important" aria-required="true">*</span>
            </label>
            <div class="form-filter">
                <?php echo $form->field($model, 'company_rs', ['template' => "{input}", 'options' => ['class' => '']])
                    ->widget(Select2::class, [
                        'data' => $rsData,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                        'options' => [
                            'class' => 'form-control',
                        ]
                    ])->label(false); ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-6">
            <label class="label">
                <?=($model->type == Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?>
                <span class="important" aria-required="true">*</span>
            </label>
            <div class="form-filter">
                <?php echo $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => ['class' => '']])
                    ->widget(Select2::class, [
                        'data' => $contractorArray,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'disabled' => true
                    ])->label(false); ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-6">
            <label class="label">
                Шаблон<span class="important" aria-required="true">*</span>
            </label>
            <div class="form-filter">
                <?php echo $form->field($model, 'agreement_template_id', ['template' => "{input}", 'options' => ['class' => '']])
                    ->widget(Select2::class, [
                        'data' => $allTemplates,
                        'pluginOptions' => [
                            'width' => '100%'
                        ],
                        'options' => [
                            'class' => 'form-control',
                        ],
                        'disabled' => true
                    ])->label(false); ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-6">
            <label class="label">
                Дата окончания<span class="important" aria-required="true">*</span>
            </label>
            <div class="date-picker-wrap" style="width: 130px">
                <?= Html::activeTextInput($model, 'payment_limit_date', [
                    'id' => 'under-date',
                    'data-delay' => $model->payment_delay ?: 10,
                    'class' => 'form-control form-control_small date-picker invoice_document_date',
                    'value' => ($model->isNewRecord) ?
                        date(DateHelper::FORMAT_USER_DATE, time() + 10 * 24 * 3600) :
                        DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
                <svg class="date-picker-icon svg-icon input-toggle">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                </svg>
            </div>
        </div>
        <br/>
        <?php if (!$model->isNewRecord) : ?>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label">
                    Шапка<span class="show_agreement">&nbsp;договора</span><span class="important" aria-required="true">*</span>
                </label>
                <?= $form->field($model->essence, 'document_header', [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ]
                ])->widget(TinyMce::class, [
                    'options' => ['rows' => 8],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "table paste code"

                        ],
                        'paste_webkit_styles' => "font-style text-align",
                        'menubar' => false,
                        'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                        'content_style' => "p {margin-top:0;margin-bottom:0}",
                        'branding' => false
                    ]
                ]) ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label">
                    Предмет договора<span class="important" aria-required="true">*</span>
                </label>
                <?= $form->field($model->essence, 'document_body', [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ]
                ])->widget(TinyMce::class, [
                    'options' => ['rows' => 8],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "table paste code"

                        ],
                        'paste_webkit_styles' => "font-style text-align",
                        'menubar' => false,
                        'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                        'content_style' => "p {margin-top:0;margin-bottom:0}",
                        'branding' => false
                    ]
                ]) ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label">
                    Ваши реквизиты<span class="important" aria-required="true">*</span>
                </label>
                <?= $form->field($model->essence, 'document_requisites_customer', [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ]
                ])->widget(TinyMce::class, [
                    'options' => ['rows' => 8],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "table paste code"

                        ],
                        'paste_webkit_styles' => "font-style text-align",
                        'menubar' => false,
                        'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                        'content_style' => "p {margin-top:0;margin-bottom:0}",
                        'branding' => false
                    ]
                ]) ?>
            </div>
        </div>
        <br/>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label">
                    Реквизиты контрагента<span class="important" aria-required="true">*</span>
                </label>
                <?= $form->field($model->essence, 'document_requisites_executer', [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ]
                ])->widget(TinyMce::class, [
                    'options' => ['rows' => 8],
                    'language' => 'ru',
                    'clientOptions' => [
                        'plugins' => [
                            "table paste code"

                        ],
                        'paste_webkit_styles' => "font-style text-align",
                        'menubar' => false,
                        'toolbar' => "code | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | table | removeformat",
                        'content_style' => "p {margin-top:0;margin-bottom:0}",
                        'branding' => false
                    ]
                ]) ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>