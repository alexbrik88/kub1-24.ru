<?php
use frontend\rbac\permissions;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model common\models\Agreement */

$cancelUrl = $model->isNewRecord ? [
    'agreement/index',
    'type' => $type,
] : [
    'agreement/view',
    'id' => $model->id,
];
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>

            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Удалить',
                    'class' => 'button-width button-clr button-regular button-hover-grey ml-4',
                ],
                'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить договор?',
            ]); ?>
        </div>
    </div>
</div>
