<?php

use common\models\Agreement;
use frontend\rbac\permissions;
use frontend\themes\mts\components\Icon;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\Agreement;
 */

?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?php if ($canUpdate): ?>
                <?= Html::button(Icon::get('envelope', ['class' => 'mr-2']).'Отправить', [
                    'class' => 'open-send-to button-regular button-hover-transparent w-full',
                    'data-toggle' => 'toggleVisible',
                    'data-target' => 'invoice'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a(Icon::get('print', ['class' => 'mr-2']).'Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                'class' => 'button-regular button-hover-transparent w-full',
                'target' => '_blank',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a(Icon::get('download', ['class' => 'mr-2']).'Скачать',
                ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()], [
                'class' => 'button-regular button-hover-transparent w-full',
                'encode' => false,
                'linkOptions' => [
                    'target' => '_blank',
                ]
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => Icon::get('copied', ['class' => 'mr-1']).'Копировать',
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите скопировать этот договор?',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => (!$model->is_completed) ?
                            Icon::get('close', ['class' => 'mr-2']).'Окончен' :
                            Icon::get('check-2', ['class' => 'mr-2']).'В работе',
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to([
                            'update-status',
                            'id' => $model->id,
                            'status' => (!$model->is_completed) ?
                                'ended' :
                                'active'
                    ]),
                    'message' => (!$model->is_completed) ?
                        'Вы уверены, что хотите закрыть договор?' :
                        'Вы уверены, что хотите восстановить договор?'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= Html::button(Icon::get('garbage', ['class' => 'mr-2']).'Удалить', [
                    'class' => 'button-regular button-hover-transparent w-full',
                    'data-toggle' => 'modal',
                    'href' => '#delete-confirm',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\Contractor::CREATE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id, 'type' => $model->type]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот договор?",
    ]);
}; ?>
