<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */
?>
<div class="agreement-new-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => false,
        'validateOnBlur' => false,
        //'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'agreement-new-form',
        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'label',
            ],
            'wrapperOptions' => [
                'class' => 'form-filter',
            ],
            'inputOptions' => [
                'class' => 'form-control',
            ],
            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
        ],
    ])); ?>

    <div class="form-body form-horizontal form-body_width">

        <?= $this->render('partial/_formMain', [
            'model' => $model,
            'form' => $form,
            'type' => $type
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
            'form' => $form,
            'type' => $type
        ])?>

    </div>

    <?php ActiveForm::end(); ?>
</div>
