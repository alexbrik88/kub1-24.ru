<?php

use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message frontend\modules\documents\components\Message; */

?>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($model->invoice->contractor_name_short, [
            '/contractor/view',
            'type' => $model->invoice->contractor->type,
            'id' => $model->invoice->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Основание:</span>
        <span>
            <?= $model->getBasisName(); ?>
        </span>
    </div>
    <div class="about-card-item">
        <?= \frontend\themes\mts\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>
