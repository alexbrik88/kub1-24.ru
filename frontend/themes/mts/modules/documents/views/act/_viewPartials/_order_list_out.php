<?php
/** @var \common\models\document\Act $model */

if (empty($precision)) {
    $precision = $model->invoice->price_precision;
}
?>

<table class="table table-style table-count out-act_table">
    <thead>
    <tr class="heading" role="row">
        <th width="2%"></th>
        <th width="25%">Наименование</th>
        <th width="10%">Количество</th>
        <th width="5%">Ед</th>
        <th width="15%">Цена</th>
        <th width="15%">Сумма</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php foreach ($model->orderActs as $key => $order): ?>
        <?php echo
        $this->render('_template_out', [
            'key' => $key,
            'order' => $order,
            'model' => $model,
            'precision' => $precision,
        ]);
        ?>
    <?php endforeach; ?>
    </tbody>
</table>
