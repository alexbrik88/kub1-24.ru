<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\themes\mts\components\Icon;
use frontend\widgets\RangeButtonWidget;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;
use common\components\ImageHelper;
use common\models\document\OrderAct;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel ActSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $company Company */

$this->title = $message->get(Message::TITLE_PLURAL);

$company = Yii::$app->user->identity->company;
$period = StatisticPeriod::getSessionName();

$exists = Invoice::find()->joinWith('acts', false, 'INNER JOIN')
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет актов. Измените период, чтобы увидеть имеющиеся акты.";
    }
} else {
    $emptyMessage = 'Вы еще не создали ни одного акта.';
}

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
        'ioType' => $ioType,
    ]);
$canUpdateStatus = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$dropItems = [];
if ($canUpdateStatus){
    if ($ioType == \frontend\models\Documents::IO_TYPE_OUT) {
        $dropItems[] = [
            'label' => 'Передан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_SEND])
            ]
        ];
        $dropItems[] = [
            'label' => 'Подписан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-status', 'type' => $ioType, 'status' => ActStatus::STATUS_RECEIVED])
            ]
        ];
    } else {
        $dropItems[] = [
            'label' => 'Скан',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 0])
            ]
        ];
        $dropItems[] = [
            'label' => 'Оригинал',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'many-documents-update-status',
                'data-url' => Url::to(['many-update-original', 'type' => $ioType, 'val' => 1])
            ]
        ];
    }
}
if ($ioType == \frontend\models\Documents::IO_TYPE_OUT && $canIndex) {
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
}

$showAutoActModal = Yii::$app->request->get('autoActModal');

$existsInvoicesToAdd = Invoice::find()
    ->where(['can_add_act' => true])
    ->byCompany(Yii::$app->user->identity->company->id)
    ->byDeleted()->byIOType($ioType)->exists();

$stat1 = [
    'sum' => OrderAct::find()
        ->andWhere(['act_id' => $searchModel->getQuery()
            ->select('`act`.`id`')
            ->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])
            ->column()])
        ->leftJoin('order', 'order.id = order_act.order_id')
        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`'),
    'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_PRINTED])->count()
];
$stat2 = [
    'sum' => OrderAct::find()
        ->andWhere(['act_id' => $searchModel->getQuery()
            ->select('`act`.`id`')
            ->andWhere(['status_out_id' => ActStatus::STATUS_SEND])
            ->column()])
        ->leftJoin('order', 'order.id = order_act.order_id')
        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`'),
    'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_SEND])->count()
];
$stat3 = [
    'sum' => OrderAct::find()
        ->andWhere(['act_id' => $searchModel->getQuery()
            ->select('`act`.`id`')
            ->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])
            ->column()])
        ->leftJoin('order', 'order.id = order_act.order_id')
        ->sum('`order`.`amount_sales_with_vat` * `order_act`.`quantity`'),
    'count' => $searchModel->getQuery()->andWhere(['status_out_id' => ActStatus::STATUS_RECEIVED])->count()
];

$this->registerJs('
    $(document).on("change", ".autoact-rule-radio", function() {
        var rule = $(".autoact-rule-radio:checked").val();
        $("#autoact-form .collapse.show").collapse("hide");
        if (rule) {
            $("#autoact-form .rule" + rule + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $("#autoact-form .rule" + rule + " .has-error").removeClass("has-error");
            $("#autoact-form .rule" + rule + " .help-block").html("");
            $("#autoact-form .rule" + rule).collapse("show");
        }
    });
    $(document).on("change", ".autoact-status-radio", function() {
        var $checked = $(".autoact-status-radio:checked");
        console.log($checked.val());
        $("#autoact-form .autoact-status-content.collapse.show").collapse("hide");
        if ($checked.val() == "1") {
            $checked.data("target")
            $($checked.data("target") + " input").prop({checked: false, disabled: false}).uniform("refresh");
            $($checked.data("target") + " .has-error").removeClass("has-error");
            $($checked.data("target") + " .help-block").html("");
            $($checked.data("target")).collapse("show");
        }
    });
    $(document).on("hidden.bs.collapse", ".autoact-rule-box", function () {
        $("#autoact-form .autoact-rule-box.collapse:not(.show) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.collapse", ".autoact-status-content", function () {
        $("#autoact-form .autoact-status-content.collapse:not(.show) input").prop({checked: false, disabled: true}).uniform("refresh");
    });
    $(document).on("hidden.bs.modal", "#autoact-form-modal", function () {
        $.pjax.reload("#autoact-form-pjax");
    });
    $(document).on("pjax:success", "#autoact-form-pjax", function () {
        $("input", this).uniform("refresh");
    });
');
$baseUrl = Yii::$app->params['mtsAssetBaseUrl'];

\frontend\themes\mts\helpers\MtsHelper::setStatisticsFontSize(max($stat1['sum'], $stat2['sum'], $stat3['sum']) );
?>

<div class="stop-zone-for-fixed-elems">
    <?php if (!Documents::getSearchQuery(Documents::DOCUMENT_INVOICE, $ioType)->byDeleted()->exists()) : ?>
        <?php
        Modal::begin([
            'clientOptions' => ['show' => true],
            'closeButton' => false,
        ]); ?>
        <h4 class="modal-title text-center mb-4">Перед тем как выставить Акт, нужно создать Счет.</h4>
        <div class="text-center">
            <?= Html::a('Создать счет', ['invoice/create', 'type' => $ioType], [
                'class' => 'button-clr button-regular button-hover-transparent button-width mr-2',
            ]); ?>
        </div>
        <?php
        Modal::end();

        ?>
        <div class="alert-success alert fade in">
            <button id="contractor_alert_close" type="button" class="close"
                    data-dismiss="alert" aria-hidden="true">×
            </button>
            Перед тем как выставить Акт,
            нужно <?= Html::a('создать счёт', ['invoice/create', 'type' => $ioType]) ?>
            .
        </div>
    <?php endif; ?>
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
        <?php if ($company->createInvoiceAllowed($ioType)) : ?>
            <?php if ($existsInvoicesToAdd) : ?>
                <button class="button-regular button-regular_red button-width ml-auto add-document">
                    <svg class="svg-icon">
                        <use xlink:href="<?=$baseUrl?>/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </button>
            <?php else : ?>
                <a class="button-regular button-regular_red button-width ml-auto" href="<?= Url::to([
                    '/documents/invoice/create',
                    'type' => $ioType,
                    'document' => Documents::SLUG_ACT,
                ]) ?>">
                    <svg class="svg-icon">
                        <use xlink:href="<?=$baseUrl?>/images/svg-sprite/svgSprite.svg#add-icon"></use>
                    </svg>
                    <span>Добавить</span>
                </a>
            <?php endif; ?>
        <?php else : ?>
            <button class="button-regular button-regular_red button-width ml-auto action-is-limited">
                <svg class="svg-icon">
                    <use xlink:href="<?=$baseUrl?>/images/svg-sprite/svgSprite.svg#add-icon"></use>
                </svg>
                <span>Добавить</span>
            </button>
        <?php endif ?>
    </div>
    <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
        <div class="wrap wrap_count">
            <div class="row">
                <div class="col-6 col-xl-3">
                    <div class="count-card wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat1['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Распечатано</div>
                        <hr>
                        <div class="count-card-foot">
                            Количество актов:
                            <?= $stat1['count'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-xl-3">
                    <div class="count-card count-card_red wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat2['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Передано</div>
                        <hr>
                        <div class="count-card-foot">
                            Количество актов:
                            <?= $stat2['count'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-xl-3">
                    <div class="count-card count-card_green wrap">
                        <div class="count-card-main"><?= TextHelper::invoiceMoneyFormat($stat3['sum'], 2); ?> ₽</div>
                        <div class="count-card-title">Подписано</div>
                        <hr>
                        <div class="count-card-foot">
                            Количество актов:
                            <?= $stat3['count'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-xl-3 d-flex flex-column justify-content-top">
                    <?= frontend\widgets\RangeButtonWidget::widget(); ?>

                    <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                        <?php Modal::begin([
                            'id' => 'autoact-form-modal',
                            'closeButton' => false,
                            'toggleButton' => [
                                'tag' => 'button',
                                'label' => 'Настройка АвтоАктов',
                                'class' => 'button-regular w-100 button-hover-content-red',
                            ],
                        ]); ?>

                        <?= $this->render('autoact') ?>

                        <?php Modal::end(); ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php else : ?>
        <div class="wrap wrap_count">
            <div class="row">
                <div class="col-6 col-xl-9">
                </div>
                <div class="col-6 col-xl-3 d-flex flex-column justify-content-between">
                    <?= frontend\widgets\RangeButtonWidget::widget(); ?>
                </div>
            </div>
        </div>
    <?php endif ?>
    <div class="table-settings row row_indents_s">
        <div class="col-6">
            <?= Html::a(Icon::get('exel'), array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link button-list button-hover-transparent button-clr mr-2',
                'title' => 'Скачать в Excel',
            ]); ?>
        </div>
        <div class="col-6">
            <?php $form = \yii\widgets\ActiveForm::begin([
                'method' => 'GET',
                'options' => [
                    'class' => 'd-flex flex-nowrap align-items-center',
                ],
            ]); ?>
                <div class="form-group flex-grow-1 mr-2">
                    <?= Html::activeTextInput($searchModel, 'byNumber', [
                        'type' => 'search',
                        'placeholder' => 'Номер акта, название или ИНН контрагента',
                        'class' => 'form-control'
                    ]); ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Найти', [
                        'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                    ]) ?>
                </div>
            <?php $form->end(); ?>
        </div>
    </div>
    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => $emptyMessage,
        'tableOptions' => [
            'class' => 'table table-style table-count-list',
        ],
        'pager' => [
            'options' => [
                'class' => 'nav-pagination list-clr',
            ],
        ],
        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'header' => Html::checkbox('', false, [
                    'class' => 'joint-operation-main-checkbox',
                ]),
                'headerOptions' => [
                    'width' => '5%',
                ],
                'contentOptions' => [
                ],
                'format' => 'raw',
                'value' => function (Act $model) {
                    return Html::checkbox('Act[' . $model->id . '][checked]', false, [
                        'class' => 'joint-operation-checkbox',
                        'data-sum' => $model->totalAmountWithNds,
                    ]);
                },
            ],
            [
                'attribute' => 'document_date',
                'label' => 'Дата акта',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '10%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function (Act $data) {
                    return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                },
            ],

            [
                'attribute' => 'document_number',
                'label' => '№ акта',
                'headerOptions' => [
                    'class' => 'sorting nowrap',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'document_number link-view',
                ],
                'format' => 'raw',
                'value' => function (Act $data) {
                    return (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $data,])
                        ? Html::a($data->fullNumber, ['/documents/act/view', 'type' => $data->type, 'id' => $data->id])
                        : $data->fullNumber);
                },
            ],
            [
                'label' => 'Скан',
                'headerOptions' => [
                    'class' => 'sorting',
                    'width' => '5%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'attribute' => 'has_file',
                'format' => 'raw',
                'value' => function ($model) {
                    return DocumentFileWidget::widget(['model' => $model]);
                },
            ],
            [
                'attribute' => 'order_sum',
                'label' => 'Сумма',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'contentOptions' => [
                    'class' => 'link-view',
                ],
                'format' => 'raw',
                'value' => function ($data) {
                    $price = TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    return '<span class="price" data-price="' . str_replace(" ", "", $price) . '">' . $price . '</span>';
                },
            ],

            [
                'attribute' => 'contractor_id',
                'label' => 'Контрагент',
                'headerOptions' => [
                    'width' => '30%',
                    'class' => 'dropdown-filter',
                ],
                'filter' => FilterHelper::getContractorList($searchModel->type, Act::tableName(), true, false, false),
                'hideSearch' => false,
                's2width' => '300px',
                'format' => 'raw',
                'value' => function (Act $data) {
                    return '<span title="' . htmlspecialchars($data->invoice->contractor_name_short) . '">' . $data->invoice->contractor_name_short . '</span>';
                },
            ],

            [
                'attribute' => 'status_out_id',
                'label' => 'Статус',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'filter' => $searchModel->getStatusArray($searchModel->type),
                's2width' => '120px',
                'format' => 'raw',
                'value' => function (Act $data) {
                    return $data->statusOut->name;
                },
                'visible' => $ioType == Documents::IO_TYPE_OUT,
            ],

            [
                'attribute' => 'invoice_document_number',
                'label' => 'Счёт №',
                'headerOptions' => [
                    'width' => '15%',
                ],
                'format' => 'raw',
                'value' => function (Act $data) {
                    $invoice = $data->invoice;
                    if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $invoice,])) {
                        return Html::a($invoice->fullNumber, [
                                '/documents/invoice/view',
                                'type' => $data->type,
                                'id' => $data->invoice->id,
                            ]) . DocumentFileWidget::widget(['model' => $invoice, 'cssClass' => 'pull-right']);
                    } else {
                        return $data->invoice->fullNumber;
                    }
                },
            ],
        ],
    ]); ?>
</div>

<?php $this->registerJs('
    $(".invoice-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
'); ?>
<?php if ($showAutoActModal) : ?>
    <?php $this->registerJs('
    $(document).ready(function () {
        $("#autoact-form-modal").modal();
    });
'); ?>
<?php endif; ?>
<?php if ($company->show_popup_expose_other_documents && $ioType == Documents::IO_TYPE_OUT) : ?>
<?php /*
    <?php $company->updateAttributes(['show_popup_expose_other_documents' => false]); ?>

    <?php Modal::begin([
        'title' => '<h2 class="header-name" style="text-transform: uppercase;">
            Подготовить акт, накладную<br> счет-фактуру
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 2,
            'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => null,
            'nextModal' => 3,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
 */ ?>
<?php endif; ?>

<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a($this->render('//svg-sprite', ['ico' => 'print']).' <span>Печать</span>', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'button-clr button-regular button-width button-hover-transparent multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? Html::a($this->render('//svg-sprite', ['ico' => 'envelope']).' <span>Отправить</span>', null, [
            'class' => 'button-clr button-regular button-width button-hover-transparent document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) : null,
        $canDelete ? Html::a($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', '#many-delete', [
            'class' => 'button-clr button-regular button-width button-hover-transparent',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::button('<span>Еще</span>  '.$this->render('//svg-sprite', ['ico' => 'shevron']), [
            'class' => 'button-regular button-regular-more button-hover-transparent button-clr dropdown-toggle space-between',
            'data-toggle' => 'dropdown',
            'style' => 'width: 90px; padding: 12px 15px;',
        ]) . Dropdown::widget([
            'items' => $dropItems,
            'options' => [
                'class' => 'dropdown-menu-right form-filter-list list-clr',
            ],
        ]), ['class' => 'dropup']) : null,
    ],
]); ?>

<!-- Скачать в Excel -->
<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/modal/_invoices_modal', [
    'company' => $company,
    'ioType'  => $ioType,
    'documentType' => Documents::SLUG_ACT,
    'documentTypeName' => 'Акт'
]) ?>

<?php if ($canDelete) : ?>
    <?php Modal::begin([
        'id' => 'many-delete',
        'closeButton' => false,
        'options' => [
            'class' => 'fade confirm-modal',
        ],
    ]); ?>
    <h4 class="modal-title text-center mb-4">
        Вы уверены, что хотите удалить выбранные акты?
    </h4>
    <div class="text-center">
        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
            'class' => 'button-clr button-regular button-hover-transparent button-width-medium modal-many-delete ladda-button',
            'data-url' => Url::to(['many-delete', 'type' => $ioType]),
        ]); ?>
        <button class="button-clr button-regular button-hover-transparent button-width-medium ml-2" type="button" data-dismiss="modal">
            Нет
        </button>
    </div>
    <?php Modal::end(); ?>
<?php endif ?>