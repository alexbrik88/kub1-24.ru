<?php

use common\models\document\Autoact;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\document\Autoact */
/* @var $value integer */
/* @var $checked boolean */

$cssClass = "autoact-rule-box rule{$value} collapse" . ($checked ? ' show' : '');
?>

<div class="<?= $cssClass ?>">
    <div class="autoact-rule-content">
        <?= $form->field($model, "[{$value}]send_together", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ]); ?>
    </div>
</div>