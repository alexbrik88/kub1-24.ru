<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\document\Autoact */
/* @var $value integer */
/* @var $checked boolean */

$cssClass = "autoact-rule-box rule{$value} collapse" . ($checked ? ' in' : '');
?>

<div class="<?= $cssClass ?>">
    <div class="autoact-rule-content">
        <?= $form->field($model, "[{$value}]no_pay_partial", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ]); ?>
        <?= $form->field($model, "[{$value}]no_pay_order", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ]); ?>
        <?= $form->field($model, "[{$value}]no_pay_emoney", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ]); ?>
        <?= $form->field($model, "[{$value}]send_auto", ['options' => ['class' => '']])->checkbox([
            'disabled' => !$checked,
        ])->hint('(Т.е. счет поменял статус на ОПЛАЧЕН, создался Акт и отправилось письмо клиенту с Актом)'); ?>
    </div>
</div>