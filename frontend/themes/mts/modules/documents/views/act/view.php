<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\themes\mts\modules\documents\widgets\DocumentLogWidget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;

$backUrl = null;
if ($useContractor && Yii::$app->user->can(permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
?>

<?php if ($backUrl !== null) : ?>
    <?= Html::a('Назад к списку', $backUrl, ['class' => 'link mb-2']); ?>
<?php endif; ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'type' => $ioType,
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->invoice->contractor_id : null),
                    ], [
                        'data-tooltip-content' => '#unpaid-invoice',
                        'data-placement' => 'right',
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 ',
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container">
                    <?= $this->render('/act/template', [
                        'model' => $model,
                        'addStamp' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('_viewPartials/_status_block_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'useContractor' => $useContractor,
            ]); ?>
            <?= $this->render('_viewPartials/_main_info', [
                'model' => $model,
                'message' => $message,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>
