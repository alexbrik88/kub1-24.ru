<?php
use common\components\TextHelper;
use yii\helpers\Html;
/** @var \common\models\document\AgentReport $model */
/** @var $order \common\models\document\AgentReportOrder */
?>

<table class="table table-striped table-bordered table-hover agent_report_table">
    <thead>
    <tr class="heading" role="row">
        <th> </th>
        <th> Покупатель</th>
        <th> Сумма оплат за месяц (руб.)</th>
        <th> Комиссионное вознаграждение %</th>
        <th> Комиссионное вознаграждение (руб.)</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php foreach ($model->agentReportOrders as $key => $order): ?>
        <?php if ($order->is_exclude) continue; ?>
        <?php $order_id = $order->id; ?>
        <tr role="row" class="odd order">
            <td width="1%" class="text-center">
                <span class="editable-field"><?= $key + 1; ?></span>
                <span class="remove-buyer-from-agent-report icon-close input-editable-field delete-row hide"></span>
            </td>
            <td width="30%" class="col-agent-report" style="text-align: left;">
                <?= Html::hiddenInput("AgentReportOrder[{$order_id}][id]", $order_id, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
                <?= $order->contractor->getShortName(); ?>
            </td>
            <td width="15%" class="col-agent-report order_payments_sum">
                <?= TextHelper::invoiceMoneyFormat($order->payments_sum, $precision) ?>
            </td>
            <td width="15%" class="col-agent-report">
                <?= Html::input('number', "AgentReportOrder[{$order_id}][agent_percent]", $order->agent_percent, [
                    'min' => 1,
                    'max' => 100,
                    'class' => 'input-editable-field input_agent_percent hide form-control',
                    'style' => 'padding: 6px 6px;',
                    'value' => TextHelper::numberFormat($order->agent_percent, $precision)
                ]) ?>
            </td>
            <td width="15%" class="col-agent-report order_agent_sum">
                <?= TextHelper::invoiceMoneyFormat($order->agent_sum, $precision); ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>