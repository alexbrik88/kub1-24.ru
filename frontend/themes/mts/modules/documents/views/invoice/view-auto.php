<?php

use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\document\Document;
use frontend\themes\mts\modules\documents\widgets\DocumentLogWidget;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceAuto */
/* @var $useContractor string */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = 'Шаблон ' . ($model->is_invoice_contract ? 'автосчёт-договора' : 'автосчёта') . '  №' . $model->fullNumber . ' от ' . $dateFormatted;
?>

<?= Html::a('Назад к списку', $useContractor ? [
    '/contractor/view',
    'type' => $model->type,
    'id' => $model->contractor_id,
    'tab' => 'autoinvoice',
] : [
    'index-auto',
], [
    'class' => 'link mb-2',
]); ?>

<?php if ($model->is_deleted): ?>
    <h3 class="text-warning">Автосчёт удалён</h3>
<?php endif; ?>

<?php Pjax::begin([
    'id' => 'view-auto-pjax',
    'enablePushState' => false,
    'timeout' => 5000,
    'linkSelector' => '.auto-status-link',
]) ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= \frontend\themes\mts\modules\documents\widgets\DocumentAutoivoiceLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(Document::UPDATE, ['model' => $model])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), $model->updateAllowed() ? [
                        'update-auto',
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->contractor_id : null),
                    ] : 'javascript:;', [
                        'data-tooltip-content' => '#unpaid-invoice',
                        'data-placement' => 'right',
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 '.
                            (!$model->updateAllowed() ? 'tooltip2-right' : null),
                    ]) ?>
                <?php endif; ?>
                <div class="doc-container doc-invoice doc-invoice-auto">
                    <?= $this->render('/invoice/template', [
                        'model' => $model,
                        'addStamp' => true,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('/invoice/view-auto/_status_block', [
                'model' => $model,
            ]); ?>

            <?= $this->render('/invoice/view-auto/_main_info', [
                'model' => $model,
                'ioType' => $model->type,
                'dateFormatted' => $dateFormatted,
                'useContractor' => $useContractor,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('view-auto/_action_buttons', [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>

<?php Pjax::end(); ?>

<?php

echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'start-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::to([
        'view-auto',
        'id' => $model->id,
        'contractorId' => Yii::$app->request->get('contractorId'),
        'status' => 'start',
    ]),
    'message' => 'Вы уверены, что хотите запустить этот шаблон?',
]);
echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'stop-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::to([
        'view-auto',
        'id' => $model->id,
        'contractorId' => Yii::$app->request->get('contractorId'),
        'status' => 'stop',
    ]),
    'message' => 'Вы уверены, что хотите остановить этот шаблон?',
]);
echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'copy-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::to(['copy-auto',
        'id' => $model->id,]),
    'message' => 'Вы уверены, что хотите скопировать этот шаблон?',
]);
echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => [
        'delete-auto',
        'id' => $model->id,
    ],
    'confirmParams' => [],
    'message' => "Вы уверены, что хотите удалить этот шаблон?",
]);
?>


<?php $this->registerJs('
    $(document).on("click", ".auto-status-link", function() {
        $(".modal.show").modal("hide");
    });
') ?>