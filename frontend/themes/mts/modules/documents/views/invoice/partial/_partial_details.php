<?php
/* @var $model common\models\Contractor */
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
$textInputConfig = [
    'options' => [
        'class' => 'form-group col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$addressInputConfig = [
    'options' => [
        'class' => 'form-group col-12',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{input}\n{error}",
];
$textInputConfig2 = [
    'options' => [
        'class' => 'form-group  col-6',
    ],
    'labelOptions' => [
        'class' => 'label',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
    ],
    'template' => "{label}\n{input}\n{error}",
];
?>

<button class="link link_collapse link_bold button-clr mb-4 collapsed" type="button" data-toggle="collapse" data-target="#dopColumns2" aria-expanded="false" aria-controls="dopColumns2">
    <span class="link-txt">Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры</span>
    <svg class="link-shevron svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>
    </svg>
</button>

<div id="dopColumns2" class="collapse dopColumns">
    <div class="row">
        <?= $form->field($model, 'director_post_name', $textInputConfig)->label('Должность руководителя'); ?>
        <?= $form->field($model, 'director_name', $textInputConfig)->label('ФИО руководителя'); ?>
        <?= $form->field($model, 'legal_address', $addressInputConfig)->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
    <div class="row">
        <?= $form->field($model, 'current_account', $textInputConfig2)->textInput([
            'maxlength' => true,
        ]); ?>

        <?= $form->field($model, 'BIC', $textInputConfig2)->widget(\common\components\widgets\BikTypeahead::classname(), [
            'remoteUrl' => Url::to(['/dictionary/bik']),
            'related' => [
                '#' . Html::getInputId($model, 'bank_name') => 'name',
                '#' . Html::getInputId($model, 'bank_city') => 'city',
                '#' . Html::getInputId($model, 'corresp_account') => 'ks',
            ],
        ])->textInput(['placeHolder' => 'Нужен для Актов и Товарных накладных']); ?>

        <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
            'maxlength' => true,
            'disabled' => true,
        ]); ?>
        <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
            'maxlength' => true,
            'disabled' => true,
        ]); ?>
        <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
            'maxlength' => true,
            'disabled' => true,
        ]); ?>
    </div>
</div>