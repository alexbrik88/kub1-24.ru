<?php

/**
 * @var $model \common\models\document\Invoice
 * @var $document string
 * @var $title string
 */

use common\components\TextHelper;
use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\WaybillStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\themes\mts\components\Icon;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\document\status\ProxyStatus;

$iconClass = 'icon-doc';
if ($document == 'act') {
    $docQuery = $model->getAct();
    $iconClass = ActStatus::getStatusIcon($model->getAct()->min('status_out_id'));
} elseif ($document == 'packing-list') {
    $docQuery = $model->getPackingList();
    $iconClass = PackingListStatus::getStatusIcon($model->getPackingList()->min('status_out_id'));
} elseif ($document == 'proxy') {
    $docQuery = $model->getProxy();
    $iconClass = ProxyStatus::getStatusIcon($model->getProxy()->min('status_out_id'));
} elseif ($document == 'waybill') {
    $docQuery = $model->getWaybill();
    $iconClass = WaybillStatus::getStatusIcon($model->getWaybill()->min('status_out_id'));
} elseif ($document == 'invoice-facture') {
    $docQuery = $model->getInvoiceFactures();
    $iconClass = InvoiceFactureStatus::getStatusIcon($model->getInvoiceFactures()->min('status_out_id'));
} elseif ($document == 'upd') {
    $docQuery = $model->getUpds();
}

$docArray = !empty($docQuery) ? $docQuery->orderBy('document_number DESC')->all() : [];

$sum = 0;
?>
<div style="display: flex; justify-content: space-between;">
    <div class="dropdown w-100 pr-2">
        <?= Html::a($this->render('//svg-sprite', ['ico' => 'add-icon']).' '.Html::tag('span', $title), 'javascript:;', [
            'class' => 'button-regular button-hover-content-red w-100 text-left dropdown-toggle '.($model->isRejected ? ' disabled' : ''),
            'style' => $model->isRejected ? ' background-color: #a2a2a2;' : '',
            'title' => mb_convert_case(($addAvailable ? 'Добавить ' : '') . $title, MB_CASE_TITLE),
            'data-toggle' => 'dropdown'
        ]) ?>
        <ul class="dropdown-menu documents-dropdown" style="right: -110px; left: auto;">
            <?php if ($addAvailable) : ?>
                <li>
                    <?= Html::a(Icon::get('add-icon') . "Добавить {$title}", [
                        $document . '/create',
                        'type' => $model->type,
                        'invoiceId' => $model->id,
                    ], [
                        'class' => 'dropdown-new',
                    ]); ?>
                </li>
            <?php endif ?>

            <?php foreach ($docArray as $item) : ?>
                <?php
                $url = [
                    $document . '/view',
                    'type' => $model->type,
                    'id' => $item->id,
                ];
                $date = DateHelper::format($item->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $itemSum = TextHelper::invoiceMoneyFormat($item->totalAmountWithNds, 2);
                $sum += $item->totalAmountWithNds;
                ?>
                <li>
                    <a class="dropdown-item" style="position: relative;" href="<?= Url::to($url) ?>">
                        <span class="dropdown-span-left">
                            № <?= $item->document_number ?> от <?= $date; ?>
                        </span>
                        <span class="dropdown-span-right">
                            <?= $itemSum ?> ₽
                        </span>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <?= Html::tag('div', TextHelper::invoiceMoneyFormat($sum, 2, ',', '&nbsp;').'&nbsp;₽', [
        'class' => 'button-regular button-hover-content-red',
        'title' => "Сумма текущих $title",
        'style' => ($model->isRejected ? ' background-color: #a2a2a2;' : ''),
        'disabled' => (bool) $model->isRejected
    ]) ?>
</div>
