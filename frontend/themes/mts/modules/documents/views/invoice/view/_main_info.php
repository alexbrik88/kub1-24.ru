<?php

use common\components\date\DateHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\themes\mts\helpers\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\models\bank\BankingParams;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string */
/* @var $newCompanyTemplate bool */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */
/* @var $company \common\models\Company */
/* @var $user \common\models\employee\Employee */

$cashFlowData = [];
if ($model->cashBankFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashBankFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashBankFlows, 'id');
    $data = Html::beginForm(['/cash/bank/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Банку', ['class' => 'link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashOrderFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashOrderFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashOrderFlows, 'id');
    $cashBox = ArrayHelper::getColumn($model->getCashOrderFlows()->groupBy('cashbox_id')->all(), 'cashbox_id');
    $cashBox = $cashBox ? current($cashBox) : null;
    $data = Html::beginForm(['/cash/order/index', 'cashbox' => $cashBox], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Кассе', ['class' => 'link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashEmoneyFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'id');
    $data = Html::beginForm(['/cash/e-money/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по E-money', ['class' => 'link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
$user = Yii::$app->user->identity;
$company = $user->company;
$alias = null;
$link = null;
$image = null;
if ($alias = Banking::aliasByBik($model->company_bik)) {
    $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
    $bankName = $bankingClass::NAME;
    $banking = new $bankingClass($company, [
        'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
    ]);
    if ($banking->getHasAutoload() && !$banking->isValidToken()) {
        $link = Html::a("Автоматическая загрузка выписки из банка с авто проставлением оплат у счетов", Url::to([
            "/cash/banking/{$alias}/default/index",
        ]), [
            'style' => 'font-size: 13px;',
        ]);

        if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
            $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                'class' => 'little_logo_bank_2',
                'style' => 'display: inline-block; ',
            ]);
        }
    }
}
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->contractor->getInvoicesAuto()->exists();
?>

<style type="text/css">
    button.link {
        border: none;
        background-color: transparent;
        font: inherit;
    }
</style>

<div class="about-card mb-3 mt-1">
    <div class="about-card-item">
        <span class="text-grey">
            <?= $message->get(Message::CONTRACTOR); ?>:
        </span>
        <?= Html::a($model->contractor_name_short, [
            '/contractor/view',
            'type' => $model->contractor->type,
            'id' => $model->contractor->id,
        ], ['class' => 'link']) ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">Оплатить до:</span>
        <span>
            <?= DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
        </span>
    </div>
    <?php if ($ioType == Documents::IO_TYPE_IN) : ?>
        <div class="about-card-item">
            <span class="text-grey">Статья расходов:</span>
            <span>
                <?= $model->invoiceExpenditureItem !== null ? $model->invoiceExpenditureItem->name : 'не указано'; ?>
            </span>
        </div>
    <?php else : ?>
        <?php if (isset($model->agreement_new_id) && $model->agreement_new_id > 0) : ?>
            <div class="about-card-item">
                <span class="text-grey">Договор №</span>
                <?php /* todo: agreements for MTS
                <?= Html::a(
                    Html::encode($model->basis_document_number) .
                    ' от ' .
                    DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    [
                        '/documents/agreement/view',
                        'id' => $model->agreement_new_id,
                    ],
                    [
                        'class' => 'link',
                    ]
                ); ?>*/ ?>
                <?= Html::encode($model->basis_document_number) . ' от ' .
                DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
            </div>
        <?php elseif ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
            <div class="about-card-item">
                <span class="text-grey">
                    <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>:
                </span>
                <span>
                    № <?= Html::encode($model->basis_document_number) ?>
                    от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </span>
            </div>
        <?php endif ?>
    <?php endif; ?>
    <?php if ($cashFlowData) : ?>
        <div class="about-card-item">
            <span class="text-grey">Оплата:</span>
            <span>
                <?= implode(', ', $cashFlowData) ?>
            </span>
        </div>
    <?php endif ?>
    <?php if ($model->currency_name != Currency::DEFAULT_NAME) : ?>
        <div id="invoice_currency_rate_box" class="about-card-item">
            <?= $this->render('_currency_info', ['model' => $model]) ?>
        </div>
    <?php endif ?>

    <div class="about-card-item">
        <?= \frontend\themes\mts\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>
<div>
    <div class="title-small mb-3 nowrap">
        <strong class="mr-1">
            <?php if ($model->comment_internal) : ?>
                Комментарий
            <?php else : ?>
                Комментарий
            <?php endif ?>
        </strong>
        <span id="comment_internal_update">
            <?= $this->render('//svg-sprite', ['ico' => 'pencil']) ?>
        </span>
        <div id="comment_internal_view" class="">
            <?= Html::encode($model->comment_internal) ?>
        </div>
        <?php if ($canUpdate) : ?>
            <?= Html::beginTag('div', [
                'id' => 'comment_internal_form',
                'class' => 'hidden',
                'style' => 'position: relative; margin-top: 4px;',
                'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
            ]) ?>
                <?= Html::tag('i', '', [
                    'id' => 'comment_internal_save',
                    'class' => 'fa fa-floppy-o',
                    'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                ]); ?>
                <?= Html::textarea('comment_internal', $model->comment_internal, [
                    'id' => 'comment_internal_input',
                    'rows' => 3,
                    'maxlength' => true,
                    'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                ]); ?>
            <?= Html::endTag('div') ?>
        <?php endif ?>
    </div>
</div>

<?php if ($alias && $link): ?>
    <div class="main-bank_logo mb-3">
        <?= $image; ?>
        <span class="bank-name" style="margin-left: 5px;">
            <?= $bankName; ?>
        </span>
        <div class="mt-3">
            <?= $link; ?>
        </div>
    </div>
<?php endif; ?>

<?php if ($model->type == Documents::IO_TYPE_OUT && !$hasAutoinvoice) : ?>
    <div>
        <div class="d-flex flex-nowrap align-items-center">
            <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                <?= Html::a($this->render('//svg-sprite', ['ico' => 'clock']) . Html::tag('span', 'АвтоСчет'), [
                    'create',
                    'type' => Documents::IO_TYPE_OUT,
                    'clone' => $model->id,
                    'auto' => 1,
                    'returnUrl' => Url::to([
                        '/contractor/view',
                        'type' => $model->contractor->type,
                        'id' => $model->contractor->id,
                        'tab' => 'autoinvoice',
                    ]),
                ], [
                    'class' => 'button-regular button-hover-content-red text-left flex-grow-1 mb-0',
                ]) ?>
            <?php else : ?>
                <button class="button-regular button-hover-content-red text-left flex-grow-1 mb-0 action-is-limited">
                    <?= $this->render('//svg-sprite', ['ico' => 'clock']) ?>
                    <span>
                        АвтоСчет
                    </span>
                </button>
            <?php endif ?>
            <div class="tooltip-box ml-3">
                <?= \yii\helpers\Html::tag('span', Icon::QUESTION, [
                    'class' => 'tooltip-hover',
                    'data-tooltip-content' => '#tooltip-autoinvoice-create',
                ]) ?>
            </div>
        </div>
        <div class="hidden">
            <div id="tooltip-autoinvoice-create" class="box-tooltip-templates">
                Создайте АвтоСчёт – счета будут сами <br>создаваться и отправляться клиенту<br>в указанный вами день.
            </div>
        </div>
    </div>
<?php endif; ?>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });
        $(document).on("change", "#activate_contractor_signature", function () {
            $.post($(this).data("url"), {status: +$(this).is(":checked")}, function (data) {});
        });
    ');
}
?>
