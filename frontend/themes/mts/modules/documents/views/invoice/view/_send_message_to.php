<?php

use common\components\helpers\Html;
use frontend\modules\documents\forms\InvoiceSendForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\document\Invoice;
use faryshta\widgets\JqueryTagsInput;
use common\models\company\CompanyType;
use common\models\document\EmailTemplate;
use common\components\helpers\ArrayHelper;
use yii\web\JsExpression;
use yii\web\View;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\Waybill;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\product\PriceList;
use common\models\employee\EmployeeRole;
use common\models\Agreement;
use common\models\driver\Driver;
use common\models\vehicle\Vehicle;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\document\AgentReport;

/* @var $this yii\web\View */
/* @var $model common\models\document\AbstractDocument */
/* @var $user common\models\employee\Employee */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $sendForm frontend\modules\documents\forms\InvoiceSendForm */

if (!($model instanceof PriceList) && !($model instanceof Driver) && !($model instanceof Vehicle)) {
    $directorLabel = '<span class="email-label">' . $contractor->director_name .
        ($contractor->director_email ?
            (' (Руководитель)<br><span style="padding-left: 30px">' . $contractor->director_email . '</span>') :
            null) .
        '</span>';
    $chiefAccountantLabel = '<span class="email-label">' . $contractor->chief_accountant_name .
        ($contractor->chief_accountant_email ?
            (' (Главный бухгалтер)<br><span style="padding-left: 30px">' . $contractor->chief_accountant_email . '</span>') :
            null) .
        '</span>';
    $contactLabel = '<span class="email-label">' . $contractor->contact_name .
        ($contractor->contact_email ?
            (' (Контакт)<br><span style="padding-left: 30px">' . $contractor->contact_email . '</span>') :
            null) .
        '</span>';
}
?>

<div id="user-email-dropdown" aria-labelledby="voice-input2">
    <?php if ($contractor) : ?>
        <div class="dropdown-menu-paddings mb-5">
            <?php if (!$contractor->chief_accountant_is_director && !$contractor->contact_is_director): ?>
                <div class="who-send-container all mb-2">
                    <?= Html::activeCheckbox($sendForm, 'sendToAll', [
                        'label' => 'Выбрать всех',
                        'class' => 'check-all-recipients',
                    ]); ?>
                </div>
            <?php endif; ?>
            <div class="who-send-container add-contractor-email">
                <div class="mb-2">
                    <?= Html::activeCheckbox($sendForm, 'sendToChief', [
                        'label' => $directorLabel,
                        'disabled' => !(bool)$contractor->director_email,
                        'data-value' => $contractor->director_email,
                        'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                    ]) ?>
                    <?php if (!$contractor->director_email): ?>
                        <?= $form->field($sendForm, 'chiefEmail', [
                            'options' => [
                                'class' => 'container-who-send-input chief-email',
                            ],
                            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                        ])->label(false)->textInput([
                            'placeHolder' => 'Укажите e-mail',
                            'class' => 'form-control',
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if (!$contractor->chief_accountant_is_director): ?>
                <div class="who-send-container add-contractor-email">
                    <div class="mb-2">
                        <?= Html::activeCheckbox($sendForm, 'sendToChiefAccountant', [
                            'label' => $chiefAccountantLabel,
                            'disabled' => !(bool)$contractor->chief_accountant_email,
                            'data-value' => $contractor->chief_accountant_email,
                            'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                        ]) ?>
                        <?php if (!$contractor->chief_accountant_email): ?>
                            <?= $form->field($sendForm, 'chiefAccountantEmail', [
                                'options' => [
                                    'class' => 'container-who-send-input chief-email',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->label(false)->textInput([
                                'placeHolder' => 'Укажите e-mail',
                                'class' => 'form-control width100',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!$contractor->contact_is_director): ?>
                <div class="who-send-container add-contractor-email">
                    <div class="mb-2">
                        <?= Html::activeCheckbox($sendForm, 'sendToContact', [
                            'label' => $contactLabel,
                            'disabled' => !(bool)$contractor->contact_email,
                            'data-value' => $contractor->contact_email,
                            'data-url' => Url::to(['/email/set-employee-email', 'contractorID' => $contractor->id]),
                        ]) ?>
                        <?php if (!$contractor->contact_email): ?>
                            <?= $form->field($sendForm, 'contactAccountantEmail', [
                                'options' => [
                                    'class' => 'container-who-send-input chief-email',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->label(false)->textInput([
                                'placeHolder' => 'Укажите e-mail',
                                'class' => 'form-control width100',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php foreach ($contractor->userEmails as $userEmail): ?>
                <div class="who-send-container">
                    <div class="mb-2">
                        <div class="checkbox">
                            <?= Html::checkbox('InvoiceSendForm[sendToUser][' . $userEmail->id . ']', false, [
                                'id' => 'invoicesendform-sendtouser',
                                'data-value' => $userEmail->email,
                                'label' => $userEmail->fio ? '<span class="email-label">' . $userEmail->fio .
                                    '<br><span style="padding-left: 25px">' . $userEmail->email . '</span></span>' :
                                    '<span class="email-label">' . $userEmail->email . '</span>',
                            ]); ?>
                        </div>
                        <div class="update-user-email-block" style="display: none">
                            <?= Html::textInput('fio', $userEmail->fio, [
                                'class' => 'form-control',
                                'id' => 'user-fio',
                                'placeholder' => 'Фамилия И.О.',
                                'style' => 'display: inline-block'
                            ]); ?>
                            <?= Html::textInput('email', $userEmail->email, [
                                'class' => 'form-control',
                                'id' => 'user-email',
                                'placeholder' => 'E-mail',
                                'style' => 'display: inline-block'
                            ]); ?>
                        </div>
                        <div class="email-actions">
                            <span class="link update-user-email">
                                <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#pencil"></use></svg>
                            </span>

                            <span class="link delete-user-email user-email-<?= $userEmail->id; ?>"
                                  data-url="<?= Url::to(['/email/delete-user-email', 'id' => $userEmail->id]); ?>"
                                  data-id="<?= $userEmail->id; ?>" data-toggle="modal"
                                  data-target="#delete-confirm-user-email">
                                  <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#garbage"></use></svg>
                            </span>

                            <span class="link save-user-email" style="display: none;"
                                  data-url="<?= Url::to(['/email/update-user-email', 'id' => $userEmail->id]); ?>">
                                  <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#check"></use></svg>
                            </span>
                            <span class="link undo-user-email" style="display: none;">
                                <svg class="svg-icon"><use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use></svg>
                            </span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <hr class="devider">
        <div class="dropdown-form-wrap">
            <div class="mb-2 add-new-user-email-form">
                <div class="mb-1">Добавление контакта в список по <?= $contractor->getTitle(true); ?></div>
                <div class="row flex-nowrap">
                    <div class="column flex-grow-1">
                        <?= Html::textInput('fio', null, [
                            'class' => 'form-control',
                            'id' => 'user-fio',
                            'placeholder' => 'Фамилия И.О.',
                        ]); ?>
                    </div>
                    <div class="column flex-grow-1">
                        <?= Html::input('email', 'email', null, [
                            'class' => 'form-control',
                            'id' => 'user-email',
                            'placeholder' => 'E-mail',
                        ]); ?>
                    </div>
                    <div class="column flex-shrink-0">
                        <?= Html::button($this->render('//svg-sprite', ['ico' => 'add-icon']), [
                            'class' => 'button-clr button-regular button-regular_red new-user-email-submit',
                            'for' => 'add-contact-to-contractor',
                        ]); ?>
                    </div>
                </div>
            </div>
            <br>
            <div class="row align-items-center justify-content-between">
                <div class="column">
                    <button class="add-emails button-clr button-regular button-regular_red button-width" type="button">
                        <span class="add-emails">Добавить</span>
                    </button>
                </div>
                <div class="column">
                    <button class="undo-adding-emails button-clr button-regular button-hover-transparent button-width" type="button" data-toggle="dropdown">
                        <span class="add-emails">Отменить</span>
                    </button>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
