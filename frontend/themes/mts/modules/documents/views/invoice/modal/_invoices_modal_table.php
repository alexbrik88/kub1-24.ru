<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>

<h3 class="modal-title mb-4">Добавить <?= $documentTypeName ?></h3>
<button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
    <svg class="svg-icon">
        <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
    </svg>
</button>

<div class="form-group invoice-list">
    <?= Html::beginForm(["/documents/{$documentType}/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod): ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>

            <div class="search-form-default">
                <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'document_number', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div id="range-ajax-button">
                <div
                    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                    data-pjax="get-vacant-invoices-pjax" id="reportrange2">

                    <?php
                    $this->registerJs("
                    $('#reportrange2').daterangepicker({
                        format: 'DD.MM.YYYY',
                        ranges: {" . StatisticPeriod::getRangesJs() . "},
                        startDate: '" . $dateFrom . "',
                        endDate: '" . $dateTo . "',
                        locale: {
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    });

                    "); ?>

                    <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                    <b class="fa fa-angle-down"></b>
                </div>

            </div>

        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="">
                    <?= common\components\grid\GridView::widget([
                        'id' => 'invoice-modal-grid-table',
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'invoice_id',
                                'label' => '',
                                'format' => 'raw',
                                'contentOptions' => [
                                    'style' => 'padding: 7px 5px 5px 5px;',
                                ],
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::checkbox(Html::getInputName($data, 'id') . '[]', false, [
                                        'value' => $data->id,
                                        'class' => 'checkbox-invoice-id',
                                    ]);
                                },
                                'headerOptions' => [
                                    'width' => '4%',
                                ],
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::a($data->fullNumber, [
                                            'invoice/view',
                                            'type' => $data->type,
                                            'id' => $data->id], [
                                            'data-pjax' => '0',
                                            'target' => '_blank'
                                        ]);
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'attribute' => 'total_amount_with_nds',
                                'value' => function (Invoice $model) {
                                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                },
                            ],
                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'headerOptions' => [
                                    'width' => '24%',
                                ],
                                'filter' => $searchModel->getContractorItemsByQuery($dataProvider->query),
                                'hideSearch' => false,
                                's2width' => '300px',
                                'format' => 'raw',
                                'value' => 'contractor_name_short',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="form-group" style="padding-bottom: 10px; padding-left: 5px;">
    <?= \yii\helpers\Html::checkbox('add-new-invoice', false, [
        'id' => 'add-new-invoice',
        'label' => "Создать {$documentTypeName} и новый счет",
    ]); ?>
</div>
<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('Добавить', [
        'id' => 'add-act-to-invoice',
        'class' => 'button-regular button-width button-regular_red button-clr',
        'style' => 'width: 130px!important;',
        'data-dismiss' => 'modal',
        'data-url' => Url::to([
            "/documents/{$documentType}/create",
            'type' => $ioType,
        ]),
        'data-url-new-invoice' => Url::to([
            '/documents/invoice/create',
            'type' => $ioType,
            'document' => $documentType,
        ])
    ]); ?>
    <?= Html::button('Отменить', [
        'class' => 'button-clr button-width button-regular button-hover-transparent',
        'style' => 'width: 130px!important;',
        'data-dismiss' => 'modal',
    ]); ?>
</div>