<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;
use frontend\rbac\UserRole;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\employee\EmployeeRole;
use frontend\widgets\BoolleanSwitchWidget;
use frontend\modules\documents\models\InvoiceSearch;


/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel InvoiceSearch */
/* @var $company \common\models\Company */
/* @var $useContractor boolean
 * @var $this \yii\web\View
 * @var $user \common\models\employee\Employee
 */

$period = StatisticPeriod::getSessionName();
$canView = false;
$user = Yii::$app->user->identity;
if (empty($company)) {
    $company = $user->company;
}

$userConfig = $user->config;
$isContractor = $this->context->id == 'contractor';
$tabConfig = [
    'scan' => $isContractor ? $userConfig->contr_inv_scan : $userConfig->invoice_scan,
    'paydate' => $isContractor ? $userConfig->contr_inv_paydate : $userConfig->invoice_paydate,
    'paylimit' => $isContractor ? $userConfig->contr_inv_paylimit : $userConfig->invoice_paylimit,
    'act' => $isContractor ? $userConfig->contr_inv_act : $userConfig->invoice_act,
    'paclist' => $isContractor ? $userConfig->contr_inv_paclist : $userConfig->invoice_paclist,
    // 'waybill' => $isContractor ? $userConfig->contr_inv_waybill : $userConfig->invoice_waybill,
    'invfacture' => $isContractor ? $userConfig->contr_inv_invfacture : $userConfig->invoice_invfacture,
    'upd' => $isContractor ? $userConfig->contr_inv_upd : $userConfig->invoice_upd,
    'author' => $isContractor ? $userConfig->contr_inv_author : $userConfig->invoice_author,
    'comment' => $isContractor ? $userConfig->contr_inv_comment : $userConfig->invoice_comment,
];

if ($type != Documents::IO_TYPE_OUT) // todo: ТТН для входящих счетов
    $tabConfig['waybill'] = false;

$existsQuery = Invoice::find()->byCompany($company->id)->byDeleted(false)->byIOType($type);
if (isset($id)) {
    $existsQuery->byContractorId($id);
}
$exists = $existsQuery->exists();

$isFilter = (boolean)($searchModel->byNumber);

if ($exists) {
    if ($isFilter) {
        $emptyMessage = "По вашему запросу, в выбранном периоде «{$period}», ничего не найдено. Измените период, и попробуйте еще раз.";
    } else {
        $emptyMessage = "В выбранном периоде «{$period}», у вас нет счётов. Измените период, чтобы увидеть имеющиеся счета.";
    }
} else {
    if ($type == Documents::IO_TYPE_OUT) {
        $emptyMessage = 'Вы еще не создали ни одного счёта. ' . Html::a('Создать счёт.', [
                '/documents/invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } elseif ($type == Documents::IO_TYPE_IN) {
        $emptyMessage = 'Вы еще не загрузили ни одного счета от поставщика. ' . Html::a('Загрузить счёт.', [
                '/documents/invoice/create',
                'type' => $type,
                'contractorId' => isset($id) ? $id : null,
            ]);
    } else {
        $emptyMessage = 'Ничего не найдено';
    }
}

$useContractor = isset($useContractor) ? $useContractor : false;
$docsTypeUpd = $company->isDocsTypeUpd;

if ($company->companyTaxationType->osno) {
    $isInvoiceFactureVisible = true;
} elseif ($company->companyTaxationType->usn) {
    $query = clone $dataProvider->query;
    $query->andWhere(['not', ['invoice.nds_view_type_id' => Invoice::NDS_VIEW_WITHOUT]]);

    $isInvoiceFactureVisible = $query->exists();
} else {
    $isInvoiceFactureVisible = false;
}

$statusFilterItems = [];
foreach ($searchModel->getStatusItemsByQuery($dataProvider->query) as $key => $value) {
    $statusFilterItems[$key] = $value;
    if ($key == InvoiceStatus::STATUS_PAYED) {
        foreach (Invoice::$paymentTypeData as $typeData) {
            $statusFilterItems["{$key},{$typeData['alias']}"] = Html::tag('span', $value, [
                    'style' => 'padding: 0 10px;'
                ]) . $typeData['icon'];
        }
    }
}

echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-currency',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
    ],
]); ?>
<style>
    .update-attribute-tooltip-content {
        text-align: center;
    }
</style>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-style table-count-list invoice-table',
    ],
    'rowOptions' => function (Invoice $data) {
        return $data->isAllStatusesComplete ? ['class' => 'invoice-payed'] : [];
    },
    'pager' => [
        'options' => [
            'class' => 'nav-pagination list-clr',
        ],
    ],
    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" :
        $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => false]),
    'columns' => [
        [
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center pad0',
                'width' => '2%',
            ],
            'contentOptions' => [
                'class' => 'text-center pad0-l pad0-r',
            ],
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return Html::checkbox('Invoice[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                    'data-sum' => $model->total_amount_with_nds,
                ]);
            },
        ],
        [
            'attribute' => 'document_date',
            'label' => 'Дата счёта',
            'headerOptions' => [
                //'class' => 'sorting',
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'link-view',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
            ],
            'contentOptions' => [
                'class' => 'document_number link-view',
            ],
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor) {
                if ($data->canView) {
                    $link = Html::a($data->fullNumber, ['/documents/invoice/view',
                        'type' => $data->type,
                        'id' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], ['class' => 'link']);
                } else {
                    $link = $data->fullNumber;
                }
                $fullName = '№' . $data->document_number . ' от ' . DateHelper::format(
                        $data->document_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    );

                return Html::tag('span', $link, ['data-full-name' => $fullName]);
            },
        ],
        [
            'label' => 'Скан',
            'headerOptions' => [
                'class' => 'col_invoice_scan col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'col_invoice_scan link-view col_contr_inv_scan' . ($tabConfig['scan'] ? '' : ' hidden'),
            ],
            'attribute' => 'has_file',
            'format' => 'raw',
            'value' => function ($model) {
                return DocumentFileWidget::widget(['model' => $model]);
            },
        ],
        [
            'label' => 'Сумма',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'link-view',
                'style' => 'white-space: nowrap;',
            ],
            'attribute' => 'total_amount_with_nds',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $viewPrice = TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
                $rubPrice = TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                $currency = $model->currency_name == Currency::DEFAULT_NAME ? '' : ' ' . $model->currency_name;
                $content = Html::tag('span', $viewPrice . $currency, [
                    'class' => 'price' . ($currency ? ' tooltip-currency' : ''),
                    'data' => [
                        'price' => str_replace(" ", "", $rubPrice),
                        'tooltip-content' => $currency ? '#tooltip_currency-' . $model->id : null,
                        'remaining-amount' => str_replace(" ", "", TextHelper::invoiceMoneyFormat($model->remaining_amount, 2)),
                    ],
                ]);
                if ($currency) {
                    $text = "Сумма счета в RUB: {$rubPrice}<br/>";
                    $text .= "Курс обмена: {$model->currency_amount} {$model->currency_name} = {$model->currency_rate} RUB";
                    $tooltip = Html::tag('span', $text, ['id' => 'tooltip_currency-' . $model->id]);
                    $content .= "\n" . Html::tag('div', $tooltip, ['class' => 'hidden']);
                }

                return $content;
            },
        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контр&shy;агент',
            'encodeLabel' => false,
            'contentOptions' => [
                'class' => 'contractor-cell',
                'style' => 'overflow: hidden;text-overflow: ellipsis; max-width:400px',
            ],
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '30%',
            ],
            'filter' => $searchModel->getContractorItems(),
            'hideSearch' => false,
            's2width' => '300px',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $name = Html::encode($model->contractor_name_short);
                return Html::a($name, [
                    '/contractor/view',
                    'type' => $model->type,
                    'id' => $model->contractor_id,
                ], [
                    'class' => 'link',
                    'title' => html_entity_decode($name),
                    'data' => [
                        'id' => $model->contractor_id,
                    ]
                ]);
            },
            'visible' => ($useContractor ? false : true),
        ],
        [
            'attribute' => 'invoice_status_id',
            'label' => 'Статус',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => function (Invoice $model) {
                $options = [
                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap;',
                    'class' => '',
                ];
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                    $options['class'] .= ' tooltip2';
                    $options['data-tooltip-content'] = '#tooltip_invoice-sum-' . $model->id;
                }

                return $options;
            },
            'selectPluginOptions' => [
                'templateResult' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
                'templateSelection' => new JsExpression('function(data, container) {
                    return $.parseHTML(data.text);
                }'),
            ],
            'filter' => $statusFilterItems,
            's2width' => '170px',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $tooltipAmount = '';
                if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL || ($model->invoice_status_id == InvoiceStatus::STATUS_OVERDUE && $model->remaining_amount !== null)) {
                    $tooltipAmount = '<span class="tooltip-template"  style="display:none;"><span id="tooltip_invoice-sum-' . $model->id . '">
                    <span style="color: #45b6af;">Оплачено: ' . TextHelper::invoiceMoneyFormat($model->payment_partial_amount, 2) . ' руб.</span></br>
                    <span style="color: #f3565d;">Задолженность: ' . TextHelper::invoiceMoneyFormat($model->remaining_amount, 2) . ' руб.</span>
                    </span></span>';
                }
                $icon = $model->invoice_status_id == InvoiceStatus::STATUS_PAYED ?
                    Html::tag('span', $model->getPaymentIcon(), ['style' => 'color: #0097FD; font-size: 14px;']) : '';

                return $model->invoiceStatus->name . " $icon" . $tooltipAmount;
            },
        ],
        [
            'attribute' => 'payment_limit_date',
            'format' => 'date',
            'headerOptions' => [
                'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paylimit col_contr_inv_paylimit' . ($tabConfig['paylimit'] ? '' : ' hidden'),
            ],
            'visible' => true,
        ],
        [
            'attribute' => 'payDate',
            'label' => 'Дата оплаты',
            'headerOptions' => [
                'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paydate col_contr_inv_paydate' . ($tabConfig['paydate'] ? '' : ' hidden'),
            ],
            'value' => function (Invoice $model) {
                return $model->payDate ? date('d.m.Y', $model->payDate) : '';
            },
            'visible' => true,
        ],
        [
            'attribute' => 'has_act',
            'label' => 'Акт',
            'headerOptions' => [
                'class' => 'col_invoice_act col_contr_inv_act' . ($tabConfig['act'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_act col_contr_inv_act' . ($tabConfig['act'] ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasActFilter($dataProvider->query),
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                if (!$data->need_act && $data->canAddAct) {
                    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                        return BoolleanSwitchWidget::widget([
                            'id' => 'add-act_' . $data->id,
                            'action' => [
                                '/documents/act/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ],
                            'inputName' => 'add_act',
                            'inputValue' => 1,
                            'label' => 'Выставить Акт?',
                            'addFalseForm' => false,
                            'toggleButton' => [
                                'tag' => 'span',
                                'label' => 'Без Акта',
                                'class' => 'color-link cursor-pointer',
                            ],
                            'options' => [
                                'style' => 'width: 110px;',
                            ],
                        ]);
                    } else {
                        return 'Без Акта';
                    }
                }
                foreach ($data->acts as $doc) {
                    $docLink = Html::a($doc->fullNumber, [
                        '/documents/act/view',
                        'type' => $doc->type,
                        'id' => $doc->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => $data->canView ? 'link' : 'no-rights-link',
                    ]);

                    $fileLink = DocumentFileWidget::widget([
                        'model' => $doc,
                        'cssClass' => 'pull-right',
                    ]);

                    $content .= Html::tag('div', $docLink . $fileLink);
                }
                if ($data->canAddAct) {
                    $canCreate = $data->canCreate;
                    $content .= Html::a('Добавить', [
                        '/documents/act/create',
                        'type' => $data->type,
                        'invoiceId' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                        'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                    ]);
                }

                return $content;
            },
        ],
        [
            'attribute' => 'has_packing_list',
            'label' => 'Товарная накладная',
            'headerOptions' => [
                'class' => 'col_invoice_paclist col_contr_inv_paclist' . ($tabConfig['paclist'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_paclist col_contr_inv_paclist' . ($tabConfig['paclist'] ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasPackingListFilter($dataProvider->query),
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                if (!$data->need_packing_list && $data->canAddPackingList) {
                    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                        return BoolleanSwitchWidget::widget([
                            'id' => 'add-packing-list_' . $data->id,
                            'action' => [
                                '/documents/packing-list/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ],
                            'inputName' => 'add_packing_list',
                            'inputValue' => 1,
                            'label' => 'Выставить ТН?',
                            'addFalseForm' => false,
                            'toggleButton' => [
                                'tag' => 'span',
                                'label' => 'Без ТН',
                                'class' => 'color-link cursor-pointer',
                            ],
                            'options' => [
                                'style' => 'width: 110px;',
                            ],
                        ]);
                    } else {
                        return 'Без ТН';
                    }
                } else {
                    foreach ($data->packingLists as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/packing-list/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? 'link' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddPackingList) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/packing-list/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }
                }

                return $content;
            }
        ],
        /* todo
        [
            'attribute' => 'has_waybill',
            'label' => 'Транспортная накладная',
            'headerOptions' => [
                'class' => 'col_invoice_waybill col_contr_inv_waybill' . ($tabConfig['waybill'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_waybill col_contr_inv_waybill' . ($tabConfig['waybill'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все', '1' => 'Есть ТТН', '0' => 'Нет ТТН'],
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd) {
                $content = '';
                foreach ($data->waybills as $doc) {
                    $docLink = Html::a($doc->fullNumber, [
                        '/documents/waybill/view',
                        'type' => $doc->type,
                        'id' => $doc->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => $data->canView ? 'link' : 'no-rights-link',
                    ]);

                    $fileLink = DocumentFileWidget::widget([
                        'model' => $doc,
                        'cssClass' => 'pull-right',
                    ]);

                    $content .= Html::tag('div', $docLink . $fileLink);
                }
                if ($data->canAddWaybill) {
                    $canCreate = $data->canCreate;
                    $content .= Html::a('Добавить', [
                        '/documents/waybill/create',
                        'type' => $data->type,
                        'invoiceId' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                        'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                    ]);
                }

                return $content;
            }
        ],
        */
        [
            'attribute' => 'has_invoice_facture',
            'label' => 'Счёт-фактура',
            'headerOptions' => [
                'class' => 'col_invoice_invfacture col_contr_inv_invfacture' . ($tabConfig['invfacture'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_invfacture col_contr_inv_invfacture' . ($tabConfig['invfacture'] ? '' : ' hidden'),
            ],
            'filter' => ['' => 'Все', '1' => 'Есть СФ', '0' => 'Нет СФ'],
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd) {
                $content = '';

                if (!$data->hasNds) {
                    return $content;
                }
                /* @var $doc InvoiceFacture */
                foreach ($data->invoiceFactures as $doc) {
                    $docLink = Html::a($doc->fullNumber, [
                        '/documents/invoice-facture/view',
                        'type' => $doc->type,
                        'id' => $doc->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => $data->canView ? 'link' : 'no-rights-link',
                    ]);

                    $fileLink = DocumentFileWidget::widget([
                        'model' => $doc,
                        'cssClass' => 'pull-right',
                    ]);

                    $content .= Html::tag('div', $docLink . $fileLink);
                }
                if ($data->canAddInvoiceFacture) {
                    $canCreate = $data->canCreate;
                    $content .= Html::a('Добавить', [
                        '/documents/invoice-facture/create',
                        'type' => $data->type,
                        'invoiceId' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ], [
                        'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                        'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                    ]);
                }

                return $content;
            },
            'visible' => $isInvoiceFactureVisible,
        ],
        [
            'attribute' => 'has_upd',
            'label' => 'УПД',
            'headerOptions' => [
                'class' => 'col_invoice_upd col_contr_inv_upd' . ($tabConfig['upd'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:100px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_upd col_contr_inv_upd' . ($tabConfig['upd'] ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getHasUpdFilter($dataProvider->query),
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor, $docsTypeUpd, $user) {
                $content = '';
                if (!$data->need_upd && $data->canAddUpd) {
                    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                        return BoolleanSwitchWidget::widget([
                            'id' => 'add-upd_' . $data->id,
                            'action' => [
                                '/documents/upd/create',
                                'type' => $data->type,
                                'invoiceId' => $data->id,
                                'contractorId' => ($useContractor ? $data->contractor_id : null),
                            ],
                            'inputName' => 'add_upd',
                            'inputValue' => 1,
                            'label' => 'Выставить УПД?',
                            'addFalseForm' => false,
                            'toggleButton' => [
                                'tag' => 'span',
                                'label' => 'Без УПД',
                                'class' => 'color-link cursor-pointer',
                            ],
                            'options' => [
                                'style' => 'width: 115px;',
                            ],
                        ]);
                    } else {
                        return 'Без УПД';
                    }
                } else {
                    /* @var $doc Upd */
                    foreach ($data->upds as $doc) {
                        $docLink = Html::a($doc->fullNumber, [
                            '/documents/upd/view',
                            'type' => $doc->type,
                            'id' => $doc->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => $data->canView ? 'link' : 'no-rights-link',
                        ]);

                        $fileLink = DocumentFileWidget::widget([
                            'model' => $doc,
                            'cssClass' => 'pull-right',
                        ]);

                        $content .= Html::tag('div', $docLink . $fileLink);
                    }
                    if ($data->canAddUpd) {
                        $canCreate = $data->canCreate;
                        $content .= Html::a('Добавить', [
                            '/documents/upd/create',
                            'type' => $data->type,
                            'invoiceId' => $data->id,
                            'contractorId' => ($useContractor ? $data->contractor_id : null),
                        ], [
                            'class' => 'button-regular button-regular_padding_bigger button-hover-content-red' . ($data->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                            'style' => ($data->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin-top: 5px;' : ''),
                        ]);
                    }
                }

                return $content;
            },
        ],

        [
            'attribute' => 'responsible_employee_id',
            'label' => 'От&shy;вет&shy;ствен&shy;ный',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:150px'
            ],
            'contentOptions' => [
                'class' => 'col_invoice_author col_contr_inv_author' . ($tabConfig['author'] ? '' : ' hidden'),
            ],
            'filter' => $searchModel->getResponsibleEmployeeByQuery($dataProvider->query),
            's2width' => '200px',
            'format' => 'raw',
            'value' => function (Invoice $model) {
                return $model->author ? $model->author->getFio(true) : '';
            },
            'visible' => !Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only,
        ],
        [
            'attribute' => 'comment_internal',
            'label' => 'Ком&shy;мен&shy;та&shy;рий',
            'encodeLabel' => false,
            'headerOptions' => [
                'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                'width' => '10%',
                'style' => 'min-width:150px'
            ],
            'contentOptions' => function (Invoice $model) use ($tabConfig) {
                $options = [
                    'class' => 'col_invoice_comment col_contr_inv_comment' . ($tabConfig['comment'] ? '' : ' hidden'),
                    'style' => 'overflow: hidden;text-overflow: ellipsis; white-space: nowrap; max-width:90px;',
                ];
                if ($model->comment_internal) {
                    $options['class'] .= ' tooltip2';
                    $options['data-tooltip-content'] = "#tooltip_invoice_comment_{$model->id}";
                }

                return $options;
            },
            'format' => 'raw',
            'value' => function (Invoice $model) {
                $tooltip = '';
                if ($model->comment_internal) {
                    $tooltip = '
                    <span class="tooltip-template" style="display:none;">
                        <span id="tooltip_invoice_comment_' . $model->id . '">
                        ' . $model->comment_internal . '
                        </span>
                    </span>';
                }

                return $model->comment_internal . $tooltip;
            },
        ],
    ],
]);

echo $this->render('../invoice-facture/_form_create', [
    'useContractor' => $useContractor,
]);

Modal::begin([
    'id' => 'no-rights-modal',
    'closeButton' => false,
    'toggleButton' => false,
]);
echo Html::button('×', ['class' => 'close', 'type' => 'button', 'data-dismiss' => 'modal', 'aria-hidden' => 'true']);
echo Html::tag('span', 'У Вас нет прав на данное действие.', ['class' => 'center-block text-center']);
Modal::end();

$contractorDuplicateLink = Url::to(['/contractor/index', 'type' => $searchModel->type, 'ContractorSearch' => [
    'duplicate' => 1,
]]);
$this->registerJs('
    $(document).on("click", "a.no-rights-link", function(e) {
        e.preventDefault();
        $("#no-rights-modal").modal("show");
    });
    $(".act-file-link-preview, .packing-list-file-link-preview, .invoice-facture-file-link-preview, .upd-file-link-preview").tooltipster({
        theme: ["tooltipster-kub"],
        contentCloning: true,
        trigger: "hover",
        side: "left",
    });
    $(document).on("change", "#invoicesearch-contractor_id", function (e) {
        if ($(this).val() == "duplicate") {
            location.href = "' . $contractorDuplicateLink . '";
        }
    });

    $("input:checkbox:not(.md-check)").uniform(\'refresh\');
');
?>
