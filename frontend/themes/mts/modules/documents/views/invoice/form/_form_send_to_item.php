<?php

use frontend\modules\documents\models\AutoinvoiceForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $autoinvoice frontend\modules\documents\models\AutoinvoiceForm */
/* @var $index integer */
/* @var $label string */
/* @var $name string */
/* @var $checked boolean */
/* @var $value string */

$name = ArrayHelper::getValue($model, ['contractor', $value . '_name']);
$email = ArrayHelper::getValue($model, ['contractor', $value . '_email']);
$director = $value == AutoinvoiceForm::TO_DIRECTOR;
$isDirectorClass = !$director && $isDirector && !$checked ? '' : ' hidden';
$noDirectorClass = !$director && $isDirector && !$checked ? ' hidden' : '';
?>

<?= Html::tag('b', $label) ?>
<?php /*if (!$director) : ?>
    <span class="glyphicon glyphicon-pencil toggle-is_director-btn<?= $isDirector ? '' : ' hidden' ?>"
        data-target=".send_to_<?=$value ?> .toggle-is_director"
        style="cursor: pointer;"></span>
<?php endif*/ ?>

<div class="send_to send_to_<?=$value ?>" style="padding-left: 25px;">
    <?php if (!$director) : ?>
        <div class="toggle-is_director yes <?= $isDirectorClass ?>">
            Совпадает с руководителем
        </div>
    <?php endif ?>
    <div class="toggle-is_director no <?= $noDirectorClass ?>">
        <div class="<?= $value ?>_name">
            <?= Html::tag('span', $name, [
                'class' => 'to_name' . ($name ? '' : ' hidden'),
            ]) ?>
            <?= Html::activeTextInput($autoinvoice, $value . '_name', [
                'placeholder' => 'ФИО',
                'class' => ($name ? 'hidden ' : '') . 'form-control',
                'style' => 'width: 100%; margin-bottom: 5px;',
                'disabled' => !empty($name),

            ]) ?>
        </div>
        <div class="<?= $value ?>_email">
            <?= Html::tag('span', $email, [
                'class' => 'to_email' . ($email ? '' : ' hidden'),
            ]) ?>
            <?= Html::activeTextInput($autoinvoice, $value . '_email', [
                'placeholder' => 'E-mail',
                'class' => ($email ? 'hidden ' : '') . 'form-control',
                'style' => 'width: 100%;',
                'disabled' => !empty($email),
            ]) ?>
        </div>
    </div>
</div>
