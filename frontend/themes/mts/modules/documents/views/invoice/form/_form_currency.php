<?php

use common\models\currency\Currency;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('span', $model->currency_name, ['class' => 'currency_name']);
$currencyAmount = Html::tag('span', $model->currency_amount, ['class' => 'currency_amount currency_rate_amount']);
$currencyRate = Html::tag('span', $model->currency_rate, ['class' => 'currency_rate currency_rate_value']);

$rateText = "{$currencyAmount} {$currencyName} = {$currencyRate} RUB";

$radioName = Html::getInputName($model, 'currency_rate_type');
$radioId = Html::getInputId($model, 'currency_rate_type');

$isHidden = $model->currency_name == Currency::DEFAULT_NAME;
$collapseState = $isHidden ? '' : 'show';

$this->registerJs("
    $('#invoice-currencyratedate').datepicker({
        format: 'dd.mm.yyyy',
        language:'ru',
        autoclose: true,
        endDate: new Date(),
    });
    $('.tooltip_currency_rate').click(function(event) {
        event.stopPropagation();
    });
");

$help = Html::tag('span', '', [
    'class' => 'tooltip_currency_rate ico-question valign-middle',
    'style' => 'position: absolute; top: -6px; right: -26px;',
    'data-tooltip-content' => '#tooltip_currency_rate',
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip_currency_rate',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>


<div class="col-6 col-xl-3 mb-3">
    <div class="form-filter mb-4">
        <div class="mb-2">
            <span>Валюта счета</span>
            <div class="tooltip-box tooltip-box_side-margins">
                <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title="Выставляйте счет в любой валюте. Выберите валюту из списка.">
                    <svg class="tooltip-question-icon svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use>
                    </svg>
                </button>
            </div>
        </div>

        <?= $form->field($model, 'currency_name', [
            'template' => "{label}\n{input}",
            'options' => ['class' => ''],
            'labelOptions' => [
                'class' => '',
                'style' => '',
            ],
        ])->label(false)->widget(Select2::class, [
            'data' => Currency::find()
                ->select(['CONCAT([[label]], ", ", [[name]])'])
                ->orderBy(['sort' => SORT_ASC])
                ->indexBy('name')->column(),
            'pluginOptions' => [
                'width' => '100%',
                'minimumResultsForSearch' => 10,
            ],
            'options' => [
                'data' => [
                    'default' => Currency::DEFAULT_NAME,
                    'initial' => $model->currency_name,
                ]
            ]
        ]); ?>

    </div>
    <div class="form-filter mb-4 currency-view-toggle collapse <?= $collapseState ?>">
        <div class="mb-2">
            <span>Обменный курс</span>
            <div class="tooltip-box tooltip-box_side-margins">
                <button type="button" class="button-clr" data-toggle="tooltip" data-placement="bottom" title="Установите обменный курс для данного счета.">
                    <svg class="tooltip-question-icon svg-icon">
                        <use xlink:href="/images/svg-sprite/svgSprite.svg#question"></use>
                    </svg>
                </button>
            </div>
        </div>

        <?= Html::A("$rateText <span class=\"caret\"></span>", '#currency-rate-box', [
            'class' => 'collapse-toggle-link currency-rate-link',
            'style' => 'position: relative;',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'false',
            'aria-controls' => 'currency-rate-box',
        ]) ?>
        <?= Html::activeHiddenInput($model, 'currency_amount', [
            'class' => 'currency_amount currency_rate_amount',
            'data' => [
                'initial' => $model->currency_amount,
            ],
        ]) ?>
        <?= Html::activeHiddenInput($model, 'currency_rate', [
            'class' => 'currency_rate currency_rate_value',
            'data' => [
                'initial' => $model->currency_rate,
            ],
        ]) ?>

    </div>
</div>

<?= $this->render('/invoice/_set_currency', ['model' => $model, 'collapseState' => $collapseState]) ?>