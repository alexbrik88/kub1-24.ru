<?php

use common\models\Company;
use common\models\document\Invoice;
use frontend\rbac\permissions\document\Document;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
/* @var $isAuto boolean */

$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');
$document = Yii::$app->request->get('document');

$cancelUrl = Url::previous('lastPage');
if (!$isAuto && Yii::$app->request->get('document') == 'proxy') {
    $cancelUrl = ['/documents/proxy/index', 'type' => $ioType];
}
?>

<div class="wrap wrap_btns check-condition visible mb-0">
    <div class="row align-items-center justify-content-between">
        <div class="column">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'button-width button-clr button-regular button-regular_red ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <?php if ($model->isNewRecord) : ?>
            <div class="column">
                <?= Html::button('АвтоСчет', [
                    'class' => 'button-width button-clr button-regular button-hover-grey autoinvoice '.$invoiceBlock,
                ]); ?>
            </div>
        <?php endif ?>
        <div class="column ml-auto">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'button-width button-clr button-regular button-hover-grey',
            ]); ?>
        </div>
    </div>
</div>
