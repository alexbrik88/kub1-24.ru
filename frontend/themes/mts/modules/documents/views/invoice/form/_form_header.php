<?php

use common\components\date\DateHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap4\Dropdown;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $form ActiveForm */
/* @var $isAuto boolean */
/* @var $document string */

$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');

if (!$model->document_date)
    $model->document_date = date('Y-m-d');
?>

<?= Html::hiddenInput('', (int)$model->isNewRecord, [
    'id' => 'isNewRecord',
]) ?>
<?= Html::activeHiddenInput($model, 'is_invoice_contract'); ?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span class="<?= $invoiceBlock ?>">
                        <span>
                            <?php if ($document == Documents::SLUG_ACT): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий акт' : 'Входящий акт' ?>
                            <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая ТН' : 'Входящая ТН' ?>
                            <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящая СФ' : 'Входящая СФ' ?>
                            <?php elseif ($document == Documents::SLUG_UPD): ?>
                                <?= ($ioType == Documents::IO_TYPE_OUT) ? 'Исходящий УПД' : 'Входящий УПД' ?>
                            <?php else: ?>
                            <?php if (!$create && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT): ?>
                                  Исходящий
                                <?php elseif (!$create && $model->type == Documents::IO_TYPE_OUT && $model->is_invoice_contract): ?>
                                    Исходящий счет-договор
                                <?php else: ?>
                                    <?= $message->get(Message::TITLE_SINGLE); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </span>
                        <?php if (!$document && !$create && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT): ?>
                            <div class="form-title-dropdown dropdown d-inline-block">
                                <?= Html::a($model->is_invoice_contract ? 'счет-договор' : 'счет', '#', [
                                    'id' => 'countDropdown',
                                    'data-toggle' => 'dropdown',
                                    'aria-haspopup' => true,
                                    'aria-expanded' => false,
                                    'role' => 'button',
                                    'class' => 'form-title-link',
                                    'style' => ''
                                ]); ?>
                                <div class="form-filter-drop dropdown-menu" aria-labelledby="countDropdown">
                                    <ul class="form-filter-list list-clr">
                                        <li><a id="create-invoice" href="javascript:;" class="<?=($model->is_invoice_contract ? null : 'active')?>">счет</a></li>
                                        <li><a id="create-invoice-contract" href="javascript:;" class="<?=($model->is_invoice_contract ? null : 'active')?>">счет-договор</a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    </span>

                    <?php if ($autoinvoice) : ?>
                        <span class="<?= $autoinvoiceBlock ?>">
                                <span>
                                <?php if ($model->isNewRecord && $model->type == Documents::IO_TYPE_OUT): ?>
                                    Шаблон
                                    <div class="form-title-dropdown dropdown d-inline-block">
                                        <?= Html::a($model->is_invoice_contract ? 'автосчёт-договор' : 'автосчёта', '#', [
                                            'id' => 'countDropdown',
                                            'data-toggle' => 'dropdown',
                                            'aria-haspopup' => true,
                                            'aria-expanded' => false,
                                            'role' => 'button',
                                            'class' => 'form-title-link',
                                            'style' => ''
                                        ]); ?>
                                        <div class="form-filter-drop dropdown-menu" aria-labelledby="countDropdown">
                                            <ul class="form-filter-list list-clr">
                                                <li><a id="create-invoice" href="javascript:;" class="<?=($model->is_invoice_contract ? null : 'active')?>">автосчет</a></li>
                                                <li><a id="create-invoice-contract" href="javascript:;" class="<?=($model->is_invoice_contract ? null : 'active')?>">автосчет-договор</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                <?php elseif ($model->type == Documents::IO_TYPE_OUT && $model->is_invoice_contract): ?>
                                    Шаблон автосчёт-договора
                                <?php else: ?>
                                    Шаблон автосчёта
                                <?php endif; ?>
                                </span>
                            </span>
                    <?php endif ?>
                    <span>№</span>
                </div>

                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2 <?=($invoiceBlock)?>">
                    <?= Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <?php if ($autoinvoice): ?>
                <div class="form-group d-inline-block mb-0 col-xl-2 <?=($autoinvoiceBlock)?>">
                    <?= Html::activeTextInput($autoinvoice, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <?php endif; ?>

                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2 <?=($invoiceBlock)?>">
                <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                        'placeholder' => 'доп. номер',
                        'style' => 'display: ' . ($isAuto ? 'none' : 'inline') . ';',
                    ]); ?>
                <?php endif ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($model->type == Documents::IO_TYPE_OUT && $model->isNewRecord && !$create && !$document) : ?>
            <div class="column">
                <a class="link link_nowrap tooltip3" data-tooltip-content="#invoice-contract-tooltip" href="#">
                    <span class="<?=($invoiceBlock)?>">Нужен счет-договор?</span>
                    <span class="<?=($autoinvoiceBlock)?>">Нужен автосчет-договор?</span>
                </a>
            </div>
        <?php endif; ?>
    </div>
</div>