<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */
/* @var $useContractor string */

//$addStamp = 1;

TooltipAsset::register($this);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$company = $model->company;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$isAuto = $model->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE;
$totalSum = $model->view_total_with_nds;
?>

<style type="text/css">
    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }
    .pre-view-table h3 {
        text-align: center;   margin: 6px 0 5px;  font-weight:bold;
    }
    .auto_tpl {
        font-style: italic; font-weight: normal;
    }
    .m-size-div div {
        font-size: 12px;
    }
    .m-size-div div span {
        font-size: 9px;
    }
    .file_upload_block {
        text-align: center;  border: 2px dashed #ffb848; color: #ffb848;  padding: 12px;
    }
    .no_min_h{
        min-height: 0px !important;
    }
</style>

    <div class="pre-view-box" style="min-width: 520px; max-width:735px; margin-top:3px;">
        <div class="col-xs-12 pad5 pre-view-table">
            <div class="col-xs-12 pad3" style="height: 80px;">
                <div class="col-xs-6 pad0 font-bold" style="height: inherit">
                    <?= $this->render('/invoice/view/_actions', [
                        'model' => $model,
                        'ioType' => $ioType,
                        'useContractor' => $useContractor,
                    ]) ?>

                    <div class="col-xs-12 pad0" style="padding-top: 22px !important;">
                        <p style="font-size: 17px"><?= $model->contractor_name_short; ?></p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 pad3" style="padding-top: 10px !important;">
                <div class="col-xs-12 pad0 bord-dark"
                     style="height: 60px; border-bottom: 0px">
                    <div class="col-xs-6 pad0">
                        <div class="col-xs-12 pad1" style="height: 40px;">
                            <div class="col-xs-12 pad1 v_middle" style="height: inherit">
                                <?= $model->contractor->bank_name; ?>
                            </div>
                        </div>


                        <div class="col-xs-12 pad1"> Банк получателя</div>
                    </div>
                    <div class="col-xs-2 pad0 bord-dark-r bord-dark-l"
                         style="height: inherit;">
                        <div class="col-xs-12 pad1"> БИК </div>
                        <div class="col-xs-12 pad1 bord-dark-t"
                             style="padding-top: 5px"> Сч. №
                        </div>
                    </div>
                    <div class="col-xs-4 pad0">
                        <div
                            class="col-xs-12 pad1">  <?= $model->contractor->BIC; ?> </div>
                        <div class="col-xs-12 pad1"
                             style="padding-top: 5px">  <?= $model->contractor->corresp_account; ?> </div>
                    </div>
                </div>

                <div class="col-xs-12 pad0 bord-dark" style="height: 82px;">
                    <div class="col-xs-6 pad0 full_h">
                        <div class="col-xs-12 pad0">
                            <div class="col-xs-6 pad1 bord-dark-r">
                                ИНН <?= $model->contractor->ITN; ?> </div>
                            <div class="col-xs-6 pad1">
                                КПП <?= $model->contractor->PPC; ?> </div>
                        </div>

                        <div class="col-xs-12 pad0 bord-dark-t">
                            <div class="col-xs-12 pad1" style="height: 41px"> <?= $model->contractor_name_short; ?> </div>
                            <div class="col-xs-12 pad1" style="padding-top: 5px">
                                Получатель
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 pad0 bord-dark-r bord-dark-l full_h">
                        Сч. №
                    </div>
                    <div class="col-xs-4 pad0 full_h">
                        <div
                            class="col-xs-12 pad1">  <?= $model->contractor->current_account; ?> </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 pad0"
                 style="padding-top: 5px !important; border-bottom: 2px solid #000000">
                <h3 style="marg"> Счет
                    № <?= $isAuto ? '<i class="auto_tpl">##</i>' : $model->fullNumber; ?>
                    от <?= $isAuto ? $model->auto->day . ' <i class="auto_tpl">месяц</i> 20<i class="auto_tpl">##</i> г.' : $dateFormatted; ?>
                </h3>
            </div>

            <div class="col-xs-12 pad3" style="padding-top: 15px !important;">
                <div class="col-xs-12 pad0">
                    <div class="col-xs-2 pad0"> Поставщик <br/> (Исполнитель):</div>
                    <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                        <?= $model->getExecutorFullRequisites(); ?>
                    </div>
                </div>
                <div class="col-xs-12 pad0" style="padding-top: 5px !important;">
                    <div class="col-xs-2 pad0"> Покупатель<br/>(Заказчик):</div>
                    <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                        <?= $model->getCustomerFullRequisites(); ?>
                    </div>
                </div>

                <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                    <div class="col-xs-12 pad0"
                         style="padding-top: 5px !important;">
                        <div class="col-xs-2 pad0"> Основание:</div>
                        <div class="col-xs-10 pad3" style="font-weight: bold;">
                            <?= $model->basis_document_name ?>
                            № <?= Html::encode($model->basis_document_number) ?>
                            от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>

            <div class="col-xs-12 pad3">
                <?= $this->render('view/_pre_view_orders_table', [
                    'model' => $model,
                    'ioType' => $ioType,
                    'isAuto' => $isAuto,
                ]); ?>
            </div>

            <div class="col-xs-12 pad0 m-t-n" style="border-bottom: 2px solid #000000;">
                <div class="col-xs-12 pad3">
                    <div class="txt-9">
                        Всего наименований <?= count($model->orders) ?>, на сумму
                        <?= TextHelper::invoiceMoneyFormat($totalSum, 2); ?>
                        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
                    </div>
                    <div class="txt-9-b text-bold">
                        <?= TextHelper::mb_ucfirst(Currency::textPrice($totalSum / 100, $model->currency_name)); ?>
                    </div>
                    <?php if ($model->has_weight): ?>
                        <div class="txt-9">
                            Общий вес:
                            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalWeight($model), $model->weightPrecision) ?> кг
                        </div>
                    <?php endif; ?>
                    <?php if ($model->has_volume): ?>
                        <div class="txt-9">
                            Общий объем:
                            <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalVolume($model), $model->volumePrecision) ?> м2
                        </div>
                    <?php endif; ?>

                    <?php if ($model->comment) : ?>
                        <div class="txt-9" style="padding-top: 10px;">
                            <?= nl2br(Html::encode($model->comment)) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-xs-12 pad3">
                <?php if ($model->contractor->companyTypeId != CompanyType::TYPE_IP): ?>
                    <!-- Руководитель -->
                    <div class="col-xs-12 pad0" style="padding-top: 10px !important; z-index: 2;">
                        <div class="col-xs-12 pad0" style="height: 65px; text-align: center">
                            <div class="col-xs-3 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Руководитель:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div> <?= $model->contractor->director_post_name; ?> </div>
                            </div>
                            <div class="col-xs-3 pad0 v_bottom">
                                <div>

                                </div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div>
                                   <?=$model->contractor->director_name?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0" style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-3 pad3"> &nbsp; </div>
                            <div class="col-xs-3 pad3"> <div class="bord-dark-t"> <span> должность </span> </div> </div>
                            <div class="col-xs-3 pad3"> <div class="bord-dark-t"> <span> подпись </span></div></div>
                            <div class="col-xs-3 pad3"> <div class="bord-dark-t"> <span> расшифровка подписи </span>  </div></div>
                        </div>
                    </div>
                    <!-- Руководитель.end -->
                    <!-- Главбух -->
                    <div class="col-xs-12 pad0" style="padding-top: 0px !important;">
                        <div class="col-xs-12 pad0" style="height: 52px;text-align: center;">
                            <div class="col-xs-5 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Главный (старший) бухгалтер:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom" style="z-index: 2">
                                <div>

                                </div>
                            </div>
                            <div class="col-xs-4 pad3 v_bottom">
                                <div>
                                    <?php if ($model->contractor->chief_accountant_is_director):?>
                                        <?=$model->contractor->director_name?>
                                    <?php else: ?>
                                        <?=$model->contractor->chief_accountant_name?>
                                    <?php endif ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 pad0" style="z-index: 2;padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-5 pad3"> &nbsp; </div>
                            <div class="col-xs-3 pad3"> <div class="bord-dark-t"> <span> подпись </span></div></div>
                            <div class="col-xs-4 pad3"> <div class="bord-dark-t"> <span> расшифровка подписи </span>  </div></div>
                        </div>
                    </div>
                    <!-- Главбух.end -->
                <?php else: ?>
                    <div class="m-t-xl"
                         style="height: 200px; -webkit-print-color-adjust: exact;">
                        <div class="col-xs-12 pad0">
                            <br/>
                            <table class="podp-r"
                                   style="margin-top: -20px; width: 675px; -webkit-print-color-adjust: exact;">
                                <tr>
                                    <td class="txt-9-b"
                                        style="padding: 0 0 0 0;width: 20%; vertical-align: bottom;">
                                        Предприниматель
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 35%; padding-top: 48px!important;"></td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; vertical-align: bottom; margin: 0 0 0 0; width: 41%;">
                                        <?=$model->contractor->director_name?>
                                    </td>
                                </tr>
                                <tr class="small-txt">
                                    <td style="padding: 0 0 0 0;width: 20%"></td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        подпись
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        расшифровка подписи
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
