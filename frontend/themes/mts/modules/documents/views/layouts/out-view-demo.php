<?php

/* @var $this \yii\web\View */
/* @var $content string */

$this->params['isDemo'] = true;

$this->beginContent('@frontend/modules/documents/views/layouts/out-view.php');
?>

<style type="text/css">
#background-example-block {
    position: absolute;
    z-index: 0;
    display: block;
    min-height: 50%;
    min-width: 50%;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
    bottom: 10%;
    top: unset;
}
#background-example-block > p#example-text {
    color: red;
    font-size: 151px;
    transform: rotate(315deg);
    -webkit-transform: rotate(315deg);
    opacity: 0.4;
    text-align: center;
    padding-right: 130px;
}
</style>

<?= $content ?>

<?php $this->endContent(); ?>
