<?php

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\themes\mts\assets\MtsAsset;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

MtsAsset::register($this);

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$canSave = ArrayHelper::getValue($this->params, 'canSave', false);
$canPay = ArrayHelper::getValue($this->params, 'canPay', false);
$invoice = ArrayHelper::getValue($this->params, 'invoice');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/favicon_red.ico" type="image/x-icon">
    <?= $this->render('_out-view-style') ?>
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<div class="wrapper__in">
    <?= $this->render('out_view/menu') ?>

    <?= NotificationFlash::widget([
            'options' => [
                'closeButton' => true,
                'showDuration' => 1000,
                'hideDuration' => 1000,
                'timeOut' => 5000,
                'extendedTimeOut' => 1000,
                'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
            ],
    ]); ?>

    <div class="mt-4">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
