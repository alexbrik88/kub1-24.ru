<?php //todo: temp for new agreements
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if (in_array(Yii::$app->controller->id, ['agreement-template', 'agreement'])) : ?>
    <div class="btn-group justify-content-end">
        <?php if (\Yii::$app->controller->id == 'agreement-template') : ?>
            <?= Html::a('Договоры', ['/documents/agreement/', 'type' => $type], [
                'class' => 'button-regular button-hover-content-red w-100',
            ]) ?>
        <?php else : ?>
            <?= Html::a('Шаблоны', ['/documents/agreement-template/', 'type' => $type], [
                'class' => 'button-regular button-hover-content-red w-100',
            ]) ?>
        <?php endif ?>
    </div>
<?php endif; ?>