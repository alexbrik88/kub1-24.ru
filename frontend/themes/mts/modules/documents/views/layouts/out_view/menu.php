<?php

use common\models\Contractor;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$logoUrl  = Url::to(['/documents/invoice/index', 'type' => 2]);
$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$isDemo = ArrayHelper::getValue($this->params, 'isDemo', false);
?>

<header class="header">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end" style="min-height: 40px;">
            <a class="header-logo logo column mr-auto" href="<?= $logoUrl ?>" style="padding-right:0;">
                <red-egg-svg>
                    <svg style="width: 4.0625rem; fill:#E30611;" class="red-egg-svg" viewBox="0 0 65 16" xmlns="http://www.w3.org/2000/svg">
                        <g transform="translate(0.000000, -25.000000)">
                            <path d="M5.714336,25 C6.903696,25 8.31736,26.146696 9.49392,28.074544 C10.70376,30.070936 11.42864,32.542568 11.42864,34.68896 C11.42864,37.8312 9.66104,41 5.714336,41 C1.764056,41 8.52651283e-14,37.8312 8.52651283e-14,34.68896 C8.52651283e-14,32.542568 0.7253792,30.070936 1.940952,28.074544 C3.107304,26.146696 4.521608,25 5.714336,25 Z M48.8568,25.857152 L48.8568,29.714192 L44.8568,29.714192 L44.8568,40.1428 L40.57104,40.1428 L40.57104,29.714192 L36.57104,29.714192 L36.57104,25.857152 L48.8568,25.857152 Z M64.28536,25.857152 L64.28536,29.71436 L57.9992,29.71436 C55.3268,29.71436 53.5692,30.66972 53.5692,33.00008 C53.5692,35.2597842 55.2218927,36.2265968 57.7587981,36.2831165 L57.9992,36.28576 L64.28536,36.28576 L64.28536,40.14296 L57.99976,40.14296 C52.22408,40.14296 49.14064,37.27152 49.14064,33.00008 C49.14064,28.809194 52.1088215,25.9660069 57.6757126,25.8602088 L57.99976,25.857152 L64.28536,25.857152 Z M23.28576,25.857152 L25.57064,34.38416 L27.85544,25.857152 L35.42888,25.857152 L35.42888,40.14296 L31.14296,40.14296 L31.14296,28.515592 L28.0276,40.14296 L23.1136,40.14296 L20.00024,28.522984 L20.00008,40.14296 L15.71448,40.14296 L15.71448,25.857152 L23.28576,25.857152 Z" id="Combined-Shape"></path>
                        </g>
                    </svg>
                </red-egg-svg>
                <span class="logo-txt">
                    Выставление счетов
                </span>
            </a>

            <ul class="menu-nav list-clr">
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в PDF', $isDemo ? null : [
                        "/documents/{$controllerId}/download",
                        'type' => 'pdf',
                        'uid' => $uid
                    ], [
                        'class' => 'menu-nav-link',
                        'target' => '_blank',
                    ]) ?>
                </li>
                <li class="menu-nav-item">
                    <?= Html::a('Скачать в WORD', $isDemo ? null : [
                        "/documents/{$controllerId}/download",
                        'type' => 'docx',
                        'uid' => $uid
                    ], [
                        'class' => 'menu-nav-link',
                        'target' => '_blank',
                    ]) ?>
                </li>
                <?php if ($this->params['canSave']) : ?>
                    <li class="menu-nav-item">
                        <?= Html::a('Сохранить в личном кабинете', $isDemo ? null : '#', [
                            'id' =>  $isDemo ? null : 'document-save-link',
                            'class' => 'menu-nav-link',
                        ]) ?>
                    </li>
                <?php endif ?>
                <li class="menu-nav-item">
                    <?= Html::a('Оплатить счет', $isDemo ? null : '#', [
                        'id' =>  $isDemo ? null : 'document-pay-link',
                        'class' => 'menu-nav-link',
                    ]) ?>
                </li>
            </ul>
        </div>
    </div>
</header>