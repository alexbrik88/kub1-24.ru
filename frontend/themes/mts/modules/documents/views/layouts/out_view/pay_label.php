<div class="menu_label_wrapper">
  <div class="menu_label_inner">
    <div class="menu_label_icon">
      <div class="icon_circle">
        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
          <defs>
            <style>
              .cls-2 {
                fill: #3a8bf7;
                fill-rule: evenodd;
              }
            </style>
          </defs>
          <path class="cls-2" d="M1425.15,79.363h-36.32a1.819,1.819,0,0,0,0,3.638h36.32A1.819,1.819,0,1,0,1425.15,79.363Zm-34.05-4.728a1.819,1.819,0,0,0,0,3.638h31.78a1.819,1.819,0,1,0,0-3.638h-0.45V58.27h0.45a0.909,0.909,0,1,0,0-1.817H1391.1a0.909,0.909,0,1,0,0,1.817h0.46V74.634h-0.46Zm27.7-16.366V74.634h-5.45V58.27h5.45Zm-9.08,0V74.634h-5.45V58.27h5.45Zm-14.53,0h5.45V74.634h-5.45V58.27Zm-6.36-3.452h36.34a1.819,1.819,0,0,0,.59-3.54l-18.02-8.119a1.818,1.818,0,0,0-1.49,0l-18.16,8.182A1.819,1.819,0,0,0,1388.83,54.818Z" transform="translate(-1387 -43)"/>
        </svg>
      </div>
    </div>

    <div class="menu_label_content">
      <div class="content_part">
        Оплатить
      </div>
      <div class="content_part">
        счет
      </div>
    </div>
  </div>
</div>
