<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use frontend\modules\documents\components\DocConverter;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
/* @var $packing_lists \common\models\document\OrderPackingList */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$maxRows = count($model->posibleOrders);
$hideAddBtn = $maxRows === count($model->ownOrders);
$precision = $model->invoice->price_precision;
?>

<?php Pjax::begin([
    'id' => 'invoice-facture-pjax-container',
    'enablePushState' => false,
    'linkSelector' => '.invoice-facture-reload',
]); ?>

<div class="page-content-in">

    <?php if ($backUrl !== null) {
        echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
            'class' => 'back-to-customers',
        ]);
    } ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-invoice-facture',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>


    <div class="row">
        <div class="col-lg-7 col-w-lg-8 col-md-7 customer_info_wrapper col-xs-12">
            <div class="col-xs-12 pad0" style="max-width: 720px;">
                <?= $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'message' => $message,
                    'dateFormatted' => $dateFormatted,
                ]); ?>
            </div>
        </div>
        <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
        ]); ?>

    </div>


    <div class="portlet wide_table width-sv">
        <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $precision,
        ]); ?>
    </div>
    <div class="portlet pull-left control-panel button-width-table  input-editable-field hide" style="display: inline-block; width: 30px;">
        <div class="btn-group pull-right" style="display: inline-block;">
            <?php Modal::begin([
                'header' => Html::tag('h1', 'Выбрать позицию из списка'),
                'toggleButton' => [
                    'id' => 'plusbtn',
                    'tag' => 'span',
                    'label' => '<i class="pull-left fa icon fa-plus-circle"></i>',
                    'class' => 'btn yellow btn-add-line-table' . ($hideAddBtn ? ' hide' : ''),
                    'data-maxrows' => $maxRows,
                    'data-url' => Url::to(['select-product', 'type' => $model->type, 'id' => $model->id]),
                ],
            ]); ?>
            <div id="available-product-list"></div>
            <?php Modal::end(); ?>
        </div>
    </div>

    <?= Html::endForm(); ?>

    <div class="portlet pull-right">
        <?= $this->render('_viewPartials/_summary_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
        ]); ?>
    </div>
</div>

<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
    'contractorId' => $contractorId,
]); ?>

<?php Pjax::end(); ?>

<?php
if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)) {
    echo \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::to(['delete', 'type' => $ioType, 'id' => $model->id, 'contractorId' => $contractorId,]),
        'confirmParams' => [],
        'message' => 'Вы уверены, что хотите удалить счет-фактуту?',
    ]);
} ?>

<?php
$this->registerJs('
var checkPlusbtn = function() {
    if ($("tbody.document-orders-rows tr").length < $("#plusbtn").data("maxrows")) {
        $("#plusbtn").removeClass("hide");
    } else {
        $("#plusbtn").addClass("hide");
    }
};
var checkRowNumbers = function() {
    $(".document-orders-rows").children("tr").each(function(i, row) {
        $(row).find("span.row-number").text(++i);
    });
};
var checkServiceUnits = function() {
    if ($("#invoicefacture-show_service_units").is(":checked")) {
        $(".service_units_visible").removeClass("hide");
        $(".service_units_hidden").addClass("hide");
    } else {
        $(".service_units_visible").addClass("hide");
        $(".service_units_hidden").removeClass("hide");
    }
};
var toggleFieldsToForm = function() {
    $(".input-editable-field").removeClass("hide");
    $(".editable-field").addClass("hide");
};
checkPlusbtn();
checkRowNumbers();

$(document).on("change", "#invoicefacture-show_service_units", function() {
    checkServiceUnits();
});
$(document).on("click", "tr .delete-row", function() {
    var row = $(this).closest("tr");
    var table = row.parent();
    row.remove();
    checkRowNumbers();
    checkPlusbtn();
});
$(document).on("click", "#plusbtn", function() {
    $.post($(this).data("url"), $("#edit-invoice-facture").serialize(), function(data) {
        $("#available-product-list").html(data);
    });
});
$(document).on("click", "#add-selected-button", function() {
    $.post($(this).data("url"), $("#select-invoice-product-form").serialize(), function(data) {
        console.log(data);
        if (data.rows.length) {
            $.each(data.rows, function(i, item) {
                $(".document-orders-rows").append(item);
            });
        }
        toggleFieldsToForm();
        checkPlusbtn();
        checkRowNumbers();
        $("#available-product-list").html("");
        $(".document-orders-rows input:not(.md-check, .md-radiobtn)").uniform();
    });
});
');
?>
