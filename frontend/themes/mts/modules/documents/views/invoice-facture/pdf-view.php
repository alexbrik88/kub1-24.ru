<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use frontend\models\Documents;
use common\models\document\InvoiceFacture;
use common\models\company\CompanyType;
use common\components\image\EasyThumbnailImage;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\InvoiceFacture */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

//$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act';


$paymentDocuments = [];
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
}
$precision = $model->invoice->price_precision;

$company = $model->company;

$addStamp = (boolean) $model->add_stamp;
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp || !$company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp || !$company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp || !$company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

?>
<style>
@media print {
    thead {
        display: table-header-group;
    }
}
</style>
<?php if ($addStamp) : ?>
    <style>
        #print_layer {
            height: 200px;
            /* background-image: url('<?= $printLink ?>'); */
            background-position: 16% 0px;
            background-repeat: no-repeat;
            -webkit-print-color-adjust: exact;
        }
        .signature_image {
            max-width:100px;
            max-height:30px;
        }
    </style>
<?php endif ?>


<?php
ob_start();
?>
<div class="page-content-in p-center-album pad-pdf-p-album">
    <div class="text-right font-size-6 line-h-small">
        <p>Приложение №1</p>

        <p>к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137</p>

        <p>(в редакции постановления Правительства Российской Федерации от 19 августа 2017 г. № 981)</p>
    </div>

    <h4>Счет-фактура № <?= $model->fullNumber; ?>
        от <?= $dateFormatted; ?></h4>

    <h4>Исправление № &mdash; от &mdash;</h4>

    <div class="font-size-8 padding-min" style="margin: 5px 0;">
        <div style="margin-bottom: 3px">
            Продавец:
            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                <?= $model->invoice->company_name_short; ?>
            <?php else: ?>
                <?= $model->invoice->contractor_name_short; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Адрес:
            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                <?= $model->invoice->company_address_legal_full; ?>
            <?php else: ?>
                <?= $model->invoice->contractor_address_legal_full; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            ИНН/КПП продавца:
            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
            <?php else: ?>
                <?= $model->invoice->contractor_inn; ?> / <?= $model->invoice->contractor_kpp; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Грузоотправитель и его адрес:
            <?= $model->invoice->production_type ? (
                    $model->consignor ?
                    $model->consignor->getRequisitesFull(null, false) :
                    'он же'
                ) : Product::DEFAULT_VALUE; ?>
        </div>

        <div style="margin-bottom: 3px">
            Грузополучатель и его адрес:
            <?php if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
                $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
            } else {
                $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
            }
            echo $model->invoice->production_type ? ($model->consignee ?
                $model->consignee->getRequisitesFull($address, false) :
                $model->invoice->contractor->getRequisitesFull($address, false)
            ) : \common\models\product\Product::DEFAULT_VALUE; ?>
        </div>

        <div style="margin-bottom: 3px">
            К платежно-расчетному документу
            <?= $paymentDocuments ? join(', ', $paymentDocuments) : '№ --- от ---'; ?>
        </div>

        <div style="margin-bottom: 3px">
            Покупатель:
            <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                <?= $model->invoice->company_name_short; ?>
            <?php else: ?>
                <?= $model->invoice->contractor_name_short; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Адрес:
            <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                <?= $model->invoice->company_address_legal_full; ?>
            <?php else: ?>
                <?= $model->invoice->contractor_address_legal_full; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            ИНН/КПП покупателя:
            <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                <?= $model->invoice->company_inn; ?> / <?= $model->invoice->company_kpp; ?>
            <?php else: ?>
                <?php if($model->invoice->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON)
                    echo $model->invoice->contractor_inn . ' / ' . $model->invoice->contractor_kpp; ?>
            <?php endif; ?>
        </div>

        <div style="margin-bottom: 3px">
            Валюта: наименование, код Российский рубль, 643
        </div>

        <div style="margin-bottom: 3px">
            Идентификатор государственного контракта, договора (соглашения)(при наличии):
            <?= $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE ?>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th rowspan="2" class="text-center font-size-8" width="13%">
                Наименование товара (описание выполненных работ, оказанных
                услуг), имущественного права
            </th>
            <th rowspan="2" class="text-center font-size-8" width="">
                Код вида товара
            </th>
            <th colspan="2" class="text-center font-size-8" width="13%">
                Единица измерения
            </th>
            <th rowspan="2" class="text-center font-size-8" width="5%">
                Коли-чество (объем)
            </th>
            <th rowspan="2" class="text-center font-size-8" width="7%">Цена
                (тариф) за единицу измерения
            </th>
            <th rowspan="2" class="text-center font-size-8" width="10%">
                Стоимость товаров (работ, услуг), имущественных прав без
                налога -
                всего
            </th>
            <th rowspan="2" class="text-center font-size-8" width="7%">В том
                числе сумма акциза
            </th>
            <th rowspan="2" class="text-center font-size-8" width="6%">
                Налоговая ставка
            </th>
            <th rowspan="2" class="text-center font-size-8" width="9%">Сумма
                налога, предъявляемая покупателю
            </th>
            <th rowspan="2" class="text-center font-size-8" width="9%">
                Стоимость товаров (работ, услуг), имущественных прав с
                налогом -
                всего
            </th>
            <th colspan="2" class="text-center font-size-8" width="13%">
                Страна происхождения товара
            </th>
            <th rowspan="2" class="text-center font-size-8" width="">
                Регистрационный номер таможенной декларации
            </th>
        </tr>
        <tr>
            <th class="text-center font-size-8" width="3%">код</th>
            <th class="text-center font-size-8" width="">условное
                обозначение (национальной)
            </th>
            <th class="text-center font-size-8" width="6%">цифровой код</th>
            <th class="text-center font-size-8" width="">краткое
                наименование
            </th>
        </tr>
        </thead>
        <tbody class="no-border-buttom">
        <tr class="td-p-0">
            <td class="text-center  font-size-8">1</td>
            <td class="text-center  font-size-8">1a</td>
            <td class="text-center  font-size-8">2</td>
            <td class="text-center  font-size-8">2a</td>
            <td class="text-center  font-size-8">3</td>
            <td class="text-center  font-size-8">4</td>
            <td class="text-center  font-size-8">5</td>
            <td class="text-center  font-size-8">6</td>
            <td class="text-center  font-size-8">7</td>
            <td class="text-center  font-size-8">8</td>
            <td class="text-center  font-size-8">9</td>
            <td class="text-center  font-size-8">10</td>
            <td class="text-center  font-size-8">10a</td>
            <td class="text-center  font-size-8">11</td>
        </tr>

        <?php foreach ($model->ownOrders as $ownOrder): ?>
            <?php
            $invoiceOrder = $ownOrder->order;
            $product = $invoiceOrder->product;
            $amountNoNds = $model->getPrintOrderAmount($ownOrder->order_id, true);
            $amountWithNds = $model->getPrintOrderAmount($ownOrder->order_id);
            $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
            if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
            } ?>
            <tr>
                <td><?= $invoiceOrder->product_title; ?></td>
                <td><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
                <td>
                    <?php
                    $value = $invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE;
                    echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                    ?>
                </td>
                <td>
                    <?php
                    $value = $invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE;
                    echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                    ?>
                </td>
                <td class="text-right">
                    <?= ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']); ?>
                </td>
                <td class="text-right">
                    <?php
                    $value = $invoiceOrder->selling_price_no_vat ?
                            TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
                            Product::DEFAULT_VALUE;
                    echo $hideUnits ? Product::DEFAULT_VALUE : $value;
                    ?>
                </td>
                <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
                <td class="text-center">
                    <?= $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза'; ?>
                </td>
                <td class="text-center"><?= $invoiceOrder->saleTaxRate->name; ?></td>
                <td class="text-right"><?= TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision); ?></td>
                <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
                <td><?= $invoiceOrder->country->code == '--'? Product::DEFAULT_VALUE : $invoiceOrder->country->code ; ?></td>
                <td><?= $invoiceOrder->country->name_short == '--'? Product::DEFAULT_VALUE : $invoiceOrder->country->name_short ; ?></td>
                <td><?= $invoiceOrder->custom_declaration_number; ?></td>
            </tr>
        <?php endforeach; ?>

        <tr>
            <td colspan="6" class="font-bold">Всего к оплате</td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2); ?></td>
            <td colspan="2" class="text-center">X</td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
            <td class="text-right"><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
            <td colspan="3" style="border: none"></td>
        </tr>
        </tbody>
    </table>
    <div id="print_layer">
    <table class="table no-border" style="">
        <tr>
            <td class="font-size-8" width="19%">
                Руководитель организации<br>или иное уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?php if ($signatureLink)
                    echo Html::tag('img', null, ['src' =>$signatureLink, 'class' => 'signature_image'])
                ?>
            </td>
            <td width="1%"></td>
            <td class="ver-bottom m-l font-size-8 text-center" width="17%"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else: ?>
                        <?= $model->invoice->contractor->director_name; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td class="font-size-8" width="18%">Главный бухгалтер<br>или иное
                уполномоченное лицо
            </td>
            <td width="10%" style="border-bottom: 1px solid black; padding:0; text-align: center">
                <?php if ($signatureLink)
                    echo Html::tag('img', null, ['src' =>$signatureLink, 'class' => 'signature_image'])
                ?>
            </td>
            <td width="1%"></td>
            <td class="ver-bottom m-l font-size-8 text-center" width="17%" style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true); ?>
                        <?php if ($model->signed_by_employee_id) : ?>
                            <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                            <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                        <?php endif ?>
                    <?php else: ?>
                        <?php if ($model->invoice->contractor->chief_accountant_is_director):?>
                            <?=$model->invoice->contractor->director_name?>
                        <?php else: ?>
                            <?=$model->invoice->contractor->chief_accountant_name?>
                        <?php endif ?>
                    <?php endif; ?>
                <?php endif; ?></td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
            <td></td>
            <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
            <td></td>
            <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
            <td></td>
            <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
            <td width="6%"></td>
        </tr>
    </table>
    <table class="table no-border" style="">
        <tr>
            <td class="ver-bottom m-l font-size-8" width="19%">
                Индивидуальный предприниматель<br>или иное уполномоченное лицо
            </td>
            <td width="10.4%" style="border-bottom: 1px solid black"></td>
            <td width="1%"></td>
            <td width="17.5%" class="ver-bottom m-l font-size-8"
                style="border-bottom: 1px solid black">
                <?php if ($model->invoice->company->company_type_id == company\CompanyType::TYPE_IP): ?>
                    <?= $model->invoice->getCompanyChiefFio(true); ?>
                <?php endif; ?></td>
            <td width="1%"></td>
            <td style="border-bottom: 1px solid black" class="ver-bottom m-l font-size-8 text-center">
                <?= $model->company->company_type_id == CompanyType::TYPE_IP ? $model->company->getCertificate(false) : null; ?>
            </td>
            <td width="6%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="text-center font-size-6 ver-top m-l">(подпись)</td>
            <td width="1%"></td>
            <td class="text-center font-size-6 ver-top m-l">(ф.и.о.)</td>
            <td width="6%"></td>
            <td class="text-center font-size-6 ver-top m-l">(реквизиты
                свидетельства
                о государственной,<br/>
                регистрации индивидуального предпринимателя)
            </td>
            <td width="6%"></td>
        </tr>
    </table>
    </div>
</div>

<?php
// замена тройного тире на одно длинное
$content = ob_get_contents();
ob_end_clean();
echo strtr($content, ['---' => '&mdash;']);
?>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>
