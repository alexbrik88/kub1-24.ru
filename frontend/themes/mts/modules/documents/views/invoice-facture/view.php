<?php

use frontend\models\Documents;
use frontend\rbac\permissions\document\Document;
use frontend\themes\mts\modules\documents\widgets\DocumentLogWidget;
use yii\helpers\Html;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$paymentDocuments = [];
$this->title = $message->get(\frontend\modules\documents\components\Message::TITLE_SINGLE) . ' №' . $model->document_number;
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
}
$precision = $model->invoice->price_precision;
$this->registerJs(<<<JS
    // buttonsScroll.init();
JS
);
 if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)) {
    echo \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => \yii\helpers\Url::toRoute(['delete', 'type' => $model->type, 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить исходящую счет-фактуру № {$model->document_number}?",
    ]);
 }

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
?>

<?php if ($backUrl !== null) : ?>
    <?= \yii\helpers\Html::a('Назад к списку', $backUrl, [
        'class' => 'link mb-2',
    ]); ?>
<?php endif ?>
<div class="wrap wrap_padding_small mb-2 position-relative">
    <div class="page-in row">
        <div class="col-12 column">
            <div class="page-border">
                <?= DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, [
                        'model' => $model,
                    ])
                ): ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'type' => $ioType,
                        'id' => $model->id,
                        'contractorId' => ($useContractor ? $model->invoice->contractor_id : null),
                    ], [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                        'title' => 'Редактировать',
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container">
                    <div style="overflow-x: auto;">
                        <div style="width: 1200px; position: relative;">
                            <?= $this->render('/invoice-facture/template', [
                                'model' => $model,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar page-in-sidebar_absolute pl-3 column pl-3">
            <?= $this->render('_viewPartials/_status_block_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'ioType' => $model->type,
                'useContractor' => $useContractor,
            ]); ?>
            <?= $this->render('_viewPartials/_main_info', [
                'model' => $model,
                'message' => $message,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
]); ?>
