<?php
use common\components\TextHelper;

/** @var \common\models\document\InvoiceFacture $model */
?>

<table class="table table-resume">
    <tbody>
    <tr role="row" class="odd">
        <td><b>Всего к оплате:</b></td>
    </tr>
    <tr role="row" class="even">
        <td>Стоимость без налога:</td>
        <td><?php echo TextHelper::invoiceMoneyFormat($model->totalAmountNoNds, 2); ?></td>
    </tr>
    <tr role="row" class="odd">
        <td>Сумма налога:</td>
        <td><?php echo TextHelper::invoiceMoneyFormat($model->totalNds, 2); ?></td>
    </tr>
    <tr role="row" class="even">
        <td>Стоимость с налогом:</td>
        <td><?php echo TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?></td>
    </tr>
    </tbody>
</table>