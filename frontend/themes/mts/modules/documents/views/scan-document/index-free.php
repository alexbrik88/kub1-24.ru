<?php

use common\components\grid\GridView;
use common\models\document\ScanDocument;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\grid\ActionColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\FileDir;
use frontend\modules\documents\components\UploadManagerHelper;
use common\components\grid\DropDownSearchDataColumn;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\ScanDocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$dirs = FileDir::getEmployeeAllowedDirs();

?>
<div class="scan-document-index-free">
    <?php Pjax::begin([
        'id' => 'free-scan-document-pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'timeout' => 10000,
    ]); ?>

        <?php $form = \yii\widgets\ActiveForm::begin([
            'method' => 'GET',
            'options' => [
                'class' => 'd-flex flex-nowrap align-items-center',
            ],
        ]); ?>
            <div class="form-group flex-grow-1 mr-2">
                <?= Html::activeTextInput($searchModel, 'name', [
                    'type' => 'search',
                    'placeholder' => 'Поиск...',
                    'class' => 'form-control'
                ]); ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Найти', [
                    'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                ]) ?>
            </div>
        <?php $form->end(); ?>

        <?= GridView::widget([
            'id' => 'free-scan-document-grid',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
            ],
            'emptyText' => 'Нет свободного скана документа.',
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'header' => '',
                    'name' => 'scan-id',
                    'headerOptions' => [
                        'width' => '20px;',
                    ],
                    'checkboxOptions' => [
                        'class' => 'scan-id-checker',
                    ],
                ],
                [
                    'attribute' => 'created_at',
                    'headerOptions' => [
                        'width' => '100px',
                    ],
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'headerOptions' => [
                        'width' => '30%',
                    ],
                    'contentOptions' => [
                        'style' => 'overflow: hidden;text-overflow: ellipsis; white-space:nowrap;',
                    ],
                    'attribute' => 'file.filename_full',
                    'label' => 'Название',
                ],
                [
                    'label' => 'Скан',
                    'headerOptions' => [
                        'width' => '30px',
                        'style' => 'padding-left: 10px!important;'
                    ],
                    'format' => 'raw',
                    'value' => function ($model) {
                        $content = '';
                        if ($model->file) {
                            $url = [
                                'file',
                                'id' => $model->id,
                                'name' => $model->file->filename_full,
                            ];
                            $content .= Html::a('<span class="pull-center icon icon-paper-clip"></span>', $url, [
                                'class' => 'scan_link',
                                'data' => [
                                    'pjax' => 0,
                                    'tooltip-content' => '#scan-tooltip-' . $model->id,
                                ]
                            ]) . "\n";
                            if ($model->file->ext == 'pdf') {
                                $img = Html::tag('embed', '', [
                                    'src' => Url::to($url),
                                    'width' => '250px',
                                    'height' => '400px',
                                    'name' => 'plugin',
                                    'type' => 'application/pdf',
                                ]);
                            } else {
                                $img = Html::img($url, [
                                    'style' => 'max-width: 300px; max-height: 300px;',
                                    'alt' => '',
                                ]);
                            }
                            $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $model->id]);
                            $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);
                        }

                        return $content;
                    }
                ],
                [
                    'label' => 'Размер',
                    'attribute' => 'file.filesize',
                    'headerOptions' => [
                        'width' => '50px',
                    ],
                    'format' => 'raw',
                    'value' => function($model) {
                        $size = ArrayHelper::getValue($model, 'file.filesize');

                        return UploadManagerHelper::formatSizeUnits($size);
                    }
                ],
                [
                    'headerOptions' => [
                        'width' => '30%',
                    ],
                    'contentOptions' => [
                        'style' => 'overflow: hidden;text-overflow: ellipsis; white-space:nowrap;',
                    ],
                    'attribute' => 'employee_id',
                    'label' => 'Ответ&shy;ствен&shy;ный',
                    'encodeLabel' => false,
                    'format' => 'raw',
                    'value' => function (ScanDocument $model) {
                        return $model->employee->getShortFio();
                    }
                ]
            ],
        ]); ?>

        <div class="mt-3 d-flex justify-content-between">
            <?= Html::submitButton('<span class="ladda-label">Прикрепить</span>', [
                'class' => 'button-regular button-width button-regular_red button-clr free-scan-bind-button',
                'style' => 'width: 140px!important;',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::button('Отменить', [
                'class' => 'button-clr button-width button-regular button-hover-transparent',
                'style' => 'width: 140px!important;',
                'data-dismiss' => 'modal',
            ]); ?>
        </div>

    <?php Pjax::end(); ?>
</div>
