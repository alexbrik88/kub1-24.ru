<?php
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AgreementTemplate */
?>
<div class="agreement-template-form">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => false,
        'validateOnBlur' => false,
        'fieldConfig' => Yii::$app->params['mtsFieldConfig'],
        'id' => 'agreement-template-form',
    ]); ?>

    <div class="form-body form-horizontal form-body_width">

        <?= $this->render('partial/_mainForm', [
            'model' => $model,
            'form' => $form,
            'type' => $type
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
            'type' => $type
        ])?>

    </div>

    <?php ActiveForm::end(); ?>
</div>
