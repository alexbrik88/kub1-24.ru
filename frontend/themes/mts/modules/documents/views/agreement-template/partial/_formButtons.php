<?php
use frontend\rbac\permissions;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $model common\models\AgreementTemplate */
?>


<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'button-width button-clr button-regular  button-regular_red w-full mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?= Html::a('Отменить', $model->isNewRecord
                ? ['agreement-template/index', 'type' => $type]
                : ['agreement-template/view', 'id' => $model->id,],
                [
                    'class' => 'button-width button-clr button-regular button-hover-transparent w-full mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]); ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Удалить',
                        'class' => 'button-width button-clr button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                    'confirmParams' => [],
                    'message' => 'Вы уверены, что хотите удалить шаблон договора?',
                ]); ?>
        </div>
    </div>
</div>