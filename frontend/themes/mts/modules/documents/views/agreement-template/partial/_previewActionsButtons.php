<?php

use common\models\AgreementTemplate;
use frontend\rbac\permissions;
use yii\bootstrap4\Html;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\AgreementTemplate;
 */

?>

<div class="wrap wrap_btns check-condition visible mb-0 actions-buttons">
    <div class="row align-items-center">
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'copied']).' <span>Копировать</span>',
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                    'message' => 'Вы уверены, что хотите скопировать этот шаблон?',
                ]); ?>
            <?php endif; ?>            
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => $this->render('//svg-sprite', ['ico' => 'archiev']) . ($model->status == $model::STATUS_ACTIVE ?
                            ' <span>В архив</span>' :
                            ' <span>Из архива</span>'),
                        'class' => 'button-regular button-hover-transparent w-full',
                    ],
                    'confirmUrl' => Url::to([
                        'update-status',
                        'id' => $model->id,
                        'status' => ($model->status == $model::STATUS_ACTIVE) ?
                            'archived' :
                            'active'
                    ]),
                    'message' => ($model->status == $model::STATUS_ACTIVE) ?
                        'Вы уверены, что хотите перенести шаблон в архив?' :
                        'Вы уверены, что хотите извлечь шаблон из архива?'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="column flex-xl-grow-1">
            <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\Contractor::CREATE)): ?>
                <?= Html::button($this->render('//svg-sprite', ['ico' => 'garbage']).' <span>Удалить</span>', [
                    'class' => 'button-regular button-hover-transparent w-full',
                    'data-toggle' => 'modal',
                    'href' => '#delete-confirm',
                ]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\Contractor::CREATE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот шаблон?",
    ]);
}; ?>
