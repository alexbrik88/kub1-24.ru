<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company;
use common\models\document\OrderWaybill;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use frontend\models\Documents;
use common\models\document\Waybill;

/* @var $this yii\web\View */
/* @var $model common\models\document\Waybill */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

//$realMassNet = $model->invoice->getRealMassNet();
//$realMassGross = $model->invoice->getRealMassGross();

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
$company = $model->invoice->company;
$precision = $model->invoice->price_precision;

$orderQuery = OrderWaybill::find()->where(['waybill_id'=>$model->id]);
$orderArray = $orderQuery->all();
$realMassGross  = $model->getRealMassGross($orderArray);
$realMassNet    = $model->getRealMassNet($orderArray);
$realPlaceCount = $model->getRealPlaceCount($orderArray);
?>

<?php echo $this->render('_viewPartials/_pdf_main', [
    'model' => $model,
    'company' => $company,
    'precision' => $precision,
    'realMassNet' => $realMassNet,
    'realMassGross' => $realMassGross,
    'realPlaceCount' => $realPlaceCount
]); ?>

<pagebreak />

<?php echo $this->render('_viewPartials/_pdf_transport', [
    'model' => $model,
    'company' => $company,
    'precision' => $precision,
    'realMassNet' => $realMassNet,
    'realMassGross' => $realMassGross,
    'realPlaceCount' => $realPlaceCount
]); ?>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>


<?php /** @var BOOLEAN $addStamp */
$addStamp = (boolean) $model->add_stamp;
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp || !$company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp || !$company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp || !$company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);
?>
<style>
    @media print {
        thead {display: table-header-group;}
    }
    <?php if (Yii::$app->request->get('actionType') == 'print') : ?>
    @page {
        size:A4 landscape;
    }
    <?php endif ?>
    <?php if ($addStamp) : ?>
    #print_layer {
        height: 230px;
        background-image: url('<?= $printLink ?>');
        background-position: 10% 30px;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature1_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '41px' : '35px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    #signature2_layer {
        background-image: url('<?= $signatureLink ?>');
        background-position: 30% <?= $model->signed_by_employee_id ? '85px' : '60px'; ?>;
        background-repeat: no-repeat;
        -webkit-print-color-adjust: exact;
    }
    <?php endif ?>

</style>