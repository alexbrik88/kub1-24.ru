<?php

use common\components\date\DateHelper;
use common\models\document\Waybill;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var Waybill $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$proxyDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->proxy_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$basisDocumentDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$productionType = explode(', ', $model->invoice->production_type);

$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$model->invoice->fullNumber}&{$model->invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#waybill-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

    <div class="portlet customer-info">
        <div class="actions" style="position: absolute; top: 0; right: 15px;">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm" style="color: #FFF;" title="Сохранить">
                    <span class="ico-Save-smart-pls"></span>
                </button>
                <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', ['view', 'type' => $model->type, 'id' => $model->id], [
                    'class' => 'btn darkblue btn-sm invoice-facture-reload input-editable-field',
                    'style' => "color: #FFF;",
                    'title' => "Отменить"
                ]) ?>
            <?php endif; ?>
        </div>
        <div class="portlet-title">

            <div class="col-md-9 caption">
                <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                №
                <?= Html::activeTextInput($model, 'document_number', [
                    'id' => 'account-number',
                    'data-required' => 1,
                    'class' => 'form-control input-editable-field',
                    'style' => 'max-width: 60px; display:inline-block;',
                    'value' => $model->getDocumentNumber(),
                ]); ?>
                <?= Html::activeTextInput($model, 'document_additional_number', [
                    'maxlength' => true,
                    'class' => 'form-control  input-editable-field',
                    'placeholder' => 'доп. номер',
                    'style' => 'max-width: 110px; display:inline-block;',
                ]); ?>

                от

                <div class="input-icon input-calendar input-editable-field margin-top-15">
                    <i class="fa fa-calendar"></i>
                    <?= Html::activeTextInput($model, 'document_date', [
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                        'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body no_mrg_bottom">
            <table class="table no_mrg_bottom">
                <?php /*
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Основание:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?php Pjax::begin([
                                        'id' => 'agreement-pjax-container',
                                        'enablePushState' => false,
                                        'linkSelector' => false,
                                    ]);
                                    echo Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'agreement',
                                        'data' => $agreementDropDownList,
                                        'pluginOptions' => [
                                            'allowClear' => false,
                                            'placeholder' => '',
                                        ],
                                    ]);
                                    Pjax::end(); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                */ ?>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Грузоотправитель:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignor_id',
                                        'data' => $model->consignorArray,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="box-pac-list-span customer-characteristic">Доверенность:</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <?= Html::activeTextInput($model, 'proxy_number', [
                                    'maxlength' => true,
                                    'class' => 'form-control  input-editable-field',
                                    'placeholder' => 'номер',
                                    'style' => 'width:200px; display: inline-block;',
                                ]); ?>
                                от
                                <div class="input-icon input-calendar input-editable-field" style="width: 47.7%;">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'proxy_date', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date-viewmode' => 'years',
                                        'style' => 'width: 100%',
                                        'value' => DateHelper::format($model->proxy_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Должность:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'given_out_position', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    ФИО:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'given_out_fio', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    <?= $message->get(Message::CONTRACTOR); ?>:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <span><?= Html::a($model->invoice->contractor_name_short, [
                                        '/contractor/view',
                                        'type' => $model->invoice->contractor->type,
                                        'id' => $model->invoice->contractor->id,
                                    ]); ?></span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Грузополучатель:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignee_id',
                                        'data' => $model->consignorArray,
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Отпуск груза разрешил</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div style="width: 50%; display:inline-block;">
                                <?= Html::activeTextInput($model, 'release_allowed_position', [
                                    'maxlength' => true,
                                    'class' => 'form-control ',
                                    'placeholder' => 'Должность',
                                    'style' => 'margin-left: 0;margin-right: 0;',
                                ]); ?>
                                </div>
                                <div style="width: 49%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'release_allowed_fullname', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'ФИО',
                                        'style' => 'width: 100%; ',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Отпуск груза произвёл</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div style="width: 50%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'release_produced_position', [
                                        'maxlength' => true,
                                        'class' => 'form-control ',
                                        'placeholder' => 'Должность',
                                        'style' => 'margin-left: 0;margin-right: 0;',
                                    ]); ?>
                                </div>
                                <div style="width: 49%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'release_produced_fullname', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'ФИО',
                                        'style' => 'width: 100%; ',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Груз получил</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div style="width: 50%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'cargo_accepted_position', [
                                        'maxlength' => true,
                                        'class' => 'form-control ',
                                        'placeholder' => 'Должность',
                                        'style' => 'margin-left: 0;margin-right: 0;',
                                    ]); ?>
                                </div>
                                <div style="width: 49%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'cargo_accepted_fullname', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'ФИО',
                                        'style' => 'width: 100%; ',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Удостоверение:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'driver_license', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Удостоверение'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Срок доставки груза:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-icon input-calendar input-editable-field" style="width: 47.7%;">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'delivery_time', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date-viewmode' => 'years',
                                        'style' => 'width: 100%;',
                                        'value' => DateHelper::format($model->delivery_time, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Путевой лист №</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <?= Html::activeTextInput($model, 'waybill_number', [
                                    'maxlength' => true,
                                    'id' => 'account-number',
                                    'data-required' => 1,
                                    'class' => 'form-control  input-editable-field ',
                                    'placeholder' => 'номер',
                                    'style' => 'width:200px;margin-left: 0;margin-right: 0;',
                                ]); ?>
                                от
                                <div class="input-icon input-calendar input-editable-field" style="width: 47.7%;">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'waybill_date', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date-viewmode' => 'years',
                                        'style' => 'width: 100%;',
                                        'value' => DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Организация:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'organization', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Организация'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Автомобиль:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'car', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Автомобиль'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Гос. номерной знак:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'car_number', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Государственный номерной знак'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Лицензионная карточка:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeDropDownList( $model, 'license_card',
                                        Waybill::$LICENSE_CARD_TYPES, [
                                            'class' => 'form-control',
                                            'style' => 'width: 47.7%; margin:0'
                                        ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Вид перевозки:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'transportation_type', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Вид перевозки'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Маршрут:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div class="input-editable-field" style="max-width: 450px;">
                                    <?= Html::activeTextInput($model, 'transportation_route', [
                                        'class' => 'form-control',
                                        'style' => 'width: 100%',
                                        'placeholder' => 'Маршрут'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Прицеп 1</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div style="width: 33%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer1_brand', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Марка',
                                    ]); ?>
                                </div>
                                <div style="width: 30%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer1_number', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Гос. номер',
                                    ]); ?>
                                </div>
                                <div style="width: 35%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer1_garage_number', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Гаражный номер',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">Прицеп 1</span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <div style="width: 33%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer2_brand', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Марка',
                                    ]); ?>
                                </div>
                                <div style="width: 30%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer2_number', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Гос. номер',
                                    ]); ?>
                                </div>
                                <div style="width: 35%; display:inline-block;">
                                    <?= Html::activeTextInput($model, 'trailer2_garage_number', [
                                        'class' => 'form-control ',
                                        'placeholder' => 'Гаражный номер',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php /*
                <tr class="contractor-address-block" data-current="<?= $model->invoice->contractor->id; ?>"
                    style="<?= 'display: ' . ($showAddress ? 'table-row;' : 'none;'); ?>">
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic"></span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <?= Html::activeRadioList($model, 'contractor_address', [
                                    Waybill::CONTRACTOR_ADDRESS_LEGAL => 'Юр. адрес',
                                    Waybill::CONTRACTOR_ADDRESS_ACTUAL => 'Физ. адрес',
                                ]); ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: 32px;">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-sm-6" style="max-width: 205px; padding-right: 0;">
                                <span class="customer-characteristic">
                                    Печать и подпись:
                                </span>
                            </div>
                            <div class="col-sm-7" style="padding-left: 7px;">
                                <label class="rounded-switch" for="waybill-add_stamp"
                                       style="margin: 4px 0 0;">
                                    <?= Html::checkbox('add_stamp', $model->add_stamp, [
                                        'id' => 'waybill-add_stamp',
                                        'class' => 'switch'
                                    ]) ?>
                                    <span class="sliderr no-gray yes-yellow round"></span>
                                </label>
                                <span class="tooltip-hover ico-question valign-middle"
                                      data-tooltip-content="#tooltip_add_stamp"
                                      style="vertical-align: top;">
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>
                */ ?>
                <table class="table no_mrg_bottom">
                    <tr>
                        <td>
                            <div style="margin-bottom: 5px;">
                                <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                    'model' => $model,
                                    'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                    'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                    'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                    'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                    'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                    'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                    'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                                ]); ?>
                            </div>
                        </td>
                    </tr>
                </table>
        </div>
    </div>

    <div class="tooltip_templates" style="display: none;">
    <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
        Добавить в ТТН печать и подпись
        <br>
        при отправке по e-mail и
        <br>
        при скачивании в PDF
    </span>
    </div>

<?php /* $this->registerJs('
    $("#waybill-consignee_id").change(function () {
        var $contractorAddressBlock = $(".contractor-address-block");
        var $contractorID = $(this).val();
        if ($contractorID == "") {
            $contractorID = $contractorAddressBlock.data("current");
        }

        $.post("/contractor/is-different-address", {
                id: $contractorID
           }, function (data) {
                if (data.isDifferent == true) {
                    $contractorAddressBlock.show();
                } else {
                    $contractorAddressBlock.hide();
                }
                $contractorAddressBlock.find("#waybill-contractor_address input[value=\"0\"]").prop("checked", true).click();
           });
    });
'); */ ?>