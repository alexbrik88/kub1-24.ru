<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\modules\documents\widgets\DocumentFileWidget;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
/* @var array $files */

$user = Yii::$app->user->identity;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 style="text-align: center; margin: 0">Прикрепить файл к <?= $documentTypeName ?> от Поставщика</h4>
</div>
<div class="invoice-list">
    <?= Html::beginForm(["/documents/upload-manager/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>
    <?= Html::hiddenInput('doc_type', $docType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod) : ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>
            <?php foreach ($files as $file)
                echo Html::hiddenInput('files[]', $file) ?>

            <div class="search-form-default">
                <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'find_by', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ договора...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div id="range-ajax-button">
                <div
                    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                    data-pjax="get-vacant-invoices-pjax" id="reportrange2">

                    <?php
                    $this->registerJs("
                    $('#reportrange2').daterangepicker({
                        format: 'DD.MM.YYYY',
                        ranges: {" . StatisticPeriod::getRangesJs() . "},
                        startDate: '" . $dateFrom . "',
                        endDate: '" . $dateTo . "',
                        locale: {
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    });

                    "); ?>

                    <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                    <b class="fa fa-angle-down"></b>
                </div>

            </div>

        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?php \yii\widgets\Pjax::begin([
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                    ]) ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-style table-count-list',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'nav-pagination list-clr',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'document_number',
                                'label' => '№ договора',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) use ($files, $docType) {
                                    /** @var Invoice $data */
                                    if ($docType == Documents::DOCUMENT_AGREEMENT) {
                                        $agreement = \common\models\Agreement::findOne($data['id']);
                                        return Html::a($agreement->fullNumber, [
                                                'agreement/update-page',
                                                'type' => $agreement->type,
                                                'id' => $agreement->id,
                                                'mode' => 'previewScan',
                                                'files' => $files,
                                            ], [
                                                'data-pjax' => '0',
                                            ]);
                                    }

                                    return $data->fullNumber;
                                },
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата договора',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '21%',
                                    'class' => 'dropdown-filter',
                                ],
                                'contentOptions' => [
                                    'class' => 'contractor-cell',
                                    'style' => 'overflow: hidden;text-overflow: ellipsis;',
                                ],
                                'attribute' => 'contractor_id',
                                'label' => 'Контр&shy;агент',
                                'encodeLabel' => false,
                                'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                                'filter' => $searchModel->getContractorFilterItems(),
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $contractor = \common\models\Contractor::findOne($data['cid']);
                                    $name = Html::encode($contractor->getShortName());
                                    return Html::a($name, [
                                        '/contractor/view',
                                        'type' => $data['type'],
                                        'id' => $data['cid'],
                                    ], [
                                        'target' => '_blank',
                                        'title' => html_entity_decode($name),
                                        'data-pjax' => 0,
                                        'data-id' => $data['cid'],
                                    ]);
                                },
                            ],
                        ],
                    ]); ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12">
        <button class="btn btn-add darkblue pull-right" data-dismiss="modal">Отменить</button>
    </div>
</div>