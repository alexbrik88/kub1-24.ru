<style>
    .page-content-wrapper .page-content {
        margin-left: 0!important;
    }
    .page-scan-preview {
        position: relative;
        width: 100%;
    }
    .page-sidebar {
        border-right:none;
        background-color:#fff;
    }
    .issuu-img-container {
        width:100%;
        text-align: center;
    }
    .issuu-img-container img {
        position:absolute;
        left:0;
        right:0;
        margin-left:auto;
        margin-right:auto;
        width: auto;
    }
    .page-scan-preview .dropdown-menu li > a.group-attach-files-to {
        cursor: default;
        font-weight: bold;
    }
    .page-scan-preview .btn:hover,
    .page-scan-preview .btn:active,
    .page-scan-preview .btn:focus {
        color:#333333!important;
    }

    .page-scan-preview .slider-body {
        border: 1px solid #ccc;
    }
    .page-scan-preview .slider-header {
        padding-top:2px;
    }
    .page-scan-preview .num-page-wrap {
        float:right;
        margin-left: 10px;
        padding-top: 2px;
    }
    .page-scan-preview .file-name {
        font-size: 18px;
        line-height: 1;
    }
</style>