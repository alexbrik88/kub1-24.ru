<?php

use common\models\Company;
use common\components\grid\GridView;
use common\components\ImageHelper;
use devgroup\dropzone\DropZoneAsset;
use frontend\models\Documents;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Modal;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\documents\components\UploadManagerHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\models\employee\Employee;
use yii\widgets\Pjax;
use frontend\rbac\permissions;
use common\models\file\FileDir;
use yii\bootstrap4\Dropdown;
use kartik\select2\Select2;
use frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\file\FileUploadType;
use yii\helpers\ArrayHelper;
use frontend\rbac\UserRole;
use common\models\document\ScanDocument;
use \common\models\Contractor;

/* @var $company Company */
/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\documents\models\UploadManagerFileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $directory FileDir or null */

$this->title = $title;

if (!isset($directory)) $directory = null;
if (!isset($isUploads)) $isUploads = false;
if (!isset($isTrash)) $isTrash = false;

$canIndex = $canCreate = $canDelete = $canUpdate = $canRestore =
    Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
    Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
    Yii::$app->user->can(UserRole::ROLE_SUPERVISOR);

$dropDirs = [];
$dropDirs[] = [
    'label' => 'В папку',
    'url' => 'javascript:;',
    'linkOptions' => [
        'class' => 'group-attach-files-to',
    ],
];

if (!$isUploads) {
    $dropDirs[] = [
        'label' => 'Загрузка',
        'url' => '#move-files-to',
        'linkOptions' => [
            'class' => 'move-files-to',
            'data-directory_id' => 0
        ],
    ];
}

if ($dirs = FileDir::getEmployeeAllowedDirs()) {
    /** @var FileDir $dir */
    foreach ($dirs as $dir) {

        if ($directory && $directory->id == $dir->id)
            continue;

        $dropDirs[] = [
            'label' => $dir->name,
            'url' => '#move-files-to',
            'linkOptions' => [
                'class' => 'move-files-to',
                'data-directory_id' => $dir->id
            ],
        ];
    }
}

$docTypesList = UploadManagerHelper::getDocumentTypesList($company);

$dropDocs = [];
foreach ($docTypesList as $label => $docTypes) {
    $dropDocs[] = [
        'label' => $label,
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'group-attach-files-to',
        ],
    ];
    foreach ($docTypes as $key => $value) {
        $dropDocs[] = [
            'label' => $value,
            'url' => '#attach-files-to',
            'linkOptions' => [
                'class' => 'attach-files-to',
                'data-doc_type' => $key
            ],
        ];
    }
}

if ($isTrash)
    $emptyText = 'Корзина пуста.';
elseif ($isUploads)
    $emptyText = 'Нет новых сканов.';
else
    $emptyText = 'Вы еще не добавили ни одного фото документа.';

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
?>

<div class="upload-manager-index">
    <div class="portlet box">
        <div class="row" style="margin-bottom:9px">
            <div class="col-sm-12">
                <div class="btn-group pull-right">
                    <div class="btn-group pull-right title-buttons">
                        <button class="btn yellow create-scan-btn" data-url="<?= Url::to(['create-scan', 'dir_id' => ($directory) ? $directory->id : null]) ?>">
                            <i class="fa fa-plus"></i> ЗАГРУЗИТЬ
                        </button>
                    </div>
                </div>
                <div class="tools search-tools tools_button hidden-sm hidden-xs pull-right">
                    <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input ">
                            <?= $form->field($searchModel, 'find_by')->textInput([
                                'type' => 'search',
                                'placeholder' => 'Поиск...',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('Найти', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
                </div>
                <div class="joint-operations" style="display:none;">
                    <?php if (!$isTrash): ?>
                        <?php if ($canUpdate && $dropDocs) : ?>
                            <div class="dropdown pull-right">
                                <?= Html::a('Прикрепить  <span class="caret"></span>', null, [
                                    'class' => 'btn darkblue-gray btn-sm dropdown-toggle',
                                    'id' => 'dropdownMenu2',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropDocs,
                                    'options' => [
                                        'style' => 'right: -66px!important; left: auto; top: 36px;',
                                        'aria-labelledby' => 'dropdownMenu1',
                                    ],
                                ]); ?>
                            </div>
                        <?php endif ?>
                        <?php if ($canUpdate && $dropDirs) : ?>
                            <div class="dropdown pull-right">
                                <?= Html::a('Переместить  <span class="caret"></span>', null, [
                                    'class' => 'btn btn-sm darkblue-gray dropdown-toggle',
                                    'id' => 'dropdownMenu1',
                                    'data-toggle' => 'dropdown',
                                    'aria-expanded' => true,
                                ]); ?>
                                <?= Dropdown::widget([
                                    'items' => $dropDirs,
                                    'options' => [
                                        'style' => 'width:204px; left: -30px; top: 36px;',
                                        'aria-labelledby' => 'dropdownMenu1',
                                    ],
                                ]); ?>
                            </div>
                        <?php endif ?>
                    <?php else: ?>
                        <?php if ($canRestore) : ?>
                            <?= Html::a('<i class="glyphicon glyphicon-share"></i> Восстановить', '#many-restore', [
                                'class' => 'btn darkblue-gray btn-sm hidden-md hidden-sm hidden-xs pull-right',
                                'data-toggle' => 'modal',
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-share"></i>', '#many-restore', [
                                'class' => 'btn darkblue-gray btn-sm hidden-lg pull-right',
                                'data-toggle' => 'modal',
                            ]); ?>
                        <?php endif ?>
                    <?php endif; ?>
                    <?php if ($canDelete) : ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                            'class' => 'btn darkblue-gray btn-sm hidden-md hidden-sm hidden-xs pull-right',
                            'data-toggle' => 'modal',
                        ]); ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                            'class' => 'btn darkblue-gray btn-sm hidden-lg pull-right',
                            'data-toggle' => 'modal',
                        ]); ?>
                    <?php endif ?>

                    <?= $this->render('_partial/_joint_operations_modals', [
                         'canUpdate' => $canUpdate,
                         'canRestore' => $canRestore,
                         'canDelete' => $canDelete
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="scroll-table-wrapper">
                <?php Pjax::begin([
                    'id' => 'pjax-uploaded-files',
                    'enablePushState' => false,
                    'timeout' => 5000
                ]) ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-style table-count-list',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'emptyText' => $emptyText,
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'rowOptions'=>function($data){
                        return ['data-file_id' => $data['id']];
                    },
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'width' => '1%',
                            ],
                            'contentOptions' => [
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return Html::checkbox('File[]', false, [
                                    'class' => 'joint-operation-checkbox',
                                    'value' => $data['id']
                                ]);
                            },
                        ],
                        [
                            'label' => '',
                            'headerOptions' => [
                                'width' => '1%',
                            ],
                            'format' => 'raw',
                            'value' => function($data) {
                                $url = UploadManagerHelper::getImgPreviewUrl($data);
                                $icon = UploadManagerHelper::getFileIcon($data);

                                if ($data['ext'] == 'pdf') {
                                    $img = Html::tag('embed', '', [
                                        'src' => Url::to($url),
                                        'width' => '250px',
                                        'height' => '400px',
                                        'name' => 'plugin',
                                        'type' => 'application/pdf',
                                    ]);
                                } else {
                                    $img = Html::img($url, [
                                        'style' => 'max-height:300px;max-width:300px;',
                                        'alt' => '',
                                    ]);
                                }
                                $content = Html::a($icon, $url, [
                                    'class' => 'scan_link text-center',
                                    'download' => 'download',
                                    'data' => [
                                        'pjax' => 0,
                                        'tooltip-content' => '#scan-tooltip-' . $data['id'],
                                    ]
                                ]);
                                $tooltip = Html::tag('span', $img, ['id' => 'scan-tooltip-' . $data['id']]);
                                $content .= Html::tag('div', $tooltip, ['class' => 'hidden']);

                                return $content;
                            }
                        ],
                        [
                            'label' => 'Файл',
                            'attribute' => 'filename_full',
                            'headerOptions' => [
                                'width' => '35%',
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($isTrash) {

                                if ($isTrash)
                                    $content = $data['filename_full'];
                                else
                                    $content =Html::a($data['filename_full'], ['preview-scans',
                                            'dir_id' => $data['directory_id'],
                                            'file_id' => $data['id']], ['data-pjax' => 0]);

                                return Html::tag('div', $content, [
                                    'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                                ]);
                            }
                        ],
                        [
                            'label' => 'Дата загрузки',
                            'attribute' => 'created_at',
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'format' => ['date', 'php:d.m.Y'],
                        ],
                        [
                            'label' => 'Размер',
                            'attribute' => 'filesize',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'format' => 'raw',
                            'value' => function($data) {

                                return UploadManagerHelper::formatSizeUnits($data['filesize']);
                            }
                        ],
                        [
                            'label' => 'Прикреплен',
                            'headerOptions' => [
                                'width' => '35%',
                                'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                            ],
                            'contentOptions' => [
                                'class' => (($isUploads || $isTrash) ? 'hidden' : ''),
                            ],
                            'format' => 'raw',
                            'value' => function($data) {
                                if ($owner = UploadManagerHelper::getAttachedDocument($data)) {

                                    /** @var \common\models\document\Invoice $invoice */
                                    $invoice = (isset($owner->invoice)) ? $owner->invoice : $owner;

                                    $doc = ($owner->type == Documents::IO_TYPE_IN ? 'Вх. ' : 'Исх. ') .
                                        (isset($owner->shortPrefix) ? $owner->shortPrefix : $owner->printablePrefix) .
                                        Html::a(' №'.$owner->getFullNumber(), $owner->viewUrl, [
                                                'data-pjax' => 0,
                                                'target' => '_blank'
                                        ]).' от '.date_format(date_create($owner->document_date), 'd.m.Y');

                                    $contractor = Html::a(Html::encode($invoice->contractor->getShortName()), [
                                        '/contractor/view',
                                        'type' => $invoice->type,
                                        'id' => $invoice->contractor_id,
                                    ], [
                                        'data-pjax' => 0,
                                        'target' => '_blank'
                                    ]);

                                    $content = Html::tag('div', $doc, ['class' => 'attached-document document']);

                                    if (isset($owner->totalAmountWithNds)) {
                                        $content .= Html::tag('div',
                                            'сумма ' . TextHelper::invoiceMoneyFormat($owner->totalAmountWithNds, 2),
                                            ['class' => 'attached-document amount']);
                                    }

                                    $content .= Html::tag('div', $contractor, ['class' => 'attached-document contractor']);

                                    return $content;
                                }

                                return '';
                            }
                        ],
                        [
                            'label' => 'Детали',
                            'headerOptions' => [
                                'class' => ((!$isUploads) ? 'hidden' : ''),
                                'width' => '35%'
                            ],
                            'contentOptions' => [
                                'class' => ((!$isUploads) ? 'hidden' : ''),
                                'style' => 'font-size:10px'
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($company) {
                                $content = '';
                                $contractors = $products = '';
                                $ioType = '';
                                $scan = UploadManagerHelper::getScanDocument($data);
                                if ($scan instanceof ScanDocument) {
                                    if ($scan->recognize) {
                                        if ($scan->recognize->is_recognized && $scan->recognize->scanRecognizeDocuments) {
                                            $rDocument = $scan->recognize->scanRecognizeDocuments[0];
                                            foreach ($rDocument->scanRecognizeContractors as $rContractor) {
                                                if ($rContractor->type == Contractor::TYPE_SELLER) {
                                                    $contractors .= 'Поставщик: ' . $rContractor->short_name . ' (' . $rContractor->inn . ')' . '<br/>';
                                                    if ($rContractor->inn == $company->inn)
                                                        $ioType = 'Исх. документ';
                                                } elseif ($rContractor->type == Contractor::TYPE_CUSTOMER) {
                                                    $contractors .= 'Покупатель: ' . $rContractor->short_name . ' (' . $rContractor->inn . ')' . '<br/>';
                                                    if ($rContractor->inn == $company->inn)
                                                        $ioType = 'Вх. документ';
                                                }
                                            }
                                            foreach ($rDocument->scanRecognizeProducts as $rProduct) {
                                                $products .= $rProduct->quantity.' X '.$rProduct->name . '<br/>';
                                            }

                                            $content .= $ioType . ' №' . $rDocument->document_number . ' от ' . DateHelper::format($rDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . '<br/>';
                                            $content .= $contractors;
                                            $content .= $products ? ('Товары:<br/>' . $products) : '';

                                        } else {
                                            $content = Html::tag('span', 'Распознается...', ['class' => 'scan-recognize-task', 'data-scan_id' => $scan->id]);
                                        }
                                    } else {
                                        $content = Html::a('Распознать', ['recognize-scan', 'scan_id' => $data['owner_id']], ['class' => 'btn btn-sm darkblue']);
                                    }
                                }

                                return Html::tag('div', $content, [
                                    'style' => 'width:0; min-width:100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                                ]);
                            }
                        ],

                        [
                            'label' => 'Папка',
                            'attribute' => 'directory_id',
                            'headerOptions' => [
                                'width' => '10%',
                                'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                            ],
                            'contentOptions' => [
                                'class' => (($isUploads || $directory || $isTrash) ? 'hidden' : ''),
                            ],
                            'format' => 'raw',
                            'value' => function($data) {
                                $directory = FileDir::findOne($data['directory_id']);
                                if ($directory)
                                    return Html::a($directory->name, '/documents/upload-manager/directory/' . $directory->id, [
                                        'data-pjax' => 0
                                    ]);

                                return '';
                            }
                        ],
                        [
                            'attribute' => 'created_at_author_id',
                            'label' => 'Загрузил',
                            'headerOptions' => [
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-left text-ellipsis',
                                'style' => 'line-height:1.2'
                            ],
                            'class' => DropDownSearchDataColumn::className(),
                            'value' => function ($data) {
                                $employee = Employee::findOne([
                                    'id' => $data['created_at_author_id']
                                ]);

                                $content = '';
                                $content .= $employee ? $employee->getShortFio() : '';
                                $content .= '<br/>';
                                $content .= Html::tag('span',
                                    ArrayHelper::getValue(FileUploadType::$nameById, $data['upload_type_id']), [
                                        'style' => 'font-size:10px; color:#666'
                                    ]);

                                return $content;
                            },
                            'format' => 'raw',
                            'filter' => $searchModel->getCreators()
                        ],
                        [
                            'label' => '',
                            'headerOptions' => [
                                'width' => '5%',
                                'style' => 'min-width:231px!important;'
                            ],
                            'format' => 'raw',
                            'value' => function($data) use ($docTypesList, $isTrash, $isUploads) {

                                if ($isTrash)
                                    return false;

                                $content = Select2::widget([
                                    'name' => 'btn-attach-image',
                                    'data' =>  $docTypesList,
                                    'options' => [
                                        'placeholder' => 'Прикрепить',
                                        'class' => 'form-control btn-attach-image',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => false,
                                        'minimumResultsForSearch' => -1,
                                    ]
                                ]);

                                return $content;
                            }
                        ],
                    ],
                ]); ?>

                <?php Pjax::end() ?>
            </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'scan-document-modal',
    'header' => Html::tag('h1', 'Фото документа'),
]); ?>

<?php Modal::end(); ?>

<?= $this->render('_partial/_invoices_modal', [
    'company' => $company,
]) ?>

<?= $this->render('_partial/_kub_doc_js') ?>

<div style="display:none" id="dz-upload-scan" class="dz-upload-scan dropzone"></div>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var dropzone_scan_upload = new Dropzone("#dz-upload-scan", {
        'paramName': 'ScanDocument[upload]',
        'url': '<?= Url::to(['create-scan', 'dir_id' => ($directory) ? $directory->id : null]) ?>',
        'dictDefaultMessage':  '<div class="icon"><?=ImageHelper::getThumb('img/upload-files-pc.png', [100, 100])?></div>',
        'dictInvalidFileType': 'Недопустимый формат файла',
        'maxFilesize': 5,
        'maxFiles': 1,
        'uploadMultiple': false,
        'acceptedFiles': "image/jpeg,image/png,application/pdf",
        'params': {'dz': true, '<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
    });
    dropzone_scan_upload.on('thumbnail', function (f, d) {
    });
    dropzone_scan_upload.on('success', function (f, d) {
        location.href = location.href;
    });
    dropzone_scan_upload.on('error', function (f, d) {
        alert(d);
    });
    dropzone_scan_upload.on('totaluploadprogress', function (progress) {
    });
</script>

<script>

    $(document).on("change", ".btn-attach-image", function (e) {
        e.preventDefault();
        var addToDoc = $(this).val();
        var files = [$(this).parents('tr').data('file_id')];

        if (!addToDoc)
            return false;

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
            $(this).val('').trigger('change');
        }
    });

    $(document).on('click', '.group-attach-files-to', function(e) {
        return false;
    });

    $(document).on("click", ".attach-files-to", function (e) {
        e.preventDefault();
        var addToDoc = $(this).data('doc_type');
        var files = [];
        $('.uploaded_files_table').find('.joint-operation-checkbox').filter(':checked').each(function() {
            files.push($(this).val());
        });

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
        }

    });

    $('.modal-attach-image-yes').on('click', function () {
        $('#invoices-list').modal();
        $('#modal-attach-image').modal('hide');
        $.pjax({
            url: '/documents/upload-manager/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            },
            push: false,
            timeout: 5000
        });
    });

    $('.modal-attach-image-no').on('click', function () {
        $.post('/documents/upload-manager/redirect-to-create-document', {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            }, function(data) {
                $('#modal-attach-image').modal('hide');
                if (data['redirect_url']) {
                    location.href = data['redirect_url'];
                }
        });
    });

    $('.move-files-to').on('click', function(e) {
        e.preventDefault();
        $('#move-files-to').find('.modal-move-files-to').attr('data-url', '/documents/upload-manager/move-files-to?directory_id=' + $(this).data('directory_id'));
        $('#move-files-to').modal('show');
    });

    $(document).on('click', '.create-scan-btn', function() {
        $('.dz-upload-scan').click();
        //$("#scan-document-modal").modal('show');
        //$('#scan-document-modal .modal-body').load($(this).data('url'));
    });
    $(document).on("hidden.bs.modal", "#scan-document-modal", function () {
        $('#scan-document-modal .modal-body').html('')
    });

    $(document).ready(function() {
        $('.scan_link').tooltipster({
            theme: ['tooltipster-kub'],
            contentCloning: true,
            side: ['right', 'left', 'top', 'bottom'],
        });

        ScanRecognize.init();
    });

    // Recognize
    ScanRecognize = {
        timerId: null,
        timerPeriod: 10000,
        taskIdx: null,
        init: function()
        {
            if ($('.scan-recognize-task').length) {
                this.timerId = setInterval(this.checkRecognizeStatus, this.timerPeriod);
            }
        },
        checkRecognizeStatus: function()
        {
            var els = $('.uploaded_files_table').find('.scan-recognize-task');
            var el;

            if (els.length === 0) {
                clearInterval(this.timerId);
                return;
            }

            if (this.taskIdx === null) {
                el = $(els).first();
            } else {
                el = $(els).eq(this.taskIdx + 1);
                if (!el.length)
                    el = $(els).first();
            }
            this.taskIdx = $(el).index();

            $.ajax({
                url: 'check-recognize-status',
                data: {scan_id: $(el).data('scan_id')},
                success: function(data) {
                    if (data.result)
                        $.pjax.reload("#pjax-uploaded-files");
                }
            });
        }
    };
</script>