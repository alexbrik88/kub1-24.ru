<?php $this->title = 'Просмотр сканов'; ?>
<?= $this->render('_partial/_style_full_screen') ?>
<?= frontend\modules\documents\widgets\SideMenuPreviewScanWidget::widget([
        'isFullScreen' => true,
        'filesIds' => $filesIds,
        'startFileId' => $startFileId
    ]);
?>