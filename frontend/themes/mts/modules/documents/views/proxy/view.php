<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\document\Document;
use frontend\themes\mts\modules\documents\widgets\DocumentLogWidget;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\file\File;
use frontend\modules\documents\components\DocConverter;

/* @var $this yii\web\View */
/* @var $model common\models\document\Proxy */
/* @var $message Message */
/* @var $ioType integer */
/* @var $contractorId integer|null */
$plus = 0;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$products = \common\models\document\OrderProxy::getAvailable($model->id);
$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = ['index', 'type' => $model->type];
$precision = $model->invoice->price_precision;

$isFullCustomerInfo = $ioType == Documents::IO_TYPE_OUT_URL;
$fullInfoPrefix = '';
if ($isFullCustomerInfo) {
    $fullInfoPrefix = 'full';
}
$script = <<< JS

if('$plus' === '1'){
  $('.testclass').hide();
}
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, $this::POS_READY);
?>

<?php if ($backUrl !== null) {
    echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
        'class' => 'link mb-2',
    ]);
} ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\mts\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model])) : ?>
                    <?= \yii\bootstrap4\Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'id' => $model->id,
                        'type' => $model->type
                    ], [
                        'data-tooltip-content' => '#unpaid-invoice',
                        'data-placement' => 'right',
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1',
                    ]) ?>
                <?php endif; ?>
                <div class="doc-container doc-proxy doc-preview">
                    <?php echo $this->render('_viewPartials/_pre-view-' .$fullInfoPrefix. Documents::$ioTypeToUrl[$ioType], [
                        'model' => $model,
                        'message' => $message,
                        'dateFormatted' => $dateFormatted,
                        'ioType' => $ioType,
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column">
            <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'message' => $message
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
]); ?>

<?= \frontend\themes\mts\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить доверенность?',
]); ?>

