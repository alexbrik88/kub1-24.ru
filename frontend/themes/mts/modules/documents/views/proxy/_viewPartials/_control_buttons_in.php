<?php
use common\components\date\DateHelper;
use frontend\rbac\permissions\document\Document;
use yii\helpers\Url;
use \common\components\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\document\Proxy;
use common\models\document\status\ProxyStatus;

/** @var \common\models\document\Proxy $model */

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#proxy-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize());
    });
');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$proxyEmployee = \common\models\EmployeeCompany::find()->with(['employee'])->andWhere([
    'employee_id' => $model->proxy_person_id,
    'company_id' => $model->invoice->company_id
])->one();

switch ($model->status_out_id) {
    case ProxyStatus::STATUS_CREATED:
        $icon = 'check-2';
        $color = '#26cd58';
        break;

    case ProxyStatus::STATUS_PRINTED:
        $icon = 'print';
        $color = '#26cd58';
        break;

    case ProxyStatus::STATUS_SEND:
        $icon = 'envelope';
        $color = '#26cd58';
        break;

    case ProxyStatus::STATUS_RECEIVED:
        $icon = 'check-double';
        $color = '#26cd58';
        break;

    case ProxyStatus::STATUS_REJECTED:
        $icon = 'stop';
        $color = '#FAC031';
        break;



    default:
        $icon = '';
        $color = '';
        break;
}
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
                background-color: <?= $color ?>;
                border-color: <?= $color ?>;
                color: #ffffff;
                ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span class="ml-3">
                <?= $model->statusOut->name; ?>
            </span>
            <span class="ml-auto mr-1"><?=  date("d.m.Y", $model->is_original_updated_at ? $model->is_original_updated_at : $model->created_at); ?></span>
        </div>
    </div>
</div>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
    'model' => $model->invoice,
])
): ?>
    <a href="<?php echo Url::toRoute(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id]); ?>"
       class="button-regular button-hover-content-red w-100 text-left pl-3 pr-3">
        <?= $this->render('//svg-sprite', ['ico' => 'new-doc']).'<span class="ml-2">СЧЕТ</span>' ?>
    </a>
<?php endif; ?>

<div class="about-card mb-3 mt-1 pb-3">
    <div class="about-card-item">
        <span class="text-grey">
            Поставщик:
        </span>
        <?= Html::a($model->invoice->contractor_name_short, [
            '/contractor/view',
            'type' => $model->invoice->contractor->type,
            'id' => $model->invoice->contractor->id,
        ]) ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Основание:
        </span>
        <?php if ($model->invoice->basis_document_name &&
        $model->invoice->basis_document_number &&
        $model->invoice->basis_document_date
        ) : ?>
            <?= $model->invoice->basis_document_name ?>
            № <?= Html::encode($model->invoice->basis_document_number) ?>
            от <?= DateHelper::format($model->invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
        <?php else : ?>
            Счет № <?= $model->invoice->fullNumber; ?>
            от <?= DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
        <?php endif; ?>
    </div>
    <div class="about-card-item">
        <span class="text-grey">
            Выдана:
        </span>
        <?= ($proxyEmployee) ? $proxyEmployee->getFio(true) : ''?>
    </div>
    <div class="about-card-item">
        <?= \frontend\themes\mts\modules\documents\widgets\DocumentFileScanWidget::widget([
            'model' => $model,
            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
        ]); ?>
    </div>
</div>