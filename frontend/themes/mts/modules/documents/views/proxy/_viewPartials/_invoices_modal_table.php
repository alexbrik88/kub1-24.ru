<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use frontend\themes\mts\helpers\Icon;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use \frontend\models\Documents;
use \yii\helpers\Url;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */

$employeeArray = ['add-modal-employee' => Icon::PLUS . ' Добавить сотрудника', '' => '---'] + ArrayHelper::map(
    \common\models\EmployeeCompany::find()->where([
        'company_id' => $companyId
    ])->indexBy('lastname')->all(),
    'employee_id',
    function ($model) {
        return $model->getShortFio(true) ?: '(не задан)';
    }
);

$this->registerJs("
    refreshUniform();
    refreshDatepicker();
");
?>

<div class="invoice-list" style="margin-top:15px">
    <?php $form = ActiveForm::begin([
            'method' => 'get',
            'action' => ['/documents/proxy/get-invoices'],
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'validateOnChange' => false,
            'validateOnBlur' => false,
            'layout'  => 'horizontal',
            'options' => [
                'id' => 'search-invoice-form',
                'class' => 'add-to-payment-order',
                'data' => [
                    'pjax' => true,
                ],
            ],
            'fieldConfig' => [
                'options' => [
                    'class' => 'form-group col-6'
                ],
                'labelOptions' => [
                    'class' => 'label',
                ],
                'wrapperOptions' => [
                    'class' => '',
                ],
                'inputOptions' => [
                    'class' => 'form-control'
                ]
            ],
    ]); ?>

    <?= Html::hiddenInput('event_date_change', 0) ?>
    <?= Html::hiddenInput('invoice_id', $invoiceId, ['id' => 'proxy-invoice_id']) ?>
    <?= Html::hiddenInput('from_invoice', $fromInvoice) ?>

    <?php if (!$fromInvoice) : ?>
        <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
        <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
        <?= Html::hiddenInput('label_name', $labelName) ?>

        <div class="table-settings row row_indents_s mb-3">
            <div class="col-12">
                <div class="d-flex flex-nowrap align-items-center">
                    <div class="form-group flex-grow-1 mr-2">
                        <?= Html::activeTextInput($searchModel, 'document_number', [
                            'type' => 'search',
                            'id' => 'invoice-number-search',
                            'placeholder' => '№ счёта...',
                            'class' => 'form-control',
                            'style' => 'margin-bottom: 0;'
                        ]); ?>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Найти', [
                            'class' => 'button-clr button-regular button-regular_padding_bigger button-regular_red',
                            'style' => 'margin-bottom: 0;'
                        ]) ?>
                    </div>
                </div>
            </div>
            <?php /*<div class="col-6">
                <div id="range-ajax-button">
                    <div class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                        data-pjax="get-vacant-invoices-pjax" id="reportrange2" data-toggle="toggleVisible" data-target="calendar">

                        <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                        <b class="fa fa-angle-down"></b>
                    </div>
                </div>
            </div>*/ ?>
        </div>

        <?= GridView::widget([
            'id' => 'invoice-modal-grid-table',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => [
                'class' => 'table table-style table-count-list',
            ],
            'pager' => [
                'options' => [
                    'class' => 'nav-pagination list-clr',
                ],
            ],
            'layout' => "{items}\n{pager}",
            'columns' => [
                [
                    'attribute' => 'invoice_id',
                    'label' => '',
                    'format' => 'raw',
                    'contentOptions' => [
                    ],
                    'value' => function ($data) use ($invoiceId) {
                        /** @var Invoice $data */
                        return Html::checkbox(Html::getInputName($data, 'id') . '[]', $invoiceId == $data->id, [
                            'value' => $data->id,
                            'class' => 'checkbox-invoice-id',
                        ]);
                    },
                    'headerOptions' => [
                        'width' => '4%',
                    ],
                ],
                [
                    'attribute' => 'document_date',
                    'label' => 'Дата счёта',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '24%',
                    ],
                    'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                ],
                [
                    'attribute' => 'document_number',
                    'label' => '№ счёта',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '24%',
                    ],
                    'format' => 'raw',
                    'value' => function ($data) {
                        /** @var Invoice $data */
                        return Html::a($data->fullNumber, ['invoice/view', 'type' => $data->type, 'id' => $data->id], ['data-pjax' => 0, 'target' => '_blank']);
                    },
                ],
                [
                    'label' => 'Сумма',
                    'headerOptions' => [
                        'class' => 'sorting',
                        'width' => '24%',
                    ],
                    'attribute' => 'total_amount_with_nds',
                    'value' => function (Invoice $model) {
                        return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'label' => 'Контрагент',
                    'headerOptions' => [
                        'width' => '24%',
                    ],
                    'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                    'format' => 'raw',
                    'value' => 'contractor_name_short',
                    's2width' => '300px'
                ],
            ],
        ]); ?>

        <div class="help-block help-block-error mb-3" style="margin-top: -8px">
            <?php $invoiceIdErrors = ArrayHelper::getValue($model->getErrors(), 'invoice_id'); ?>
            <?= (!empty($invoiceIdErrors)) ? $invoiceIdErrors[0] : '' ?>
        </div>

    <?php endif; ?>

    <div class="row mt-1">
        <div class="form-group col-6">
            <?= Html::button('Добавить счет', [
                'id' => 'add-new-invoice',
                'class' => 'button-regular button-regular_red button-clr',
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group col-6">
            <label class="label">Действительна по</label>
            <div class="date-picker-wrap" style="width: 130px;">
                <?= \yii\bootstrap4\Html::textInput('Proxy[limit_date_input]', $model->limit_date_input, [
                    'class' => 'form-control date-picker']) ?>
                <svg class="date-picker-icon svg-icon input-toggle">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                </svg>
            </div>
        </div>
    </div>
    <div class="row">
        <?= $form->field($model, 'proxy_person_id')->widget(Select2::class, [
            'data' => $employeeArray,
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'escapeMarkup' => new JsExpression('function(text) {return text;}'),
                'width' => '100%'
            ],
        ])->label('Доверенное лицо'); ?>
    </div>

    <?php $form->end(); ?>

</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::button('Добавить', [
        'id' => 'add-to-invoice',
        'class' => 'button-regular button-width button-regular_red button-clr',
    ]); ?>
    <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
</div>
