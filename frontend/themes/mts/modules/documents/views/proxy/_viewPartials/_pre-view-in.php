<?php
use \yii\helpers\Url;
use \common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use \common\models\document\OrderProxy;
use \common\components\TextHelper;
use \common\models\product\Product;
use \php_rutils\RUtils;
use \common\models\company\CompanyType;
$precision = $model->invoice->price_precision;
$realMassNet = $model->invoice->getRealMassNet();
$realMassGross = $model->invoice->getRealMassGross();
$company = $model->invoice->company;
$addStamp = (boolean) $model->add_stamp;
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$addStamp || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$addStamp || !$company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$addStamp || !$company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 83, 25, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}
$printLink = (!$addStamp || !$company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);
?>
<style>

    .ver-top {vertical-align: top;}
    .font-size-6 {font-size:9px}
    .font-size-7 {font-size:10px}
    .font-size-8 {font-size:11.5px}
    .font-size-8-bold {font-size:11.5px; font-weight: bold}
    .table.no-border, .customer-info.no-border {
        border:none!important;
    }
    .table.no-border td {
        border: none;
    }
    .order-packing .page-number {
        font-size: 8pt;
        font-style: italic;
        text-align: right;
    }
    .order-packing td {
        border: 1px solid #000000;
        padding: 2px 2px!important;
        border-color:#000!important
    }
    .ver-bottom { vertical-align: bottom;!important; }

</style>
<?php if ($addStamp) : ?>
    <style>
        #print_layer {
            height: 230px;
            background-image: url('<?= $printLink ?>');
            background-position: 10% 30px;
            background-repeat: no-repeat;
            -webkit-print-color-adjust: exact;
        }
        #signature1_layer {
            background-image: url('<?= $signatureLink ?>');
            background-position: 30% <?= $model->signed_by_employee_id ? '41px' : '56px'; ?>;
            background-repeat: no-repeat;
            -webkit-print-color-adjust: exact;
        }
        #signature2_layer {
            background-image: url('<?= $signatureLink ?>');
            background-position: 30% <?= $model->signed_by_employee_id ? '85px' : '93px'; ?>;
            background-repeat: no-repeat;
            -webkit-print-color-adjust: exact;
        }
    </style>
<?php endif ?>
        <div class="col-xs-12 pad3">
            <div class="no-border">
                <div class="pad0" style="position: relative;">
                    <div class="caption">
                        <div>
                            <div class="portlet customer-info no-border" style="margin-bottom:0px">
                                <div class="portlet-title pad0" style="position: relative;">
                                    <div class="caption">
                                        <div style="padding-right: 60px; font-size: 18px; font-weight: bold">
                                            <?= $message->get(\frontend\modules\documents\components\Message::TITLE_SHORT_SINGLE); ?>
                                            № <span class="editable-field"><?= $model->getDocumentNumber(); ?></span>
                                            <span class="editable-field"><?= $model->document_additional_number; ?></span>
                                            от
                                            <span id="document_date_item" class="editable-field"  data-tooltip-content="#tooltip_document_date"><?= $dateFormatted; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <style>
                                .proxy .bt {border-top: 1px solid #000!important;}
                                .proxy .bl {border-left: 1px solid #000!important;}
                                .proxy .br {border-right: 1px solid #000!important;}
                                .proxy .bb {border-bottom: 1px solid #000!important;}
                                .proxy .bt2 {border-top: 2px solid #000!important;}
                                .proxy .bl2 {border-left: 2px solid #000!important;}
                                .proxy .br2 {border-right: 2px solid #000!important;}
                                .proxy .bb2 {border-bottom: 2px solid #000!important;}
                                .proxy .bt2 {border-top: 2px solid #000!important;}
                                .proxy p {margin:0 !important;}
                                .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                                    padding: 2px !important;
                                    border: 1px solid #000 !important;
                                }
                                .no-border > thead > tr > th, .no-border > tbody > tr > th, .no-border > tfoot > tr > th, .no-border > thead > tr > td, .no-border > tbody > tr > td, .no-border > tfoot > tr > td {
                                    border:none!important;
                                }
                            </style>
                            <?= $this->render('../pdf-view', ['model' => $model, 'preview_only' => true]) ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
