<?php
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

if (!isset($fromInvoice))
    $fromInvoice = 0;

?>

<?= $this->render('_style') ?>
<?= $this->render('_add_employee_modal') ?>

<?php Modal::begin([
    'id' => 'accounts-list',
    'title' => '',
    'toggleButton' => false,
    'options' => [
        'style' => 'z-index: 99998']
]) ?>

    <h4 class="modal-title">Создание доверенности к счету</h4>
    <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
        <svg class="svg-icon">
            <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
        </svg>
    </button>

    <?php Pjax::begin([
        'id' => 'get-vacant-invoices-pjax',
        'formSelector' => '#search-invoice-form',
        'timeout' => 5000,
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

    <div class="invoice-list"></div>

    <div class="mt-3 d-flex justify-content-between">
        <?= Html::button('Добавить', [
            'id' => 'add-to-invoice',
            'class' => 'button-regular button-width button-regular_red button-clr',
        ]); ?>
        <button type="button" class="button-clr button-width button-regular button-hover-transparent" data-dismiss="modal">Отменить</button>
    </div>
    <?php Pjax::end(); ?>
<?php Modal::end(); ?>

<?php
$this->registerJs("
    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"label_name\"]').val(picker.chosenLabel);
        $(form).find('input[name=\"event_date_change\"]').val(1);
        $('#search-invoice-form').submit();
    });

    $('.add-proxy').on('click', function (e) {
        e.preventDefault();
        $('#accounts-list').modal();
        $.pjax({
            url: '/documents/proxy/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {from_invoice: {$fromInvoice}},
            push: false,
            timeout: 5000
        });
    });

    $(document).on('click', '.checkbox-invoice-id', function() {
        var checkboxes = $('.checkbox-invoice-id');
        var addNew = $('#add-new-invoice');
        if ($(checkboxes).filter(':checked').length > 1) {
            $(checkboxes).filter(':checked').prop('checked', false);
            $(this).prop('checked', true);
            $(checkboxes).uniform();
        }
    });

    $(document).on('click', '#add-to-invoice', function () {
        var checkboxes = $('.checkbox-invoice-id');
        var invoiceId = $(checkboxes).filter(':checked').first().val() || -1;
        $('#proxy-invoice_id').val(invoiceId);
        $('#search-invoice-form').submit();
    });

    $(document).on('click', '#add-new-invoice', function() {
        $('#proxy-invoice_id').val(0);    
        $('#search-invoice-form').submit();
    });

    $(document).on('pjax:success', '#get-vacant-invoices-pjax',  function() {
        // $('.date-picker').datepicker({format: 'dd.mm.yyyy', language:'ru', autoclose: true}).on('change.dp', dateChanged);
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"event_date_change\"]').val(0);

    });

"); ?>


