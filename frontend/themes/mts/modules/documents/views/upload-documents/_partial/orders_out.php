<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.06.2017
 * Time: 19:06
 */

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\UploadedDocuments;
use common\models\document\UploadedDocumentOrder;

/* @var $this yii\web\View
 * @var $uploadedDocument UploadedDocuments
 * @var $uploadedDocumentOrder UploadedDocumentOrder
 */
?>
<div class="portlet overl-auto">
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr class="heading" role="row">
            <th width="5%" tabindex="0" aria-controls="datatable_ajax"
                rowspan="1" colspan="1">
                №
            </th>
            <th width="20%" tabindex="0" aria-controls="datatable_ajax"
                rowspan="1" colspan="1">
                Наименование
            </th>
            <th width="10%" class="" tabindex="0"
                aria-controls="datatable_ajax" rowspan="1" colspan="1">
                Количество
            </th>
            <th width="5%" class="" tabindex="0"
                aria-controls="datatable_ajax" rowspan="1" colspan="1">
                Ед.измерения
            </th>
            <th width="15%" class="" tabindex="0"
                aria-controls="datatable_ajax" rowspan="1" colspan="1">
                Цена
            </th>
            <th width="15%" class="" tabindex="0"
                aria-controls="datatable_ajax" rowspan="1" colspan="1">
                Сумма
            </th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($uploadedDocument->uploadedDocumentOrders as $key => $uploadedDocumentOrder): ?>
            <tr role="row" class="odd">
                <td><?= ++$key; ?></td>
                <td id="uploadeddocumentorder-<?= $uploadedDocumentOrder->id ?>-name-td"
                    style="<?= $uploadedDocumentOrder->validate(['name']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <?= $uploadedDocumentOrder->name; ?>
                </td>
                <td id="uploadeddocumentorder-<?= $uploadedDocumentOrder->id ?>-count-td"
                    style="<?= $uploadedDocumentOrder->validate(['count']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <?= $uploadedDocumentOrder->count; ?>
                </td>
                <td id="uploadeddocumentorder-<?= $uploadedDocumentOrder->id ?>-product_unit_id-td"
                    style="<?= $uploadedDocumentOrder->validate(['product_unit_id']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <?= $uploadedDocumentOrder->productUnit ? $uploadedDocumentOrder->productUnit->name : '---'; ?>
                </td>
                <td class="text-left"
                    id="uploadeddocumentorder-<?= $uploadedDocumentOrder->id ?>-price-td"
                    class="text-right"
                    style="<?= $uploadedDocumentOrder->validate(['price']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <?= TextHelper::invoiceMoneyFormat($uploadedDocumentOrder->price, 2); ?>
                </td>
                <td class="text-right"
                    id="uploadeddocumentorder-<?= $uploadedDocumentOrder->id ?>-amount-td"
                    style="<?= $uploadedDocumentOrder->validate(['amount']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <?= TextHelper::invoiceMoneyFormat($uploadedDocumentOrder->amount, 2); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<div class="portlet pull-right m-t-n-10 total-amount-out">
    <table class="table table-resume">
        <tbody>
        <tr role="row" class="even">
            <td><b>Итого:</b></td>
            <td id="uploadeddocuments-total_amount_no_nds-td"
                style="<?= $uploadedDocument->validate(['total_amount_no_nds']) ? '' : 'color:#ef9ba8;'; ?>">
                <?= TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_no_nds, 2); ?>
            </td>
        </tr>
        <tr role="row" class="odd">
            <?php if ($uploadedDocument->nds_view_type_id !== Invoice::NDS_VIEW_WITHOUT): ?>
                <td><b>В том числе НДС:</b></td>
                <td id="uploadeddocuments-total_amount_nds-td"
                    style="<?= $uploadedDocument->validate(['total_amount_nds']) ? '' : 'color:#ef9ba8;'; ?>">
                    <?= TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_nds, 2); ?>
                </td>
            <?php else: ?>
                <td><b>Без налога (НДС):</b></td>
                <td>-</td>
            <?php endif; ?>
        </tr>
        <tr role="row" class="even">
            <td><b>Всего к оплате:</b></td>
            <td id="uploadeddocuments-total_amount_with_nds-td"
                style="<?= $uploadedDocument->validate(['total_amount_with_nds']) ? '' : 'color:#ef9ba8;'; ?>">
                <?= TextHelper::invoiceMoneyFormat($uploadedDocument->total_amount_with_nds, 2); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>
