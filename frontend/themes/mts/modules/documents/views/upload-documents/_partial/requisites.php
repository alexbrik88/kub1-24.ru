<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.06.2017
 * Time: 11:00
 */

use common\components\helpers\ArrayHelper;
use common\models\document\UploadedDocuments;
use yii\bootstrap4\ActiveForm;
use common\components\date\DateHelper;
use yii\bootstrap4\Html;
use common\models\company\CompanyType;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $uploadedDocument UploadedDocuments
 * @var $form ActiveForm
 * @var $existsContractor boolean
 */

$inputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
        'style' => 'width:34%;font-weight: bold;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-7 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

?>
<div class="requisites-block uploaded-documents-block">
    <div class="col-md-12" style="padding-right: 1px;margin-bottom: 10px;">
        <?= Html::a('<i class="icon-pencil"></i>', 'javascript:;', [
            'title' => 'Редактировать',
            'class' => 'btn darkblue btn-sm update-uploaded-documents-requisites out',
            'style' => 'float: right;',
        ]); ?>

        <?= Html::a('<i class="ico-Cancel-smart-pls fs" style="font-size:17px;"></i>', 'javascript:;', [
            'title' => 'отменить',
            'class' => 'btn darkblue btn-sm in undo-update-uploaded-documents-requisites',
            'style' => 'float: right;color:white;',
        ]); ?>

        <?= Html::a('<i class="fa fa-floppy-o fa-2x" style="font-size:16px!important;"></i>', 'javascript:;', [
            'title' => 'Сохранить',
            'class' => 'btn darkblue btn-sm in save-upload-documents-requisites',
            'style' => 'float: right;margin-right: 5px;color:white;',
        ]); ?>
    </div>
    <div class="portlet overl-auto">
        <table
            class="table table-striped table-bordered table-hover table-requisites">
            <tr>
                <td colspan="2" width="45%" id="uploadeddocuments-bank_name-td"
                    style="<?= $uploadedDocument->validate(['bank_name']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <span class="out">
                        <?= $uploadedDocument->bank_name; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'bank_name', [
                        'class' => 'form-control in',
                    ]); ?>
                </td>
                <td width="10%">
                    БИК
                </td>
                <td width="35%" id="uploadeddocuments-bik-td"
                    style="<?= $uploadedDocument->validate(['bik']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <span class="out">
                        <?= $uploadedDocument->bik; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'bik', [
                        'class' => 'form-control in',
                    ]); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="45%"
                    style="border-top: 0;height: 50px;vertical-align: bottom;">
                    Банк получателя
                </td>
                <td width="10%">
                    Сч. №
                </td>
                <td width="35%"
                    style="border-top: 0; <?= $uploadedDocument->validate(['ks']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>"
                    id="uploadeddocuments-ks-td">
                    <span class="out">
                        <?= $uploadedDocument->ks; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'ks', [
                        'class' => 'form-control in',
                    ]); ?>
                </td>
            </tr>
            <tr>
                <td width="22.5%" id="uploadeddocuments-inn-td"
                    style="<?= $uploadedDocument->validate(['inn']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <span style="display:inline-block;">ИНН</span>
                    <span class="out">
                        <?= $uploadedDocument->inn; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'inn', [
                        'class' => 'form-control in inline',
                    ]); ?>
                </td>
                <td width="22.5%" id="uploadeddocuments-kpp-td"
                    style="<?= $uploadedDocument->validate(['kpp']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <span style="display:inline-block;">КПП</span>
                    <span class="out">
                        <?= $uploadedDocument->kpp; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'kpp', [
                        'class' => 'form-control in inline',
                    ]); ?>
                </td>
                <td width="10%">Сч. №</td>
                <td width="35%" id="uploadeddocuments-rs-td"
                    style="<?= $uploadedDocument->validate(['rs']) ? '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>">
                    <span class="out">
                        <?= $uploadedDocument->rs; ?>
                    </span>
                    <?= Html::activeTextInput($uploadedDocument, 'rs', [
                        'class' => 'form-control in',
                    ]); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="45%"
                    id="uploadeddocuments-contractor_name-td" <?= $uploadedDocument->validate(['contractor_name', 'contractor_type_id']) ?
                    '' : 'background-color:#f3565d!important; opacity:0.3;'; ?>>
                    <span class="out">
                        <?= $uploadedDocument->getFullContractorName(); ?>
                    </span>
                    <?= Html::activeDropDownList($uploadedDocument, 'contractor_type_id',
                        ArrayHelper::map(CompanyType::find()->inContractor()->all(), 'id', 'name_short'), [
                            'class' => 'form-control in inline',
                        ]); ?>
                    <?= Html::activeTextInput($uploadedDocument, 'contractor_name', [
                        'class' => 'form-control in inline',
                    ]); ?>
                </td>
                <td width="10%" style="border-top: 0;"></td>
                <td width="35%" style="border-top: 0;"></td>
            </tr>
            <tr>
                <td colspan="2" width="45%"
                    style="border-top: 0;vertical-align: bottom;">
                    Получатель
                </td>
                <td width="10%" style="border-top: 0;"></td>
                <td width="35%" style="border-top: 0;"></td>
            </tr>
        </table>
    </div>
    <div class="form-actions in">
        <div class="row action-buttons">
            <div class="col-sm-3 col-xs-3">
                <?= Html::a('Сохранить', 'javascript:;', [
                    'class' => 'btn darkblue text-white widthe-100 hidden-md hidden-sm hidden-xs save-upload-documents-requisites',
                ]); ?>
                <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                    'class' => 'btn darkblue text-white widthe-100 hidden-lg save-upload-documents-requisites',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="col-sm-6 col-xs-6"></div>
            <div class="col-sm-3 col-xs-3">
                <?= Html::a('Отменить', 'javascript:;', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undo-update-uploaded-documents-requisites',
                ]); ?>
                <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                    'class' => 'btn darkblue widthe-100 hidden-lg undo-update-uploaded-documents-requisites',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 p-l-0 p-r-0 text-center invoice-number-block">
    <?= $form->field($uploadedDocument, 'document_number', array_merge($inputConfig, [
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block;margin-bottom: 0;',
        ],
        'inputOptions' => [
            'class' => 'form-control m-l-n sel-w',
            'data' => [
                'id' => 'document_number',
                'url' => Url::to(['change-document-number', 'id' => $uploadedDocument->id]),
            ],
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7 inp_one_line width-inp p-l-0',
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-2 p-r-0',
            'style' => 'width:30%;font-weight: bold;',
        ],
    ])); ?>

    <?= $form->field($uploadedDocument, 'document_date', array_merge($inputConfig, [
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block;margin-bottom: 0;',
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-2 p-l-0 p-r-0',
            'style' => 'width:16%;font-weight: bold;',
        ],
        'inputOptions' => [
            'class' => 'form-control m-l-n sel-w date-picker',
            'value' => DateHelper::format($uploadedDocument->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'data' => [
                'id' => 'document_date',
                'url' => Url::to(['change-document-date', 'id' => $uploadedDocument->id]),
            ],
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7 inp_one_line width-inp p-l-0',
        ],
        'template' => "{label}\n{beginWrapper}\n<div class='input-icon'>
                                           <i class='fa fa-calendar'></i>{input}\n{error}\n</div>{endWrapper}",
    ])); ?>
</div>
