<?php

use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\Html;

/** @var \common\models\document\Upd $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

?>

<div class="col-lg-5 col-w-lg-4 col-md-5 control-panel margin991 pull-right">
    <div class="status-panel btn-group pull-right">
        <div class="btn darkblue no-margin-l box-status-act" title="Дата изменения статуса">
            <?= date("d.m.Y", $model->status_out_updated_at); ?>
        </div>
        <div class="btn btn-status darkblue" title="Статус">
            <i class="icon pull-left <?= $model->statusOut->getIcon(); ?>"></i>
            <?= $model->statusOut->name; ?>
        </div>
    </div>
    <div class="document-panel widthe-i box-width-100">
        <?php if ($model->getInvoices()->count() == 1): ?>
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                'model' => $model->invoice,
            ])): ?>
                <a href="<?= Url::toRoute(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id, 'contractorId' => $contractorId,]); ?>"
                   class="btn btn-account yellow pull-right width179">
                    <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                    СЧЕТ
                </a>
            <?php endif; ?>
        <?php else: ?>
            <a data-toggle="dropdown" class="btn yellow dropdown-toggle width179 pull-right"
               style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;"
               title="Счет">
                <i class="pull-left icon icon-doc"></i>
                Счет
            </a>
            <ul class="dropdown-menu documents-dropdown" style="right: 20px;left: auto;">
                <?php foreach ($model->invoices as $invoice): ?>
                    <li>
                        <a href="<?= Url::to(['invoice/view', 'type' => $model->type, 'id' => $invoice->id, 'contractorId' => $contractorId,]) ?>"
                           class="dropdown-item " style="position: relative;">
                            <span class="dropdown-span-left">
                                № <?= $invoice->document_number; ?>
                                от <?= DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            </span>
                            <span class="dropdown-span-right">
                                <i class="fa fa-rub"></i>
                                <?= TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) ?>
                            </span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>

</div>