<?php

use common\models\document\status\UpdStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\BoolleanSwitchWidget;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */

$status = $model->statusOut;

$statusIcon = [
    UpdStatus::STATUS_CREATED => 'check-2',
    UpdStatus::STATUS_PRINTED => 'print',
    UpdStatus::STATUS_SEND => 'check-2',
    UpdStatus::STATUS_RECEIVED => 'check-double',
    UpdStatus::STATUS_REJECTED => 'check-2',
];
$statusColor = [
    UpdStatus::STATUS_CREATED => '#26cd58',
    UpdStatus::STATUS_PRINTED => '#FAC031',
    UpdStatus::STATUS_SEND => '#FAC031',
    UpdStatus::STATUS_RECEIVED => '#26cd58',
    UpdStatus::STATUS_REJECTED => '#E30611',
];
$icon = ArrayHelper::getValue($statusIcon, $status->id);
$color = ArrayHelper::getValue($statusColor, $status->id);
?>

<div class="sidebar-title d-flex flex-wrap align-items-center">
    <div class="column flex-grow-1 mt-1 mt-xl-0">
        <div class="button-regular mb-3 pl-3 pr-3 w-100" style="
            background-color: <?= $color ?>;
            border-color: <?= $color ?>;
            color: #ffffff;
        ">
            <?= $this->render('//svg-sprite', ['ico' => $icon]) ?>
            <span class="ml-3"><?= $status->name ?></span>
            <span class="ml-auto mr-1"><?= date('d.m.Y', $model->status_out_updated_at) ?></span>
        </div>
    </div>
</div>
<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
    'model' => $model->invoice,
])) : ?>
    <?= Html::a($this->render('//svg-sprite', ['ico' => 'new-doc']).' <span class="ml-3 mr-1">СЧЕТ</span>', [
        '/documents/invoice/view',
        'type' => $model->type,
        'id' => $model->invoice->id,
    ], [
        'class' => 'button-regular button-hover-content-red w-100 text-left pl-3 pr-3',
    ]) ?>
<?php endif; ?>