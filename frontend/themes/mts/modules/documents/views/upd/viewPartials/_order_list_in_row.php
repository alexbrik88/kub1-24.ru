<?php

use common\components\TextHelper;
use common\models\document\OrderUpd;
use common\models\product\Product;
use yii\helpers\Html;

/* @var \common\models\document\PackingList $model */
/* @var \common\models\document\orderUpd $orderUpd */
/* @var \common\models\Company $company */
/* @var integer $key */

$order = $orderUpd->order;
$invoice = $model->invoice;
$hasNds = $invoice->hasNds;

$product = $order->product;
$isGoods = ($product->production_type == Product::PRODUCTION_TYPE_GOODS);
$isService = $product->production_type == Product::PRODUCTION_TYPE_SERVICE;
$hideUnits = ($isService && !$model->show_service_units);
$maxQuantity = OrderUpd::availableQuantity($order, $model->id);
if ($maxQuantity != intval($maxQuantity)) {
    $maxQuantity = rtrim(number_format($maxQuantity, 10, '.', ''), 0);
}
$quantityValue = ($isGoods || $orderUpd->quantity != 1) ? $orderUpd->quantity : Product::DEFAULT_VALUE;

$unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
$amountNoNds = $model->getPrintOrderAmount($orderUpd->order_id, $hasNds);
$amountWithNds = $model->getPrintOrderAmount($orderUpd->order_id);
?>
<tr role="row" class="odd order">
    <td><?= $order->product_title ?></td>
    <td><?= $product->item_type_code ?: Product::DEFAULT_VALUE; ?></td>
    <td>
        <?= Html::tag('span', $unitName, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td><?= $isGoods && $product->code ? $product->code : Product::DEFAULT_VALUE; ?></td>
    <td>
        <?php $value = $product->productUnit ? $product->productUnit->code_okei : Product::DEFAULT_VALUE; ?>
        <?= Html::tag('span', $value, [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td><?= $isGoods && $order->box_type ? $order->box_type : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->count_in_place ? $order->count_in_place : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->place_count ? $order->place_count : Product::DEFAULT_VALUE; ?></td>
    <td><?= $isGoods && $order->mass_gross ? $order->mass_gross : Product::DEFAULT_VALUE; ?></td>
    <td>
        <?= $isService ? Html::tag('span', Product::DEFAULT_VALUE, [
            'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
        ]) : null; ?>
        <?= Html::input('number', "orderParams[{$order->id}][quantity]", $orderUpd->quantity, [
            'style' => 'width: 75px;',
            'class' => 'form-control-number product-count width-100' . (
                $isService ? ' service_units_visible' . ($hideUnits ? ' hide' : '') : ''
            ),
            'max' => $maxQuantity,
            'min' => 0,
        ]); ?>
    </td>
    <td class="text-right">
        <?= Html::tag('span', TextHelper::invoiceMoneyFormat($orderUpd->priceNoNds, $precision), [
            'class' => $isService ? 'service_units_visible' . ($hideUnits ? ' hide' : '') : null,
        ]); ?>
        <?php if ($isService) {
            echo Html::tag('span', Product::DEFAULT_VALUE, [
                'class' => 'service_units_hidden' . ($hideUnits ? '' : ' hide'),
            ]);
        } ?>
    </td>
    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
    <td><?= $hasNds ? $order->purchaseTaxRate->name : 'Без НДС'; ?></td>
    <td class="text-right"><?= $hasNds ? TextHelper::invoiceMoneyFormat($orderUpd->amountNds, $precision) : Product::DEFAULT_VALUE; ?></td>
    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
    <td>
        <?= Html::dropDownList(
            "orderParams[{$order->id}][country_id]",
            $orderUpd->country_id,
            $countryList,
            [
                'class' => 'form-control',
                'style' => 'width: 150px;',
            ]
        ); ?>
    </td>
    <td>
        <?= Html::textInput(
            "orderParams[{$order->id}][custom_declaration_number]",
            $orderUpd->custom_declaration_number,
            [
                'class' => 'form-control',
            ]
        ); ?>
    </td>
    <td>
        <button class="delete-row button-clr" type="button">
            <svg class="table-count-icon svg-icon">
                <use xlink:href="/images/svg-sprite/svgSprite.svg#circle-close"></use>
            </svg>
        </button>
    </td>
</tr>
