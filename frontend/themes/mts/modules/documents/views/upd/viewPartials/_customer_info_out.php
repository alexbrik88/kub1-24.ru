<?php

use common\components\date\DateHelper;
use common\models\document\UpdPaymentDocument;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\product\Product;
use kartik\select2\Select2;
use common\models\document\Upd;
use \philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\models\Contractor;
use \frontend\themes\mts\helpers\Icon;

/* @var \yii\web\View $this */
/* @var \common\models\document\Upd $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$basisDocumentDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = 'Счет&' . $model->invoice->fullNumber . '&' . $model->invoice->document_date . '&';
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$agreementDropDownList = ['' => '   '] + ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');

$paymentDocumentsArray = $model->paymentDocuments;
$paymentDocuments = [];
if ($paymentDocumentsArray) {
    foreach ($paymentDocumentsArray as $doc) {
        $date = date('d.m.Y', strtotime($doc->payment_document_date));
        $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
    }
} else {
    $paymentDocumentsArray[] = new UpdPaymentDocument;
}

$hasService = $model->getOrders()->joinWith('product product')
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])->exists();

$showAddress = false;
if ($model->consignee) {
    if (!empty($model->consignee->actual_address) && $model->consignee->legal_address !== $model->consignee->actual_address) {
        $showAddress = true;
    }
} else {
    if (!empty($model->invoice->contractor->actual_address) && $model->invoice->contractor->legal_address !== $model->invoice->contractor->actual_address) {
        $showAddress = true;
    }
}
?>

<div class="wrap">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="column">
            <div class="row row_indents_s flex-nowrap align-items-center">
                <div class="form-title d-inline-block column">
                    <span>
                        <span>
                            Исходящий УПД
                        </span>
                    </span>
                    <span>№</span>
                </div>
                <!-- document_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?= \yii\helpers\Html::activeTextInput($model, 'document_number', [
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control',
                    ]); ?>
                </div>
                <!-- document_additional_number -->
                <div class="form-group d-inline-block mb-0 col-xl-2">
                    <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
                        <?= Html::activeTextInput($model, 'document_additional_number', [
                            'maxlength' => true,
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                            'placeholder' => 'доп. номер',
                            'style' => 'display: inline;',
                        ]); ?>
                    <?php endif ?>
                </div>
                <div class="form-txt d-inline-block mb-0 column">от</div>
                <!-- document_date -->
                <div class="form-group d-inline-block mb-0">
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control form-control_small date-picker invoice_document_date',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <div class="row d-block">
        <div class="form-group col-6">
            <label class="label">Грузоотправитель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignor_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузоотправителя '] + $model->consignorArray,
                'options' => [
                    'value' => $model->consignor_id ?: '',
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Покупатель<span class="important">*</span></label>
                <div class="row">
                    <div class="col-6">
                        <?php echo Select2::widget([
                            'name' => 'contractor_id',
                            'data' => [$model->invoice->contractor_id => $model->invoice->contractor_name_short],
                            'disabled' => true,
                            'pluginOptions' => [
                                'width' => '100%'
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-12">
            <div class="row">
                <div class="col-3">
                    <label class="label">Транспортная накладная №</label>
                    <?= Html::activeTextInput($model, 'waybill_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field ',
                        'placeholder' => 'номер',
                    ]); ?>
                </div>
                <div class="col-3">
                    <label class="label">От</label>
                    <div class="date-picker-wrap">
                        <?= Html::activeTextInput($model, 'waybill_date', [
                            'class' => 'form-control date-picker']) ?>
                        <svg class="date-picker-icon svg-icon input-toggle">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="form-group col-12">
            <label class="label">К платежно-расчётному документу</label>
            <?php if ($paymentDocumentsArray) : ?>
            <?php /** @var $doc UpdPaymentDocument */ ?>
            <div id="payment-inputs-block">
                <?php foreach ($paymentDocumentsArray as $doc) : ?>
                    <div class="row payment-row mb-2">
                        <div class="col-3">
                            <?= Html::textInput('payment[number][]', $doc->payment_document_number, [
                                'class' => 'form-control',
                                'placeholder' => 'номер',
                            ]) ?>
                        </div>
                        <div class="col-3">
                            <div class="date-picker-wrap">
                                <?= Html::textInput('payment[date][]',
                                    $doc->payment_document_date ? date('d.m.Y', strtotime($doc->payment_document_date)) : '', [
                                    'class' => 'form-control date-picker',
                                    //'placeholder' => 'дата',
                                ]) ?>
                                <svg class="date-picker-icon svg-icon input-toggle">
                                    <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                                </svg>
                            </div>
                            <span class="close del-payment-doc" style="position: absolute;font-size: 26px;padding: 3px;right: -15px;top:2px;cursor: pointer">×</span>
                        </div>
                    </div>
                <?php endforeach; ?>
                <!-- template -->
                <div class="row payment-row-template mb-2" style="display: none">
                    <div class="col-3">
                        <?= Html::textInput('payment[number][]', null, [
                            'class' => 'form-control',
                            'placeholder' => 'номер',
                        ]) ?>
                    </div>
                    <div class="col-3">
                        <div class="date-picker-wrap">
                            <?= Html::textInput('payment[date][]', null, [
                                    'class' => 'form-control date-picker-field',
                                    //'placeholder' => 'дата',
                                ]) ?>
                            <svg class="date-picker-icon svg-icon input-toggle">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#calendar"></use>
                            </svg>
                        </div>
                        <span class="close del-payment-doc" style="position: absolute;font-size: 26px;padding: 3px;right: -15px;top:2px;cursor: pointer">×</span>
                    </div>
                </div>
            </div>
            <?php endif ?>
            <div class="row">
                <div class="col-3">
                    <div id="add-payment-doc" class="btn-group pull-left" style="display: inline-block;">
                        <span class="btn-add-line-table button-regular button-hover-content-red" style="width:40px">
                            <svg class="svg-icon">
                                <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                            </svg>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <div class="form-group col-6">
            <label class="label">Гос.контракт, договор (соглашение)</label>
            <?= Html::activeTextInput($model, 'state_contract', [
                'maxlength' => true,
                'class' => 'form-control input-editable-field ',
                'placeholder' => '',
            ]); ?>
        </div>
        <br>


        <div class="form-group col-12">
            <div class="form-filter">
                <label class="label" for="client">Основание</label>
                <div class="row">
                    <div class="col-6">
                        <?php Pjax::begin([
                            'id' => 'agreement-pjax-container',
                            'enablePushState' => false,
                            'linkSelector' => false,
                        ]);
                        echo Select2::widget([
                            'model' => $model,
                            'attribute' => 'agreement',
                            'data' => $agreementDropDownList,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'width' => '100%',
                                'escapeMarkup' => new JsExpression('function(text) {return text;}')
                            ],
                        ]);
                        Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="form-group col-6">
            <label class="label">Грузополучатель</label>

            <?= Select2::widget([
                'model' => $model,
                'attribute' => 'consignee_id',
                'data' => ['add-modal-contractor' => Icon::PLUS . ' Добавить грузополучателя '] + $model->consignorArray,
                'options' => [
                    'value' => $model->consignor_id ?: '',
                    'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                ],
                'pluginOptions' => [
                    'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 40); }'),
                    'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                    'width' => '100%'
                ],
            ]); ?>
        </div>
        <br>
        <div class="form-group col-6 mb-0">
            <?= Html::activeCheckbox($model, 'add_stamp', [
                'class' => '',
                'label' => false,
            ]); ?>
            <label class="label ml-1" for="packinglist-add_stamp">Печать и подпись</label>
            <span class="tooltip-hover ico-question valign-middle"
                  data-tooltip-content="#tooltip_add_stamp"
                  style="vertical-align: top;">
                </span>
        </div>
        <br>
        <div class="form-group col-6 mb-0">
            <?= Html::activeCheckbox($model, 'show_service_units', [
                'class' => '',
                'label' => false,
            ]); ?>
            <label class="label ml-1" for="packinglist-add_stamp">Отобразить данные по услугам в столбцах 4, 6, 11, 12</label>
            <span class="tooltip-hover ico-question valign-middle"
                  data-tooltip-content="#tooltip_add_stamp"
                  style="vertical-align: top;">
                </span>
        </div>

    </div>
</div>

<div class="tooltip_templates" style="display: none;">
        <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
            Добавить в ТН печать и подпись
            <br>
            при отправке по e-mail и
            <br>
            при скачивании в PDF
        </span>
</div>

<?php /*
    <div class="portlet customer-info">
        <div class="portlet-title">
            <div class="actions">
                <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                    <button type="submit" class="btn-save btn darkblue btn-sm hide" style="color: #FFF;"
                            title="Сохранить">
                        <span class="ico-Save-smart-pls"></span>
                    </button>
                    <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', [
                    'view',
                    'type' => $model->type,
                    'id' => $model->id
                ], [
                    'class' => 'btn darkblue btn-sm upd-reload input-editable-field hide',
                    'style' => "color: #FFF;",
                    'title' => "Отменить"
                ]) ?>
                    <a href="#" class="edit edit-in btn darkblue btn-sm" title="Редактировать">
                        <i class="icon-pencil"></i>
                    </a>
                <?php endif; ?>
            </div>
            <div class="caption">
                <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                №
                <span class="editable-field">
                <?= $model->fullNumber; ?>
            </span>
                <?php
                echo Html::activeTextInput($model, 'document_number', [
                    'data-required' => 1,
                    'class' => 'form-control input-editable-field hide',
                    'style' => 'max-width: 60px; display:inline-block; margin-right: 10px;',
                    'value' => $model->getDocumentNumber(),
                ]);
                echo Html::activeTextInput($model, 'document_additional_number', [
                    'maxlength' => true,
                    'class' => 'form-control  input-editable-field hide',
                    'placeholder' => 'доп. номер',
                    'style' => 'max-width: 110px; display:inline-block;',
                ]);
                ?>
                от
                <span class="editable-field"><?= $dateFormatted; ?></span>
                <span class="input-icon input-calendar input-editable-field hide margin-top-15">
                <i class="fa fa-calendar"></i>
                    <?= Html::activeTextInput($model, 'documentDate', [
                        'class' => 'form-control date-picker',
                        'data-date-viewmode' => 'years',
                    ]); ?>
            </span>
            </div>
        </div>
        <div class="portlet-body no_mrg_bottom">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                Грузоотправитель:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <div class="editable-field">
                                    <?= $model->consignor ? $model->consignor->getTitle(true) : 'Он же'; ?>
                                </div>
                                <div class="input-editable-field hide">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignor_id',
                                        'data' => ['add-modal-contractor' => '[ + Добавить грузоотправителя ]'] + $model->consignorArray,
                                        'options' => [
                                            'value' => $model->consignor_id ?: '',
                                            'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                                        ],
                                        'pluginOptions' => [
                                            'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 20); }'),
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                            </div>
                            <div class="col-sm-6 upd-info-value">
                            <span>
                                <?= Html::a($model->invoice->contractor_name_short, [
                                    '/contractor/view',
                                    'type' => $model->invoice->contractor->type,
                                    'id' => $model->invoice->contractor->id,
                                ]) ?>
                            </span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="box-tr-pac-list">
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                Транспортная накладная:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <span class="editable-field">№ <?= $model->waybill_number ? $model->waybill_number : '---'; ?></span>
                                <?= Html::activeTextInput($model, 'waybill_number', [
                                    'maxlength' => true,
                                    'data-required' => 1,
                                    'class' => 'form-control  input-editable-field hide',
                                    'placeholder' => 'номер',
                                    'style' => 'width:120px; display: inline-block;',
                                ]); ?>
                                от
                                <span class="editable-field"><?= $model->waybill_date ? $waybillDateFormatted : '---'; ?></span>

                                <div class="input-icon input-calendar input-editable-field hide">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'waybillDate', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'size' => 16,
                                        'data-date-viewmode' => 'years',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                К платежно-расчётному документу:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <span class="editable-field"><?= $paymentDocuments ? join(', ', $paymentDocuments) : '№ --- от ---'; ?></span>
                                <div class="input-editable-field hide">
                                    <div id="payment-inputs-block">
                                        <?php if ($paymentDocumentsArray) : ?>
                                            <?php foreach ($paymentDocumentsArray as $doc) : ?>
                                                <div class="payment-row" style="margin-bottom: 2px;">
                                                    <?= Html::textInput('payment[number][]', $doc->payment_document_number, [
                                                        'class' => 'form-control',
                                                        'style' => 'width:120px; display: inline-block;',
                                                        // 'placeholder' => '№',
                                                    ]) ?>
                                                    от
                                                    <div class="input-icon input-calendar input-editable-field hide">
                                                        <i class="fa fa-calendar"></i>
                                                        <?= Html::textInput(
                                                            'payment[date][]',
                                                            $doc->payment_document_date ? date('d.m.Y', strtotime($doc->payment_document_date)) : '',
                                                            [
                                                                'class' => 'form-control date-picker',
                                                                'size' => 16,
                                                                'data-date-viewmode' => 'years',
                                                                'style' => 'width:120px; display: inline-block;',
                                                            ]
                                                        ) ?>
                                                    </div>
                                                    <span class="close del-payment-doc"
                                                          style="float: none; font-size: inherit; padding: 3px; line-height: 5px;">×</span>
                                                </div>
                                            <?php endforeach ?>
                                        <?php else : ?>

                                        <?php endif ?>
                                    </div>
                                    <div>
                                        <div class="portlet pull-left control-panel button-width-table">
                                            <div id="add-payment-doc" class="btn-group pull-left"
                                                 style="display: inline-block;">
                                            <span class="btn yellow btn-add-line-table">
                                                <i class="pull-left fa icon fa-plus-circle"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                <?= $model->getAttributeLabel('state_contract') ?>:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <div class="editable-field">
                                    № <?= $model->state_contract ? $model->state_contract : '---' ?>
                                </div>
                                <div class="input-editable-field hide">
                                    <?= Html::activeTextInput($model, 'state_contract', [
                                        'maxlength' => true,
                                        'class' => 'form-control',
                                        'style' => 'width: 100%;',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                Основание:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <div class="editable-field">
                                <span class="editable-field">
                                    <?= $model->agreementType ? $model->agreementType->name : ($model->basis_document_name == 'Счет' ? 'Счет' : '---') ?>
                                </span>
                                    №
                                    <span class="editable-field"><?= $model->basis_document_number ? $model->basis_document_number : '---'; ?></span>
                                    от
                                    <span class="editable-field"><?= $model->basis_document_date ? $basisDocumentDate : '---'; ?></span>
                                </div>
                                <div class="input-editable-field hide">
                                    <?php
                                    Pjax::begin([
                                        'id' => 'agreement-pjax-container',
                                        'enablePushState' => false,
                                        'linkSelector' => false,
                                    ]);

                                    echo Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'agreement',
                                        'data' => $agreementDropDownList,
                                        'hideSearch' => true,
                                        'pluginOptions' => [
                                            'allowClear' => true,
                                            'placeholder' => '',
                                        ],
                                    ]);

                                    Pjax::end();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                                Грузополучатель:
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <div class="editable-field">
                                    <?= $model->consignee ? $model->consignee->getTitle(true) : 'Он же'; ?>
                                </div>
                                <div class="input-editable-field hide" style="margin-bottom: 8px;">
                                    <?= Select2::widget([
                                        'model' => $model,
                                        'attribute' => 'consignee_id',
                                        'value' => '',
                                        'data' => ['add-modal-contractor' => '[ + Добавить грузополучателя ]'] + $model->consignorArray,
                                        'options' => [
                                            'value' => $model->consignee_id ?: '',
                                            'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                                        ],
                                        'pluginOptions' => [
                                            'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 20); }'),
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr class="contractor-address-block input-editable-field hide"
                    data-current="<?= $model->invoice->contractor->id; ?>"
                    style="<?= 'display: ' . ($showAddress ? 'table-row;' : 'none;'); ?>">
                    <td>
                        <div class="row">
                            <div class="col-sm-6 upd-info-label">
                            </div>
                            <div class="col-sm-6 upd-info-value">
                                <?= Html::activeRadioList($model, 'contractor_address', [
                                    Upd::CONTRACTOR_ADDRESS_LEGAL => 'Юр. адрес',
                                    Upd::CONTRACTOR_ADDRESS_ACTUAL => 'Физ. адрес',
                                ]); ?>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="color:#333; display:block; float:left; margin:5px 8px 0 0">
                                    Печать и подпись:
                                </div>
                                <label class="rounded-switch" for="add_stamp"
                                       style="margin: 4px 0 0;">
                                    <?= Html::checkbox('add_stamp', $model->add_stamp, [
                                        'id' => 'add_stamp',
                                        'class' => 'switch'
                                    ]) ?>
                                    <span class="sliderr no-gray yes-yellow round"></span>
                                </label>
                                <span class="tooltip-hover ico-question valign-middle"
                                      data-tooltip-content="#tooltip_add_stamp"
                                      style="vertical-align: top;">
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>

                <?php if ($hasService) : ?>
                    <tr class="input-editable-field hide">
                        <td>
                            <div style="margin-top: 10px;">
                                <span class="customer-characteristic">Отобразить данные по услугам в столбцах 4, 6, 11, 12:</span>
                                <span>
                                <?= Html::activeCheckbox($model, 'show_service_units', [
                                    'label' => false,
                                ]); ?>
                            </span>
                            </div>
                            <div style="clear: both; font-size: 10px; font-weight: normal; line-height: 15px;">
                                Обычно данные не должны быть отображены, но если ваш клиент просит, то отобразите.
                            </div>
                        </td>
                    </tr>
                <?php endif ?>
            </table>

            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <div style="margin-bottom: 5px;">
                            <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                'model' => $model,
                                'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                            ]); ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="tooltip_templates" style="display: none;">
        <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
            Добавить в УПД печать и подпись
            <br>
            при отправке по e-mail и
            <br>
            при скачивании в PDF
        </span>
    </div>
*/ ?>

<?php $this->registerJs('
    $("#upd-consignee_id").change(function () {
    var $contractorAddressBlock = $(".contractor-address-block");
    var $contractorID = $(this).val();
    if ($contractorID == "") {
        $contractorID = $contractorAddressBlock.data("current");
    }

    $.post("/contractor/is-different-address", {
            id: $contractorID
       }, function (data) {
            if (data.isDifferent == true) {
                $contractorAddressBlock.show();
            } else {
                $contractorAddressBlock.hide();
            }
            $contractorAddressBlock.find("#upd-contractor_address input[value=\"0\"]").prop("checked", true).click();
       });
    });
'); ?>

<?php
$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>
