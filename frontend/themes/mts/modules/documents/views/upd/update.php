<?php

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\status\ActStatus;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions\Contractor;
use frontend\rbac\permissions\document\Document;
use yii\bootstrap4\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$company = Yii::$app->user->identity->company;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$invoiceOrderCount = Order::find()
    ->andWhere(['in', 'invoice_id', ArrayHelper::getColumn($model->invoices, 'id')])
    ->count();
$hideAddBtn = $invoiceOrderCount === count($model->ownOrders);
$precision = $model->invoice->price_precision;

$canUpdate = $model->status_out_id != ActStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$totalQuantity = $model->getOwnOrders()->joinWith('product')->andWhere([
    'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
])->sum('order_upd.quantity');

$cancelUrl = Url::previous('lastPage');
?>

<div class="form">

    <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-act',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>

        <?= $this->render('viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'message' => $message,
            'dateFormatted' => $dateFormatted,
            'canUpdate' => $canUpdate,
        ]); ?>

        <!-- ORDERS TABLE -->
        <div class="wrap">

            <?= $this->render('viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'precision' => $precision,
            ]); ?>

            <div class="row align-flex-start justify-content-between mt-3">
                <div class="column">
                    <span id="plusbtn"
                          class="btn-add-line-table button-regular button-hover-content-red <?= $hideAddBtn ? 'hide' : '' ?>"
                          data-maxrows="<?= $invoiceOrderCount ?>"
                          data-url="<?= Url::to(['select-product', 'type' => $model->type, 'id' => $model->id])?>">
                        <svg class="svg-icon">
                            <use xlink:href="/images/svg-sprite/svgSprite.svg#add-icon"></use>
                        </svg>
                        <span>Добавить</span>
                    </span>
                </div>
                <div class="column">
                    <table class="total-txt">
                        <tr>
                            <td class="text-right">
                                <strong>Итого:</strong>
                            </td>
                            <td class="text-right">
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                Кол-во (масса нетто):
                            </td>
                            <td class="text-right">
                                <strong><?= $totalQuantity * 1 ?></strong>
                            </td>
                        </tr>
                        <?php if ($model->invoice->hasNds) : ?>
                            <tr>
                                <td class="text-right">
                                    Сумма без учёта НДС:
                                </td>
                                <td class="text-right">
                                    <strong>
                                        <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2) ?>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    Сумма НДС:
                                </td>
                                <td class="text-right">
                                    <strong>
                                        <?= TextHelper::invoiceMoneyFormat($model->totalNds, 2) ?>
                                    </strong>
                                </td>
                            </tr>
                        <?php else : ?>
                            <tr>
                                <td class="text-right">
                                    Без налога (НДС):
                                </td>
                                <td class="text-right">
                                    <strong>-</strong>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td class="text-right">
                                Сумма с учётом НДС:
                            </td>
                            <td class="text-right">
                                <strong>
                                    <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2) ?>
                                </strong>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="wrap wrap_btns check-condition visible mb-0">
            <div class="row align-items-center justify-content-between">
                <div class="column">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'button-width button-regular button-regular_red button-clr pl-4 pr-4',
                        'data-style' => 'expand-right',
                    ]); ?>
                </div>
                <div class="column">
                    <?= Html::a('Отменить', isset($cancellUrl) ? $cancellUrl : ['index', 'type' => $model->type], [
                        'class' => 'button-clr button-width button-regular button-hover-transparent pl-4 pr-4',
                    ]); ?>
                </div>
            </div>
        </div>

    <?= Html::endForm(); ?>
</div>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'title' => '<h4 id="agreement-modal-header">Добавить договор</h4>',
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();
?>

<div id="add-products-to-list" class="modal fade t-p-f modal_scroll_center mobile-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title">Выбрать позицию из списка</h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body">
                <div id="available-product-list"></div>
            </div>
        </div>
    </div>
</div>

<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div id="add-new" class="modal fade t-p-f modal_scroll_center mobile-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <h4 class="modal-title"></h4>
            <button type="button" class="modal-close close" data-dismiss="modal" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs('
var checkPlusbtn = function() {
    if ($("tbody.document-orders-rows tr").length < $("#plusbtn").data("maxrows")) {
        $("#plusbtn").removeClass("hide");
    } else {
        $("#plusbtn").addClass("hide");
    }
};
var checkRowNumbers = function() {
    $(".document-orders-rows").children("tr").each(function(i, row) {
        $(row).find("span.row-number").text(++i);
    });
};
var toggleFieldsToForm = function() {
    $(".input-editable-field").removeClass("hide");
    $(".editable-field").addClass("hide");
};
var checkServiceUnits = function() {
    if ($("#upd-show_service_units").is(":checked")) {
        $(".service_units_visible").removeClass("hide");
        $(".service_units_hidden").addClass("hide");
    } else {
        $(".service_units_visible").addClass("hide");
        $(".service_units_hidden").removeClass("hide");
    }
};
checkPlusbtn();
checkRowNumbers();

$(document).on("change", "#upd-show_service_units", function() {
    checkServiceUnits();
});
$(document).on("change", "#upd-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/documents/agreement/create',
        'contractor_id' => $model->invoice->contractor_id,
        'type' => $model->type,
        'returnTo' => 'upd',
        'container' => 'agreement-select-container',
    ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            refreshDatepicker();
        })
        $("#agreement-modal-container").modal("show");
        $("#upd-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#upd-agreement").val(window.AgreementValue).trigger("change");
    }
})

$(document).on("click", "tr .delete-row", function() {
    var row = $(this).closest("tr");
    var table = row.parent();
    row.remove();
    checkRowNumbers();
    checkPlusbtn();
});
$(document).on("click", "#plusbtn", function() {
    $.post($(this).data("url"), $("#edit-upd").serialize(), function(data) {
        $("#available-product-list").html(data);
        $("#add-products-to-list").modal("show");
    });
});
$(document).on("click", "#add-selected-button", function() {
    $.post($(this).data("url"), $("#select-invoice-product-form").serialize(), function(data) {
        console.log(data);
        if (data.rows.length) {
            $.each(data.rows, function(i, item) {
                $(".document-orders-rows").append(item);
            });
        }
        toggleFieldsToForm();
        checkPlusbtn();
        checkRowNumbers();
        $("#available-product-list").html("");
        $(".document-orders-rows input:not(.md-check, .md-radiobtn)").uniform();
    });
});
$(document).on("click", "#add-payment-doc", function() {
    var row = $("#payment-inputs-block .payment-row-template").clone();
    $(row).removeClass("payment-row-template").addClass("payment-row");
    $(row).find(".date-picker-field").removeClass("date-picker-field").addClass("date-picker");
    $(row).appendTo("#payment-inputs-block").show();
    refreshDatepicker();
});
$(document).on("click", ".del-payment-doc", function() {
    if ($("#payment-inputs-block .payment-row").length == 1) {
        $(this).closest(".payment-row").find("input[type=text]").val("");
    } else {
        $(this).closest(".payment-row").remove();
    }
    return false;
});
');
?>

