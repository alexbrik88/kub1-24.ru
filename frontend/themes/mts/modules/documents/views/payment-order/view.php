<?php

use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\document\PaymentOrder::INDEX)) : ?>
    <?= Html::a('Назад к списку', ['index'], ['class' => 'link mb-2']); ?>
<?php endif; ?>

<div class="wrap wrap_padding_small mb-2">
    <div class="page-in row">
        <div class="page-in-content column">
            <div class="page-border">
                <?= frontend\themes\mts\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                    'toggleButton' => [
                        'class' => 'button-list button-hover-transparent button-clr mb-3 mr-2',
                        'label' => $this->render('//svg-sprite', ['ico' => 'info']),
                        'title' => 'Последние действия',
                    ]
                ]); ?>

                <?php if (Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model])) : ?>
                    <?= Html::a($this->render('//svg-sprite', ['ico' => 'pencil']), [
                        'update',
                        'id' => $model->id,
                    ], [
                        'title' => 'Редактировать',
                        'class' => 'button-list button-hover-transparent button-clr mb-3 ml-1 ',
                    ]) ?>
                <?php endif; ?>

                <div class="doc-container">
                    <?= $this->render('_view', [
                        'model' => $model,
                        'company' => $company,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="page-in-sidebar column pl-3">
            <?= $this->render('view/status-block', [
                'model' => $model,
            ]); ?>
            <?= $this->render('view/main-info', [
                'model' => $model,
                'company' => $company,
            ]); ?>
        </div>
    </div>
</div>

<?= $this->render('view/action-buttons', [
    'model' => $model,
]); ?>
