<?php
/* @var $model common\models\Contractor */
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

?>

<div class="dopColumns">

    <?= $form->field($model, 'current_account', array_merge($textInputConfig, [
        'options' => [
            'class' => 'form-group required',
        ],
    ]))->textInput([
        'maxlength' => true,
    ]); ?>

    <?= $form->field($model, 'BIC', $textInputConfig)->widget(\common\components\widgets\BikTypeahead::classname(), [
        'remoteUrl' => Url::to(['/dictionary/bik']),
        'related' => [
            '#' . Html::getInputId($model, 'bank_name') => 'name',
            '#' . Html::getInputId($model, 'bank_city') => 'city',
            '#' . Html::getInputId($model, 'corresp_account') => 'ks',
        ],
    ])->textInput(['placeHolder' => '']); ?>

    <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
        'maxlength' => true,
        'disabled' => true,
    ]); ?>
    <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
        'maxlength' => true,
        'disabled' => true,
    ]); ?>
    <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
        'maxlength' => true,
        'disabled' => true,
    ]); ?>
</div>