<?= $form->field($model, 'physical_lastname', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group row required',
    ],
]))->label('Фамилия:')->textInput([
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
    ],
]); ?>
<?= $form->field($model, 'physical_firstname', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group row required',
    ],
]))->label('Имя:')->textInput([
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
    ],
]); ?>
<?= $form->field($model, 'physical_patronymic', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group row required',
    ],
]))->label('Отчество:')->textInput([
    'maxlength' => true,
    'readonly' => (boolean)$model->physical_no_patronymic,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
    ],
]); ?>
<?= $form->field($model, 'physical_no_patronymic', array_merge($checkboxConfig, [
    'options' => [
        'class' => 'form-group row',
    ],
]))->label('Нет отчества:')->checkbox([], false); ?>
