<?php

use common\models\company\CompanyType;
use common\models\dictionary\bik\BikDictionary;
use yii\helpers\ArrayHelper;

/* @var $modelArray common\models\document\PaymentOrder[] */
/* @var $company common\models\Company */

$date = date('d.m.Y');
$time = date('H:i:s');
$model = reset($modelArray);

ArrayHelper::multisort($modelArray, function($item) { return $item->document_date ? strtotime($item->document_date) : 0;}, SORT_ASC);
$dateTill = ($modelArray && ($dateTime = DateTime::createFromFormat('Y-m-d', end($modelArray)->document_date))) ? $dateTime->format('d.m.Y') : '';
$dateFrom = ($modelArray && ($dateTime = DateTime::createFromFormat('Y-m-d', reset($modelArray)->document_date))) ? $dateTime->format('d.m.Y') : '';

$content = "1CClientBankExchange\r\n";
$content .= "ВерсияФормата=1.02\r\n";
$content .= "Кодировка=Windows\r\n";
$content .= "Отправитель=Бухгалтерия предприятия, редакция 3.0\r\n";
$content .= "Получатель=\r\n";
$content .= "ДатаСоздания={$date}\r\n";
$content .= "ВремяСоздания={$time}\r\n";
$content .= "ДатаНачала={$dateFrom}\r\n";
$content .= "ДатаКонца={$dateTill}\r\n";
$content .= (($model = reset($modelArray)) ? "РасчСчет={$model->company_rs}" : "") . "\r\n";
$content .= "Документ=Платежное поручение\r\n";
$content .= "Документ=Платежное требование\r\n";

foreach ($modelArray as $model) {
    $docDate = ($dateTime = DateTime::createFromFormat('Y-m-d', $model->document_date)) ? $dateTime->format('d.m.Y') : '';

    $content .= "СекцияДокумент=Платежное поручение\r\n";
    $content .= "Номер={$model->document_number}\r\n";
    $content .= "Дата={$docDate}\r\n";
    $content .= "Сумма={$model->amountFormated}\r\n";
    $content .= "ПлательщикСчет={$model->company_rs}\r\n";
    $content .= "Плательщик=ИНН {$model->company_inn} {$model->company_name}\r\n";
    $content .= "ПлательщикИНН={$model->company_inn}\r\n";
    $content .= "Плательщик1={$model->company_name}\r\n";
    $content .= "ПлательщикРасчСчет={$model->company_rs}\r\n";
    $content .= "ПлательщикБанк1={$model->company_bank_name}\r\n";
    $content .= "ПлательщикБанк2={$model->getCompanyBankCity()}\r\n";
    $content .= "ПлательщикБИК={$model->company_bik}\r\n";
    $content .= "ПлательщикКорсчет={$model->company_ks}\r\n";
    $content .= "ПолучательСчет={$model->contractor_current_account}\r\n";
    $content .= "Получатель=ИНН {$model->contractor_inn} {$model->contractor_name}\r\n";
    $content .= "ПолучательИНН={$model->contractor_inn}\r\n";
    $content .= "Получатель1={$model->contractor_name}\r\n";
    $content .= "ПолучательРасчСчет={$model->contractor_current_account}\r\n";
    $content .= "ПолучательБанк1={$model->contractor_bank_name}\r\n";
    $content .= "ПолучательБанк2={$model->getContractorBankCity()}\r\n";
    $content .= "ПолучательБИК={$model->contractor_bik}\r\n";
    $content .= "ПолучательКорсчет={$model->contractor_corresponding_account}\r\n";
    $content .= "ВидПлатежа=\r\n";
    $content .= "ВидОплаты={$model->operationTypeCode}\r\n";
    $content .= "СтатусСоставителя={$model->taxpayersStatusCode}\r\n";
    $content .= "ПлательщикКПП={$model->company_kpp}\r\n";
    $content .= "ПолучательКПП={$model->contractor_kpp}\r\n";

    if ($model->presence_status_budget_payment) {
    $content .= "ПоказательКБК={$model->kbk}\r\n";
    $content .= "ОКАТО={$model->oktmo_code}\r\n";
    $content .= "ПоказательОснования={$model->reasonCode}\r\n";
    $content .= "ПоказательПериода={$model->tax_period_code}\r\n";
    $content .= "ПоказательНомера={$model->document_number_budget_payment}\r\n";
    $content .= "ПоказательДаты={$model->dateBudgetPayment}\r\n";
    $content .= "ПоказательТипа={$model->typeCode}\r\n";
    }

    $content .= "Очередность={$model->ranking_of_payment}\r\n";
    $content .= "НазначениеПлатежа={$model->purpose_of_payment}\r\n";
    $content .= "Код={$model->uin_code}\r\n";
    $content .= "КонецДокумента\r\n";
}

$content .= "КонецФайла";

echo mb_convert_encoding($content, 'windows-1251');
