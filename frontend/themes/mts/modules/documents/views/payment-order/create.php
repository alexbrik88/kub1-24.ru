<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */
/* @var $company common\models\Company */

$this->title = 'Создать платёжное поручение';
$this->params['breadcrumbs'][] = ['label' => 'Payment Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>

<div class="stop-zone-for-fixed-elems">
    <div class="page-head d-flex flex-wrap align-items-center">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'company' => $company,
    ]) ?>
</div>

<?php if (Yii::$app->session->remove('show_example_popup') || Yii::$app->request->get('show_example')) {
    // echo $this->render('partial/_first_show_modal');
}
?>

<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">
            </div>
        </div>
    </div>
</div>
