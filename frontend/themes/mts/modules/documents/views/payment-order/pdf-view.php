<?php

use common\components\date\DateHelper;
use frontend\themes\mts\widgets\ConfirmModalWidget;
use php_rutils\RUtils;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\PaymentOrder */

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-act out-document payment-order';
?>

<div class="page-content-in p-center pad-pdf-p pad-top-paym-order">
    <table class="table no-border m-t" style="margin-top: 0">
        <tr>
            <td style="border-bottom: 1px solid black; width:18%"></td>
            <td style="width: 10%"></td>
            <td style="border-bottom: 1px solid black; width: 18%"></td>
            <td style="width: 40%"></td>
            <td style="border: 1px solid black; width: 12%" class="text-center">0401060</td>
        </tr>
        <tr>
            <td class="text-center ver-top m-l">Поступ. в банк плат.</td>
            <td></td>
            <td class="text-center ver-top m-l">Списано со сч. плат.</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="table no-border" style="margin:0 0 4px 0">
        <tr>
            <td class="toc-table" style="font-size: 11pt;">ПЛАТЕЖНОЕ ПОРУЧЕНИЕ №<?= $model->document_number; ?></td>
            <td></td>
            <td class="text-center font-bold" style="width: 20%; border-bottom: 1px solid black;">
                <?= RUtils::dt()->ruStrFTime([
                    'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                    'date' => $model->document_date,
                ]); ?>
            </td>
            <td width="3%"></td>
            <td class="text-center" style="width: 20%;border-bottom: 1px solid black;"></td>
            <td></td>
            <td class="text-center" style="width: 3.5%; border: 1px solid black"><?= (!empty($model->taxpayers_status_id)) ? $model->taxpayersStatus->code : ''; ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td class="text-center m-l ver-top">Дата</td>
            <td></td>
            <td class="text-center m-l ver-top">Вид платежа</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table class="no-border" style="margin-top: 0">
        <tr>
            <td class="ver-top" style="width:10%; margin: 0; padding: 0 0 28px 0;border-right: 1px solid black">Сумма прописью</td>
            <td class="ver-top" style="margin: 0; padding: 0 0 0 5px"><?= $model->sum_in_words; ?></td>
        </tr>
    </table>
    <table class="table-no-margin">
        <tr>
            <td width="28.5%" class="no-border-left">ИНН <?= $model->company_inn ?></td>
            <td width="28.5%" >КПП <?= $model->company_kpp ?></td>
            <td width="9%" rowspan="2" class="ver-top">Сумма</td>
            <td rowspan="2" colspan="3" class="ver-top no-border-right"><?= \common\components\TextHelper::invoiceMoneyFormat($model->sum, 2, '-'); ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom:none"><?= $model->company_name ?><br/></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom:none;border-top: none;"><br/></td>
            <td rowspan="2" class="ver-top">Сч. №</td>
            <td rowspan="2" colspan="3" class="ver-top no-border-right" style="border-bottom: none"><?= $model->company_rs; ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-top: none">Плательщик</td>
        </tr>

        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom: none"><?= $model->company_bank_name; ?></td>
            <td class="ver-top">БИК</td>
            <td class="ver-top no-border-right" colspan="3" style="border-top: none; border-bottom: none"><?= $model->company_bik ? : $model->company_bik; ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-top: 0"><br/>Банк плательщика</td>
            <td class="ver-top">Сч. №</td>
            <td class="ver-top no-border-right" colspan="3" style="border-top: none"><?= $model->company_ks; ?></td>
        </tr>

        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom: none"><?= $model->contractor_bank_name; ?></td>
            <td class="ver-top">БИК</td>
            <td class="ver-top no-border-right" colspan="3" style="border-top: none; border-bottom: none"><?= $model->contractor_bik; ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-top: 0"><br/>Банк получателя</td>
            <td class="ver-top">Сч. №</td>
            <td class="ver-top no-border-right" colspan="3" style="border-top: none"><?= $model->contractor_corresponding_account; ?></td>
        </tr>

        <tr>
            <td class="no-border-left">ИНН <?= $model->contractor_inn; ?></td>
            <td>КПП <?= $model->contractor_kpp; ?></td>
            <td rowspan="2" class="ver-top">Сч. №</td>
            <td rowspan="2" colspan="3" class="ver-top no-border-right"><?= $model->contractor_current_account; ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom:none"><?= $model->contractor_name; ?><br/></td>
        </tr>

        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom:none;border-top: none"></td>
            <td>Вид оп.</td>
            <td width="10%" style="border-bottom:none;border-top: none"><?= (!empty($model->operation_type_id)) ? $model->operationType->code : ''; ?></td>
            <td width="73px">Срок плат.</td>
            <td class="no-border-right" style="border-bottom:none;border-top: none"><?= DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-bottom:none;border-top: none"></td>
            <td>Наз. пл.</td>
            <td style="border-bottom:none;border-top: none"></td>
            <td>Очер. плат.</td>
            <td class="no-border-right" style="border-bottom:none;border-top: none"><?= $model->ranking_of_payment; ?></td>
        </tr>
        <tr>
            <td class="no-border-left" colspan="2" style="border-top: none">Получатель</td>
            <td>Код</td>
            <td style="border-top: none"><?= $model->uin_code; ?></td>
            <td>Рез. поле</td>
            <td class="no-border-right" style="border-top: none"></td>
        </tr>
    </table>
    <table class="table-no-margin no-border-top" >
        <tr>
            <td class="no-border-left no-border-top" width="24.5%"><br/><?= $model->kbk; ?></td>
            <td class="no-border-top" width="17%"><br/><?= $model->oktmo_code; ?></td>
            <td class="no-border-top" width="5%"><br/><?= (!empty($model->payment_details_id)) ? $model->paymentDetails->code : ''; ?></td>
            <td class="no-border-top" width="16%"><br/><?= $model->tax_period_code; ?></td>
            <td class="no-border-top" width="17%"><br/><?= $model->document_number_budget_payment ?></td>
            <td class="no-border-top" width="12%"><br/>
                <?php if ($model->document_date_budget_payment !== null) {
                    echo RUtils::dt()->ruStrFTime([
                        'format' => \common\components\date\DateHelper::FORMAT_USER_DATE,
                        'date' => $model->document_date_budget_payment,
                    ]);
                } elseif ($model->presence_status_budget_payment) {
                    echo 0;
                } ?>
            </td>
            <td class="no-border-right no-border-top" width=""><br/><?= (!empty($model->payment_type_id)) ? $model->paymentType->code : ''; ?></td>
        </tr>
    </table>
    <div class="txt-paym-order">
        <?= $model->purpose_of_payment; ?>
    </div>
    <p class="txt-paym-order-sm">Назначение платежа</p>
    <div class="line-hr-sm"></div>

    <table class="table-no-margin no-border">
        <tr>
            <td width="33%" class="text-center"></td>
            <td width="33%" class="text-center txt-font-8">Подписи</td>
            <td width="33%" class="text-center txt-font-8">Отметки банка</td>
        </tr>
    </table>

    <table class="table-no-margin no-border" style="margin-top: 26px">
        <tr>
            <td width="33%" style="margin: 0; padding: 0" class="text-center ver-bottom">М.П.</td>
            <td width="33%" style="border-bottom: 1px solid black"></td>
            <td width="33%"></td>
        </tr>
    </table>
    <table class="table-no-margin no-border" style="margin-top: 53px">
        <tr>
            <td width="33.5%"></td>
            <td width="33%" style="border-bottom: 1px solid black"></td>
            <td width="33%"></td>
        </tr>
    </table>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak />
<?php endif; ?>
