<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutOrderDocumentForm */
/* @var $form yii\bootstrap4\ActiveForm */

$allowedProduct = $model->getPriceList()->getPriceListOrders()->indexBy('product_id')->all();
$totalPrice = 0;
$isArticle = false;
?>

<table class="table table-bordered table-hover" style="margin-bottom: 20px;">
    <tr>
        <?php if ($isArticle) : ?>
            <th>Артикул</th>
        <?php endif ?>
        <th>Наименование</th>
        <th style="width: 120px; text-align: center;">Цена (руб)</th>
        <th style="width: 120px; text-align: center;">Количество</th>
    </tr>
    <?php foreach ($model->product as $pid => $count) : ?>
        <?php if ($count > 0 && isset($allowedProduct[$pid])) : ?>
            <?php
            $product = \common\models\product\Product::findOne($pid);
            $price = isset($model->price[$pid]) ? $model->price[$pid] * 100 : $product->price_for_sell_with_nds;
            $totalPrice += $price * $count;
            ?>
            <tr>
                <?php if ($isArticle) : ?>
                    <td><?= $product->article ?></td>
                <?php endif ?>
                <td>
                    <?= $product->title ?>
                </td>
                <td style="text-align: right;">
                    <?= number_format(($price/100), 2, ',', '&nbsp;') ?>
                </td>
                <td style="text-align: center;">
                    <?= $count ?>
                </td>
            </tr>
        <?php endif; ?>
    <?php endforeach; ?>
    <tr style="font-weight: bold;">
        <td colspan="<?= $isArticle ? 2 : 1; ?>" style="text-align: right;">Итого</td>
        <td style="text-align: right;">
            <?= number_format(($totalPrice/100), 2, ',', '&nbsp;') ?>
        </td>
        <td style="text-align: center;">
            <?= array_sum($model->product) ?>
        </td>
    </tr>
</table>

<?= $this->render('_orders_form', [
    'form' => $form,
    'model' => $model,
]) ?>
