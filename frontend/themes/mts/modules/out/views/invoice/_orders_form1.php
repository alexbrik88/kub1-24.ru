<?php

use frontend\modules\out\models\OutInvoiceForm;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\out\models\OutInvoiceForm */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<?php foreach ((array) $model->getOrderList() as $key => $order) : ?>
    <?php foreach ($order->getAttributes() as $name => $value) : ?>
        <?= Html::activeHiddenInput($model, "order_list[{$key}][{$name}]", ['value' => $value]); ?>
    <?php endforeach ?>
<?php endforeach ?>
