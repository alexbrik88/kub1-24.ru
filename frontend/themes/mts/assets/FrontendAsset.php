<?php

namespace frontend\themes\mts\assets;

use yii\web\AssetBundle;

/**
 * DocSendAsset
 */
class FrontendAsset extends AssetBundle
{
    /**
     * @var array
     */
    public $css = [
    ];
    /**
     * @var array
     */
    public $js = [
        'scripts/cash-flow-form.js',
        'scripts/vidimus.js',
        'scripts/document-file-scan.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
