<?php

namespace frontend\themes\mts\assets;

use yii\web\AssetBundle;

/**
 * DocSendAsset
 */
class UniformAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/mts/assets/plugins/uniform';

    /**
     * @var array
     */
    public $css = [
        'css/custom.uniform.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'custom.uniform.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
