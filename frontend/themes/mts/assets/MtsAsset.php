<?php

namespace frontend\themes\mts\assets;

use yii\web\AssetBundle;

/**
 * MtsAsset
 */
class MtsAsset extends AssetBundle
{
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapThemeAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'css' => [],
            'js' => []
        ];
        \Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'css' => [],
            'js' => []
        ];
    }

    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/mts/assets/web';

    /**
     * @var array
     */
    public $css = [
        'css/vendorCss.css',
        'css/style.css',
        'css/vidimus.css',
        'css/document-file-scan.css',
        'custom.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'js/vendor.min.js',
        'js/main.js',
        'js/custom.js',
        'js/export.js',
        'custom2.js',
        'expenditure-item-widget.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'frontend\themes\mts\assets\MtsCommonAsset',
        'frontend\themes\mts\assets\FrontendAsset'
    ];
}
