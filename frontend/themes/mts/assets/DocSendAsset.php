<?php

namespace frontend\themes\mts\assets;

use yii\web\AssetBundle;
/**
 * DocSendAsset
 */
class DocSendAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/themes/mts/assets/web';

    /**
     * @var array
     */
    public $css = [
    ];
    /**
     * @var array
     */
    public $js = [
        'js/send-doc.js'
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'common\assets\SimpleAjaxUploaderAsset'
    ];
}
