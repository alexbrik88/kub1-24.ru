$.pjax.defaults.timeout = 20000;

$(document).on('keypress', 'input.date-picker', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
        var v = $(this).val().split('.');
        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            var picker = $(this).data('datepicker');
            if (picker) {
                var curDate = picker.selectedDates[0];
                if (!curDate || curDate.getTime() != date.getTime()) {
                    picker.selectDate(date);
                }
            }
        }
    }
});

// MXFI functions
function empty(mixed_var) {
    return (
        mixed_var === '' ||
        mixed_var === 0 ||
        mixed_var === '0' ||
        mixed_var === null ||
        mixed_var === false ||
        mixed_var === 'false' ||
        mixed_var === undefined
    );
}

function initDatepicker() {

    $('.date-picker').each(function(i, dp) {

        if ($(dp).hasClass('hasDatepicker'))
            return;

        var v = $(dp).val().split('.');

        $(dp).addClass('hasDatepicker').inputmask({"mask": "9{2}.9{2}.9{4}"});

        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            if (date.getTime() === date.getTime()) {
                $(dp).data('datepicker').selectDate(date);
            }
        }

    });
}

function tooltipstering() {
    $('*[title]:not(.tooltipstered)').filter(':not([data-toggle="tooltip"])').tooltipster({
        theme: 'tooltipster-mts'
    });
}

function refreshDatepicker() {

    $('.date-picker').each(function(i, dp) {

        if ($(dp).hasClass('hasDatepicker'))
            return;

        var v = $(dp).val().split('.');

        $(dp).datepicker({
            keyboardNav: false,
            showOtherMonths: false,
            autoClose: true,
            // inline: true,
            prevHtml:   '<svg class="svg-icon">'+
                '<use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>'+
                '</svg>',
            nextHtml:   '<svg class="svg-icon">'+
                '<use xlink:href="/images/svg-sprite/svgSprite.svg#shevron"></use>'+
                '</svg>',
            onShow: function(dp, animationCompleted) {
                if ( !animationCompleted ) {
                    dp.el.parentElement.appendChild(dp.$datepicker[0]);
                    dp.el.parentElement.querySelector('.datepicker').classList.add('visible');
                    dp.el.parentElement.querySelector('.svg-icon').classList.add('red');
                }
            },
            onHide: function(dp, animationCompleted) {
                if ( !animationCompleted ) {
                    dp.el.parentElement.appendChild(dp.$datepicker[0]);
                    dp.el.parentElement.querySelector('.datepicker').classList.remove('visible');
                    dp.el.parentElement.querySelector('.svg-icon').classList.remove('red');
                }
            },
            onSelect: function(formattedDate, date, inst) {
                $(inst.el).trigger('change');
            }
        });

        $(dp).addClass('hasDatepicker').inputmask({"mask": "9{2}.9{2}.9{4}"});

        if (v.length == 3) {
            var date = new Date(v[2], v[1] - 1, v[0]);
            if (date.getTime() === date.getTime()) {
                $(dp).data('datepicker').selectDate(date);
            }
        }

    });
}

function refreshUniform() {

    if (typeof $.fn.uniform === 'undefined') {
        return;
    }

    $("input:checkbox:not(.md-check)").uniform('refresh');
    $("input:radio:not(.md-radio)").uniform('refresh');
}


$(document).ready(function() {
    tooltipstering();
    initDatepicker();
});

$(document).on('click', '.date-picker-icon', function(evt) {
    evt.stopPropagation();
    var $input = $(this).closest('.date-picker-wrap').find('input');
    if ($input.length && !$input.prop('disabled')) {
        $input.datepicker().data('datepicker').show();
    }
});

$.fn.serializeFormAsObject = function() {
    var data = {};

    function buildInputObject(arr, val) {
        if (arr.length < 1)
            return val;
        var objkey = arr[0];
        if (objkey.slice(-1) == "]") {
            objkey = objkey.slice(0,-1);
        }
        var result = {};
        if (arr.length == 1){
            result[objkey] = val;
        } else {
            arr.shift();
            var nestedVal = buildInputObject(arr,val);
            result[objkey] = nestedVal;
        }
        return result;
    }

    $.each(this.serializeArray(), function() {
        var val = this.value;
        var c = this.name.split("[");
        var a = buildInputObject(c, val);
        $.extend(true, data, a);
    });

    return data;
};

function createSimpleSelect2(id)
{
    if (!$("#" + id).length)
        return false;

    // Create Select2 in banking modal
    var select2_banking_modal = {
        "allowClear":false,
        "placeholder":"",
        "language":{},
        "theme":"krajee-bs4",
        "width":"100%",
        "minimumResultsForSearch":Infinity
    };

    var s2options_xxx_banking_modal = {
        "themeCss":".select2-container--krajee-bs4",
        "sizeCss":"",
        "doReset":true,
        "doToggle":false,
        "doOrder":false
    };

    if (jQuery("#" + id).data("select2")) { jQuery("#" + id).select2("destroy"); }
    jQuery.when(jQuery("#" + id).select2(select2_banking_modal)).done(initS2Loading(id,"s2options_xxx_banking_modal"));
}

$(document).ajaxComplete(function(event, request, settings) {
    //refreshUniform();
    //refreshDatepicker();
});

window._offsetMCSLeft = 0;

$('.products-scroll-table').mCustomScrollbar({
    horizontalScroll: true,
    alwaysShowScrollbar: 0,
    axis:"x",
    scrollInertia: 300,
    advanced:{
        autoExpandHorizontalScroll: true,
        updateOnContentResize: true,
        updateOnImageLoad: false
    },
    mouseWheel:{ enable: false },
    callbacks: {
        whileScrolling:function(){
            window._offsetMCSLeft = this.mcs.left;
        }
    }
});

/**
 * Fix scrolling when a modal is opened
 */
window._bodyScrollTop;
$(document).on("show.bs.modal", function() {
    window._bodyScrollTop = window.pageYOffset || document.documentElement.scrollTop;
});
$(document).on("shown.bs.modal", function() {
    if (window._bodyScrollTop) {
        window.scrollTo({
            top: window._bodyScrollTop,
        });
    }
});
$(document).on("hidden.bs.modal", function() {
    if (window._bodyScrollTop) {
        window.scrollTo({
            top: window._bodyScrollTop,
        });
    }
});

/**
 * Refresh Uniform after replacing HTML content loaded from the server
 */
$(document).on('pjax:complete', function() {
    $("input:checkbox:not(.md-check), input:radio:not(.md-radio)", this).uniform('refresh');
    $(".date-picker", this).datepicker(mtsDatepickerConfig);
    tooltipstering();
});

/**
 * Prevent default redirection behavior
 */
$(document).on('pjax:timeout, pjax:error', function(event) {
    event.preventDefault();
    console.log(event.type);
});

$(document).on('click', '.form-errors-fixed > span', function () {
    $(this).parent().children('.alert').hide();
});
$(document).on('click', '[data-dismiss-single="modal"]', function(e) {
    e.preventDefault();
    $(this).closest(".modal").modal("hide");
});

$(document).on('click', 'a.document-many-send, .modal-many-delete, .modal-many-create-act, .modal-many-charge, ' +
    '.create-one-act, .create-one-upd, .create-one-invoice-facture, .modal-many-change-responsible, ' +
    '.modal-many-change-status, .modal-not-need-document-submit, .modal-move-files-to, .modal-many-restore, ' +
    'a.document-many-send-with-docs', function () {
    if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
    $('#ajax-loading').show();
    var $this = $(this);
    $('#many-send-error .form-body .row').remove();
    if (!$this.hasClass('clicked')) {
        if ($('.joint-operation-checkbox:checked').length > 0) {
            $this.addClass('clicked');
            $.post($this.data('url'), $('.joint-operation-checkbox, .modal-document-date, #contractor-responsible_employee,' +
                '#contractor-status input').serialize(), function (data) {
                $('#ajax-loading').hide();
                if (data.notSend !== undefined) {
                    $('.joint-operation-checkbox:checked').click();
                    $('#many-send-error .form-body').append('<div class="row">' + data.notSend + '</div>');
                    $('#many-send-error').modal();
                    $this.removeClass('clicked');
                }
                if (data.message !== undefined) {
                    if ($('.alert-success').length > 0) {
                        $('.alert-success').remove();
                    }
                    $('.page-content').prepend('<div id="w2-success-0" class="alert-success alert fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        data.message + '</div>');
                }
            });
        }
    }
});

$(document).on('tcw.change', '.mCustomScrollbar table', function (e) {
    $(this).closest('.custom-scroll-table').mCustomScrollbar("update");
});

/**
 * If `dropdoun-menu` clipped by container with `overflow: hidden` add css class `clipped_by_container` to `dropdown` element
 */
$('.dropdown.clipped_by_container').on('show.bs.dropdown', function () {
    var dropdown = $(this);
    $('body').append(dropdown.css({
        'position':'absolute',
        'left':dropdown.offset().left,
        'top':dropdown.offset().top
    }).detach());
});
$('.dropdown.clipped_by_container').on('hidden.bs.dropdown', function () {
    var dropdown = $(this);
    var parentSelector = dropdown.data('parent');
    $(parentSelector).append(dropdown.css({
        'position':'static',
        'left':'auto',
        'top':'auto'
    }).detach());
});

$(document).on('change', 'select[data-tax-kbk]', function () {
    var kbk = $(this).data('tax-kbk')[this.value];
    if (kbk) {
        $('#cashbankflowsform-kbk, #cashbankflows-kbk', this.form).val(kbk);
    }
});