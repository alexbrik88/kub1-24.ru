<?php

namespace frontend\controllers;

use common\components\getResponse\GetResponseApi;
use common\components\getResponse\GetResponseContact;
use Yii;
use common\models\SentEmailStart;
use common\models\SentEmailType;
use console\components\sender\Findirector;
use frontend\models\FinanceRegistrationForm;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * FinanceController.
 */
class FinanceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'only' => [
                    'registration',
                ],
                'cors' => [
                    'Origin' => \Yii::$app->params['corsFilter'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['X-PJAX', 'X-PJAX-Container'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, [
            'registration',
        ])) {
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionRegistration()
    {
        $this->layout = 'registration';

        $postData = Yii::$app->request->post();
        $model = new FinanceRegistrationForm();
        $model->scenario = isset($postData[$model->formName()]['reCaptcha']) ?
            FinanceRegistrationForm::SCENARIO_CAPTCHA :
            FinanceRegistrationForm::SCENARIO_DEFAULT;

        if (\Yii::$app->request->post('ajax')) {
            $model->load($postData);
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        $useReCaptcha = Yii::$app->params['useReCaptcha'];
        if ($model->load($postData) && $model->validate()) {
            if ($useReCaptcha && $model->scenario == FinanceRegistrationForm::SCENARIO_DEFAULT) {
                $model->scenario = FinanceRegistrationForm::SCENARIO_CAPTCHA;

                return $this->render('registration_captcha', ['model' => $model]);
            }

            if ($model->save(false)) {
                $user = $model->getUser();

                if (Yii::$app->user->login($user)) {

                    // first letter sent from getResponse!
                    //$code = $user->generatePIN(4);
                    //$subject = 'Добро пожаловать в КУБ24.ФинДиректор';
                    //$mailer = clone \Yii::$app->mailer;
                    //$mailer->htmlLayout = 'layouts/html2';
                    //$sent = $mailer->compose([
                    //    'html' => 'system/new-findirector-registration/html',
                    //    'text' => 'system/new-findirector-registration/text',
                    //], [
                    //    'code' => $code,
                    //    'login' => $user->email,
                    //    'password' => $model->password,
                    //    'subject' => $subject,
                    //])
                    //    ->setFrom([\Yii::$app->params['emailList']['info'] => Findirector::$from])
                    //    ->setTo($user->email)
                    //    ->setSubject($subject)
                    //    ->send();
                    //
                    //if ($sent) {
                    //    Yii::$app->session->set('email_confirm_code_sent', true);
                    //    $user->updateAttributes([
                    //        'email_confirm_key' => $code,
                    //    ]);
                    //}

                    SentEmailStart::start(SentEmailType::FINDIRECTOR, $user->id);

                    // Save contact into GetResponse service
                    $gr = new GetResponseApi();
                    $gr->createGetResponseContact($user, $model);

                    return $this->redirect(['/analytics/options/start']);
                }
            }
        }

        if ($useReCaptcha && $model->scenario == FinanceRegistrationForm::SCENARIO_CAPTCHA) {
            return $this->render('registration_captcha', ['model' => $model]);
        }

        if (YII_ENV_DEV) {
            return $this->render('registration', [
                'model' => $model,
            ]);
        } else if (!Yii::$app->request->headers->has('origin')) {
            return $this->goHome();
        }
    }
}
