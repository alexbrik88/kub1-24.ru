<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.06.2018
 * Time: 15:53
 */

namespace frontend\controllers;


use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\service\PaymentType;
use frontend\components\FrontendController;
use frontend\models\PaymentInvoiceForm;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\rbac\permissions;
use Yii;

/**
 * Class InvoicePaymentController
 * @package frontend\controllers
 */
class InvoicePaymentController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['online-payment'],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws \Throwable
     */
    public function actionOnlinePayment()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($company->strict_mode) {
            return $this->strictMode();
        }
        $model = new PaymentInvoiceForm($company);
        if ($model->load(Yii::$app->request->post())) {
            $model->paymentTypeId = PaymentType::TYPE_ONLINE;
            if ($model->validate() && $model->makePayment()) {
                if ($fdf = $model->pdfContent) {
                    $uid = uniqid();
                    $session = Yii::$app->session;
                    $session->set('paymentDocument.uid', $uid);
                    $session->set("paymentDocument.{$uid}", $model->pdfContent);
                }
                $paymentForm = new OnlinePaymentForm([
                    'scenario' => OnlinePaymentForm::SCENARIO_SEND,
                    'company' => $model->company,
                    'payment' => $model->payment,
                    'user' => Yii::$app->user->identity,
                ]);

                return $this->renderPartial('online-payment', [
                    'paymentForm' => $paymentForm,
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при создании платежа. Попробуйте ещё раз.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['/site/index']);
    }
}
