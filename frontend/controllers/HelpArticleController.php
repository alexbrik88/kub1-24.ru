<?php

namespace frontend\controllers;

use common\components\filters\AccessControl;
use common\models\helpArticle\HelpArticle;
use common\models\helpArticle\HelpArticleSearch;
use frontend\components\FrontendController;
use frontend\rbac\permissions;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * HelpArticleController implements the CRUD actions for HelpArticle model.
 */
class HelpArticleController extends FrontendController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\HelpArticle::INDEX],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => [permissions\HelpArticle::INDEX],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all HelpArticle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HelpArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'articles' => HelpArticle::find()->orderBy('sequence')->all(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HelpArticle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'previousArticle' => HelpArticle::find()->where(['sequence' => $model->sequence - 1])->one(),
            'nextArticle' => HelpArticle::find()->where(['sequence' => $model->sequence + 1])->one(),
        ]);
    }

    /**
     * Finds the HelpArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return HelpArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HelpArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
