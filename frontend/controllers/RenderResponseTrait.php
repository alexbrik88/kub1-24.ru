<?php

namespace frontend\controllers;

use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use yii\web\Response;

trait RenderResponseTrait
{
    use WebTrait;

    /**
     * @param string $view
     * @param array $params
     * @return Response
     * @throws
     */
    protected function renderResponse(string $view, array $params = []): Response
    {
        if ($this->request->isAjax) {
            $this->response->content = $this->getController()->renderAjax($view, $params);
        } else {
            $this->response->content = $this->getController()->render($view, $params);
        }

        return $this->response;
    }

    /**
     * @return Controller
     * @throws InvalidConfigException
     */
    private function getController(): Controller
    {
        if ($this instanceof Controller) {
            return $this;
        }

        if ($this instanceof Action) {
            return $this->controller;
        }

        throw new InvalidConfigException();
    }
}
