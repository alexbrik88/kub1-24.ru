<?php

namespace frontend\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\components\getResponse\GetResponseApi;
use common\components\ImageHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\MenuItem;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\Discount;
use common\models\DiscountType;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeClick;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\Inquirer;
use common\models\news\News;
use common\models\news\NewsReaction;
use common\models\notification\Notification;
use common\models\PhoneFeedback;
use common\models\product\Product;
use common\models\product\ProductCategoryField;
use common\models\product\ProductSearch;
use common\models\service\SubscribeHelper;
use common\models\service\ServiceModule;
use common\models\ServiceMoreStock;
use frontend\components\EmployeeRating;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\FundingFlowForm;
use frontend\models\LoginForm;
use frontend\models\NotificationSearch;
use frontend\models\PasswordResetRequestForm;
use frontend\models\RegistrationForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\log\LogSearch;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboardContext;
use frontend\modules\analytics\models\financeDashboard\OutFinanceDashboard;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\cash\models\WidgetConfigAccountBalance as WidgetConfig;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'unpaid', 'get-modal-registration', 'change-store-settings',
                            'phone-feedback', 'cancel-modal-show', 'check-vk-post', 'employee-click', 'menu-item',
                            'change-theme', 'only-new-design', 'toggle-news-reaction', 'view-news'],
                        'allow' => true,
                        'roles' => [permissions\Service::HOME_PAGE],
                    ],
                    [
                        'actions' => ['activities'],
                        'allow' => true,
                        'roles' => [permissions\Service::HOME_ACTIVITIES],
                    ],
                    [
                        'actions' => ['notifications'],
                        'allow' => true,
                        'roles' => [permissions\Service::HOME_TAX_CALENDAR],
                    ],
                    [
                        'actions' => ['add-funding-flow'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\Service::HOME_FINANCES)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => ['add-expenditure-item', 'add-income-item'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => ['tax-robot-statistic-range', 'statistic-range', 'statistic-range-months', 'range-button-title', 'sidebar-status'],
                        'allow' => true,
                        'roles' => [permissions\Service::HOME_PAGE],
                    ],

                    [
                        'actions' => ['error', 'get-jivo-site-params'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['login', 'registration', 'validate-login', 'robot-registration'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['employee-rating', 'change-theme-color'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => [permissions\User::SIGNUP],
                    ],
                    [
                        'actions' => ['reset-password', 'request-password-reset'],
                        'allow' => true,
                        'roles' => [permissions\User::RESET_PASSWORD],
                    ],
                    [
                        'actions' => [
                            'strict-mode-error',
                            'subscription-expired-error',
                            'inquirer',
                            'view-remaining-modal',
                            'create-service-more-stock',
                            'view-discount-modal',
                            'config',
                            'view-popup-kub-do-it',
                            'change-table-view-mode',
                            'change-product',
                            'demo-login',
                            'demo-logout',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['change-company'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return EmployeeCompany::find()->joinWith('company')->andWhere([
                                'employee_company.employee_id' => ArrayHelper::getValue(Yii::$app->user, 'identity.id'),
                                'employee_company.company_id' => Yii::$app->request->get('id'),
                                'employee_company.is_working' => true,
                                'company.blocked' => false,
                            ])->exists();
                        },
                    ],
                    [
                        'actions' => ['check-discount'],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                    [
                        'actions' => ['currency-rate', 'save-widget-config'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'validate-login' => ['post'],
                    'demo-login' => ['post'],
                    'demo-logout' => ['post'],
                    'check-discount' => ['post'],
                    'view-discount-modal' => ['post'],
                    'config' => ['post'],
                    'change-table-view-mode' => ['post']
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['config'],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'only' => ['registration', 'robot-registration', 'login', 'validate-login', 'request-password-reset'],
                'cors' => [
                    'Origin' => \Yii::$app->params['corsFilter'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['X-PJAX', 'X-PJAX-Container'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $roleId = Yii::$app->user->identity->currentEmployeeCompany->employee_role_id ?? null;
        if ($roleId == EmployeeRole::ROLE_PARTNER_RELATIONS_MANAGER) {
            return $this->redirect('/affiliate');
        }

        if (in_array($action->id, [
            'registration', 'robot-registration', 'login', 'validate-login', 'request-password-reset',
        ])) {
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => \frontend\components\ErrorAction::className(),
            ],
        ];
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $identity = Yii::$app->user->identity;

        if ($identity->company === null) {
            if (($company = $identity->getCompanies()->isBlocked(false)->one()) !== null) {
                $identity->updateAttributes(['company_id' => $company->id]);
                return $this->redirect(['index']);
            } else {
                Yii::$app->user->logout();
                throw new ForbiddenHttpException("Компания не найдена.");
            }
        }

        if (YII_ENV_DEV || YII_ENV_PROD) {
            $isAnalyticsMenu = Yii::$app->user->identity->currentEmployeeCompany->at_business_analytics_now ?? false;
            if ($isAnalyticsMenu) {
                Yii::$app->session->set('modules.reports.finance.year', date('Y')); // exclude different years
                return $this->actionAnalyticsIndex();
            }
        }

        return $this->actionSiteIndex();
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSiteIndex()
    {
        $identity = Yii::$app->user->identity;

        // TEMP!
        if (Yii::$app->user->getIsMtsUser()) {
            return $this->redirect(['/documents/invoice/index', 'type' => 2]);
        }

        $dateRange = StatisticPeriod::getSessionPeriod();

        // activities
        $activitiesSearchModel = new LogSearch();
        $activitiesSearchModel->company_id = Yii::$app->user->identity->company->id;
        $activitiesDataProvider = $activitiesSearchModel->search([]);

        $notifications = (new NotificationSearch([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->search(Yii::$app->request->get(), \Yii::$app->user->identity->company);

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 66);

        $invoiceSearchModel = new InvoiceSearch([
            'type' => Documents::IO_TYPE_OUT,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        $showBanks = Yii::$app->user->can(permissions\Service::HOME_CASH);

        return $this->render('index', [
            'inInvoiceStatistics' => InvoiceStatistic::getStatisticInfo(Documents::IO_TYPE_IN,
                \Yii::$app->user->identity->company->id,
                InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN], $dateRange),

            'cashStatisticInfo' => CashStatisticInfo::getPeriodStatisticInfo([], $showBanks),
            'foraignCashStatisticInfo' => CashStatisticInfo::getForeignCurrensyStatistic([], $showBanks),
            'notifications' => $notifications,
            'currency' => Currency::find()->where(['and', ['name' => ['USD', 'EUR']]])->all(),

            'activitiesSearchModel' => $activitiesSearchModel,
            'activitiesDataProvider' => $activitiesDataProvider,
            'invoicesSearchModel' => $invoiceSearchModel
        ]);
    }

    public function actionAnalyticsIndex()
    {
        $identity = Yii::$app->user->identity;
        $company = $identity->company;
        $tab = (string)Yii::$app->request->get('tab');
        $action = (string)Yii::$app->request->get('action');

        // todo: move into Action
        if ($action === 'refreshFinanceDashboardLink') {
            OutFinanceDashboard::refreshLink($company);
            Yii::$app->session->setFlash('success', 'Ссылка успешно удалена.');
            return $this->redirect(Url::current(['action' => null]));
        }

        // todo: move into Action
        if ($action === 'updateFinanceDashboardOption') {
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'financeDashboardOption',
                'value' => Yii::$app->request->get('value', 1),
            ]));
            return $this->redirect(Url::current(['action' => null, 'value' => null]));
        }

        try {
            $dashboard = new FinanceDashboard($tab);
            $searchModel = $dashboard->getSearchModel();
        } catch (\Throwable $e) {
            $viewError = '@frontend/modules/analytics/views/finance-dashboard/tab/error';
            if (empty(FinanceDashboard::sanitizeTab($tab))) {
                return $this->render($viewError, ['company' => $company,
                    'dashboardErrorMessage' => 'Страница не найдена.',
                ]);
            } else {
                Yii::error(VarDumper::dumpAsString($e), 'analytics');
                return $this->render($viewError, ['company' => $company,
                    'dashboardErrorMessage' => 'Ошибка инициализации дашборда. Обратитесь в службу технической поддержки.',
                ]);
            }
        }

        $this->view->params = [
            'identity' => $identity,
            'company' => $company,
            'dashboard' => $dashboard,
            'searchModel' => $searchModel,
            'tab' => $dashboard->getTab(),
        ];

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (Yii::$app->request->get('_old')) {} else {
            return $this->render('@frontend/modules/analytics/views/finance-dashboard/index', $this->view->params);
        }

        // OLD VERSION
        $showBanks = Yii::$app->user->can(permissions\Service::HOME_CASH);
        $searchModel = new \frontend\modules\analytics\models\dashboardChart\DashboardChartSearch($identity->company);
        return $this->render('@frontend/modules/analytics/views/site/index', [
            'company' => $identity->company,
            'searchModel' => $searchModel,
            'cashStatisticInfo' => CashStatisticInfo::getPeriodStatisticInfo([], $showBanks),
            'foraignCashStatisticInfo' => CashStatisticInfo::getForeignCurrensyStatistic([], $showBanks),
            'currencyInfo' => Currency::find()->where(['and', ['name' => ['USD', 'EUR']]])->all(),
        ]);
    }

    /**
     * @return string
     */
    public function actionEmployeeRating($type, $date)
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }

        $rating = new EmployeeRating([
            'type' => $type,
            'date' => $date,
        ]);

        return $this->renderAjax('_indexPartials/_employee_rating', [
            'rating' => $rating,
        ]);
    }

    /**
     * @return string
     */
    public function actionNotifications()
    {
        $notifications = (new NotificationSearch([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->search(Yii::$app->request->get(), \Yii::$app->user->identity->company);

        return $this->renderAjax('_indexPartials/_taxCalendar', [
            'notifications' => $notifications,
        ]);
    }

    /**
     * @return string
     */
    public function actionActivities()
    {
        // activities
        $activitiesSearchModel = new LogSearch();
        $activitiesSearchModel->company_id = Yii::$app->user->identity->company->id;
        $activitiesDataProvider = $activitiesSearchModel->search(Yii::$app->request->get());

        return $this->renderAjax('_indexPartials/_activities', [
            'activitiesSearchModel' => $activitiesSearchModel,
            'activitiesDataProvider' => $activitiesDataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if (\Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $redirectUrl = Yii::$app->getUser()->getReturnUrl(['/site/index']);
                if ($company = Yii::$app->user->identity->company) {
                    if ($company->login_redirect_times > 0 && !empty($company->login_redirect_url)) {
                        $redirectUrl = $company->login_redirect_url;
                        $company->updateAttributes([
                            'login_redirect_times' => $company->login_redirect_times - 1,
                        ]);
                    } elseif (Yii::$app->user->getIsMtsUser()) {
                        switch ($company->registration_page_type_id) {
                            case RegistrationPageType::PAGE_TYPE_MTS_INVOICE:
                                $redirectUrl = ['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT];
                                break;
                            case RegistrationPageType::PAGE_TYPE_MTS_BUH_IP6:
                                $redirectUrl = ['/tax/robot/index'];
                                break;
                            case RegistrationPageType::PAGE_TYPE_MTS_B2B:
                                $redirectUrl = ['/b2b/module'];
                                break;
                        }
                    } else {
                        if ($company->registration_page_type_id == RegistrationPageType::PAGE_TYPE_FOREIGN_INVOICE_LANDING) {
                            if (!$company->getForeignCurrencyInvoices()->exists()) {
                                $redirectUrl = ['/documents/foreign-currency-invoice/create', 'type' => Documents::IO_TYPE_OUT];
                            }
                        } else {
                            if (!$company->getInvoices()->exists()) {
                                $redirectUrl = ['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT];
                            }
                        }
                    }

                    // Update contact in GetResponse service
                    $gr = new GetResponseApi();
                    $gr->updateGetResponseContact(Yii::$app->user->identity, [
                        'last_sign_in' => date('Y-m-d H:i:s')
                    ]);
                }

                return $this->redirect($redirectUrl);
            }
        }

        if (YII_ENV_DEV || Yii::$app->user->getIsMtsUser()) {
            return $this->render('login', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(Yii::$app->user->loginUrl);
        }
    }

    /**
     * @return array
     */
    public function actionValidateLogin()
    {
        $model = new LoginForm();

        $model->load(\Yii::$app->request->post());
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return ActiveForm::validate($model);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDemoLogin()
    {
        $identity = Yii::$app->user->identity;

        $employee = Employee::findOne([
            'id' => Yii::$app->params['service']['demo_employee_id'] ?? null,
            'is_deleted' => false,
            'is_active' => true,
        ]);
        if ($employee === null) {
            throw new NotFoundHttpException("Демо пользователь не найден");
        }

        $company = Company::findOne([
            'id' => Yii::$app->params['service']['demo_company_id'] ?? null,
            'blocked' => false,
        ]);
        if ($company === null) {
            throw new NotFoundHttpException("Демо компания не найдена");
        }

        $employeeCompany = EmployeeCompany::findOne([
            'employee_id' => $employee->id,
            'company_id' => $company->id,
            'is_working' => true,
        ]);
        if ($employeeCompany === null) {
            throw new NotFoundHttpException("Демо сотрудник не найден");
        }

        if ($employeeCompany->employee_role_id != EmployeeRole::ROLE_DEMO) {
            $employeeCompany->updateAttributes(['employee_role_id' => EmployeeRole::ROLE_DEMO]);
        }

        if ($employee->company_id != $company->id) {
            $employee->updateAttributes(['company_id' => $company->id]);
        }

        Yii::$app->user->logout();
        Yii::$app->user->login($employee);

        Yii::$app->session->set('__is_demo', true);
        Yii::$app->session->set('__before_demo_id', $identity->id);
        Yii::$app->session->set('__before_demo_url', Yii::$app->request->referrer);

        return $this->redirect(['/analytics/finance/odds']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDemoLogout()
    {
        $id = Yii::$app->session->get('__before_demo_id');
        $url = Yii::$app->session->get('__before_demo_url');

        Yii::$app->user->logout();

        if ($id && ($identity = Employee::findIdentity($id)) && Yii::$app->user->login($identity)) {
            return $this->redirect($url ?? ['/site/index']);
        }


        return $this->goHome();
    }

    /**
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        if (Yii::$app->request->isPost) {
            Yii::$app->user->logout();

            return $this->goHome();
        } else {
            return $this->goHome();
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionStatisticRange()
    {
        $name = Yii::$app->request->post('name');
        $date_from = date_create_from_format('Y-m-d', Yii::$app->request->post('date_from')) ?:
            date_create_from_format('d.m.Y', Yii::$app->request->post('date_from'));
        $date_to = date_create_from_format('Y-m-d', Yii::$app->request->post('date_to')) ?:
            date_create_from_format('d.m.Y', Yii::$app->request->post('date_to'));
        $result = false;

        if ($name && $date_from && $date_to) {
            Yii::$app->user->identity->setStatisticRangeDates($date_from->format('Y-m-d'), $date_to->format('Y-m-d'));
            Yii::$app->user->identity->setStatisticRangeName($name, $date_from->format('Y-m-d'), $date_to->format('Y-m-d'));

            $result = true;
        }

        if (Yii::$app->request->isAjax) {
            return $result;
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['/site/index']);
    }

    /**
     *
     */
    public function actionStatisticRangeMonths()
    {
        $name = Yii::$app->request->post('name');
        $date_from = (date_create_from_format('d.m.Y', Yii::$app->request->post('date_from')));
        $date_to = (date_create_from_format('d.m.Y', Yii::$app->request->post('date_to')));
        $result = false;

        if ($name && $date_from && $date_to) {
            $date_from->modify('first day of this month');
            $date_to->modify('last day of this month');
            Yii::$app->user->identity->setStatisticRangeDates($date_from->format('Y-m-d'), $date_to->format('Y-m-d'));
            Yii::$app->user->identity->setStatisticRangeName($name, $date_from->format('Y-m-d'), $date_to->format('Y-m-d'));

            $result = true;
        }

        if (Yii::$app->request->isAjax) {
            return $result;
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['/site/index']);
    }

    /**
     *
     */
    public function actionTaxRobotStatisticRange()
    {
        $post = Yii::$app->request->post();

        if (!empty($post)) {

            $session = Yii::$app->session;
            $session->set('robot_range_date_from', $post['date_from']);
            $session->set('robot_range_date_to', $post['date_to']);
            $session->set('robot_range_name', $post['name']);

            echo true;

        } else {

            echo false;
        }
    }

    /**
     * @param $companyID
     * @return bool
     */
    public function actionChangeStoreSettings($companyID)
    {
        $company = Company::findOne($companyID);

        if ($company->load(Yii::$app->request->post()) && $company->save()) {
            //return $this->redirect(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_SETTINGS]);
        }
        return $this->redirect(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_SETTINGS]);
    }

    /**
     *
     */
    public function actionSidebarStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $config = Yii::$app->user->identity->config;

        if ($config->load(Yii::$app->request->post(), '')) {
            $config->save();
        }

        return $config->minimize_side_menu;
    }

    /**
     *
     */
    public function actionRangeButtonTitle()
    {
        echo StatisticPeriod::getSessionName() . ' <b class="fa fa-angle-down"></b>';
    }

    /**
     * @return string
     */
    public function actionStrictModeError()
    {
        if (Yii::$app->user->isGuest
            || Yii::$app->user->identity->company->strict_mode == Company::OFF_STRICT_MODE
        ) {
            return $this->redirect('index');
        }

        return $this->render('strictModeError');
    }

    /**
     * @return string
     */
    public function actionSubscriptionExpiredError()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->company->getHasActualSubscription($groupId)) {
            return $this->redirect('index');
        }

        return $this->render('subscriptionExpiredError');
    }

    /**
     * @param $type
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetModalRegistration($type = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $header = null;
        $description = null;
        $html = null;
        $showMainModal = false;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($type !== null) {
            $attribute = Company::$afterRegistrationBlockAttributes[$type];
            $company->after_registration_block_type = $type;
            $company->$attribute += 1;
            $company->save(true, ['after_registration_block_type', $attribute]);
            if ($type == Company::AFTER_REGISTRATION_EXPOSE_INVOICE) {
                return true;
            }
        }
        switch ($type) {
            case null:
                $showMainModal = true;
                break;
            case 2:
                $header = 'Подготовить акт, накладную<br> счет-фактуру';
                $data = [
                    'type' => 2,
                    'description' => 'Для этого нужно выставить счет<br> и нажать нужную кнопку:',
                    'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                    'link' => Url::to(['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT]),
                    'image' => ImageHelper::getThumb('img/modal_registration/block-2.jpg', [680, 340], [
                        'class' => 'hide-video',
                        'style' => 'max-width: 100%;box-shadow: 0.3em 0.3em 10px rgba(122,122,122,0.5);',
                    ]),
                    'previousModal' => null,
                    'nextModal' => 3,
                ];
                break;
            case 3:
                $header = 'Анализ бизнеса';
                $data = [
                    'type' => 3,
                    'description' => 'ЗАБУДЬТЕ об ОТЧЕТАХ в ЭКСЕЛЬ!<br>
                    Все отчеты строятся автоматически и без ошибок.<br>
                    Графики по месяцам. Аналитика. Источники прибыли.',
                    'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                    'link' => Url::to(['/reports/finance/odds']),
                    'image' => ImageHelper::getThumb('img/modal_registration/block-5.jpg', [680, 340], [
                        'class' => 'hide-video',
                        'style' => 'max-width: 100%;box-shadow: 0.3em 0.3em 10px rgba(122,122,122,0.5);',
                    ]),
                    'previousModal' => 2,
                    'nextModal' => 4,
                ];
                break;
            case 4:
                $header = 'Контроль работы сотрудников';
                $data = [
                    'type' => 4,
                    'description' => 'ОТЧЁТЫ СТРОЯТСЯ АВТОМАТИЧЕСКИ.<br>
                    Доступно ОНЛАЙН, 24/7<br>
                    Выполнение планов. Графики по месяцам. Лучший сотрудник.',
                    'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                    'link' => Url::to(['/reports/employees/index']),
                    'image' => ImageHelper::getThumb('img/modal_registration/block-4.png', [680, 340], [
                        'class' => 'hide-video',
                        'style' => 'max-width: 100%;box-shadow: 0.3em 0.3em 10px rgba(122,122,122,0.5);',
                    ]),
                    'previousModal' => 3,
                    'nextModal' => 5,
                ];
                break;
            case 5:
                $header = 'Учёт товара';
                $data = [
                    'type' => 5,
                    'description' => 'ОПРИХОДОВАНИЕ, СПИСАНИЕ, ПЕРЕМЕЩЕНИЕ ТОВАРА.<br>
                    КОНТРОЛЬ ОСТАТКОВ. ПЛАНИРОВАНИЕ ЗАКУПОК.',
                    'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                    'link' => Url::to(['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS]),
                    'image' => ImageHelper::getThumb('img/modal_registration/block-3.jpg', [680, 340], [
                        'class' => 'hide-video',
                        'style' => 'max-width: 100%;box-shadow: 0.3em 0.3em 10px rgba(122,122,122,0.5);',
                    ]),
                    'previousModal' => 4,
                    'nextModal' => 6,
                ];
                break;
            case 6:
                $header = 'Повышение продаж';
                $data = [
                    'type' => 6,
                    'description' => 'ПРОДАВАЙТЕ БОЛЬШЕ без УЧАСТИЯ ПРОДАВЦОВ<br>
                    <span style="font-size: 15px;">Ваш сайт сможет самостоятельно выставлять счета покупателям.<br>
                    Создайте личные ОНЛАЙН-КАБИНЕТЫ для покупателей с доступом к складу.</span>',
                    'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
                    'link' => Url::to(['/contractor/sale-increase']),
                    'image' => ImageHelper::getThumb('img/modal_registration/block-6.jpg', [680, 340], [
                        'class' => 'hide-video',
                        'style' => 'max-width: 100%;box-shadow: 0.3em 0.3em 10px rgba(122,122,122,0.5);',
                    ]),
                    'previousModal' => 5,
                    'nextModal' => null,
                ];
                break;
            default:
                throw new NotFoundHttpException();
        }
        if ($showMainModal) {
            return [
                'result' => true,
                'showMainModal' => true,
            ];
        }

        return [
            'result' => true,
            'header' => $header,
            'html' => $this->renderPartial('../layouts/modal/_template_submodal', $data),
        ];
    }

    /**
     * @return bool
     */
    public function actionCancelModalShow()
    {
        $type = Yii::$app->request->post('type');
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        if ($type) {
            if (isset(Company::$notShowModalAttributes[$type])) {
                $company->setAttribute(Company::$notShowModalAttributes[$type], false);
                return $company->save(true, [Company::$notShowModalAttributes[$type]]);
            }
        }

        return false;
    }

    /**
     * @return mixed|string
     */
    public function actionAddFundingFlow()
    {
        $model = new FundingFlowForm();

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
        }

        // refreshes block
        return $this->renderAjax('_indexPartials/_finances');
    }

    /**
     * @return mixed|string
     */
    public function actionAddExpenditureItem()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $companyID = Yii::$app->user->identity->company->id;
        $model = new InvoiceExpenditureItem([
            'company_id' => $companyID,
            'name' => Yii::$app->request->post('name'),
        ]);

        if ($model->save()) {
            $expenseItemFlowOfFunds = new ExpenseItemFlowOfFunds();
            $expenseItemFlowOfFunds->company_id = $companyID;
            $expenseItemFlowOfFunds->expense_item_id = $model->id;
            $expenseItemFlowOfFunds->is_visible = $model->is_visible;

            $profitAndLossCompanyItem = new ProfitAndLossCompanyItem();
            $profitAndLossCompanyItem->company_id = $companyID;
            $profitAndLossCompanyItem->item_id = $model->id;
            $profitAndLossCompanyItem->expense_type = ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE;
            $profitAndLossCompanyItem->can_update = true;

            if ($expenseItemFlowOfFunds->save() && $profitAndLossCompanyItem->save()) {
                return ['itemId' => $model->id, 'itemName' => $model->name];
            }
        }

        return [];
    }

    /**
     * @return mixed|string
     */
    public function actionAddIncomeItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $name = trim(Yii::$app->request->post('name'));
        $name = $name ? mb_strtoupper(mb_substr($name, 0, 1)) . mb_substr($name, 1) : '';
        $companyID = Yii::$app->user->identity->company->id;

        $model = new InvoiceIncomeItem([
            'company_id' => $companyID,
            'name' => $name,
        ]);

        if ($model->save()) {
            $incomeItemFlowOfFunds = new IncomeItemFlowOfFunds();
            $incomeItemFlowOfFunds->company_id = $companyID;
            $incomeItemFlowOfFunds->income_item_id = $model->id;
            $incomeItemFlowOfFunds->is_visible = $model->is_visible;
            if ($incomeItemFlowOfFunds->save()) {
                return ['itemId' => $model->id, 'itemName' => $model->name];
            }
        }

        return [];
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $message = $model->sendEmail() ? 'На Ваш e-mail: ' . $model->email . ' были высланы дальнейшие инструкции.' : 'Ошибка! Письмо не отправлено.';

            return '<div style="margin-bottom: 10px;">' . $message . '</div>';
        }

        if (YII_ENV_DEV) {
            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        } else if (!Yii::$app->request->headers->has('origin')) {
            return $this->goHome();
        }
    }

    /**
     * @param $token
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'Новый пароль сохранён.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * @param null $req
     * @return mixed|string|\yii\web\Response
     */
    public function actionRegistration()
    {
        $this->layout = 'registration';

        $postData = Yii::$app->request->post();
        $model = new RegistrationForm();
        $model->companyType = CompanyType::TYPE_OOO;
        $model->scenario = isset($postData[$model->formName()]['reCaptcha']) ?
            RegistrationForm::SCENARIO_CAPTCHA :
            RegistrationForm::SCENARIO_DEFAULT;

        if (\Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        $useReCaptcha = Yii::$app->params['useReCaptcha'];
        if ($model->load($postData) && $model->validate()) {
            if ($useReCaptcha && $model->scenario == RegistrationForm::SCENARIO_DEFAULT) {
                $model->scenario = RegistrationForm::SCENARIO_CAPTCHA;

                return $this->render('registration_captcha', ['model' => $model]);
            }

            if ($model->save(false)) {
                $user = $model->getUser();

                if (Yii::$app->user->login($user)) {
                    $mailer = clone \Yii::$app->mailer;
                    $mailer->htmlLayout = 'layouts/html2';
                    $mailer->compose([
                        'html' => 'system/new-registration/html',
                        'text' => 'system/new-registration/text',
                    ], [
                        'login' => $user->email,
                        'password' => $model->password,
                        'subject' => 'Добро пожаловать в КУБ24',
                    ])
                        ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                        ->setTo($user->email)
                        ->setSubject('Добро пожаловать в КУБ24')
                        ->send();

                    Yii::$app->session->set('show_example_popup', 1);

                    return $this->redirect($model->getRedirectUrl());
                }
            }
        }

        if ($useReCaptcha && $model->scenario == RegistrationForm::SCENARIO_CAPTCHA) {
            return $this->render('registration_captcha', ['model' => $model]);
        }

        if (YII_ENV_DEV) {
            return $this->render('registration', [
                'model' => $model,
            ]);
        } else if (!Yii::$app->request->headers->has('origin')) {
            return $this->redirect(Yii::$app->request->referrer ? : Yii::$app->getHomeUrl());
        }
    }

    /**
     * @return mixed|string|\yii\web\Response
     */
    public function actionRobotRegistration()
    {
        $model = new \frontend\models\RobotForm();

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->registration()) {
                    $user = $model->user;

                    if (Yii::$app->user->login($user)) {
                        $mailer = clone \Yii::$app->mailer;
                        $mailer->htmlLayout = 'layouts/html2';
                        $mailer->compose([
                            'html' => 'system/new-registration/html',
                            'text' => 'system/new-registration/text',
                        ], [
                            'login' => $user->email,
                            'password' => $model->password,
                            'subject' => 'Добро пожаловать в КУБ24',
                        ])
                            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                            ->setTo($user->email)
                            ->setSubject('Добро пожаловать в КУБ24')
                            ->send();

                        return $this->redirect(['/tax/robot/index']);
                    }
                } else {
                    return $this->redirect('http://robot.kub-24.ru/zajavka-otpravlena/');
                }
            }
        }

        return $this->render('robot-registration', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionUnpaid()
    {
        $searchModel = new InvoiceSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'type' => 1,
            'invoice_status_id' => [
                InvoiceStatus::STATUS_CREATED,
                InvoiceStatus::STATUS_PAYED_PARTIAL,
                InvoiceStatus::STATUS_OVERDUE,
            ],
        ]);

        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            StatisticPeriod::getSessionPeriod()
        );

        return $this->render('_indexPartials/_unpaid_invoices', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => 1,
            'invoice_status_id' => 1,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionChangeCompany($id)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $user->company_id = $id;
        if (!$user->save(true, ['company_id'])) {
            Yii::$app->session->setFlash('error', 'Ошибка при переключении в компанию');
        }

        // todo: temp, because no mts site/index page
        if (Yii::$app->user->getIsMtsUser()) {
            $canInvoiceIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_OUT,
            ]);

            if ($canInvoiceIndex) {
                return $this->redirect([
                    '/documents/invoice/index',
                    'type' => Documents::IO_TYPE_OUT,
                ]);
            } else {
                return $this->redirect(['/company/index']);
            }
        }

        return $this->redirect('/site/index');
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionChangeProduct($product)
    {
        if (!in_array($product, ServiceModule::$productArray)) {
            throw new NotFoundHttpException();
        }

        $isBusinessAnalytics = $product == ServiceModule::ANALYTICS;
        $employeeCompany = Yii::$app->user->identity->currentEmployeeCompany ?? null;
        $company = ($employeeCompany) ? $employeeCompany->company : null;
        if ($employeeCompany && $employeeCompany->at_business_analytics_now != $isBusinessAnalytics) {
            $employeeCompany->updateAttributes([
                'at_business_analytics_now' => $isBusinessAnalytics,
            ]);
        }
        if ($isBusinessAnalytics && $company && $company->business_analytics_first_visit_at === null) {
            $company->updateAttributes([
                'business_analytics_first_visit_at' => time()
            ]);
        }

        return $this->redirect(ServiceModule::indexRoute($product));
    }

    /**
     * @param string $theme
     * @return \yii\web\Response
     */
    public function actionChangeTheme($theme)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $user->updateAttributes(['is_old_kub_theme' => ($theme == 'old') ? 1 : 0]);

        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => 'is_old_kub_theme',
            'value' => $theme,
            'expire' => 0,
        ]));

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => 2]);
    }

    /**
     * @return array
     */
    public function actionGetJivoSiteParams()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->user->isGuest) {
            return [
                'name' => Yii::$app->user->identity->firstname,
                'companyName' => Yii::$app->user->identity->company->getTitle(true),
            ];
        } else {
            return [
                'name' => '',
                'companyName' => '',
            ];
        }
    }

    /**
     * @return bool
     */
    public function actionMenuItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $attribute = Yii::$app->request->post('attribute');
        $val = Yii::$app->request->post('value');
        if ($attribute === null || $val === null || Yii::$app->user->identity->getIsDemo()) {
            return false;
        }
        /* @var $menuItem MenuItem */
        $menuItem = Yii::$app->user->identity->menuItem ?: new MenuItem([
            'company_id' => Yii::$app->user->identity->company->id,
            'employee_id' => Yii::$app->user->identity->id,
        ]);

        $menuItem->setAttributes([
            $attribute => $val
        ]);

        return $menuItem->save();
    }

    /**
     * @return array
     */
    public function actionInquirer()
    {
        $model = new Inquirer([
            'company_id' => Yii::$app->user->identity->company->id,
            'employee_id' => Yii::$app->user->id,
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Ваш ответ принят.');
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : 'index');
    }

    /**
     *
     */
    public function actionViewRemainingModal()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $expireDays = SubscribeHelper::getExpireLeftDays(SubscribeHelper::getExpireDate(SubscribeHelper::getPayedSubscriptions($user->company->id)));
        $showModalAttribute = 'show_reminder_' . $expireDays;
        if (in_array($expireDays, [7, 5, 3, 1]) &&
            $user->employee_role_id == EmployeeRole::ROLE_CHIEF &&
            (int)date('H') >= 7 &&
            $user->company->$showModalAttribute
        ) {
            $company = $user->company;
            $company->$showModalAttribute = false;
            $company->save(true, [$showModalAttribute]);
        }
    }

    /**
     *
     */
    public function actionViewPopupKubDoIt()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $company->show_popup_kub_do_it = false;

        return $company->save(true, ['show_popup_kub_do_it']);
    }

    /**
     *
     */
    public function actionViewDiscountModal()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $discount = $company->getDiscounts()->andWhere([
            'type_id' => DiscountType::TYPE_1,
        ])->one();
        if ($discount) {
            $discount->updateAttributes(['is_known' => true]);

            return $discount->is_known;
        }

        return null;
    }

    /**
     * @return array
     */
    public function actionCreateServiceMoreStock()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $discount = $company->getDiscounts()->andWhere([
            'type_id' => DiscountType::TYPE_1,
        ])->one();

        if ($discount) {
            $model = new ServiceMoreStock(['company_id' => $company->id]);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $discount->updateAttributes(['value' => Discount::TYPE_1 + 10]);

                return ['result' => true];
            } else {
                return [
                    'result' => false,
                    'errors' => $model->getErrors(),
                ];
            }
        }

        return ['result' => false];
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionCheckDiscount()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $discount = $company->getDiscounts()->andWhere([
            'type_id' => DiscountType::TYPE_1,
        ])->one();
        if ($discount) {
            $discount->updateAttributes(['is_active' => !Yii::$app->request->post('discount', false)]);

            return ['is_active' => $discount->is_active];
        }

        return null;
    }

    /**
     * @return array
     */
    public function actionPhoneFeedback()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $phone = Yii::$app->request->post('phone');
        $question = Yii::$app->request->post('question');
        $type = Yii::$app->request->post('type');
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        if ($phone && $type !== null) {
            $phoneFeedback = new PhoneFeedback();
            $phoneFeedback->employee_id = $user->id;
            $phoneFeedback->company_id = $user->company->id;
            $phoneFeedback->popup_type = $type;
            $phoneFeedback->phone = $phone;
            $phoneFeedback->question = $question;
            if ($phoneFeedback->save()) {
                Yii::$app->mailer->compose([
                    'html' => 'system/phone-feedback/html',
                    'text' => 'system/phone-feedback/text',
                ], [
                    'user' => $user,
                    'company' => $user->company,
                    'subject' => 'Нужна помощь',
                    'type' => $type,
                    'phone' => $phoneFeedback->phone,
                    'question' => $phoneFeedback->question,
                ])
                    ->setFrom([Yii::$app->params['emailList']['support'] => Yii::$app->params['emailFromName']])
                    ->setTo(Yii::$app->params['emailList']['support'])
                    ->setSubject('Нужна помощь')
                    ->send();
            }

            return ['result' => true,];
        }

        return ['result' => false,];
    }

    /**
     * @return mixed
     */
    public function actionConfig()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $config = Yii::$app->user->identity->config;

        if ($config->load(Yii::$app->request->post())) {
            $config->save();
        }

        $config = (array)$config->attributes;

        if (strlen($config['product_category_fields'])) {
            $fields = json_decode($config['product_category_fields']);
            $config['product_category_field_isbn'] = ArrayHelper::getValue($fields, ProductCategoryField::ISBN, 0);
            $config['product_category_field_author'] = ArrayHelper::getValue($fields, ProductCategoryField::AUTHOR, 0);
            $config['product_category_field_publishing_year'] = ArrayHelper::getValue($fields, ProductCategoryField::PUBLISHING_YEAR, 0);
            $config['product_category_field_publishing_house'] = ArrayHelper::getValue($fields, ProductCategoryField::PUBLISHING_HOUSE, 0);
            $config['product_category_field_format'] = ArrayHelper::getValue($fields, ProductCategoryField::FORMAT, 0);
            $config['product_category_field_book_binding'] = ArrayHelper::getValue($fields, ProductCategoryField::BOOK_BINDING, 0);
            unset($config['product_category_fields']);
        }

        return $config;
    }

    /**
     * @return mixed
     */
    public function actionCurrencyRate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $date = \DateTime::createFromFormat('d.m.Y|', Yii::$app->request->post('date'));
        $currentDate = new \DateTime;
        if (!$date || $date > $currentDate) {
            $date = $currentDate;
        }
        $date->modify('today');

        return CurrencyRate::getCurrencyRate(Yii::$app->request->post('name'), $date);
    }

    /**
     * @param $type
     */
    public function actionEmployeeClick($type)
    {
        $user = Yii::$app->user->identity;

        $employeeClick = new EmployeeClick();
        $employeeClick->employee_id = $user->id;
        $employeeClick->click_type = $type;
        $employeeClick->click_date = date(DateHelper::FORMAT_DATE);
        $employeeClick->save();
    }

    /**
     * @return array
     */
    public function actionCheckVkPost()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        $postLink = Yii::$app->request->post('post_link');
        $postLink = stristr($postLink, 'wall');
        $postLink = strtok($postLink, '%');
        $postID = preg_replace('/[^0-9_]/', '', $postLink);
        if ($postID && !$company->shared_kub_vk) {
            $accessToken = Yii::$app->params['vkAccessToken'];
            $url = "https://api.vk.com/method/wall.getById?posts={$postID}&v=5.52&access_token={$accessToken}";
            $data = json_decode(file_get_contents($url), true);
            if (isset($data['response'][0]['attachments'][0]['link'])) {
                $data = $data['response'][0]['attachments'][0]['link'];
                $postUrl = $data['url'];
                if ($postUrl == 'https://kub-24.ru') {
                    $company->shared_kub_vk = true;
                    if ($company->save(true, ['shared_kub_vk'])) {
                        return [
                            'result' => true,
                            'msg' => 'Вам начислен 1 счет.',
                        ];
                    }
                }
            }
        }

        return [
            'result' => false,
            'msg' => $company->shared_kub_vk ? 'Вы уже поделились записью и получили вознаграждение.' : 'Неверная ссылка.',
        ];
    }

    /**
     * @return mixed
     */
    public function actionChangeTableViewMode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $config = Yii::$app->user->identity->config;
        $attr = Yii::$app->request->post('attribute');
        $attrInvert = Yii::$app->request->post('invert-attribute');

        if ($attr && isset($config->{$attr})) {
            $config->{$attr} = ($config->{$attr}) ? 0 : 1;
            if ($config->{$attr} && ($attrInvert && isset($config->{$attrInvert}))) {
                $config->{$attrInvert} = 0;
            }
            $config->save(false);

            return ['result' => true];
        }

        return ['result' => false];
    }

    public function actionOnlyNewDesign($redirectUri)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        if (!$user->is_old_kub_theme) {
            return $this->redirect($redirectUri)->send();
        }

        return $this->render('only-new-design');
    }

    public function actionSaveWidgetConfig($widget = 'AccountBalance')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($widget == 'AccountBalance') {
            /** @var Employee $user */
            $user = Yii::$app->user->identity;
            $company = $user->company;
            $model = WidgetConfig::findOne([
                'company_id' => $company->id,
                'employee_id' => $user->id
            ]) ?: new WidgetConfig([
                'company_id' => $company->id,
                'employee_id' => $user->id
            ]);

            $exceptData = Yii::$app->request->post('exceptData', []);

            $options = [
                'rs' => [],
                'cashboxes' => [],
                'emoneys' => [],
            ];

            foreach ($exceptData as $name) {
                @list($prefix, $wallet, $id) = explode('_', $name);
                if (!$wallet || !$id) continue;
                switch ($wallet) {
                    case \common\models\cash\CashFlowsBase::WALLET_BANK:
                        $options['rs'][] = $id;
                        break;
                    case \common\models\cash\CashFlowsBase::WALLET_CASHBOX:
                        $options['cashboxes'][] = $id;
                        break;
                    case \common\models\cash\CashFlowsBase::WALLET_EMONEY:
                        $options['emoneys'][] = $id;
                        break;
                }
            }

            $model->options = json_encode($options);
            return ['success' => (int)$model->save()];
        }

        return ['success' => 1];
    }

    /**
     * @return array
     */
    public function actionChangeThemeColor()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $element = Yii::$app->request->post('element');
        $colorIndex = (int)Yii::$app->request->post('color_index');

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        if ($element == 'table-links' && $colorIndex) {
            $user->updateAttributes(['links_color' => $colorIndex]);
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function actionToggleNewsReaction(int $newsId, int $reaction): array {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->currentEmployeeCompany->company;
        /** @var News $news */
        $news = News::find()->andWhere(['id' => $newsId])->one();
        if ($news === null) {
            return ['result' => false];
        }

        /** @var NewsReaction $newsReaction */
        $newsReaction = NewsReaction::find()
            ->andWhere(['news_id' => $newsId])
            ->andWhere(['company_id' => $company->id])
            ->one();
        if ($newsReaction !== null) {
            if ($newsReaction->reaction == $reaction) {
                $newsReaction->delete();

                return [
                    'result' => true,
                    'likesCount' => $news->getLikesCount(),
                    'dislikesCount' => $news->getDislikesCount(),
                ];
            }

            $newsReaction->reaction = $reaction;
            if (!$newsReaction->save()) {
                return ['result' => false];
            }

            return [
                'result' => true,
                'likesCount' => $news->getLikesCount(),
                'dislikesCount' => $news->getDislikesCount(),
            ];
        }

        $newsReaction = new NewsReaction();
        $newsReaction->news_id = $newsId;
        $newsReaction->company_id = $company->id;
        $newsReaction->reaction = $reaction;
        if (!$newsReaction->save()) {
            return ['result' => false];
        }

        return [
            'result' => true,
            'likesCount' => $news->getLikesCount(),
            'dislikesCount' => $news->getDislikesCount(),
        ];
    }

    public function actionViewNews(): void {
        /** @var EmployeeCompany $currentEmployeeCompany */
        $currentEmployeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
        $currentEmployeeCompany->news_viewed_at = time();
        $currentEmployeeCompany->save(true, ['news_viewed_at']);
    }
}
