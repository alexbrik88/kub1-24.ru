<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * LogController.
 */
class ActivityController extends Controller
{
    protected $_idleTime = 1800;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Страница не найдена.');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $active = false;

        if (!Yii::$app->user->isGuest) {
            $idleTime = !empty(Yii::$app->params['idleTime']) ? Yii::$app->params['idleTime'] : $this->_idleTime;
            if (Yii::$app->request->get('update') == '1' && Yii::$app->user->identity->company) {
                \common\models\company\CompanyLastVisit::create(
                    Yii::$app->user->identity->company,
                    Yii::$app->user->identity,
                    Yii::$app->getRequest()->getUserIP()
                );
            }
            $lastVisit = Yii::$app->user->identity->last_visit_at;

            if (($lastVisit + $idleTime) <= time()) {
                Yii::$app->user->logout();
            } else {
                $active = true;
            }
        }

        return ['active' => $active];
    }
}
