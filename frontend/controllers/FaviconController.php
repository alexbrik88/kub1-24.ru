<?php

namespace frontend\controllers;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * FaviconController.
 */
class FaviconController extends Controller
{
    /**
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->getIsMtsUser()) {
            $file = Yii::getAlias('@frontend/web/img/favicon_red.ico');
        } else {
            $file = Yii::getAlias('@frontend/web/img/fav.svg');
        }
        $fileName = 'favicon.ico';

        return \Yii::$app->response->sendFile($file, $fileName, [
            'inline' => true,
        ]);
    }
}
