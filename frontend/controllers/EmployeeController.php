<?php

namespace frontend\controllers;

use backend\models\Prompt;
use backend\models\PromptHelper;
use common\components\Cropper;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\sender\unisender\UniSender;
use common\components\TextHelper;
use common\components\UploadedFile;
use common\models\Chat;
use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\employee\EmployeeSearch;
use common\models\EmployeeCompany;
use common\models\prompt\PageType;
use common\models\TimeZone;
use frontend\components\FrontendController;
use frontend\models\EmployeeCompanySearch;
use frontend\models\SalaryConfigForm;
use frontend\rbac\permissions;
use Yii;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends FrontendController
{
    //public $layout = 'employee';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view-notification', 'view-messages'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index', 'confirm-email', 'change-remote-employee'],
                        'allow' => true,
                        'roles' => [permissions\Employee::INDEX],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [permissions\Employee::CREATE],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => [permissions\Employee::VIEW],
                    ],
                    [
                        'actions' => ['update', 'img-form', 'img-upload', 'img-crop', 'img-file',
                            'img-delete',],
                        'allow' => true,
                        'roles' => [permissions\Employee::UPDATE],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [permissions\Employee::DELETE],
                    ],
                    [
                        'actions' => ['complete-registration'],
                        'allow' => true,
                        'roles' => [permissions\Employee::COMPLETE_REGISTRATION],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'img-delete' => ['post'],
                    'view-notification' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;
        $searchModel = new EmployeeCompanySearch([
            'company_id' => $company->id,
            'is_working' => 1,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, Yii::$app->user->identity->company->company_type_id + 1, PageType::TYPE_EMPLOYEE, true);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'prompt' => $prompt,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Сотрудник изменен');

            return $this->refresh();
        }
        $model->deleteImage('signature_file');

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new EmployeeCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = Yii::$app->user->identity->company;

        $model = new EmployeeCompany([
            'scenario' => 'create',
            'send_email' => true,
            'company_id' => $company->id,
            'employee_role_id' => EmployeeRole::ROLE_EMPLOYEE,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'is_product_admin' => true,
            'date_hiring' => date('d.m.Y'),
            'sex' => Employee::MALE,
        ]);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Сотрудник добавлен');

            return $this->redirect(['view', 'id' => $model->employee->id]);
        }
        $model->deleteImage('signature_file');

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($apply = (int) Yii::$app->request->post('apply', 0)) {
            $model->apply_signature = true;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Сотрудник изменен');
            return $this->redirect(['view', 'id' => $model->employee_id]);
        }
        $model->deleteImage('signature_file');

        $salaryModel = new SalaryConfigForm([
            'employeeCompany' => $model,
        ]);

        return $this->render('update', [
            'model' => $model,
            'salaryModel' => $salaryModel
        ]);
    }

    /**
     * Deletes an existing EmployeeCompany model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $companyId
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (\Yii::$app->user->id == $id) {
            Yii::$app->session->setFlash('error', 'Вы не можете удалить сами себя, это может сделать другой сотрудник с ролью "Руководитель". Если вы являетесь единственным сотрудником с ролью "Руководитель", то назначте другому сотруднику эту роль.');

            return $this->redirect(\Yii::$app->request->referrer);
        }

        /* @var $model EmployeeCompany */
        $model = $this->findModel($id);

        if ($model->employee_id == $model->company->owner_employee_id) {
            Yii::$app->session->setFlash('error', 'Вы не можете удалить владельца учетной записи компании.');

            return $this->redirect(\Yii::$app->request->referrer);
        }

        /* @var $employee Employee */
        $employee = $model->employee;
        /* @var $company Company */
        $company = $model->company;

        $chiefArray = EmployeeCompany::find()
            ->leftJoin(Employee::tableName(), '{{%employee_company}}.[[employee_id]] = {{%employee}}.[[id]]')
            ->andWhere([
                'employee_company.is_working' => true,
                'employee_company.company_id' => $company->id,
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
            ])->indexBy('employee_id')->all();

        if (count($chiefArray) == 1 && isset($chiefArray[$model->employee_id])) {
            Yii::$app->session->setFlash('error', 'Вы не можете удалить единственного сотрудника с ролью "Руководитель".');

            return $this->redirect(\Yii::$app->request->referrer);
        }

        if ($model->delete()) {
            if (!$employee->getCompanies()->isBlocked(Company::UNBLOCKED)->exists()) {
                $employee->is_active = Employee::NOT_ACTIVE;
                $employee->is_deleted = Employee::DELETED;
                $employee->save(false, [
                    'is_active', 'is_deleted',
                ]);
            } elseif ($employee->company_id == $company->id) {
                $employee->company_id = $employee->getCompanies()->isBlocked(Company::UNBLOCKED)->one()->id;
                $employee->save(true, ['company_id']);
            }
            Yii::$app->session->setFlash('success', 'Сотрудник удален');
        }
        Yii::$app->session->setFlash('success', 'Сотрудник удален');

        return $this->redirect(['index']);
    }

    /**
     * @return mixed|string|Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCompleteRegistration()
    {
        /** @var Employee $model */
        $model = Yii::$app->user->identity;

        $model->setScenario(Employee::SCENARIO_CONTINUE_REGISTRATION);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->firstname_initial = mb_substr($model->firstname, 0, 1, 'UTF-8') . '.';
            $model->patronymic_initial = mb_substr($model->patronymic, 0, 1, 'UTF-8') . '.';
            $model->date_hiring = date(DateHelper::FORMAT_DATE);

            $model->employee_role_id = $model->getOldAttribute('employee_role_id');
            $model->is_registration_completed = true;
            $model->email_confirm_key = TextHelper::randString(45);

            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                /** @var Company $company */
                $company = Yii::$app->user->identity->company;

                $company->setAttributes([
                    'chief_post_name' => $model->position,
                    'chief_lastname' => $model->lastname,
                    'chief_firstname' => $model->firstname,
                    'chief_patronymic' => $model->patronymic,
                    'time_zone_id' => $model->time_zone_id,

                    'email' => $model->email,
                    'phone' => $model->phone,
                ], false);

                if ($model->save() && $company->save()) {
                    $params = [
                        'model' => $model,
                        'lkUrl' => Yii::$app->urlManager->createAbsoluteUrl(['/']),
                        'confirmUrl' => Yii::$app->urlManager->createAbsoluteUrl([
                            '/employee/confirm-email', 'key' => $model->email_confirm_key,
                        ]),
                    ];

                    $send = Yii::$app->mailer
                        ->compose([
                            'html' => 'system/confirm-email/html',
                            'text' => 'system/confirm-email/text',
                        ], [
                            'user' => $model,
                            'confirmUrl' => Yii::$app->urlManager->createAbsoluteUrl([
                                '/employee/confirm-email', 'key' => $model->email_confirm_key,
                            ]),
                        ])
                        ->setFrom([Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
                        ->setTo([$model->email => $model->getFio(true),])
                        ->setSubject('Подтвердите регистрацию в сервисе КУБ')
                        ->send();

                    if ($send) {
                        return true;
                    } else {
                        $model->addError('email', 'Указан не существеющий Email');
                    }
                }

                $db->getTransaction()->rollBack();

                return false;
            });

            if ($saved) {
                return $this->redirect(['/company',]);
            }
        }

        return $this->render('complete-registration', [
            'model' => $model,
        ]);
    }

    /**
     * @param $key
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionConfirmEmail($key)
    {
        /** @var Employee $model */
        $model = Employee::find()
            ->byIsActive(true)
            ->andWhere([
                'email_confirm_key' => $key,
            ])
            ->one();

        if ($model === null || (!Yii::$app->user->isGuest && Yii::$app->user->identity->id != $model->id)) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        if ($model->is_email_confirmed) {
            Yii::$app->session->setFlash('info', 'Вы уже подтвердили свой e-mail.');
        } else {
            $model->is_email_confirmed = true;
            $saved = $model->save(false, ['is_email_confirmed']);

            if ($saved) {
                Yii::$app->session->setFlash('success', 'Ваш e-mail успешно подтверждён.');
            } else {
                Yii::$app->session->setFlash('error', 'При подтверджении e-mail произошла ошибка. Обратитесь к администратору.');
            }
        }

        return $this->redirect(['/']);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionViewNotification()
    {
        if ($user = Yii::$app->user->identity) {
            $user->updateAttributes([
                'view_notification_date' => date('Y-m-d H:i:s'),
            ]);
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionViewMessages()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));

        /* @var $newMessage Chat */
        foreach (Chat::find()->andWhere(['and',
            ['to_employee_id' => $model->employee_id],
            ['is_new' => true],
        ])->orderBy('created_at')->all() as $newMessage) {
            $newMessage->is_new = false;
            $newMessage->save(true, ['is_new']);
        }
    }

    /**
     * @param $id
     * @param $attr
     * @param string $action
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionImgForm($id = null, $attr = null, $action = 'view')
    {
        $actionArray = ['view', 'upload', 'crop'];
        $model = ($id) ? $this->findModel($id) :
            new EmployeeCompany(['company_id' => Yii::$app->user->identity->company->id]);

        return $this->render('partial/_partial_files_signature', [
            'model' => $model,
            'attr' => $attr,
            'action' => in_array($action, $actionArray) ? $action : 'view',
        ]);
    }

    /**
     * @param $id
     * @param $attr
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionImgUpload($id = null, $attr = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = ($id) ? $this->findModel($id) :
            new EmployeeCompany(['company_id' => Yii::$app->user->identity->company->id]);

        if ($file = UploadedFile::getInstanceByName($attr)) {
            if (in_array($file->type, ['image/jpeg', 'image/png'])) {
                if ($model->saveImage($attr, $file)) {
                    return ['status' => 'success'];
                }
            } else {
                return ['status' => 'error', 'message' => 'Допустимый формат файла: jpg, jpeg, png.'];
            }
        }

        return ['status' => 'error', 'message' => 'Ошибка загрузки файла.'];
    }

    /**
     * @throws NotFoundHttpException|\Exception
     */
    public function actionImgCrop()
    {
        $id = Yii::$app->request->post('id');
        $attr = Yii::$app->request->post('attr');
        $width = Yii::$app->request->post('width');
        $model = ($id) ? $this->findModel($id) :
            new EmployeeCompany(['company_id' => Yii::$app->user->identity->company->id]);
        $imgPath = $model->getTmpImage($attr);
        $cropper = new Cropper([
            'imagePath' => $imgPath,
            'destPath' => $imgPath,
            'width' => $width,
        ]);
        $cropper->crop($attr);
    }

    /**
     * @param $id
     * @param $attr
     * @throws NotFoundHttpException
     */
    public function actionImgFile($id = null, $attr = null)
    {
        $model = ($id) ? $this->findModel($id) :
            new EmployeeCompany(['company_id' => Yii::$app->user->identity->company->id]);

        $imgPath = $model->getTmpImage($attr);
        if (is_file($imgPath)) {
            return Yii::$app->response->sendFile($imgPath, null, ['inline' => true])->send();
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionImgDelete()
    {
        $id = Yii::$app->request->post('id');
        $attr = Yii::$app->request->post('attr');
        $model = ($id) ? $this->findModel($id) :
            new EmployeeCompany(['company_id' => Yii::$app->user->identity->company->id]);
        $model->deleteImage($attr);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $company = Yii::$app->user->identity->company;

        $model = EmployeeCompany::find()->with(['employee'])->andWhere([
            'employee_id' => $id,
            'company_id' => $company->id,
        ])->one();

        if ($model && $model->employee && !$model->employee->is_deleted) {
            $model->populateRelation('company', $company);

            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    public function actionChangeRemoteEmployee()
    {
        $company = Yii::$app->user->identity->company;
        $isChecked = Yii::$app->request->post('isChecked');
        $remoteEmployeeID = \yii\helpers\ArrayHelper::getValue(Yii::$app->params['service'], 'remote_employee_id');
        $remoteEmployee = Employee::findOne($remoteEmployeeID);

        if (!$remoteEmployee) {

            Yii::$app->session->setFlash('error', 'Оператор не определен');

            return $this->redirect(Yii::$app->request->referrer ?: '/employee/index');
        }

        if ($isChecked) {

            if (EmployeeCompany::find()->where([
                'company_id' => $company->id,
                'employee_id' => $remoteEmployeeID
            ])->exists()) {

                return $this->redirect(Yii::$app->request->referrer ?: '/employee/index');
            }

            $model = new EmployeeCompany([
                'company_id' => $company->id,
                'employee_id' => $remoteEmployee->id,
                'scenario' => 'create',
                'send_email' => false,
                'employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
                'is_product_admin' => true,
                // required data
                'lastname' => $remoteEmployee->lastname,
                'firstname' => $remoteEmployee->firstname,
                'patronymic' => $remoteEmployee->patronymic,
                'has_no_patronymic' => $remoteEmployee->has_no_patronymic,
                'sex' => $remoteEmployee->sex,
                'phone' => $remoteEmployee->phone,
                'date_hiring' => date('Y-m-d'),
                'position' => 'Тех.поддержка',
                'can_business_analytics' => 1,
                'can_ba_all' => 1
            ]);

            $model->save(false);
            Yii::$app->session->setFlash('success', 'Оператор подключен');

            if ($remoteEmployee->company_id === null) {
                $remoteEmployee->company_id = $company->id;
                $remoteEmployee->save(true, ['company_id']);
            }

        } else {

            $isCurrentEmployeeRemote = $remoteEmployee->id == Yii::$app->user->id;
            $remoteEmployeeCompany = EmployeeCompany::find()->where([
                'company_id' => $company->id,
                'employee_id' => Yii::$app->params['service']['remote_employee_id']
            ])->one();

            if ($remoteEmployeeCompany) {

                $remoteEmployeeCompany->delete();
                Yii::$app->session->setFlash('success', 'Оператор отключен');

                // Change company after deleted from
                if ($isCurrentEmployeeRemote) {
                    $otherCompany = $remoteEmployee->getCompanies()->one();

                    if ($otherCompany) {
                        Yii::$app->user->identity->company_id = $otherCompany->id;
                        Yii::$app->user->identity->save(true, ['company_id']);
                    } else {
                        Yii::$app->user->identity->company_id = null;
                        Yii::$app->user->identity->save(false, ['company_id']);
                        Yii::$app->user->logout();
                    }

                    return $this->goHome();
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: '/employee/index');
    }
}
