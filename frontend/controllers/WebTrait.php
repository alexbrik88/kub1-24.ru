<?php

namespace frontend\controllers;

use common\models\employee\Employee;
use frontend\components\WebParams;
use frontend\components\WebUser;
use Yii;
use yii\web\Request;
use yii\web\Response;
use yii\web\Session;
use yii\web\User;

/**
 * @property-read Request $request
 * @property-read Response $response
 * @property-read Session $session
 * @property-read WebUser $user
 */
trait WebTrait
{
    /**
     * @var WebParams|null
     */
    private $_params;

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->getParams()->getRequest();
    }

    /**
     * @return Response
     */
    public function getResponse(): Response
    {
        return $this->getParams()->getResponse();
    }

    /**
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->getParams()->getSession();
    }

    /**
     * @return WebUser
     */
    public function getUser(): User
    {
        return $this->getParams()->getUser();
    }

    /**
     * @return Employee|null
     * @throws
     * @deprecated
     */
    public function getEmployee(): ?Employee
    {
        $identity = $this->getParams()->getUser()->getIdentity();

        if ($identity instanceof Employee) {
            return $identity;
        }

        return null;
    }

    /**
     * @return WebParams
     * @throws
     */
    private function getParams(): WebParams
    {
        if ($this->_params === null) {
            $this->_params = Yii::createObject(WebParams::class);
        }

        return $this->_params;
    }
}
