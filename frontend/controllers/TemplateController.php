<?php

namespace frontend\controllers;

use common\models\file\actions\GetFileAction;
use common\components\filters\AccessControl;
use common\models\template\TemplateFile;
use common\models\template\TemplateSearch;
use frontend\components\FrontendController;
use frontend\rbac\permissions;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Company;

/**
 * TemplateController implements the CRUD actions for Template model.
 */
class TemplateController extends FrontendController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions'   =>  [
                            'index',
                            'get-file',
                            'find',
//                            'create',
//                            'update',
//                            'view',
                        ],
                        'allow'     =>  true,
                        'roles'     =>  [permissions\Template::INDEX],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'get-file' => [
                'class' => GetFileAction::className(),
                'model' => TemplateFile::className(),
                'strictByCompany' => false,
                'fileNameField' => 'file_name',
                'folderPath' => DIRECTORY_SEPARATOR . TemplateFile::tableName(),
                'fileNameCallback' => function (TemplateFile $model) {
                    return $model->template_id . DIRECTORY_SEPARATOR . $model->id;
                },
            ],
        ]);
    }

    /**
     * Lists all Template models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new TemplateSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 67);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'templates' => $dataProvider->models,
        ]);
    }
    public function actionFind()
    {
        $searchModel = new TemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $queryModels = TemplateFile::find()->where(['like','file_name',Yii::$app->request->get('q')]);
        return $this->render('index', [
            'queryModels' => $queryModels,
            'searchModel' => $searchModel,
            'templates' => $dataProvider->models,
        ]);
    }
    public function actionUpdate()
    {
        $searchModel = new TemplateSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('update', [
            'searchModel' => $searchModel,
            'templates' => $dataProvider->models,
        ]);
    }

    public function actionCreate()
    {
        $modelCompany = new Company();
        $modelTemplateFile = new TemplateFile();
        $searchModel = new TemplateSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('create', [
            'modelCompany'      =>  $modelCompany,
            'modelTemplateFile' =>  $modelTemplateFile,
            'searchModel'       =>  $searchModel,
            'templates'         =>  $dataProvider->models,
        ]);
    }

    public function actionView()
    {
        $searchModel = new TemplateSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'templates' => $dataProvider->models,
        ]);
    }
}
