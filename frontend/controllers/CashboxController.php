<?php

namespace frontend\controllers;

use Yii;
use common\components\filters\AjaxFilter;
use common\models\cash\Cashbox;
use frontend\models\CashboxSearch;
use frontend\components\FrontendController;
use frontend\rbac\UserRole;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * CashboxController implements the CRUD actions for Cashbox model.
 */
class CashboxController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cashbox models.
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;
        $searchModel = new CashboxSearch(['company_id' => $company->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cashbox model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $company = Yii::$app->user->identity->company;
        $model = $this->findModel($id, $company->id);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('view', [
                'model' => $model,
            ]);
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Cashbox model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = Yii::$app->user->identity->company;
        $model = new Cashbox([
            'company_id' => $company->id,
            'is_accounting' => false,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!Yii::$app->request->isAjax) {
                return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'success' => true,
                ]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function actionCreateWithAjaxValidation()
    {
        $company = Yii::$app->user->identity->company;
        $model = new Cashbox([
            'company_id' => $company->id,
            'is_accounting' => false,
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'status' => 1,
            ];
        }

        return [
            'status' => 0,
        ];
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $company = Yii::$app->user->identity->company;
        $model = $this->findModel($id, $company->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!Yii::$app->request->isAjax) {
                return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            } else {
                return $this->renderAjax('update', [
                    'model' => $model,
                    'success' => true,
                ]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cashbox model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $company = Yii::$app->user->identity->company;
        $this->findModel($id, $company->id)->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => 2,
                'js' => '$.pjax.reload("#cashbox-pjax-container", {timeout: 5000});',
            ];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Cashbox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cashbox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $company_id)
    {
        if (($model = Cashbox::findOne(['id' => $id, 'company_id' => $company_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
