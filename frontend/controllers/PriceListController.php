<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.10.2018
 * Time: 11:41
 */

namespace frontend\controllers;


use backend\models\Prompt;
use backend\models\PromptHelper;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\AgreementTemplate;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\product\phpexcel_value_binder\PHPExcel_PriceListCellValueBinder;
use common\models\product\PriceList;
use common\models\product\PriceListCheckedProducts;
use common\models\product\PriceListContact;
use common\models\product\PriceListNotification;
use common\models\product\PriceListOrder;
use common\models\product\PriceListStatus;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\prompt\PageType;
use common\models\selling\SellingSubscribe;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\PriceListSearch;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\AgreementSearch;
use frontend\rbac\permissions;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class PriceListController
 * @package frontend\controllers
 */
class PriceListController extends FrontendController
{
    private $_redirectAction = 'step-company';

    private static $stepActions = [
        'step-instruction',
        'step-company',
        'step-products',
        'step-services',
        'step-price-lists'
    ];

    private static $createActions = [
        'create',
        'continue-create',
        'create-card'
    ];

    private static $updateActions = [
        'update',
        'update-card',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['delete', 'many-delete'],
                        'allow' => true,
                        'roles' => [permissions\Product::DELETE],
                    ],
                    [
                        'actions' => ['create', 'continue-create', 'set-checked-products', 'send'],
                        'allow' => true,
                        'roles' => [permissions\Product::CREATE],
                    ],
                    [
                        'actions' => ['step-company'],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                    [
                        'actions' => ['step-products', 'step-services', 'step-price-lists', 'step-instruction', 'index'],
                        'allow' => true,
                        'roles' => [permissions\Product::INDEX],
                    ],
                    [
                        'actions' => ['view', 'product-index'],
                        'allow' => true,
                        'roles' => [permissions\Product::VIEW],
                    ],
                    [
                        'actions' => ['create-card'],
                        'allow' => true,
                        'roles' => [permissions\Product::CREATE],
                    ],
                    [
                        'actions' => ['update-card', 'comment-internal', 'update-is-archive', 'copy'],
                        'allow' => true,
                        'roles' => [permissions\Product::UPDATE],
                    ],
                    [
                        'actions' => ['view-card', 'print'],
                        'allow' => true,
                        'roles' => [permissions\Product::VIEW],
                    ],
                    [
                        'actions' => ['out-view', 'download', 'notification-viewed', 'create-trial-subscribe'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|Response
     * @throws \yii\web\BadRequestHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        if (!$user)
            return parent::beforeAction($action);

        if (parent::beforeAction($action)) {

            if (in_array($action->id, self::$stepActions)) {
                if (!$this->actionIsAllowed($action->id)) {
                    $this->redirect($this->_redirectAction)->send();

                    return false;
                }
            }
            if (in_array($action->id, self::$createActions)) {

                $actualSubscription = SubscribeHelper::findActualSubscription($user->company, SubscribeTariffGroup::PRICE_LIST);
                $lastActiveSubscribe = SubscribeHelper::getLastExpiredSubscription($user->company, SubscribeTariffGroup::PRICE_LIST);

                if (!$actualSubscription && !$lastActiveSubscribe) {
                    $this->_createTrialSubscribe();
                }

                if (!$this->_isAllowCreate()) {
                    Yii::$app->session->setFlash('error', 'Лимит на создание прайс-листов исчерпан');
                    $this->redirect(['/price-list/step-price-lists'])->send();

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->actionIsAllowed($this->action->id);

        return $this->redirect($this->_redirectAction);
    }

    /**
     * @param $productionType
     * @param null $store
     * @return Response
     * @throws \yii\db\StaleObjectException
     */
    public function actionCreate($productionType, $store = null)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = new PriceList();
        $model->production_type = $productionType;
        $model->company_id = $user->company_id;
        $model->author_id = $user->id;
        $model->is_deleted = false;

        if ($model->load(Yii::$app->request->post()) && $model->save() && $model->loadOrders($store)) {
            if ($model->include_type_id == PriceList::INCLUDE_SELECTIVELY_PRODUCTS) {
                return $this->redirect([
                    '/product/add-to-price-list',
                    'productionType' => $productionType,
                    'priceList' => $model->id,
                    'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],
                    'per-page' => 0,
                ]);
            }

            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

            Yii::$app->session->setFlash('success', 'Прайс-лист создан.');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при создании прайс-листа.');
        }
        Yii::$app->session->set('showPriceList', 1);

        return $this->redirect([
            '/product/index',
            'productionType' => $productionType,
            'ProductSearch' => [
                'filterStatus' => ProductSearch::IN_WORK
            ],
        ]);
    }

    /**
     * @param $id
     * @param $productionType
     * @param null $store
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionContinueCreate($id, $productionType, $store = null)
    {
        $model = $this->findModel($id, $productionType);
        $products = Yii::$app->request->post('products');
        $products = $products ? json_decode($products) : null;

        if ($model->loadOrders($store, $products)) {
            $priceListCheckedProducts = $model->priceListCheckedProducts;
            $priceListCheckedProducts->delete();
            $model->is_finished = true;
            $model->save(true, ['is_finished']);

            Yii::$app->session->setFlash('success', 'Прайс-лист создан.');
        }
        Yii::$app->session->set('showPriceList', 1);

        return $this->redirect([
            '/product/index',
            'productionType' => $productionType,
            'ProductSearch' => [
                'filterStatus' => ProductSearch::IN_WORK
            ],
            'per-page' => 10,
        ]);
    }

    /**
     * @param $id
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $productionType)
    {
        $model = $this->findModel($id, $productionType);

        $model->autoUpdatePrices();
        $model->autoUpdateQuantity();

        $this->layout = 'price-list';

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid)
    {
        $this->layout = 'price-list';

        /** @var PriceList $model */
        $model = PriceList::find()
            ->andWhere(['uid' => $uid])
            ->andWhere(['is_finished' => true])
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        $model->autoUpdatePrices();
        $model->autoUpdateQuantity();
        $model->setNotification(Yii::$app->request->get('nuid'));

        return $this->render('view', [
            'model' => $model,
            'uid' => $uid
        ]);
    }

    /**
     * @param $type
     * @param null $uid
     * @param null $id
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionDownload($type, $uid = null, $id = null)
    {
        /* @var PriceList $model */
        $model = ($uid) ?
            PriceList::find()->where(['uid' => $uid])->one() :
            PriceList::find()->where(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        switch ($type) {
            case 'pdf':
                return $this->_pdf($model, 'pdf-view', ['model' => $model], $model->pageOrientation == 'landscape' ? 'A4-L' : 'A4-P');
                break;

            default:
                return $this->_xls($model);
                break;
        }
    }

    public function actionPrint($id = null)
    {
        /* @var PriceList $model */
        $model = PriceList::find()->where(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])->one();

        if (!$model)
            throw new NotFoundHttpException('Страница не найдена.');

        $this->view->params['asset'] = InvoicePrintAsset::className();

        Yii::$app->view->registerJs('window.print();');

        return $this->_pdf($model, 'pdf-view', ['model' => $model, 'asHTML' => TRUE], $model->pageOrientation == 'landscape' ? 'A4-L' : 'A4-P');
    }

    /**
     * @param $id
     * @param $productionType
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionSend($id, $productionType = null)
    {
        $model = $this->findModel($id, $productionType);

        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = PriceList::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {

            $this->createNotifications($model, $sendForm->sendTo);

            $saved = $sendForm->sendPriceList();

            if (is_array($saved)) {

                $model->updateAttributes([
                    'status_id' => PriceListStatus::STATUS_SEND,
                    'send_count' => $model->send_count + count(explode(',', $sendForm->sendTo)),
                    'last_sended_to' => $this->_getLastSendedToCompanies($sendForm->sendTo)
                ]);

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);

                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['/product/index', 'productionType' => $productionType, 'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK],]);
    }

    /**
     * @param $id
     * @param $productionType
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionSetCheckedProducts($id, $productionType)
    {
        $checkedProducts = Yii::$app->request->post('checkedProducts');

        $priceList = $this->findModel($id, $productionType);

        $priceListCheckedProducts = $priceList->priceListCheckedProducts;
        if (!$priceList->priceListCheckedProducts) {
            $priceListCheckedProducts = new PriceListCheckedProducts();
            $priceListCheckedProducts->price_list_id = $priceList->id;
        }
        $priceListCheckedProducts->checked_products = $checkedProducts;

        return $priceListCheckedProducts->save();
    }

    /**
     * @param $productionType
     * @return Response
     */
    public function actionProductIndex($productionType)
    {
        Yii::$app->session->set('showPriceList', 1);

        return $this->redirect(Url::to([
            '/product/index',
            'productionType' => $productionType,
            'ProductSearch' => [
                'filterStatus' => ProductSearch::IN_WORK,
            ],
        ]));
    }

    /**
     * @param $id
     * @param $productionType
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id, $productionType = null)
    {
        $model = $this->findModel($id, $productionType);
        $model->is_deleted = true;
        $result = $model->save(false, ['is_deleted']);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'result' => $result,
                'message' => $result ? 'Прайс-лист удален.' : 'Ошибка при удалении прайс-листа.',
                'id' => $id,
            ];
        }

        if ($result) {
            Yii::$app->session->setFlash('success', 'Прайс-лист удален');
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при удалении прайс-листа');
        }

        return $this->redirect('index');
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete()
    {
        $result = null;
        $models = Yii::$app->request->post('PriceList', []);
        if (is_array($models)) {
            foreach ($models as $id => $model) {
                if ($model['checked']) {
                    /* @var $model PriceList */
                    $model = $this->findModel($id);
                    $model->is_deleted = true;
                    $result = $model->save(false, ['is_deleted']);

                    if (!$result)
                        break;

                    LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);
                }
            }
        }

        if ($result)
            Yii::$app->session->setFlash('success', 'Прайс-листы удалены');
        elseif ($result !== null)
            Yii::$app->session->setFlash('error', 'Ошибка при удалении прайс-листов');

        return $this->redirect(Yii::$app->request->referrer ?: 'index');
    }

    /**
     * @param $id
     * @param $productionType
     * @return PriceList
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $productionType = null)
    {
        /* @var PriceList $model */
        $model = PriceList::find()
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['id' => $id])
            ->andFilterWhere(['production_type' => $productionType])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }

    public function actionCreateCard()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = new PriceList();
        $model->production_type = Product::PRODUCTION_TYPE_ALL;
        $model->company_id = $user->company_id;
        $model->author_id = $user->id;
        $model->is_deleted = false;
        $model->name = 'Прайс-лист_' . date(DateHelper::FORMAT_USER_DATE);
        $model->include_name_column = 1;
        $model->include_reminder_column = 1;
        $model->include_product_unit_column = 1;
        $model->include_price_column = 1;
        $model->sort_type = PriceList::SORT_BY_NAME;

        $modelContact = new PriceListContact();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            $modelContact->load(Yii::$app->request->post());

            return ActiveForm::validate($model, $modelContact);
        }

        if ($model->load(Yii::$app->request->post()) && $modelContact->load(Yii::$app->request->post())) {

            $model->setPriceListType();
            $model->loadProductCategoryColumns();

            $storesIds = (array)Yii::$app->request->post('storeId');
            if ($storesIds && $storesIds[0] == 'all')
                $storesIds = null;

            if ($model->_price_list_type_id == PriceList::FOR_SELECTIVELY) {
                $orderArray = (array)Yii::$app->request->post('orderArray');
                $productsIds = array_filter((array)ArrayHelper::getColumn($orderArray, 'product_id'));
                $pricesByProducts = (array)ArrayHelper::map($orderArray, 'product_id', 'price');

                if (!array_filter($productsIds)) {
                    $model->addError('production_type', 'Прайс-лист не может быть пустым');
                }

            } else {
                $productsIds = $pricesByProducts = [];
            }

            if ($model->validate(null, false) && $modelContact->validate(['firstname', 'lastname', 'site', 'email', 'patronymic'])) {

                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $model->save();
                    $model->link('contact', $modelContact);
                    foreach ((array)$storesIds as $storeId) {
                        if ($store = $user->getStores()->andWhere(['id' => $storeId])->one())
                            $model->link('stores', $store);
                    }

                    // slice products list by tariff rules
                    $subscribeProductsLimit = $this->_subscribeProductsLimit();

                    if ($model->loadOrders($storesIds, $productsIds, $pricesByProducts, $subscribeProductsLimit)) {
                        Yii::$app->session->setFlash('success', 'Прайс-лист создан.');

                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

                        $transaction->commit();

                        $this->_sortOrders($model);

                    } else {
                        Yii::$app->session->setFlash('error', 'Ошибка при создании прайс-листа.');
                        $transaction->rollBack();
                    }

                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ошибка при создании прайс-листа.');
                    $transaction->rollBack();
                }

                return $this->redirect(['/price-list/view-card', 'id' => $model->id]);

            } else {
                // var_dump($model->getErrors(), $modelContact->getErrors()); die;
            }
        } else {
            $modelContact->setAttributes([
                'email' => $user->email,
                'phone' => $user->phone,
                'lastname' => $user->lastname,
                'firstname' => $user->firstname,
                'patronymic' => $user->patronymic,
                'no_patronymic' => $user->has_no_patronymic
            ]);

            $lastPriceList = PriceList::find()->where([
                'company_id' => $user->company_id,
                'is_deleted' => 0])
                ->orderBy(['created_at' => SORT_DESC])
                ->one();

            if ($lastPriceList && $lastPriceList->priceListContact) {
                $modelContact->site = $lastPriceList->priceListContact->site;
                if (!$modelContact->phone)
                    $modelContact->phone = $lastPriceList->priceListContact->phone;
            }
        }

        /** @var Subscribe $subscribe */
        $subscribe = $user->company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST);

        return $this->render('create-card', [
            'user' => $user,
            'company' => $user->company,
            'model' => $model,
            'modelContact' => $modelContact,
            'productionType' => Product::PRODUCTION_TYPE_ALL,
            'tariffRules' => $this->_getTariffRules($subscribe),
            'actualSubscription' => SubscribeHelper::findActualSubscription($user->company, SubscribeTariffGroup::PRICE_LIST),
            'lastActiveSubscribe' => SubscribeHelper::getLastExpiredSubscription($user->company, SubscribeTariffGroup::PRICE_LIST),
        ]);
    }

    protected function _getTariffRules($subscribe)
    {
        if ($subscribe) {
            if ($tariff = $subscribe->tariff) {
                return [
                    'has_auto_update' => $tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST),
                    'has_product_view' => $tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST),
                    'has_checkout' => $tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST),
                    'has_notification' => $tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST),
                ];
            } else { // promocode
                return [
                    'has_auto_update' => 1,
                    'has_product_view' => 1,
                    'has_checkout' => 1,
                    'has_notification' => 1,
                ];
            }
        }

        $tariff = new SubscribeTariff([
            'id' => SubscribeTariff::TARIFF_PRICE_LIST_TRIAL,
            'tariff_group_id' => SubscribeTariffGroup::PRICE_LIST
        ]);

        return [
            'has_auto_update' => $tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST),
            'has_product_view' => $tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST),
            'has_checkout' => $tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST),
            'has_notification' => $tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST),
        ];
    }

    public function actionUpdateCard($id)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $model PriceList */
        $model = $this->findModel($id);
        $modelContact = $model->priceListContact;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());
            $modelContact->load(Yii::$app->request->post());

            return ActiveForm::validate($model, $modelContact);
        }

        if ($model->load(Yii::$app->request->post()) && $modelContact->load(Yii::$app->request->post())) {

            $model->setPriceListType();
            $model->loadProductCategoryColumns();

            // disable change prices in sended price-lists
            if (!$model->auto_update_prices && $model->status_id == PriceListStatus::STATUS_SEND) {
                $model->has_discount = $model->getOldAttribute('has_discount');
                $model->discount = $model->getOldAttribute('discount');
                $model->has_markup = $model->getOldAttribute('has_markup');
                $model->markup = $model->getOldAttribute('markup');
            }

            $storesIds = (array)Yii::$app->request->post('storeId');
            if ($storesIds && $storesIds[0] == 'all')
                $storesIds = null;

            if ($model->_price_list_type_id == PriceList::FOR_SELECTIVELY) {
                $orderArray = (array)Yii::$app->request->post('orderArray');
                $productsIds = (array)ArrayHelper::getColumn($orderArray, 'product_id');
                $pricesByProducts = (array)ArrayHelper::map($orderArray, 'product_id', 'price');

                if (!array_filter($productsIds)) {
                    $model->addError('production_type', 'Прайс-лист не может быть пустым');
                    $model->_price_list_type_id = $model->getOldAttribute('_price_list_type_id');
                    $model->setPriceListType();
                }
            } else {
                $productsIds = $pricesByProducts = [];
            }

            if ($model->validate(null, false) && $modelContact->validate(['firstname', 'lastname', 'site', 'email', 'patronymic'])) {

                $transaction = Yii::$app->db->beginTransaction();

                try {

                    $model->unlinkAll('stores', true);
                    // $model->unlinkAll('orders', true);

                    $model->save();
                    $model->link('contact', $modelContact);

                    foreach ((array)$storesIds as $storeId) {
                        if ($store = $user->getStores()->andWhere(['id' => $storeId])->one())
                            $model->link('stores', $store);
                    }

                    // slice products list by tariff rules
                    $subscribeProductsLimit = $this->_subscribeProductsLimit();

                    if ($model->loadOrders($storesIds, $productsIds, $pricesByProducts, $subscribeProductsLimit)) {

                        Yii::$app->session->setFlash('success', 'Прайс-лист обновлен.');

                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);

                        $transaction->commit();

                        $this->_sortOrders($model);

                    } else {
                        Yii::$app->session->setFlash('error', 'Ошибка при обновлении прайс-листа.');
                        $transaction->rollBack();
                    }

                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ошибка при обновлении прайс-листа.');
                    $transaction->rollBack();
                }

                return $this->redirect(['/price-list/view-card', 'id' => $model->id]);

            } else {
                // var_dump($model->getErrors(), $modelContact->getErrors()); die;
            }
        } else {
            if (!$modelContact) {
                $modelContact = new PriceListContact();

                $modelContact->setAttributes([
                    'price_list_id' => $model->id,
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'lastname' => $user->lastname,
                    'firstname' => $user->firstname,
                    'patronymic' => $user->patronymic,
                    'no_patronymic' => $user->has_no_patronymic
                ]);
                $modelContact->save(false);
            }
        }

        /** @var Subscribe $subscribe */
        $subscribe = $user->company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST) ?:
            SubscribeHelper::getLastExpiredSubscription($user->company, SubscribeTariffGroup::PRICE_LIST);

        if (!$subscribe)
            throw new Exception('Price-list subscribe not found');

        return $this->render('create-card', [
            'user' => $user,
            'company' => $user->company,
            'model' => $model,
            'modelContact' => $modelContact,
            'productionType' => Product::PRODUCTION_TYPE_ALL,
            'tariffRules' => $this->_getTariffRules($subscribe),
            'actualSubscription' => SubscribeHelper::findActualSubscription($model->company, SubscribeTariffGroup::PRICE_LIST),
            'lastActiveSubscribe' => SubscribeHelper::getLastExpiredSubscription($model->company, SubscribeTariffGroup::PRICE_LIST)
        ]);
    }

    /**
     * @param $id
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewCard($id)
    {
        $model = $this->findModel($id);

        $model->autoUpdatePrices();
        $model->autoUpdateQuantity();

        if ($model->uid == null) {
            $model->uid = PriceList::generateUid();
            $model->save(false, ['uid']);
        }

        return $this->render('view-card', [
            'model' => $model,
            'actualSubscription' => SubscribeHelper::findActualSubscription($model->company, SubscribeTariffGroup::PRICE_LIST),
            'lastActiveSubscribe' => SubscribeHelper::getLastExpiredSubscription($model->company, SubscribeTariffGroup::PRICE_LIST)
        ]);
    }

    /***************
     *  STEPS PART
     **************/

    // STEP 1
    public function actionStepInstruction()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $tariffGroup = SubscribeTariffGroup::findOne(SubscribeTariffGroup::PRICE_LIST);
        $tariffList = $tariffGroup->getAllTariffs()
            ->orderBy(['id' => SORT_ASC])
            ->indexBy('id')
            ->all();

        return $this->render('step-instruction', [
            'step' => 1,
            'user' => $user,
            'company' => $user->company,
            'tariffGroup' => $tariffGroup,
            'tariffList' => $tariffList,
        ]);
    }

    // STEP 2
    public function actionStepCompany()
    {
        CheckingAccountant::$formNameModify = false;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = $user->company;
        $taxation = $model->companyTaxationType;
        $accounts = $model->getCheckingAccountants()->orderBy([
            'type' => SORT_ASC,
            'id' => SORT_ASC,
        ])->all();
        if (empty($accounts)) {
            $accounts[] = new CheckingAccountant([
                'company_id' => $model->id,
                'type' => CheckingAccountant::TYPE_MAIN,
            ]);
        }
        if ($accounts[0]->type != CheckingAccountant::TYPE_MAIN) {
            $accounts[0]->type = CheckingAccountant::TYPE_MAIN;
        }

        $model->scenario = $model->getIsLikeIP() ?
            Company::SCENARIO_B2B_IP :
            Company::SCENARIO_B2B_OOO;

        if (Yii::$app->request->getIsPost()) {
            $newCount = max(0, count(Yii::$app->request->post('CheckingAccountant')) - count($accounts));
            if ($newCount) {
                // Creatin new CheckingAccountant instances
                for ($i=0; $i < $newCount; $i++) {
                    $accounts[] = new CheckingAccountant([
                        'company_id' => $model->id,
                        'type' => CheckingAccountant::TYPE_ADDITIONAL,
                    ]);
                }
            }

            $isLoad = $model->load(Yii::$app->request->post());
            $isLoad = $taxation->load(Yii::$app->request->post()) && $isLoad;
            $isLoad = Model::loadMultiple($accounts, Yii::$app->request->post()) && $isLoad;

            if (Yii::$app->request->post('ajax')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array_merge(ActiveForm::validate($model, $taxation), ActiveForm::validateMultiple($accounts));

                return $result;
            }

            if ($isLoad && Model::validateMultiple($accounts)) {
                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $taxation, $accounts) {
                    if ($model->save() && $taxation->save()) {
                        foreach ($accounts as $account) {
                            $account->save();
                        }
                        // To delete accounts
                        $deleteRs = Yii::$app->request->post('delete_rs');
                        if (!empty($deleteRs)) {
                            foreach ((array)$deleteRs as $deleteId) {
                                $deleteModel = CheckingAccountant::findOne([
                                    'id' => $deleteId,
                                    'company_id' => $model->id,
                                ]);
                                if ($deleteModel) {
                                    try {
                                        $deleteModel->delete();
                                    } catch (\Exception $e) {
                                        Yii::$app->session->setFlash('error', "Невозможно удалить р/с " . $deleteModel->rs);
                                    }
                                }
                            }
                        }

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });

                if ($isSaved) {

                    $model->updateAttributes([
                        'strict_mode' => Company::OFF_STRICT_MODE,
                        'strict_mode_date' => time()
                    ]);

                    return $this->redirect("step-products");
                }
            }
        }

        return $this->render('step-company', [
            'model' => $model,
            'taxation' => $taxation,
            'accounts' => $accounts,
        ]);
    }

    // STEP 3
    public function actionStepProducts() {

        return $this->myProducts(3, Product::PRODUCTION_TYPE_GOODS);
    }

    // STEP 4
    public function actionStepServices() {

        return $this->myProducts(4, Product::PRODUCTION_TYPE_SERVICE);
    }

    // STEP 5
    public function actionStepPriceLists()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $searchModel = new PriceListSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('step-price-lists', [
            'step' => 4,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'company' => $company,
            'actualSubscription' => SubscribeHelper::findActualSubscription($user->company, SubscribeTariffGroup::PRICE_LIST),
            'lastActiveSubscribe' => SubscribeHelper::getLastExpiredSubscription($user->company, SubscribeTariffGroup::PRICE_LIST),
        ]);
    }

    private function actionIsAllowed($action)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $setFlash = ($action === 'index') ? false : true;

        if ($action == 'step-instruction') {
            return true;
        }

        if ($action == 'index' && !$this->_hasAnyTariff()) {
            $this->_redirectAction = '/price-list/step-instruction';
            return false;
        }

        if ($action == 'step-company') {
            return true;
        }

        $company->scenario = $company->company_type_id == CompanyType::TYPE_IP ?
            Company::SCENARIO_B2B_IP :
            Company::SCENARIO_B2B_OOO;

        if (!$company->validate() || $company->mainCheckingAccountant === null || !$company->companyTaxationType->validate()) {
            $this->_redirectAction = '/price-list/step-company';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо заполнить реквизиты.");
            }
            return false;
        }

        $this->_redirectAction = '/price-list/step-price-lists';

        return true;
    }

    private function _hasAnyTariff()
    {
        return Yii::$app->user->identity->company->getHasActualSubscription(SubscribeTariffGroup::PRICE_LIST) ?:
            SubscribeHelper::getLastExpiredSubscription(Yii::$app->user->identity->company, SubscribeTariffGroup::PRICE_LIST);
    }

    private function myProducts($stepNum, $productionType) {

        $store = null;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $storeList = [];

        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
            'status' => Product::ACTIVE,
            'documentType' => 2

        ]);

        if (null === Yii::$app->request->get('ProductSearch["filterStatus"]', null)) {
            $searchModel->filterStatus = ProductSearch::IN_WORK;
        }

        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $page_type = PageType::TYPE_PRODUCT;
            $storeList = $user
                ->getStores()
                ->select(['name'])
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
            $storeIds = array_keys($storeList);

            if ($store === null) {
                $store = $user->getStores()->orderBy(['is_main' => SORT_DESC])->select('id')->scalar();
            } elseif ($store == 'archive') {
                $store == 'all';
                $searchModel->status = Product::ARCHIVE;
            }

            $searchModel->store_id = $store == 'all' ?
                $company->getStores()->select('id')->column() :
                (in_array($store, $storeIds) ? $store : -1);
        } else {
            $page_type = PageType::TYPE_SERVICE;
            if ($store == 'archive') {
                $searchModel->status = Product::ARCHIVE;
            }
        }
        $getParams = Yii::$app->request->get();
        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, $company->company_type_id + 1, $page_type, true);
        $dataProvider = $searchModel->search($getParams);
        $dataProvider->pagination->pageSize = PageSize::get();

        $defaultSortingAttr = isset($getParams['defaultSorting']) ? $getParams['defaultSorting'] : ProductSearch::DEFAULT_SORTING_TITLE;
        $config = $user->config;
        if ($defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID && $config->product_group == false) {
            $config->product_group = true;
            $config->save(true, ['product_group']);
        }

        if ($company->strict_mode) {
            $referrer = Yii::$app->request->referrer;
            Yii::$app->session->setFlash('error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' . Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => Url::current(),
                ])) . '.'
            );
        }
        Url::remember('', 'product_list');

        return $this->render('step-products', [
            'company' => $company,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productionType' => $productionType,
            'prompt' => $prompt,
            'storeList' => $storeList,
            'store' => $store,
            'defaultSortingAttr' => $defaultSortingAttr,
            'priceList' => null,
            'user' => $user,
            'step' => $stepNum
        ]);

    }


    /**
     * @param $model
     * @param $view
     * @param $params
     * @param string $format
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    protected function _pdf($model, $view, $params, $format = 'A4-P')
    {
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => $params,

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $model->getPdfFileName(),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => $format,
        ]);

        if (isset($params['asHTML']))
            return $renderer->renderHtml();

        $renderer->output();
    }

    /**
     * @param PriceList $model
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    protected function _xls($model)
    {
        $isProduct = $model->production_type == Product::PRODUCTION_TYPE_GOODS || $model->production_type == Product::PRODUCTION_TYPE_ALL;
        $sortAttr = $model->getSortAttributeName();
        $priceListOrders = $model->getPriceListOrders()
            ->joinWith('productGroup')
            ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
            ->all();

        $fileName = $model->getXlsFileName();

        $xls = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder(new \PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder);

        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Прайс-лист');

        $totalRows = count($priceListOrders) + 1;
        $totalCols = 1;
        $head = ['№'];
        $row = 1;
        if ($model->include_name_column) {
            $head[] = 'Наименование';
            $totalCols++;
        }
        if ($isProduct && $model->include_article_column) {
            $head[] = 'Артикул';
            $totalCols++;
        }
        if ($model->include_product_group_column) {
            $head[] = 'Группа';
            $totalCols++;
        }
        if ($isProduct && $model->include_reminder_column) {
            $head[] = 'Остаток';
            $totalCols++;
        }
        if ($model->include_product_unit_column) {
            $head[] = 'Ед. измерения';
            $totalCols++;
        }
        if ($model->include_price_column) {
            $head[] = 'Цена';
            $totalCols++;
        }
        if ($isProduct && $model->include_description_column) {
            $head[] = 'Описание';
            $totalCols++;
        }

        $char = function($col) { return self::getExcelColumnChar($col); };

        // subhead
        $author = $model->priceListContact ?: $model->author;
        $contacts = [];
        if (!empty($author->phone)) $contacts[] = 'Тел: ' . $author->phone;
        if (!empty($author->email)) $contacts[] = 'E-mail: ' . $author->email;
        if (!empty($author->site))  $contacts[] = 'Сайт: ' . $author->site;
        $contacts = implode(', ', $contacts);

        $sheet->mergeCells("A{$row}:{$char($totalCols)}{$row}");
        $sheet->setCellValue("A{$row}", $model->company->getTitle(true));
        $row++;
        $sheet->mergeCells("A{$row}:{$char($totalCols)}{$row}");
        $sheet->setCellValue("A{$row}", $author->getFio());
        $row++;
        $sheet->mergeCells("A{$row}:{$char($totalCols)}{$row}");
        $sheet->setCellValue("A{$row}", $contacts);
        $row++;
        $sheet->getStyle("A1:A3")
            ->getAlignment()
            ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        // head
        $col = 1;
        foreach ($head as $key => $value) {
            $sheet->setCellValue("{$char($col)}{$row}", $value);
            $col++;
        }

        $totalColsChar = self::getExcelColumnChar($totalCols);

        $sheet->getStyle("A4:{$totalColsChar}".(3+$totalRows))->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );

        // body
        $num = 0;
        foreach ($priceListOrders as $key => $priceListOrder) {

            $row++;
            $col = 1;
            $num++;

            if ($num > 0) {
                $sheet->setCellValue("{$char($col)}{$row}", $num);
            }
            if ($model->include_name_column) {
                $col++;
                $cell = $priceListOrder->name;
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
            }
            if ($isProduct && $model->include_article_column) {
                $col++;
                $cell = $priceListOrder->article;
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
            }
            if ($model->include_product_group_column) {
                $col++;
                $cell = $priceListOrder->productGroup ? $priceListOrder->productGroup->title : '---';
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
            }
            if ($isProduct && $model->include_reminder_column) {
                $col++;
                $cell = $priceListOrder->quantity;
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
                $sheet->getStyle("{$char($col)}{$row}")
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            }
            if ($model->include_product_unit_column) {
                $col++;
                $cell = $priceListOrder->productUnit ? $priceListOrder->productUnit->name : '---';
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
                $sheet->getStyle("{$char($col)}{$row}")
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            }
            if ($model->include_price_column) {
                $col++;
                $cell = TextHelper::invoiceMoneyFormat($priceListOrder->price_for_sell, 2, '.', '');
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
                $sheet->getStyle("{$char($col)}{$row}")
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            }
            if ($isProduct && $model->include_description_column) {
                $col++;
                $cell = $priceListOrder->description;
                $sheet->setCellValue("{$char($col)}{$row}", $cell);
            }
        }

        for ($i = 1; $i <= $totalCols; $i++) {
            $xls->getActiveSheet()->getColumnDimension($char($i))->setAutoSize(true);
        }

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" );
        header ( "Content-Disposition: attachment; filename=" . $fileName );


        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($xls, 'Xlsx');
        $objWriter->save('php://output');
        exit;
    }

    public static function getExcelColumnChar($num) {
        $numeric = ($num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($num - 1) / 26);
        if ($num2 > 0) {
            return self::getExcelColumnChar($num2) . $letter;
        } else {
            return $letter;
        }
    }

    /**
     * @param $id
     * @param $value
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateIsArchive($id, $value)
    {
        $model = $this->findModel($id);

        if ('1' == $value) {
            $model->updateAttributes(['status_id' => PriceListStatus::STATUS_ARCHIVE]);
            //LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
        } elseif ('0' == $value) {
            $model->updateAttributes(['status_id' => ($model->send_count > 0) ? PriceListStatus::STATUS_SEND : PriceListStatus::STATUS_CREATED]);
            //LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
        } else {
            return $this->redirect(['view-card', 'id' => $id]);
        }

        Yii::$app->session->setFlash('success', ($value) ? 'Прайс-лист перемещен в архив' : 'Прайс-лист перемещен из архива');

        return $this->redirect(['view-card', 'id' => $id]);
    }

    /**
     * @param $id
     * @return Response
     * @throws \yii\base\Exception
     */
    public function actionCopy($id)
    {
        $model = $this->findModel($id);
        $cloneModel = $this->cloneModel($id);

        if ($cloneModel->save(false)) {
            foreach ($model->priceListOrders as $order) {
                $cloneOrder = $this->cloneOrder($order);
                $cloneOrder->price_list_id = $cloneModel->primaryKey;
                $cloneOrder->save(false);
            }
            if ($model->contact) {
                $cloneContact = $this->cloneContact($model->contact);
                $cloneContact->price_list_id = $cloneModel->primaryKey;
                $cloneContact->save(false);
            }

            // LogHelper::log($cloneModel, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

            Yii::$app->session->setFlash('success', 'Прайс-лист скопирован');

            return $this->redirect(['view-card', 'id' => $cloneModel->id]);

        } else {
            return $this->redirect(['view-card', 'id' => $id]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $id
     * @return PriceList
     * @throws NotFoundHttpException
     */
    protected function cloneModel($id)
    {
        $clone = clone $this->findModel($id)->cloneSelf();

        return $clone;
    }

    protected function cloneOrder(PriceListOrder $order)
    {
        $clone = clone $order;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->price_list_id = null;

        return $clone;
    }

    protected function cloneContact(PriceListContact $contact)
    {
        $clone = clone $contact;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->price_list_id = null;

        return $clone;
    }

    protected function _getLastSendedToCompanies($emails)
    {
        $company = Yii::$app->user->identity->company;
        $emailsArr = explode(',', $emails);
        $retEmailsArr = [];
        foreach ($emailsArr as $email) {
            $contractor = $company->getContractors()->byContractor(Contractor::TYPE_CUSTOMER)
                ->andWhere(['director_email' => $email])
                ->one();

            /** @var $contractor Contractor */
            if ($contractor) {
                $retEmailsArr[] = $contractor->getShortName();
            } else {
                $retEmailsArr[] = $email;
            }
        }

        return implode(', ', $retEmailsArr);
    }

    /**
     * @param PriceList $priceList
     * @param $emails
     * @return bool
     * @throws \Exception
     */
    public function createNotifications(PriceList $priceList, $emails)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $emailsArr = explode(',', $emails);
        $retEmailsArr = [];
        foreach ($emailsArr as $email) {

            $contractor = $company->getContractors()->byContractor(Contractor::TYPE_CUSTOMER)
                ->andWhere(['director_email' => $email])
                ->one();

            $notification = new PriceListNotification([
                'company_id' => $company->id,
                'price_list_id' => $priceList->id,
                'uid' => PriceListNotification::generateUid($priceList->company),
                'created_at' => time(),
                'contractor_email' => $email
            ]);

            /** @var $contractor Contractor */
            if ($contractor) {
                $notification->contractor_id = $contractor->id;
            }

            if (!$notification->save()) {
                return false;
            }
        }

        return true;
    }

    public function actionNotificationViewed($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        /** @var PriceListNotification $notification */
        $notification = PriceListNotification::find()->where([
            'company_id' => $company->id,
            'id' => $id,
        ])->one();

        if ($notification === null || !$notification->company || $notification->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        if ($notification) {

            $notification->updateAttributes([
                'is_notification_viewed' => 1,
                'notification_viewed_at' => time()
            ]);

            $company->updateAttributes([
                'has_price_list_notifications' => 0
            ]);

            return 1;
        }

        return 0;
    }

    protected function _createTrialSubscribe()
    {
        if ($this->_existsSubscribe(SubscribeTariffGroup::PRICE_LIST)) {
            //Yii::$app->session->setFlash('error', 'Тариф уже активирован');

            return false;
        }

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        /** @var SubscribeTariff $tariff */
        $tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_PRICE_LIST_TRIAL);
        /** @var Subscribe $subscribe */
        $subscribe = new Subscribe([
            'company_id' => $company->id,
            'tariff_group_id' => $tariff->tariff_group_id,
            'tariff_limit' => $tariff->tariff_limit,
            'tariff_id' => $tariff->id,
            'duration_month' => $tariff->duration_month,
            'duration_day' => $tariff->duration_day,
            'status_id' => SubscribeStatus::STATUS_PAYED,
        ]);

        if ($subscribe->save()) {
            $subscribe->activate();
            //Yii::$app->session->setFlash('success', 'Пробный тариф активирован');

            return true;
        }

        //Yii::$app->session->setFlash('error', 'Ошибка при активации пробного тарифа');

        return false;
    }

    // todo: subscribe
    protected function _existsSubscribe($groupId)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        /** @var Subscribe $subscribe */
        return Subscribe::findOne([
            'company_id' => $company->id,
            'tariff_group_id' => $groupId
        ]);
    }

    // todo: subscribe
    protected function _isAllowCreate()
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        /** @var Subscribe $subscribe */
        $subscribe = $company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST, true);

        if (!$subscribe)
            return false;

        return !$subscribe->getIsLimitReached();
    }

    // todo: subscribe
    public function _subscribeProductsLimit()
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        /** @var Subscribe $subscribe */
        $subscribe = $company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST);

        if (!$subscribe)
            return null;

        if ($tariff = $subscribe->tariff)
            return $tariff->getNamedParamValue('products_count', SubscribeTariffGroup::PRICE_LIST);

        // promocode
        return 500;
    }

    protected function _sortOrders(PriceList $model)
    {
        $sortedProductsIds = Yii::$app->request->post('sort', []);
        $orders = $model->getPriceListOrders()->all();
        foreach ($orders as $order) {
            $sortPosition = array_search($order->product_id, $sortedProductsIds) + 1;
            $order->updateAttributes(['sort' => $sortPosition]);
        }
    }
}