<?php

namespace frontend\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use common\models\file\FileHeaderHelper;
use common\models\file\actions\GetFileAction;
use common\models\report\Report;
use common\models\report\ReportFile;
use common\models\report\ReportSearch;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\ProfitAndLossContactUs;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Report::INDEX],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [permissions\Report::CREATE],
                    ],

                    [
                        'actions' => ['update', 'upload-file'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Report::UPDATE, [
                                'model' => $action->controller->findModel(),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['delete', 'delete-file'],
                        'allow' => true,
                        'roles' => [permissions\Report::DELETE],
                    ],
                    [
                        'actions' => ['view', 'get-file', 'list-file'],
                        'allow' => true,
                        'roles' => [permissions\Report::VIEW],
                    ],

                    [
                        'actions' => ['learn-more-about-profit-and-loss'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportSearch();

        $searchModel->setAttribute('company_id', Yii::$app->user->identity->company->id);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * View an existing Report model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        if (\Yii::$app->request->isAjax) {
            $model = $this->findModel($id);

            return $this->renderAjax('_view', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Report model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id = null)
    {
        if (!\Yii::$app->user->identity->company->uploadAllowed()) {
            $link = Html::a('перейдите на платный тариф', ['/subscribe']);
            \Yii::$app->session->setFlash('error', "У вас закончилось место на диске для хранения сканов и документов. Использовано более 1 ГБ. Освободите место или $link");

            return $this->redirect(\Yii::$app->request->referrer);
        }

        if ($id) {
            $model = $this->findModel($id);

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Отчет добавлен');

                return $this->redirect(['index']);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else {
            $model = Report::findOne([
                'is_created' => 0,
                'company_id' => Yii::$app->user->identity->company->id,
            ]);

            if ($model === null) {
                $model = new Report(['scenario' => Report::SCENARIO_CREATE]);
                $model->save();
            }

            if ($model->getReportFiles()->exists()) {
                foreach ($model->reportFiles as $file) {
                    $file->delete();
                }
            }

            return $this->redirect(['create',
                'id' => $model->id,
            ]);
        }
    }

    /**
     * Update an existing Report model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (\Yii::$app->request->isAjax) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Отчет изменен');

                return $this->renderAjax('_view', [
                    'model' => $model,
                ]);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }


    /**
     * Deletes an existing Report model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Отчет удален');

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUploadFile($id)
    {
        if (!\Yii::$app->user->identity->company->uploadAllowed()) {
            echo json_encode(['success' => false, 'msg' => "У вас закончилось место на диске для хранения сканов и документов. Использовано более 1 ГБ. Освободите место или перейдите на платный тариф."]);
            Yii::$app->end();
        }

        $model = $this->findModel($id);

        if (count($model->reportFiles) >= $model->maxFiles) {
            echo json_encode(['success' => false, 'msg' => 'Максимальное количество файлов: ' . $model->maxFiles . '.']);
            Yii::$app->end();
        }
        $model->uploadedFile = [UploadedFile::getInstanceByName('file')];

        if ($model->validate(['uploadedFile']) && $model->upload(false)) {
            echo json_encode(['success' => true, 'msg' => 'Файл успешно загружен']);
        } else {
            echo json_encode(['success' => false, 'msg' => $model->getFirstError('uploadedFile')]);
        }
        Yii::$app->end();
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDeleteFile($id)
    {
        $model = $this->findReportFileModel($id);
        $report = $model->report;
        $model->delete();

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_file_list', [
                'model' => $report,
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionListFile($id)
    {
        $model = $this->findModel($id);

        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_file_list', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Show uploaded file.
     * @param integer $rid
     * @param integer $fid
     * @param string $filename
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionGetFile($rid, $fid, $filename)
    {
        $model = $this->findModel($rid);
        $file = $model->getReportFiles()->andWhere(['id' => $fid])->one();
        $filePath = $file ? $file->getFilePath() : '';

        if ($file && is_file($filePath)) {
            FileHeaderHelper::contentHeaders($file, $filePath, $file->file_name);
            FileHeaderHelper::cachingHeaders($filePath);

            echo file_get_contents($filePath);

            Yii::$app->end();
        } else {
            throw new NotFoundHttpException('Requested file not found.');
        }
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = Report::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->andWhere([
                'id' => $id,
            ])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Report not found.');
        }
    }

    /**
     * @param $id
     *
     * @return ReportFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findReportFileModel($id)
    {
        if (($model = ReportFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionLearnMoreAboutProfitAndLoss()
    {
        $sendForm = new ProfitAndLossContactUs();

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($sendForm);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {

            $subject = 'Заявка на первоначальный ввод данных.';

            \Yii::$app->mailer->compose([
                'html' => 'system/learn-more-about-profit-and-loss/html',
                'text' => 'system/learn-more-about-profit-and-loss/text',
            ], [
                'subject' => $subject,
                'footerText' => 'Отправлено со страницы ОПиУ',
                'form' => $sendForm
            ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo(\Yii::$app->params['emailList']['support'])
            ->setSubject($subject)
            ->send();

            Yii::$app->session->setFlash('success', 'Запрос успешно отправлен.');

            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'learn-more-about-profit-and-loss',
                'value' => time(),
                'expire' => time() + 3600 * 24
            ]));
        } else {
            var_dump($sendForm->getErrors());exit;
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
