<?php

namespace frontend\controllers;

use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentValidation;
use common\components\moysklad\MoyskladObjectValidation;
use common\models\product\Product;
use common\models\product\ProductGroup;
use Yii;
use common\components\filters\AccessControl;
use common\components\moysklad\MoyskladImport;
use common\models\Contractor;
use common\models\ContractorAccount;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\converter\MoyskladConverter;
use common\components\moysklad\parser\MoyskladParser;
use common\models\Agreement;
use frontend\components\FrontendController;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;

/**
 * FOR DEBUG ONLY!
 *
 * Class MoyskladController
 * @package frontend\controllers
 */

class MoyskladController extends FrontendController {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['parse', 'convert', 'clear'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ]);
    }

    public function actionClear()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $deleteObjects = 1;

        /// DEBUG //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (YII_ENV_DEV) {
            if ($deleteObjects) {
                MoyskladImport::deleteAll(['company_id' => $company->id]);
                MoyskladObject::deleteAll(['company_id' => $company->id]);
                MoyskladDocument::deleteAll(['company_id' => $company->id]);
                MoyskladObjectValidation::deleteAll(['import_id' => MoyskladImport::find()->where(['company_id' => $company->id])->select('id')->column()]);
                MoyskladDocumentValidation::deleteAll(['import_id' => MoyskladImport::find()->where(['company_id' => $company->id])->select('id')->column()]);
            } else {
                MoyskladObject::updateAll(['is_validated' => null, 'validated_at' => null], ['company_id' => $company->id]);
                MoyskladDocument::updateAll(['is_validated' => null, 'validated_at' => null], ['company_id' => $company->id]);
            }
            Yii::$app->db->createCommand("delete from `cash_order_flow_to_invoice` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `cash_order_flows` where company_id = {$company->id}")->execute();
            Yii::$app->db->createCommand("delete from `order_invoice_facture` where order_id IN (select id from `order` where invoice_id in (select id from invoice where company_id = {$company->id}))")->execute();
            Yii::$app->db->createCommand("delete from `invoice_facture` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `order_upd` where order_id IN (select id from `order` where invoice_id in (select id from invoice where company_id = {$company->id}))")->execute();
            Yii::$app->db->createCommand("delete from `upd` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `order_packing_list` where order_id IN (select id from `order` where invoice_id in (select id from invoice where company_id = {$company->id}))")->execute();
            Yii::$app->db->createCommand("delete from `packing_list` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `order_act` where order_id IN (select id from `order` where invoice_id in (select id from invoice where company_id = {$company->id}))")->execute();
            Yii::$app->db->createCommand("delete from `act` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `order` where invoice_id in (select id from invoice where company_id = {$company->id})")->execute();
            Yii::$app->db->createCommand("delete from `invoice` where company_id = {$company->id}")->execute();
            ContractorAccount::deleteAll(['contractor_id' => Contractor::find()->where(['company_id' => $company->id])->select('id')->column()]);
            Agreement::deleteAll(['contractor_id' => Contractor::find()->where(['company_id' => $company->id])->select('id')->column()]);
            Contractor::deleteAll(['company_id' => $company->id]);
            Product::deleteAll(['company_id' => $company->id]);
            ProductGroup::deleteAll(['company_id' => $company->id]);
        }
        // DEBUG //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        echo 'Cleared';
        exit;
    }

    public function actionParse()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        ////////////////////////////
        $filename = 'D:\export_15.xml';
        ////////////////////////////

        $model = new MoyskladImport([
            'company_id' => $company->id,
            'employee_id' => $user->id,
            'store_id' => $company->mainStore->id,
            'document_format_id' => MoyskladImport::DOCUMENT_FORMAT_ACT_WITH_PACKING_LIST,
            'loading_step' => MoyskladImport::STEP_PARSING_FILE,
            'created_at' => time()
        ]);

        if ($model->save()) {

            $parser = new MoyskladParser($model);
            $parser->stat = new MoyskladImportStatistics($model);

            // parse XML
            $parser->open($filename);
            $parser->parse();
            $parser->close();
            $parser->stat->update();

            echo 'Parsed success: ' . $model->id;
        }

        exit;
    }

    public function actionConvert($id = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        /** @var MoyskladImport $model */
        $model = MoyskladImport::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['employee_id' => $user->id])
            ->andFilterWhere(['id' => $id])
            ->orderBy(['id' => SORT_DESC])
            ->one();

        if ($model) {

            $converter = new MoyskladConverter($model);
            $converter->stat = new MoyskladImportStatistics($model);
            $converter->validation = new MoyskladValidationErrors($model);


                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0')->execute();

                // save dicts
                $converter->insertContractors();
                $converter->insertProductGroups();
                $converter->insertProducts();
                //$converter->insertStores();

                // docs
                $converter->insertInvoices();
                $converter->insertInvoicesGeneratedFromUpds();
                $converter->insertUpds();
                $converter->insertPaymentOrders();
                $converter->insertInvoiceFactures();

                // contractor types
                $converter->updateContractorsTypes();

                // products prices
                $converter->updateProductsPrices();

                $converter->stat->update();

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 1')->execute();

                echo 'Converted success';


        } else {
            echo 'model not found!';
        }

        exit;
    }
}