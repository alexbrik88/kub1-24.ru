<?php

namespace frontend\controllers;

use Yii;
use yii\rest\Controller;

/**
 * WebhookController.
 */
class WebhookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return yii\helpers\ArrayHelper::merge(
            parent::behaviors(),
            [
                'authenticator'  => new \yii\helpers\UnsetArrayValue(),
            ]
        );
    }

    /**
     * @return mixed
     */
    public function actionUnisenderUnsubscribed()
    {
        $r = Yii::$app->request->getRawBody();
        file_put_contents(Yii::getAlias('@runtime/logs/unsubscribed.log'), var_export($r, true)."\n\n", FILE_APPEND);

        return;
    }
}
