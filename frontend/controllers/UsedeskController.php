<?php

namespace frontend\controllers;

use common\models\employee\Employee;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * UsedeskController.
 */
class UsedeskController extends \yii\rest\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['authenticator']);
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'matchCallback' => function ($rule, $action) {
                        $secretKey = ArrayHelper::getValue(Yii::$app->params, 'usedesk_secret_key');

                        return $secretKey && Yii::$app->request->get('secret_key') === $secretKey;
                    },
                ],
            ],
            'denyCallback' => function ($rule, $action) {
                throw new ForbiddenHttpException('Invalid "secret_key"');
            }
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::$app->set('user', [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\employee\Employee',
            'enableSession' => false,
            'loginUrl' => null,
        ]);
    }

    /**
     * Declares the allowed HTTP verbs.
     * @return array the allowed HTTP verbs.
     */
    protected function verbs()
    {
        return [
            'user-info' => ['POST']
        ];
    }

    /**
     * @return mixed
     */
    public function actionUserInfo()
    {
        $model = null;
        $data = Yii::$app->getRequest()->getBodyParams();

        if (isset($data['client_data']['emails'])) {
            foreach ((array) $data['client_data']['emails'] as $email) {
                $employee = Employee::findIdentityByLogin($email);
                if ($employee && $employee->company) {
                    $model = $employee->company;
                    break;
                }
            }
        }

        return [
            'html' => $this->renderPartial('user-info', ['model' => $model]),
        ];
    }
}
