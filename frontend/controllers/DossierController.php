<?php

namespace frontend\controllers;

use common\components\zchb\ZCHBAPIException;
use common\components\zchb\ZCHBContainer;
use common\components\zchb\ZCHBHelper;
use common\models\company\CompanyFirstEvent;
use common\components\zchb\Card;
use common\components\zchb\Contact;
use common\components\zchb\CourtArbitration;
use common\components\zchb\Diff;
use common\components\zchb\Enforcement;
use common\components\zchb\Manager;
use common\models\Contractor;
use common\models\DossierLog;
use common\models\employee\Employee;
use common\models\service\SubscribeTariffGroup;
use frontend\components\DossierController as DossierControllerComponents;
use frontend\components\ZCHBHelperContractorLog;
use frontend\models\FsspTask;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\base\Action;

/**
 * Построение досье по контрагентам.
 */
class DossierController extends DossierControllerComponents
{

    /** @var Contractor */
    protected $contractor;

    /**
     * DossierController constructor.
     * @param       $id
     * @param       $module
     * @param array $config
     * @throws NotFoundHttpException
     * @throws ZCHBAPIException
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $id = Yii::$app->request->get('modelId') ?? Yii::$app->request->get('id');
        $this->contractor = $this->findModel($id);
    }

    public function tabLink(string $tab)
    {
        return [$tab, 'id' => $this->contractor->id];
    }

    public function actionIndex()
    {
        $this->getView()->params['contractor_id'] = $this->contractor->id;

        if (!$this->contractor->verified) {
            $this->contractor->saveVerified();
        }

        return parent::actionIndex();
    }

    /**
     * Тарифы
     * @return string
     */
    public function actionTariff()
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        if ($this->contractor->type == Contractor::TYPE_CUSTOMER) {
            CompanyFirstEvent::checkEvent($employee->company, 73);
        }
        $tariffGroup = SubscribeTariffGroup::findOne(SubscribeTariffGroup::CHECK_CONTRACTOR);
        $tariffList = $tariffGroup->getActualTariffs()->orderBy([
            'price' => SORT_ASC,
        ])->all();

        return $this->renderTab([
            'isAllowedDossier' => true,
            'company' => $employee->company,
            'tariffGroup' => $tariffGroup,
            'tariffList' => $tariffList,
        ]);
    }

    /**
     * @param int $modelId ID контрагента
     * @return string
     */
    public function actionAjaxDebt(int $modelId)
    {
        try {
            $task = FsspTask::createOrGet(FsspTask::ENTITY_TYPE_CONTRACTOR, $this->contractor->id);
            if ($task->status !== FsspTask::STATUS_SUCCESS && $task->status !== FsspTask::STATUS_REQUEST) {
                $task->restart(
                    $this->contractor->companyType,
                    $this->contractor->name,
                    [
                        $this->contractor->legal_address,
                        $this->contractor->postal_address,
                        $this->contractor->actual_address
                    ]);
            }
            //Если загрузить данные не удалось, то попробовать снова
            if ($task->status === FsspTask::STATUS_ERROR) {
                if ($task->date === null || time() < strtotime($task->date ?? '-1') + FsspTask::RELOAD_TIMEOUT_ERROR) {
                    $task = null;
                } else {
                    $task->restart(
                        $this->contractor->companyType,
                        $this->contractor->name,
                        [
                            $this->contractor->legal_address,
                            $this->contractor->postal_address,
                            $this->contractor->actual_address
                        ]);
                }
            } elseif ($task->status === FsspTask::STATUS_REQUEST) {
                $task->loadTaskData($this->contractor->name, $this->contractor->companyType);
            }
            return $this->renderTab([
                'modelId' => $modelId,
                'task' => $task
            ]);
        } catch (Exception $e) {
            return $this->renderTab([], 'ajax-exception');
        }
    }


    /**
     * Очищает кэш, что приведёт к повторной загрузке данных
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionCacheClear(): Response
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $subscribe = $company->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR);
        if (!$subscribe->getIsLimitReached()) {
            $model = new DossierLog([
                'employee_id' => $employee->id,
                'company_id' => $company->id,
                'contractor_id' => $this->contractor->id,
                'created_at' => time(),
                'expired_at' => $subscribe->expired_at,
            ]);
            if ($model->save(false)) {
                Card::clearCache($this->contractor->ITN);
                Contact::clearCache($this->contractor->ITN);
                CourtArbitration::clearCache($this->contractor->ITN);
                Diff::clearCache($this->contractor->ITN);
                Enforcement::clearCache($this->contractor->ITN);
                $task = FsspTask::findByEntity(FsspTask::ENTITY_TYPE_CONTRACTOR, $this->contractor->id);
                if ($task !== null) {
                    /** @var FsspTask $task */
                    $task->restart(
                        $this->contractor->companyType,
                        $this->contractor->name,
                        [
                            $this->contractor->legal_address,
                            $this->contractor->postal_address,
                            $this->contractor->actual_address
                        ]);
                }

                return $this->redirect(Url::current(['index']));
            }
        }

        return $this->redirect(Url::current(['tariff']));
    }

    protected function currentTab()
    {
        $tab = substr(Yii::$app->request->resolve()[0], 8) ?? 'index';
        if ($tab === false) {
            $tab = 'index';
        }
        /** @noinspection PhpUnhandledExceptionInspection */
        return $tab;
    }

    protected function getModelId()
    {
        return $this->contractor->id;
    }

    protected function cardId()
    {
        return $this->contractor->ITN;
    }

    /**
     * @param int $id
     * @return Contractor
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null): Contractor
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        if ($employee === null || $employee->company === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        /* @var Contractor $model */
        $model = Contractor::find()
            ->byCompany($employee->company->id)
            ->andWhere([
                'id' => $id ?: Yii::$app->request->getQueryParam('id'),
                'is_deleted' => false
            ])->one();
        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $model;
    }

    /**
     * @inheritDoc
     * @param Action $action
     */
    public function beforeAction($action)
    {
        if ($this->_isAllowedDossier() === false) {
            /** @noinspection PhpUndefinedFieldInspection */
            $action->actionMethod = 'actionTariff';
            $action->id = 'tariff';
        } else {
            $this->view->params['dossierContractor'] = $this->contractor;
        }
        return parent::beforeAction($action);
    }

    protected static function getManager()
    {
        return new Manager(ZCHBHelperContractorLog::instance());
    }

    /**
     * @param array       $data
     * @param string|null $viewFile
     * @return string
     */
    protected function renderTab(array $data, string $viewFile = null)
    {
        if (Yii::$app->request->isAjax === false) {
            $data['isAllowedDossier'] = true;
            $data = [
                'tabFile' => '../dossier/_container',
                'dossierTab' => '../dossier/' . ($viewFile ?? $this->action->id),
                'dossierData' => $data
            ];
            /** @var Employee $employee */
            $employee = Yii::$app->user->identity;
            return $this->render('../contractor/view-customer', [
                'ioType' => $this->contractor->type,
                'tab' => 'dossier',
                'model' => $this->contractor,
                'company' => $employee->company,
                'tabData' => $data
            ]);
        } else {
            $this->layout = 'empty';
            return $this->render($viewFile ?? $this->action->id, $data);
        }
    }

    protected function contractorTitle()
    {
        return $this->contractor->getShortName(true);
    }

    private function _isAllowedDossier()
    {
        if (($allowed = ArrayHelper::getValue(Yii::$app->params, 'contractorDossierAllowed')) !== null) {
            return $allowed;
        }
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $dossierQuery = $company->getDossierLogs()->andWhere([
            'and',
            ['contractor_id' => $this->contractor->id],
            ['inn' => $this->contractor->ITN],
            ['>=', 'expired_at', time()],
        ]);
        if ($dossierQuery->exists()) {
            return true;
        }

        if ($subscribe = $company->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR)) {
            $dossierQuery = $company->getDossierLogs()->andWhere([
                'and',
                ['contractor_id' => $this->contractor->id],
                ['inn' => $this->contractor->ITN],
                ['>=', 'created_at', $subscribe->activated_at],
            ]);
            if ($dossierQuery->exists()) {
                return true;
            } elseif (!$subscribe->getIsLimitReached()) {
                $model = new DossierLog([
                    'employee_id' => $employee->id,
                    'company_id' => $company->id,
                    'contractor_id' => $this->contractor->id,
                    'inn' => $this->contractor->ITN,
                    'created_at' => time(),
                    'expired_at' => $subscribe->expired_at,
                ]);

                return $model->save(false);
            }
        }

        return false;
    }
}
