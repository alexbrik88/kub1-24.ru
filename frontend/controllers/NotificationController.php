<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2017
 * Time: 18:55
 */

namespace frontend\controllers;


use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\company\CompanyNotification;
use common\models\notification\Notification;
use frontend\components\FrontendController;
use frontend\models\NotificationSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class NotificationController
 * @package frontend\controllers
 */
class NotificationController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_AUTHENTICATED,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $month
     * @param $year
     * @return string
     */
    public function actionIndex($month, $year)
    {
        $dataProvider = (new NotificationSearch([
            'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
        ]))->searchIndex(Yii::$app->request->get(), $month, $year, \Yii::$app->user->identity->company);

        $monthInt = (int) $month;
        $prevMonth = $monthInt > 1 ? $monthInt - 1 : 12;
        $prevMonth = strlen($prevMonth) == 1 ? '0' . $prevMonth : $prevMonth;
        $prevYear = $monthInt > 1 ? $year : $year - 1;

        $nextMonth = $month < 12 ? $monthInt + 1 : 1;
        $nextMonth = strlen($nextMonth) == 1 ? '0' . $nextMonth : $nextMonth;
        $nextYear = $month < 12 ? $year : $year + 1;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'period' => [
                'month' => $month,
                'year' => $year,
                'prevMonth' => $prevMonth,
                'prevYear' => $prevYear,
                'nextMonth' => $nextMonth,
                'nextYear' => $nextYear,
            ],
        ]);
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $status = Yii::$app->request->post('status');
        if ($status !== null) {
            $companyNotification = $model->companyNotification;
            if ($companyNotification === null) {
                $companyNotification = new CompanyNotification();
                $companyNotification->company_id = Yii::$app->user->identity->company->id;
                $companyNotification->notification_id = $model->id;
                $companyNotification->status = Notification::STATUS_NOT_PASSED;
                $companyNotification->save();
            }
            $companyNotification->status = $status;
            $companyNotification->save(true, ['status']);
        }

        return true;
    }

    /**
     * @param $id
     * @return Notification
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Notification::findOne($id);
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}