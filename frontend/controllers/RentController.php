<?php

namespace frontend\controllers;

use common\components\date\DateHelper;
use common\components\filters\AjaxFilter;
use common\components\image\EasyThumbnailImage;
use common\models\Contractor;
use common\models\document\AbstractDocument;
use common\models\document\OrderDocument;
use common\models\employee\Employee;
use common\models\rent\Attribute;
use common\models\rent\Category;
use common\models\rent\AttributeValue;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use common\models\file;
use DateTime;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\RentHelper;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\RentAgreementSearch;
use frontend\models\RentChart;
use frontend\models\RentEntitySearch;
use frontend\models\RentInvoiceSearch;
use frontend\models\RentEntityInvoiceSearch;
use frontend\models\RentSearch;
use frontend\models\RentEntity;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Учёт аренды техники
 * `GET /rent?keyword={keyword}` - календарь
 * `GET /rent/list?entity={Entity::id}&contractor={Contractor::id}` - список аренд
 * `GET /rent/view?id={Entity::id}` - карточка объекта аренды
 * `POST /rent/entity` - добавить/редактировать объект аренды
 * `POST /rent/img-upload` - предзагрузка изображения на сервер
 * `POST /rent/add` - добавить аренду
 * `POST /rent/delete` - удаляет объекты аренды
 * `POST /rent/archive` - отправляет объекты аренды в архив
 * `POST /rent/rent-delete` - удаляет аренду
 * `GET /rent/list?contractor={contractorID}&entity={entityID}` - список аренд
 * `GET /rent/ajax-attribute?category={categoryID}&entity={entityID}` - часть HTML-формы, содержащая дополнительные атрибуты
 * `GET /rent/ajax-rent-item?id={RentAgreement::id}` - данные аренды для формы редактирования
 * `GET /rent/get-xls?[category=][&keyword=]` - выгрузка данных в формате XLS
 * `POST /rent/ajax-chart-data` - данные графиков
 */
class RentController extends FrontendController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'calendar',
                            'entity-list',
                            'entity-view',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Rent::VIEWER,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            permissions\Rent::ADMINISTRATOR,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'entity-archive' => ['post'],
                    'entity-delete' => ['post'],
                    'entity-retired' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => ['ajax-attribute', 'ajax-chart-data'],
            ],
        ]);
    }

    /**
     * Календарь
     * @param string $mode
     * @return string
     */
    public function actionCalendar(string $mode = RentSearch::CALENDAR_MODE_MONTH)
    {
        $this->layout = 'rent';

        if ($mode == RentSearch::CALENDAR_MODE_MONTH) {
            $calendarRange = [StatisticPeriod::dateRange('from')->format('Y-m-d'), StatisticPeriod::dateRange('to')->format('Y-m-d')];
        } else {
            $calendarRange = [date('Y-m-d'), (new DateTime())->modify('+1 month')->modify('-1 day')->format('Y-m-d')];
        }

        $searchModel = new RentSearch([
            'scenario' => RentSearch::SCENARIO_CALENDAR,
            'calendarMode' => $mode,
            'calendarRange' => $calendarRange,
            'company_id' => self::_companyId(),
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('calendar', [
            'searchModel' => $searchModel,
            'provider' => $dataProvider,
            'categoryList' => Category::flatList(),
            'entityRent' => $this->_entityRent(),
            'contractors' => Contractor::flatList(self::_companyId(), Contractor::TYPE_CUSTOMER),
            'entities' => Entity::flatList(self::_companyId()),
        ]);
    }

    /**
     * Список объектов аренды
     * @return string
     */
    public function actionEntityList()
    {
        $this->layout = 'rent';

        $searchModel = new RentEntitySearch([
            'company_id' => self::_companyId(),
            'archive' => false
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('entity-list', [
            'searchModel' => $searchModel,
            'provider' => $dataProvider,
            'entityRent' => $this->_entityRent(),
            'contractors' => Contractor::flatList(self::_companyId(), Contractor::TYPE_CUSTOMER),
            'entities' => Entity::flatList(self::_companyId()),
            'categoryList' => Category::flatList(),
            'statusList' => RentEntity::STATUS_LABELS,
            'ownershipList' => RentEntity::OWNERSHIP_TYPE_LABELS,
            'employeeList' => Employee::flatList($this->_companyId())
        ]);
    }

    /**
     * Добавление/редактирование объекта аренды
     * @param int|null $id ID объекта
     * @param bool $copy Создании копии
     * @return string
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionEntityForm(int $id = null, $copy = false)
    {
        $entity = self::_entity($id, $copy);

        if (Yii::$app->request->isPost) {
            $entity->scenario = RentEntity::SCENARIO_ATTR_IMAGE;
            $entity->load(Yii::$app->request->post());
            $entity->setOwnershipRent($entity->owner->type != Contractor::TYPE_FOUNDER);
             $entity->markImageDelete(Yii::$app->request->post('delete_pic', []));
            if ($entity->save() === true) {
                Yii::$app->session->setFlash('success', 'Объект аренды сохранён');
                return $this->redirect(['entity-view', 'id' => $entity->id]);
            }
        }

        return $this->render('entity-form', [
            'entity' => $entity,
            'categoryList' => Category::flatList(),
            'ownerList' => ArrayHelper::map(Contractor::getAllContractorList(Contractor::TYPE_SELLER), 'id', 'name'),
            'attributes' => Attribute::getArrayByCategory($entity->category_id),
            'attributeValue' => $entity->isRelationPopulated('attributeValues') ? $entity->attributeValues
                : $entity->getAttributeValues()->indexBy('attribute_id')->all(),
            'defaultContractor' => Contractor::findOne(['company_id' => self::_companyId()])
        ]);
    }

    /**
     * Карточка объекта аренды
     * @param int $id
     * @param string $tab
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEntityView(int $id, string $tab = 'rent')
    {
        $entity = self::_entity($id);

        $rentSearch = new RentAgreementSearch(Yii::$app->user->identity->company, $entity);

        $rentDataProvider = $rentSearch->search(Yii::$app->request->get());
        $rentDataProvider->pagination->pageSize = PageSize::get();

        $invoiceSearch = new RentEntityInvoiceSearch($entity);

        $invoiceDataProvider = $invoiceSearch->search(Yii::$app->request->get());
        $invoiceDataProvider->pagination->pageSize = PageSize::get();

        return $this->render('entity-view', [
            'entity' => $entity,
            'attributes' => Attribute::getArrayByCategory($entity->category_id),
            'attributeValue' => $entity->getAttributeValues()->indexBy('attribute_id')->all(),
            'contractors' => Contractor::flatList(self::_companyId(), Contractor::TYPE_CUSTOMER),
            'employees' => Employee::flatList(self::_companyId()),
            'categoryList' => Category::flatList(),
            'rentSearch' => $rentSearch,
            'rentProvider' => $rentDataProvider,
            'invoiceSearch' => $invoiceSearch,
            'invoiceProvider' => $invoiceDataProvider,
            'tabActive' => $tab,
        ]);
    }

    /**
     * Загрузка изображения во временный директорий
     * @param int|null $id ID товара
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionImgUpload(int $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $entity = self::_entity($id);
        try {
            $filename = $entity->uploadImage(Yii::$app->request->post('attr'));
        } catch (\Exception $e) {
            return ['status' => 'error', 'message' => $e->getMessage()];
        }
        return ['status' => 'success', 'src' => EasyThumbnailImage::thumbnailSrc($filename, RentEntity::THUMBNAIL_WIDTH, RentEntity::THUMBNAIL_HEIGHT)];
    }

    /**
     * Убирает объекты в архив
     * @throws Throwable
     */
    public function actionEntityArchive()
    {
        if (Entity::archiveMany(self::_companyId(), explode(',', Yii::$app->request->post('id')))) {
            Yii::$app->session->setFlash('success', 'Объект аренды отправлен в архив');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось отправить в архив объект аренды');
        }

        return $this->redirect(['index']);
    }

    /**
     * `POST /rent/delete` - удаляет объекты аренды
     * @return Response
     * @throws Throwable
     */
    public function actionEntityDelete()
    {
        if (Entity::deleteMany(self::_companyId(), explode(',', Yii::$app->request->post('id')))) {
            Yii::$app->session->setFlash('success', 'Объект аренды удален');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось удалить объект аренды');
        }

        return $this->redirect(['entity-list']);
    }

    /**
     * Выбывание объектов аренды
     * @return Response
     * @throws Throwable
     */
    public function actionEntityRetired()
    {
        $ids = Yii::$app->request->post('ids', []);
        $arrayIds = array_map(function($id){
            return intval($id);
        }, explode(',', $ids));

        if (Entity::retiredMany(self::_companyId(), $arrayIds)) {
            Yii::$app->session->setFlash('success', 'Объект аренды сделан выбывшим');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось сделать выбывшим объект аренды');
        }

        return $this->redirect(['entity-list']);
    }

    /**
     * AJAX, HTML-часть формы, содрежащая атрибуты, зависимые от категории объекта
     * @param int      $category ID категории
     * @param string   $tab      таб
     * @param int|null $entity   ID объекта аренды
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxAttribute(int $category, string $tab, $entity = null)
    {
        $this->layout = false;
        if ($entity) {
            $attributeValue = AttributeValue::find()->where(['entity_id' => $entity])->indexBy('attribute_id')->all();
        } else {
            $attributeValue = [];
        }
        $entityModel = self::_entity($entity);
        $entityModel->category_id = $category;

        return $this->render('entity-form/ajax-attribute', [
            'entity' => $entityModel,
            'attributes' => Attribute::getArrayByCategory($category, $tab),
            'attributeValue' => $attributeValue,
            'isAjax' => true,
        ]);
    }

    /**
     * Экспорт аренд в XLS
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;
        $searchModel = new RentSearch([
            'scenario' => RentSearch::SCENARIO_LIST,
            'company_id' => self::_companyId()
        ]);

        return RentHelper::xlsExport($searchModel->search(Yii::$app->request->get())->query->all());
    }

    /**
     * Обновление графиков
     * @return string
     */
    public function actionAjaxChartData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $chart = Yii::$app->request->post('chart');
        $period = Yii::$app->request->post('period');
        $objectType = Yii::$app->request->post('object-type');
        $offset = Yii::$app->request->post('offset');

        return $this->renderPartial('chart/chart', [
            'model' => new RentChart($company),
            'customPeriod' => $period,
            'customOffset' => $offset,
            'customObjectType' => $objectType,
        ]);
    }

    /**
     * @param int|null $id
     * @param bool $copy
     * @param bool $throwException
     * @return RentEntity
     * @throws NotFoundHttpException
     */
    private static function _entity($id = null, $copy = false, $throwException = true): RentEntity
    {
        if (!$id) {
            $entity = RentEntity::instanceEntity(self::_companyId(), Yii::$app->user->id);
            $entity->owner_id = self::_contractor()->id;
            return $entity;
        }

        $entity = RentEntity::findOne(['id' => $id, 'company_id' => self::_companyId()]);
        if ($entity === null && $throwException) {
            throw new NotFoundHttpException('Объект недвижимости #' . $id . ' не существует');
        }

        if ($copy) {
            $entity = clone $entity;
            $entity->title .= " (копия)";
        }

        return $entity;
    }

    /**
     * @param int|null $id
     * @param bool $copy
     * @param bool $throwException
     * @return RentAgreement
     * @throws NotFoundHttpException
     */
    private static function _entityRent($id = null, $copy = false, $throwException = true)
    {
        if (!$id) {
            return new RentAgreement([
                'document_author_id' => Yii::$app->user->id,
                'company_id' => Yii::$app->user->identity->company->id,
                'document_date' => date('Y-m-d'),
                'date_begin' => date('Y-m-d'),
                'date_end' => date('Y-12-31'),
                'document_number' => RentAgreement::getNextNumber(Yii::$app->user->identity->company),
            ]);
        }

        $entityRent = RentAgreement::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($entityRent === null && $throwException) {
            throw new NotFoundHttpException('Договор аренды #' . $id . ' не существует');
        }

        if ($copy) {
            $entityRent->document_date = date(DateHelper::FORMAT_DATE);
        }

        return $copy && $entityRent ? clone $entityRent : $entityRent;
    }

    /**
     * @return Contractor|null
     */
    private static function _contractor()
    {
        return Contractor::findOne(['company_id' => self::_companyId()]);
    }

    /**
     * @return int
     */
    private static function _companyId(): int
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        return $employee->company->id;
    }
}