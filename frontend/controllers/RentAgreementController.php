<?php

namespace frontend\controllers;

use Yii;
use common\models\file;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\RentAgreementCashSearch;
use frontend\models\RentAgreementInvoiceSearch;
use frontend\models\RentAgreementForm;
use frontend\models\RentAgreementItemForm;
use frontend\models\RentAgreementSearch;
use frontend\models\RentSearch;
use frontend\rbac\permissions;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Договоры аренды
 */
class RentAgreementController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Rent::VIEWER,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            permissions\Rent::ADMINISTRATOR,
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'file-get' => [
                'class' => file\actions\GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function($model) {
                    return $model->getUploadPath();
                },
            ],
            'file-list' => [
                'class' => file\actions\FileListAction::className(),
                'model' => RentAgreement::class,
                'fileLinkCallback' => function (file\File $file, $ownerModel) {
                    return Url::to([
                        'file-get',
                        'type' => $ownerModel->type,
                        'id' => $ownerModel->id,
                        'file-id' => $file->id,
                    ]);
                },
            ],
            'file-delete' => [
                'class' => file\actions\FileDeleteAction::className(),
                'model' => RentAgreement::class,
            ],
            'file-upload' => [
                'class' => file\actions\FileUploadAction::className(),
                'model' => RentAgreement::class,
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . RentAgreement::$uploadDirectory,
            ],
            'scan-bind' => [
                'class' => \frontend\modules\documents\components\actions\ScanBindAction::className(),
                'model' => RentAgreement::class,
            ],
            'scan-list' => [
                'class' => \frontend\modules\documents\components\actions\ScanListAction::className(),
                'model' => RentAgreement::class,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ]
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'rent';

        $company = Yii::$app->user->identity->company;
        $searchModel = new RentAgreementSearch($company);

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $invoiceSearchModel = new RentAgreementInvoiceSearch($model);
        $invoiceDataProvider = $invoiceSearchModel->search(Yii::$app->request->get());
        $cashSearchModel = new RentAgreementCashSearch($model);
        $cashDataProvider = $cashSearchModel->search(Yii::$app->request->get());

        return $this->render('view', [
            'model' => $model,
            'invoiceSearchModel' => $invoiceSearchModel,
            'invoiceDataProvider' => $invoiceDataProvider,
            'cashSearchModel' => $cashSearchModel,
            'cashDataProvider' => $cashDataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate($clone = null)
    {
        $employeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
        $employeeCompany->populateRelation('company', Yii::$app->user->identity->company);
        $employeeCompany->populateRelation('employee', Yii::$app->user->identity);

        $model = new RentAgreementForm($employeeCompany, $clone ? (clone $this->findModel($clone)) : null);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->rentAgreement->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionUpdate($id)
    {
        $employeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
        $employeeCompany->populateRelation('company', Yii::$app->user->identity->company);
        $employeeCompany->populateRelation('employee', Yii::$app->user->identity);

        $model = new RentAgreementForm($employeeCompany, $this->findModel($id));

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->rentAgreement->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @return string
     */
    public function actionManyDelete()
    {
        $idArray = (array) Yii::$app->request->post('RentAgreement');

        foreach ((array) $idArray as $id) {
            $this->findModel($id)->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionCreateInvoice($id)
    {
        $model = $this->findModel($id);

        if ($model->createInvoice()) {
            $this->redirect(['/documents/invoice/update', 'type' => $model->invoice->type, 'id' => $model->invoice->id]);
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать счет');
            $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * @return string
     */
    public function actionItem($id, $hasDiscount)
    {
        $entity = $this->findEntity($id);
        $item = new RentAgreementItemForm();
        $item->rent_entity_id = $entity->id;
        $item->price = $entity->price();

        return $this->renderPartial('_form_table_row', [
            'item' => $item,
            'hasDiscount' => $hasDiscount,
            'key' => 1,
        ]);
    }

    /**
     * @return string
     */
    public function actionEntityList($term = null, $page = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;

        $results = [];
        $limit = 100;
        $page = max(1, (int) $page);
        $offset = $limit * ($page-1);
        $paginationMore = false;

        $query = Entity::find()->andWhere([
            'company_id' => $company->id,
            'deleted' => false,
            'archive' => false,
        ])->andFilterWhere([
            'like', 'title', $term,
        ])->andFilterWhere([
            'not', ['id' => Yii::$app->request->get('excludeIds')],
        ])->offset($offset)->limit($limit + 1);

        $results = $query->all();

        if (count($results) == $limit + 1) {
            array_pop($results);
            $paginationMore = true;
        }

        return [
            'results' => $results,
            'pagination' => [
                'more' => $paginationMore,
            ],
        ];
    }

    /**
     * @return string
     */
    private function findEntity($id)
    {
        $company = Yii::$app->user->identity->company;

        $model = Entity::findOne([
            'id' => $id,
            'company_id' => $company->id,
            'deleted' => false,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * @return string
     */
    private function findModel($id)
    {
        $company = Yii::$app->user->identity->company;

        $model = RentAgreement::findOne([
            'id' => $id,
            'company_id' => $company->id,
            'deleted' => false,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}
