<?php

namespace frontend\controllers;

use common\components\moysklad\converter\MoyskladConverter;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use common\components\moysklad\MoyskladImport;
use common\components\moysklad\parser\MoyskladParser;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use yii\helpers\ArrayHelper;
use frontend\rbac\permissions;
use yii\web\UploadedFile;
use common\components\moysklad\uploader\MoyskladUploader;

class ProductAjaxController extends FrontendController {

    const MAX_EXECUTION_TIME_S = 900;
    const MEMORY_LIMIT_MB = 1024;

    const ERROR_MESSAGE = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';
    const PROCESS_MESSAGE = 'Импорт был запущен ранее. Повторите действие через %s мин.';
    const FILE_UPLOADS_SUCCESS_MESSAGE = 'Файл успешно загружен';
    const FILE_UPLOADS_ERROR_MESSAGE = 'К сожалению, не удалось загрузить файл. Повторите действие позже или обратитесь в службу поддержки.';
    const FILE_PARSING_SUCCESS_MESSAGE = 'Парсинг файла успешно завершен';
    const FILE_PARSING_ERROR_MESSAGE = 'К сожалению, не удалось завершить парсинг файл. Повторите действие позже или обратитесь в службу поддержки.';
    const FILE_IMPORT_SUCCESS_MESSAGE = 'Импорт успешно завершен';
    const FILE_IMPORT_ERROR_MESSAGE = 'К сожалению, не удалось завершить импорт. Повторите действие позже или обратитесь в службу поддержки.';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['moysklad-upload-file', 'moysklad-parse-file', 'moysklad-import', 'moysklad-progress'],
                        'allow' => true,
                        'roles' => [permissions\Product::CREATE],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return MoyskladImport|null
     */
    public function getUnfinishedImport()
    {
        /** @var Employee $employee */
        /** @var Company $company */
        /** @var MoyskladImport $import */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $import = MoyskladImport::find()
            ->where(['company_id' => $company->id])
            ->andWhere(['>', 'loading_step', 0])
            ->orderBy(['created_at' => SORT_DESC])
            ->one();

        if ($import)
            return (time() - $import->created_at <= self::MAX_EXECUTION_TIME_S) ? $import : null;

        return null;
    }

    public function actionMoyskladUploadFile()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var Employee $employee */
        /** @var Company $company */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        // prevent parallel upload //////////////////////////////////////////////////////////////
        if ($unfinishedImport = $this->getUnfinishedImport()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $elapsedTimeMin = floor(1 / 60 * (self::MAX_EXECUTION_TIME_S - (time() - $unfinishedImport->created_at)));
            return [
                'result' => false,
                'message' => sprintf(self::PROCESS_MESSAGE, $elapsedTimeMin)
            ];
        }
        //////////////////////////////////////////////////////////////////////////////////////

        $file = UploadedFile::getInstanceByName('uploadfile');
        if ($file && is_file($file->tempName)) {
            $uploader = new MoyskladUploader($company, $employee);
            if ($uploader->saveTmpFile($file->tempName)) {
                return [
                    'result' => true,
                    'message' => self::FILE_UPLOADS_SUCCESS_MESSAGE,
                    'step_num' => MoyskladImport::STEP_PARSING_FILE,
                    'step_description' => 'Парсинг файла...',
                    'html_statistics' => (new MoyskladImport)->getHtmlStatistics(),
                ];
            } else {
                return [
                    'result' => false,
                    'message' => self::FILE_UPLOADS_ERROR_MESSAGE,
                ];
            }
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            '_info' => 'File not uploaded successfully'
        ];
    }

    public function actionMoyskladParseFile()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        ini_set('max_execution_time', self::MAX_EXECUTION_TIME_S);
        ini_set('memory_limit', self::MEMORY_LIMIT_MB.'M');

        /** @var Employee $employee */
        /** @var Company $company */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $uploader = new MoyskladUploader($company, $employee);
        $filename = $uploader->getUploadsDir() . DIRECTORY_SEPARATOR . $uploader->getTmpFilename();

        if (is_file($filename)) {

            $model = new MoyskladImport([
                'company_id' => $company->id,
                'employee_id' => $employee->id,
                'store_id' => $company->mainStore->id,
                'document_format_id' => MoyskladImport::DOCUMENT_FORMAT_UPD,
                'loading_step' => MoyskladImport::STEP_PREPARING_OBJECTS,
                'created_at' => time()
            ]);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $parser = new MoyskladParser($model);
                $parser->stat = new MoyskladImportStatistics($model);

                try {

                    $_time = microtime(true);

                    // parse XML
                    $parser->open($filename);
                    $parser->parse();
                    $parser->close();
                    $parser->stat->update();

                    return [
                        'result' => true,
                        'import_id' => $model->id,
                        'message' => self::FILE_PARSING_SUCCESS_MESSAGE,
                        'step_num' => $model->loading_step,
                        'step_description' => $model->getStepDescription(),
                        'html_statistics' => $model->getHtmlStatistics(),
                        'timeUsage' => 'Time usage: '.round(microtime(true) - $_time, 2).'s',
                        'memoryUsage' => 'Memory: ' . round(memory_get_usage() / 1024 / 1024, 2) . 'M'
                    ];

                } catch (\Throwable $e) {

                    $model->progressStep($model::STEP_END_WITH_ERROR);
                    $model->setRuntimeError($e);

                    return [
                        'result' => false,
                        'message' => self::FILE_PARSING_ERROR_MESSAGE,
                        'e' => $model->getRuntimeErrorMessage($e)
                    ];
                }
            }
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            '_info' => 'File not found'
        ];
    }
    
    public function actionMoyskladProgress($import_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = MoyskladImport::findOne([
            'id' => $import_id,
            'company_id' => $company->id,
            'employee_id' => $user->id,
        ]);

        if ($model) {
            return [
                'result' => true,
                'step_num' => $model->loading_step,
                'step_description' => $model->getStepDescription(),
                'html_statistics' => $model->getHtmlStatistics(),
                'html_validation_objects' => $model->getHtmlValidationObjects(),
                'html_validation_documents' => $model->getHtmlValidationDocuments(),
            ];
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            '_info' => 'Import model not found'
        ];
    }

    public function actionMoyskladImport($import_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        ini_set('max_execution_time', self::MAX_EXECUTION_TIME_S);
        ini_set('memory_limit', self::MEMORY_LIMIT_MB.'M');

        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = MoyskladImport::findOne([
            'id' => $import_id,
            'company_id' => $company->id,
            'employee_id' => $user->id,
        ]);

        if ($model) {

            $converter = new MoyskladConverter($model);
            $converter->stat = new MoyskladImportStatistics($model);
            $converter->validation = new MoyskladValidationErrors($model);

            try {

                $_time = microtime(true);

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 0;')->execute();
                
                $model->progressStep($model::STEP_PREPARING_OBJECTS);
                // $model->preparingObjects();

                // save dicts
                $model->progressStep($model::STEP_CONTRACTORS);
                $converter->insertContractors();
                $converter->stat->update();

                $model->progressStep($model::STEP_PRODUCTS);
                //$converter->insertStores();
                $converter->insertProductGroups();
                $converter->insertProducts();
                $converter->stat->update();

                // save docs
                $model->progressStep($model::STEP_INVOICES);
                $converter->insertInvoices();
                $converter->insertInvoicesGeneratedFromUpds();
                $converter->stat->update();

                $model->progressStep($model::STEP_UPDS);
                $converter->insertUpds();
                $converter->stat->update();

                // todo: $model->progressStep(...)
                $converter->insertPaymentOrders();
                $converter->stat->update();

                $model->progressStep($model::STEP_FACTURES);
                $converter->insertInvoiceFactures();
                $converter->stat->update();

                // contractor types
                $converter->updateContractorsTypes();

                // products prices
                $converter->updateProductsPrices();

                Yii::$app->db->createCommand('SET FOREIGN_KEY_CHECKS = 1;')->execute();

                $model->progressStep($model::STEP_END);

                $uploader = new MoyskladUploader($company, $user);
                $uploader->removeTmpFile();

                return [
                    'result' => true,
                    'message' => self::FILE_IMPORT_SUCCESS_MESSAGE,
                    'step_num' => $model->loading_step,
                    'step_description' => $model->getStepDescription(),
                    'html_statistics' => $model->getHtmlStatistics(),
                    'html_validation_objects' => $model->getHtmlValidationObjects(),
                    'html_validation_documents' => $model->getHtmlValidationDocuments(),
                    'timeUsage' => 'Time usage: '.round(microtime(true) - $_time, 2).'s',
                    'memoryUsage' => 'Memory: ' . round(memory_get_usage() / 1024 / 1024, 2) . 'M'
                ];

            } catch (\Throwable $e) {

                $model->progressStep($model::STEP_END_WITH_ERROR);
                $model->setRuntimeError($e);

                return [
                    'result' => false,
                    'message' => self::FILE_IMPORT_ERROR_MESSAGE,
                    'e' => $model->getRuntimeErrorMessage($e)
                ];
            }
        }

        return [
            'result' => false,
            'message' => self::ERROR_MESSAGE,
            '_info' => 'Import model not found #2'
        ];
    }    
}