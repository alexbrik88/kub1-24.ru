<?php

namespace frontend\controllers;

use yii\base\Configurable;
use yii\web\Response;

interface ActionInterface extends Configurable
{
    /**
     * @return Response
     * @throws
     */
    public function run(): Response;
}
