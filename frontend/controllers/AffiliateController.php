<?php
namespace frontend\controllers;

use common\components\helpers\Html;
use common\models\company\CompanyAffiliateLeadRewardsSearch;
use common\models\company\CompanyAffiliateLeads;
use common\models\company\CompanyAffiliateLeadsSearch;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyAffiliateLinkSearch;
use common\models\company\CompanyAffiliateSearch;
use common\models\company\CompanyInvoiceSearch;
use common\models\employee\Employee;
use common\models\service\ServiceModule;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\company\CompanyInviteForm;
use frontend\rbac\UserRole;
use Throwable;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AffiliateController
 * @package frontend\controllers
 */
class AffiliateController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_PARTNER_RELATIONS_MANAGER
                        ]
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'img-delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @param Action $action
     * @return bool|Response
     * @throws BadRequestHttpException
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * @param null $tab
     * @return string|Response
     */
    public function actionIndex($tab = null)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $offer = $company->is_approve_offer;

        if (empty($offer)) {
            return $this->render('_partials/offer-agreement');
        }

        switch ($tab) {
            case 'reward-payments':
                $tabData = [
                    'tabFile' => '_partials/tabs/null'
                ];
                break;
            case 'invoice':
                $searchModel = new CompanyInvoiceSearch();
                $dataProvider = $searchModel->search(array_merge(['company_id' => $company->id],
                    Yii::$app->request->queryParams));
                $tabData = [
                    'tabFile' => '_partials/tabs/invoice',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ];
                break;
            case 'affiliate-link':
                CompanyAffiliateLink::generateAffiliateLinks($company->id);

                $searchModel = new CompanyAffiliateLinkSearch();
                $dataProvider = $searchModel->search(array_merge(['company_id' => $company->id],
                    Yii::$app->request->queryParams));
                $tabData = [
                    'tabFile' => '_partials/tabs/affiliateLink',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ];
                break;
            case 'leads':
                $searchModel = new CompanyAffiliateLeadsSearch([
                    'company_id' => $company->id,
                ]);
                $dataProvider = $searchModel->search(array_merge(['company_id' => $company->id],
                    Yii::$app->request->queryParams));
                $tabData = [
                    'tabFile' => '_partials/tabs/leads',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ];
                break;
            case 'rewards':
                $searchModel = new CompanyAffiliateLeadRewardsSearch(['company_id' => $company->id]);
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                $tabData = [
                    'tabFile' => '_partials/tabs/rewards',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ];
                break;
            default:
                $searchModel = new CompanyAffiliateSearch();
                $dataProvider = $searchModel->search(array_merge(['company_id' => $company->id],
                    Yii::$app->request->queryParams));
                $tabData = [
                    'tabFile' => '_partials/tabs/clients',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                ];
                break;

        }

        isset($dataProvider) && $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'tab' => $tab,
            'tabData' => $tabData,
        ]);
    }


    public function actionOfferApprovement()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $offer = (int)Yii::$app->request->post('offer', null);

        try {
            $company->updateAttributes([
                'is_approve_offer' => $offer,
            ]);
        } catch (Exception $e) {
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    /**
     * @return false|string
     */
    public function actionEditAffiliateLink()
    {
        $id = (int)Yii::$app->request->post('id');
        $name = Yii::$app->request->post('name');

        $model = CompanyAffiliateLink::findOne(['id' => $id]);

        $model->name = Html::encode($name);

        if ($model->save(true)) {
            return json_encode(['success' => 'true']);
        }

        return json_encode(['success' => 'false']);
    }

    /**
     * @return array
     */
    public function actionLeadValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CompanyAffiliateLeads();

        if ($model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }

        return [];
    }

    public function actionLeadCreate()
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = new CompanyAffiliateLeads([
            'company_id' => $company->id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Клиент добавлен.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_partials/_edit_leads_form', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionLeadUpdate()
    {
        $id = (int)Yii::$app->request->post('id', null);

        $model = $this->findModelAffiliateLead($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Информация о клиенте обновлена.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_partials/_edit_leads_form', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return false|string
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionLeadDelete($id)
    {
        $model = $this->findModelAffiliateLead((int)$id);

        if (!$model->delete()) {
            Yii::$app->session->setFlash('success', 'Что-то пошло не так. Перегрузите страницу '
                . 'и попробуйте снова');
        }

        Yii::$app->session->setFlash('success', 'Информация о клиенте удалена.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array
     */
    public function actionInviteLeadFormValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CompanyInviteForm();

        if ($model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }

        return [];
    }

    public function actionGetInviteLeadForm()
    {
        $id = (int)Yii::$app->request->post('id', null);

        $modelLead = $this->findModelAffiliateLead($id);

        $model = new CompanyInviteForm();
        $model->load($modelLead->toArray());

        return $this->render('_partials/send-invite-panel', [
            'model' => $model,
        ]);
    }

    public function actionSendInviteLeadForm()
    {
        $model = new CompanyInviteForm();

        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            Yii::$app->session->setFlash('success', 'Приглашение отправлено.');
        };

        Yii::$app->session->setFlash('success', 'Отправка завершилось неудачей.');

        $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCustomLinkValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CompanyAffiliateLink();

        if ($model->load(Yii::$app->request->post())) {
            return ActiveForm::validate($model);
        }

        return [];
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionCreateCustomLink()
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = new CompanyAffiliateLink();

        $params = Yii::$app->request->post();

        if (!empty($params)) {
            CompanyAffiliateLink::generateCustomAffiliateLink($company->id, Yii::$app->request->post('CompanyAffiliateLink'));

            Yii::$app->session->setFlash('success', 'Ссылка создана.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_partials/_edit_links_form', [
            'model' => $model,
            'products' => ServiceModule::$serviceModuleLabel,
        ]);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionUpdateAffiliateLink()
    {
        $id = (int)Yii::$app->request->post('id', null);

        $model = $this->findModelAffiliateLink($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Ссылка обновлена.');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('_partials/_edit_links_form', [
            'model' => $model,
            'products' => ServiceModule::$serviceModuleLabel,
        ]);
    }

    /**
     * @param null $id
     * @return array|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModelAffiliateLead($id = null)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $model = CompanyAffiliateLeads::find()
            ->andWhere([
                'id' => $id,
                'company_id' => $company->id,
            ])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $model;
    }

    /**
     * @param null $id
     * @return array|ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModelAffiliateLink($id = null)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = CompanyAffiliateLink::find()
            ->andWhere([
                'id' => $id,
                'company_id' => $company->id,
            ])
            ->one();

        if (!$model) {
            throw new NotFoundHttpException('The requested model does not exist.');
        }

        return $model;
    }

}
