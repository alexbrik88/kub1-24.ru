<?php
namespace frontend\controllers;

use common\models\company\CompanyIndustry;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\rbac\permissions;
use frontend\components\FrontendController;
use common\models\cash\Cashbox;
use common\models\Company;
use common\models\companyStructure\NewSalePointTypeForm;
use common\models\companyStructure\SalePoint;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Store;
use common\models\TimeZone;

class CompanyStructureController extends FrontendController
{
    public static $viewPath = '/company/form/modal_company_structure/';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Company::INDEX, permissions\Company::PROFILE],
                    ],
                    [
                        'actions' => ['visit-card', 'send-visit-card', 'document-print'],
                        'allow' => true,
                        'roles' => [permissions\Company::VISIT_CARD],
                    ],
                    [
                        'actions' => [
                            'add-sale-point',
                            'add-store',
                            'add-cashbox',
                            'add-employee',
                            'update-sale-point',
                            'update-cashbox',
                            'delete-sale-point',
                            'delete-cashbox',
                            'send-proposal',
                            // industry
                            'add-company-industry',
                            'update-company-industry',
                            'delete-company-industry',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (!Yii::$app->request->isAjax) {
                $this->refresh();
            }

            return true;
        }

        return false;
    }

    public function actionAddSalePoint()
    {
        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $typeId = Yii::$app->request->get('type_id', 1);
        $cashboxes = $employers = [];
        $model = new SalePoint([
            'company_id' => $company->id,
            'responsible_employee_id' => $user->id,
            'type_id' => $typeId,
            'online_payment_type_id' => $company->onlinePaymentTypes ? $company->onlinePaymentTypes[0]->id : null
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $cashboxesIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'cashboxes');
            $employersIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'employers');
            $cashboxes = Cashbox::find()->where(['company_id' => $company->id, 'id' => $cashboxesIds])->all();
            $employers = Employee::find()->where(['company_id' => $company->id, 'id' => $employersIds])->all();

            if ($model->isCashboxRequired() && empty($cashboxes)) {
                $model->addError('cashboxes', 'Необходимо выбрать кассу');
            }

            if ($model->validate()) {

                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $cashboxes, $employers) {

                    $model->unlinkAll('cashboxes', true);
                    $model->unlinkAll('employers', true);

                    if ($model->save()) {

                        $cashboxes = array_slice($cashboxes, 0, SalePoint::MAX_CASHBOXES);
                        foreach ($cashboxes as $val) { $model->link('cashboxes', $val); }
                        foreach ($employers as $val) { $model->link('employers', $val); }

                        LogHelper::log($model, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_CREATE);

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });
            }
        }

        return $this->renderAjax(self::$viewPath . '_modal_sale_point', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'newCashboxes' => $cashboxes,
            'newEmployers' => $employers,
            'isUpdate' => false,
        ]);
    }

    public function actionAddStore()
    {
        $company = Yii::$app->user->identity->company;
        $model = new Store([
            'company_id' => $company->id,
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax(self::$viewPath . '_modal_store', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionAddCashbox()
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $model = new Cashbox([
            'company_id' => $company->id,
            'is_accounting' => true,
            'ofd_type_id' => $company->ofdTypes ? $company->ofdTypes[0] : null
        ]);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax(self::$viewPath . '_modal_cashbox', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionAddEmployee()
    {
        $company = Yii::$app->user->identity->company;
        $isSaved = false;

        $model = new EmployeeCompany([
            'scenario' => EmployeeCompany::SCENARIO_CREATE_SALE_POINT,
            'send_email' => false,
            'company_id' => $company->id,
            'employee_role_id' => EmployeeRole::ROLE_EMPLOYEE,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'is_product_admin' => false,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {

            $isSaved = true;

        }

        return $this->renderAjax(self::$viewPath . '_modal_employee', [
            'model' => $model,
            'isSaved' => $isSaved
        ]);
    }

    public function actionUpdateSalePoint($id)
    {
        $isUpdate = false;
        $isSaved = false;
        $company = Yii::$app->user->identity->company;

        $cashboxes = $employers = [];
        $model = $this->findSalePoint($id, $company->id);

        if ($model->load(Yii::$app->request->post())) {

            $isUpdate = true;
            $cashboxesIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'cashboxes');
            $employersIds = ArrayHelper::getValue(Yii::$app->request->post('SalePoint'), 'employers');
            $cashboxes = Cashbox::find()->where(['company_id' => $company->id, 'id' => $cashboxesIds])->all();
            $employers = Employee::find()->where(['company_id' => $company->id, 'id' => $employersIds])->all();

            if ($model->isCashboxRequired() && empty($cashboxes)) {
                $model->addError('cashboxes', 'Необходимо выбрать кассу');
                $isValidate = false;
            } else {
                $isValidate = $model->validate();
            }

            if ($isValidate) {

                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $cashboxes, $employers) {

                    $model->unlinkAll('cashboxes', true);
                    $model->unlinkAll('employers', true);

                    if ($model->save()) {

                        $cashboxes = array_slice($cashboxes, 0, SalePoint::MAX_CASHBOXES);
                        foreach ($cashboxes as $val) { $model->link('cashboxes', $val); }
                        foreach ($employers as $val) { $model->link('employers', $val); }

                        Yii::$app->session->setFlash('success', 'Точка продаж обновлена');
                        LogHelper::log($model, LogEntityType::TYPE_SALE_POINT, LogEvent::LOG_EVENT_UPDATE);

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });
            }
        }

        return $this->renderAjax(self::$viewPath . '_modal_sale_point', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'isUpdate' => $isUpdate,
            'newCashboxes' => $cashboxes,
            'newEmployers' => $employers,
        ]);
    }

    public function actionUpdateCashbox($id)
    {
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;
        $model = $this->findCashbox($id, $company->id);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
        }

        return $this->renderAjax(self::$viewPath . '_modal_cashbox', [
            'model' => $model,
            'company' => $company,
            'wasUpdated' => true
        ]);
    }

    public function actionDeleteSalePoint($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = $this->findSalePoint($id, $company->id);

        if (LogHelper::delete($model, LogEntityType::TYPE_SALE_POINT)) {
            return ['result' => true];
        }

        Yii::$app->session->setFlash('error', "Не удалось удалить отдел");

        return ['result' => false];
    }

    public function actionDeleteCashbox($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = $this->findCashbox($id, $company->id);

        if ($model->delete()) {
            return ['result' => true];
        }

        return ['result' => false];
    }

    public function actionSendProposal()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $model = new NewSalePointTypeForm();
        $isSended = false;
        if (Yii::$app->request->method == 'POST') {

            $model->description = ArrayHelper::getValue(Yii::$app->request->post('NewSalePointTypeForm'), 'description');

            if ($model->validate()) {
                \Yii::$app->mailer->compose([
                    'html' => 'system/new-sale-point-type/html',
                    'text' => 'system/new-sale-point-type/text',
                ], [
                    'subject' => 'Предложение добавить структуру компании',
                    'form' => $model,
                    'userFIO' => $employee->getFio(),
                    'email' => $company->email,
                    'phone' => $company->phone,
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo(\Yii::$app->params['emailList']['support'])
                    ->setSubject('Предложение добавить структуру компании')
                    ->send();

                $isSended = true;
            }
        }

        return $this->renderAjax(self::$viewPath . '_modal_proposal', [
            'model' => $model,
            'company' => $company,
            'isSended' => $isSended
        ]);
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findSalePoint($id, $company_id)
    {
        $model = SalePoint::find()
            ->andWhere([SalePoint::tableName() . '.id' => $id,])
            ->andWhere(['company_id' => $company_id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Отдел не найден');
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findCashbox($id, $company_id)
    {
        $model = Cashbox::find()
            ->andWhere([Cashbox::tableName() . '.id' => $id,])
            ->andWhere(['company_id' => $company_id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Касса не найдена');
    }

    // INDUSTRY

    /**
     * @return string
     * @throws \Throwable
     */
    public function actionAddCompanyIndustry()
    {
        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;
        $model = new CompanyIndustry([
            'company_id' => $company->id,
            'employee_id' => $user->id
        ]);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $isSaved = LogHelper::save($model, LogEntityType::TYPE_COMPANY_INDUSTRY, LogEvent::LOG_EVENT_CREATE);
            }
        }

        return $this->renderAjax(self::$viewPath . '_modal_company_industry', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'isSavedNewRecord' => $isSaved
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdateCompanyIndustry($id)
    {
        $isSaved = false;
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;
        $model = $this->findCompanyIndustry($id, $company->id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $isSaved = LogHelper::save($model, LogEntityType::TYPE_COMPANY_INDUSTRY, LogEvent::LOG_EVENT_UPDATE);
            }
        }

        return $this->renderAjax(self::$viewPath . '_modal_company_industry', [
            'model' => $model,
            'company' => $company,
            'isSaved' => $isSaved,
            'isSavedNewRecord' => false
        ]);
    }

    public function actionDeleteCompanyIndustry($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $model = $this->findCompanyIndustry($id, $company->id);

        if (LogHelper::delete($model, LogEntityType::TYPE_COMPANY_INDUSTRY)) {
            return ['result' => true];
        }

        Yii::$app->session->setFlash('error', "Не удалось удалить направление");

        return ['result' => false];
    }

    /**
     * @param $id
     * @return array|\yii\db\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findCompanyIndustry($id, $company_id)
    {
        $model = CompanyIndustry::find()
            ->andWhere([CompanyIndustry::tableName() . '.id' => $id,])
            ->andWhere(['company_id' => $company_id])
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Направление не найдено');
    }

}