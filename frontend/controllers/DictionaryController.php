<?php
namespace frontend\controllers;

use common\components\AddressDictionaryHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\Ifns;
use common\models\address\AddressDictionary as Fias;
use common\models\dictionary\address\AddressDictionary;
use common\models\dictionary\bik\BikDictionary;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\rbac\permissions;
use Yii;
use yii\web\Controller;
use yii\helpers\Html;

/**
 * Dictionary controller
 */
class DictionaryController extends Controller
{
    /**
     * @var string
     */
    public $layout = '//view/layouts/clear';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow only AJAX requests using the post Method
                    [
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'contractor-dropdown' => [
                'class' => 'frontend\components\ContractorDropdownAction',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionBik($q = '')
    {
        $bikArray = [];
        $q = trim($q);
        if ($q !== '') {
            $bikArray = BikDictionary::find()
                ->byActive()
                ->byBikFilter($q)
                ->limit(20)
                ->asArray()
                ->all();
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $bikArray;
    }

    /**
     * @param $type
     * @param $q
     * @return array
     */
    public function actionAddress($type, $q)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searcher = new AddressDictionary($type, $q, Yii::$app->request->get());

        return (array) $searcher->search();
    }

    /**
     * @param null $companyId
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionEmployee($companyId = null, $q)
    {
        $companyId = $companyId ? $companyId : Yii::$app->user->identity->company->id;
        $companyIdArray = Yii::$app->user->identity->getCompanies()->andWhere([
            '!=', 'id', $companyId
        ])->select('id')->column();
        $employeeArray = [];
        $q = trim($q);
        if ($q !== '') {
            $employeeArray = Employee::find()->alias('employee')
                ->distinct()
                ->joinWith('employeeCompany employee_company', false)
                ->leftJoin([
                    'employee_company_exists' => EmployeeCompany::tableName(),
                ], '{{employee_company_exists}}.[[employee_id]] = {{employee}}.[[id]] AND {{employee_company_exists}}.[[company_id]] = :company', [
                    ':company' => $companyId,
                ])
                ->select(['employee.email', 'employee_company.*', 'DATE_FORMAT({{employee_company}}.[[date_hiring]], "%d.%m.%Y") as date_hiring_format',
                    'DATE_FORMAT({{employee_company}}.[[birthday]], "%d.%m.%Y") as birthday_format',
                    'DATE_FORMAT({{employee_company}}.[[date_dismissal]], "%d.%m.%Y") as date_dismissal_format',])
                ->andWhere(['employee_company_exists.company_id' => null])
                ->andWhere([
                    'or',
                    ['employee_company.company_id' => $companyIdArray],
                    ['employee.id' => Employee::SUPPORT_KUB_EMPLOYEE_ID],
                ])
                ->andWhere(['employee.email' => $q])
                ->byIsDeleted(Employee::NOT_DELETED)
                ->limit(1)
                ->asArray()
                ->all();
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $employeeArray;
    }

    /**
     * @param $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionIfns()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $q = ltrim(Yii::$app->request->get('q'), '0');
        if ($q !== null) {
            $query = Ifns::find()
                ->select('ga, gb, g1, g2, g4, g6, g7, g8, g9, g11')
                ->where(['like', 'ga', $q.'%', false])
                ->limit(10);

            $result = $query->all();

            array_walk($result, function (&$data) {
                $data->g1 = implode(', ', array_filter(explode(',', $data->g1)));
            });

            return $result;
        }

        return [];
    }

    /**
     * @return array
     */
    public function actionFias($q = null, $level, $guid = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (/*empty($q) || */ empty($level) || (empty($guid) && $level != 1)) {
            return [];
        }

        $query = Fias::find()
            ->select(['AOGUID', 'FULLNAME'])
            ->where(['AOLEVEL' => $level, 'ACTSTATUS' => 1])
            ->andWhere(['like', 'FULLNAME', $q . '%', false])
            ->orderBy(['FORMALNAME' => SORT_ASC])
            ->limit(100);

        if ($guid && $level != 1) {
            $query->andWhere(['PARENTGUID' => $guid]);
        }

        return $query->all();
    }

    /**
     * @return array
     */
    public function actionFiasItem($guid = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return ['result' => $guid ? Fias::findOne(['AOGUID' => $guid, 'ACTSTATUS' => 1]) : null];
    }

    /**
     * @return array
     */
    public function actionContractorDropdownOld($type, $term = null, $q = null, $page = null, $_type = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX, ['type' => $type])) {
            return [
                'results' => [],
                'pagination' => [
                    'more' => false,
                ],
            ];
        }

        $results1 = [];
        $results2 = [];
        $limit = 100;
        $search = $term ?? $q;
        $page = max(1, (int) $page);
        $offset = $limit * ($page-1);
        $paginationMore = false;

        if ($cid = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id'])) {
            $excludeIds = Yii::$app->getRequest()->get('excludeIds');
            if ($page == 1) {
                $staticData = Yii::$app->getRequest()->get('staticData');
                $excludeCashbox = Yii::$app->getRequest()->get('excludeCashbox');
                if (is_array($staticData)) {
                    foreach ($staticData as $key => $value) {

                        if ($excludeCashbox && "order.{$excludeCashbox}" == $key)
                            continue;

                        if (is_string($value)) {
                            $value = [
                                'id' => $key,
                                'text' => $value,
                            ];
                        }
                        if (isset($value['id'], $value['text'])) {
                            if (!$search || stripos($value['text'], $search) !== false) {
                                $results1[] = [
                                    'id' => $value['id'],
                                    'text' => $value['text'],
                                ];
                            }
                        }
                    }
                }
            }

            $query = Contractor::find()->joinWith('companyType', false)->select([
                'id' => "MIN({{contractor}}.[[id]])",
                'contractor.name',
                'type' => "company_type.name_short",
                'contractor.ITN',
                'contractor.PPC',
                'contractor.verified',
                'contractor.face_type',
                'contractor.foreign_legal_form',
            ])->andWHere([
                'or',
                ['contractor.company_id' => null],
                ['contractor.company_id' => $cid],
            ])->byType($type)->andWhere([
                'contractor.is_deleted' => false,
                'contractor.status' => Contractor::ACTIVE,
            ])->andFilterWhere([
                'like',
                'contractor.name',
                $search,
            ])->andFilterWhere([
                'contractor.face_type' => Yii::$app->getRequest()->get('faceType'),
            ])->andFilterWhere([
                'not',
                ['contractor.id' => empty($excludeIds) ? null : explode(',', $excludeIds)],
            ])->groupBy([
                'contractor.id',
                //'contractor.ITN',
                //'contractor.PPC',
                //'contractor.name',
            ])->orderBy([
                new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
                'type' => SORT_ASC,
                'name' => SORT_ASC,
            ])->offset($offset)->limit($limit + 1);

            if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX_STRANGERS)) {
                $query->andWhere([
                    'contractor.responsible_employee_id' => Yii::$app->user->id
                ]);
            }
            if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX_NOT_ACCOUNTING)) {
                $query->andWhere(['or',
                    ['contractor.not_accounting' => false],
                    ['contractor.responsible_employee_id' => Yii::$app->user->id],
                ]);
            }

            $results2 = $query->asArray()->all();

            if (count($results2) == $limit + 1) {
                array_pop($results2);
                $paginationMore = true;
            }

            array_walk($results2, function (&$item) {
                $item['title'] = $item['name'];
                $item['name'] = Html::encode($item['name']);
                $type = $item['face_type'] == Contractor::TYPE_FOREIGN_LEGAL_PERSON ? $item['foreign_legal_form'] : $item['type'];
                $item['text'] = ($type ? $type.' ' : '') . $item['name'];
            });
        }

        // contractors doubles
        if (count($results2) > 1) {
            for ($n = 1; $n < count($results2); $n++) {
                if ($results2[$n - 1]['ITN'] == $results2[$n]['ITN']) {
                    $results2[$n]['showPPC'] = 'КПП ' . $results2[$n]['PPC'];
                    if ($results2[$n - 1]['PPC'] != $results2[$n]['PPC']) {
                        $results2[$n - 1]['showPPC'] = 'КПП ' . $results2[$n - 1]['PPC'];;
                    } else {
                        $results2[$n - 1]['showPPC'] = 'ДУБЛЬ';
                    }
                }
            }
        }

        $results = array_merge($results1, $results2);

        return [
            'results' => $results,
            'pagination' => [
                'more' => $paginationMore,
            ],
        ];
    }
}
