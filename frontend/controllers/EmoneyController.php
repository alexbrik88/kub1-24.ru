<?php

namespace frontend\controllers;

use Yii;
use common\models\cash\Emoney;
use frontend\models\EmoneySearch;
use frontend\rbac\UserRole;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * EmoneyController implements the CRUD actions for Emoney model.
 */
class EmoneyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => 'common\components\filters\AjaxFilter',
                //'only' => ['create', 'update'],
            ],
        ];
    }

    /**
     * Lists all Emoney models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmoneySearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emoney model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emoney model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Emoney([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('create', [
                'model' => $model,
                'success' => true,
            ]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function actionCreateWithAjaxValidation()
    {
        $company = Yii::$app->user->identity->company;
        $model = new Emoney([
            'company_id' => $company->id,
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return [
                'status' => 1,
            ];
        }

        return [
            'status' => 0,
        ];
    }

    /**
     * Updates an existing Emoney model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if (!Yii::$app->request->isAjax) {
                return $this->redirect(Yii::$app->request->referrer ?: ['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
                'success' => true,
            ]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Emoney model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => 2,
                'js' => '$.pjax.reload("#emoney-pjax-container", {timeout: 10000});',
            ];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Emoney model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emoney the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Emoney::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function ajaxValidate($model)
    {
        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }
}
