<?php
namespace frontend\controllers;

use backend\models\Bank;
use common\components\AddressDictionaryHelper;
use common\components\Cropper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CompanyType;
use common\models\company\CompanyNotification;
use common\models\company\CompanySite;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyVisit;
use common\models\CompanyHelper;
use common\models\CompanyProductType;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\Ifns;
use common\models\notification\Notification;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\TaxationType;
use common\models\TimeZone;
use Exception;
use frontend\assets\PrintAsset;
use frontend\components\FrontendController;
use frontend\models\AffiliateProgramForm;
use frontend\models\CompanySiteSearch;
use frontend\models\Documents;
use frontend\models\OutInvoiceSearch;
use frontend\models\VisitCardSendForm;
use frontend\modules\subscribe\forms\PaymentForm;
use frontend\rbac\permissions;
use Yii;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class CompanyController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['create', 'continue-create'],
                'rules' => [
                    [
                        'actions' => ['get-companies-to-crm'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Company::INDEX, permissions\Company::PROFILE],
                    ],
                    [
                        'actions' => ['visit-card', 'send-visit-card', 'document-print'],
                        'allow' => true,
                        'roles' => [permissions\Company::VISIT_CARD],
                    ],
                    [
                        'actions' => [
                            'update', 'view-example-invoice', 'create-checking-accountant',
                            'update-checking-accountant', 'delete-checking-accountant', 'apply-to-bank',
                            'img-form', 'img-upload', 'img-crop', 'img-file', 'img-delete', 'generate-affiliate-link',
                            'withdraw-money', 'ifns',
                            'type-update',
                            'view-example-invoice-facture', 'view-example-upd'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                    [
                        'actions' => ['validate-company', 'add-logo-landing'],
                        'allow' => true,
                    ],
                ],
            ],
            'createAccess' => [
                'class' => 'yii\filters\AccessControl',
                'only' => ['create', 'continue-create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\Company::CREATE],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    if (Yii::$app->user->getIsGuest()) {
                        Yii::$app->user->loginRequired();
                    } else {
                        throw new ForbiddenHttpException(
                            'Ваш аккаунт имеет компании на «Пробном» или «Бесплатном» тарифе. '.
                            'Чтобы добавить еще одну компанию, нужно '.
                            Html::a('оплатить сервис', ['/subscribe/default/index'])
                        );
                    }
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'validate-company' => ['post'],
                    'add-logo-landing' => ['post'],
                    'img-delete' => ['post'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'only' => ['validate-company', 'add-logo-landing'],
                'cors' => [
                    'Origin' => \Yii::$app->params['corsFilter'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['X-PJAX', 'X-PJAX-Container'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['validate-company', 'add-logo-landing', 'get-companies-to-crm',])) {
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Note: same with visitcard
     * @return string
     */
    public function actionIndex($update = false)
    {
        if ($update) {
            $alertMsg = 'Все изменения будут применены ТОЛЬКО к новым документам, т.е. к тем, которые вы создадите после данных изменений.';
            Yii::$app->session->setFlash('success', 'Профиль компании изменен.' .'<br/>'. $alertMsg);
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $searchModel = new CheckingAccountantSearch(['company_id' => $company->id]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get(), false);
        $applicationToBank = new ApplicationToBank([
            'company_name' => $company->getTitle(true),
            'inn' => $company->inn,
            'legal_address' => $company->getAddressLegalFull(),
            'fio' => $company->getChiefFio(),
            'contact_phone' => $company->phone,
            'contact_email' => Yii::$app->user->identity->email,
        ]);
        $banks = Bank::find()->byStatus(!Bank::BLOCKED)->all();
        $affiliateProgramForm = new AffiliateProgramForm();
        $affiliateProgramForm->amount = $company->affiliate_sum;
        $affiliateProgramForm->type = AffiliateProgramForm::RS_TYPE;


        $outInvoiceSearch = new OutInvoiceSearch([
            'company_id' => $company->id,
        ]);
        $outInvoiceProvider = $outInvoiceSearch->search(Yii::$app->request->queryParams);

        $companySiteSearch = new CompanySiteSearch([
            'company_id' => $company->id,
        ]);
        $companySiteProvider = $companySiteSearch->search(Yii::$app->request->queryParams);
        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 78);

        return $this->render('index', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'banks' => $banks,
            'applicationToBank' => $applicationToBank,
            'affiliateProgramForm' => $affiliateProgramForm,
            'outInvoiceSearch' => $outInvoiceSearch,
            'outInvoiceProvider' => $outInvoiceProvider,
            'companySiteSearch' => $companySiteSearch,
            'companySiteProvider' => $companySiteProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionVisitCard()
    {
        $company = Yii::$app->user->identity->company;

        return $this->render('visit-card', [
            'company' => $company,
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws Exception
     */
    public function actionDocumentPrint($actionType)
    {
        /** @var Company $model */
        $model = Yii::$app->user->identity->company;

        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }

        $renderer = new PdfRenderer([
            'view' => 'print',
            'params' => array_merge([
                'company' => $model,
            ]),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $model->getPdfFileName($model),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);

        $this->view->params['asset'] = PrintAsset::className();
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                return $renderer->renderHtml();
        }
    }

    /**
     * @return array|Response
     */
    public function actionSendVisitCard()
    {
        $this->layout = 'empty';

        $company = Yii::$app->user->identity->company;
        $sendForm = new VisitCardSendForm([
            'company' => $company,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        $message = null;
        if (Yii::$app->request->isPost) {
            $sendForm->load(Yii::$app->request->post());

            if ($sendForm->validate()) {
                if ($sendForm->send()) {
                    Yii::$app->session->setFlash('success', 'Сообщение отправлено');
                    $company->email_messages_send_visit_card += 1;
                    $company->save(true, ['email_messages_send_visit_card']);
                } else {
                    Yii::$app->session->setFlash('error', 'При отправке сообщения произошла ошибка');
                }
            }
        }

        return $this->redirect(['visit-card',]);
    }

    /**
     * @return mixed|string|Response
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $employee = Yii::$app->user->identity;
        $model = new Company([
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'email' => $employee->email,
            'address_legal_is_actual' => true,
            'chief_is_chief_accountant' => true,
            'scenario' => Company::SCENARIO_CREATE_COMPANY,
            'owner_employee_id' => Yii::$app->user->id,
            'created_by' => Yii::$app->user->id,
        ]);
        $companyTaxationType = new CompanyTaxationType;
        $model->populateRelation('companyTaxationType', $companyTaxationType);
        if (Yii::$app->request->post('ajax')) {
            $model->companyTaxationType->load(Yii::$app->request->post());
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) &&
            $model->companyTaxationType->load(Yii::$app->request->post()) &&
            $model->companyTaxationType->validate() &&
            $model->save()
        ) {
            $model->companyTaxationType->company_id = $model->id;
            if ($model->companyTaxationType->save()) {
                if (!$employee->companies) {
                    $employeeCompany = new EmployeeCompany([
                        'company_id' => $model->id,
                    ]);
                    $employeeCompany->employee = $employee;
                    $employeeCompany->employee_role_id = EmployeeRole::ROLE_CHIEF;
                    $employeeCompany->date_dismissal = null;
                    $employeeCompany->is_working = true;

                    if ($employeeCompany->save(false)) {
                        $employee->updateAttributes([
                            'company_id' => $model->id,
                            'main_company_id' => $model->id,
                        ]);
                        $model->updateAttributes([
                            'main_id' => $model->id,
                        ]);

                        return $this->redirect('/site/index');
                    }
                }

                return $this->redirect(Url::to(['continue-create', 'id' => $model->id]));
            }
        }

        // todo: temp, because no mts site/index page
        if (Yii::$app->user->getIsMtsUser()) {
            $canInvoiceIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                'ioType' => Documents::IO_TYPE_OUT,
            ]);

            if ($canInvoiceIndex) {
                return $this->redirect([
                    '/documents/invoice/index',
                    'type' => Documents::IO_TYPE_OUT,
                ]);
            } else {
                return $this->redirect(['/company/index']);
            }
        }

        return $this->redirect('/site/index');
    }

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionContinueCreate($id)
    {
        $mainCompany = Yii::$app->user->identity->mainCompany;
        $model = $this->findNewModel($id);
        $model->setScenario(Company::SCENARIO_USER_UPDATE);
        $searchModel = new CheckingAccountantSearch(['company_id' => $model->id]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        $checkingAccountant = new CheckingAccountant();
        $applicationToBank = new ApplicationToBank([
            'company_name' => $model->getTitle(true),
            'inn' => $model->inn,
            'legal_address' => $model->getAddressLegalFull(),
            'fio' => $model->getChiefFio(),
            'contact_phone' => $model->phone,
            'contact_email' => Yii::$app->user->identity->email,
        ]);
        $model->main_id = Yii::$app->user->identity->company->main_id;
        $banks = Bank::find()->byStatus(!Bank::BLOCKED)->all();

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if (Yii::$app->request->isGet && !Yii::$app->request->isAjax) {
            $model->deleteTemporaryImages();
        }

        if ($model->load(Yii::$app->request->post()) &&
            $model->validate() &&
            $model->companyTaxationType->load(Yii::$app->request->post()) &&
            $model->companyTaxationType->validate()
        ) {
            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                if ($model->save() && $model->companyTaxationType->save(false)) {
                    /* @var SubscribeTariff $tariff */
                    $tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_TRIAL);

                    $subscribe = new Subscribe([
                        'company_id' => $model->id,
                        'tariff_group_id' => $tariff->tariff_group_id,
                        'tariff_limit' => $tariff->tariff_limit,
                        'tariff_id' => $tariff->id,
                        'duration_month' => $tariff->duration_month,
                        'duration_day' => $tariff->duration_day,
                        'status_id' => SubscribeStatus::STATUS_PAYED,
                    ]);
                    /* @var Employee $user */
                    $user = Yii::$app->user->identity;
                    $user->company_id = $model->id;
                    $employeeCompany = new EmployeeCompany([
                        'employee_id' => $user->id,
                        'company_id' => $model->id,
                        'employee_role_id' => $user->employee_role_id,
                    ]);
                    $employeeCompany->setEmployee($user);
                    /* @var $notification Notification */
                    if ($subscribe->save() &&
                        $employeeCompany->save(false) &&
                        $user->save(false, ['company_id']) &&
                        $user->linkPaidTariffsWithCompany($model, true)
                    ) {
                        $model->activeSubscribe = $subscribe;
                        return true;
                    }
                }
                $db->transaction->rollBack();

                return false;
            });

            if ($isSaved) {
                CompanyVisit::checkVisit($model);
                if ($mainCompany) {
                    \common\models\company\CompanyFirstEvent::checkEvent($mainCompany, 50);
                }
                if ($model->strict_mode == Company::OFF_STRICT_MODE) {
                    \common\models\company\CompanyFirstEvent::checkEvent($model, 1);
                }
                if ($model->getCheckingAccountants()->exists()) {
                    \common\models\company\CompanyFirstEvent::checkEvent($model, 2);
                }
                Yii::$app->session->setFlash('success', 'Компания ' . $model->getShortName() . ' добавлена');

                return $this->redirect(['/site/index']);
            }
        };

        return $this->render('create', [
            'model' => $model,
            'ifns' => $model->ifns ? : new Ifns(),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'checkingAccountant' => $checkingAccountant,
            'banks' => $banks,
            'applicationToBank' => $applicationToBank,
            'submitAction' => Url::to(['continue-create', 'id' => $model->id]),
        ]);
    }

    /**
     * @param null $backUrl
     * @return array|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($backUrl = null)
    {
        /** @var Company $model */
        $model = Yii::$app->user->identity->company;
        //$model->tmpId = uniqid();
        $model->setScenario(Company::SCENARIO_USER_UPDATE);

        $searchModel = new CheckingAccountantSearch(['company_id' => $model->id]);
        $dataProvider = $searchModel->search(\Yii::$app->request->get(), false);
        $checkingAccountant = new CheckingAccountant();
        $applicationToBank = new ApplicationToBank([
            'company_name' => $model->getTitle(true),
            'inn' => $model->inn,
            'legal_address' => $model->getAddressLegalFull(),
            'fio' => $model->getChiefFio(),
            'contact_phone' => $model->phone,
            'contact_email' => Yii::$app->user->identity->email,
        ]);
        $banks = Bank::find()->byStatus(!Bank::BLOCKED)->all();

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $model->companyTaxationType->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model, $model->companyTaxationType);
        }

        if (Yii::$app->request->isGet && !Yii::$app->request->isAjax) {
            $model->deleteTemporaryImages();
        }

        if ($model->load(Yii::$app->request->post()) &&
            $model->companyTaxationType->load(Yii::$app->request->post()) &&
            $model->companyTaxationType->validate()
        ) {
            $model->capitalInput = $model->capital;
            if ($model->validate()) {
                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    if ($model->save() && $model->companyTaxationType->save()) {
                        $user = Yii::$app->user->identity;
                        if ($user->employee_role_id === EmployeeRole::ROLE_CHIEF &&
                            !$user->validate([
                                'position',
                                'lastname',
                                'firstname',
                                'patronymic',
                                'firstname_initial',
                                'patronymic_initial'
                            ])
                        ) {
                            $model->setDirectorInitials();
                        }

                        return true;
                    }
                });

                if ($isSaved) {
                    return $this->redirect([$backUrl !== null ? $backUrl : 'index', 'update' => true]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'ifns' => $model->ifns ?: new Ifns(),
            'backUrl' => $backUrl,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'checkingAccountant' => $checkingAccountant,
            'banks' => $banks,
            'applicationToBank' => $applicationToBank,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionTypeUpdate()
    {
        /** @var Company $model */
        $model = Yii::$app->user->identity->company;
        //$model->tmpId = uniqid();
        $model->setScenario(Company::SCENARIO_CREATE_COMPANY);

        $result = false;
        if ($model->strict_mode && $model->load(Yii::$app->request->post()) && $model->validate(['company_type_id'])) {
            $result = $model->save(false, [
                'self_employed',
                'company_type_id',
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'result' => $result,
        ];
    }

    /**
     * Updates an existing Company model.
     * @param string $backUrl
     * @return mixed|string
     */
    public function actionIfns()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($kpp = Yii::$app->request->post('kpp', '')) {
            $ga = mb_substr($kpp, 0, 4) * 1;

            return [
                'ifns' => $ga ? Ifns::findOne(['ga' => $ga]) : null,
            ];
        } elseif (($city = Yii::$app->request->post('city', '')) && ($street = Yii::$app->request->post('street', ''))) {
            $data = AddressDictionaryHelper::searchData($city, $street);
            if (isset($data['ifnsul'], $data['oktmo'])) {
                return [
                    'ifns' => Ifns::findOne(['ga' => $data['ifnsul']]),
                    'oktmo' => $data['oktmo'],
                ];
            }
        }

        return [];
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImgForm($id, $attr, $action = 'view', $apply = 0)
    {
        $actionArray = ['view', 'upload', 'crop'];
        if (strpos(Yii::$app->request->referrer, 'continue-create') !== false) {
            $model = $this->findNewModel($id);
        } else {
            $model = $this->findModel($id);
        }

        if ($model === null || !in_array($attr, array_keys(Company::$imageAttrArray))) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $view = $attr == 'chiefAccountantSignatureImage' ? '_partial_files_chief_accountant' : '_partial_files_tabs';
        return $this->render('form/' . $view, [
            'model' => $model,
            'attr' => $attr,
            'apply' => $apply,
            'action' => in_array($action, $actionArray) ? $action : 'view',
        ]);
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImgUpload($id, $attr)
    {
        if (strpos(Yii::$app->request->referrer, 'continue-create') !== false) {
            $model = $this->findNewModel($id);
        } else {
            $model = $this->findModel($id);
        }

        if ($model === null || !in_array($attr, array_keys(Company::$imageAttrArray))) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($file = UploadedFile::getInstanceByName($attr)) {
            if (in_array($file->type, ['image/jpeg', 'image/png'])) {
                $tmp = $this->isTmpUpload();
                if (CompanyHelper::saveImage($model, $attr, $file, $tmp) && !$tmp) {
                    $model->save(false, [Company::$imageAttrArray[$attr]]);

                    return ['status' => 'success'];
                }
            } else {
                return ['status' => 'error', 'message' => 'Допустимый формат файла: jpg, jpeg, png.'];
            }
        }

        return ['status' => 'error', 'message' => 'Ошибка загрузки файла.'];
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImgCrop()
    {
        $id = Yii::$app->request->post('id');
        $attr = Yii::$app->request->post('attr');
        $width = Yii::$app->request->post('width');
        $apply = (int) Yii::$app->request->post('apply', 0);
        if (strpos(Yii::$app->request->referrer, 'continue-create') !== false) {
            $model = $this->findNewModel($id);
        } else {
            $model = $this->findModel($id);
        }

        if ($model === null || !in_array($attr, array_keys(Company::$imageAttrArray))) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $tmp = $this->isTmpUpload();
        $imgModel = $model->getImageModel($attr, $tmp);
        if ($imgModel === null) {
            throw new NotFoundHttpException('Файл не найден.');
        }
        $imgPath = $imgModel->getFilePath();

        $cropper = new Cropper([
            'imagePath' => $imgPath,
            'destPath' => $imgPath,
            'width' => $width,
        ]);

        $cropper->crop($attr);

        if (!$tmp && $apply) {
            $imgModel->applyForAll();
        }
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImgFile($id, $attr, $tmp = null)
    {
        if (strpos(Yii::$app->request->referrer, 'continue-create') !== false) {
            $model = $this->findNewModel($id);
        } else {
            $model = $this->findModel($id);
        }

        if ($model === null || !in_array($attr, array_keys(Company::$imageAttrArray))) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        //$tmp = $this->isTmpUpload();
        $imgPath = $model->getImage($attr, $tmp);
        if (is_file($imgPath)) {
            return \Yii::$app->response->sendFile($imgPath, null, ['inline' => true])->send();
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionImgDelete()
    {
        $id = Yii::$app->request->post('id');
        $attr = Yii::$app->request->post('attr');
        if (strpos(Yii::$app->request->referrer, 'continue-create') !== false) {
            $model = $this->findNewModel($id);
        } else {
            $model = $this->findModel($id);
        }

        if ($model === null || !in_array($attr, array_keys(Company::$imageAttrArray))) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $tmp = $this->isTmpUpload();
        CompanyHelper::deleteImage($model, $attr, $tmp);
    }

    /**
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionCreateCheckingAccountant($companyId = null)
    {
        $newCompany = ($companyId) ? Company::findOne(['id' => $companyId, 'owner_employee_id' => Yii::$app->user->id, 'strict_mode' => 1]) : null;

        if (Yii::$app->request->isAjax) {
            $model = new CheckingAccountant([
                'company_id' => Yii::$app->user->identity->company->id,
            ]);

            Yii::$app->response->format = Response::FORMAT_JSON;

            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post())) {
                if (($newCompany && $model->_saveForNewCompany($newCompany)) || $model->save()) {
                    return [
                        'status' => 1,
                        'rs_form_list' => $this->renderAjax('form/_rs_update_form_list', [
                            'model' => $model->company,
                        ]),
                    ];
                } else {
                    \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
                }
            }

            return [
                'status' => 0,
                'html' => $this->renderAjax('form/modal_rs/_create', [
                    'checkingAccountant' => $model,
                ])
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @param null $companyId
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdateCheckingAccountant($id, $companyId = null)
    {
        if (Yii::$app->request->isAjax) {
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            /* @var $model CheckingAccountant */
            $model = $company->getCheckingAccountants()->andWhere(['id' => $id])->one();

            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return [
                    'status' => 1,
                    'rs_form_list' => $this->renderAjax('form/_rs_update_form_list', [
                        'model' => $company,
                    ]),
                ];
            }

            return [
                'status' => 0,
                'html' => $this->renderAjax('form/modal_rs/_update', [
                    'checkingAccountant' => $model,
                    'company' => $company,
                ])
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteCheckingAccountant($id)
    {
        if (Yii::$app->request->isAjax) {
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            /* @var $model CheckingAccountant */
            $model = $company->getCheckingAccountants()->andWhere(['id' => $id])->one();

            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }

            $model->delete();

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'status' => 1,
                'rs_form_list' => $this->renderAjax('form/_rs_update_form_list', [
                    'model' => $model->company,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $bankId
     * @param $redirectAction
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    public function actionApplyToBank($bankId, $redirectAction)
    {
        /* @var $bank Bank */
        $model = new ApplicationToBank();
        $bank = Bank::findOne($bankId);
        if ($bank == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->bank_id = $bankId;
            $model->company_id = Yii::$app->user->identity->company->id;
            if ($model->save() && $model->send()) {
                $model->bank->request_count += 1;
                $model->bank->save();
                Yii::$app->session->setFlash('success', 'Заявка на открытие расчетного счета отправлена.');
            } else {
                if (!Yii::$app->session->hasFlash('error')) {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка при отправке заявки на открытие расчетного счета.');
                }
            }

            return $this->redirect($redirectAction);
        }

        return $this->render('form/_apply_to_bank', [
            'model' => $model,
        ]);
    }

    /**
     * @param $page
     * @return string
     * @throws BadRequestHttpException
     */
    public function actionGenerateAffiliateLink($page)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            if ($company->affiliate_link === null) {
                do {
                    $affiliateLink = Yii::$app->security->generateRandomString(15);
                } while (Company::find()->andWhere(['affiliate_link' => $affiliateLink])->exists());
                $company->affiliate_link = $affiliateLink;
                $company->affiliate_link_created_at = time();
                $company->save(true, ['affiliate_link', 'affiliate_link_created_at']);
            }
            $model = new PaymentForm($company, [
                'employee' => Yii::$app->user->identity,
            ]);
            $formData = $model->getFormData();

            return [
                'page' => $page,
                'html' => $this->renderPartial($page == 'company' ?
                    'view-profile/_affiliate_program' :
                    '@frontend/modules/subscribe/views/default/partial/_affiliate_program', [
                        'company' => $company,
                        'tariffArray' => ArrayHelper::getValue($formData, 'tariffArray', []),
                        'isOpenedAffiliateBlock' => true,
                    ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $page
     * @return mixed|Response
     */
    public function actionWithdrawMoney($page)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $affiliateProgramForm = new AffiliateProgramForm();
        $affiliateProgramForm->amount = $company->affiliate_sum;
        $affiliateProgramForm->type = AffiliateProgramForm::RS_TYPE;
        if ($company->phone) {
            $affiliateProgramForm->phone = $company->phone;
        }
        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($affiliateProgramForm);
        }

        if ($affiliateProgramForm->load(Yii::$app->request->post()) && $affiliateProgramForm->_save()) {
            Yii::$app->session->setFlash('success', 'Заявка на получение вознаграждения отправлена.');
        }

        return $this->redirect([$page == 'company' ? 'index' : '/subscribe']);
    }

    /**
     * @param $nds
     * @return string
     */
    public function actionViewExampleInvoice($nds)
    {
        return '<div align="center">' . Html::img($nds == true ? '/img/temp/with-nds.jpg' : '/img/temp/without-nds.jpg') . '</div>';
    }

    /**
     * @param $ver
     * @return string
     */
    public function actionViewExampleInvoiceFacture($ver)
    {
        return '<div align="center">' . Html::img($ver == 2 ? '/img/temp/invoice-facture-2.jpg?1' : '/img/temp/invoice-facture-1.jpg?1') . '</div>';
    }

    /**
     * @param $ver
     * @return string
     */
    public function actionViewExampleUpd($ver)
    {
        return '<div align="center">' . Html::img($ver == 2 ? '/img/temp/upd-2.jpg?1' : '/img/temp/upd-1.jpg?1') . '</div>';
    }

    /**
     * @return array|bool
     */
    public function actionGetCompaniesToCrm()
    {
        if (Yii::$app->request->isPost) {
            $companiesData = Yii::$app->request->post('Company');

            if ($companiesData !== null) {
                $result = [];
                foreach ($companiesData as $companyData) {
                    if (isset($companyData['inn']) &&
                        isset($companyData['kpp']) &&
                        $companyData['inn'] !== null &&
                        ($companies = Company::find()
                            ->isTest(!Company::TEST_COMPANY)
                            ->isBlocked(Company::UNBLOCKED)
                            ->andWhere(['and',
                                ['inn' => $companyData['inn']],
                                ['kpp' => $companyData['kpp']],
                            ])->all()) !== null
                    ) {
                        /* @var $company Company */
                        foreach ($companies as $company) {
                            $employeeChief = $company->getEmployeeChief();
                            $result[] = [
                                'user_fio' => $company->getChiefFio(),
                                'user_id' => $employeeChief->id,
                                'company_name' => $company->getShortName(),
                                'chat_photo' => $employeeChief->chat_photo,
                                'is_online' => $employeeChief->isOnline(),
                                'inn' => $companyData['inn'],
                                'kpp' => $companyData['kpp'],
                            ];
                        }
                    }
                }

                return serialize($result);
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function actionValidateCompany()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset(Yii::$app->request->post('Company')['inn'])) {
            if (Company::find()->andWhere(['and',
                    ['inn' => Yii::$app->request->post('Company')['inn']],
                    ['!=', 'id', Yii::$app->request->post('company_id')],
                ])->one() !== null
            ) {
                return ['result' => false, 'uniqueCompany' => true,];
            }
        }
        if (isset(Yii::$app->request->post('Company')['company_type_id'])) {
            $scenario = Yii::$app->request->post('Company')['company_type_id'] == CompanyType::TYPE_IP ? Company::SCENARIO_IP_UPDATE_LANDING : Company::SCENARIO_OOO_UPDATE_LANDING;
        } else {
            $scenario = Company::SCENARIO_OOO_UPDATE_LANDING;
        }
        $model = new Company([
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'address_legal_is_actual' => true,
            'chief_is_chief_accountant' => false,
            'scenario' => $scenario,
        ]);
        $checkingAccountant = new CheckingAccountant();
        $checkingAccountant->scenario = CheckingAccountant::SCENARIO_LANDING_CREATION;
        $checkingAccountant->company_id = 1; // рандомный id компании, для валидации, потому что мне впадлу писать новый сценарий и не влажу в оценку x)
        if ($model->load(Yii::$app->request->post()) && $checkingAccountant->load(Yii::$app->request->post()) && $model->validate() && $checkingAccountant->validate()) {
            if ($model->company_type_id == CompanyType::TYPE_IP) {
                $model->ip_firstname_initials = mb_substr($model->ip_firstname, 0, 1);
                $model->ip_patronymic_initials = mb_substr($model->ip_patronymic, 0, 1);
            }
            $model->chief_firstname_initials = mb_substr($model->chief_firstname, 0, 1);
            $model->chief_patronymic_initials = mb_substr($model->chief_patronymic, 0, 1);

            return [
                'result' => true,
                'name' => $model->getShortName(),
                'chiefFio' => $model->getChiefFio(true),
            ];
        }
        $checkingAccountant->validate();

        return [
            'result' => false,
            'errors' => [
                'company' => $model->getErrors(),
                'checking-accountant' => $checkingAccountant->getErrors(),
            ],
        ];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionAddLogoLanding()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->post('id') !== null) {
            $company = $this->findModel(Yii::$app->request->post('id'));
            if (CompanyHelper::load($company, true) && CompanyHelper::validate($company, true) && CompanyHelper::save($company, true)) {
                return [
                    'result' => true,
                    'images' => [
                        'logo' => $company->logo_link,
                        'print' => $company->print_link,
                        'chief' => $company->chief_signature_link,
                    ],
                ];
            }

            return ['result' => false, 'errors' => $company->getErrors()];
        }
        throw new BadRequestHttpException('company id cannot be null');
    }

    /**
     * @param $id
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $employee = Yii::$app->user->identity;
        if ($employee && $id) {
            $model = $employee->getCompanies()->andWhere(['id' => $id])->one();

            if ($model !== null) {
                return $model;
            }
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * @param $id
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function findNewModel($id)
    {
        $employee = Yii::$app->user->identity;
        if ($employee && $id) {
            $model = Company::find()
                ->alias('company')
                ->joinWith('employeeCompanies employee')
                ->andWhere([
                    'company.id' => $id,
                    'company.created_by' => $employee->id,
                    'employee.company_id' => null,
                ])
                ->one();

            if ($model !== null) {
                return $model;
            }
        }

        throw new NotFoundHttpException('Запрошенная страница устарела или не существует.');
    }

    /**
     * @param $id
     * @return Company
     * @throws NotFoundHttpException
     */
    protected function isTmpUpload()
    {
        $referrer = Yii::$app->request->referrer;
        return in_array(parse_url($referrer, PHP_URL_PATH), [
            '/company/update',
            '/company/continue-create',
        ]);
    }
}
