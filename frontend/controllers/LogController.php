<?php

namespace frontend\controllers;

use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\models\log\LogSearch;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * LogController.
 */
class LogController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Service::HOME_ACTIVITIES],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogSearch();
        $searchModel->company_id = Yii::$app->user->identity->company->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
