<?php

namespace frontend\controllers;

use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\modules\import\models\ImportJobData;
use frontend\components\FrontendController;
use frontend\models\jobs\JobsSearch;
use frontend\rbac\UserRole;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class JobsController extends FrontendController
{
    use WebTrait;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return Response
     */
    public function actionIndex(): Response
    {
        $searchModel = new JobsSearch();
        $dataProvider = $searchModel->search($this->request->get());
        $hasDebug = (bool) $this->request->get('debug');

        $this->response->content = $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'showHidden' => $hasDebug,
        ]);

        return $this->response;
    }

    /**
     * @return void
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionMarkAsViewed(): void
    {
        $id = $this->request->get('id');

        if (!is_scalar($id)) {
            throw new BadRequestHttpException();
        }

        $model = ImportJobData::findOne(['id' => $id]);

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        $model->is_viewed = true;
        $model->save(true, ['is_viewed']);
    }
}
