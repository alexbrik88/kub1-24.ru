<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use frontend\components\FrontendController;
use frontend\modules\analytics\models\financeDashboard\FinanceDashboard;
use frontend\modules\analytics\models\financeDashboard\OutFinanceDashboard;

/**
 * OutAnalyticsDashboardController
 */
class OutAnalyticsDashboardController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($uid, $tab = null)
    {
        $this->layout = '@frontend/modules/analytics/views/finance-dashboard/out/out-view-layout';

        if (Yii::$app->user->isGuest)
            throw new NotFoundHttpException('Внешняя ссылка на дашборд в разработке');

        /* @var FinanceDashboard $dashboard */
        $dashboard = self::getDashboardByUid($uid, $tab);
        $searchModel = $dashboard->getSearchModel();

        $this->view->params = [
            'identity' => $dashboard->getEmployee(),
            'company' => $dashboard->getCompany(),
            'dashboard' => $dashboard,
            'searchModel' => $searchModel,
            'tab' => $dashboard->getTab(),
        ];

        return $this->render('@frontend/modules/analytics/views/finance-dashboard/index', $this->view->params);
    }

    private static function getDashboardByUid($uid, $tab): FinanceDashboard
    {
        if ($outDashboard = OutFinanceDashboard::findOne(['uid' => $uid])) {
            $company = $outDashboard->company;
            $employee = $company->ownerEmployee;
        }

        try {
            $dashboard = new FinanceDashboard($tab, $employee, $company);
        } catch (\Throwable $e) {
            if (empty(FinanceDashboard::sanitizeTab($tab))) {
                throw new NotFoundHttpException('Вкладка не найдена.');
            } else {
                Yii::error(VarDumper::dumpAsString($e), 'analytics');
                throw new NotFoundHttpException('Ошибка инициализации дашборда. Обратитесь в службу технической поддержки.');
            }
        }

        return $dashboard ?? null;
    }
}
