<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.05.2018
 * Time: 17:01
 */

namespace frontend\controllers;

use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Report;
use common\models\paymentReminder\Settings;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\PaymentReminderContractorSearch;
use frontend\models\PaymentReminderMessageContractorSearch;
use frontend\models\PaymentReminderMessageSearch;
use frontend\models\PaymentReminderReportSearch;
use frontend\rbac\permissions\PaymentReminder;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class PaymentReminderController
 * @package frontend\controllers
 */
class PaymentReminderController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [
                            PaymentReminder::INDEX,
                        ],
                    ],
                    [
                        'actions' => [
                            'template',
                            'template-status',
                            'update',
                            'change-status',
                        ],
                        'allow' => true,
                        'roles' => [
                            PaymentReminder::TEMPLATE,
                        ],
                    ],
                    [
                        'actions' => [
                            'settings',
                            'customers',
                            'customer-add',
                            'customer-delete',
                            'update-message-contractor',
                        ],
                        'allow' => true,
                        'roles' => [
                            PaymentReminder::SETTINGS,
                        ],
                    ],
                    [
                        'actions' => [
                            'report',
                        ],
                        'allow' => true,
                        'roles' => [
                            PaymentReminder::REPORT,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'user' => Yii::$app->user->identity,
        ]);
    }

    public function actionTemplate()
    {
        $paymentReminderMessageSearchModel = new PaymentReminderMessageSearch();
        $paymentReminderMessageDataProvider = $paymentReminderMessageSearchModel->search(Yii::$app->request->get());
        $paymentReminderMessageDataProvider->pagination->pageSize = 10;
        return $this->render('template', [
            'user' => Yii::$app->user->identity,
            'paymentReminderMessageSearchModel' => $paymentReminderMessageSearchModel,
            'paymentReminderMessageDataProvider' => $paymentReminderMessageDataProvider,
        ]);
    }

    public function actionTemplateStatus($id)
    {
        $model = $this->findModel($id);
        $status = $model->status ? 'включен' : 'отключен';
        $flash = 'error';
        $addText = '';

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(true, ['status'])) {
                    $flash = 'success';
                    $status = $model->status ? 'включен' : 'отключен';
                }
            } else {
                $addText  = ' Необходимо заполнить шаблон.';
            }
        }

        Yii::$app->session->setFlash($flash, "Шаблон №{$model->number} {$status}.{$addText}");

        return $this->redirect(['template']);
    }

    public function actionSettings()
    {
        $request = Yii::$app->request;

        $user = Yii::$app->user->identity;

        $model = $user->company->paymentReminderSettings ? : new Settings([
            'company_id' => $user->company->id,
            'send_mode' => Settings::SEND_DISABLE,
        ]);

        $searchModel = new PaymentReminderMessageContractorSearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dataProvider = $searchModel->search(array_merge(
            $request->get(),
            $request->post()
        ));
        $dataProvider->pagination->pageSize = PageSize::get();

        $contractorSearchModel = new PaymentReminderContractorSearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $contractorDataProvider = $contractorSearchModel->search(array_merge(
            Yii::$app->request->get(),
            Yii::$app->request->post()
        ));
        $contractorDataProvider->pagination->pageSize = PageSize::get();

        if ($request->post('cancel')) {
            Yii::$app->session->setFlash('success', 'Изменения правил рассылки отменены.');
        } else {
            if ($request->post('ajax')) {
                return $this->ajaxValidate($model);
            }

            if ($model->load($request->post()) && $model->save()) {
                $data = Yii::$app->request->post('PaymentReminderMessageContractor');
                /* @var $company Company */
                $company = Yii::$app->user->identity->company;
                if ($data && is_array($data)) {
                    foreach ($data as $messageAttribute => $oneData) {
                        if ($oneData && is_array($oneData)) {
                            foreach ($oneData as $contractorID => $value) {
                                /* @var $model PaymentReminderMessageContractor */
                                $model = PaymentReminderMessageContractor::find()
                                    ->andWhere(['company_id' => $company->id])
                                    ->andWhere(['contractor_id' => $contractorID])
                                    ->one();
                                if ($model !== null && $model->load([$messageAttribute => $value], '')) {
                                    $model->save();
                                }
                            }
                        }
                    }
                }
                Yii::$app->session->setFlash('success', 'Правила рассылки автоматических писем сохранены.');
            }
        }

        return $this->render('settings', [
            'model' => $model,
            'user' => $user,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'contractorSearchModel' => $contractorSearchModel,
            'contractorDataProvider' => $contractorDataProvider,
        ]);
    }

    public function actionCustomers()
    {
        $searchModel = new PaymentReminderContractorSearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = 10;

        return $this->render('customers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCustomerAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ids = (array) Yii::$app->request->post('selection');
        $companyId = Yii::$app->user->identity->company->id;
        $count = 0;

        if ($ids) {
            $activeNumbers = PaymentReminderMessage::find()->select('number')->andWhere([
                'company_id' => Yii::$app->user->identity->company->id,
                'status' => PaymentReminderMessage::STATUS_ACTIVE
            ])->column();
            $attributes = [];

            foreach (range(1, 10) as $number) {
                $attribute = "message_{$number}";
                $attributes[$attribute] = in_array($number, $activeNumbers) ? 1 : 0;
            }

            foreach ($ids as $contractorId) {
                $contractorExist = Contractor::find()->where([
                    'company_id' => $companyId,
                    'id' => $contractorId,
                ])->exists();
                $modelExist = PaymentReminderMessageContractor::find()->where([
                    'company_id' => $companyId,
                    'contractor_id' => $contractorId,
                ])->exists();
                if ($contractorExist && !$modelExist) {
                    $model = new PaymentReminderMessageContractor([
                        'company_id' => $companyId,
                        'contractor_id' => $contractorId,
                    ]);
                    $model->setAttributes($attributes);
                    if ($model->save()) {
                        $count++;
                    }
                }
            }
        }

        return ['count' => $count];
    }

    public function actionCustomerDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = PaymentReminderMessageContractor::find()->where([
            'company_id' => Yii::$app->user->identity->company->id,
            'id' => $id,
        ])->one();

        if ($model !== null) {
            $model->delete();
        }

        return;
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionReport()
    {
        $dateRange = \frontend\components\StatisticPeriod::getSessionPeriod();
        $searchModel = new PaymentReminderReportSearch();
        $searchModel->company_id = Yii::$app->user->identity->company->id;
        if (!Yii::$app->user->can(PaymentReminder::REPORT_ALL)) {
            $searchModel->responsible_employee_id = Yii::$app->user->identity->id;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return mixed|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Шаблона письма №' . $model->number . ' обновлен');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при обновлении шаблона письма №' . $model->number);
        }

        return $this->redirect(['template']);
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $status = Yii::$app->request->post('status');
        if (isset($status)) {
            $model->updateAttributes([
                'status' => (bool) $status,
            ]);
            Yii::$app->session->setFlash('success', 'Шаблон №' . $model->number . ' ' . ($model->status ? 'включен' : 'отключен') . '.');
        }

        return $this->redirect(['template']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionUpdateMessageContractor()
    {
        $request = Yii::$app->request;

        $user = Yii::$app->user->identity;

        $data = $request->post('PaymentReminderMessageContractor');
        /* @var $company Company */
        $company = $user->company;
        if ($data && is_array($data)) {
            foreach ($data as $messageAttribute => $oneData) {
                if ($oneData && is_array($oneData)) {
                    foreach ($oneData as $contractorID => $value) {
                        /* @var $model PaymentReminderMessageContractor */
                        $model = PaymentReminderMessageContractor::find()
                            ->andWhere(['company_id' => $company->id])
                            ->andWhere(['contractor_id' => $contractorID])
                            ->one();
                        if ($model !== null && $model->load([$messageAttribute => $value], '')) {
                            $model->save();
                        }
                    }
                }
            }
        }

        if ($request->isAjax) {
            $searchModel = new PaymentReminderMessageContractorSearch([
                'company_id' => Yii::$app->user->identity->company->id,
            ]);
            $dataProvider = $searchModel->search(array_merge(
                $request->get(),
                $request->post()
            ));
            $dataProvider->pagination->pageSize = PageSize::get();

            return $this->render('_selected', [
                'user' => $user,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->redirect(['settings']);
    }

    /**
     * @param $id
     * @param $templateNumber
     * @param $status
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionSetTemplateToContractor($id = null, $templateNumber, $status)
    {
        $messageTemplateStatus = PaymentReminderMessageContractor::getMessageTemplateStatus($templateNumber);
        if ($messageTemplateStatus) {
            $attribute = 'message_' . $templateNumber;
            if ($id == null) {
                /* @var $models PaymentReminderMessageContractor[] */
                $models = PaymentReminderMessageContractor::find()
                    ->joinWith('contractor')
                    ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
                    ->andWhere([PaymentReminderMessageContractor::tableName() . '.company_id' => Yii::$app->user->identity->company->id])
                    ->andWhere([$attribute => !$status])
                    ->all();
                foreach ($models as $model) {
                    $model->$attribute = $status;
                    $model->save(true, [$attribute]);
                }

                return true;
            } else {
                $model = $this->findModelPaymentReminderMessageContractor($id);
                if ($model->hasAttribute($attribute)) {
                    $model->$attribute = $status;
                    return $model->save(true, [$attribute]);
                }
            }
        }

        return false;
    }

    public function actionCreateMessageContractor()
    {
        $contractor = Contractor::findOne([
            'id' => Yii::$app->request->post('company_id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($contractor) {
            $query = PaymentReminderMessageContractor::find()->where([
                'company_id' => $contractor->company_id,
                'contractor_id' => $contractor->id,
            ]);
            if (!$query->exists()) {
                $model = new PaymentReminderMessageContractor([
                    'company_id' => $contractor->company_id,
                    'contractor_id' => $contractor->id,
                ]);
                $model->save();
            }
        }
    }

    /**
     * Finds the PaymentReminderMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentReminderMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = PaymentReminderMessage::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Activities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentReminderMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelPaymentReminderMessageContractor($id)
    {
        if (($model = PaymentReminderMessageContractor::find()
                ->joinWith('contractor')
                ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
                ->andWhere([PaymentReminderMessageContractor::tableName() . '.id' => $id])
                ->andWhere([PaymentReminderMessageContractor::tableName() . '.company_id' => Yii::$app->user->identity->company->id])
                ->one()) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
