<?php

namespace frontend\controllers;

use frontend\models\Qiwi;
use Yii;
use yii\web\Controller;

/**
 * Class QiwiController
 * @package frontend\controllers
 */
class QiwiController extends Controller
{
    public $layoutWrapperCssClass;

    /**
     * @return string
     */
    public function actionPayForm()
    {
        $this->layout = 'empty';

        $model = new Qiwi;
        $model->load(Yii::$app->request->get(), '');

        return $this->render('pay-form', [
            'data' => $model->formData(),
        ]);
    }

    /**
     * @return string
     */
    public function actionSuccess($uid = null)
    {
        return $this->render('success');
    }
}
