<?php

namespace frontend\controllers;

use backend\models\Prompt;
use backend\models\PromptHelper;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\components\pdf\PdfRenderer;
use common\models\Agreement;
use common\models\Company;
use common\models\Contractor;
use common\models\contractor\ContractorGroup;
use common\models\ContractorAccount;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\file;
use common\models\prompt\PageType;
use common\models\service\Payment;
use common\models\store\StoreCompanyContractor;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\models\CashContractorSearch;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\ClientForm;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\ContactForm;
use frontend\modules\crm\models\ContractorFormMutator;
use frontend\modules\documents\components\AgentReportHelper;
use frontend\modules\documents\models\AgreementSearch;
use frontend\modules\documents\models\ForeignCurrencyInvoiceSearch;
use frontend\models\CollateForm;
use frontend\models\CompanySiteSearch;
use frontend\models\ContractorSearch;
use frontend\models\Documents;
use frontend\models\OutInvoiceSearch;
use frontend\models\SellingSearch;
use frontend\models\StoreCompanyContractorSearch;
use frontend\modules\analytics\models\AnalysisSearch;
use frontend\modules\analytics\models\DisciplineSearch;
use frontend\modules\documents\models\AutoinvoiceSearch;
use frontend\modules\donate\models\DonateWidgetSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\permissions\crm\ClientAccess;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ContractorController implements the CRUD actions for Contractor model.
 */
class ContractorController extends FrontendController
{
    const PAYMENT_DELAY_UPDATE_ALL = 3;
    const PAYMENT_DELAY_UPDATE_UNPAID = 2;
    const PAYMENT_DELAY_NO_UPDATE = 1;
    
    public static $tabs = [
        Contractor::TYPE_SELLER => [
            null,
            'agreements',
            'info',
            'selling',
            'agent_report',
            'foreign_invoice',
        ],
        Contractor::TYPE_CUSTOMER => [
            null,
            'autoinvoice',
            'agreements',
            'analytics',
            'selling',
            'info',
            'foreign_invoice',
        ],
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authAccess' => [
                'rules' => new \yii\helpers\ReplaceArrayValue([
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]),
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\Contractor::INDEX],
                        'roleParams' => [
                            'type' => Yii::$app->request->getQueryParam('type'),
                        ],
                    ],
                    [
                        'actions' => ['store-account', 'sale-increase'],
                        'allow' => true,
                        'roles' => [permissions\Contractor::INDEX],
                        'roleParams' => [
                            'type' => Contractor::TYPE_CUSTOMER,
                        ],
                    ],
                    [
                        'actions' => [
                            'alert-close',
                            'is-different-address',
                            'find-duplicate',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'view',
                            'print-agent-report',
                            'send-agent-report',
                            'has-nds',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Contractor::VIEW],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(),
                                'type' => Yii::$app->request->getQueryParam('type'),
                            ];
                        }
                    ],
                    [
                        'actions' => ['operations'],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                        ],
                    ],
                    [
                        'actions' => [
                            'create',
                            'add-modal-contractor',
                            'add-group',
                            'group-edit',
                            'group-delete'

                        ],
                        'allow' => true,
                        'roles' => [permissions\Contractor::CREATE],
                        'roleParams' => function ($rule) {
                            return [
                                'type' => Yii::$app->request->getQueryParam('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'agreement-create',
                            'agreement-update',
                            'agreement-file-get',
                            'agreement-file-list',
                            'agreement-file-delete',
                            'agreement-file-upload',
                            'check-availability',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                                    'type' => Contractor::TYPE_SELLER,
                                ]) || Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                                    'type' => Contractor::TYPE_CUSTOMER,
                                ]);
                        },
                    ],
                    [
                        'actions' => [
                            'change-responsible',
                            'change-article',
                            'change-status',
                            'change-payment-delay',
                            'change-payment-priority',
                            'change-industry',
                            'change-sale-point'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $contractors = Yii::$app->request->post('Contractor', []);
                            $model = ($contractors) ? $this->findModel(array_keys($contractors)[0]) : null;
                            return Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                                'model' => $model,
                                'type' => Yii::$app->request->getQueryParam('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['validate-contractor'],
                        'roles' => ['@'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'update',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $model = $action->controller->findModel();

                            return Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                                'model' => $model,
                                'type' => ArrayHelper::getValue($model, 'type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['account-create', 'account-update','account-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $model = $action->controller->findModel(Yii::$app->request->get('cid'));

                            return Yii::$app->getUser()->can(permissions\Contractor::UPDATE, [
                                'model' => $model,
                                'type' => ArrayHelper::getValue($model, 'type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Contractor::DELETE],
                        'roleParams' => function ($rule) {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'merge',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => ['agreement-delete'],
                        'allow' => true,
                        'roles' => [permissions\document\Document::DELETE],
                        'roleParams' => function ($rule) {
                            return [
                                'model' => $this->findAgreementModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'actions' => ['collate'],
                        'allow' => true,
                        'roles' => [permissions\document\Collate::CREATE],
                    ],
                    [
                        'actions' => ['create-autoinvoice'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ]) && Yii::$app->request->get('type') == Contractor::TYPE_CUSTOMER;
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'validate-contractor' => ['post'],
                    'store-account' => ['post'],
                    'account-delete' => ['post'],
                    'find-duplicate' => ['post'],
                    'merge' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'store-account',
                    'account-create',
                    'account-update',
                    'account-delete',
                    'find-duplicate',
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'only' => ['validate-contractor'],
                'cors' => [
                    'Origin' => \Yii::$app->params['corsFilter'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['X-PJAX', 'X-PJAX-Container'],
                ],
            ],
            'installTypes' => [
                'class' => \frontend\modules\crm\behaviors\InstallTypesBehavior::class,
                'events' => [self::EVENT_BEFORE_ACTION],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['validate-contractor',])) {
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        if (Yii::$app->user->can(permissions\Contractor::CREATE)) {
            $actions = array_merge($actions, [
                'agreement-file-get' => [
                    'class' => file\actions\GetFileAction::className(),
                    'model' => file\File::className(),
                    'idParam' => 'file-id',
                    'fileNameField' => 'filename_full',
                    'folderPath' => function (file\File $model) {
                        return $model->getUploadPath();
                    },
                ],
                'agreement-file-list' => [
                    'class' => file\actions\FileListAction::className(),
                    'model' => Agreement::className(),
                    'fileLinkCallback' => function (file\File $file, Agreement $ownerModel) {
                        return Url::to(['agreement-file-get', 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                    },
                ],
                'agreement-file-delete' => [
                    'class' => file\actions\FileDeleteAction::className(),
                    'model' => Agreement::className(),
                ],
                'agreement-file-upload' => [
                    'class' => file\actions\FileUploadAction::className(),
                    'model' => Agreement::className(),
                    'maxFileCount' => 5,
                    'maxFileSize' => 5 * 1024 * 1024,
                    'folderPath' => 'documents' . DIRECTORY_SEPARATOR . Agreement::$uploadDirectory,
                ],
                'add-modal-contractor' => [
                    'class' => 'frontend\components\AddModalContractorAction',
                ],
            ]);
        }

        return $actions;
    }

    /**
     * Lists all Contractor models.
     *
     * @param            $type
     * @param bool|false $strict
     * @return string
     */
    public function actionIndex($type, $strict = false)
    {
        $type = $this->findType($type);
        $page_type = $type == Contractor::TYPE_CUSTOMER ? PageType::TYPE_CUSTOMER : PageType::TYPE_PROVIDER;
        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE,
            Yii::$app->user->identity->company->company_type_id + 1, $page_type, true);

        $searchModel = new ContractorSearch([
            'is_deleted' => false,
            'company_id' => Yii::$app->user->identity->company->id,
            'type' => $type,
        ]);

        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            StatisticPeriod::getSessionPeriod()
        );

        if (Yii::$app->request->get('xls') && Yii::$app->user->can(UserRole::ROLE_CHIEF)) {
            if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
                Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
                return $this->redirect(['/subscribe/default/index']);
            }
            Yii::$app->response->format = Response::FORMAT_RAW;
            $filename = $type == Contractor::TYPE_CUSTOMER ? 'Список_покупателей' : 'Список_поставщиков';
            $dataProvider->pagination->pageSize = -1;

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = $searchModel->getExcelWriter($dataProvider);
            $writer->save('php://output');
            exit;
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($strict !== false) {
            Yii::$app->session->setFlash(
                'error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' .
                Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => $strict,
                ])) . '.'
            );
        }

        if ($type == Contractor::TYPE_CUSTOMER) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 76);
        }
        if ($type == Contractor::TYPE_SELLER) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 77);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type,
            'prompt' => $prompt,
        ]);
    }

    /**
     * Displays a single Contractor model.
     *
     * @param         $type
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id, $tab = null)
    {
        $model = $this->findModel($id);
        $type = $this->findType($type);
        $ioType = $this->findIoType($type);
        $tab = $this->findTab($model, $tab);
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        switch ($tab) {
            case 'autoinvoice':
                $searchModel = new AutoinvoiceSearch([
                    'company_id' => Yii::$app->user->identity->company->id,
                    'contractor_id' => $model->id,
                ]);
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                $tabData = [
                    'tabFile' => 'view/tab_autoinvoice',
                    'model' => $model,
                    'company' => $company,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'type' => $type,
                ];
                break;
            case 'agreements':
                $searchAgreementModel = new AgreementSearch([
                    'company_id' => $company->id,
                    'contractor_id' => $model->id,
                ]);
                $dataAgreementProvider = $searchAgreementModel->search(Yii::$app->request->queryParams);

                $dataAgreementProvider->pagination->pageSize = \frontend\components\PageSize::get();

                $tabData = [
                    'tabFile' => 'view/tab_agreements',
                    'model' => $model,
                    'company' => $company,
                    'searchAgreementModel' => $searchAgreementModel,
                    'dataAgreementProvider' => $dataAgreementProvider,
                    'type' => $type,
                ];
                break;
            case 'analytics':
                $analysisSearchModel = new AnalysisSearch(['company' => $company]);
                $disciplineSearchModel = new DisciplineSearch(['company' => $company]);

                $tabData = [
                    'tabFile' => 'view/tab_analytics',
                    'model' => $model,
                    'company' => $company,
                    'analyticsMonthNumber' => Yii::$app->request->get('analyticsMonthNumber', date('m')),
                    'analyticsYearNumber' => Yii::$app->request->get('analyticsYearNumber', date('Y')),
                    'abcGroup' => $analysisSearchModel->search()->query->select('abc.group')->andWhere([
                        'contractor.id' => $model->id,
                    ])->scalar(),
                    'disciplineGroup' => $disciplineSearchModel->search()->query->select('calc.group')->andWhere([
                        'contractor.id' => $model->id,
                    ])->scalar(),
                    'type' => $type,
                ];
                break;

            case 'selling':
                if (!isset($ioType)) {
                    $ioType = $type;
                    $this->setIoType($ioType);
                }
                $searchModel = new SellingSearch([
                    'company_id' => $company->id,
                    'contractorId' => $model->id,
                    'ioType' => $ioType,
                ]);
                $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
                $tabData = [
                    'tabFile' => 'view/tab_selling',
                    'model' => $model,
                    'company' => $company,
                    'sellingMonthNumber' => Yii::$app->request->get('sellingMonthNumber', date('m')),
                    'sellingYearNumber' => Yii::$app->request->get('sellingYearNumber', date('Y')),
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'ioType' => $ioType,
                    'type' => $type,
                ];
                break;

            case 'info':
                $contacts = null;

                if (Yii::$app->user->can(permissions\crm\ContactAccess::VIEW, ['contractor' => $model])) {
                    $contacts = ArrayHelper::getColumn(ContactForm::createMultiple($model), 'contact');
                }

                $tabData = [
                    'tabFile' => 'view/tab_customer_info',
                    'model' => $model,
                    'company' => $company,
                    'contacts' => $contacts,
                    'activeTab' => 'default',
                    'is_edit' => Yii::$app->request->get('edit'),
                    'type' => $type,
                ];
                break;

            case 'agent_report':

                AgentReportHelper::generateReportsForAgent($model);

                $searchModel = Documents::loadSearchProvider(Documents::DOCUMENT_AGENT_REPORT, Documents::IO_TYPE_OUT);
                $searchModel->agent_id = $model->id;
                $dateRange = StatisticPeriod::getSessionPeriod();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                $tabData = [
                    'tabFile' => 'view/tab_agent_report',
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'ioType' => Documents::IO_TYPE_OUT,
                    'model' => $model,
                    'company' => $company,
                    'type' => $type,
                ];

                break;

            case 'foreign_invoice':
                if (!isset($ioType)) {
                    $ioType = $type;
                    $this->setIoType($ioType);
                }
                $currency = Yii::$app->request->get('currency');
                if ($currency && !in_array($currency, Currency::$foreignArray)) {
                    throw new NotFoundHttpException();
                }
                $currencyNameArray = array_filter($company->getForeignCurrencyInvoices()->select('currency_name')->andWhere([
                    'contractor_id' => $model->id,
                ])->distinct()->column());
                if (empty($currencyNameArray)) {
                    $currencyNameArray = [Currency::DEFAULT_FOREIGN_CURRENCY];
                }
                $currencyName = $currency ?: reset($currencyNameArray);
                $searchModel = Documents::loadSearchProvider(
                    Documents::DOCUMENT_FOREIGN_CURRENCY_INVOICE,
                    ($type == Contractor::TYPE_CUSTOMER ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN)
                );
                $searchModel = new ForeignCurrencyInvoiceSearch([
                    'type' => ($type == Contractor::TYPE_CUSTOMER ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN),
                    'company_id' => $company->id,
                    'contractor_id' => $model->id,
                    'currency_name' => $currencyName,
                    'employeeCompany' => $employee->currentEmployeeCompany,
                ]);
                $dateRange = StatisticPeriod::getSessionPeriod();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                $tabData = [
                    'tabFile' => 'view/tab_foreign_invoice',
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'ioType' => $type,
                    'currencyNameArray' => $currencyNameArray,
                    'currencyName' => $currencyName,
                    'type' => $type,
                ];
                break;

            case null:
            default:
                if (!isset($ioType)) {
                    $ioType = $type;
                    $this->setIoType($ioType);
                }
                $searchModel = Documents::loadSearchProvider(
                    Documents::DOCUMENT_INVOICE,
                    $ioType
                );
                $searchModel->company_id = Yii::$app->user->identity->company->id;
                $searchModel->contractor_id = $model->id;
                $dateRange = StatisticPeriod::getSessionPeriod();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                $tabData = [
                    'tabFile' => 'view/tab_invoice',
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'ioType' => $ioType,
                    'type' => $type,
                ];
                break;
        }
        if ($type == Contractor::TYPE_CUSTOMER) {
            \common\models\company\CompanyFirstEvent::checkEvent($company, 73);
        }

        return $this->render('view-customer', [
            'type' => $type,
            'ioType' => $ioType,
            'tab' => $tab,
            'model' => $model,
            'company' => $company,
            'tabData' => $tabData,
            'user' => Yii::$app->user,
        ]);
    }

    public function actionOperations($type, $id)
    {
        $model = $this->findModel($id);
        $type = $this->findType($type);
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $searchModel = new CashContractorSearch();
        $dataProvider = $searchModel->searchByContractor($id, Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 50);

        $tabData = [
            'tabFile' => 'view/tab_operations',
            'user' => $employee,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'widgetWallet' => null,
            'contractorType' => $type,
            'contractorId' => $id
        ];

        return $this->render('view-customer', [
            'type' => $type,
            'ioType' => $type,
            'tab' => 'operations',
            'model' => $model,
            'company' => $company,
            'tabData' => $tabData,
            'user' => Yii::$app->user,
        ]);
    }

    /**
     * Creates a new Contractor model.
     * If creation is successful, the browser will be redirected to the 'view'
     * page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var Employee $user */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = Contractor::newModel($company, $employee);
        $model->setScenario('insert');
        $model->type = (int)$this->findType(Yii::$app->request->get('type'));

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ((int)Yii::$app->request->post('Contractor')['face_type'] === Contractor::TYPE_PHYSICAL_PERSON) {
                $model->taxation_system = Contractor::WITHOUT_NDS;
            }
            if ($model->save()) {
                Yii::$app->session->setFlash('success',
                    ($model->type == Contractor::TYPE_CUSTOMER ? 'Покупатель ' : 'Поставщик ') . 'добавлен');

                return $this->redirect(['view', 'type' => $model->type, 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contractor model.
     * If update is successful, the browser will be redirected to the 'view'
     * page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id, $type = null)
    {
        $post = Yii::$app->request->post();
        $model = $this->findModel($id);
        if ($model->company_id === null) {
            throw new ForbiddenHttpException("Контрагент {$model->shortTitle} не может быть изменен.");
        }
        $model->setScenario('insert');

        if ($model->client && $model->client->is_crm_created) {
            $mutator = new ContractorFormMutator($model);
            $mutator->mutateRequiredValidator($model->face_type);
        }

        $clientForm = Yii::$app->user->can(permissions\crm\ClientAccess::EDIT, ['contractor' => $model])
            ? new ClientForm($model)
            : null;

        $contactForms = Yii::$app->user->can(permissions\crm\ContactAccess::EDIT, ['contractor' => $model])
            ? ContactForm::createMultiple($model)
            : null;

        if (Yii::$app->request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->type == Contractor::TYPE_CUSTOMER) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 74);
        }

        $isLoad = [ // Вызвать загрузку всех форм
            $model->load($post) && $model->loadAgent($post), // loadAgent() всегда TRUE?
            $clientForm && $clientForm->load($post),
            $contactForms && ContactForm::loadMultiple($contactForms, $post),
        ];

        if (array_sum($isLoad) > 0) {
            $isValid = [ // Вызвать проверку всех форм
                $model->validate(),
                $model->validateAgent(),
                !$clientForm || $clientForm->validate(),
                !$contactForms || ContactForm::validateMultiple($contactForms),
            ];

            if (array_sum($isValid) == count($isValid)) {
                $model->save();
                $model->saveAgent();

                if ($clientForm) {
                    $clientForm->saveModel();
                }

                if ($contactForms) {
                    array_walk($contactForms, function(ContactForm $contactForm) { $contactForm->updateContact(); });
                }

                Yii::$app->session->setFlash('success',
                    'Информация по ' . (($type ?? $model->type) == Contractor::TYPE_CUSTOMER ? 'покупателю ' : 'продавцу ') . 'изменена');

                // update invoices payment_limit_date
                if (Yii::$app->request->post('customer-payment-delay-update-rule'))
                    self::updateInvoicesPaymentLimitDate($model, Documents::IO_TYPE_OUT);
                if (Yii::$app->request->post('seller-payment-delay-update-rule'))
                    self::updateInvoicesPaymentLimitDate($model, Documents::IO_TYPE_IN);
                
                if (Yii::$app->request->get('returnTo') == 'customer') {
                    return $this->redirect([
                        'view',
                        'type' => $type ?? $model->type,
                        'id' => $model->id,
                        'activeTab' => Contractor::TAB_CONTRACTOR_INFO
                    ]);
                }

                return $this->redirect([
                    'view',
                    'type' => $type ?? $model->type,
                    'id' => $model->id,
                    'tab' => 'info',
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'type' => $type,
            'clientForm' => $clientForm,
            'contactForms' => $contactForms,
        ]);
    }

    public function actionValidateContractor()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Contractor();
        if (isset(Yii::$app->request->post('Contractor')['face_type'])) {
            $scenario = Yii::$app->request->post('Contractor')['face_type'] == 1 ? Contractor::SCENARIO_FIZ_FACE : Contractor::SCENARIO_LEGAL_FACE;
        } else {
            $scenario = Contractor::SCENARIO_LEGAL_FACE;
        }
        $model->setScenario($scenario);
        $model->type = Contractor::TYPE_CUSTOMER;
        $model->status = Contractor::ACTIVE;
        $model->taxation_system = 0;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return [
                'result' => true,
                'name' => $model->face_type == Contractor::TYPE_LEGAL_PERSON ?
                    $model->getNameWithType() :
                    $model->physical_lastname . ' ' . $model->physical_firstname . ' ' . $model->physical_patronymic,
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * Deletes an existing Contractor model.
     * If deletion is successful, the browser will be redirected to the 'index'
     * page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        /* @var Contractor $model */
        $model = $this->findModel($id);

        if ($model->company_id === null) {
            throw new ForbiddenHttpException("Контрагент {$model->shortTitle} не может быть удален.");
        }
        $model->updateAttributes(['is_deleted' => true]);

        Yii::$app->session->setFlash('success',
            ($model->type == Contractor::TYPE_CUSTOMER ? 'Покупатель ' : 'Продавец ') . 'удален');

        return $this->redirect(['index', 'type' => $model->type,]);
    }

    /**
     * Creates a new ContractorAccount model.
     *
     * @param integer $cid
     * @return mixed
     */
    public function actionAccountCreate($cid)
    {
        /* @var Contractor $contractor */
        $contractor = $this->findModel($cid);

        $isForeign = $contractor->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
        $model = new ContractorAccount([
            'contractor_id' => $contractor->id,
            'currency_id' => $isForeign ? Currency::DEFAULT_USD : Currency::DEFAULT_ID,
            'is_foreign_bank' => $isForeign,
        ]);
        $model->populateRelation('contractor', $contractor);

        if (Yii::$app->request->isGet || Yii::$app->request->get('showModal')) {
            return $this->renderAjax('form/_partial_account_form', [
                'model' => $model,
                'contractor' => $contractor,
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $model;
    }

    /**
     * Updates an existing ContractorAccount model.
     *
     * @param integer $cid
     * @param integer $id
     * @return mixed
     */
    public function actionAccountUpdate($cid, $id)
    {
        /* @var Contractor $contractor */
        $contractor = $this->findModel($cid);

        /* @var ContractorAccount $model */
        $model = $this->findAccountModel($contractor, $id);

        if (Yii::$app->request->isGet || Yii::$app->request->get('showModal')) {
            return $this->renderAjax('form/_partial_account_form', [
                'model' => $model,
                'contractor' => $contractor,
            ]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        return $model;
    }

    /**
     * Deletes an existing ContractorAccount model.
     *
     * @param integer $cid
     * @param integer $id
     * @return mixed
     */
    public function actionAccountDelete($cid, $id)
    {
        /* @var Contractor $contractor */
        $contractor = $this->findModel($cid);

        /* @var ContractorAccount $model */
        $model = $this->findAccountModel($contractor, $id);

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $model->delete();
    }

    /**
     * register alert close event
     */
    public function actionAlertClose()
    {
        $user = \Yii::$app->user->identity;
        $user->alert_close_count++;
        $user->alert_close_time = time();
        $user->save(false, ['alert_close_count', 'alert_close_time']);

        echo 1;
        \Yii::$app->end();
    }

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionAgreementCreate($id)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('index');
        }

        /* @var Contractor $model */
        $contractor = $this->findModel($id);
        $model = Agreement::find()->where([
            'contractor_id' => $contractor->id,
            'is_created' => false,
        ])->one();

        if ($model === null) {
            $model = new Agreement([
                'company_id' => $contractor->company_id,
                'contractor_id' => $contractor->id,
                'is_created' => false,
            ]);
        } else {
            foreach ($model->files as $file) {
                $file->delete();
            }
        }
        $model->company_id = $contractor->company_id;
        $model->document_date_input = date('d.m.Y');
        $model->document_name = 'Договор';
        $model->document_number = '';
        $model->created_at = time();
        $model->created_by = Yii::$app->user->id;

        $model->save(false);

        return $this->render('form/_partial_agreement_form', [
            'model' => $model,
            'contractor' => $contractor,
        ]);
    }

    /**
     * Updates an existing Agreement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionAgreementUpdate($id)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('index');
        }

        $model = $this->findAgreementModel($id);
        $isNewRecord = $model->document_number ? false : true;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && ($model->is_created = true) && $model->save(false)) {
            return $this->render('form/_partial_agreement_view', [
                'model' => $model,
                'contractor' => $model->contractor,
                'isNewRecord' => $isNewRecord,
            ]);
        } else {
            return $this->render('form/_partial_agreement_form', [
                'model' => $model,
                'contractor' => $model->contractor,
            ]);
        }
    }

    /**
     * Deletes an existing Agreement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionAgreementDelete($id)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('index');
        }

        $this->findAgreementModel($id)->delete();
        Yii::$app->response->format = Response::FORMAT_JSON;

        return 1;
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionCollate($step, $id)
    {
        if (!in_array($step, CollateForm::$viewArray)) {
            throw new NotFoundHttpException('The requested page does not exist.!');
        }

        $contractor = $this->findModel($id);
        $company = Yii::$app->user->identity->company;
        $today = new \DateTime();
        $dateRange = StatisticPeriod::getSessionPeriod();
        $rangeTo = date_create_from_format('Y-m-d', $dateRange['to']);

        $model = new CollateForm($company, $contractor, [
            'dateFrom' => date_create_from_format('Y-m-d', $dateRange['from'])->format('d.m.Y'),
            'dateTill' => $rangeTo > $today ? $today->format('d.m.Y') : $rangeTo->format('d.m.Y'),
            'dateSigned' => $today->format('d.m.Y'),
        ]);

        $params = [];

        if ($step !== 'form') {
            $model->accounting = Yii::$app->request->getQueryParam('accounting');
            $model->document = Yii::$app->request->getQueryParam('document');
            $model->dateFrom = Yii::$app->request->getQueryParam('from');
            $model->dateTill = Yii::$app->request->getQueryParam('till');
            $model->dateSigned = Yii::$app->request->getQueryParam('signed');
            $model->fillByContractor = Yii::$app->request->getQueryParam('fillByContractor');
            if (!$model->validate()) {
                $model->view = 'form';

                return $this->render($model->viewPath, [
                    'model' => $model,
                ]);
            }
        }

        switch ($step) {
            case 'form':
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $model->view = 'view';
                }
                break;

            case 'send':
                $model->scenario = 'send';
                $model->view = $step;
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $sent = $model->send();
                    Yii::$app->session->setFlash($sent ? 'success' : 'error', "Отправлено писем: $sent");

                    return $this->redirect([
                        'view',
                        'type' => $model->contractor->type,
                        'id' => $model->contractor->id
                    ]);
                }
                break;

            case 'print':
                $model->isPrint = true;
                return $model->getRenderer(PdfRenderer::DESTINATION_BROWSER, $step)->renderHtml();
                break;

            case 'pdf':
                $model->isPrint = true;
                return $model->getRenderer(PdfRenderer::DESTINATION_STRING, $step)->output(true);
                break;

            case 'excel':
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $model->urlTitle . '.xls"');
                header('Cache-Control: max-age=0');
                $writer = $model->excelWriter;
                $writer->save('php://output');
                exit;
                break;

            default:
                $model->view = 'form';
                break;
        }

        return $this->render($model->viewPath, array_merge($params, ['model' => $model]));
    }

    /**
     * @return array
     */
    public function actionCheckAvailability()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $inn = Yii::$app->request->post('inn');
        $type = Yii::$app->request->post('type');
        if ($inn) {
            /* @var $contractor Contractor */
            $contractor = Contractor::find()
                ->byCompany(Yii::$app->user->identity->company->id)
                ->byDeleted()
                ->byStatus(Contractor::ACTIVE)
                ->byContractor($type)
                ->andWhere(['ITN' => $inn])
                ->one();
            if ($contractor) {
                return [
                    'result' => true,
                    'contractorLink' => '<span style="color: #f3565d;">' . ($contractor->type == Contractor::TYPE_CUSTOMER ? 'Покупатель' : 'Поставщик') . ' с таким ИНН уже заведен.</span> ' .
                        Html::a('Перейти в профиль ' . ($contractor->type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика'),
                            Url::to(['view', 'id' => $contractor->id, 'type' => $contractor->type]), [
                                'style' => 'display: block;',
                            ]),
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionIsDifferentAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $contractor = $this->findModel($id);

        return [
            'isDifferent' => !empty($contractor->actual_address) && $contractor->actual_address !== $contractor->legal_address,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionStoreAccount($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $contractor = $this->findModel($id);
        $link = $contractor->getStoreCompanyContractors()->one();
        $activate = (boolean)Yii::$app->request->post('store_account', 0);
        if ($activate) {
            if ($link) {
                $link->updateAttributes([
                    'status' => StoreCompanyContractor::STATUS_ACTIVE,
                ]);
            } else {
                $contractor->createStoreAccount();
            }
        } else {
            if ($link) {
                $link->updateAttributes([
                    'status' => StoreCompanyContractor::STATUS_BLOCKED,
                ]);
            }
        }

        $link = $contractor->getStoreCompanyContractors()->one();
        $error = Yii::$app->session->removeFlash('error');
        $success = Yii::$app->session->removeFlash('success');
        $message = ($link && $link->status == StoreCompanyContractor::STATUS_ACTIVE) ?
            htmlspecialchars('Кабинет покупателя ' . $contractor->getShortName() .
                ' активирован. Ссылка к кабинету, логин и пароль для входа отправлены на почту: ' . $contractor->director_email) :
            'Кабинет покупателя заблокирован.';
        $label = $link ? ($link->status == StoreCompanyContractor::STATUS_ACTIVE ? 'Отключить кабинет' : 'Включить кабинет') : 'Создать кабнет';

        return [
            'value' => $link && $link->status == StoreCompanyContractor::STATUS_ACTIVE ? true : false,
            'message' => $message,
            'label' => $label,
            'canAddStoreCabinet' => $company->canAddStoreCabinet(),
        ];
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionSaleIncrease($activeTab = Contractor::TAB_BENEFIT)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $company->createStoreUser();
        $searchModel = new StoreCompanyContractorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();
        $payments = Payment::find()
            ->select([
                Payment::tableName() . '.*',
                'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
            ])
            ->byCompany($company->id)
            ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
            ->andWhere(['is_confirmed' => true])
            ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
            ->all();
        $companySiteSearch = new CompanySiteSearch([
            'company_id' => $company->id,
        ]);
        $companySiteProvider = $companySiteSearch->search(Yii::$app->request->queryParams);
        $outInvoiceSearch = new OutInvoiceSearch([
            'company_id' => $company->id,
        ]);
        $outInvoiceProvider = $outInvoiceSearch->search(Yii::$app->request->queryParams);
        $donateWidgetSearch = new DonateWidgetSearch([
            'company_id' => $company->id,
        ]);
        $donateWidgetProvider = $donateWidgetSearch->search(Yii::$app->request->queryParams);

        return $this->render('sale-increase', [
            'company' => $company,
            'activeTab' => $activeTab,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payments' => $payments,
            'companySiteSearch' => $companySiteSearch,
            'companySiteProvider' => $companySiteProvider,
            'outInvoiceSearch' => $outInvoiceSearch,
            'outInvoiceProvider' => $outInvoiceProvider,
            'donateWidgetSearch' => $donateWidgetSearch,
            'donateWidgetProvider' => $donateWidgetProvider,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionHasNds($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        return [
            'hasNds' => $model->taxation_system == Contractor::WITH_NDS,
        ];
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangeResponsible($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $responsibleEmployeeID = Yii::$app->request->post('responsibleEmployee');
        if ($responsibleEmployeeID) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $contractor->responsible_employee_id = $responsibleEmployeeID;
                    $contractor->save(true, ['responsible_employee_id']);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Ответственный изменен.');

        $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangeArticle($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $articleID = Yii::$app->request->post('article');
        if ($articleID) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $attr = ($type == Contractor::TYPE_SELLER) ? 'invoice_expenditure_item_id' : 'invoice_income_item_id';
                    if ($contractor->isKub()) {
                        $kubContractorItem = $contractor->getKubItem();
                        $kubContractorItem->{$attr} = $articleID;
                        $kubContractorItem->save(true, [$attr]);
                    } else {
                        $contractor->{$attr} = $articleID;
                        $contractor->save(true, [$attr]);
                    }
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Статья ' . ($type == Contractor::TYPE_SELLER ? 'расхода' : 'прихода') . ' изменена.');

        $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangeIndustry($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $industryID = Yii::$app->request->post('industry');
        if (strlen($industryID)) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $attr = ($type == Contractor::TYPE_SELLER) ? 'seller_industry_id' : 'customer_industry_id';
                    if ($contractor->isKub()) {
                        continue; // todo
                    } else {
                        $contractor->{$attr} = $industryID ?: null;
                        $contractor->save(true, [$attr]);
                    }
                }
            }
            Yii::$app->session->setFlash('success', 'Направление изменено');
        } else {
            Yii::$app->session->setFlash('error', 'Выберите направление');
        }

        $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }


    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangeSalePoint($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $salePointID = Yii::$app->request->post('sale_point');
        if (strlen($salePointID)) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $attr = ($type == Contractor::TYPE_SELLER) ? 'seller_sale_point_id' : 'customer_sale_point_id';
                    if ($contractor->isKub()) {
                        continue; // todo
                    } else {
                        $contractor->{$attr} = $salePointID ?: null;
                        $contractor->save(true, [$attr]);
                    }
                }
            }

            Yii::$app->session->setFlash('success', 'Точка продаж изменена');
        } else {
            Yii::$app->session->setFlash('error', 'Выберите точку продаж');
        }

        $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangePaymentDelay($type)
    {
        $type = $this->findType($type);
        $attr = $type == Contractor::TYPE_SELLER ? 'seller_payment_delay' : 'customer_payment_delay';
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $paymentDelay = (int)Yii::$app->request->post('paymentDelay', null);
        if ($paymentDelay !== null && $paymentDelay >= 0) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $contractor->$attr = $paymentDelay;
                    $contractor->save(true, [$attr]);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Отсрочка платежа изменена.');

        $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangePaymentPriority($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $paymentPriority = (int)Yii::$app->request->post('paymentPriority', null);
        if (in_array($paymentPriority, [Contractor::PAYMENT_PRIORITY_HIGH, Contractor::PAYMENT_PRIORITY_MEDIUM, Contractor::PAYMENT_PRIORITY_LOW])) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $contractor->payment_priority = $paymentPriority;
                    $contractor->save(true, ['payment_priority']);
                }
            }

            Yii::$app->session->setFlash('success', 'Приоритет в оплате изменен.');
        } else {

            Yii::$app->session->setFlash('success', 'Необходимо выбрать приоритет.');
        }

        $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = Yii::$app->request->post('Contractor', []);
        $status = Yii::$app->request->post('status');
        if ($status !== null) {
            foreach ($contractors as $id => $contractor) {
                if ($contractor['checked']) {
                    $contractor = $this->findModel($id);
                    $contractor->status = $status;
                    $contractor->save(true, ['status']);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Статус изменен.');

        $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionCreateAutoinvoice($type, $id)
    {
        $type = $this->findType($type, [Contractor::TYPE_CUSTOMER]);
        $model = $this->findModel($id);

        Yii::$app->session->set('documents.invoice.create-return', Yii::$app->request->referrer);

        $this->redirect([
            '/documents/invoice/create',
            'type' => $type,
            'contractorId' => $id,
            'auto' => 1,
        ]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionFindDuplicate($type, $id = null)
    {
        $type = (int) $this->findType($type);
        $company = Yii::$app->user->identity->company;
        $model = $id ? $this->findModel($id) : new Contractor(['company_id' => $company->id, 'type' => $type]);
        $model->setScenario('insert');
        $model->load(Yii::$app->request->post());
        $contractorArray = $model->getDuplicateQuery()->all();

        if (empty($contractorArray)) {
            throw new NotFoundHttpException();
        }

        return $this->renderAjax('find-duplicate', [
            'type' => $type,
            'model' => $model,
            'contractorArray' => $contractorArray,
        ]);
    }

    /**
     * @param $type
     * @throws NotFoundHttpException
     */
    public function actionMerge($type)
    {
        $type = (int) $this->findType($type);
        $company = Yii::$app->user->identity->company;
        $toId = Yii::$app->request->post('merge_to');
        $modelTo = $this->findModel($toId);
        $fromIds = Yii::$app->request->post('merge_from');
        if (is_array($fromIds) && ($key = array_search($modelTo->id, $fromIds)) !== false) {
            unset($fromIds[$key]);
        }
        $modelFromArray = $modelTo->getDuplicateQuery()->andWhere([
            'id' => $fromIds,
        ])->all();

        if ($modelTo && $modelFromArray) {
            $success = [];
            $failure = [];
            $bothTypes = Yii::$app->request->post('opposite');
            foreach ($modelFromArray as $modelFrom) {
                if (Contractor::merge($modelTo, $modelFrom, $bothTypes)) {
                    $success[] = $modelFrom;
                } else {
                    $failure[] = $modelFrom;
                }
            }
            if ($success) {
                $title = $type == Contractor::TYPE_CUSTOMER ? 'Покупатель' : 'Продавец';
                $msg = $title.' успешно объединен с '.$modelTo->getShortTitle();
                Yii::$app->session->setFlash('success', $msg);

                return $this->redirect([
                    'view',
                    'type' => $type,
                    'id' => $modelTo->id,
                ]);
            }
        }

        $title = $type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'продавца';
        $msg = 'Не удалось объединить '.$title;
        Yii::$app->session->setFlash('error', $msg);

        return $this->redirect(Yii::$app->request->referrer ?: [
            'view',
            'type' => $type,
            'id' => $modelFrom->id,
        ]);
    }

    /**
     * @param integer $type
     * @return integer
     * @throws NotFoundHttpException
     */
    protected function findType($type, $inArray = [Contractor::TYPE_CUSTOMER, Contractor::TYPE_SELLER])
    {
        if (in_array($type, $inArray)) {
            return $type;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return integer|null
     * @throws NotFoundHttpException
     */
    protected function findIoType() : ?int
    {
        $ioType = Yii::$app->request->get('io_type');
        if (!isset($ioType) || in_array($ioType, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            return $ioType;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $type
     * @return integer
     * @throws NotFoundHttpException
     */
    protected function setIoType($ioType)
    {
        $params = Yii::$app->getRequest()->getQueryParams();
        $params['io_type'] = $ioType;
        $_GET['io_type'] = $ioType;
        Yii::$app->getRequest()->getQueryParams($params);
    }

    /**
     * @param integer $type
     * @param string  $tab
     * @return Contractor the loaded model
     * @throws NotFoundHttpException
     */
    protected function findTab($model, $tab)
    {
        if (($model->is_seller && in_array($tab, self::$tabs[Contractor::TYPE_SELLER])) ||
            ($model->is_customer && in_array($tab, self::$tabs[Contractor::TYPE_CUSTOMER]))
        ) {
            return $tab;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Contractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @param bool    $allowDeleted
     * @return Contractor the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null, $allowDeleted = false)
    {
        $query = Contractor::find()
            ->addSelect(['*'])
            ->byCompany(ArrayHelper::getValue(Yii::$app->user->identity, ['company', 'id']))
            ->andWhere([
                Contractor::tableName().'.id' => $id ?: Yii::$app->request->getQueryParam('id'),
            ]);

        if (!$allowDeleted) {
            $query->andWhere(['is_deleted' => false]);
        }

        /* @var Contractor $model */
        $model = $query->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Contractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @param bool    $allowDeleted
     * @return Contractor the loaded model
     * @throws NotFoundHttpException
     */
    protected function findAccountModel(Contractor $contractor, $id)
    {
        /* @var ContractorAccount $model */
        $model = $contractor->getContractorAccounts()->andWhere([
            'id' => $id,
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.!');
        }
    }

    /**
     * Finds the Contractor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @param bool    $allowDeleted
     * @return Contractor the loaded model
     * @throws NotFoundHttpException
     */
    protected function findAgreementModel($id)
    {
        /* @var Contractor $model */
        $model = Agreement::find()
            ->where([
                'id' => $id,
                'company_id' => Yii::$app->user->identity->company->id,
            ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.!');
        }
    }

    private static function updateInvoicesPaymentLimitDate(Contractor $contractor, int $ioType): void
    {
        $contractorId = $contractor->id;

        $paymentDelay = ($ioType == Documents::IO_TYPE_OUT)
            ? intval($contractor->customer_payment_delay ?: 10)
            : intval($contractor->seller_payment_delay ?: 10);

        $paymentDelayUpdateRule = ($ioType == Documents::IO_TYPE_OUT)
            ? intval(Yii::$app->request->post('customer-payment-delay-update-rule'))
            : intval(Yii::$app->request->post('seller-payment-delay-update-rule'));

        switch ($paymentDelayUpdateRule) {
            case self::PAYMENT_DELAY_UPDATE_ALL:
                $byStatuses = [
                    InvoiceStatus::STATUS_CREATED,
                    InvoiceStatus::STATUS_SEND,
                    InvoiceStatus::STATUS_VIEWED,
                    InvoiceStatus::STATUS_APPROVED,
                    InvoiceStatus::STATUS_OVERDUE,
                    InvoiceStatus::STATUS_PAYED_PARTIAL,
                    InvoiceStatus::STATUS_PAYED
                ];
                break;
            case self::PAYMENT_DELAY_UPDATE_UNPAID:
                $byStatuses = [
                    InvoiceStatus::STATUS_CREATED,
                    InvoiceStatus::STATUS_SEND,
                    InvoiceStatus::STATUS_VIEWED,
                    InvoiceStatus::STATUS_APPROVED,
                    InvoiceStatus::STATUS_OVERDUE
                ];
                break;
            default:
                return;
        }

        try {
            self::_updateInvoicesPaymentLimitDate($contractorId, $paymentDelay, $ioType, $byStatuses);
        } catch (\Throwable $e) {
            \Yii::error(__METHOD__."\n".\yii\helpers\VarDumper::dumpAsString($e), 'validation');
        }
    }

    private static function _updateInvoicesPaymentLimitDate(int $contractorId, int $paymentDelay, int $ioType, array $invoiceStatuses): void
    {
        if ($contractorId && $paymentDelay && $ioType && !empty($invoiceStatuses)) {
            $companyId = Yii::$app->user->identity->currentEmployeeCompany->company->id;
            \common\models\document\Invoice::updateAll([
                'payment_limit_date' => new Expression("
                    DATE_ADD([[document_date]], INTERVAL {$paymentDelay} DAY)
                ")
            ],[
                'company_id' => $companyId,
                'contractor_id' => $contractorId,
                'type' => $ioType,
                'invoice_status_id' => $invoiceStatuses,
                'is_deleted' => 0,
            ]);
        }
    }


    public function actionAddGroup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new ContractorGroup([
            'title' => strip_tags(Yii::$app->request->post('title')),
            'company_id' => Yii::$app->user->identity->company->id,
            'type' => Yii::$app->request->post('type')
        ]);
        return $model->save() ? ['itemId' => $model->id, 'itemName' => $model->title] : [];
    }

    /**
     * @return array
     * @throws \yii\db\StaleObjectException
     */
    public function actionGroupDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = ContractorGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Группа не найдена.',
            ];
        }
        if ($model->getContractors(Yii::$app->request->post('type'))
            ->andWhere(['is_deleted' => false])
            ->exists()) {
            return [
                'success' => false,
                'message' => 'Группу удалить нельзя, так как в ней есть пользователи.',
            ];
        }
        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Группа  успешно удалена.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Группу не удалось удалить.',
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionGroupEdit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = ContractorGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Группа товара не найдена.',
            ];
        }
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->title,
            ];
        } else {
            return [
                'success' => false,
                'message' => $model->getFirstError('title'),
            ];
        }
    }

}
