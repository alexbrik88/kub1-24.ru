<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.04.2018
 * Time: 11:32
 */

namespace frontend\controllers;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\service\PaymentType;
use frontend\components\FrontendController;
use frontend\models\PaymentStoreForm;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use frontend\rbac\permissions;
use Yii;

/**
 * Class StorePaymentController
 * @package frontend\controllers
 */
class StorePaymentController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['expose-invoice', 'online-payment'],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param $period
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function actionExposeInvoice($period)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($company->strict_mode) {
            return $this->strictMode();
        }
        $model = new PaymentStoreForm($company);
        if ($model->load(Yii::$app->request->post())) {
            $model->paymentTypeId = PaymentType::TYPE_INVOICE;
            if ($model->validate() && $model->makePayment($period)) {
                if ($fdf = $model->pdfContent) {
                    $uid = uniqid();
                    $session = Yii::$app->session;
                    $session->set('paymentDocument.uid', $uid);
                    $session->set("paymentDocument.{$uid}", $model->pdfContent);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Форма оплаты заполнена неправильно.');
            }
        }

        return $this->redirect(['/documents/invoice/view', 'id' => $model->outInvoice->id, 'type' => $model->outInvoice->type]);
    }

    /**
     * @param $period
     * @return string|\yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function actionOnlinePayment($period)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($company->strict_mode) {
            return $this->strictMode();
        }
        $model = new PaymentStoreForm($company);
        if ($model->load(Yii::$app->request->post())) {
            $model->paymentTypeId = PaymentType::TYPE_ONLINE;
            if ($model->validate() && $model->makePayment($period)) {
                if ($fdf = $model->pdfContent) {
                    $uid = uniqid();
                    $session = Yii::$app->session;
                    $session->set('paymentDocument.uid', $uid);
                    $session->set("paymentDocument.{$uid}", $model->pdfContent);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Форма оплаты заполнена неправильно.');
            }
        }
        $paymentForm = new OnlinePaymentForm([
            'scenario' => OnlinePaymentForm::SCENARIO_SEND,
            'company' => $model->company,
            'payment' => $model->payment,
            'user' => Yii::$app->user->identity ? : $model->company->employeeChief,
        ]);

        return $this->renderPartial('online-payment', [
            'paymentForm' => $paymentForm,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    protected function strictMode()
    {
        Yii::$app->session->setFlash('emptyCompany', Html::tag('div', Html::tag('button', '×', [
                'type' => 'button',
                'class' => 'close',
                'data-dismiss' => 'alert',
                'aria-hidden' => 'true',
                'style' => 'display: block;',
            ]) . 'Данное действие недоступно. ' . Html::a('Заполните информацию о компании и банковские реквизиты.', ['/company/update']), [
            'class' => 'alert-danger alert fade in',
        ]));

        return $this->redirect(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_PAYMENT]);
    }
}