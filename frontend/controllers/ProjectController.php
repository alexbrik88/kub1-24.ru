<?php

namespace frontend\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\AgreementTemplate;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\ContractorAutoProject;
use common\models\document\Invoice;
use common\models\document\status\AgreementStatus;
use common\models\employee\Employee;
use common\models\file\actions\FileDeleteAction;
use common\models\file\actions\FileListAction;
use common\models\file\actions\FileUploadAction;
use common\models\file\actions\GetFileAction;
use common\models\file;
use common\models\project\ProjectCommonCharts;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectCustomerSearch;
use common\models\project\ProjectEmployee;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateForm;
use common\models\project\ProjectEstimateItem;
use common\models\project\ProjectEstimateItemSearch;
use common\models\project\ProjectEstimateItemSendForm;
use common\models\project\ProjectEstimateSearch;
use common\models\project\ProjectSearch;
use frontend\assets\PrintAsset;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use frontend\models\project\ProjectForm;
use frontend\models\project\ProjectTaskForm;
use frontend\models\project\ProjectTaskFormFactory;
use frontend\models\project\ProjectTaskSelectFormFactory;
use frontend\models\project\ProjectTask;
use frontend\models\project\RepositoryFactory;
use frontend\rbac\permissions;
use frontend\themes\kub\helpers\Icon;
use frontend\rbac\UserRole;
use Intervention\Image\Exception\NotWritableException;
use Throwable;
use Yii;
use common\models\project\Project;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use yii\helpers\VarDumper;

/**
 * Class ProjectController
 * @package frontend\controllers
 *
 * * @property-read ProjectTask $task
 */
class ProjectController extends FrontendController
{
    private static $analyticsClasses = [
        'ProfitAndLossSearchModel'
    ];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'accessAuth' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                        ],
                        'roles' => [permissions\Project::INDEX],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'add-modal-contractor',
                            'add-customer',
                            'copy',
                            'append-customer'
                        ],
                        'roles' => [permissions\Project::CREATE],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'view',
                        ],
                        'roles' => [permissions\Project::VIEW],
                        'roleParams' => function(): array {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                            'update-options',
                        ],
                        'roles' => [permissions\Project::UPDATE],
                        'roleParams' => function(): array {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'estimate',
                        ],
                        'roles' => [permissions\Project::UPDATE],
                        'roleParams' => function(): array {
                            if (Yii::$app->request->get('type') == 'create') {
                                return [
                                    'model' => $this->findModel(Yii::$app->request->get('id')),
                                ];
                            } else {
                                return [
                                    'model' => $this->findEstimate(Yii::$app->request->get('id'))->project ?? null,
                                ];
                            }
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'delete-project',
                        ],
                        'roles' => [permissions\Project::DELETE],
                        'roleParams' => function(): array {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                            ];
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'many-delete-project',
                        ],
                        'roles' => [permissions\Project::DELETE],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_FINANCE_DIRECTOR,
                            UserRole::ROLE_FINANCE_ADVISER,
                            UserRole::ROLE_SUPERVISOR,
                            UserRole::ROLE_SALES_SUPERVISOR
                        ],
                    ],
                    // ODDS
                    [
                        'allow' => true,
                        'actions' => [
                            'odds-part',
                            'odds-item-list'
                        ],
                        'roles' => [permissions\Project::VIEW],
                        'roleParams' => function(): array {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('project_id')),
                            ];
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'agreement-file-get' => [
                'class' => GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function (file\File $model) {
                    return $model->getUploadPath();
                },
            ],
            'agreement-file-list' => [
                'class' => FileListAction::className(),
                'model' => Agreement::className(),
                'fileLinkCallback' => function (file\File $file, Agreement $ownerModel) {
                    return Url::to(['agreement-file-get', 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],
            'agreement-file-delete' => [
                'class' => FileDeleteAction::className(),
                'model' => Agreement::className(),
            ],
            'agreement-file-upload' => [
                'class' => FileUploadAction::className(),
                'model' => Agreement::className(),
                'maxFileCount' => 5,
                'maxFileSize' => 5 * 1024 * 1024,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . Agreement::$uploadDirectory,
            ],

            'file-get' => [
                'class' => file\actions\GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function($model) {
                    return $model->getUploadPath();
                },
            ],

            'file-list' => [
                'class' => file\actions\FileListAction::className(),
                'model' => ProjectEstimate::class,
                'fileLinkCallback' => function (file\File $file, $ownerModel) {
                    return Url::to(['file-get', 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],

            'file-delete' => [
                'class' => file\actions\FileDeleteAction::className(),
                'model' => ProjectEstimate::class,
            ],

            'file-upload' => [
                'class' => file\actions\FileUploadAction::className(),
                'model' => ProjectEstimate::class,
                'noLimitCountFile' => true,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . ProjectEstimate::$uploadDirectory,
            ],

            'scan-bind' => [
                'class' => \frontend\modules\documents\components\actions\ScanBindAction::className(),
                'model' => ProjectEstimate::class,
            ],

            'scan-list' => [
                'class' => \frontend\modules\documents\components\actions\ScanListAction::className(),
                'model' => ProjectEstimate::class,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $menuItem = Yii::$app->user->identity->menuItem;
            if (!$menuItem->project_item) {
                $menuItem->updateAttributes(['project_item' => true]);
            }

            return true;
        }

        return false;
    }

    /**
     * @param Project $model
     * @param string $tab
     * @param int $type
     * @return array
     * @throws NotFoundHttpException
     */
    private static function getTabData(Project $model, string $tab, int $type):array
    {
        $company = Yii::$app->user->identity->company;
        
        switch ($tab) {
            case 'estimate':
                $searchModel = new ProjectEstimateSearch(['project_id' => $model->id]);
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                return [
                    'tabFile' => 'view/tab_estimate',
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ];
            case 'info':
                return [
                    'tabFile' => 'view/tab_information',
                    'model' => $model,
                    'is_edit' => Yii::$app->request->get('edit'),
                ];
            case 'description':
                return [
                    'tabFile' => 'view/tab_description',
                    'model' => $model,
                    'is_edit' => Yii::$app->request->get('edit'),
                ];
            case 'commands':

                $employee = ProjectEmployee::find()
                    ->where(['project_id' => $model->id, 'company_id' => $company->id])
                    ->all();

                return [
                    'tabFile' => 'view/tab_commands',
                    'model' => $model,
                    'company' => $company,
                    'employee' => $employee,
                ];
            case 'tasks':
                return [
                    'tabFile' => 'view/tab_tasks',
                    'model' => $model,
                    'company' => $company,
                ];
            case 'analytics':
                $yearSearch = array_filter([
                    ArrayHelper::getValue($_GET, ['OddsSearch', 'year']),
                    ArrayHelper::getValue($_GET, ['ProfitAndLossSearchModel', 'year']),
                    ArrayHelper::getValue($_GET, ['SellingReportSearch2', 'year']),
                    $_GET['year'] ?? null,
                    date('Y'),
                ]);
                $start_year = (int) date_create($model->start_date)->format('Y');
                $current_year = (int) date('Y');
                $yearFilter = range(min($start_year, $current_year), $current_year);
                $year = reset($yearSearch);
                if (!in_array($year, $yearFilter)) {
                    $year = $current_year;
                }

                return [
                    'tabFile' => 'view/tab_analytics',
                    'model' => $model,
                    'company' => $company,
                    'tab' => 'analytics',
                    'subtab' => Yii::$app->request->get('subtab', 'odds'),
                    'year' => $year,
                    'yearFilter' => array_combine($yearFilter, $yearFilter),
                    'activeTab' => Yii::$app->request->get('activeTab'),
                ];
            case 'analytics-by-project':
            case 'analytics-by-estimates':
            case 'analytics-by-labor-costs':
                $searchModel = null;
                $dataProvider = null;
                if ($tab === 'analytics-by-estimates') {
                    $searchModel = new ProjectEstimateSearch(['project_id' => $model->id]);
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                }

                return [
                    'tabFile' => 'view/tab_analytics',
                    'model' => $model,
                    'company' => $company,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'tab' => $tab === 'analytics' || $tab === 'analytics-by-project' ? 'analytics-by-project' : $tab,
                ];
            case 'invoice-incoming':
            case 'invoice-outgoing':
            case 'invoice':
                $searchModel = Documents::loadSearchProvider(
                    Documents::DOCUMENT_INVOICE,
                    ($type == Contractor::TYPE_CUSTOMER ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN)
                );
                $searchModel->company_id = Yii::$app->user->identity->company->id;
                $searchModel->project_id = $model->id;
                $dateRange = StatisticPeriod::getSessionPeriod();
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                return [
                    'tabFile' => 'view/tab_invoice',
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'tab' => $tab === 'invoice' || $tab === 'invoice-outgoing' ? 'invoice-outgoing' : 'invoice-incoming',
                    'ioType' => ($type == Contractor::TYPE_CUSTOMER ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN),
                ];
            case 'money':
            default:
                $searchModel = new CashFlowsProjectSearch();

                $params = \yii\helpers\ArrayHelper::merge($_POST, Yii::$app->request->queryParams, [
                    'company_id' => $company->id,
                    'project_id' => $model->id,
                ]);

                $dataProvider = $searchModel->search($params, StatisticPeriod::getSessionPeriod());
                $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

                return [
                    'tabFile' => 'view/tab_money',
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'company' => $company,
                    'ioType' => $type,
                ];
        }
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $userConfig = Yii::$app->user->identity->config;
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        // charts
        $palModel = &$searchModel::$pal; // ProjectCommonCharts::getPalModel()
        $analyticsModel = ProjectCommonCharts::getAnalyticsModel();

        return $this->render('index', [
            'model' => $this,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'userConfig' => $userConfig,
            'palModel' => $palModel,
            'analyticsModel' => $analyticsModel
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $type = Yii::$app->request->get('type') ?: Contractor::TYPE_SELLER;
        $tab = Yii::$app->request->get('tab') ?: 'money';

        $model = $this->findModel($id);
        $modelOptions = $this->getModelOptions($model);

        $company = Yii::$app->user->identity->company;

        return $this->render('view', [
            'ioType' => $type,
            'company' => $company,
            'model' => $model,
            'modelOptions' => $modelOptions,            
            'tab' => $tab,
            'tabData' => self::getTabData($model, $tab, $type),
        ]);
    }

    /**
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreate($copy = null)
    {
        $employee = Yii::$app->user->identity;
        $model = new ProjectForm($employee->currentEmployeeCompany);

        if ($copy) {
            $model->copy($this->findModel($copy));
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['project/view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $employee = Yii::$app->user->identity;
        $model = new ProjectForm($employee->currentEmployeeCompany, $this->findModel($id));

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['project/view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionAddCustomer($cid)
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $contractor = $cid ? Contractor::find()->andWhere([
            'id' => $cid,
        ])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $company->id],
        ])->one() : null;
        if ($contractor === null) {
            throw new NotFoundHttpException();
        }
        $model = new ProjectForm($employee->currentEmployeeCompany);
        $customer = $model->addCustomer($contractor);

        return $this->renderPartial('partial/_project_form_customers_row', [
            'key' => 0,
            'model' => $model,
            'customer' => $customer,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @param $clone
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionEstimate($id, $type, $clone = null)
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        if ('create' == $type) {
            if ($clone) {
                $projectId = $id;
                $cloningEstimateId = $clone;
                $model = $this->cloneEstimate($projectId, $cloningEstimateId);
            } else {
                $model = new ProjectEstimate(['project_id' => $id]);
                $model->number = $model->getNextNumber();
                $model->customer_id = $model->project->customer_id;
            }
        } else {
            $model = $this->findEstimate($id);
        }

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $searchModel = new ProjectEstimateItemSearch(['project_estimate_id' => $model->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('estimate', [
            'model' => $model,
            'type' => $type,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteProject($id)
    {
        $result = LogHelper::delete($this->findModel($id), LogEntityType::TYPE_PROJECT,
            function ($model) {
                if ($this->unlinkProjectFlowsAndInvoices($model))
                    if ($model->delete())
                        return true;

                return false;
            });

        if ($result)
            Yii::$app->session->setFlash('success', 'Проект удален');
        else
            Yii::$app->session->setFlash('error', 'Ошибка при удалении проекта');

        return $this->redirect(['index']);
    }

    /**
     * @return Response
     * @throws Exception
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteProject()
    {
        $result = true;
        $ids = Yii::$app->request->post('id', []);
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $model = $this->findModel($id);
                if ($this->unlinkProjectFlowsAndInvoices($model)) {
                    if ($model->delete()) {
                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);
                        continue;
                    }
                }

                $result = false;
                break;
            }
        }

        if ($result)
            Yii::$app->session->setFlash('success', 'Проекты удалены');
        else
            Yii::$app->session->setFlash('error', 'Ошибка при удалении проектов');

        return $this->redirect(Yii::$app->request->referrer ?: 'index');
    }

    /**
     * @param $id
     * @return false|Response
     * @throws \Exception
     */
    public function actionDeleteCashOrderFlows($id) {
        $cashOrderFlows = CashOrderFlows::findOne(['id' => $id]);
        if (LogHelper::delete($cashOrderFlows, LogEntityType::TYPE_PROJECT,
            function ($model) {
                return $model->delete();
            })
        ) {
            Yii::$app->session->setFlash('success', 'Операция удалена');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return false;
    }

    /**
     * @param $id
     * @return false|Response
     * @throws \Exception
     */
    public function actionDeleteCashBankFlows($id) {
        $cashBankFlows = CashBankFlows::findOne(['id' => $id]);
        if (LogHelper::delete($cashBankFlows, LogEntityType::TYPE_PROJECT,
            function ($model) {
                return $model->delete();
            })
        ) {
            Yii::$app->session->setFlash('success', 'Операция удалена');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return false;
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBasisDocument()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        $contractorId = (int)Yii::$app->request->get('contractorId');
        $container = Yii::$app->request->get('container');

        $user = Yii::$app->user->identity;
        $company = $user->company;

        $contractor = Contractor::findOne([
            'id' => $contractorId,
            'company_id' => $company->id]
        );

        $agreementDropDownList = [];
        $companyAgreementID = [];

        $model = new Project();

        if (!empty($contractor)) {
            $agreementArray = Agreement::find()
                ->joinWith('agreementType')
                ->andWhere(['company_id' => $company->id])
                ->andWhere(['contractor_id' => $contractor->id])
                ->orderBy([
                    'agreement_type.name' => SORT_ASC,
                    'agreement.document_date' => SORT_DESC,
                    'agreement.id' => SORT_DESC,
                ])
                ->all();
            $agreementDropDownList += ['add-modal-agreement' => Icon::PLUS . ' Добавить договор '];

            /** @var $agreement \common\models\Agreement */
            foreach ($agreementArray as $agreement) {
                $agreementDropDownList[$agreement->id] = '№ ' . $agreement->document_number . ' от '
                    . DateHelper::format(
                        $agreement->document_date,
                        DateHelper::FORMAT_USER_DATE,
                        DateHelper::FORMAT_DATE
                    );
                $companyAgreementID[$agreement->getListItemValue()] = $agreement->document_number;
            }

        }

        return $this->render('partial/_basis_document', [
            'model' => $model,
            'contractor_id' => $contractorId,
            'pjaxId' => $container,
            'agreementDropDownList' => $agreementDropDownList,
            'companyAgreementID' => $companyAgreementID,
        ]);
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionCreateBasisDocument($type = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['index', 'type' => $type]);
        }

        $model = Agreement::find()->where([
            'created_by' => Yii::$app->user->id,
            'company_id' => Yii::$app->user->identity->company->id,
            'is_created' => false,
        ])->one();

        if ($model === null) {
            $model = new Agreement([
                'is_created' => false,
                'created_at' => time(),
                'created_by' => Yii::$app->user->id,
                'company_id' => Yii::$app->user->identity->company->id,
                'document_date' => date(DateHelper::FORMAT_DATE),
                'document_number' => '',
                'document_name' => '',
            ]);

            $model->save(false);

        } else {
            foreach ($model->files as $file) {
                $file->delete();
            }
        }

        $fixed_template = Yii::$app->request->get('from_template');
        $fixed_contractor_id = Yii::$app->request->get('contractor_id');
        $fixed_type = Yii::$app->request->get('type');

        $model->create_agreement_from = 0;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_name = '';
        $model->document_number = '';
        $model->contractor_id = $fixed_contractor_id;
        $model->type = $fixed_type;

        if ($fixed_template) {
            $model->agreement_template_id = (int)$fixed_template;
            $model->create_agreement_from = 1;
        }

        if ($templateDocumentTypeId = Yii::$app->request->get('templateDocumentTypeId')) {
            $model->document_type_id = $templateDocumentTypeId;
            $model->create_agreement_from = 1;
        }

        return $this->renderAjax('partial/_documentForm', [
            'model' => $model,
            'fixedContractor' => $fixed_contractor_id ? true : false,
            'fixedType' => $fixed_type ? true : false,
            'fixedTemplate' => $fixed_template,
            'newRecord' => true,
            'disableAllFields' => true,
            'templateDocumentTypeId' => $templateDocumentTypeId
        ]);
    }

    /**
     * @param $id
     * @param null $type
     * @return false|string
     * @throws \Exception
     */
    public function actionUpdateBasisDocument($id, $type = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['index', 'type' => $type]);
        }

        $newRecord = Yii::$app->request->get('old_record') ? false : true;

        /** @var $model Agreement */
        $model = Agreement::find()
            ->where([
                'id' => $id,
                'company_id' => Yii::$app->user->identity->company->id,
            ])
            ->one();

        if ($fixed_contractor = Yii::$app->request->get('contractor_id', false))
            $model->contractor_id = $fixed_contractor;
        else
            $model->contractor_id = Yii::$app->request->post('contractor_id', false);

        if ($fixed_type = Yii::$app->request->get('type', false))
            $model->type = $fixed_type;
        else
            $model->type = Yii::$app->request->post('type', false);

        if ($fixed_template = Yii::$app->request->get('from_template', false)) {
            $model->create_agreement_from = 1;
            $model->agreement_template_id = (int)$fixed_template;
        } else {
            $postAgreement = Yii::$app->request->post('Agreement');
            if (isset($postAgreement['create_agreement_from']))
                $model->create_agreement_from = $postAgreement['create_agreement_from'];
            if (isset($postAgreement['agreement_template_id']))
                $model->agreement_template_id = $postAgreement['agreement_template_id'];
        }

        if ($model->agreement_template_id > 0) {
            $tplModel = AgreementTemplate::findOne([
                'id' => $model->agreement_template_id,
                'company_id' => Yii::$app->user->identity->company->id,
            ]);
            $model->document_type_id = ($tplModel) ? $tplModel->document_type_id : $model->document_type_id;
        }

        $returnTo = Yii::$app->session->get('return_from_agreement') ? : Yii::$app->request->get('returnTo');

        $model->is_created = true;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            if ($model->create_agreement_from == Agreement::CREATE_AGREEMENT_FROM_TEMPLATE) {
                $model->generateEssence();
            }

            LogHelper::save($model, LogEntityType::TYPE_PROJECT, ($newRecord ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE), function ($model) {
                return (boolean)$model->updateAttributes([
                    'status_id' => AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });

            return json_encode(['success' => 'true', 'data' => ['agreement' => $model->id]]);
        }

        return $this->render('partial/_documentForm', [
            'model' => $model,
            'fixedContractor' => (bool)$fixed_contractor,
            'fixedType' => (bool)$fixed_type,
            'fixedTemplate' => (bool)$fixed_template,
            'newRecord' => $newRecord,
            'disableAllFields' => false,
            'returnTo' => $returnTo
        ]);
    }

    /**
     * @return string
     */
    public function actionGetEmployer() {
        $userConfig = Yii::$app->user->identity->config;
        $company = $userConfig->employee->company;

        $employers = ArrayHelper::map(Employee::find()
            ->where(['company_id' => $company->id])
            ->all(), 'id', 'fio');

        return $this->renderAjax('partial/_responsibleField', [
            'employers' => $employers,
        ]);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeResponsible() {
        $ids = \Yii::$app->request->post('id', []);
        $responsible = \Yii::$app->request->post('responsible', false);

        foreach ($ids as $id) {
            $project = $this->findModel($id);
            $project->responsible = intval($responsible);
            $project->save(true, ['responsible']);
        }

        Yii::$app->session->setFlash('success', 'Ответственный изменен.');

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus() {
        $ids = \Yii::$app->request->post('id');
        $status = \Yii::$app->request->post('status');

        if (strlen($status)) {
            foreach ((array)$ids as $id) {
                $project = $this->findModel($id);
                $project->status = intval($status);
                if (LogHelper::save($project, LogEntityType::TYPE_PROJECT, LogEvent::LOG_EVENT_UPDATE_STATUS)) {
                    Yii::$app->session->setFlash('success', 'Статус изменен.');
                };
            }
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionChangeEndDate() {

        $ids = \Yii::$app->request->post('id');
        $date = \Yii::$app->request->post('end_date');

        if (strlen($date) && ($d = date_create_from_format('d.m.Y', $date))) {
            foreach ((array)$ids as $id) {
                $project = $this->findModel($id);
                $project->end_date = $d->format('Y-m-d');
                if ($project->validate()) {
                    if (LogHelper::save($project, LogEntityType::TYPE_PROJECT, LogEvent::LOG_EVENT_UPDATE_STATUS)) {
                        Yii::$app->session->setFlash('success', 'Дата окончания изменена.');
                    };
                } else {
                    Yii::$app->session->setFlash('error', $project->getFirstError('end_date'));
                    break;
                }
            }
        } else {
            Yii::$app->session->setFlash('error', 'Неверный формат даты');
        }

        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateOptions($id)
    {
        set_time_limit(90);

        $project = $this->findModel($id);
        $disabledContractorIds = ProjectCustomer::find()->alias('c')
            ->joinWith('project p')
            ->where(['not', ['c.project_id' => $project->id]])
            ->andWhere(['p.status' => Project::STATUS_INPROGRESS])
            ->andWhere(['c.customer_id' => ArrayHelper::getColumn($project->contractors, 'id')])
            ->select('c.customer_id')
            ->column();

        $transaction = Yii::$app->db->beginTransaction();

        try {

            foreach ($project->contractors as $contractor) {

                $modelOptions = $this->getModelOptions($project, $contractor->id);

                if (in_array($contractor->id, $disabledContractorIds)) {
                    $modelOptions->delete();
                    continue;
                }

                if ($modelOptions->load(Yii::$app->request->post())) {
                    if ($modelOptions->save()) {
                        continue;
                    } else {
                        $transaction->rollBack();
                        break;
                    }
                }
            }

            $commited = $transaction->commit() || 1;

        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::error(VarDumper::dumpAsString($e->getMessage()), 'validation');
        }

        if (isset($commited)) {
            Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось сохранить настройки');
            if (isset($modelOptions))
                Yii::error(VarDumper::dumpAsString($modelOptions->getErrors()), 'validation');
        }

        return $this->redirect(Yii::$app->request->referrer ?: 'index');
    }

    /**
     * @return string[]
     * @throws ForbiddenHttpException
     */
    public function actionProjectEstimateChangeItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $data = Yii::$app->request->post('ProjectEstimateForm');
        $form = new ProjectEstimateForm([
            'company' => $company,
        ]);

        foreach ($data as $formData) {
            if ($form->load($formData, '') && $form->change()) {
                Yii::$app->session->setFlash('success', 'Смета обновлена');
            } else {
                Yii::$app->session->setFlash('success', 'Ошибка обновления');
                return ['success' => 'false'];
            }
        }

        return ['success' => 'true'];
    }

    /**
     * @param null $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionProjectEstimateFormValidate($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = ($id)
            ? $this->findEstimate($id)
            : new ProjectEstimate();

        $model->load(Yii::$app->request->post());
        $model->validate();

        $result = [];
        foreach ($model->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($model, $attribute)] = $errors;
        }

        return $result;
    }

    public function actionProjectEstimateCreate()
    {
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = new ProjectEstimate();
        $isLoaded = $model->load(Yii::$app->request->post());

        if ($isLoaded) {

            if (!Project::find()->where(['company_id' => $company->id, 'id' => $model->project_id])->exists())
                throw new Exception('Project not found');

            if ($model->validate()) {

                if ($model->_save()) {
                    Yii::$app->session->setFlash('success', 'Смета успешно создана.');
                    return $this->redirect(Url::to(['project/estimate', 'id' => $model->id, 'type' => 'view']));
                }
            }
        }

        Yii::$app->session->setFlash('error', 'Ошибка создания сметы.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionProjectEstimateUpdate($id)
    {
        $model = $this->findEstimate($id);

        if ($model->load(Yii::$app->request->post()) && $model->_save()) {
            Yii::$app->session->setFlash('success', 'Смета успешно обновлена.');
        }

        return $this->redirect(Url::to(['project/estimate', 'id' => $model->id, 'type' => 'view']));
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionGetProjectEstimateOrder()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = (int)Yii::$app->request->post('estimate', null);
        $type = (int)Yii::$app->request->post('type', null);

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $projectEstimate = $this->findEstimate($id);

        if (!$projectEstimate) {
            throw new NotFoundHttpException("Смета не найдена.");
        }

        return ['success' => 'true', 'data' => $projectEstimate->getEstimateProducts($company, $type)];
    }

    /**
     * @return false|string
     * @throws \Throwable
     */
    public function actionManyDelete() {
        $ids = \Yii::$app->request->post('id');
        $typeArray = \Yii::$app->request->post('type');

        $projectId = \Yii::$app->request->post('project_id');

        $db = \Yii::$app->getDb();

        $transactions = $db->beginTransaction();

        try {
            for ($i = 1; $i <= count($ids); $i++) {
                if ($typeArray[$i] == CashFlowsProjectSearch::TYPE_PAYMENT_BANK) {
                    $model = CashBankFlows::find()
                        ->where(['id' => $ids[$i]])
                        ->one();
                } elseif ($typeArray[$i] == CashFlowsProjectSearch::TYPE_PAYMENT_ORDER) {
                    $model = CashOrderFlows::find()
                        ->where(['id' => $ids[$i]])
                        ->one();
                }

                if (!$model->delete()) {
                    throw new \Exception("Delete fail");
                };
            }

            $transactions->commit();

        } catch (\Exception $e) {
            $transactions->rollBack();

            return json_encode([
                'success' => false
            ]);
        }

        return json_encode([
            'success' => true,
            'href' => '/project/view?id=' . $projectId,
        ]);
    }

    public function actionManyDeleteEstimateItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ids = Yii::$app->request->post('ids', []);
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $db = Yii::$app->getDb();
        $transactions = $db->beginTransaction();

        try {
            foreach ($ids as $id) {
                $model = ProjectEstimateItem::findOne(['id' => $id]);
                if (!$model) {
                    continue;
                }

                if ($model->projectEstimate->project->company_id !== $company->id) {
                    throw new ForbiddenHttpException('Access denied');
                }

                $model->delete();
            }

            $transactions->commit();

        } catch (\Exception $e) {
            $transactions->rollBack();
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    public function actionManyDeleteProjectEstimateItem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ids = Yii::$app->request->post('ids');
        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $db = Yii::$app->getDb();
        $transactions = $db->beginTransaction();

        try {
            foreach ($ids as $id) {
                $model = $this->findEstimate($id, true);
                if (!$model) {
                    continue;
                }

                if ($model->project->company_id !== $company->id) {
                    throw new ForbiddenHttpException('Access denied');
                }

                /** @var ProjectEstimateItem $items */
                foreach ($model->projectEstimateItems as $items) {
                    $items->delete();
                }

                if (!$model->delete()) {
                    throw new \Exception("Delete fail");
                };
            }

            $transactions->commit();

        } catch (\Exception $e) {
            $transactions->rollBack();
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    /**
     * @return false|string
     */
    public function actionChangeManyItem() {
        $ids = \Yii::$app->request->get('id');
        $type = \Yii::$app->request->get('type');

        $projectId = \Yii::$app->request->get('project_id');

        $incomeBankItemValue = \Yii::$app->request->post('incomeBankManyItem', false);
        $incomeOrderItemValue = \Yii::$app->request->post('incomeOrderManyItem', false);
        $expenseBankItemValue = \Yii::$app->request->post('expenseBankItem', false);
        $expenseOrderItemValue = \Yii::$app->request->post('expenseOrderItem', false);

        switch (true) {
            case $incomeBankItemValue:
                $property = 'income';
                $item = $incomeBankItemValue;
                break;
            case $incomeOrderItemValue:
                $property = 'income';
                $item = $incomeOrderItemValue;
                break;
            case $expenseBankItemValue:
                $property = 'expense';
                $item = $expenseBankItemValue;
                break;
            case $expenseOrderItemValue:
                $property = 'expense';
                $item = $expenseOrderItemValue;
                break;
        }

        $db = \Yii::$app->getDb();
        $transaction = $db->beginTransaction();
        try {
            foreach ($ids as $id) {
                if ($type == CashFlowsProjectSearch::TYPE_PAYMENT_BANK) {
                    /** @var CashBankFlows $cashFlowsBase */
                    $cashFlowsBase = CashBankFlows::find()->where(['id' => $id])->one();
                } else {
                    /** @var CashOrderFlows $cashFlowsBase */
                    $cashFlowsBase = CashOrderFlows::find()->where(['id' => $id])->one();
                }

                if ('income' === $property) {
                    $cashFlowsBase->income_item_id = $item;
                } else if('expense' === $property) {
                    $cashFlowsBase->expenditure_item_id = $item;
                }

                $cashFlowsBase->save(false);
            }

            $transaction->commit();

            return json_encode([
                'success' => 'true',
                'href' => '/project/view?id=' . $projectId
            ]);
        } catch (\Exception $e) {
            echo $e->getMessage();

            $transaction->rollBack();
        }

        return json_encode([
            'success' => 'false',
        ]);
    }

    public function actionAppendCustomer()
    {
        $companyId = Yii::$app->user->identity->currentEmployeeCompany->company->id;
        $project_id = (int)Yii::$app->request->post('project_id', null);
        $customer_id = (int)Yii::$app->request->post('customer_id', null);
        $agreement_id = (int)Yii::$app->request->post('agreement_id', null);
        $amount = (int)Yii::$app->request->post('amount', null);

        if ($customer_id == 0) {
            throw new NotWritableException('Введены некорректные данные');
        }

        // create

        if (!$project_id) {
            return $this->renderAjax('@frontend/themes/kub/views/project/partial/_basis_customer_row_create', [
                'company_id' => (int)$companyId,
                'contractor_id' => (int)$customer_id,
                'agreement_id' => (int)$agreement_id,
                'amount' => $amount,
                'canDelete' => true,
            ]);
        }

        // update

        $transactions = Yii::$app->getDb()->beginTransaction();

        $projectCustomer = new ProjectCustomer([
            'project_id' => $project_id,
            'customer_id' => $customer_id,
        ]);

        if ($amount) {
            $projectCustomer->amount = $amount;
        }

        if ($agreement_id) {
            $projectCustomer->agreement_id = $agreement_id;
        }

        if ($projectCustomer->save()) {
            $transactions->commit();
        } else {
            $transactions->rollBack();
            return "";
        }

        $searchModel = new ProjectCustomerSearch();
        $item = $searchModel
            ->getBaseQuery(['id' => $project_id])
            ->andWhere([ProjectCustomer::tableName() . '.id' => $projectCustomer->id])
            ->one();

        return $this->renderAjax('@frontend/themes/kub/views/project/partial/_basis_customer_row', [
            'item' => $item,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAppendProjectCustomer()
    {
        $id = Yii::$app->request->post('id', null);

        $model = $this->findModel($id);

        return $this->renderAjax('@frontend/themes/kub/views/project/partial/_basis_append_customer_row', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionUpdateProjectCustomer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id', null);

        $project_id = (int)Yii::$app->request->post('project_id', null);
        $customer_id = (int)Yii::$app->request->post('customer_id', null);
        $agreement_id = (int)Yii::$app->request->post('agreement_id', null);
        $amount = (int)Yii::$app->request->post('amount', null);

        $projectCustomer = ProjectCustomer::findOne(['id' => $id]);

        if ($projectCustomer && $project_id) {
            $projectCustomer->project_id = $project_id;
        }

        if ($projectCustomer && $customer_id) {
            $projectCustomer->customer_id = $customer_id;
        }

        if ($projectCustomer && $agreement_id) {
            $projectCustomer->agreement_id = $agreement_id;
        }

        $projectCustomer->amount = $amount;

        if ($projectCustomer && $projectCustomer->save()) {
            return [
                'agreement' => $projectCustomer->agreement ? $projectCustomer->agreement->getTitle() : null,
                'amount' => TextHelper::invoiceMoneyFormat($projectCustomer->amount, 2),
            ];
        }

        return [];
    }

    /**
     * @return string[]
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionProjectCustomerDelete($project_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id', null);
        $project = $this->findModel($project_id);
        $customer = ProjectCustomer::findOne(['id' => $id, 'project_id' => $project->id]);

        if ($customer && $customer->delete()) {

            self::unlinkCustomerFlowsAndInvoices($customer);

            return ['success' => 'true'];
        }

        return ['success' => 'false'];
    }

    public function actionManyDeleteProjectCustomer($project_id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $project = $this->findModel($project_id);
        $projectCustomers = Yii::$app->request->post('ProjectCustomer', []);

        if (is_array($projectCustomers)) {

            $realProjectCustomersCount = ProjectCustomer::find()->where(['project_id' => $project->id])->count();

            if (count($projectCustomers) < $realProjectCustomersCount) {
                foreach ($projectCustomers as $id => $projectCustomer) {
                    if ($projectCustomer['checked']) {
                        /* @var $model ProjectCustomer */
                        $customer = ProjectCustomer::findOne(['id' => $id, 'project_id' => $project->id]);
                        if ($customer && $customer->delete()) {

                            self::unlinkCustomerFlowsAndInvoices($customer);

                            continue;
                        } else {
                            Yii::$app->session->setFlash('error', 'Невозможно удалить заказчиков');
                            break;
                        }
                    }
                }

                Yii::$app->session->setFlash('success', 'Заказчики успешно удалены');

            } else {

                Yii::$app->session->setFlash('error', 'Нельзя удалить всех заказчиков');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index']);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBasisAgreementField()
    {
        $id = Yii::$app->request->post('id', null);
        $customerId = Yii::$app->request->post('customer_id', null);

        $model = $this->findModel($id);

        return $this->renderAjax('@frontend/themes/kub/views/project/partial/_basis_agreement_field', [
            'model' => $model,
            'customerId' => $customerId,
        ]);
    }

    /**
     * @return Response
     */
    public function actionCreateTask()
    {
        $projectId = Yii::$app->request->get('project_id');

        $form = (new ProjectTaskFormFactory)->modelCreate();
        $form->setAttribute('project_id', $projectId);

        $hasProject = $projectId ? true : false;

        return $this->handleForm($form, $hasProject);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateTask(): Response
    {
        $task_id = (int)Yii::$app->request->get('task_id');
        /** @var ProjectTask $task */
        $task = (new RepositoryFactory)->createProjectTaskRepository()->getById($task_id);

        if (!$task) {
            throw new NotFoundHttpException('Задача не найдена.');
        }

        return $this->handleForm((new ProjectTaskFormFactory())->modelUpdate($task), true);
    }

    /**
     * @return string
     */
    public function actionManyUpdateTask()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $taskSelectForm = (new ProjectTaskSelectFormFactory)->createForm();

        if ($taskSelectForm->load(Yii::$app->request->post()) && $taskSelectForm->updateModels()) {
            Yii::$app->session->setFlash('success', 'Задачи успешно обновлены.');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @param $actionType
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProjectEstimateItemPrint($id, $actionType, $type)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        /** @var Company $company */
        $company = $user->company;

        $model = $this->findEstimate($id);

        if (!$company->id === $model->project->company->id) {
            throw new NotFoundHttpException('Смета не найдена');
        }

        $renderer = new PdfRenderer([
            'view' => '@frontend/themes/kub/views/project/view/estimate/pdf-view',
            'params' => array_merge([
                'model' => $model,
                'actionType' => $actionType,
                'type' => $type,
                'estimateId' => $id,
            ]),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $model->getPrintTitle(),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);

        $this->view->params['asset'] = PrintAsset::class;
        if ($this->action->id != 'out-view') {
            Yii::$app->view->registerJs('window.print();');
        }

        return $renderer->renderHtml();
    }

    /**
     * @param $id
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|\yii\console\Response|Response
     * @throws NotFoundHttpException
     * @throws \Mpdf\MpdfException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionProjectEstimateDownloadPdf($id, $actionType, $type)
    {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        /** @var Company $company */
        $company = $user->company;

        $model = $this->findEstimate($id);

        if (!$company->id === $model->project->company->id) {
            throw new NotFoundHttpException('Смета не найдена');
        }

        $renderer = new PdfRenderer([
            'view' => '@frontend/themes/kub/views/project/view/estimate/pdf-view',
            'params' => array_merge([
                'model' => $model,
                'actionType' => $actionType,
                'type' => $type,
                'estimateId' => $id,
            ]),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $model->getPdfFileName(),
            'displayMode' => PdfRenderer::DISPLAY_MODE_DEFAULT,
            'format' => 'A4-L',
        ]);

        $this->view->params['asset'] = PrintAsset::class;

        return $renderer->output();
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetPermanentFiles($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $estimateItems = Yii::$app->request->post('estimateItems', []);
        $type = (int)Yii::$app->request->post('type');

        $user = Yii::$app->user->identity;
        /** @var Company $company */
        $company = $user->company;

        $model = $this->findEstimate($id);

        if (!$model) {
            throw new NotFoundHttpException('Смета не найдена');
        }

        if (!$company->id == $model->project->company_id) {
            throw new NotFoundHttpException('Смета не принадлежит вашей компании');
        }

        return [
            'result' => true,
            'files' => $model->loadEmailFiles($id, $type),
        ];
    }

    /**
     * @return Response
     * @throws \yii\db\StaleObjectException
     */
    public function actionSendProjectEstimate()
    {
        $model = new ProjectEstimateItemSendForm();

        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            Yii::$app->session->setFlash('success', 'Смета отправлена');
        } else {
            Yii::$app->session->setFlash('success', 'Отправка завершилась неудачей');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param ProjectTaskForm $form
     * @param bool $hasClient
     * @return Response
     */
    private function handleForm(ProjectTaskForm $form, bool $hasProject): Response
    {
        if ($form->load(Yii::$app->request->post()) && $form->validate() && $form->saveModel()) {
            return $this->redirect(Yii::$app->request->referrer);
        }

        Yii::$app->response->content = $this->renderAjax('view/tasks/task-form', compact('form', 'hasProject'));

        return Yii::$app->response;
    }

    public function actionCopy($id) {
        $model = $this->findModel($id);

        return $this->redirect(['create', 'copy' => $model->id]);
    }

    /**
     * @param $id
     * @return Project
     * @throws NotFoundHttpException
     */
    public function cloneRequest($id): Project
    {
        $model = $this->findModel($id);
        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->name = null;
        $clone->start_date = null;
        $clone->end_date = null;
        $clone->number = Project::getNextProjectNumber($model->company);

        return $clone;
    }

    /**
     * @param $id
     * @return Project
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Project::find()
            ->where(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->with('contractors')
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested project does not exist.');
        }
    }

    /**
     * @param $id
     * @return ProjectEstimate|null
     * @throws NotFoundHttpException
     */
    protected function findEstimate($id, $skipIfEmpty = false)
    {
        $model = ProjectEstimate::find()->andWhere([
            ProjectEstimate::tableName() . '.id' => $id,
        ])->byCompany(Yii::$app->user->identity->company->id)->one();

        if ($model !== null) {
            return $model;
        } elseif ($skipIfEmpty) {
            return null;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $project
     * @param $contractorId
     * @return ContractorAutoProject
     */
    private function getModelOptions(Project $project, $contractorId = null): ContractorAutoProject
    {
        return 
            $this->findModelOptions($project, $contractorId) 
                ?: $this->newModelOptions($project, $contractorId);
    }
    private function findModelOptions(Project $project, $contractorId = null)
    {
        return ContractorAutoProject::find()
            ->where(['project_id' => $project->id])
            ->andFilterWhere(['contractor_id' => $contractorId])
            ->one();
    }
    private function newModelOptions(Project $project, $contractorId = null)
    {
        return new ContractorAutoProject([
            'project_id' => $project->id,
            'contractor_id' => $contractorId,
            'project_start' => $project->start_date,
            'project_end' => $project->end_date,
            'project_status' => $project->status,
        ]);
    }

    /**
     * @param $projectId
     * @param $id
     * @return ProjectEstimate|null
     * @throws NotFoundHttpException
     */
    protected function cloneEstimate($projectId, $id)
    {
        if ($model = $this->findEstimate($id, true)) {

            $clone = clone $model;
            unset($clone->id);
            $clone->isNewRecord = true;
            $clone->status = ProjectEstimate::STATUS_DRAFT;
            $clone->date = date('Y-m-d');
            $cloneItems = [];
            foreach ($model->projectEstimateItems as $item) {
                $cloneItems[] = $this->cloneEstimateItem($item, $clone);
            }
            $clone->populateRelation('projectEstimateItems', $cloneItems);

            return $clone;
        }

        return (new ProjectEstimate(['project_id' => $projectId]));
    }

    /**
     * @param ProjectEstimate $estimate
     * @param ProjectEstimateItem $item
     * @return ProjectEstimateItem
     */
    protected function cloneEstimateItem(ProjectEstimateItem $item, ProjectEstimate $estimate)
    {
        $cloneItem = clone $item;
        unset($cloneItem->id);
        $cloneItem->isNewRecord = true;
        $cloneItem->id_project_estimate = null;
        $cloneItem->populateRelation('invoice', $estimate);

        return $cloneItem;
    }

    /**
     * @param int $activeTab
     * @param string $periodSize
     * @return string
     * @throws \Exception
     */
    public function actionOddsPart($activeTab = OddsSearch::TAB_ODDS, $periodSize = 'months')
    {
        $ODDS_PROJECT_ID = Yii::$app->request->get('project_id', -1);
        $ODDS_PLOJECT_FILTER = ['filters' => ['project_id' => $ODDS_PROJECT_ID]];

        $searchModel = new OddsSearch();

        $dataFull = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, $ODDS_PLOJECT_FILTER) :
            $searchModel->search($activeTab, $ODDS_PLOJECT_FILTER);

        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        if ($activeTab == OddsSearch::TAB_ODDS_BY_PURSE_DETAILED) {
            $viewTable = '_partial/odds2_table_detailed';
        } else {
            $viewTable = '_partial/odds2_table';
        }

        return $this->render("@frontend/modules/analytics/views/finance/{$viewTable}" . ($periodSize == 'days' ? '_days' : ''), [
            'searchModel' => $searchModel,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'userConfig' => Yii::$app->user->identity->config,
            'warnings' => [],
            'floorMap' => Yii::$app->request->post('floorMap', [])
        ]);
    }

    /**
     * @return bool|string
     * @throws \Exception
     */
    public function actionOddsItemList()
    {
        $items = Yii::$app->request->post('items');
        $items = json_decode($items, true);

        if ($items) {
            $activeTab = $items['activeTab'] ?? OddsSearch::TAB_ODDS;
            $searchModel = new FlowOfFundsReportSearch();
            $searchModel->project_id = Yii::$app->request->get('project_id', $items['project_id'] ?? -1);
            $viewTable = '_partial/item_table';

            $searchModel->income_item_id = isset($items['income']) ? $items['income'] : null;
            $searchModel->expenditure_item_id = isset($items['expense']) ? $items['expense'] : null;
            $searchModel->period = isset($items['period']) ? $items['period'] : null;
            $searchModel->group = isset($items['group']) ? $items['group'] : null;
            $searchModel->flow_type = isset($items['flow_type']) ? $items['flow_type'] : null;
            $searchModel->payment_type = isset($items['payment_type']) ? $items['payment_type'] : null;
            $searchModel->wallet_id = isset($items['wallet_id']) ? $items['wallet_id'] : null;

            $itemsDataProvider = $searchModel->searchItems(Yii::$app->request->get());
            $itemsDataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 50);

            return $this->render("@frontend/modules/analytics/views/finance/{$viewTable}", [
                'searchModel' => $searchModel,
                'itemsDataProvider' => $itemsDataProvider,
                'activeTab' => $activeTab,
                'subTab' => null
            ]);
        }

        return false;
    }

    private function unlinkCustomerFlowsAndInvoices(ProjectCustomer $customer)
    {
        $updateFields = [
            'project_id' => null
        ];
        $updateCondition = [
            'company_id' => $customer->project->company_id,
            'project_id' => $customer->project->id,
            'contractor_id' => $customer->customer_id
        ];

        try {
            self::_updateFlowsAndInvoices($updateFields, $updateCondition);
        } catch (\Throwable $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()), 'validation');
            return false;
        }

        return true;
    }

    private function unlinkProjectFlowsAndInvoices(Project $project)
    {
        $updateFields = [
            'project_id' => null
        ];
        $updateCondition = [
            'company_id' => $project->company_id,
            'project_id' => $project->id
        ];

        try {
            self::_updateFlowsAndInvoices($updateFields, $updateCondition);
        } catch (\Throwable $e) {
            Yii::error(VarDumper::dumpAsString($e->getMessage()), 'validation');
            return false;
        }

        return true;
    }

    private static function _updateFlowsAndInvoices(array $fields, array $condition)
    {
        // check company
        $companyId = Yii::$app->user->identity->company->id;
        if (!isset($condition['company_id']) || $condition['company_id'] != $companyId)
            throw new Exception('Can\'t update project flows and invoices #1: ' . serialize($condition));

        // check fields
        if (!array_key_exists('project_id', $fields) || count($fields) > 1)
            throw new Exception('Can\'t update project flows and invoices #2: ' . serialize($fields));

        Invoice::updateAll($fields, $condition);
        foreach (CashFactory::getCashClasses() as $cashClass) {
            $cashClass::updateAll($fields, $condition);
        }

        return true;
    }
}
