<?php

namespace frontend\controllers;

use common\models\Auth;
use common\models\employee\Employee;
use frontend\models\AuthSignupForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Auth controller for the `mts` module
 */
class AuthController extends Controller
{
    public function init()
    {
        parent::init();

        Yii::$app->getUrlManager()->setHostInfo(null);
    }

    public function actions()
    {
        return [
            'login' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        $data = $client->getUserAttributes();


        $uid = ArrayHelper::getValue($data, 'uid');
        $email = ArrayHelper::getValue($data, 'email');

        $auth = Auth::findOne([
            'provider' => $client->getId(),
            'user_uid' => $uid,
        ]);

        if ($auth === null) {
            $auth = new Auth([
                'provider' => $client->getId(),
                'user_uid' => $uid,
                'userData' => $data,
            ]);
            $auth->save();
        }

        if ($auth->employee) { // вход существующего пользователя
            Yii::$app->user->login($auth->employee);

            return $this->goBack(['/site/index']);
        } else { // регистрация, если пользователь не существует
            $session = Yii::$app->getSession();
            $session->set('__auth_id', $auth->id);

            return $this->redirect(['signup']);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $authId = Yii::$app->session->get('__auth_id');
        $regPageId = Yii::$app->session->get('__registration_page_type_id');

        $auth = Auth::findOne($authId);
        if ($auth === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model = new AuthSignupForm([
            'email' => ArrayHelper::getValue($auth, 'userData.email'),
            'scenario' => AuthSignupForm::SCENARIO_EMAIL,
            'registrationPageTypeId' => $regPageId,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $scenario = Yii::$app->request->post('scenario');
                $employee = Employee::findIdentityByLogin($model->email);
                if ($employee) { // связываем аккаунты, если пользователь существует
                    $model->scenario = AuthSignupForm::SCENARIO_LOGIN;
                    $model->load(Yii::$app->request->post());
                    if ($scenario == AuthSignupForm::SCENARIO_LOGIN && $model->login()) {
                        $auth->updateAttributes(['employee_id' => $employee->id]);
                        Yii::$app->session->remove('__auth_id');
                        Yii::$app->session->remove('__registration_page_type_id');

                        return $this->goBack(['/site/index']);
                    }
                } else { // создаем новый аккаунт, если пользователь не существует
                    $model->scenario = AuthSignupForm::SCENARIO_CREATE;
                    $model->load(Yii::$app->request->post());
                    if ($scenario == AuthSignupForm::SCENARIO_CREATE && $model->save()) {
                        $employee = $model->getUser();
                        $auth->updateAttributes(['employee_id' => $employee->id]);
                        if ($attributes = ArrayHelper::getValue($auth, 'userData.employeeAttributes')) {
                            $employee->updateAttributes($attributes);
                        }
                        if ($attributes = ArrayHelper::getValue($auth, 'userData.companyAttributes')) {
                            $employee->company->updateAttributes($attributes);
                        }

                        if (Yii::$app->user->login($employee)) {
                            Yii::$app->session->remove('__auth_id');
                            Yii::$app->session->remove('__registration_page_type_id');
                            $mailer = clone \Yii::$app->mailer;
                            $mailer->htmlLayout = 'layouts/html2';
                            $mailer->compose([
                                'html' => 'system/new-registration/html',
                                'text' => 'system/new-registration/text',
                            ], [
                                'login' => $employee->email,
                                'password' => $model->password,
                                'subject' => 'Добро пожаловать в КУБ24',
                            ])
                                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                                ->setTo($employee->email)
                                ->setSubject('Добро пожаловать в КУБ24')
                                ->send();

                            Yii::$app->session->set('show_example_popup', 1);

                            return $this->goBack($model->getRedirectUrl());
                        }
                    }
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
}
