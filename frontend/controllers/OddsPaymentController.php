<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.06.2018
 * Time: 4:38
 */

namespace frontend\controllers;


use common\models\Company;
use common\models\service\PaymentType;
use frontend\components\FrontendController;
use common\components\helpers\ArrayHelper;
use common\components\filters\AccessControl;
use frontend\models\OddsPaymentForm;
use frontend\modules\subscribe\forms\OnlinePaymentForm;
use frontend\rbac\permissions;
use yii\helpers\Html;
use Yii;

class OddsPaymentController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['expose-invoice', 'online-payment'],
                        'allow' => true,
                        'roles' => [permissions\Subscribe::INDEX],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionExposeInvoice()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($company->strict_mode) {
            return $this->strictMode();
        }

        $model = new OddsPaymentForm($company);
        $model->paymentTypeId = PaymentType::TYPE_INVOICE;
        if ($model->validate() && $model->makePayment()) {
            if ($fdf = $model->pdfContent) {
                $uid = uniqid();
                $session = Yii::$app->session;
                $session->set('paymentDocument.uid', $uid);
                $session->set("paymentDocument.{$uid}", $model->pdfContent);
            }
        } else {
            Yii::$app->session->setFlash('error', 'Форма оплаты заполнена неправильно.');
        }

        return $this->redirect(['/documents/invoice/view', 'id' => $model->outInvoice->id, 'type' => $model->outInvoice->type]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionOnlinePayment()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if ($company->strict_mode) {
            return $this->strictMode();
        }
        $model = new OddsPaymentForm($company);
        $model->paymentTypeId = PaymentType::TYPE_ONLINE;
        if ($model->validate() && $model->makePayment()) {
            if ($fdf = $model->pdfContent) {
                $uid = uniqid();
                $session = Yii::$app->session;
                $session->set('paymentDocument.uid', $uid);
                $session->set("paymentDocument.{$uid}", $model->pdfContent);
            }
        } else {
            Yii::$app->session->setFlash('error', 'Форма оплаты заполнена неправильно.');
        }
        $paymentForm = new OnlinePaymentForm([
            'scenario' => OnlinePaymentForm::SCENARIO_SEND,
            'company' => $model->company,
            'payment' => $model->payment,
            'user' => Yii::$app->user->identity ?: $model->company->employeeChief,
        ]);

        return $this->renderPartial('online-payment', [
            'paymentForm' => $paymentForm,
        ]);
    }

    /**
     * @return \yii\web\Response
     */
    protected function strictMode()
    {
        Yii::$app->session->setFlash('emptyCompany', Html::tag('div', Html::tag('button', '×', [
                'type' => 'button',
                'class' => 'close',
                'data-dismiss' => 'alert',
                'aria-hidden' => 'true',
                'style' => 'display: block;',
            ]) . 'Данное действие недоступно. ' . Html::a('Заполните информацию о компании и банковские реквизиты.', ['/company/update']), [
            'class' => 'alert-danger alert fade in',
        ]));

        return $this->redirect(Yii::$app->request->referrer);
    }
}