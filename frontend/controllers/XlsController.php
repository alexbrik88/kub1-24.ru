<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.12.2016
 * Time: 6:07
 */

namespace frontend\controllers;

use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\components\XlsHelper;
use frontend\rbac\UserRole;
use himiklab\thumbnail\FileNotFoundException;
use PhpOffice\PhpWord\Style\Cell;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class XlsController
 * @package frontend\controllers
 */
class XlsController extends FrontendController
{
    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['upload', 'undelete', 'download-template', 'download-epf'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_PRODUCT_ADMIN],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array|bool
     * @throws \PHPExcel_Exception
     */
    public function actionUpload()
    {
        ini_set('max_execution_time', 1200);
        if (YII_ENV_PROD) {
            ini_set('memory_limit', '4096M');
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $file = UploadedFile::getInstanceByName('uploadfile');
        $className = Yii::$app->request->post('className');
        if ($file !== null) {
            $xlsHelper = new XlsHelper($file);
            if ($className == 'Product') {
                $result = $xlsHelper->read();
            } elseif ($className == 'CashOrderFlows') {
                $xlsHelper->loadCashboxes();
                $result = $xlsHelper->readForOrder();
            } else {
                $result = null;
            }
            if ($result) {
                $invoiceData = [];
                $message = null;
                if ($className == 'Product') {
                    $errorList = empty($result['errorArray']) ? '' : implode(', ', array_filter(array_unique($result['errorArray'])));
                    $message = "Добавлено новых позиций: {$result['successCount']} шт<br>
                    Обновлено позиций: {$result['updateCount']} шт<br>
                    Не загрузилось: {$result['errorCount']} шт";
                    if (!empty($errorList)) {
                        $message .= '<br>Необходимо исправить следующие ошибки: '.$errorList.'.';
                    }
                } elseif ($className == 'CashOrderFlows') {
                    $message = 'Успешно загружено ' . $result['successCount'] . ' операций <br>';
                    if ($result['errorCount'] > 0) {
                        $message .= '
                        Из-за ошибки не загружено ' . $result['errorCount'] . ' операций (' . $result['errorProductsName'] . ') <br>
                        Данные не соответствуют формату, просьба поправить и повторить загрузку.';
                    }
                }
                if (isset($result['invoiceData']) && $result['invoiceData']) {
                    $invoiceData = $result['invoiceData'];
                }

                return [
                    'result' => !$result['error'],
                    'invoiceData' => $invoiceData,
                    'message' => $message,
                    'successProducts' => $result['success'],
                ];
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function actionUndelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $createdModels = Yii::$app->request->post('createdModels');
        if ($createdModels) {
            XlsHelper::unDelete(Yii::$app->request->post('className'), explode(', ', $createdModels));
        }

        return true;
    }


    /**
     * @param $type
     * @throws FileNotFoundException
     * @throws NotFoundHttpException
     */
    public function actionDownloadTemplate($type)
    {
        if ($type !== null) {
            $fileName = XlsHelper::$xlsTemplate[$type];
            $path = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'xls' . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . $fileName;
            if (file_exists($path)) {
                return Yii::$app->response->sendFile($path, $fileName, ['mimeType' => mime_content_type($path)])->send();
            } else {
                throw new FileNotFoundException('File not found');
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $type
     * @throws FileNotFoundException
     * @throws NotFoundHttpException
     */
    public function actionDownloadEpf($type = null)
    {
        if ($type == 'import1C') {
            $fileName = 'auto_import_1c.zip';
            $attachmentName = 'Выгрузка-1С-Бухгалтерия-в-КУБ24+инструкция.zip';
            $path = \Yii::getAlias('@frontend/modules/import/docs') . DIRECTORY_SEPARATOR . $fileName;
        } elseif ($type == 'import1C_UT') {
            $fileName = 'auto_import_1c_ut.zip';
            $attachmentName = 'Выгрузка-1С-УТ-в-КУБ24+инструкция.zip';
            $path = \Yii::getAlias('@frontend/modules/import/docs') . DIRECTORY_SEPARATOR . $fileName;
        } elseif ($type == 'import1C_Fresh') {
            $fileName = 'auto_import_1c_fresh.zip';
            $attachmentName = 'Выгрузка-1С-Фреш-в-КУБ24+инструкция.zip';
            $path = \Yii::getAlias('@frontend/modules/import/docs') . DIRECTORY_SEPARATOR . $fileName;
        } elseif ($type == 'import1C_KA') {
            $fileName = 'auto_import_1c_ka.zip';
            $attachmentName = 'Выгрузка-1С-комплексная-автоматизация-в-КУБ24+инструкция.zip';
            $path = \Yii::getAlias('@frontend/modules/import/docs') . DIRECTORY_SEPARATOR . $fileName;
        } else {
            $fileName = \frontend\modules\export\models\one_c\OneCExport::IC_FILENAME;
            $attachmentName = 'ибЗагрузкаВыгрузкаXml_28092020.epf';
            $path = \Yii::getAlias('@frontend/modules/export/docs') . DIRECTORY_SEPARATOR . $fileName;
        }
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, $attachmentName, ['mimeType' => mime_content_type($path)])->send();
        } else {
            throw new FileNotFoundException('File not found');
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
