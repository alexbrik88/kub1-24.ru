<?php

namespace frontend\controllers;

use common\modules\import\components\ImportCommandManager;
use Yii;
use yii\web\Response;
use frontend\rbac\UserRole;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use frontend\components\FrontendController;
use common\models\employee\Employee;
use common\modules\import\models\ImportJobData;
use common\modules\import\models\ImportJobDataRepository;

/**
 * Class QueueNotificationController
 * @package frontend\controllers
 */
class QueueNotificationController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_AUTHENTICATED,
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function actionCheckUnviewedJobs()
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $form = new ImportJobDataRepository($employee->company);

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($employee->has_finished_unviewed_jobs) {

            $jobsData = $form->getFinishedUnviewedJobsData($employee);

            if (empty($jobsData)) {

                if (!$form->hasUnfinishedJobs($employee)) {
                    $employee->needCheckFinishedUnviewedJobs(false);
                    return [
                        'result' => 1,
                        'is_last' => 1
                    ];
                }

                return [
                    'result' => 0,
                ];
            }

            /** @var ImportJobData $jobData */
            $job = reset($jobsData);
            $commandClass = ImportCommandManager::getInstance()->getTypeById($job->type)->getCommandClass();
            $commandLabel = $commandClass::getImportCommandLabel();
            $jobTypeName = "Загрузка \"{$commandLabel}\" завершена";

            return [
                'result' => 1,
                'id' => $job->id,
                'message' => nl2br($jobTypeName ."\r\n". $job->result),
                'is_last' => (count($jobsData) - 1 == 0) ? '1' : ''
            ];
        }

        return [
            'result' => 0
        ];
    }

    public function actionSetJobIsViewed($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        $model = ImportJobData::findOne([
            'id' => $id,
            'employee_id' => $employee->id
        ]);

        if ($model) {
            $model->updateAttributes(['is_viewed' => 1]);

            $form = new ImportJobDataRepository($employee->company);
            $jobsData = $form->getFinishedUnviewedJobsData($employee);
            $employee->needCheckFinishedUnviewedJobs(!empty($jobsData));

            return ['result' => 1];
        }

        return ['result' => 0];
    }
}