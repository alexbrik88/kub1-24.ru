<?php

namespace frontend\controllers;

use common\components\filters\AjaxFilter;
use common\models\Contractor;
use common\models\out\OutInvoice;
use frontend\components\FrontendController;
use frontend\models\OutInvoiceSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * OutInvoiceController implements the CRUD actions for OutInvoice model.
 */
class OutInvoiceController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OutInvoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutInvoiceSearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $backTo
     * @return mixed|string|\yii\web\Response
     */
    public function actionCreate($backTo = 'B2B')
    {
        $company = Yii::$app->user->identity->company;
        if (($model = OutInvoice::findOne(['id' => Yii::$app->request->post('out_id'), 'company_id' => $company->id])) === null) {
            $model = new OutInvoice([
                'company_id' => $company->id,
                'status' => OutInvoice::ACTIVE
            ]);
        }
        $backUrl = $backTo == 'company' ? Url::to(['/company/index', '#' => 'additional'])
            : Url::to(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_INVOICE_LINKS]);

        if ($backTo == 'B2B')
            $backUrl = Url::to(['/b2b/module']);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($backUrl);
        } else {
            return $this->render('create', [
                'model' => $model,
                'backUrl' => $backUrl,
            ]);
        }
    }

    /**
     * @param $id
     * @param string $backTo
     * @return mixed|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $backTo = 'B2B')
    {
        $model = $this->findModel($id);
        $backUrl = $backTo == 'company' ? Url::to(['/company/index', '#' => 'additional'])
            : Url::to(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_INVOICE_LINKS]);

        if ($backTo == 'B2B')
            $backUrl = Url::to(['/b2b/module']);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect($backUrl);
        } else {
            return $this->render('update', [
                'model' => $model,
                'backUrl' => $backUrl,
            ]);
        }
    }

    /**
     * @param $id
     * @param string $backTo
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id, $backTo = 'company')
    {
        $model = $this->findModel($id);
        $model->status = OutInvoice::DELETED;
        if ($model->save(false, ['status'])) {
            Yii::$app->session->setFlash('success', 'Ссылка создания счета удалена.');
        }
        $backUrl = $backTo == 'company' ? Url::to(['/company/index', '#' => 'additional'])
            : Url::to(['/contractor/sale-increase', 'activeTab' => Contractor::TAB_INVOICE_LINKS]);

        if ($backTo == 'B2B')
            $backUrl = '/b2b/module';

        return $this->redirect($backUrl);
    }

    /**
     * Deletes an existing OutInvoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionProductsTable()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $model = new OutInvoice();
        $model->populateRelation('company', $company);

        $query = $company->getProducts()->alias('product')->select([
            'product.id',
            'product.title',
            'product.price_for_sell_with_nds',
        ])->andWhere([
            'product.production_type' => Yii::$app->request->post('productType'),
            'product.is_deleted' => false,
            'product.not_for_sale' => false,
        ])->andWhere([
            'not',
            ['product.id' => Yii::$app->request->post('exists')]
        ]);

        if (Yii::$app->request->post('add_all') == 1) {
            $query->andFilterWhere(['like', 'product.title', Yii::$app->request->post('title')]);
        } else {
            $query->andWhere([
                'product.id' => Yii::$app->request->post('in_order'),
            ]);
        }

        $productArray = $query->all();
        $result = '';

        foreach ($productArray as $product) {
            $result .= $this->renderPartial('_form_product_row', [
                'model' => $model,
                'product' => $product,
            ]);
        }

        return ['result' => $result];
    }

    /**
     * Finds the OutInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return OutInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = OutInvoice::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
            'status' => array_keys(OutInvoice::$statusAvailable),
        ]);
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
