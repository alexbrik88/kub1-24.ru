<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\web\Response;

/**
 * LogController.
 */
class ThumbnailController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex($filename)
    {
        $ds = DIRECTORY_SEPARATOR;
        $company_id = Yii::$app->user->identity->company->id;
        $cachePath = Yii::getAlias("@runtime") . "{$ds}cache{$ds}thumbnails{$ds}{$company_id}";
        $file = $cachePath . $ds . substr($filename, 0, 2) . $ds . $filename;

        if (is_file($file)) {
            $options = ['inline' => true];

            return \Yii::$app->response->sendFile($file, $filename, $options);
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
