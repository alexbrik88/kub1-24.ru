<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 22.08.2018
 * Time: 17:24
 */

namespace frontend\controllers;


use common\components\helpers\Html;
use common\models\Agreement;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\Proxy;
use common\models\document\SalesInvoice;
use common\models\document\Waybill;
use common\models\document\Upd;
use common\models\document\UserEmail;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use himiklab\thumbnail\FileNotFoundException;
use Yii;
use yii\helpers\Url;
use yii\web\Response;
use common\models\Company;
use common\models\document\EmailSignature;
use common\models\document\EmailTemplate;
use yii\web\NotFoundHttpException;
use common\models\document\EmailFile;
use common\components\helpers\ArrayHelper;
use common\components\filters\AccessControl;
use common\models\document\AgentReport;

/**
 * Class EmailController
 * @package frontend\controllers
 */
class EmailController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['email-signature', 'email-template', 'delete-template', 'update-template',
                            'upload-email-file', 'delete-email-file', 'download-file', 'set-employee-email',
                            'add-user-email', 'get-permanent-files', 'update-user-email', 'delete-user-email'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function actionEmailSignature()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $signature = Yii::$app->request->post('signature');
        $emailSignature = $company->getEmailSignature($user->id);
        if (!$emailSignature) {
            $emailSignature = new EmailSignature();
            $emailSignature->company_id = $company->id;
            $emailSignature->employee_id = $user->id;
        }
        $emailSignature->text = $signature;
        $emailSignature->save();

        return [
            'result' => true,
            'signature' => nl2br($emailSignature->text),
        ];
    }

    /**
     * @return array
     */
    public function actionEmailTemplate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $templateName = Yii::$app->request->post('templateName');
        $text = Yii::$app->request->post('InvoiceSendForm')['emailText'];
        $countTemplates = count($company->getEmailTemplates($user->id));
        /* @var $activeTemplate EmailTemplate */
        $activeTemplate = $company->getActiveEmailTemplate($user->id);
        if ($templateName && $text && $countTemplates < 3) {
            $countTemplates++;
            $emailTemplate = new EmailTemplate();
            $emailTemplate->company_id = $company->id;
            $emailTemplate->name = $templateName;
            $emailTemplate->text = $text;
            $emailTemplate->status = EmailTemplate::STATUS_ACTIVE;
            $emailTemplate->employee_id = $user->id;
            if ($emailTemplate->save()) {
                if ($activeTemplate) {
                    $activeTemplate->status = EmailTemplate::STATUS_NOT_ACTIVE;
                    $activeTemplate->save();
                }

                return [
                    'result' => true,
                    'id' => $emailTemplate->id,
                    'name' => $emailTemplate->name,
                    'text' => $emailTemplate->text,
                    'canAdd' => $countTemplates < 3,
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteTemplate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $emailTemplate = EmailTemplate::findOne($id);
        if (!$emailTemplate) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $emailTemplate->delete();
        $countTemplates = count($company->getEmailTemplates(Yii::$app->user->identity->id));

        return [
            'result' => true,
            'canAdd' => $countTemplates < 3,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateTemplate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $emailTemplate = EmailTemplate::findOne($id);
        if (!$emailTemplate) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $name = Yii::$app->request->post('name');
        $text = Yii::$app->request->post('text');

        if ($name && $text) {
            $emailTemplate->name = $name;
            $emailTemplate->text = $text;
            if ($emailTemplate->save()) {
                return [
                    'result' => true,
                    'name' => $emailTemplate->name,
                    'text' => $emailTemplate->text,
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @return array
     */
    public function actionUploadEmailFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        if (is_array($_FILES)) {
            $emailFile = new EmailFile(['company_id' => $company->id]);

            return $emailFile->loadFile();
        }

        return [
            'result' => false,
            'msg' => 'Произошла ошибка при сохранении файла!',
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteEmailFile($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $emailFile = EmailFile::findOne($id);
        if (!$emailFile) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($emailFile->_delete()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'msg' => 'Произошла ошибка при удалении файла.',
        ];
    }

    /**
     * @param $id
     * @return \yii\console\Response|Response
     * @throws FileNotFoundException
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        /* @var EmailFile $model */
        $model = EmailFile::findOne($id);

        if ($model !== null) {
            $path = Yii::getAlias($model->getFilePath() . DIRECTORY_SEPARATOR . $model->file_name);
            if (file_exists($path)) {
                return Yii::$app->response->sendFile($path, $model->file_name, [
                    'mimeType' => $model->mime,
                    'inline' => true,
                ]);
            } else {
                throw new FileNotFoundException('File not found');
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $contractorID
     * @return array
     */
    public function actionSetEmployeeEmail($contractorID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        /* @var $contractor Contractor */
        $contractor = Contractor::findOne($contractorID);

        if ($contractor && $contractor->company_id == $company->id) {
            $attribute = $labelText = $email = null;
            $invoiceSendFormPost = Yii::$app->request->post('InvoiceSendForm');
            if (isset($invoiceSendFormPost['chiefEmail'])) {
                $contractor->director_email = $invoiceSendFormPost['chiefEmail'];
                $attribute = 'director_email';
                $labelText = $contractor->director_name . ' (Руководитель)<br> <span style="padding-left: 25px">' .
                    $contractor->director_email . '</span>';
                $email = $contractor->director_email;
            }
            if (isset($invoiceSendFormPost['chiefAccountantEmail'])) {
                $contractor->chief_accountant_email = $invoiceSendFormPost['chiefAccountantEmail'];
                $attribute = 'chief_accountant_email';
                $labelText = $contractor->chief_accountant_name . ' (Главный бухгалтер)<br> <span style="padding-left: 25px">' .
                    $contractor->chief_accountant_email . '</span>';
                $email = $contractor->chief_accountant_email;
            }
            if (isset($invoiceSendFormPost['contactAccountantEmail'])) {
                $contractor->contact_email = $invoiceSendFormPost['contactAccountantEmail'];
                $attribute = 'contact_email';
                $labelText = $contractor->contact_name . ' (Контакт)<br> <span style="padding-left: 25px">' .
                    $contractor->contact_email . '</span>';
                $email = $contractor->contact_email;
            }
            if ($attribute && $contractor->save(true, [$attribute])) {
                return [
                    'result' => true,
                    'labelText' => $labelText,
                    'email' => $email,
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionGetPermanentFiles($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $tableName = Yii::$app->request->post('tableName');
        if ($tableName) {
            switch ($tableName) {
                case Invoice::tableName():
                    $model = Invoice::findOne($id);
                    break;
                case ForeignCurrencyInvoice::tableName():
                    $model = ForeignCurrencyInvoice::findOne($id);
                    break;
                case Act::tableName():
                    $model = Act::findOne($id);
                    break;
                case PackingList::tableName():
                    $model = PackingList::findOne($id);
                    break;
                case SalesInvoice::tableName():
                    $model = SalesInvoice::findOne($id);
                    break;
                case Waybill::tableName():
                    $model = Waybill::findOne($id);
                    break;
                case InvoiceFacture::tableName();
                    $model = InvoiceFacture::findOne($id);
                    break;
                case Upd::tableName():
                    $model = Upd::findOne($id);
                    break;
                case Agreement::tableName():
                    $model = Agreement::findOne($id);
                    break;
                case Proxy::tableName():
                    $model = Proxy::findOne($id);
                    break;
                case AgentReport::tableName():
                    $model = AgentReport::findOne($id);
                    break;
                case OrderDocument::tableName():
                    $model = OrderDocument::findOne($id);
                    break;
                case GoodsCancellation::tableName():
                    $model = GoodsCancellation::findOne($id);
                    break;
                default:
                    throw new NotFoundHttpException('The requested page does not exist.');
                    break;
            }
            if (!$model) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            return [
                'result' => true,
                'files' => $model->loadEmailFiles(),
            ];
        }

        return ['result' => false];
    }

    /**
     * @param $contractorID
     * @return array
     */
    public function actionAddUserEmail($contractorID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company \common\models\Company */
        $company = Yii::$app->user->identity->company;
        /* @var $contractor Contractor */
        $contractor = Contractor::findOne($contractorID);
        $fio = Yii::$app->request->post('fio');
        $email = Yii::$app->request->post('email');
        if ($contractor) {
            if ($email && $contractor->getUserEmails()->count() < 5) {
                $userEmail = new UserEmail();
                $userEmail->company_id = $company->id;
                $userEmail->contractor_id = $contractorID;
                $userEmail->fio = $fio;
                $userEmail->email = $email;
                if ($userEmail->save()) {

                    return [
                        'result' => true,
                        'canAdd' => $company->getUserEmails()->count() < 5,
                        'html' => $this->renderPartial('new-contractor-email', ['userEmail' => $userEmail]),
                    ];
                }
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateUserEmail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userEmail = UserEmail::findOne($id);
        if (!$userEmail) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $fio = Yii::$app->request->post('fio');
        $email = Yii::$app->request->post('email');

        if ($email) {
            $userEmail->fio = $fio;
            $userEmail->email = $email;
            if ($userEmail->save()) {
                return [
                    'result' => true,
                    'label' => $userEmail->fio ?
                        ($userEmail->fio . '<br><span style="padding-left: 25px">' . $userEmail->email . '</span>') :
                        $userEmail->email,
                    'email' => $userEmail->email,
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDeleteUserEmail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userEmail = UserEmail::findOne($id);
        if (!$userEmail) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($userEmail->delete()) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'msg' => 'Произошла ошибка при удалении почты.',
        ];
    }
}