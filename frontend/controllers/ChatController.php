<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.09.2017
 * Time: 4:29
 */

namespace frontend\controllers;


use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\Chat;
use common\models\chat\CrmUser;
use common\models\ChatVolume;
use common\models\EmployeeCompany;
use php_rutils\RUtils;
use WAMP\WAMPClient;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * Class ChatController
 * @package frontend\controllers
 */
class ChatController extends Controller
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'change-employee', 'view-message', 'send-message', 'get-new-messages',
                            'change-volume-setting', 'upload-file',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['send-from-crm', 'download-file'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|Response
     */
    public function beforeAction($action)
    {
        throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));

        if ($this->action->id == 'send-from-crm' || $this->action->id == 'get-new-messages') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @param null $userID
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($userID = null)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $employeeCompanies = $user->company->getEmployeeCompaniesChat();
        $crmAccountant = $this->getCrmAccountant($user);
        if ($userID) {
            if (!$user->company->getEmployeeCompanies()->andWhere(['employee_id' => $userID])->exists()) {
                foreach (EmployeeCompany::find()->select(['company_id'])->andWhere(['employee_id' => $userID])->column() as $companyID) {
                    if (EmployeeCompany::find()->andWhere(['and',
                        ['company_id' => $companyID],
                        ['employee_id' => $user->id],
                    ])->exists()
                    ) {
                        $user->company_id = $companyID;
                        $user->save(true, ['company_id']);
                        $employeeCompanies = $user->company->getEmployeeCompanies()
                            ->andWhere(['and',
                                ['is_working' => 1],
                                ['not', ['employee_id' => $user->id]],
                            ])
                            ->all();
                    }
                }
                if (!$user->company->getEmployeeCompanies()->andWhere(['employee_id' => $userID])->exists()) {
                    if (stristr($userID, 'crm') == false) {
                        throw new NotFoundHttpException('Chat with this user not found');
                    }
                }
            } else if ($user->id == $userID) {
                throw new NotFoundHttpException('Chat with this user not found');
            }
        } elseif (!empty($employeeCompanies)) {
            reset($employeeCompanies);
            $userID = current($employeeCompanies)->employee_id;
        }

        $kubSupportEmployee = Employee::findOne(Employee::SUPPORT_KUB_EMPLOYEE_ID);
        if ($userID == null && $kubSupportEmployee) {
            $userID = Employee::SUPPORT_KUB_EMPLOYEE_ID;
        }
        $user->readMessages($userID);

        return $this->render('index', [
            'employeeCompanies' => $employeeCompanies,
            'kubSupportEmployee' => $kubSupportEmployee,
            'user' => $user,
            'userID' => $userID,
            'crmAccountant' => $crmAccountant,
        ]);
    }

    /**
     * @param Employee $user
     * @return mixed
     */
    private function getCrmAccountant(Employee $user)
    {
        $domain = YII_ENV_DEV ? 'http://devbuh.kub-24.ru/' : 'https://buh.kub-24.ru/';
        $curl = curl_init($domain . 'site/get-accountant-to-kub-client');
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
            'Company' => [
                'inn' => $user->company->inn,
                'kpp' => $user->company->kpp,
            ],
        ]));
        $out = @unserialize(curl_exec($curl));
        $accountant = $out ? : null;
        if ($accountant) {
            if (($existsCrmUser = CrmUser::find()->where(['user_id' => $accountant['user_id']])->one()) == null) {
                $existsCrmUser = new CrmUser();
            }
            foreach ($accountant as $key => $value) {
                $existsCrmUser->$key = $value;
            }
            $existsCrmUser->save();

            if (!ChatVolume::find()->andWhere(['and',
                ['from_employee_id' => $user->id],
                ['to_employee_id' => 'crm-' . $accountant['user_id']],
            ])->exists()
            ) {
                $chatVolume = new ChatVolume();
                $chatVolume->company_id = $user->company->id;
                $chatVolume->from_employee_id = $user->id;
                $chatVolume->to_employee_id = 'crm-' . $accountant['user_id'];
                $chatVolume->has_volume = true;
                $chatVolume->save();
            }
        }

        return $accountant;
    }

    /**
     * @param $id
     * @return array|string
     */
    public function actionChangeEmployee($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        if ($id !== null && $user->id !== $id &&
            ($user->company->getEmployeeCompanies()->andWhere(['employee_id' => $id])->exists() ||
                $id == Employee::SUPPORT_KUB_EMPLOYEE_ID || $user->id == Employee::SUPPORT_KUB_EMPLOYEE_ID || stristr($id, 'crm') !== false)
        ) {
            $notification = [];
            $user->readMessages($id);
            /* @var $newMessages Chat[] */
            $newMessages = Chat::find()->andWhere(['and',
                ['to_employee_id' => $user->id],
                ['is_new' => true],
            ])->orderBy('created_at')->all();
            $newMessagesIDs = Chat::find()->andWhere(['and',
                ['to_employee_id' => $user->id],
                ['is_new' => true],
            ])->orderBy('created_at')->column();
            foreach ($newMessages as $newMessage) {
                $notification['html'][$newMessage->id] = $this->renderPartial('_partial/notification', [
                    'newMessage' => $newMessage,
                ]);
            }
            $notification['countNewMessages'] = count($newMessagesIDs);
            $notification['idArray'] = serialize($newMessagesIDs);

            return [
                'result' => true,
                'html' => $this->renderPartial('_partial/chat', [
                    'user' => $user,
                    'userID' => $id,
                ]),
                'notificationArray' => $notification,
            ];
        }

        return ['result' => false,];
    }

    /**
     * @return array
     */
    public function actionSendMessage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $toUserID = Yii::$app->request->post('to_user_id');
        $message = Yii::$app->request->post('message');

        if ($toUserID !== null && $message !== null && $user->id !== $toUserID &&
            ($user->company->getEmployeeCompanies()->andWhere(['employee_id' => $toUserID])->exists() ||
                $toUserID == Employee::SUPPORT_KUB_EMPLOYEE_ID || $user->id == Employee::SUPPORT_KUB_EMPLOYEE_ID || stristr($toUserID, 'crm') !== false)
        ) {
            $chat = new Chat();
            $chat->from_employee_id = $user->id;
            $chat->to_employee_id = $toUserID;
            $chat->message = $message;
            $chat->is_new = true;
            $chat->is_viewed = false;

            if ($chat->save()) {
                $chatVolume = $user->currentEmployeeCompany->getChatVolume($toUserID);
                $result = [
                    'result' => true,
                    'id' => $chat->id,
                    'action_type' => 'ajax',
                    'name' => $chat->fromEmployee->getFio(),
                    'from_user_id' => $user->id,
                    'to_user_id' => $toUserID,
                    'createdAt' => date('H:i', $chat->created_at),
                    'message' => $chat->message,
                    'notificationTime' => RUtils::dt()->ruStrFTime([
                        'date' => $chat->created_at,
                        'format' => 'd F',
                        'monthInflected' => true,
                    ]),
                    'notificationMessage' => strip_tags($chat->message),
                    'messageID' => $chat->id,
                    'fromEmployeePhoto' => $user->getChatPhotoSrc(),
                    'fromEmployeeHasSong' => $chatVolume ? $chatVolume->has_volume : null,
                    'messageType' => Chat::TYPE_MESSAGE,
                ];
                $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
                try {
                    $client->connect();
                    $client->call('send', $result);
                    $client->disconnect();
                } catch (\Exception $e) {
                }

                if (stristr($toUserID, 'crm') !== false) {
                    $employeeChief = $user->company->getEmployeeChief();
                    $domain = YII_ENV_DEV ? 'http://devbuh.kub-24.ru/' : 'https://buh.kub-24.ru/';
                    $curl = curl_init($domain . 'chat/send-from-kub');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
                        'Message' => [
                            'from_employee_id' => $user->id,
                            'to_employee_id' => preg_replace("/[^0-9]/", '', $toUserID),
                            'message' => $chat->message,
                        ],
                        'User' => [
                            'user_fio' => $user->company->getChiefFio(),
                            'user_id' => $employeeChief->id,
                            'company_name' => $user->company->getShortName(),
                            'chat_photo' => $employeeChief->chat_photo,
                            'is_online' => $employeeChief->isOnline(),
                        ],
                    ]));
                    curl_exec($curl);
                }

                return $result;
            }
        }

        return ['result' => false,];
    }

    /**
     * @return array
     */
    public function actionGetNewMessages()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $existsID = Yii::$app->request->post('existsMessages');
        $existsID = $existsID ? unserialize($existsID) : [];
        if (!empty($newMessages = Chat::find()->andWhere(['and',
            ['to_employee_id' => $user->id],
            ['is_new' => Chat::NEW_MESSAGE],
            ['not', ['id' => $existsID]]
        ])->all())
        ) {
            $result = ['result' => true];
            /* @var $newMessage Chat */
            foreach ($newMessages as $newMessage) {
                $isFromEmployeeCrm = $newMessage->isFromEmployeeCrm();
                $existsID[] = $newMessage->id;
                $result['messages'][] = [
                    'name' => $isFromEmployeeCrm ? $newMessage->fromEmployeeCrm->user_fio : $newMessage->fromEmployee->getFio(),
                    'from_employee_id' => $isFromEmployeeCrm ? ('crm-' . $newMessage->fromEmployeeCrm->user_id) : $newMessage->fromEmployee->employee_id,
                    'companyName' => $isFromEmployeeCrm ? 'Бухгалтера Смарт+' : (
                    $newMessage->fromEmployee->employee_id == Employee::SUPPORT_KUB_EMPLOYEE_ID ? $newMessage->fromEmployee->employee->mainCompany->getShortName() :
                        $newMessage->fromEmployee->company->getShortName()
                    ),
                    'createdAt' => date('H:i', $newMessage->created_at),
                    'message' => $newMessage->message,
                    'notificationTime' => RUtils::dt()->ruStrFTime([
                        'date' => $newMessage->created_at,
                        'format' => 'd F',
                        'monthInflected' => true,
                    ]),
                    'notificationMessage' => strip_tags($newMessage->message),
                    'messageID' => $newMessage->id,
                    'fromEmployeePhoto' => $isFromEmployeeCrm ? ('//' . $newMessage->fromEmployeeCrm->chat_photo) : $newMessage->fromEmployee->getChatPhotoSrc(),
                    'toEmployeeHasSong' => $user->currentEmployeeCompany->getChatVolume($isFromEmployeeCrm ? ('crm-' . $newMessage->fromEmployeeCrm->user_id) : $newMessage->from_employee_id)->has_volume,
                ];
            }
            $result['existsID'] = serialize($existsID);
            return $result;
        }
        return ['result' => false];
    }

    /**
     *
     */
    public function actionViewMessage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $messageID = Yii::$app->request->post('messageID');
        $chat = Chat::findOne($messageID);
        if ($chat) {
            $chat->is_new = 0;
            $chat->is_viewed = true;
            $chat->save(true, ['is_new', 'is_viewed']);
            $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
            $client->connect();
            $client->call('send', [
                'chat' => $chat,

            ]);
            $client->disconnect();
        }
    }

    /**
     * @param $friendID
     * @return array
     */
    public function actionChangeVolumeSetting($friendID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $volumeStatus = (bool)Yii::$app->request->post('volume');
        $chatVolume = $user->currentEmployeeCompany->getChatVolume($friendID);
        if ($chatVolume && $volumeStatus !== null) {
            $chatVolume->has_volume = $volumeStatus;
            $chatVolume->save(true, ['has_volume']);
        }
        return ['result' => $chatVolume->has_volume];
    }

    /**
     * @param $toEmployeeID
     * @return array
     */
    public function actionUploadFile($toEmployeeID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        if ($toEmployeeID !== null && !empty($_FILES) && $user->id !== $toEmployeeID &&
            ($user->company->getEmployeeCompanies()->andWhere(['employee_id' => $toEmployeeID])->exists() ||
                $toEmployeeID == Employee::SUPPORT_KUB_EMPLOYEE_ID || $user->id == Employee::SUPPORT_KUB_EMPLOYEE_ID || stristr($toEmployeeID, 'crm') !== false)
        ) {
            $chatFile = UploadedFile::getInstanceByName('ChatFile');
            $chat = new Chat();
            $chat->from_employee_id = $user->id;
            $chat->to_employee_id = $toEmployeeID;
            $chat->message = $chatFile->name;
            $chat->is_file = true;
            $chat->is_new = true;
            $chat->is_viewed = false;

            if ($chat->save() && $chatFile->saveAs($chat->getUploadPath() . DIRECTORY_SEPARATOR . $chatFile->name)) {
                $chatVolume = $user->currentEmployeeCompany->getChatVolume($toEmployeeID);
                $result = [
                    'result' => true,
                    'sendToSocket' => false,
                    'id' => $chat->id,
                    'action_type' => 'ajax',
                    'name' => $chat->fromEmployee->getFio(),
                    'from_user_id' => $user->id,
                    'to_user_id' => $toEmployeeID,
                    'createdAt' => date('H:i', $chat->created_at),
                    'message' => $chat->message,
                    'notificationTime' => RUtils::dt()->ruStrFTime([
                        'date' => $chat->created_at,
                        'format' => 'd F',
                        'monthInflected' => true,
                    ]),
                    'notificationMessage' => strip_tags($chat->message),
                    'messageID' => $chat->id,
                    'fromEmployeePhoto' => $user->getChatPhotoSrc(),
                    'fromEmployeeHasSong' => $chatVolume ? $chatVolume->has_volume : null,
                    'messageType' => Chat::TYPE_FILE,
                ];

                $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
                $client->connect();
                $client->call('send', $result);
                $client->disconnect();

                if (stristr($toEmployeeID, 'crm') !== false) {
                    $domain = YII_ENV_DEV ? 'http://devbuh.kub-24.ru/' : 'https://buh.kub-24.ru/';
                    $curl = curl_init($domain . 'chat/send-from-kub');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
                        'Message' => [
                            'from_employee_id' => $user->id,
                            'to_employee_id' => preg_replace("/[^0-9]/", '', $toEmployeeID),
                            'message' => Html::a($chatFile->name, '//' . Yii::$app->request->serverName . Url::to(['/chat/download-file', 'id' => $chat->id, 'crmID' => $toEmployeeID])),
                            'id' => $chat->id,
                        ],
                        'User' => [
                            'user_fio' => $user->getFio(),
                            'user_id' => $user->id,
                            'company_name' => $user->company->getTitle(),
                            'chat_photo' => $user->chat_photo,
                            'is_online' => $user->isOnline(),
                        ],
                    ]));
                    curl_exec($curl);
                }
            }
        }
    }

    /**
     * @param $id
     * @param null $crmID
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id, $crmID = null)
    {
        $model = $this->findModel($id);
        $user = Yii::$app->user->identity;

        if ((!Yii::$app->user->isGuest && in_array($user->id, [$model->from_employee_id, $model->to_employee_id])) ||
            ($model->to_employee_id == $crmID)
        ) {
            if ($handle = opendir($model->getUploadPath())) {
                while (false !== ($file = readdir($handle))) {
                    if (!in_array($file, ['..', '.'])) {
                        $filePath = $model->getUploadPath() . DIRECTORY_SEPARATOR . $file;

                        if (is_file($filePath)) {
                            return \Yii::$app->response->sendFile($filePath, $file)->send();
                        } else throw new NotFoundHttpException('Файл не найден');
                    }
                }
            }
        }
        throw new NotFoundHttpException('Файл не найден');
    }

    /**
     * @return array|bool
     */
    public function actionSendFromCrm()
    {
        $user = Yii::$app->request->post('User');
        $message = Yii::$app->request->post('Message');

        if ($user && $message) {
            $kubUser = Employee::findOne($message['to_employee_id']);
            if ($kubUser) {
                if (($existsCrmUser = CrmUser::find()->where(['user_id' => $user['user_id']])->one()) == null) {
                    $existsCrmUser = new CrmUser();
                }
                foreach ($user as $key => $value) {
                    $existsCrmUser->$key = $value;
                }
                if (!$existsCrmUser->save()) {
                    return false;
                }

                if (!ChatVolume::find()->andWhere(['and',
                    ['from_employee_id' => $kubUser->id],
                    ['to_employee_id' => 'crm-' . $user['user_id']],
                ])->exists()
                ) {
                    $chatVolume = new ChatVolume();
                    $chatVolume->company_id = $kubUser->currentEmployeeCompany->company_id;
                    $chatVolume->from_employee_id = $kubUser->id;
                    $chatVolume->to_employee_id = 'crm-' . $user['user_id'];
                    $chatVolume->has_volume = true;
                    $chatVolume->save();
                }

                $chat = new Chat();
                $chat->from_employee_id = 'crm-' . $message['from_employee_id'];
                $chat->to_employee_id = $message['to_employee_id'];
                $chat->message = $message['message'];
                $chat->is_new = true;
                $chat->is_viewed = false;

                if ($chat->save()) {
                    $result = [
                        'result' => true,
                        'id' => $chat->id,
                        'action_type' => 'ajax',
                        'name' => $user['user_fio'],
                        'from_user_id' => 'crm-' . $message['from_employee_id'],
                        'to_user_id' => $chat->to_employee_id,
                        'createdAt' => date('H:i', $chat->created_at),
                        'message' => $chat->message,
                        'notificationTime' => RUtils::dt()->ruStrFTime([
                            'date' => $chat->created_at,
                            'format' => 'd F',
                            'monthInflected' => true,
                        ]),
                        'notificationMessage' => strip_tags($chat->message),
                        'messageID' => $chat->id,
                        'fromEmployeePhoto' => '//' . $existsCrmUser->chat_photo,
                        'toEmployeeHasSong' => $kubUser->currentEmployeeCompany->getChatVolume('crm-' . $message['from_employee_id'])->has_volume,
                        'messageType' => $message['messageType'],
                    ];

                    $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['chat']['port']);
                    $client->connect();
                    $client->call('send', $result);
                    $client->disconnect();
                }
            }
        }

        return false;
    }

    /**
     * Finds the Activities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}