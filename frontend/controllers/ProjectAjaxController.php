<?php

namespace frontend\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\AgreementTemplate;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\ContractorAutoProject;
use common\models\document\Invoice;
use common\models\document\status\AgreementStatus;
use common\models\employee\Employee;
use common\models\file\actions\FileDeleteAction;
use common\models\file\actions\FileListAction;
use common\models\file\actions\FileUploadAction;
use common\models\file\actions\GetFileAction;
use common\models\file;
use common\models\project\ProjectCommonCharts;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectCustomerSearch;
use common\models\project\ProjectEmployee;
use common\models\project\ProjectEstimate;
use common\models\project\ProjectEstimateForm;
use common\models\project\ProjectEstimateItem;
use common\models\project\ProjectEstimateItemSearch;
use common\models\project\ProjectEstimateItemSendForm;
use common\models\project\ProjectEstimateSearch;
use common\models\project\ProjectSearch;
use frontend\assets\PrintAsset;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\detailing\DetailingUserConfig;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use frontend\models\project\ProjectTaskForm;
use frontend\models\project\ProjectTaskFormFactory;
use frontend\models\project\RepositoryFactory;
use frontend\models\project\ProjectTaskSelectFormFactory;
use frontend\models\project\ProjectTask;
use frontend\rbac\permissions;
use frontend\themes\kub\helpers\Icon;
use frontend\rbac\UserRole;
use Intervention\Image\Exception\NotWritableException;
use Yii;
use common\models\project\Project;
use yii\base\Exception;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\FlowOfFundsReportSearch;
use yii\helpers\VarDumper;

/**
 * Class ProjectAjaxController
 * @package frontend\controllers
 */
class ProjectAjaxController extends FrontendController
{
    const VIEW = '@frontend/views/project/';

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\Project::INDEX],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetIndexPageData()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;

        if (Yii::$app->request->post('chart-detailing-index-ajax')) {

            $company = Yii::$app->user->identity->company;
            $onPage = 'project'; // (string)Yii::$app->request->post('on-page');
            $chartType = (int)Yii::$app->request->post('chart-type');
            $offset = (int)Yii::$app->request->post('offset');

            switch ($chartType) {
                case Config::CHART_TYPE_INCOME:
                case Config::CHART_TYPE_EXPENSE:
                    $analyticsModel = ProjectCommonCharts::getAnalyticsModel();
                    break;
                case Config::CHART_TYPE_PROFITABILITY:
                case Config::CHART_TYPE_PROFIT:
                case Config::CHART_TYPE_REVENUE:
                    $palModel = ProjectCommonCharts::getPalModel();
                    break;
                default:
                    $analyticsModel = ProjectCommonCharts::getAnalyticsModel();
                    $palModel = ProjectCommonCharts::getPalModel();
                    break;
            }

            return $this->renderPartial( self::VIEW . 'chart/_index_charts', [
                'isAjax' => true,
                'palModel' => $palModel ?? null,
                'analyticsModel' => $analyticsModel ?? null,
                'customChartType' => $chartType,
                'customOffset' => $offset,
                'onPage' => $onPage,
            ]);
        }

        return null;
    }

    /**
     * @return array
     */
    public function actionSetIndexPageChartType()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $onPage = (string)Yii::$app->request->post('on-page');
        $chartType = (int)Yii::$app->request->post('chart-type');

        if ($chartType == DetailingUserConfig::CHART_TYPE_PROFITABILITY || in_array($chartType, array_keys(DetailingUserConfig::$chartTypeName))) {
            $config = Yii::$app->user->identity->config;
            $config->updateAttributes([
                'detailing_index_chart_type' => (int)$chartType
            ]);

            return ['success' => 1];
        }

        return ['success' => 0];
    }

    /**
     * @return array
     */
    public function actionSetUserChartSeriesFilter()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $onPage = (string)Yii::$app->request->post('on-page');
        $checked = (array)Yii::$app->request->post('checked');

        foreach ($checked as &$c)
            if (!intval($c))
                unset($c);

        if (!$checked || in_array('all', $checked))
            Yii::$app->response->cookies->remove('project_chart_series_user_filter');
        else
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'project_chart_series_user_filter',
                'value' => $checked,
            ]));

        return ['success' => 1];
    }
}