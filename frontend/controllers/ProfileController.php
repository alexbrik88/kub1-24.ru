<?php
namespace frontend\controllers;

use common\components\filters\AccessControl;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use frontend\models\ChangeEmailForm;
use frontend\models\ChangeNotifyForm;
use frontend\models\ChangePasswordForm;
use frontend\models\ThemeForm;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use frontend\models\ChangeTimeZoneForm;
use common\components\getResponse\GetResponseApi;

/**
 * Site controller
 */
class ProfileController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'changepassword',
                            'changeemail',
                            'changenotify',
                            'changetimezone',
                            'theme',
                        ],
                        'allow' => true,
                        'roles' => [permissions\User::PROFILE_EDIT],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $employee = Yii::$app->user->identity;

        return $this->render('index', [
            'employee' => $employee,
        ]);
    }

    public function actionChangepassword()
    {
        $model = new ChangePasswordForm();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if (($p = $model->load(Yii::$app->request->post())) && ($s = $model->save())) {
            Yii::$app->getSession()->setFlash('success', 'Новый пароль сохранён.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении пароля.');
        }

        return $this->redirect(['profile/index']);
    }

    public function actionChangeemail()
    {
        $model = new ChangeEmailForm(Yii::$app->user->identity);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Адрес электронной почты изменён.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении адреса электронной почты.');
        }

        return $this->redirect(['index']);
    }

    public function actionTheme()
    {
        $model = new ThemeForm(Yii::$app->user->identity);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Настройка темы сохранена.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при сохранении настройки темы.');
        }

        return $this->redirect(['index']);
    }

    public function actionChangenotify()
    {
        $model = new ChangeNotifyForm();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->saveNotify()) {
            Yii::$app->getSession()->setFlash('success', 'Настройки уведомлений изменены.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении настроек уведомления.');
        }

        // Update contact in GetResponse service
        $gr = new GetResponseApi();
        $gr->updateGetResponseContact($model->user, [
            'notify' => [
                'newFeatures' => $model->notifyNewFeatures,
                'nearlyReport' => $model->notifyNearlyReport,
            ]
        ]);

        return $this->redirect(['profile/index']);
    }

    public function actionChangetimezone()
    {
        $model = new ChangeTimeZoneForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Часовой пояс изменен.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении часового пояса.');
        }

        return $this->redirect(['profile/index']);
    }

}
