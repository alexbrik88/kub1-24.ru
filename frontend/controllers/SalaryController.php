<?php

namespace frontend\controllers;

use common\models\EmployeeCompany;
use common\models\employee\EmployeeSalary;
use common\models\employee\EmployeeSalarySummary;
use frontend\components\FrontendController;
use frontend\models\EmployeeSalarySearch;
use frontend\models\EmployeeSalarySummarySearch;
use frontend\models\SalaryConfigForm;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalaryController implements the CRUD actions for EmployeeSalary model.
 */
class SalaryController extends FrontendController
{
    public $layout = 'employee';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'config-calc' => ['POST'],
                    'update-summary' => ['POST'],
                    'update-detail' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => 'common\components\filters\AjaxFilter',
                'only' => [
                    'config-calc',
                    'update-summary',
                    'update-detail',
                ],
            ],
        ];
    }

    /**
     * Lists all EmployeeSalarySummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;
        $date = new \DateTime('first day of this month');

        $query = EmployeeSalarySummary::find()->andWhere([
            'company_id' => $company->id,
            'date' => $date->format('Y-m-d'),
        ]);

        if (!$query->exists()) {
            $model = EmployeeSalarySummary::new($company, $date);
            $model->save();
        }

        $searchModel = new EmployeeSalarySummarySearch([
            'company_id' => $company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmployeeSalarySummary model.
     * @param string $date
     * @return mixed
     */
    public function actionSummary($date)
    {
        $model = $this->findEmployeeSalarySummary($date);

        $searchModel = new EmployeeSalarySearch([
            'company_id' => $model->company_id,
            'date' => $model->date,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('summary', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmployeeSalary model.
     * @param integer $id
     * @param string $date
     * @return mixed
     */
    public function actionDetail($id, $date)
    {
        return $this->render('detail', [
            'model' => $this->findEmployeeSalary($id, $date),
        ]);
    }

    /**
     * Updates an existing EmployeeSalarySummary model.
     * @param string $date
     * @return mixed
     */
    public function actionUpdateSummary($date)
    {
        $model = $this->findEmployeeSalarySummary($date);

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($model->load(Yii::$app->request->post())) {
            return $model->save();
        }

        return false;
    }

    /**
     * Updates an existing EmployeeSalary model.
     * @param integer $id
     * @param string $date
     * @return mixed
     */
    public function actionUpdateDetail($id, $date)
    {
        $model = $this->findEmployeeSalarySummary($date);

        foreach ($model->employeeSalaries as $item) {
            if ($item->employee_id == $id) {
                if ($item->load(Yii::$app->request->post())) {
                    return $model->save();
                } else {
                    break;
                }
            }
        }

        return false;
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionConfig($id)
    {
        $employeeCompany = $this->findEmployeeCompany($id);

        $model = new SalaryConfigForm([
            'employeeCompany' => $employeeCompany,
        ]);

        if (Yii::$app->getRequest()->getIsAjax()) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Настройки расчета зарплаты сохранены.');

            return $this->redirect(['/employee/view', 'id' => $id]);
        }

        return $this->render('config', ['model' => $model]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionConfigCalc($id)
    {
        $employeeCompany = $this->findEmployeeCompany($id);

        $form = new SalaryConfigForm([
            'employeeCompany' => $employeeCompany,
        ]);

        $form->load(Yii::$app->getRequest()->post());

        $result = $form->getFormatCalculatedData();

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'result' => $result,
        ];
    }

    /**
     * Finds the EmployeeCompany model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeCompany the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEmployeeCompany($id)
    {
        $company = Yii::$app->user->identity->company;

        $model = EmployeeCompany::find()->with(['employee'])->andWhere([
            'employee_id' => $id,
            'company_id' => $company->id,
        ])->one();

        if ($model && $model->employee && !$model->employee->is_deleted) {
            $model->populateRelation('company', $company);

            return $model;
        }

        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * Finds the EmployeeSalary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeSalary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEmployeeSalary($id, $date)
    {
        $company = Yii::$app->user->identity->company;

        $model = EmployeeSalary::findOne([
            'company_id' => $company->id,
            'employee_id' => $id,
            'date' => date_create($date)->modify('first day of this month')->format('Y-m-d'),
        ]);

        if ($model !== null) {
            $model->populateRelation('company', $company);

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the EmployeeSalarySummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeeSalarySummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEmployeeSalarySummary($date)
    {
        $company = Yii::$app->user->identity->company;

        $model = EmployeeSalarySummary::findOne([
            'company_id' => $company->id,
            'date' => date_create($date)->modify('first day of this month')->format('Y-m-d'),
        ]);

        if ($model !== null) {
            $model->populateRelation('company', $company);

            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
