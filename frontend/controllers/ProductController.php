<?php

namespace frontend\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\components\image\EasyThumbnailImage;
use common\models\address\Country;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\CompanyHelper;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\product\CustomPrice;
use common\models\product\CustomPriceGroup;
use common\models\product\PriceList;
use common\models\product\PriceListCheckedProducts;
use common\models\product\Product;
use common\models\product\ProductField;
use common\models\product\ProductGroup;
use common\models\product\ProductInitialBalance;
use common\models\product\ProductSearch;
use common\models\product\ProductStore;
use common\models\product\ProductStoreDaily;
use common\models\product\ProductUnit;
use common\models\product\Store;
use common\models\service\SubscribeTariffGroup;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\Documents;
use frontend\models\CompositeServiceForm;
use frontend\models\ProductMoveForm;
use frontend\models\ProductNdsForm;
use frontend\models\ProductPriceForm;
use frontend\models\ProductXyzSearch;
use frontend\models\SimpleProductSearch;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\bootstrap\Dropdown;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\prompt\PageType;
use backend\models\PromptHelper;
use backend\models\Prompt;
use common\models\TaxRate;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends FrontendController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'composite-item',
                            'set-new-group',
                            'search',
                            'get-products',
                            'get-products-table',
                            'get-units',
                            'get-xls',
                            'filter-date',
                            'add-modal-details-address',
                            'validate-company-modal',
                            'simple-index',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Product::INDEX],
                    ],
                    [
                        'actions' => ['create-store', 'update-store'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'create',
                            'add-group',
                            'add-price',
                            'add-to-price-list',
                            'create-service',
                            'create-service-composite',
                            'create-simple-product',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Product::CREATE],
                    ],
                    [
                        'actions' => ['view', 'reserve-documents'],
                        'allow' => true,
                        'roles' => [permissions\Product::VIEW],
                    ],
                    [
                        'actions' => ['update', 'group-edit', 'price-edit', 'many-price', 'move', 'archive', 'to-store',
                            'to-group', 'combine', 'to-archive', 'change-nds', 'to-store-from-archive', 'img-upload', 'change-custom-field-name'],
                        'allow' => true,
                        'roles' => [permissions\Product::UPDATE],
                    ],
                    [
                        'actions' => ['delete', 'group-delete', 'price-delete', 'many-delete'],
                        'allow' => true,
                        'roles' => [permissions\Product::DELETE],
                    ],
                    [
                        'actions' => ['get-products', 'turnover',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Invoice::CREATE, [
                                    'ioType' => Yii::$app->request->getQueryParam('documentType'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['get-products-table', 'get-new-product-form', 'create-from-invoice'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Invoice::CREATE, [
                                    'ioType' => Yii::$app->request->getBodyParam('documentType'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['xyz'],
                        'allow' => true,
                        // Удалить условие, когда задача будет принята
                        'matchCallback' => function ($rule, $action) {
                            return YII_ENV_DEV || Yii::$app->user->identity->company->id == 486;
                        },
                    ],
                    [
                        'actions' => ['config-custom-price'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update-all-prices-for-buy'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'simple-index',
                    'create-simple-product',
                    'get-products',
                    'to-group',
                    'combine',
                    'config-custom-price',
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'archive' => ['POST'],
                    'to-group' => ['POST'],
                    'combine' => ['POST'],
                    'change-nds' => ['POST'],
                    'config-custom-price' => ['POST'],
                    'change-custom-field-name' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Product models.
     *
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($productionType, $store = null)
    {
        $this->_checkProductionType($productionType);
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $storeList = [];

        $searchModel = new \frontend\models\ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
            'status' => Product::ACTIVE,
        ]);

        $priceList = new PriceList();
        $priceList->production_type = $productionType;
        $priceList->company_id = $company->id;
        $priceList->name = 'Прайс-лист_' . date(DateHelper::FORMAT_USER_DATE);
        $priceList->include_name_column = true;
        $priceList->include_reminder_column = true;
        $priceList->include_product_unit_column = true;
        $priceList->include_price_column = true;
        $priceList->sort_type = PriceList::SORT_BY_NAME;
        $priceList->populateRelation('company', $company);

        if ($store == 'archive') {
            $searchModel->storeArchive = true;
        }
        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $page_type = PageType::TYPE_PRODUCT;
            $storeList = $user
                ->getStores()
                ->select('name')
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
            $storeId = -1;
            if ($storeList) {
                $storeIds = array_keys($storeList);
                if ($store === null) {
                    $storeId = reset($storeIds);
                } elseif ($store == 'archive' || $store == 'all') {
                    $storeId = $storeIds;
                } elseif ($store) {
                    $storeId = $store;
                }
            }

            $searchModel->store_id = $storeId;
        } else {
            $page_type = PageType::TYPE_SERVICE;
            $storeId = null;
        }
        $getParams = Yii::$app->request->get();
        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, $company->company_type_id + 1, $page_type, true);
        $userConfig = Yii::$app->user->identity->config;
        $dataProvider = $searchModel->search($getParams, null, !$userConfig->product_zeroes_quantity);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $defaultSortingAttr = isset($getParams['defaultSorting']) ? $getParams['defaultSorting'] : ProductSearch::DEFAULT_SORTING_TITLE;
        $config = $user->config;
        if ($defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID && $config->product_group == false) {
            $config->product_group = true;
            $config->save(true, ['product_group']);
        }

        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 79);
        }
        if ($productionType == Product::PRODUCTION_TYPE_SERVICE) {
            \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 80);
        }
        Url::remember('', 'product_list');

        return $this->render('index', [
            'company' => $company,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productionType' => $productionType,
            'prompt' => $prompt,
            'storeList' => $storeList,
            'store' => $store,
            'storeId' => $storeId,
            'defaultSortingAttr' => $defaultSortingAttr,
            'priceList' => $priceList,
            'user' => $user,
        ]);
    }

    /**
     * Lists all Simple Product models.
     *
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSimpleIndex($productionType)
    {
        $this->_checkProductionType($productionType);

        $company = Yii::$app->user->identity->company;
        $searchModel = new SimpleProductSearch($company, $productionType);
        $dataProvider = $searchModel->search(ArrayHelper::merge(Yii::$app->request->get(), Yii::$app->request->post()));
        $dataProvider->pagination->pageSize = 10;

        return $this->renderAjax('simple-index', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'productionType' => $productionType,
        ]);
    }

    /**
     * Displays a single Product model.
     *
     * @param $productionType
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($productionType, $id)
    {
        $canABC = false;
        $abcModalTitle = null;
        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $canABC = BusinessAnalyticsAccess::can(BusinessAnalyticsAccess::SECTION_PRODUCTS) || Yii::$app->user->identity->company->getIsTrialNotPaid();
            if (!$canABC) {
                if (Yii::$app->request->get('mode') == 'abc') {
                    throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                }

                $absTariffList = SubscribeTariffGroup::find()->select([
                    'name' => 'CONCAT(\'"\', [[name]], \'"\')',
                ])->where([
                    'is_active' => true,
                    'id' => BusinessAnalyticsAccess::$sectionSubscribes[BusinessAnalyticsAccess::SECTION_PRODUCTS]
                ])->orderBy('name')->column();
                if ($absTariffList) {
                    $abcModalTitle = sprintf('Этот отчет доступен только на тарифах %s. Перейти к оплате тарифа?', implode(', ', $absTariffList));
                }
            }
        }

        $estimate = Yii::$app->request->get('estimate', null);

        $model = $this->findModel($productionType, $id, true);

        $model->clearPicturesTempDir();

        return $this->render('view', [
            'model' => $model,
            'canViewPriceForBuy' => Yii::$app->user->can(\frontend\rbac\permissions\Product::PRICE_IN_VIEW, ['model' => $model]),
            'estimate' => $estimate,
            'canABC' => $canABC,
            'abcModalTitle' => $abcModalTitle,
        ]);
    }

    /**
     * Toggle Product model status "archive".
     *
     * @param $productionType
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionArchive($productionType, $id)
    {
        $model = $this->findModel($productionType, $id);
        $model = $this->findModel($productionType, $id);

        switch ($model->status) {
            case Product::ACTIVE:
                $model->status = Product::ARCHIVE;
                $model->archived_at = time();
                $model->save(false);
                break;

            case Product::ARCHIVE:
                $model->status = Product::ACTIVE;
                $model->save(false);
                break;
        }

        return $this->redirect([
            'view',
            'productionType' => $productionType,
            'id' => $id,
        ]);
    }

    /**
     * Performs ajax validation
     * @param Product $model
     * @return mixed
     */
    protected function ajaxValidate($model)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model->load(Yii::$app->request->post());

        if ($model instanceof Product) {
            $model->loadInitialBalance(Yii::$app->request->post());
            if ($model->loadedInitialBalance) {
                $model->validateInitialBalance();

                return ActiveForm::validate($model, $model->loadedInitialBalance);
            }
        }

        return ActiveForm::validate($model);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param $productionType
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate($productionType)
    {
        $model = $this->createHelper($productionType);
        $model->setScenario(Product::SCENARIO_CREATE);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if (Yii::$app->user->identity->company->companyTaxationType->osno) {
            $model->price_for_buy_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            $model->price_for_sell_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
        } else {
            $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }

        $model->price_for_buy_with_nds = '0';

        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        $productField = ProductField::findOne(['company_id' => $user->company->id]);
        if (!$productField) {
            $productField = new ProductField(['company_id' => $user->company->id, 'title' => 'Ваше название']);
        }

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }
        if (($createFromProduct = Yii::$app->request->get('fromProduct')) && empty(Yii::$app->request->post())) {
            $model->setDataFromCopiedProduct($createFromProduct);
        }

        $model->loadSuppliers(Yii::$app->request->post('supplier'));
        $model->loadCustomPrices(Yii::$app->request->post('custom_price'));
        $model->loadInitialBalance(Yii::$app->request->post());
        $model->load(Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->validateSuppliers() && $model->validateCustomPrices()) {

            $model->save(false);
            $model->saveSuppliers();
            $model->saveCustomPrices();
            $model->savePictures(true);
            $model->saveInitialBalance();
            $message = $productionType == Product::PRODUCTION_TYPE_GOODS ? 'Товар добавлен' : 'Услуга добавлена';
            Yii::$app->session->setFlash('success', $message);

            return $this->redirect(['view', 'productionType' => $productionType, 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'canViewPriceForBuy' => Yii::$app->user->can(\frontend\rbac\permissions\Product::PRICE_IN_VIEW, ['model' => $model]),
                'productField' => $productField,
            ]);
        }
    }

    /**
     * Creates a new Simple Service.
     *
     * @return mixed
     */
    public function actionCreateService()
    {
        $model = $this->createHelper(Product::PRODUCTION_TYPE_SERVICE);
        $model->setScenario(Product::SCENARIO_CREATE);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Услуга добавлена');

            return $this->redirect(['view', 'productionType' => $model->production_type, 'id' => $model->id]);
        }

        return $this->renderAjax('create_service', [
            'company' => Yii::$app->user->identity->company,
            'model' => $model,
        ]);
    }

    /**
     * @return array|mixed|string
     */
    public function actionCreateSimpleProduct($productionType)
    {
        $this->_checkProductionType($productionType);
        $company = Yii::$app->user->identity->company;

        $model = $this->createHelper($productionType);
        $model->setScenario(Product::SCENARIO_CREATE);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if (Yii::$app->user->identity->company->companyTaxationType->osno) {
            $model->price_for_buy_nds_id = TaxRate::RATE_20;
            $model->price_for_sell_nds_id = TaxRate::RATE_20;
        } else {
            $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }

        $model->price_for_buy_with_nds = '0';

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $service = $this->createHelper(Product::PRODUCTION_TYPE_SERVICE);
            $service->setScenario(Product::SCENARIO_CREATE);

            return $this->renderAjax('create-simple-product-success', [
                'data' => [
                    'item' => $this->renderPartial('partial/_service_form_composite_item', [
                        'compositeModel' => new CompositeServiceForm($service),
                        'simpleProduct' => $model,
                        'quantity' => 1,
                    ]),
                    'services' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                    ])->count(),
                    'goods' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_GOODS,
                    ])->count(),
                ],
            ]);
        }

        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $title = 'Добавить товар/материал';
        } elseif ($productionType == Product::PRODUCTION_TYPE_SERVICE) {
            $title = 'Добавить услугу/работу';
        }

        return $this->renderAjax('create-simple-product', [
            'title' => $title,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Composite Service
     *
     * @return mixed
     */
    public function actionCreateServiceComposite()
    {
        $model = $this->createHelper(Product::PRODUCTION_TYPE_SERVICE);
        $model->setScenario(Product::SCENARIO_CREATE);
        $model->unit_type = Product::UNIT_TYPE_COMPOSITE;

        $compositeModel = new CompositeServiceForm($model);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            $compositeModel->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return ActiveForm::validate($model, $compositeModel);
        }

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($model->load($post) && $compositeModel->load($post)) {
                if (intval($model->validate()) * intval($compositeModel->validate())) {
                    $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $compositeModel) {
                        if ($model->save(false)) {
                            if ($compositeModel->save(false)) {
                                return true;
                            }
                        }

                        if ($db->getTransaction()->isActive) {
                            $db->getTransaction()->rollBack();
                        }

                        return false;
                    });

                    if ($isSaved) {
                        Yii::$app->session->setFlash('success', 'Составная услуга добавлена');

                        return $this->redirect(['view', 'productionType' => $model->production_type, 'id' => $model->id]);
                    }
                }
            }
        }

        return $this->renderAjax('create_service', [
            'company' => Yii::$app->user->identity->company,
            'model' => $model,
            'compositeModel' => $compositeModel,
        ]);
    }

    /**
     * Creates a new Composite Service
     *
     * @return mixed
     */
    public function actionCompositeItem()
    {
        $items = [];
        $ids = (array) Yii::$app->request->get('id');

        $product = $this->createHelper(Product::PRODUCTION_TYPE_SERVICE);
        $product->setScenario(Product::SCENARIO_CREATE);

        $compositeModel = new CompositeServiceForm($product);

        foreach ($ids as $id) {
            $simpleProduct = $this->findModel(null, $id);
            $items[] = $this->renderPartial('partial/_service_form_composite_item', [
                'compositeModel' => $compositeModel,
                'simpleProduct' => $simpleProduct,
                'quantity' => 1,
            ]);
        }

        return implode("\n", $items);
    }

    /**
     * @param $productionType
     *
     * @return Product
     * @throws NotFoundHttpException
     */
    protected function createHelper($productionType)
    {
        $this->_checkProductionType($productionType);

        $company = Yii::$app->user->identity->company;

        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = $company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;
        $model->product_unit_id = ProductUnit::UNIT_COUNT;
        $model->price_for_buy_with_nds = '0';
        $model->group_id = ProductGroup::WITHOUT;

        if ($company->companyTaxationType->osno) {
            $model->price_for_buy_nds_id = TaxRate::RATE_20;
            $model->price_for_sell_nds_id = TaxRate::RATE_20;
        } else {
            $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }

        return $model;
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param $productionType
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($productionType, $id)
    {
        $activeTab = Yii::$app->request->get('tab') ?: Yii::$app->request->post('tab');

        $this->_checkProductionType($productionType);
        $model = $this->findModel($productionType, $id);
        $model->setScenario(Product::SCENARIO_UPDATE);
        $compositeModel = $model->unit_type == Product::UNIT_TYPE_COMPOSITE ? new CompositeServiceForm($model) : null;

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            if ($compositeModel) {
                $compositeModel->load(Yii::$app->request->post());
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $compositeModel ? ActiveForm::validate($model, $compositeModel) : ActiveForm::validate($model);
        }

        if ($model->creator_id == null) {
            $model->creator_id = Yii::$app->user->identity->id;
        }

        $model->loadSuppliers(Yii::$app->request->post('supplier'));
        $model->loadCustomPrices(Yii::$app->request->post('custom_price'));
        $model->loadCategoryItems(Yii::$app->request->post('category_items'));
        $model->loadInitialBalance(Yii::$app->request->post());

        $productField = ProductField::findOne(['company_id' => $model->company_id]);
        if (!$productField) {
            $productField = new ProductField(['company_id' => $model->company_id, 'title' => 'Ваше название']);
        }

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            if ($compositeModel) {
                $compositeModel->load(Yii::$app->request->post());
            }
            if (intval($model->validate()) *
                intval($model->validateSuppliers()) *
                intval($model->validateCustomPrices()) *
                intval($model->validateInitialBalance()) *
                intval($compositeModel === null || $compositeModel->validate())
            ) {
                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $compositeModel) {
                    if ($model->save(false) &&
                        $model->saveSuppliers() &&
                        $model->saveCustomPrices() &&
                        $model->savePictures() &&
                        $model->saveCategoryItems() &&
                        $model->saveInitialBalance() &&
                        ($compositeModel === null || $compositeModel->save(false))
                    ) {
                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });

                if ($isSaved) {
                    // update price for buy
                    if (!$model->price_for_buy_with_nds && $model->initialBalance && $model->initialBalance->price) {
                        $model->updateAttributes(['price_for_buy_with_nds' => $model->getGrowingPriceForBuy()]);
                    }
                    $message = $productionType == Product::PRODUCTION_TYPE_GOODS ? 'Товар изменен' : 'Услуга изменена';
                    Yii::$app->session->setFlash('success', $message);

                    return $this->redirect(['view', 'productionType' => $productionType, 'id' => $model->id, 'tab' => $activeTab ?: null]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'compositeModel' => $compositeModel,
            'productField' => $productField,
            'canViewPriceForBuy' => Yii::$app->user->can(\frontend\rbac\permissions\Product::PRICE_IN_VIEW, ['model' => $model])
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $productionType
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($productionType = null, $id)
    {
        $model = $this->findModel($productionType, $id);

        if ($model->delete()) {
            $message = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товар удален' : 'Услуга удалена';
            Yii::$app->session->setFlash('success', $message);
        } else {
            Yii::$app->session->setFlash('error', $model->deleteError);
        }

        return $this->redirect(['index', 'productionType' => $model->production_type]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $productionType
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionManyDelete($productionType)
    {
        $company = Yii::$app->user->identity->company;
        $deletedCount = 0;
        $inInvoiceCount = $inAutoinvoiceCount = $inOrderDocumentCount = $inPriceListCount = [];
        if ($idArray = Yii::$app->request->post('selection')) {
            foreach ($idArray as $id) {
                $model = Product::find()->byDeleted(false)->byUser()->andWhere([
                    'id' => $id,
                    'production_type' => $productionType,
                    'company_id' => $company->id,
                ])->one();

                if ($model !== null) {
                    if ($model->getInvoices()->andWhere(['is_deleted' => false])->exists()) {
                        $inInvoiceCount[] = '"' . $model->title . '"';
                    } elseif ($model->getInvoicesAuto()->andWhere(['is_deleted' => false])->exists()) {
                        $inAutoinvoiceCount[] = '"' . $model->title . '"';
                    } elseif ($model->getOrderDocuments()->andWhere(['is_deleted' => false])->exists()) {
                        $inOrderDocumentCount[] = '"' . $model->title . '"';
                    } elseif ($model->getPriceLists()->andWhere(['is_deleted' => false])->exists()) {
                        $inPriceListCount[] = '"' . $model->title . '"';
                    } else {
                        if ($model->getOrders()->exists() || $model->getOrderDocumentProducts()->exists()) {
                            $model->is_deleted = true;
                            if ($model->save(false, ['is_deleted'])) {
                                $deletedCount++;
                            }
                        } else {
                            try {
                                if ($model->delete()) {
                                    $deletedCount++;
                                }
                            } catch (\yii\db\IntegrityException $e) {
                                $model->is_deleted = true;
                                if ($model->save(false, ['is_deleted'])) {
                                    $deletedCount++;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($deletedCount > 0) {
            $message = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товаров удалено: ' : 'Услуг удалено: ';
            Yii::$app->session->setFlash('success', $message . $deletedCount);
        }
        $error = [];
        if ($inInvoiceCount) {
            $error[] = 'Не удалось удалить по причине использования в счете: ' . implode(', ', $inInvoiceCount);
        }
        if ($inAutoinvoiceCount) {
            $error[] = 'Не удалось удалить по причине использования в шаблоне австосчета: ' . implode(', ', $inAutoinvoiceCount);
        }
        if ($inOrderDocumentCount) {
            $error[] = 'Не удалось удалить по причине использования в заказе: ' . implode(', ', $inOrderDocumentCount);
        }
        if ($inPriceListCount) {
            $error[] = 'Не удалось удалить по причине использования в прайс-листе: ' . implode(', ', $inPriceListCount);
        }
        if ($error) {
            Yii::$app->session->setFlash('error', $error);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return mixed
     */
    public function actionGetUnits()
    {
        $productType = Yii::$app->request->post('productType', 1);

        $units = ProductUnit::findSorted()
            ->andWhere($productType ? ['goods' => 1] : ['services' => 1])
            ->all();

        $unitsOptions = [];
        foreach ($units as $unit) {
            $unitsOptions[$unit->id] = ['title' => $unit->title];
        }

        $model = new Product();
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'html' => Html::activeDropDownList($model, 'product_unit_id', ArrayHelper::map($units, 'id', 'name'), [
                'class' => 'form-control field-width',
                'prompt' => '',
                'options' => $unitsOptions,
            ]),
        ];
    }

    /**
     * @return string|NotFoundHttpException
     */
    public function actionGetProducts()
    {
        $documentType = Yii::$app->request->get('documentType', Documents::IO_TYPE_OUT);
        $productType = Yii::$app->request->get('productType', -1);
        $storeId = Yii::$app->request->get('store_id');
        $contractorId = Yii::$app->request->get('contractorId');

        $searchModel = new ProductSearch();
        $searchModel->filterStatus = ProductSearch::IN_WORK;
        $searchModel->documentType = $documentType;
        $searchModel->production_type = $productType;
        $searchModel->company_id = Yii::$app->user->identity->company->id;
        $searchModel->title = Yii::$app->request->get('title', '');
        $searchModel->exclude = Yii::$app->request->get('exists', []);
        $searchModel->store_id = $storeId;

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = 10;

        $this->layout = 'empty';

        return $this->render('partial/productInvoiceList', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentType' => $documentType,
            'productType' => $productType,
            'contractorId' => $contractorId,
            'storeId' => $storeId
        ]);
    }

    /**
     * @param bool|false $toOrderDocument
     * @param int $documentType
     * @return array|ActiveRecord[]
     */
    public function actionGetProductsTable($toOrderDocument = false, $documentType = 2)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $product = new Product();

        return $product->getProductsByIds(
            Yii::$app->request->post('in_order'),
            Yii::$app->user->identity->company->id,
            $toOrderDocument,
            $documentType,
            Yii::$app->request->post('contractorId')
        );
    }

    /**
     * @return string
     */
    public function actionGetNewProductForm()
    {
        $documentType = Yii::$app->request->getBodyParam('documentType');
        $productionType = Yii::$app->request->post('production_type');
        $model = $this->createHelper($productionType);

        $this->layout = 'empty';

        return $this->render('partial/newProductForm', [
            'model' => $model,
            'documentType' => $documentType,
        ]);
    }

    /**
     * @param $productionType integer
     * @return string
     */
    public function actionCreateFromInvoice($productionType)
    {
        $documentType = Yii::$app->request->getBodyParam('documentType');
        $model = $this->createHelper($productionType);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return json_encode($model->getProductsByIds([$model->id], Yii::$app->user->identity->company->id));
        }

        return $this->renderAjax('partial/newProductForm', [
            'model' => $model,
            'documentType' => $documentType,
        ]);
    }

    /**
     * @param $productionType integer
     * @return string
     */
    public function actionSearch($q = null, $production_type = null, $unit_type = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $productArray = $q ? $company->getProducts()
            ->select(['id', 'title text'])
            ->andWhere([
                'is_deleted' => false,
                'status' => Product::ACTIVE,
            ])
            ->andWhere([
                'or',
                ['like', 'title', $q],
                ['like', 'article', $q . '%', false],
            ])
            ->andFilterWhere([
                'production_type' => $production_type,
                'unit_type' => $unit_type,
            ])
            ->limit(5)
            ->asArray()
            ->all() : [];

        return $productArray;
    }

    /**
     * @return mixed|string
     */
    public function actionAddGroup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new ProductGroup([
            'title' => Yii::$app->request->post('title'),
            'production_type' => Yii::$app->request->post('productionType'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        return $model->save() ? ['itemId' => $model->id, 'itemName' => $model->title] : [];
    }

    /**
     * @return array
     * @throws \yii\db\StaleObjectException
     */
    public function actionGroupDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = ProductGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Группа товара не найдена.',
            ];
        }
        if ($model->getProducts()
            ->andWhere(['is_deleted' => false])
            ->exists()) {
            return [
                'success' => false,
                'message' => 'Группу товара удалить нельзя, так как в ней есть товары.',
            ];
        }
        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Группа товара успешно удалена.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Группу товара не удалось удалить.',
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionGroupEdit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = ProductGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Группа товара не найдена.',
            ];
        }
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->title,
            ];
        } else {
            return [
                'success' => false,
                'message' => $model->getFirstError('title'),
            ];
        }
    }

    /**
     *
     * @return mixed
     */
    public function actionManyPrice($productionType)
    {
        $this->_checkProductionType($productionType);
        $company = Yii::$app->user->identity->company;
        $model = new ProductPriceForm($company);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->changePrice();
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['index', 'productionType' => $productionType]);
    }

    /**
     *
     * @return mixed
     */
    public function actionMove($id)
    {
        $product = $this->findModel(null, $id);
        $model = new ProductMoveForm($product);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!Yii::$app->request->isAjax) {
                return $this->redirect(['view', 'productionType' => $product->production_type, 'id' => $id]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('move', [
                'model' => $model,
            ]);
        } else {
            return $this->render('move', [
                'model' => $model,
            ]);
        }
    }

    /**
     *
     * @return mixed
     */
    public function actionChangeNds()
    {
        $model = new ProductNdsForm([
            'company' => Yii::$app->user->identity->company,
        ]);

        if (Yii::$app->request->post('ajax') == 'product-nds-form') {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }

        if (Yii::$app->request->isAjax) {
            return;
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Moving the entire selected product from one store to another store
     *
     * @return mixed
     */
    public function actionToStore()
    {
        $company = Yii::$app->user->identity->company;
        $countType = Yii::$app->request->post('count_type', 0);
        $numberCount = Yii::$app->request->post('number_count', 0);
        $toStore = Yii::$app->user->identity->getStores()->andWhere([
            'store.id' => Yii::$app->request->post('to_store_id'),
        ])->one();
        if ($toStore !== null) {
            $fromStore = Yii::$app->user->identity->getStores()->andWhere([
                'and',
                ['store.id' => Yii::$app->request->post('from_store_id')],
                ['not', ['store.id' => $toStore->id]],
            ])->one();
            $productArray = Yii::$app->user->identity->company
                ->getProducts()
                ->with('company')
                ->byDeleted()
                ->byUser()
                ->andWhere([
                    'product.id' => Yii::$app->request->post('product_id'),
                ])
                ->all();

            if ($fromStore !== null && $productArray) {
                foreach ($productArray as $product) {
                    $from = ProductStore::findOne([
                        'product_id' => $product->id,
                        'store_id' => $fromStore->id,
                    ]);
                    $fromStoreDaily = ProductStoreDaily::initial($product->id, $fromStore->id, $company);
                    $toStoreDaily = ProductStoreDaily::initial($product->id, $toStore->id, $company);
                    if ($from !== null) {
                        if ($from->quantity != 0) {
                            $to = ProductStore::findOne([
                                'product_id' => $product->id,
                                'store_id' => $toStore->id,
                            ]) ?: new ProductStore([
                                'product_id' => $product->id,
                                'store_id' => $toStore->id,
                            ]);
                            if ($countType == Product::MOVE_ALL) {
                                $to->updateAttributes(['quantity' => ($to->quantity + $from->quantity)]);
                                $from->updateAttributes(['quantity' => 0]);
                                $toStoreDaily->addition($from->quantity);
                                $fromStoreDaily->subtract($from->quantity);
                            } elseif ($countType == Product::MOVE_PARTIAL && $numberCount > 0) {
                                if ($from->quantity > $numberCount) {
                                    $to->updateAttributes(['quantity' => ($to->quantity + $numberCount)]);
                                    $from->updateAttributes(['quantity' => ($from->quantity - $numberCount)]);
                                    $toStoreDaily->addition($numberCount);
                                    $fromStoreDaily->subtract($numberCount);
                                } else {
                                    $to->updateAttributes(['quantity' => ($to->quantity + $from->quantity)]);
                                    $from->updateAttributes(['quantity' => 0]);
                                    $toStoreDaily->addition($from->quantity);
                                    $fromStoreDaily->subtract($from->quantity);
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @throws \yii\db\Exception
     */
    public function actionToGroup()
    {
        $group = ($group_id = Yii::$app->request->post('group_id')) ? ProductGroup::findOne([
            'and',
            ['id' => $group_id],
            [
                'or',
                'company_id' => Yii::$app->user->identity->company->id,
                'company_id' => null,
            ],
        ]) : null;

        if ($group !== null) {
            Yii::$app->db->createCommand()->update(Product::tableName(), ['group_id' => $group->id], [
                'id' => Yii::$app->request->post('product_id'),
                'company_id' => Yii::$app->user->identity->company->id,
            ])->execute();
        }
    }

    /**
     * @throws \yii\db\Exception
     */
    public function actionToArchive()
    {
        Yii::$app->db->createCommand()->update(Product::tableName(), [
            'status' => Product::ARCHIVE
        ], [
            'id' => Yii::$app->request->post('product_id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ])->execute();
    }

    /**
     * @throws \yii\db\Exception
     */
    public function actionToStoreFromArchive()
    {
        Yii::$app->db->createCommand()->update(Product::tableName(), [
            'status' => Product::ACTIVE
        ], [
            'id' => Yii::$app->request->post('product_id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ])->execute();
    }

    /**
     *
     * @return mixed
     */
    public function actionCombine()
    {
        $product = Product::findOne([
            'id' => Yii::$app->request->post('target_id'),
            'company_id' => Yii::$app->user->identity->company->id,
            'status' => Product::ACTIVE,
            'is_deleted' => false,
        ]);
        if ($product !== null) {
            $nameList = $product->combineProductsToThis(Yii::$app->request->post('product_id', []));
            if ($nameList) {
                $nameList = implode(', ', $nameList);
                $nameProductionType = (Yii::$app->request->post('production_type', Product::PRODUCTION_TYPE_GOODS) == Product::PRODUCTION_TYPE_SERVICE) ?
                    'Услуги' : 'Товары';
                Yii::$app->session->setFlash('success', "{$nameProductionType} {$product->title}, {$nameList} объединены в {$product->title}.");
            }
        }
    }

    /**
     * @param $productionType
     * @param null $store
     * @throws NotFoundHttpException
     */
    public function actionGetXls($productionType, $store = null)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        Yii::$app->response->format = Response::FORMAT_RAW;

        $this->_checkProductionType($productionType);
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
            'status' => Product::ACTIVE,
        ]);
        if ($store == 'archive') {
            $searchModel->storeArchive = true;
        }
        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $storeList = $user
                ->getStores()
                ->select(['name'])
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
            $storeId = -1;
            if ($storeList) {
                $storeIds = array_keys($storeList);
                if ($store === null) {
                    $storeId = reset($storeIds);
                } elseif ($store == 'archive' || $store == 'all') {
                    $storeId = $storeIds;
                } elseif ($store) {
                    $storeId = $store;
                }
            }

            $searchModel->store_id = $storeId;
        }

        $getParams = Yii::$app->request->get();
        $products = $searchModel->search($getParams);
        $products->pagination->pageSize = \frontend\components\PageSize::get();
        if (isset($getParams['sort'])) {
            if ($getParams['sort'][0] == '-') {
                $attrName = mb_substr($getParams['sort'], 1);
                $sort = SORT_DESC;
            } else {
                $attrName = $getParams['sort'];
                $sort = SORT_ASC;
            }
            $orderBy = [$attrName => $sort];
        } else {
            if (isset($getParams['defaultSorting'])) {
                if ($getParams['defaultSorting'] == ProductSearch::DEFAULT_SORTING_GROUP_ID) {
                    $orderAttribute = 'product_group.title';
                } else {
                    $orderAttribute = $getParams['defaultSorting'];
                }
            } else {
                $orderAttribute = 'title';
            }
            $orderBy = [$orderAttribute => SORT_ASC];
        }

        Product::generateXlsTable($products->query->orderBy($orderBy)->all(), $productionType);
    }

    /**
     * @param $productionType
     * @param null $store
     * @throws NotFoundHttpException
     */
    public function actionReserveDocuments($id)
    {
        $product = $this->findModel(Product::PRODUCTION_TYPE_GOODS, $id);
        $documents = $product->getReserveDocuments();
        $result = [];
        foreach ($documents['invoices'] as $doc) {
            $result[] = Html::a("Счет № {$doc->fullNumber} от " . date('d.m.Y', strtotime($doc->document_date)), [
                '/documents/invoice/view',
                'type' => $doc->type,
                'id' => $doc->id,
            ]);
        }
        foreach ($documents['orderDocuments'] as $doc) {
            $result[] = Html::a("Заказ № {$doc->fullNumber} от " . date('d.m.Y', strtotime($doc->document_date)), [
                '/documents/order-document/view',
                'id' => $doc->id,
            ]);
        }

        return $this->asJson([
            'result' => $result ? implode('<br>', $result) : 'Резерв не найден',
        ]);
    }

    /**
     * @param $productionType
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreateStore($productionType)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $store = Yii::$app->request->post('store');
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            $model = new Store([
                'company_id' => $company->id,
            ]);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'store' => $model->id]);

                /*
                $user = Yii::$app->user->identity;
                $storeSelectList = $user->getStores()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC])
                    ->indexBy('id')
                    ->column();
                $hasArchive = $company->getProducts()->andWhere([
                    'production_type' => $productionType,
                    'status' => Product::ARCHIVE,
                ])->exists();
                $storeItems = [];
                if ($storeSelectList) {
                    if (count($storeSelectList) > 1) {
                        $storeSelectList += ['all' => 'Все склады'];
                    }
                }
                if ($hasArchive) {
                    if (count($storeSelectList) === 0) {
                        $storeSelectList[''] = 'Склад';
                    }
                    $storeSelectList += ['archive' => 'Архив'];
                }
                foreach ($storeSelectList as $key => $name) {
                    $priceList = Yii::$app->request->get('priceList');
                    $storeItems[] = [
                        'label' => $name .
                            (!in_array($key, ['all', 'archive']) ? '<span class="glyphicon glyphicon-pencil update-store ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/store/update', 'id' => $key]) . '"></span>' : null),
                        'url' => Url::to([$priceList ? 'add-to-price-list' : 'index', 'productionType' => $productionType, 'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK], 'store' => $key ?: null, 'priceList' => $priceList]),
                        'linkOptions' => [
                            'class' => $store == $key ? 'active' : '',
                        ],
                    ];
                }
                $storeItems[] = [
                    'label' => '[ + ДОБАВИТЬ СКЛАД ]',
                    'options' => [
                        'class' => 'add-store ajax-modal-btn',
                        'data-title' => 'Добавить склад',
                        'data-url' => Url::to(['/store/create']),
                    ],
                ];

                return [
                    'result' => true,
                    'id' => $model->id,
                    'name' => $model->name,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $storeItems,
                    ]),
                ];
                */
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/store/create', [
                    'model' => $model,
                ]),
            ];
        }

        throw new BadRequestHttpException();
    }

    /**
     * @param $id
     * @param $productionType
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdateStore($id, $productionType)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $store = Yii::$app->request->post('store');
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            $model = Store::findOne($id);
            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /* @var $user \common\models\employee\Employee */
                $user = Yii::$app->user->identity;
                $storeSelectList = $user->getStores()
                    ->select('name')
                    ->orderBy(['is_main' => SORT_DESC])
                    ->indexBy('id')
                    ->column();
                $hasArchive = $company->getProducts()->andWhere([
                    'production_type' => $productionType,
                    'status' => Product::ARCHIVE,
                ])->exists();
                $storeItems = [];
                if ($storeSelectList) {
                    if (count($storeSelectList) > 1) {
                        $storeSelectList += ['all' => 'Все склады'];
                    }
                }
                if ($hasArchive) {
                    if (count($storeSelectList) === 0) {
                        $storeSelectList[''] = 'Склад';
                    }
                    $storeSelectList += ['archive' => 'Архив'];
                }
                foreach ($storeSelectList as $key => $name) {
                    $priceList = Yii::$app->request->get('priceList');
                    $storeItems[] = [
                        'label' => $name .
                            (!in_array($key, ['all', 'archive']) ? '<span class="glyphicon glyphicon-pencil update-store ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/store/update', 'id' => $key]) . '"></span>' : null),
                        'url' => Url::to([$priceList ? 'add-to-price-list' : 'index', 'productionType' => $productionType, 'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK], 'store' => $key ?: null, 'priceList' => $priceList]),
                        'linkOptions' => [
                            'class' => $store == $key ? 'active' : '',
                        ],
                    ];
                }
                $storeItems[] = [
                    'label' => '[ + ДОБАВИТЬ СКЛАД ]',
                    'options' => [
                        'class' => 'add-store ajax-modal-btn',
                        'data-title' => 'Добавить склад',
                        'data-url' => Url::to(['/store/create']),
                    ],
                ];

                return [
                    'result' => true,
                    'id' => $model->id,
                    'name' => $model->name,
                    'label' => $store == $model->id ? $model->name : null,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $storeItems,
                    ]),
                ];
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/store/update', [
                    'model' => $model,
                ]),
            ];
        }

        throw new BadRequestHttpException();
    }

    /**
     * @param $productionType
     * @param $priceList
     * @param null $store
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddToPriceList($productionType, $priceList, $store = null)
    {
        if (Yii::$app->request->isAjax) {
            return $this->redirect(['add-to-price-list'] + Yii::$app->request->get());
        }
        $this->_checkProductionType($productionType);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $storeList = [];

        /* @var PriceList $priceList */
        $priceList = PriceList::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['id' => $priceList])
            ->andWhere(['production_type' => $productionType])
            ->one();
        if ($priceList === null || $priceList->include_type_id !== PriceList::INCLUDE_SELECTIVELY_PRODUCTS) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
            'status' => Product::ACTIVE,
        ]);

        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $storeList = $user->getStores()
                ->select(['name'])
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
            $storeIds = array_keys($storeList);
            if ($store === null) {
                $store = $user->getStores()->orderBy(['is_main' => SORT_DESC])->select('id')->scalar();
            } elseif ($store == 'archive') {
                $searchModel->status = Product::ARCHIVE;
            }
            $searchModel->store_id = $store == 'all' ?
                $company->getStores()->select('id')->column() :
                (in_array($store, $storeIds) ? $store : -1);
        } else {
            if ($store == 'archive') {
                $searchModel->status = Product::ARCHIVE;
            }
        }
        $getParams = Yii::$app->request->get();
        $dataProvider = $searchModel->search($getParams);
        if (!$priceList->priceListCheckedProducts) {
            $priceListCheckedProducts = new PriceListCheckedProducts();
            $priceListCheckedProducts->price_list_id = $priceList->id;
            /* @var $query ActiveQuery */
            $query = clone $dataProvider->query;
            $totalProductsId = $query->column();
            $priceListCheckedProducts->checked_products = json_encode($totalProductsId);
            $priceListCheckedProducts->save();
            $priceList->populateRelation('priceListCheckedProducts', $priceListCheckedProducts);
        }
        $dataProvider->pagination->pageSize = PageSize::get();
        $defaultSortingAttr = isset($getParams['defaultSorting']) ? $getParams['defaultSorting'] : ProductSearch::DEFAULT_SORTING_TITLE;

        return $this->render('add-to-price-list', [
            'company' => $company,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productionType' => $productionType,
            'storeList' => $storeList,
            'store' => $store,
            'defaultSortingAttr' => $defaultSortingAttr,
            'priceList' => $priceList,
        ]);
    }

    /**
     * @param $actionType
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionFilterDate($actionType)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $dateType = Yii::$app->request->post('date_type');
            $dateFrom = Yii::$app->request->post('date_from');
            $dateTo = Yii::$app->request->post('date_to');
            $dateText = null;

            switch ($actionType) {
                case 'set':
                    switch ($dateType) {
                        case ProductSearch::FILTER_YESTERDAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE, strtotime('-1 day'));
                            break;
                        case ProductSearch::FILTER_TODAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE);
                            break;
                        case ProductSearch::FILTER_WEEK:
                            $dateFrom = date(DateHelper::FORMAT_USER_DATE, strtotime('Mon this week'));
                            $dateTo = date(DateHelper::FORMAT_USER_DATE, strtotime('Sun this week'));
                            $dateText = "{$dateFrom} - {$dateTo}";
                            break;
                        case ProductSearch::FILTER_MONTH:
                            $dateFrom = date('01.m.Y');
                            $dateTo = date(cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) . '.m.Y');
                            $dateText = FlowOfFundsReportSearch::$month[date('m')] . ' ' . date('Y');
                            break;
                        default:
                            throw new BadRequestHttpException('Incorrect date type.');
                            break;
                    }
                    break;
                case 'plus':
                    switch ($dateType) {
                        case ProductSearch::FILTER_YESTERDAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 day', strtotime($dateFrom)));
                            break;
                        case ProductSearch::FILTER_TODAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 day', strtotime($dateFrom)));
                            break;
                        case ProductSearch::FILTER_WEEK:
                            $dateFrom = date(DateHelper::FORMAT_USER_DATE, strtotime('+7 days', strtotime($dateFrom)));
                            $dateTo = date(DateHelper::FORMAT_USER_DATE, strtotime('+7 days', strtotime($dateTo)));
                            $dateText = "{$dateFrom} - {$dateTo}";
                            break;
                        case ProductSearch::FILTER_MONTH:
                            $dateFrom = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 month', strtotime($dateFrom)));
                            $dateTo = date(DateHelper::FORMAT_USER_DATE, strtotime('+1 month', strtotime($dateTo)));
                            $dateText = FlowOfFundsReportSearch::$month[date('m', strtotime($dateFrom))] . ' ' . date('Y', strtotime($dateFrom));
                            break;
                        default:
                            throw new BadRequestHttpException('Incorrect date type.');
                            break;
                    }
                    break;
                case 'minus':
                    switch ($dateType) {
                        case ProductSearch::FILTER_YESTERDAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE, strtotime('-1 day', strtotime($dateFrom)));
                            break;
                        case ProductSearch::FILTER_TODAY:
                            $dateFrom = $dateTo = $dateText = date(DateHelper::FORMAT_USER_DATE, strtotime('-1 day', strtotime($dateFrom)));
                            break;
                        case ProductSearch::FILTER_WEEK:
                            $dateFrom = date(DateHelper::FORMAT_USER_DATE, strtotime('-7 days', strtotime($dateFrom)));
                            $dateTo = date(DateHelper::FORMAT_USER_DATE, strtotime('-7 days', strtotime($dateTo)));
                            $dateText = "{$dateFrom} - {$dateTo}";
                            break;
                        case ProductSearch::FILTER_MONTH:
                            $dateFrom = date(DateHelper::FORMAT_USER_DATE, strtotime('-1 month', strtotime($dateFrom)));
                            $dateTo = date(DateHelper::FORMAT_USER_DATE, strtotime('-1 month', strtotime($dateTo)));
                            $dateText = FlowOfFundsReportSearch::$month[date('m', strtotime($dateFrom))] . ' ' . date('Y', strtotime($dateFrom));
                            break;
                        default:
                            throw new BadRequestHttpException('Incorrect date type.');
                            break;
                    }
                    break;
                default:
                    throw new BadRequestHttpException('Incorrect action type.');
                    break;
            }

            return [
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'dateText' => $dateText,
            ];
        }

        throw new BadRequestHttpException();
    }


    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $productionType
     * @param integer $id
     *
     * @param bool $allowDeleted
     *
     * @return Product the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($productionType, $id, $allowDeleted = false)
    {
        /* @var Product $model */
        $query = Product::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byUser()
            ->andWhere([
                'id' => $id,
            ]);

        if ($productionType !== null) {
            $query->byProductionType($productionType);
        }

        $model = $query->one();

        if ($model !== null && ($model->status != Product::DELETED || $allowDeleted)) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена');
        }
    }

    /**
     * @param int $productionType
     *
     * @throws NotFoundHttpException
     */
    private function _checkProductionType($productionType)
    {
        if (!isset(Product::$productionTypes[$productionType])) {
            throw new NotFoundHttpException('Не указан тип товара.');
        }
    }

    /**
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionTurnover($productionType, $turnoverType)
    {
        $this->_checkProductionType($productionType);

        $page_type = $productionType == Product::PRODUCTION_TYPE_GOODS ? PageType::TYPE_PRODUCT : PageType::TYPE_SERVICE;
        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, Yii::$app->user->identity->company->company_type_id + 1, $page_type, true);

        $searchModel = new \frontend\models\ProductTurnoverSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'production_type' => $productionType,
            'turnoverType' => $turnoverType,
            'status' => Product::ACTIVE,
        ]);

        $userConfig = Yii::$app->user->identity->config;
        $dataProvider = $searchModel->search(Yii::$app->request->get(), !$userConfig->product_zeroes_turnover);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        Url::remember('', 'product_list');
        if (Yii::$app->request->get('xls')) {
            $dataProvider->pagination->pageSize = 0;
            return Product::generateTurnoverXlsTable($searchModel, $dataProvider);
        }

        return $this->render('turnover', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productionType' => $productionType,
            'prompt' => $prompt,
        ]);
    }

    /**
     * @param int $period
     * @return string
     */
    public function actionXyz($period = ProductXyzSearch::DEFAULT_PERIOD)
    {
        $searchModel = new ProductXyzSearch([
            'period' => $period,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $searchModel->buildHeaderTableData();

        return $this->render('xyz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array|mixed
     */
    public function actionAddModalDetailsAddress()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if ($model->load(Yii::$app->request->post()) && $checkingAccountant->load(Yii::$app->request->post())) {
            $modelValidate = $model->validate();
            $accountValidate = $checkingAccountant->validate();
            if ($modelValidate && $accountValidate) {
                $result = Yii::$app->db->transaction(function (Connection $db) use ($checkingAccountant, $model) {
                    if ($checkingAccountant->_save($model)) {
                        $model->populateRelation('mainCheckingAccountant', $checkingAccountant);
                        $model->chief_is_chief_accountant = true;
                        if ($model->setDirectorInitials() && $model->save()) {

                            return true;
                        }
                    }
                    $db->transaction->rollBack();

                    return false;
                });

                if ($result && $model->strict_mode == Company::OFF_STRICT_MODE) {
                    return [
                        'name' => $model->name_short,
                        'type' => $model->companyType->name_short,
                    ];
                }
            }
        }

        $header = $this->renderAjax('@frontend/modules/documents/views/invoice/partial/create-company_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('@frontend/modules/documents/views/invoice/partial/create-company_body', [
            'checkingAccountant' => $checkingAccountant,
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed
     */
    public function actionValidateCompanyModal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $checkingAccountant->load(Yii::$app->request->post());

            return ActiveForm::validate($model, $checkingAccountant);
        }
    }

    /**
     * @return mixed|string
     */
    public function actionAddPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new CustomPriceGroup([
            'name' => Yii::$app->request->post('title'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        return $model->save() ? ['itemId' => $model->id, 'itemName' => $model->name] : [];
    }

    /**
     * @return array
     * @throws \yii\db\StaleObjectException
     */
    public function actionPriceDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = CustomPriceGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Цена не найдена.',
            ];
        }

        if (CustomPrice::find()->where(['price_group_id' => $model->id])->exists()) {
            return [
                'success' => false,
                'message' => 'Цену удалить нельзя, так как к ней привязаны товары.',
            ];
        }
        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Цена успешно удалена.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Цену не удалось удалить.',
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionPriceEdit()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = CustomPriceGroup::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Цена не найдена.',
            ];
        }
        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->name,
            ];
        } else {
            return [
                'success' => false,
                'message' => $model->getFirstError('name'),
            ];
        }
    }

    /**
     * @return mixed
     */
    public function actionConfigCustomPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $prices = CustomPriceGroup::find()->where(['company_id' => Yii::$app->user->identity->company->id])->all();
        $post = Yii::$app->request->post();
        $ret = [];
        foreach ($prices as $price) {
            $id = 'custom_price_' . $price->id;
            $ret[$id] = $price->show_column = (isset($post[$id])) ? 1 : 0;
            $price->updateAttributes(['show_column']);
        }

        return $ret;
    }


    /**
     * @param $id
     * @param $attr
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionImgUpload($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $attr = Yii::$app->request->post('attr');

        if ($id) {
            $model = Product::find()
                ->byCompany(Yii::$app->user->identity->company->id)
                ->byUser()
                ->andWhere([
                    'id' => $id,
                ])->one();
        } else {
            $model = new Product();
            $model->company_id = Yii::$app->user->identity->company->id;
            $model->id = 0; // for tmp files
        }

        if (!$model || !in_array($attr, ['pic1', 'pic2', 'pic3', 'pic4'])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if ($file = UploadedFile::getInstanceByName($attr)) {
            if (in_array($file->type, ['image/jpeg', 'image/png'])) {

                // режим просмотра
                $uploadPath = $model->getUploadPath();

                // режим редактир-я
                if (!Yii::$app->request->post('immediately'))
                    $uploadPath .= DIRECTORY_SEPARATOR . 'temp';

                $filename = $uploadPath . DIRECTORY_SEPARATOR . $attr . '.' . $file->extension;
                if (!file_exists($uploadPath)) {
                    FileHelper::createDirectory($uploadPath);
                }
                $file->saveAs($filename);

                return ['status' => 'success', 'src' => EasyThumbnailImage::thumbnailSrc($filename, 193, 136)];

            } else {

                return ['status' => 'error', 'message' => 'Допустимый формат файла: jpg, jpeg, png.'];

            }
        }

        return ['status' => 'error', 'message' => 'Ошибка загрузки файла.'];
    }

    /**
     * @return mixed
     */
    public function actionChangeCustomFieldName()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $productField = ProductField::findOne(['company_id' => Yii::$app->user->identity->company->id]);
        if (!$productField) {
            $productField = new ProductField(['company_id' => Yii::$app->user->identity->company->id, 'title' => 'Ваше название']);
        }
        $productField->load(Yii::$app->request->post());
        $productField->title = htmlspecialchars($productField->title);
        if ($productField->save()) {
            return ['result' => 1, 'title' => $productField->title];
        }

        return ['result' => 0, 'errors' => $productField->getErrors()];
    }

    /**
     * @return Response
     */
    public function actionUpdateAllPricesForBuy()
    {
        ini_set('max_execution_time', 7200);
        $allProductsIds = Product::find()->where(['company_id' => Yii::$app->user->identity->company->id])->select('id')->column();
        $cnt = 0;
        foreach ($allProductsIds as $productId) {
            if ($product = Product::findOne($productId)) {
                $growingPrice = $product->getGrowingPriceForBuy();
                $product->updateAttributes(['price_for_buy_with_nds' => $growingPrice]);
                $cnt++;
            }
        }
        Yii::$app->session->setFlash('success', "Обновлено {$cnt} продуктов/услуг");

        return $this->redirect(Yii::$app->request->referrer ?: ['index', 'productionType' => 1]);
    }
}
