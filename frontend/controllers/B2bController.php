<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.01.2019
 * Time: 15:24
 */

namespace frontend\controllers;


use backend\models\Prompt;
use backend\models\PromptHelper;
use common\components\filters\AccessControl;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\out\OutInvoice;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\product\ProductType;
use common\models\prompt\PageType;
use common\models\service\SubscribeTariffGroup;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\models\OutInvoiceSearch;
use frontend\modules\donate\models\DonateWidgetSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class B2bController
 * @package frontend\controllers
 */
class B2bController extends FrontendController
{
    public $layout = 'b2b';

    private $_redirectAction = 'company';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int $activeTab
     * @return string
     */
    public function actionIndex($activeTab = Contractor::TAB_INVOICE_BENEFIT)
    {
        $this->actionIsAllowed($this->action->id);

        return $this->redirect($this->_redirectAction);
    }

    // STEP 1
    public function actionCompany()
    {
        CheckingAccountant::$formNameModify = false;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = $user->company;
        $taxation = $model->companyTaxationType;
        $accounts = $model->getCheckingAccountants()->orderBy([
            'type' => SORT_ASC,
            'id' => SORT_ASC,
        ])->all();
        if (empty($accounts)) {
            $accounts[] = new CheckingAccountant([
                'company_id' => $model->id,
                'type' => CheckingAccountant::TYPE_MAIN,
            ]);
        }
        if ($accounts[0]->type != CheckingAccountant::TYPE_MAIN) {
            $accounts[0]->type = CheckingAccountant::TYPE_MAIN;
        }

        $model->scenario = $model->company_type_id == CompanyType::TYPE_IP ?
            Company::SCENARIO_B2B_IP :
            Company::SCENARIO_B2B_OOO;

        // To delete accounts
        $deleteRs = Yii::$app->request->post('delete_rs');
        if (!empty($deleteRs)) {
            foreach ((array)$deleteRs as $deleteId) {
                $deleteModel = CheckingAccountant::findOne($deleteId);
                if ($deleteModel) {
                    try {
                        $deleteModel->delete();
                    } catch (\Exception $e) {
                        Yii::$app->session->setFlash('error', "Невозможно удалить р/с " . $deleteModel->rs);
                    }
                }
            }
        }

        if (Yii::$app->request->getIsPost()) {
            $newCount = max(0, count(Yii::$app->request->post('CheckingAccountant')) - count($accounts));
            if ($newCount) {
                // Creatin new CheckingAccountant instances
                for ($i=0; $i < $newCount; $i++) {
                    $accounts[] = new CheckingAccountant([
                        'company_id' => $model->id,
                        'type' => CheckingAccountant::TYPE_ADDITIONAL,
                    ]);
                }
            }

            $isLoad = $model->load(Yii::$app->request->post());
            $isLoad = $taxation->load(Yii::$app->request->post()) && $isLoad;
            $isLoad = Model::loadMultiple($accounts, Yii::$app->request->post()) && $isLoad;

            if (Yii::$app->request->post('ajax')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $result = array_merge(ActiveForm::validate($model, $taxation), ActiveForm::validateMultiple($accounts));

                return $result;
            }

            if ($isLoad && Model::validateMultiple($accounts)) {
                $isSaved = \Yii::$app->db->transaction(function ($db) use ($model, $taxation, $accounts) {
                    if ($model->save() && $taxation->save()) {
                        foreach ($accounts as $account) {
                            $account->save();
                        }

                        return true;
                    }

                    if ($db->getTransaction()->isActive) {
                        $db->getTransaction()->rollBack();
                    }

                    return false;
                });

                if ($isSaved) {
                    return $this->redirect("description");
                }
            }
        }

        return $this->render('company', [
            'model' => $model,
            'taxation' => $taxation,
            'accounts' => $accounts,
        ]);
    }

    private function getAdditionalAccounts($company) {

        return CheckingAccountant::find()
            ->byCompany($company->id)
            ->byType(CheckingAccountant::TYPE_ADDITIONAL)
            ->all();
    }

    private function getNewAccounts($company) {
        // To add new accounts
        $newAccounts = [];
        for ($i=1; $i<=10; $i++) {
            $newAccounts[] = new CheckingAccountant(['id' => 'new_'.$i, 'isNewRecord'=>false, 'company_id' => $company->id, 'type' => CheckingAccountant::TYPE_ADDITIONAL]);
        }

        return $newAccounts;
    }

    private function validateAccounts($accounts, $skipEmpty = false) {
        $validation = [];
        foreach ((array)$accounts as $a) {
            if ($a->load(Yii::$app->request->post()) && (!$skipEmpty || trim($a->bik)))
                $validation = array_merge($validation, ActiveForm::validate($a));
        }

        return $validation;
    }

    private function saveAccounts($accounts) {
        list($additionalAccounts, $newAccounts) = $accounts;
        foreach ($additionalAccounts as $aa) {
            if (!$aa->load(Yii::$app->request->post()) || !$aa->save())
                return false;
        }

        foreach ($newAccounts as $na) {
            if ($na->load(Yii::$app->request->post()) && trim($na->bik)) {
                $na->id = null;
                $na->isNewRecord = true;
                if (!$na->save())
                    return false;
            }
        }

        return true;
    }

    // STEP 2
    public function actionDescription() {

        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }

        $company = Yii::$app->user->identity->company;

        return $this->render('description', [
            'company' => $company,
            'isPaid' => $this->isPaid
        ]);
    }

    // STEP 3
    public function actionProducts() {

        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }

        return $this->myProducts(3, Product::PRODUCTION_TYPE_GOODS);
    }

    // STEP 4
    public function actionServices() {

        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }

        return $this->myProducts(4, Product::PRODUCTION_TYPE_SERVICE);
    }

    // STEP 5
    public function actionModule() {

        if (!$this->actionIsAllowed($this->action->id)) {
            return $this->redirect($this->_redirectAction);
        }

        $this->dropDemoInvoices();
        $this->refreshLinksStatuses();

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $company->createStoreUser();
        $outInvoiceSearch = new OutInvoiceSearch([
            'company_id' => $company->id,
        ]);
        $outInvoiceProvider = $outInvoiceSearch->search(Yii::$app->request->queryParams);
        $donateWidgetSearch = new DonateWidgetSearch([
            'company_id' => $company->id,
        ]);
        $donateWidgetProvider = $donateWidgetSearch->search(Yii::$app->request->queryParams);

        return $this->render('module', [
            'outInvoiceSearch' => $outInvoiceSearch,
            'outInvoiceProvider' => $outInvoiceProvider,
            'donateWidgetSearch' => $donateWidgetSearch,
            'donateWidgetProvider' => $donateWidgetProvider,
            'company' => $company,
            'isPaid' => $this->isPaid
        ]);
    }

    private function dropDemoInvoices() {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $invoices = Invoice::find()->where([
            'company_id' => $company->id,
            'from_demo_out_invoice' => true,
        ])->andWhere(['<', 'created_at', time() - 300])->all();

        foreach ($invoices as $invoice) {
            /** @var Invoice $invoice */
            Yii::$app->db->transaction(function (Connection $db) use ($invoice) {
                if ($invoice->deletingAll()) {

                    return true;
                }

                $db->transaction->rollBack();

                return false;
            });

        }
    }

    private function refreshLinksStatuses() {
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $isPaid = $this->getIsPaid();

        $invoices = OutInvoice::find()->where([
            'company_id' => $company->id,
            'status' => array_keys(OutInvoice::$statusAvailable)
        ])->all();

        /** @var OutInvoice $invoice */
        foreach ($invoices as $invoice) {
            if ($isPaid && $invoice->status == OutInvoice::INACTIVE)
                $invoice->updateAttributes(['status' => OutInvoice::ACTIVE]);
            if (!$isPaid && $invoice->status == OutInvoice::ACTIVE)
                $invoice->updateAttributes(['status' => OutInvoice::INACTIVE]);
        }
    }

    private function myProducts($stepNum, $productionType) {

        $store = null;

        //$this->_checkProductionType($productionType);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $storeList = [];

        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
            'status' => Product::ACTIVE,
        ]);

        if (null === Yii::$app->request->get('ProductSearch["filterStatus"]', null)) {
            $searchModel->filterStatus = ProductSearch::IN_WORK;
        }

        if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
            $page_type = PageType::TYPE_PRODUCT;
            $storeList = $user
                ->getStores()
                ->select(['name'])
                ->orderBy(['is_main' => SORT_DESC])
                ->indexBy('id')
                ->column();
            $storeIds = array_keys($storeList);

            if ($store === null) {
                $store = $user->getStores()->orderBy(['is_main' => SORT_DESC])->select('id')->scalar();
            } elseif ($store == 'archive') {
                $store == 'all';
                $searchModel->status = Product::ARCHIVE;
            }

            $searchModel->store_id = $store == 'all' ?
                $company->getStores()->select('id')->column() :
                (in_array($store, $storeIds) ? $store : -1);
        } else {
            $page_type = PageType::TYPE_SERVICE;
            if ($store == 'archive') {
                $searchModel->status = Product::ARCHIVE;
            }
        }
        $getParams = Yii::$app->request->get();
        $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, $company->company_type_id + 1, $page_type, true);
        $dataProvider = $searchModel->search($getParams);
        $dataProvider->pagination->pageSize = PageSize::get();

        $defaultSortingAttr = isset($getParams['defaultSorting']) ? $getParams['defaultSorting'] : ProductSearch::DEFAULT_SORTING_TITLE;
        $config = $user->config;
        if ($defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID && $config->product_group == false) {
            $config->product_group = true;
            $config->save(true, ['product_group']);
        }

        if ($company->strict_mode) {
            $referrer = Yii::$app->request->referrer;
            Yii::$app->session->setFlash('error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' . Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => Url::current(),
                ])) . '.'
            );
        }
        Url::remember('', 'product_list');

        return $this->render('products', [
            'company' => $company,
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productionType' => $productionType,
            'prompt' => $prompt,
            'storeList' => $storeList,
            'store' => $store,
            'defaultSortingAttr' => $defaultSortingAttr,
            'priceList' => null,
            'user' => $user,
            'step' => $stepNum
        ]);

    }

    /**
     * @return boolean
     */
    private function actionIsAllowed($action)
    {
        $setFlash = ($action === 'index') ? false : true;

        if ($action == 'company') {
            return true;
        }

        /** @var Company $company */
        $company = Yii::$app->user->identity->company;

        $company->scenario = $company->company_type_id == CompanyType::TYPE_IP ?
            Company::SCENARIO_B2B_IP :
            Company::SCENARIO_B2B_OOO;

        if (!$company->validate() || $company->mainCheckingAccountant === null || !$company->companyTaxationType->validate()) {
            $this->_redirectAction = '/b2b/company';
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо заполнить реквизиты.");
            }
            return false;
        }
        if ($action == 'description') {
            return true;
        }

        if ($action == 'products' || $action == 'services') {
            return true;
        }

        if ($action == 'module') {
            return true;
        }

        $this->_redirectAction = '/b2b/module';

        return false;
    }

    /**
     * @return integer
     */
    public function getIsPaid()
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'b2b-paid', false)) {
            return true;
        }

        $company = Yii::$app->user->identity->company;
        return $company->getHasActualSubscription(SubscribeTariffGroup::B2B_PAYMENT);
    }
}
