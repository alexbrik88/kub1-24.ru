<?php

namespace frontend\controllers;

use common\components\zchb\Card;
use common\components\zchb\Contact;
use common\components\zchb\CourtArbitration;
use common\components\zchb\Diff;
use common\components\zchb\Enforcement;
use common\components\zchb\ZCHBAPIException;
use common\models\Company;
use common\models\DossierLog;
use common\models\employee\Employee;
use common\models\service\SubscribeTariffGroup;
use frontend\components\DossierController;
use frontend\models\FsspTask;
use Yii;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Response;

class DossierCompanyController extends DossierController
{
    /** @var Company */
    private $_company;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $this->_company = $employee->company;
    }

    /**
     * @return string
     */
    public function actionAjaxDebt()
    {
        try {
            $task = FsspTask::createOrGet(FsspTask::ENTITY_TYPE_COMPANY, $this->_company->id);
            if ($task->status !== FsspTask::STATUS_SUCCESS && $task->status !== FsspTask::STATUS_REQUEST) {
                $task->restart(
                    $this->_company->companyType,
                    $this->_company->name_full,
                    [
                        $this->_company->address_legal_postcode,
                        $this->_company->address_legal,
                        $this->_company->address_actual_postcode,
                        $this->_company->address_actual
                    ]);
            }
            //Если загрузить данные не удалось, то попробовать снова
            if ($task->status === FsspTask::STATUS_ERROR) {
                if ($task->date === null || time() < strtotime($task->date ?? '-1') + FsspTask::RELOAD_TIMEOUT_ERROR) {
                    $task = null;
                } else {
                    $task->restart(
                        $this->_company->companyType,
                        $this->_company->name_full,
                        [
                            $this->_company->address_legal_postcode,
                            $this->_company->address_legal,
                            $this->_company->address_actual_postcode,
                            $this->_company->address_actual
                        ]);
                }
            } elseif ($task->status === FsspTask::STATUS_REQUEST) {
                $task->loadTaskData($this->_company->name_full, $this->_company->companyType);
            }
            return $this->renderTab([
                'modelId' => 0,
                'task' => $task
            ]);
        } catch (Exception $e) {
            return $this->renderTab([], 'ajax-exception');
        }

    }

    /**
     * Очищает кэш, что приведёт к повторной загрузке данных
     * @return Response
     * @throws InvalidConfigException
     */
    public function actionCacheClear(): Response
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $subscribe = $company->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR);
        if (!$subscribe->getIsLimitReached()) {
            $model = new DossierLog([
                'employee_id' => $employee->id,
                'company_id' => $company->id,
                'contractor_id' => null,
                'created_at' => time(),
                'expired_at' => $subscribe->expired_at,
            ]);
            if ($model->save(false)) {
                Card::clearCache($this->_company->ogrn ?? $this->_company->inn);
                Contact::clearCache($this->_company->ogrn ?? $this->_company->inn);
                CourtArbitration::clearCache($this->_company->ogrn ?? $this->_company->inn);
                Diff::clearCache($this->_company->ogrn ?? $this->_company->inn);
                Enforcement::clearCache($this->_company->ogrn ?? $this->_company->inn);
                $task = FsspTask::findByEntity(FsspTask::ENTITY_TYPE_COMPANY, $this->_company->id);
                if ($task !== null) {
                    /** @var FsspTask $task */
                    $task->restart(
                        $this->_company->companyType,
                        $this->_company->name_full,
                        [
                            $this->_company->address_legal_postcode,
                            $this->_company->address_legal,
                            $this->_company->address_actual_postcode,
                            $this->_company->address_actual
                        ]);
                }

                return $this->redirect(Url::current(['index']));
            }
        }

        return $this->redirect(Url::current(['tariff']));
    }

    /**
     * Тарифы
     * @return string
     */
    public function actionTariff()
    {
        $tariffGroup = SubscribeTariffGroup::findOne(SubscribeTariffGroup::CHECK_CONTRACTOR);
        $tariffList = $tariffGroup->getActualTariffs()->orderBy([
            'price' => SORT_ASC,
        ])->all();

        return $this->renderTab([
            'isAllowedDossier' => true,
            'company' => $this->_company,
            'tariffGroup' => $tariffGroup,
            'tariffList' => $tariffList,
        ]);
    }

    public function tabLink(string $tab)
    {
        return ['/dossier-company/' . $tab];

    }

    protected function currentTab()
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return substr(Yii::$app->request->resolve()[0], 16);
    }

    protected function getModelId()
    {
        return $this->_company->id;
    }

    protected function cardId()
    {
        return $this->_company->ogrn ?? $this->_company->inn;
    }

    protected function renderTab(array $data, string $viewFile = null)
    {
        $this->layout = 'empty';
        if (Yii::$app->request->get('ajax') === '1') {
            $data = [
                'dossierTab' => '../dossier/' . ($viewFile ?? $this->action->id),
                'dossierData' => $data
            ];
            return $this->render('//dossier/_container', $data);
        } else {
            return $this->render('//dossier/' . ($viewFile ?? $this->action->id), $data);
        }
    }

    protected function contractorTitle()
    {
        return $this->_company->getShortName();
    }

    /**
     * @inheritDoc
     * @param Action $action
     */
    public function beforeAction($action)
    {
        if ($this->_isAllowedDossier() === false) {
            $action->actionMethod = 'actionTariff';
            $action->id = 'tariff';
        }

        return parent::beforeAction($action);
    }

    private function _isAllowedDossier()
    {
        if (($allowed = ArrayHelper::getValue(Yii::$app->params, 'companyDossierAllowed')) !== null) {
            return (bool)$allowed;
        }
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $dossierQuery = $company->getDossierLogs()->andWhere([
            'and',
            ['contractor_id' => null],
            ['inn' => $company->inn],
            ['>=', 'expired_at', time()],
        ]);
        if ($dossierQuery->exists()) {
            return true;
        }

        if ($subscribe = $company->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR)) {
            $dossierQuery = $company->getDossierLogs()->andWhere([
                'and',
                ['contractor_id' => null],
                ['inn' => $company->inn],
                ['>=', 'created_at', $subscribe->activated_at],
            ]);
            if ($dossierQuery->exists()) {
                return true;
            } elseif (!$subscribe->getIsLimitReached()) {
                $model = new DossierLog([
                    'employee_id' => $employee->id,
                    'company_id' => $company->id,
                    'contractor_id' => null,
                    'inn' => $company->inn,
                    'created_at' => time(),
                    'expired_at' => $subscribe->expired_at,
                ]);

                return $model->save(false);
            }
        }

        return false;
    }
}