<?php

namespace frontend\components;

use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\service\SubscribeTariffGroup;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Slass BusinessAnalyticsAccess
 */
class BusinessAnalyticsAccess extends \yii\base\Component
{
    /**
     * Разделы аналитики
     */
    const SECTION_FINANCE = 'finance';
    const SECTION_FINANCE_PLUS = 'finance_plus';
    const SECTION_MARKETING = 'marketing';
    const SECTION_MARKETING_PLUS = 'marketing_plus';
    const SECTION_SALES = 'sales';
    const SECTION_PRODUCTS = 'products';
    const SECTION_INVOICE_PAYMENT_LIMIT = 'invoice_payment_limit';

    /**
     * Для каких тарифов, какой раздел доступен
     *
     * @var array
     */
    public static $sectionSubscribes = [
        self::SECTION_FINANCE => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_FINANCE,
        ],
        self::SECTION_FINANCE_PLUS => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
        ],
        self::SECTION_MARKETING => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_MARKETING_PLUS,
            SubscribeTariffGroup::BI_MARKETING,
        ],
        self::SECTION_MARKETING_PLUS => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_MARKETING_PLUS,
        ],
        self::SECTION_SALES => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_MARKETING_PLUS,
            SubscribeTariffGroup::BI_MARKETING,
            SubscribeTariffGroup::BI_SALES,
            SubscribeTariffGroup::BI_FINANCE,
        ],
        self::SECTION_PRODUCTS => [
            SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_PRODUCTS,
        ],
        self::SECTION_INVOICE_PAYMENT_LIMIT => [
            SubscribeTariffGroup::BI_ALL_INCLUSIVE,
            SubscribeTariffGroup::BI_FINANCE_PLUS,
            SubscribeTariffGroup::BI_FINANCE,
        ]
    ];

    private static $_subscribeArray;

    private static $_tariffNameArray;

    private static $_tariffGroupArray;

    /**
     * @param $section string
     * @return boolean
     */
    public static function can($section, $throw = false)
    {
        if (self::matchEmployee($section)) {
            if (self::matchSubscribe($section)) {
                if (self::matchLimit($section)) {
                    return true;
                } elseif ($throw) {
                    $limit = self::sectionSubscribeLimit($section);

                    throw new \yii\web\ForbiddenHttpException("Превышен лимит сотрудников ({$limit})");
                }
            } elseif ($throw) {
                $tarriffNameArray = self::sectionTariffNames($section);

                throw new \yii\web\ForbiddenHttpException(
                    'Чтобы получить доступ в раздел, необходимо ' .
                    \yii\helpers\Html::a('оплатить', ['/subscribe/default/index'], [
                        'class' => 'link',
                        'data-method' => 'POST',
                        'data-params' => [
                            'tariff_group_id[]' => end(self::$sectionSubscribes[$section]),
                        ],
                    ]) .
                    ' один из тарифов: ' . implode(', ', $tarriffNameArray)
                );
            }
        } elseif ($throw) {
            throw new \yii\web\ForbiddenHttpException('Нет доступа к разделу');
        }

        return false;
    }

    /**
     * @param $section string
     * @return array
     */
    public static function indexRoute()
    {
        $company = Yii::$app->user->identity->company ?? null;
        if ($company && $company->analytics_module_activated) {
            return self::hasFlows() ? ['/analytics/finance/odds'] : ['/analytics/options/levels'];
        }

        return ['/analytics/options/start'];
    }

    /**
     * @param $section string
     * @return boolean
     */
    public static function hasFlows()
    {
        $company = Yii::$app->user->identity->company ?? null;
        if ($company && $company->analytics_module_activated) {
            return $company->getCashBankFlows()->exists() ||
                   $company->getCashOrderFlows()->exists() ||
                   $company->getCashEmoneyFlows()->exists();
        }

        return false;
    }

    /**
     * @param $section
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     */
    public static function canAccess($section)
    {
        // Remote employee
        //if ((Yii::$app->user->id ?? null) == Yii::$app->params['service']['remote_employee_id'])
        //    return true;

        if (self::matchEmployee($section)) {
            if (self::matchSubscribe($section)) {
                if (self::matchLimit($section)) {
                    return true;
                }

                throw new \yii\web\ForbiddenHttpException('Превышен лимит сотрудников');
            }

            $tarriffNameArray = self::sectionTariffNames($section);

            throw new \yii\web\ForbiddenHttpException(
                'Чтобы получить доступ в раздел, необходимо ' .
                \yii\helpers\Html::a('оплатить', ['/subscribe/default/index'], [
                    'class' => 'link',
                    'data-method' => 'POST',
                    'data-params' => [
                        'tariff_group_id[]' => end(self::$sectionSubscribes[$section]),
                    ],
                ]) .
                ' один из тарифов: ' . implode(', ', $tarriffNameArray)
            );
        }

        throw new \yii\web\ForbiddenHttpException('Нет доступа к разделу');
    }

    /**
     * @param $section string
     * @return array
     */
    public static function sectionSubscribeArray(string $section)
    {
        if (!isset(self::$_subscribeArray[$section])) {
            self::$_subscribeArray[$section] = [];
            $company = Yii::$app->user->identity->company ?? null;
            if ($company !== null) {
                $tariffIdArray = self::$sectionSubscribes[$section] ?? [];
                foreach ($tariffIdArray as $tariffId) {
                    if ($subscribe = $company->getActualSubscription($tariffId)) {
                        self::$_subscribeArray[$section][] = $subscribe;
                    }
                }
            }
        }

        return self::$_subscribeArray[$section];
    }

    /**
     * @param $section string
     * @return array
     */
    public static function sectionTariffGroups(string $section)
    {
        if (!isset(self::$_tariffGroupArray[$section])) {
            $tariffIdArray = self::$sectionSubscribes[$section] ?? [];
            self::$_tariffGroupArray[$section] = SubscribeTariffGroup::find()->where([
                'id' => $tariffIdArray,
                'is_active' => true,
            ])->indexBy('id')->all();
        }

        return self::$_tariffGroupArray[$section];
    }

    /**
     * @param $section string
     * @return array
     */
    public static function sectionTariffNames(string $section)
    {
        if (!isset(self::$_tariffNameArray[$section])) {
            self::$_tariffNameArray[$section] = [];
            $tariffGroupArray = self::sectionTariffGroups($section);
            foreach ($tariffGroupArray as $tariffGroup) {
                self::$_tariffNameArray[$section][] = '"'.$tariffGroup->name.'"';
            }
        }

        return self::$_tariffNameArray[$section];
    }

    /**
     * @param $section string
     * @return integer
     */
    public static function sectionSubscribeLimit($section)
    {
        $limitArray = [0];

        foreach (self::sectionSubscribeArray($section) as $subscribe) {
            $limitArray[] = $subscribe->tariff_limit;
        }

        return max($limitArray);
    }

    /**
     * @param $section string
     * @return boolean
     */
    public static function matchCompanyId($section = null)
    {
        $company_id = Yii::$app->user->identity->company->id ?? -1;
        $allow_ids = (array) (Yii::$app->params['can_analytics_ids'] ?? []);

        return in_array($company_id, $allow_ids);
    }

    /**
     * @param $section string
     * @return boolean
     */
    public static function matchSubscribe($section)
    {
        $freeAccess = ArrayHelper::getValue(Yii::$app->params, ['analytics-free-access']);

        return $freeAccess || count(self::sectionSubscribeArray($section)) > 0;
    }

    /**
     * @param $section string
     * @return boolean
     */
    public static function matchLimit($section)
    {
        if (($employee = Yii::$app->user->identity) && ($company = $employee->company)) {
            if (ArrayHelper::getValue(Yii::$app->params, ['analytics-free-access'])) {
                return true;
            }

            $limit = self::sectionSubscribeLimit($section);
            $ids = EmployeeCompany::find()->select('employee_id')->where([
                'company_id' => $company->id,
                'can_business_analytics' => true,
            ])->orderBy([
                'can_business_analytics_time' => SORT_ASC,
            ])->limit($limit)->column();

            return in_array($employee->id, $ids);
        }

        return false;
    }

    /**
     * @param $section string
     * @return boolean
     */
    public static function matchEmployee($section)
    {
        /** @var $employeeCompany EmployeeCompany */
        if (($employee = Yii::$app->user->identity) && ($employeeCompany = $employee->currentEmployeeCompany))
        {
            switch ($section) {
                case self::SECTION_FINANCE:
                case self::SECTION_FINANCE_PLUS:
                    return $employeeCompany->can_ba_finance || $employeeCompany->can_ba_all;
                case self::SECTION_MARKETING:
                case self::SECTION_MARKETING_PLUS:
                    return $employeeCompany->can_ba_marketing || $employeeCompany->can_ba_all;
                case self::SECTION_PRODUCTS:
                    return $employeeCompany->can_ba_product || $employeeCompany->can_ba_all;
                case self::SECTION_SALES:
                    return $employeeCompany->can_ba_sales || $employeeCompany->can_ba_all;
            }
        }

        return false;
    }
}
