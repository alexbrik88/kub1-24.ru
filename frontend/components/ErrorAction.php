<?php

namespace frontend\components;

use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\base\UserException;

class ErrorAction extends \yii\web\ErrorAction
{
    protected function getExceptionMessage() : string
    {
        if ($this->exception instanceof UserException) {
            $message = $this->exception->getMessage();
            $isDemo = Yii::$app->user->id == (Yii::$app->params['service']['demo_employee_id'] ?? -1);
            if ($isDemo && $this->exception->statusCode == 403) {
                $message = "У Вас нет прав на данное действие в Демо аккаунте.";
            }

            return $message;
        }

        return $this->defaultMessage;
    }
}
