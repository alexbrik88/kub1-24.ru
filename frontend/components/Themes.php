<?php

namespace frontend\components;

use frontend\modules\cash\modules\banking\components\Banking;
use Yii;
use yii\helpers\ArrayHelper;

class Themes extends \yii\base\Component
{
    public function init()
    {
        parent::init();

        if ($this->isNewKubUrl()) {
            // always new Kub theme for some requests
            Yii::$app->params['bsVersion'] = '4';
            $bandle = Yii::$app->assetManager->getBundle('frontend\themes\kub\assets\KubAsset');
            Yii::$app->params['kubAssetBaseUrl'] = $bandle->baseUrl;
            Yii::$app->params['kubAssetBasePath'] = $bandle->basePath;
            Yii::$app->view->theme = Yii::createObject([
                'class' => 'yii\base\Theme',
                'basePath' => '@frontend/themes/kub',
                'baseUrl' => '@web/themes/kub',
                'pathMap' => [
                    '@common/models/file/widgets/views' => '@frontend/themes/kub/file',
                    '@frontend/views' => '@frontend/themes/kub/views',
                    '@frontend/modules' => '@frontend/themes/kub/modules',
                    '@frontend/widgets' => '@frontend/themes/kub/widgets',
                ],
            ]);
        } else if (Yii::$app->user->getIsMtsUser()) {
            Yii::$app->params['bsVersion'] = '4';
            $bandle = Yii::$app->assetManager->getBundle('frontend\themes\mts\assets\MtsAsset');
            Yii::$app->params['mtsAssetBaseUrl'] = $bandle->baseUrl;
            Yii::$app->params['mtsAssetBasePath'] = $bandle->basePath;
            Yii::$app->view->theme = Yii::createObject([
                'class' => 'yii\base\Theme',
                'basePath' => '@frontend/themes/mts',
                'baseUrl' => '@web/themes/mts',
                'pathMap' => [
                    '@common/models/file/widgets/views' => '@frontend/themes/mts/file',
                    '@frontend/views' => '@frontend/themes/mts/views',
                    '@frontend/modules' => '@frontend/themes/mts/modules',
                    '@frontend/widgets' => '@frontend/themes/mts/widgets',
                ],
            ]);
        } else {
            if ($this->isMobaleThemeRoute() && (Yii::$app->detect->isMobile() || isset(Yii::$app->params['isMobileTheme']))) {
                Yii::$app->params['bsVersion'] = '4';
                $bandle = Yii::$app->assetManager->getBundle('frontend\themes\mobile\assets\MobileAsset');
                Yii::$app->params['kubAssetBaseUrl'] = $bandle->baseUrl;
                Yii::$app->params['kubAssetBasePath'] = $bandle->basePath;
                Yii::$app->view->theme = Yii::createObject([
                    'class' => 'yii\base\Theme',
                    'basePath' => '@frontend/themes/mobile',
                    'baseUrl' => '@web/themes/mobile',
                    'pathMap' => [
                        '@common/models/file/widgets/views' => '@frontend/themes/mobile/file',
                        '@frontend/views' => '@frontend/themes/mobile/views',
                        '@frontend/modules' => '@frontend/themes/mobile/modules',
                        '@frontend/widgets' => '@frontend/themes/mobile/widgets',
                    ],
                ]);
            } elseif (!Yii::$app->user->getIsOldKubUser() && !Yii::$app->request->get('old')) {
                Yii::$app->params['bsVersion'] = '4';
                $bandle = Yii::$app->assetManager->getBundle('frontend\themes\kub\assets\KubAsset');
                Yii::$app->params['kubAssetBaseUrl'] = $bandle->baseUrl;
                Yii::$app->params['kubAssetBasePath'] = $bandle->basePath;
                Yii::$app->view->theme = Yii::createObject([
                    'class' => 'yii\base\Theme',
                    'basePath' => '@frontend/themes/kub',
                    'baseUrl' => '@web/themes/kub',
                    'pathMap' => [
                        '@common/models/file/widgets/views' => '@frontend/themes/kub/file',
                        '@frontend/views' => '@frontend/themes/kub/views',
                        '@frontend/modules' => '@frontend/themes/kub/modules',
                        '@frontend/widgets' => '@frontend/themes/kub/widgets',
                    ],
                ]);
            }
        }
    }

    public function isMobaleThemeRoute()
    {
        $request = Yii::$app->getRequest()->resolve();
        if ($p = ArrayHelper::getValue($request, [1, 'p'])) {
            $route = ltrim(ArrayHelper::getValue(Banking::routeDecode($p), 0, ''), '/');
        } else {
            $route = ArrayHelper::getValue($request, 0);
        }

        return (strpos($route, 'tax/robot') === 0);
    }


    /**
     * @return bool
     */
    private function isPriceListOutView()
    {
        return strpos(Yii::$app->request->url, '/price-list/out-view') !== false
            || strpos(Yii::$app->request->url, '/out/order-document') !== false
            || strpos(Yii::$app->request->url, '/bill/order-document') !== false;
    }

    /**
     * @return bool
     */
    private function isOrderDocumentOutView()
    {
        return strpos(Yii::$app->request->url, '/documents/order-document/out-view') !== false;
    }

    /**
     * @return bool
     */
    private function isNewKubUrl()
    {
        return strpos(Yii::$app->request->url, '/crm/') === 0 ||
                strpos(Yii::$app->request->url, '/analytics/') === 0 ||
                strpos(Yii::$app->request->url, '/system/') === 0 ||
                strpos(Yii::$app->request->url, '/retail/') === 0 ||
                strpos(Yii::$app->request->url, '/ofd/') === 0 ||
                strpos(Yii::$app->request->url, '/project/') === 0 ||
                strpos(Yii::$app->request->url, '/site/reset-password') === 0 ||
                strpos(Yii::$app->request->url, '/site/request-password-reset') === 0 ||
                strpos(Yii::$app->request->url, '/cash/default/operations') === 0 ||
                $this->isPriceListOutView() ||
                $this->isOrderDocumentOutView();
    }
}
