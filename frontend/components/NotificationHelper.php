<?php
namespace frontend\components;


use common\models\notification\Notification;
use common\models\Company;

class NotificationHelper extends Notification
{
    public function getNotifications(Company $company)
    {
        $query = static::find();

        $nextDay = strtotime("-1 day");

        $query->byActivationDate()
            ->byEventDate($nextDay)
            ->byNotificationType($this->notification_type)
            ->byWhom($company)
            ->orderBy('event_date ASC');

        return $query->all();
    }
}