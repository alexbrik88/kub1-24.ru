<?php

namespace frontend\components;

use Yii;
use yii\helpers\ArrayHelper;

class PageSize
{
    public static $defaultSize = 10;
    public static $minSize = 10;
    public static $maxSize = 100;
    public static $items = [
        10 => '10',
        20 => '20',
        50 => '50',
        100 => '100',
    ];

    protected static $_size = [];

    public static function get($pageSizeParam = null, $defaultSize = null, $page = null)
    {
        if (empty($pageSizeParam) || !is_string($pageSizeParam)) {
            $pageSizeParam = 'per-page';
        }
        if (empty($page) || !is_string($page)) {
            if ($page === true) {
                $page = ArrayHelper::getValue(Yii::$app, ['controller',  'action', 'uniqueId'], 'default');
            } else {
                $page = 'default';
            }
        }

        if (!isset(self::$_size[$page][$pageSizeParam])) {
            if ($defaultSize === null) {
                $defaultSize = self::$defaultSize;
            }
            $user = ArrayHelper::getValue(Yii::$app, ['user',  'identity']);
            $userPageSize = $user !== null ? $user->getPageSize($page, $pageSizeParam) : null;
            $setPageSize = false;
            if (($pageSize = Yii::$app->request->getQueryParam($pageSizeParam)) === null) {
                if (($pageSize = $userPageSize) === null) {
                    $pageSize = $defaultSize;
                }
            } else {
                $setPageSize = true;
            }
            $pageSize = min(self::$maxSize, max(self::$minSize, intval($pageSize)));
            if ($setPageSize && $user !== null && $pageSize != $userPageSize) {
                $user->setPageSize($page, $pageSizeParam, $pageSize);
            }

            self::$_size[$page][$pageSizeParam] = $pageSize;
        }

        return self::$_size[$page][$pageSizeParam];
    }
}
