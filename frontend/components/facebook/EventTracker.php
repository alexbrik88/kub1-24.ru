<?php

namespace frontend\components\facebook;

use Yii;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\ActionSource;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\DeliveryCategory;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use yii\helpers\ArrayHelper;

/**
 * EventTracker
 */
class EventTracker
{
    /**
     * example:
     * \frontend\components\facebook\EventTracker::event('connectbank');
     *
     * @param  string $eventName
     * @param  array  $data
     */
    public static function event($eventName, $data = [])
    {
        $access_token = Yii::$app->params['facebook']['api']['access_token'] ?? null;
        $pixel_id = Yii::$app->params['facebook']['pixel_id'] ?? null;
        $log = Yii::$app->params['facebook']['tracker_log'] ?? false;

        if ($access_token && $pixel_id) {
            $cookies = Yii::$app->request->cookies;

            $api = Api::init(null, null, $access_token);
            $api->setLogger(new CurlLogger());

            $user_data = (new UserData())
                ->setEmail(ArrayHelper::getValue(Yii::$app->user, ['identity', 'email']))
                ->setClientIpAddress($_SERVER['REMOTE_ADDR'] ?? null)
                ->setClientUserAgent($_SERVER['HTTP_USER_AGENT'] ?? null)
                ->setFbc($cookies->getValue('_fbc'))
                ->setFbp($cookies->getValue('_fbp'));

            $event = (new Event())
                ->setEventName($eventName)
                ->setEventTime(time())
                ->setEventSourceUrl(Yii::$app->request->referrer)
                ->setUserData($user_data)
                ->setActionSource(ActionSource::WEBSITE);

            $events = array();
            array_push($events, $event);

            $request = (new EventRequest($pixel_id))->setEvents($events);
            try {
                $response = $request->execute();

                if ($log) {
                    $logFile = Yii::getAlias('@runtime/logs/facebook_tracker.log');
                    file_put_contents($logFile, date('c').PHP_EOL.var_export($response, true).PHP_EOL.PHP_EOL, FILE_APPEND);
                }
            } catch (\Exception $e) {
                $logFile = Yii::getAlias('@runtime/logs/facebook_tracker.log');
                file_put_contents($logFile, date('c').PHP_EOL.var_export($e, true).PHP_EOL.PHP_EOL, FILE_APPEND);
            }
        }
    }
}
