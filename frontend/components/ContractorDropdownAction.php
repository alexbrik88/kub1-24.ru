<?php

namespace frontend\components;

use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\Config;
use frontend\modules\crm\models\Task;
use frontend\rbac\permissions\crm\ClientAccess;
use Yii;
use common\models\Contractor;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ContractorDropdownAction
 */
class ContractorDropdownAction extends \yii\base\Action
{
    /**
     * Runs the action.
     * This method return Contractor dropdown array.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($type, $term = null, $q = null, $page = null, $_type = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!Yii::$app->user->can(\frontend\rbac\permissions\Contractor::INDEX, ['type' => $type])) {
            return [
                'results' => [],
                'pagination' => [
                    'more' => false,
                ],
            ];
        }

        $results1 = [];
        $results2 = [];
        $limit = 100;
        $search = $term ?? $q;
        $page = max(1, (int) $page);
        $offset = $limit * ($page-1);
        $paginationMore = false;

        if ($cid = \yii\helpers\ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id'])) {
            $excludeIds = Yii::$app->getRequest()->get('excludeIds');
            if ($page == 1) {
                $staticData = Yii::$app->getRequest()->get('staticData');
                $excludeCashbox = Yii::$app->getRequest()->get('excludeCashbox');
                if (is_array($staticData)) {
                    foreach ($staticData as $key => $value) {

                        if ($excludeCashbox && "order.{$excludeCashbox}" == $key)
                            continue;

                        if (is_string($value)) {
                            $value = [
                                'id' => $key,
                                'text' => $value,
                            ];
                        }
                        if (isset($value['id'], $value['text'])) {
                            if (!$search || stripos($value['text'], $search) !== false) {
                                $results1[] = [
                                    'id' => $value['id'],
                                    'text' => $value['text'],
                                ];
                            }
                        }
                    }
                }
            }

            $query = Contractor::find()->alias('contractor')->joinWith('companyType company_type', false)->select([
                'id' => "MIN({{contractor}}.[[id]])",
                'contractor.name',
                'type' => "company_type.name_short",
                'contractor.ITN',
                'contractor.PPC',
                'contractor.verified',
                'contractor.face_type',
                'contractor.foreign_legal_form',
                'industry_id' => $type == Contractor::TYPE_CUSTOMER ? 'contractor.customer_industry_id' : 'contractor.seller_industry_id',
                'sale_point_id' => $type == Contractor::TYPE_CUSTOMER ? 'contractor.customer_sale_point_id' : 'contractor.seller_sale_point_id',
            ])->andWhere([
                'or',
                ['contractor.company_id' => null],
                ['contractor.company_id' => $cid],
            ])->byType(empty($_type) ? $type : [$type, $_type])->andWhere([
                'contractor.is_deleted' => false,
                'contractor.status' => Contractor::ACTIVE,
            ])->andFilterWhere([
                'like',
                'contractor.name',
                $search,
            ])->andFilterWhere([
                'contractor.face_type' => Yii::$app->getRequest()->get('faceType'),
            ])->andFilterWhere([
                'not',
                ['contractor.id' => empty($excludeIds) ? null : explode(',', $excludeIds)],
            ])->groupBy([
                'contractor.id',
                //'contractor.ITN',
                //'contractor.PPC',
                //'contractor.name',
            ])->orderBy([
                new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
                'type' => SORT_ASC,
                'name' => SORT_ASC,
            ])->offset($offset)->limit($limit + 1);

            $filter = null;
            if ($_type == Contractor::TYPE_POTENTIAL_CLIENT) { // Ещё костыли под ЦРМ подъехали! Ура, товарищи!
                $filter = $this->getByCrmClientFilter($query);
            }

            if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
                if ($filter) {
                    $filter = [
                        'or',
                        $filter,
                        $this->getByInvoiceFilter($query)
                    ];
                } else {
                    $filter = $this->getByInvoiceFilter($query);
                }
            }

            if ($filter) {
                $query->andWhere($filter);
            }

            $results2 = $query->asArray()->all();

            if (count($results2) == $limit + 1) {
                array_pop($results2);
                $paginationMore = true;
            }

            array_walk($results2, function (&$item) {
                $type = $item['face_type'] == Contractor::TYPE_FOREIGN_LEGAL_PERSON ? $item['foreign_legal_form'] : $item['type'];
                $item['title'] = ($type ? $type.' ' : '') . $item['name'];
                $item['name'] = Html::encode($item['name']);
                $item['text'] = ($type ? $type.' ' : '') . $item['name'];
            });
        }

        // contractors doubles
        if (count($results2) > 1) {
            for ($n = 1; $n < count($results2); $n++) {
                if ($results2[$n - 1]['ITN'] == $results2[$n]['ITN']) {
                    $results2[$n]['showPPC'] = 'КПП ' . $results2[$n]['PPC'];
                    if ($results2[$n - 1]['PPC'] != $results2[$n]['PPC']) {
                        $results2[$n - 1]['showPPC'] = 'КПП ' . $results2[$n - 1]['PPC'];;
                    } else {
                        $results2[$n - 1]['showPPC'] = 'ДУБЛЬ';
                    }
                }
            }
        }

        $results = array_merge($results1, $results2);

        return [
            'results' => $results,
            'pagination' => [
                'more' => $paginationMore,
            ],
        ];
    }

    /**
     * @param ActiveQuery $query
     * @return void
     */
    private function getByCrmClientFilter(ActiveQuery $query): array
    {
        $user = Yii::$app->user;
        $config = Config::getInstance($user->identity->company) ?: Config::createInstance($user->identity->company);
        $filter = null;

        if (!$config->show_customers) {
            $query->joinWith('crmClient client', false);
            $filter = ['not', ['client.contractor_id' => null]];
        }

        if (!$user->can(ClientAccess::VIEW_ALL)) {
            $query->joinWith('crmTasks task', false);
            $f = [
                'or',
                ['contractor.responsible_employee_id' => $user->identity->id],
                ['task.employee_id' => $user->identity->id],
            ];
            if ($filter === null) {
                $filter = $f;
            } else {
                $filter = [
                    'and',
                    $filter,
                    $f,
                ];
            }
        }

        return $filter;
    }

    /**
     * @param ActiveQuery $query
     * @return void
     */
    private function getByInvoiceFilter(ActiveQuery $query): array
    {
        $query->joinWith(['invoices'], false);

        return [
            'and',
            ['invoice.is_deleted' => false],
            [
                'or',
                ["invoice.document_author_id" => Yii::$app->user->id],
                ["contractor.responsible_employee_id" => Yii::$app->user->id],
            ]
        ];
    }
}
