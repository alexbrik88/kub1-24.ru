<?php

namespace frontend\components;

use common\models\service\SubscribeTariffGroup;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Slass Icon
 *
 */
class Icon extends \yii\base\Component
{
    public static $tariffIconByGroup = [
        SubscribeTariffGroup::STANDART => 'template',
        SubscribeTariffGroup::LOGISTICS => 'delivery',
        SubscribeTariffGroup::B2B_PAYMENT => 'bank',
        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6 => 'percentage',
        SubscribeTariffGroup::ANALYTICS => 'chart',
        SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING => 'description',
        SubscribeTariffGroup::TAX_IP_USN_6 => 'mix',
        8 => 'find',
        9 => 'recognize',
    ];

    public static $_filemtime = false;

    /**
     * Gets svg-sprite file modification time
     * @param  integet $default
     * @return integet
     */
    public static function filemtime($default = null)
    {
        if (self::$_filemtime === false) {
            self::$_filemtime = filemtime(Yii::getAlias('@webroot/img/svg/svgSprite.svg')) ? : null;
        }

        return self::$_filemtime;
    }

    /**
     * Rendering svg-sprite icon
     * @param  string $id      icon id
     * @param  array  $options html options
     * @return string
     */
    public static function get($id, $options = [])
    {
        Html::addCssClass($options, 'svg-icon');

        return Html::tag('svg', Html::tag('use', '', [
            'xlink:href' => Url::to(['/img/svg/svgSprite.svg', 'v' => self::filemtime(), '#' => $id])
        ]), $options);
    }

    /**
     * Rendering themes svg-sprite icon
     * @param  string $id      SubscribeTariffGroup::id
     * @param  string $default
     * @return string
     */
    public static function forTariff($groupId, $default = null)
    {
        return ArrayHelper::getValue(self::$tariffIconByGroup, $groupId, $default);
    }
}
