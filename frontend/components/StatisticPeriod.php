<?php
namespace frontend\components;

use common\components\date\DateHelper;
use common\models\employee\Employee;
use DateTime;
use DateTimeInterface;
use Yii;
use yii\helpers\ArrayHelper;

class StatisticPeriod
{
    const TODAY = 'Сегодня';
    const YESTERDAY = 'Вчера';
    const LAST_7_DAYS = 'Последние 7 дней';
    const LAST_30_DAYS = 'Последние 30 дней';
    const THIS_MONTH = 'Этот месяц';
    const THIS_QUARTER = 'Этот квартал';
    const THIS_YEAR = 'Этот год';
    const PREVIOUS_MONTH = 'Предыдущий месяц';
    const PREVIOUS_QUARTER = 'Предыдущий квартал';
    const PREVIOUS_YEAR = 'Предыдущий год';

    const CUSTOM_RANGE = 'Указать диапазон';

    const DEFAULT_PERIOD = self::THIS_YEAR;

    protected static $_period;

    public static $periodJs = [
        self::TODAY => [
            'from' => "moment()",
            'to' => "moment()",
        ],
        self::YESTERDAY => [
            'from' => "moment().subtract(1, 'days')",
            'to' => "moment().subtract(1, 'days')",
        ],
        self::LAST_7_DAYS => [
            'from' => "moment().subtract(6, 'days')",
            'to' => "moment()",
        ],
        self::LAST_30_DAYS => [
            'from' => "moment().subtract(29, 'days')",
            'to' => "moment()",
        ],
        self::THIS_MONTH => [
            'from' => "moment().startOf('month')",
            'to' => "moment().endOf('month')",
        ],
        self::THIS_QUARTER => [
            'from' => "moment().startOf('quarter')",
            'to' => "moment().endOf('quarter')",
        ],
        self::THIS_YEAR => [
            'from' => "moment().startOf('year')",
            'to' => "moment().endOf('year')",
        ],
        self::PREVIOUS_MONTH => [
            'from' => "moment().subtract(1, 'month').startOf('month')",
            'to' => "moment().subtract(1, 'month').endOf('month')",
        ],
        self::PREVIOUS_QUARTER => [
            'from' => "moment().subtract(1, 'quarter').startOf('quarter')",
            'to' => "moment().subtract(1, 'quarter').endOf('quarter')",
        ],
        self::PREVIOUS_YEAR => [
            'from' => "moment().subtract(1, 'year').startOf('year')",
            'to' => "moment().subtract(1, 'year').endOf('year')",
        ],
    ];

    /**
     * @return mixed
     */
    public static function setDefaultStatisticRange()
    {
        Yii::$app->user->identity->updateAttributes([
            'statistic_range_date_from' => self::getDefaultPeriod()['from'],
            'statistic_range_date_to' => self::getDefaultPeriod()['to'],
            'statistic_range_name' => self::DEFAULT_PERIOD,
        ]);
    }

    /**
     * @return mixed
     */
    public static function getSessionPeriod()
    {
        if (empty(self::$_period)) {
            if (!Yii::$app->request->isConsoleRequest && Yii::$app->user->identity) {
                if (!Yii::$app->user->identity->isSetStatisticRange) {
                    self::setDefaultStatisticRange();
                }
                self::$_period = [
                    'from' => Yii::$app->user->identity->getStatisticRangeDates('from'),
                    'to' => Yii::$app->user->identity->getStatisticRangeDates('to'),
                ];
            } else {
                self::$_period = self::getDefaultPeriod();
            }
        }

        return self::$_period;
    }

    /**
     * @return mixed
     */
    public static function dateRange($point)
    {
        return date_create_from_format('Y-m-d', self::getSessionPeriod()[$point]);
    }

    /**
     * @return DateTime
     */
    public static function defaultPeriodFrom()
    {
        return (new \DateTime())->modify('first day of January this year');
    }

    /**
     * @return DateTime
     */
    public static function defaultPeriodTo()
    {
        return (new \DateTime())->modify('last day of December this year');
    }

    /**
     * @return array
     */
    public static function getDefaultPeriod()
    {
        $date['from'] = static::defaultPeriodFrom()->format(DateHelper::FORMAT_DATE);
        $date['to'] = static::defaultPeriodTo()->format(DateHelper::FORMAT_DATE);

        return $date;
    }

    /**
     * @return string
     */
    public static function getSessionName()
    {

        if (Yii::$app->user->identity) {
            if (!Yii::$app->user->identity->isSetStatisticRange) {
                self::setDefaultStatisticRange();
            }

            return Yii::$app->user->identity->getStatisticRangeName();
        }

        return self::DEFAULT_PERIOD;
    }

    protected static function _mb_ucfirst($string, $encoding = 'utf-8')
    {
        $firstChar = mb_substr($string, 0, 1, $encoding);
        $then = mb_substr($string, 1, null, $encoding);
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    public static function getSessionNameMonths()
    {
        $periodSessionName = self::getSessionName();
        if (count($_period = explode(' - ', $periodSessionName)) == 2) {
            $from = date_create_from_format('d.m.Y', trim($_period[0]));
            $to = date_create_from_format('d.m.Y', trim($_period[1]));
            if ($from && $to) {
                $periodSessionName =
                    self::_mb_ucfirst(Yii::$app->formatter->asDate($from->format('Y-m-d'), 'LLLL Y'))
                    . ' - ' .
                self::_mb_ucfirst(Yii::$app->formatter->asDate($to->format('Y-m-d'), 'LLLL Y'));
            }
        }
        return $periodSessionName;
    }

    /**
     * @return string
     */
    public static function getRangesJs()
    {
        $range = '';

        foreach (self::$periodJs as $period => $value) {
            $range .= '\'' . $period . '\': [' . $value['from'] . ', ' . $value['to'] . '], ';
        }

        return $range;
    }

    public static function getStartEndDateJs($type)
    {

        return DateHelper::format(self::getSessionPeriod()[$type], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }

    /**
     * @return DateTimeInterface
     */
    public static function getDateFrom(): DateTimeInterface
    {
        $period = self::getSessionPeriod();
        $date = DateTime::createFromFormat(DateHelper::FORMAT_DATE, $period['from']);

        return $date->setTime(0, 0, 0, 0);
    }

    /**
     * @return DateTimeInterface
     */
    public static function getDateTo(): DateTimeInterface
    {
        $period = self::getSessionPeriod();
        $date = DateTime::createFromFormat(DateHelper::FORMAT_DATE, $period['to']);

        return $date->setTime(23, 59, 59, 999999);
    }
}
