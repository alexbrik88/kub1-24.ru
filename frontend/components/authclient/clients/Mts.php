<?php

namespace frontend\components\authclient\clients;

use Yii;
use yii\authclient\OAuth2;
use yii\helpers\ArrayHelper;

/**
 * Mts allows authentication via Mts OAuth.
 */
class Mts extends OAuth2
{
    /**
     * {@inheritdoc}
     */
    public $authUrl = 'https://login.mts.ru/amserver/oauth2/auth';
    /**
     * {@inheritdoc}
     */
    public $tokenUrl = 'https://login.mts.ru/amserver/oauth2/token';
    /**
     * {@inheritdoc}
     */
    public $apiBaseUrl = 'https://login.mts.ru';
    /**
     * {@inheritdoc}
     */
    public $clientId = 'test@app.b2b.mts.ru';
    /**
     * {@inheritdoc}
     */
    public $clientSecret = 'test';

    /**
     * {@inheritdoc}
     */
    protected function defaultName()
    {
        return 'mts';
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultTitle()
    {
        return 'Mts';
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if ($this->scope === null) {
            $this->scope = implode(' ', [
                'mobile:phone',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function initUserAttributes()
    {
        $data = $this->api('amserver/oauth2/api', 'GET');

        $attributes = [
            'uid' => ArrayHelper::getValue($data, 'mobile:phone'),
            'employeeAttributes' => [
                'is_mts_user' => 1,
                'phone' => '+7'.ArrayHelper::getValue($data, 'mobile:phone'),
            ],
        ];
        if ($regPageId = Yii::$app->session->get('__registration_page_type_id')) {
            $attributes['companyAttributes']['registration_page_type_id'] = $regPageId;
        }

        return $attributes;
    }

    /**
     * {@inheritdoc}
     */
    public function applyAccessTokenToRequest($request, $accessToken)
    {
        $request->getHeaders()->set('Authorization', 'Bearer '. $accessToken->getToken());
    }
}
