<?php

namespace frontend\components;

use common\models\address\Country;
use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\LogImportProduct;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductStore;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class ImportProductHelper
 *
 * @package frontend\components
 */
class ImportProductHelper
{
    const TYPE_INSERT = 1;
    const TYPE_UPDATE = 2;

    const MAX_ROWS = 10000;

    /**
     * @var array
     */
    public static $attributes = [
        Product::PRODUCTION_TYPE_SERVICE => [
            'title',
            'group_id',
            'price_for_buy_with_nds',
            'price_for_buy_nds_id',
            'price_for_sell_with_nds',
            'price_for_sell_nds_id',
            'product_unit_id',
        ],
        Product::PRODUCTION_TYPE_GOODS => [
            'title',
            'group_id',
            'price_for_buy_with_nds',
            'price_for_buy_nds_id',
            'price_for_sell_with_nds',
            'price_for_sell_nds_id',
            'product_unit_id',
            'initQuantity',
            'code',
            'box_type',
            'count_in_place',
            'place_count',
            'mass_gross',
            'country_origin_id',
            'customs_declaration_number',
            'article',
            'provider_id',
            'weight',
            'volume',
            'irreducibleQuantity',
            'comment',
            'photo',
            'comment_photo'
        ],
    ];

    protected static $existsAttributes = [
        'title',
        'article',
    ];

    protected static $notDbAttributes = [
        'initQuantity',
        'irreducibleQuantity',
        'photo',
    ];

    protected static $errorList = [
        'title' => 'Название должно быть заполнено',
        'group_id' => null,
        'price_for_buy_with_nds' => 'Значение "Цена покупки" должно быть числом',
        'price_for_buy_nds_id' => null,
        'price_for_sell_with_nds' => 'Значение "Цена продажи" должно быть числом',
        'price_for_sell_nds_id' => null,
        'product_unit_id' => null,
        'initQuantity' => 'Значение "Количество на складе начальное" должно быть числом',
        'code' => null,
        'box_type' => null,
        'count_in_place' => 'Значение "Количество в одном месте" должно быть числом',
        'place_count' => 'Значение "Количество мест, штук" должно быть числом',
        'mass_gross' => 'Значение "Масса брутто" должно быть числом',
        'country_origin_id' => null,
        'customs_declaration_number' => null,
        'article' => null,
        'provider_id' => null,
        'weight' => 'Значение "Вес" должно быть числом',
        'volume' => 'Значение "Объем" должно быть числом',
        'irreducibleQuantity' => 'Значение "Неснижаемый остаток" должно быть числом',
        'comment' => null,
        'photo' => null,
        'comment_photo' => null,
    ];

    protected static $startTime;
    protected static $logModel;
    protected static $logModelId;
    protected static $company;
    protected static $employeeId;
    protected static $companyId;
    protected static $productionType;
    protected static $isGoods;
    protected static $insertCount;
    protected static $updateCount;
    protected static $errorKeys;
    protected static $data = [];
    protected static $errors = [];

    /**
     * @param  $company Company
     * @param  $employee Employee
     * @param  $sourceFile string
     * @param  $productionType integer
     * @return array
     */
    public static function import(Company $company, Employee $employee, $sourceFile, $productionType)
    {
        self::$startTime = time();
        self::$errorKeys = [];
        self::$errors = [];
        self::$insertCount = 0;
        self::$updateCount = 0;

        $attributes = ArrayHelper::getValue(self::$attributes, $productionType);

        if (!$attributes) {
            return false;
        }

        self::$company = $company;
        self::$companyId = $company->id;
        self::$employeeId = $employee->id;
        self::$productionType = $productionType;
        self::$isGoods = $productionType == Product::PRODUCTION_TYPE_GOODS;

        self::$logModel = new LogImportProduct([
            'employee_id' => self::$employeeId,
            'company_id' => self::$companyId,
            'production_type' => self::$productionType,
            'created_at' => self::$startTime,
        ]);

        if (!self::$logModel->save()) {
            return false;
        }

        self::$logModelId = self::$logModel->id;

        $tmpDir = Yii::getAlias('@runtime/import');
        FileHelper::createDirectory($tmpDir);
        $file = tempnam($tmpDir, '_product_');
        exec("xlsx2csv {$sourceFile} {$file}");

        // for tests on Windows
        if (!empty(Yii::$app->params['is_win7'])) {
            self::xlsx2csv($sourceFile, $file);
        }

        if (is_file($file)) {
            $errors = [];
            if (($h = fopen($file, "r")) !== false) {
                $attrCount = count($attributes);
                $skip = true;
                $importKey = 0;
                $rowCount = 0;
                $importData = [];
                while (($rowData = fgetcsv($h, 10000, ",")) !== false) {
                    $importKey++;
                    if ($skip) {
                        $skip = false;
                        continue;
                    }
                    $dataCount = count($rowData);
                    if ($dataCount > $attrCount) {
                        $rowData = array_slice($rowData, 0, $attrCount);
                    } elseif ($dataCount < $attrCount) {
                        $rowData = array_merge($rowData, array_fill($dataCount, $attrCount - $dataCount, ''));
                    }
                    if (empty(trim($rowData[0]))) {
                        self::$errors[$importKey] = self::$errorList['title'];
                        self::$errorKeys[] = $importKey;
                        break;
                    }

                    list($row, $count, $title, $article, $photo) = self::prepareValues($rowData, $attributes);

                    if (!self::validate($row, $importKey)) {
                        self::$errorKeys[] = $importKey;
                        break;
                    }

                    $rowCount++;

                    $importData['keys'][] = $importKey;
                    $importData['import'][$importKey] = $row;
                    $importData['titles'][$importKey] = $title;

                    if (self::$isGoods) {
                        $importData['quantity'][$importKey] = $count;
                        if ($article) {
                            $importData['articles'][$importKey] = $article;
                        }
                        if ($photo) {
                            $importData['photos'][$importKey] = $photo;
                        }
                    }

                    if ($rowCount == self::MAX_ROWS) {
                        self::importData($importData);
                        $rowCount = 0;
                        $importData = [];
                    }
                }

                self::importData($importData);

                fclose($h);
            }

            unlink($file);

            if (self::$insertCount > 0) {
                Product::updateAll(['status' => Product::ACTIVE], [
                    'log_import_product_id' => self::$logModelId,
                    'import_type_id' => self::TYPE_INSERT,
                ]);
            }

            self::$logModel->updateAttributes([
                'completed_at' => time(),
                'inserted_rows' => self::$insertCount,
                'updated_rows' => self::$updateCount,
            ]);

            return [
                'error' => !empty(self::$errorKeys),
                'success' => '',
                'successCount' => self::$insertCount,
                'updateCount' => self::$updateCount,
                'errorCount' => count(self::$errorKeys),
                'invoiceData' => null,
                'errorProductsName' => implode(', ', self::$errorKeys),
                'errorArray' => self::$errors,
            ];
        } else {
            return false;
        }
    }

    /**
     * @return
     */
    protected static function validate($data, $importKey)
    {
        $ruls = [
            'title' => function ($value) {
                return !empty($value);
            },
            'group_id' => function ($value) {
                return $value === null || is_integer($value);
            },
            'price_for_buy_with_nds' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'price_for_buy_nds_id' => function ($value) {
                return $value === null || is_integer($value);
            },
            'price_for_sell_with_nds' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'price_for_sell_nds_id' => function ($value) {
                return $value === null || is_integer($value);
            },
            'product_unit_id' => function ($value) {
                return is_integer($value);
            },
            'initQuantity' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'code' => function ($value) {
                return true;
            },
            'box_type' => function ($value) {
                return true;
            },
            'count_in_place' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'place_count' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'mass_gross' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'country_origin_id' => function ($value) {
                return is_integer($value);
            },
            'customs_declaration_number' => function ($value) {
                return true;
            },
            'article' => function ($value) {
                return true;
            },
            'provider_id' => function ($value) {
                return $value === null || is_integer($value);
            },
            'weight' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'volume' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'irreducibleQuantity' => function ($value) {
                return empty($value) || is_numeric($value);
            },
            'comment' => function ($value) {
                return true;
            },
            'photo' => function ($value) {
                return true;
            },
            'comment_photo' => function ($value) {
                return true;
            },
        ];

        foreach ($data as $key => $value) {
            if (!call_user_func($ruls[$key], $value)) {
                self::$errors[$importKey] = self::$errorList[$key];
                return false;
            }
        }

        return true;
    }

    /**
     * @return
     */
    protected static function prepareValues($data, $attributes)
    {
        $row = [];
        $count = [];
        $title = null;
        $article = null;
        $photo = null;

        foreach ($attributes as $key => $attrName) {
            $value = trim($data[$key]);
            switch ($attrName) {
                case 'title':
                    $row[$attrName] = $title = $value;
                    break;

                case 'article':
                    $row[$attrName] = $article = $value;
                    break;

                case 'price_for_buy_nds_id':
                case 'price_for_sell_nds_id':
                    $row[$attrName] = self::taxRateId($value);
                    break;

                case 'product_unit_id':
                    $row[$attrName] = self::unitId($value);
                    break;

                case 'country_origin_id':
                    $row[$attrName] = self::countryId($value);
                    break;

                case 'group_id':
                    $row[$attrName] = self::groupId($value);
                    break;

                case 'provider_id':
                    $row[$attrName] = self::contractorId($value);
                    break;

                case 'mass_gross':
                case 'weight':
                case 'volume':
                    $row[$attrName] = strtr($value, ',', '.');
                    break;

                case 'price_for_buy_with_nds':
                case 'price_for_sell_with_nds':
                    $row[$attrName] = strtr($value, ',', '.');
                    break;

                case 'initQuantity':
                case 'irreducibleQuantity':
                    $count[$attrName] = strtr($value, ',', '.');
                    break;

                case 'photo':
                    $photo = $value;
                    break;

                default:
                    $row[$attrName] = $value;
                    break;
            }
        }

        return [
            $row,
            $count,
            $title,
            $article,
            $photo,
        ];
    }

    /**
     * @return
     */
    protected static function importData($importData)
    {
        $keys = ArrayHelper::getValue($importData, 'keys');
        $import = ArrayHelper::getValue($importData, 'import');
        $titles = ArrayHelper::getValue($importData, 'titles');
        $quantity = ArrayHelper::getValue($importData, 'quantity');
        $articles = ArrayHelper::getValue($importData, 'articles');
        $photos = ArrayHelper::getValue($importData, 'photos');

        if (!is_array($import)) {
            return;
        }

        $idsByTitle = self::idsByTitle($titles);
        $idsByArticle = self::idsByArticle($articles);

        $insertData = [];
        $updateData = [];

        foreach ($import as $key => $value) {
            $productId = null;
            if (isset($idsByTitle[$value['title']])) {
                $productId = $idsByTitle[$value['title']];
            } elseif (self::$isGoods && $value['article'] && isset($idsByArticle[$value['article']])) {
                $productId = $idsByArticle[$value['article']];
            }
            /*if ($productId === null) {
                $insertData[] = self::prepareInsertData($key, $value);
            } else {
                $updateData[] = self::prepareUpdateData($key, $value, $productId);
            }*/
            $insertData[] = self::prepareInsertData($key, $value, $productId);
        }

        self::insertData($insertData);
        //self::updateData($updateData);
        self::updateQuantity($keys, $quantity);
    }

    /**
     * @return
     */
    protected static function idsByTitle($array)
    {
        return empty($array) ? [] : Product::find()->select(['id', 'title'])->where([
            'company_id' => self::$companyId,
            'production_type' => self::$productionType,
            'is_deleted' => false,
            'status' => [
                Product::ACTIVE,
                Product::ARCHIVE,
            ],
            'title' => $array,
        ])->indexBy('title')->column();
    }

    /**
     * @return
     */
    protected static function idsByArticle($array)
    {
        return empty($array) ? [] : Product::find()->select(['id', 'article'])->where([
            'company_id' => self::$companyId,
            'production_type' => self::$productionType,
            'is_deleted' => false,
            'status' => [
                Product::ACTIVE,
                Product::ARCHIVE,
            ],
            'article' => $array,
        ])->indexBy('article')->column();
    }

    /**
     * @return
     */
    protected static function prepareInsertData($key, $data, $productId)
    {
        if (self::$isGoods) {
            return [
                empty($productId) ? null : $productId,
                self::$employeeId,
                self::$companyId,
                self::$productionType,
                Product::DELETED,
                self::$startTime,
                self::$logModelId,
                $key,
                self::TYPE_INSERT,
                $data['title'],
                $data['group_id'],
                round(floatval($data['price_for_buy_with_nds']) * 100),
                $data['price_for_buy_nds_id'],
                round(floatval($data['price_for_sell_with_nds']) * 100),
                $data['price_for_sell_nds_id'],
                $data['product_unit_id'],
                $data['code'],
                $data['box_type'],
                $data['count_in_place'],
                $data['place_count'],
                (float)$data['mass_gross'] * 1,
                $data['country_origin_id'],
                $data['customs_declaration_number'],
                $data['article'],
                $data['provider_id'],
                (float)$data['weight'] * 1,
                (float)$data['volume'] * 1,
                $data['comment'],
                $data['comment_photo'],
            ];
        } else {
            return [
                empty($productId) ? null : $productId,
                self::$employeeId,
                self::$companyId,
                self::$productionType,
                Product::DELETED,
                self::$startTime,
                self::$logModelId,
                $key,
                self::TYPE_INSERT,
                $data['title'],
                $data['group_id'],
                round(floatval($data['price_for_buy_with_nds']) * 100),
                $data['price_for_buy_nds_id'],
                round(floatval($data['price_for_sell_with_nds']) * 100),
                $data['price_for_sell_nds_id'],
                $data['product_unit_id'],
                Country::COUNTRY_WITHOUT,
            ];
        }
    }

    /**
     * @return
     */
    protected static function prepareUpdateData($key, $data, $productId)
    {
        return [
            $productId,
            self::$logModelId,
            $key,
            self::TYPE_UPDATE,
            $data['price_for_buy_with_nds'] === '' ? null : $data['price_for_buy_with_nds'] * 100,
            $data['price_for_sell_with_nds'] === '' ? null : $data['price_for_sell_with_nds'] * 100,
        ];
    }

    /**
     * @return
     */
    protected static function insertData($rows)
    {
        if (!empty($rows)) {
            if (self::$isGoods) {
                $fields = [
                    'id',
                    'creator_id',
                    'company_id',
                    'production_type',
                    'status',
                    'created_at',
                    'log_import_product_id',
                    'import_row_id',
                    'import_type_id',
                    'title',
                    'group_id',
                    'price_for_buy_with_nds',
                    'price_for_buy_nds_id',
                    'price_for_sell_with_nds',
                    'price_for_sell_nds_id',
                    'product_unit_id',
                    'code',
                    'box_type',
                    'count_in_place',
                    'place_count',
                    'mass_gross',
                    'country_origin_id',
                    'customs_declaration_number',
                    'article',
                    'provider_id',
                    'weight',
                    'volume',
                    'comment',
                    'comment_photo',
                ];
            } else {
                $fields = [
                    'id',
                    'creator_id',
                    'company_id',
                    'production_type',
                    'status',
                    'created_at',
                    'log_import_product_id',
                    'import_row_id',
                    'import_type_id',
                    'title',
                    'group_id',
                    'price_for_buy_with_nds',
                    'price_for_buy_nds_id',
                    'price_for_sell_with_nds',
                    'price_for_sell_nds_id',
                    'product_unit_id',
                    'country_origin_id',
                ];
            }

            $db = Yii::$app->db;
            $sql = $db->queryBuilder->batchInsert(Product::tableName(), $fields, $rows);
            $execute = $db->createCommand($sql . '
                ON DUPLICATE KEY UPDATE
                    [[log_import_product_id]] = VALUES([[log_import_product_id]]),
                    [[import_row_id]] = VALUES([[import_row_id]]),
                    [[import_type_id]] = VALUES([[import_type_id]]),
                    [[price_for_buy_with_nds]] = COALESCE(VALUES([[price_for_buy_with_nds]]), [[price_for_buy_with_nds]]),
                    [[price_for_sell_with_nds]] = COALESCE(VALUES([[price_for_sell_with_nds]]), [[price_for_sell_with_nds]])
            ')->execute();

            $countRows = count($rows);
            $updated = $execute - $countRows;

            self::$insertCount += $countRows - $updated;
            self::$updateCount += $updated;
        }
    }

    /**
     * @return
     */
    protected static function updateData($rows)
    {
        if (!empty($rows)) {
            $count = count($rows);
            $db = Yii::$app->db;

            $fields = [
                'id',
                'log_import_product_id',
                'import_row_id',
                'import_type_id',
                'price_for_buy_with_nds',
                'price_for_sell_with_nds',
            ];

            $sql = $db->queryBuilder->batchInsert(Product::tableName(), $fields, $rows);
            $execute = $db->createCommand($sql . '
                ON DUPLICATE KEY UPDATE
                    [[log_import_product_id]] = VALUES([[log_import_product_id]]),
                    [[import_row_id]] = VALUES([[import_row_id]]),
                    [[import_type_id]] = VALUES([[import_type_id]]),
                    [[price_for_buy_with_nds]] = COALESCE(VALUES([[price_for_buy_with_nds]]), [[price_for_buy_with_nds]]),
                    [[price_for_sell_with_nds]] = COALESCE(VALUES([[price_for_sell_with_nds]]), [[price_for_sell_with_nds]])
            ')->execute();

            if ($execute) {
                self::$updateCount += count($rows);
            }
        }
    }

    /**
     * @return int|null
     */
    protected static function updateQuantity($keys, $quantity)
    {
        $storeIdQuery = self::$company->getStores()->select('id')->andWhere(['is_main' => true]);

        if (self::$isGoods && !empty($quantity) && ($storeId = $storeIdQuery->scalar())) {
            $store = [];
            $prodIds = [];

            $imported = Product::find()->select(['id', 'import_row_id'])->where([
                'company_id' => self::$companyId,
                'log_import_product_id' => self::$logModelId,
                'import_row_id' => $keys,
            ])->indexBy('import_row_id')->column();

            foreach ($quantity as $key => $value) {
                if (isset($imported[$key]) && (isset($value['initQuantity']) || isset($value['irreducibleQuantity']))) {
                    $store[] = [
                        $imported[$key],
                        $storeId,
                        (float)$value['initQuantity'] * 1,
                        is_numeric($value['initQuantity']) ? $value['initQuantity'] * 1 : 0,
                        self::$startTime,
                        self::$startTime,
                        is_numeric($value['irreducibleQuantity']) ? $value['irreducibleQuantity'] * 1 : 0,
                    ];
                    $prodIds[] = $imported[$key];
                }
            }

            if (!empty($store)) {
                $db = Yii::$app->db;

                $fields = [
                    'product_id',
                    'store_id',
                    'quantity',
                    'initial_quantity',
                    'created_at',
                    'updated_at',
                    'irreducible_quantity',
                ];

                $sql = $db->queryBuilder->batchInsert(ProductStore::tableName(), $fields, $store);
                $db->createCommand($sql . '
                    ON DUPLICATE KEY UPDATE
                        [[quantity]] = IF(
                            VALUES([[initial_quantity]]) IS NULL,
                            [[quantity]],
                            [[quantity]] + VALUES([[initial_quantity]]) - [[initial_quantity]]
                        ),
                        [[initial_quantity]] = COALESCE(VALUES([[initial_quantity]]), [[initial_quantity]]),
                        [[created_at]] = VALUES([[created_at]]),
                        [[updated_at]] = VALUES([[updated_at]]),
                        [[irreducible_quantity]] = COALESCE(VALUES([[irreducible_quantity]]), [[irreducible_quantity]])
                ')->execute();

                ProductStore::updateAll([
                    'quantity' => new Expression('(quantity - initial_quantity)'),
                    'initial_quantity' => 0,
                    'created_at' => self::$startTime,
                    'updated_at' => self::$startTime,
                    'irreducible_quantity' => 0,
                ], [
                    'and',
                    ['product_id' => $prodIds],
                    ['not', ['store_id' => $storeId]],
                ]);
            }
        }
    }

    /**
     * @return int|null
     */
    protected static function taxRateId($value)
    {
        if (!isset(self::$data['taxRateId'])) {
            $items = TaxRate::find()->all();
            self::$data['taxRateId'] = ArrayHelper::map($items, function ($model) {
                return trim($model->name, '%');
            }, 'id');
        }

        return isset(self::$data['taxRateId'][$value]) ? self::$data['taxRateId'][$value] : TaxRate::RATE_WITHOUT;
    }

    /**
     * @return int|null
     */
    protected static function unitId($value)
    {
        if (!isset(self::$data['unitId'])) {
            $items = ProductUnit::find()->all();
            self::$data['unitId'] = ArrayHelper::map($items, 'name', 'id');
        }

        return isset(self::$data['unitId'][$value]) ? self::$data['unitId'][$value] : ProductUnit::UNIT_COUNT;
    }

    /**
     * @return int|null
     */
    protected static function countryId($value)
    {
        if (!isset(self::$data['countryId'])) {
            $items = Country::find()->all();
            self::$data['countryId'] = ArrayHelper::map($items, 'name_short', 'id');
        }
        $value = mb_strtoupper($value);

        return isset(self::$data['countryId'][$value]) ? self::$data['countryId'][$value] : Country::COUNTRY_WITHOUT;
    }

    /**
     * @return int|null
     */
    protected static function contractorId($value)
    {
        if (empty($value)) {
            return null;
        }
        if (!isset(self::$data['contractorId'])) {
            self::$data['contractorId'] = [];
        }
        if (isset(self::$data['contractorId'][$value]) || array_key_exists($value, self::$data['contractorId'])) {
            return self::$data['contractorId'][$value];
        } else {
            $id = (int) Contractor::find()->select('id')
                ->byCompany(self::$companyId)
                ->byContractor(Contractor::TYPE_SELLER)
                ->byIsDeleted(Contractor::NOT_DELETED)
                ->byStatus(Contractor::ACTIVE)
                ->andWhere(['like', 'name', $value])
                ->scalar();

            self::$data['contractorId'][$value] = $id ? : null;
        }

        return self::$data['contractorId'][$value];
    }

    /**
     * @return int|null
     */
    protected static function groupId($value)
    {
        if (empty($value)) {
            return ProductGroup::WITHOUT;
        } else {
            if (!isset(self::$data['groupId'])) {
                $items = ProductGroup::find()->where([
                    'or',
                    ['company_id' => null],
                    ['company_id' => self::$companyId],
                ])->andWhere([
                    'or',
                    ['production_type' => null],
                    ['production_type' => self::$productionType],
                ])->all();
                self::$data['groupId'] = ArrayHelper::map($items, 'title', 'id');
            }

            $value = mb_substr($value, 0, 60);
            if (!isset(self::$data['groupId'][$value])) {
                $groupModel = new ProductGroup([
                    'title' => $value,
                    'company_id' => self::$companyId,
                    'production_type' => self::$productionType,
                ]);
                if ($groupModel->save(false)) {
                    self::$data['groupId'][$value] = $groupModel->id;
                } else {
                    return ProductGroup::WITHOUT;
                }
            }

            return self::$data['groupId'][$value];
        }
    }

    /**
     * @param $sourceFile
     * @param $file
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    protected static function xlsx2csv($sourceFile, $file)
    {
        $reader = \PHPExcel_IOFactory::createReader('Excel2007');
        $reader->setReadDataOnly(true);
        $excel = $reader->load($sourceFile);
        $writer = \PHPExcel_IOFactory::createWriter($excel, 'CSV');
        $writer->save($file);
    }
}
