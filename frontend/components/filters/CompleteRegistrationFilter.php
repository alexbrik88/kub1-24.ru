<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 31.8.15
 * Time: 17.01
 * Email: t.kanstantsin@gmail.com
 */

namespace frontend\components\filters;

use yii\helpers\Url;
use Yii;
use yii\base\ActionFilter;

/**
 * Class CompleteRegistrationFilter
 * @package frontend\components\filters
 */
class CompleteRegistrationFilter extends ActionFilter
{
    /**
     * @inheritdoc
     */
    public $actionId = [
        'create',
    ];

    public $controllerId = [
        'contractor',
        'order',
        //'payment-order', // mxfi 20-61
        'bank',
        'e-money',
    ];

    public function beforeAction($action)
    {
        if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->is_registration_completed
            && Yii::$app->user->identity->company && Yii::$app->user->identity->company->strict_mode
        ) {
            // mxfi 20-72 (for TaxRobot)
            if (in_array($action->controller->id, ['order', 'bank']) && strpos(Yii::$app->request->referrer, 'tax/robot/bank')) {
                return parent::beforeAction($action);
            }

            if (in_array($action->id, $this->actionId) &&
                in_array($action->controller->id, $this->controllerId) &&
                !($action->id == 'get-companies-to-crm' && $action->controller->id == 'company')
            ) {
                if (in_array($action->controller->id, ['bank', 'e-money',])) {
                    $strict = '?strict=' . str_replace('create', 'index', Url::current());
                } elseif (in_array($action->controller->id, ['order', 'payment-order',])) {
                    $strict = '?strict=' . Url::current();
                } else {
                    $strict = '&strict=' . Url::current();
                }

                return $action->controller->redirect([str_replace('create', 'index', Url::current()) . $strict]);
            }
        }

        return parent::beforeAction($action);
    }
}