<?php

namespace frontend\components;

use yii\base\Component;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;
use yii\web\Session;
use yii\web\User;

/**
 * @property-read Response $response
 * @property-read Request $request
 * @property-read Session $session
 * @property-read WebUser $user
 */
class WebParams extends Component
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var User
     */
    private $user;

    /**
     * @inheritDoc
     * @param Request $request
     * @param Response $response
     * @param Session $session
     * @param User $user
     * @param string $id
     * @param Controller $controller
     * @param mixed[] $config
     */
    public function __construct(Request $request, Response $response, Session $session, User $user, array $config = [])
    {
        $this->request = $request;
        $this->response = $response;
        $this->session = $session;
        $this->user = $user;

        parent::__construct($config);
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @return WebUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
