<?php

namespace frontend\components;

use Yii;

trait SalaryFormTrait
{
    /**
     * @param string $value
     * @return string
     */
    protected function setDateInputValue($value)
    {
        return $value && ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @param string $value
     * @return string
     */
    protected function getDateInputValue($value)
    {
        return $value ? date_create_from_format('Y-m-d', $value)->format('d.m.Y') : null;
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setSalary1PrepayDate($value)
    {
        $this->salary_1_prepay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getSalary1PrepayDate()
    {
        return $this->getDateInputValue($this->salary_1_prepay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setBonus1PrepayDate($value)
    {
        $this->bonus_1_prepay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getBonus1PrepayDate()
    {
        return $this->getDateInputValue($this->bonus_1_prepay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setSalary2PrepayDate($value)
    {
        $this->salary_2_prepay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getSalary2PrepayDate()
    {
        return $this->getDateInputValue($this->salary_2_prepay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setBonus2PrepayDate($value)
    {
        $this->bonus_2_prepay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getBonus2PrepayDate()
    {
        return $this->getDateInputValue($this->bonus_2_prepay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setSalary1PayDate($value)
    {
        $this->salary_1_pay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getSalary1PayDate()
    {
        return $this->getDateInputValue($this->salary_1_pay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setBonus1PayDate($value)
    {
        $this->bonus_1_pay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getBonus1PayDate()
    {
        return $this->getDateInputValue($this->bonus_1_pay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setSalary2PayDate($value)
    {
        $this->salary_2_pay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getSalary2PayDate()
    {
        return $this->getDateInputValue($this->salary_2_pay_date);
    }

    /**
     * @param string $value
     * @return string
     */
    protected function setBonus2PayDate($value)
    {
        $this->bonus_2_pay_date = $this->setDateInputValue($value);
    }

    /**
     * @return string
     */
    protected function getBonus2PayDate()
    {
        return $this->getDateInputValue($this->bonus_2_pay_date);
    }

    /**
     * @param number $value
     * @return integer
     */
    protected function setMoneyInputValue($value)
    {
        return trim($value) === '' ? null : round($value * 100);
    }

    /**
     * @param integer $value
     * @return number
     */
    protected function getMoneyInputValue($value)
    {
        return $value === null ? null : bcdiv($value, 100, 2);
    }

    /**
     * @param number $value
     */
    public function setSalary1Amount($value)
    {
        $this->salary_1_amount = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getSalary1Amount()
    {
        return $this->getMoneyInputValue($this->salary_1_amount);
    }

    /**
     * @param number $value
     */
    public function setSalary1PrepaySum($value)
    {
        $this->salary_1_prepay_sum = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getSalary1PrepaySum()
    {
        return $this->getMoneyInputValue($this->salary_1_prepay_sum);
    }

    /**
     * @param number $value
     */
    public function setBonus1Amount($value)
    {
        $this->bonus_1_amount = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getBonus1Amount()
    {
        return $this->getMoneyInputValue($this->bonus_1_amount);
    }

    /**
     * @param number $value
     */
    public function setBonus1PrepaySum($value)
    {
        $this->bonus_1_prepay_sum = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getBonus1PrepaySum()
    {
        return $this->getMoneyInputValue($this->bonus_1_prepay_sum);
    }

    /**
     * @param number $value
     */
    public function setSalary2Amount($value)
    {
        $this->salary_2_amount = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getSalary2Amount()
    {
        return $this->getMoneyInputValue($this->salary_2_amount);
    }

    /**
     * @param number $value
     */
    public function setSalary2PrepaySum($value)
    {
        $this->salary_2_prepay_sum = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getSalary2PrepaySum()
    {
        return $this->getMoneyInputValue($this->salary_2_prepay_sum);
    }

    /**
     * @param number $value
     */
    public function setBonus2Amount($value)
    {
        $this->bonus_2_amount = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getBonus2Amount()
    {
        return $this->getMoneyInputValue($this->bonus_2_amount);
    }

    /**
     * @param number $value
     */
    public function setBonus2PrepaySum($value)
    {
        $this->bonus_2_prepay_sum = $this->setMoneyInputValue($value);
    }

    /**
     * @return number
     */
    public function getBonus2PrepaySum()
    {
        return $this->getMoneyInputValue($this->bonus_2_prepay_sum);
    }
}
