<?php

namespace frontend\components;

use common\models\employee\Employee;
use Yii;
use yii\web\IdentityInterface;
use yii\web\User;

/**
 * @property Employee|IdentityInterface|null $identity
 */
class WebUser extends User
{
    public function getIsMtsUser()
    {
        if (isset(Yii::$app->params['is_mts_user'])) {
            return (bool) Yii::$app->params['is_mts_user'];
        }

        if (isset($_SERVER['HTTP_HOST']) && strpos(strtolower($_SERVER['HTTP_HOST']), 'mts') !== false) {
            return true;
        }

        $value = Yii::$app->request->cookies->getValue('_is_mts_user', 0);

        if (!Yii::$app->user->isGuest) {
            $dbValue = Yii::$app->user->identity->is_mts_user;
            if ($dbValue != $value) {
                Yii::$app->response->cookies->add(new \yii\web\Cookie([
                    'name' => '_is_mts_user',
                    'value' => $dbValue,
                ]));
                $value = $dbValue;
            }
        }

        return (bool) $value;
    }

    public function getIsOldKubUser()
    {
        if (isset(Yii::$app->params['is_old_kub_theme'])) {
            return (bool) Yii::$app->params['is_old_kub_theme'];
        }

        if ($this->isGuest) {
            return (bool) Yii::$app->request->cookies->getValue('is_old_kub_theme', 0);
        }

        if ($this->identity && $this->identity->company) {
            return (bool) $this->identity->is_old_kub_theme;
        }

        return true;
    }
}
