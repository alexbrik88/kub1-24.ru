<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.12.2016
 * Time: 7:54
 */

namespace frontend\components;

use common\components\date\DateHelper;
use common\models\address\Country;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\notification\ForWhom;
use common\models\notification\Notification;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductStore;
use common\models\product\ProductUnit;
use common\models\project\Project;
use common\models\TaxRate;
use frontend\modules\cash\models\CashContractorType;
use frontend\rbac\permissions\CashOrder;
use Intervention\Image\Exception\NotFoundException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class XlsHelper
 * @package frontend\components
 */
class XlsHelper
{
    /**
     * Instance of xls file
     * @var UploadedFile|null
     */
    public $xls = null;
    /**
     * @var string|null
     */
    public $filePath = null;
    /**
     *
     */
    const PRODUCT_SERVICES = 1;
    /**
     *
     */
    const PRODUCT_GOODS = 2;
    /**
     *
     */
    const CASH_ORDER = 3;

    const CASH_EXCERPT = 4;

    /**
     * @var array
     */
    public static $xlsTemplate = [
        self::PRODUCT_SERVICES => 'Uslugi.xlsx',
        self::PRODUCT_GOODS => 'Tovari.xlsx',
        self::CASH_ORDER => 'Shablon_Kassa.xlsx',
        self::CASH_EXCERPT => 'Shablon_Bank.xlsx',
    ];

    /**
     * @var array
     */
    public $attributes = [
        'Product' => [
            Product::PRODUCTION_TYPE_SERVICE => [
                'title',
                'group_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
            ],
            Product::PRODUCTION_TYPE_GOODS => [
                'title',
                'group_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'initQuantity',
                'code',
                'box_type',
                'count_in_place',
                'place_count',
                'mass_gross',
                'country_origin_id',
                'customs_declaration_number',
                'article',
                'provider_id',
                'weight',
                'volume',
                'store_irreducible_quantity',
                'comment',
                'photo',
                'comment_photo'
            ],
        ],
        'CashOrderFlows' => [
            'flow_type_income',
            'flow_type_expense',
            'contractor_id',
            'item_id',
            'date',
            'number',
            'description',
            'reason_id',
        ],
    ];

    /**
     * @var array
     */
    public $cashboxesList = [];

    /**
     * XlsHelper constructor.
     * @param UploadedFile $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->filePath = $file->tempName;
    }

    /**
     * @return array
     */
    public function importProduct()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $productionType = ArrayHelper::getValue(Yii::$app->request->post('Product'), 'production_type');

        return ImportProductHelper::import($company, Yii::$app->user->identity, $this->filePath, $productionType);
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function read()
    {
        if (($result = $this->importProduct()) !== false) {
            return $result;
        }

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $product = false;
        $service = false;
        $model = null;
        $error = [];
        $success = [];
        $update = [];
        $allCount = 0;
        $class = Yii::$app->request->post('className');
        $addToInvoice = Yii::$app->request->post('addToInvoice');
        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->filePath);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $invoiceData = null;

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $i = 0;
            $allCount++;
            $modelOptions = $this->getModel($class);
            /* @var $model Product */
            $model = $modelOptions['model'];
            $attributes = $modelOptions['attributes'];
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            $emptyRow = true;
            foreach ($cellIterator as $cell) {
                if (!empty(trim($cell->getCalculatedValue()))) {
                    $emptyRow = false;
                }
            }
            if ($emptyRow) {
                break;
            }
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cell) {
                if ($i === count($attributes)) {
                    break;
                }
                if ($model instanceof Product) {
                    $this->setProductAttributes($cell, $model, $attributes[$i]);
                }
                $i++;
            }
            $existsProduct = null;
            if ($model->production_type == Product::PRODUCTION_TYPE_GOODS) {
                foreach (['article', 'title'] as $attribute) {
                    if (!empty($model->$attribute)) {
                        /* @var $existsProduct Product */
                        $existsProduct = Product::find()
                            ->byCompany($company->id)
                            ->byUser()
                            ->andWhere([Product::tableName() . '.is_deleted' => false])
                            ->byProductionType(Product::PRODUCTION_TYPE_GOODS)
                            ->andWhere([$attribute => $model->$attribute])
                            ->one();
                        if ($existsProduct !== null) {
                            break;
                        }
                    }
                }
            }
            if ($existsProduct) {
                $existsProduct->initQuantity = current($model->initQuantity);
                $existsProduct->importInitCount = $model->importInitCount;
                $existsProduct->irreducibleQuantity = current($model->irreducibleQuantity);
                foreach ($attributes as $attribute) {
                    if ($model->hasAttribute($attribute)) {
                        $existsProduct->$attribute = $model->$attribute;
                    }
                }
                if ($existsProduct->save()) {
                    $update[] = $existsProduct->id;
                }
            } else {
                if (empty($model->title) || !$model->save()) {
                    if (!empty($model->title)) {
                        $error[] = '"' . $model->title . '"';
                    }
                } else {
                    if ($model->imageUrl) {
                        if (filter_var($model->imageUrl, FILTER_VALIDATE_URL)) {
                            $extension = pathinfo($model->imageUrl)['extension'];
                            if ($extension) {
                                $path = $model->getUploadPath() . DIRECTORY_SEPARATOR . uniqid() . '.' . $extension;
                                $ch = curl_init($model->imageUrl);
                                $fp = fopen($path, 'wb');
                                curl_setopt($ch, CURLOPT_FILE, $fp);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_exec($ch);
                                curl_close($ch);
                                fclose($fp);
                            }
                        }
                    }
                    if ($model->production_type == Product::PRODUCTION_TYPE_GOODS) {
                        $product = true;
                        $company->import_xls_product_count += 1;
                    } else {
                        $service = true;
                        $company->import_xls_service_count += 1;
                    }
                    $company->save(true, ['import_xls_product_count', 'import_xls_service_count']);
                    $success[] = $model->id;
                }
            }
        }
        //if (empty($error)) {
        self::unDelete($class, $success);
        //}
        if ($product) {
            \common\models\company\CompanyFirstEvent::checkEvent($company, 48);
        }
        if ($service) {
            \common\models\company\CompanyFirstEvent::checkEvent($company, 49);
        }
        if ($addToInvoice) {
            $invoiceData = [
                'items' => $model->getProductsByIds($success, $company->id),
            ];
        }

        return [
            'error' => !empty($error),
            // 'allCount' => $allCount,
            'successCount' => count($success),
            'updateCount' => count($update),
            'success' => implode($success, ', '),
            'errorCount' => count($error),
            'invoiceData' => $invoiceData,
            'errorProductsName' => implode($error, ', '),
        ];
    }

    public function loadCashboxes()
    {
        $this->cashboxesList = Yii::$app->user->identity
            ->getCashboxes()
            ->select('name')
            ->indexBy('id')
            ->column();
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws \PHPExcel_Exception
     * @throws \PhpOffice\PhpSpreadsheet\Calculation\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function readForOrder()
    {
        $model = null;
        $error = [];
        $success = [];
        $allCount = 0;
        $class = 'CashOrderFlows';
        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->filePath);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $fluidAttributes = null;
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) {
                $fluidAttributes = $this->getCashOrderFlowsAttributesScheme($row);
                continue;
            }
            $allCount++;
            $modelOptions = $this->getModel($class, $fluidAttributes);
            /* @var $model CashOrderFlows */
            $model = $modelOptions['model'];
            $attributes = $modelOptions['attributes'];
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            $emptyRow = true;
            foreach ($cellIterator as $cell) {
                if (!empty(trim($cell->getCalculatedValue()))) {
                    $emptyRow = false;
                }
            }
            if ($emptyRow) {
                break;
            }
            /* @var \PHPExcel_Cell $cell */
            $this->setCashOrderFlowsAttributes($row, $model, $attributes);
            if (empty($model->number) || !$model->save()) {
                if (!empty($model->number)) {
                    $errMsg = '';
                    // debug
                    if (YII_ENV_DEV || Yii::$app->user->id === 2) {
                        foreach ($model->getErrors() as $errKey => $errVal) {
                            $errMsg = $errKey . '('.$errVal[0].') ';
                            break;
                        }
                    }

                    $error[] = '"' . $model->number . '" ' . $errMsg;
                }
            } else {
                $success[] = $model->id;
            }
        }

        return [
            'error' => !empty($error),
            // 'allCount' => $allCount,
            'successCount' => count($success),
            'success' => implode(', ', $success),
            'errorCount' => count($error),
            'errorProductsName' => implode(', ', $error),
        ];
    }

    /**
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function readForEmailSummary()
    {
        $model = null;
        $error = [];
        $success = [];
        $notFound = [];
        $allCount = 0;
        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->filePath);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $i = 0;
            $allCount++;
            $company = null;
            $attributes = [
                'form_source',
                'form_keyword',
                'form_region',
                'form_date',
                'form_hour',
                'form_sign_in_page',
                'form_entrance_page',
                'form_unique_events_count',
            ];
            $cellIterator = $row->getCellIterator();

            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                $value = mb_substr($cell->getCalculatedValue(), 0, 255);
                if ($cellKey == 'A') {
                    if (!empty($value)) {
                        if (($company = $this->getCompany($value)) == null) {
                            $notFound[] = '"' . $value . '"';
                            continue 2;
                        }
                    } else {
                        continue 2;
                    }
                } elseif ($company !== null) {
                    $attribute = $attributes[$i];
                    $company->$attribute = $value;
                    $i++;
                    if ($i >= count($attributes)) {
                        break;
                    }
                }
            }
            if ($company !== null) {
                $company->form_date = substr($company->form_date, 0, 4) .
                    '-' . substr($company->form_date, 4, 2) .
                    '-' . substr($company->form_date, 6, 2);
                if (!$company->save(true, ['form_source', 'form_keyword',
                    'form_region', 'form_date', 'form_hour', 'form_sign_in_page', 'form_entrance_page',
                    'form_unique_events_count',])
                ) {
                    $error[] = '"' . $company->email . '"';
                } else {
                    $success[] = $company->id;
                }
            }
        }

        return [
            'successCount' => count($success),
            'notFoundCount' => count($notFound),
            'notFoundEmails' => implode($notFound, ', '),
            'success' => implode($success, ', '),
            'errorCount' => count($error),
            'errorCompanyEmail' => implode($error, ', '),
        ];
    }

    /**
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function readForNotification()
    {
        $model = null;
        $error = [];
        $success = [];
        $allCount = 0;
        $xls = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->filePath);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $i = 0;
            $allCount++;
            $company = null;
            $attributes = [
                'notification_type',
                'for_whom',
                'event_date',
                'activation_date',
                'title',
                'fine',
                'text',
            ];
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            $emptyRow = true;
            foreach ($cellIterator as $cell) {
                if (!empty(trim($cell->getCalculatedValue()))) {
                    $emptyRow = false;
                }
            }
            if ($emptyRow) {
                break;
            }
            $model = new Notification();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                $attribute = $attributes[$i] ?? null;
                if ($attribute === null) {
                    break;
                }
                if (\PhpOffice\PhpSpreadsheet\Shared\Date::isDateTime($cell)) {
                    $value = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cell->getCalculatedValue())->format('Y-m-d');
                } else {
                    $value = $cell->getCalculatedValue();
                }

                if ($cellKey == 'A') {
                    if (mb_strtolower($value) == 'налоговое') {
                        $model->$attribute = Notification::NOTIFICATION_TYPE_TAX;
                    } elseif (mb_strtolower($value) == 'системное') {
                        $model->$attribute = Notification::NOTIFICATION_TYPE_SYSTEM;
                    }
                } elseif ($cellKey == 'B') {
                    /* @var $forWhom ForWhom */
                    $forWhom = ForWhom::find()->andWhere(['name' => $value])->one();
                    if ($forWhom) {
                        $model->$attribute = $forWhom->id;
                    }
                } else {
                    $model->$attribute = $value;
                }
                $i++;
            }
            if (!$model->save()) {
                $error[] = '"' . $model->title . '"';
            } else {
                $success[] = $model->id;
            }
        }

        return [
            'successCount' => count($success),
            'success' => implode($success, ', '),
            'errorCount' => count($error),
            'errorCompanyEmail' => implode($error, ', '),
        ];
    }

    /**
     * @param $email
     * @return Company
     */
    public function getCompany($email)
    {
        return Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['email' => $email])
            ->andWhere(['not', ['email' => null]])
            ->orderBy([Company::tableName() . '.created_at' => SORT_ASC])
            ->one();
    }


    /**
     * @param $className
     * @param null $attributes
     * @return array
     * @throws NotFoundHttpException
     */
    public function getModel($className, $attributes = null)
    {
        switch ($className) {
            case 'Product':
                $model = new Product([
                    'scenario' => Product::SCENARIO_CREATE,
                    'company_id' => Yii::$app->user->identity->company->id,
                    'country_origin_id' => Country::COUNTRY_WITHOUT,
                    'is_deleted' => true,
                ]);
                $model->load(Yii::$app->request->post());
                $model->creator_id = Yii::$app->user->identity->id;
                $attributes = $this->attributes[$className][$model->production_type];
                break;
            case 'CashOrderFlows':
                $cashbox = Yii::$app->user->identity->getCashboxes()->andWhere([
                    'cashbox.id' => Yii::$app->request->post('cashbox'),
                ])->one();
                if ($cashbox === null) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
                $model = new CashOrderFlows([
                    'company_id' => Yii::$app->user->identity->company->id,
                    'author_id' => Yii::$app->user->identity->id,
                    'cashbox_id' => $cashbox->id,
                    'is_accounting' => $cashbox->is_accounting,
                ]);
                if (empty($attributes)) {
                    $attributes = $this->attributes[$className];
                }
                break;
            default:
                throw new NotFoundException();
        }

        return [
            'model' => $model,
            'attributes' => $attributes,
        ];
    }

    /**
     * @param $class
     * @param $modelArray
     */
    public static function unDelete($class, $modelArray)
    {
        $className = self::getClassName($class);
        foreach ($modelArray as $id) {
            $product = $className::findOne($id);
            if ($product !== null) {
                $product->is_deleted = false;
                $product->save(true, ['is_deleted']);
            }
        }
    }

    /**
     * @param $name
     * @return Product|Contractor
     */
    public static function getClassName($name)
    {
        switch ($name) {
            case 'Product':
                $className = Product::className();
                break;
            case 'Contractor':
                $className = Contractor::className();
                break;
            case 'CashOrderFlows':
                $className = CashOrderFlows::className();
                break;
            default:
                throw new NotFoundException();
        }

        return $className;
    }

    /**
     * @param $row
     * @return array
     */
    public function getCashOrderFlowsAttributesScheme($row)
    {
        $attributes = [];
        /* @var \PHPExcel_Cell $cell */
        foreach ($row->getCellIterator() as $cell) {
            $title = mb_strtolower(trim($cell->getValue()));
            if (mb_substr($title, 0, 6) == 'приход') {
                $attributes[] = 'flow_type_income';
            } elseif (mb_substr($title, 0, 6) == 'расход') {
                $attributes[] = 'flow_type_expense';
            } elseif (mb_substr($title, 0, 10) == 'покупатель') {
                $attributes[] = 'contractor_id';
            } elseif (mb_substr($title, 0, 6) == 'статья') {
                $attributes[] = 'item_id';
            } elseif (mb_substr($title, 0, 4) == 'дата' && mb_strpos($title, 'признания') === false) {
                $attributes[] = 'date';
            } elseif (mb_substr($title, 0, 5) == 'номер') {
                $attributes[] = 'number';
            } elseif (mb_substr($title, 0, 10) == 'назначение') {
                $attributes[] = 'description';
            } elseif (mb_substr($title, 0, 9) == 'основание') {
                $attributes[] = 'reason_id';
            } elseif (mb_strpos($title, 'физ') !== false && mb_strpos($title, 'лицо') !== false) {
                $attributes[] = 'is_physical_contractor';
            }  elseif (mb_substr($title, 0, 5) == 'касса') {
                $attributes[] = 'cashbox_name';
            } elseif (mb_substr($title, 0, 4) == 'дата' && mb_strpos($title, 'признания') !== false) {
                $attributes[] = 'recognitionDateInput';
            } elseif (mb_substr($title, 0, 6) == 'проект') {
                $attributes[] = 'project_id';
            }
        }

        // default columns count 8 (without "is_physical_contractor", "cashbox_name")
        if (count($attributes) < 8)
            return null;

        return $attributes;
    }

    /**
     * @param $row
     * @param CashOrderFlows $model
     * @param $attributes
     */
    public function setCashOrderFlowsAttributes($row, CashOrderFlows $model, $attributes)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $ownCompanyNames = [
            'short' => $company->companyType->name_short . ' ' . $company->name_short,
            'full' =>  $company->companyType->name_short . ' ' . $company->name_full,
        ];
        $isPhysicalContractor = false;
        $isOwnCompanyContractor = false;

        // prepare
        $i = 0;
        $cellIterator = $row->getCellIterator();
        foreach ($cellIterator as $cell) {
            if ($i === count($attributes)) {
                break;
            }
            $value = trim($cell->getCalculatedValue());
            if ($attributes[$i] == 'flow_type_income' && !empty($value)) {
                $model->flow_type = CashOrderFlows::FLOW_TYPE_INCOME;
            }
            if ($attributes[$i] == 'flow_type_expense' && !empty($value)) {
                $model->flow_type = CashOrderFlows::FLOW_TYPE_EXPENSE;
            }
            if ($attributes[$i] == 'is_physical_contractor' && !empty($value) && !$isOwnCompanyContractor) {
                $isPhysicalContractor = true;
            }
            if ($attributes[$i] == 'contractor_id' && (empty($value) || in_array($value, $ownCompanyNames))) {
                $isOwnCompanyContractor = true;
                $isPhysicalContractor = false;
            }
            if ($attributes[$i] == 'cashbox_name') {
                if ($cashboxId = array_search($value, $this->cashboxesList)) {
                    $model->cashbox_id = $cashboxId;
                }
            }
            $i++;
        }

        // parse xls
        $i = 0;
        $cellIterator = $row->getCellIterator();
        foreach ($cellIterator as $cell) {
            if ($i === count($attributes)) {
                break;
            }

            $attribute = $attributes[$i];
            $attributeValue = trim($cell->getCalculatedValue());

            if (in_array($attribute, ['flow_type_income', 'flow_type_expense'])) {
                if (!empty($attributeValue)) {
                    $model->amount = $attributeValue;
                }
            } elseif ($attribute == 'contractor_id') {
                $tContractor = Contractor::tableName();
                $tType = CompanyType::tableName();
                $contractorType = ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) ? Contractor::TYPE_CUSTOMER : Contractor::TYPE_SELLER;
                /* @var $cashContractor CashContractorType */

                if ($isOwnCompanyContractor) {
                    $model->contractor_id = 'company';
                }
                elseif ($isPhysicalContractor) {
                    /* @var $contractor Contractor */
                    $contractor = Contractor::find()
                        ->select([
                            'contractor.id',
                            'name',
                        ])
                        ->byCompany($company->id)
                        ->byDeleted()
                        ->byStatus(Contractor::ACTIVE)
                        ->byContractor($contractorType)
                        ->andWhere(['name' => $attributeValue])
                        ->one();
                    if ($contractor) {
                        $model->contractor_id = $contractor->id;
                    } else {
                        $model->contractor_id = self::createNewPhysicalContractor($contractorType, $attributeValue, $model->is_accounting);
                    }
                }
                elseif ($cashContractor = CashContractorType::find()->andWhere(['like', 'text', $attributeValue])->one()) {
                    $model->contractor_id = $cashContractor->name;
                } else {
                    /* @var $contractor Contractor */
                    $contractor = Contractor::find()
                        ->select([
                            'contractor.id',
                            "CONCAT({{{$tType}}}.[[name_short]], ' ', {{{$tContractor}}}.[[name]]) as fullName",
                        ])
                        ->leftJoin($tType, "{{{$tContractor}}}.[[company_type_id]] = {{{$tType}}}.[[id]]")
                        ->byCompany($company->id)
                        ->byDeleted()
                        ->byStatus(Contractor::ACTIVE)
                        ->byContractor($contractorType)
                        ->andHaving(['fullName' => $attributeValue])
                        ->one();
                    if ($contractor) {
                        $model->contractor_id = $contractor->id;
                    }
                }

            } elseif ($attribute == 'item_id') {

                /* @var $itemClassName InvoiceIncomeItem|InvoiceExpenditureItem */
                $itemClassName = $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME
                    ? InvoiceIncomeItem::className()
                    : InvoiceExpenditureItem::className();
                $itemAttributeName = $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME
                    ? 'income_item_id'
                    : 'expenditure_item_id';
                $model->$itemAttributeName = $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME
                    ? InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER
                    : InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;

                $item = null;
                if (empty($attributeValue) && $model->contractor_id) {
                    if ($contractor = Contractor::findOne($model->contractor_id)) {
                        $contractorAttributeId = 'invoice_' . $itemAttributeName;
                        $item = $itemClassName::find()
                            ->andWhere(['id' => $contractor->{$contractorAttributeId}])
                            ->andWhere(['or',
                                ['company_id' => $company->id],
                                ['company_id' => null],
                            ])->one();
                    }
                } else {

                    $item = $itemClassName::find()
                        ->andWhere(['name' => $attributeValue])
                        ->andWhere(['or',
                            ['company_id' => $company->id],
                            ['company_id' => null],
                        ])->one();
                }
                if ($item) {
                    $model->$itemAttributeName = $item->id;
                } else {
                    $item = new $itemClassName();
                    $item->name = $attributeValue;
                    $item->company_id = $company->id;
                    if ($item->save()) {
                        $model->$itemAttributeName = $item->id;
                    }
                }

            } elseif ($attribute == 'date') {
                if (is_numeric($attributeValue)) {
                    $attributeTimestampValue = ($attributeValue - 25569) * 86400;
                    $date = date(DateHelper::FORMAT_USER_DATE, $attributeTimestampValue);
                } else {
                    $date = date(DateHelper::FORMAT_USER_DATE, strtotime($attributeValue));
                }
                $model->date = $date;

            } elseif (in_array($attribute, ['number', 'description'])) {
                $model->{$attribute} = $attributeValue;
                if ($attribute == 'description' && !$attributeValue)
                    $model->{$attribute} = '-';
                if ($attribute = 'number' && !$attributeValue) {
                    $model->number = $model::getNextNumber($company->id, $model->flow_type, $model->date);
                }

            } elseif ($attribute == 'reason_id') {
                /* @var $reason CashOrdersReasonsTypes */
                $reason = CashOrdersReasonsTypes::find()
                    ->andWhere(['name' => $attributeValue])
                    ->andWhere(['or',
                        ['flow_type' => $model->flow_type],
                        ['flow_type' => null],
                    ])->one();
                if ($reason) {
                    $model->reason_id = $reason->id;
                }

            } elseif ($attribute == 'recognitionDateInput' && !empty($attributeValue)) {
                if (is_numeric($attributeValue)) {
                    $attributeTimestampValue = ($attributeValue - 25569) * 86400;
                    $date = date(DateHelper::FORMAT_USER_DATE, $attributeTimestampValue);
                } else {
                    $date = date(DateHelper::FORMAT_USER_DATE, strtotime($attributeValue));
                }
                $model->recognitionDateInput = $date;

            } elseif ($attribute == 'project_id' && !empty($attributeValue)) {
                $project = Project::find()
                    ->andWhere(['name' => $attributeValue])
                    ->andWhere(['company_id' => $company->id])
                    ->one();

                if ($project) {
                    $model->project_id = $project->id;
                } else {
                    $model->project_id = self::createNewProject($attributeValue);
                }
            }

            $i++;
        }
    }

    public function readBankOperations(): array {
        $attributes = [
            'A' => 'paymentDate',
            'B' => 'income',
            'C' => 'expense',
            'D' => 'contractorName',
            'E' => 'contractorInn',
            'F' => 'isContractorPhysical',
            'G' => 'itemName',
            'H' => 'paymentOrderNumber',
            'I' => 'recognitionDate',
            'J' => 'purpose',
            'K' => 'contractorRs',
            'L' => 'bankName',
            'M' => 'bankBik',
            'N' => 'payedInvoices',
            'O' => 'direction',
            'P' => 'project',
            'Q' => 'salePoint',
        ];
        $xls = IOFactory::load($this->filePath);
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $fluidAttributes = null;
        $companyBankName = $sheet->getCell('B1')->getFormattedValue();
        $companyBik = $sheet->getCell('B2')->getFormattedValue();
        $companyRs = $sheet->getCell('B3')->getFormattedValue();
        $currency = $sheet->getCell('B4')->getFormattedValue();
        $balanceStart = $sheet->getCell('B5')->getFormattedValue();
        $balanceEnd = $sheet->getCell('B6')->getFormattedValue();
        $items = [];
        $i = 0;
        foreach ($rowIterator as $key => $row) {
            if ($key <= 8) {
                continue;
            }

            $cellIterator = $row->getCellIterator();
            foreach ($cellIterator as $cellKey => $cell) {
                if (!isset($attributes[$cellKey])) {
                    break;
                }

                $items[$i][$attributes[$cellKey]] = trim($cell->getFormattedValue());
            }

            if (empty($items[$i]['income']) && empty($items[$i]['expense'])) {
                unset($items[$i]);
                break;
            }

            $i++;
        }

        if (empty($companyRs)) {
            return [];
        }

        return [
            'companyRs' => $companyRs,
            'companyBankName' => $companyBankName,
            'companyBik' => $companyBik,
            'currency' => $currency,
            'balanceStart' => $balanceStart,
            'balanceEnd' => $balanceEnd,
            'items' => $items,
        ];
    }

    /**
     * @param $projectName
     * @return mixed|null
     */
    protected static function createNewProject($projectName)
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->currentEmployeeCompany->company;
        $model = new Project([
            'company_id' => $company->id,
            'responsible' => $employee->id,
            'name' => $projectName,
            'start_date' => date('Y-01-01'),
            'end_date' => date('Y-12-31'),
            'number' => Project::getNextProjectNumber($company),
            'status' => Project::STATUS_INPROGRESS
        ]);

        if ($model->save(false)) {
            return $model->primaryKey;
        }

        return null;
    }

    /**
     * @param $type
     * @param $fullName
     * @param $isAccounting
     * @return int|null
     */
    protected static function createNewPhysicalContractor($type, $fullName, $isAccounting)
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->currentEmployeeCompany->company;
        $model = new Contractor([
            'company_id' => $company->id,
            'employee_id' => $employee->id,
            'responsible_employee_id' => $employee->id,
            'type' => $type,
            'face_type' => Contractor::TYPE_PHYSICAL_PERSON,
            'status' => Contractor::ACTIVE,
            'taxation_system' => 0,
            'contact_is_director' => 1,
            'not_accounting' => (int) !$isAccounting,
            'invoice_income_item_id' => ($type == Contractor::TYPE_CUSTOMER) ? InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER : null,
            'invoice_expenditure_item_id' => ($type == Contractor::TYPE_SELLER) ? InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT : null
        ]);
        $model->setScenario('insert');

        while (strpos($fullName, '  ') !== false) {
            $fullName = str_replace('  ', ' ', $fullName);
        }
        $nameArr = explode(' ', $fullName);
        if (count($nameArr) == 1) {
            $model->physical_lastname = $nameArr[0];
            $model->physical_firstname = '';
            $model->physical_no_patronymic = true;
        } elseif (count($nameArr) == 2) {
            $model->physical_lastname = $nameArr[0];
            $model->physical_firstname = $nameArr[1];
            $model->physical_no_patronymic = true;
        } else {
            $model->physical_lastname = $nameArr[0];
            $model->physical_firstname = $nameArr[1];
            $model->physical_patronymic = $nameArr[2];
        }

        if ($model->save(false)) {
            return $model->primaryKey;
        }

        return null;
    }

    /**
     * @param $cell
     * @param Product $model
     * @param $attribute
     */
    public function setProductAttributes($cell, $model, $attribute)
    {
        $attributeValue = trim($cell->getCalculatedValue());
        if (in_array($attribute, ['price_for_buy_nds_id', 'price_for_sell_nds_id'])) {
            $attributeValue = is_numeric($attributeValue) ? ($attributeValue . '%') : $attributeValue;
            /* @var $taxRate TaxRate */
            $taxRate = TaxRate::find()->andWhere(['name' => $attributeValue])->one();
            $model->setAttribute($attribute, $taxRate ? $taxRate->id : 100);
        } elseif ($attribute == 'product_unit_id') {
            /* @var $productUnit ProductUnit */
            $productUnit = ProductUnit::find()->andWhere(['like', 'name', $attributeValue])->one();
            $model->setAttribute($attribute, $productUnit ? $productUnit->id : $attributeValue);
        } elseif ($attribute == 'group_id') {
            /* @var $productGroup ProductGroup */
            $productGroup = ProductGroup::find()
                ->andWhere(['company_id' => [0, Yii::$app->user->identity->company->id]])
                ->andWhere(['production_type' => [0, $model->production_type]])
                ->andWhere(['title' => trim($cell->getCalculatedValue())])->one();
            if ($productGroup) {
                $model->setAttribute($attribute, $productGroup->id);
            } else {
                $productGroup = new ProductGroup([
                    'title' => trim($cell->getCalculatedValue()),
                    'company_id' => Yii::$app->user->identity->company->id,
                    'production_type' => $model->production_type,
                ]);
                if ($productGroup->save()) {
                    $model->setAttribute($attribute, $productGroup->id);
                }
            }
        } elseif ($attribute == 'country_origin_id') {
            /* @var $country Country */
            $country = Country::find()->andWhere(['like', 'name_short', trim($cell->getCalculatedValue())])->one();
            $model->setAttribute($attribute, $country ? $country->id : Country::COUNTRY_WITHOUT);
        } elseif (in_array($attribute, ['price_for_buy_with_nds', 'price_for_sell_with_nds'])) {
            $model->setAttribute($attribute, empty(trim($cell->getCalculatedValue())) ? 0 : trim($cell->getCalculatedValue()));
        } elseif ($attribute == 'mass_gross') {
            $model->setAttribute($attribute, (int)trim($cell->getCalculatedValue()));
        } elseif ($attribute == 'initQuantity') {
            $model->initQuantity = is_numeric($cell->getCalculatedValue()) ? $cell->getCalculatedValue() * 1 : 0;
            $model->importInitCount = is_numeric($attributeValue) ? $attributeValue * 1 : 0;
        } elseif ($attribute == 'provider_id') {
            if ($attributeValue) {
                /* @var $contractor Contractor */
                $contractor = Contractor::getSorted()
                    ->byCompany(Yii::$app->user->identity->company->id)
                    ->byContractor(Contractor::TYPE_SELLER)
                    ->byIsDeleted(Contractor::NOT_DELETED)
                    ->byStatus(Contractor::ACTIVE)
                    ->andHaving(['like', 'name', $attributeValue])
                    ->one();
                if ($contractor) {
                    $model->setAttribute($attribute, $contractor->id);
                }
            }
        } elseif ($attribute == 'store_irreducible_quantity') {
            $model->irreducibleQuantity = $attributeValue * 1;
        } elseif ($attribute == 'photo') {
            $model->imageUrl = $attributeValue;
        } elseif (in_array($attribute, ['weight', 'volume'])) {
            $model->setAttribute($attribute, floatval(str_replace(',', '.', $attributeValue)));
        } else {
            $model->setAttribute($attribute, trim($cell->getCalculatedValue()));
        }
    }
}