<?php

namespace frontend\components;

use common\components\zchb\ZCHBAPIException;
use common\components\zchb\ZCHBHelper;

use common\components\helpers\ArrayHelper;
use common\models\ZchbLog;
use Yii;
use yii\db\Exception;

/**
 * Помощник доступа к API ЗАЧЕСТНЫЙБИЗНЕС.
 * Дополнительно ведёт лог запросов
 */
class ZCHBHelperContractorLog extends ZCHBHelper
{

    private $_contractorId;
    /** @var ZchbLog[] */
    private $_log = [];

    /**
     * @param bool $useCache
     * @param int  $contractorId ID контрагента, используется для логирования
     * @return self
     * @throws ZCHBAPIException
     */
    public static function instance(bool $useCache = true, int $contractorId = null): self
    {
        static $self;
        if ($self === null) {
            $apiKey = Yii::$app->params['zachestniybiznes_api_key'] ?? null;
            if (is_string($apiKey) === false) {
                throw new ZCHBAPIException('API-ключ доступа не определён');
            }
            $self = new static($apiKey, $useCache, $contractorId);
        }
        return $self;
    }

    public function __construct(string $apiKey, bool $useCache = true, $contractorId = null)
    {
        parent::__construct($apiKey, $useCache);
        $this->_contractorId = $contractorId;
    }

    /**
     * @throws Exception
     */
    public function __destruct()
    {
        if (count($this->_log) > 0) {
            $rows = ArrayHelper::getColumn($this->_log, 'attributes');
            Yii::$app->db->createCommand()->batchInsert($this->_log[0]::tableName(), $this->_log[0]->attributes(),
                $rows)->execute();
        }
    }

    protected function makeRequest(string $method, string $params)
    {
        try {
            $result = parent::makeRequest($method, $params);
            $this->_log($method, $params, true);
        } catch (ZCHBAPIException $e) {
            $this->_log($method, $params, false);
            throw $e;
        }
        return $result;
    }

    /**
     * @param string $method
     * @param string $params
     * @return array|null
     * @throws ZCHBAPIException
     */
    protected function makeRequestFake(string $method, string $params)
    {
        try {
            $result = parent::makeRequest($method, $params);
            $this->_log($method, $params, true);
        } catch (ZCHBAPIException $e) {
            $this->_log($method, $params, false);
            throw $e;
        }
        return $result;
    }

    /**
     * @param string $method
     * @param string $query
     * @param bool   $success
     */
    private function _log(string $method, string $query, bool $success)
    {
        if ($this->_contractorId === null) {
            return;
        }
        $this->_log[] = new ZchbLog([
            'contractor_id' => $this->_contractorId,
            'date' => date('Y-m-d H:i:s'),
            'method' => $method,
            'query' => $query,
            'success' => $success,
            'url' => Yii::$app->request->url
        ]);
    }

}

