<?php
namespace frontend\components;

use common\components\CommonController;
use common\components\date\DateHelper;
use common\components\sender\unisender\UniSender;
use common\models\Chat;
use common\models\Company;
use common\models\company\Attendance;
use common\models\company\CompanyVisit;
use common\models\DiscountType;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\service\ServiceModule;
use common\models\service\ServiceModuleVisit;
use common\models\service\SubscribeHelper;
use common\models\TimeZone;
use frontend\components\StatisticPeriod;
use frontend\components\filters\CompleteRegistrationFilter;
use frontend\rbac\permissions;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;

/**
 * Class FrontendController
 * @package frontend\components
 */
abstract class FrontendController extends CommonController
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'authAccess' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'completeRegistration' => CompleteRegistrationFilter::className(),
        ];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->getUniqueId() == 'site/error') return true;

        if (!Yii::$app->user->isGuest && ($identity = Yii::$app->user->identity)) {
            /* @var Company $company */
            $company = $identity->company;

            if ($company === null) {
                $company = $identity->companies[0] ?? null;
                if ($company !== null) {
                    $identity->updateAttributes(['company_id' => $company->id]);
                    Yii::$app->getResponse()->redirect(Url::to(['/site/index']), 302)->send();
                    exit();
                } elseif ($action->getUniqueId() !== 'company/create') {
                    throw new ForbiddenHttpException("Компания не найдена.");
                }
            }
        }

        if (parent::beforeAction($action)) {
            if ($action->id == 'logout') return true;
            $request = Yii::$app->getRequest();

            /* @var Employee $identity */
            $identity = Yii::$app->user->identity;
            if ($identity && $identity->is_active == 0 && $action->id != 'error') {
                Yii::$app->user->logout();
                throw new ForbiddenHttpException("Ваш аккаунт заблокирован. Обратитесь в службу технической поддержки.");
            }

            \common\components\SyncTimeZone::sync();

            if (!Yii::$app->user->isGuest) {
                $session = Yii::$app->session;
                $session->set('currentEmployeeId', $identity->id);

                $dateSqlString = date('Y-m-d');

                /* @var Company $company */
                $company = $identity->company;

                if ($company) {
                    $session->set('currentCompanyId', $company->id);

                    if (!$request->getIsAjax()) {
                        if (!Attendance::find()->andWhere(['and',
                            ['company_id' => $company->id],
                            ['date' => date(DateHelper::FORMAT_DATE)],
                        ])->exists()
                        ) {
                            $attendance = new Attendance();
                            $attendance->company_id = $company->id;
                            $attendance->date = date(DateHelper::FORMAT_DATE);
                            $attendance->activity_status = $company->activation_type;
                            if (Attendance::find()->andWhere(['company_id' => $company->id])->exists()) {
                                $attendance->is_first_visit = true;
                            }
                            $attendance->save();
                        }

                        CompanyVisit::checkVisit($company);
                        ServiceModuleVisit::checkVisit($company);
                        \common\models\company\CompanyLastVisit::create($company, $identity, Yii::$app->getRequest()->getUserIP());

                        if ($identity->is_registration_completed) {
                            if (empty($session['statistic_range'])) {
                                $session['statistic_range'] = [
                                    'name' => StatisticPeriod::DEFAULT_PERIOD,
                                    'date_from' => StatisticPeriod::defaultPeriodFrom()->format(DateHelper::FORMAT_DATE),
                                    'date_to' => StatisticPeriod::defaultPeriodTo()->format(DateHelper::FORMAT_DATE),
                                ];
                            }
                        }

                        // overdue invoices status update
                        if ($session->get("{$company->id}_invoice_status_checked_date") != $dateSqlString) {
                            $overdueInvoices = Invoice::find()
                                ->where([
                                    'company_id' => $company->id,
                                    'is_deleted' => Invoice::NOT_IS_DELETED,
                                    'invoice_status_id' => [
                                        InvoiceStatus::STATUS_CREATED,
                                        InvoiceStatus::STATUS_SEND,
                                        InvoiceStatus::STATUS_VIEWED,
                                        InvoiceStatus::STATUS_APPROVED,
                                    ],
                                ])
                                ->andWhere(['<', 'payment_limit_date', $dateSqlString])
                                ->andWhere(['not', ['payment_limit_rule' => [Invoice::PAYMENT_LIMIT_RULE_DOCUMENT, Invoice::PAYMENT_LIMIT_RULE_MIXED]]])
                                ->all();

                            if ($overdueInvoices) {
                                foreach ($overdueInvoices as $invoice) {
                                    $invoice->invoice_status_id = InvoiceStatus::STATUS_OVERDUE;
                                    $invoice->invoice_status_updated_at = strtotime("{$invoice->payment_limit_date} +1 day");
                                    $invoice->save(false, ['invoice_status_id', 'invoice_status_updated_at']);
                                }
                            }

                            $session->set("{$company->id}_invoice_status_checked_date", $dateSqlString);
                        }

                        if (!$company->hasActualSubscription && !$company->free_tariff_start_at) {
                            $company->applyFreeTariff();
                            if ($chief = $company->getEmployeeChief()) {
                                \Yii::$app->mailer->compose([
                                    'html' => 'system/free-tariff/html',
                                    'text' => 'system/free-tariff/text',
                                ], [
                                    'subject' => 'Тариф БЕСПЛАТНО',
                                    'companyName' => $company->getTitle(true),
                                ])
                                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                                    ->setTo($chief->email)
                                    ->setSubject('Тариф БЕСПЛАТНО.')
                                    ->send();
                            }
                        }

                        if ($session->get("discount_type_1_checked_{$company->id}") != $dateSqlString) {
                            DiscountType::checkForType1($company);
                            $session->set("discount_type_1_checked_{$company->id}", $dateSqlString);
                        }
                        if ($session->get("discount_type_2_checked_{$company->id}") != $dateSqlString) {
                            DiscountType::checkForType2($company);
                            $session->set("discount_type_2_checked_{$company->id}", $dateSqlString);
                        }
                    }

                    if ($company->strict_mode == Company::ON_STRICT_MODE) {
                        if ($action->id !== 'strict-mode-error' &&
                            !Yii::$app->user->can(permissions\Company::STRICT_MODE_ACCESS)
                        ) {
                            Yii::$app->getResponse()->redirect(Url::to(['/site/strict-mode-error']), 302)->send();
                            exit();
                        }
                    }
                }
            }

            $referrer = $request->getReferrer();
            $isUpdateDocument = ($action->controller->module->id == 'documents' && $action->id == 'update' && $referrer == $request->getAbsoluteUrl());
            $isReferrerUpdateDocument = (strpos($referrer, '/create') || strpos($referrer, '/update'));
            if (!$request->getIsAjax() && $referrer && $referrer != $request->getAbsoluteUrl() && !$isUpdateDocument) {
                if (!$isReferrerUpdateDocument)
                    Url::remember($referrer, 'lastPage');
            }

            return true;
        } else {
            return false;
        }
    }
}
