<?php

namespace frontend\components;

use common\components\filters\AccessControl;
use common\components\zchb\Card;
use common\components\zchb\CourtArbitration;
use common\components\zchb\FinancialStatement;
use common\components\zchb\Manager;
use common\components\zchb\ReliabilityHelper;
use common\components\zchb\ZCHBAPIException;
use common\components\zchb\ZCHBHelper;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Отображение досье.
 * Абстрактный контроллер, не привязанный к конкретным сущностям, по которым строится досье.
 * Для доступа к данным, полученным через API ЗАЧЕСТНЫЙБИЗНЕС, рекомендуется использовать $this->manager.
 */
abstract class DossierController extends FrontendController
{

    /** @var Manager Предоставляет доступ к разным частям досье */
    public $manager;

    /**
     * Должен вернуть ссылку, пригодную для Url::toRoute() на указанную вкладку
     * @param string $tab Имя вкладки
     * @return array
     */
    abstract public function tabLink(string $tab);

    /**
     * Должен вернуть имя текущей открытой вкладки
     * @return mixed
     */
    abstract protected function currentTab();

    /**
     * Должен "отренденить" представление
     * Если $viewFile не задан, то его следует взять из $this->view.
     * @param array       $data     Данные представления
     * @param string|null $viewFile Имя файла представления
     * @return mixed
     */
    abstract protected function renderTab(array $data, string $viewFile = null);

    /**
     * Должен вернуть название сущности, для которой формируется досье
     * @return string
     */
    abstract protected function contractorTitle();

    /**
     * Должен вернуть идентификатор сущности, по которой формируется досье, используется для построения ссылок
     * @return int
     */
    abstract protected function getModelId();

    /**
     * Должен вернуть идентификатор (ОГРН/ИНН) для карточки контрагента
     * @return string ОГРН/ИНН
     * @see Card
     */
    abstract protected function cardId();

    /**
     * @inheritDoc
     * @throws ZCHBAPIException
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = static::getManager();
        $this->view->params['tabLink'] = [$this, 'tabLink'];
        $this->view->params['currentTab'] = $this->currentTab();
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'founders',
                            'connections',
                            'reliability',
                            'finance',
                            'arbitr',
                            'okved',
                            'accounting',
                            'debt',
                            'cache-clear',
                            'tariff',
                            'empty-itn',
                            'ajax-connections',
                            'ajax-finance',
                            'ajax-fl-card',
                            'ajax-arbitr',
                            'ajax-debt'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function beforeAction($action)
    {
        // Отключено
        throw new NotFoundHttpException('The requested page does not exist.');

        if (!$this->cardId()) {
            /** @noinspection PhpUndefinedFieldInspection */
            $action->actionMethod = 'actionEmptyItn';
            $action->id = 'empty-itn';
        }
        return parent::beforeAction($action);
    }

    /**
     * "Заглушка" на случай, если у контрагента нет ОГРН/ИНН
     */
    public function actionEmptyItn() {
        return $this->renderTab([]);
    }

    /**
     * Главная вкладка
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        try {
            $reliabilityHelper = new ReliabilityHelper($this->cardId(), $this->manager);
            $data = [
                'modelId' => $this->getModelId(),
                'title' => $this->contractorTitle(),
                'card' => $reliabilityHelper->card,
                'diff' => $reliabilityHelper->diff,
                'courtArbitration' => $reliabilityHelper->ca,
                'contact' => $this->manager->contactNew($reliabilityHelper->card->ОГРН ?? $reliabilityHelper->card->ИНН),
                'reliability' => $reliabilityHelper
            ];
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
        return $this->renderTab($data);
    }

    /**
     * Вкладка "Учредители"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFounders()
    {
        try {
            return $this->renderTab([
                'modelId' => $this->getModelId(),
                'card' => $this->manager->cardInstance($this->cardId())
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Связи"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConnections()
    {
        try {
            return $this->renderTab($this->dataConnections());
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Надёжность"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionReliability()
    {
        try {
            return $this->renderTab([
                'reliability' => new ReliabilityHelper($this->cardId(), $this->manager)
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Финансы"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFinance()
    {
        try {
            $card = $this->manager->cardInstance($this->cardId());
            if ($card->ОГРН === null) {
                return $this->renderTab(['fs' => null]);
            }
            $fs = $this->manager->financialStatementNew($card->ОГРН ?? $card->ИНН);
            $out = [
                'fs' => $fs,
                'card' => $card
            ];
            if ($fs !== null) {
                /** @var FinancialStatement $fs */
                $year = $fs->lastYear([
                    $fs::INDICATOR_REVENUE,
                    $fs::INDICATOR_PROFIT,
                    $fs::INDICATOR_TAX,
                    $fs::INDICATOR_FIXED_ASSETS
                ]);
                $fs->setYear($year);
                $amount = $fs->amountRevenue($year);
                $out['revenue'] = [$amount, $amount - $fs->amountRevenue(-1)];
                $amount = $fs->amountProfit($year);
                $out['profit'] = [$amount, $amount - $fs->amountProfit(-1)];
                $amount = $fs->amountFixedAssets($year);
                $out['fixedAssets'] = [$amount, $amount - $fs->amountFixedAssets(-1)];
                $amount = $fs->valueOf('report->Доходы и расходы по обычным видам деятельности->Коммерческие расходы') +
                    $fs->valueOf('report->Доходы и расходы по обычным видам деятельности->Управленческие расходы') +
                    $fs->valueOf('report->Прочие доходы и расходы->Проценты к уплате') +
                    $fs->valueOf('report->Прочие доходы и расходы->Прочие расходы') +
                    $fs->valueOf(2410);
                $out['expenses'] = [
                    $amount,
                    $amount - $fs->valueOf('report->Доходы и расходы по обычным видам деятельности->Коммерческие расходы',
                        -1) -
                    $fs->valueOf('report->Доходы и расходы по обычным видам деятельности->Управленческие расходы', -1) -
                    $fs->valueOf('report->Прочие доходы и расходы->Проценты к уплате', -1) -
                    $fs->valueOf('report->Прочие доходы и расходы->Прочие расходы', -1) -
                    $fs->valueOf(2410, -1),
                ];
                $amount = $fs->valueOf(1230);
                $out['receivables'] = [(int)$amount, (int)($amount - $fs->valueOf(1230, -1))];
                if ($card->tax[Card::TAX_STS] > 0) {
                    $out['taxList'] = [$card::TAX_STS, $card::TAX_ESTATE, $card::TAX_NO_TAX];
                } else {
                    $out['taxList'] = [$card::TAX_VAT, $card::TAX_PROFIT, $card::TAX_ESTATE, $card::TAX_NO_TAX];
                }
            }
            return $this->renderTab($out);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Суды"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionArbitr()
    {
        try {
            $card = $this->manager->cardInstance($this->cardId());
            if ($card->ОГРН === null && $card->ИНН === null) {
                return $this->renderTab([
                    'modelId' => $this->getModelId(),
                    'court' => null
                ]);
            }
            /** @var CourtArbitration $court */
            $court = $this->manager->courtArbitrationInstance($card->ОГРН ?? $card->ИНН);
            $court->inn = $card->ИНН;
            return $this->renderTab([
                'modelId' => $this->getModelId(),
                'court' => $court
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Долги"
     * @return string
     */
    public function actionDebt()
    {
        return $this->renderTab([
            'modelId' => $this->getModelId()
        ]);
    }

    /**
     * Вкладка "Виды деятельности"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOkved()
    {
        try {
            return $this->renderTab(['card' => $this->manager->cardInstance($this->cardId())]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Вкладка "Бухгалтерская отчётность"
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAccounting()
    {
        try {
            $card = $this->manager->cardInstance($this->cardId());
            if ($card->ОГРН === null) {
                $fs = null;
            } else {
                $fs = FinancialStatement::instance($card->ОГРН ?? $card->ИНН);
            }
            return $this->renderTab([
                'card' => $card,
                'fs' => $fs
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab(['title' => $this->contractorTitle()], 'exception');
        }
    }

    /**
     * Блок "Связи" на главной вкладке (AJAX)
     * @param string $ogrn
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAjaxConnections(string $ogrn)
    {
        try {
            return $this->renderTab($this->dataConnections($ogrn));
        } catch (ZCHBAPIException $e) {
            return $this->renderTab([], 'ajax-exception');
        }

    }

    /**
     * Блок финансовой отчётности на главной вкладке (AJAX)
     * `contractor/ajax-finance?id={id}
     * @param string $ogrn
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAjaxFinance(string $ogrn)
    {
        try {
            $fs = $this->manager->financialStatementNew($ogrn);
            $year = $fs->lastYear([$fs::INDICATOR_REVENUE, $fs::INDICATOR_PROFIT]);
            $year2 = $fs->lastYear([$fs::INDICATOR_TAX, $fs::INDICATOR_FIXED_ASSETS]);
            $card = $this->manager->cardInstance($ogrn);
            $deduction = $card->tax[Card::TAX_DISABILITY] + $card->tax[Card::TAX_MEDIC] + $card->tax[Card::TAX_PENSION];
            return $this->renderTab([
                'title' => $this->contractorTitle(),
                'fs' => $fs,
                'revenue' => $fs::amountFormat($fs->amountRevenue($year), null, '0'),
                'revenueChange' => $fs->amountRevenue($year) - $fs->amountRevenue($year - 1),
                'profit' => $fs::amountFormat($fs->amountProfit($year), null, '0'),
                'profitChange' => $fs->amountProfit($year) - $fs->amountProfit($year - 1),
                'lastYear' => $year2,
                'tax' => $fs::amountFormat($card->tax[Card::TAX_PROFIT] + $card->tax[Card::TAX_VAT] + $card->tax[Card::TAX_STS]),
                'fixedAssets' => $fs::amountFormat($fs->amountFixedAssets($year2), null, '0 ₽'),
                'fixedAssetsChange' => $fs->amountFixedAssets($year) - $fs->amountFixedAssets($year - 1),
                'deduction' => $fs::amountFormat($deduction), //отчисления
                'arrearsTotal' => $fs::amountFormat(array_sum($card->arrearsTotal()), null, '0')
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab([], 'ajax-exception');
        }
    }

    /**
     * Всплывающее окно с данными физического лица (AJAX)
     * `contractor/ajax-fl-card?id={id}&contrctorId={contractorId}
     * @param string $id ИНН
     * @return string
     */
    public function actionAjaxFlCard(string $id)
    {
        try {
            return $this->renderTab(['flCard' => $this->manager->flCardInstance($id)]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab([], 'ajax-exception');
        }
    }

    /**
     * AJAX данные по судебным делам
     * Может запрашивать много данных через API
     *
     * @param string      $ogrn
     * @param string      $inn
     * @param string|null $role
     * @param string|null $status
     * @param string|null $sort
     * @return string
     */
    public function actionAjaxArbitr(
        string $ogrn,
        string $inn,
        string $role = null,
        string $status = null,
        string $sort = null
    ) {
        try {
            $courtCard = $this->manager->courtArbitrationCardNew($ogrn, $inn);
            if ($role !== null) {
                $courtCard->filterByRole($role);
            }
            if ($status !== null) {
                $courtCard->filterByStatus($status);
            }
            if ($sort !== null) {
                $courtCard->sort($sort);
            }
            return $this->renderTab([
                'modelId' => $this->getModelId(),
                'courtCard' => $courtCard
            ]);
        } catch (ZCHBAPIException $e) {
            return $this->renderTab([], 'ajax-exception');
        }
    }

    /**
     * @return Manager
     * @throws ZCHBAPIException
     */
    protected static function getManager()
    {
        return new Manager(ZCHBHelper::instance());
    }

    /**
     * @param string $id
     * @return array
     * @throws NotFoundHttpException
     * @throws ZCHBAPIException
     */
    protected function dataConnections($id = null)
    {
        $card = $this->manager->cardInstance($id ?? $this->cardId());
        $data = [
            'card' => $card,
            'connections' => [],
            'countByDirector' => 0,
            'countByFounder' => 0,
        ];
        if ($card->ОГРН !== null || $card->ИНН !== null) {
            $connectionHelper = $this->manager->connectHelperNew();
            $connections = $connectionHelper->get($card);
            $cnt1 = $cnt2 = 0;
            foreach ($connections as $item) {
                if ($item['byDirector'] === true) {
                    $cnt1++;
                }
                if ($item['byFounder'] === true) {
                    $cnt2++;
                }
            }
            $data['connections'] = $connections;
            $data['countByDirector'] = $cnt1;
            $data['countByFounder'] = $cnt2;
        }
        return $data;
    }

}