<?php
namespace frontend\components;

use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use DateTime;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\documents\components\InvoiceStatistic;
use Yii;
use yii\db\Query;
use yii\web\JsExpression;

class HighchartsFlowsHelper extends \yii\base\Component
{
    const STEP_DAY = 1;
    const STEP_WEEK = 2;
    const STEP_MONTH = 3;

    const DEFAULT_STEP = self::STEP_DAY;

    public static $stepItems = [
        self::STEP_DAY => 'День',
        self::STEP_WEEK => 'Неделя',
        self::STEP_MONTH => 'Месяц',
    ];

    public static $keyFormat = [
        self::STEP_DAY => 'Y-m-d',
        self::STEP_WEEK => 'Y-m-W',
        self::STEP_MONTH => 'Y-m',
    ];

    public static $labelFormat = [
        self::STEP_DAY => 'd MMMM',
        self::STEP_WEEK => 'd MMMM',
        self::STEP_MONTH => 'LLLL Y',
    ];

    public $from;
    public $to;
    public $step;

    protected $company;

    /**
     * Constructor.
     * @param Company $company
     * @param array $config additional configurations for this object
     */
    public function __construct(Company $company, $config = [])
    {
        $this->company = $company;
        parent::__construct($config);
    }

    /**
     * @return mixed
     */
    public function getOptions($from, $to, $step)
    {
        $company = $this->company;

        if (!in_array($step, array_keys(self::$stepItems))) {
            $step = self::DEFAULT_STEP;
        }
        $keyFormat = self::$keyFormat[$step];
        $labelFormat = self::$labelFormat[$step];
        $balance = 0;

        $select = ['amount'];
        $where = [
            'and',
            ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['<', 'date', $from],
            //['not', ['IFNULL([[income_item_id]], 0)' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]],
        ];
        $balance += (int) $company->getAllFlows($select, $where)->sum('amount');
        $where = [
            'and',
            ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
            ['<', 'date', $from],
            //['not', ['IFNULL([[expenditure_item_id]], 0)' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]],
        ];
        $balance -= (int) $company->getAllFlows($select, $where)->sum('amount');

        $select = [
            'date',
            'amount',
        ];
        $where = [
            'and',
            ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
            ['between', 'date', $from, $to],
            //['not', ['IFNULL([[expenditure_item_id]], 0)' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]],
        ];
        $expensesDataArray = (new Query)
            ->select([
                'sum' => 'SUM([[amount]])',
                'date',
            ])
            ->from(['t' => $company->getAllFlows($select, $where)])
            ->groupBy('date')
            ->orderBy(['date' => SORT_ASC])
            ->indexBy('date')
            ->column();
        $where = [
            'and',
            ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['between', 'date', $from, $to],
            //['not', ['IFNULL([[income_item_id]], 0)' => InvoiceIncomeItem::ITEM_OWN_FOUNDS]],
        ];
        $incomesDataArray = (new Query)
            ->select([
                'sum' => 'SUM([[amount]])',
                'date',
            ])
            ->from(['t' => $company->getAllFlows($select, $where)])
            ->groupBy('date')
            ->orderBy(['date' => SORT_ASC])
            ->indexBy('date')
            ->column();

        $beginDate = date_create_from_format('Y-m-d', $from);
        $endDate = date_create_from_format('Y-m-d', $to);
        $todayDate = new DateTime;

        $categories = [];
        $expensesData = [];
        $incomesData = [];
        $balanceData = [];

        do {
            $key = $beginDate->format($keyFormat);
            $date = $beginDate->format('Y-m-d');
            $categories[$key] = $beginDate->format('d') . ' ' . (mb_strtolower(Month::$monthGenitiveRU[$beginDate->format('m')]));
            if (!array_key_exists($key, $expensesData)) {
                $expensesData[$key] = null;
            }
            if (!array_key_exists($key, $incomesData)) {
                $incomesData[$key] = null;
            }
            if (!array_key_exists($key, $balanceData)) {
                $balanceData[$key] = null;
            }
            if ($beginDate <= $todayDate) {
                $exp = isset($expensesDataArray[$date]) ? (int) $expensesDataArray[$date] : 0;
                $inc = isset($incomesDataArray[$date]) ? (int) $incomesDataArray[$date] : 0;
                $balance += $inc;
                $balance -= $exp;

                $expensesData[$key] += $exp / 100;
                $incomesData[$key] += $inc / 100;
                $balanceData[$key] = $balance / 100;
            }
            $beginDate->modify('+1 day');
        } while ($beginDate <= $endDate);

        return [
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => array_values($categories),
            ],
            'yAxis' => [
                [
                    'title' => ['text' => 'Приход / Расход'],
                    'allowDecimals' => false,
                ],
                [
                    'title' => ['text' => 'Остаток'],
                    'allowDecimals' => false,
                    'opposite' => true,
                ],
            ],
            'series' => [
                [
                    'type' => 'column',
                    'name' => 'Приход',
                    'color' => '#45b6af',
                    'data' => array_values($incomesData),
                    'groupPadding' => 0.05,
                    'pointPadding' => 0.05,
                    'tooltip' => [
                        'valueDecimals' => 2,
                        'valueSuffix' => ' руб.',
                    ]
                ],
                [
                    'type' => 'column',
                    'name' => 'Расход',
                    'color' => '#f3565d',
                    'data' => array_values($expensesData),
                    'groupPadding' => 0.05,
                    'pointPadding' => 0.05,
                    'tooltip' => [
                        'valueDecimals' => 2,
                        'valueSuffix' => ' руб.',
                    ]
                ],
                [
                    'type' => 'line',
                    'name' => 'Остаток денег',
                    'color' => '#456baf',
                    'yAxis' => 1,
                    'data' => array_values($balanceData),
                ],
            ],
            'exporting' => [
                'buttons' => [
                    'contextButton' => [
                        'menuItems' => new JsExpression('
                            Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems.slice(0,2).concat(
                                Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems[3],
                                Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems[4]
                            )
                        '),
                    ],
                ],
            ],
        ];
    }
}