<?php

namespace frontend\components;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\models\Documents;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use Yii;
use yii\db\Query;

class EmployeeRating extends \yii\base\Component
{
    const TYPE_AMOUNT = 'amount';
    const TYPE_COUNT = 'count';
    const TYPE_PAYMENT = 'payment';
    const TYPE_DEBT = 'debt';

    protected $_type;
    protected $_date;
    protected $_currentData;
    protected $_previousData;

    public static $typeArray = [
        self::TYPE_AMOUNT,
        self::TYPE_COUNT,
        self::TYPE_PAYMENT,
        self::TYPE_DEBT,
    ];

    public static $selectArray = [
        self::TYPE_AMOUNT => 'SUM([[total_amount_with_nds]])',
        self::TYPE_COUNT => 'COUNT([[document_author_id]])',
        self::TYPE_PAYMENT => 'SUM({{pay}}.[[flow_sum]])',
        self::TYPE_DEBT => 'SUM([[remaining_amount]])',
    ];

    public static $unitArray = [
        self::TYPE_AMOUNT => '<i class="fa fa-rub"></i>',
        self::TYPE_COUNT => 'шт.',
        self::TYPE_PAYMENT => '<i class="fa fa-rub"></i>',
        self::TYPE_DEBT => '<i class="fa fa-rub"></i>',
    ];

    public static $colors = [
        self::TYPE_AMOUNT => [1 => '#57B8AE', 2 => '#92CDDC'],
        self::TYPE_COUNT => [1 => '#57B8AE', 2 => '#92CDDC'],
        self::TYPE_PAYMENT => [1 => '#57B8AE', 2 => '#92CDDC'],
        self::TYPE_DEBT => [1 => '#f3565d', 2 => '#92CDDC'],
    ];

    public static $roleArray = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_SUPERVISOR,
        EmployeeRole::ROLE_SUPERVISOR_VIEWER,
        EmployeeRole::ROLE_DEMO,
    ];

    /**
     * @return array
     */
    public function setType($value)
    {
        $this->_type = in_array($value, self::$typeArray) ? $value : reset(self::$typeArray);
    }

    /**
     * @return array
     */
    public function setDate($value)
    {
        $this->_date = date_create($value) ?: date_create();
        $this->_date->modify('first day of this month');
    }

    /**
     * @return array
     */
    public function getType()
    {
        if ($this->_type === null) {
            $this->_type = reset(self::$typeArray);
        }

        return $this->_type;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        if ($this->_date === null) {
            if (!Yii::$app->request->isConsoleRequest && Yii::$app->user->identity) {
                $widgetDate = StatisticPeriod::getDateTo();
                $this->_date = ($widgetDate->format('Y-m-d') <= date('Y-m-d'))
                    ? $widgetDate
                    : date_create();
            } else {
                $this->_date = date_create();
            }
        }

        $this->_date->modify('first day of this month');

        return clone $this->_date;
    }

    /**
     * @return DateTime
     */
    public function getNextDate()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of +1 month');
    }

    /**
     * @return DateTime
     */
    public function getPrevDate()
    {
        $date = clone $this->getDate();

        return $date->modify('first day of -1 month');
    }

    /**
     * @return array
     */
    public function getEmployee()
    {
        return Yii::$app->user->identity;
    }

    /**
     * @return array
     */
    public function getCompany()
    {
        return Yii::$app->user->identity->company;
    }

    /**
     * @return array
     */
    public function getUnit()
    {
        return self::$unitArray[$this->type];
    }

    /**
     * @return array
     */
    public function getCanAll()
    {
        $role = $this->employee->currentEmployeeCompany->employee_role_id;

        return in_array($role, self::$roleArray);
    }

    /**
     * @return false|string
     */
    public function getStartDate()
    {
        $query = $this->company->getInvoices()
            ->select('document_date')
            ->andWhere([
                'is_deleted' => false,
                'type' => Documents::IO_TYPE_OUT,
            ])
            ->andWhere(['>', 'document_date', '0000-00-00'])
            ->orderBy(['document_date' => SORT_ASC]);

        if (!$this->canAll) {
            $query->andWhere(['document_author_id' => $this->employee->id]);
        }

        $firstDate = $query->scalar();
        $firstDate = $firstDate ? DateHelper::format($firstDate, 'Y-m-01', DateHelper::FORMAT_DATE) : date('Y-m-01');

        //$date = $firstDate ? date_create($firstDate) : date_create();

        return $firstDate;
    }

    /**
     * @return array
     */
    public function getDateItems()
    {
        $items = [];
        $date = $this->getStartDate();
        $currentDate = date('Y-m-01');

        do {
            $items[$date] = FlowOfFundsReportSearch::$month[DateHelper::format($date, 'm', 'Y-m-01')] . ' ' .
                DateHelper::format($date, 'Y', 'Y-m-01');
            $date = date('Y-m-01', strtotime('+1 month', strtotime($date)));
        } while ($date <= $currentDate);

        return $items;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceQuery()
    {
        $query = Invoice::find()->alias('invoice')
            ->innerJoin(['user' => EmployeeCompany::tableName()], [
                'and',
                '{{invoice}}.[[company_id]] = {{user}}.[[company_id]]',
                '{{invoice}}.[[document_author_id]] = {{user}}.[[employee_id]]',
            ])
            ->innerJoin(['employee' => Employee::tableName()], '{{user}}.[[employee_id]] = {{employee}}.[[id]]')
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'employee.is_deleted' => false,
                'user.is_working' => true,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ]);

        if (!$this->canAll) {
            $query->andWhere(['document_author_id' => $this->employee->id]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBaseQuery()
    {
        $query = $this->getInvoiceQuery()
            ->select([
                'value' => self::$selectArray[$this->type],
                'document_author_id',
            ])
            ->orderBy(['value' => SORT_DESC])
            ->groupBy('document_author_id')
            ->indexBy('document_author_id');

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentQuery()
    {
        $query = $this->getBaseQuery()
            ->andWhere([
                'between',
                'document_date',
                $this->date->format('Y-m-d'),
                $this->date->modify('last day of this month')->format('Y-m-d'),
            ]);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousQuery()
    {
        $query = $this->getBaseQuery()
            ->andWhere([
                'between',
                'document_date',
                $this->prevDate->format('Y-m-d'),
                $this->prevDate->modify('last day of this month')->format('Y-m-d'),
            ]);

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentQuery($date)
    {
        $dateFrom = $date->format('Y-m-d');
        $dateTill = $date->modify('last day of this month')->format('Y-m-d');
        $paySelect = ['link.amount', 'link.invoice_id'];
        $payWhere = [
            'and',
            ['flow.company_id' => $this->company->id],
            ['flow.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['between', 'flow.date', $dateFrom, $dateTill],
        ];
        $payQuery = new Query;
        $payQuery->select([
            'invoice_id',
            'flow_sum' => 'SUM([[amount]])',
        ])->from(['t' => CashFlowsBase::getAllFlows($paySelect, $payWhere, true, 'innerJoin')])->groupBy('t.invoice_id');

        $query = $this->getInvoiceQuery()
            ->select(['value' => self::$selectArray[$this->type], 'document_author_id',])
            ->innerJoin(['pay' => $payQuery], "{{invoice}}.[[id]] = {{pay}}.[[invoice_id]]")
            ->orderBy(['value' => SORT_DESC])
            ->groupBy('document_author_id')
            ->indexBy('document_author_id');

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentPaymentQuery()
    {
        return $this->getPaymentQuery($this->date);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousPaymentQuery()
    {
        return $this->getPaymentQuery($this->prevDate);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentData()
    {
        if ($this->_currentData === null) {
            if ($this->getType() == self::TYPE_PAYMENT) {
                $query = $this->getCurrentPaymentQuery();
            } else {
                $query = $this->getCurrentQuery();
            }

            $this->_currentData = $query->column();
        }

        return $this->_currentData;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviousData()
    {
        if ($this->_previousData === null) {
            if ($this->getType() == self::TYPE_PAYMENT) {
                $query = $this->getPreviousPaymentQuery();
            } else {
                $query = $this->getPreviousQuery();
            }

            $this->_previousData = $query->column();
        }

        return $this->_previousData;
    }

    /**
     * @return array
     */
    public function getRatingData()
    {
        $curr = $this->currentData;
        $prev = $this->previousData;

        return [
            'current' => $curr,
            'previous' => $prev,
            'max' => max($curr ? max($curr) : 0, $prev ? max($prev) : 0),
        ];
    }

    /**
     * @return string
     */
    public function getFio($id)
    {
        $user = EmployeeCompany::findOne([
            'employee_id' => $id,
            'company_id' => $this->company->id,
        ]) ?: Employee::findOne($id);

        return $user ? $user->getFio(true) : '&nbsp;';
    }

    /**
     * @return string
     */
    public function format($value)
    {
        if ($this->type == self::TYPE_COUNT) {
            return $value . '&nbsp;' . $this->unit;
        } else {
            return number_format($value / 100, 2, ',', ' ') . '&nbsp;' . $this->unit;
        }
    }

    /**
     * @return string
     */
    public function getColor1()
    {
        return self::$colors[$this->getType()][1];
    }

    /**
     * @return string
     */
    public function getColor2()
    {
        return self::$colors[$this->getType()][2];
    }
}
