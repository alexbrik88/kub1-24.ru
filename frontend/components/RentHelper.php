<?php

namespace frontend\components;

use common\components\excel\Excel;
use common\models\rent\RentAgreement;
use frontend\models\RentEntity;

class RentHelper
{

    /**
     * Экспорт аренд в XLS
     * @param RentAgreement[] $models
     * @return string
     */
    public static function xlsExport(array $models)
    {
        $excel = new Excel();
        $date = RentEntity::currentDateFrom();
        $title = 'Аренды ' . substr($date, -2) . '.' . substr($date, 5, 2) . '.' . substr($date, 0, 4);
        $date = RentEntity::currentDateTo();
        $title .= ' - ' . substr($date, -2) . '.' . substr($date, 5, 2) . '.' . substr($date, 0, 4);
        return $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => $title,
            'rangeHeader' => range('A', 'I'),
            'columns' => [
                [
                    'attribute' => 'date_begin',
                    'format' => ['date', 'php:d.m.Y']
                ],
                [
                    'attribute' => 'date_end',
                    'format' => ['date', 'php:d.m.Y']
                ],
                [
                    'attribute' => 'entity.title',
                    'value' => function (RentAgreement $rent) {
                        return $rent->entity->title;
                    },
                ],
                'entity.category.title',
                [
                    'attribute' => 'contractor.name',
                    'value' => function (RentAgreement $rent) {
                        return $rent->contractor->name;
                    },
                ],
                'attribute' => 'agreement.listItemName',
                [
                    'attribute' => 'amount',
                    'value' => function (RentAgreement $rent) {
                        return number_format($rent->amount, 0, '.', ' ') . ' руб.';
                    }
                ],
                [
                    'attribute' => 'payment',
                    'value' => function (RentAgreement $rent) {
                        $invoice = $rent->invoice;
                        return $invoice !== null && $invoice->getIsFullyPaid() ? 'Оплачен' : '';
                    }
                ],
                [
                    'attribute' => 'invoice_id',
                    'value' => function (RentAgreement $rent) {
                        if ($rent->invoice !== null) {
                            return '№' . $rent->invoice->fullNumber . ' от ' . date_format(date_create($rent->invoice->document_date), 'd.m.Y');
                        }
                        return '';
                    },
                ]
            ],
            'headers' => [
                'contractor.name' => 'Арендатор',
                'agreement.listItemName' => 'Договор',
                'payment' => 'Статус оплаты',
                'invoice_id' => 'Счёт',
            ],
            'format' => 'Excel2007',
            'fileName' => $title . '.xlsx',
        ]);
    }

}