<?php

namespace frontend\components;

use Yii;
use common\components\DadataClient;
use common\models\Contractor;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class AddModalContractorAction
 */
class AddModalContractorAction extends \yii\base\Action
{
    public $data = [];
    public $prepareModelAndData;
    public $returnData;
    public $beforeRender;
    public $headerView = '@frontend/views/contractor/form_modal/create-contractor_header';
    public $bodyView = '@frontend/views/contractor/form_modal/create-contractor_body';
    public $headerContent;
    public $bodyContent;

    /**
     * Runs the action.
     * @param $type
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run($type = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = Contractor::newModel($company, $employee);
        $model->setScenario('insert');
        $model->type = Yii::$app->request->post('type') ?: ($type ?: Contractor::TYPE_CUSTOMER);

        if (is_callable($this->prepareModelAndData)) {
            $model = call_user_func($this->prepareModelAndData, $this, $model, $company);
        }

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->director_name)) {
                $model->director_name = 'ФИО руководителя';
            }
            if ($model->save()) {
                $model->contractorAccount->save();

                return is_callable($this->returnData) ? call_user_func($this->returnData, $this, $model) : [
                    'id' => $model->id,
                    'name' => $model->name,
                    'name-short' => $model->companyType ? $model->companyType->name_short : '',
                ];
            }
        }

        if (is_callable($this->beforeRender)) {
            call_user_func($this->beforeRender, $this, $model);
        }


        if ($this->headerContent) {
            $header = is_callable($this->headerContent) ? call_user_func($this->headerContent, $this, $model) : $this->headerContent;
        } else {
            $headerView = is_callable($this->headerView) ? call_user_func($this->headerView, $this, $model) : $this->headerView;
            $header = $this->controller->renderAjax($headerView, [
                'model' => $model,
            ]);
        }

        if ($this->bodyContent) {
            $body = is_callable($this->bodyContent) ? call_user_func($this->bodyContent, $this, $model) : $this->bodyContent;
        } else {
            $bodyView = is_callable($this->bodyView) ? call_user_func($this->bodyView, $this, $model) : $this->bodyView;
            $body = $this->controller->renderAjax($bodyView, [
                'model' => $model,
                'documentType' => $model->type,
            ]);
        }

        return [
            'header' => $header,
            'body' => $body,
        ];
    }
}
