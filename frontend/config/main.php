<?php

use frontend\rbac\permissions;
use frontend\rbac\UserRole;

$params = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => 'app-frontend',
    'homeUrl' => '/',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'themes',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => require(__DIR__ . '/aliases.php'),
    'modules' => [
        'amocrm' => [
            'class' => 'frontend\modules\amocrm\Module',
            'modules' => [
                'api' => [
                    'class' => 'frontend\modules\amocrm\modules\api\Module',
                ],
                'iframe' => [
                    'class' => 'frontend\modules\amocrm\modules\iframe\Module',
                ],
            ],
        ],
        'bitrix24' => [
            'class' => 'frontend\modules\bitrix24\Module',
            'modules' => [
                'iframe' => [
                    'class' => 'frontend\modules\bitrix24\modules\iframe\Module',
                ],
            ],
        ],
        'analytics' => [
            'class' => 'frontend\modules\analytics\Module',
            'modules' => [
                'marketing' => [
                    'class' => 'frontend\modules\analytics\modules\marketing\Module',
                ],
            ],
            'as access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ["@"],
                    ],
                ],
            ],
        ],
        'documents' => [
            'class' => 'frontend\modules\documents\Module',
        ],
        'donate' => [
            'class' => 'frontend\modules\donate\Module',
        ],
        'cash' => [
            'class' => 'frontend\modules\cash\Module',
        ],
        'subscribe' => [
            'class' => 'frontend\modules\subscribe\Module',
        ],
        'export' => [
            'class' => 'frontend\modules\export\Module'
        ],
        'import' => [
            'class' => 'frontend\modules\import\Module'
        ],
        'reports' => [
            'class' => 'frontend\modules\reports\Module',
            'as access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\Reports::VIEW],
                    ],
                ],
            ],
        ],
        'retail' => [
            'class' => 'frontend\modules\retail\Module',
            'as access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ],
        'api' => [
            'class' => 'frontend\modules\api\Module',
        ],
        'tax' => [
            'class' => 'frontend\modules\tax\Module',
        ],
        'out' => [
            'class' => 'frontend\modules\out\Module',
        ],
        'mts' => [
            'class' => 'frontend\modules\mts\Module',
        ],
        'crm' => [
            'class' => 'frontend\modules\crm\Module',
            'as access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ["@"],
                    ],
                ],
            ],
        ],
        'integration' => [
            'class' => 'frontend\modules\integration\Module',
        ],
        'ofd' => [
            'class' => 'frontend\modules\ofd\Module',
            'modules' => [
                'platforma' => [
                    'class' => 'frontend\modules\ofd\modules\platforma\Module',
                ],
                'taxcom' => [
                    'class' => 'frontend\modules\ofd\modules\taxcom\Module',
                ],
            ],
        ],
        'reference' => [
            'class' => 'frontend\modules\reference\Module',
        ],
        'telegram'=> [
            'class' => 'frontend\modules\telegram\Module',
        ],
        'edm' => [
            'class' => 'frontend\modules\edm\Module',
        ],
        'acquiring' => [
            'class' => 'frontend\modules\acquiring\Module',
        ],
        'logistics' => [
            'class' => 'frontend\modules\logistics\Module',
        ],
        'cards' => [
            'class' => 'frontend\modules\cards\Module',
        ],
        'fresh' => [
            'class' => 'frontend\modules\fresh\Module',
        ],
        'urotdel' => [
            'class' => 'frontend\modules\urotdel\Module',
        ],
        'system' => [
            'class' => 'frontend\modules\system\Module',
            'as access' => [
                'class' => 'common\components\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [UserRole::ROLE_SYSADMIN],
                    ],
                ],
            ],
        ],
    ],
    'container' => require __DIR__ . '/container.php',
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'mts' => [
                    'class' => 'frontend\components\authclient\clients\Mts',
                ],
            ],
        ],
        'detect' => [
            'class' => 'Detection\MobileDetect',
        ],
        'jwt' => [
            'class' => 'sizeg\jwt\Jwt',
            // 'key'   => 'secret',
            'key'   => 'rWg7kxL767K5SlUIK5ufIfMnHdS1L8F8bYKm593icunpy7TkrbHScufH4vm9oEOV',
        ],
        'b24jwt' => [
            'class' => 'sizeg\jwt\Jwt',
            // 'key'   => 'secret',
            'key'   => 'rWg7kxL767K5SlUIK5ufIfMnHdS1L8F8bYKm593icunpy7TkrbHScufH4vm9oEOV',
        ],
        'themes' => [
            'class' => 'frontend\components\Themes',
        ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '',
            'secretV2' => '',
            'siteKeyV3' => '',
            'secretV3' => '',
        ],
        'urlManager' => [
            'rules' => require(__DIR__ . '/routes.php'),
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
            'defaultRoles' => UserRole::$defaultRoles,
        ],
        'dadataSuggestApi' => [
            'class' => 'skeeks\yii2\dadataSuggestApi\DadataSuggestApi',
            'authorization_token' => '78497656dfc90c2b00308d616feb9df60c503f51',
            'timeout' => 12,
        ],
        'multiCompanyManager' => [
            'class' => 'frontend\modules\analytics\models\AnalyticsMultiCompanyManager',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        /*'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:405',
                        'yii\web\NotFoundHttpException',
                        'yii\web\ForbiddenHttpException',
                        'yii\web\UnauthorizedHttpException',
                        'yii\web\MethodNotAllowedHttpException',
                        'yii\web\BadRequestHttpException',*/
                        'validation',
                        'robokassa',
                        'banking',
                        'api_debug',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['validation'],
                    'logFile' => '@runtime/logs/validation.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'warning'],
                    'categories' => ['robokassa'],
                    'logFile' => '@runtime/logs/robokassa.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'warning'],
                    'categories' => ['banking'],
                    'logFile' => '@runtime/logs/banking.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'warning'],
                    'categories' => ['ofd'],
                    'logFile' => '@runtime/logs/ofd.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'warning'],
                    'categories' => ['entera'],
                    'logFile' => '@runtime/logs/entera.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['api_debug'],
                    'logFile' => '@runtime/logs/api_debug.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info', 'warning', 'error'],
                    'categories' => ['analytics'],
                    'logFile' => '@runtime/logs/analytics.log',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' => 'yii\web\Request',
        'response' => 'yii\web\Response',
        'user' => 'frontend\components\WebUser',
        'session' => 'yii\web\DbSession',
    ],
    'params' => $params,
];

if (YII_ENV_PROD) {
    $config['components']['log']['targets'][] = [
        'class' => 'notamedia\sentry\SentryTarget',
        'dsn' => 'https://303f518fffb44bf1bb29bd3320b6eef1:be7df5c101d943c1b7cfbe17fa9642b1@sentry.io/1209463',
        'levels' => ['error', 'warning'],
        'except' => [
            'yii\web\HttpException:400',
            'yii\web\HttpException:401',
            'yii\web\HttpException:403',
            'yii\web\HttpException:404',
            'yii\web\HttpException:405',
            'yii\web\NotFoundHttpException',
            'yii\web\ForbiddenHttpException',
            'yii\web\UnauthorizedHttpException',
            'yii\web\MethodNotAllowedHttpException',
            'yii\web\BadRequestHttpException',
            'validation',
            'robokassa',
            'banking',
            'api_debug',
        ],
        'context' => true
    ];
}

return $config;
