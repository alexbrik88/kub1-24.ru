<?php
return [
    'maxCashSum' => 999 * 1000 * 1000, // without kopeck
    'useReCaptcha' => false,
    'email' => [
        'logo' => '@frontend/web/img/footer-logo.png',
        'logoWhite' => '@frontend/web/img/Logo_opacity_small.png',
        'link' => [
            'style' => 'font-style: italic; color: #70b0d6; text-decoration: underline;',
            'target' => '_blank',
        ],
    ],
    'apiKey' => 'l20xLi4se224nLciccSe6vNs0KuItvMy',
    'testOutInvoiceLink' => 'https://lk.kub-24.ru/out/invoice/5ae1805cc1811',
    'vkAccessToken' => '3b7354672ec1179e7505ebb6c6c58ad5cde1da7823883738f3594d779baddb9253e43998c01dd259fd264',
    'googlePlayLink' => 'https://play.google.com/store/apps/details?id=com.kub.android&?utm_source=lk.kub-24',
    /**
     * ID компаний, для которых необходим редирект на страницу просмотра счета в виджете выставления счетов
     * @var array
     */
    'outInvoiceRedirectCompanyIds' => [],

    //API requests logging
    'api-log' => false,

    // Subscribe payment tariff group visible
    'visible-tariff-group-1' => true,
    'visible-tariff-group-2' => false,
    'visible-tariff-group-3' => false,
    'visible-tariff-group-4' => false,
    'visible-tariff-group-5' => false,

    //Taxrobot always paid
    'tax-robot-paid' => false, // set TRUE fore free Taxrobot
    'tax-robot-companies' => [], // companies for which taksrobot is available
    'taxrobot-fail-email' => [
        'support@domain',
    ],

    //B2B always paid
    'b2b-paid' => false,

    //1C export enabled
    '1c-export-enabled' => false,

    //Carrotquest API keys
    'carrotquest' => [
        'app_name' => '',
        'app_id' => '',
        'api_key' => '',
        'api_secret' => '',
        'user_auth_key' => '',
    ],

    /**
     * MTS integration
     */
    'mts' => [
        'saml' => [
            'identifier' => 'db4b9487-e747-4d24-98a3-a9b2266aaf06',
            'fingerprint' => '26:6A:6D:02:81:B6:74:E3:DD:7E:35:15:3B:BB:90:42:CB:92:48:16:11:0A:91:52:D4:C1:9F:63:44:9E:7B:91',
            'algorithm' => 'sha256',
            'debug' => false,
        ],
        'rpc' => [
            'encryptionKey' => 'EF4V5eFj0buDjDe38Ynx6RKDbkzgb4VN',
        ],
        'purchaseUrl' => 'https://b2b.mts.ru/catalog/services-boxes#category=kub24&page=1',
        'tariffList' => [
            1 => 'https://b2b.mts.ru/catalog/service/870-Vistavlenie_schetov',
            3 => 'https://b2b.mts.ru/catalog/service/884-Priem_V2V_plategey',
            4 => 'https://b2b.mts.ru/catalog/service/876-Buhgalteriya_dlya_IP',
            5 => 'https://b2b.mts.ru/catalog/service/878-Finansovaya_analitika',
        ]
    ],

    'mtsFieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'checkOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'radioOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],

    'kubFieldConfig' => [
        'options' => [
            'class' => 'form-group',
        ],
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'checkOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'radioOptions' => [
            'class' => '',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],

    'mobileFieldConfig' => [
        'labelOptions' => [
            'class' => 'label',
        ],
        'wrapperOptions' => [
            'class' => 'form-filter',
        ],
        'inputOptions' => [
            'class' => 'form-control',
        ],
        'checkOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'radioOptions' => [
            'class' => 'form-group',
            'labelOptions' => [
                'class' => 'label',
            ],
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],
    'amocrm' => [
        'tokenExpire' => 36000,
        'fieldConfig' => [
            'inputOptions' => ['class' => ['widget' => 'form-control form-control-sm']],
            'labelOptions' => ['class' => ['widget' => 'label']],
            'checkOptions' => [
                'class' => ['widget' => ''],
                'labelOptions' => [
                    'class' => ['widget' => 'label']
                ],
            ],
        ],
        'datepickerConfig' => [
            'type' => 1,
            'size' => 'sm',
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true,
                'autoclose' => true,
            ],
            'options' => [
                'size' => 10,
                'class' => 'form-control-sm date-picker ico'
            ],
        ],
    ],
    'bitrix24' => [
        'tokenExpire' => 36000,
        'fieldConfig' => [
            'inputOptions' => ['class' => ['widget' => 'form-control form-control-sm']],
            'labelOptions' => ['class' => ['widget' => 'label']],
            'checkOptions' => [
                'class' => ['widget' => ''],
                'labelOptions' => [
                    'class' => ['widget' => 'label']
                ],
            ],
        ],
        'datepickerConfig' => [
            'type' => 1,
            'size' => 'sm',
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'todayHighlight' => true,
                'autoclose' => true,
            ],
            'options' => [
                'size' => 10,
                'class' => 'form-control-sm date-picker ico'
            ],
        ],
    ],

    'offerLicenseAgreementLink' => 'https://kub-24.ru/TermsOfUseAll/TermsOfUseAll.pdf',

    'isMTSbank' => isset($_SERVER['HTTP_HOST']) && strpos(strtolower($_SERVER['HTTP_HOST']), 'mtsbank') !== false,
    'isKUBbank' => isset($_SERVER['HTTP_HOST']) && strpos(strtolower($_SERVER['HTTP_HOST']), 'kubbank') !== false,
    'fsspToken' => 'MvOuhqHUayQ3',
];
