<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.10.15
 * Time: 14.58
 * Email: t.kanstantsin@gmail.com
 */

return [
    '/favicon.ico' => '/favicon/index',
    '/bill/<controller:[A-z-]+>/<view:pdf>/<uid:([A-z\d]{5}|[A-z\d]{32})>' => '/documents/<controller>/out-view',
    '/bill/<controller:[A-z-]+>/<uid:([A-z\d]{5}|[A-z\d]{32})>' => '/documents/<controller>/out-view',
    '/logistics/bill/<controller:[A-z-]+>/<uid:\d+>/<type:\d+>' => '/logistics/<controller>/out-view',
    '/bill/<controller:[A-z-]+>/<uid:obrasetc-silki-na-schet>' => '/documents/<controller>/out-view',
    '/download/<controller:invoice>/<type:pdf|docx>/<uid:[A-z\d]{5}>' => '/documents/<controller>/download',

    '/site/change-company/<id:\d+>' => '/site/change-company',

    '/contractor/collate/<step:\w+>' => '/contractor/collate',
    '/documents/scan-document/file/<id:\d+>/<name:.+>' => '/documents/scan-document/file',
    '/documents/scan-document/print/<id:\d+>/<name:.+>' => '/documents/scan-document/print',
    '/report/get-file/<rid:\d+>/<fid:\d+>/<filename:.+>' => '/report/get-file',

    '/cash/bank/<bik:(all|\d{9})>' => '/cash/bank/index',
    'cash/banking/<alias:[A-z-]+>/default/<action:registration|login>/<page:[A-z-]+>' => 'cash/banking/<alias>/default/<action>',

    'out/invoice/<id:\w{13}>' => 'out/invoice/create',
    'donate/default/<id:\w{13}>' => 'donate/default/index',

    'out/order-document/<uid:\w+>' => 'out/order-document/create',
    'documents/upload-manager/directory/<id:\d+>' => 'documents/upload-manager/directory',

    '/auth/login/<authclient:\w+>' => '/auth/login',
    '/mts/service/<id:\w+>' => '/mts/service/index',

    // Application API rules
    'api' => [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => [
            'api/act',
            'api/agreement',
            'api/cash/bank',
            'api/cash/e-money',
            'api/cash/order',
            'api/company',
            'api/contractor',
            'api/employee-company',
            'api/invoice',
            'api/invoice-facture',
            'api/scan-document',
            'api/packing-list',
            'api/payment-order',
            'api/product',
            'api/product-group',
            'api/upd',
        ],
    ],
    '/api/invoice/<id:\d+>/<action:paid|send|pdf>' => '/api/invoice/<action>',
    '/api/<controller:act|packing-list|invoice-facture|upd>/<id:\d+>/<action:pdf|send>' => '/api/<controller>/<action>',
    '/api/<controller:invoice|act|packing-list|invoice-facture|upd>/<id:\d+>/<action:png>' => '/api/<controller>/<action>',
    '/api/payment-order/<id:\d+>/<action:payment-status>' => '/api/payment-order/<action>',
    'DELETE api/invoice/<id:\d+>/paid' => 'api/invoice/unpaid',
    'PUT,PATCH api/company/change-company/<id:\d+>' => 'api/company/change-company',
    'GET api/scan-document/<id:\d+>/file' => 'api/scan-document/file',
    'GET api/api-doc/swagger.yaml' => 'api/api-doc/swagger',
    'GET api/log' => 'api/log/index',
    'PUT,PATCH api/contractor-account/<cid:\d+>/<id:\d+>' => 'api/contractor-account/update',
    'DELETE api/contractor-account/<cid:\d+>/<id:\d+>' => 'api/contractor-account/delete',
    'GET,HEAD api/contractor-account/<cid:\d+>/<id:\d+>' => 'api/contractor-account/view',
    'POST api/contractor-account/<cid:\d+>' => 'api/contractor-account/create',
    'GET,HEAD api/contractor-account/<cid:\d+>' => 'api/contractor-account/index',

    // document print route
    '<controller:\w+>/<actionType:print|pdf>/<id:\d+>/<filename:[\w\d\-_\.:]+>' => '<controller>/document-print',

    '/integration/email/<id:\d+>/<action:view|send|reply|cc|detail|attachment>' => '/integration/email/<action>',
    '/integration/email/<id:\d+>' => '/integration/email/view',
    '/integration/vk/<id:\d+>' => '/integration/vk/by-company',
    '/integration/evotor/receipt/<id:[a-z0-9-]+>' => '/integration/evotor/receipt-item',
    '/integration/insales/client/<clientId:\d+>/shipping-address' => '/integration/insales/shipping-address',
    '/integration/insales/order/<orderId:\d+>/position' => '/integration/insales/position',

    '/acquiring' => '/acquiring/acquiring/index',
    '/acquiring/<activeItem:\d+>' => '/acquiring/acquiring/index',

    '/cards' => '/cards/default/index',
];
