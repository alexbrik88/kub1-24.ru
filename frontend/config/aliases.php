<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 25.6.15
 * Time: 11.57
 */

return [
    // documents
    '@documents' => '@frontend/modules/documents',
    '@documentsUrl' => '/documents',
    // subscribe
    '@subscribe' => '@frontend/modules/subscribe',
    '@subscribeUrl' => '/subscribe',
    // banking
    '@banking' => '@frontend/modules/cash/modules/banking',
];
