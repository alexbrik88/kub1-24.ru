<?php

return [
    'singletons' => [
        'yii\web\Request' => [
            'class' => 'yii\web\Request',
            'baseUrl' => '',
            'csrfParam' => '_frontendCSRF',
            'csrfCookie' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
            'parsers' => [
                'multipart/form-data' => 'common\components\web\MultipartFormDataParser',
                'application/json' => 'yii\web\JsonParser',
            ],
            'secureProtocolHeaders' => [
                'x-scheme' => ['https'],
                'X-Forwarded-Proto' => ['https'],
            ],
        ],
        'yii\web\Response' => [
            'class' => 'yii\web\Response',
        ],
        'frontend\components\WebUser' => [
            'class' => 'frontend\components\WebUser',
            'identityClass' => 'common\models\employee\Employee',
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
            'enableAutoLogin' => false,
        ],
        'yii\web\User' => 'frontend\components\WebUser', // Alias
        'yii\web\DbSession' => [
            'class' => 'yii\web\DbSession',
            'name' => 'advanced-frontend',
            'sessionTable' => 'session_frontend',
            'timeout' => 3600*12,
            'writeCallback' => function ($session) {
                return [
                    'employee_id' => Yii::$app->user->id,
                    'last' => time(),
                ];
            },
            'cookieParams' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD,
            ],
        ],
        'yii\web\Session' => 'yii\web\DbSession', // Alias
    ],
];
