<?php
namespace frontend\rbac\rules\strictMode;

use common\models\Company;
use Yii;
use yii\rbac\Rule;

/**
 * Class StrictBase
 * @package frontend\rbac\rules\strictMode
 */
abstract class StrictBase extends Rule
{
    /**
     * @param Company $company
     * @return bool
     */
    protected function hasCompanyStrictMode(Company $company = null)
    {
        if ($company === null) {
            $company = $this->getCompany();
        }

        return $company !== null && $company->strict_mode == Company::ON_STRICT_MODE;
    }

    /**
     * @return Company|null
     */
    protected function getCompany()
    {
        return (Yii::$app->user->identity !== null
                && method_exists(Yii::$app->user->identity, 'hasProperty')
                && Yii::$app->user->identity->hasProperty('company')
            ? Yii::$app->user->identity->company : null);
    }
}