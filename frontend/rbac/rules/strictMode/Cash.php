<?php
namespace frontend\rbac\rules\strictMode;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;

class Cash extends StrictBase
{
    public $name = 'strict-mode.cash';

    public function execute($user, $item, $params)
    {
        if ($this->hasCompanyStrictMode()) {
            if (ArrayHelper::getValue($params, 'throw', true)) {
                $companyProfileLink = Html::a('Профиль компании', ['/company/index']);

                throw new ForbiddenHttpException('
                Данное действие недоступно, т. к. Ваша компания находится в режиме ограниченной функциональности.
                Чтобы получить полный доступ, вам необходимо заполнить все обязательные поля по Вашей компании в разделе "' . $companyProfileLink . '".
                ');
            }

            return false;
        }

        return true;
    }
}