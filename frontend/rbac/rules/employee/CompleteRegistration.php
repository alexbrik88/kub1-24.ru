<?php
namespace frontend\rbac\rules\employee;

use common\models\Contractor;
use common\models\employee\Employee;
use common\models\User;
use Yii;
use yii\rbac\Rule;

/**
 * Class CompleteRegistration
 * @package frontend\rbac\rules
 */
class CompleteRegistration extends Rule
{
    /**
     * @var string
     */
    public $name = 'employee.complete-registration-rule';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        /* @var Employee $userModel */
        $userModel = Yii::$app->user->identity;

        return !Yii::$app->user->isGuest && $userModel !== null && !$userModel->is_registration_completed;
    }
}