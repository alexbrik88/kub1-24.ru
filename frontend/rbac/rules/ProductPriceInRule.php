<?php
namespace frontend\rbac\rules;

use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ProductPriceInRule extends Rule
{
    public $name = 'product.price-in-view-rule';

    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) {
            return true;
        }

        if ($employeeCompany = ArrayHelper::getValue(Yii::$app->user, ['identity', 'currentEmployeeCompany'])) {
            if ($employeeCompany->can_view_price_for_buy) {
                return true;
            }
            if ($model = ($params['model'] ?? null)) {
                if ($employeeCompany->is_product_admin && $model->creator_id == $employeeCompany->employee_id) {
                    return true;
                }
            }
        }

        return false;
    }
}
