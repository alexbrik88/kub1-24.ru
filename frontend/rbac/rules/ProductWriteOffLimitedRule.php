<?php
namespace frontend\rbac\rules;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ProductWriteOffLimitedRule extends Rule
{
    public $name = 'ProductWriteOffLimitedRule';

    public function execute($user, $item, $params)
    {
        return (bool) ArrayHelper::getValue(Yii::$app->user, ['identity', 'currentEmployeeCompany', 'can_product_write_off'], false);
    }
}
