<?php
namespace frontend\rbac\rules;

use common\models\employee\EmployeeRole;
use Yii;
use yii\rbac\Rule;

class ProductAdminRule extends Rule
{
    public $name = 'productAdminRule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest && ($employeeCompany = Yii::$app->user->identity->currentEmployeeCompany)) {
            $demoEmployeeId = Yii::$app->params['service']['demo_employee_id'] ?? null;
            return $employeeCompany->is_product_admin && $employeeCompany->employee_id != $demoEmployeeId;
        }

        return false;
    }
}
