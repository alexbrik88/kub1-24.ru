<?php
namespace frontend\rbac\rules;

use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\rbac\Rule;

class EmployeeRule extends Rule
{
    public $name = 'employeeRule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest &&
            Yii::$app->user->identity->is_deleted == false &&
            Yii::$app->user->identity->company !== null &&
            ($employee = Yii::$app->user->identity->currentEmployeeCompany) !== null &&
            $employee->is_working == true &&
            isset(EmployeeRole::$roleArray[$employee->employee_role_id])
        ) {
            return $item->name === EmployeeRole::$roleArray[$employee->employee_role_id];
        }

        return false;
    }
}
