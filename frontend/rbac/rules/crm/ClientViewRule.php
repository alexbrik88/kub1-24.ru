<?php

namespace frontend\rbac\rules\crm;

use common\models\Contractor;
use common\models\employee\Employee;
use frontend\modules\crm\models\Task;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\web\User;

final class ClientViewRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public $name = __CLASS__;

    /**
     * @var int[]
     */
    protected static $id;

    /**
     * @inheritDoc
     */
    protected function canAccess(User $user, Employee $employee, array $params): bool
    {
        if ($user->can(ClientAccess::VIEW_ALL)) {
            return true;
        }

        if (self::$id === null) {
            $on = sprintf('%s.id = %s.contractor_id', Contractor::tableName(), Task::tableName());
            self::$id = Contractor::find()
                ->select(Contractor::tableName() . '.id')
                ->andWhere([Contractor::tableName() . '.company_id' => $employee->company->id])
                ->leftJoin(Task::tableName(), $on)
                ->andWhere(['or',
                    [Contractor::tableName() . '.responsible_employee_id' => $employee->id],
                    [Task::tableName() . '.employee_id' => $employee->id],
                ])
                ->column();
        }

        /** @var Contractor $contractor */
        $contractor = $params['contractor'] ?? null;

        if ($contractor && !in_array($contractor->id, self::$id)) {
            return false;
        }

        return true;
    }
}
