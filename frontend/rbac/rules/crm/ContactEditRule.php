<?php

namespace frontend\rbac\rules\crm;

use common\models\Contractor;
use common\models\employee\Employee;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\web\User;

final class ContactEditRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public $name = __CLASS__;

    /**
     * @inheritDoc
     */
    protected function canAccess(User $user, Employee $employee, array $params): bool
    {
        /** @var Contractor $contractor */
        $contractor = $params['contractor'] ?? null;

        if ($contractor) {
            if (!$contractor->is_customer && !$contractor->client) {
                return false;
            }
        }

        if ($user->can(ClientAccess::EDIT, $params)) {
            return true;
        }

        return false;
    }
}
