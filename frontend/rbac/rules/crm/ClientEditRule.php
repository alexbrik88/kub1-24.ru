<?php

namespace frontend\rbac\rules\crm;

use common\models\Contractor;
use common\models\employee\Employee;
use frontend\rbac\permissions\crm\ClientAccess;
use yii\web\User;

final class ClientEditRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public $name = __CLASS__;

    /**
     * @inheritDoc
     */
    protected function canAccess(User $user, Employee $employee, array $params): bool
    {
        if (!isset($params['contractor'])) {
            return true;
        }

        /** @var Contractor $contractor */
        $contractor = $params['contractor'] ?? null;

        if (in_array($contractor->type, [Contractor::TYPE_POTENTIAL_CLIENT, Contractor::TYPE_CUSTOMER])) {
            if ($contractor->responsible_employee_id == $employee->id) {
                return true;
            } else if ($user->can(ClientAccess::EDIT_ALL)) {
                return true;
            }
        }

        return false;
    }
}
