<?php

namespace frontend\rbac\rules\crm;

use common\models\employee\Employee;
use frontend\modules\crm\models\Deal;
use frontend\rbac\permissions\crm\DealAccess;
use yii\web\User;

final class DealEditRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public $name = __CLASS__;

    /**
     * @inheritDoc
     */
    protected function canAccess(User $user, Employee $employee, array $params): bool
    {
        if ($user->can(DealAccess::EDIT_ALL)) {
            return true;
        }

        /** @var Deal $deal */
        $deal = $params['deal'] ?? null;

        if ($deal && $deal->employee_id != $employee->id) {
            return false;
        }

        return true;
    }
}
