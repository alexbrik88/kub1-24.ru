<?php

namespace frontend\rbac\rules\crm;

use common\models\employee\Employee;
use Yii;
use yii\rbac\Rule;
use yii\web\User;

abstract class AbstractRule extends Rule
{
    /**
     * @inheritDoc
     */
    final public function execute($user, $item, $params)
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        return $this->canAccess(Yii::$app->user, $employee, $params);
    }

    /**
     * @param User $user
     * @param Employee $employee
     * @param array $params
     * @return bool
     */
    abstract protected function canAccess(User $user, Employee $employee, array $params): bool;
}
