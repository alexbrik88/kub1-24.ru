<?php

namespace frontend\rbac\rules\crm;

use common\models\employee\Employee;
use frontend\modules\crm\models\Task;
use frontend\rbac\permissions\crm\TaskAccess;
use yii\web\User;

final class TaskViewRule extends AbstractRule
{
    /**
     * @inheritDoc
     */
    public $name = __CLASS__;

    /**
     * @inheritDoc
     */
    protected function canAccess(User $user, Employee $employee, array $params): bool
    {
        if ($user->can(TaskAccess::VIEW_ALL)) {
            return true;
        }

        /** @var Task $task */
        $task = $params['task'] ?? null;

        if ($task && $task->employee_id != $employee->id) {
            return false;
        }

        return true;
    }
}
