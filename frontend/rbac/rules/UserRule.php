<?php
namespace frontend\rbac\rules;

use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\rbac\Rule;

class UserRule extends Rule
{
    public $name = 'userRule';

    public function execute($user, $item, $params)
    {
        $demoEmployeeId = Yii::$app->params['service']['demo_employee_id'] ?? null;
        if ($demoEmployeeId && $user == $demoEmployeeId) {
            return false;
        }

        if (Yii::$app->user->isGuest) {
            return $item->name === UserRole::ROLE_GUEST;
        } else {
            return $item->name === UserRole::ROLE_AUTHENTICATED;
        }
    }
}