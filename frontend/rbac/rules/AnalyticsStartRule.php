<?php
namespace frontend\rbac\rules;

use Yii;
use common\models\employee\EmployeeRole;
use yii\rbac\Rule;

class AnalyticsStartRule extends Rule
{
    public $name = 'AnalyticsStartRule';

    public function execute($user, $item, $params)
    {
        if (($employee = Yii::$app->user->identity) !== null &&
            ($company = $employee->company) !== null &&
            ($employeeCompany = $employee->currentEmployeeCompany) !== null
        ) {
            if (($company->analytics_module_activated && $employeeCompany->can_business_analytics)
                || $employeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF
            ) {
                return true;
            }
        }

        return false;
    }
}
