<?php
namespace frontend\rbac\rules\company;

use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use Yii;
use yii\rbac\Rule;

class CompanyCreate extends Rule
{
    public $name = 'company.create-rule';

    public function execute($user, $item, $params)
    {
        if ($employee = Yii::$app->user->identity) {
            return $employee->getCanAddCompany();
        }

        return false;
    }
}
