<?php
namespace frontend\rbac\rules;

use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\rbac\Rule;

class SysadminRule extends Rule
{
    public $name = 'sysadminRule';

    public function execute($user, $item, $params)
    {
        if (!empty(Yii::$app->params['service']['company_id']) &&
            ($employee = Yii::$app->user->identity) !== null &&
            $employee->is_deleted == false &&
            $employee->company !== null &&
            $employee->company->id == Yii::$app->params['service']['company_id'] &&
            ($employeeCompany = $employee->currentEmployeeCompany) !== null &&
            $employeeCompany->is_working == true
        ) {
            return $employee->employee_role_id == EmployeeRole::ROLE_CHIEF;
        }

        return false;
    }
}
