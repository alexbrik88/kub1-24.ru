<?php
namespace frontend\rbac\rules;

use Yii;
use yii\rbac\Rule;

class AnalyticsActivatedRule extends Rule
{
    public $name = 'analyticsActivatedRule';

    public function execute($user, $item, $params)
    {
        if (($employee = Yii::$app->user->identity) !== null &&
            ($company = $employee->company) !== null &&
            $company->analytics_module_activated &&
            ($employeeCompany = $employee->currentEmployeeCompany) &&
            $employeeCompany->can_business_analytics
        ) {
            return true;
        }

        return false;
    }
}
