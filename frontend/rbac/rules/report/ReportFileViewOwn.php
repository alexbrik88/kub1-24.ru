<?php
namespace frontend\rbac\rules;

use common\models\Contractor;
use Yii;
use yii\rbac\Rule;

class ReportFileViewOwn extends Rule
{
    public $name = 'report.file.view-own-rule';

    public function execute($user, $item, $params)
    {
        /* @var Contractor $model */
        $model = isset($params['model']) ? $params['model'] : null;

        return $model !== null && $model->employee_id == $user;
    }
}