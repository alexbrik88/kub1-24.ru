<?php
namespace frontend\rbac\rules\report;

use common\models\Contractor;
use common\models\report\Report;
use Yii;
use yii\rbac\Rule;

class ReportUpdateOwn extends Rule
{
    public $name = 'report.update-own';

    public function execute($user, $item, $params)
    {
        /* @var Report $model */
        $model = isset($params['model']) ? $params['model'] : null;

        return $model !== null && $model->author_id == $user;
    }
}