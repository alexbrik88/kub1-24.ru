<?php
namespace frontend\rbac\rules\contractor;

use common\models\Contractor;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ContractorAccounting extends Rule
{
    public $name = 'contractor.accounting-rule';

    public function execute($user, $item, $params)
    {
        /* @var Contractor $model */
        $model = ArrayHelper::getValue($params, 'model');

        if ($model instanceof Contractor && !$model->not_accounting) {
            return true;
        } else {
            return false;
        }
    }
}
