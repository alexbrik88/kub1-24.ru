<?php
namespace frontend\rbac\rules\contractor;

use common\models\Contractor;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ContractorOwn extends Rule
{
    public $name = 'contractor.own-rule';

    public function execute($user, $item, $params)
    {
        /* @var Contractor $model */
        $model = ArrayHelper::getValue($params, 'model');

        if (isset($user, $model->company_id, $model->responsible_employee_id) && $user == $model->responsible_employee_id) {
            return true;
        } else {
            return false;
        }
    }
}
