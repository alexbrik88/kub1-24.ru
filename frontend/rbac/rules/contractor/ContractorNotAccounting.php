<?php
namespace frontend\rbac\rules\contractor;

use common\models\Contractor;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ContractorNotAccounting extends Rule
{
    public $name = 'contractor.not-accounting-rule';

    public function execute($user, $item, $params)
    {
        /* @var Contractor $model */
        $model = ArrayHelper::getValue($params, 'model');

        if ($model instanceof Contractor && $model->not_accounting) {
            return true;
        } else {
            return false;
        }
    }
}
