<?php
namespace frontend\rbac\rules\contractor;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ContractorDelete extends Rule
{
    public $name = 'contractor.delete-rule';

    public function execute($user, $item, $params)
    {
        $id = ArrayHelper::getValue($params, ['model', 'id']);
        $serviceContractorId = ArrayHelper::getValue(Yii::$app->params, ['service', 'contractor_id']);

        return isset($id, $serviceContractorId) && $serviceContractorId != $id;
    }
}
