<?php
namespace frontend\rbac\rules\contractor;

use common\models\Contractor;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class ContractorCustomer extends Rule
{
    public $name = 'contractor.customer-rule';

    public function execute($user, $item, $params)
    {
        $type = ArrayHelper::getValue($params, 'type', ArrayHelper::getValue($params, 'model.type'));

        return $type == Contractor::TYPE_CUSTOMER;
    }
}
