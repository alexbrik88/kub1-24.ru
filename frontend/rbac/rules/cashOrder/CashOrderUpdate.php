<?php
namespace frontend\rbac\rules\cashOrder;

use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\rbac\Rule;

class CashOrderUpdate extends Rule
{
    public $name = 'cashOrder.update-rule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) {
                return true;
            } elseif (isset($params['model'])) {
                return Yii::$app->user->identity->getCashboxes()->andWhere([
                    'cashbox.id' => $params['model']->cashbox_id,
                ])->exists();
            }
        }

        return false;
    }
}
