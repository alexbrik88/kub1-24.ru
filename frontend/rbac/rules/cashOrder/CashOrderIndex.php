<?php
namespace frontend\rbac\rules\cashOrder;

use common\models\Company;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use Yii;
use yii\rbac\Rule;

class CashOrderIndex extends Rule
{
    public $name = 'cashOrder.index-rule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                Yii::$app->user->can(UserRole::ROLE_DEMO) ||
                Yii::$app->user->identity->getCashboxes()->exists()
            ) {
                return true;
            }
        }

        return false;
    }
}
