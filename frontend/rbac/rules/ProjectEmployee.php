<?php
namespace frontend\rbac\rules;

use Yii;
use common\models\project\Project;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class ProjectEmployee
 * @package frontend\rbac\rules
 */
class ProjectEmployee extends Rule
{
    public $name = 'project.employee-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = ArrayHelper::getValue($params, 'model');
            if ($model instanceof Project) {
                return in_array($user, $model->getProjectEmployees()->select('employee_id')->column());
            }
        }

        return false;
    }
}
