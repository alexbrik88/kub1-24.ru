<?php
namespace frontend\rbac\rules\document;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class DocumentAccessByContractor
 * @package frontend\rbac\rules
 */
class DocumentAuthor extends Rule
{
    public $name = 'document.responsible-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = $params['model'] ?? null;
            if ($model !== null) {
                if (isset($model->document_author_id)) {
                    $author = $model->document_author_id;
                } elseif (isset($model->invoice)) {
                    $author = $model->invoice->document_author_id;
                } elseif (isset($model->invoices)) {
                    $author = $model->invoices[0]->document_author_id;
                } else {
                    $author = null;
                }

                if ($author && $author == $user) {
                    return true;
                }
            }
        }

        return false;
    }
}
