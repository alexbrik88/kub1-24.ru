<?php
namespace frontend\rbac\rules\document;

use common\components\helpers\ArrayHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use Yii;

class DocumentCreateLimited extends DocumentAccessOwn
{
    public $name = 'document.document.create-limited-rule';

    public function execute($user, $item, $params)
    {
        $model = $this->getModel($params);
        $type = ArrayHelper::getValue($params, 'ioType');

        if ($model !== null) {
            return $model->type == Documents::IO_TYPE_IN && $model->document_author_id == $user;
        } else {
            return $type == Documents::IO_TYPE_IN;
        }
    }
}