<?php
namespace frontend\rbac\rules\document;

use Yii;
use common\models\Agreement;
use common\models\Contractor;
use yii\rbac\Rule;

/**
 * Class DocumentAccessByContractor
 * @package frontend\rbac\rules
 */
class DocumentContractorResponsible extends Rule
{
    public $name = 'document.contractor-responsible-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = $params['model'] ?? null;
            if ($model !== null) {
                if ($model instanceof Agreement && !$model->contractor_id) {
                    return true;
                }
                if (isset($model->contractor)) {
                    $contractor = $model->contractor;
                } elseif (isset($model->invoice)) {
                    $contractor = $model->invoice->contractor;
                } elseif (isset($model->invoices)) {
                    $contractor = $model->invoices[0]->contractor;
                } else {
                    $contractor = null;
                }

                if ($contractor instanceof Contractor && $contractor->responsible_employee_id == $user) {
                    return true;
                }
            }
        }

        return false;
    }
}
