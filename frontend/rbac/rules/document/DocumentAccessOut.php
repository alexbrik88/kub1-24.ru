<?php
namespace frontend\rbac\rules\document;

use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

class DocumentAccessOut extends Rule
{
    public $name = 'document.access-out-rule';

    public function execute($user, $item, $params)
    {
        return ArrayHelper::getValue($params, 'model.type', ArrayHelper::getValue($params, 'ioType')) == Documents::IO_TYPE_OUT;
    }
}
