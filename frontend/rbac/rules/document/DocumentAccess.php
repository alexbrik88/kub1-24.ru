<?php
namespace frontend\rbac\rules\document;

use Yii;
use common\models\Agreement;
use common\models\EmployeeCompany;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class DocumentAccess
 * @package frontend\rbac\rules
 */
class DocumentAccess extends Rule
{
    public $name = 'document.access-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = $params['model'] ?? null;
            $role = ArrayHelper::getValue(Yii::$app->user->identity, ['currentEmployeeCompany', 'employee_role_id']);
            $own_only = ArrayHelper::getValue(Yii::$app->user->identity, ['currentEmployeeCompany', 'document_access_own_only']);
            if (in_array($role, EmployeeCompany::$docAccessAll) || !$own_only || $model instanceof Agreement) {
                return true;
            }
            if ($model !== null && $own_only) {
                $author = $responsible = null;
                if (isset($model->document_author_id)) {
                    $author = $model->document_author_id;
                }
                if (isset($model->responsible_employee_id)) {
                    $responsible = $model->responsible_employee_id;
                } elseif (isset($model->invoice)) {
                    $responsible = $model->invoice->responsible_employee_id;
                }
                if ($author == $user || $responsible == $user) {
                    return true;
                }
            }
        }

        return false;
    }
}
