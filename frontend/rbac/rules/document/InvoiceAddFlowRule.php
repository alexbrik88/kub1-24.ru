<?php
namespace frontend\rbac\rules\document;

use common\models\EmployeeCompany;
use frontend\rbac\permissions\Cash;
use Yii;

class InvoiceAddFlowRule extends DocumentAccessOwn
{
    public $name = 'invoiceAddFlowRule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest && ($cec = Yii::$app->user->identity->currentEmployeeCompany)) {
            if (in_array($cec->employee_role_id, EmployeeCompany::$addFlowRolesNeedConfig)) {
                return (boolean) $cec->can_invoice_add_flow;
            } else {
                return true;
            }
        }

        return false;
    }
}
