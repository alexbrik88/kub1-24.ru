<?php
namespace frontend\rbac\rules\document;

use common\models\document\AbstractDocument;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeRole;
use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class DocumentAccessOwn
 * @package frontend\rbac\rules
 */
class DocumentAccessOwn extends Rule
{
    public $name = 'document.access-own-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = ArrayHelper::getValue($params, 'model');
            $author = ArrayHelper::getValue($model, 'document_author_id');

            if ($author == $user) {
                return true;
            }

            if ($model instanceof AbstractDocument) {
                $invoice = $model instanceof Invoice ? $model : (isset($model->invoice) ? $model->invoice : null);

                if ($invoice) {
                    return $invoice->document_author_id == $user || (
                        Yii::$app->user->identity->currentEmployeeCompany->employee_role_id != EmployeeRole::ROLE_ASSISTANT
                        &&
                        EmployeeCompany::find()->where([
                            'employee_id' => $invoice->document_author_id,
                            'company_id' => $invoice->company_id,
                            'employee_role_id' => EmployeeRole::ROLE_ASSISTANT
                        ])->exists()
                    );
                }
            }
        }

        return false;
    }
}
