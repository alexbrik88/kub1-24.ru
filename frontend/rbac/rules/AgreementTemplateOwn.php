<?php
namespace frontend\rbac\rules;

use Yii;
use common\models\AgreementTemplate;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class AgreementTemplateOwn
 * @package frontend\rbac\rules
 */
class AgreementTemplateOwn extends Rule
{
    public $name = 'agreement-template.own-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = ArrayHelper::getValue($params, 'model');
            if ($model instanceof AgreementTemplate) {
                return $model->created_by == $user;
            }
        }

        return false;
    }
}
