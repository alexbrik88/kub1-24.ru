<?php
namespace frontend\rbac\rules;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rbac\Rule;

/**
 * Class ProjectResponsible
 * @package frontend\rbac\rules
 */
class ProjectResponsible extends Rule
{
    public $name = 'project.responsible-rule';

    /**
     * @param int|string $user
     * @param \yii\rbac\Item $item
     * @param array $params
     * @return bool
     */
    public function execute($user, $item, $params)
    {
        if ($user) {
            $model = ArrayHelper::getValue($params, 'model');
            $author = ArrayHelper::getValue($model, 'responsible');

            if ($author == $user) {
                return true;
            }
        }

        return false;
    }
}
