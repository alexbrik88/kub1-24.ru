<?php
namespace frontend\rbac;

use yii\base\BaseObject;

class UserRole extends BaseObject
{

    const ROLE_SYSADMIN = 'sysadmin';
    const ROLE_GUEST = 'guest';
    const ROLE_AUTHENTICATED = 'authenticated';
    const ROLE_CHIEF = 'chief';
    const ROLE_ACCOUNTANT = 'accountant';
    const ROLE_MANAGER = 'manager';
    const ROLE_SALES_MANAGER = 'sales-manager';
    const ROLE_ASSISTANT = 'assistant';
    const ROLE_SUPERVISOR = 'supervisor';
    const ROLE_SALES_SUPERVISOR = 'sales-supervisor';
    const ROLE_SUPERVISOR_VIEWER = 'supervisor-viewer';
    const ROLE_SALES_SUPERVISOR_VIEWER = 'sales-supervisor-viewer';
    const ROLE_PURCHASE_SUPERVISOR = 'purchase-supervisor';
    const ROLE_PRODUCT_ADMIN = 'product-admin';
    const ROLE_EMPLOYEE = 'employee';
    const ROLE_FOUNDER = 'founder';
    const ROLE_MARKETER = 'marketer';
    const ROLE_DEMO = 'demo';
    const ROLE_FINANCE_DIRECTOR = 'finance-director';
    const ROLE_FINANCE_ADVISER = 'finance-adviser';
    const ROLE_PARTNER_RELATIONS_MANAGER = 'partner-relations-manager';

    /**
     * @var array
     */
    public static $defaultRoles = [
        self::ROLE_SYSADMIN,
        self::ROLE_GUEST,
        self::ROLE_AUTHENTICATED,
        self::ROLE_CHIEF,
        self::ROLE_ACCOUNTANT,
        self::ROLE_MANAGER,
        self::ROLE_SUPERVISOR,
        self::ROLE_SUPERVISOR_VIEWER,
        self::ROLE_SALES_MANAGER,
        self::ROLE_SALES_SUPERVISOR,
        self::ROLE_SALES_SUPERVISOR_VIEWER,
        self::ROLE_PURCHASE_SUPERVISOR,
        self::ROLE_ASSISTANT,
        self::ROLE_PRODUCT_ADMIN,
        self::ROLE_EMPLOYEE,
        self::ROLE_FOUNDER,
        self::ROLE_MARKETER,
        self::ROLE_DEMO,
        self::ROLE_FINANCE_DIRECTOR,
        self::ROLE_FINANCE_ADVISER,
        self::ROLE_PARTNER_RELATIONS_MANAGER
    ];
}
