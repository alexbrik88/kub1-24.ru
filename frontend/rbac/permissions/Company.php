<?php

namespace frontend\rbac\permissions;


/**
 * Class CompanyPermission
 * @package frontend\rbac
 */
class Company
{

    const READER = 'company.reader';

    const PROFILE = 'company.profile';
    const VISIT_CARD = 'company.visit-card';


    const MANAGER = 'company.manager';
    const ADMINISTRATOR = 'company.administrator';

    const INDEX = 'company.index';
    const CREATE = 'company.create';
    const UPDATE = 'company.update';


    /**
     * Allows access to service when strict mode is on.
     */
    const STRICT_MODE_ACCESS = 'company.strict-mode-access';

}
