<?php

namespace frontend\rbac\permissions;


/**
 * Class BusinessAnalytics
 * @package frontend\rbac
 */
class BusinessAnalytics
{
    const START = 'business-analytics.start';
    const INDEX = 'business-analytics.index';
    const ADMIN = 'business-analytics.admin';
}
