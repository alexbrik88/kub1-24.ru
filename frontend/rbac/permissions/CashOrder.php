<?php

namespace frontend\rbac\permissions;

/**
 * Class CashPermission
 * @package frontend\rbac
 */
class CashOrder
{
    const CREATOR = 'cashOrder.creator';

    const INDEX = 'cashOrder.index';
    const VIEW = 'cashOrder.view';
    const CREATE = 'cashOrder.create';
    const UPDATE = 'cashOrder.update';
    const DELETE = 'cashOrder.delete';
}
