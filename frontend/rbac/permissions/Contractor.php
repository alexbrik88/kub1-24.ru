<?php

namespace frontend\rbac\permissions;


/**
 * Class ContractorPermission
 * @package frontend\rbac
 */
class Contractor
{
    const INDEX = 'contractor.index';
    const INDEX_SELLER = 'contractor.index-seller';
    const INDEX_CUSTOMER = 'contractor.index-customer';
    const INDEX_NOT_ACCOUNTING = 'contractor.index-not-accounting';
    const INDEX_ACCOUNTING = 'contractor.index-accounting';
    const INDEX_OWN = 'contractor.index-own';
    const INDEX_STRANGERS = 'contractor.index-strangers';

    const CREATE = 'contractor.create';
    const CREATE_SELLER = 'contractor.create-seller';
    const CREATE_CUSTOMER = 'contractor.create-customer';

    const VIEW = 'contractor.view';
    const VIEW_SELLER = 'contractor.view-seller';
    const VIEW_CUSTOMER = 'contractor.view-customer';
    const VIEW_OWN = 'contractor.view-own';
    const VIEW_OWN_SELLER = 'contractor.view-own-seller';
    const VIEW_OWN_CUSTOMER = 'contractor.view-own-customer';
    const VIEW_ACCOUNTING = 'contractor.view-accounting';
    const VIEW_ACCOUNTING_SELLER = 'contractor.view-accounting-seller';
    const VIEW_ACCOUNTING_CUSTOMER = 'contractor.view-accounting-customer';

    const UPDATE = 'contractor.update';
    const UPDATE_SELLER = 'contractor.update-seller';
    const UPDATE_CUSTOMER = 'contractor.update-customer';
    const UPDATE_OWN = 'contractor.update-own';
    const UPDATE_OWN_SELLER = 'contractor.update-own-seller';
    const UPDATE_OWN_CUSTOMER = 'contractor.update-own-customer';
    const UPDATE_ACCOUNTING = 'contractor.update-accounting';
    const UPDATE_ACCOUNTING_SELLER = 'contractor.update-accounting-seller';
    const UPDATE_ACCOUNTING_CUSTOMER = 'contractor.update-accounting-customer';

    const DELETE = 'contractor.delete';
    const DELETE_SELLER = 'contractor.delete-seller';
    const DELETE_CUSTOMER = 'contractor.delete-customer';
}
