<?php

namespace frontend\rbac\permissions;


/**
 * Class Employee
 * @package frontend\rbac
 */
class Employee
{
    const CREATOR = 'employee.creator';

    const INDEX = 'employee.index';
    const CREATE = 'employee.create';
    const VIEW = 'employee.view';


    const MANAGER = 'employee.manager';

    const UPDATE = 'employee.update';


    const ADMINISTRATOR = 'employee.administrator';

    const DELETE = 'employee.delete';


    const COMPLETE_REGISTRATION = 'employee.complete-registration';
}
