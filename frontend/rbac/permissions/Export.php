<?php

namespace frontend\rbac\permissions;

/**
 * Class Export
 * @package frontend\rbac
 */
class Export
{
    const CREATOR = 'export.creator';

    const INDEX = 'export.index';
    const CREATE = 'export.create';
}
