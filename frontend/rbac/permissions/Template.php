<?php

namespace frontend\rbac\permissions;


/**
 * Class Template
 * @package frontend\rbac\permissions
 */
class Template
{
    const READER = 'template.reader';

    const INDEX = 'template.index';
}
