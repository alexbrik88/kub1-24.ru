<?php

namespace frontend\rbac\permissions;


/**
 * Class CashPermission
 * @package frontend\rbac
 */
class Subscribe
{
    const MANAGER = 'subscribe.manager';

    const INDEX = 'subscribe.index';
}
