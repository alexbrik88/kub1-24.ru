<?php

namespace frontend\rbac\permissions;


/**
 * Class ServicePermission
 * @package frontend\rbac
 */
class Service
{
    const HOME_PAGE = 'service.home-page';

    const HOME_LOGO = 'service.home-logo';

    /**
     * Выручка
     */
    const HOME_PROCEEDS = 'service.home-proceeds';
    /**
     * Затраты
     */
    const HOME_EXPENSES = 'service.home-expenses';
    /**
     * Приход
     */
    const HOME_RECEIPTS = 'service.home-receipts';

    /**
     * Прибыль/убыток ("результат")
     */
    const HOME_FLOWS = 'service.home-flows';

    /**
     * Финансирование
     */
    const HOME_FINANCES = 'service.home-finances';

    /**
     * Деньги
     */
    const HOME_CASH = 'service.home-cash';

    /**
     * Курс валют
     */
    const HOME_EXCHANGE_RATE = 'service.home-exchange-rate';

    /**
     * Налоговый календарь
     */
    const HOME_TAX_CALENDAR = 'service.home-tax-calendar';

    /**
     * Последние действия
     */
    const HOME_ACTIVITIES = 'service.home-activities';

    /**
     * Последние действия
     */
    const DEBTS_HELPER_ALL_DATA = 'service.debts-helper-all-data';
}
