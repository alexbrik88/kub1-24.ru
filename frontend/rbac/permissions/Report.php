<?php

namespace frontend\rbac\permissions;


/**
 * Class Report
 * @package frontend\rbac
 */
class Report
{
    const CREATOR = 'report.creator';

    const INDEX = 'report.index';
    const VIEW = 'report.view';
    const CREATE = 'report.create';
    const UPDATE_OWN = 'report.update-own';


    const MANAGER = 'report.manager';

    const UPDATE = 'report.update';


    const ADMINISTRATOR = 'report.administrator';

    const DELETE = 'report.delete';
}
