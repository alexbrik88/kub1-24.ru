<?php

namespace frontend\rbac\permissions;


/**
 * Class PaymentReminder
 * @package frontend\rbac
 */
class PaymentReminder
{
    const INDEX = 'payment_reminder.index';
    const SETTINGS = 'payment_reminder.settings';
    const TEMPLATE = 'payment_reminder.template';
    const REPORT = 'payment_reminder.report';
    const REPORT_OWN = 'payment_reminder.report_own';
    const REPORT_ALL = 'payment_reminder.report_all';

    const VIEWER = 'payment_reminder.viewer';
    const ADMINISTRATOR = 'payment_reminder.administrator';
}
