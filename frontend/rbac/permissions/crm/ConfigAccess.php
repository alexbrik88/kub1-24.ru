<?php

namespace frontend\rbac\permissions\crm;

final class ConfigAccess
{
    public const VIEW = 'crm.config.view';
    public const EDIT = 'crm.config.edit';
}
