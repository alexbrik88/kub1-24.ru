<?php

namespace frontend\rbac\permissions\crm;

final class ContractorCampaignAccess
{
    public const CREATE = 'crm.contractor-campaign.create';
    public const VIEW = 'crm.contractor-campaign.view';
    public const EDIT = 'crm.contractor-campaign.edit';
}
