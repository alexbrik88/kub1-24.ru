<?php

namespace frontend\rbac\permissions\crm;

final class ClientStatusAccess
{
    public const CREATE = 'crm.client-status.create';
    public const VIEW = 'crm.client-status.view';
    public const EDIT = 'crm.client-status.edit';
}
