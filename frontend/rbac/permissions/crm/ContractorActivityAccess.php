<?php

namespace frontend\rbac\permissions\crm;

final class ContractorActivityAccess
{
    public const CREATE = 'crm.contractor-activity.create';
    public const VIEW = 'crm.contractor-activity.view';
    public const EDIT = 'crm.contractor-activity.edit';
}
