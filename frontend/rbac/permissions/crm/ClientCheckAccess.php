<?php

namespace frontend\rbac\permissions\crm;

final class ClientCheckAccess
{
    public const CREATE = 'crm.client-check.create';
    public const VIEW = 'crm.client-check.view';
    public const EDIT = 'crm.client-check.edit';
}
