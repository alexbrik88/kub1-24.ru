<?php

namespace frontend\rbac\permissions\crm;

final class TaskAccess
{
    public const INDEX = 'crm.task.index';
    public const VIEW = 'crm.task.view';
    public const VIEW_ALL = 'crm.task.view-all';
    public const CREATE = 'crm.task.create';
    public const EDIT = 'crm.task.edit';
    public const EDIT_ALL = 'crm.task.edit-all';
    public const DELETE = 'crm.task.delete';
}
