<?php

namespace frontend\rbac\permissions\crm;

final class ClientAccess
{
    public const INDEX = 'crm.client.index';
    public const VIEW = 'crm.client.view';
    public const VIEW_ALL = 'crm.client.view-all';
    public const EDIT = 'crm.client.edit';
    public const EDIT_ALL = 'crm.client.edit-all';
}
