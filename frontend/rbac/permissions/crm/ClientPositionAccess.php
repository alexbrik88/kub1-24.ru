<?php

namespace frontend\rbac\permissions\crm;

final class ClientPositionAccess
{
    public const CREATE = 'crm.client-position.create';
    public const VIEW = 'crm.client-position.view';
    public const EDIT = 'crm.client-position.edit';
}
