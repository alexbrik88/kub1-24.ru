<?php

namespace frontend\rbac\permissions\crm;

final class ClientReasonAccess
{
    public const CREATE = 'crm.client-reason.create';
    public const VIEW = 'crm.client-reason.view';
    public const EDIT = 'crm.client-reason.edit';
}
