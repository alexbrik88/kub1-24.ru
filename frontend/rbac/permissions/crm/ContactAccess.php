<?php

namespace frontend\rbac\permissions\crm;

final class ContactAccess
{
    public const VIEW = 'crm.contact.view';
    public const EDIT = 'crm.contact.edit';
}
