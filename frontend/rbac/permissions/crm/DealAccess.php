<?php

namespace frontend\rbac\permissions\crm;

final class DealAccess
{
    public const INDEX = 'crm.deal.index';
    public const VIEW = 'crm.deal.view';
    public const VIEW_ALL = 'crm.deal.view-all';
    public const CREATE = 'crm.deal.create';
    public const EDIT = 'crm.deal.edit';
    public const EDIT_ALL = 'crm.deal.edit-all';
}
