<?php

namespace frontend\rbac\permissions\crm;

final class DealTypeAccess
{
    public const CREATE = 'crm.deal-type.create';
    public const VIEW = 'crm.deal-type.view';
    public const EDIT = 'crm.deal-type.edit';
}
