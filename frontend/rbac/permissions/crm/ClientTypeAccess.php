<?php

namespace frontend\rbac\permissions\crm;

final class ClientTypeAccess
{
    public const CREATE = 'crm.client-type.create';
    public const VIEW = 'crm.client-type.view';
    public const EDIT = 'crm.client-type.edit';
}
