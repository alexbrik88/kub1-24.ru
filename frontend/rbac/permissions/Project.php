<?php

namespace frontend\rbac\permissions;


/**
 * Class  Project
 * @package frontend\rbac
 */
class  Project
{
    const CREATE = 'project.create';
    const INDEX = 'project.index';
    const INDEX_ALL = 'project.index_all';
    const INDEX_OWN = 'project.index_own';
    const VIEW = 'project.view';
    const VIEW_OWN = 'project.view_own';
    const VIEW_EMPLOYEE = 'project.view_employee';
    const UPDATE = 'project.update';
    const UPDATE_OWN = 'project.update_own';
    const DELETE = 'project.delete';

    const VIEWER = 'project.viewer';
    const MANAGER = 'project.manager';
    const ADMINISTRATOR = 'project.administrator';
}
