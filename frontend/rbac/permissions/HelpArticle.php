<?php

namespace frontend\rbac\permissions;


/**
 * Class Template
 * @package frontend\rbac\permissions
 */
class HelpArticle
{
    const READER = 'help-article.reader';

    const INDEX = 'help-article.index';
    const VIEW = 'help-article.view';
}
