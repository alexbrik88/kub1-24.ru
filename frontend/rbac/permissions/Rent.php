<?php

namespace frontend\rbac\permissions;


/**
 * Class Rent
 * @package frontend\rbac
 */
class Rent
{
    const VIEWER = 'rent.viewer';

    const INDEX = 'rent.index';
    const VIEW = 'rent.view';


    const MANAGER = 'rent.manager';

    const CREATE = 'rent.create';
    const UPDATE = 'rent.update';


    const ADMINISTRATOR = 'rent.administrator';

    const DELETE = 'rent.delete';
}
