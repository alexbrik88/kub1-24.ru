<?php

namespace frontend\rbac\permissions;

use frontend\rbac\UserRole;

/**
 * Class ProductPermission
 * @package frontend\rbac
 */
class Product
{
    public static $writeOffRoles = [
        UserRole::ROLE_CHIEF,
        UserRole::ROLE_SUPERVISOR,
        UserRole::ROLE_ACCOUNTANT,
        UserRole::ROLE_FINANCE_DIRECTOR,
        UserRole::ROLE_FINANCE_ADVISER,
    ];

    public static $writeOffLimitedRoles = [
        UserRole::ROLE_MANAGER,
    ];

    const VIEWER = 'product.viewer';

    const INDEX = 'product.index';
    const VIEW = 'product.view';
    const PRICE_IN_VIEW = 'product.price-in-view';
    const WRITE_OFF = 'product.write-off';
    const WRITE_OFF_LIMITED = 'product.write-off-limited';


    const MANAGER = 'product.manager';

    const CREATE = 'product.create';
    const UPDATE = 'product.update';
    const DELETE = 'product.delete';
}
