<?php

namespace frontend\rbac\permissions\document;


/**
 * Class Invoice
 * @package frontend\rbac\permissions\document
 */
class Invoice
{
    const CREATE = 'document.invoice.create';
    const CREATE_IN = 'document.invoice.create-in';
    const CREATE_OUT = 'document.invoice.create-out';


    const MANAGER = 'document.invoice.manager';
    const VIEW = 'document.invoice.manager';
    const ADD_CASH_FLOW = 'document.invoice.add-cash-flow';

    const ADMINISTRATOR = 'document.invoice.administrator';
}
