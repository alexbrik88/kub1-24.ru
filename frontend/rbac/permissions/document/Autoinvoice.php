<?php

namespace frontend\rbac\permissions\document;


/**
 * Class Invoice
 * @package frontend\rbac\permissions\document
 */
class Autoinvoice
{
    const MANAGER = 'document.auto-invoice.manager';

    const INDEX = 'document.auto-invoice.index';
    const VIEW = 'document.auto-invoice.view';
    const STOP = 'document.auto-invoice.stop';
    const START = 'document.auto-invoice.start';
    const CREATE = 'document.auto-invoice.create';
    const UPDATE = 'document.auto-invoice.update';

    const ADMINISTRATOR = 'document.auto-invoice.administrator';

    const DELETE = 'document.auto-invoice.delete';
}
