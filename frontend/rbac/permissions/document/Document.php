<?php

namespace frontend\rbac\permissions\document;


/**
 * Class Document
 * @package frontend\rbac\permissions\document
 */
class Document
{
    const SOME_ACCESS = 'document.some_access';
    const DOCUMENTS = 'document.documents';
    const DOCUMENTS_IN = 'document.documents-in';
    const DOCUMENTS_OUT = 'document.documents-out';


    const LIMITED_CREATOR = 'document.document.limited-creator';
    const LIMITED_CREATOR_IN = 'document.document.limited-creator-in';
    const LIMITED_CREATOR_OUT = 'document.document.limited-creator-out';

    const CREATE_LIMITED = 'document.document.create-limited';

    const CREATOR = 'document.document.creator';
    const CREATOR_IN = 'document.document.creator-in';
    const CREATOR_OUT = 'document.document.creator-out';

    const INDEX = 'document.document.index';
    const INDEX_IN = 'document.document.index-in';
    const INDEX_OUT = 'document.document.index-out';
    const INDEX_OWN = 'document.document.index-own';
    const CREATE_OWN = 'document.document.create-own';
    const VIEW_OWN = 'document.document.view-own';
    const UPDATE_OWN = 'document.document.update-own';
    const UPDATE_STATUS_OWN = 'document.document.update-status-own';
    const FILE_UPLOAD_OWN = 'document.document.file-upload-own';
    const FILE_DELETE_OWN = 'document.document.file-delete-own';

    const LIMITED_MANAGER = 'document.document.limited-manager';
    const LIMITED_MANAGER_IN = 'document.document.limited-manager-in';
    const LIMITED_MANAGER_OUT = 'document.document.limited-manager-out';

    const UPDATE_STATUS = 'document.document.update-status';
    const UPDATE_STATUS_IN = 'document.document.update-status-in';
    const UPDATE_STATUS_OUT = 'document.document.update-status-out';
    const VIEW = 'document.document.view';
    const VIEW_IN = 'document.document.view-in';
    const VIEW_OUT = 'document.document.view-out';
    const INDEX_ALL = 'document.document.index_all';
    const INDEX_STRANGERS = 'document.document.index_strangers';

    const MANAGER = 'document.document.manager';
    const MANAGER_IN = 'document.document.manager-in';
    const MANAGER_OUT = 'document.document.manager-out';

    const CREATE = 'document.document.create';
    const CREATE_IN = 'document.document.create-in';
    const CREATE_OUT = 'document.document.create-out';
    const UPDATE = 'document.document.update';
    const UPDATE_IN = 'document.document.update-in';
    const UPDATE_OUT = 'document.document.update-out';
    const FILE_UPLOAD = 'document.document.file-upload';
    const FILE_DELETE = 'document.document.file-delete';


    const ADMINISTRATOR = 'document.document.administrator';
    const ADMINISTRATOR_IN = 'document.document.administrator-in';
    const ADMINISTRATOR_OUT = 'document.document.administrator-out';

    const DELETE = 'document.document.delete';
    const DELETE_IN = 'document.document.delete-in';
    const DELETE_OUT = 'document.document.delete-out';
    const DELETE_OWN = 'document.document.delete-own';


    const STRICT_MODE = 'document.document.strict-mode';
    const STRICT_MODE_ACCESS = 'document.document.strict-mode-access';


    //
    const FILE_UPLOAD_OUT = 'document.file-upload-out';
    const VIEW_CONTRACTOR_RESPONSIBLE = 'document.view-contractor-responsible';
    const UPDATE_CONTRACTOR_RESPONSIBLE = 'document.update-contractor-responsible';
    const FILE_UPLOAD_CONTRACTOR_RESPONSIBLE = 'document.file-upload-contractor-responsible';
    const UPDATE_STATUS_CONTRACTOR_RESPONSIBLE = 'document.update-status-contractor-responsible';
    const VIEW_AUTHOR_CONTRACTOR_RESPONSIBLE = 'document.view-author-contractor-responsible';
    const UPDATE_AUTHOR_CONTRACTOR_RESPONSIBLE = 'document.update-author-contractor-responsible';
    const FILE_UPLOAD_AUTHOR_CONTRACTOR_RESPONSIBLE = 'document.file-upload-author-contractor-responsible';
    const UPDATE_STATUS_AUTHOR_CONTRACTOR_RESPONSIBLE = 'document.update-status-author-contractor-responsible';
    const VIEW_ALL_CONTRACTOR_RESPONSIBLE = 'document.view-all-contractor-responsible';
    const UPDATE_ALL_CONTRACTOR_RESPONSIBLE = 'document.update-all-contractor-responsible';
    const FILE_UPLOAD_ALL_CONTRACTOR_RESPONSIBLE = 'document.file-upload-all-contractor-responsible';
    const UPDATE_STATUS_ALL_CONTRACTOR_RESPONSIBLE = 'document.update-status-all-contractor-responsible';
    const VIEW_CONTRACTOR_RESPONSIBLE_OUT = 'document.view-contractor-responsible-out';
    const UPDATE_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-contractor-responsible-out';
    const FILE_UPLOAD_CONTRACTOR_RESPONSIBLE_OUT = 'document.file-upload-contractor-responsible-out';
    const UPDATE_STATUS_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-status-contractor-responsible-out';
    const VIEW_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT = 'document.view-author-contractor-responsible-out';
    const UPDATE_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-author-contractor-responsible-out';
    const FILE_UPLOAD_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT = 'document.file-upload-author-contractor-responsible-out';
    const UPDATE_STATUS_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-status-author-contractor-responsible-out';
    const VIEW_ALL_CONTRACTOR_RESPONSIBLE_OUT = 'document.view-all-contractor-responsible-out';
    const UPDATE_ALL_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-all-contractor-responsible-out';
    const FILE_UPLOAD_ALL_CONTRACTOR_RESPONSIBLE_OUT = 'document.file-upload-all-contractor-responsible-out';
    const UPDATE_STATUS_ALL_CONTRACTOR_RESPONSIBLE_OUT = 'document.update-status-all-contractor-responsible-out';
    //
}
