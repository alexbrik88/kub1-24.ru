<?php

namespace frontend\rbac\permissions\document;


/**
 * Class PaymentOrder
 * @package frontend\rbac\permissions\document
 */
class PaymentOrder
{
    const CREATOR = 'document.payment-order.creator';

    const INDEX = 'document.payment-order.index';
    const CREATE = 'document.payment-order.create';
    const VIEW = 'document.payment-order.view';


    const MANAGER = 'document.payment-order.manager';

    const UPDATE = 'document.payment-order.update';


    const ADMINISTRATOR = 'document.payment-order.administrator';

    const DELETE = 'document.payment-order.delete';


    const STRICT_MODE_ACCESS = 'document.payment-order.strict-mode-access';
}
