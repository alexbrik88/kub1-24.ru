<?php

namespace frontend\rbac\permissions\document;


/**
 * Class Collate
 * @package frontend\rbac\permissions\document
 */
class Collate
{
    const CREATE = 'document.collate.create';
}
