<?php

namespace frontend\rbac\permissions;


/**
 * Class UserPermission
 * @package frontend\rbac
 */
class User
{
    const SIGNUP = 'user.signup';
    const LOGIN = 'user.login';
    const RESET_PASSWORD = 'user.reset-password';

    const LOGOUT = 'user.logout';
    const PROFILE_EDIT = 'user.profile-edit';
}
