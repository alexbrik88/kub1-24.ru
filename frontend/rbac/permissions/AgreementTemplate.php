<?php

namespace frontend\rbac\permissions;


/**
 * Class AgreementTemplate
 * @package frontend\rbac\permissions
 */
class AgreementTemplate
{
    const INDEX = 'agreement-template.index';
    const INDEX_IN = 'agreement-template.index_in';
    const INDEX_OUT = 'agreement-template.index_out';

    const VIEW = 'agreement-template.view';
    const VIEW_IN = 'agreement-template.view_in';
    const VIEW_OUT = 'agreement-template.view_out';

    const CREATE = 'agreement-template.create';
    const CREATE_IN = 'agreement-template.create_in';
    const CREATE_OUT = 'agreement-template.create_out';

    const UPDATE = 'agreement-template.update';
    const UPDATE_IN = 'agreement-template.update_in';
    const UPDATE_OUT = 'agreement-template.update_out';
    const UPDATE_OWN = 'agreement-template.update_own';
    const UPDATE_IN_OWN = 'agreement-template.update_in_own';
    const UPDATE_OUT_OWN = 'agreement-template.update_out_own';

    const DELETE = 'agreement-template.delete';
    const DELETE_IN = 'agreement-template.delete_in';
    const DELETE_OUT = 'agreement-template.delete_out';
    const DELETE_OWN = 'agreement-template.delete_own';
    const DELETE_IN_OWN = 'agreement-template.delete_in_own';
    const DELETE_OUT_OWN = 'agreement-template.delete_out_own';
}
