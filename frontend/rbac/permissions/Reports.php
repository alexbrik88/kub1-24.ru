<?php

namespace frontend\rbac\permissions;


/**
 * Class Reports
 * @package frontend\rbac\permissions
 */
class Reports
{
    const VIEW = 'reports.view';
    const FINANCE = 'reports.finance';
    const REPORTS = 'reports.reports';
    const REPORTS_CLIENTS = 'reports.reports-clients';
    const REPORTS_INVOICES = 'reports.reports-invoices';
    const REPORTS_SUPPLIERS = 'reports.reports-suppliers';
    const REPORTS_EMPLOYEES = 'reports.reports-employees';
    const REPORTS_SALES = 'reports.reports-sales';

    const VIEWER_REPORTS_C_I = 'reports.viewer-reports-c-i';
    const VIEWER_REPORTS_C_I_S = 'reports.viewer-reports-c-i-s';
    const VIEWER_REPORTS_ALL = 'reports.viewer-reports-all';
    const VIEWER_ALL = 'reports.viewer-all';
}
