<?php

namespace frontend\rbac\permissions;


/**
 * Class CashPermission
 * @package frontend\rbac
 */
class Cash
{
    const CREATOR = 'cash.creator';

    const INDEX = 'cash.index';
    const VIEW = 'cash.view';
    const CREATE = 'cash.create';


    const MANAGER = 'cash.manager';

    const UPDATE = 'cash.update';


    const ADMINISTRATOR = 'cash.administrator';

    const DELETE = 'cash.delete';

    const STRICT_MODE = 'cash.strict-mode';

    const LINKAGE = 'cash.linkage';
}
