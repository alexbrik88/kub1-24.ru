<?php

use frontend\models\AuthSignupForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuthSignupForm */

$this->title = 'Войти';

switch ($model->scenario) {
    case AuthSignupForm::SCENARIO_LOGIN:
        $view = '_login';
        break;
    case AuthSignupForm::SCENARIO_CREATE:
        $view = '_create';
        break;

    default:
        $view = '_email';
        break;
}
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-5">
        <?php Pjax::begin([
            'id' => 'auth-signup-pjax',
            'formSelector' => '#auth-signup-form',
            'linkSelector' => false,
        ]) ?>

        <?php $form = ActiveForm::begin([
            'id' => 'auth-signup-form',
            'enableClientValidation' => false,
        ]); ?>

        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

        <?= Html::hiddenInput('scenario', $model->scenario) ?>

        <?= $this->render($view, [
            'form' => $form,
            'model' => $model,
        ]) ?>

        <?php ActiveForm::end(); ?>

        <?php Pjax::end() ?>
    </div>
</div>
