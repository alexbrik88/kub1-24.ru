<?php

use common\models\company\CompanyType;
use frontend\models\AuthSignupForm;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\AuthSignupForm */

$typeArray = CompanyType::find()->andWhere(['id' => AuthSignupForm::$typeIds])->all();
?>

    <?= $form->field($model, 'email')->textInput(); ?>

    <div>
        <?= $form->field($model, 'companyType', [
            'options' => [
                'class' => 'company-type-chooser',
            ],
        ])->radioList(ArrayHelper::map($typeArray, 'id', 'name')); ?>
        <div class="company-type-block-non-ip">
        </div>
        <div class="company-type-block-ip">
        </div>
    </div>

    <?= $form->field($model, 'taxationTypeOsno', [
        'parts' => [
            '{input}' => Html::tag(
                'div',
                Html::activeCheckbox($model, 'taxationTypeOsno', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypeUsn', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypeEnvd', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypePsn', ['disabled' => true])
            )
        ]
    ])->label('Система налогобложения')->render(); ?>

    <?= $form->field($model, 'checkrules')->checkbox()->label('Принимаю условия
        <a href="#" target="_blank">лицензионного соглашения</a>'); ?>

    <?= Html::submitButton('Попробовать бесплатно'); ?>
</div>
