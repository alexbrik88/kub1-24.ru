<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\models\AuthSignupForm */

?>

<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'password')->passwordInput() ?>
<?= $form->field($model, 'rememberMe')->checkbox() ?>
<div style="color:#999;margin:1em 0">
    Если Вы забыли пароль, то Вы можете
    <?= Html::a('сбросить его', ['site/request-password-reset'], [
        'target' => '_blank',
    ]) ?>.
</div>
<div class="form-group">
    <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
</div>
