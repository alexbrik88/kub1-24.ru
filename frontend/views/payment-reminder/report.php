<?php

use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PaymentReminderReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по отправленным письмам';

?>

<?= $this->render('_menu', ['currentTab' => 'report']) ?>

<div class="report-index">
    <div class="row mb-4">
        <div class="col-sm-12">
            <div class="pull-right" style="display: inline-block; width: 190px;">
                <?= frontend\widgets\RangeButtonWidget::widget([
                    'cssClass' => 'btn default btn-calendar doc-gray-button btn_row btn_select_days',
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet box darkblue blk_wth_srch">
        <div class="portlet-title row-fluid">
            <div class="caption list_recip col-sm-12">
                <?= Html::encode($this->title) ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'emptyText' => 'У вас пока нет отправленных писем',
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered dataTable',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'nav-pagination list-clr',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        'date:date',
                        [
                            'attribute' => 'contractor_id',
                            'value' => 'contractor.nameWithType',
                            'class' => DropDownSearchDataColumn::className(),
                            'filter' => $searchModel->getContractorFilterItems(),
                        ],
                        [
                            'attribute' => 'debt_amount',
                            'value' => function ($model) {
                                return TextHelper::invoiceMoneyFormat($model->debt_amount, 2);
                            }
                        ],
                        'invoice:ntext',
                        'days_overdue',
                        [
                            'attribute' => 'is_sent',
                            'format' => 'html',
                            'class' => DropDownDataColumn::className(),
                            'filter' => $searchModel->getIsSentFilterItems(),
                            'value' => function ($model) {
                                return Html::tag('span', $model->getIsSentText(), [
                                    'class' => $model->is_sent ? '' : 'text-danger',
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'category_id',
                            'class' => DropDownDataColumn::className(),
                            'filter' => $searchModel->getCategoryFilterItems(),
                            'value' => 'category.name',
                        ],
                        'event:html',
                        'subject',
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
