<?php

use common\components\helpers\Html;
use yii\helpers\Url;
use common\models\employee\Employee;
use frontend\models\PaymentReminderMessageSearch;
use yii\data\ActiveDataProvider;
use common\components\grid\GridView;
use common\models\paymentReminder\PaymentReminderMessage;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\grid\ActionColumn;
use common\components\grid\DropDownDataColumn;
use frontend\models\PaymentReminderMessageContractorSearch;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use frontend\widgets\TableConfigWidget;
use common\components\debts\DebtsHelper;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\components\FilterHelper;
use frontend\models\Documents;
use yii\widgets\Pjax;
use common\models\employee\EmployeeRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\widgets\ActiveForm;
use common\components\date\DateHelper;

/* @var \yii\web\View $this
 * @var $user                                         Employee
 * @var $paymentReminderMessageSearchModel            PaymentReminderMessageSearch
 * @var $paymentReminderMessageDataProvider           ActiveDataProvider
 * @var $paymentReminderMessageContractorSearchModel  PaymentReminderMessageContractorSearch
 * @var $paymentReminderMessageContractorDataProvider ActiveDataProvider
 * @var $paymentReminderMessage                       PaymentReminderMessage
 */

$this->title = 'Шаблоны писем должникам';

?>

<style type="text/css">
.mx_-7 {
    margin-left: -7px !important;
    margin-right: -7px !important;
}
.px_7 {
    padding-left: 7px !important;
    padding-right: 7px !important;
}
.pt_7 {
    padding-top: 7px !important;
}
.pb_7 {
    padding-bottom: 7px !important;
}
.turn_on-next-arrow {
    transform: scaleY(-1);
    position: absolute;
    bottom: 0;
    left: -22%;
    width: 43%;
}
.border {
    border: 1px solid #dee2e6 !important;
}
.h-100 {
    height: 100%;
}
.text-title {
    color: #4679AE;
}
</style>

<div class="debt-report-content container-fluid payment-reminder-block" style="padding: 0; margin-top: -10px;">
    <?= $this->render('_menu', ['currentTab' => 'index']) ?>

    <div class="wrap">
        <h3 class="page-title mb-3"><?= $this->title; ?></h3>
        <div class="payment-reminder-index">
            <div class="fx-row align-items-stretch mx_-7">
                <div class="page-description col-9 px_7">
                    <div class="border rounded" style="position: relative; height: 100%;">
                        <div style="
                            position: absolute;
                            top: 20px;
                            left: 0;
                            bottom: 0;
                            width: 57%;
                            background-image: url('/img/reminder/group.png');
                            background-repeat: no-repeat;
                            background-position: left bottom;
                            background-size: contain;
                        ">
                        </div>
                        <div class="fx-row">
                            <div class="col"></div>
                            <div class="col">
                                <div style="margin: 20px 0; line-height: 1.8;">
                                    <strong>
                                        Надоело звонить клиентам и напоминать о долгах?
                                        Поручите дебиторку КУБу.
                                    </strong>
                                    <div>
                                        В случае просрочки оплаты счета, КУБ запустит автоматическую серию писем:
                                        от вежливых напоминаний об оплате до “последнего китайского предупреждения”.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-banner col-3 px_7">
                    <div style="background-color: #f2f2f7; font-size: 12px; line-height: 1.8; padding: 10px 15px; height: 100%;">
                        <div>
                            <?= Html::img('/img/reminder/is_important.svg', [
                                'alt' => '',
                            ]) ?>
                        </div>
                        <div style="padding-top: 10px;">
                            <b>Важно:</b>
                            <?php if ($user->company->isFreeTariff): ?>
                                на БЕСПЛАТНОМ тарифе вы можете включить только шаблон письма №1.
                                Что бы использовать все шаблоны писем автоматической рассылки,
                                <?= Html::a('оплатите сервис', Url::to(['/subscribe'])); ?>.
                            <?php else: ?>
                                Если у вас закончится платный тариф, то все письма отключаться, кроме письма №1.
                                Что бы работа с должниками не прекращалась,
                                <?= Html::a('оплатите сервис на несколько месяцев', Url::to(['/subscribe'])); ?>.
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="mt-5">
                Ваши выгоды
            </h4>
            <div class="fx-row align-items-stretch mt-3 mx_-7">
                <div class="col px_7">
                    <div class="d-flex align-items-center border rounded-sm h-100 p-3">
                        <?= Html::img('/img/reminder/control.svg', [
                            'alt' => '',
                            'class' => 'mr-3',
                        ]) ?>
                        <div>
                            <span class="text-primary">Контроль должников на автопилоте</span>
                        </div>
                    </div>
                </div>
                <div class="col px_7">
                    <div class="d-flex align-items-center border rounded-sm h-100 p-3">
                        <?= Html::img('/img/reminder/good.svg', [
                            'alt' => '',
                            'class' => 'mr-3',
                        ]) ?>
                        <div>
                            <span class="text-primary">Уменьшение дебиторки</span>
                            <div class="text-muted" style="font-size: 12px;">
                                долгов клиентов перед вашей компанией
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col px_7">
                    <div class="d-flex align-items-center border rounded-sm h-100 p-3">
                        <?= Html::img('/img/reminder/calendar.svg', [
                            'alt' => '',
                            'class' => 'mr-3',
                        ]) ?>
                        <div>
                            <span class="text-primary">Повышение дисциплины клиентов по оплате ваших счетов</span>
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="mt-5">
                Типы писем
            </h4>
            <div class="fx-row align-items-stretch mt-3 mx_-7">
                <div class="col-8 px_7">
                    <div class="fx-row align-items-stretch flex-wrap mx_-7">
                        <div class="col-6 px_7 pb_7">
                            <div class="border rounded-sm h-100 p-3">
                                <div class="d-flex align-items-center" style="margin-bottom: 10px;">
                                    <?= Html::img('/img/reminder/letters_1.svg', [
                                        'alt' => '',
                                        'class' => 'mr-3',
                                    ]) ?>
                                    <div class="text-primary">
                                        По каждому не оплаченному счету в отдельности
                                    </div>
                                </div>
                                <div class="text-muted" style="font-size: 14px; line-height: 1.4;">
                                    Если счет не оплачен вовремя, то будут отправляться письма напоминания.
                                    Какие письма отправлять, вы можете выбирать сами.
                                    Текст в письме можно исправить.
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px_7 pb_7">
                            <div class="border rounded-sm h-100 p-3">
                                <div class="d-flex align-items-center" style="margin-bottom: 10px;">
                                    <?= Html::img('/img/reminder/letters_3.svg', [
                                        'alt' => '',
                                        'class' => 'mr-3',
                                    ]) ?>
                                    <div class="text-primary">
                                        По общей <br> сумме долга
                                    </div>
                                </div>
                                <div class="text-muted" style="font-size: 14px; line-height: 1.4;">
                                    Если сумма долга превысила допустимый лимит, то будут отправляться письма напоминания.
                                    Лимит по долгу вы устанавливаете самостоятельно.
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px_7 pt_7">
                            <div class="border rounded-sm h-100 p-3">
                                <div class="d-flex align-items-center" style="margin-bottom: 10px;">
                                    <?= Html::img('/img/reminder/letters_2.svg', [
                                        'alt' => '',
                                        'class' => 'mr-3',
                                    ]) ?>
                                    <div class="text-primary">
                                        Счет оплачен <br> вовремя
                                    </div>
                                </div>
                                <div class="text-muted" style="font-size: 14px; line-height: 1.4;">
                                    Если клиент платит вовремя, то будет полезно поблагодарить такого клиента.
                                    После оплаты счета, если счет не просрочен, клиенту придет письмо с благодарностью об оплате.
                                </div>
                            </div>
                        </div>
                        <div class="col-6 px_7 pt_7">
                            <div class="border rounded-sm h-100 p-3">
                                <div class="d-flex align-items-center" style="margin-bottom: 10px;">
                                    <?= Html::img('/img/reminder/letters_4.svg', [
                                        'alt' => '',
                                        'class' => 'mr-3',
                                    ]) ?>
                                    <div class="text-primary">
                                        Все типы писем одновременно
                                    </div>
                                </div>
                                <div class="text-muted" style="font-size: 14px; line-height: 1.4;">
                                    Реагируйте на любую просрочку оплаты счета и на превышение лимита долга,
                                    не забывайте благодарить клиентов за своевременную оплату.
                                    Это дисциплинирует клиентов платить вовремя.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4 px_7">
                    <div class="border rounded-sm h-100 pt-3">
                        <div class="h-100" style="
                            background-image: url('/img/reminder/letters.png');
                            background-repeat: no-repeat;
                            background-position: center bottom;
                            background-size: contain;
                        "></div>
                    </div>
                </div>
            </div>
            <h4 class="mt-5">
                Как включить <strong>автосбор долгов</strong>
            </h4>
            <div class="fx-row align-items-stretch mx_-7 mt-3 mb-5">
                <div class="col-3 px_7">
                    <div class="border rounded-sm h-100 d-flex flex-column justify-content-between">
                        <div class="mt-3 mx-3 text-primary">
                            <div style="font-size: 22px;">
                                <strong>
                                    Выберите
                                </strong>
                            </div>
                            <div style="font-size: 18px;">
                                нужные письма
                            </div>
                        </div>
                        <div class="my-2 mx-3">
                            Определите, какие письма вы хотите отправлять клиентам.
                        </div>
                        <div class="mt-auto mx-2">
                            <?= Html::img('/img/reminder/how_to_turn_on_1.png', [
                                'alt' => '',
                                'class' => 'w-100',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-3 px_7">
                    <div class="border rounded-sm h-100 d-flex flex-column justify-content-between">
                        <div class="mt-3 mx-3 text-primary">
                            <div style="font-size: 22px;">
                                <strong>
                                    Отредактируйте
                                </strong>
                            </div>
                            <div style="font-size: 18px;">
                                письма
                            </div>
                        </div>
                        <div class="my-2 mx-3">
                            Отредактируйте текст шаблонов писем под свои требования или оставьте их, как есть.
                        </div>
                        <div class="mt-auto mx-2">
                            <?= Html::img('/img/reminder/how_to_turn_on_2.png', [
                                'alt' => '',
                                'class' => 'w-100',
                            ]) ?>
                            <?= Html::img('/img/taxrobot/arrow.svg', [
                                'class' => 'turn_on-next-arrow',
                                'alt' => '',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-3 px_7">
                    <div class="border rounded-sm h-100 d-flex flex-column justify-content-between">
                        <div class="mt-3 mx-3 text-primary">
                            <div style="font-size: 22px;">
                                <strong>
                                    Определите
                                </strong>
                            </div>
                            <div style="font-size: 18px;">
                                правила
                            </div>
                        </div>
                        <div class="my-2 mx-3">
                            Задайте параметры правил рассылки писем.
                            Например, минимальная сумма долга или на какой день после просрочки оплаты счета, отправляем первое письмо.
                        </div>
                        <div class="mt-auto mx-2">
                            <?= Html::img('/img/reminder/how_to_turn_on_3.png', [
                                'alt' => '',
                                'class' => 'w-100',
                            ]) ?>
                            <?= Html::img('/img/taxrobot/arrow.svg', [
                                'class' => 'turn_on-next-arrow',
                                'alt' => '',
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-3 px_7">
                    <div class="border rounded-sm h-100 d-flex flex-column justify-content-between">
                        <div class="mt-3 mx-3 text-primary">
                            <div style="font-size: 22px;">
                                <strong>
                                    Укажите
                                </strong>
                            </div>
                            <div style="font-size: 18px;">
                                клиентов
                            </div>
                        </div>
                        <div class="my-2 mx-3">
                            Включите "АвтоСбор долгов" для всех покупателей или каких-то определенных.
                        </div>
                        <div class="mt-auto mx-2">
                            <?= Html::img('/img/reminder/how_to_turn_on_4.png', [
                                'alt' => '',
                                'class' => 'w-100',
                            ]) ?>
                            <?= Html::img('/img/taxrobot/arrow.svg', [
                                'class' => 'turn_on-next-arrow',
                                'alt' => '',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>