<?php

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\helpers\Html;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Settings;
use frontend\models\Documents;
use frontend\modules\documents\components\FilterHelper;
use frontend\assets\PaymentReminderAsset;
use frontend\models\PaymentReminderMessageContractorSearch;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\web\View;

/* @var View $this
 * @var Settings                               $model
 * @var Employee                               $user
 * @var PaymentReminderMessageContractorSearch $searchModel
 * @var ActiveDataProvider                     $dataProvider
 */
PaymentReminderAsset::register($this);
$this->title = 'Правила рассылки автоматических писем';

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-user-help'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'maxWidth' => 500,
    ],
]);

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-contractor-count',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-user-help'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'maxWidth' => 500,
    ],
]);

/** @noinspection PhpUnhandledExceptionInspection */
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'maxWidth' => 500,
    ],
]);

$contractorData = Contractor::getAllContractorList(Contractor::TYPE_CUSTOMER, false);

$this->registerJs(<<<JS
$('#form-payment-reminder-message-contractor .tooltipstered').tooltipster('disable');
JS
);
?>
<style type="text/css">
    .pl_25 {
        padding-left: 25px;
    }
</style>

<div class="debt-report-content container-fluid payment-reminder-block" style="padding: 0; margin-top: -10px;">
    <?= $this->render('_menu', ['currentTab' => 'subscribe']) ?>
    <div class="payment-reminder-settings">
        <h3 class="page-title">
            <?= Html::encode($this->title); ?>
        </h3>

        <?php $form = ActiveForm::begin([
            'id' => 'payment-reminder-settings-form',
        ]); ?>
            <div class="form-group">
                <?= Html::activeRadio($model, 'send_mode', [
                    'id' => 'settings-send_mode-0',
                    'uncheck' => null,
                    'value' => Settings::SEND_DISABLE,
                    'label' => Settings::$modeItems[Settings::SEND_DISABLE],
                    'data-toggle' => 'checked',
                ]) ?>
            </div>
            <div class="form-group">
                <?= Html::activeRadio($model, 'send_mode', [
                    'id' => 'settings-send_mode-1',
                    'uncheck' => null,
                    'value' => Settings::SEND_BY_SUITABLE,
                    'label' => Settings::$modeItems[Settings::SEND_BY_SUITABLE],
                    'data-toggle' => 'checked',
                    'data-target' => '#SEND_BY_SUITABLE',
                ]) ?>
                <div id="SEND_BY_SUITABLE" class="collapse pl_25">
                    <?= $form->field($model, 'overdue_invoice')->checkbox() ?>
                    <?= $form->field($model, 'exceeded_limit')->checkbox() ?>
                    <?= $form->field($model, 'invoice_paid')->checkbox() ?>
                    <?= $form->field($model, 'add_exceptions')->checkbox([
                        'label' => 'Добавить исключения из правил: не отправлять письма этим покупателям',
                        'labelOptions' => [
                            'id' => 'settings-add_exceptions-label'
                        ],
                        'data-toggle' => 'checked',
                        'data-target' => '#ADD_EXCEPTIONS',
                    ])->hint(Html::tag('div', $form->field($model, 'excludeIds')->widget(Select2::class,[
                        'data' => $contractorData,
                        'hideSearch' => true,
                        'options' => [
                            'placeholder' => '',
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'closeOnSelect' => false,
                            'width' => '100%',
                            'escapeMarkup' => new JsExpression('function(text) {return text;}')
                        ],
                        'toggleAllSettings' => [
                            'selectLabel' => false,
                            'unselectLabel' => false,
                        ]
                    ])->label(false), [
                        'id' => 'ADD_EXCEPTIONS',
                        'class' => 'collapse pl_25',
                    ])) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::activeRadio($model, 'send_mode', [
                    'id' => 'settings-send_mode-2',
                    'uncheck' => null,
                    'value' => Settings::SEND_TO_SELECTED,
                    'label' => Settings::$modeItems[Settings::SEND_TO_SELECTED],
                    'data-toggle' => 'checked',
                    'data-target' => '#SEND_TO_SELECTED',
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>

        <div id="SEND_TO_SELECTED" class="collapse pl_25">
            <?= $this->render('_selected', [
                'user' => $user,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-sm-12">
            <div class="d-flex justify-content-between">
                <?= Html::submitButton('Сохранить', [
                    'form' => 'payment-reminder-settings-form',
                    'class' => 'btn darkblue text-white ladda-button',
                    'style' => 'min-width: 130px;'
                ]) ?>
                <?= Html::beginForm() ?>
                    <?= Html::hiddenInput('cancel', 1) ?>
                    <?= Html::submitButton('Отменить', [
                        'class' => 'btn darkblue text-white',
                        'style' => 'min-width: 130px;'
                    ]) ?>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'add_message_contractor_modal',
    'header' => 'Выбрать покупателя',
]) ?>

<?= $this->render('customers', [
    'searchModel' => $contractorSearchModel,
    'dataProvider' => $contractorDataProvider,
]) ?>

<?php Modal::end() ?>

<div class="tooltip-template hidden">
    <?php foreach (PaymentReminderMessage::find()->andWhere(['company_id' => $user->company_id])->all() as $paymentReminderMessage): ?>
        <span class="tooltip-payment-reminder-body text-left"
              id="template_body-header-<?= $paymentReminderMessage->number; ?>"
              style="display: inline-block;">
            <?= $paymentReminderMessage->message_template_body; ?>
        </span>
    <?php endforeach; ?>
    <span class="tooltip-payment-reminder-contractor-count text-left" id="template_contractor_count"
          style="display: inline-block;">
        Кол-во покупателей,<br>
        которым подключен<br>
        данный шаблон
    </span>
</div>

<?php $this->registerJs(<<<JS
(function( $ ) {
    $('.collapse').collapse({
      toggle: false
    });
    var toggleChecked = function() {
        $('input[data-toggle=checked]').each(function () {
            console.log(this.checked);
            let target = $($(this).data('target'));
            if (target.length > 0) {
                target.collapse(this.checked ? 'show' : 'hide');
            }
        });

    };
    $('input[data-toggle=checked]').on('change', function (e) {
        toggleChecked();
    });
    toggleChecked();
})(jQuery);
JS
) ?>
