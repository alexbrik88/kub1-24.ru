<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.05.2018
 * Time: 6:48
 */

use common\components\date\DateHelper;
use common\models\paymentReminder\PaymentReminderMessage;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $model PaymentReminderMessage
 * @var $isFreeTariff boolean
 */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
Modal::begin([
    'header' => 'Настройка шаблона письма №' . $model->number,
    'id' => 'modal-update-payment-reminder-' . $model->id,
]);
$form = ActiveForm::begin([
    'action' => Url::to(['update', 'id' => $model->id]),
    'options' => [
        'class' => 'form-horizontal form-payment-reminder-message',
        'id' => 'form-payment-reminder-message-' . $model->id,
    ],
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized',],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="form-body">
    <span class="text-center col-md-12 warning-text hidden">Текст и параметры шаблона измените под ваши требования</span>
    <?= Html::activeHiddenInput($model, 'activate'); ?>
    <?= Html::activeHiddenInput($model, 'status'); ?>
    <?= Html::activeHiddenInput($model, 'number'); ?>
    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                Условие отправки:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                <?= $model->event_header; ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [6, 7, 8, 9])): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text" for="checkingaccountant-rs">
                Условие отправки:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                Сумма просроченного долга больше суммы: <?= Html::activeTextInput($model, 'debt_sum', [
                    'class' => 'form-control',
                    'style' => 'width: 20%;display: inline;',
                ]); ?>
                <span id="tooltip_payment-reminder-debt-sum-<?= $model->id; ?>"
                      class="tooltip3 ico-question valign-middle"
                      data-tooltip-content="#tooltip_debt-sum-reminder-<?= $model->id; ?>"
                      style="margin-left: 10px;cursor: pointer;"></span>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                Условие отправки:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                Счет оплачен до истечения срока оплаты.
            </div>
        </div>
    <?php endif; ?>
    <?php if ((int)$model->number == 1): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text" for="checkingaccountant-rs">
                День отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "За {input}  дней до окончания срока отсрочки платежа по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [2, 3, 4, 5])): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text" for="checkingaccountant-rs">
                День отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input}  день просрочки оплаты по счету. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php elseif ((int)$model->number == 6): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                День отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                В день превышения суммы всей просроченной задолженности над указанной выше суммой.
            </div>
        </div>
    <?php elseif (in_array((int)$model->number, [7, 8, 9])): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                День отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                <?= $form->field($model, 'days', [
                    'options' => [
                        'class' => '',
                        'style' => 'display:inline-block;',
                    ],
                    'template' => "На {input}  день превышения суммы всей просроченной задолженности над указанной выше суммой. \n{error}\n{hint}",
                ])->textInput([
                    'style' => 'display: inline;width: 10%;',
                ])->label(false); ?>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                День отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                В день изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>

    <?= $form->field($model, 'message_template_subject', $config)->textInput([
        'maxlength' => true,
    ])->label('Тема письма:'); ?>

    <div class="form-group required">
        <label class="col-md-4 control-label bold-text">
            Текст письма:
        </label>

        <div class="col-md-8 inp_one_line-product">
            <div class="message-template-body_editable_block" contenteditable="true"
                 style="border: 1px solid #cecece;width: 97%;padding: 6px 12px;">
                <?= $model->message_template_body; ?>
            </div>
            <?= Html::activeHiddenInput($model, 'message_template_body'); ?>
        </div>
    </div>

    <?php if (in_array((int)$model->number, [1, 2, 3, 4, 6, 7, 8, 10])): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                Кому:
            </label>

            <div class="col-md-8 inp_one_line-product">
                <?= $form->field($model, 'send_for_chief', [
                    'options' => [
                        'class' => '',
                        'style' => 'width: 30%;display: inline-block;',
                    ],
                ])->checkbox([], true)->label('Руководителю'); ?>

                <?= $form->field($model, 'send_for_contact', [
                    'options' => [
                        'class' => '',
                        'style' => 'display: inline-block;',
                    ],
                ])->checkbox([], true)->label('Контактному лицу'); ?>

                <?= $form->field($model, 'send_for_all', [
                    'options' => [
                        'class' => '',
                    ],
                ])->checkbox([], true)->label('Всем, кто есть в карточке Покупателя'); ?>
            </div>
        </div>
    <?php else: ?>
        <div class="form-group row required">
            <label class="col-md-4 control-label bold-text">
                Кому:
            </label>
            <div class="col-md-8 inp_one_line-product mt-2">
                <?= $form->field($model, 'email1', ['options' => ['class' => 'mb-3']])->label(false) ?>
                <?= $form->field($model, 'email2', ['options' => ['class' => 'mb-3']])->label(false) ?>
                <?= $form->field($model, 'email3', ['options' => ['class' => '']])->label(false) ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number == 10): ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                Не отправлять письмо:
            </label>

            <div class="col-md-8 inp_one_line-product">
                <?= Html::activeCheckbox($model, 'not_send_where_order_payment', [
                    'label' => 'При оплате по кассе',
                    'labelOptions' => [
                        'style' => 'padding-top: 7px;',
                    ],
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_emoney_payment', [
                    'label' => 'При оплате по e-money',
                ]); ?>
                <?= Html::activeCheckbox($model, 'not_send_where_partial_payment', [
                    'label' => 'При частичной оплате счета',
                ]); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ((int)$model->number !== 10): ?>
        <?php if (!$model->timeInput) {
            $model->timeInput = '09:45';
        } ?>
        <?php $time = explode(':', $model->timeInput) ?>
        <?= $form->field($model, 'timeInput', array_merge($config, [
            'template' => "{label}\n{beginWrapper}\n<div class=\"input-group bootstrap-timepicker timepicker\">
                {input}
                <span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-time\"></i></span>
            </div>\n{error}\n{hint}\n{endWrapper}",
            'wrapperOptions' => [
                'class' => 'col-md-4 inp_one_line-product',
            ],
        ]))->textInput([
            'class' => 'form-control time-picker time-picker-' . $model->id,
            'readonly' => true,
            'style' => 'background-color: #ffffff;',
        ])->label('Время отправки письма') ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.time-picker-<?=$model->id?>').timepicker({
                    showMeridian: false,
                    minuteStep: 5,
                });
            });
        </script>
    <?php else: ?>
        <div class="form-group required">
            <label class="col-md-4 control-label bold-text">
                Время отправки письма:
            </label>

            <div class="col-md-8 inp_one_line-product" style="line-height: 34px;">
                После изменения статуса счета на «Оплачен».
            </div>
        </div>
    <?php endif; ?>
    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?php if (!$isFreeTariff || $model->number == 1): ?>
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                        'style' => 'width: 130px!important;',
                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ]); ?>
                <?php else: ?>
                    <?= Html::button('Сохранить', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs no-rules-update',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#activate-modal-no-rules-' . $model->id,
                        ],
                    ]); ?>
                    <?= Html::button('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg no-rules-update',
                        'data' => [
                            'toggle' => 'modal',
                            'target' => '#activate-modal-no-rules-' . $model->id,
                        ],
                        'title' => 'Сохранить',
                    ]); ?>
                    <?= $modalNoRules = $this->render('no_rules_modal', [
                        'model' => $model,
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:59.45%;">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg back',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $form->end(); ?>
<?php Modal::end(); ?>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_send-to-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Поставьте e-mail нашего юриста: urist@kub-24.ru <br>
        При получении письма он свяжется с вами, что бы подготовить претензию. <br>
        Стоимость подготовки претензии 3000 рублей
    </span>
    <span id="tooltip_debt-sum-reminder-<?= $model->id; ?>" style="display: inline-block; text-align: center;">
        Укажите сумму, если сумма всей просроченной <br>
        задолженности ее превысит, то отправится <br>
        письмо
    </span>
</div>
<?php $this->registerJs('
    $(".no-rules-update").click(function () {
        $(this).closest(".modal").modal("hide");
    });
'); ?>

