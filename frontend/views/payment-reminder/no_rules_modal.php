<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.05.2018
 * Time: 6:26
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div id="activate-modal-no-rules-<?= $model->id; ?>" class="confirm-modal fade modal" role="dialog" tabindex="-1"
     aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        Только для платных тарифов.
                        <br>
                        Перейти к оплате?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('<span class="ladda-label">ОК</span>', Url::to(['/subscribe']), [
                            'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button',
                            'data-style' => 'expand-right',
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::button('Отменить', [
                            'class' => 'btn darkblue',
                            'data-dismiss' => 'modal',
                            'style' => 'width: 100px;',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
