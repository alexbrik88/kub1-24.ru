<?php

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Settings;
use frontend\assets\PaymentReminderAsset;
use frontend\components\Icon;
use frontend\models\Documents;
use frontend\models\PaymentReminderMessageContractorSearch;
use frontend\modules\documents\components\FilterHelper;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$isFreeTariff = $user->company->isFreeTariff;

$activeTemplates = PaymentReminderMessage::find()
    ->andWhere(['company_id' => $user->company_id])
    ->andWhere(['status' => PaymentReminderMessage::STATUS_ACTIVE])
    ->indexBy('number')
    ->all();

$hasActiveTemplate = !empty($activeTemplates);

$columns = [
    [
        'attribute' => 'contractor',
        'label' => 'Покупатель',
        'headerOptions' => [
            'rowspan' => 2,
            'colspan' => 1,
            'width' => '15%',
            'class' => 'fixed-column',
        ],
        'contentOptions' => [
            'class' => 'fixed-column',
        ],
        'class' => DropDownSearchDataColumn::class,
        'filter' => $searchModel->getContractorFilter(),
        'value' => function (PaymentReminderMessageContractor $model) {
            return $model->contractor->getTitle(true);
        }
    ],
    [
        'attribute' => 'invoiceSum',
        'label' => 'Долг',
        'headerOptions' => [
            'rowspan' => 2,
            'colspan' => 1,
            'class' => 'col_payment_reminder_message_contractor_invoice_sum',
            'width' => '8%',
        ],
        'contentOptions' => [
            'class' => 'sorting col_payment_reminder_message_contractor_invoice_sum',
        ],
        'value' => function (PaymentReminderMessageContractor $model) {
            return DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_MORE_90, false, true, $model->contractor_id);
        }
    ],
    [
        'label' => 'Письма',
        'headerOptions' => [
            'type' => 'header',
            'class' => 'text-center',
            'rowspan' => 1,
            'colspan' => 10,
            'style' => 'border-bottom: 1px solid #ddd;',
        ],
    ],
];

foreach (range(1, 10) as $i) {
    $selectedAll = true;
    foreach ($dataProvider->models as $item) {
        $selectedAll = $selectedAll && $item->{'message_' . $i};
    }
    $isActive = isset($activeTemplates[$i]);
    $columns[] = [
        'attribute' => 'message_'.$i,
        'header' => Html::checkbox(null, $selectedAll, [
            'class' => "paymentremindermessagecontractor-message-all messagecontractor_{$i}_all",
            'label' => '№'.$i,
            'labelOptions' => [
                'style' => 'font-weight: 600; margin-right: 5px;',
            ],
            'data' => [
                'can-update' => $isActive,
                'number' => 1,
                'target' => "input:checkbox.messagecontractor_$i",
            ],
            'disabled' => true,
        ]),
        'headerOptions' => [
            'rowspan' => 1,
            'colspan' => 1,
            'class' => "tooltip2 col_payment_reminder_message_contractor_message_$i" .
                ($isActive ? ' can_update' : ''),
            'data-tooltip-content' => $isActive ? '#template_body-header-'.$i : null,
            'title' => $isActive ? null : 'Данное письмо не активно, включите его на шаге "Настройка шаблонов писем"',
            'width' => '5%',
            'style' => 'border-right: 1px solid #ddd;',
        ],
        'contentOptions' => [
            'title' => $isActive ? null : 'Данное письмо не активно, включите его на шаге "Настройка шаблонов писем"',
            'class' => "col_payment_reminder_message_contractor_message_$i" .
                ($isActive ? ' can_update' : ''),
        ],
        'format' => 'raw',
        'value' => function (PaymentReminderMessageContractor $model) use ($user, $i, $isActive) {
            $lastDate = DateHelper::format($model->{"last_message_date_$i"}, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

            return Html::activeCheckbox($model, "message_$i", [
                'id' => "messagecontractor_{$i}_{$model->contractor_id}",
                'class' => "paymentremindermessagecontractor-message messagecontractor_{$i}",
                'label' => false,
                'name' => "PaymentReminderMessageContractor[message_{$i}][{$model->contractor_id}]",
                'disabled' => true,
                'data' => [
                    'can-update' => $isActive,
                    'number' => $i,
                    'target' => "input:checkbox.messagecontractor_$i",
                ],
            ]) .
                '<span class="payment-reminder-tooltip"
                    data-contractor_email="' . $model->{"last_message_contractor_email_$i"} . '"
                    data-date="' . $lastDate . '">' . $lastDate . '</span>';
        }
    ];
}

$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'template' => '{delete}',
    'headerOptions' => [
        'rowspan' => 2,
        'colspan' => 1,
        'width' => '3%',
    ],
    'buttons' => [
        'delete' => function ($url, $model, $key) {
            return Html::a(Icon::get('garbage'), '#reminder-comtractor-delete-'.$model->id, [
                'data-toggle' => 'modal',
            ]);
        },
    ],
];
?>

<?php Pjax::begin([
    'id' => 'payment-reminder-message-contractor-pjax',
    'enablePushState' => false,
    'timeout' => 10000,
    'options' => [
        'class' => 'pb-0',
    ],
]); ?>
<div class="portlet box darkblue blk_wth_srch">
    <div class="portlet-title row-fluid">
        <div class="caption list_recip col-sm-5">
        </div>
        <div class="actions float-l pensil-box" style="float: right;padding-top: 4px;">
            <?= Html::button('Добавить', [
                'class' => '',
                'title' => 'Добавить покупателя в список',
                'data-toggle' => 'modal',
                'data-target' => '#add_message_contractor_modal',
            ]); ?>
            <?= Html::button('<i class="icon-pencil" style="font-size: inherit;"></i> Редактировать', [
                'class' => 'update-payment-reminder-message-contractor' . ($hasActiveTemplate ? null : ' tooltip3 disabled'),
                'data-tooltip-content' => $hasActiveTemplate ? null : '#tooltip-active-template',
                'data-tooltipster' => $hasActiveTemplate ? null : "{'trigger':'hover'}",
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls" style="font-size: inherit;"></span> Сохранить', [
                'form' => 'form-payment-reminder-message-contractor',
                'class' => 'save-payment-reminder-message-contractor hidden ladda-button',
            ]); ?>
            <?= Html::button('<i class="ico-Cancel-smart-pls" style="font-size: inherit;"></i> Отменить', [
                'class' => 'undo-payment-reminder-message-contractor hidden',
            ]); ?>
        </div>
        <div class="tooltip-template hidden">
            <span class="text-left" id="tooltip-active-template" style="display: inline-block;">
                У вас нет активных писем.<br>
                Необходимо включить хотя бы одно письмо.
            </span>
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['update-message-contractor', '#' => 'settings']),
            'options' => [
                'class' => 'form-payment-reminder-message-contractor',
                'id' => 'form-payment-reminder-message-contractor',
                'data-pjax' => 1,
            ],
        ]); ?>
            <?= GridView::widget([
                'id' => 'payment-reminder-message-contractor-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'scrollable-table table table-bordered table-hover dataTable documents_table mb-0',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', [
                    'totalCount' => $dataProvider->totalCount,
                ]),
                'columns' => $columns,
            ]); ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php foreach ($dataProvider->models as $model) : ?>
    <?php Modal::begin([
        'id' => 'reminder-comtractor-delete-'.$model->id,
        'options' => [
            'class' => 'confirm-modal fade',
        ],
        'closeButton' => false,
        'toggleButton' => false,
    ]); ?>

        <div style="margin-bottom: 30px;">
            <?= Html::encode("Удалить {$model->contractor->getTitle(true)} из списка?") ?>
        </div>
        <?= Html::beginForm([
            'customer-delete',
            'id' => $model->id
        ], 'post', [
            'class' => 'form-actions row reminder-comtractor-delete-form',
        ]) ?>
            <div class="col-xs-6">
                <?= Html::submitButton('ДА', [
                    'class' => 'btn darkblue pull-right',
                ]); ?>
            </div>
            <div class="col-xs-6">
                <?= Html::button('НЕТ', [
                    'class' => 'btn darkblue',
                    'data-dismiss' => 'modal',
                ]); ?>
            </div>
        <?= Html::endForm() ?>
    <?php Modal::end() ?>
<?php endforeach ?>

<?php Pjax::end(); ?>
