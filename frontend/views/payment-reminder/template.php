<?php

use common\components\helpers\Html;
use common\models\employee\Employee;
use common\components\grid\GridView;
use common\models\paymentReminder\PaymentReminderMessage;
use common\components\grid\DropDownDataColumn;
use common\models\employee\EmployeeRole;
use frontend\assets\PaymentReminderAsset;
use frontend\models\PaymentReminderMessageSearch;
use frontend\widgets\TableConfigWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use yii\web\View;

/* @var View $this
 * @var Employee                     $user
 * @var PaymentReminderMessageSearch $paymentReminderMessageSearchModel
 * @var ActiveDataProvider           $paymentReminderMessageDataProvider
 */
PaymentReminderAsset::register($this);
$this->title = 'Шаблоны писем должникам';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-message-body'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'maxWidth' => 500,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-contractor-count',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-payment-reminder-contractor-count'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
$isFreeTariff = $user->company->isFreeTariff;
$hasActiveTemplate = PaymentReminderMessage::find()
    ->andWhere(['company_id' => $user->company_id])
    ->andWhere(['status' => PaymentReminderMessage::STATUS_ACTIVE])
    ->exists();
?>
<div class="debt-report-content container-fluid payment-reminder-block" style="padding: 0; margin-top: -10px;">
    <?= $this->render('_menu', ['currentTab' => 'template']) ?>
    <div class="payment-reminder-index">
        <div class="portlet box darkblue blk_wth_srch">
            <div class="portlet-title row-fluid">
                <div class="caption list_recip col-sm-3">Настройка шаблонов писем</div>
            </div>
            <div class="portlet-body">
                <div class="table-container" style="">
                    <?= GridView::widget([
                        'id' => 'payment-reminder-message-grid',
                        'dataProvider' => $paymentReminderMessageDataProvider,
                        'filterModel' => $paymentReminderMessageSearchModel,
                        'tableOptions' => [
                            'class' => 'table table-bordered table-hover dataTable documents_table fix-thead',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'number',
                                'label' => '№',
                                'headerOptions' => [
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) use ($user) {
                                    if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                                        return Html::a($model->number, null, [
                                            'title' => Yii::t('yii', 'Редактировать'),
                                            'aria-label' => Yii::t('yii', 'Редактировать'),
                                            'data-pjax' => 0,
                                            'data' => [
                                                'toggle' => 'modal',
                                                'target' => '#modal-update-payment-reminder-' . $model->id,
                                                'pjax' => 0,
                                            ],
                                        ]);
                                    }

                                    return $model->number;
                                }
                            ],
                            [
                                'class' => DropDownDataColumn::className(),
                                'attribute' => 'category_id',
                                'label' => 'Категория',
                                'headerOptions' => [
                                    'width' => '7%',
                                    'class' => 'dropdown-filter category-header',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'filter' => $paymentReminderMessageSearchModel->getCategoryFilter(),
                                'value' => function (PaymentReminderMessage $model) {
                                    return $model->category->name;
                                }
                            ],
                            [
                                'attribute' => 'event_header',
                                'label' => 'Событие',
                                'headerOptions' => [
                                    'width' => '27%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) {
                                    return '<b>' . $model->event_header . '</b><br>' . $model->event_body;
                                }
                            ],
                            [
                                'attribute' => 'days',
                                'label' => '"X" дней',
                                'headerOptions' => [
                                    'width' => '7%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) {
                                    return $model->days ? $model->days : '-';
                                }
                            ],
                            [
                                'attribute' => 'message_template_subject',
                                'label' => 'Тема шаблона письма',
                                'headerOptions' => [
                                    'width' => '10%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) {
                                    return $model->message_template_subject;
                                }
                            ],
                            [
                                'attribute' => 'message_template_body',
                                'label' => 'Текст шаблона письма',
                                'headerOptions' => [
                                    'width' => '15%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) {
                                    return '<span class="tooltip2" data-tooltip-content="#template_body-' . $model->id . '">' .
                                        mb_substr($model->message_template_body, 0, 35) .
                                        '...</span>' .
                                        '<div class="tooltip-template hidden">
                                    <span class="tooltip-payment-reminder-body text-left" id="template_body-' . $model->id . '" style="display: inline-block;">' .
                                        $model->message_template_body .
                                        '</span>
                                </div>';
                                }
                            ],
                            [
                                'attribute' => 'status',
                                'label' => 'Статус',
                                'headerOptions' => [
                                    'width' => '19%',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                                    $modalNoRules = $this->render('no_rules_modal', [
                                        'model' => $model,
                                    ]);
                                    $canChange = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,]);

                                    return '<span class="payment-reminder-switch col-md-12">' .
                                        Html::a('Включить', null, [
                                            'class' => 'text-center btn activate-message-template ' . ($model->status ? 'yellow-text active' : 'yellow'),
                                            'style' => 'padding: 4px;margin: 0;font-size: 12px;width: 50%;',
                                            'data' => $canChange ? [
                                                'toggle' => 'modal',
                                                'target' => ($isFreeTariff && $model->number != 1) ? '#activate-modal-no-rules-' . $model->id :
                                                    ('#modal-update-payment-reminder-' . $model->id),
                                            ] : [],
                                        ]) .
                                        Html::a('Отключить', $canChange ? Url::to(['change-status', 'id' => $model->id]) : null, [
                                            'class' => 'text-center btn ' . (!$model->status ? 'yellow-text active' : 'yellow'),
                                            'style' => 'padding: 4px;margin: 0;font-size: 12px;width: 50%;',
                                            'data' => $canChange ? [
                                                'method' => 'post',
                                                'params' => [
                                                    'status' => PaymentReminderMessage::STATUS_INACTIVE,
                                                    '_csrf' => Yii::$app->request->csrfToken,
                                                ],
                                            ] : [],
                                        ]) . '</span>' . $modalNoRules;
                                }
                            ],
                            [
                                'label' => 'Кол-во покупателей',
                                'headerOptions' => [
                                    'class' => 'tooltip2-contractor-count',
                                    'data-tooltip-content' => '#template_contractor_count',
                                    'width' => '10%',
                                    'style' => 'cursor: pointer;',
                                ],
                                'contentOptions' => [
                                    'class' => 'sorting',
                                ],
                                'format' => 'raw',
                                'value' => function (PaymentReminderMessage $model) {
                                    return $model->getContractorCount();
                                }
                            ],
                            [
                                'headerOptions' => [
                                    'width' => '90px',
                                ],
                                'class' => ActionColumn::className(),
                                'template' => '{edit}',
                                'buttons' => [
                                    'edit' => function ($url, PaymentReminderMessage $model) use ($isFreeTariff, $user) {
                                        if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR,])) {
                                            $updateModal = $this->render('update_modal', [
                                                'model' => $model,
                                                'isFreeTariff' => $isFreeTariff,
                                            ]);

                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', null, [
                                                    'class' => 'update-payment-reminder-message',
                                                    'title' => Yii::t('yii', 'Редактировать'),
                                                    'aria-label' => Yii::t('yii', 'Редактировать'),
                                                    'data-pjax' => 0,
                                                    'data' => [
                                                        'toggle' => 'modal',
                                                        'target' => '#modal-update-payment-reminder-' . $model->id,
                                                        'pjax' => 0,
                                                    ],
                                                ]) . $updateModal;
                                        }

                                        return null;
                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="row" id="widgets">
            <div class="col-md-3 col-sm-3">
                <a name="settings"></a>
            </div>
            <div class="col-md-3 col-sm-3"></div>
            <div class="col-md-3 col-sm-3"></div>
            <div class="col-md-3 col-sm-3">
                <div class="table-icons" style="margin-top: -20px;">
                    <?= TableConfigWidget::widget([
                        'items' => [
                            [
                                'attribute' => 'payment_reminder_message_contractor_invoice_sum',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_1',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_2',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_3',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_4',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_5',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_6',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_7',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_8',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_9',
                            ],
                            [
                                'attribute' => 'payment_reminder_message_contractor_message_10',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>

        <div class="tooltip-template hidden">
            <?php foreach (PaymentReminderMessage::find()->andWhere(['company_id' => $user->company_id])->all() as $paymentReminderMessage): ?>
                <span class="tooltip-payment-reminder-body text-left"
                      id="template_body-header-<?= $paymentReminderMessage->number; ?>"
                      style="display: inline-block;">
                    <?= $paymentReminderMessage->message_template_body; ?>
                </span>
            <?php endforeach; ?>
            <span class="tooltip-payment-reminder-contractor-count text-left" id="template_contractor_count"
                  style="display: inline-block;">
                    Кол-во покупателей,<br>
                    которым подключен<br>
                    данный шаблон
                </span>
        </div>
    </div>
</div>