<?php

use frontend\rbac\permissions\PaymentReminder;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/**
 * @var string $currentTab
 */
NavBar::begin([
    'options' => [
        'class' => 'navbar-report navbar-default',
    ],
    'brandOptions' => [
        'style' => 'margin-left: 0;'
    ],
    'containerOptions' => [
        'style' => 'padding: 0;'
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
        'style' => 'padding: 0;'
    ],
]);
/** @noinspection PhpUnhandledExceptionInspection */
echo Nav::widget([
    'id' => 'debt-report-menu',
    'items' => [
        [
            'label' => 'Описание',
            'url' => ['index'],
            'active' => $currentTab === 'index',
            'visible' => Yii::$app->user->can(PaymentReminder::INDEX),
        ],
        [
            'label' => 'Шаблоны писем',
            'url' => ['template'],
            'active' => $currentTab === 'template',
            'visible' => Yii::$app->user->can(PaymentReminder::TEMPLATE),
        ],
        [
            'label' => 'Автоматическая рассылка писем',
            'url' => ['settings'],
            'active' => $currentTab === 'settings',
            'visible' => Yii::$app->user->can(PaymentReminder::SETTINGS),
        ],
        [
            'label' => 'Отчёт',
            'url' => ['report'],
            'active' => $currentTab === 'report',
            'visible' => Yii::$app->user->can(PaymentReminder::REPORT),
        ],
    ],
    'options' => ['class' => 'navbar-nav'],
]);
NavBar::end();