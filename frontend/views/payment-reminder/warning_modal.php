<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.05.2018
 * Time: 5:44
 */

use yii\helpers\Html;
use common\models\paymentReminder\PaymentReminderMessage;
use yii\helpers\Url;

/* @var $url string
 * @var $isFreeTariff boolean
 * @var $model PaymentReminderMessage
 */
?>
<div id="activate-modal-warning-<?= $model->id; ?>" class="confirm-modal fade modal" role="dialog" tabindex="-1"
     aria-hidden="false"
     style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">В КУБе шаблон заполнен в качестве примера!
                        <br>
                        Рекомендуем его настроить под ваши требования. Для этого нажмите на
                        <br>
                        иконку редактировать.
                        <br>
                        Если вы это уже сделали, то данное письмо считайте, как
                        <br>
                        дополнительное напоминание.
                        <br>
                        Спасибо за понимание!
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <?= Html::a('<span class="ladda-label">ОК</span>', Url::to(['change-status', 'id' => $model->id]), [
                            'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button',
                            'data-style' => 'expand-right',
                            'data-method' => 'post',
                            'style' => 'float: none!important;',
                            'data' => ($isFreeTariff && $model->number != 1) ? '' :
                                [
                                    'method' => 'post',
                                    'params' => [
                                        'status' => PaymentReminderMessage::STATUS_ACTIVE,
                                        '_csrf' => Yii::$app->request->csrfToken,
                                    ],
                                ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
