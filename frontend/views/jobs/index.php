<?php

namespace frontend\views\jobs;

use common\modules\import\components\ImportCommandManager;
use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\ImportJobData;
use frontend\widgets\RangeButtonWidget;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use yii\web\View;

/* @var View $this
 * @var $searchModel \frontend\models\jobs\JobsSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var bool $showHidden
 */

$this->title = 'Фоновые задачи';
$commandManager = ImportCommandManager::getInstance();

?>
<div class="portlet box">
    <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>

    <h3 class="page-title"><?= $this->title; ?></h3>

    <div class="portlet box darkblue">
        <div class="portlet-title"></div>
        <div class="portlet-body accounts-list">
            <div class="scroll-table-wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Дата создания',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (ImportJobData $job) {
                                return date(DateHelper::FORMAT_USER_DATETIME, $job->created_at);
                            },
                        ],
                        [
                            'attribute' => 'finished_at',
                            'label' => 'Дата завершения',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (ImportJobData $job) {
                                if ($job->finished_at === null) {
                                    return '';
                                }

                                return date(DateHelper::FORMAT_USER_DATETIME, $job->finished_at);
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'label' => 'Тип задачи',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (ImportJobData $jobData) use ($commandManager) {
                                $commandClass = $commandManager->getTypeById($jobData->type)->getCommandClass();

                                return $commandClass::getImportCommandName();
                            },
                        ],
                        [
                            'attribute' => 'params',
                            'label' => 'Параметры',
                            'format' => 'raw',
                            'value' => function (ImportJobData $job) {
                                $helper = new ImportParamsHelper($job);
                                $value = '';

                                if ($helper->hasDateFrom() && $helper->hasDateTo()) {
                                    $value .= sprintf(
                                        'Начало периода: %s<br>Конец периода: %s<br>',
                                        $helper->getDateFrom()->format(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS),
                                        $helper->getDateTo()->format(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS)
                                    );
                                }

                                if ($helper->hasIdentifier()) {
                                    $value .= sprintf('Учётная запись: %s<br>', $helper->getIdentifier());
                                }

                                if ($value) {
                                    return $value;
                                }

                                return 'Не указано';
                            },
                        ],
                        [
                            'attribute' => 'result',
                            'label' => 'Статус',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function (ImportJobData $job) {
                                return $job->result;
                            },
                        ],
                        [
                            'attribute' => 'error',
                            'label' => 'Описание ошибки',
                            'enableSorting' => false,
                            'format' => 'raw',
                            'visible' => $showHidden,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
