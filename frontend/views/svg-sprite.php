<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $ico string */
/* @var $options array */

$options = $_params_;
$ico = ArrayHelper::remove($options, 'ico');
$mtime = ArrayHelper::remove($options, 'mtime');
Html::addCssClass($options, 'svg-icon');
$url = Yii::getAlias('@web/img/svg/svgSprite.svg');
$href = Url::to([$url, 'v' => $mtime ? : '2', '#' => $ico]);

echo Html::tag('svg', "<use xlink:href=\"{$href}\"></use>", $options);
