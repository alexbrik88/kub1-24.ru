<?php

use frontend\models\CollateForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$company = $model->company;
$contractor = $model->contractor;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<div class="modal-header" style="margin: -15px -15px 15px; background-color: #00b7af; color: #fff;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1 style="font-size: 32px;">
        <span class="ico-Send-smart-pls fs pull-left" style="font-size:inherit;line-height:inherit;"></span>
        Выберите, кому отправить акт сверки
    </h1>
</div>

<?php $form = ActiveForm::begin([
    'id' => 'send-collate-form',
    'action' => [
        '/contractor/collate',
        'step' => 'send',
        'id' => $contractor->id,
        'accounting' => $model->accounting,
        'document' => $model->document,
        'from' => $model->dateFrom,
        'till' => $model->dateTill,
    ],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="form-body">
    <div class="form-group row">
        <div class="container-who-send-label">
            <?= $form->field($model, 'sendToChief', ['template' => "{label}\n{input}"])
            ->error(false)
            ->checkbox([
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
            ]); ?>
        </div>
        <div class="container-who-send-input chief-email">
            <?php if (empty($contractor->director_email)) : ?>
                <?php if (!empty($contractor->director_name)) : ?>
                    <span class="text-muted inline-block col-xs-6 mar-top-8 pad-left-none" style="padding-right: 15px;">
                        <?= $contractor->director_name; ?>
                    </span>
                <?php endif ?>
                <?= $form->field($model, 'chiefEmail', ['options' => ['class' => 'col-xs-6 pad-right-none pad-left-none']])
                ->label(false)
                ->textInput([
                    'placeHolder' => 'Укажите e-mail',
                    'class' => 'form-control width100',
                ]);?>
            <?php else : ?>
                <span class="text-muted mar-top-8 inline-block col-xs-6 pad-left-none">
                    <?= $contractor->director_name; ?>
                </span>
                <span class="text-muted mar-top-8 inline-block col-xs-6 pad-right-none pad-left-none">
                    <?= $contractor->director_email; ?>
                </span>
            <?php endif ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'sendToChief', ['template' => "{error}", 'options' => ['class' => '']])->error(); ?>
        </div>
    </div>

    <?php if (!$contractor->chief_accountant_is_director): ?>
        <div class="form-group row">
            <div class="container-who-send-label">
                <?= $form->field($model, 'sendToChiefAccountant')->checkbox(); ?>
            </div>
            <div class="container-who-send-input chief-email">
                <?php if (empty($contractor->chief_accountant_email)) : ?>
                    <?php if (!empty($contractor->chief_accountant_name)) : ?>
                        <span class="text-muted inline-block col-xs-6 mar-top-8 pad-left-none" style="padding-right: 15px;">
                            <?= $contractor->chief_accountant_name; ?>
                        </span>
                    <?php endif ?>
                    <?= $form->field($model, 'chiefAccountantEmail', ['options' => ['class' => 'col-xs-6 pad-right-none pad-left-none']])
                    ->label(false)
                    ->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php else : ?>
                    <span class="text-muted mar-top-8 inline-block col-xs-6 pad-left-none">
                        <?= $contractor->chief_accountant_name; ?>
                    </span>
                    <span class="text-muted mar-top-8 inline-block col-xs-6 pad-right-none pad-left-none">
                        <?= $contractor->chief_accountant_email; ?>
                    </span>
                <?php endif ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!$contractor->contact_is_director): ?>
        <div class="form-group row">
            <div class="container-who-send-label">
                <?= $form->field($model, 'sendToContact')->checkbox([
                    'labelOptions' => [
                        'style' => 'width: ' . $labelWidth . 'px;',
                    ],
                ]); ?>
            </div>
            <div class="container-who-send-input">
                <?php if (empty($contractor->contact_email)) { ?>
                    <?php if (!empty($contractor->contact_name)) { ?>
                        <span class="text-muted inline-block col-lg-6 col-md-6 mar-top-8 pad-left-none">
                            <?= $contractor->contact_name; ?>
                        </span>
                    <?php } ?>
                    <?= $form->field($model, 'contactAccountantEmail', ['options' => ['class' => 'col-lg-6 col-md-6 pad-right-none pad-left-none']])
                    ->label(false)
                    ->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php } else { ?>
                    <span class="text-muted" style="padding-right: 15px;">
                        <?= $contractor->contact_name; ?>
                    </span>
                    <span class="text-muted" style="padding-left: 15px;">
                        <?= $contractor->contact_email; ?>
                    </span>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group row">
        <div class="container-who-send-label">
            <?= $form->field($model, 'sendToOther')->checkbox([
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
            ]); ?>
        </div>
        <div class="container-who-send-input">
            <?= $form->field($model, 'otherEmail')->label(false)->textInput([
                'placeHolder' => 'Укажите e-mail',
                'class' => 'form-control width100' . (isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? ' tooltip-self-send' : ''),
                'data-tooltip-content' => isset($_COOKIE["tooltip_send-example_{$company->id}"]) ? '#tooltip_send-example' : null,
            ]); ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12">
            <?= Html::activeLabel($model, 'emailText', [
                'class' => 'control-label',
                'style' => 'cursor: pointer; border-bottom: 1px dashed #e5e5e5; margin-bottom: 0;',
            ]) ?>
            <div class="email_text_input" style="margin-top: -1px;">
                <?= $form->field($model, 'emailText')->textArea([
                    'rows' => 4,
                    'onkeyup' => 'emailTextResize()',
                    'style' => 'padding: 10px; width: 100%; resize: none; overflow: hidden; border-color: #e5e5e5 !important;',
                ])->label(false); ?>
            </div>
            <?php $this->registerJs('
                function emailTextResize() {
                    var inputArea = document.getElementById("collateform-emailtext")
                    $(inputArea).height(100);
                    $(inputArea).height(inputArea.scrollHeight);
                }
                $(document).on("change", "#collateform-emailtext", function() {
                    emailTextResize();
                });
                emailTextResize();
            '); ?>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="box-txt-come-your-mail">
        Ответное письмо клиента на данное письмо,
        <br>
        придет на вашу почту – <span><?= Yii::$app->user->identity->email; ?></span>
    </div>
    <?= Html::submitButton('ОК', [
        'class' => 'btn yellow pull-right',
    ]); ?>
</div>

<?php $form->end(); ?>

<?php $pjax->end(); ?>
