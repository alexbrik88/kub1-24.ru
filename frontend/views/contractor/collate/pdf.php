<?php

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$this->title = $model->pdfTitle;
?>

<div class="page-content-in p-center pad-pdf-p" style="padding-top: 0!important; padding-bottom: 0!important;">
<?= $this->render('pdf-view', ['model' => $model, 'addStamp' => true]) ?>
</div>
