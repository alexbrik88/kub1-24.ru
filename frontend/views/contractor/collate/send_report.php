<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */
/* @var $sent integer */

$company = $model->company;
$contractor = $model->contractor;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
?>

<?php $pjax = Pjax::begin([
    'id' => 'collate-pjax-container',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'linkSelector' => '.collate-pjax-link',
    'timeout' => 5000,
]); ?>

<div class="modal-header" style="margin: -15px -15px 15px; background-color: #00b7af; color: #fff;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1 style="font-size: 32px;">
        <span class="ico-Send-smart-pls fs pull-left" style="font-size:inherit;line-height:inherit;"></span>
        Отправка акта сверки
    </h1>
</div>

<div class="form-group">
    Отправлено писем: <?= $sent ?>
</div>

<div class="form-actions">
    <?= Html::tag('button', 'ОК', [
        'class' => 'btn yellow pull-right',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]); ?>
</div>

<?php $pjax->end(); ?>
