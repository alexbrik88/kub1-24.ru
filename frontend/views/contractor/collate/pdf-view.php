<?php

use common\components\ImageHelper;
use common\components\TextHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Contractor;
use common\models\company\CompanyType;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CollateForm */

$companyName = $model->company->getTitle(true);
$contractorName = $model->contractor->getShortName(true);
$agreementArray = $model->contractor->getAgreements()->all();
if (count($agreementArray) == 1) {
    $agreement = reset($agreementArray);
} else {
    $agreement = null;
}
$signatureLink = $printLink = null;
if (!empty($addStamp)) {
    $chiefSignatureImage = $model->company->getImage('chiefSignatureImage');
    if ($chiefSignatureImage) {
        $defaultSignatureHeight = 50;
        $signatureHeight = ImageHelper::getSignatureHeight($chiefSignatureImage, 50, 100);
        $signatureLink = EasyThumbnailImage::thumbnailSrc($chiefSignatureImage, 165, $signatureHeight, EasyThumbnailImage::THUMBNAIL_INSET_BOX);
    }
    $printImage = $model->company->getImage('printImage');
    if ($printImage) {
        $printLink = EasyThumbnailImage::thumbnailSrc($printImage, 150, 150, EasyThumbnailImage::THUMBNAIL_INSET_BOX);
    }
}
?>

<style type="text/css">
.collate-table th {
    font-weight: bold;
    text-align: center;
}
.collate-table th,
.collate-table td {
    border: 1px solid #ccc;
    padding: 5px 10px;
}
.parties-table td {
    border: none;
}
.p-center {
    width: 1000px;
}
.item:after {
    content: "";
    display: block;
    border-bottom: 1px solid blue;
}
</style>

<div style="margin: 0 0 15px; text-align: center; font-size: 33px;">
    Акт сверки
</div>
<div style="margin: 15px 0 20px; text-align: center; font-size: 14px;">
    взаимных расчетов за период: <?= $model->dateFrom . ' - ' . $model->dateTill ?>
    <br>
    между <?= $companyName ?> (ИНН <?= $model->company->inn ?>)
    и <?= $contractorName ?> (ИНН <?= $model->contractor->ITN ?>)
    <?php if ($agreement) : ?>
        по <?= $agreement->agreementType->name_dative ?>
        № <?= $agreement->document_number ?>
        <?php if ($d = date_create_from_format('Y-m-d', $agreement->document_date)) : ?>
            от <?= $d->format('d.m.Y') ?>
        <?php endif ?>
    <?php endif ?>
</div>

<div style="margin-bottom: 10px; font-size: 14px; text-align: justify;">
    Мы, нижеподписавшиеся, <?= $model->company->chief_post_name ?> <?= $companyName ?><?= $model->company->company_type_id !== CompanyType::TYPE_IP ? " {$model->company->getChiefFio()}" : null ?>,
    с одной стороны, и <?= $model->contractor->director_post_name ?> <?= $contractorName ?><?= $model->contractor->company_type_id !== CompanyType::TYPE_IP ? " {$model->contractor->director_name}" : null ?>,
    с другой стороны, составили настоящий акт сверки в том, что состояние взаимных расчетов по данным учета следующее:
</div>

<table id="collate-table" class="collate-table" cellspacing="0" style="width: 100%">
    <thead>
    <tr>
        <td colspan="4">
            По данным <?= $companyName ?>, руб.
        </td>
        <td colspan="4">
            По данным <?= $contractorName ?>, руб.
        </td>
    </tr>
    <tr>
        <td class="text-center" style="width: 5%;">Дата</td>
        <td class="text-center" style="width: 20%;">Документ</td>
        <td class="text-center" style="width: 10%;">Дебет</td>
        <td class="text-center" style="width: 10%;">Кредит</td>
        <td class="text-center" style="width: 5%;">Дата</td>
        <td class="text-center" style="width: 20%;">Документ</td>
        <td class="text-center" style="width: 10%;">Дебет</td>
        <td class="text-center" style="width: 10%;">Кредит</td>
    </tr>
    </thead>
    <tr>
        <td colspan="2" style="font-weight: bold;">Сальдо начальное</td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->initDebit ? number_format($model->initDebit / 100, 2, ',', ' ') : '';?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->initCredit ? number_format($model->initCredit / 100, 2, ',', ' ') : '';?>
        </td>
        <td colspan="2" style="font-weight: bold;">Сальдо начальное</td>
        <?php if (!$model->fillByContractor): ?>
            <td style="font-weight: bold; text-align: right;"></td>
            <td style="font-weight: bold; text-align: right;"></td>
        <?php else: ?>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->initCredit ? number_format($model->initCredit / 100, 2, ',', ' ') : '';?>
            </td>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->initDebit ? number_format($model->initDebit / 100, 2, ',', ' ') : '';?>
            </td>
        <?php endif; ?>
    </tr>
    <?php if ($model->getTableRows()) : ?>
        <?= $model->getTableRows() ?>
    <?php else : ?>
        <tr>
            <td colspan="8">За указанный период движений не было</td>
        </tr>
    <?php endif ?>
    <tr>
        <td colspan="2" style="font-weight: bold;">Обороты за период</td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->periodDebit ? number_format($model->periodDebit / 100, 2, ',', ' ') : ''; ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->periodCredit ? number_format($model->periodCredit / 100, 2, ',', ' ') : ''; ?>
        </td>
        <td colspan="2" style="font-weight: bold;">Обороты за период</td>
        <?php if (!$model->fillByContractor): ?>
            <td style="font-weight: bold; text-align: right;"></td>
            <td style="font-weight: bold; text-align: right;"></td>
        <?php else: ?>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->periodCredit ? number_format($model->periodCredit / 100, 2, ',', ' ') : ''; ?>
            </td>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->periodDebit ? number_format($model->periodDebit / 100, 2, ',', ' ') : ''; ?>
            </td>
        <?php endif; ?>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: bold;">Сальдо конечное</td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->totalDebit ? number_format($model->totalDebit / 100, 2, ',', ' ') : ''; ?>
        </td>
        <td style="font-weight: bold; text-align: right;">
            <?= $model->totalCredit ? number_format($model->totalCredit / 100, 2, ',', ' ') : ''; ?>
        </td>
        <td colspan="2" style="font-weight: bold;">Сальдо конечное</td>
        <?php if (!$model->fillByContractor): ?>
            <td style="font-weight: bold; text-align: right;"></td>
            <td style="font-weight: bold; text-align: right;"></td>
        <?php else: ?>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->totalCredit ? number_format($model->totalCredit / 100, 2, ',', ' ') : ''; ?>
            </td>
            <td style="font-weight: bold; text-align: right;">
                <?= $model->totalDebit ? number_format($model->totalDebit / 100, 2, ',', ' ') : ''; ?>
            </td>
        <?php endif; ?>
    </tr>
</table>

<div style="margin-top: 20px">
    <div style="background: url('<?= $signatureLink ?>'); background-repeat: no-repeat; background-position: 10px 120px;">
        <div style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: 185px bottom;">
            <table class="parties-table" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="width: 50%; padding-right: 23px;">
                        По данным <?= $companyName ?>
                        <br>
                        <span style="margin-top: 5px; font-weight: bold;display: block;">
                            на <?= $model->dateTill ?>
                            <?php if ($model->totalDebit || $model->totalCredit): ?> задолженность в пользу<?php endif; ?>
                            <?php if ($model->totalDebit) : ?>
                                <?= $companyName . ' ' . number_format($model->totalDebit / 100, 2, ',', ' ') . ' руб.' ?>
                                (<?= TextHelper::amountToWords($model->totalDebit / 100); ?>)
                            <?php elseif ($model->totalCredit) : ?>
                                <?= $contractorName . ' ' . number_format($model->totalCredit / 100, 2, ',', ' ') . ' руб.' ?>
                                (<?= TextHelper::amountToWords($model->totalCredit / 100); ?>)
                            <?php else : ?>
                                задолженности нет
                            <?php endif ?>
                        </span>
                        <br>
                        <br>
                        <br>
                    </td>
                    <td style="width: 50%;padding-left: 23px;">
                        <?php if ($model->fillByContractor): ?>
                            По данным <?= $contractorName ?>
                            <br>
                            <span style="margin-top: 5px; font-weight: bold;display: block;">
                                на <?= $model->dateTill ?>
                                <?php if ($model->totalDebit || $model->totalCredit): ?> задолженность в пользу<?php endif; ?>
                                <?php if ($model->fillByContractor): ?>
                                    <?php if ($model->totalDebit) : ?>
                                        <?= $companyName . ' ' . number_format($model->totalDebit / 100, 2, ',', ' ') . ' руб.' ?>
                                        (<?= TextHelper::amountToWords($model->totalDebit / 100); ?>)
                                    <?php elseif ($model->totalCredit) : ?>
                                        <?= $contractorName . ' ' . number_format($model->totalCredit / 100, 2, ',', ' ') . ' руб.' ?>
                                        (<?= TextHelper::amountToWords($model->totalCredit / 100); ?>)
                                    <?php else : ?>
                                        задолженности нет
                                    <?php endif ?>
                                <?php endif ?>
                            </span>
                            <br>
                            <br>
                            <br>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%; height: 60px; vertical-align: top;padding-right: 23px;">
                        От <?= $companyName ?>
                        <br>
                        <?= $model->company->chief_post_name ?>
                    </td>
                    <td style="width: 50%; height: 60px; vertical-align: top;padding-left: 23px;">
                        От <?= $contractorName ?>
                        <br>
                        <?= $model->contractor->director_post_name ?>
                    </td>
                </tr>
            </table>
            <table class="parties-table" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="width: 50%; height: 40px; vertical-align: bottom;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="border-bottom: 1px solid #333333; text-align: right;">
                                        <?= ($name = $model->company->getChiefFio(true)) ? '(' . $name . ')' : ''; ?>
                                    </td>
                                    <td style="width: 23px;"></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 50%; height: 40px;vertical-align: bottom;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 23px;"></td>
                                    <td style="border-bottom: 1px solid #333333; text-align: right;">
                                        (<?= $model->contractor->company_type_id !== CompanyType::TYPE_IP ? $model->contractor->getDirectorFio() : $contractorName ?>)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; height: 40px; vertical-align: top; padding-right: 23px;">
                            <span>
                                М.П.
                                <span class="pull-right">
                                    <?= $model->dateSigned ? $model->dateSigned . 'г.' : ''; ?>
                                </span>
                            </span>
                        </td>
                        <td style="width: 50%; height: 40px; vertical-align: top; padding-left: 23px;">
                            <span>
                                М.П.
                                <span class="pull-right">
                                    <?= $model->dateSigned ? $model->dateSigned . 'г.' : ''; ?>
                                </span>
                            </span>
                        </td>
                    </tr>
                </table>
        </div>
    </div>
</div>
