<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use common\widgets\Modal;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\bootstrap\Nav;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */
/* @var $user Employee */
/* @var $activeTab integer */
/* @var $analyticsMonthNumber string */
/* @var $sellingMonthNumber string */

$this->title = $model->nameWithType;
$this->context->layoutWrapperCssClass = 'customer';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-store',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

$ioType = $model->type;
$user = Yii::$app->user->identity;
$company = $user->company;
$agreement = $model->getAgreements()->orderBy([
    'IF([[document_type_id]] = 1, 0, 1)' => SORT_ASC,
    'document_date' => SORT_ASC,
])->one();
$isStoreActive = $model->getStoreCompanyContractors()
    ->andWhere(['status' => StoreCompanyContractor::STATUS_ACTIVE])
    ->exists();
$hasStore = $model->getStoreCompanyContractors()->one();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$disabled = !$canAddStoreCabinet && !$isStoreActive;
$invCount = $company->getInvoiceLeft();

$this->registerJs('
    $(document).on("change", "#activate_store_account", function() {
        var isChecked = $("#activate_store_account").prop("checked");
        $.post("' . Url::to(['store-account', 'id' => $model->id]) . '", $(this).serialize(), function(data) {
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value != undefined) {
                if (data.value == false) {
                    $icon = "error";
                }
                $("#activate_store_account").prop("checked", data.value);
            } else {
                $("#activate_store_account").prop("checked", !isChecked);
            }
            if (data.label != undefined) {
                   $(".contractor-store-label").text(data.label);
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    });
');
?>
    <a class="back-to-customers"
       href="<?= Url::to(['/contractor/index', 'type' => $model->type]) ?>">Назад к
        списку</a>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet customer-info">
                <div class="portlet-title">
                    <div class="col-md-9 caption">
                        <?= Html::encode($this->title); ?>
                    </div>
                    <div class="actions">
                        <a class="btn darkblue btn-sm info-button"
                           data-toggle="modal" href="#basic">
                            <i class="icon-info" style="color:white"></i>
                        </a>
                        <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::UPDATE, [
                            'model' => $model,
                        ])
                        ): ?>
                            <a href="<?= Url::to(['/contractor/update', 'type' => $model->type, 'id' => $model->id]) ?>"
                               class="darkblue btn-sm btn-link"
                               title="Редактировать">
                                <i class="icon-pencil"></i>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="portlet-body" style="margin-bottom: 10px;">
                    <table id="datatable_ajax" class="table"
                           style="margin-bottom: 0;">
                        <tr>
                            <td>
                                <span class="customer-characteristic">Тип:</span>
                                <span><?= ($model->type == Contractor::TYPE_CUSTOMER) ? 'покупатель' : 'поставщик'; ?></span>
                            </td>
                            <td>
                                <?php $name = $model->getRealContactName(); ?>
                                <?php if (!empty($name)): ?>
                                    <span
                                            class="customer-characteristic">Контакт:</span>
                                    <span><?= $name; ?></span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Налогообложение:</span>
                                <span><?php if ($model->taxation_system == Contractor::WITH_NDS) {
                                        echo 'с ндс';
                                    } elseif ($model->taxation_system == Contractor::WITHOUT_NDS) {
                                        echo 'без ндс';
                                    } else {
                                        echo Contractor::NOT_SET;
                                    } ?></span>
                            </td>
                            <td>
                                <?php $phone = $model->getRealContactPhone(); ?>
                                <?php if (!empty($phone)): ?>
                                    <span
                                            class="customer-characteristic">Телефон:</span>
                                    <span><?= $phone; ?></span>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="customer-characteristic">Статус:</span>
                                <span><?= ($model->status == Contractor::ACTIVE) ? 'активен' : 'не активен'; ?></span>
                            </td>
                            <td>
                                <?php $email = $model->getRealContactEmail(); ?>
                                <?php if (!empty($email)): ?>
                                    <span
                                            class="customer-characteristic">E-mail:</span>

                                    <a href="mailto:<?= $email; ?>"><?= $email; ?></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                    <?php if ($agreement) : ?>
                        <div style="padding-left: 8px;">
                            <b><?= $agreement->agreementType->name ?>:</b>
                            № <?= Html::encode($agreement->document_number) ?>
                            от <?= DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        </div>
                    <?php endif ?>
                    <?php if (YII_ENV_DEV || in_array(Yii::$app->user->identity->company->id, [2031, 11270, 349])): ?>
                        <table class="table no_mrg_bottom" style="width: auto;">
                            <tr>
                                <td style="line-height: 32px; white-space: nowrap;">
                                    <span class="customer-characteristic contractor-store-label">
                                        <?= $hasStore ? ($isStoreActive ? 'Отключить кабинет' : 'Включить кабинет') : 'Создать кабинет'; ?>
                                    </span>
                                </td>
                                <td style="padding-left: 0;">
                                    <table>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <label class="rounded-switch<?= $disabled ? ' tooltip2-store' : ''; ?>"
                                                       for="activate_store_account"
                                                       data-tooltip-content="<?= $disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : ''; ?>"
                                                       style="margin: 4px 0 0;">
                                                    <?= Html::checkbox('store_account', $isStoreActive, [
                                                        'id' => 'activate_store_account',
                                                        'class' => 'switch',
                                                        'disabled' => $disabled,
                                                        'data-toggle' => 'modal',
                                                        'data-target' => !$isStoreActive && !$disabled ?
                                                            (empty($model->director_email) ?
                                                                '#empty-contractor-email' :
                                                                '#add-store-account') : null,
                                                    ]); ?>
                                                    <span class="sliderr no-gray yes-yellow round"></span>
                                                </label>
                                                <?php if ($disabled): ?>
                                                    <div class="tooltip-template" style="display: none;">
                                                    <span id="tooltip_pay-to-add-cabinet-<?= $model->id; ?>"
                                                          style="display: inline-block; text-align: center;">
                                                        Кабинет можно добавить только после оплаты
                                                    </span>
                                                    </div>
                                                <?php endif; ?>
                                            </td>
                                            <td style="vertical-align: top;">
                                        <span class="tooltip2-right ico-question valign-middle"
                                              data-tooltip-content="<?= $isStoreActive ? '#tooltip_add_stamp' : '#tooltip_add_cabinet'; ?>"
                                              style="vertical-align: top;">
                                        </span>
                                                <?php if (!$isStoreActive): ?>
                                                    <div class="tooltip-template" style="display: none;">
                                                        <span id="tooltip_add_cabinet"
                                                              style="display: inline-block;">
                                                            Передвиньте бегунок, что бы дать доступ покупателю к <br>
                                                            остаткам на вашем складе и он смог самостоятельно <br>
                                                            формировать заказы и выставлять себе счета
                                                        </span>
                                                    </div>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?php if ($model->type == Contractor::TYPE_CUSTOMER && !in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_ACCOUNTANT, EmployeeRole::ROLE_ASSISTANT])): ?>
                <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index', 'PaymentReminderMessageContractorSearch' => [
                    'contractor' => $model->id,
                ]]), [
                    'class' => 'btn yellow',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <div class="btn-group pull-right title-buttons portlet" style="margin-bottom: 15px;">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                        'ioType' => $ioType,
                    ])
                    && $model->company_id !== null
                ): ?>
                    <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($ioType)) : ?>
                        <a href="<?= Url::to(['/documents/invoice/create', 'type' => $ioType, 'contractorId' => $model->id]); ?>"
                           class="btn yellow">
                            <i class="fa fa-plus"></i> СЧЕТ
                        </a>
                    <?php else : ?>
                        <button class="btn yellow action-is-limited"
                            <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                            <i class="fa fa-plus"></i> СЧЕТ
                        </button>
                        <?php if ($invCount === 0 && date(DateHelper::FORMAT_USER_DATE, $company->show_invoice_payment_popup_date) == date(DateHelper::FORMAT_USER_DATE)): ?>
                            <?= $this->render('@frontend/widgets/views/invoicePaymentModal', [
                                'company' => $company,
                            ]); ?>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif; ?>
            </div>

            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>

            <div class="btn-group pull-right title-buttons portlet" style="margin-top: 10px;width: 52%;">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)) {
                    $url = Url::to(['/contractor/collate', 'step' => 'form', 'id' => $model->id]);
                    Modal::begin([
                        'id' => 'modal-collate-report',
                        'closeButton' => false,
                        'toggleButton' => [
                            'tag' => 'button',
                            'id' => 'collate-report-button',
                            'label' => '<i class="fa fa-plus"></i> Акт сверки',
                            'class' => 'btn yellow',
                            'style' => 'padding: 7px 10px; border-radius: 0; text-transform: uppercase; float: right;',
                        ],
                    ]);

                    $pjax = Pjax::begin([
                        'id' => 'collate-pjax-container',
                        'enablePushState' => false,
                        'enableReplaceState' => false,
                        'linkSelector' => '.collate-pjax-link',
                        'timeout' => 5000,
                    ]);

                    $pjax->end();

                    Modal::end();

                    $this->registerJs('
                $(document).on("click", "#collate-report-button", function() {
                    $.pjax.reload("#collate-pjax-container", {url: ' . json_encode($url) . ', push: false, replace: false, timeout: 5000})
                });
                $(document).on("pjax:complete", "#collate-pjax-container", function(event) {
                    if ($("#collate-pjax-container .date-picker").length) {
                        $("#collate-pjax-container .date-picker").datepicker({
                            format: "dd.mm.yyyy",
                            keyboardNavigation: false,
                            forceParse: false,
                            language: "ru",
                            autoclose: true
                        }).on("change.dp", dateChanged);

                        function dateChanged(ev) {
                            if (ev.bubbles == undefined) {
                                var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                                if (ev.currentTarget.value == "") {
                                    if ($input.data("last-value") == null) {
                                        $input.data("last-value", ev.currentTarget.defaultValue);
                                    }
                                    var $lastDate = $input.data("last-value");
                                    $input.datepicker("setDate", $lastDate);
                                } else {
                                    $input.data("last-value", ev.currentTarget.value);
                                }
                            }
                        };
                    }
                });
                $(document).on("hidden.bs.modal", "#modal-collate-report", function () {
                    $("#collate-pjax-container").html("");
                })
                ');
                } ?>
            </div>
        </div>
        <?php if ($ioType == Contractor::TYPE_CUSTOMER && (YII_ENV_DEV || in_array($model->company_id, [1, 486, 21474]))): ?>
            <div class="col-md-6 collapse navbar-collapse contractor-navbar">
                <?= Nav::widget([
                    'id' => 'contractor-menu',
                    'items' => [
                        [
                            'label' => 'Счета',
                            'url' => '#tab1',
                            'options' => [
                                'class' => $activeTab == Contractor::TAB_INVOICE ? 'active' : null,
                            ],
                            'linkOptions' => [
                                'data-toggle' => 'tab',
                            ],
                        ],
                        [
                            'label' => 'Аналитика',
                            'url' => '#tab2',
                            'options' => [
                                'class' => $activeTab == Contractor::TAB_ANALYTICS ? 'active' : null,
                            ],
                            'linkOptions' => [
                                'data-toggle' => 'tab',
                            ],
                        ],
                        [
                            'label' => 'Продажи',
                            'url' => '#tab3',
                            'options' => [
                                'class' => $activeTab == Contractor::TAB_SELLING ? 'active' : null,
                            ],
                            'linkOptions' => [
                                'data-toggle' => 'tab',
                            ],
                        ],
                    ],
                    'options' => ['class' => 'nav-tabs'],
                ]); ?>
            </div>
        <?php endif; ?>
    </div>
<?php if ($ioType == Contractor::TYPE_CUSTOMER && (YII_ENV_DEV || in_array($model->company_id, [1, 486, 21474]))): ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab <?= $activeTab == Contractor::TAB_INVOICE ? 'active' : null; ?>">
            <?= $this->render('view/tab_invoice', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'company' => $company,
                'ioType' => $ioType,
            ]); ?>
        </div>
        <div id="tab2" class="tab-pane analytics-tab <?= $activeTab == Contractor::TAB_ANALYTICS ? 'active' : null; ?>">
            <?= $this->render('view/tab_analytics', [
                'model' => $model,
                'analyticsMonthNumber' => $analyticsMonthNumber,
            ]); ?>
        </div>
        <div id="tab3" class="tab-pane selling-tab <?= $activeTab == Contractor::TAB_SELLING ? 'active' : null; ?>">
            <?= $this->render('view/tab_selling', [
                'model' => $model,
                'sellingMonthNumber' => $sellingMonthNumber,
            ]); ?>
        </div>
    </div>
<?php else: ?>
    <?= $this->render('view/tab_invoice', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'company' => $company,
        'ioType' => $ioType,
    ]); ?>
<?php endif; ?>
<?php $this->registerJs('
    $(document).ready(function () {
        $(".contractor-navbar").css("top", (+$(".customer-info").height() + 5) + "px");
    });
'); ?>