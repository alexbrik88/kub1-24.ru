<?php

use common\models\Agreement;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \common\models\Contractor */

//$this->title = 'Обновить контрагента: ' . ' ' . $model->name;
$this->context->layoutWrapperCssClass = 'create-counterparty';

echo $this->render('form/_form', [
    'model' => $model,
    'type' => $type,
    'face_type_opt' => 1,
]);