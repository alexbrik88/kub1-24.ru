<?php

use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\ContractorAccount */
/* @var $contractor common\models\Contractor */

?>

<?php Modal::begin([
    'id' => 'contractor-account-delete-modal-' . $model->id,
    'closeButton' => false,
    'options' => [
        'class' => 'confirm-modal fade',
    ],
    'toggleButton' => [
        'tag' => 'a',
        'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
        'title' => 'Удалить',
    ],
]); ?>
<div class="form-body">
    <div class="row">Вы уверены, что хотите удалить расчетный счет?</div>
</div>
<div class="form-actions row">
    <div class="col-xs-6">
        <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
            'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button contractor-account-delete',
            'data' => [
                'style' => 'expand-right',
                'url' => Url::to([
                    '/contractor/account-delete',
                    'cid' => $contractor->id,
                    'id' => $model->id,
                ]),
                'params' => [
                    '_csrf' => Yii::$app->request->csrfToken,
                ],
            ],
        ]); ?>
    </div>
    <div class="col-xs-6">
        <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
    </div>
</div>
<?php Modal::end(); ?>
