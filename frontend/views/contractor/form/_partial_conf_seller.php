<?php

use common\models\Contractor;
use frontend\modules\crm\models\ClientForm;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use frontend\widgets\ExpenditureDropdownWidget;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\Contractor */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$delayInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-6',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$textAreaConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-2',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-10',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$discountInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-6',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-3',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template'=>"{label}\n{beginWrapper}\n<div class=\"input-group\">{input}\n<span style=\"display:table-cell;padding-left:12px;vertical-align:middle;\">%</span></div>\n{endWrapper}\n{hint}\n{error}"
];

$responsibleEmployers = ArrayHelper::map($model->company->getEmployeeCompanies()
    ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])->all(), 'employee_id', 'fio');
?>

<div class="portlet box darkblue">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'seller_payment_delay', $delayInputConfig)->textInput([
                    'maxlength' => true,
                    'style' => 'width: 100%;',
                    'type' => 'number',
                    'min' => 0,
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'seller_discount', $discountInputConfig)->input('number', [
                    'min' => 0,
                    'max' => 99.99,
                    'step' => 'any',
                    'style' => 'width: 100%;',
                ])->label('Фиксированная скидка на всё, %'); ?>
            </div>
        </div>
    </div>
</div>
