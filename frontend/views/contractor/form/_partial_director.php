<?php
/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $textInputConfig string */

use yii\helpers\Html; ?>

<div class="col-md-4 legal">
    <div class="portlet box box-leader darkblue">
        <div class="portlet-title">
            <div class="caption">Руководитель</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">
            <?= $form->field($model, 'director_post_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_name', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group required',
                ],
            ]))->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_email', $textInputConfig)->textInput([
                'id' => 'legal-director_email',
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'director_phone', $textInputConfig)->textInput([
                'id' => 'legal-director_phone',
                'class' => 'form-control phone_input',
            ]); ?>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'director_in_act', [
                        'class' => 'fio_in_act',
                        'label' => false,
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>