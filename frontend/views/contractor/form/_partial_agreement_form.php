<?php

use common\models\AgreementType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Agreement */
/* @var $form yii\widgets\ActiveForm */

$header = \Yii::$app->controller->action->id == 'agreement-create' ? 'Добавить договор' : 'Редактировать договор';
Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]) ?>

<style type="text/css">
.agreement-form .form-control {width: 100%;}
</style>
<div class="agreement-form" data-header="<?= $header ?>">

    <?php $form = ActiveForm::begin([
        'id' => 'contractor-agreement-form',
        'enableClientValidation' => false,
        'action' => ['agreement-update', 'id' => $model->id],
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
            ],
        ],
        'options' => [
            'data-pjax' => true,
            'data-max-files' => 5,
        ],
    ]); ?>

    <?= $form->field($model, 'document_type_id')->dropDownList(
        AgreementType::find()->select(['name', 'id'])->orderBy(['sort' => SORT_ASC])->indexBy('id')->column()
    ) ?>

    <?= $form->field($model, 'document_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'document_date_input', [
        'template' => Yii::$app->params['formDatePickerTemplate'],
    ])->textInput(['class' => 'form-control date-picker']) ?>

    <?= \common\models\file\widgets\FileUpload::widget([
        'uploadUrl' => Url::to(['agreement-file-upload', 'id' => $model->id,]),
        'deleteUrl' => Url::to(['agreement-file-delete', 'id' => $model->id,]),
        'listUrl' => Url::to(['agreement-file-list', 'id' => $model->id,]),
    ]); ?>

    <div class="action-buttons row">
        <div class="spinner-button col-md-4 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width:130px!important;',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', ['class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Сохранить',])   ?>
        </div>
        <div class="col-xs-7"></div>
        <div class="col-md-8 col-xs-1" style="float: right;">
            <button type="button" class="btn darkblue btn-cancel darkblue widthe-100 hidden-md hidden-sm hidden-xs float-right" data-dismiss="modal" style="width: 130px !important;">Отмена</button>
            <button type="button" class="btn darkblue btn-cancel darkblue widthe-100 hidden-lg" title="Отмена"><i class="fa fa-reply fa-2x"></i></button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
<?php
if (is_file($file = \Yii::getAlias('@common/assets/web/scripts/upload.js'))) {
    echo $this->renderFile($file);
}
?>
</script>
<?php Pjax::end() ?>
