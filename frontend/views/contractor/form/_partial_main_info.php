<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\address\Country;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-5 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$checkboxConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-5 inp_one_line width-inp',
        'style' => 'padding-top: 8px;'
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$inputListConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];

$helpAccounting = Html::tag('span', '', [
    'class' => 'tooltip2 ico-question',
    'style' => 'display: inline-block; margin: -6px 5px; vertical-align: middle;',
    'data-tooltip-content' => '#tooltip_not_accounting',
]);


$isForeign = ($model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON);
?>

<div class="form-group form-group_width">
    <?=
    $form->field($model, 'ITN', array_merge($textInputConfig, [
        'options' => [
            'class' => $model->face_type == 2 ? 'legal' : 'legal required',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}<p class='exists-contractor'></p>\n{endWrapper}",
    ]))->label('ИНН:')->textInput([
        'placeholder' => $model->face_type == 2 ? '' : 'Автозаполнение по ИНН' ,
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'prod' => YII_ENV_PROD ? 1 : 0,
        ],
    ]);
    ?>
    <?=
    $form->field($model, 'physical_lastname', array_merge($textInputConfig, [
        'options' => [
            'class' => 'physical required',
        ],
    ]))->label('Фамилия:')->textInput([
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
        ],
    ]);
    ?>
    <?=
    $form->field($model, 'face_type', array_merge($textInputConfig, [
        'options' => [
            'id' => 'contractor_company_type',
            'class' => 'one_line_date_dismissal',
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-1 control-label_no-marg control-w',
        ],
        'inputOptions' => array_merge($face_type_opt, [
            'class' => 'form-control m-l-n sel-w',
        ]),
        'wrapperOptions' => [
            'class' => 'col-md-2 inp_one_line',
        ],
    ]))->label('Юр/Физ лицо')->dropDownList(
        Contractor::$contractorFaceType, [
            'id' => 'contractor_face_type',
        ]
    );
    ?>
</div>
<div class="legal form-group form-group_width">
    <?= $form->field($model, 'name', array_merge($textInputConfig, [
        'options' => [
            'class' => 'required',
        ],
    ]))->label('Название контрагента:')->textInput([
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
            'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
        ],
    ]); ?>

    <?= $form->field($model, 'companyTypeId', array_merge($textInputConfig, [
        'options' => [
            'id' => 'contractor_company_type',
            'class' => 'one_line_date_dismissal not_foreign_legal required',
            'style' => $isForeign ? 'display: none;' : null,
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-1 control-w control-label_no-marg not_foreign_legal',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-2 inp_one_line not_foreign_legal',
            'style' => $isForeign ? 'display: none;' : null,
        ],
    ]))->label('Форма')->dropDownList(
        $model->getTypeArray(),
    ); ?>
    <?= $form->field($model, 'foreign_legal_form', array_merge($textInputConfig, [
        'options' => [
            'class' => 'one_line_date_dismissal foreign_legal required',
            'style' => $isForeign ? null : 'display: none;',
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-1 control-w control-label_no-marg foreign_legal',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-2 inp_one_line foreign_legal',
            'style' => $isForeign ? null : 'display: none;',
        ],
    ]))->textInput([
        'maxlength' => true,
        'placeholder' => 'Пример: LCC',
    ])->label('Форма'); ?>
</div>
<div class="foreign_legal form-group form-group_width" style="<?= $isForeign ? null : 'display: none;'; ?>">
    <?= $form->field($model, 'decoding_legal_form', array_merge($textInputConfig, [
        'options' => [
            'class' => 'required',
        ],
    ]))->textInput([
        'maxlength' => true,
    ])->label('Pacшифровка формы'); ?>
</div>

<div class="physical form-group form-group_width">
    <?=
    $form->field($model, 'physical_firstname', array_merge($textInputConfig, [
        'options' => [
            'class' => 'physical required margin-bottom-15',
        ],
    ]))->label('Имя:')->textInput([
        'maxlength' => true,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
        ],
    ]);
    ?>
</div>
<div class="physical form-group form-group_width margin-bottom-n">
    <?=
    $form->field($model, 'physical_patronymic', array_merge($textInputConfig, [
        'options' => [
            'class' => 'physical required margin-bottom-15',
        ],
    ]))->label('Отчество:')->textInput([
        'maxlength' => true,
        'readonly' => (boolean) $model->physical_no_patronymic,
        'data' => [
            'toggle' => 'popover',
            'trigger' => 'focus',
            'placement' => 'bottom',
        ],
    ]);
    ?>
</div>
<div class="physical form-group form-group_width">
    <?= $form->field($model, 'physical_no_patronymic', array_merge($checkboxConfig, [
        'options' => [
            'class' => 'physical required margin-bottom-15',
        ],
    ]))->label('Нет отчества:')->checkbox([], false); ?>
</div>

<div class="form-group form-group_width margin-bottom-15">
    <?= $form->field($model, 'not_accounting', array_merge($checkboxConfig, [
        'options' => [
            'class' => '',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}{$helpAccounting}\n{error}\n{endWrapper}",
    ]))->checkbox([], false); ?>
    <div class="hidden">
        <span id="tooltip_not_accounting">
            Документы по такому клиенту не попадают в выгрузку для 1с и видны только для руководителя
        </span>
    </div>
    <?php if ($model->getCanHasOpposite()) : ?>
        <?php
        $model->opposite = (int) $model->getHasOpposite();
        $helpOpposite = Html::tag('span', '', [
            'class' => 'tooltip2-opposite tooltip2 ico-question',
            'style' => '',
            'data-tooltip-content' => '#tooltip_has_opposite',
        ]);
        ?>
        <?= $form->field($model, 'opposite', array_merge($checkboxConfig, [
            'options' => [
                'class' => '',
            ],
            'labelOptions' => [
                'class' => 'control-label col-md-1 control-w',
                'style' => 'white-space:nowrap;',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-2 opposite-checkbox',
                'style' => 'padding-top: 8px;',
            ],
            'template' => "{label}\n{beginWrapper}\n{input}{$helpOpposite}\n{error}\n{endWrapper}",
        ]))->checkbox([
            'disabled' => $model->getHasOpposite(),
        ], false); ?>
        <div class="hidden">
            <span id="tooltip_has_opposite">
                Поставив галочку, данный контрагент будет отображаться и в Покупателях и Поставщиках
            </span>
        </div>
    <?php endif ?>
</div>

<div class="legal">
    <?= $form->field($model, 'taxation_system', $inputListConfig)->label()->radioList(
        [
            Contractor::WITH_NDS => 'С НДС',
            Contractor::WITHOUT_NDS => 'Без НДС',
        ],
        [
            'class' => 'radio-list inp_one_line-contractor',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label', Html::radio($name, $checked, [
                        'value' => $value
                    ]) . $label, [
                    'class' => 'radio-inline width120 marg_left mrg_lf_checkbox',
                ]);
            },
        ]
    ); ?>
</div>

<?= $form->field($model, 'status', $inputListConfig)->label()->radioList(
    [
        Contractor::ACTIVE => 'Активен',
        Contractor::INACTIVE => 'Не активен',
    ], [
    'class' => 'radio-list inp_one_line-contractor',
    'item' => function ($index, $label, $name, $checked, $value) {
        return Html::tag('label', Html::radio($name, $checked, [
                'value' => $value,
            ]) . $label, [
            'class' => 'radio-inline width120 marg_left mrg_lf_checkbox',
        ]);
    },
]);
?>
<div class="form-group">
    <?=
    $form->field($model, 'order_currency', array_merge($textInputConfig, [
        'options' => [
            'id' => 'contractor_order_currency',
            'class' => 'foreign_legal',
            'style'=>'display:none'
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-2 control-label_no-marg',
        ],
        'inputOptions' => [
            'class' => 'form-control m-l-n sel-w',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5 inp_one_line',
        ],
    ]))->label('Выставлять счета в:')->dropDownList(
        \common\models\currency\Currency::getAllCurrency(), [
            'id' => 'order_currency',
        ]
    );
    ?>
    <?=
    $form->field($model, 'order_currency', array_merge($textInputConfig, [
        'options' => [
            'id' => 'contractor_company_type',
            'class' => 'one_line_date_dismissal foreign_legal',
            'style' => [
                'display' =>'none'
            ],
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-3 control-label_no-marg control-w text-right',
        ],
        'inputOptions' => array_merge($face_type_opt, [
            'class' => 'form-control m-l-n sel-w',
        ]),
        'wrapperOptions' => [
            'class' => 'col-md-3 inp_one_line',
        ],
    ]))->label('Страна')->dropDownList(
        Country::find()->select(['name_short', 'id'])->indexBy('id')->column(),
                ['prompt' => 'Выберите страну']
    );
    ?>

    <div class="foreign_legal col-md-5 text-left contractor-currency-disclaimer">
        <i>*Конвертация в RUB происходит автоматически, на день оплаты по курсу банка России.</i></div>
</div>
