<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */
/* @var $textInputConfig array */

$requiredInputConfig = $textInputConfig;
if (isset($requiredInputConfig['options']['class'])) {
    $requiredInputConfig['options']['class'] .= ' required';
} else {
    $requiredInputConfig['options']['class'] = ' required';
}
?>



<div class="col-md-4 legal">
    <div class="portlet box box-contact darkblue">
        <div class="portlet-title">
            <div class="caption">Контакт</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'contact_is_director', [
                        'id' => 'contact_is_director_input',
                        'label' => false,
                    ]); ?>
                    Совпадает с руководителем
                </label>
            </div>

            <?= $form->field($model, 'contact_name', $requiredInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'contact_email', $textInputConfig)->textInput([
                'maxlength' => true,
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <?= $form->field($model, 'contact_phone', $textInputConfig)->textInput([
                'class' => 'form-control phone_input',
                'disabled' => $model->contact_is_director? true: false,
            ]); ?>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'contact_in_act', [
                        'class' => 'fio_in_act',
                        'label' => false,
                        'disabled' => $model->contact_is_director? true: false
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
            </div>
        </div>
    </div>
</div>
