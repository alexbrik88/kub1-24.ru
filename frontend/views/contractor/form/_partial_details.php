<?php
/* @var $model common\models\Contractor */
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$textInputConfig2 = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
        'placeHolder' => 'Нужен для Актов и Товарных накладных',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$isForeign = $model->isForeign;
?>

    <div class="portlet box darkblue legal">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="contractor-local<?= $isForeign ? ' hidden' : '' ?>">
                        <?php if ($model->company_type_id !== CompanyType::TYPE_IP): ?>
                            <?= $form->field($model, 'PPC', $textInputConfig)->textInput([
                                'maxlength' => false,
                            ]); ?>
                        <?php endif; ?>
                        <?= $form->field($model, 'BIN', $textInputConfig)->textInput([
                            'maxlength' => true,
                        ])->label($model->company_type_id == \common\models\company\CompanyType::TYPE_IP ? 'ОГРНИП' : 'ОГРН'); ?>
                        <?= $form->field($model, 'okpo', $textInputConfig)->textInput([
                            'maxlength' => true,
                        ]); ?>
                    </div>
                    <div class="contractor-foreign<?= $isForeign ? '' : ' hidden' ?>">
                        <?= Html::activeHiddenInput($model, 'PPC', [
                            'id' => 'foreign-contractor-ppc',
                            'value' => '',
                            'disabled' => !$isForeign,
                        ]) ?>
                        <?= Html::activeHiddenInput($model, 'BIN', [
                            'id' => 'foreign-contractor-bin',
                            'value' => '',
                            'disabled' => !$isForeign,
                        ]) ?>
                        <?= Html::activeHiddenInput($model, 'okpo', [
                            'id' => 'foreign-contractor-okpo',
                            'value' => '',
                            'disabled' => !$isForeign,
                        ]) ?>
                    </div>
                    <?= $form->field($model, 'legal_address', $textInputConfig)->textInput([
                        'maxlength' => true,
                    ]); ?>
                    <?= $form->field($model, 'actual_address', $textInputConfig)->textInput([
                        'maxlength' => true,
                    ]); ?>
                    <?= $form->field($model, 'postal_address', $textInputConfig)->textInput([
                        'maxlength' => true,
                    ]); ?>
                </div>
                <?php if ($model->isNewRecord) : ?>
                    <div class="col-md-6">
                        <div class="contractor-local<?= $isForeign ? ' hidden' : '' ?>">
                            <?= $form->field($model, 'current_account', $textInputConfig2)->textInput([
                                'maxlength' => '20',
                            ]); ?>

                            <?= $form->field($model, 'BIC', $textInputConfig2)->widget(\common\components\widgets\BikTypeahead::classname(), [
                                'remoteUrl' => Url::to(['/dictionary/bik']),
                                'related' => [
                                    '#' . Html::getInputId($model, 'bank_name') => 'name',
                                    '#' . Html::getInputId($model, 'bank_city') => 'city',
                                    '#' . Html::getInputId($model, 'corresp_account') => 'ks',
                                ],
                            ])->textInput(['placeHolder' => 'Нужен для Актов и Товарных накладных']); ?>

                            <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                            <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                            <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
                                'maxlength' => true,
                                'readonly' => true,
                            ]); ?>
                        </div>
                        <div class="contractor-foreign<?= $isForeign ? '' : ' hidden' ?>">
                            <?= $form->field($model, 'current_account', $textInputConfig2)->textInput([
                                'id' => 'foreign-contractor-current_account',
                                'disabled' => !$isForeign,
                                'maxlength' => '35',
                            ]); ?>

                            <?= $form->field($model, 'BIC', $textInputConfig2)->textInput([
                                'id' => 'foreign-contractor-bic',
                                'disabled' => !$isForeign,
                                'placeHolder' => 'Нужен для Актов и Товарных накладных',
                            ]); ?>

                            <?= $form->field($model, 'bank_name', $textInputConfig)->textInput([
                                'id' => 'foreign-contractor-bank_name',
                                'disabled' => !$isForeign,
                            ]); ?>
                            <?= $form->field($model, 'bank_city', $textInputConfig)->textInput([
                                'id' => 'foreign-contractor-bank_city',
                                'disabled' => !$isForeign,
                            ]); ?>
                            <?= $form->field($model, 'corresp_account', $textInputConfig)->textInput([
                                'id' => 'foreign-contractor-corresp_account',
                                'disabled' => !$isForeign,
                            ]); ?>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="col-sm-12">
                        <label class="control-label label-width">Банковские реквизиты</label>
                        <?= $this->render('_partial_account', ['contractor' => $model]) ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>

    <div class="portlet box darkblue physical contractor-passport-info-wrapper">
        <div class="portlet-body min-width-container-passport">
            <div class="row">
                <div class="col-xs-12 pad0">
                    <div class="col-md-6">
                        <div class="form-group field-contractor-physical_passport_isrf">
                            <?= $form->field($model, 'physical_passport_isRf', array_merge($textInputConfig, ['options' => ['class' => '']]))
                                ->radioList(['1' => 'РФ', '0' => 'не РФ'], [
                                    'item' => function ($index, $label, $name, $checked, $value) {
                                        return Html::tag('label',
                                            Html::radio($name, $checked, ['value' => $value]) . $label,
                                            [
                                                'class' => 'radio-inline p-o radio-padding',
                                            ]);
                                    },
                                ]); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group field-physical-passport-country <?= $model->physical_passport_isRf == 1 ? 'hide' : '' ?>">
                            <?= $form->field($model, 'physical_passport_country', array_merge($textInputConfig, [
                                'options' => [
                                    'class' => '',

                                ],
                            ]))
                            ?>
                        </div>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="form-group field-contractor-ppc">
                        <?=
                        $form->field($model, 'physical_passport_series', array_merge($textInputConfig, ['options' => ['class' => '']]))
                            ->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => ($model->physical_passport_isRf == 1) ? '9{2} 9{2}' : '[9|a| ]{1,25}',
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => ($model->physical_passport_isRf == 1) ? 'XX XX' : '',
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="form-group field-contractor-bin">
                        <?=
                        $form->field($model, 'physical_passport_issued_by', array_merge($textInputConfig, [
                            'options' => [
                                'class' => '',
                            ],
                        ]))->label('Кем выдан:')->textInput([
                            'maxlength' => false,
                            'data' => [
                                'toggle' => 'popover',
                                'trigger' => 'focus',
                                'placement' => 'bottom',
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="form-group field-contractor-legal_address">
                        <?=
                        $form->field($model, 'physical_passport_department', array_merge($textInputConfig, [
                            'options' => [
                                'class' => '',
                            ],
                        ]))->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => ($model->physical_passport_isRf == 1) ? '9{3}-9{3}' : '[9|a| ]{1,255}' ,
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ($model->physical_passport_isRf == 1) ? 'XXX-XXX' : '',
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="form-group form-contractor-physical_address">
                        <?= $form->field($model, 'physical_address', array_merge($textInputConfig, [
                            'options' => [
                                'class' => 'physical required',
                            ],
                        ]))->label('Адрес регистрации:')->textInput([
                            'maxlength' => true,
                            'data' => [
                                'toggle' => 'popover',
                                'trigger' => 'focus',
                                'placement' => 'bottom',
                            ],
                        ]); ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group field-contractor-current_account">
                        <?=
                        $form->field($model, 'physical_passport_number', array_merge($textInputConfig, [
                            'options' => [
                                'class' => '',
                            ],
                        ]))->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => ($model->physical_passport_isRf == 1) ? '9{6}' : '[9|a| ]{1,25}',
                            //'regex'=> "[0-9]*",
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => ($model->physical_passport_isRf == 1) ? 'XXXXXX' : '',
                            ],
                        ]);
                        ?>
                    </div>

                    <div class="form-group field-contractor-current_account">
                        <div class="field-contractor-physical_passport_number">
                            <label for="under-date" class="control-label col-md-6 label-width">Дата выдачи:</label>

                            <div class="col-md-6 inp_one_line width-inp">
                                <div class="input-icon">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'physical_passport_date_output', [
                                        'id' => 'under-date',
                                        'class' => 'form-control date-picker',
                                        'data-date-viewmode' => 'years',
                                        'value' => $model->physical_passport_date_output ?
                                            date('d.m.Y', strtotime($model->physical_passport_date_output)): '',
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        var Passport_isRf = (function () {
            var inputs = {
                'contractor-physical_passport_series': {"mask": "9{2} 9{2}"},
                'contractor-physical_passport_department': {"mask": "9{3}-9{3}"},
                'contractor-physical_passport_number': {"mask": "9{6}"}
            };
            var placeholders = {
                'contractor-physical_passport_series': 'XX XX',
                'contractor-physical_passport_department': 'XXX-XXX',
                'contractor-physical_passport_number': 'XXXXXX'
            };
            var inputsFonNotRf = {
                'contractor-physical_passport_series': {"mask": "[9|a| ]{1,25}"},
                'contractor-physical_passport_department': {"mask": "[9|a]{1,255}"},
                'contractor-physical_passport_number': {"mask": "[9|a]{1,25}"}
            };

            var init = function(){

            };

            var hideInputsMask = function () {
                $.each(inputsFonNotRf, function (o, val) {
                    if ($("#" + o).inputmask) {
                        $("#" + o).inputmask(val);
                        $("#" + o).attr('placeholder','');
                    }
                });
            };

            var showInputsMask = function () {
                $.each(inputs, function (o, val) {
                    var inp = document.getElementById(o);
                    if (inp) {
                        $(inp).inputmask(val);
                        $(inp).attr('placeholder',placeholders[o]);
                    }
                });
            };


            return {hideInputsMask: hideInputsMask, showInputsMask: showInputsMask, init: init}
        })();



        window.addEventListener('load', function () {
            $(".radio-inline").click(function () {
                var radio = $(this).find('input:radio');

                if (radio.length > 0) {
                    radio = radio[0];
                    if (radio.name == 'Contractor[physical_passport_isRf]') {
                        if (radio.value == 0) {
                            $('.field-physical-passport-country').removeClass('hide');
                            Passport_isRf.hideInputsMask();
                        }
                        else {
                            $('.field-physical-passport-country').addClass('hide');
                            Passport_isRf.showInputsMask();
                        }
                    }
                }
            });
        });

    </script>


<?php
$this->registerJS(<<<JS
JS
);
