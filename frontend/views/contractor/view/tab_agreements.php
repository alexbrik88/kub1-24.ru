<?php

use common\components\grid\GridView;
use common\models\Agreement;
use common\models\EmployeeCompany;
use common\models\file\widgets\FileUpload;
use frontend\widgets\BtnConfirmModalWidget;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\components\grid\DropDownSearchDataColumn;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model Contractor */
/* @var $searchAgreementModel \frontend\modules\documents\models\AgreementSearch */
/* @var $dataAgreementProvider yii\data\ActiveDataProvider */

?>

<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right title-buttons portlet" style="margin-bottom: 15px;">
            <?= Html::a('<i class="fa fa-plus" style="padding: 0;"></i> ДОГОВОР', '#', [
                'class' => 'btn yellow agreement-modal-link',
                'data-url' => Url::to(['/documents/agreement/create', 'contractor_id' => $model->id, 'type' => $model->type, 'returnTo' => 'contractor'])
            ]) ?>
        </div>
    </div>
</div>

<?php Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'timeout' => 5000
]) ?>

<div class="portlet box darkblue">

    <div class="portlet-title">
        <div class="caption">
            Список договоров: <?= $dataAgreementProvider->totalCount ?>
        </div>
    </div>
    <div class="portlet-body">
        <?php if ($model->isNewRecord) : ?>
            <h3>Возможность добавить договор появится после сохранения контрагента</h3>
        <?php else : ?>

            <?= GridView::widget([
                'dataProvider' => $dataAgreementProvider,
                'filterModel' => $searchAgreementModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'columns' => [
                    [
                        'attribute' => 'document_number',
                        'label' => 'Номер',
                        'headerOptions' => ['style' => 'width:100px;'],
                        'format' => 'raw',
                        'value' => function ($data) {
                            $full_number = ($data['document_additional_number']) ?
                                $data['document_additional_number'].'-'.$data['document_number'] :
                                $data['document_number'];
                            $can_view = Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => Agreement::findOne($data['id']),
                            ]);

                            return  ($can_view && $data['tid']) ? // if has template
                                Html::a($full_number, ['/documents/agreement/view', 'id' => $data['id'], 'contractor' => $data['cid']]) :
                                $full_number;
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата',
                        'headerOptions' => ['style' => 'width:100px;'],
                        'format' => ['date', 'php:d.m.Y'],
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'filter' => $searchAgreementModel->getAgreementTypeFilterItems(),
                        'attribute' => 'document_type_id',
                        'label' => 'Тип документа',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data['agreementType'];
                        },
                    ],
                    [
                        'attribute' => 'document_name',
                        'label' => 'Название',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return $data['document_name'];
                        },
                    ],
                    [
                        'attribute' => 'agreement_template_id',
                        'label' => 'Шаблон',
                        'class' => DropDownSearchDataColumn::className(),
                        'filter' => $searchAgreementModel->getAgreementTemplates(),
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data['tid']) {
                                $tpl = \common\models\AgreementTemplate::findOne($data['tid']);
                                return '№ '.$tpl->document_number.' от '.date('d.m.Y', strtotime($tpl->document_date));
                            }
                            return '---';
                        },
                    ],
                    [
                        'attribute' => 'has_file',
                        'label' => 'Скан',
                        'format' => 'html',
                        'headerOptions' => ['style' => 'width:60px;'],
                        'content' => function ($data) {
                            $agreement = Agreement::findOne($data['id']);
                            $file = ($agreement && $agreement->files) ? $agreement->files[0] : null;
                            return $file ? Html::a('<span class="icon icon-paper-clip m-r-10"></span>', [
                                '/contractor/agreement-file-get',
                                'id' => $agreement->id,
                                'file-id' => $file->id,
                            ], [
                                'class' => 'file-link',
                                'target' => '_blank',
                                'download' => '',
                                'data-pjax' => 0,
                            ]) : '';
                        },
                    ],
                    [
                        'attribute' => 'created_by',
                        'label' => 'Ответственный',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-left text-ellipsis',
                        ],
                        'class' => DropDownSearchDataColumn::className(),
                        'value' => function ($data) use ($searchAgreementModel) {
                            $employee = EmployeeCompany::findOne([
                                'employee_id' => $data['created_by'],
                                'company_id' => $searchAgreementModel->company_id
                            ]);

                            return (!empty($employee)) ? $employee->getShortFio() : '';
                        },
                        'format' => 'raw',
                        'filter' => $searchAgreementModel->getAgreementEmployees()
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, $data) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                    'data-url' => $url,
                                    'class' => 'agreement-modal-link',
                                    'title' => Yii::t('yii', 'Обновить'),
                                    'aria-label' => Yii::t('yii', 'Обновить'),
                                ]);
                            },
                            'delete' => function ($url, $data) {
                                return BtnConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                        'class' => '',
                                        'tag' => 'a',
                                    ],
                                    'confirmUrl' => $url,
                                    'message' => 'Вы уверены, что хотите удалить договор?',
                                ]);
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = '/documents/agreement/update';
                                    break;
                                case 'delete':
                                    $url = 'agreement-delete';
                                    break;
                            }

                            Yii::$app->session->set('return_from_agreement', 'contractor');

                            return Url::to([$url, 'id' => $model['id'], 'type' => $model['type'], 'contractor_id' => $model['cid'], 'old_record' => 1]);
                        },
                    ],
                ],
            ]); ?>

        <?php endif ?>
    </div>

</div>

<?php Pjax::end() ?>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();
$successFlashMessageDelete = "<div id='w2-success-0' class='alert-success alert fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>Договор удален</div>";

$this->registerJs('
$(document).on("click", ".agreement-modal-link", function(e) {
    e.preventDefault();
    $.pjax({url: $(this).data("url"), container: "#agreement-form-container", push: false});
    $(document).on("pjax:success", function() {
        $("#agreement-modal-header").html($("[data-header]").data("header"));
        $(".date-picker").datepicker({format:"dd.mm.yyyy",language:"ru",autoclose:true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
    })
    $("#agreement-modal-container").modal("show");
});
$(document).on("show.bs.modal", "#agreement-modal-container", function(event) {
        $(".alert-success").remove();
});
$(document).on("click", "button.btn-confirm-yes", function(){
    var $this = $(this);
    $.ajax({
        url: $this.data("url"),
        type: $this.data("type"),
        data: $this.data("params"),
        success: function(data) {
            $.pjax.reload("#agreement-pjax-container", {push: false, timeout: 5000});
            $(".page-content").prepend("' . $successFlashMessageDelete . '");
        }
    });
    return false;
});
');
