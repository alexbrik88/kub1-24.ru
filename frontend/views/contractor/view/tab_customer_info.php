<?php

use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

?>
<div class="form-body form-horizontal">
    <div class="actions text-right" style="margin-top:-15px">
        <?= Html::a('<i class="icon-pencil"></i>', 'javascript:;', [
            'class' => 'darkblue btn-sm btn-link contractor-out-block update-contractor',
            'title' => 'Редактировать',
        ]); ?>
        <?= Html::submitButton('<span class="ico-Save-smart-pls"></span>', [
            'class' => 'btn darkblue btn-sm contractor-in-block',
            'title' => 'Сохранить',
            'form' => 'update-contractor-form',
            'style' => 'color: #FFF;display: none;',
        ]); ?>
        <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', 'javascript:;', [
            'class' => 'btn darkblue btn-sm contractor-in-block undo-contractor',
            'title' => 'Отменить',
            'style' => 'color: #FFF;display: none;',
        ]); ?>
    </div>
    <div class="contractor-out-block">
        <?= $this->render('customer_info/_partial_main', [
            'model' => $model,
        ]) ?>
        <div class="clearfix"></div>

        <div class="profile-form-tabs">
            <?= Tabs::widget([
                'options' => ['class' => 'nav-form-tabs row'],
                'headerOptions' => ['class' => 'col-xs-3'],
                'items' => array_merge([
                    [
                        'label' => '<span class="legal">Реквизиты</span><span class="physical">Данные паспорта</span>',
                        'encode' => false,
                        'content' => $this->render('customer_info/_partial_details', [
                            'model' => $model,
                        ]),
                        'active' => true,
                    ],
                    [
                        'label' => 'Комментарии',
                        'content' => $this->render('customer_info/_partial_comment', [
                            'model' => $model,
                        ]),
                    ],
                ], ($model->type == Contractor::TYPE_SELLER) ?
                    [
                        [
                            'label' => 'Агент',
                            'content' => $this->render('customer_info/_partial_agent', [
                                'model' => $model,
                            ]),
                        ]
                    ] : []),
            ]); ?>
        </div>

        <div class="clearfix"></div>

        <div class="physical portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Контактные данные</div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="director_email"
                                   class="control-label col-md-6 label-width">E-mail:</label>

                            <div class="col-md-6 field-width">
                                <span class="form-control-static"><?php echo $model->director_email; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="director_phone"
                                   class="control-label col-md-6 label-width">Телефон:</label>

                            <div class="col-md-6 field-width">
                                <span class="form-control-static"><?php echo $model->director_phone; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row small-boxes">
            <?= $this->render('customer_info/_partial_director', [
                'model' => $model,
            ]); ?>

            <?= $this->render('customer_info/_partial_chief_accountant', [
                'model' => $model,
            ]); ?>

            <?= $this->render('customer_info/_partial_contact', [
                'model' => $model,
            ]); ?>
        </div>
    </div>
    <?php if ($model->company_id) : ?>
        <div class="contractor-in-block" style="display: none;">
            <?= $this->render('@frontend/views/contractor/update', [
                'model' => $model,
                'type' => $type,
            ]); ?>
        </div>
    <?php endif ?>
</div>

<?php if ($is_edit) {
    $this->registerJs("$(document).ready(function() { $('.update-contractor').click(); }); ");
}