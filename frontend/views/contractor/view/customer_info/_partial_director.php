<?php
/* @var $model common\models\Contractor */

use yii\helpers\Html; ?>

<div class="col-md-4 legal">
    <div class="portlet box box-leader darkblue">
        <div class="portlet-title">
            <div class="caption">Руководитель</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">

            <div class="form-group">
                <label for="director_post_name"
                       class="control-label col-md-4 label-width bold-text">Должность:</label>

                <div class="col-md-8 field-width">
                    <span class="form-control-static"><?php echo $model->director_post_name; ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="director_name"
                       class="control-label col-md-4 label-width bold-text">ФИО:</label>

                <div class="col-md-8 field-width">
                    <span class="form-control-static"><?php echo $model->director_name; ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="director_email"
                       class="control-label col-md-4 label-width bold-text">E-mail:</label>

                <div class="col-md-8 field-width">
                    <span class="form-control-static"><?php echo $model->director_email; ?></span>
                </div>
            </div>
            <div class="form-group">
                <label for="director_phone"
                       class="control-label col-md-4 label-width bold-text">Телефон:</label>

                <div class="col-md-8 field-width">
                    <span class="form-control-static"><?php echo $model->director_phone; ?></span>
                </div>
            </div>
            <div class="form-group">
                <label class="checkbox-inline match-with-leader" style="height: 39px;">
                    <?= Html::activeCheckbox($model, 'director_in_act', [
                        'label' => false,
                        'disabled' => true
                    ]); ?>
                    Указывать ФИО в Актах
                </label>
            </div>
        </div>
    </div>
</div>