<?php

use yii\helpers\ArrayHelper;
use common\models\employee\Employee;
use yii\helpers\Html;
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\contractor\ContractorAgentBuyer;

/** @var $model \common\models\Contractor */

?>

<div class="portlet box darkblue edit-customer">
    <div class="portlet-body details">
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="is_agent"
                           class="control-label col-md-6 label-width">Агент:</label>
                    <div class="col-md-6 field-width">
                        <span class="form-control-static">
                            <?= Html::checkbox('is_agent_input_info', $model->is_agent, [
                                'label' => false,
                                'disabled' => true
                            ]); ?>
                        </span>
                    </div>
                </div>
                <?php if ($model->is_agent): ?>
                    <div class="form-group">
                        <label for="payment_delay"
                               class="control-label col-md-6 label-width">Комиссионное вознаграждение:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo TextHelper::moneyFormat($model->agent_payment_percent, 2); ?> %</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="source"
                               class="control-label col-md-6 label-width">Тип платежа:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo ($model->agentPaymentType) ? $model->agentPaymentType->name : ''; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="comment"
                               class="control-label col-md-6 label-width">Дата начала расчетов:</label>

                        <div class="col-md-6 field-width">
                        <span class="form-control-static">
                            <?php echo DateHelper::format($model->agent_start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .' г.'; ?>
                        </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12 label-width">Покупатели:</label>
                        <div class="col-md-12 field-width">
                            <span class="form-control-static">
                                <?php foreach ($model->agentBuyersInfo as $buyerInfo): ?>
                                    <?php $start_date = DateHelper::format($buyerInfo->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                    <?= $start_date.' г. '.Html::a($buyerInfo->buyer->getShortName(),  Url::to(['/contractor/view', 'type' => Contractor::TYPE_CUSTOMER, 'id' => $buyerInfo->buyer->id]), [
                                        'target' => '_blank'
                                    ]) ?><br/>
                                <?php endforeach; ?>
                            </span>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="col-md-6"></div>

        </div>
    </div>
</div>