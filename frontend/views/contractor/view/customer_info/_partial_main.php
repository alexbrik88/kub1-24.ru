<?php

use common\models\Contractor;
use yii\helpers\Html;

/* @var $model common\models\Contractor */

$helAccounting = Html::tag('span', '', [
    'class' => 'tooltip2 ico-question',
    'style' => 'display: inline-block; margin: -6px 5px; vertical-align: middle;',
    'data-tooltip-content' => '#tooltip_not_accounting',
]);

?>

<div class="form-group form-group_width edit-customer">
    <div class="col-md-6">
        <div class="form-group">
            <label for="ITN"
                   class="control-label col-md-6 label-width">ИНН:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo $model->ITN; ?></span>
            </div>
        </div>

        <div class="physical form-group">
            <label for="physical-lastname"
                   class="control-label col-md-6 label-width">Фамилия:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo $model->physical_lastname; ?></span>
            </div>
        </div>
        <div class="physical form-group">
            <label for="physical-firstname"
                   class="control-label col-md-6 label-width">Имя:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo $model->physical_firstname; ?></span>
            </div>
        </div>
        <?php if (!$model->physical_no_patronymic) { ?>
            <div class="physical form-group">
                <label for="physical-patronymic"
                       class="control-label col-md-6 label-width">Отчество:</label>

                <div class="col-md-6 field-width">
                    <span class="form-control-static"><?php echo $model->physical_patronymic; ?></span>
                </div>
            </div>
        <?php } ?>

        <div class="legal form-group">
            <label for="name"
                   class="control-label col-md-6 label-width">Название контрагента:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo $model->name; ?></span>
            </div>
        </div>

        <div class="form-group">
            <label for="not-accounting"
                   class="control-label col-md-6 label-width">Не для бухгалтерии:</label>

            <div class="col-md-5" style="padding: 6px 0 0 15px;">
                <div class="checker">
                    <span class="<?php if ($model->not_accounting) echo 'checked'; ?>">
                        <input value="1" disabled="" style="pointer-events: none;" type="checkbox">
                    </span>
                </div>
                <?= $helAccounting ?>
                <div class="hidden">
                    <span id="tooltip_not_accounting">
                        Документы по такому клиенту не попадают в выгрузку для 1с и видны только для руководителя
                    </span>
                </div>
            </div>
        </div>

        <div class="legal form-group">
            <label for="taxation-system"
                   class="control-label col-md-6 label-width">Налогообложение:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static">
                    <?php echo ($model->taxation_system == Contractor::WITH_NDS) ? 'С НДС' : 'Без НДС'; ?>
                </span>
            </div>
        </div>

        <div class="form-group">
            <label for="status"
                   class="control-label col-md-6 label-width">Статус:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static">
                    <?php echo ($model->status == Contractor::ACTIVE) ? 'Активен' : 'Не активен'; ?>
                </span>
            </div>
        </div>

    </div>

    <div class="col-md-6 left-column">
        <div class="form-group">
            <label for="face-type"
                   class="control-label col-md-6 label-width">Юр/Физ лицо:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo Contractor::$contractorFaceType[$model->face_type]; ?></span>
            </div>
        </div>
        <div class="legal form-group">
            <label for="companyTypeId"
                   class="control-label col-md-6 label-width">Форма:</label>

            <div class="col-md-6 field-width">
                <span class="form-control-static"><?php echo $model->getTypeArray()[$model->companyTypeId]; ?></span>
            </div>
        </div>
    </div>

</div>