<?php
/* @var $model common\models\Contractor */

use common\models\company\CompanyType;

?>

    <div class="legal portlet box darkblue edit-customer">
        <div class="portlet-body details">
            <div class="row">
                <div class="col-md-6">
                    <?php if (!$model->company_type_id !== CompanyType::TYPE_IP): ?>
                        <div class="form-group">
                            <label for="PPC"
                                   class="control-label col-md-6 label-width">КПП:</label>

                            <div class="col-md-6 field-width">
                                <span class="form-control-static"><?php echo $model->PPC; ?></span>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="BIN"
                               class="control-label col-md-6 label-width">
                                <?= ($model->company_type_id == CompanyType::TYPE_IP) ? 'ОГРНИП' : 'ОГРН' ?>:
                        </label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->BIN; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="okpo"
                               class="control-label col-md-6 label-width">ОКПО:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->okpo; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="legal_address"
                               class="control-label col-md-6 label-width">Юридический адрес:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->legal_address; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="actual_address"
                               class="control-label col-md-6 label-width">Фактический адрес:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->actual_address; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="postal_address"
                               class="control-label col-md-6 label-width">Адрес для почты:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->postal_address; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 left-column">
                    <div class="form-group">
                        <label for="current_account"
                               class="control-label col-md-6 label-width">Р/с:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->current_account; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="BIC"
                               class="control-label col-md-6 label-width">БИК:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->BIC; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="bank_name"
                               class="control-label col-md-6 label-width">Наименование банка:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->bank_name; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="corresp_account"
                               class="control-label col-md-6 label-width">К/с:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->corresp_account; ?></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="physical portlet box darkblue contractor-passport-info-wrapper">
        <div class="portlet-body min-width-container-passport">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="physical_passport_isRf"
                               class="control-label col-md-6 label-width">Паспорт:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?= ($model->physical_passport_isRf == 0) ? 'не РФ' : 'РФ'; ?></span>
                        </div>
                    </div>
                    <?php if ($model->physical_passport_isRf == 0) : ?>
                    <div class="form-group">
                        <label for="physical_passport_country"
                               class="control-label col-md-6 label-width">Страна:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?= $model->physical_passport_country ?></span>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="physical_passport_series"
                               class="control-label col-md-6 label-width">Серия:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->physical_passport_series; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="physical_passport_issued_by"
                               class="control-label col-md-6 label-width">Кем выдан:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->physical_passport_issued_by; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="physical_passport_department"
                               class="control-label col-md-6 label-width">Код подразделения:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->physical_passport_department; ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="physical_address"
                               class="control-label col-md-6 label-width">Адрес регистрации:</label>

                        <div class="col-md-6 field-width">
                            <span class="form-control-static"><?php echo $model->physical_address; ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 left-column">
                    <label for="physical_passport_number"
                           class="control-label col-md-6 label-width">Номер:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?php echo $model->physical_passport_number; ?></span>
                    </div>
                    <label for="physical_passport_date_output"
                           class="control-label col-md-6 label-width">Дата выдачи:</label>

                    <div class="col-md-6 field-width">
                        <span class="form-control-static"><?= $model->physical_passport_date_output ?
                            date('d.m.Y', strtotime($model->physical_passport_date_output)): '—'; ?></span>
                    </div>
                </div>

            </div>
        </div>
    </div>