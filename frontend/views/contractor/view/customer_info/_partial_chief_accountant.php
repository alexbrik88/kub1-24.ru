<?php
use yii\helpers\Html;

/* @var $model common\models\Contractor */

?>

<div class="col-md-4 legal">
    <div class="portlet box box-accounter darkblue">
        <div class="portlet-title">
            <div class="caption">Главный бухгалтер</div>
        </div>
        <div class="portlet-body" style="min-height: 236px;">

            <div class="form-group">

                <label class="checkbox-inline match-with-leader" style="height: 39px; pointer-events: none;">
                    <div class="checker">
                        <span class="<?php if ($model->chief_accountant_is_director) echo 'checked' ?>">
                            <input value="1" checked="" disabled="" type="checkbox">
                        </span>
                    </div>
                    Совпадает с руководителем
                </label>

            </div>

            <div class="<?php if ($model->chief_accountant_is_director) echo 'hide' ?>">
                <div class="form-group">
                    <label for="chief_accountant_name"
                           class="control-label col-md-4 label-width bold-text">ФИО:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->chief_accountant_name; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="chief_accountant_email"
                           class="control-label col-md-4 label-width bold-text">E-mail:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->chief_accountant_email; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="chief_accountant_phone"
                           class="control-label col-md-4 label-width bold-text">Телефон:</label>

                    <div class="col-md-8 field-width">
                        <span class="form-control-static"><?php echo $model->chief_accountant_phone; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline match-with-leader" style="height: 39px;">
                        <?= Html::activeCheckbox($model, 'chief_accountant_in_act', [
                            'label' => false,
                            'disabled' => true
                        ]); ?>
                        Указывать ФИО в Актах
                    </label>
                </div>
            </div>

        </div>
    </div>
</div>
