<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.07.2018
 * Time: 13:22
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\components\debts\DebtsHelper;
use frontend\models\Documents;
use kartik\checkbox\CheckboxX;
use miloschuman\highcharts\HighchartsAsset;
use yii\helpers\Json;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use common\models\Contractor;
use frontend\modules\reports\models\DisciplineSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $analyticsMonthNumber string */
/* @var $analyticsYearNumber string */
/* @var $abcGroup integer */
/* @var $disciplineGroup integer */

$previousMonthNumber = date('m', strtotime('-1 month', strtotime("01.{$analyticsMonthNumber}." . $analyticsYearNumber)));
$previousYear = date('Y', strtotime('-1 month', strtotime("01.{$analyticsMonthNumber}." . $analyticsYearNumber)));

$totalRevenueAmount = $model->getTotalRevenue();
$totalCostPrice = $model->getTotalCostPrice();
$totalContractorProfit = $totalRevenueAmount - $totalCostPrice;
$allBilledAmount = $model->getAllBilledAmount();
$allPayedAmount = $model->getAllPayedInvoicesAmount();
$balance = $allBilledAmount - $allPayedAmount;
$currentInvoiceSum = $model->getInvoiceSumAnalyticsData($analyticsMonthNumber, $analyticsYearNumber);
$previousMonthInvoiceSum = $model->getInvoiceSumAnalyticsData($previousMonthNumber, $previousYear);
if (empty($previousMonthInvoiceSum)) {
    $invoiceSumPercent = 100;
    if (empty($currentInvoiceSum)) {
        $invoiceSumPercent = 0;
    }
} else {
    $invoiceSumPercent = round(($currentInvoiceSum * 100 / $previousMonthInvoiceSum), 2) - 100;
}
$currentInvoiceCount = $model->getInvoiceCountAnalyticsData($analyticsMonthNumber, $analyticsYearNumber);
$previousMonthInvoiceCount = $model->getInvoiceCountAnalyticsData($previousMonthNumber, $previousYear);
if (empty($previousMonthInvoiceCount)) {
    $invoiceCountPercent = 100;
    if (empty($currentInvoiceCount)) {
        $invoiceCountPercent = 0;
    }
} else {
    $invoiceCountPercent = round(($currentInvoiceCount * 100 / $previousMonthInvoiceCount), 2) - 100;
}
$currentInvoicePayedSum = $model->getInvoicePayedSumAnalyticsData($analyticsMonthNumber, $analyticsYearNumber);
$previousMonthInvoicePayedSum = $model->getInvoicePayedSumAnalyticsData($previousMonthNumber, $previousYear);
if (empty($previousMonthInvoicePayedSum)) {
    $invoicePayedSumPercent = 100;
    if (empty($currentInvoicePayedSum)) {
        $invoicePayedSumPercent = 0;
    }
} else {
    $invoicePayedSumPercent = round(($currentInvoicePayedSum * 100 / $previousMonthInvoicePayedSum), 2) - 100;
}
$currentInvoiceDebtSum = $model->getInvoiceDebtSumAnalyticsData($analyticsMonthNumber, $analyticsYearNumber);
$previousMonthInvoiceDebtSum = $model->getInvoiceDebtSumAnalyticsData($previousMonthNumber, $previousYear);
if (empty($previousMonthInvoiceDebtSum)) {
    $invoiceDebtSumPercent = 100;
    if (empty($currentInvoiceDebtSum)) {
        $invoiceDebtSumPercent = 0;
    }
} else {
    $invoiceDebtSumPercent = round(($currentInvoiceDebtSum * 100 / $previousMonthInvoiceDebtSum), 2) - 100;
}

$averageInvoiceAmount = 0;
$averageInvoiceCount = 0;
$averageInvoicePayedSum = 0;
$averageInvoiceDebtSum = 0;

HighchartsAsset::register($this)->withScripts([
    'modules/exporting',
    'themes/grid-light',
]);

$invoiceSumHighChartsOptions = $model->getAnalyticsInvoiceAmountHighChartsOptions($analyticsMonthNumber, $analyticsYearNumber);
$highChartsInvoiceSumOptionsJson = Json::encode($invoiceSumHighChartsOptions);
$invoiceCountHighChartsOptions = $model->getAnalyticsInvoiceCountHighChartsOptions($analyticsMonthNumber, $analyticsYearNumber);
$highChartsInvoiceCountOptionsJson = Json::encode($invoiceCountHighChartsOptions);
$invoicePayedSumHighChartsOptions = $model->getAnalyticsInvoicePayedAmountHighChartsOptions($analyticsMonthNumber, $analyticsYearNumber);
$highChartsInvoicePayedSumOptionsJson = Json::encode($invoicePayedSumHighChartsOptions);
$invoiceDebtSumHighChartsOptions = $model->getAnalyticsInvoiceDebtAmountHighChartsOptions($analyticsMonthNumber, $analyticsYearNumber);
$highChartsInvoiceDebtSumOptionsJson = Json::encode($invoiceDebtSumHighChartsOptions);

$seriesInvoiceSumData = $invoiceSumHighChartsOptions['series'][0]['data'];
foreach ($seriesInvoiceSumData as $oneAmount) {
    $averageInvoiceAmount += $oneAmount * 100;
}
$averageInvoiceAmount /= 12;

$seriesInvoiceCountData = $invoiceCountHighChartsOptions['series'][0]['data'];
foreach ($seriesInvoiceCountData as $oneCount) {
    $averageInvoiceCount += $oneCount;
}
$averageInvoiceCount /= 12;

$seriesInvoicePayedSumData = $invoicePayedSumHighChartsOptions['series'][0]['data'];
foreach ($seriesInvoicePayedSumData as $onePayedSum) {
    $averageInvoicePayedSum += $onePayedSum;
}
$averageInvoicePayedSum /= 12;

$seriesInvoiceDebtSumData = $invoiceDebtSumHighChartsOptions['series'][0]['data'];
foreach ($seriesInvoiceDebtSumData as $oneDebtSum) {
    $averageInvoiceDebtSum += $oneDebtSum;
}
$averageInvoiceDebtSum /= 12;

$ioType = ($model->type == Contractor::TYPE_SELLER) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;

$dateItems = [];
foreach ($model->getAnalyticsDateItems($ioType) as $key => $value) {
    $dateItems[] = [
        'label' => mb_strtolower($value) . ' ' . DateHelper::format($key, 'Y', 'Y-m'),
        'url' => Url::current([
            'analyticsMonthNumber' => DateHelper::format($key, 'm', 'Y-m'),
            'analyticsYearNumber' => DateHelper::format($key, 'Y', 'Y-m'),
        ]),
        'linkOptions' => ['class' => $key == ($analyticsYearNumber . '-' . $analyticsMonthNumber) ? 'active' : ''],
    ];
}
?>
    <div class="row">
        <div class="col-md-4">
            <div class="bold">Общие показатели</div>
            <div style="margin-top: 15px;">
                <span class="bold">Активен с:</span>
                <span><?= date(DateHelper::FORMAT_USER_DATE, $model->getActivationDate()); ?></span>
            </div>
            <div>
                <span class="bold">Условия оплаты:</span>
                <span><?= $model->payment_delay; ?> дн.</span>
            </div>
            <div>
                <span class="bold">Платежная дисциплина:</span>
                <span>
                    <?= $disciplineGroup ?
                        Html::a(('группа "' . DisciplineSearch::$groups[$disciplineGroup] . '"'),
                            Url::to(['/reports/discipline/index', 'DisciplineSearch' => [
                                'id' => $model->id,
                            ]]), [
                                'style' => 'color:' . $model->getDisciplineGroupColor($disciplineGroup) . '!important',
                            ]) :
                        null; ?>
                </span>
            </div>
            <div>
                <span class="bold">АВС анализ:</span>
                <span>
                    <?= Html::a('группа ' . $model->getGroupText($abcGroup), Url::to(['/reports/analysis/index', 'AnalysisSearch' => [
                        'id' => $model->id,
                    ]]), [
                        'style' => 'color:' . $model->getGroupColor($abcGroup) . '!important',
                    ]); ?>
                </span>
            </div>

            <div class="bold" style="margin-top: 30px;">Данные с начала сотрудничества</div>
            <div class="col-md-12 pad0" style="margin-top: 10px;">
                <span class="col-md-6 pad0">Суммарная выручка</span>
                <span class="col-md-6 pad0 text-right">
                <?= TextHelper::invoiceMoneyFormat($totalRevenueAmount, 2); ?> <i class="fa fa-rub"></i>
            </span>
                <span class="tooltip2-right ico-question valign-middle tooltip-total-revenue_contractor"
                      data-tooltip-content="#tooltip_total_revenue"></span>
            </div>
            <div class="col-md-12 pad0 border-bottom">
                <span class="col-md-6 pad0">Себестоимость</span>
                <span class="col-md-6 pad0 text-right">
                <?= TextHelper::invoiceMoneyFormat($totalCostPrice, 2); ?> <i class="fa fa-rub"></i>
            </span>
            </div>
            <div class="col-md-12 pad0 border-bottom" style="margin-top: 10px;">
                <span class="col-md-6 pad0">Прибыль по клиенту</span>
                <span class="col-md-6 pad0 text-right"
                      style="<?= $totalContractorProfit < 0 ? 'color: #f3565d;' : 'color: #45b6af'; ?>">
                <?= TextHelper::invoiceMoneyFormat($totalContractorProfit, 2); ?> <i class="fa fa-rub"></i>
            </span>
            </div>

            <div class="col-md-12 pad0" style="margin-top: 30px;">
                <span class="col-md-6 pad0">Выставлено счетов</span>
                <span class="col-md-6 pad0 text-right">
                <?= TextHelper::invoiceMoneyFormat($allBilledAmount, 2); ?> <i class="fa fa-rub"></i>
            </span>
            </div>
            <div class="col-md-12 pad0 border-bottom">
                <span class="col-md-6 pad0">Получено оплат</span>
                <span class="col-md-6 pad0 text-right">
                <?= TextHelper::invoiceMoneyFormat($allPayedAmount, 2); ?> <i class="fa fa-rub"></i>
            </span>
            </div>
            <div class="col-md-12 pad0 border-bottom" style="margin-top: 10px;">
                <span class="col-md-6 pad0">Баланс</span>
                <span class="col-md-6 pad0 text-right"
                      style="<?= $balance < 0 ? 'color: #f3565d;' : 'color: #45b6af;'; ?>">
                <?= TextHelper::invoiceMoneyFormat($balance, 2); ?> <i class="fa fa-rub"></i>
            </span>
            </div>

            <div class="col-md-12 pad0 body" style="margin-top: 30px;">
                <div class="box-mar-bot-10" style="margin-bottom: 10px;">
                    <div class="nowrap m-l-n">
                        <span style="font-weight: 700;">Вся задолженность:</span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="nowrap m-l-n">
                        <span style="color: #f3565d; font-weight: 700;">Просрочено на:</span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>Текущая задолженность</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                         <?= TextHelper::invoiceMoneyFormat(DebtsHelper::getCurrentDebt($model->id, Documents::IO_TYPE_OUT), 2); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>0-10 дней</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, $model->id); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>11-30 дней</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, $model->id); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>31-60 дней</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, $model->id); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>61-90 дней</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, $model->id); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                        <span>Больше 90 дней</span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, $model->id); ?>
                        <i class="fa fa-rub"></i>
                    </span>
                    </div>
                </div>
                <div class="box-mar-bot-10">
                    <div class="col-md-6 col-sm-6 col-xs-6 nowrap m-l-n pad0">
                    <span>
                        <b style="color: #f3565d;">
                            Вся задолженность
                        </b>
                    </span>
                    </div>
                    <div class="col-md-6 col-sm-6 pull-left text-right pad0">
                    <span>
                        <b style="color: #f3565d;">
                            <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, true, $model->id); ?>
                            <i class="fa fa-rub"></i>
                        </b>
                    </span>
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-6 text-left">
            <div class="bold" style="padding: 5px 10px;background-color: #e5e5e5;">
                <div style="display: inline-block;">Ключевые показатели эффективности за</div>
                <div style="display: inline-block; width: 200px;">
                    <div class="dropdown">
                        <?= Html::tag('div', mb_strtolower(FlowOfFundsReportSearch::$month[$analyticsMonthNumber]) . ' ' . $analyticsYearNumber, [
                            'class' => 'dropdown-toggle',
                            'data-toggle' => 'dropdown',
                            'style' => 'display: inline-block; border-bottom: 1px dashed #333333; cursor: pointer;',
                        ]) ?>
                        <?= Dropdown::widget([
                            'id' => 'employee-rating-dropdown',
                            'items' => $dateItems,
                        ]) ?>
                    </div>
                </div>
            </div>
            <table id="analytics-contractor-table" class="table table-bordered table-hover dataTable no-footer"
                   style="table-layout: fixed;" role="grid">
                <tbody>
                <tr class="odd" role="row">
                    <td width="17%" style="padding-right: 0;padding-left: 0;">
                        <?= CheckboxX::widget([
                            'id' => "analytics-main-checkbox",
                            'name' => "analytics-main-checkbox",
                            'value' => true,
                            'options' => [
                                'class' => 'analytics-contractor-main-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= FlowOfFundsReportSearch::$month[$previousMonthNumber] . ' ' . $previousYear; ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= FlowOfFundsReportSearch::$month[$analyticsMonthNumber] . ' ' . $analyticsYearNumber; ?>
                    </td>
                    <td width="8%" class="text-right">
                        %%
                    </td>
                    <td width="35%" class="text-right">
                        Среднее (12 месяцев)
                    </td>
                </tr>
                <tr>
                    <td width="17%" style="padding-right: 0;padding-left: 0;">
                        <?= CheckboxX::widget([
                            'id' => 'invoice-sum-checkbox',
                            'name' => 'invoice-sum-checkbox',
                            'value' => false,
                            'options' => [
                                'class' => 'analytics-contractor-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Счета <i class="fa fa-rub"></i>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($previousMonthInvoiceSum, 2); ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($currentInvoiceSum, 2); ?>
                    </td>
                    <td width="8%" class="text-right">
                        <?= $invoiceSumPercent > 0 ? ('+' . $invoiceSumPercent) : ($invoiceSumPercent); ?>
                    </td>
                    <td width="35%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($averageInvoiceAmount, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="pad0">
                        <div id="high-charts-container-invoice-sum" class="high-chart"></div>
                    </td>
                </tr>
                <tr class="odd" role="row">
                    <td width="17%" style="padding-left: 0;padding-right: 0;">
                        <?= CheckboxX::widget([
                            'id' => 'invoice-count-checkbox',
                            'name' => 'invoice-count-checkbox',
                            'value' => true,
                            'options' => [
                                'class' => 'analytics-contractor-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Счета (шт)
                    </td>
                    <td width="20%" class="text-right">
                        <?= $previousMonthInvoiceCount; ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= $currentInvoiceCount; ?>
                    </td>
                    <td width="8%" class="text-right">
                        <?= $invoiceCountPercent > 0 ? ('+' . $invoiceCountPercent) : $invoiceCountPercent; ?>
                    </td>
                    <td width="35%" class="text-right">
                        <?= round($averageInvoiceCount, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="pad0">
                        <div id="high-charts-container-invoice-count" class="high-chart" style="display: none;"></div>
                    </td>
                </tr>
                <tr class="odd" role="row">
                    <td width="17%" style="padding-left: 0;padding-right: 0;">
                        <?= CheckboxX::widget([
                            'id' => 'invoice-payed-checkbox',
                            'name' => 'invoice-payed-checkbox',
                            'value' => true,
                            'options' => [
                                'class' => 'analytics-contractor-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Оплата <i class="fa fa-rub"></i>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($previousMonthInvoicePayedSum, 2); ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($currentInvoicePayedSum, 2); ?>
                    </td>
                    <td width="8%" class="text-right">
                        <?= $invoicePayedSumPercent > 0 ? ('+' . $invoicePayedSumPercent) : $invoicePayedSumPercent; ?>
                    </td>
                    <td width="35%" class="text-right">
                        <?= round($averageInvoicePayedSum, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="pad0">
                        <div id="high-charts-container-invoice-payed" class="high-chart" style="display: none;"></div>
                    </td>
                </tr>
                <tr class="odd" role="row">
                    <td width="17%" style="padding-left: 0;padding-right: 0;">
                        <?= CheckboxX::widget([
                            'id' => 'invoice-debt-checkbox',
                            'name' => 'invoice-debt-checkbox',
                            'value' => true,
                            'options' => [
                                'class' => 'analytics-contractor-checkbox',
                            ],
                            'pluginOptions' => [
                                'size' => 'xs',
                                'threeState' => false,
                                'inline' => false,
                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                            ],
                        ]); ?> Долг <i class="fa fa-rub"></i>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($previousMonthInvoiceDebtSum, 2); ?>
                    </td>
                    <td width="20%" class="text-right">
                        <?= TextHelper::invoiceMoneyFormat($currentInvoiceDebtSum, 2); ?>
                    </td>
                    <td width="8%" class="text-right">
                        <?= $invoiceDebtSumPercent > 0 ? ('+' . $invoiceDebtSumPercent) : $invoiceDebtSumPercent; ?>
                    </td>
                    <td width="35%" class="text-right">
                        <?= round($averageInvoiceDebtSum, 2); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="pad0">
                        <div id="high-charts-container-invoice-debt" class="high-chart" style="display: none;"></div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="tooltip-template" style="display: none;">
    <span id="tooltip_total_revenue" style="display: inline-block; text-align: center;">
        Выручка считается по <br>
        товарный накладным и актам
    </span>
    </div>
    <script type="text/javascript">
        window.highchartsInvoiceSumOptions = <?= $highChartsInvoiceSumOptionsJson; ?>;
        window.highchartsInvoiceCountOptions = <?= $highChartsInvoiceCountOptionsJson; ?>;
        window.highchartsInvoicePayedSumOptions = <?= $highChartsInvoicePayedSumOptionsJson; ?>;
        window.highchartsInvoiceDebtSumOptions = <?= $highChartsInvoiceDebtSumOptionsJson; ?>;
    </script>
<?php
$js = <<<JS
function createHighcharts() {
    Highcharts.chart('high-charts-container-invoice-sum', window.highchartsInvoiceSumOptions);
    Highcharts.chart('high-charts-container-invoice-count', window.highchartsInvoiceCountOptions);
    Highcharts.chart('high-charts-container-invoice-payed', window.highchartsInvoicePayedSumOptions);
    Highcharts.chart('high-charts-container-invoice-debt', window.highchartsInvoiceDebtSumOptions);
}

createHighcharts();
JS;
$this->registerJs($js);
