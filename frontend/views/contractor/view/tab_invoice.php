<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.07.2018
 * Time: 12:48
 */

use common\components\date\DateHelper;
use common\components\debts\DebtsHelper;
use common\components\helpers\Url;
use common\models\document\Invoice;
use common\models\employee\EmployeeRole;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use frontend\modules\documents\widgets\StatisticWidget;
use common\models\Company;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company Company */

$user = Yii::$app->user->identity;
$company = $user->company;

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, ['ioType' => $ioType]) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canPay = Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW);

$dropItems = [];
if ($canCreate) {
    $dropItems[] = [
        'label' => 'Создать Акты',
        'url' => '#many-create-act',
        'linkOptions' => [
            'class' => 'create-acts',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один Акт',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-act',
            'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать ТН',
        'url' => '#many-create-packing-list',
        'linkOptions' => [
            'class' => 'create-packing-list',
            'data-toggle' => 'modal',
        ],
    ];
    if ($company->companyTaxationType->osno) {
        $dropItems[] = [
            'label' => 'Создать СФ',
            'url' => '#many-create-invoice-facture',
            'linkOptions' => [
                'class' => 'create-invoice-factures',
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать одну СФ',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-invoice-facture',
                'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Создать УПД',
        'url' => '#many-create-upd',
        'linkOptions' => [
            'class' => 'create-upd',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-upd',
            'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
}
if ($canIndex) {
    if (!$canCreate && $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_MANAGER) {
        $dropItems[] = [
            'label' => 'Создать один Акт',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-act',
                'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один УПД',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-upd',
                'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        if ($company->companyTaxationType->osno) {
            $dropItems[] = [
                'label' => 'Создать одну СФ',
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'create-one-invoice-facture',
                    'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
                ],
            ];
        }
    }
    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
        $dropItems[] = [
            'label' => 'Без Акта',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'act-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'Акт',
                    'attribute_text' => 'Без Акта',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_ACT,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без ТН',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'packing-list-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'ТН',
                    'attribute_text' => 'Без ТН',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_PACKING_LIST,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без УПД',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'upd-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'УПД',
                    'attribute_text' => 'Без УПД',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_UPD,
                ],
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
}

$sendDropItems = [];
if ($canSend) {
    $sendDropItems[] = [
        'label' => 'Счета',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
        ],
    ];
    $sendDropItems[] = [
        'label' => 'Счета + Акт/ТН/СФ/УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType, 'send_with_documents' => true]),
        ],
    ];
}
?>

<?php if ($model->isSellerCustomer) : ?>
    <div class="mb-4">
        <?= Nav::widget([
            'id' => 'contractor-menu-invoices',
            'options' => ['class' => 'nav nav-tabs nav-tabs_indents_else nav-tabs_border_bottom_grey w-100 mr-3'],
            'items' => [
                [
                    'label' => 'Продажи',
                    'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_OUT]),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                ],
                [
                    'label' => 'Закупки',
                    'url' => Url::currentArray(['io_type' => Documents::IO_TYPE_IN]),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                ],
            ],
        ]); ?>
    </div>
<?php endif ?>

<div class="row">
    <?= StatisticWidget::widget([
        'type' => $model->type,
        'outerClass' => 'col-md-3 col-sm-3',
        'contractorId' => $model->id,
        'searchModel' => $searchModel,
    ]); ?>
    <div class="col-md-3 col-sm-3 contractor-debt tooltip2"
         data-tooltip-content="#tooltip_contractor-debt">
        <div class="body">
            <div class="row box-mar-bot-10">
                <div class="col-md-12 nowrap m-l-n " style="margin-top: -4px;">
                    <span style="color: #f3565d; font-weight: 700;">Просрочено на:</span>
                </div>
            </div>
            <div class="row box-mar-bot-10">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                    <span>0-10 дней</span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, $model->id); ?></span>
                </div>
            </div>
            <div class="row box-mar-bot-10">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                    <span>11-30 дней</span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, $model->id); ?></span>
                </div>
            </div>
            <div class="row box-mar-bot-10">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                    <span>31-60 дней</span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, $model->id); ?></span>
                </div>
            </div>
            <div class="row box-mar-bot-10">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                    <span>61-90 дней</span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, $model->id); ?></span>
                </div>
            </div>
            <div class="row box-mar-bot-10">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                    <span>Больше 90 дней</span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                    <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, $model->id); ?></span>
                </div>
            </div>
            <div class="row box-mar-bot-10" style="padding-top: 0px;">
                <div class="col-md-3 col-sm-8 col-xs-6 nowrap m-l-n ">
                            <span><b style="color: #f3565d;">Вся
                                    задолженность</b></span>
                </div>
                <div class="col-md-8 col-sm-3 pull-right m-r text-right ">
                            <span><b
                                    style="color: #f3565d;"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, true, $model->id); ?></b></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="table-icons" style="margin-top: -20px;">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'contr_inv_scan',
                    ],
                    [
                        'attribute' => 'contr_inv_paydate',
                    ],
                    [
                        'attribute' => 'contr_inv_paylimit',
                    ],
                    [
                        'attribute' => 'contr_inv_act',
                    ],
                    [
                        'attribute' => 'contr_inv_paclist',
                    ],
                    [
                        'attribute' => 'contr_inv_invfacture',
                    ],
                    [
                        'attribute' => 'contr_inv_upd',
                    ],
                    [
                        'attribute' => 'contr_inv_author',
                        'visible' => (
                            Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR) ||
                            Yii::$app->user->can(UserRole::ROLE_SUPERVISOR_VIEWER)
                        ),
                    ],
                    [
                        'attribute' => 'contr_inv_comment',
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список счетов
        </div>
        <div class="actions joint-operations col-md-9 col-sm-9" style="display:none;padding-right: 0px!important;margin-right: -7px;">
            <?php if ($dropItems) : ?>
                <div class="dropdown">
                    <?= Html::a('Еще  <span class="caret"></span>', null, [
                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                        'id' => 'dropdownMenu1',
                        'data-toggle' => 'dropdown',
                        'aria-expanded' => true,
                        'style' => 'height: 28px;',
                    ]); ?>
                    <?= Dropdown::widget([
                        'items' => $dropItems,
                        'options' => [
                            'style' => 'right: -30px; left: auto; top: 28px;',
                            'aria-labelledby' => 'dropdownMenu1',
                        ],
                    ]); ?>
                </div>
            <?php endif ?>

            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>

                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">Вы уверены, что хотите удалить
                                        выбранные счета?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                            'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                            'data-url' => Url::to(['/documents/invoice/many-delete', 'type' => $ioType]),
                                            'data-style' => 'expand-right',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($canPay) : ?>
                <?= Html::a('Оплачены', '#many-paid-modal', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>

                <?php Modal::begin([
                    'id' => 'many-paid-modal',
                    'options' => [
                        'data-url' => Url::to(['/documents/invoice/add-flow-form', 'type' => $ioType]),
                    ]
                ]); ?>
                <div id="many-paid-content"></div>
                <?php Modal::end(); ?>

                <?php $this->registerJs('
                                $(document).on("shown.bs.modal", "#many-paid-modal", function() {
                                    var idArray = $(".joint-operation-checkbox:checked").map(function () {
                                        return $(this).closest("tr").data("key");
                                    }).get();
                                    var url = this.dataset.url + "&id=" + idArray.join();
                                    $.post(url, function(data) {
                                        $("#many-paid-content").html(data);
                                        $("#many-paid-content input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
                                        $("#many-paid-content input:radio:not(.md-radiobtn)").uniform();
                                    })
                                });
                                $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
                                    $("#many-paid-content").html("");
                                });
                            ') ?>
            <?php endif ?>

            <?php if ($ioType == Documents::IO_TYPE_IN): ?>
                <?= Html::a('<i class="fa fa-bank m-r-sm"></i> Платежка', '#many-charge', [
                    'class' => 'btn btn-default btn-sm',
                    'data-toggle' => 'modal',
                ]); ?>
                <div id="many-charge" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8 text-left many-charge-text"
                                             style="font-size: 15px;">
                                            <span style="display: block;">Вы уверены, что хотите подготовить платежки в банк:</span>
                                            <span class="template" style="display: none;">
                                                            <span class="number"></span>. <span
                                                    class="contractor-title"></span>:
                                                            счета <span class="invoice-count"></span> шт., сумма <span
                                                    class="summary"></span>
                                                            <i class="fa fa-rub"></i>
                                                        </span>
                                            <span class="total-row"
                                                  style="display: block;font-weight: bold;">
                                                            Итого: счета <span class="total-invoice-count"></span> шт., сумма
                                                            <span class="total-summary"></span> <i
                                                    class="fa fa-rub"></i>
                                                        </span>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                            'class' => 'btn darkblue pull-right modal-many-charge ladda-button',
                                            'data-url' => Url::to(['/documents/payment-order/many-create',]),
                                            'data-style' => 'expand-right',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal">НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($canSend) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                    'class' => 'btn btn-default btn-sm document-many-send',
                    'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
                ]); ?>
                <div class="modal fade confirm-modal" id="many-send-error"
                     tabindex="-1"
                     role="modal"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close"
                                        data-dismiss="modal"
                                        aria-hidden="true"></button>
                                <h3 style="text-align: center; margin: 0">Ошибка
                                    при отправке счетов</h3>
                            </div>
                            <div class="modal-body">
                                <div class="form-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($sendDropItems) : ?>
                <div class="dropdown document-many-send-dropdown hidden">
                    <?= Html::a('Отправить <span class="caret"></span>', null, [
                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                        'id' => 'sendDropdownMenu1',
                        'data-toggle' => 'dropdown',
                        'aria-expanded' => true,
                        'style' => 'height: 28px;',
                    ]); ?>
                    <?= Dropdown::widget([
                        'items' => $sendDropItems,
                        'options' => [
                            'style' => 'right: -30px; left: auto; top: 28px;',
                            'aria-labelledby' => 'sendDropdownMenu1',
                        ],
                    ]); ?>
                </div>
            <?php endif ?>

            <?php if ($canPrint) : ?>
                <?= Html::a('<i class="fa fa-print"></i> Печать', [
                    '/documents/invoice/many-document-print',
                    'actionType' => 'pdf',
                    'type' => $searchModel->type,
                    'multiple' => '',
                ], [
                    'class' => 'btn btn-default btn-sm multiple-print',
                    'target' => '_blank',
                ]); ?>
            <?php endif ?>

            <?php if ($canCreate) : ?>
                <div id="many-create-act" class="confirm-modal fade modal"
                     role="dialog" tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="under-date"
                                                   class="col-md-3 col-sm-3 control-label label-acts-establish1">Акты
                                                создать от даты:</label>

                                            <div class="col-md-5 inp_one_line_min cal-acts-establish">
                                                <div class="input-icon">
                                                    <i class="fa fa-calendar"></i>
                                                    <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                        'id' => 'under-date',
                                                        'class' => 'form-control date-picker modal-document-date',
                                                        'data-date-viewmode' => 'years',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-3">
                                        <?= Html::a('Создать', null, [
                                            'class' => 'btn darkblue pull-right modal-many-create-act',
                                            'data-url' => Url::to([
                                                '/documents/act/many-create',
                                                'type' => $ioType,
                                                'contractor' => $model->id,
                                            ]),
                                            'style' => 'width: 100px;',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal" style="width: 100px;">Отменить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="many-create-packing-list" class="confirm-modal fade modal"
                     role="dialog" tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="under-date"
                                                   class="col-md-4 col-sm-4 control-label label-acts-establish2">Товарные
                                                Накладные создать от даты:</label>

                                            <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                                <div class="input-icon">
                                                    <i class="fa fa-calendar"></i>
                                                    <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                        'id' => 'under-date',
                                                        'class' => 'form-control date-picker modal-document-date',
                                                        'data-date-viewmode' => 'years',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-3">
                                        <?= Html::a('Создать', null, [
                                            'class' => 'btn darkblue pull-right modal-many-create-act',
                                            'data-url' => Url::to([
                                                '/documents/packing-list/many-create',
                                                'type' => $ioType,
                                                'contractor' => $model->id,
                                            ]),
                                            'style' => 'width: 100px;',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal" style="width: 100px;">Отменить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="many-create-invoice-facture" class="confirm-modal fade modal"
                     role="dialog" tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="under-date"
                                                   class="col-md-2 control-label label-acts-establish3">Счета-Фактуры
                                                создать от даты:</label>

                                            <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                                <div class="input-icon">
                                                    <i class="fa fa-calendar"></i>
                                                    <?= Html::textInput('InvoiceFacture[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                        'id' => 'under-date',
                                                        'class' => 'form-control date-picker modal-document-date',
                                                        'data-date-viewmode' => 'years',
                                                    ]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-3">
                                        <?= Html::a('Создать', null, [
                                            'class' => 'btn darkblue pull-right modal-many-create-act',
                                            'data-url' => Url::to([
                                                '/documents/invoice-facture/many-create',
                                                'type' => $ioType,
                                                'contractor' => $model->id,
                                            ]),
                                            'style' => 'width: 100px;',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-3">
                                        <button type="button" class="btn darkblue"
                                                data-dismiss="modal" style="width: 100px;">Отменить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
    <?php ActiveForm::begin([
        'method' => 'POST',
        'action' => ['/documents/invoice/generate-xls'],
        'id' => 'generate-xls-form',
    ]); ?>
    <?php ActiveForm::end(); ?>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= $this->render('@frontend/modules/documents/views/invoice/_invoices_table', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'useContractor' => true,
                'type' => $ioType,
                'id' => $model->id,
            ]); ?>
        </div>
    </div>
    <div class="modal fade" id="basic" tabindex="-1" role="basic"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="uneditable created-by"
                         style="text-align: center">создал
                        <span><?= date(DateHelper::FORMAT_USER_DATE, $model->created_at); ?></span>
                        <span><?= $model->employee ? $model->employee->fio : ''; ?></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default"
                            data-dismiss="modal">OK
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
                <span id="tooltip_contractor-debt" style="display: inline-block; text-align: center;">
                    Расшифровка «Не оплачено в срок»<br>
                    за весь период работы с клиентом.<br>
                    В блоках слева, данные за период, указанный выше.
                </span>
    <span id="tooltip_doc_upd" style="display: inline-block; text-align: center;">
                    У вас нет ни одной УПД.<br>
                    Что бы начать формировать УПД,<br>
                    измените настройки в профиле компании
                </span>
</div>
<?php yii\bootstrap\Modal::begin([
    'id' => 'store-account-modal',
    'closeButton' => false,
]); ?>
<h4 id="store-account-modal-content"></h4>
<div style="text-align: center; margin-top: 30px;">
    <button type="button" class="btn darkblue text-white"
            data-dismiss="modal">OK
    </button>
</div>
<?php yii\bootstrap\Modal::end(); ?>
<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            '/documents/invoice/many-document-print',
            'actionType' => 'pdf',
            'type' => $searchModel->type,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? (Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['/documents/invoice/many-send', 'type' => $ioType]),
        ]) . ($sendDropItems ? Html::tag('div', Html::a('Отправить  <b class="caret"></b>', '#', [
                    'id' => 'dropdownMenu3',
                    'class' => 'btn btn-sm darkblue text-white dropdown-toggle document-many-send-dropdown hidden',
                    'data-toggle' => 'dropdown',
                ]) . Dropdown::widget([
                    'items' => $sendDropItems,
                    'options' => [
                        'style' => 'left: auto; right: 0;'
                    ],
                ]), ['class' => 'dropup']) : null)
        ) : ($ioType == Documents::IO_TYPE_IN ? Html::a('<i class="fa fa-bank m-r-sm"></i> Платежка', '#many-charge', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a('Оплачены', '#many-paid-modal', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>


<?php if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF): ?>
    <?php Modal::begin([
        'id' => 'not-need-document-modal',
    ]); ?>
    <div class="col-md-12">
        <div style="margin-bottom: 20px;">
            <div class="bold">
                Для выбранных счетов в столбце <span class="document-name"></span> проставить
                «<span class="document-attribute_text"></span>».
            </div>
            Это можно будет отменить, кликнув на «<span class="document-attribute_text"></span>»
        </div>
        <div>
            <div class="bold uppercase">Внимание!</div>
            Это делается для того, чтобы в разделе финансы у вас правильно строился Управленческий
            Баланс. Это позволит убрать суммы из строки “Предоплата покупателей (авансы)”<br>
            Ситуации, когда <b>НУЖНО</b> проставить, что «<span class="document-attribute_text"></span>»:
            <ul style="list-style: decimal;padding-inline-start: 15px;">
                <li>
                    <b><i>Счет покупатель оплатил наличными</i></b>. Вы эти деньги проводить по
                    бухгалтерии НЕ будете. Соответственно покупателю выставлять
                    <span class="document-name"></span> не будете.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил через Банк</i></b>. Вы проводить эти деньги по
                    бухгалтерии обязаны. Но <span class="document-name"></span> выставляете не в КУБ,
                    а в КУБ хотите получить правильный Управленческий Баланс.
                </li>
                <li>
                    <b><i>Счет покупатель оплатил наличными, и вы пробили кассовый чек</i></b>. Вы
                    проводить эти деньги по бухгалтерии обязаны. Но <span class="document-name"></span>
                    выставляете не в КУБ, а в КУБ хотите получить правильный Управленческий
                    Баланс.
                </li>
            </ul>
        </div>
    </div>
    <div class="form-actions row col-md-12 p-r-0" style="margin-top: 15px;">
        <div class="col-xs-3">
            <?= Html::a('Применить', null, [
                'class' => 'btn darkblue pull-left modal-not-need-document-submit',
                'data-url' => Url::to(['/documents/invoice/not-need-document', 'type' => $ioType, 'param' => '']),
                'style' => 'width: 120px;color: #ffffff;',
            ]); ?>
        </div>
        <div class="col-xs-3"></div>
        <div class="col-xs-3"></div>
        <div class="col-xs-3 p-r-0">
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue pull-right',
                'data-dismiss' => 'modal',
                'style' => 'width: 100px;color: #ffffff;',
            ]); ?>
        </div>
    </div>
    <?php Modal::end(); ?>
<?php endif; ?>

<script>
    $(".act-not-need, .packing-list-not-need, .upd-not-need").click(function (e) {
        $("#not-need-document-modal span.document-name").text($(this).data("name"));
        $("#not-need-document-modal span.document-attribute_text").text($(this).data("attribute_text"));
        $("#not-need-document-modal .modal-not-need-document-submit").data("url",  $("#not-need-document-modal .modal-not-need-document-submit").data("url") + $(this).data("param"));
    });
</script>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => true,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE
    ]); ?>
<?php endif; ?>