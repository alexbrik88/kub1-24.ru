<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.03.2018
 * Time: 6:30
 */

use yii\bootstrap\Tabs;
use common\models\Company;
use common\models\Contractor;
use yii\data\ActiveDataProvider;
use frontend\models\StoreCompanyContractorSearch;
use common\models\service\Payment;
use frontend\models\CompanySiteSearch;

/* @var $this yii\web\View
 * @var $company Company
 * @var $activeTab integer
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel StoreCompanyContractorSearch
 * @var $payments Payment[]
 * @var $companySiteSearch CompanySiteSearch
 * @var $companySiteProvider ActiveDataProvider
 */
?>
<div class="portlet box darkblue details top-blue">
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="profile-form-tabs sale-increase-tabs">
                <?= Tabs::widget([
                    'options' => ['class' => 'nav-form-tabs row'],
                    'headerOptions' => ['class' => 'col-xs-3 text-center'],
                    'items' => [
                        [
                            'label' => 'Выгоды',
                            'content' => $this->render('benefit'),
                            'active' => $activeTab == Contractor::TAB_BENEFIT,
                        ],
                        [
                            'label' => 'Настройки',
                            'content' => $this->render('settings', [
                                'company' => $company,
                                'companySiteSearch' => $companySiteSearch,
                                'companySiteProvider' => $companySiteProvider,
                            ]),
                            'active' => $activeTab == Contractor::TAB_SETTINGS,
                        ],
                        [
                            'label' => 'Кабинеты',
                            'content' => $this->render('cabinets', [
                                'company' => $company,
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                'payments' => $payments,
                            ]),
                            'active' => $activeTab == Contractor::TAB_CABINETS,
                        ],
                        [
                            'label' => 'Оплата',
                            'content' =>  $this->render('payment', [
                                'company' => $company,
                            ]),
                            'active' => $activeTab == Contractor::TAB_PAYMENT,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
