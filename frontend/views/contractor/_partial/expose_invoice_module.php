<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.03.2018
 * Time: 6:33
 */

use yii\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use frontend\models\OutInvoiceSearch;
use common\models\Contractor;
use \frontend\modules\donate\models\DonateWidgetSearch;

/* @var $outInvoiceSearch OutInvoiceSearch
 * @var $outInvoiceProvider ActiveDataProvider
 * @var $donateWidgetSearch DonateWidgetSearch
 * @var $donateWidgetProvider ActiveDataProvider
 * @var $activeTab integer
 */
?>
<div class="portlet box darkblue details top-blue">
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="profile-form-tabs sale-increase-invoice-tabs">
                <?= Tabs::widget([
                    'options' => ['class' => 'nav-form-tabs row'],
                    'headerOptions' => ['class' => 'col-xs-3 text-center'],
                    'items' => [
                        [
                            'label' => 'Выгоды',
                            'content' => $this->render('invoice_benefit'),
                            'active' => $activeTab == Contractor::TAB_INVOICE_BENEFIT,
                        ],
                        [
                            'label' => 'Ссылки',
                            'content' => $this->render('links', [
                                'outInvoiceSearch' => $outInvoiceSearch,
                                'outInvoiceProvider' => $outInvoiceProvider,
                                'donateWidgetSearch' => $donateWidgetSearch,
                                'donateWidgetProvider' => $donateWidgetProvider,
                            ]),
                            'active' => $activeTab == Contractor::TAB_INVOICE_LINKS,
                        ],
                        [
                            'label' => 'Настройки',
                            'content' => $this->render('invoice_settings'),
                            'active' => $activeTab == Contractor::TAB_INVOICE_SETTINGS,
                        ],
                        [
                            'label' => 'Оплата',
                            'content' => $this->render('invoice_payment'),
                            'active' => $activeTab == Contractor::TAB_INVOICE_PAYMENT,
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
