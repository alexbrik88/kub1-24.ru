<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.04.2018
 * Time: 7:21
 */

use common\models\Company;
use common\models\service\StoreTariff;
use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\models\PaymentStoreForm;
use yii\helpers\Url;
use common\components\TextHelper;
use yii2mod\slider\IonSlider;
use yii\web\JsExpression;

/* @var $company Company
 * @var $storeTariff StoreTariff
 */

$storeTariffs = StoreTariff::find()->asArray()->all();
$paymentStoreForm = new PaymentStoreForm();
$paymentStoreForm->tariffId = StoreTariff::TARIFF_2_CABINETS;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
    <div class="row" style="padding-top: 20px;">
        <div class="col-md-12">
            <span class="text-bold" style="font-size: 19px;text-transform: uppercase;">
                5-ть личных кабинетов БЕСПЛАТНО БЕЗ ОГРАНИЧЕНИЯ по времени <br><br>
	            Если нужно больше, то их можно докупить.
            </span>
        </div>
        <div class="col-md-5 col-md-push-7">
            <div class="cabinet-payment row" style="margin-left: 0;margin-right: 0;">
                <span class="payment-header">
                    Выберите количество дополнительных личных кабинетов
                </span>
                <?= Html::activeHiddenInput($paymentStoreForm, 'tariffId'); ?>
                <?= IonSlider::widget([
                    'name' => 'cabinet-slider',
                    'pluginOptions' => [
                        'min' => 1,
                        'max' => 100,
                        'from' => 1,
                        'values' => [
                            1, 2, 5, 10, 20, 50, 100,
                        ],
                        'grid' => true,
                        'onChange' => new JsExpression('
                            function (data) {
                                var $tariff =  ' . json_encode($storeTariffs) . ';

                                changeTariff(data.from);

                                function changeTariff($number) {
                                    var $newTariff = $tariff[$number];
                                    $(".cabinets-count span").text($newTariff.cabinets_count);
                                    $(".payment-year .cabinets-amount span").text($newTariff.total_amount);
                                    $(".payment-month .cabinets-amount span").text(+$newTariff.cabinets_count * 300);
                                    $("#paymentstoreform-tariffid").val($newTariff.id);
                                }
                            }
                        '),
                    ],
                    'options' => [
                        'id' => 'ion-slider-cabinet-tariff',
                    ],
                ]); ?>
                <div class="col-md-12">
                    <div class="col-md-6 payment-month">
                        <div class="payment-info-block">
                            <div class="cabinets-count">
                                Количество: <span class="bold-text">2</span>
                            </div>
                            <div class="duration">
                                Срок: <span class="bold-text">1 месяц</span>
                            </div>
                            <div class="cabinets-amount">
                                Стоимость: <span class="bold-text">600</span> <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 payment-year">
                        <div class="payment-info-block">
                            <div class="cabinets-count">
                                Количество: <span class="bold-text">2</span>
                            </div>
                            <div class="duration">
                                Срок: <span class="bold-text">12 месяцев</span>
                            </div>
                            <div class="cabinets-amount">
                                Стоимость: <span class="bold-text">6000</span> <i class="fa fa-rub"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 pay-month">
                        <?php if (YII_ENV_DEV || Yii::$app->user->identity->company->id !== 11270): ?>
                            <?= Html::button('Оплатить', [
                                'class' => 'btn darkblue tooltip2 pay',
                                'data-tooltip-content' => '#tooltip_pay-month',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6 pay-year">
                        <?php if (YII_ENV_DEV || Yii::$app->user->identity->company->id !== 11270): ?>
                            <?= Html::button('Оплатить', [
                                'class' => 'btn darkblue tooltip2 pay',
                                'data-tooltip-content' => '#tooltip_pay-year',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-pull-5 created-scroll">
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                <tr class="heading">
                    <th class="text-center">Кол-во кабинетов</th>
                    <th class="text-center">
                        Стоимость <br>
                        1 кабинета в месяц <br>
                        При оплате за МЕСЯЦ
                    </th>
                    <th class="text-center">
                        Стоимость <br>
                        1 кабинета в месяц <br>
                        При оплате за ГОД
                    </th>
                </tr>
                <?php foreach (StoreTariff::find()->all() as $storeTariff): ?>
                    <tr>
                        <td class="text-center"><?= $storeTariff->cabinets_count; ?></td>
                        <td class="text-center">
                            300 <i class="fa fa-rub"></i>
                        </td>
                        <td class="text-center">
                            <?= $storeTariff->cost_one_month_for_one_cabinet; ?>
                            <i class="fa fa-rub"></i>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="tooltip-template" style="display: none;">
        <span id="tooltip_pay-month" style="display: inline-block; text-align: center;">
            <span style="display: block;font-size: 16px;font-weight: bold;margin-bottom: 10px;">Оплата</span>
            <?= Html::a('Картой', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #45b6af;color: white;',
                'data-href' => Url::to(['/store-payment/online-payment', 'period' => PaymentStoreForm::PERIOD_MONTH]),
            ]); ?>
            <?= Html::a('Выставить счет', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #ffaa24;color: #ffffff;',
                'data-href' => Url::to(['/store-payment/expose-invoice', 'period' => PaymentStoreForm::PERIOD_MONTH]),
            ]); ?>
        </span>
        <span id="tooltip_pay-year" style="display: inline-block; text-align: center;">
            <span style="display: block;font-size: 16px;font-weight: bold;margin-bottom: 10px;">Оплата</span>
            <?= Html::a('Картой', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #45b6af;color: white;',
                'data-href' => Url::to(['/store-payment/online-payment', 'period' => PaymentStoreForm::PERIOD_YEAR]),
            ]); ?>
            <?= Html::a('Выставить счет', null, [
                'class' => 'btn darkblue',
                'style' => 'background: #ffaa24;color: #ffffff;',
                'data-href' => Url::to(['/store-payment/expose-invoice', 'period' => PaymentStoreForm::PERIOD_YEAR]),
            ]); ?>
        </span>
    </div>
<?php $this->registerJs('
    $(document).ready(function () {
        $(document).on("click", "#tooltip_pay-year a, #tooltip_pay-month a", function () {
            var $data = $("#paymentstoreform-tariffid:hidden").serialize();
            $.post($(this).data("href"), $data, function($data) {
                $("button.pay").append($data);
            });
        });
    });
'); ?>