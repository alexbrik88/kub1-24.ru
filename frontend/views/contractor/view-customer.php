<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use common\widgets\Modal;
use frontend\models\Documents;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\web\View;

/**
 * @var View       $this
 * @var int        $type
 * @var int        $ioType
 * @var string     $tab
 * @var Contractor $model
 * @var Company    $company
 * @var mixed[]    $tabData
 */
$this->title = $model->nameWithType;
$this->context->layoutWrapperCssClass = 'customer';

if (!isset($type)) {
    $type = $model->type;
}
if (!isset($ioType)) {
    $ioType = $model->type;
}
$tabLink = Url::toRoute([
    'contractor/view',
    'type' => $type,
    'id' => $model->id,
    'per-page' => Yii::$app->request->get('per-page')
]);

$tabFile = ArrayHelper::remove($tabData, 'tabFile');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-store',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'side' => 'top',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

$user = Yii::$app->user->identity;
$company = $user->company;
$isStoreActive = $model->getStoreCompanyContractors()
    ->andWhere(['status' => StoreCompanyContractor::STATUS_ACTIVE])
    ->exists();
$hasStore = $model->getStoreCompanyContractors()->one();
$canAddStoreCabinet = $company->canAddStoreCabinet();
$disabled = !$canAddStoreCabinet && !$isStoreActive;
$invCount = $company->getInvoiceLeft();

$contractorNavColumnClass = 'col-xs-12 col-sm-4 col-md-1-5 col-lg-1-5';
/*if ($model->is_customer) {
    $tabInvoice = [
        'label' => ($tab == 'autoinvoice' ? 'АвтоСчета' : 'Счета'.($model->is_seller ? ' исходяшие' : '')) .
            Html::a('<b class="caret" style="margin: 0;"></b>', '#', [
                'data-toggle' => 'dropdown',
                'class' => 'dropdown-toggle',
            ]) . Dropdown::widget([
                'items' => [
                    ['label' => 'Счета', 'url' => $tabLink],
                    ['label' => 'АвтоСчета', 'url' => $tabLink . '&tab=autoinvoice'],
                ],
            ]),
        'url' => $tabLink . ($tab === 'autoinvoice' ? '&tab=autoinvoice' : ''),
        'active' => $tab == null || $tab == 'autoinvoice',
        'options' => [
            'class' => $contractorNavColumnClass . ' dropdown tab-label-dropdown',
        ],
        'encode' => false,
    ];
} else {
    $tabInvoice = [
        'label' => 'Счета',
        'url' => $tabLink,
        'active' => $tab == null,
        'options' => [
            'class' => $contractorNavColumnClass,
        ],
    ];
}*/

$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->getInvoicesAuto()->exists();

$dossierAllowed = $model->face_type == Contractor::TYPE_LEGAL_PERSON;
//$needDossierConfirm = $company->needDossierConfirm($model);

if ($model->is_customer) {
    if ($tab == 'autoinvoice') {
        $label = '<span class="ia-tab pr-2" data-href="' . $tabLink . '&tab=autoinvoice' . '">АвтоСчета</span>';
    } else {
        $label = '<span class="ia-tab pr-2" data-href="' . $tabLink . '">Счета</span>';
    }
    $tabInvoice = [
        'label' => ($tab == 'autoinvoice' ? 'АвтоСчета' : 'Счета') . Html::a('<b class="caret" style="margin: 0;"></b>', '#', [
            'data-toggle' => 'dropdown',
            'class' => 'dropdown-toggle',
        ]) . Dropdown::widget([
            'items' => [
                ['label' => 'Счета', 'url' => $tabLink],
                ['label' => 'АвтоСчета', 'url' => $tabLink . '&tab=autoinvoice'],
            ],
        ]),
        'url' => $tabLink . ($tab === 'autoinvoice' ? '&tab=autoinvoice' : ''),
        'active' => $tab == null || $tab == 'autoinvoice',
        'options' => [
            'class' => $contractorNavColumnClass . ' dropdown tab-label-dropdown',
        ],
        'encode' => false,
    ];
} else {
    $tabInvoice = [
        'label' => 'Счета',
        'url' => $tabLink,
        'options' => [
            'class' => $contractorNavColumnClass,
        ],
    ];
}

$this->registerJs('
    $(document).on("change", "#activate_store_account:not(:checked)", function() {
        changeCabinet($("#activate_store_account").serialize());
    });
    $(document).on("click", "#add-store-account .yes", function() {
        changeCabinet({store_account: 1});
    });

    function changeCabinet(val) {
        var isChecked = $("#activate_store_account").prop("checked");
        $.post("' . Url::to(['store-account', 'id' => $model->id]) . '", val, function(data) {
            var $icon = "success";
            if (data.message != undefined) {
                var $message = data.message.replace(new RegExp("&quot;", "g"), "\\"");
            }
            if (data.value != undefined) {
                if (data.value == false) {
                    $icon = "error";
                }
                $("#activate_store_account").prop("checked", data.value);
            } else {
                $("#activate_store_account").prop("checked", !isChecked);
            }
            var $value = $("#activate_store_account").is(":checked");
            if ($value) {
                $("#activate_store_account").removeAttr("data-toggle");
                $("#activate_store_account").removeAttr("data-target");
            } else if (data.canAddStoreCabinet == 1) {
                $("#activate_store_account").attr("data-toggle", "modal");
                $("#activate_store_account").attr("data-target", "#add-store-account");
            }

            if (data.label != undefined) {
                $(".contractor-store-label").text(data.label);
            }
            swal({
                html: true,
                text: $message,
                icon: $icon,
            });
        });
    }

    var $customerBlockIn = $(".contractor-in-block");
    var $customerBlockOut = $(".contractor-out-block");
    $(".update-contractor").click(function (e) {
        if ($(".contractor-info-tab").hasClass("active")) {
            $customerBlockIn.show();
            $customerBlockOut.hide();
        } else {
            location.href = $(".contractor-info-tab > a").attr("href") + "&edit=1";
        }
    });

    $(".undo-contractor").click(function (e) {
        $customerBlockIn.hide();
        $customerBlockOut.show();
    });
');
?>
<a class="back-to-customers" href="<?= Url::to(['/contractor/index', 'type' => $type]) ?>">
    Назад к списку
</a>
<div class="row">
    <div class="col-md-6">
        <div class="portlet customer-info" style="border: 1px solid #4276a4;">
            <div class="portlet-title">
                <div class="col-md-9 caption">
                    <?= Html::encode($this->title); ?>
                    <?php if (!$model->verified && $model->face_type != Contractor::TYPE_PHYSICAL_PERSON && $model->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON) { ?>
                        <?= $this->render('../dossier/_popup-confirm', [
                            'link' => Url::toRoute(['/dossier', 'id' => $model->id]),
                            'button' => [
                                'title' => 'Проверить ' . ($model->type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика'),
                                'class' => 'dossier-bg'
                            ]
                        ]) ?>
                    <?php } ?>
                </div>
                <div class="actions">
                    <a class="btn darkblue btn-sm info-button"
                       data-toggle="modal" href="#basic">
                        <i class="icon-info" style="color:white"></i>
                    </a>
                    <?php if (Yii::$app->user->can(\frontend\rbac\permissions\Contractor::UPDATE, [
                        'model' => $model,
                    ])
                    ): ?>
                        <a href="javascript:;" class="darkblue btn-sm btn-link update-contractor contractor-out-block"
                           title="Редактировать">
                            <i class="icon-pencil"></i>
                        </a>
                        <?= Html::submitButton('<span class="ico-Save-smart-pls"></span>', [
                            'class' => 'btn darkblue btn-sm contractor-in-block',
                            'title' => 'Сохранить',
                            'form' => 'update-contractor-form',
                            'style' => 'color: #FFF;display: none;',
                        ]); ?>
                        <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', 'javascript:;', [
                            'class' => 'btn darkblue btn-sm contractor-in-block undo-contractor',
                            'title' => 'Отменить',
                            'style' => 'color: #FFF;display: none;',
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body" style="margin-bottom: 10px;">
                <table id="datatable_ajax" class="table"
                       style="margin-bottom: 0;">
                    <tr>
                        <td>
                            <?php $name = $model->getRealContactName(); ?>
                            <?php if (!empty($name)): ?>
                                <span class="customer-characteristic">Контакт:</span>
                                <span><?= $name; ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $phone = $model->getRealContactPhone(); ?>
                            <?php if (!empty($phone)): ?>
                                <span class="customer-characteristic">Телефон:</span>
                                <span><?= $phone; ?></span>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php $email = $model->getRealContactEmail(); ?>
                            <?php if (!empty($email)): ?>
                                <span class="customer-characteristic">E-mail:</span>
                                <a href="mailto:<?= $email; ?>"><?= $email; ?></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <?php if ($model->type == Contractor::TYPE_CUSTOMER): ?>
                    <table class="table no_mrg_bottom" style="width: auto;">
                        <tr>
                            <td style="line-height: 32px; white-space: nowrap;">
                                    <span class="customer-characteristic contractor-store-label">
                                        <?= $hasStore ?
                                            ($isStoreActive ? 'Отключить кабинет' : 'Включить кабинет')
                                            : ($model->type == Contractor::TYPE_CUSTOMER ?
                                                Html::a('Создать кабинет покупателя',
                                                    Url::to(['/contractor/sale-increase'])) :
                                                'Создать кабинет'); ?>
                                    </span>
                            </td>
                            <td style="padding-left: 0;">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <label class="rounded-switch<?= $disabled ? ' tooltip2-store' : ''; ?>"
                                                   for="activate_store_account"
                                                   data-tooltip-content="<?= $disabled ? ('#tooltip_pay-to-add-cabinet-' . $model->id) : ''; ?>"
                                                   style="margin: 4px 0 0;">
                                                <?= Html::checkbox('store_account', $isStoreActive, [
                                                    'id' => 'activate_store_account',
                                                    'class' => 'switch',
                                                    'disabled' => $disabled,
                                                    'data-toggle' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                        (!$isStoreActive && !$disabled ?
                                                            (empty($model->director_email) ? 'modal' : 'modal') : null) :
                                                        'modal',
                                                    'data-target' => $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                                        !$isStoreActive && !$disabled ?
                                                            (empty($model->director_email) ?
                                                                '#empty-contractor-email' :
                                                                '#add-store-account') : null :
                                                        '#no-rights-store-account',
                                                ]); ?>
                                                <span class="sliderr no-gray yes-yellow round"></span>
                                            </label>
                                            <?php if ($disabled): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                    <span id="tooltip_pay-to-add-cabinet-<?= $model->id; ?>"
                                                          style="display: inline-block; text-align: center;">
                                                        Кабинет можно добавить после
                                                        <?= Html::a('оплаты', Url::to([
                                                            '/contractor/sale-increase',
                                                            'activeTab' => Contractor::TAB_PAYMENT,
                                                        ])); ?>
                                                    </span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                        <td style="vertical-align: top;">
                                        <span class="tooltip2-right-click ico-question valign-middle"
                                              data-tooltip-content="<?= $isStoreActive ? '#tooltip_add_stamp' : '#tooltip_add_cabinet'; ?>"
                                              style="vertical-align: top;cursor: pointer;">
                                        </span>
                                            <?php if (!$isStoreActive): ?>
                                                <div class="tooltip-template" style="display: none;">
                                                        <span id="tooltip_add_cabinet"
                                                              style="display: inline-block;">
                                                            Самый быстрый способ увеличить продажи товара – упростить процесс заказа товара. <br>
                                                            Для этого подключите покупателей к вашему складу! <br>
                                                            <?= Html::a('Узнать подробнее',
                                                                Url::to(['/contractor/sale-increase'])); ?>
                                                        </span>
                                                </div>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="btn-group pull-right title-buttons portlet" style="margin-bottom: 15px;">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                    'ioType' => $ioType,
                ])
                && $model->company_id !== null
            ): ?>
                <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                    <a href="<?= Url::to([
                        '/documents/invoice/create',
                        'type' => $ioType,
                        'contractorId' => $model->id
                    ]); ?>"
                       class="btn yellow">
                        <i class="fa fa-plus"></i> СЧЕТ
                    </a>
                <?php else : ?>
                    <button class="btn yellow action-is-limited"
                        <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                        <i class="fa fa-plus"></i> СЧЕТ
                    </button>
                    <?php if ($invCount === 0 && date(DateHelper::FORMAT_USER_DATE,
                            $company->show_invoice_payment_popup_date) == date(DateHelper::FORMAT_USER_DATE)): ?>
                        <?= $this->render('@frontend/widgets/views/invoicePaymentModal', [
                            'company' => $company,
                        ]); ?>
                    <?php endif ?>
                <?php endif ?>
            <?php endif; ?>
        </div>

        <div class="clearfix"></div>
        <div style="width:288px" class="pull-right">
            <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="clearfix"></div>

        <div class="btn-group pull-right portlet" style="margin-top: 10px; margin-bottom: 5px;">
            <?php if ($model->type == Contractor::TYPE_CUSTOMER && !$hasAutoinvoice && $tab != 'autoinvoice') : ?>
                <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                    <?= Html::a('<i class="fa fa-plus"></i> АвтоСчет', [
                        'create-autoinvoice',
                        'type' => Documents::IO_TYPE_OUT,
                        'id' => $model->id,
                    ], [
                        'class' => 'btn yellow',
                    ]) ?>
                <?php else : ?>
                    <button class="btn yellow action-is-limited">
                        <i class="fa fa-plus"></i> АвтоСчет
                    </button>
                <?php endif ?>
            <?php endif; ?>

            <?php if ($model->type == Contractor::TYPE_CUSTOMER && !in_array($user->currentEmployeeCompany->employee_role_id,
                    [EmployeeRole::ROLE_ACCOUNTANT, EmployeeRole::ROLE_ASSISTANT])): ?>
                <?= Html::a('АвтоСбор Долгов', Url::to([
                    '/payment-reminder/index',
                    'PaymentReminderMessageContractorSearch' => [
                        'contractor' => $model->id,
                    ],
                ]), [
                    'class' => 'btn yellow',
                    'style' => 'margin-left:15px;',
                ]); ?>
            <?php endif; ?>

            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Collate::CREATE)) {
                $url = Url::to(['/contractor/collate', 'step' => 'form', 'id' => $model->id]);
                Modal::begin([
                    'id' => 'modal-collate-report',
                    'closeButton' => false,
                    'toggleButton' => [
                        'tag' => 'button',
                        'id' => 'collate-report-button',
                        'label' => '<i class="fa fa-plus"></i> Акт сверки',
                        'class' => 'btn yellow',
                        'style' => 'margin-left:15px; padding: 7px 10px; border-radius: 0; text-transform: uppercase;',
                    ],
                ]);

                $pjax = Pjax::begin([
                    'id' => 'collate-pjax-container',
                    'enablePushState' => false,
                    'enableReplaceState' => false,
                    'linkSelector' => '.collate-pjax-link',
                    'timeout' => 5000,
                ]);

                $pjax->end();

                Modal::end();

                $this->registerJs('
                $(document).on("click", "#collate-report-button", function() {
                    $.pjax.reload("#collate-pjax-container", {url: ' . json_encode($url) . ', push: false, replace: false, timeout: 5000})
                });
                $(document).on("pjax:complete", "#collate-pjax-container", function(event) {
                    if ($("#collate-pjax-container .date-picker").length) {
                        $("#collate-pjax-container .date-picker").datepicker({
                            format: "dd.mm.yyyy",
                            keyboardNavigation: false,
                            forceParse: false,
                            language: "ru",
                            autoclose: true
                        }).on("change.dp", dateChanged);

                        function dateChanged(ev) {
                            if (ev.bubbles == undefined) {
                                var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                                if (ev.currentTarget.value == "") {
                                    if ($input.data("last-value") == null) {
                                        $input.data("last-value", ev.currentTarget.defaultValue);
                                    }
                                    var $lastDate = $input.data("last-value");
                                    $input.datepicker("setDate", $lastDate);
                                } else {
                                    $input.data("last-value", ev.currentTarget.value);
                                }
                            }
                        };
                    }
                });
                $(document).on("hidden.bs.modal", "#modal-collate-report", function () {
                    $("#collate-pjax-container").html("");
                })
                ');
            } ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<div style="margin: 0 0 25px 5px; border-bottom: 1px solid #4276a4">
    <?= Nav::widget([
        'id' => 'contractor-menu',
        'items' => [
            $tabInvoice,
            [
                'label' => 'Договоры',
                'url' => $tabLink . '&tab=agreements',
                'active' => $tab == 'agreements',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
            ],
            [
                'label' => 'Аналитика',
                'url' => $tabLink . '&tab=analytics',
                'active' => $tab == 'analytics',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'visible' => $ioType == Contractor::TYPE_CUSTOMER,
            ],
            [
                'label' => $model->isSellerCustomer ? 'Оборот' : ($ioType == Contractor::TYPE_CUSTOMER ? 'Продажи' : 'Закупки'),
                'url' => $tabLink . '&tab=selling',
                'active' => $tab == 'selling',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'visible' => ($ioType == Contractor::TYPE_CUSTOMER || $ioType == Contractor::TYPE_SELLER && !$model->is_agent)
            ],
            [
                'label' => 'Отчеты',
                'url' => $tabLink . '&tab=agent_report',
                'active' => $tab == 'agent_report',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'visible' => ($ioType == Contractor::TYPE_SELLER && $model->is_agent),
            ],
            [
                'label' => 'Карточка',
                'url' => $tabLink . '&tab=info',
                'active' => $tab == 'info',
                'options' => [
                    'class' => $contractorNavColumnClass . ' contractor-info-tab',
                ],
            ],
            /*[
                'label' => 'Досье',
                'url' => str_replace('/contractor/view', '/dossier', $tabLink),
                'active' => $tab === 'dossier',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'visible' => $dossierAllowed && !$needDossierConfirm,
            ],*/
            /*[
                'label' => 'Досье',
                'active' => $tab === 'dossier',
                'options' => [
                    'class' => $contractorNavColumnClass,
                ],
                'linkOptions' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#popup-confirm'
                ],
                'visible' => $dossierAllowed && $needDossierConfirm,
            ],*/
        ],
        'options' => ['class' => 'nav-form-tabs row nav nav-tabs'],
    ]); ?>
</div>

<?php if ($tabFile) : ?>
    <div class="tab-content">
        <div id="tab1" class="tab-pane invoice-tab active">
            <?= $this->render($tabFile, $tabData); ?>
        </div>
    </div>
<?php endif ?>

<div id="add-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        Вы уверены, что хотите создать личный кабинет для <br>
                        данного покупателя? После нажатия на кнопку ДА, <br>
                        покупателю будет отослано на e-mail письмо <br>
                        с приглашением в личный кабинет. <br>
                        В письме также будет логин и пароль для входа.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="empty-contractor-email" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У данного покупателя не заполнено поле с e-mail. <br>
                        Заполните в закладке "Карточка покупателя".
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="no-rights-store-account" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">
                        У вас не достаточно прав для создания кабинета. Обратитесь к руководителю.
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">
                            ОК
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
