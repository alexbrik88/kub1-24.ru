<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.03.2018
 * Time: 5:40
 */

use yii\bootstrap\Tabs;
use common\models\Company;
use yii\data\ActiveDataProvider;
use frontend\models\StoreCompanyContractorSearch;
use common\models\service\Payment;
use frontend\models\CompanySiteSearch;
use frontend\models\OutInvoiceSearch;
use frontend\modules\donate\models\DonateWidgetSearch;
use common\models\Contractor;

/* @var $this yii\web\View
 * @var $company Company
 * @var $activeTab integer
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel StoreCompanyContractorSearch
 * @var $payments Payment[]
 * @var $companySiteSearch CompanySiteSearch
 * @var $companySiteProvider ActiveDataProvider
 * @var $outInvoiceSearch OutInvoiceSearch
 * @var $outInvoiceProvider ActiveDataProvider
 * @var $donateWidgetSearch DonateWidgetSearch
 * @var $donateWidgetProvider ActiveDataProvider
 */

$this->title = 'Увеличение продаж';
?>
<a class="back-to-customers"
   href="<?= Yii::$app->request->referrer ?>">Назад к
    списку</a>
<div class="profile-form-tabs">
    <?= Tabs::widget([
        'options' => ['class' => 'nav-form-tabs row main-tabs'],
        'headerOptions' => ['class' => 'col-xs-3 text-center', 'style' => 'float: left;width: 289px;'],
        'items' => [
            [
                'label' => 'Кабинет покупателя',
                'content' => $this->render('_partial/contractor_store', [
                    'company' => $company,
                    'activeTab' => $activeTab,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'payments' => $payments,
                    'companySiteSearch' => $companySiteSearch,
                    'companySiteProvider' => $companySiteProvider,
                ]),
                'active' => in_array($activeTab, [Contractor::TAB_BENEFIT, Contractor::TAB_SETTINGS,
                    Contractor::TAB_CABINETS, Contractor::TAB_PAYMENT,]),
            ],
            [
                'label' => 'Модуль выставления счетов',
                'content' => $this->render('_partial/expose_invoice_module', [
                    'activeTab' => $activeTab,
                    'outInvoiceSearch' => $outInvoiceSearch,
                    'outInvoiceProvider' => $outInvoiceProvider,
                    'donateWidgetSearch' => $donateWidgetSearch,
                    'donateWidgetProvider' => $donateWidgetProvider,
                ]),
                'active' => in_array($activeTab, [Contractor::TAB_INVOICE_BENEFIT, Contractor::TAB_INVOICE_LINKS,
                    Contractor::TAB_INVOICE_SETTINGS, Contractor::TAB_INVOICE_PAYMENT,]),
            ],
        ],
    ]); ?>
</div>
