<?php

use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$this->title = 'Редактировать ' . ($model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'товар' : 'услугу');

$this->context->layoutWrapperCssClass = 'page-good';
?>
<div class="update-product">
    <div style="border-bottom: 1px solid #eee;margin-bottom: 10px;">
        <h3 class="page-title" style="margin-bottom: 10px;display: inline-block;"><?= $this->title ?></h3>
        <?= Html::a('<i class="ico-Cancel-smart-pls fs" style="color: white;"></i>', Url::to(['view', 'productionType' => $model->production_type, 'id' => $model->id]), [
            'class' => 'darkblue btn-sm btn-link',
            'title' => 'Отменить',
            'style' => 'display: inline-block;margin-top:6px;float: right;',
        ]); ?>
        <?= Html::a('<i class="fa fa-floppy-o fa-2x" style="color: white;font-size: 14px!important;"></i>', null, [
            'class' => 'darkblue btn-sm btn-link submit-product-form',
            'title' => 'Сохранить',
            'style' => 'display: inline-block;margin-top:6px;float:right;margin-right:5px;',
        ]); ?>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]); ?>
</div>
