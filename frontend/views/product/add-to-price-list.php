<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.10.2018
 * Time: 22:50
 */

use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\ProductSearch;
use frontend\widgets\StoreFilterWidget;
use common\components\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\components\TextHelper;
use common\components\helpers\ArrayHelper;
use common\models\product\ProductUnit;
use common\models\product\ProductGroup;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $defaultSortingAttr string */
/* @var $priceList PriceList */
/* @var $store */

$this->title = Product::$productionTypes[$productionType];
$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];
$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();
if ($productionType == 1) {
    $emptyMessage = 'Вы еще не добавили ни одного товара';
} elseif ($productionType == 0) {
    $emptyMessage = 'Вы еще не добавили ни одной услуги';
} else {
    $emptyMessage = 'Ничего не найдено';
}
$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';
}
$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$hasFilters = !($searchModel->filterStatus == ProductSearch::IN_WORK) ||
    (bool)$searchModel->filterComment ||
    (bool)$searchModel->filterImage ||
    (bool)$searchModel->filterDate;
$cookies = Yii::$app->response->cookies;

$this->registerJs('
    $(document).on("click", "input.select-on-check-all", function(e) {
        $("input.product_checker").prop("checked", this.checked).uniform("refresh");
        $("input.select-on-check-all").prop("checked", this.checked).uniform("refresh");
    });
    $(document).on("change", "input.product_checker", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#mass_actions").fadeIn();
            if ($("input.product_checker:not(:checked)").length) {
                $(".select-on-check-all").prop("checked", false).uniform("refresh");
            } else {
                $(".select-on-check-all").prop("checked", true).uniform("refresh");
            }
        } else {
            $("#mass_actions").fadeOut();
            $(".select-on-check-all").prop("checked", false).uniform("refresh");
        }
    });
    $(document).on("click", "#confirm-many-delete", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
    $(".tooltip-help").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "side": "right",
        "contentAsHTML": true,
    });
');
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons"></div>
    <h3 class="page-title">
        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
            <?= StoreFilterWidget::widget([
                'pageTitle' => $this->title,
                'store' => $store,
                'productionType' => $productionType,
                'help' => null,
                'company' => $company,
            ]); ?>
        <?php else: ?>
            <?= Html::encode($this->title); ?>
        <?php endif; ?>
    </h3>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12" style="font-size: 15px;margin-bottom: 25px;color: #f3565d;">
        Сейчас отмечены галочками все <?= $productionType == Product::PRODUCTION_TYPE_GOODS ? 'товары' : 'услуги'; ?>.
        Снимите галочки с тех <?= $productionType == Product::PRODUCTION_TYPE_GOODS ? 'товаров' : 'услуг'; ?>, которые
        вам не нужны в прайс-листе.<br>
        После завершения нажмите кнопку «Сохранить»
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption caption_for_input">
            <?= $tableHeader[$productionType] . ': ' . $dataProvider->totalCount; ?>
        </div>
        <div class="tools search-tools tools_button col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => Url::current([$searchModel->formName() => null]),
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                        <?= $form->field($searchModel, 'title')->textInput([
                            'placeholder' => 'Поиск...',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="filter-block pull-right">
            <div class="dropdown pull-right">
                <?= Html::a('Фильтр  <span class="caret"></span>', null, [
                    'class' => 'btn btn-default btn-sm dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'style' => $hasFilters ? 'color: #f3565d!important;border-color: #f3565d!important;' : 'color: #def1ff;',
                ]); ?>
                <ul class="dropdown-menu">
                    <li>
                        <div class="row">
                            <span class="filter-label">Показывать</span>
                            <?= Html::activeDropDownList($searchModel, 'filterStatus', $filterStatusItems, [
                                'class' => 'form-control',
                                'data-id' => 'filterStatus',
                            ]); ?>
                        </div>
                    </li>
                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                        <li>
                            <div class="row text-right">
                                <span class="filter-label">Наличие картинки</span>
                                <?= Html::activeDropDownList($searchModel, 'filterImage', [
                                    ProductSearch::ALL => 'Все',
                                    ProductSearch::HAS_IMAGE => 'Есть',
                                    ProductSearch::NO_IMAGE => 'Нет',
                                ], [
                                    'class' => 'form-control',
                                    'data-id' => 'filterImage',
                                ]); ?>
                            </div>
                        </li>
                        <li>
                            <div class="row text-right">
                                <span class="filter-label">Наличие описания</span>
                                <?= Html::activeDropDownList($searchModel, 'filterComment', [
                                    ProductSearch::ALL => 'Все',
                                    ProductSearch::HAS_COMMENT => 'Есть',
                                    ProductSearch::NO_COMMENT => 'Нет',
                                ], [
                                    'class' => 'form-control',
                                    'data-id' => 'filterComment',
                                ]); ?>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li style="border-bottom: 0;">
                        <div class="row text-right">
                            <span class="filter-label">Добавлен</span>
                            <?= Html::activeDropDownList($searchModel, 'filterDate', [
                                ProductSearch::ALL => 'Все',
                                ProductSearch::FILTER_YESTERDAY => 'Вчера',
                                ProductSearch::FILTER_TODAY => 'Сегодня',
                                ProductSearch::FILTER_WEEK => 'Неделя',
                                ProductSearch::FILTER_MONTH => 'Месяц',
                            ], [
                                'class' => 'form-control',
                                'data-id' => 'filterDate',
                                'data-url' => Url::to(['filter-date', 'actionType' => 'set']),
                            ]); ?>
                            <?= Html::activeHiddenInput($searchModel, 'dateFrom', [
                                'data-id' => 'dateFrom',
                            ]); ?>
                            <?= Html::activeHiddenInput($searchModel, 'dateTo', [
                                'data-id' => 'dateTo',
                            ]); ?>
                        </div>
                        <div class="row block-date"
                             style="display: <?= $searchModel->filterDate == ProductSearch::ALL ? 'none;' : 'block;'; ?>">
                            <span class="filter-label"></span>
                            <div class="change-period-date">
                                <span class="glyphicon glyphicon-chevron-left pull-left minus"
                                      data-url="<?= Url::to(['filter-date', 'actionType' => 'minus']); ?>"></span>
                                <span class="date-text">
                                    <?php if (in_array($searchModel->filterDate, [ProductSearch::FILTER_YESTERDAY, ProductSearch::FILTER_TODAY])): ?>
                                        <?= $searchModel->dateFrom; ?>
                                    <?php elseif ($searchModel->filterDate == ProductSearch::FILTER_WEEK): ?>
                                        <?= "{$searchModel->dateFrom} - {$searchModel->dateTo}"; ?>
                                    <?php else: ?>
                                        <?= FlowOfFundsReportSearch::$month[date('m', strtotime($searchModel->dateFrom))] . ' ' . date('Y', strtotime($searchModel->dateFrom)); ?>
                                    <?php endif; ?>
                                </span>
                                <span class="glyphicon glyphicon-chevron-right pull-right plus"
                                      data-url="<?= Url::to(['filter-date', 'actionType' => 'plus']); ?>"></span>
                            </div>
                        </div>
                    </li>
                    <li style="border-bottom: 0;">
                        <div class="form-actions">
                            <div class="row action-buttons buttons-fixed">
                                <div class="spinner-button col-sm-3 col-xs-3"
                                     style="width: 35%;padding-left: 0;">
                                    <?= Html::button('Найти', [
                                        'class' => 'apply-filters btn darkblue widthe-100',
                                    ]); ?>
                                </div>
                                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 30%;"></div>
                                <div class="spinner-button col-sm-1 col-xs-1"
                                     style="width: 35%;padding-right: 0;">
                                    <?= Html::button('Сбросить', [
                                        'class' => 'apply-default-filters btn darkblue widthe-100',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
        <?= Html::hiddenInput('checkedProducts', $priceList->priceListCheckedProducts->checked_products, [
            'id' => 'checkedProducts',
            'data-url' => Url::to(['/price-list/set-checked-products', 'id' => $priceList->id, 'productionType' => $productionType]),
        ]); ?>
        <?= Html::hiddenInput('checkedAll', (int)(count(json_decode($priceList->priceListCheckedProducts->checked_products)) == $dataProvider->totalCount), [
            'id' => 'checkedAll',
        ]); ?>
        <div class="table-container products-table clearfix product-price-list-table" style="">
            <?= GridView::widget([
                'id' => 'product-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'rowOptions' => [
                    'role' => 'row',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'cssClass' => 'product_checker',
                        'headerOptions' => [
                            'style' => 'width: 20px;',
                        ],
                        'checkboxOptions' => function (Product $data) use ($priceList) {
                            $value = false;
                            if (in_array($data->id, json_decode($priceList->priceListCheckedProducts->checked_products))) {
                                $value = true;
                            }
                            return ['checked' => $value];
                        }
                    ],
                    [
                        'attribute' => 'article',
                        'headerOptions' => [
                            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' : null,
                        ],
                        'contentOptions' => [
                            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' : null,
                        ],
                    ],
                    [
                        'attribute' => 'title',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (Product $data) use ($productionType) {
                            if (Yii::$app->user->can(frontend\rbac\permissions\Product::VIEW)) {
                                $content = Html::a(Html::encode($data->title), [
                                    'view',
                                    'productionType' => $productionType,
                                    'id' => $data->id,
                                ]);
                            } else {
                                $content = Html::encode($data->title);
                            }

                            return Html::tag('div', $content, ['class' => 'product-title-cell']);
                        },
                    ],
                    [
                        'attribute' => 'group_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Группа ' . ($productionType ? 'товара' : 'услуги'),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '14%',
                        ],
                        'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
                        'value' => 'group.title',
                    ],
                    [
                        'attribute' => 'available',
                        'label' => 'Доступно',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->available * 1;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'reserve',
                        'label' => 'Резерв',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            $tooltipId = 'product-reserve-tooltip-' . $data->id;
                            $content = Html::a($data->reserve * 1, null, [
                                'class' => 'product-reserve-value',
                                'data' => [
                                    'tooltip-content' => '#' . $tooltipId,
                                    'url' => Url::to(['/product/reserve-documents', 'id' => $data->id]),
                                ],
                            ]);
                            $content .= Html::tag('span', Html::tag('span', '', ['id' => $tooltipId]), [
                                'class' => 'hidden',
                            ]);

                            return $content;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'quantity',
                        'label' => 'Остаток',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->quantity * 1;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'product_unit_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Ед. измерения',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '5%',
                        ],
                        'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
                        'value' => 'productUnit.name',
                    ],
                    [
                        'label' => 'Картинка',
                        'headerOptions' => [
                            'width' => '30px',
                        ],
                        'format' => 'raw',
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        'value' => function (Product $data) {
                            $content = '';
                            if ($thumb = $data->getImageThumb(200, 300)) {
                                $tooltipId = 'tooltip-product-image-' . $data->id;
                                $content = Html::tag('span', '', [
                                    'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                                    'data-tooltip-content' => '#' . $tooltipId,
                                    'style' => 'cursor: pointer;',
                                ]);
                                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                $content .= Html::beginTag('div', ['id' => $tooltipId]);
                                $content .= Html::img($thumb, ['alt' => '']);
                                $content .= Html::endTag('div');
                                $content .= Html::endTag('div');
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'comment_photo',
                        'label' => 'Описание',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        'value' => function ($data) {
                            $content = '';
                            if ($data->comment_photo) {
                                $tooltipId = 'tooltip-product-comment-' . $data->id;
                                $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                                    'class' => 'product-comment-box',
                                    'data-tooltip-content' => '#' . $tooltipId,
                                    'style' => 'cursor: pointer;'
                                ]);
                                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                                $content .= Html::encode($data->comment_photo);
                                $content .= Html::endTag('div');
                                $content .= Html::endTag('div');
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'price_for_buy_with_nds',
                        'label' => 'Цена покупки',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return TextHelper::invoiceMoneyFormat($data->price_for_buy_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'price_for_buy_nds_id',
                        'label' => 'НДС покупки',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => 'priceForBuyNds.name',
                        'visible' => Yii::$app->user->identity->company->hasNds(),
                    ],
                    [
                        'attribute' => 'amountForBuy',
                        'label' => 'Стоимость покупки',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return TextHelper::invoiceMoneyFormat($data->amountForBuy, 2);
                        },
                        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                    ],
                    [
                        'attribute' => 'price_for_sell_with_nds',
                        'label' => 'Цена продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->not_for_sale ? 'Не для продажи' :
                                TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'price_for_sell_nds_id',
                        'label' => 'НДС продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->not_for_sale ? 'Не для продажи' : ($data->priceForSellNds ? $data->priceForSellNds->name : '---');
                        },
                        'visible' => Yii::$app->user->identity->company->hasNds(),
                    ],
                    [
                        'attribute' => 'amountForSell',
                        'label' => 'Стоимость продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return TextHelper::invoiceMoneyFormat($data->amountForSell, 2);
                        },
                        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                    ],
                ],
            ]); ?>
        </div>
        <?= Html::endForm(); ?>
    </div>
</div>
<style>
    .summary-info {
        font-weight: bold;
        width: calc(100% - 275px);
        border: 1px solid #4276a4;
        border-bottom-color: rgb(66, 118, 164);
        margin: 0 20px;
    }
    .buttons-scroll-fixed {
        position: fixed;
        bottom: 0px;
        z-index: 9999;
        background:#fff;
        margin-left: -20px;
    }
    @media (max-width: 992px) {
        .summary-info {  width: calc(100% - 40px);}
    }
    @media(max-width:767px){
        .summary-info {  width: calc(100% - 20px);}
    }
</style>
<div id="summary-container" class="buttons-scroll-fixed col-xs-12 col-sm-12 col-lg-12  pad0">
    <div class="summary-info">
        <div style="padding: 5px 10px;">
            <table class="pull-left">
                <tbody>
                <tr>
                    <td style="width: 80px; height: 34px; color: #4276a4;">
                        <?= Html::a('Сохранить', 'javascript:;', [
                            'class' => 'btn btn-sm darkblue text-white save-price-list-products',
                            'data-url' => Url::to(['/price-list/continue-create', 'id' => $priceList->id, 'productionType' => $productionType, 'store' => $store]),
                        ]); ?>
                    <td style="width: 80%;"></td>
                    <td style="width: 80px; height: 34px; color: #4276a4;">
                        <?= Html::a('Отменить', Url::to([
                            '/price-list/product-index',
                            'productionType' => $productionType,
                        ]), [
                            'class' => 'btn btn-sm darkblue text-white',
                        ]); ?>
                </tr>
                </tbody>
            </table>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(".preview-product-photo").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "side": "right",
        "contentAsHTML": true
    });
    $(".product-comment-box").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "contentAsHTML": true
    });
    $(document).ready(function () {
        $("#product-grid tbody tr").each(function() {
            var $row = $(this);
            var height = $(".product-title-cell", $row).height();
            $(".product-comment-box", $row).css("height", height + "px");
        });
    });
    $(document).on("click", ".product-reserve-value:not(.tooltipstered)", function() {
        var reserve = $(this);
        var tooltipId = reserve.data("tooltip-content")
        $.ajax({
            url: reserve.data("url"),
            success: function(data) {
                if (data.result) {
                    $(reserve.data("tooltip-content")).html(data.result);
                    reserve.tooltipster({
                        theme: "tooltipster-kub",
                        trigger: "click",
                        contentCloning: true
                    });
                    reserve.tooltipster("show");
                }
            }
        })
    });
    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = $(this).val();
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select").each(function () {
            var $attrName = $(this).data("id");
            var $value = 0;
            if ($attrName == "filterStatus") {
                $value = 2;
            }
            $filterData["ProductSearch[" + $attrName + "]"] = $value;
        });
        $(".filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
    }

    $(document).on("change", "#productsearch-filterdate", function (e) {
        changePeriod($(this).val(), $(this).data("url"));
    });

    function changePeriod(dateType, url) {
        var dateFrom = $("#productsearch-datefrom");
        var dateTo = $("#productsearch-dateto");
        var dateText = $(".block-date .date-text");
        var blockDate = $(".block-date");

        if (dateType == 0) {
            dateFrom.val("");
            dateTo.val("");
            dateText.text("");
            blockDate.hide();
        } else {
            $.post(url, {
                date_type: dateType,
                date_from: dateFrom.val(),
                date_to: dateTo.val()
            }, function (data) {
                dateFrom.val(data.dateFrom);
                dateTo.val(data.dateTo);
                dateText.text(data.dateText);
                blockDate.show();
            });
        }
    }
    var $checkedProductXhr = null;
    $(document).on("change", ".product-price-list-table .product_checker", function (e) {
        var $checkedProduct = $("#checkedProducts");
        var $checkedProductValue = JSON.parse($checkedProduct.val());
        var $exists = $checkedProductValue.indexOf($(this).val());

        if ($(this).is(":checked")) {
            if ($exists == -1) {
                $checkedProductValue.push($(this).val());
            }
        } else {
            if ($exists != -1) {
                $checkedProductValue.splice($exists, 1);
            }
        }
        $checkedProduct.val(JSON.stringify($checkedProductValue));
        if ($checkedProductXhr !== null) {
            $checkedProductXhr.abort();
        }
        $checkedProductXhr = $.post($checkedProduct.data("url"), {
            checkedProducts: JSON.stringify($checkedProductValue)
        }, function (data) {});
    });

    $(document).on("click", ".save-price-list-products", function (e) {
        $(this).removeClass("save-price-list-products");
        $.post($(this).data("url"), {
            products: $("#product_checker_form #checkedProducts").val()
        }, function (data) {});
    });

    $(document).ready(function (e) {
        var $checkedAll = $("#checkedAll");
        if ($checkedAll.val() == 1 && !$(".select-on-check-all").is(":checked")) {
            $(".select-on-check-all").click();
        }
    });

    $(".select-on-check-all").change(function () {
        $("#checkedAll").val($(this).is(":checked"))
    });
'); ?>
