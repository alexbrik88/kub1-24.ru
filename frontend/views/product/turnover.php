<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\models\product\ProductSearch;
use frontend\widgets\RangeButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */

//$this->title = Product::$productionTypes[$productionType].', оборот.';
$this->title = 'Оборот товаров';
$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Оборот товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Оборот услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
?>

<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE)) : ?>
            <a href="<?= Url::toRoute(['create', 'productionType' => $productionType]) ?>"
               class="btn yellow">
                <i class="fa fa-plus"></i>
                ДОБАВИТЬ
            </a>
        <?php endif; ?>
    </div>
    <h3 class="page-title">
        <?= Html::encode($this->title) . ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? ' в <i class="fa fa-rub"></i>' : null); ?>
    </h3>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="row">
            <div class="col-md-3 col-md-offset-9">
                <?= RangeButtonWidget::widget(['cssClass' => 'btn_select_days btn_no_right cash-btn-select-days',]); ?>
            </div>
        </div>
    </div>
</div>

<div class="row statistic-block" style="margin-top: 0; margin-bottom: 5px;">
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #dfba49; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                    <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceStart(), 2); ?>
                    <i class="fa fa-rub"></i>
                <?php else: ?>
                    <?= TextHelper::numberFormat($searchModel->getBalanceStart()) ?>
                <?php endif; ?>
            </div>
            <div class="stat-label">
                Остаток на начало периода
            </div>
            <div class="stat-footer" style="background-color: #d4af46;">
                <?= $searchModel->dateStart->format('d.m.Y') ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #45b6af; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                    <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceIn(), 2); ?>
                    <i class="fa fa-rub"></i>
                <?php else: ?>
                    <?= TextHelper::numberFormat($searchModel->getBalanceIn()) ?>
                <?php endif; ?>
            </div>
            <div class="stat-label">
                Закуплено всего
            </div>
            <div class="stat-footer" style="background-color: #43a9a2;">&nbsp;</div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #f3565d; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                    <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceOut(), 2); ?>
                    <i class="fa fa-rub"></i>
                <?php else: ?>
                    <?= TextHelper::numberFormat($searchModel->getBalanceOut()) ?>
                <?php endif; ?>
            </div>
            <div class="stat-label">
                Продано всего
            </div>
            <div class="stat-footer" style="background-color: #e25157;">&nbsp;</div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #dfba49; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?php if ($searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT): ?>
                    <?= TextHelper::invoiceMoneyFormat($searchModel->getBalanceEnd(), 2) ?>
                    <i class="fa fa-rub"></i>
                <?php else: ?>
                    <?= TextHelper::numberFormat($searchModel->getBalanceEnd()) ?>
                <?php endif; ?>
            </div>
            <div class="stat-label">
                Остаток на конец периода
            </div>
            <div class="stat-footer" style="background-color: #d4af46;">
                <?= $searchModel->dateEnd->format('d.m.Y') ?>
            </div>
        </div>
    </div>
</div>

<div class="portlet box darkblue">
    <?= Html::beginForm(['turnover', 'productionType' => $productionType,], 'GET'); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group">
                <div class="input-cont">
                    <?= Html::activeTextInput($searchModel, 'title', [
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <span class="input-group-btn">
                    <?= Html::submitButton('Найти', [
                        'class' => 'btn green-haze',
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>

    <div class="portlet-title">
        <div class="caption caption_for_input">
            Оборот товаров
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="table-container" style="">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,

                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                'showFooter' => false,
                'footerRowOptions' => ['class' => 'padding-left-10'],
                'columns' => [
                    [
                        'attribute' => 'title',
                        'class' => DataColumn::className(),
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'footer' => 'Итого единиц ',
                        'value' => function ($data) use ($searchModel) {
                            $title = Html::encode($data['title']);
                            $content = (Yii::$app->user->can(\frontend\rbac\permissions\Product::VIEW)) ? Html::a($title, [
                                'view',
                                'productionType' => $searchModel->production_type,
                                'id' => $data['id'],
                            ]) : $title;

                            return Html::tag('div', $content, ['class' => 'product-title-wide-cell']);
                        },
                    ],
                    [
                        'attribute' => 'group_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Группа товара',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '14%',
                        ],
                        'filter' => ArrayHelper::merge([null => 'Все'], $searchModel->getGroupFilter()),
                        'value' => function ($data) use ($searchModel) {
                            return $searchModel->getGroupFilter()[$data['group_id']] ?? '';
                        },
                        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                    ],
                    [
                        'attribute' => 'product_unit_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Ед.измерения',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '14%',
                        ],
                        'filter' => ArrayHelper::merge([null => 'Все'], $searchModel->getUnitFilter()),
                        'value' => function ($data) use ($searchModel) {
                            return $searchModel->getUnitFilter()[$data['product_unit_id']] ?? '';
                        },
                        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE || $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? false : true),
                    ],
                    [
                        'attribute' => 'balance_start',
                        'label' => 'Остаток на начало периода',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'value' => function ($model) use ($searchModel) {
                            $result = $model['balance_start'] * 1;

                            return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                        },
                    ],
                    [
                        'attribute' => 'balance_in',
                        'label' => 'Закуплено',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'value' => function ($model) use ($searchModel) {
                            $result = $model['balance_in'] * 1;

                            return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                        },
                    ],
                    [
                        'attribute' => 'balance_out',
                        'label' => 'Продано',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'value' => function ($model) use ($searchModel) {
                            $result = $model['balance_out'] * 1;

                            return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                        },
                    ],
                    [
                        'attribute' => 'balance_end',
                        'label' => 'Остаток на конец периода',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'value' => function ($model) use ($searchModel) {
                            $result = $model['balance_end'] * 1;

                            return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                        },
                    ],
                ],
            ]); ?>

        </div>
    </div>
</div>
