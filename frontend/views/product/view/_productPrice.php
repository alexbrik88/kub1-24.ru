<?php

use common\components\TextHelper;
use common\models\product\Product;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */
?>

<table class="table table-resume" style="width: auto; margin-left: -10px;">
    <colgroup>
        <col width="150" align="right">
        <col width="150" align="right">
    </colgroup>
    <tbody>
    <tr class="even">
        <td class="bold-text">Цена покупки:</td>
        <td><?= $canViewPriceForBuy ?
                TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                Product::DEFAULT_VALUE; ?></td>
    </tr>
    <?php if (Yii::$app->user->identity->company->hasNds()): ?>
        <tr class="odd">
            <td class="bold-text">НДС покупки:</td>
            <td><?= $model->priceForBuyNds ? $model->priceForBuyNds->name : '---'; ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
<table class="table table-resume" style="width: auto; margin-left: -10px;">
    <colgroup>
        <col width="150" align="right">
        <col width="150" align="right">
    </colgroup>
    <tbody>
    <?php if ($model->not_for_sale) : ?>
        <tr class="even">
            <td class="bold-text">Не для продажи</td>
        </tr>
    <?php else : ?>
        <tr class="even">
            <td class="bold-text">Цена продажи:</td>
            <td><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?></td>
        </tr>
        <?php if (Yii::$app->user->identity->company->hasNds()): ?>
            <tr class="odd">
                <td class="bold-text">НДС продажи:</td>
                <td><?= $model->priceForSellNds->name; ?></td>
            </tr>
        <?php endif; ?>
    <?php endif; ?>
    </tbody>
</table>