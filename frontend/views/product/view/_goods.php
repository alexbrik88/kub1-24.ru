<?php

use common\models\product\Product;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$canViewPriceForBuy = Yii::$app->user->can(\frontend\rbac\permissions\Product::PRICE_IN_VIEW, [
    'model' => $model,
]);
$isTurnover = preg_match('|/product/turnover|', Yii::$app->request->referrer);

$posibleTabs = [
    '_productPrice',
    '_productQuantity',
    '_productInfo',
    '_productComment',
    '_productTurnover',
];
$tabActive = Yii::$app->request->get('tab');
if (!$tabActive || !in_array($tabActive, $posibleTabs)) {
    $tabActive = '_productPrice';
}

?>

<div class="portlet-body goods-info">
    <div class="row">
        <div class="col-sm-4">
            <table class="table table-resume" style="width: auto; max-width: 500px; margin-left: -15px;">
                <colgroup>
                    <col width="180" align="right">
                </colgroup>
                <tbody>
                <tr class="even">
                    <td class="bold-text">Группа:</td>
                    <td><?= $model->group ? Html::encode($model->group->title) : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Код:</td>
                    <td><?= $model->code ? Html::encode($model->code) : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Артикул:</td>
                    <td><?= $model->article ? Html::encode($model->article) : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Штрих код:</td>
                    <td><?=  Html::encode($model->barcode ?: Product::DEFAULT_VALUE); ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Единица измерения:</td>
                    <td><?= $model->productUnit ? Html::encode($model->productUnit->name) : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Вес (кг):</td>
                    <td><?= $model->weight ? $model->weight : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Объем (м3):</td>
                    <td><?= $model->volume ? $model->volume : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-5" style="padding-top: 10px;">
                    <span style="display: block;font-weight: bold;padding-bottom: 10px;">Изображение</span>

                    <div class="product-photo-preview">
                        <?= ($src = $model->getImageThumb(200, 200)) ?
                            Html::img($src, ['alt' => '']) : null; ?>
                    </div>
                </div>
                <div class="col-sm-7" style="padding-top: 10px;">
                    <span style="display: block;font-weight: bold;padding-bottom: 10px;">Описание</span>
                    <?= Html::encode($model->comment_photo); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="profile-form-tabs" style="margin: 15px 0;">
        <?php Pjax::begin([
            'id' => 'product-view-tabs-pjax',
            'linkSelector' => '#product-view-tabs > li > a',
            'timeout' => 10000,
        ]); ?>

        <?= Tabs::widget([
            'id' => 'product-view-tabs',
            'options' => [
                'class' => 'nav-form-tabs',
                'style' => 'margin: 0 -5px;',
            ],
            'headerOptions' => [
                'style' => 'padding: 0 5px;',
            ],
            'linkOptions' => [
                'style' => 'padding: 10px 15px 11px;',
            ],
            'items' => [
                [
                    'label' => 'Цены',
                    'url' => Url::current(['tab' => null]),
                    'active' => $tabActive == '_productPrice',
                ],
                [
                    'label' => 'Остатки',
                    'url' => Url::current(['tab' => '_productQuantity']),
                    'active' => $tabActive == '_productQuantity',
                ],
                [
                    'label' => 'Доп. информация',
                    'url' => Url::current(['tab' => '_productInfo']),
                    'active' => $tabActive == '_productInfo',
                ],
                [
                    'label' => 'Комментарии',
                    'url' => Url::current(['tab' => '_productComment']),
                    'active' => $tabActive == '_productComment',
                ],
                [
                    'label' => 'Оборот',
                    'url' => Url::current(['tab' => '_productTurnover']),
                    'active' => $tabActive == '_productTurnover',
                ],
            ],
        ]); ?>

        <div class="portlet box darkblue">
            <div class="portlet-body">
                <?= $this->render($tabActive, [
                    'model' => $model,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]) ?>
            </div>
        </div>

        <?php Pjax::end(); ?>
    </div>
</div>
