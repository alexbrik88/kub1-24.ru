<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\product\Product;
use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

$this->title = $model->title;

$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';

$isMoveable = $model->production_type == Product::PRODUCTION_TYPE_GOODS && max(0, $model->quantity) > 0;
switch ($model->status) {
    case Product::DELETED:
        $statusText = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товар удалён' : 'Услуга удалена';
        break;

    case Product::ARCHIVE:
        $statusText = 'Находится в архиве';
        break;

    default:
        $statusText = '';
        break;
}

$backUrl = Url::previous('product_list');
?>

<style type="text/css">
    .product-view .table.table-resume td {
        padding: 10px 15px;
        text-align: left !important;
    }
</style>

<div class="product-view page-content-in">
    <?= Html::a('Назад к списку', $backUrl ?: [
        'index',
        'productionType' => $model->production_type,
    ], [
        'class' => 'back-to-customers',
    ]) ?>

    <?php if ($statusText) : ?>
        <h3 class="text-warning"><?= $statusText; ?></h3>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption" style="font-size: 23px;font-weight: bold;">
                        <?= $model->title; ?>
                    </div>

                    <div class="actions">
                        <a class="btn darkblue btn-sm info-button" data-toggle="modal" href="#basic"
                           style="width:33px; height: 27px;">
                            <i class="icon-info" style="color:white"></i>
                        </a>
                        <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                            <a href="<?= Url::toRoute(['product/update', 'productionType' => $model->production_type, 'id' => $model->id,]); ?>"
                               title="Редактировать" class="btn darkblue btn-sm">
                                <i class="icon-pencil"></i></a>
                        <?php endif; ?>
                        <?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS): ?>
                            <?= Html::a('Перемещение товара', ['move', 'id' => $model->id], [
                                'class' => 'btn yellow ajax-modal-btn' . ($isMoveable ? '' : ' disabled'),
                                'disabled' => !$isMoveable,
                                'data-pjax' => '0',
                                'data-title' => 'Перемещение товара',
                                'style' => 'vertical-align: top;',
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2" style="padding: 0;">
        </div>

        <div class="col-sm-12">
            <?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS): ?>
                <?= $this->render('view/_goods', [
                    'model' => $model,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]) ?>
            <?php elseif ($model->production_type == Product::PRODUCTION_TYPE_SERVICE): ?>
                <?= $this->render('view/_service', [
                    'model' => $model,
                    'canViewPriceForBuy' => $canViewPriceForBuy,
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row action-buttons buttons-fixed" id="buttons-fixed" style="text-align: center;">
    <?php if (in_array($model->status, [Product::ACTIVE, Product::ARCHIVE])
        && Yii::$app->user->can(frontend\rbac\permissions\Product::UPDATE)
    ) : ?>
        <?= \frontend\widgets\ConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => $model->status == Product::ACTIVE ? 'В архив' : 'Извлечь из архива',
                'class' => 'btn darkblue',
                'style' => 'min-width: 118px; padding-left: 15px; padding-right: 15px;'
            ],
            'confirmUrl' => Url::to(['archive', 'productionType' => $model->production_type, 'id' => $model->id]),
            'confirmParams' => [],
            'message' => $model->status == Product::ACTIVE ? 'Перенести в архив?' : 'Извлечь из архива?',
        ]);
        ?>
    <?php endif; ?>
    <?php if (!$model->is_deleted && Yii::$app->user->can(frontend\rbac\permissions\Product::DELETE)): ?>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="float: right;">
            <?= \frontend\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['delete', 'productionType' => $model->production_type, 'id' => $model->id]),
                'confirmParams' => [],
                'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
            ]);
            echo \frontend\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                    'title' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите удалить ' . ($model->production_type ? 'товар' : 'услугу') . '?',
            ]);
            ?>
        </div>
    <?php endif; ?>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="created-by">
                    создан
                    <span><?= date("d.m.Y", $model->created_at); ?></span>
                    <?php if ($model->creator) : ?>
                        <span><?= $model->creator->fio; ?></span>
                    <?php endif ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
