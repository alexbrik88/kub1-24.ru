<?php
use common\components\grid\DropDownSearchDataColumn;
use common\components\grid\GridView;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\models\Documents;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\product\Store;

$productIdArray = Yii::$app->request->post('product', []);
$storeItems = [];
?>

<?php Pjax::begin([
    'id' => 'pjax-product-grid',
    'linkSelector' => false,
    'formSelector' => '#products_in_order',
    'timeout' => 5000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<div class="modal-header" id="for-product-type">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if ($productType == 1 ) : ?>
        <?php $emptyText = 'Все товары уже добавлены'; ?>
        <h1>Выбрать товар из списка</h1>
    <?php else : ?>
        <?php $emptyText = 'Все услуги уже добавлены'; ?>
        <h1>Выбрать услугу из списка</h1>
    <?php endif; ?>
</div>
<div class="modal-body">

    <?= Html::beginForm(['/product/get-products'], 'get', [
        'id' => 'products_in_order',
        'class' => 'add-to-invoice',
        'data' => [
            'pjax' => true,
        ]
    ]); ?>

    <?= Html::hiddenInput('documentType', $documentType, [
        'id' => 'documentTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('productType', $productType, [
        'id' => 'productTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('searchTitle', $searchModel->title, [
        'id' => 'searchTitleHidden',
    ]); ?>

    <?=  Html::hiddenInput('store_id', $storeId, [
        'id' => 'storeIdHidden',
    ]); ?>

    <?php foreach ((array) $searchModel->exclude as $value) {
        echo Html::hiddenInput('exists[]', $value);
    } ?>

    <?php if ($productType == 1) {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;

        $storeSelectList = $user->getStores()
            ->orderBy(['is_main' => SORT_DESC])
            ->indexBy('id')
            ->all();

        $selectedStoreId = $storeId;

        if ($selectedStoreId && ($selectedStore = Store::findOne($selectedStoreId))) {
            $currentStoreId = $selectedStore->id;
            $currentStoreName = $selectedStore->name;
        } elseif ($storeSelectList) {
            $currentStore = reset($storeSelectList);
            $currentStoreId = $currentStore->id;
            $currentStoreName = $currentStore->name;
        } else {
            $currentStoreId = null;
            $currentStoreName = "НЕТ СКЛАДА";
        }

        foreach ($storeSelectList as $key => $store) {
            $storeItems[] = [
                'label' => $store->name,
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'store-opt ' . ($currentStoreId == $key ? 'active' : ''),
                    'data-id' => $store->id
                ],
            ];
        }
    } ?>

    <div class="form-body">
        <div class="portlet m-b">
            <div class="row search-form-default">
                <div class="col-md-6">
                    <?php if ($productType == 1 && $storeItems) : ?>
                    <div class="dropdown">
                        <button class="store-dropdown dropdown" type="button" data-toggle="dropdown">
                            <span class="store-name"><?= $currentStoreName ?></span><span class="caret"></span>
                        </button>
                            <?= Dropdown::widget([
                                'id' => 'store-select-list-dropdown',
                                'encodeLabels' => false,
                                'items' => $storeItems,
                            ]) ?>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <div class="input-cont">
                            <?= Html::textInput('title', $searchModel->title, [
                                'placeholder' => 'Поиск...',
                                'class' => 'form-control',
                                'id' => 'product-title-search',
                            ]) ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet m-b add-to-invoice-table" id="add-to-invoice-tbody">
            <div class="portlet-body accounts-list">
                <div class="table-container" style="">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                            'id' => 'datatable_ajax',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'rowOptions' => [
                            'role' => 'row',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right m-b-0',
                            ],
                        ],
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'layout' => "{items}\n{pager}",
                        'emptyText' => $searchModel->exclude ? $emptyText : 'Ничего не найдено.',
                        'columns' => [
                            [
                                'format' => 'raw',
                                'value' => function (Product $model) use ($productIdArray) {
                                    return Html::checkbox('in_order[]', in_array($model->id, $productIdArray), [
                                        'id' => 'product-' . $model->id,
                                        'value' => $model->id,
                                        'class' =>'product_selected',
                                    ]);
                                },
                                'headerOptions' => [
                                    'width' => '3%'
                                ],
                                'header' => Html::checkbox('in_order_all', false, [
                                    'id' => 'product_selected-all',
                                ]),
                            ],
                            [
                                'attribute' => 'title',
                                'label' => 'Продукция',
                                'headerOptions' => [
                                    'width' => '65%'
                                ],
                            ],
                            [
                                'attribute' => 'article',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                            ],
                            //Группа
                            [
                                'attribute' => 'group_id',
                                'label' => 'Группа',
                                'class' => DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'filter' => [null => 'Все'] + ArrayHelper::map(ProductGroup::getGroups(), 'id', 'title'),
                                'value' => 'group.title',
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS,
                            ],
                            //Количество на складе
                            [
                                'attribute' => 'quantity',
                                'label' => 'Количество на складе',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'value' => function ($data) {
                                    return $data->quantity * 1;
                                },
                                'visible' => $productType == Product::PRODUCTION_TYPE_GOODS &&
                                             $documentType == Documents::IO_TYPE_OUT,
                            ],
                            [
                                'attribute' => 'price_for_sell_with_nds',
                                'label' => 'Цена продажи',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds);
                                },
                                'visible' => $documentType == Documents::IO_TYPE_OUT,
                            ],
                            [
                                'attribute' => 'price_for_buy_with_nds',
                                'label' => 'Цена покупки',
                                'headerOptions' => [
                                    'width' => '30%'
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds);
                                },
                                'visible' => $documentType == Documents::IO_TYPE_IN,
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= Html::button('Добавить отмеченные', [
                    'class' => 'btn darkblue pull-right btn-w button-add-marked',
                    'data-dismiss' => 'modal',
                    'id' => 'add-to-invoice-button',
                    'style' => 'margin-left: 15px;'
                ]); ?>
                <?= Html::button('Добавить все', [
                    'class' => 'btn darkblue pull-right btn-w button-add-all',
                    'data-dismiss' => 'modal',
                    'id' => 'add-all-to-invoice-button',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).on("change", "#product_selected-all", function() {
    $("input.product_selected").prop("checked", $(this).prop("checked")).trigger("change").uniform("refresh");
});
</script>

<?php Pjax::end(); ?>