<?php
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\Contractor;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */

$options = [
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line-product',
        'style' => 'padding-left: 4px;',
    ],
];
?>
<style type="text/css">
    .field-product-provider_id .select2-selection__clear {
        position: absolute !important;
    }
</style>

<?= $form->field($model, 'box_type', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'count_in_place', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'place_count', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'mass_gross', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
    'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
]); ?>

<?= $form->field($model, 'country_origin_id', $options)
    ->dropDownList(ArrayHelper::map(Country::getCountries(), 'id', 'name_short')); ?>

<?= $form->field($model, 'customs_declaration_number', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'item_type_code', $options)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'provider_id', $options)->widget(Select2::classname(), [
    'data' => Contractor::getAllContractorListArray(Contractor::TYPE_SELLER),
    'options' => [
        'prompt' => '',
    ],
    'pluginOptions' => [
        'minimumResultsForSearch' => 10,
        'allowClear' => true,
    ],
]); ?>
