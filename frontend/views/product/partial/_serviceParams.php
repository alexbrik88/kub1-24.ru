<?php

use common\models\product\Product;
use common\models\product\ProductUnit;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\widgets\ProductGroupDropdownWidget;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$units = ProductUnit::findSorted()
    ->andWhere(['services' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}

$storeArray = $model->company->getStores()->orderBy(['is_main' => SORT_DESC])->all();

?>

<?= $form->field($model, 'title', [
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
        'style' => 'padding-left: 4px;',
    ],
])->textInput([
    'maxlength' => true,
]); ?>

<?= $form->field($model, 'group_id', [
    'options' => [
        'class' => 'form-group'
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
        'style' => 'padding-left: 4px;',
    ],
])->widget(ProductGroupDropdownWidget::classname(), [
    'productionType' => Product::PRODUCTION_TYPE_SERVICE,
])->label('Группа услуги'); ?>

<?= $this->render('_productPrice', [
    'form' => $form,
    'model' => $model,
    'thinFieldOptions' => $thinFieldOptions,
    'canViewPriceForBuy' => $canViewPriceForBuy,
]) ?>

<?= $form->field($model, 'product_unit_id', [
    'options' => [
        'class' => 'form-group required'
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
        'style' => 'padding-left: 4px;',
    ],
])->widget(Select2::classname(), [
    'data' => ArrayHelper::map($units, 'id', 'name'),
    'options' => [
        'prompt' => '',
        'options' => $unitsOptions,
    ]
]); ?>
