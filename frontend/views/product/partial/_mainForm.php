<?php

use common\models\product\Product;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $model common\models\product\Product */
/* @var $isService bool */
/* @var $isGoods bool */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $canViewPriceForBuy boolean */

$thinFieldOptions = [
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
    ],
];

$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;
$isGoods = $model->production_type == Product::PRODUCTION_TYPE_GOODS;
$company = Yii::$app->user->identity->company;
$company_id = $company->id;

$this->registerJs('
$(document).on("change", "#product-not_for_sale", function() {
    $("input.for_sale_input").prop("disabled", this.checked);
    $("input.for_sale_input:radio:not(.md-radiobtn)").uniform("refresh");
    if (this.checked) {
        $("#product-price_for_sell_with_nds").val("");
        $(".field-product-price_for_sell_with_nds").removeClass("has-error");
        $(".field-product-price_for_sell_with_nds .help-block-error").html("");
    }
});
');
?>

<?= Html::activeHiddenInput($model, 'production_type', [
    'id' => 'production_type_input',
]); ?>

<?= $form->errorSummary($model); ?>

<?php if ($isGoods) : ?>
    <?= $this->render('_productParams', [
        'model' => $model,
        'form' => $form,
        'thinFieldOptions' => $thinFieldOptions,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]); ?>
<?php else : ?>
    <?= $this->render('_serviceParams', [
        'model' => $model,
        'form' => $form,
        'thinFieldOptions' => $thinFieldOptions,
        'canViewPriceForBuy' => $canViewPriceForBuy,
    ]); ?>
<?php endif ?>