<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.10.2018
 * Time: 0:27
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;
use common\models\product\PriceListIncludeType;
use common\components\helpers\Html;
use common\models\product\Product;
use common\models\product\PriceList;
use common\models\company\CompanyType;
use common\models\Company;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $priceList PriceList
 * @var $productionType integer
 * @var $store
 * @var $company Company
 */

$isFilledCompany  = ($company->strict_mode != Company::ON_STRICT_MODE);
$isFilledProducts = ($productsCount + $servicesCount > 0);

if (!$isFilledCompany || !$isFilledProducts) {
    echo yii2tooltipster::widget([
        'options' => [
            'class' => '.tooltip2',
        ],
        'clientOptions' => [
            'theme' => ['tooltipster-kub'],
            'trigger' => 'hover',
            'contentAsHTML' => true,
        ],
    ]);
}
?>
<?php $form = ActiveForm::begin([
    'id' => 'create-price-list-form',
    'action' => Url::to(['/price-list/create', 'productionType' => $productionType, 'store' => $store]),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnBlur' => false,
    'validateOnType' => false,
    'validateOnChange' => false,
    'validateOnSubmit' => true,
]); ?>
<div class="main-block">
    <div class="header">
        <table style="width: 100%; height: ">
            <tr>
                <td style="width: 10%; vertical-align: middle; text-align: center;">
                    <?= Html::tag('div', '<img src="/img/icons/price-list.png" style="width: 30px; height: 30px;">', [
                        'style' => 'display: inline-block;padding: 10px;background-color: #4276a4;border-radius: 30px!important;width: 60px;height: 60px;',
                    ]) ?>
                </td>
                <td style="padding-left: 10px; font-size: 18px;">
                    Создать прайс-лист
                </td>
            </tr>
        </table>
    </div>
    <?php if (!$isFilledCompany) : ?>
        <div class="block-pricelist-new-company">
            <p style="font-weight:bold">
                1. Внесите данные по вашему <?= ($company->company_type_id == CompanyType::TYPE_IP) ? 'ИП' : 'ООО' ?>
            </p>
            <p><?= Html::a('Заполнить реквизиты по ИНН', 'javascript:;', ['class' => 'first-invoice-company']); ?></p>
        </div>
    <?php endif; ?>
    <?php if (!$isFilledProducts): ?>
        <div class="block-pricelist-add-products">
            <p style="font-weight:bold">
                2. Добавьте товар
            </p>
            <p>Если у вас много товара, то <?= Html::a('Загрузите из Excel', 'javascript:;', ['class' => 'show-import-xls-modal']); ?>
            или <?= Html::a('загрузите вручную', Url::toRoute(['create', 'productionType' => $productionType])); ?></p>
        </div>
    <?php endif; ?>
    <?php if (!$isFilledCompany || !$isFilledProducts) : ?>
        <div class="block-price-options">
            <p style="font-weight:bold">
                3. <?= Html::a('Настроить прайс-лист', 'javascript:;', ['class' => 'toggle-block-price-list']); ?>
            </p>
        </div>
    <?php endif; ?>
    <div class="block-price-list" style="display: <?= ($isFilledCompany && $isFilledProducts) ? 'block' : 'none' ?>;">
        <div class="block-price-list-name">
            <div class="price-list-label">
                Название:
            </div>
            <div class="form-group form-md-line-input form-md-floating-label field-price-list-name">
                <div class="out">
                            <span class="name">
                                <?= $priceList->name; ?>
                            </span>
                    <i class="glyphicon glyphicon-pencil update-price-list-name"></i>
                </div>
                <div class="in" style="display: none;">
                    <?= $form->field($priceList, 'name')->label(false); ?>
                    <i class="fa fa-floppy-o fa-2x save-price-list-name"></i>
                    <i class="fa fa-reply fa-2x undo-price-list-name"></i>
                </div>
            </div>
        </div>
        <div class="block-price-list-include_type_id">
            <div class="price-list-label">
                Включить в прайс-лист:
            </div>
            <?= $form->field($priceList, 'include_type_id')
                ->dropDownList($productionType ? PriceList::$productIncludeTypes : PriceList::$serviceIncludeTypes)
                ->label(false); ?>
        </div>
        <div class="block-price-list-include_columns">
            <div class="price-list-label">
                Столбцы в прайс-листе:
            </div>
            <div class="include-columns_list form-group">
                <?= Html::activeCheckbox($priceList, 'include_name_column', [
                    'label' => 'Наименование',
                ]); ?>
                <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                    <?= Html::activeCheckbox($priceList, 'include_article_column', [
                        'label' => 'Артикул',
                    ]); ?>
                <?php endif; ?>
                <?= Html::activeCheckbox($priceList, 'include_product_group_column', [
                    'label' => 'Группа ' . ($productionType == Product::PRODUCTION_TYPE_GOODS ? 'товаров' : 'услуг'),
                ]); ?>
                <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                    <?= Html::activeCheckbox($priceList, 'include_reminder_column', [
                        'label' => 'Остаток',
                    ]); ?>
                <?php endif; ?>
                <?= Html::activeCheckbox($priceList, 'include_product_unit_column', [
                    'label' => 'Ед. измерения',
                ]); ?>
                <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                    <?= Html::activeCheckbox($priceList, 'include_description_column', [
                        'label' => 'Описание',
                    ]); ?>
                <?php endif; ?>
                <?= Html::activeCheckbox($priceList, 'include_price_column', [
                    'label' => 'Цена (продажи)',
                ]); ?>
            </div>
        </div>
        <div class="block-price-list-sort_type">
        <div class="price-list-label">
            Сортировка по столбцу:
        </div>
        <?= $form->field($priceList, 'sort_type')
            ->radioList([
                PriceList::SORT_BY_NAME => 'Наименование',
                PriceList::SORT_BY_GROUP => 'Группа ' . ($productionType == Product::PRODUCTION_TYPE_GOODS ? 'товаров' : 'услуг'),
            ])->label(false); ?>
    </div>
    </div>
    <?php if (!$isFilledCompany || !$isFilledProducts) : ?>
        <p style="font-weight:bold">
            4.	Теперь вы можете разместить онлайн  прайс лист у себя на сайте или где угодно или отправить его по e-mail.
        </p>
    <?php endif; ?>

    <div class="form-actions">
        <div class="row action-buttons">
            <div class="col-sm-5 col-xs-5" style="width: 11%;">
                <?= Html::button('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue text-white mt-ladda-btn ladda-button widthe-100 submit tooltip2',
                    'data-tooltip-content' => '#tooltip_submit',
                    'data-style' => 'expand-right',
                    'disabled' => (!$isFilledCompany || !$isFilledProducts)
                ]); ?>
            </div>
            <div class="col-sm-2 col-xs-2" style="width: 15.3%;"></div>
            <div class="col-sm-5 col-xs-5" style="width: 9.9%;">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue text-white pull-right widthe-100 side-panel-close-button',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<div style="display:none!important">
    <div id="tooltip_submit">Необходимо заполнить ваши реквизиты и добавить товар</div>
</div>

<?php $this->registerJs('
$(document).on("click", ".update-price-list-name", function (e) {
    $(this).closest(".out").hide();
    $(this).closest(".out").siblings(".in").show();
    $(this).closest(".block-price-list-name").find(".price-list-label").css("vertical-align", "baseline");
});

$(document).on("click", ".undo-price-list-name", function (e) {
    $(this).closest(".in").hide();
    $(this).closest(".in").siblings(".out").show();
    $(this).closest(".block-price-list-name").find(".price-list-label").css("vertical-align", "top");
    $(this).siblings(".field-pricelist-name").find("input").val($(this).closest(".in").siblings(".out").find(".name").text().trim());
});

$(document).on("click", ".save-price-list-name", function (e) {
    var $input = $(this).siblings(".field-pricelist-name").find("input");
    var $value = $input.val();
    var $nameText = $(this).closest(".in").siblings(".out").find(".name");
    
    if ($value == "") {
        $value = $nameText.text().trim();
        $input.val($value);
    }
    $nameText.text($value);
    $(this).closest(".in").hide();
    $(this).closest(".in").siblings(".out").show();
    $(this).closest(".block-price-list-name").find(".price-list-label").css("vertical-align", "top");
});

$(document).on("click", ".price-list-panel .side-panel-close-button", function (e) {
    if ($(this).hasClass("to-view")) {
        $(this).closest(".create-price-list").fadeOut();
        $(this).closest(".create-price-list").siblings(".view-price-list").fadeIn();
    } else {
        hidePanel();
    }
});

$(document).on("click", ".show-import-xls-modal", function(e) {
    e.preventDefault();
    $("#import-xls").modal();
});
$(document).on("click", ".toggle-block-price-list", function(e) {
    e.preventDefault();
    if ($(".block-price-list").is(":visible"))
        $(".block-price-list").fadeOut();
    else
        $(".block-price-list").fadeIn();
});


'); ?>