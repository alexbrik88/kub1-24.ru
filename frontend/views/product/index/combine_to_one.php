<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\widgets\ProductGroupDropdownWidget;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$articleCss = $userConfig->product_article ? '' : ' hidden';

$JS = <<<JS
$(document).on('click', 'button.combine_to_one_apply:not(.disabled)', function() {
    if ($(this).data('units') == '1') {
        $.ajax({
            type: 'post',
            url: $(this).data('url'),
            data: {
                target_id: $('input[name=target]:checked').val(),
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    } else {
        $('.same-units-hint').css('color', 'red');
    }
})
$(document).on('show.bs.modal', '#combine_to_one_modal', function() {
    if ($("input.product_checker:checkbox:checked").length > 1) {
        var unitArray = [];
        $('.quantity_correct', this).removeClass('hidden');
        $("input.product_checker:checkbox:checked").each(function(i, el) {
            var prod = $('#product-grid tr[data-key='+el.value+']');
            var target = $('tr.template').clone().removeClass('hidden template');
            var unit = prod.find('td.col_product_unit').html();
            unitArray[unit] = unit;
            target.find('td.control').html('<input type="radio" name="target" value="'+el.value+'">');
            target.find('td.col_product_article').html(prod.find('.col_product_article').html());
            target.find('td.col_product_name').html(prod.find('td.col_product_name a').html());
            target.find('td.col_product_group').html(prod.find('td.col_product_group').html());
            target.find('td.col_product_unit').html(unit);
            target.appendTo('tbody.combine_product_items', this);
            $('button.combine_to_one_apply').data('units', unitArray.length);
        });
        $('input:radio', this).uniform();
    } else {
        $('.quantity_fail', this).removeClass('hidden');
    }
});
$(document).on('hidden.bs.modal', '#combine_to_one_modal', function() {
    $('.quantity_correct, .quantity_fail', this).addClass('hidden');
    $('tbody.combine_product_items', this).html('');
    $('button.combine_to_one_apply').addClass('disabled');
    $('.same-units-hint').css('color', '#333');
});
$(document).on('click', '.combine_target_row', function() {
    $('input:radio', this).prop('checked', true);
    $('.combine_target_row input:radio').uniform('refresh');
    $('button.combine_to_one_apply').removeClass('disabled');
});
JS;
?>


<?php Modal::begin([
    'header' => '<h1>Объединить в один товар</h1>',
    'id' => 'combine_to_one_modal',
    'options' => [
        //'style' => 'width: 470px;',
    ]
]); ?>
<div class="move_to_group">
    <div class="quantity_fail hidden">
        Для объединения товаров в один, необходимо выбрать больше одного товара.
    </div>
    <div class="quantity_correct hidden">
        <div style="margin-bottom: 10px; font-size: 14px;">
            Выберите товар, к которому будут объеденены другие товары.
            <div class="same-units-hint" style="font-size: 12px;">
                Объединены будут товары с одинаковой единицей измерения.
            </div>
        </div>
        <table class="combine_product_table" style="width: 100%;">
            <thead>
                <tr class="">
                    <th style="padding: 2px 5px; font-weight: bold;"></th>
                    <th style="padding: 2px 5px; font-weight: bold;" class="col_product_article<?= $articleCss ?>">Артикул</th>
                    <th style="padding: 2px 5px; font-weight: bold;">Наименование</th>
                    <th style="padding: 2px 5px; font-weight: bold;">Группа</th>
                    <th style="padding: 2px 5px; font-weight: bold;">Ед.изм.</th>
                </tr>
            </thead>
            <tbody class="combine_product_items">
            </tbody>
            <tfoot class="hidden">
                <tr class="template combine_target_row" style="cursor: pointer; border-top: 1px solid #ccc;">
                    <td style="padding: 0;" class="control"></td>
                    <td style="padding: 2px 5px;" class="col_product_article<?= $userConfig->product_article ? '' : ' hidden'; ?>"></td>
                    <td style="padding: 2px 5px;" class="col_product_name"></td>
                    <td style="padding: 2px 5px;" class="col_product_group"></td>
                    <td style="padding: 2px 5px;" class="col_product_unit"></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <div style="margin-top: 30px;">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white combine_to_one_apply quantity_correct hidden disabled',
            'style' => 'width: 110px;',
            'data' => [
                'units' => '1',
                'url' => Url::to(['/product/combine']),
            ],
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'style' => 'width: 110px;',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>
<?php Modal::end(); ?>
<?php $this->registerJs($JS); ?>