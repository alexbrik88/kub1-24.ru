<?php

use common\models\TaxRate;
use frontend\models\ProductNdsForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $toggleButton array */

$model = new ProductNdsForm;
$ndsItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');
$idsName = Html::getInputName($model, 'ids');

$js = <<<JS
var checkOldNdsValueStatus = function() {
    $('#productndsform-from input[type=radio]').prop({
        disabled: $('#productndsform-type input[type=checkbox]:checked').length == 0
    }).uniform('refresh');
}
$(document).on('hidden.bs.modal', '#product-nds-modal', function (e) {
    $('.has-error', this).removeClass('has-error');
    $('.has-success', this).removeClass('has-success');
    $('.help-block', this).html('');
    $('input', this).prop('checked', false).uniform('refresh');
});
$(document).on('change', '.product_checker', function (e) {
    var productId = this.value;
    console.log(productId);
    if (this.checked) {
        $('<input>').attr({
            type: 'hidden',
            id: 'product-nds-selected-' + productId,
            name: '{$idsName}[]',
            value: productId
        }).appendTo('#product-nds-selected');
    } else {
        $('#product-nds-selected-' + productId).remove();
    }
    if ($('.product_checker:checked').length) {
        $('#productndsform-change').prop({checked: true}).uniform('refresh');
        $('#productndsform-type input[type=checkbox]').prop({checked: false}).uniform('refresh');
    } else {
        $('#productndsform-change').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
});
$(document).on('change', '#productndsform-change', function (e) {
    if (this.checked) {
        $('#productndsform-type input[type=checkbox]').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
});
$(document).on('change', '#productndsform-type input[type=checkbox]', function (e) {
    if ($('#productndsform-type input[type=checkbox]:checked').length) {
        $('#productndsform-change').prop({checked: false}).uniform('refresh');
    }
    checkOldNdsValueStatus();
});
checkOldNdsValueStatus();
JS;

$this->registerJs($js);
?>

<style type="text/css">
    #product-nds-form label {
        margin-right: 20px;
    }
</style>
<?php Modal::begin([
    'id' => 'product-nds-modal',
    'header' => Html::tag('h1', 'Массовое изменение НДС'),
    'toggleButton' => $toggleButton
]); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'product-nds-form',
        'action' => ['/product/change-nds'],
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
    ]); ?>

        <div style="display: flex;">
            <?= $form->field($model, 'type')->checkboxList(ProductNdsForm::$typeItems, []) ?>
            <?= $form->field($model, 'change', [
                'options' => ['style' => 'margin-top: 26px;']
            ])->checkbox(['disabled' => true]) ?>
        </div>
        <div id="product-nds-selected"></div>

        <?= $form->field($model, 'price')->checkboxList(ProductNdsForm::$priceItems, []) ?>

        <?= $form->field($model, 'from')->radioList($ndsItems, []); ?>

        <?= $form->field($model, 'to')->radioList($ndsItems, []); ?>

        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]); ?>
        <?= Html::submitButton('Изменить', [
            'class' => 'btn darkblue text-white',
        ]); ?>

    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
