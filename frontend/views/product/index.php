<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\components\XlsHelper;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use frontend\rbac\permissions;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\document\status\OrderDocumentStatus;
use common\models\Company;
use common\components\ImageHelper;
use common\models\product\ProductSearch;
use frontend\widgets\StoreFilterWidget;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\models\product\PriceList;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */
/* @var $defaultSortingAttr string */
/* @var $priceList PriceList */
/* @var $store */
/* @var $user \common\models\employee\Employee */

$this->title = Product::$productionTypes[$productionType];

$userConfig = Yii::$app->user->identity->config;

$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();
$xlsMessageType = $productionType ? 'товаров' : 'услуг';

if ($productionType == 1) {
    $emptyMessage = 'Вы еще не добавили ни одного товара';
} elseif ($productionType == 0) {
    $emptyMessage = 'Вы еще не добавили ни одной услуги';
} else {
    $emptyMessage = 'Ничего не найдено';
}

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);
$canDocumentIndexIn = Yii::$app->user->can(permissions\document\Document::INDEX, [
    'ioType' => Documents::IO_TYPE_IN,
]);

$actionItems = [];
if ($canUpdate) {
    if (count($storeList) > 1 && $productionType == Product::PRODUCTION_TYPE_GOODS) {
        $actionItems[] = [
            'label' => 'Переместить на склад',
            'url' => '#move_to_store_modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    $actionItems[] = [
        'label' => 'Переместить в группу',
        'url' => '#move_to_group_modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    $actionItems[] = [
        'label' => 'Изменить цену',
        'url' => '#many-price-modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
        $actionItems[] = [
            'label' => 'Объединить товары',
            'url' => '#combine_to_one_modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    if ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE) {
        $actionItems[] = [
            'label' => 'Переместить в архив',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-archive',
                'data' => [
                    'url' => '/product/to-archive',
                ],
            ],
        ];
    } else {
        $actionItems[] = [
            'label' => 'Извлечь из архива',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-store-from-archive',
                'data' => [
                    'url' => '/product/to-store-from-archive',
                ],
            ],
        ];
    }
}
$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';
}
$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$hasFilters = !($searchModel->filterStatus == ProductSearch::IN_WORK) ||
    (bool)$searchModel->filterComment ||
    (bool)$searchModel->filterImage ||
    (bool)$searchModel->filterDate;

if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $tableConfigItems = [
        [
            'attribute' => 'product_article',
        ],
        [
            'attribute' => 'product_group',
            'label' => 'Группа товара',
        ],
        [
            'attribute' => 'product_image',
        ],
        [
            'attribute' => 'product_comment',
        ],
    ];
    if ($canDocumentIndexIn) {
        $tableConfigItems[] = [
            'attribute' => 'product_purchase_price',
        ];
        $tableConfigItems[] = [
            'attribute' => 'product_purchase_amount',
        ];
    }
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа товара',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
} else {
    $tableConfigItems = [
        [
            'attribute' => 'product_group',
            'label' => 'Группа услуги',
        ],
    ];
    if ($canDocumentIndexIn) {
        $tableConfigItems[] = [
            'attribute' => 'product_purchase_price',
        ];
    }
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа услуги',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
}

$this->registerJs('
    $(document).on("click", "input.select-on-check-all", function(e) {
        $("input.product_checker").prop("checked", this.checked).uniform("refresh");
        $("input.select-on-check-all").prop("checked", this.checked).uniform("refresh");
    });
    $(document).on("change", "input.product_checker", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#mass_actions").fadeIn();
            if ($("input.product_checker:not(:checked)").length) {
                $(".select-on-check-all").prop("checked", false).uniform("refresh");
            } else {
                $(".select-on-check-all").prop("checked", true).uniform("refresh");
            }
        } else {
            $("#mass_actions").fadeOut();
            $(".select-on-check-all").prop("checked", false).uniform("refresh");
        }
    });
    $(document).on("click", "#confirm-many-delete", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
    $(".tooltip-help").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "side": "right",
        "contentAsHTML": true,
    });
');
$help = $productionType == Product::PRODUCTION_TYPE_GOODS ? Html::tag('span', '', [
    'class' => 'tooltip-help ico-question',
    'data-tooltip-content' => '#tooltip_store_help',
    'style' => 'margin: 0 0 0 15px;; cursor: pointer;'
]) : null;

$priceListsExists = $company->getPriceLists($productionType)
    ->andWhere(['is_deleted' => false])
    ->orderBy(['created_at' => SORT_DESC])
    ->exists();

$taxationType = $company->companyTaxationType;
?>

<div class="portlet box">
    <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
        <div class="btn-group pull-right title-buttons" style="margin-left: 15px;">
            <a href="<?= Url::toRoute(['create', 'productionType' => $productionType]) ?>"
               class="btn yellow">
                <i class="fa fa-plus"></i>
                ДОБАВИТЬ
            </a>
        </div>
    <?php endif; ?>
    <h3 class="page-title">
        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
            <?= StoreFilterWidget::widget([
                'pageTitle' => $this->title,
                'store' => $store,
                'productionType' => $productionType,
                'help' => $help,
                'company' => $company,
            ]); ?>
        <?php else: ?>
            <?= Html::encode($this->title); ?>
        <?php endif; ?>
        <span style="display: none;">
            <span id="tooltip_store_help">
                Можно добавить несколько складов,
                <br>
                для этого нужно войти в
                <?= Html::a('профиль<br>компании', [
                    '/company/update',
                    'backUrl' => Url::current(),
                    '#' => 'add_tabs-tab2',
                ]) ?>
                и там добавить новый склад
            </span>
        </span>
    </h3>
</div>
<div class="row" style="margin-bottom: 20px;">
    <div class="<?= $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'col-md-7 col-sm-7' : 'col-md-9 col-sm-9'; ?> p-r-0">
        <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)
            && Yii::$app->user->can(permissions\document\Invoice::CREATE)
        ) : ?>
            <?= Html::a('СОЗДАТЬ СЧЁТ', null, [
                'class' => 'btn yellow add-to-invoice',
                'data-action' => Url::to(['documents/invoice/create', 'type' => 2]),
                'data-tooltip-content' => '#add-to-invoice-tooltip',
                'disabled' => true,
            ]) ?>
            <div style="display: none;">
                    <span id="add-to-invoice-tooltip">
                        Выберите
                        <?= $productionType ? 'товар' : 'услугу'; ?>
                        для создания счета
                    </span>
            </div>
            <?php if (YII_ENV_DEV || in_array($company->id, [2031, 11270])): ?>
                <?= Html::a('СОЗДАТЬ ЗАКАЗ', null, [
                    'class' => 'btn yellow add-to-order',
                    'data-action' => Url::to(['documents/order-document/create',]),
                    'data-tooltip-content' => '#add-to-order-tooltip',
                    'disabled' => true,
                    'style' => 'margin: 0 15px;',
                ]); ?>
                <div style="display: none;">
                    <span id="add-to-order-tooltip">
                        Выберите
                        <?= $productionType ? 'товар' : 'услугу'; ?>
                        для создания заказа
                    </span>
                </div>
            <?php endif; ?>
            <?= Html::a('СОЗДАТЬ ПОСТУПЛЕНИЕ', null, [
                'class' => 'btn yellow add-to-invoice',
                'data-action' => Url::to(['documents/invoice/create', 'type' => 1]),
                'data-tooltip-content' => '#add-to-invoice-tooltip',
                'disabled' => true,
            ]) ?>
            <?php $this->registerJs('
                    $(".add-to-invoice, .add-to-order").tooltipster({
                        "theme": ["tooltipster-kub"],
                        "trigger": "hover",
                        "contentAsHTML": true
                    });
                    $(document).on("change", "input.product_checker", function(e) {
                        if ($("input.product_checker:checked").length) {
                            $(".add-to-invoice, .add-to-order").attr("disabled", false);
                            $(".add-to-invoice, .add-to-order").tooltipster("disable");
                        } else {
                            $(".add-to-invoice, .add-to-order").attr("disabled", true);
                            $(".add-to-invoice, .add-to-order").tooltipster("enable");
                        }
                    });
                    $(document).on("click", ".add-to-invoice, .add-to-order", function(e) {
                        e.preventDefault();
                        if ($("input.product_checker:checked").length) {
                            $("#product_checker_form").attr("action", this.dataset.action).submit();
                        }
                    });
                '); ?>
        <?php endif ?>
    </div>
    <?php if ($productionType == Product::PRODUCTION_TYPE_SERVICE): ?>
        <div class="col-md-2 col-sm-2 p-r-0">
            <div class="btn-group pull-right title-buttons" style="width: 100%;">
                <?= Html::a('Прайс-лист', 'javascript:;', [
                    'class' => 'btn yellow price-list-trigger pull-right',
                ]); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-3 col-sm-3">
        <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
            <?= Html::button('<i class="fa fa-download"></i> Загрузить из Excel', [
                'class' => 'btn yellow w100proc no-padding pull-right',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#import-xls',
                ],
            ]); ?>
        <?php endif; ?>
    </div>
</div>
<?php if ($dataProvider->totalCount <= 10000 && $productionType == Product::PRODUCTION_TYPE_GOODS) : ?>
    <div class="row statistic-block">
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _yellow" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number fontSizeSmall">
                            <span class="details-sum">
                                <?= TextHelper::invoiceMoneyFormat($searchModel->getAllAmountBuy(), 2); ?>
                            </span>
                        <i class="fa fa-rub"></i>
                    </div>
                    <div class="desc">
                        Стоимость склада<br>
                        в ЗАКУПКЕ
                    </div>
                </div>
                <div class="more">
                    Количество штук: <?= TextHelper::numberFormat($searchModel->getAllQuantity()); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _green" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number fontSizeSmall">
                            <span class="details-sum">
                                <?= TextHelper::invoiceMoneyFormat($searchModel->getAllAmountSell(), 2); ?>
                            </span>
                        <i class="fa fa-rub"></i>
                    </div>
                    <div class="desc">
                        Стоимость склада<br>
                        при ПРОДАЖЕ
                    </div>
                </div>
                <div class="more">
                    Количество штук: <?= TextHelper::numberFormat($searchModel->getAllQuantity()); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat _grey" style="position: relative;">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number fontSizeSmall">
                            <span class="details-sum">
                                <?= TextHelper::invoiceMoneyFormat($searchModel->getAllReserveAmount(), 2); ?>
                            </span>
                        <i class="fa fa-rub"></i>
                    </div>
                    <div class="desc">
                        Зарезервированно<br>
                        покупателями на СУММУ
                    </div>
                </div>
                <div class="more">
                    Количество штук: <?= TextHelper::numberFormat($searchModel->getAllReserve()); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3">
            <div class="btn-group pull-right title-buttons" style="width: 100%;margin-top: 39px;">
                <?= Html::a('Прайс-лист', 'javascript:;', [
                    'class' => 'btn yellow price-list-trigger pull-right',
                ]); ?>
            </div>
            <div class="btn-group pull-right title-buttons" style="width: 100%;margin-top: 15px;">
                <a href="<?= Url::to(['/contractor/sale-increase',]); ?>" class="btn yellow" style="width: 100%;">
                    Увеличение онлайн продаж
                </a>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="row">
    <div class="col-sm-12 table-icons" style="margin-top: -12px;">
        <?= TableConfigWidget::widget([
            'items' => $tableConfigItems,
            'sortingItems' => $sortingItems,
        ]); ?>
        <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
            <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link pull-right',
                'title' => 'Скачать в Excel',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption caption_for_input">
            <?= $tableHeader[$productionType] . ': ' . $dataProvider->totalCount; ?>
        </div>
        <div class="tools search-tools tools_button col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'action' => Url::current([$searchModel->formName() => null]),
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                        <?= $form->field($searchModel, 'title')->textInput([
                            'placeholder' => 'Поиск...',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div id="mass_actions" class="actions joint-operations col-sm-5"
             style="float:left;display: none;width: 25%;">
            <?php if ($canUpdate) : ?>
                <?= $this->render('partial/_many_price', [
                    'company' => $company,
                    'productionType' => $productionType,
                ]) ?>
            <?php endif ?>
            <?php if ($actionItems) : ?>
                <div class="pull-left" style="position: relative; display: inline-block;">
                    <div class="dropdown">
                        <?= Html::a('Действия  <span class="caret"></span>', null, [
                            'class' => 'btn btn-default btn-sm dropdown-toggle',
                            'data-toggle' => 'dropdown',
                            'style' => 'height: 28px; color: #def1ff;',
                        ]); ?>
                        <?= Dropdown::widget([
                            'items' => $actionItems,
                            'options' => [
                                'style' => 'right: -30px !important; left: auto; top: 25px;',
                            ],
                        ]); ?>
                    </div>
                </div>
            <?php endif ?>
            <?php if ($canDelete) : ?>
                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                    'class' => 'btn btn-default btn-sm pull-left',
                    'data-toggle' => 'modal',
                ]); ?>
                <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                     tabindex="-1" aria-hidden="true"
                     style="display: none; margin-top: -51.5px;">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        Вы уверены, что хотите удалить выбранные
                                        <?= $productionType ? 'товары' : 'услуги'; ?>?
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-6">
                                        <?= Html::tag('button', '<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', [
                                            'id' => 'confirm-many-delete',
                                            'class' => 'btn darkblue pull-right ladda-button',
                                            'data-action' => Url::to(['many-delete', 'productionType' => $productionType]),
                                            'data-style' => 'expand-right',
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <button type="button" class="btn darkblue" data-dismiss="modal">
                                            НЕТ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="filter-block pull-right">
            <div class="dropdown pull-right">
                <?= Html::a('Фильтр  <span class="caret"></span>', null, [
                    'class' => 'btn btn-default btn-sm dropdown-toggle',
                    'data-toggle' => 'dropdown',
                    'style' => $hasFilters ? 'color: #f3565d!important;border-color: #f3565d!important;' : 'color: #def1ff;',
                ]); ?>
                <ul class="dropdown-menu">
                    <li>
                        <div class="row">
                            <span class="filter-label">Показывать</span>
                            <?= Html::activeDropDownList($searchModel, 'filterStatus', $filterStatusItems, [
                                'class' => 'form-control',
                                'data-id' => 'filterStatus',
                            ]); ?>
                        </div>
                    </li>
                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                        <li>
                            <div class="row text-right">
                                <span class="filter-label">Наличие картинки</span>
                                <?= Html::activeDropDownList($searchModel, 'filterImage', [
                                    ProductSearch::ALL => 'Все',
                                    ProductSearch::HAS_IMAGE => 'Есть',
                                    ProductSearch::NO_IMAGE => 'Нет',
                                ], [
                                    'class' => 'form-control',
                                    'data-id' => 'filterImage',
                                ]); ?>
                            </div>
                        </li>
                        <li>
                            <div class="row text-right">
                                <span class="filter-label">Наличие описания</span>
                                <?= Html::activeDropDownList($searchModel, 'filterComment', [
                                    ProductSearch::ALL => 'Все',
                                    ProductSearch::HAS_COMMENT => 'Есть',
                                    ProductSearch::NO_COMMENT => 'Нет',
                                ], [
                                    'class' => 'form-control',
                                    'data-id' => 'filterComment',
                                ]); ?>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li style="border-bottom: 0;">
                        <div class="row text-right">
                            <span class="filter-label">Добавлен</span>
                            <?= Html::activeDropDownList($searchModel, 'filterDate', [
                                ProductSearch::ALL => 'Все',
                                ProductSearch::FILTER_YESTERDAY => 'Вчера',
                                ProductSearch::FILTER_TODAY => 'Сегодня',
                                ProductSearch::FILTER_WEEK => 'Неделя',
                                ProductSearch::FILTER_MONTH => 'Месяц',
                            ], [
                                'class' => 'form-control',
                                'data-id' => 'filterDate',
                                'data-url' => Url::to(['filter-date', 'actionType' => 'set']),
                            ]); ?>
                            <?= Html::activeHiddenInput($searchModel, 'dateFrom', [
                                'data-id' => 'dateFrom',
                            ]); ?>
                            <?= Html::activeHiddenInput($searchModel, 'dateTo', [
                                'data-id' => 'dateTo',
                            ]); ?>
                        </div>
                        <div class="row block-date"
                             style="display: <?= $searchModel->filterDate == ProductSearch::ALL ? 'none;' : 'block;'; ?>">
                            <span class="filter-label"></span>
                            <div class="change-period-date">
                                        <span class="glyphicon glyphicon-chevron-left pull-left minus"
                                              data-url="<?= Url::to(['filter-date', 'actionType' => 'minus']); ?>"></span>
                                <span class="date-text">
                                            <?php if (in_array($searchModel->filterDate, [ProductSearch::FILTER_YESTERDAY, ProductSearch::FILTER_TODAY])): ?>
                                                <?= $searchModel->dateFrom; ?>
                                            <?php elseif ($searchModel->filterDate == ProductSearch::FILTER_WEEK): ?>
                                                <?= "{$searchModel->dateFrom} - {$searchModel->dateTo}"; ?>
                                            <?php else: ?>
                                                <?= FlowOfFundsReportSearch::$month[date('m', strtotime($searchModel->dateFrom))] . ' ' . date('Y', strtotime($searchModel->dateFrom)); ?>
                                            <?php endif; ?>
                                        </span>
                                <span class="glyphicon glyphicon-chevron-right pull-right plus"
                                      data-url="<?= Url::to(['filter-date', 'actionType' => 'plus']); ?>"></span>
                            </div>
                        </div>
                    </li>
                    <li style="border-bottom: 0;">
                        <div class="form-actions">
                            <div class="row action-buttons buttons-fixed">
                                <div class="spinner-button col-sm-3 col-xs-3"
                                     style="width: 35%;padding-left: 0;">
                                    <?= Html::button('Найти', [
                                        'class' => 'apply-filters btn darkblue widthe-100',
                                    ]); ?>
                                </div>
                                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 30%;"></div>
                                <div class="spinner-button col-sm-1 col-xs-1"
                                     style="width: 35%;padding-right: 0;">
                                    <?= Html::button('Сбросить', [
                                        'class' => 'apply-default-filters btn darkblue widthe-100',
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <?php if ($canUpdate && ($taxationType->osno || $taxationType->usn)): ?>
                <div class="pull-right" style="padding-right: 15px;">
                    <?= $this->render('_nds_form', [
                        'toggleButton' => [
                            'label' => 'Изменить НДС',
                            'class' => 'btn btn-sm pull-right',
                            'style' => 'padding: 4px 9px 3px; background: transparent; border: 1px solid #def1ff; color: #def1ff;',
                        ]
                    ]) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
        <div class="table-container products-table clearfix" style="">
            <?= GridView::widget([
                'id' => 'product-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'emptyText' => $emptyMessage,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'rowOptions' => [
                    'role' => 'row',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'cssClass' => 'product_checker',
                        'headerOptions' => [
                            'style' => 'width: 20px;',
                        ],
                    ],
                    [
                        'attribute' => 'article',
                        'headerOptions' => [
                            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                                'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                                'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                        ],
                    ],
                    [
                        'attribute' => 'title',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_name',
                        ],
                        'format' => 'raw',
                        'value' => function (Product $data) use ($productionType) {
                            if (Yii::$app->user->can(frontend\rbac\permissions\Product::VIEW)) {
                                $content = Html::a(Html::encode($data->title), [
                                    'view',
                                    'productionType' => $productionType,
                                    'id' => $data->id,
                                ]);
                            } else {
                                $content = Html::encode($data->title);
                            }

                            return Html::tag('div', $content, ['class' => 'product-title-cell']);
                        },
                    ],
                    [
                        'attribute' => 'group_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Группа ' . ($productionType ? 'товара' : 'услуги'),
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                            'width' => '14%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                        ],
                        'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
                        'value' => 'group.title',
                    ],
                    [
                        'attribute' => 'available',
                        'label' => 'Доступно',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->available * 1;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'reserve',
                        'label' => 'Резерв',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            $tooltipId = 'product-reserve-tooltip-' . $data->id;
                            $content = Html::a($data->reserve * 1, null, [
                                'class' => 'product-reserve-value',
                                'data' => [
                                    'tooltip-content' => '#' . $tooltipId,
                                    'url' => Url::to(['/product/reserve-documents', 'id' => $data->id]),
                                ],
                            ]);
                            $content .= Html::tag('span', Html::tag('span', '', ['id' => $tooltipId]), [
                                'class' => 'hidden',
                            ]);

                            return $content;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'quantity',
                        'label' => 'Остаток',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->quantity * 1;
                        },
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                    ],
                    [
                        'attribute' => 'product_unit_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'enableSorting' => false,
                        'label' => 'Ед. измерения',
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_unit',
                        ],
                        'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
                        'value' => 'productUnit.name',
                    ],
                    [
                        'label' => 'Картинка',
                        'headerOptions' => [
                            'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                            'width' => '30px',
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        'value' => function (Product $data) {
                            $content = '';
                            if ($thumb = $data->getImageThumb(200, 300)) {
                                $tooltipId = 'tooltip-product-image-' . $data->id;
                                $content = Html::tag('span', '', [
                                    'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                                    'data-tooltip-content' => '#' . $tooltipId,
                                    'style' => 'cursor: pointer;',
                                ]);
                                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                $content .= Html::beginTag('div', ['id' => $tooltipId]);
                                $content .= Html::img($thumb, ['alt' => '']);
                                $content .= Html::endTag('div');
                                $content .= Html::endTag('div');
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'comment_photo',
                        'label' => 'Описание',
                        'headerOptions' => [
                            'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                            'width' => '25%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                        ],
                        'format' => 'raw',
                        'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        'value' => function ($data) {
                            $content = '';
                            if ($data->comment_photo) {
                                $tooltipId = 'tooltip-product-comment-' . $data->id;
                                $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                                    'class' => 'product-comment-box',
                                    'data-tooltip-content' => '#' . $tooltipId,
                                    'style' => 'cursor: pointer;'
                                ]);
                                $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                                $content .= Html::encode($data->comment_photo);
                                $content .= Html::endTag('div');
                                $content .= Html::endTag('div');
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'price_for_sell_with_nds',
                        'label' => 'Цена продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->not_for_sale ? 'Не для продажи' :
                                TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
                        },
                    ],
                    [
                        'attribute' => 'price_for_sell_nds_id',
                        'label' => 'НДС продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return $data->not_for_sale ? 'Не для продажи' : ($data->priceForSellNds ? $data->priceForSellNds->name : '---');
                        },
                        'visible' => Yii::$app->user->identity->company->hasNds(),
                    ],
                    [
                        'attribute' => 'amountForSell',
                        'label' => 'Стоимость продажи',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'value' => function ($data) {
                            return TextHelper::invoiceMoneyFormat($data->amountForSell, 2);
                        },
                        'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                    ],
                    [
                        'attribute' => 'price_for_buy_with_nds',
                        'label' => 'Цена покупки',
                        'headerOptions' => [
                            'class' => 'col_product_purchase_price' . ($userConfig->product_purchase_price ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_purchase_price' . ($userConfig->product_purchase_price ? '' : ' hidden'),
                        ],
                        'value' => function ($data) {
                            return Yii::$app->user->can(permissions\Product::PRICE_IN_VIEW, [
                                'model' => $data,
                            ]) ? TextHelper::invoiceMoneyFormat($data->price_for_buy_with_nds, 2) : Product::DEFAULT_VALUE;
                        },
                        'visible' => $canDocumentIndexIn,
                    ],
                    [
                        'attribute' => 'price_for_buy_nds_id',
                        'label' => 'НДС покупки',
                        'headerOptions' => [
                            'class' => 'col_product_purchase_price' . ($userConfig->product_purchase_price ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_purchase_price' . ($userConfig->product_purchase_price ? '' : ' hidden'),
                        ],
                        'value' => 'priceForBuyNds.name',
                        'visible' => $canDocumentIndexIn && Yii::$app->user->identity->company->hasNds(),
                    ],
                    [
                        'attribute' => 'amountForBuy',
                        'label' => 'Стоимость закупки',
                        'headerOptions' => [
                            'class' => 'col_product_purchase_amount' . ($userConfig->product_purchase_amount ? '' : ' hidden'),
                        ],
                        'contentOptions' => [
                            'class' => 'col_product_purchase_amount' . ($userConfig->product_purchase_amount ? '' : ' hidden'),
                        ],
                        'value' => function ($data) {
                            return Yii::$app->user->can(permissions\Product::PRICE_IN_VIEW, [
                                'model' => $data,
                            ]) ? TextHelper::invoiceMoneyFormat($data->amountForBuy, 2) : Product::DEFAULT_VALUE;
                        },
                        'visible' => $canDocumentIndexIn && ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                    ],
                ],
            ]); ?>
        </div>
        <?= Html::endForm(); ?>
    </div>
</div>
<?php if ($countProduct == 0 && !Yii::$app->request->get('modal')) : ?>
    <?= \frontend\widgets\PromptWidget::widget([
        'prompt' => $prompt,
    ]); ?>
<?php endif; ?>

<?php
if (Yii::$app->user->can(permissions\Product::CREATE)) {
    echo $this->render('/xls/_import_xls', [
        'header' => 'Загрузка ' . $xlsMessageType . ' из Excel',
        'text' => '<p>Для загрузки списка ' . $xlsMessageType . ' из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
        'formData' => Html::hiddenInput('className', 'Product') . Html::hiddenInput('Product[production_type]', $productionType),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => $productionType ? XlsHelper::PRODUCT_GOODS : XlsHelper::PRODUCT_SERVICES]),
    ]);

    if (Yii::$app->request->get('modal')) {
        $this->registerJs('$("#import-xls").modal();');
    }
    if (Yii::$app->request->get('modal_price')) {
        $this->registerJs('$(document).ready(function() { $(".price-list-trigger").click(); }); ');
    }
    if ($company->show_popup_import_products) {
        $company->updateAttributes(['show_popup_import_products' => false]);
        $this->registerJs('$("#import-xls").modal();');
    }
}

$this->registerJs('
    $(".preview-product-photo").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "side": "right",
        "contentAsHTML": true
    });
    $(".product-comment-box").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "contentAsHTML": true
    });
    $(document).ready(function () {
        $("#product-grid tbody tr").each(function() {
            var $row = $(this);
            var height = $(".product-title-cell", $row).height();
            $(".product-comment-box", $row).css("height", height + "px");
            /*$(".product-comment-box", $row).pseudo(":before", "height", height + "px");*/
        });
    });
    $(document).on("click", ".product-reserve-value:not(.tooltipstered)", function() {
        var reserve = $(this);
        var tooltipId = reserve.data("tooltip-content")
        $.ajax({
            url: reserve.data("url"),
            success: function(data) {
                if (data.result) {
                    $(reserve.data("tooltip-content")).html(data.result);
                    reserve.tooltipster({
                        theme: "tooltipster-kub",
                        trigger: "click",
                        contentCloning: true
                    });
                    reserve.tooltipster("show");
                }
            }
        })
    });
    $("#config-product_group").change(function () {
        if (!$(this).is(":checked")) {
            $(".sorting-table-config-item#title").click().uniform();
        }
    });
    $(document).on("click", "a#many-to-archive, a#many-to-store-from-archive", function() {
        $.ajax({
            type: "post",
            url: $(this).data("url"),
            data: {
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    });
    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = $(this).val();
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select").each(function () {
            var $attrName = $(this).data("id");
            var $value = 0;
            if ($attrName == "filterStatus") {
                $value = 2;
            }
            $filterData["ProductSearch[" + $attrName + "]"] = $value;
        });
        $(".filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
    }

    $(document).on("change", "#productsearch-filterdate", function (e) {
        changePeriod($(this).val(), $(this).data("url"));
    });

    function changePeriod(dateType, url) {
        var dateFrom = $("#productsearch-datefrom");
        var dateTo = $("#productsearch-dateto");
        var dateText = $(".block-date .date-text");
        var blockDate = $(".block-date");

        if (dateType == 0) {
            dateFrom.val("");
            dateTo.val("");
            dateText.text("");
            blockDate.hide();
        } else {
            $.post(url, {
                date_type: dateType,
                date_from: dateFrom.val(),
                date_to: dateTo.val()
            }, function (data) {
                dateFrom.val(data.dateFrom);
                dateTo.val(data.dateTo);
                dateText.text(data.dateText);
                blockDate.show();
            });
        }
    }
'); ?>

<?php if (Yii::$app->session->remove('show_example_popup')): ?>

    <?= $this->render('index/_first_show_modal') ?>

<?php elseif (!Yii::$app->request->get('modal_price') && !Yii::$app->request->get('modal') && ($company->show_popup_product && $priceListsExists) && $productionType == Product::PRODUCTION_TYPE_GOODS): ?>
    <?php Modal::begin([
        'header' => '<h2 class="header-name" style="text-transform: uppercase;">
            Учёт товара
            </h2>',
        'footer' => $this->render('//layouts/modal/_partial_footer', [
            'type' => Company::AFTER_REGISTRATION_STOCK_CONTROL,
        ]),
        'id' => 'modal-loader-items'
    ]); ?>
    <div class="col-xs-12" style="padding: 0" id="modal-loader">
        <?= $this->render('//layouts/modal/_template_submodal', [
            'type' => 5,
            'description' => 'ОПРИХОДОВАНИЕ, СПИСАНИЕ, ПЕРЕМЕЩЕНИЕ ТОВАРА.<br>
            КОНТРОЛЬ ОСТАТКОВ. ПЛАНИРОВАНИЕ ЗАКУПОК.',
            'video' => 'https://www.youtube.com/embed/jXesTUBlxl4',
            'link' => Url::to(['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS]),
            'image' => ImageHelper::getThumb('img/modal_registration/block-3.jpg', [680, 340], [
                'class' => 'hide-video',
                'style' => 'max-width: 100%',
            ]),
            'previousModal' => 4,
            'nextModal' => 6,
        ]); ?>
    </div>
    <style>
        #modal-loader-items .modal-body {
            padding: 0;
        }
    </style>
    <?php Modal::end(); ?>
    <?php $this->registerJs('
        $(document).ready(function () {
            $(".modal#modal-loader-items").modal();
        });
    '); ?>
<?php endif; ?>

<?php if ($canUpdate): ?>
    <?= $this->render('index/move_to_group', [
        'productionType' => $productionType,
    ]); ?>
    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
        <?= $this->render('index/move_to_store', ['storeList' => $storeList]); ?>
        <?= $this->render('index/combine_to_one', ['userConfig' => $userConfig]); ?>
    <?php endif; ?>
<?php endif; ?>
<?= $this->render('index/price_list', [
    'priceList' => $priceList,
    'productionType' => $productionType,
    'company' => $company,
    'store' => $store,
    'user' => $user,
]); ?>
