<?php
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $canViewPriceForBuy boolean */

if (!isset($canViewPriceForBuy)) {
    $canViewPriceForBuy = true;
}
?>
<div class="product-form">
    <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'options' => ['enctype' => 'multipart/form-data'],
        'id' => 'product-form',
    ])); ?>
    <div class="form-body form-horizontal form-body_width">
        <?= $this->render('partial/_mainForm', [
            'model' => $model,
            'form' => $form,
            'canViewPriceForBuy' => $canViewPriceForBuy,
        ])?>

        <?= $this->render('partial/_formButtons', [
            'model' => $model,
        ])?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
