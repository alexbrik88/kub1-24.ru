<?php
/** @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\TimeZone;

/** @var $employee \common\models\employee\Employee */

$changePasswordModel = new \frontend\models\ChangePasswordForm();
$changeEmailModel = new \frontend\models\ChangeEmailForm($employee);
$changeNotifyModel = new \frontend\models\ChangeNotifyForm();
$changeTimeZone = new \frontend\models\ChangeTimeZoneForm();
$changeNotifyModel->notifyNearlyReport = $employee->notify_nearly_report;
$changeNotifyModel->notifyNewFeatures = $employee->notify_new_features;

$this->title = 'Настройки профиля';

$this->context->layoutWrapperCssClass = 'profile-setup';
?>

<h3 class="page-title">Профиль: <?= $employee->currentEmployeeCompany->getFio(); ?></h3>

<?php $flash = Yii::$app->session->getFlash('change_profile'); ?>
<?php if (!empty($flash)) {
    echo \yii\bootstrap\Alert::widget([
        'body' => $flash,
    ]);
} ?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение пароля
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changepassword']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <div class="form-body form-horizontal">
            <?= $form->field($changePasswordModel, 'oldPassword')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changePasswordModel, 'newPassword')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changePasswordModel, 'newPasswordRepeat')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo Html::submitButton('Изменить', [
                            'class' => 'btn darkblue text-white',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение часового пояса
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body form-horizontal">
            <div class="form-group">
                <label for="name" class="control-label col-md-4 label-width">Текущий
                    часовой пояс:</label>

                <div class="col-md-5 pad-e-b inp_one_line_prof">
                    <p class="form-control-static pad-e-p"><?php $timeZone = TimeZone::findOne($employee->time_zone_id);
                        echo $timeZone->out_time_zone ?></p>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changetimezone']),
            'class' => 'form-horizontal',
        ]); ?>
        <?php $changeTimeZone->newTimeZone = TimeZone::DEFAULT_TIME_ZONE; ?>
        <?= $form->field($changeTimeZone, 'newTimeZone')->label(null, [
            'class' => 'control-label contr_lab_left',
        ])->dropDownList(
            ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <?php echo Html::submitButton('Изменить', [
                        'class' => 'btn darkblue text-white',
                    ]); ?>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение адреса электронной почты
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changeemail']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>

        <div class="form-body form-horizontal">

            <div class="form-group">
                <label for="name" class="control-label col-md-4 label-width">Текущая
                    электронная почта:</label>

                <div class="col-md-5 pad-e-b inp_one_line_prof">
                    <p class="form-control-static pad-e-p"><?php echo $employee->email; ?></p>
                </div>
            </div>

            <?= $form->field($changeEmailModel, 'email')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->textInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changeEmailModel, 'password')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo Html::submitButton('Изменить', [
                            'class' => 'btn darkblue text-white',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Получение уведомлений
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['profile/changenotify']),
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
        ]); ?>
        <div class="form-body form-horizontal">
            <?= $form->field($changeNotifyModel, 'notifyNewFeatures')->label(null, [
                'class' => 'control-label checkbox-inline',
            ])->checkbox([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changeNotifyModel, 'notifyNearlyReport')->label(null, [
                'class' => 'control-label checkbox-inline',
            ])->checkbox([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo Html::submitButton('Изменить', [
                            'class' => 'btn darkblue text-white',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>