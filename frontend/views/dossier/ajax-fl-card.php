<?php

use common\components\zchb\FlCard;

/**
 * @var FlCard $flCard
 */
?>
<?php if ($flCard === null) { ?>
    <p>К сожалению дополнительной информации нет.</p>
<?php } else { ?>
    <h3><?= $flCard->fio ?></h3>
    <p><b>ФИО</b>: <?= $flCard->fio ?></p>
    <p><b>ИНН</b>: <?= $flCard->inn ?></p>
    <p><b>Регион ведения бизнеса</b>: <?= $flCard->region ?></p>
    <?php if (count($flCard->director) > 0) { ?>
        <h4>Руководитель:</h4>
        <ul>
            <?php foreach ($flCard->director as $item) { ?>
                <li><?= $item['НаимЮЛСокр'] ?> (<?= $item['Активность'] ?>)</li>
            <?php } ?>
        </ul>
    <?php } ?>
    <?php if (count($flCard->founder) > 0) { ?>
        <h4>Учредитель:</h4>
        <ul>
            <?php foreach ($flCard->founder as $item) { ?>
                <li><?= $item['НаимЮЛСокр'] ?> (<?= $item['Активность'] ?>)</li>
            <?php } ?>
        </ul>
    <?php } ?>
    <?php if ($flCard->ie) { ?>
        <h4>ИП:</h4>
        <ul>
            <?php foreach ($flCard->ie as $item) { ?>
                <li><?= $item['ФИО'] ?> (<?= $item['Активность'] ?>)</li>
            <?php } ?>
        </ul>

    <?php } ?>
<?php } ?>