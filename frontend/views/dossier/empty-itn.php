<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">Проверка контрагентов</div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="t-subtitle">
            Проверка контрагента не возможна, т.к. у него не задан ОГРН/ИНН.
        </div>
    </div>
</div>