<?php

use common\components\zchb\Card;
use common\models\Contractor;
use yii\helpers\Url;

/**
 * @var Card $card
 * @var array[] $connections
 * @var int $countByDirector
 * @var int $countByFounder
 */
$tabLink = Url::toRoute($this->params['tabLink']('connections'));
if (count($connections) === 0) {
    echo '<p class="tile-item__text">Связей ', $card->НаимЮЛПолн, ' с другими организациями не выявлено.</p>';
    return;
} ?>
<p class="tile-item__text">Выявлено <?= count($connections) ?> связанных c <?= $card->НаимЮЛПолн ?> организаций.</p>
<div class="connexion">
    <div class="connexion-col">
        <div class="connexion-col__title">Всего</div>
        <div class="connexion-col__num"><a class="num gtm_c_all" href="<?= $tabLink ?>"
                                           rel="nofollow"><?= count($connections) ?></a></div>
    </div>
    <?php /*
    <div class="connexion-col">
        <div class="connexion-col__title"><span style="color:red">По адресу</span></div>
        <div class="connexion-col__num"><a class="num gtm_c_1" href="#"
                                           rel="nofollow"><span style="color:red">4</span></a></div>
    </div>
    */ ?>
    <div class="connexion-col">
        <div class="connexion-col__title">По руководителю</div>
        <div class="connexion-col__num"><a class="num gtm_c_2" href="<?= $tabLink ?>#director"
                                           rel="nofollow"><?= $countByDirector ?></a></div>
    </div>
    <div class="connexion-col">
        <div class="connexion-col__title">По учредителю</div>
        <div class="connexion-col__num"><a class="num gtm_c_3" href="<?= $tabLink ?>#founder"
                                           rel="nofollow"><?= $countByFounder ?></a></div>
    </div>
    <a href="<?= $tabLink ?>" class="see-details gtm_c_more" rel="nofollow"><span>Все связи</span></a>
</div>