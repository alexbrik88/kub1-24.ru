<?php

use common\components\zchb\CourtArbitrationCard;

/**
 * @var CourtArbitrationCard $courtCard
 */
?>
<?php if ($courtCard->count() === 0) { ?>
    <p></p>
    <p><strong>Судебных дел нет.</strong></p>

<?php }
foreach ($courtCard as $id => $item) {
    $lastEvent = $item['chronology'][count($item['chronology']) - 1];
    $lastDoc = end($lastEvent['docs']);
    ?>

    <div class="company-item">
        <div class="license-name">№ <?= $item['number'] ?> <span class="license-name__date">от <span
                        class="green-mark"><?= $item['dateStart'] ?></span></span>
            <a target="_blank" href="https://kad.arbitr.ru/Card/<?= $id ?>" class="out-link"
               rel="nofollow noopener"><span>на сайте КАД</span></a>
        </div>

        <div class="company-item__text"><?= $item['type'] ?></div>

        <div class="company-item-info">
            <dl>
                <dt>Сумма:</dt>
                <dd><?= number_format($item['amount'], 0, '.', ' ') ?> руб.</dd>
            </dl>
        </div>

        <div class="company-item-info">
            <dl>
                <dt>Статус:</dt>
                <dd><?= $courtCard::statusLabel($item['status']) ?></dd>
            </dl>
        </div>
        <div class="company-item-info">
            <dl>
                <dt>Инстанция:</dt>
                <dd><?= $courtCard::instanceLabel($item['instance']) ?></dd>
            </dl>
        </div>

        <div class="company-item-info">
            <dl>
                <dt>Последнее обновление:</dt>
                <dd><?= date('d.m.Y', $lastDoc['date']) ?></dd>
            </dl>
        </div>

        <div class="company-item-info">
            <dl>
                <dt>Истец:</dt>
                <dd><span><?= $courtCard::implodeItem($item[$courtCard::ROLE_APPLICANT],
                            $item['role'] !== $courtCard::ROLE_APPLICANT) ?></span></dd>
            </dl>
        </div>
        <div class="company-item-info">
            <dl>
                <dt>Ответчик:</dt>
                <dd><span><?= $courtCard::implodeItem($item[$courtCard::ROLE_DEFENDANT],
                            $item['role'] !== $courtCard::ROLE_DEFENDANT) ?></span></dd>
            </dl>
        </div>
        <?php if (isset($item[$courtCard::ROLE_THIRD]) === true) { ?>
            <div class="company-item-info">
                <dl>
                    <dt>Третья сторона:</dt>
                    <dd><span><?= $courtCard::implodeItem($item[$courtCard::ROLE_THIRD],
                                $item['role'] !== $courtCard::ROLE_THIRD) ?></span></dd>
                </dl>
            </div>
        <?php } ?>
        <?php if (isset($item[$courtCard::ROLE_OTHER]) === true) { ?>
            <div class="company-item-info">
                <dl>
                    <dt>Иные участники:</dt>
                    <dd><span><?= $courtCard::implodeItem($item[$courtCard::ROLE_OTHER],
                                $item['role'] !== $courtCard::ROLE_OTHER) ?></span></dd>
                </dl>
            </div>
        <?php } ?>
        <div class="company-item-info">
            <dl>
                <dt>ПОСЛЕДНИЙ ДОКУМЕНТ:</dt>
                <dd><?= $lastDoc['name'] ?></dd>
            </dl>
        </div>
    </div>
<?php } ?>
<script>
    {
        var descr = document.getElementsByClassName('content-frame__description')[0];
        descr.getElementsByTagName('span')[0].innerHTML = '<?=$courtCard::plural($courtCard->count($courtCard::STATUS_FINISH),
            'завершённое дело', 'завершённых дела', 'завершённых дел',
            'завершённых дел нет')?>';
        descr.style.display = '';
    }
</script>
