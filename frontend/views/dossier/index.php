<?php
/** @noinspection PhpUnhandledExceptionInspection */

use common\components\zchb\Card;
use common\components\zchb\Contact;
use common\components\zchb\CourtArbitration;
use common\components\zchb\Diff;
use common\components\zchb\ReliabilityHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View              $this
 * @var int               $modelId
 * @var string            $title
 * @var Card              $card Информация по организации, полученная от ЗАЧЕСТНЫЙБИЗНЕС
 * @var Diff              $diff Исторические изменения организации
 * @var CourtArbitration  $courtArbitration
 * @var Contact           $contact
 * @var ReliabilityHelper $reliability
 */
$tabLink = $this->params['tabLink'];

$dataTime = Yii::$app->user->identity->company->getDossierLogs()->andWhere([
    'contractor_id' => $this->params['contractor_id'] ?? null,
])->max('created_at');
?>
<script>setDossierTab('index');</script>
<div class="row">
    <div class="col-sm-12">
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12 contactor-name">
                    <span><?= $title ?></span>
                    <div class="reload">
                        <?= $this->render('_popup-confirm', [
                            'message' => 'Вы уверены, что хотите отправить новый запрос для обновления информации?',
                            'link' => Url::toRoute($tabLink('cache-clear')),
                            'button' => [
                                'class' => 'icon-refresh',
                                'tag' => 'span',
                            ]
                        ]) ?>
                        <?= date('d.m.Y в H:i', $dataTime) ?><br/>
                        данные были получены
                    </div>
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <div class="company-info" id="anketa">
                    <div class="mb-3">
                        <div class="company-name" itemprop="legalName"><?= $card->НаимЮЛПолн ?></div>
                        <div class="company-status active-<?= $card->isActive() === true ? 'yes' : 'no' ?>"><?= $card->isActive() === true ? 'Действующая организация' : 'Организация не действует' ?></div>
                    </div>
                    <div class="clear">
                        <div class="leftcol">
                            <div class="company-requisites">
                                <div class="company-row">
                                    <?php if ($card->ОГРН !== null) { ?>
                                        <dl class="company-col">
                                            <dt class="company-info__title">ОГРН</dt>
                                            <dd class="company-info__text has-copy">
                                                <span class="copy_target" id="clip_ogrn"><?= $card->ОГРН ?></span>
                                            </dd>
                                            <dd class="company-info__text">от <?= $card->dateOGRN() ?></dd>
                                        </dl>
                                    <?php } ?>
                                    <?php if ($card->ИНН || $card->КПП) { ?>
                                        <dl class="company-col">
                                            <dt class="company-info__title">ИНН/КПП</dt>
                                            <dd class="company-info__text has-copy" itemprop="taxID">
                                                <span class="copy_target" id="clip_inn"><?= $card->ИНН ?></span>
                                            </dd>
                                            <dd class="company-info__text has-copy">
                                                <span class="copy_target" id="clip_kpp"><?= $card->КПП ?></span>
                                            </dd>
                                        </dl>
                                    <?php } ?>
                                </div>
                                <div class="company-row">
                                    <dl class="company-col">
                                        <dt class="company-info__title">Дата регистрации</dt>
                                        <dd class="company-info__text"
                                            itemprop="foundingDate"><?= $card->ДатаПостУч ?></dd>
                                    </dl>
                                    <dl class="company-col">
                                        <dt class="company-info__title">Уставный капитал</dt>
                                        <dd class="company-info__text">
                                                <span class="copy_target"><?= number_format($card->СумКап, 0, '.',
                                                        ' ') ?> руб.</span>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="company-row company-info__all">
                                    <a class="gtm_main_requisites" rel="nofollow"
                                       onclick="return rightPanel.fromContainer(document.getElementById('requisite'));">Все
                                        реквизиты</a>
                                    <span class="small">(ФНС / ПФР / ФСС / РОССТАТ)</span>
                                </div>
                            </div>
                            <div class="company-row">
                                <span class="company-info__title">Юридический адрес</span>
                                <address class="company-info__text" itemprop="address" itemscope=""
                                         itemtype="https://schema.org/PostalAddress"><?= $card->schemaAddress() ?>
                                    <meta itemprop="addressCountry" content="RU">
                                    <meta itemprop="addressLocality" content="<?= mb_strtolower($card->ТипГород,
                                        'UTF-8') ?> <?= mb_convert_case($card->НаимГород, MB_CASE_TITLE,
                                        'UTF-8') ?>">
                                </address>
                            </div>
                            <div class="company-row hidden-parent">
                                <span class="company-info__title">Руководитель</span>
                                <span class="chief-title"><?= $card->firstDirectorPosition() ?></span>
                                <span class="company-info__text">
                                    <a href="<?= Url::toRoute([
                                        'ajax-fl-card',
                                        'id' => $card->firstDirectorINN(),
                                        'modelId' => $modelId
                                    ]) ?>"
                                       class="link-arrow gtm_main_fl popup" onclick="return rightPanel.fromURL(this.href);">
                                        <span><?= $card->firstDirectorName() ?></span>
                                    </a></span>
                                <span class="chief-title"><?php if ($card->firstDirectorDate() !== null) { ?>с <?= Yii::$app->formatter->asDate($card->firstDirectorDate(),
                                        'long') ?><?php } ?></span>
                            </div>
                            <dl class="company-row">
                                <dt class="company-info__title">Среднесписочная численность
                                    <span class="tooltip-popup"><span><b>Источник</b>: реестр &quot;Сведения о среднесписочной численности работников организации&quot; ФНС РФ<br>Данные за <?= $card->employeeCountYear ?> год</span></span>
                                </dt>
                                <dd class="company-info__text"><?= $card->employeeCountFormatted() ?>
                                    (<?= $card->employeeCountYear ?>)
                                </dd>
                            </dl>
                            <dl class="company-row">
                                <dt class="company-info__title">Специальный налоговый режим
                                    <span class="tooltip-popup"><span><b>Источник</b>: реестр &quot;Сведения о специальных налоговых режимах, применяемых налогоплательщиками&quot; ФНС РФ<br>Данные за <?= $card->reportingIndicatorsLast['year'] ?? '--' ?> год</span></span>
                                </dt>
                                <dd class="company-info__text">
                                    <?= $card->specialTaxRegime() ?> (<?= $card->reportingIndicatorsLast['year'] ?? '--' ?>)
                                </dd>
                            </dl>
                        </div>
                        <div class="rightcol">
                            <div class="company-row">
                                <span class="company-info__title">Основной вид деятельности</span>
                                <span class="company-info__text"><?= $card->OKVEDTitle ?> <span
                                            class="bolder">(<?= $card->КодОКВЭД ?>)</span></span>
                                <a href="<?= Url::toRoute($tabLink('okved')) ?>" class="gtm_main_okved" rel="nofollow">Все виды
                                    деятельности (<?= $card->OKVEDCount() ?>)</a>
                            </div>
                            <div class="company-row">
                                <span class="company-info__title">Налоговый орган</span>
                                <span class="company-info__text"><?= $card->НаимНО ?></span>
                                <span class="chief-title">с <?= $card->taxServiceDate() ?></span>
                            </div>
                            <dl class="company-row">
                                <dt class="company-info__title">Коды статистики</dt>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКПО</span>
                                    <span class="copy_target" id="clip_okpo"><?= $card->ОКПО ?></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКАТО</span>
                                    <span class="copy_target" id="clip_okato"><?= $card->ОКАТО ?></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКОГУ</span>
                                    <span class="copy_target" id="clip_okogu"><?= $card->ОКОГУ ?></span>
                                </dd>
                                <dd class="company-info__text has-copy" itemprop="taxID">
                                    <span class="copy_title">ОКТМО</span>
                                    <span class="copy_target" id="clip_oktmo"><?= $card->ОКТМО ?></span>
                                </dd>
                            </dl>
                            <div class="company-row">
                                    <span class="company-info__title">Реестр МСП
                                        <span class="tooltip-popup"><span><b>Источник</b>: Единый реестр субъектов малого и среднего предпринимательства ФНС РФ. Данные за <?= date('Y') ?> год</span></span>
                                    </span>
                                <span class="company-info__text">Статус: <?= $card->categoryMSP() ?></span>
                                <?php /*
                                    <span class="chief-title"><span style="color:red">присвоен 1 августа 2016 г.</span></span>
                                    */ ?>
                            </div>

                        </div>
                    </div>
                    <div class="company-info__intelligence">
                        <?php if ($diff->count() > 0) { ?>
                            По организации доступны <a class="gtm_main_history" rel="nofollow"
                                                       onclick="return rightPanel.fromContainer(document.getElementById('diff'));">исторические
                                сведения</a> (<?= $diff->count() ?> изменений).
                        <?php } ?>
                    </div>
                    <div class="anketa-bottom">
                        <div class="anketa-actual">Актуально на <?= date('d.m.Y', $card->cacheTime()) ?></div>
                        <p class="information-text">
                            <?= $card->Описание ?>.
                            Статус организации: <?= $card->Активность ?>.
                            Руководителем является
                            <?= $card->firstDirectorPosition() ?> <?= $card->firstDirectorName() ?>
                            (ИНН <?= $card->firstDirectorINN() ?>). Размер уставного
                            капитала - <?= number_format($card->СумКап, 0, '.', ' ') ?> рублей.
                        </p>
                        <div class="anketa-clip">
                            <p class="information-text">
                                В выписке из ЕГРЮЛ в качестве учредителя
                                указано <?= $card::plural($card->founderCount(), 'физическое лицо',
                                    'физических лица', 'физических лиц') ?>. Основной
                                вид
                                деятельности - <?= $card->OKVEDTitle ?>
                                <?php if ($card->OKVEDCount() > 0) { ?>
                                    , также указано <?= $card::plural($card->OKVEDCount(), 'дополнительный вид',
                                        'дополнительных вида', 'дополнительных видов') ?>
                                <?php } ?>.
                                <?php if ($card->categoryMSP()) { ?>
                                    Организация присутствует в реестре Малого и среднего бизнеса (МСП) как <?= $card->categoryMSP() ?>.
                                <?php } ?>
                                <?php if ($diff->count() > 0) { ?>
                                    В исторических сведениях доступно <?= $diff->count() ?> записей об изменениях, последнее изменение
                                    датировано <?= $diff->lastDate() ?>
                                <?php } ?>
                            </p>
                            <p class="information-text">
                                Организация состоит на учете в налоговом органе <?= $card->НаимНО ?>
                                с <?= $card->taxServiceDate() ?>, присвоен КПП <?= $card->КПП ?>.
                                Регистрационный номер в
                                ПФР - <?= $card->РегНомПФ ?>, ФСС - <?= $card->РегНомФСС ?>.</p>
                            <p class="information-text">
                                <?php /*
                                        <span style="color:red">Информации об участии ООО "Смарт+" в тендерах не найдено.</span>
                                        */ ?>
                                <?php if ($courtArbitration->empty() === true) { ?>
                                    Данных об участии в арбитражных делах нет.
                                <?php } else { ?>
                                    Есть данные об участии организации в <?= $courtArbitration::plural($courtArbitration->count(),
                                        'завершенном арбитражном деле',
                                        'завершенных арбитражных делах',
                                        'завершенных арбитражных делах') ?>.
                                <?php } ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Надёжность</div>
            </div>
            <div class="portlet-body accounts-list">
                <p style="font-weight:bold;">Уровень надёжности:
                    <span class="rely-tile-badge"
                          style="background-color:<?= $reliability->color() ?>;"><?= $reliability->reliabilityString() ?></span>
                </p>
                <p class="tile-item__text"><?= $card::pluralReplace($reliability->count(), 'Выявлен {n} факт',
                        'Выявлено {n} факта', 'Выявлено {n} фактов') ?> об организации:</p>
                <div class="connexion">
                    <div class="connexion-col">
                        <div class="connexion-col__title">Положительные</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow"
                               href="<?= Url::toRoute($tabLink('reliability')) ?>#positive"><?= $reliability->positiveCount() ?></a>
                        </div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">Отрицательные</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow"
                               href="<?= Url::toRoute($tabLink('reliability')) ?>#negative"><?= $reliability->negativeCount() ?></a>
                        </div>
                    </div>
                    <div class="connexion-col">
                        <div class="connexion-col__title">Требующие<br/>внимания</div>
                        <div class="connexion-col__num">
                            <a rel="nofollow"
                               href="<?= Url::current($tabLink('reliability')) ?>#attention"><?= $reliability->attentionCount() ?></a>
                        </div>
                    </div>
                </div>
                <a href="<?= Url::toRoute($tabLink('reliability')) ?>" class="see-details" rel="nofollow">Подробнее о рейтинге и
                    фактах</a>
            </div>
        </div>
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Связи</div>
            </div>
            <div class="portlet-body accounts-list" id="connection">
                <?php if ($card->ОГРН || $card->ИНН) { ?>
                    <div class="loader"><span></span><span></span><span></span></div>
                    <script>
                        $('#connection').load('<?=Url::toRoute([
                            'ajax-connections',
                            'ogrn' => $card->ОГРН ?? $card->ИНН,
                            'modelId' => $modelId
                        ])?>');
                    </script>
                <?php } else { ?>
                    <p style="font-style:italic;">ОГРН или ИНН не известны, поэтому информации по связаям с другими
                        организациями нет.</p>
                <?php } ?>
            </div>
        </div>
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Учредители</div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">
                    <?php if ($card->founderCount() === 0) { ?>
                        В ЕГРЮЛ данных об учредителях нет.
                    <?php } else { ?>
                        Согласно данным ЕГРЮЛ учредителем <?= $title ?> является
                        <?= $card::plural($card->founderCount(), 'лицо', 'лица', 'лиц') ?>:    <?php } ?>
                </p>
                <?php foreach ($card->founders as $item) { ?>
                    <div class="founder-item">
                        <div class="founder-item__title">
                            <?php if ($item['isFL'] === true) { ?>
                                <a href="<?= Url::toRoute([
                                    'ajax-fl-card',
                                    'id' => $item['inn'] ?? $item['ogrn'],
                                    'modelId' => $modelId
                                ]) ?>" class="link-arrow gtm_f_list popup"
                                   onclick="return rightPanel.fromURL(this.href);">
                                    <span><?= $item['name'] ?></span>
                                </a>
                            <?php } else { ?>
                                <span><?= $item['name'] ?></span>
                            <?php } ?>
                        </div>
                        <dl class="founder-item__dl">
                            <dt>Доля:</dt>
                            <dd>
                                <?php if ($item['dol_abs'] > 0 && $card->СумКап > 0) { ?>
                                    <?= number_format($item['dol_abs'], 0, '.', ' ') ?> руб. <span
                                            class="percent">(<?= round($item['dol_abs'] / $card->СумКап * 100) ?>%)</span>
                                <?php } else { ?>
                                    нет информации
                                <?php } ?>
                            </dd>
                            <?php if ($item['isFL'] === true) { ?>
                                <dt>ИНН:</dt>
                                <dd><?= $item['inn'] ?></dd>
                            <?php } else { ?>
                                <dt>ОГРН:</dt>
                                <dd><?= $item['ogrn'] ?></dd>
                            <?php } ?>
                        </dl>
                        <div style="clear:both;"></div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Судебные дела</div>
            </div>
            <div class="portlet-body accounts-list">
                <?php if ($courtArbitration->empty() === true) { ?>
                    <p class="tile-item__text">Данных о судебных делах с участием <?= $title ?> нет.</p>
                <?php } else { ?>
                    <p class="tile-item__text">Имеются данные
                        о <?= $courtArbitration::plural($courtArbitration->count(), 'завершённом судебном деле',
                            'завершённых судебных делах',
                            'завершённых судебных делах') ?> с участием <?= $title ?>:</p>
                    <dl class="text-dl">
                        <?php if ($courtArbitration->countApplicant() > 0) { ?>
                            <dt>В качестве истца:</dt>
                            <dd><?= $courtArbitration->countApplicant() ?></dd>
                        <?php } ?>
                        <?php if ($courtArbitration->countDefendant() > 0) { ?>
                            <dt>В качестве ответчика:</dt>
                            <dd><?= $courtArbitration->countDefendant() ?></dd>
                        <?php } ?>
                        <?php if ($courtArbitration->countThirdParty() > 0 || $courtArbitration->countOtherParty() > 0) { ?>
                            <dt>В роли третьей стороны:</dt>
                            <dd><?= $courtArbitration->countThirdParty() + $courtArbitration->countOtherParty() ?></dd>
                        <?php } ?>
                    </dl>
                    <a href="<?= Url::toRoute($tabLink('arbitr')) ?>" class="see-details gtm_ar_more" rel="nofollow">Все судебные
                        дела</a>
                <?php } ?>
            </div>
        </div>

        <?php /*
            <div class="portlet box darkblue">
                <div class="portlet-title row-fluid">
                    <div class="caption col-sm-12">
                        Гос. закупки
                    </div>
                </div>
                <div class="portlet-body accounts-list">
                    <p class="tile-item__text"><span style="color:red">Сведения об участии ООО "Смарт+" в госзакупках в качестве поставщика или
                    заказчика по 44-ФЗ, 94-ФЗ и 223-ФЗ отсутствуют.</span></p>
                </div>
            </div>
            */ ?>

        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Долги</div>
            </div>
            <div class="portlet-body accounts-list" id="enforcement">
                <?php if ($card->Реестр01 == 0) { ?>
                    Данных о судебных производствах нет.
                <?php } else { ?>
                    Имеется взыскиваемая судебными приставами задолженность по уплате налогов, превышающая 1000 рублей.
                <?php } ?>
            </div>
        </div>

        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">
                    Проверки
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">
                    <?php if ($card->Проверки > 0) { ?>
                        В отношениее <?= $title ?> зарегистрировано <?= $card::plural($card->Проверки,
                            'проведённая проверка', 'проведённые проверки',
                            'проведённых проверок') ?>.
                    <?php } else { ?>
                        Данных о проведении в отношении <?= $title ?> плановых и внеплановых проверок нет.
                    <?php } ?>

                </p>
            </div>
        </div>
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">
                    Филиалы и представительства
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <p class="tile-item__text">
                    <?php if ($card->СвФилиал > 0) { ?>
                        <?= $title ?> имеет <?= $card::plural($card->СвФилиал, 'филиал',
                            'филиала', 'филиалов') ?>.
                    <?php } else { ?>
                        Сведения о филиалах и представительствах <?= $title ?> отсутствуют.
                    <?php } ?></p>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">
                    Финансы
                </div>
            </div>
            <div class="portlet-body accounts-list" id="finance">
                <?php if ($card->ОГРН) { ?>
                    <div class="loader"><span></span><span></span><span></span></div>
                    <script>
                        $('#finance').load('<?=Url::toRoute([
                            'ajax-finance',
                            'ogrn' => $card->ОГРН,
                            'modelId' => $modelId
                        ])?>');
                    </script>
                <?php } else { ?>
                    <p style="font-style:italic;">Информация не доступна для ИП.</p>
                <?php } ?>
            </div>
        </div>
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Краткая информация</div>
            </div>
            <div class="portlet-body accounts-list">
                <div class="enquiry" itemprop="description">
                    <h2 class="tile-item__title">Краткая справка</h2>

                    <p class="enquiry-text"><?= $title ?>
                        зарегистрирована <?= $card->ДатаПостУч ?> регистратором <?= $card->НаимНО ?>. Руководитель
                        организации: <?= $card->firstDirectorPosition() ?> <?= $card->firstDirectorName() ?>.
                        Юридический адрес <?= $title ?> - <?= $card->Адрес ?>.</p>

                    <p class="enquiry-text">Основным видом деятельности является «<?= $card->OKVEDTitle ?>»,
                        зарегистрировано <?= $card->OKVEDCount() ?> дополнительных видов
                        деятельности. Организации <?= $card->НаимЮЛПолн ?> присвоены ИНН
                        <?= $card->ИНН ?>, ОГРН <?= $card->ОГРН ?>, ОКПО <?= $card->ОКПО ?>.</p>
                    <?php if ($contact->empty() === true) { ?>
                        <p class="enquiry-text">Телефон, адрес электронной почты, адрес официального сайта и другие
                            контактные данные <?= $title ?> отсутствуют в ЕГРЮЛ и могут
                            быть
                            добавлены представителем организации.</p>
                    <?php } else { ?>
                        <p class="enquiry-text">Контактная информация:</p>
                        <ul>
                            <?php foreach ($contact->getData(true) as $item) { ?>
                                <li><?= $item['label'] ?>: <?= $item['value'] ?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>

        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12">Лицензии</div>
            </div>
            <div class="portlet-body accounts-list">
                <?= $card::pluralReplace($card->license, 'Организации выдана {n} лицензия',
                    'Организации выдано {n} лицензии', 'Организации выдано {n} лицензий',
                    'Организация не имеет лицензий') ?>.
            </div>
        </div>

    </div>
</div>

<div id="requisite" style="display:none;">
    <h3>Реквизиты <?= $title ?></h3>
    <ul>
        <?php if ($card->ОГРН) { ?>
            <li>ОГРН: <?= $card->ОГРН ?></li>
        <?php } ?>
        <?php if ($card->ИНН) { ?>
            <li>ИНН: <?= $card->ИНН ?></li>
        <?php } ?>
        <?php if ($card->КПП) { ?>
            <li>КПП: <?= $card->КПП ?></li>
        <?php } ?>
        <?php if ($card->ОКПО) { ?>
            <li>ОКПО: <?= $card->ОКПО ?></li>
        <?php } ?>
        <?php if ($card->ОКАТО) { ?>
            <li>ОКАТО: <?= $card->ОКАТО ?></li>
        <?php } ?>
        <?php if ($card->ОКТМО) { ?>
            <li>ОКТМО: <?= $card->ОКТМО ?></li>
        <?php } ?>
        <?php if ($card->ОКФС) { ?>
            <li>ОКФС: <?= $card->ОКФС ?></li>
        <?php } ?>
        <?php if ($card->КодОПФ) { ?>
            <li>Код ОПФ: <?= $card->КодОПФ ?></li>
        <?php } ?>
        <?php if ($card->КодОКВЭД) { ?>
            <li>Код ОКВЭД: <?= $card->КодОКВЭД ?></li>
        <?php } ?>
        <?php if ($card->РегНомПФ) { ?>
            <li>Номер ПФ: <?= $card->РегНомПФ ?></li>
        <?php } ?>
        <?php if ($card->РегНомФСС) { ?>
            <li>Номер ФСС: <?= $card->РегНомФСС ?></li>
        <?php } ?>
        <?php if ($card->Адрес) { ?>
            <li>Адрес: <?= $card->Адрес ?></li>
        <?php } ?>
    </ul>

</div>

<div id="diff" style="display:none;">
    <h3>Исторические сведения об организации</h3>
    <ul>
        <?php foreach ($diff as $date => $item) { ?>
            <li>
                <strong><?= date('d.m.Y', $date) ?></strong>
                <ul>
                    <?php foreach ($item as $changes) { ?>
                        <li><?= $changes['value'] ?></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
    </ul>
</div>