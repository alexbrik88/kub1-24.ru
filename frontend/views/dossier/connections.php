<?php

use common\components\zchb\Card;

/**
 * @var Card    $card
 * @var array[] $connections
 * @var int     $countByDirector
 * @var int     $countByFounder
 */
//Руководители и учредители в одном списке
$all = [];
foreach ($card->directors ?? [] as $item) {
    $all[$item['inn']] = $item['fl'];
}
foreach ($card->founders as $item) {
    if (isset($all[$item['inn']]) === false) {
        $all[$item['inn']] = $item['name'];
    }
}
?>
<script>setDossierTab('connections');</script>
<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">
            Связи (<?= count($connections) ?>)
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">Сведения об аффилированных организациях, полученные на основании
                анализа
                информации из ЕГРЮЛ
                <span class="tooltip-popup"><span>Единый государственный реестр юридических лиц ФНС РФ</span></span>
                и контактных данных.
            </div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content noyellow">

                <div class="tabs-line">
                    <div class="tabs-line__wrap">
                        <label>Все<br/>
                            <select class="all">
                                <option value="">(Все)</option>
                                <?php foreach ($all as $inn => $item) { ?>
                                    <option value="<?= $inn ?>"><?= $item ?></option>
                                <?php } ?>
                            </select>
                        </label>
                        <?php /* <a href="#" class="btn-link size-md" style="color:red">По адресу (4)</a> */ ?>
                        <label>По руководителю (<?= $countByDirector ?>)<br/>
                            <select class="director">
                                <option value="">(Все)</option>
                                <?php foreach ($card->directors ?? [] as $item) { ?>
                                    <option value="<?= $item['inn'] ?>"><?= $item['fl'] ?></option>
                                <?php } ?>
                            </select>
                        </label>
                        <label>По учредителю (<?= $countByFounder ?>)<br/>
                            <select class="founder">
                                <option value="">(Все)</option>
                                <?php foreach ($card->founders as $item) { ?>
                                    <option value="<?= $item['inn'] ?>"><?= $item['name'] ?></option>
                                <?php } ?>
                            </select>
                        </label>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <?php /*
                <div class="sort">
                    <span class="sort__label" style="color:red">Сортировка <span class="sort__label--opener js-sort--opener"></span></span>
                    <div class="sort-list">
                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=strength&amp;order=asc"
                               class="sort-item sort-item--default sort-item--down"><span style="color:red">Аффилированность</span></a>
                        </div>

                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=capital&amp;order=desc"
                               class="sort-item"><span style="color:red">Уставный капитал</span></a>
                        </div>

                        <div class="sort-item-wrap">
                            <a href="/connections/6583754?sort=date&amp;order=desc"
                               class="sort-item"><span style="color:red">Дата регистрации</span></a>
                        </div>
                    </div>
                </div>
                */ ?>
                <?php foreach ($connections as $item) {
                    $class = 'fl-all-' . $item['flINN'];
                    if ($item['byDirector'] === true) {
                        $class .= ' fl-director-' . $item['flINN'];
                    }
                    if ($item['byFounder'] === true) {
                        $class .= ' fl-founder-' . $item['flINN'];
                    }
                    if ($item['byDirector'] === true) {
                        $s = 'руководителю';
                    } else {
                        $s = '';
                    }
                    if ($item['byFounder'] === true) {
                        if ($s !== '') {
                            $s .= ', ';
                        }
                        $s .= 'учредителю';
                    }
                    ?>
                    <div class="company-item <?= $class ?>">
                        <div class="company-item__title"><?= $item['НаимЮЛСокр'] ?></div>
                        <div class="company-item__text"><span class="green-mark">По <?= $s ?></span></div>
                        <div class="company-item-info">
                            <dl>
                                <dd><i><?= $item['post'] ?></i> <?= $item['fio'] ?></dd>
                            </dl>
                        </div>
                        <address class="company-item__text"><span class="yellow-mark"><?= $item['Адрес'] ?></span>
                        </address>
                        <?php if ($item['byFounder'] === true) { ?>
                            <p style="margin-bottom:5px;">Учредитель <?= $item['fio'] ?></p>
                        <?php } ?>
                        <div class="company-item-info">
                            <?php if (isset($item['ОГРН']) === true) { ?>
                                <dl>
                                    <dt>ОГРН</dt>
                                    <dd><?= $item['ОГРН'] ?></dd>
                                </dl>
                            <?php } ?>
                            <?php if (isset($item['ИНН']) === true) { ?>
                                <dl>
                                    <dt>ИНН</dt>
                                    <dd><?= $item['ИНН'] ?></dd>
                                </dl>
                            <?php } ?>
                            <?php if (isset($item['registrationDate']) === true) { ?>
                                <dl>
                                    <dt>Дата регистрации</dt>
                                    <dd><?= /** @noinspection PhpUnhandledExceptionInspection */
                                        Yii::$app->formatter->asDate($item['registrationDate'], 'long') ?></dd>
                                </dl>
                            <?php } ?>
                            <?php /*
                            <dl>
                                <dt>Уставный капитал</dt>
                                <dd>10 000 руб.</dd>
                            </dl>
*/ ?>
                        </div>
                    </div>
                <?php } ?>

                <?php /*
                    <div class="company-item-info">
                        <dl>
                            <dt>Основной вид деятельности</dt>
                            <dd>69 Деятельность в области права и бухгалтерского учета</dd>
                        </dl>
                    </div>
*/ ?>
            </div>
        </div>

    </div>
</div>
<script>
    $('.tabs-line__wrap > label > select').change(function () {
        let items = $('.company-item');
        if (this.value !== '') {
            items.addClass('hidden');
            let className = '.fl-' + this.className + '-' + this.value;
            items = items.filter(className);
        }
        items.removeClass('hidden');
    });
</script>
