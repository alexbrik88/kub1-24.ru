<?php

use common\components\TextHelper;
use common\models\Company;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\subscribe\forms\PaymentForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;

/**
 * @var View                 $this
 * @var Company              $company
 * @var SubscribeTariffGroup $tariffGroup
 * @var array                $tariffList
 * @var bool                 $isAllowedDossier
 */
$model = new PaymentForm($company);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);
?>
    <div class="portlet box darkblue">
        <div class="portlet-title row-fluid">
            <div class="caption col-sm-12">Проверка контрагентов</div>
        </div>
        <div class="portlet-body accounts-list">

            <div class="t-subtitle">
                Вы уверены, что клиент вам заплатит, а поставщик не пропадет с вашими деньгами?<br/>
                Получите полную информацию о ваших контрагентах:
            </div>
            <div class="bg_man_in_lens">
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="t-tile">
                            <div class="title">
                                <div class="img img-width-1"><img src="/img/dossier/1.png"/></div>
                                <div class="txt"><b>ДАННЫЕ</b><br/>по контрагенту</div>
                            </div>
                            <div class="list">
                                <ul>
                                    <li>Полная информация о контрагенте</li>
                                    <li>Актуальные данные из ЕГРЮЛ</li>
                                    <li>Виды деятельности</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="t-tile">
                            <div class="title">
                                <div class="img img-width-2"><img src="/img/dossier/2.png"/></div>
                                <div class="txt"><b>МАССОВЫЙ</b><br/>директор и учредитель</div>
                            </div>
                            <div class="list">
                                <ul>
                                    <li>Данные о наличии руководителя в реестре массовых директоров</li>
                                    <li>Данные о наличии учредителя в реестре массовых учредителей</li>
                                    <li>Данные по компаниям, где совпадают учредители и генеральные директора</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="t-tile">
                            <div class="title">
                                <div class="img img-width-2"><img src="/img/dossier/3.png"/></div>
                                <div class="txt"><b>ФИНАНСОВЫЕ</b><br/>отчеты</div>
                            </div>
                            <div class="list">
                                <ul>
                                    <li>Выручка, прибыль, кол-во сотрудников</li>
                                    <li>Уплаченные налоги и взносы</li>
                                    <li>Бухгалтерский баланс и другие отчеты</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <div class="t-tile">
                            <div class="title">
                                <div class="img img-width-2"><img src="/img/dossier/4.png"/></div>
                                <div class="txt"><b>РИСК</b><br/>неисполнения обязательств</div>
                            </div>
                            <div class="list">
                                <ul>
                                    <li>Суды в роли ответчика, истца или третьего лица</li>
                                    <li>Судебные производства о банкротстве или ликвидации</li>
                                    <li>Исполнительные производства</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-sm-4 col-xs-6">
                    <div class="st-tile bg_man_1">
                        <div class="title">ДАННЫЕ</div>
                        <div class="subtitle">предоставляются из открытых источников</div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="st-tile bg_woman_2">
                        <div class="title">12 МЕСЯЦЕВ</div>
                        <div class="subtitle">срок действия оплаченных проверок<br/><span style="white-space: nowrap">Не использованные</span>
                            - обнулятся
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="st-tile bg_woman_3">
                        <div class="title">ДОСТУПНО</div>
                        <div class="subtitle">для всех компаний в вашем аккаунте</div>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <br/>
            <div class="row row-5ths">
                <?php foreach ($tariffList as $tariff) : ?>
                    <?php $hit = $tariff->id == 31; ?>
                    <div class="col-lg-5ths col-md-3 col-sm-4 col-xs-6">
                        <div class="tariff-block <?= $hit ? 'hit' : '' ?>">
                            <?php if ($hit) : ?>
                                <div class="hit">ХИТ</div>
                            <?php endif; ?>
                            <div class="header">
                                <?= \php_rutils\RUtils::numeral()->getPlural($tariff->tariff_limit, [
                                    'проверку',
                                    'проверки',
                                    'проверок',
                                ]) ?>
                            </div>
                            <div class="body">
                                <div class="price">
                                    <?= TextHelper::numberFormat($tariff->price); ?> ₽
                                </div>
                                <div class="average_price">
                                    <span class="small top-left">₽</span>
                                    <?= TextHelper::numberFormat(
                                        $tariff->tariff_limit ? round($tariff->price / $tariff->tariff_limit) : 0
                                    ) ?>
                                    <span class="small">/проверка</span>
                                </div>
                            </div>
                            <div class="footer">
                                <?= Html::button('Оплатить', [
                                    'class' => 'btn tooltip2-pay ' . ($hit ? 'darkblue-invert' : 'darkblue text-white'),
                                    'data-tooltip-content' => '#tooltip_pay_' . $tariff->id,
                                ]) ?>
                            </div>
                        </div>
                        <div class="hidden">
                            <div id="tooltip_pay_<?= $tariff->id ?>" class="tooltip_pay_type"
                                 style="display: inline-block;">
                                <div class="pay-tariff-wrap">
                                    <strong>
                                        Тариф:
                                    </strong>
                                    "<span class="pay-tariff-name"><?= $tariffGroup->name ?></span>"
                                </div>
                                <div class="pay-tariff-wrap">
                                    <strong>
                                        Период:
                                    </strong>
                                    <span class="pay-tariff-period"><?= $tariff->getReadableDuration() ?></span>
                                </div>
                                <div class="pay-tariff-wrap">
                                    <strong>
                                        Количество проверок:
                                    </strong>
                                    <span class="pay-tariff-period"><?= $tariff->tariff_limit ?></span>
                                </div>
                                <div style="font-size: 16px; margin: 10px 0;  text-align: center;">
                                    Способ оплаты
                                </div>
                                <div style="display: flex; height: 57px;">
                                    <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                                        'id' => 'tariff-pay-form-' . $tariff->id . '-' . PaymentType::TYPE_ONLINE,
                                        'class' => 'tariff-group-payment-form',
                                        'style' => 'margin-right: 1px;'
                                    ]) ?>
                                    <?= Html::activeHiddenInput($model, 'tariffId', [
                                        'value' => $tariff->id,
                                    ]) ?>
                                    <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                                        'value' => PaymentType::TYPE_ONLINE,
                                    ]) ?>
                                    <?= Html::submitButton('Картой', [
                                        'class' => 'btn darkblue text-white ladda-button',
                                        'data-style' => 'expand-down',
                                        'style' => 'width: 140px; height: auto; background: #45b6af;',
                                    ]) ?>
                                    <div class="hidden submit-response-content"></div>
                                    <?= Html::endForm() ?>
                                    <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                                        'id' => 'tariff-pay-form-' . $tariff->id . '-' . PaymentType::TYPE_INVOICE,
                                        'class' => 'tariff-group-payment-form',
                                        'style' => 'margin-left: 1px;'
                                    ]) ?>
                                    <?= Html::activeHiddenInput($model, 'tariffId', [
                                        'value' => $tariff->id,
                                    ]) ?>
                                    <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                                        'value' => PaymentType::TYPE_INVOICE,
                                    ]) ?>
                                    <?= Html::submitButton('Выставить счет', [
                                        'class' => 'btn darkblue text-white ladda-button',
                                        'data-style' => 'expand-down',
                                        'style' => 'width: 140px; height: auto; background: #ffaa24;',
                                    ]) ?>
                                    <div class="hidden submit-response-content"></div>
                                    <?= Html::endForm() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
        <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
            <div class="modal-content">
                <div class="modal-header" style="border-bottom: none; padding: 20px 10px 0px 0px">
                    <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;"
                            aria-hidden="true"></button>
                </div>
                <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                    <div class="text-center pad5">
                        <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                        <div>
                            <p>
                                Счет отправлен на
                                <br>
                                <strong id="send-to-email"></strong>.
                            </p>
                            <p>
                                Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                                <br>
                                <span>support@kub-24.ru</span>
                            </p>
                        </div>

                        <a class="btn" data-dismiss="modal" style="background-color: #45B6AF; color: #fff;"> ОК </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php if (empty($isAllowedDossier)): ?>
    <script>
        $(document).ready(function () {
            $('.nav-dossier .nav-link').attr('href', 'javascript:void(0)');
        });
    </script>
<?php endif; ?>

<?php $this->registerJs(<<<JS
    $(document).on('submit', 'form.tariff-group-payment-form', function (e) {
        e.preventDefault();
        var form = this;
        $.post(form.action, $(form).serialize(), function(data) {
            console.log(data);
            Ladda.stopAll();
            if (data.content) {
                $('.submit-response-content', form).first().html(data.content);
            } else if (data.done) {
                if (data.email && (data.type == '2' || data.type == '3')) {
                    $("#send-to-email").html(data.email);
                    $("#info-after-pay").modal("show");
                }
                if (data.link) {
                    var tab = window.open(data.link, '_blank');
                    if (tab) {
                        tab.focus();
                    } else {
                        alert('Please allow popups for this website');
                    }
                }
            }
            $('.tooltipstered').tooltipster('close');
        });
    });
JS
) ?>