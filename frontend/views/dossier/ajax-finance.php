<?php

use common\components\zchb\FinancialStatement;
use frontend\widgets\DissierChartWidget;
use yii\helpers\Url;

/**
 * @var string             $title        Название контрагента
 * @var FinancialStatement $fs
 * @var string             $revenue
 * @var float              $revenueChange
 * @var string             $profit
 * @var float              $profitChange
 * @var string             $tax
 * @var string             $fixedAssets
 * @var float              $fixedAssetsChange
 * @var int                $lastYear
 * @var int                $deduction
 * @var float              $arrearsTotal Сумма недоимок
 */
$tabLink = $this->params['tabLink'];
?>
<div class="tab tab_1 active">
    <?= DissierChartWidget::widgetFS($fs, $fs::INDICATOR_REVENUE) ?>
</div>
<div class="tab tab_2">
    <?= DissierChartWidget::widgetFS($fs, $fs::INDICATOR_PROFIT) ?>
</div>
<div class="tab tab_3">
    <?= DissierChartWidget::widgetFS($fs, $fs::INDICATOR_FIXED_ASSETS) ?>
</div>
<div class="finance-columns tab-control">
    <div class="finance-col">
        <div class="finance-link">
            <div class="tab-opener gtm_acc_1 active">Выручка</div>
        </div>
        <div class="finance-data"><?= $revenue ?></div>
        <div class="finance-changes lt<?php if (strip_tags($revenueChange) > 0) {
            echo ' reverted';
        } ?>">
            <span class="arrow"><?= $revenueChange > 0 ? '&uarr;' : ($revenueChange < 0 ? '&darr;' : '') ?></span>
            <?= $fs::amountFormat($revenueChange, false, '') ?>
        </div>
    </div>
    <div class="finance-col">
        <div class="finance-link">
            <div class="tab-opener gtm_acc_2">Прибыль</div>
        </div>
        <div class="finance-data"><?= $profit ?></div>
        <div class="finance-changes lt<?php if ($profitChange > 0) {
            echo ' reverted';
        } ?>">
            <span class="arrow"><?= $profitChange > 0 ? '&uarr;' : ($profitChange < 0 ? '&darr;' : '') ?></span>
            <?= $fs::amountFormat($profitChange, false, '') ?>
        </div>
    </div>
    <div class="finance-col">
        <div class="finance-link">
            <div class="tab-opener gtm_acc_3">Стоимость</div>
        </div>
        <div class="finance-data"><?= $fixedAssets ?></div>
        <div class="finance-changes lt">
            <?= $fixedAssetsChange > 0 ? '&uarr;' : ($fixedAssetsChange < 0 ? '&darr;' : '') ?>
            <?= $fs::amountFormat($fixedAssetsChange, false, '') ?>
        </div>
    </div>
</div>
<script>
    jQuery('.tab-opener').click(function () {
        jQuery('.tab-opener,.tab').removeClass('active');
        jQuery('.tab_' + this.className.substr(19)).addClass('active');
        jQuery(this).addClass('active');
    });
</script>
<p class="tile-item__text">Данные по финансовым показателям <?= $title ?> приведены на основании
    <span class="bolder"><a href="<?= Url::toRoute($tabLink('accounting')) ?>" class="gtm_acc_more" rel="nofollow">бухгалтерской отчетности за <?= implode(',',
                $fs->yearList(true, 2110, false)) ?> годы</a></span>.
</p>
<p class="company-name nabors" style="font-weight:bold;">Согласно сведениям ФНС за <?= $lastYear ?> год:</p>
<div class="finance-columns">
    <div class="finance-col">
        <div class="finance-title">Налоги</div>
        <div class="finance-data"><?= $tax ?></div>
    </div>
    <div class="finance-col">
        <div class="finance-title">Взносы</div>
        <div class="finance-data"><?= $deduction ?></div>
    </div>
    <div class="finance-col">
        <div class="finance-title">Недоимки</div>
        <div class="finance-data"><?= $arrearsTotal ?></div>
    </div>
</div>
<p class="tile-item__text">
    <span class="bolder"><a class="gtm_fs_more" href="<?= Url::toRoute($tabLink('finance')) ?>"
                            rel="nofollow">Смотреть подробные сведения</a></span>
    об уплаченных организацией в 2018 году налогах и сборах (по каждому налогу и сбору).
</p>