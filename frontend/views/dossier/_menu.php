<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\View;

/**
 * @var View $this
 * @var bool $clickable
 */
$tabLink = $this->params['tabLink'];

if (isset($clickable) === false) {
    $clickable = true;
}
$tab = $this->params['currentTab'];
NavBar::begin([
    'brandUrl' => null,
    'options' => [
        'class' => 'navbar-report navbar-default navbar-dossier',
    ],
    'brandOptions' => [
        'style' => 'margin-left: 0;',
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
        'style' => 'padding: 0;',
    ],
]);
/** @noinspection PhpUnhandledExceptionInspection */
echo Nav::widget([
    'items' => [
        [
            'label' => 'Главная',
            'url' => $clickable === true ? $tabLink('index') : null,
            'active' => $tab == 'index',
            'options' => ['class' => 'dossier-tab-index'],
        ],
        [
            'label' => 'Учредители',
            'url' => $clickable === true ? $tabLink('founders') : null,
            'active' => $tab == 'founders',
            'options' => ['class' => 'dossier-tab-founders'],
        ],
        [
            'label' => 'Связи',
            'url' => $clickable === true ? $tabLink('connections') : null,
            'active' => $tab == 'connections',
            'options' => ['class' => 'dossier-tab-connections'],
        ],
        [
            'label' => 'Надежность',
            'url' => $clickable === true ? $tabLink('reliability') : null,
            'active' => $tab == 'reliability',
            'options' => ['class' => 'dossier-tab-reliability'],
        ],
        [
            'label' => 'Финансы',
            'url' => $clickable === true ? $tabLink('finance') : null,
            'active' => $tab == 'finance',
            'options' => ['class' => 'dossier-tab-finance'],
        ],
        [
            'label' => 'Суды',
            'url' => $clickable === true ? $tabLink('arbitr') : null,
            'active' => $tab == 'arbitr',
            'options' => ['class' => 'dossier-tab-arbitr'],
        ],
        [
            'label' => 'Долги',
            'url' => $clickable === true ? $tabLink('debt') : null,
            'active' => $tab == 'debt',
            'options' => ['class' => 'dossier-tab-debt'],
        ],
        [
            'label' => 'Виды деят.',
            'url' => $clickable === true ? $tabLink('okved') : null,
            'active' => $tab == 'okved',
            'options' => ['class' => 'dossier-tab-okved'],
        ],
        [
            'label' => 'Отчетность',
            'url' => $clickable === true ? $tabLink('accounting') : null,
            'active' => $tab == 'accounting',
            'options' => ['class' => 'dossier-tab-accounting'],
        ],
    ],
    'options' => ['class' => 'nav-dossier navbar-nav navbar-right'],
]);
NavBar::end();