<?php

use common\components\zchb\ReliabilityHelper;

/**
 * @var ReliabilityHelper $reliability
 */
?>
<script>setDossierTab('reliability');</script>
<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">
            Анализ надёжности организации
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="content-frame__header content-frame__header--rely">
            <div class="rely-rating-badge">
                Уровень надёжности:
                <span style="background-color:<?= $reliability->color() ?>"><?= $reliability->reliabilityString() ?></span>
            </div>
        </div>
        <p><br /></p>
        <div class="tabs-line">
            <div class="tabs-line__wrap">
                <?php /* <span class="btn-link size-md active">Все</span> */ ?>
                <a class="btn-link size-md rely-negative">Отрицательные
                    (<?= $reliability->negativeCount() ?>)</a>
                <a href="#attention" class="btn-link size-md rely-warning popup">Требуют внимания
                    (<?= $reliability->attentionCount() ?>)</a>
                <a href="#positive" class="btn-link size-md rely-positive popup">Положительные
                    (<?= $reliability->positiveCount() ?>)</a>
            </div>
        </div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Отрицательные факты</h2></div>
            </div>
            <?php foreach ($reliability->negative() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-negative"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div id="attention"></div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Требующие внимания факты</h2></div>
            </div>
            <?php foreach ($reliability->attention() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-warning"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div id="positive"></div>
        <div class="requisites-list">
            <div class="content-frame__header with-margin-top">
                <div class="tile-item__title"><h2>Положительные факты</h2></div>
            </div>
            <?php foreach ($reliability->positive() as $item) { ?>
                <div class="requisites-item">
                    <div class="requisites-item__name">
                        <div class="rely-name"><?= $item['title'] ?></div>
                        <div class="rely-short-positive"><?= $item['value'] ?></div>
                    </div>
                    <div class="requisites-item__value"><?= $item['description'] ?></div>
                </div>
            <?php } ?>
        </div>
        <div class="disclaimer">
            Значение рейтинга формируется в результате анализа открытых общедоступных данных РФ и фактов хозяйственной
            деятельности организации с использованием скоринговой модели.
            Данная оценка является мнением сервиса &laquo;КУБ&raquo; и не дает каких-либо гарантий или заверений третьим лицам,
            а также не является рекомендацией для принятия коммерческих или иных решений. Значение рейтинга
            автоматически пересчитывается при изменении фактов хозяйственной деятельности и поступлении новой информации
            из источников открытых данных.
        </div>
    </div>
</div>