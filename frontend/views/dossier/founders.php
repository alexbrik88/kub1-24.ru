<?php

use common\components\zchb\Card;
use yii\helpers\Url;

/**
 * @var int  $modelId
 * @var Card $card
 *
 */
?>
<script>setDossierTab('founders');</script>
<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">
            Учредители (<?= $card->founderCount() ?>)
        </div>
    </div>
    <div class="portlet-body accounts-list">

        <div class="content-frame__header">
            <div class="content-frame__description">
                Согласно данным ЕГРЮЛ <span class="tooltip-popup"><span>Единый государственный реестр юридических лиц ФНС РФ</span></span><br/>
                учредителем организации является <?= $card::plural($card->founderCount(), 'физическое лицо',
                    'физических лица', 'физических лиц') ?>.
            </div>
            <div class="content-frame__description">Уставный капитал: <?= number_format($card->СумКап, 0, '.', ' ') ?>
                руб.
            </div>
        </div>

        <div class="main-wrap">
            <div class="main-wrap__content">
                <?php foreach ($card->founders as $item) { ?>
                    <div class="company-item">
                        <div class="company-item__title">
                            <?php if ($item['isFL'] === true) { ?>
                                <a href="<?= Url::toRoute([
                                    'ajax-fl-card',
                                    'id' => $item['inn'],
                                    'modelId' => $modelId
                                ]) ?>" onclick="return rightPanel.fromURL(this.href);" class="popup"><?= $item['name'] ?></a>
                            <?php } else { ?>
                                <?= $item['name'] ?>
                            <?php } ?>
                        </div>
                        <div class="company-item-info">
                            <dl>
                                <dt>Доля:</dt>
                                <dd>
                                    <?php if ($item['dol_abs'] > 0 && $card->СумКап > 0){ ?>
                                    <?= number_format($item['dol_abs'], 0, '.', ' ') ?> руб. <span
                                            class="green-mark">(<?= round($item['dol_abs'] / $card->СумКап * 100) ?>%)</span>
                                </dd>
                                <?php } else { ?>
                                    нет информации
                                <?php } ?>
                            </dl>
                        </div>
                        <div class="company-item-info">
                            <dl>
                                <?php if ($item['inn']) { ?>
                                    <dt>ИНН:</dt>
                                    <dd><?= $item['inn'] ?></dd>
                                <?php } ?>
                                <?php if ($item['ogrn']) { ?>
                                    <dt>ОГРН:</dt>
                                    <dd><?= $item['ogrm'] ?></dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</div>