<?php

use frontend\assets\InterfaceCustomAsset;
use frontend\assets\ContractorDossierAsset;
use yii\web\View;

/**
 * @var View    $this
 * @var string  $dossierTab
 * @var mixed[] $dossierData
 *
 */
$this->title = 'Досье ' . $this->title;
ContractorDossierAsset::register($this);
InterfaceCustomAsset::register($this);
echo $this->render('_menu');
?>
<script>
    function setDossierTab(tab) {
        $('.navbar-dossier li,.nav-dossier li a').removeClass('active');
        $('.dossier-tab-' + tab + ',.dossier-tab-' + tab + ' a').addClass('active');
    }

    function linkReplace(container) {
        if (container === undefined) container = document.getElementById('dossierContainer');
        $('a:not(.popup)', container).each(function () {
            if (this.href) this.onclick = function () {

                return loadDossierTab(this.href);
            };
        });
        return false;
    }

    if (typeof loadDossierTab == 'undefined') {
        function loadDossierTab(link) {
            $('#dossierContainer').html('<div class="loader"><span></span><span></span><span></span></div>').load(link, function () {
                linkReplace();
                try {
                    history.pushState(null, null, link);
                } catch (e) {
                }
            });
            return false;
        }
    }

</script>
<div id="dossierContainer">
    <script>
        $('.navbar-dossier a,.nav-dossier a').click(function () {
            return loadDossierTab(this.href);
        });
    </script>
    <?= $this->render($dossierTab, $dossierData) ?>
    <script>linkReplace();</script>
</div>
