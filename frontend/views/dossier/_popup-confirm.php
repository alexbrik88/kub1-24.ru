<?php

// use frontend\components\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var string            $message Текст сообщения во всплывающем диалоге
 * @var string            $link    Ссылка, на которую нужно перейти в случае подтверждения
 * @var array|string|null $button
 * @var bool              $newTab  Открывать в новой вкладке
 */

if (isset($button) === true) {
    if (is_array($button) === true) {
        $tag = ArrayHelper::remove($button, 'tag', 'span');
        $label = ArrayHelper::remove($button, 'label', '');
        if ($tag === 'button' && !isset($button['type'])) {
            $button['type'] = 'button';
        }
        $button = array_merge([
            'data-toggle' => 'modal',
            'data-target' => '#popup-confirm',
        ], $button);

        $button = Html::tag($tag, $label, $button);
    }
} else {
    $button = '';
}
if (isset($message) === false || !$message) {
    $message = 'Вы уверены, что хотите проверить данного контрагента?';
}
if (isset($newTab) === false) {
    $newTab = substr(Url::current(), 0, 16) !== '/contractor/view';
}
$options = [
    'class' => 'btn darkblue pull-right ',
    'onclick' => '$("#popup-confirm button[data-dismiss]").click();'
];
if ($newTab) {
    $options['target'] = '_blank';
}
?>
<?= $button ?>
<div id="popup-confirm" class="confirm-modal fade modal" role="dialog"
     tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row"><?= $message ?></div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('Да', $link, $options); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>