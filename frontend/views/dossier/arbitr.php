<?php

use common\components\zchb\CourtArbitration;
use common\components\zchb\CourtArbitrationCard;
use yii\helpers\Url;

/**
 * @var int                   $modelId
 * @var CourtArbitration|null $court
 *
 */
?>
<script>setDossierTab('arbitr');</script>
<?php if ($court === null) {
    echo '<p>К сожалению информации о судебных делах нет</p>';
    return;
} ?>
<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">Судебные дела (<?= $court->count() ?>)</div>
    </div>
    <div class="portlet-body accounts-list">
        <?php if ($court->count() === 0) { ?>
            <p><b>Судебных дел нет</b></p>
        <?php } else { ?>
            <div class="content-frame__header">
                <div class="content-frame__description" style="display:none;">
                    Сведения об участии организации в судебных процессах: <span></span>.
                </div>
            </div>
            <div class="main-wrap">
                <div class="main-wrap__content">

                    <div class="tabs-line">
                        <div class="tabs-line__wrap">
                            <label>Роль<br>
                                <select onchange="arbitr({'role': this.value});">
                                    <option value="">(все)</option>
                                    <option value="<?= $court::ROLE_APPLICANT ?>">истец</option>
                                    <option value="<?= $court::ROLE_DEFENDANT ?>">ответчик</option>
                                    <option value="<?= $court::ROLE_THIRD ?>">третья сторона</option>
                                    <option value="<?= $court::ROLE_OTHER ?>">иное</option>
                                </select>
                            </label>
                            <label>Статус<br>
                                <select onchange="arbitr({'status': this.value});">
                                    <option value="">(все)</option>
                                    <option value="<?= CourtArbitrationCard::STATUS_PROCESS ?>">рассматривается</option>
                                    <option value="<?= CourtArbitrationCard::STATUS_FINISH ?>">завершено</option>
                                    <option value="<?= CourtArbitrationCard::STATUS_APPEAL ?>">апелляция</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div style="clear:both;"></div>

                    <div id="arbitr">
                        <p></p>
                        <p>Подготовка отчёта...</p>
                    </div>
                </div>
            </div>
            <script>
                var arbitr = function (link) {
                    let container = $('#arbitr');
                    let config = {'date': null, 'status': null, 'role': null, 'sort': null};
                    return function (cfg) {
                        container.html('<div class="loader"><span></span><span></span><span></span></div>');
                        config = $.extend(config, cfg);
                        let lnk = link;
                        if (config.role) lnk += '&role=' + config.role;
                        if (config.status) lnk += '&status=' + config.status;
                        if (config.sort) lnk += '&sort=' + config.sort;
                        $('#arbitr').load(lnk);
                    };
                }('<?=Url::toRoute([
                    'ajax-arbitr',
                    'ogrn' => $court->ogrn,
                    'inn' => $court->inn,
                    'modelId' => $modelId
                ])?>');
                arbitr();
            </script>
        <?php } ?>
    </div>
</div>