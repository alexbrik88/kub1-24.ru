<?php

use frontend\assets\InterfaceCustomAsset;

InterfaceCustomAsset::register($this);
?>
<div class="ajax-exception">
    <h4 style="font-size: 21px; font-weight: bold">Что-то пошло не так...</h4>
    <img src="/img/bg/dossier-exception.png"/>
    <h4 style="font-size: 21px; font-weight: bold; margin:20px 0;">Мы уже работаем над этим. Напишите в техподдержку.</h4>
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <label style="color:#999; font-size: 13px">Телефон</label>
            <div>8 800 500 5436</div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <label style="color:#999; font-size: 13px">E-mail</label>
            <div>support@kub-24.ru</div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <label style="color:#999; font-size: 13px">Чат</label>
            <div>Кнопка в правом нижнем углу</div>
        </div>
    </div>
</div>
<style>
    .ajax-exception img {
        max-width: 56%;
        display: block;
        margin: auto;
    }
</style>