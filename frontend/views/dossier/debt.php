<?php

use yii\helpers\Url;

/**
 * @var int $modelId
 */
?>
<script>setDossierTab('debt');</script>
<div class="portlet box darkblue">
    <div class="portlet-title row-fluid">
        <div class="caption col-sm-12">Исполнительные производства</div>
    </div>
    <div class="portlet-body accounts-list" id="debt">
        <div class="loader"><span></span><span></span><span></span></div>
        <script>
            $('#debt').load('<?=Url::toRoute([
                'ajax-debt',
                'modelId' => $modelId
            ])?>');
        </script>
    </div>
</div>