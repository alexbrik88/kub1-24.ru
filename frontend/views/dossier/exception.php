<div class="row">
    <div class="col-sm-12">
        <div class="portlet box darkblue">
            <div class="portlet-title row-fluid">
                <div class="caption col-sm-12 contactor-name">
                    <span><?= $title ?></span>
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <div class="company-info" id="anketa">
                    <div class="row">
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <h4 style="font-size: 21px; font-weight: bold">Что-то пошло не так...</h4>
                            <img src="/img/bg/dossier-exception.png"/>
                            <h4 style="font-size: 21px; font-weight: bold; margin:20px 0;">Мы уже работаем над этим. Напишите в техподдержку.</h4>
                            <div class="row">
                                <div class="col-sm-4 col-xs-12">
                                    <label style="color:#999; font-size: 13px">Телефон</label>
                                    <div>8 800 500 5436</div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label style="color:#999; font-size: 13px">E-mail</label>
                                    <div>support@kub-24.ru</div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label style="color:#999; font-size: 13px">Чат</label>
                                    <div>Кнопка в правом нижнем углу</div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>