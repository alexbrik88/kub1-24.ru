<?php

use frontend\models\FsspTask;
use yii\helpers\Url;

/**
 * @var int      $modelId
 * @var FsspTask $task
 */
if ($task === null || $task->status !== $task::STATUS_SUCCESS) { ?>
    <?php if ($task === null) { ?>
        <p>Информации о долгах нет</p>
    <?php } else { ?>
        <p>Идёт сбор данных, отчёт будет доступен в течении нескольких минут...</p>
        <script>
            setTimeout(function () {
                let debt = $('#debt');
                debt.html('<div class="loader"><span></span><span></span><span></span></div>');
                debt.load('<?=Url::toRoute([
                    'ajax-debt',
                    'modelId' => $modelId
                ])?>');
            }, 6000);
        </script>
    <?php } ?>
    <?php return; ?>
<?php } ?>

<?php if (count($task->items) === 0) { ?>
    <p>Исполнительных производств не найдено.</p>
<?php } else {
    foreach ($task->items as $item) { ?>
        <dl class="debt">
            <dt>Статус:</dt>
            <dd><?= $item->end_date === null ? 'не завершено' : 'завершено ' . date('d.m.Y',
                        strtotime($item->end_date)) . ' на основании ' . $item->endBaseString() ?></dd>
            <dt>Должник:</dt>
            <dd><?= $item->name ?></dd>
            <dt>Предмет исполнения:</dt>
            <dd><?= $item->subject ?></dd>
            <dt>Исполнительный документ:</dt>
            <dd><?= $item->production_number ?> от <?= date('d.m.Y',
                    strtotime($item->production_date)) ?></dd>
            <dt>Отделение ФССП:</dt>
            <dd><?= $item->department ?></dd>
            <dt>Судебный пристав:</dt>
            <dd><?= str_replace('<br>', '; ', $item->bailiff) ?></dd>
            <dd><?= $item->detail ?></dd>
        </dl>
    <?php }
} ?>