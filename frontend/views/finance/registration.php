<?php

use common\models\company\CompanyType;
use frontend\models\RegistrationForm;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \frontend\models\RegistrationForm $model */

$this->title = 'Регистрация';

?>
<div class="registration-view">
    <h4 style="text-transform: uppercase;"><?= $this->title ?></h4>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'finance-registration-form',
        'method' => 'POST',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'name')->textInput(); ?>
    <?= $form->field($model, 'email')->textInput(); ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>
    <?= $form->field($model, 'promoCode')->textInput(); ?>

    <?= \yii\helpers\Html::submitButton('Попробовать бесплатно', [
        'class' => 'btn btn-primary w-100',
    ]); ?>

    <div class="mt-4">
        Регистрируясь в системе, Вы принимаете условия
        <a href="#" target="_blank">лицензионного соглашения</a>
    </div>

    <?php $form->end(); ?>
</div>
