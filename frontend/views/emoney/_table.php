<?php

use common\components\grid\GridView;
use frontend\models\EmoneySearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmoneySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],

    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => "{items}\n{pager}",

    'columns' => [
        [
            'attribute' => 'name',
            'headerOptions' => [
            ],
        ],
        [
            'attribute' => 'is_accounting',
            'format' => 'boolean',
        ],
        [
            'label' => 'Тип',
            'value' => 'typeLabel',
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'controller' => '/emoney',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $data) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'class' => 'ajax-modal-btn',
                        'data-pjax' => '0',
                        'data-title' => 'Изменить E-money',
                        'title' => Yii::t('yii', 'Изменить'),
                        'aria-label' => Yii::t('yii', 'Изменить'),
                    ]);
                },
                'delete' => function ($url, $data) {
                    if (!$data->is_main && $data->canDelete()) {
                        return BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                'class' => '',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить E-money?',
                        ]);
                    }

                    return '';
                },
            ],
            'visible' => ArrayHelper::getValue($_params_, 'actionVisible', false),
        ],
    ],
]); ?>
