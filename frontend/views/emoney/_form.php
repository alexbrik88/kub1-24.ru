<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Emoney */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emoney-form">
    <?php $form = ActiveForm::begin([
        'id' => 'emoney-form',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => '',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_accounting', [
        'labelOptions' => [
            'class' => 'col-sm-4',
        ],
    ])->checkbox([], false) ?>

    <?php if (!$model->is_main) : ?>
        <?= $form->field($model, 'is_main', [
            'labelOptions' => [
                'class' => 'col-sm-4',
            ],
        ])->checkbox([], false) ?>

        <?php if (!$model->isNewRecord) : ?>
            <?= $form->field($model, 'is_closed', [
                'labelOptions' => [
                    'class' => 'col-sm-4',
                ],
            ])->checkbox([], false) ?>
        <?php endif ?>
    <?php endif ?>

    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue text-white ladda-button',
                'data-style' => 'expand-right',
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white pull-right',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Html::script('
$("#emoney-form input:checkbox:not(.md-check)").uniform();
', [
    'type' => 'text/javascript',
]); ?>
