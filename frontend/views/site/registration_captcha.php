<?php

use himiklab\yii2\recaptcha\ReCaptcha2;
use himiklab\yii2\recaptcha\ReCaptcha3;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model frontend\models\FinanceRegistrationForm */

$this->title = 'Подтвердите, что вы не робот';
?>

<h4><?= Html::encode($this->title) ?></h4>
<?= Html::beginForm() ?>
    <?= Html::activeHiddenInput($model, 'email') ?>
    <?= Html::activeHiddenInput($model, 'companyType') ?>
    <?= Html::activeHiddenInput($model, 'taxationTypeOsno') ?>
    <?= Html::activeHiddenInput($model, 'taxationTypeUsn') ?>
    <?= Html::activeHiddenInput($model, 'taxationTypeEnvd') ?>
    <?= Html::activeHiddenInput($model, 'taxationTypePsn') ?>
    <?= Html::activeHiddenInput($model, 'registrationPageTypeId') ?>
    <?= Html::activeHiddenInput($model, 'promoCode') ?>
    <?= Html::activeHiddenInput($model, 'googleAnalyticsId') ?>
    <?= Html::activeHiddenInput($model, 'utm') ?>
    <?= Html::activeHiddenInput($model, 'source') ?>
    <?= Html::activeHiddenInput($model, 'checkrules') ?>
    <?= Html::activeHiddenInput($model, 'phone') ?>
    <?php if ($model->password) : ?>
        <?= Html::activeHiddenInput($model, 'password') ?>
    <?php endif ?>

    <div class="my-5">
        <?= ReCaptcha2::widget([
            'model' => $model,
            'attribute' => 'reCaptcha',
        ]) ?>
        <?php if ($error = $model->getFirstError('reCaptcha')) : ?>
            <div class="mt-4 text-danger">
                <?= $error ?>
            </div>
        <?php endif ?>
    </div>

    <?= Html::submitButton('Подтвердить', [
        'class' => 'btn btn-primary ladda-button w-100',
    ]); ?>
<?= Html::endForm() ?>
