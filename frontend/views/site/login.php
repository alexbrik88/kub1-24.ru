<?php

use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Войти';
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order login-page';
$bankClassArray = Banking::$registerModelClassArray;
?>
<?= \frontend\widgets\Alert::widget(); ?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'username') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div style="color:#999;margin:1em 0">
            Если Вы забыли пароль, то Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
        </div>
        <div class="form-group">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div>
    <?php if ($bankClassArray) : ?>
        <div class="form-group row text-bold">
            <label class="col-sm-12 text-bold">Или войдите с помощью:</label>
            <?php foreach ($bankClassArray as $class) : ?>
                <div class="col-sm-12">
                    <?= Html::a($class::NAME, ["/cash/banking/{$class::$alias}/default/login"], [
                        'class' => 'bank-' . $class::$alias,
                        'data-tooltip-content' => '#' . $class::$alias . '-choose-token'
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="form-group row text-bold">
        <div class="col-sm-12">
            Войти с помощью аккаунта
            <?= Html::a('МТС', ['/auth/login', 'authclient' => 'mts']) ?>
        </div>
    </div>
</div>

<!-- Sberbank Auth -->
<div style="display: none">
    <div id="sberbank-choose-token" class="update-attribute-tooltip-content" style="width: 175px;text-align:center">
        <div class="mar-b-5 text-bold text-center">У вас есть токен?</div>
        <div class="btn-group" style="border: 1px solid #ddd;">
            <?= Html::a('Да', ["/cash/banking/{$class::$alias}/default/login", 'by_token' => 1], [
                'class' => 'btn sberbank-token',
                'style' => 'width: 49px; color: #fff; background-color: #3379b5;'
            ]) ?>
            <?= Html::a('Нет', ["/cash/banking/{$class::$alias}/default/login", 'by_token' => 0], [
                'class' => 'btn sberbank-web',
                'style' => 'width: 49px; color: #333; background-color: #fff;'
            ]) ?>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(".bank-sberbank").attr('href', 'javascript:;').tooltipster({
                theme: ["tooltipster-kub"],
                contentCloning: true,
                trigger: "click",
                side: "top"
            });
        });
    </script>
</div>