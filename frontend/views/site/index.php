<?php
/* @var $this yii\web\View */

use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use Intervention\Image\ImageManagerStatic as Image;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\TaxRobotHelper;
use common\models\company\CompanyType;

/* @var $inInvoiceStatistics Array */
/* @var $pieData Array */
/* @var $cashStatisticInfo Array */
/* @var $notifications common\models\notification\Notification[] */
/* @var $currency common\models\currency\Currency[] */
/* @var $activitiesSearchModel \frontend\models\log\LogSearch */
/* @var $activitiesDataProvider \yii\data\ActiveDataProvider */

$this->title = 'Личный кабинет';
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order home-page';
$invoiceTextVariants = ['счёт', 'счёта', 'счетов',];

$logoWidth = Company::$imageDataArray['logoImage']['width'];
$logoHeight = Company::$imageDataArray['logoImage']['height'];
$ratio = $logoHeight / $logoWidth * 100;
$company = Yii::$app->user->identity->company;
?>

    <div class="row" style="margin-bottom: 20px;">
        <div class="home-head col-sm-9">
            <h3 class="page-title company-name"><?= $company->getTitle(true); ?></h3>
            <?= Html::a('<i class="fa fa-gear"></i>', Url::to(['/company/index']), [
                'id' => 'update-company_profile',
            ]); ?>
            <?php if ($company->getCanTaxModule() && Yii::$app->user->can(\frontend\rbac\UserRole::ROLE_CHIEF)): ?>
                <?php $taxRobot = new TaxRobotHelper($company); ?>
                <div class="taxrobot-top-link">
                    <?= $taxRobot->getTopLink('quarter') ?: $taxRobot->getTopLink('year'); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-sm-3">
            <?php if (is_file($path = $company->getImage('logoImage'))) : ?>
                <div
                        style="position: relative; padding-top: <?= $ratio ?>%; text-align: center;">
                    <div
                            style="position: absolute; top: 0; bottom: 0; left: 0; right: 0;">
                        <?= EasyThumbnailImage::thumbnailImg($path, $logoWidth, $logoHeight, EasyThumbnailImage::THUMBNAIL_INSET, [
                            'style' => 'max-width: 100%; max-height: 100%;'
                        ]); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

<?php if (Yii::$app->user->can(frontend\rbac\permissions\Service::HOME_LOGO)): ?>
    <div class="row">
        <div class="col-md-9">
            <div class="portlet box text-right">
                <?php if ($company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                    <a href="#"
                       onclick="window.location.href='<?= Url::toRoute(['documents/invoice/create', 'type' => 2]); ?>'"
                       class="btn yellow">СОЗДАТЬ СЧЁТ</a>
                <?php else : ?>
                    <a href="#" class="btn yellow action-is-limited">СОЗДАТЬ
                        СЧЁТ</a>
                <?php endif ?>
            </div>
        </div>
        <div class="col-md-3 home-button">
            <?= frontend\widgets\RangeButtonWidget::widget(); ?>
        </div>
    </div>
<?php endif; ?>

    <div class="row home-widgets">
        <div class="col-md-9">
            <?php
            // COMPANY_53146
            if ((YII_ENV_DEV || $company->id == 53146) && Yii::$app->request->get('new_graphs')) {
                echo $this->render('_indexPartials/_new_graphs.php', ['company' => $company]);
            }
            ?>
            <?php
            if (Yii::$app->user->can(permissions\Service::HOME_PROCEEDS)) {
                echo $this->render('_indexPartials/_proceeds');
            }

            echo $this->render('_indexPartials/_debts');

            echo $this->render('_indexPartials/_employee_rating');

            Pjax::begin([
                'id' => 'period-pjax-container',
                'enableReplaceState' => false,
                'enablePushState' => false,
                'linkSelector' => false,
            ]);

            if (Yii::$app->user->can(permissions\Service::HOME_FLOWS)) {
                echo $this->render('_indexPartials/_flows', ['company' => $company]);
            }

            echo $this->render('_indexPartials/_debts_seller');

            if (Yii::$app->user->can(permissions\Service::HOME_EXPENSES)) {
                echo $this->render('_indexPartials/_expenses', ['inInvoiceStatistics' => $inInvoiceStatistics,
                    'invoiceTextVariants' => $invoiceTextVariants,]);
            }

            if (Yii::$app->user->can(permissions\Service::HOME_RECEIPTS)) {
                echo $this->render('_indexPartials/_receipts', ['company' => $company]);
            }

            Pjax::end();

            /*if (Yii::$app->user->can(permissions\Service::HOME_FINANCES)) {
                echo $this->render('_indexPartials/_finances');
            }*/

            if (Yii::$app->user->can(permissions\Service::HOME_ACTIVITIES)) {
                echo $this->render('_indexPartials/_activities', ['activitiesSearchModel' => $activitiesSearchModel,
                    'activitiesDataProvider' => $activitiesDataProvider,]);
            }
            ?>
        </div>
        <div class="col-md-3">
            <?php
            if (Yii::$app->user->can(permissions\Service::HOME_CASH)) {
                echo $this->render('_indexPartials/_cash', ['cashStatisticInfo' => $cashStatisticInfo,]);
            }

            if (Yii::$app->user->can(permissions\Service::HOME_EXCHANGE_RATE)) {
                if (!empty($currency)) {
                    echo $this->render('_indexPartials/_exchangeRate', ['currency' => $currency,]);
                }
            }

            if (!empty($notifications)) {
                echo $this->render('_indexPartials/_taxCalendar', ['notifications' => $notifications,]);
            }
            ?>
        </div>
    </div>

<?php
$periodRanges = StatisticPeriod::getRangesJs();
$periodStart = StatisticPeriod::getStartEndDateJs('from');
$periodEnd = StatisticPeriod::getStartEndDateJs('to');

$this->registerJs(<<<JS
$('.details-sum').each(function() {
    $(this).numerator({
        easing: 'linear', // easing options.
        duration: 3000, // the length of the animation.
        delimiter: ' ',
        rounding: 2, // decimal places.
        toValue: $(this).attr('data-value')/100
    });
});

$('.statistic-period-btn').daterangepicker({
    format: 'DD.MM.YYYY',
    ranges: {{$periodRanges}},
    startDate: '{$periodStart}',
    endDate: '{$periodEnd}',
    locale: {
        daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        firstDay: 1
    }
});

$(document).on("change", ".period-step-select", function () {
    $.pjax.reload("#period-pjax-container", {data: {
        "step": $(this).val(),
        "expenses_type": $("#expenses_type-select").val(),
    }, type: "post"});
});

$(document).on('pjax:success', '#period-pjax-container', function() {
    $('.statistic-period-btn', this).daterangepicker({
        format: 'DD.MM.YYYY',
        ranges: {{$periodRanges}},
        startDate: '{$periodStart}',
        endDate: '{$periodEnd}',
        locale: {
            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            firstDay: 1
        }
    });
})
JS
    , $this::POS_READY
);