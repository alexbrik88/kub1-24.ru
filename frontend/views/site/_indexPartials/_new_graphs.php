
<div class="portlet box darkblue widget-result widget-home">
    <div class="portlet-title">
        <div class="caption" style="">

        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
        </div>
        <div class="actions p-t-5 p-b-0">
            <div class="btn-group widget-home-popup">
            </div>
        </div>
    </div>
    <div class="portlet-body">

        <?= \miloschuman\highcharts\Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],

            'options' => [
                'chart' => [
                    'type' => 'column',
                ],
                'legend' => [
                    'layout' => 'vertical',
                    'align' => 'right',
                    'verticalAlign' => 'top',
                    'x' => -40,
                    'y' => 60,
                    'floating' => true,
                    'borderWidth' => 0,
                    'backgroundColor' => '#FFFFFF',
                    'shadow' => true
                ],
                'tooltip' => [
                    'shared' => true
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'title' => ['text' => 'Расходы'],
                'yAxis' => [
                    ['min' => 0, 'index' => 0, 'title' => ''],
                ],
                'xAxis' => [
                    ['categories' => ['янв', 'фев', 'март', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек']],

                ],
                'series' => [
                    [
                        'name' => 'Факт',
                        'data' => [15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000],
                        'color' => 'rgba(255,213,139,1)'
                    ],
                    [
                        'name' => 'План',
                        'data' => array_reverse([15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000, 15000, 20000, 18000]),
                        'marker' => [
                            'symbol' => 'c-rect',
                            'lineWidth' => 3,
                            'lineColor' =>  'rgba(50,50,50,1)',
                            'radius' => 10
                        ],
                        'type' => 'scatter',
                    ]
                ],
                'plotOptions' => [
                    'scatter' => [
                        'tooltip' => [
                            'crosshairs' => true,
                            'headerFormat' => '{point.x}',
                            'pointFormat' => '<br /><b>План: {point.y} Р</b>',
                        ]
                    ],
                    'series' => [
                        'tooltip' => [
                            'crosshairs' => true,
                            'headerFormat' => '{point.x}',
                            'pointFormat' => '<br /><b>Факт: {point.y} Р</b>',
                        ]
                    ]
                ]
            ],
        ]); ?>

    <?= \miloschuman\highcharts\Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],

        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true
            ],
            'legend' => [
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'top',
                'x' => -40,
                'y' => 60,
                'floating' => true,
                'borderWidth' => 0,
                'backgroundColor' => '#FFFFFF',
                'shadow' => true
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0
                ]
            ],
            'tooltip' => [
                'shared' => true
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => 'Структура расходов'],
            'yAxis' => [
                ['min' => 0, 'index' => 0, 'title' => ''],
            ],
            'xAxis' => [
                ['categories' => ['Транспорт', 'Реклама', 'Аренда']],

            ],
            'series' => [
                ['name' => 'Факт', 'pointPadding' => 0.25, 'data' => [15000, 20000, 18000], 'target' => 17000, 'color' => 'rgba(195,195,195,1)'],
                ['name' => 'План', 'pointPadding' => 0.4, 'data' => [12500, 20000, 18000], 'color' => 'rgba(247,163,92,.9)']
            ]
        ],
    ]); ?>

    <?= \miloschuman\highcharts\Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],

        'options' => [
            'chart' => [
                'type' => 'column',
                'inverted' => true
            ],
            'legend' => [
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'bottom',
                'x' => -40,
                'y' => -60,
                'floating' => true,
                'borderWidth' => 0,
                'backgroundColor' => '#FFFFFF',
                'shadow' => true
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => true,
                    ],
                    'grouping' => false,
                    'shadow' => false,
                    'borderWidth' => 0
                ]
            ],
            'tooltip' => [
                'shared' => true
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'title' => ['text' => 'Доля валовой прибыли, факт'],
            'yAxis' => [
                ['min' => 0, 'index' => 0, 'title' => ''],
            ],
            'xAxis' => [
                ['categories' => ['Лыжи', 'Доски', 'Услуги']],

            ],
            'series' => [
                ['name' => 'Выручка', 'pointPadding' => 0.25, 'data' => [206920296, 224913365, 6801380], 'color' => 'rgba(183,225,115,1)'],
                ['name' => 'Вал. прибыль', 'pointPadding' => 0.4, 'data' => [106920296, 124913365, 2341380], 'color' => 'rgba(41,138,188,.9)']
            ]
        ],
    ]); ?>

    </div>
</div>

<script>
    $(document).ready(function() {
        Highcharts.SVGRenderer.prototype.symbols['c-rect'] = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };
    });
</script>