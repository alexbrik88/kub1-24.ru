<?php

use frontend\components\HighchartsFlowsHelper;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\documents\components\InvoiceStatistic;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\components\helpers\Month;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */

$step = (int) Yii::$app->request->post('step', 1);
if (!in_array($step, array_keys(HighchartsFlowsHelper::$stepItems))) {
    $step = HighchartsFlowsHelper::DEFAULT_STEP;
}
$keyFormat = HighchartsFlowsHelper::$keyFormat[$step];
$labelFormat = HighchartsFlowsHelper::$labelFormat[$step];
$period = StatisticPeriod::getSessionPeriod();
$beginDate = date_create_from_format('Y-m-d', $period['from']);
$endDate = date_create_from_format('Y-m-d', $period['to']);
$todayDate = new DateTime;

$categories = [];
$sumData = [];
$countData = [];

$counArray = InvoiceStatistic::getCountPaidByDayForPeriod($beginDate, $endDate, $company);
$sumArray = CashStatisticInfo::getSumByDayForPeriod($beginDate, $endDate, $company);

do {
    $key = $beginDate->format($keyFormat);
    $date = $beginDate->format('Y-m-d');
    $categories[$key] = $beginDate->format('d') . ' ' . (mb_strtolower(Month::$monthGenitiveRU[$beginDate->format('m')]));
    if (!array_key_exists($key, $sumData)) {
        $sumData[$key] = null;
    }
    if (!array_key_exists($key, $countData)) {
        $countData[$key] = null;
    }
    if ($beginDate <= $todayDate) {
        $sumData[$key] += isset($sumArray[$date]) ? $sumArray[$date] / 100 : 0;
        $countData[$key] += isset($counArray[$date]) ? $counArray[$date] : 0;
    }

    $beginDate->modify('+1 day');
} while ($beginDate <= $endDate);

$this->registerJs('
$(document).on("change", "#receipts-step-select", function () {
    $.pjax.reload("#receipts-pjax", {data: {step: $(this).val()}, type: "post"});
});
');
?>

<div class="portlet box darkblue widget-expense widget-home">
    <div class="portlet-title">
        <div class="caption">
            Приход за
            <div class="statistic-period-btn" style="display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;">
                <?= mb_strtolower(StatisticPeriod::getSessionName()); ?>
            </div>
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide"
               data-original-title="" title=""></a>
        </div>
        <div class="actions p-t-5 p-b-0">
            <div class="btn-group widget-home-popup">
                <?= Html::dropDownList('step', $step, HighchartsFlowsHelper::$stepItems, [
                    'class' => 'form-control period-step-select',
                    'style' => 'height: 28px; padding: 0 10px; width: 130px; margin: 2px;',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall">
        <div style="margin: 0 15px 15px; text-align: center; font-weight: bold;">
            <div style="display: inline-block; margin-right: 15px;">
                Приход:
                <div style="display: inline; color: #45b6af; font-weight: bold;">
                    +<?= number_format(array_sum($sumData), 2, ',', '&nbsp;') ?>
                </div>
                <i class="fa fa-rub"></i>
            </div>
            <div style="display: inline-block;">
                Кол-во оплаченных счетов:
                <div style="display: inline; color: #db4437; font-weight: bold;">
                    <?= number_format(array_sum($countData), 0, ',', '&nbsp;') ?>
                </div>
                шт.
            </div>
        </div>
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => [
                'title' => [
                    'text' => '',
                ],
                'lang' => [
                    'printChart' => 'На печать',
                    'downloadPNG' => 'Скачать PNG',
                    'downloadJPEG' => 'Скачать JPEG',
                    'downloadPDF' => 'Скачать PDF',
                    'downloadSVG' => 'Скачать SVG',
                    'contextButtonTitle' => 'Меню',
                ],
                'xAxis' => [
                    'categories' => array_values($categories),
                ],
                'yAxis' => [
                    [
                        'title' => ['text' => 'Сумма'],
                        'allowDecimals' => false,
                    ],
                    [
                        'title' => ['text' => 'Кол-во'],
                        'allowDecimals' => false,
                        'opposite' => true,
                    ],
                ],
                'series' => [
                    [
                        'type' => 'column',
                        'name' => 'Приход',
                        'color' => '#45b6af',
                        'data' => array_values($sumData),
                        'tooltip' => [
                            'valueDecimals' => 2,
                            'valueSuffix' => ' руб.',
                        ]
                    ],
                    [
                        'type' => 'line',
                        'name' => 'Кол-во оплаченных счетов',
                        'color' => '#db4437',
                        'yAxis' => 1,
                        'data' => array_values($countData),
                    ],
                ],
                'exporting' => [
                    'buttons' => [
                        'contextButton' => [
                            'menuItems' => new JsExpression('
                                Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems.slice(0,2).concat(
                                    Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems[3],
                                    Highcharts.defaultOptions.exporting.buttons.contextButton.menuItems[4]
                                )
                            '),
                        ],
                    ],
                ],
            ]
        ]); ?>
    </div>
</div>
