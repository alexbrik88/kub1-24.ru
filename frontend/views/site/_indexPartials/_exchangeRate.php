<?php
/* @var $currency common\models\currency\Currency[] */
use common\components\date\DateHelper;

?>
<div class="widget-siedbar widget-curse widget-home portlet box darkblue">
    <div class="portlet-title">
        <div class="caption capt_mini">
            Курс валют
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <?php foreach ($currency as $item) :
            if ($item->current_value > $item->old_value) {
                $cssClass = 'green';
                $iconClass = 'fa-caret-up';
            } elseif ($item->current_value < $item->old_value) {
                $cssClass = 'red';
                $iconClass = 'fa-caret-down';
            } else {
                $cssClass = '';
                $iconClass = 'fa-minus';
            }
            ?>
            <div class="row">
                <div class="col-xs-6" style="padding-right: 0px;">
                    <span style="float: left; padding-right: 5px;">
                        <i class="fa <?= $iconClass . ' ' . $cssClass; ?>"></i>
                    </span>
                    <span class="pull-left money-cl_menu">
                        <?= $item->name; ?> ЦБ <?= DateHelper::format($item->update_date, DateHelper::FORMAT_USER_DAY_MONTH, DateHelper::FORMAT_DATE); ?>
                    </span>
                </div>
                <div class="col-xs-6" style="padding-left: 5px;">
                    <span class="<?= $cssClass; ?> pull-right" style="padding-left: 5px; width: 45px; display: inline-block;">
                        <?= round($item->current_value - $item->old_value, 4); ?>
                    </span>
                    <span class="pull-right growth_currency" style="font-weight: bold;">
                        <?= $item->current_value; ?>
                    </span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>