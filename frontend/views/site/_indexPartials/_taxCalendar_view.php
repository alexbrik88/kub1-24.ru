<?php
/* @var common\models\notification\Notification $notification  */
?>

<span class="day">
    <?= date_format(new \DateTime($model->event_date), 'd'); ?>
</span>

<br/>

<span class="month">
    <?= \php_rutils\RUtils::dt()->ruStrFTime([
        'format' => 'F',
        'monthInflected' => true,
        'date' => $model->event_date,
    ]); ?>
</span>
