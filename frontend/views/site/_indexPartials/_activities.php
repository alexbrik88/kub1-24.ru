<?php
use common\components\date\DateHelper;
use frontend\models\log\Log;

/* @var \frontend\models\log\LogSearch $activitiesSearchModel */
/* @var \frontend\models\log\LogSearch $activitiesDataProvider */

$activitiesDataProvider->pagination->pageSize = 10;
?>

<?php \yii\widgets\Pjax::begin([
    'options' => [
        'id' => 'activities-pjaxasdf',
    ],
    'enableReplaceState' => false,
    'enablePushState' => false,
    'linkSelector' => '.activity-link-pjsx',
]); ?>

<div class="widget-latest-actions widget-home portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Последние действия
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""></a>
        </div>
        <div class="actions p-t-5 p-b-0">
            <div class="btn-group widget-home-popup">
                <?= $this->render('/log/_form_entity', [
                    'searchModel' => $activitiesSearchModel,
                    'action' => ['/site/activities'],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <?php $date = null; ?>
        <?= \yii\widgets\ListView::widget([
            'options' => [
                'class' => 'list-view',
            ],
            'layout' => "<ul class=\"feeds\">{items}</ul>",
            'dataProvider' => $activitiesDataProvider,
            'pager' => null,
            'itemView' => function (Log $model) use (&$date) {
                $currentDate = date(DateHelper::FORMAT_USER_DATE, $model->created_at);

                $output = '';

                if ($date != $currentDate) {
                    $date = $currentDate;
                    $output .= '<span class="date vert-middle-pos">' . \php_rutils\RUtils::dt()->ruStrFTime([
                            'format' => 'd F',
                            'monthInflected' => true,
                            'date' => $model->created_at,
                        ]) . '</span>';
                }

                return $output . $this->render('/log/_view', [
                    'model' => $model,
                ]);
            },
        ]); ?>
        <ul class="widget-pagination pagination-widget-latest-actions">
            <li>

                <?= \yii\helpers\Html::a('Посмотреть все', ['/log/index'])?>
            </li>
        </ul>
    </div>
</div>

<?php \yii\widgets\Pjax::end() ?>

