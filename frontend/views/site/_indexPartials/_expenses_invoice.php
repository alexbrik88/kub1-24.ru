<?php
use common\components\TextHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\helpers\Html;

/* @var array $inInvoiceStatistics */
/* @var array $invoiceTextVariants */

$paidSum = InvoiceStatistic::getPaidSum();
?>

<?= frontend\modules\documents\widgets\CostsStatisticsWidget::widget(); ?>

<div class="widget-info">Не оплачено
    <span class="red-nds">
        <?= Html::a($inInvoiceStatistics['count'] . ' ' . \php_rutils\RUtils::numeral()->choosePlural($inInvoiceStatistics['count'], $invoiceTextVariants), [
            '/documents/invoice/index',
            'type' => Documents::IO_TYPE_IN,
            'InvoiceSearch[invoice_status_id]' => implode(',', InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][Documents::IO_TYPE_IN])
        ]); ?>
    </span>
    на сумму
    <span class="red-nds"><?= TextHelper::invoiceMoneyFormat($inInvoiceStatistics['sum'] - $paidSum, 2); ?></span>
    руб.
</div>
