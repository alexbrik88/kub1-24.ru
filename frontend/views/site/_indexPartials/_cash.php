<?php
/* @var $cashStatisticInfo Array */
use common\components\TextHelper;

$bankCollapse = (count($cashStatisticInfo) > 4);
?>
<div class="portlet box darkblue widget-siedbar widget-money widget-home">
    <div class="portlet-title">
        <div class="caption capt_mini">
            Деньги
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse check-money-close" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall">
        <div class="body" style="position: relative; min-height: 122px;">
            <table class="dashboard-cash" width="100%" border="0" cellspacing="0" cellpadding="0">
                <?php foreach ($cashStatisticInfo as $key => $item) : ?>
                    <tr class="<?= $item['cssClass'] ?>">
                        <td>
                            <div>
                                <span class="flow-label" data-target="<?= $item['target'] ?>">
                                    <?= $item['typeName']; ?>:
                                </span>
                            </div>
                        </td>
                        <td>
                            <div class="text-right" style="white-space: nowrap; padding-left: 10px;">
                                <?= TextHelper::invoiceMoneyFormat($item['endBalance'], 2); ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).on("click", "table.dashboard-cash .toggle-string .flow-label", function() {
    $("table.dashboard-cash " + $(this).data("target") + " td > div").slideToggle(150);
});
</script>