<?php
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\StatisticPeriod;
use frontend\components\HighchartsFlowsHelper;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\documents\components\InvoiceStatistic;
use miloschuman\highcharts\Highcharts;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */

$step = (int) Yii::$app->request->post('step', 1);
$period = StatisticPeriod::getSessionPeriod();
$helper = new HighchartsFlowsHelper($company);
$options = $helper->getOptions($period['from'], $period['to'], $step);
$incomeSum = round(array_sum($options['series'][0]['data']), 2);
$expenseSum = round(array_sum($options['series'][1]['data']), 2);
$sumArray = $options['series'][2]['data'];
$sumArray = array_filter($sumArray);
$endSum = round(end($sumArray), 2);
$startSum = $endSum - $incomeSum + $expenseSum;
?>

<div class="portlet box darkblue widget-result widget-home">
    <div class="portlet-title">
        <div class="caption" style="">
            Движение денег за
            <div class="statistic-period-btn" style="display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;">
                <?= mb_strtolower(StatisticPeriod::getSessionName()); ?>
            </div>
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
        </div>
        <div class="actions p-t-5 p-b-0">
            <div class="btn-group widget-home-popup">
                <?= Html::dropDownList('step', $step, HighchartsFlowsHelper::$stepItems, [
                    'class' => 'form-control period-step-select',
                    'style' => 'height: 28px; padding: 0 10px; width: 130px; margin: 2px;',
                ]) ?>
            </div>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall">
        <div style="margin: 0 15px 15px; text-align: center; font-weight: bold;">
            <div style="display: inline-block; margin-right: 10px;">
                На начало:
                <div style="display: inline; color: #456baf; font-weight: bold;">
                    <?= ($startSum > 0 ? '+' : '') . number_format($startSum, 2, ',', '&nbsp;') ?>
                </div>
                <i class="fa fa-rub"></i>
            </div>
            <div style="display: inline-block;">
                <div style="display: inline-block; margin-right: 10px;">
                    Приход:
                    <div style="display: inline; color: #45b6af; font-weight: bold;">
                        +<?= number_format($incomeSum, 2, ',', '&nbsp;') ?>
                    </div>
                    <i class="fa fa-rub"></i>
                </div>
                <div style="display: inline-block; margin-right: 10px;">
                    Расход:
                    <div style="display: inline; color: #f3565d; font-weight: bold;">
                        -<?= number_format($expenseSum, 2, ',', '&nbsp;') ?>
                    </div>
                    <i class="fa fa-rub"></i>
                </div>
                <div style="display: inline-block;">
                    Остаток:
                    <div style="display: inline; color: #456baf; font-weight: bold;">
                        <?= ($endSum > 0 ? '+' : '') . number_format($endSum, 2, ',', '&nbsp;') ?>
                    </div>
                    <i class="fa fa-rub"></i>
                </div>
            </div>
        </div>
        <?= Highcharts::widget([
            'scripts' => [
                'modules/exporting',
                'themes/grid-light',
            ],
            'options' => $options,
        ]); ?>
    </div>
</div>
