<?php

use yii\grid\GridView;
use common\models\notification\Notification;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;

/* @var $notifications ActiveDataProvider */
$viewAll = $notifications->count ? '<i>Все события в ' . Notification::$monthHelper[date('n')] . '</i>' : 'Все события';
?>
<?php \yii\widgets\Pjax::begin([
    'options' => [
        'id' => 'tax-calendar-pjax',
    ],
    'enableReplaceState' => false,
    'enablePushState' => false,
]); ?>
<div class="widget-siedbar widget-calendar widget-home portlet box darkblue">
    <div class="portlet-title">
        <div class="caption capt_mini">
            Налоговый календарь
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse" data-original-title=""
               title=""></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <?= GridView::widget([
                'dataProvider' => $notifications,
                'tableOptions' => [
                    'class' => 'table',
                ],
                'showHeader' => false,

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper breakall-td tax-calendar-p-r calend_hov_non',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'widget-pagination pagination-widget-latest-actions tax-calendar',
                    ],
                    'prevPageCssClass' => 'home-arrow home-arrow-left',
                    'nextPageCssClass' => 'home-arrow home-arrow-right',
                    'prevPageLabel' => '<i class="fa fa-angle-left "></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
                    'linkOptions' => [
                        'class' => 'test',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'emptyText' => 'В ближайшие 10 дней нет событий по налоговому календарю.',
                'columns' => [
                    [
                        'label' => '',
                        'contentOptions' => [
                            'class' => 'tag',
                        ],
                        'format' => 'raw',
                        'value' => function (Notification $model) {
                            return $this->render('_taxCalendar_view', [
                                'model' => $model,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'text',
                        'format' => 'raw',
                        'value' => function (Notification $model) {
                            return Html::a($model->title, Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')])) .
                            '<span class="novelty-text col-md-12">
                                <b>Кто: </b>' . $model->text .
                            '</span>';
                        },
                    ],
                ],
            ]); ?>
        </div>
        <div class="all-novelty-in-month text-left">
            <?= Html::a($viewAll, Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]), [
                'style' => 'padding-left: 2px;',
            ]); ?>
        </div>
    </div>
</div>
<?php \yii\widgets\Pjax::end() ?>
