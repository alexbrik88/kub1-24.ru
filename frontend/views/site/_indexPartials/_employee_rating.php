<?php
use frontend\components\EmployeeRating;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */

if (empty($rating)) {
    $rating = new EmployeeRating;
}
$data = $rating->ratingData;
$prefDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'F Y',
    'monthInflected' => false,
    'date' => $rating->prevDate->format('Y-m-d')]);
$urlDate = $rating->date->format('Y-m-01');
$dateItems = [];
foreach ($rating->dateItems as $key => $value) {
    $dateItems[] = [
        'label' => $value,
        'url' => ['/site/employee-rating', 'date' => $key, 'type' => $rating->type],
        'linkOptions' => ['class' => $urlDate == $key ? 'active' : ''],
    ];
}

$labelDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'F Y',
    'monthInflected' => false,
    'date' => $rating->date->format('Y-m-d')]);
?>

<?php Pjax::begin([
    'id' => 'employee-rating-pjax',
    'enableReplaceState' => false,
    'enablePushState' => false,
    'timeout' => 10000,
]); ?>

<div class="portlet box darkblue widget-expense widget-home">
    <div class="portlet-title">
        <div class="caption">
            Лучшие сотрудники за
            <div style="display: inline-block; width: 200px;">
                <div class="dropdown">
                    <?= Html::tag('div', $labelDate, [
                        'class' => 'dropdown-toggle',
                        'data-toggle' => 'dropdown',
                        'style' => 'display: inline-block; border-bottom: 1px dashed #fff; cursor: pointer;',
                    ])?>
                    <?= Dropdown::widget([
                        'id' => 'employee-rating-dropdown',
                        'items' => array_reverse($dateItems),
                    ])?>
                </div>
            </div>
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide" data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body portlet-body-checkall">

        <?php NavBar::begin([
            'options' => [
                'class' => 'navbar-report navbar-default',
                'style' => 'min-height: auto; margin-bottom: 10px;'
            ],
            'innerContainerOptions' => [
                'class' => 'row',
                'style' => 'padding: 0;'
            ],
        ]);
        echo Nav::widget([
            'id' => 'employee-rating-menu',
            'encodeLabels' => false,
            'items' => [
                [
                    'label' => 'СЧЕТА <i class="fa fa-rub"></i>',
                    'url' => ['/site/employee-rating', 'date' => $urlDate, 'type' => 'amount'],
                    'active' => $rating->type == 'amount',
                ],
                [
                    'label' => 'СЧЕТА (шт.)',
                    'url' => ['/site/employee-rating', 'date' => $urlDate, 'type' => 'count'],
                    'active' => $rating->type == 'count',
                ],
                [
                    'label' => 'ОПЛАТЫ <i class="fa fa-rub"></i>',
                    'url' => ['/site/employee-rating', 'date' => $urlDate, 'type' => 'payment'],
                    'active' => $rating->type == 'payment',
                ],
                [
                    'label' => 'ДОЛГИ <i class="fa fa-rub"></i>',
                    'url' => ['/site/employee-rating', 'date' => $urlDate, 'type' => 'debt'],
                    'active' => $rating->type == 'debt',
                ],
                [
                    'label' => 'Все сотрудники',
                    'url' => ['/reports/employees/index'],
                    'options' => ['class' => 'all-employees'],
                    'active' => $rating->type == 'debt',
                    'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                                 Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
                ],
            ],
            'options' => ['class' => 'navbar-nav navbar-left'],
        ]);
        NavBar::end();
        ?>

        <?php if (count($data['current']) > 0) : ?>
            <div class="row">
                <div class="col-sm-12" style="display: inline-block; max-width: 500px; margin-bottom: 10px;">
                    <table  class="employee-rating-detail" style="width: 100%; line-height: 18px;">
                        <thead>
                            <tr>
                                <th width="80%"> </th>
                                <th width="10%"> </th>
                                <th width="10%"> </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $top = array_slice($data['current'], 0, 5, true) ?>
                            <?php foreach ($top as $key => $val1) : ?>
                                <?php
                                $width1 = $val1 * 100 / ($data['max'] ? : 1);
                                $val2 = isset($data['previous'][$key]) ? $data['previous'][$key] : 0;
                                $width2 = $val2 * 100 / ($data['max'] ? : 1);
                                $percentage = $val2 > 0 ? round(($val1 - $val2) * 100 / $val2, 2) : null;
                                ?>
                                <tr style="vertical-align: bottom;">
                                    <td style="padding: 5px 8px 0 0;">
                                        <div>
                                            <?= $rating->getFio($key) ?>
                                        </div>
                                        <div style="position: relative; min-width: 250px;">
                                            <?= Html::tag('div', '', [
                                                'class' => 'employee-rating-bar bar1',
                                                'style' => "background-color: {$rating->color1};" .
                                                           ($width1 > 0 ? 'min-width: 1px;' : ''),
                                                'data-width' => "{$width1}%",
                                            ]) ?>
                                        </div>
                                    </td>
                                    <td nowrap style="padding: 0 8px; font-weight: bold; text-align: right;">
                                        <?= $rating->format($val1) ?>
                                    </td>
                                    <td nowrap style="padding: 0 8px; font-weight: bold;">
                                        <?php if ($percentage !== null) : ?>
                                            <?= '(' . ($percentage >= 0 ? '+' : '') . $percentage . '&nbsp;%)'; ?>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <tr style="font-size: 10px; line-height: 12px;">
                                    <td style="padding: 0; padding-right: 8px;">
                                        <div style="position: relative; min-width: 250px;">
                                            <?= Html::tag('div', '', [
                                                'class' => 'employee-rating-bar bar2',
                                                'style' => "background-color: {$rating->color2};" .
                                                           ($width2 > 0 ? 'min-width: 1px;' : ''),
                                                'data-width' => "{$width2}%",
                                            ]) ?>
                                        </div>
                                    </td>
                                    <td nowrap style="padding: 0 8px; text-align: right;">
                                        <?= $rating->format($val2) ?>
                                    </td>
                                    <td nowrap style="padding: 0 8px;">
                                        <?= $prefDate ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <?php
                $currTotal = array_sum($data['current']);
                $prevTotal = array_sum($data['previous']);
                $maxTotal = max($currTotal, $prevTotal);
                $percTotal = $prevTotal > 0 ? round(($currTotal - $prevTotal) * 100 / $prevTotal, 2) : null;
                $tooltip = $percTotal !== null ? Html::tag('div', ($percTotal >= 0 ? '+' : '') . $percTotal . '&nbsp;%', [
                    'class' => 'total-month-tooltip',
                ]) : '';
                $height1 = $prevTotal * 100 / ($maxTotal ? : 1);
                $height2 = $currTotal * 100 / ($maxTotal ? : 1);
                ?>
                <div class="col-sm-12" style="display: inline-block; max-width: 300px;">
                    <table class="employee-rating-total">
                        <thead>
                            <tr>
                                <th width="60px"> </th>
                                <th width="60px"> </th>
                                <th > </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" style="padding-top: 5px; text-align: right;">По всем сотрудникам</td>
                            </tr>
                            <tr>
                                <td style="border-bottom: 1px solid #666; padding-top: 15px;">
                                    <div class="total-month-wrap">
                                        <?= Html::tag('div', '', [
                                            'class' => 'total-month-value',
                                            'style' => "background-color: {$rating->color2};" .
                                                       ($height1 > 0 ? 'min-width: 1px;' : ''),
                                            'data-height' => "{$height1}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td style="border-bottom: 1px solid #666; padding-top: 15px;">
                                    <div class="total-month-wrap">
                                        <?= Html::tag('div', $tooltip, [
                                            'class' => 'total-month-value',
                                            'style' => "background-color: {$rating->color1};" .
                                                       ($height2 > 0 ? 'min-height: 1px;' : ''),
                                            'data-height' => "{$height2}%",
                                        ]) ?>
                                    </div>
                                </td>
                                <td style="vertical-align: middle; padding-left: 25px;">
                                    <table style="width: auto;">
                                        <tbody>
                                            <tr>
                                                <td style="padding-right: 5px;">
                                                    <div class="total-month-mark" style="background-color: <?=$rating->color1?>;"></div>
                                                </td>
                                                <td style="text-align: right; font-weight: bold;">
                                                    <?= $rating->format($currTotal) ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-right: 5px;">
                                                    <div class="total-month-mark" style="background-color: <?=$rating->color2?>"></div>
                                                </td>
                                                <td style="text-align: right;">
                                                    <?= $rating->format($prevTotal) ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                        'format' => 'F Y',
                                        'monthInflected' => false,
                                        'date' => $rating->prevDate->format('Y-m-d')]);; ?>
                                </td>
                                <td style="text-align: center;">
                                    <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                        'format' => 'F Y',
                                        'monthInflected' => false,
                                        'date' => $rating->date->format('Y-m-d')]);; ?>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('.employee-rating-bar').each(function() {
                        $(this).animate({'width': $(this).data('width')});
                    });
                    $('.total-month-value').each(function() {
                        $(this).animate({'height': $(this).data('height')});
                    });
                });
            </script>
        <?php else : ?>
            <div>
                Нет данных
            </div>
        <?php endif ?>
    </div>
</div>

<?php Pjax::end() ?>
