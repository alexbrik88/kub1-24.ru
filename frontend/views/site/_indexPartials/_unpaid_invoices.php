<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $useContractor boolean */

$useContractor = isset($useContractor) ? $useContractor : false;
$this->title = 'Неоплаченные счета';
?>
    <a class="back-to-customers" href="<?= Url::to(['/site/index']) ?>">Назад на
        главную</a>
    <h3 class="page-title"><?= $this->title; ?></h3>

<?php
echo common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap overfl_text_hid no-paid-table',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => "{items}\n{pager}",
    'columns' => [
        [
            'attribute' => 'document_date',
            'label' => 'Дата счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '14%',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ счёта',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '14%',
            ],
            'format' => 'raw',
            'value' => function (Invoice $data) use ($useContractor) {
                return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $data,
                ])
                    ? Html::a($data->fullNumber, ['/documents/invoice/view',
                        'type' => $data->type,
                        'id' => $data->id,
                        'contractorId' => ($useContractor ? $data->contractor_id : null),
                    ])
                    : $data->fullNumber;
            },

        ],
        [
            'attribute' => 'contractor_id',
            'label' => 'Контрагент',
            'class' => DropDownDataColumn::className(),
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '14%',
            ],
            'filter' => FilterHelper::getContractorList($searchModel->type, null, true, false, false),
            'format' => 'raw',
            'value' => 'contractor_name_short',
            'visible' => ($useContractor ? false : true),
        ],
        [
            'label' => 'Сумма долга',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '14%',
            ],
            'attribute' => 'total_amount_with_nds',
            'value' => function (Invoice $model) {
                return TextHelper::invoiceMoneyFormat($model->getAvailablePaymentAmount(), 2);
            },
        ],
    ],
]);


