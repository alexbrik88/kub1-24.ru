<?php

use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use common\models\Contractor;
use yii\bootstrap\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.01.2017
 * Time: 8:22
 */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);

$allSum = DebtsHelper::getPayedSum() + DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false) ? DebtsHelper::getPayedSum() + DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false) : 1;
$percent = round((DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false) / $allSum) * 100, 2);
?>
<style>#pieLabel1 {
        display: none;
    }</style>
<div class="portlet box darkblue widget-expense widget-home">
    <div class="portlet-title">
        <div class="caption">
            Задолженность клиентов
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide"
               data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body" style="padding-right: 25px; position: relative">
        <div class="row" role="row">
            <div class="col-md-2">
                <div class="portlet-body">
                    <div id="debts-donut" class="chart"
                         style="width:100%; margin: 0 auto; margin-left: -15px; width: 172px;height: 144px;" data-overdue-percent="<?= $percent; ?>"></div>
                    <?php $this->registerJs('js:
                                    $(function() {
                                    ChartsFlotcharts.initPieChartsDebts(' . json_encode([
                            [
                                'label' => 'Не оплачено в срок',
                                'data' => DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false),
                                'color' => '#f3565d',
                            ],
                            [
                                'label' => 'Оставшшиеся',
                                'data' => DebtsHelper::getPayedSum(),
                                'color' => '#45b6af',
                            ],
                        ]) . ');
                                    });', \yii\web\View::POS_END); ?>
                </div>
            </div>
            <div class="col-md-6" style="padding-left: 30px;">
                <b style="margin-bottom: 5px; display: block">ПРОСРОЧКА ДНЕЙ</b>

                <div class="body">
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>0-10</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span><?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_0_10); ?>
                                %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10); ?></span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>11-30</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span><?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_11_30); ?>
                                %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30); ?></span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>31-60</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span><?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_31_60); ?>
                                %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60); ?></span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>61-90</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span><?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_61_90); ?>
                                %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>Больше 90</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span><?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_MORE_90); ?>
                                %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90); ?></span>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 7px;">
                        <div class="col-md-3 col-sm-8  col-xs-4 nowrap m-l-n ">
                    <span><b style="color: #f3565d;">Вся
                            задолженность</b></span>
                        </div>
                        <div
                            class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>100 %</span>
                        </div>
                        <div
                            class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span><b
                                    style="color: #f3565d;"><?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true); ?></b></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div style="margin-bottom: 5px; font-weight: bold;">ТОП-5 ДОЛЖНИКОВ</div>
                <div style="height: 102px; overflow: hidden;">
                    <?php foreach (DebtsHelper::getTop(5) as $contractorID => $sum): ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-7 mar-bot-3 nowrap m-l-n"
                                 style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                <span class="tooltip2"
                                      data-tooltip-content="#tooltip_contractor-debt-<?= $contractorID; ?>"><?= Contractor::findOne($contractorID)->getNameWithType(); ?></span>
                            </div>
                            <div
                                class="col-md-6 col-sm-6 col-xs-5 pull-right text-right ">
                                <span><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row" style="padding-top: 7px;">
                    <div class="col-sm-12 pull-right text-right ">
                        <?= Html::a('Отчет по всем должникам', \yii\helpers\Url::to(['/reports/debt-report/debtor']), [
                            //'style' => 'float: right;padding-top: 16px; position: absolute; bottom: 12px; right: 15px;',
                        ]); ?>
                    </div>
                </div>
                <div class="tooltip_templates" style="display: none;">
                    <?php foreach (DebtsHelper::getTop(5) as $contractorID => $sum): ?>
                        <?php $contractor = Contractor::findOne($contractorID); ?>
                        <span id="tooltip_contractor-debt-<?= $contractorID; ?>"
                              style="display: inline-block; text-align: center;">
                            <?= $contractor->nameWithType; ?>
                        </span>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
