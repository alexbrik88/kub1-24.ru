<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.07.2018
 * Time: 16:52
 */

use common\components\debts\DebtsHelper;
use common\components\TextHelper;
use common\models\Contractor;
use yii\bootstrap\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use frontend\models\Documents;
use yii\helpers\Url;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);

$debtsMore90 = DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, true, false, null, Documents::IO_TYPE_IN);
$payedSum = DebtsHelper::getPayedSum(Documents::IO_TYPE_IN);
$currentDebt = DebtsHelper::getCurrentDebt(null, Documents::IO_TYPE_IN);
$allSum = $payedSum + $debtsMore90;
$allSum = $allSum ? $allSum : 1;
$percent = round(($debtsMore90 / $allSum) * 100, 2);

$totalDebt = $debtsMore90 + $currentDebt;
?>
<style>#pieLabel1 {
        display: none;
    }</style>
<div class="portlet box darkblue widget-expense widget-home">
    <div class="portlet-title">
        <div class="caption">
            Задолженность перед поставщиками
        </div>
        <div class="tools arrow-tools">
            <a href="javascript:;" class="collapse checkall-slide"
               data-original-title="" title=""></a>
        </div>
    </div>
    <div class="portlet-body" style="padding-right: 25px; position: relative">
        <div class="row" role="row">
            <div class="col-md-2">
                <div class="portlet-body">
                    <div id="debts-donut-seller" class="chart"
                         style="width:100%; margin: 0 auto; margin-left: -15px; width: 172px;height: 144px;"
                         data-overdue-percent="<?= $percent; ?>"></div>
                    <?php $this->registerJs('js:
                        $(function() {
                            ChartsFlotcharts.initPieChartsDebtsSeller(' . json_encode([
                            [
                                'label' => 'Не оплачено в срок',
                                'data' => $debtsMore90,
                                'color' => '#f3565d',
                            ],
                            [
                                'label' => 'Оставшшиеся',
                                'data' => $payedSum,
                                'color' => '#45b6af',
                            ],
                        ]) . ');
                        });', \yii\web\View::POS_END); ?>
                </div>
            </div>
            <div class="col-md-6" style="padding-left: 30px;">
                <b style="margin-bottom: 5px; display: block">ПРОСРОЧКА ДНЕЙ</b>

                <div class="body">
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>Текущая задолженность</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= $totalDebt ? round($currentDebt * 100 / $totalDebt, 2) : '0'; ?> %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= TextHelper::invoiceMoneyFormat($currentDebt, 2); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>0-10</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_0_10, null, true, Documents::IO_TYPE_IN); ?>
                                %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_0_10, false, true, null, Documents::IO_TYPE_IN); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>11-30</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_11_30, null, true, Documents::IO_TYPE_IN); ?>
                                %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_11_30, false, true, null, Documents::IO_TYPE_IN); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>31-60</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_31_60, null, true, Documents::IO_TYPE_IN); ?>
                                %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_31_60, false, true, null, Documents::IO_TYPE_IN); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row mar-bot-3">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>61-90</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_61_90, null, true, Documents::IO_TYPE_IN); ?>
                                %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_61_90, false, true, null, Documents::IO_TYPE_IN); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-8 col-xs-4 nowrap m-l-n ">
                            <span>Больше 90</span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>
                                <?= DebtsHelper::getDebtsPercentage(DebtsHelper::PERIOD_MORE_90, null, true, Documents::IO_TYPE_IN); ?>
                                %
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <?= DebtsHelper::getDebtsSum(DebtsHelper::PERIOD_MORE_90, false, true, null, Documents::IO_TYPE_IN); ?>
                            </span>
                        </div>
                    </div>
                    <div class="row" style="padding-top: 7px;">
                        <div class="col-md-3 col-sm-8  col-xs-4 nowrap m-l-n ">
                            <span>
                                <b style="color: #f3565d;">
                                    Вся задолженность
                                </b>
                            </span>
                        </div>
                        <div class="col-md-4 col-sm-1 pull-right m-r text-right w15p0">
                            <span>100 %</span>
                        </div>
                        <div class="col-md-4 col-sm-2 pull-right text-right w40">
                            <span>
                                <b style="color: #f3565d;">
                                    <?= TextHelper::invoiceMoneyFormat($totalDebt, 2); ?>
                                </b>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div style="margin-bottom: 5px; font-weight: bold;">ТОП-5 КРЕДИТОРОВ</div>
                <div style="height: 102px; overflow: hidden;">
                    <?php foreach (DebtsHelper::getTop(5, Documents::IO_TYPE_IN) as $contractorID => $sum): ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-7 mar-bot-3 nowrap m-l-n"
                                 style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                                <span class="tooltip2"
                                      data-tooltip-content="#tooltip_contractor-debt-<?= $contractorID; ?>">
                                    <?= Contractor::findOne($contractorID)->getNameWithType(); ?>
                                </span>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-5 pull-right text-right ">
                                <span>
                                    <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
                                </span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="row" style="padding-top: 27px;">
                    <div class="col-sm-12 pull-right text-right ">
                        <?= Html::a('Отчет по всем кредиторам', Url::to(['/reports/debt-report-seller/debtor'])); ?>
                    </div>
                </div>
                <div class="tooltip_templates" style="display: none;">
                    <?php foreach (DebtsHelper::getTop(5, Documents::IO_TYPE_IN) as $contractorID => $sum): ?>
                        <?php $contractor = Contractor::findOne($contractorID); ?>
                        <span id="tooltip_contractor-debt-<?= $contractorID; ?>"
                              style="display: inline-block; text-align: center;">
                            <?= $contractor->nameWithType; ?>
                        </span>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

