<?php
use common\models\cash\CashFlowsBase;
use common\models\document\InvoiceExpenditureItem;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$company = Yii::$app->user->identity->company;
$datePeriod = StatisticPeriod::getSessionPeriod();
$select = [
    'exp_id' => 'IFNULL([[expenditure_item_id]], 0)',
    'amount',
];
$where = [
    'and',
    ['flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE],
    ['between', 'date', $datePeriod['from'], $datePeriod['to']],
    ['not', ['expenditure_item_id' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS]],
];
$flowQuery = $company->getAllFlows($select, $where);
$expensesQuery = (new Query)->select([
    'sum' => 'SUM([[amount]])',
    'exp_id',
])->from(['t' => $flowQuery])->groupBy('exp_id')->orderBy(['sum' => SORT_DESC])->indexBy('exp_id');
$expensesDataArray = $expensesQuery->column();
$totalSum = array_sum($expensesDataArray);
$expensesItems = InvoiceExpenditureItem::find()->select(['name', 'id'])->where([
    'or',
    ['company_id' => null],
    ['company_id' => $company->id],
])->indexBy('id')->column();
$expensesItems[0] = 'Прочее';

$tableData = [];
$pieData = [];
$other = 0;
$p = $totalSum * 0.05;
foreach ($expensesDataArray as $key => $value) {
    if ($value < $p) {
        $other += $value;
        unset($expensesDataArray[$key]);
    }
}
if ($other > 0) {
    $expensesDataArray[0] = $other;
}
$key = 0;
foreach ($expensesDataArray as $id => $sum) {
    $name = isset($expensesItems[$id]) ? $expensesItems[$id] : '';
    $color = isset(InvoiceStatistic::$donutChartColor[$key]) ? InvoiceStatistic::$donutChartColor[$key] : null;
    $tableData[] = [
        'color' => $color,
        'name' => $name,
        'sum' => number_format(($sum / 100), 2, ',', '&nbsp;'),
        'percent' => round(($sum * 100 / ($totalSum ? : 1)), 2),
    ];
    $pieData[] = [
        'label' => mb_strtolower($name),
        'value' => round(($sum / 100), 2),
        'color' => $color,
    ];
    $key++;
}
?>
<div class="row">
    <div class="col-md-6">
        <table class="expenses-payment-table table">
            <tbody>
                <?php foreach ($tableData as $row) : ?>
                    <tr role="row">
                        <td style="width: 30px; padding: 1px;">
                            <?php if ($row['color']) : ?>
                                <span class="color-expense" style="background-color:<?=$row['color']?>"></span>
                            <?php endif ?>
                        </td>
                        <td style=""><?= $row['name'] ?></td>
                        <td style="width: 100px; text-align: right;"><?= $row['sum'] ?></td>
                        <td style="width: 10%; text-align: right;"><?= $row['percent'] ?>&nbsp;%</td>
                    </tr>
                <?php endforeach ?>
                <tr class="font-bold" role="row" style="">
                    <td style="width: 30px;"></td>
                    <td style="">ИТОГО</td>
                    <td style="width: 100px; text-align: right;">
                        <?= number_format(($totalSum / 100), 2, ',', '&nbsp;') ?>
                    </td>
                    <td style="width: 10%; text-align: right;">100&nbsp;%</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <?php if ($totalSum) : ?>
            <div id="morris_chart_donut" style="width:100%; margin: 0 auto; max-width: 175px; height: 175px;"></div>
            <script type="text/javascript">
            $(document).ready(function() {
                var morrisDonut = Morris.Donut({
                    element: "morris_chart_donut",
                    formatter: function (y, data) { return y.toLocaleString('ru-RU', { minimumFractionDigits: 2, maximumFractionDigits: 2 }); },
                    data: <?= Json::encode($pieData) ?>
                });
                /*
                morrisDonut.setLabels = function(label1, label2) {
                  var inner, maxHeightBottom, maxWidth, text2bbox, text2scale;
                  inner = (Math.min(this.el.width() / 2, this.el.height() / 2) - 10) * 2 / 3;
                  maxWidth = 1.8 * inner;
                  maxHeightBottom = inner / 3;
                  this.text1.attr({
                    text: label1,
                    transform: ''
                  });
                  this.text2.attr({
                    text: label2,
                    transform: ''
                  });
                  text2bbox = this.text2.getBBox();
                  text2scale = Math.min(maxWidth / text2bbox.width, maxHeightBottom / text2bbox.height);
                  return this.text2.attr({
                    transform: "S" + text2scale + "," + text2scale + "," + (text2bbox.x + text2bbox.width / 2) + "," + text2bbox.y
                  });
                };
                */
                morrisDonut.select(0);
            });
            </script>
        <?php endif ?>
    </div>
</div>
<div>
    <?= Html::a('Отчет по затратам', ['/reports/expenses-report/payments']) ?>
</div>
