<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Запрос на сброс пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
    <?= \frontend\widgets\Alert::widget(); ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>Пожалуйста введите Ваш e-mail. На него будет отправлена ссылка для восстановления пароля.</p>
    <div class="row">
        <div class="col-lg-5">
            <?php \yii\widgets\Pjax::begin(['enablePushState' => false]); ?>
            <?php $form = ActiveForm::begin([
                'id' => 'request-password-reset-form',
                'enableAjaxValidation' => true,
                'validateOnBlur' => false,
                'validateOnChange' => false,
                'validateOnSubmit' => true,
                'options' => [
                    'data-pjax' => '',
                ],
            ]); ?>
                <?= $form->field($model, 'email') ?>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
