<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order error-page';
?>

<div class="alert alert-danger">
    <?= nl2br($message) ?>
</div>