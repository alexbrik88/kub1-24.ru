<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.04.2020
 * Time: 15:38
 */

/* @var $this yii\web\View
 */

$this->title = 'Доступно только в новом дизайне';
?>
<div class="only-new-design">
    <div class="text">
        Доступно только в новом дизайне.
    </div>
    <div class="text" style="margin-top: 20px;">
        Переключатель на новый дизайн сверху.
    </div>
</div>