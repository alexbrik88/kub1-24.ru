<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 31.8.15
 * Time: 10.13
 * Email: t.kanstantsin@gmail.com
 */

use common\models\company\CompanyType;
use frontend\models\RegistrationForm;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \frontend\models\RegistrationForm $model */

$typeArray = CompanyType::find()->select('name_short')->andWhere([
    'id' => RegistrationForm::$typeIds,
])->indexBy('id')->column();
$typeItems = [];
foreach (RegistrationForm::$typeIds as $typeId) {
    $typeItems[$typeId] = $typeArray[$typeId] ? : 'Самозанятый';
}
$bankClassArray = Banking::$registerModelClassArray;
$this->title = 'Регистрация';

$this->registerJs(<<<JS
$(document).on('change', '#registrationform-companytype input[type=radio]', function() {
    if ($('#registrationform-companytype input[type=radio][value=0]').is(':checked')) {
        $('.field-registrationform-taxationtypeosno input[type=checkbox]')
            .prop({checked: false, disabled: true})
            .uniform('refresh');
    } else {
        $('.field-registrationform-taxationtypeosno input[type=checkbox]')
            .prop({disabled: false})
            .uniform('refresh');
    }
});
JS
);
?>

<div class="registration-view">
    <h4 style="text-transform: uppercase;"><?= $this->title ?></h4>
    <p style="color: #1fb7f4; font-size: 16px; padding: 10px 0 10px 0 !important;">
        Бесплатный пробный период 14 дней
    </p>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'registration-form',
        'method' => 'POST',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'email')->textInput(); ?>

    <div>
        <?= $form->field($model, 'companyType', [
            'options' => [
                'class' => 'company-type-chooser',
            ],
        ])->radioList($typeItems); ?>
        <div class="company-type-block-non-ip">
        </div>
        <div class="company-type-block-ip">
        </div>
    </div>

    <?= $form->field($model, 'taxationTypeOsno', [
        'parts' => [
            '{input}' => Html::tag(
                'div',
                Html::activeCheckbox($model, 'taxationTypeOsno', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypeUsn', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypeEnvd', ['labelOptions' => ['class' => 'pad-r-10']]) .
                Html::activeCheckbox($model, 'taxationTypePsn', ['disabled' => true])
            )
        ]
    ])->label('Система налогобложения')->render(); ?>

    <?= \yii\helpers\Html::input('submit', null, 'ДАЛЕЕ', [
        'class' => 'btn btn-primary ladda-button w-100',
    ]); ?>

    <p>
        Регистрируясь в системе, Вы принимаете условия
        <a href="#" target="_blank">лицензионного соглашения</a>
    </p>

    <?php $form->end(); ?>
</div>

<div>
    <?php if ($bankClassArray) : ?>
        <div class="form-group row text-bold">
            <label class="col-sm-12 text-bold">Или зарегистрируйтесь с помощью:</label>
            <?php foreach ($bankClassArray as $class) : ?>
                <div class="col-sm-12">
                    <?= Html::a($class::NAME, ["/cash/banking/{$class::$alias}/default/registration"], [
                        'class' => 'bank-' . $class::$alias,
                        'data-tooltip-content' => '#' . $class::$alias . '-choose-token'
                    ]) ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>

<!-- Sberbank Auth -->
<div style="display: none">
    <div id="sberbank-choose-token" class="update-attribute-tooltip-content" style="width: 175px;text-align:center">
        <div class="mar-b-5 text-bold text-center">У вас есть токен?</div>
        <div class="btn-group" style="border: 1px solid #ddd;">
            <?= Html::a('Да', ["/cash/banking/{$class::$alias}/default/registration", 'by_token' => 1], [
                'class' => 'btn sberbank-token',
                'style' => 'width: 49px; color: #fff; background-color: #3379b5;'
            ]) ?>
            <?= Html::a('Нет', ["/cash/banking/{$class::$alias}/default/registration", 'by_token' => 0], [
                'class' => 'btn sberbank-web',
                'style' => 'width: 49px; color: #333; background-color: #fff;'
            ]) ?>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(".bank-sberbank").attr('href', 'javascript:;').tooltipster({
                theme: ["tooltipster-kub"],
                contentCloning: true,
                trigger: "click",
                side: "top"
            });
        });
    </script>
</div>