<?php

use frontend\models\RobotForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RobotForm */
/* @var $form ActiveForm */

$radioItems = [
    1 => 'Да',
    0 => 'Нет',
];
$conf_checkbox = [
    'labelOptions' => [
        'style' => 'display: block;',
    ]
];
$conf_radio_1 = [
    'options' => [
        'class' => 'form-group row',
    ],
    'template' => "<div class=\"col-md-5\">\n{label}\n</div>\n" .
                "<div class=\"col-md-7\">\n{input}\n" .
                Html::activeCheckbox($model, 'envd', $conf_checkbox) . "\n" .
                Html::activeCheckbox($model, 'patent', $conf_checkbox) . "\n" .
                "</div>\n" .
                "<div class=\"col-md-12\">\n{error}\n</div>\n",
];
$conf_radio_2 = [
    'options' => [
        'class' => 'form-group row',
    ],
    'template' => "<div class=\"col-md-5\">\n{label}\n</div>\n" .
                "<div class=\"col-md-7\">\n{input}\n</div>\n" .
                "<div class=\"col-md-12\">\n{error}\n</div>\n",
];
?>

<h3>
    Наш робот-бухгалтер пока не подходит для всех ИП на УСН,
    поэтому просьба ответить на несколько уточняющих вопросов.
</h3>

<div class="site-robot-registration" style="max-width: 600px;">
    <?php $form = ActiveForm::begin([
        'id' => 'robot-registration-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'tax', $conf_radio_1)->radioList(RobotForm::$taxArray, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'style' => 'display: block;',
                ],
            ]);
        },
    ]) ?>

    <?= $form->field($model, 'worker', $conf_radio_2)->radioList($radioItems, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'style' => 'display: block;',
                ],
            ]);
        },
    ]) ?>

    <?= $form->field($model, 'cashbox', $conf_radio_2)->radioList($radioItems, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'style' => 'display: block;',
                ],
            ]);
        },
    ]) ?>

    <?= $form->field($model, 'clientbank', $conf_radio_2)->radioList($radioItems, [
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'style' => 'display: block;',
                ],
            ]);
        },
    ]) ?>

    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
    ]) ?>
    <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('ПОЛУЧИТЬ ДОСТУП К РОБОТУ-БУХГАЛТЕРУ', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- site-robot-registration -->
