<?php

use yii\helpers\Html;

$this->title = 'Бизнес платежи';

?>

<?= $this->render('_style') ?>
<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 2, 'company' => $company]) ?>
        <div class="col-xs-12 col-lg-12 pad0 step-by-step-form">

            <div class="row">
                <div class="col-xs-6">
                    <span class="caption" style="margin-top:0">
                        Зарабатывайте больше с модулем B2B платежей. <br/>
                        <a href="<?= Yii::$app->params['testOutInvoiceLink'] ?>" target="_blank" style="font-size:13px;font-weight: normal">Пример, как это работает.</a>
                    </span>
                    <span class="b2b-description-block">
                        <strong>Модуль В2В оплат позволяет:</strong>
                        <ol>
                            <li>Автоматизировать на вашем сайте выставление счетов для покупателей юрлиц (ООО и ИП).<br/>
                            Счет формируется по выбранному товару в корзине.</li>
                            <li>Автоматизировать прием платежей от ООО и ИП.<br/>
                            Мгновенное получение оплат от покупателей ООО и ИП.<br/>
                            Оплата происходит так же быстро, как и оплата от от физ лиц по банковским картам.</li>
                            <li>Увеличивать продажи<br/>
                                <ul>
                                    <li>Конверсия в покупку у юрлиц составляет 10%, а конверсия в покупку у физ лиц всего 3%.</li>
                                    <li>Юрлица покупают почти вдвое больше, чем физлица.</li>
                                    <li>Наш модуль В2В дополнительно увеличивает конверсию за счет выставления счета на вашем сайте и мгновенной оплаты.</li>
                                </ul>
                            </li>
                        </ol>
                        <strong>С модулем В2В оплат ваш сайт не теряет клиентов ООО и ИП.</strong>
                    </span>
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-6">
                    <span class="caption">Для НЕ интернет-магазинов (нельзя добавить товар в корзину): landing page, социальных сетей, e-mail писем</span>
                    <span>

                        Создайте ссылку с набором товаров или услуг и разместите ее в Инстаграмм, на Landing page или отправьте ссылку
                        в письме покупателю. Он сам выберет товар, сам выставит себе счет и моментально его оплатит.
                    </span>
                    <br/>
                    <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> Создать модуль B2B для НЕ интернет-магазинов',
                        ['/out-invoice/create', 'backTo' => 'B2B', 'type' => 'landing'], [
                            'class' => 'btn darkblue darkblue-invert' . ($isPaid ? '' : ' b2b-pay-panel-trigger'),
                            'style' => 'margin-top:10px'
                        ]); ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-6">
                    <span class="caption">Для интернет магазинов</span>
                    <span>
                        Ваш интернет-магазин сможет самостоятельно выставлять счета покупателям ООО и ИП.
                        У покупателя будет возможность получить счет в любое удобное время и в любой точке мира.
                        Не нужно дозваниваться до ваших сотрудников, бухгалтеров, с просьбой выставить счет.<br/>
                        Покупатель из счета на вашем сайте попадает в свой онлайн банк, где мы уже подготовим ему платежку на оплату.
                        Ему останется её только подписать.<br/>
                        Ваша админка – это ваш личный кабинет в КУБ. Там вся информация по заказам, оплаченным и
                        не оплаченным счетам, аналитика по результату за период,
                        по конкретному товару и каждому клиенту. <br/>
                        Вы не упустите ни одного клиента юридическое лицо с нашим Модулем В2В оплат.
                    </span>
                    <br/>
                    <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> Создать модуль B2B для интернет-магазинов',
                        ['/out-invoice/create', 'backTo' => 'B2B', 'type' => 'shop'], [
                            'class' => 'btn darkblue darkblue-invert' . ($isPaid ? '' : ' b2b-pay-panel-trigger'),
                            'style' => 'margin-top:10px'
                        ]); ?>
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-12 caption">
                    <span>Тарифы</span>
                </div>
                <div class="col-xs-6">

                    <?= $this->render('parts_description/tariffs_table') ?>

                </div>

            </div>

        </div>

        <!-- buttons -->
        <div id="buttons-bar-fixed">
            <div class="col-xs-12 pad0 buttons-block">
                <?= Html::a('Назад', ['company'], [
                    'class' => 'btn darkblue',
                    'style' => 'min-width: 180px;'
                ]) ?>
                <?= Html::a('Далее', ['products'], [
                    'class' => 'btn darkblue pull-right',
                    'style' => 'min-width: 180px;'
                ]); ?>
            </div>
        </div>

    </div>
</div>