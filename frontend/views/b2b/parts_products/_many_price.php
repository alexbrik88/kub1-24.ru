<?php

use common\models\product\Product;
use common\models\product\ProductGroup;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $productionType int */

$model = new ProductPriceForm($company);
$unitTemplate = "<div class=\"row\">\n<div class=\"col-sm-4\">\n{label}\n</div>\n";
$unitTemplate .= "<div class=\"col-sm-8\">\n{input}\n{hint}\n{error}\n</div>\n</div>";
$unitConf = [
    'template' => $unitTemplate,
];

$inputTemplate1 = "<div class=\"price-inp-wrap\">\n<span class=\"inp-after\">на</span>\n{input}\n</div>\n{hint}\n{error}";
$inputConf1 = [
    'template' => $inputTemplate1,
];

$inputTemplate2 = "<div class=\"price-inp-wrap\">\n<span class=\"inp-after\"></span>\n{input}\n</div>\n{hint}\n{error}";
$inputConf2 = [
    'template' => $inputTemplate2,
];

$this->registerJs('
var resetManyPriceForm = function() {
    $("#productpriceform-unit input:radio").prop("checked", false).uniform("refresh");
    $("#many-price-form select, #many-price-form :text").val("").prop("disabled", true);
    $("#productpriceform-form_error").val("")
    $("#product-id-inputs").html("");
    $("#many-price-form .change_value .inp-after").html("");
    $("#many-price-form").yiiActiveForm("resetForm");
}
$(document).on("show.bs.modal", "#many-price-modal", function() {
    resetManyPriceForm();
    $("input.product_checker:checked").each(function(i, item) {
        $(item).clone().attr({
            "type" : "hidden",
            "name" : "ProductPriceForm[product_id][]",
        }).appendTo("#product-id-inputs");
    });
});
$(document).on("hide.bs.modal", "#many-price-modal", function() {
    resetManyPriceForm();
});
$(document).on("change", "#productpriceform-unit input:radio", function() {
    $("#many-price-form select").prop("disabled", false);
    $("#many-price-form .change_value .inp-after").html(
        $("#productpriceform-unit input:radio:checked").val() == "1" ? "%" : "руб."
    );
});
$(document).on("change", "#productpriceform-buy_type, #productpriceform-sell_type", function() {
    $("#productpriceform-form_error")
        .val($("#productpriceform-buy_type").val() + $("#productpriceform-sell_type").val())
        .trigger("change");
    var changeValue = $(this).closest(".input_group").find(".change_value input:text");
    if (!$(this).val()) {
        changeValue.val("").prop("disabled", true);
        $("#many-price-form").yiiActiveForm("validateAttribute", changeValue.attr("id"));
    } else {
        changeValue.prop("disabled", false);
    }
});
$(document).on("keyup", "#many-price-form :text", function() {
    var value = $(this).val().replace(/,/g, ".").replace(/[^\d.]/g, "").match( /\d+(\.\d{0,2})?/ );
    $(this).val(value ? value[0] : "");
});
');
?>

<?php Modal::begin([
    'id' => 'many-price-modal',
    'header' => '<h1>Изменить цены по выбранным ' . ($productionType == 1 ? 'товарам' : 'услугам') . '</h1>',
    //'toggleButton' => [
    //    'tag' => 'a',
    //    'label' => 'Цена',
    //    'class' => 'btn btn-default btn-sm',
    //],
    'options' => [
        'data-url' => Url::to(['many-price', 'productionType' => $productionType]),
    ]
]); ?>
    <?php $form = ActiveForm::begin([
        'id' => 'many-price-form',
        'action' => ['/product/many-price', 'productionType' => $productionType],
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
    ]); ?>

    <div class="form-group">
        <?= $form->field($model, 'unit', $unitConf)->radioList(ProductPriceForm::$unitItems, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value'  => $value,
                    'label'  => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline',
                        'style' => 'font-size: 14px;'
                    ]
                ]);
            }
        ]); ?>
    </div>

    <div class="form-group">
        <div class="row input_group">
            <div class="col-sm-4">
                <label>Цена покупки</label>
            </div>
            <div class="col-sm-4 change_type" style="padding-right: 0;">
                <?= $form->field($model, 'buy_type', $inputConf1)->label(false)->dropDownList(ProductPriceForm::$typeItems, [
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-sm-4 change_value">
                <?= $form->field($model, 'buy_value', $inputConf2)->label(false)->textInput([
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row input_group">
            <div class="col-sm-4">
                <label>Цена продажи</label>
            </div>
            <div class="col-sm-4 change_type" style="padding-right: 0;">
                <?= $form->field($model, 'sell_type', $inputConf1)->label(false)->dropDownList(ProductPriceForm::$typeItems, [
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-sm-4 change_value">
                <?= $form->field($model, 'sell_value', $inputConf2)->label(false)->textInput([
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
    </div>

    <div id="product-id-inputs"></div>

    <?= $form->field($model, 'form_error')->label(false)->hiddenInput(); ?>

    <div>
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white',
            'style' => 'width: 110px;',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'style' => 'width: 110px;',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>
<?php Modal::end(); ?>