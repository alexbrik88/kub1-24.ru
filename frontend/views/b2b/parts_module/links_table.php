<?php

use common\components\grid\DropDownDataColumn;
use common\components\grid\GridView;
use common\models\out\OutInvoice;
use frontend\models\Documents;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\Company;
use \common\models\document\Invoice;
use \common\models\document\status\InvoiceStatus;
use \common\models\service\SubscribeHelper;
use common\components\date\DateHelper;
use \common\models\service\SubscribeTariffGroup;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OutInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $backTo string */
/* @var $company Company */
/* @var $isPaid bool */

$backTo = isset($backTo) ? $backTo : 'B2B';
$company = Yii::$app->user->identity->company;

//$isBlocked = !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT);

$actualSubscribes = SubscribeHelper::getPayedSubscriptions($company->id, SubscribeTariffGroup::B2B_PAYMENT);
$expireDate = SubscribeHelper::getExpireDate($actualSubscribes);
$expireDateFormatted = ($expireDate) ? date(DateHelper::FORMAT_USER_DATE, $expireDate) : '';
?>
<div class="out-invoice-index">

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-8">
            <div style="font-size: 23px;">
                Список ссылок на Модуль В2В оплат.
            </div>
            <a href="<?= Yii::$app->params['testOutInvoiceLink'] ?>" target="_blank" style="font-size:13px;font-weight:normal">Пример, как это работает.</a>
        </div>
        <div class="col-sm-4">
            <?= Html::a('Создать ссылку', ['/out-invoice/create', 'backTo' => $backTo, 'type' => 'landing'], [
                'class' => 'btn yellow pull-right'
            ]); ?>
            <?php /* <?= Html::a('Создать ссылку', $isPaid ?
                ['/out-invoice/create', 'backTo' => $backTo] : 'javascript:;', [
                'class' => 'btn yellow pull-right' . ($isPaid ? '' : ' b2b-pay-panel-trigger'),
            ]); ?> */ ?>
        </div>
    </div>

    <?php $pjax = Pjax::begin([
        'id' => 'out-invoice-contractor-pjax',
        'timeout' => 10000,
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => 'У вас пока нет ни одной ссылки на модуль B2B. ' .
            Html::a('Создать ссылку', ['/out-invoice/create', 'backTo' => $backTo, 'type' => 'landing'], ['data-pjax' => 0]),
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
            'id' => 'datatable_ajax',
            'aria-describedby' => 'datatable_ajax_info',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],

        'options' => [
            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => [
                    'width' => '20',
                ],
                'header' => '##',
            ],
            [
                'attribute' => 'site',
                'label' => 'Сайт',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->return_url;
                }
            ],
            [
                'headerOptions' => [
                    'width' => '15%',
                ],
                'attribute' => 'is_demo',
                'label' => 'Ссылка на модуль В2В',
                'format' => 'raw',
                'value' => function ($model) use ($isPaid) {
                    if ($model->is_outer_shopcart) {
                        return '';
                    }

                    $moduleLink = Html::a('Ссылка для сайта', $model->outUrl, [
                        'title' => 'Посмотреть',
                        'aria-label' => 'Посмотреть',
                        'target' => '_blank',
                        'data' => [
                            'pjax' => 0
                        ]]);
                    $demoModuleLink = ($model->demoOutUrl ? Html::a("Демо ссылка", $model->demoOutUrl, [
                        'title' => 'Посмотреть',
                        'aria-label' => 'Посмотреть',
                        'target' => '_blank',
                        'data' => [
                            'pjax' => 0
                        ]
                        ]) : '');

                    $copyLink = Html::a("<span class='fa fa-clipboard'></span>" . Html::hiddenInput('link', $model->outUrl), '#', [
                        'title' => 'Скопировать ссылку',
                        'aria-label' => 'Скопировать ссылку',
                        'class' => 'copy-link',
                        'data' => [
                            'pjax' => 0
                        ]
                    ]);

                    return  ($isPaid) ? ($moduleLink . '&nbsp;&nbsp;' . $copyLink) : $demoModuleLink;
                }
            ],
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'attribute' => 'has_payment',
                'contentOptions' => ['class' => 'text-center'],
                'label' => 'Оплата',
                'format' => 'raw',
                'value' => function ($model) use ($isPaid, $expireDateFormatted) {

                    $linkPayModule = Html::a('Оплатить', 'javascript:;', [
                        'class' => 'btn darkblue text-white b2b-pay-panel-trigger',
                        'style' => 'max-width:100px'
                    ]);

                    return  ($isPaid) ? $expireDateFormatted : $linkPayModule;
                }
            ],
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'attribute' => 'links_count',
                'label' => 'Кол-во счетов',
                'format' => 'raw',
                'value' => function ($model) {

                return (int)Invoice::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted()
                    ->byDemo()
                    ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
                    ->andWhere(['from_out_invoice' => true])
                    ->andWhere(['by_out_link_id' => $model->id])
                    ->count();
                }
            ],
            [
                'headerOptions' => [
                    'width' => '10%',
                ],
                'attribute' => 'bills_count',
                'label' => 'Кол-во оплат',
                'format' => 'raw',
                'value' => function ($model) use ($company) {
                    return (int)Invoice::find()
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byDeleted()
                        ->byStatus(InvoiceStatus::STATUS_PAYED)
                        ->andWhere(['company_id' => $company->id])
                        ->andWhere(['from_out_invoice' => true])
                        ->andWhere(['by_out_link_id' => $model->id])
                        ->count();
                }
            ],
            [
                'attribute' => 'note',
                'enableSorting' => false,
                'headerOptions' => [
                    'width' => '30%',
                ],
            ],
            [
                'attribute' => 'status',
                'enableSorting' => false,
                'class' => DropDownDataColumn::className(),
                'filter' => [null => 'Все'] + OutInvoice::$statusAvailable,
                'headerOptions' => [
                    'width' => '100',
                ],
                'value' => 'statusValue',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'out-invoice',
                'template' => '{look}',
                'headerOptions' => [
                    'width' => '20',
                ],
                'buttons' => [
                    'look' => function ($url, $model, $key) use ($isPaid) {

                        return Html::a("<span class='glyphicon glyphicon-eye-".($isPaid ? 'open':'close')."'></span>", ($isPaid) ? $model->outUrl : null, [
                            'title' => 'Посмотреть',
                            'aria-label' => 'Посмотреть',
                            'class' => ($isPaid) ? '' : 'b2b-pay-panel-trigger',
                            'target' => '_blank',
                            'data' => [
                                'pjax' => 0
                            ]
                        ]);
                    }
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'out-invoice',
                'template' => '{update} {delete}',
                'headerOptions' => [
                    'width' => '40',
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) use ($backTo) {
                        return Html::a("<span class='glyphicon glyphicon-pencil'></span>",
                            Url::to(['/out-invoice/update', 'id' => $model->id, 'backTo' => $backTo]), [
                                'title' => 'Изменить',
                                'aria-label' => 'Изменить',
                                'data-pjax' => '0',
                            ]);
                    },
                    'delete' => function ($url, $model, $key) use ($backTo) {
                        return \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'title' => 'Удалить',
                                'aria-label' => 'Удалить',
                                'data-pjax' => '0',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => Url::to(['/out-invoice/delete', 'id' => $model->id, 'backTo' => $backTo]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить ссылку?',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

    <?php $pjax->end(); ?>
</div>

<?php

$this->registerJs('

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).val()).select();
  document.execCommand("copy");
  $temp.remove();
}

$(".copy-link").bind("click", function(e) {
   e.preventDefault();
   var linkElement = $(this).find("input[name=\'link\']");
   copyToClipboard(linkElement);

   window.toastr.success("Ссылка скопирована в буфер обмена", "", {
        "closeButton": true,
        "showDuration": 1000,
        "hideDuration": 1000,
        "timeOut": 1000,
        "extendedTimeOut": 1000,
        "escapeHtml": false,
    });

});


');