<?php

use yii\helpers\Html;

$this->title = 'Список ссылок на Модуль В2В оплат';
?>

<?= $this->render('_style') ?>
<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 5, 'company' => $company]) ?>
        <div class="col-xs-12 col-lg-12 pad0">

            <?= $this->render('parts_module/links_table', [
                'searchModel' => $outInvoiceSearch,
                'dataProvider' => $outInvoiceProvider,
                'backTo' => 'B2B',
                'isPaid' => $isPaid
            ]); ?>
            <?php /* if (Yii::$app->user->identity->company->canDonateWidget()) : ?>
                <div style="padding-top: 25px;">
                    <?= $this->render('@frontend/modules/donate/views/donate-widget/index', [
                        'searchModel' => $donateWidgetSearch,
                        'dataProvider' => $donateWidgetProvider,
                        'backTo' => '/b2b/module',
                    ]); ?>
                </div>
            <?php endif */ ?>

        </div>

        <!-- buttons -->
        <div id="buttons-bar-fixed">
            <div class="col-xs-12 pad0 buttons-block">
                <?= Html::a('Назад', ['services'], [
                    'class' => 'btn darkblue',
                    'style' => 'min-width: 180px;'
                ]) ?>
            </div>
        </div>

    </div>
</div>

<?= $this->render('_pay_modal', [
    'company' => $company
]); ?>

<?php if (!$isPaid && Yii::$app->request->get('modal')) {
    $this->registerJs("$('.b2b-pay-panel').modal('show');");
} ?>