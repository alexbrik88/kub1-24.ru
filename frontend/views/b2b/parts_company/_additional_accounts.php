<?php

use common\models\company\CheckingAccountant;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\Company */
/* @var $account common\models\company\CheckingAccountant[] */

$hasCount = count($accounts);
$newAccounts = [];
for ($i=0; $i < 10; $i++) {
    $newAccounts[$hasCount + $i] = new CheckingAccountant([
        'company_id' => $model->id,
        'type' => CheckingAccountant::TYPE_ADDITIONAL,
    ]);
}
?>

<div id="company-accounts-list">
    <?php foreach ($accounts as $key => $account) : ?>
        <?php $css = $key == 0 ? 'show-first-account-number' . ($hasCount == 1 ? ' hidden' : '') : ''; ?>

        <div class="company-accounts-item">
            <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
                <span class="caption">
                    Расчетный счет
                    <span class="<?= $css ?>">
                        №<?= $key + 1 ?>
                    </span>
                </span>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bik")->textInput([
                            'maxlength' => true,
                            'class' => 'form-control input-sm dictionary-bik',
                            'data' => [
                                'url' => Url::to(['/dictionary/bik']),
                                'target-name' => '#' . Html::getInputId($account, "[$key]bank_name"),
                                'target-city' => '#' . Html::getInputId($account, "[$key]bank_city"),
                                'target-ks' => '#' . Html::getInputId($account, "[$key]ks"),
                                'target-rs' => '#' . Html::getInputId($account, "[$key]rs"),
                            ]
                        ])->label('БИК вашего банка') ?>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]rs")->textInput(['maxlength' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bank_name")->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]ks")->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bank_city")->textInput([
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php endforeach ?>
    <?php foreach ($newAccounts as $key => $account) : ?>
        <div class="company-accounts-item"   style="display: none">
            <div class="col-xs-12" style="margin-top: 15px;margin-bottom: 10px;">
                <span class="caption">
                    Расчетный счет
                    <span class="">
                        №<?= $key + 1 ?>
                    </span>
                </span>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bik")->textInput([
                            'disabled' => true,
                            'maxlength' => true,
                            'class' => 'form-control input-sm dictionary-bik',
                            'data' => [
                                'url' => Url::to(['/dictionary/bik']),
                                'target-name' => '#' . Html::getInputId($account, "[$key]bank_name"),
                                'target-city' => '#' . Html::getInputId($account, "[$key]bank_city"),
                                'target-ks' => '#' . Html::getInputId($account, "[$key]ks"),
                                'target-rs' => '#' . Html::getInputId($account, "[$key]rs"),
                            ]
                        ])->label('БИК вашего банка') ?>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]rs")->textInput([
                            'disabled' => true,
                            'maxlength' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bank_name")->textInput([
                            'disabled' => true,
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]ks")->textInput([
                            'disabled' => true,
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <?= $form->field($account, "[$key]bank_city")->textInput([
                            'disabled' => true,
                            'maxlength' => true,
                            'readonly' => true,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    <?php endforeach ?>
</div>

<div class="col-xs-12">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="col-md-6 pad0">
                <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить расчетный счет', [
                    'class' => 'btn darkblue darkblue-invert',
                    'id' => 'add-new-account',
                    'style' => 'margin-top:10px'
                ]); ?>
            </div>
            <div class="col-md-6">

            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            &nbsp;
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php
$this->registerJs(<<<JS
    $('#add-new-account').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        var item = $('#company-accounts-list .company-accounts-item:hidden').first();
        $('input', item).prop('disabled', false);
        item.show(250);
    });
JS
);
?>
