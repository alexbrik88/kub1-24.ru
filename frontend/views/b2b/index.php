<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.01.2019
 * Time: 18:34
 */

use frontend\models\OutInvoiceSearch;
use yii\data\ActiveDataProvider;
use frontend\modules\donate\models\DonateWidgetSearch;

/* @var $this yii\web\View
 * @var $outInvoiceSearch OutInvoiceSearch
 * @var $outInvoiceProvider ActiveDataProvider
 * @var $donateWidgetSearch DonateWidgetSearch
 * @var $donateWidgetProvider ActiveDataProvider
 * @var $activeTab integer
 */

$this->title = 'Модуль В2В';
?>
<div class="b2b-index">
    <?= $this->render('/contractor/_partial/expose_invoice_module', [
        'activeTab' => $activeTab,
        'outInvoiceSearch' => $outInvoiceSearch,
        'outInvoiceProvider' => $outInvoiceProvider,
        'donateWidgetSearch' => $donateWidgetSearch,
        'donateWidgetProvider' => $donateWidgetProvider,
    ]); ?>
</div>
