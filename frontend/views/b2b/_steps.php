<?php

use yii\helpers\Url;
use common\models\company\CompanyType;
use common\models\Company;

function getStepClass($itemStep, $step) {
    return $itemStep < $step ? 'finish' : ($itemStep == $step ? 'active' : '');
}
?>

<div class="sbs-menu">
    <!-- Step 1 -->
    <a class="sbs-el sbs-step-1 <?= getStepClass(1, $step) ?>" href="<?= Url::to(['company']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-gear">
                </td>
                <td class="text-block">
                    Реквизиты вашего <?= $company->companyType->name_short ?>
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 2 -->
    <a class="sbs-el sbs-step-2 <?= getStepClass(2, $step) ?>" href="<?= Url::to(['description']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-file-text">
                </td>
                <td class="text-block">
                    Описание модуля B2B
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 3 -->
    <a class="sbs-el sbs-step-3 <?= getStepClass(3, $step) ?>" href="<?= Url::to(['products']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-cubes">
                </td>
                <td class="text-block">
                    Товары
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 4 -->
    <a class="sbs-el sbs-step-4 <?= getStepClass(4, $step) ?>" href="<?= Url::to(['services']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-pencil"></i>
                    <i class="fa fa-wrench fa-pencil-wrench"></i>
                </td>
                <td class="text-block">
                    Услуги
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 5 -->
    <a class="sbs-el sbs-step-5 <?= getStepClass(5, $step) ?>" href="<?= Url::to(['module']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="flaticon-growth">
                </td>
                <td class="text-block">
                    Модуль B2B
                </td>
            </tr>
        </table>
    </a>
</div>