<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\components\XlsHelper;
use frontend\models\Documents;
use frontend\models\ProductPriceForm;
use frontend\rbac\permissions;
use frontend\widgets\TableConfigWidget;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\document\status\OrderDocumentStatus;
use common\models\Company;
use common\components\ImageHelper;
use common\models\product\ProductSearch;
use frontend\widgets\StoreFilterWidget;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use common\models\product\PriceList;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $prompt backend\models\Prompt */
/* @var $defaultSortingAttr string */
/* @var $priceList PriceList */
/* @var $store */
/* @var $user \common\models\employee\Employee */

$this->title = Product::$productionTypes[$productionType];

$userConfig = Yii::$app->user->identity->config;

$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];

$this->context->layoutWrapperCssClass = 'page-good';
$countProduct = Product::find()->byUser()->where(['and',
    ['creator_id' => Yii::$app->user->identity->id],
    ['production_type' => $productionType],
])->count();
$xlsMessageType = $productionType ? 'товаров' : 'услуг';

if ($productionType == 1) {
    $emptyMessage = 'Вы еще не добавили ни одного товара';
} elseif ($productionType == 0) {
    $emptyMessage = 'Вы еще не добавили ни одной услуги';
} else {
    $emptyMessage = 'Ничего не найдено';
}

$canDelete = Yii::$app->user->can(permissions\Product::DELETE);
$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE);

$actionItems = [];
if ($canUpdate) {
    if (count($storeList) > 1 && $productionType == Product::PRODUCTION_TYPE_GOODS) {
        $actionItems[] = [
            'label' => 'Переместить на склад',
            'url' => '#move_to_store_modal',
            'linkOptions' => [
                'data-toggle' => 'modal',
            ],
        ];
    }
    //$actionItems[] = [
    //    'label' => 'Переместить в группу',
    //    'url' => '#move_to_group_modal',
    //    'linkOptions' => [
    //        'data-toggle' => 'modal',
    //    ],
    //];
    $actionItems[] = [
        'label' => 'Изменить цену',
        'url' => '#many-price-modal',
        'linkOptions' => [
            'data-toggle' => 'modal',
        ],
    ];
    //if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    //    $actionItems[] = [
    //        'label' => 'Объединить товары',
    //        'url' => '#combine_to_one_modal',
    //        'linkOptions' => [
    //            'data-toggle' => 'modal',
    //        ],
    //    ];
    //}
    if ((int)$searchModel->filterStatus !== ProductSearch::IN_ARCHIVE) {
        $actionItems[] = [
            'label' => 'Переместить в архив',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-archive',
                'data' => [
                    'url' => '/product/to-archive',
                ],
            ],
        ];
    } else {
        $actionItems[] = [
            'label' => 'Извлечь из архива',
            'url' => 'javascript:;',
            'linkOptions' => [
                'id' => 'many-to-store-from-archive',
                'data' => [
                    'url' => '/product/to-store-from-archive',
                ],
            ],
        ];
    }
}
$filterStatusItems[ProductSearch::IN_WORK] = 'Только в работе';
if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $filterStatusItems[ProductSearch::IN_RESERVE] = 'Только в резерве';
}
$filterStatusItems[ProductSearch::IN_ARCHIVE] = 'Только архивные';
$filterStatusItems[ProductSearch::ALL] = 'Все';

$hasFilters = !($searchModel->filterStatus == ProductSearch::IN_WORK) ||
    (bool)$searchModel->filterComment ||
    (bool)$searchModel->filterImage ||
    (bool)$searchModel->filterDate;

if ($productionType == Product::PRODUCTION_TYPE_GOODS) {
    $tableConfigItems = [
        [
            'attribute' => 'product_article',
        ],
        [
            'attribute' => 'product_group',
            'label' => 'Группа товара',
        ],
        [
            'attribute' => 'product_image',
        ],
        [
            'attribute' => 'product_comment',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа товара',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
} else {
    $tableConfigItems = [
        [
            'attribute' => 'product_group',
            'label' => 'Группа услуги',
        ],
    ];
    $sortingItems = [
        [
            'attribute' => 'title',
            'label' => 'Наименование',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_TITLE,
        ],
        [
            'attribute' => 'group_id',
            'label' => 'Группа услуги',
            'checked' => $defaultSortingAttr == ProductSearch::DEFAULT_SORTING_GROUP_ID,
        ],
    ];
}

$this->registerJs('
    $(document).on("change", "input.select-on-check-all", function(e) {
        $("input.product_checker").prop("checked", this.checked).uniform("refresh").trigger("change");
    });
    $(document).on("change", "input.product_checker", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#mass_actions").fadeIn();
        } else {
            $("#mass_actions").fadeOut();
        }
    });
    $(document).on("click", "#confirm-many-delete", function(e) {
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
    $(".tooltip-help").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "side": "right",
        "contentAsHTML": true,
    });
');

$taxationType = $company->companyTaxationType;
?>

<?= $this->render('_style') ?>
<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => $step, 'company' => $company]) ?>
        <div class="col-xs-12 col-lg-12 pad0">

            <input type="hidden" id="is_b2b" value="1">
            <input type="hidden" id="production_type" value="<?= $productionType ?>">

            <div class="portlet box">
                <span style="font-size:16px; font-weight: bold"><?= Product::$productionTypes[$productionType] ?></span>
                <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
                    <div class="btn-group pull-right title-buttons" style="margin-left: 15px;">
                        <span class="btn yellow add-modal-new-product">
                            <i class="fa fa-plus"></i>
                            ДОБАВИТЬ
                        </span>
                        <?= Html::button('<i class="fa fa-download"></i> Загрузить из Excel', [
                            'class' => 'btn yellow w100proc no-padding pull-right',
                            'style' => 'width:200px;margin-left:15px;',
                            'data' => [
                                'toggle' => 'modal',
                                'target' => '#import-xls',
                            ],
                        ]); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row" style="margin-top:37px">
                <div class="col-sm-12 table-icons" style="margin-top: -12px;">
                    <?= TableConfigWidget::widget([
                        'items' => $tableConfigItems,
                        'sortingItems' => $sortingItems,
                    ]); ?>
                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                        <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['/product/get-xls'], ['productionType' => Product::PRODUCTION_TYPE_GOODS] + Yii::$app->request->queryParams), [
                            'class' => 'get-xls-link pull-right',
                            'title' => 'Скачать в Excel',
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption caption_for_input">
                        <?= $tableHeader[$productionType] . ': ' . $dataProvider->totalCount; ?>
                    </div>
                    <div class="tools search-tools tools_button col-sm-4">
                        <div class="form-body">
                            <?php $form = ActiveForm::begin([
                                'method' => 'GET',
                                'action' => Url::current([$searchModel->formName() => null]),
                                'fieldConfig' => [
                                    'template' => "{input}\n{error}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ],
                            ]); ?>
                            <div class="search_cont">
                                <div class="wimax_input" style="width: 100%!important;padding-right: 20px;float: right;">
                                    <?= $form->field($searchModel, 'title')->textInput([
                                        'placeholder' => 'Поиск...',
                                    ]); ?>
                                </div>
                                <div class="wimax_button">
                                    <?= Html::submitButton('НАЙТИ', [
                                        'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                                    ]) ?>
                                </div>
                            </div>
                            <?php $form->end(); ?>
                        </div>
                    </div>
                    <div id="mass_actions" class="actions joint-operations col-sm-5"
                         style="float:left;display: none;width: 25%;">
                        <?php if ($canUpdate) : ?>
                            <?= $this->render('parts_products/_many_price', [
                                'company' => $company,
                                'productionType' => $productionType,
                            ]) ?>
                        <?php endif ?>
                        <?php if ($actionItems) : ?>
                            <div class="pull-left" style="position: relative; display: inline-block;">
                                <div class="dropdown">
                                    <?= Html::a('Действия  <span class="caret"></span>', null, [
                                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                                        'data-toggle' => 'dropdown',
                                        'style' => 'height: 28px; color: #def1ff;',
                                    ]); ?>
                                    <?= Dropdown::widget([
                                        'items' => $actionItems,
                                        'options' => [
                                            'style' => 'right: -30px !important; left: auto; top: 25px;',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        <?php endif ?>
                        <?php if ($canDelete) : ?>
                            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                                'class' => 'btn btn-default btn-sm pull-left',
                                'data-toggle' => 'modal',
                            ]); ?>
                            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                                 tabindex="-1" aria-hidden="true"
                                 style="display: none; margin-top: -51.5px;">
                                <div class="modal-dialog ">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="form-body">
                                                <div class="row">
                                                    Вы уверены, что хотите удалить выбранные
                                                    <?= $productionType ? 'товары' : 'услуги'; ?>?
                                                </div>
                                            </div>
                                            <div class="form-actions row">
                                                <div class="col-xs-6">
                                                    <?= Html::tag('button', '<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', [
                                                        'id' => 'confirm-many-delete',
                                                        'class' => 'btn darkblue pull-right ladda-button',
                                                        'data-action' => Url::to(['/product/many-delete', 'productionType' => $productionType]),
                                                        'data-style' => 'expand-right',
                                                    ]); ?>
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn darkblue" data-dismiss="modal">
                                                        НЕТ
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="filter-block pull-right">
                        <div class="dropdown pull-right">
                            <?= Html::a('Фильтр  <span class="caret"></span>', null, [
                                'class' => 'btn btn-default btn-sm dropdown-toggle',
                                'data-toggle' => 'dropdown',
                                'style' => $hasFilters ? 'color: #f3565d!important;border-color: #f3565d!important;' : 'color: #def1ff;',
                            ]); ?>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="row">
                                        <span class="filter-label">Показывать</span>
                                        <?= Html::activeDropDownList($searchModel, 'filterStatus', $filterStatusItems, [
                                            'class' => 'form-control',
                                            'data-id' => 'filterStatus',
                                        ]); ?>
                                    </div>
                                </li>
                                <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS): ?>
                                    <li>
                                        <div class="row text-right">
                                            <span class="filter-label">Наличие картинки</span>
                                            <?= Html::activeDropDownList($searchModel, 'filterImage', [
                                                ProductSearch::ALL => 'Все',
                                                ProductSearch::HAS_IMAGE => 'Есть',
                                                ProductSearch::NO_IMAGE => 'Нет',
                                            ], [
                                                'class' => 'form-control',
                                                'data-id' => 'filterImage',
                                            ]); ?>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row text-right">
                                            <span class="filter-label">Наличие описания</span>
                                            <?= Html::activeDropDownList($searchModel, 'filterComment', [
                                                ProductSearch::ALL => 'Все',
                                                ProductSearch::HAS_COMMENT => 'Есть',
                                                ProductSearch::NO_COMMENT => 'Нет',
                                            ], [
                                                'class' => 'form-control',
                                                'data-id' => 'filterComment',
                                            ]); ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <li style="border-bottom: 0;">
                                    <div class="row text-right">
                                        <span class="filter-label">Добавлен</span>
                                        <?= Html::activeDropDownList($searchModel, 'filterDate', [
                                            ProductSearch::ALL => 'Все',
                                            ProductSearch::FILTER_YESTERDAY => 'Вчера',
                                            ProductSearch::FILTER_TODAY => 'Сегодня',
                                            ProductSearch::FILTER_WEEK => 'Неделя',
                                            ProductSearch::FILTER_MONTH => 'Месяц',
                                        ], [
                                            'class' => 'form-control',
                                            'data-id' => 'filterDate',
                                            'data-url' => Url::to(['filter-date', 'actionType' => 'set']),
                                        ]); ?>
                                        <?= Html::activeHiddenInput($searchModel, 'dateFrom', [
                                            'data-id' => 'dateFrom',
                                        ]); ?>
                                        <?= Html::activeHiddenInput($searchModel, 'dateTo', [
                                            'data-id' => 'dateTo',
                                        ]); ?>
                                    </div>
                                    <div class="row block-date"
                                         style="display: <?= $searchModel->filterDate == ProductSearch::ALL ? 'none;' : 'block;'; ?>">
                                        <span class="filter-label"></span>
                                        <div class="change-period-date">
                                                    <span class="glyphicon glyphicon-chevron-left pull-left minus"
                                                          data-url="<?= Url::to(['filter-date', 'actionType' => 'minus']); ?>"></span>
                                            <span class="date-text">
                                                        <?php if (in_array($searchModel->filterDate, [ProductSearch::FILTER_YESTERDAY, ProductSearch::FILTER_TODAY])): ?>
                                                            <?= $searchModel->dateFrom; ?>
                                                        <?php elseif ($searchModel->filterDate == ProductSearch::FILTER_WEEK): ?>
                                                            <?= "{$searchModel->dateFrom} - {$searchModel->dateTo}"; ?>
                                                        <?php else: ?>
                                                            <?= FlowOfFundsReportSearch::$month[date('m', strtotime($searchModel->dateFrom))] . ' ' . date('Y', strtotime($searchModel->dateFrom)); ?>
                                                        <?php endif; ?>
                                                    </span>
                                            <span class="glyphicon glyphicon-chevron-right pull-right plus"
                                                  data-url="<?= Url::to(['filter-date', 'actionType' => 'plus']); ?>"></span>
                                        </div>
                                    </div>
                                </li>
                                <li style="border-bottom: 0;">
                                    <div class="form-actions">
                                        <div class="row action-buttons buttons-fixed">
                                            <div class="spinner-button col-sm-3 col-xs-3"
                                                 style="width: 35%;padding-left: 0;">
                                                <?= Html::button('Найти', [
                                                    'class' => 'apply-filters btn darkblue widthe-100',
                                                ]); ?>
                                            </div>
                                            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 30%;"></div>
                                            <div class="spinner-button col-sm-1 col-xs-1"
                                                 style="width: 35%;padding-right: 0;">
                                                <?= Html::button('Сбросить', [
                                                    'class' => 'apply-default-filters btn darkblue widthe-100',
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <?php if ($canUpdate && ($taxationType->osno || $taxationType->usn)): ?>
                            <div class="pull-right" style="padding-right: 15px;">
                                <?= $this->render('parts_products/_nds_form', [
                                    'toggleButton' => [
                                        'label' => 'Изменить НДС',
                                        'class' => 'btn btn-sm pull-right',
                                        'style' => 'padding: 4px 9px 3px; background: transparent; border: 1px solid #def1ff; color: #def1ff;',
                                    ]
                                ]) ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="portlet-body accounts-list">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'b2b_products_list',
                    ]); ?>

                    <?= Html::beginForm(null, 'POST', ['id' => 'product_checker_form']); ?>
                    <div class="table-container products-table clearfix" style="">
                        <?= GridView::widget([
                            'id' => 'product-grid',
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'emptyText' => $emptyMessage,
                            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                                'id' => 'datatable_ajax',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],
                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],
                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],
                            'rowOptions' => [
                                'role' => 'row',
                            ],
                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                            'columns' => [
                                [
                                    'class' => 'yii\grid\CheckboxColumn',
                                    'cssClass' => 'product_checker',
                                    'headerOptions' => [
                                        'style' => 'width: 20px;',
                                    ],
                                ],
                                [
                                    'attribute' => 'article',
                                    'headerOptions' => [
                                        'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                                            'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                                    ],
                                    'contentOptions' => [
                                        'class' => $productionType == Product::PRODUCTION_TYPE_SERVICE ? 'hidden' :
                                            'col_product_article' . ($userConfig->product_article ? '' : ' hidden'),
                                    ],
                                ],
                                [
                                    'attribute' => 'title',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'col_product_name',
                                    ],
                                    'format' => 'raw',
                                    'value' => function (Product $data) use ($productionType) {
                                        if (Yii::$app->user->can(frontend\rbac\permissions\Product::VIEW)) {
                                            $content = Html::a(Html::encode($data->title), [
                                                '/product/view',
                                                'productionType' => $productionType,
                                                'id' => $data->id,
                                            ]);
                                        } else {
                                            $content = Html::encode($data->title);
                                        }

                                        return Html::tag('div', $content, ['class' => 'product-title-cell']);
                                    },
                                ],
                                [
                                    'attribute' => 'group_id',
                                    'class' => DropDownSearchDataColumn::className(),
                                    'enableSorting' => false,
                                    'label' => 'Группа ' . ($productionType ? 'товара' : 'услуги'),
                                    'headerOptions' => [
                                        'class' => 'dropdown-filter col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                                        'width' => '14%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'col_product_group' . ($userConfig->product_group ? '' : ' hidden'),
                                    ],
                                    'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(null, $productionType), 'id', 'title')),
                                    'format' => 'raw',
                                    'value' => 'group.title',
                                ],
                                [
                                    'attribute' => 'available',
                                    'label' => 'Доступно',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->available * 1;
                                    },
                                    'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                                ],
                                [
                                    'attribute' => 'reserve',
                                    'label' => 'Резерв',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        $tooltipId = 'product-reserve-tooltip-' . $data->id;
                                        $content = Html::a($data->reserve * 1, null, [
                                            'class' => 'product-reserve-value',
                                            'data' => [
                                                'tooltip-content' => '#' . $tooltipId,
                                                'url' => Url::to(['/product/reserve-documents', 'id' => $data->id]),
                                            ],
                                        ]);
                                        $content .= Html::tag('span', Html::tag('span', '', ['id' => $tooltipId]), [
                                            'class' => 'hidden',
                                        ]);

                                        return $content;
                                    },
                                    'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                                ],
                                [
                                    'attribute' => 'quantity',
                                    'label' => 'Остаток',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->quantity * 1;
                                    },
                                    'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                                ],
                                [
                                    'attribute' => 'product_unit_id',
                                    'class' => DropDownSearchDataColumn::className(),
                                    'enableSorting' => false,
                                    'label' => 'Ед. измерения',
                                    'headerOptions' => [
                                        'class' => 'dropdown-filter',
                                        'width' => '5%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'col_product_unit',
                                    ],
                                    'filter' => [null => 'Все'] + ArrayHelper::map(ProductUnit::getUnits(), 'id', 'name'),
                                    'format' => 'raw',
                                    'value' => 'productUnit.name',
                                ],
                                [
                                    'label' => 'Картинка',
                                    'headerOptions' => [
                                        'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                                        'width' => '30px',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'col_product_image' . ($userConfig->product_image ? '' : ' hidden'),
                                    ],
                                    'format' => 'raw',
                                    'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                                    'value' => function (Product $data) {
                                        $content = '';
                                        if ($thumb = $data->getImageThumb(200, 300)) {
                                            $tooltipId = 'tooltip-product-image-' . $data->id;
                                            $content = Html::tag('span', '', [
                                                'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                                                'data-tooltip-content' => '#' . $tooltipId,
                                                'style' => 'cursor: pointer;',
                                            ]);
                                            $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                            $content .= Html::beginTag('div', ['id' => $tooltipId]);
                                            $content .= Html::img($thumb, ['alt' => '']);
                                            $content .= Html::endTag('div');
                                            $content .= Html::endTag('div');
                                        }

                                        return $content;
                                    },
                                ],
                                [
                                    'attribute' => 'comment_photo',
                                    'label' => 'Описание',
                                    'headerOptions' => [
                                        'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                                        'width' => '25%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'col_product_comment' . ($userConfig->product_comment ? '' : ' hidden'),
                                    ],
                                    'format' => 'raw',
                                    'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                                    'value' => function ($data) {
                                        $content = '';
                                        if ($data->comment_photo) {
                                            $tooltipId = 'tooltip-product-comment-' . $data->id;
                                            $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                                                'class' => 'product-comment-box',
                                                'data-tooltip-content' => '#' . $tooltipId,
                                                'style' => 'cursor: pointer;'
                                            ]);
                                            $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                            $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                                            $content .= Html::encode($data->comment_photo);
                                            $content .= Html::endTag('div');
                                            $content .= Html::endTag('div');
                                        }

                                        return $content;
                                    },
                                ],
                                [
                                    'attribute' => 'price_for_buy_with_nds',
                                    'label' => 'Цена покупки',
                                    'headerOptions' => [
                                        'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) use ($user) {
                                        $currentEmployeeCompany = $user->currentEmployeeCompany;
                                        $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                                            (!$currentEmployeeCompany->can_view_price_for_buy &&
                                                $currentEmployeeCompany->is_product_admin &&
                                                $data->creator_id == $currentEmployeeCompany->employee_id);

                                        return $canViewPriceForBuy ?
                                            TextHelper::invoiceMoneyFormat($data->price_for_buy_with_nds, 2) :
                                            Product::DEFAULT_VALUE;
                                    },
                                ],
                                [
                                    'attribute' => 'price_for_buy_nds_id',
                                    'label' => 'НДС покупки',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => 'priceForBuyNds.name',
                                    'visible' => Yii::$app->user->identity->company->hasNds(),
                                ],
                                [
                                    'attribute' => 'amountForBuy',
                                    'label' => 'Стоимость покупки',
                                    'headerOptions' => [
                                        'class' => $user->currentEmployeeCompany->can_view_price_for_buy ? 'sorting' : null,
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) use ($user) {
                                        $currentEmployeeCompany = $user->currentEmployeeCompany;
                                        $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                                            (!$currentEmployeeCompany->can_view_price_for_buy &&
                                                $currentEmployeeCompany->is_product_admin &&
                                                $data->creator_id == $currentEmployeeCompany->employee_id);

                                        return $canViewPriceForBuy ?
                                            TextHelper::invoiceMoneyFormat($data->amountForBuy, 2) :
                                            Product::DEFAULT_VALUE;
                                    },
                                    'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                                ],
                                [
                                    'attribute' => 'price_for_sell_with_nds',
                                    'label' => 'Цена продажи',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->not_for_sale ? 'Не для продажи' :
                                            TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
                                    },
                                ],
                                [
                                    'attribute' => 'price_for_sell_nds_id',
                                    'label' => 'НДС продажи',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return $data->not_for_sale ? 'Не для продажи' : ($data->priceForSellNds ? $data->priceForSellNds->name : '---');
                                    },
                                    'visible' => Yii::$app->user->identity->company->hasNds(),
                                ],
                                [
                                    'attribute' => 'amountForSell',
                                    'label' => 'Стоимость продажи',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($data) {
                                        return TextHelper::invoiceMoneyFormat($data->amountForSell, 2);
                                    },
                                    'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                                ],
                            ],
                        ]); ?>
                    </div>
                    <?= Html::endForm(); ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>

            </div>

        </div>

        <!-- buttons -->
        <div id="buttons-bar-fixed">
            <div class="col-xs-12 pad0 buttons-block">
                <?= Html::a('Назад', ['description'], [
                    'class' => 'btn darkblue',
                    'style' => 'min-width: 180px;'
                ]) ?>
                <?= Html::a('Далее', [(Yii::$app->controller->action->id == 'products') ? 'services' : 'module'], [
                    'class' => 'btn darkblue pull-right',
                    'style' => 'min-width: 180px;'
                ]); ?>
            </div>
        </div>

    </div>
</div>

<?php
if (Yii::$app->user->can(permissions\Product::CREATE)) {
    echo $this->render('/xls/_import_xls', [
        'header' => 'Загрузка ' . $xlsMessageType . ' из Excel',
        'text' => '<p>Для загрузки списка ' . $xlsMessageType . ' из Excel,
                   <br>
                   заполните шаблон таблицы и загрузите ее тут.
                   </p>',
        'formData' => Html::hiddenInput('className', 'Product') . Html::hiddenInput('Product[production_type]', $productionType),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => $productionType ? XlsHelper::PRODUCT_GOODS : XlsHelper::PRODUCT_SERVICES]),
    ]);

    if (Yii::$app->request->get('modal')) {
        $this->registerJs('
            $("#import-xls").modal();
            window.history.replaceState(null, null, window.location.pathname);
        ');
    }
}

$this->registerJs('
    $(".preview-product-photo").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "side": "right",
        "contentAsHTML": true
    });
    $(".product-comment-box").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "hover",
        "contentAsHTML": true
    });
    $(document).ready(function () {
        $("#product-grid tbody tr").each(function() {
            var $row = $(this);
            var height = $(".product-title-cell", $row).height();
            $(".product-comment-box", $row).css("height", height + "px");
            /*$(".product-comment-box", $row).pseudo(":before", "height", height + "px");*/
        });
    });
    $(document).on("click", ".product-reserve-value:not(.tooltipstered)", function() {
        var reserve = $(this);
        var tooltipId = reserve.data("tooltip-content")
        $.ajax({
            url: reserve.data("url"),
            success: function(data) {
                if (data.result) {
                    $(reserve.data("tooltip-content")).html(data.result);
                    reserve.tooltipster({
                        theme: "tooltipster-kub",
                        trigger: "click",
                        contentCloning: true
                    });
                    reserve.tooltipster("show");
                }
            }
        })
    });
    $("#config-product_group").change(function () {
        if (!$(this).is(":checked")) {
            $(".sorting-table-config-item#title").click().uniform();
        }
    });
    $(document).on("click", "a#many-to-archive, a#many-to-store-from-archive", function() {
        $.ajax({
            type: "post",
            url: $(this).data("url"),
            data: {
                product_id: $("input.product_checker:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(),
            },
            success: function() {
                location.reload();
            }
        });
    });
    $(".filter-block ul.dropdown-menu").click(function(e) {
        if ($(e.target).hasClass("plus") || $(e.target).hasClass("minus")) {
            changePeriod($("#productsearch-filterdate").val(), $(e.target).data("url"));
        }
        e.stopPropagation();
    });
    $(".filter-block .apply-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select, .filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = $(this).val();
        });
        applyProductFilter($filterData);
    });
    $(".filter-block .apply-default-filters").click(function () {
        var $filterData = [];

        $(".filter-block ul.dropdown-menu select").each(function () {
            var $attrName = $(this).data("id");
            var $value = 0;
            if ($attrName == "filterStatus") {
                $value = 2;
            }
            $filterData["ProductSearch[" + $attrName + "]"] = $value;
        });
        $(".filter-block ul.dropdown-menu input").each(function () {
            $filterData["ProductSearch[" + $(this).data("id") + "]"] = "";
        });
        applyProductFilter($filterData);
    });

    function applyProductFilter(data) {
        var kvp = document.location.search.substr(1).split("&");
        for (var key in data) {
            var $key = key;
            var $value = data[key];
            if (kvp !== "") {
                var i = kvp.length;
                var x;
                while (i--) {
                    x = kvp[i].split("=");
                    if (x[0] == $key) {
                        x[1] = $value;
                        kvp[i] = x.join("=");
                        break;
                    }
                }
                if (i < 0) {
                    kvp[kvp.length] = [$key, $value].join("=");
                }
            } else {
                kvp[0] = $key + "=" + $value;
            }
        }
        document.location.search = kvp.join("&");
    }

    $(document).on("change", "#productsearch-filterdate", function (e) {
        changePeriod($(this).val(), $(this).data("url"));
    });

    function changePeriod(dateType, url) {
        var dateFrom = $("#productsearch-datefrom");
        var dateTo = $("#productsearch-dateto");
        var dateText = $(".block-date .date-text");
        var blockDate = $(".block-date");

        if (dateType == 0) {
            dateFrom.val("");
            dateTo.val("");
            dateText.text("");
            blockDate.hide();
        } else {
            $.post(url, {
                date_type: dateType,
                date_from: dateFrom.val(),
                date_to: dateTo.val()
            }, function (data) {
                dateFrom.val(data.dateFrom);
                dateTo.val(data.dateTo);
                dateText.text(data.dateText);
                blockDate.show();
            });
        }
    }

    // ADD NEW PRODUCT
    $(document).on("click", ".add-modal-new-product", function () {
        var ioType = $("#out-invoice-form").length ? INVOICE.InvoiceOut : INVOICE.documentIoType;
        var document = $("input#create-document").val();
        var form = {
            documentType: 2,
            Product: {production_type: $("#production_type").val(), flag: 1},
            document: document,
            isB2B: 1
        };
        $("#order-add-select, #product-add-select").select2("close");
        INVOICE.addNewProduct("/documents/invoice/add-modal-product", form);

        $("#add-new").on("hidden.bs.modal", function () {
            $.pjax.reload({container: "#b2b_products_list"});
        });

    });

'); ?>

<?php Modal::begin([
    'header' => '<h1>Переместить в группу</h1>',
    'id' => 'move_to_group_modal',
    'options' => [
        'style' => 'width: 470px;',
    ]
]); ?>

<div class="move_to_group">
    <div style="margin-bottom: 10px;">
        Выберите группу, в которую будут перемещены выбранные товары
    </div>

    <?= \frontend\widgets\ProductGroupDropdownWidget::widget([
        'id' => 'product_group_select',
        'name' => 'product_id',
        'productionType' => $productionType,
    ]) ?>

    <div style="margin-top: 30px;">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white move_to_group_apply',
            'style' => 'width: 110px;',
            'data' => [
                'url' => Url::to(['/product/to-group']),
            ],
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'style' => 'width: 110px;',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>