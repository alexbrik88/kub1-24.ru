<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:22
 */

use common\models\company\CheckingAccountant;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $title string */
/* @var $id string */
/* @var $company Company|null */

?>
<div class="modal fade <?= $checkingAccountant->isNewRecord ? 'new-company-rs' : 'exists-company-rs'; ?>" id="<?= $id; ?>" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0"><?= $title ?></h3>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', [
                    'checkingAccountant' => $checkingAccountant,
                    'company' => $company,
                ]); ?>
            </div>
        </div>
    </div>
</div>
