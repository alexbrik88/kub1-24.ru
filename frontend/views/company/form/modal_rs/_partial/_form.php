<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:24
 */

use common\models\company\CheckingAccountant;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Html;
use common\components\widgets\BikTypeahead;
use common\models\Company;

/* @var $checkingAccountant CheckingAccountant */
/* @var $checkingAccountantForm yii\bootstrap\ActiveForm */
/* @var $company Company|null */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$checkingAccountantForm = ActiveForm::begin([
    'action' => $checkingAccountant->isNewRecord ? [
        'create-checking-accountant',
        'companyId' => $company ? $company->id : null
    ] : [
        'update-checking-accountant',
        'id' => $checkingAccountant->id,
        'companyId' => $company ? $company->id : null
    ],
    'options' => [
        'class' => 'form-horizontal form-checking-accountant',
        'id' => 'form-checking-accountant-' . $checkingAccountant->id,
        'is_new_record' => $checkingAccountant->isNewRecord ? 1 : 0,
    ],
    'enableAjaxValidation' => true,
]);
?>
<div class="form-body">
    <?= $checkingAccountantForm->field($checkingAccountant, 'bik', $config)
        ->widget(BikTypeahead::classname(), [
            'remoteUrl' => Url::to(['/dictionary/bik']),
            'related' => [
                '#' . Html::getInputId($checkingAccountant, 'bank_name') => 'name',
                '#' . Html::getInputId($checkingAccountant, 'bank_city') => 'city',
                '#' . Html::getInputId($checkingAccountant, 'ks') => 'ks',
            ],
        ])->textInput(); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'bank_name', $config)->textInput([
        'disabled' => '',
    ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'name', $config)->textInput(); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'bank_city', $config)->textInput([
        'disabled' => '',
    ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'ks', $config)->textInput([
        'disabled' => '',
    ]); ?>

    <?= $checkingAccountantForm->field($checkingAccountant, 'rs', $config)->textInput([
        'maxlength' => true,
    ]); ?>

    <?php if ($checkingAccountant->type !== CheckingAccountant::TYPE_MAIN) : ?>
        <?= $checkingAccountantForm->field($checkingAccountant, 'isMain', $config)->checkbox([], false); ?>

        <?php if (!$checkingAccountant->isNewRecord && count(Yii::$app->user->identity->company->checkingAccountants) !== 1) : ?>
            <?= $checkingAccountantForm->field($checkingAccountant, 'isClosed', $config)->checkbox([], false) ?>
        <?php endif ?>
    <?php endif; ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:59.45%;">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue gray-darkblue widthe-100 hidden-lg back',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $checkingAccountantForm->end(); ?>
