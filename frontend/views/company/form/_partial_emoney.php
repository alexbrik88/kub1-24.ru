<?php

use frontend\models\EmoneySearch;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $inputConfig array */

$searchModel = new EmoneySearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>

<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'emoney-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                        <?= $this->render('@frontend/views/emoney/_table', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'actionVisible' => true,
                        ]) ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-12">
                <div class="btn-group pull-left">
                    <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить E-money', [
                        'class' => 'btn yellow ajax-modal-btn',
                        'data-title' => 'Добавить E-money',
                        'data-url' => Url::to(['/emoney/create']),
                    ]); ?>
                </div>
            </div>
        </div>
        <h4>Для пользователей QIWI Касса</h4>
        <?= $form->field($model, 'qiwi_public_key', $inputConfig)->textInput([
            'maxlength' => true,
        ]); ?>
    </div>
</div>