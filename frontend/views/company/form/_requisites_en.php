<?php

use common\components\date\DateHelper;
use common\components\TextHelper;

/**
 * @var $form
 * @var $inputConfig
 * @var $model
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div style="padding: 15px 15px 0px 15px">
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'name_short_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: KUB',
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name_full_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Limited Liability Company «KUB»',
                    ]); ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'form_legal_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: LLC',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'address_legal_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Russia, Moscow, street, etc.',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'address_actual_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Russia, Moscow, street, etc.',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 left-column">
                    <?= $form->field($model, 'chief_post_name_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Chief Executive Officer',
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'lastname_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petrov',
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'firstname_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petr',
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'patronymic_en', $inputConfig)->textInput([
                        'maxlength' => true,
                        'placeholder' => 'Пример: Petrovich',
                    ]); ?>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-6" style="padding-left:0px">
                <?= $form->field($model, 'not_has_patronymic_en',
                      [
                          'options' => [
                              'class' => 'form-check',
                          ],
                          'labelOptions' => [
                              'class' => 'form-check-label control-label',
                              ],
                          'template' => "{input}\n{label}"
                      ]
                )->checkbox([], false); ?>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
