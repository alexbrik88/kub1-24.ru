<?php

use common\components\image\EasyThumbnailImage;
use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $attr styring */

$attrArray = Company::$imageAttrArray;
$dataArray = Company::$imageDataArray;

$imgPath = $model->getImage($attr, true);
if (!$imgPath) {
    $imgPath = $model->getImage($attr);
}
if (Yii::$app->request->isPjax) {
    $model->{$attr.'Apply'} = Yii::$app->request->get('apply', 0);
}
$fileTypeText = 'Допустимый формат: jpg, jpeg, png.';
$uploadUrl = Url::toRoute(['/company/img-upload', 'id' => $model->id, 'attr' => $attr]);
$width = $dataArray[$attr]['width'];
$height = $dataArray[$attr]['height'];
$dummyText = "Перетащите файл изображения сюда или<br>кликните тут, чтобы выбрать его с вашего компьютера.";
$dummyText .= "<br>{$dataArray[$attr]['dummy_text']}.<br>{$fileTypeText}";

$DZpreviewTemplate = '<div class=\"dz-preview dz-file-preview\">\n<div class=\"dz-image\">\n<img data-dz-thumbnail />\n</div>\n<div class=\"dz-details\">\n<div class=\"dz-size\">\n<span data-dz-size></span>\n</div>\n<div class=\"dz-filename\">\n<span data-dz-name></span>\n</div>\n</div>\n<div class=\"dz-error-message\">\n<span data-dz-errormessage></span>\n</div>\n<div class=\"dz-success-mark\">\n</div>\n<div class=\"dz-error-mark\">\n</div>\n</div>';
?>
<div class="portlet box darkblue" style="margin-bottom: 0;">
    <div class="portlet-body">
        <?php $pjax = Pjax::begin([
            'id' => "{$attr}-pjax-container",
            'enablePushState' => false,
            'timeout' => 5000,
        ]); ?>
        <?php if ($action === 'crop') : ?>
            <?= $this->render('_partial_files_crop', ['model' => $model, 'attr' => $attr]) ?>
        <?php elseif ($action === 'upload' || !is_file($imgPath)) : ?>
            <div id="company_img_<?= $attr ?>" class="dropzone" style="position: relative; padding-bottom: 30px;">
                <div id="progress_<?= $attr ?>" class="progress progress-striped active"
                     style="position: absolute; bottom: 10px; left: 10px; right: 10px; margin: 0; background-color: #fff; display: none;">
                    <div id="progress_bar_<?= $attr ?>" class="progress-bar" role="progressbar" style="width: 0;"></div>
                </div>
            </div>
            <script type="text/javascript">
                Dropzone.autoDiscover = false;
                var dropzone_<?= $attr ?> = new Dropzone("#company_img_<?= $attr ?>", {
                    'paramName': '<?= $attr ?>',
                    'url': '<?= $uploadUrl ?>',
                    'dictDefaultMessage': '<?= $dummyText ?>',
                    'dictInvalidFileType': '<?= $fileTypeText ?>',
                    'maxFilesize': 5,
                    'maxFiles': 1,
                    'uploadMultiple': false,
                    'acceptedFiles': "image/jpeg,image/png",
                    'params': {'<?= \Yii::$app->request->csrfParam ?>': '<?= \Yii::$app->request->getCsrfToken() ?>'},
                    'previewTemplate': "<?= $DZpreviewTemplate ?>"
                });
                dropzone_<?= $attr ?>.on('thumbnail', function (f, d) {
                    $('#progress_<?= $attr ?>').show();
                });
                dropzone_<?= $attr ?>.on('success', function (f, d) {
                    pjaxImgForm('/company/img-form', <?= $model->id ?>, '<?= $attr ?>', 'crop');
                });
                dropzone_<?= $attr ?>.on('error', function (f, d) {
                    alert(d);
                    this.removeAllFiles(true);
                    $('#progress_<?= $attr ?>').hide();
                    $('#progress_bar_<?= $attr ?>').width(0);
                });
                dropzone_<?= $attr ?>.on('totaluploadprogress', function (progress) {
                    $('#progress_bar_<?= $attr ?>').width(progress + '%');
                });
            </script>
        <?php else : ?>
            <span id="del_<?= $attr; ?>" class="btn darkblue btn-sm" title="Редактировать"
                  onclick="pjaxImgForm('/company/img-form', '<?= $model->id; ?>', '<?= $attr; ?>', 'upload')"
                  style="position: absolute;right: 16px;top: 56px;z-index: 11;">
                <i class="icon-pencil"></i>
            </span>
            <div id="<?= $attr ?>-image-edit">
                <table class="company-image-preview">
                    <tr>
                        <td>
                            <?php if (is_file($imgPath)) : ?>
                                <?= EasyThumbnailImage::thumbnailImg($imgPath, $width, $height, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                            <?php else : ?>
                                <div class="col-md-12">
                                    <div class="portlet align-center company_img_dummy"><?= $dataArray[$attr]['dummy_text'] ?></div>
                                </div>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="company-image-edit align-center">
                <?= Html::tag('span', 'Сохранить', [
                    'class' => "btn darkblue text-white",
                    'data-dismiss' => 'modal',
                ]) ?>
            </div>
        <?php endif; ?>
        <?= Html::activeHiddenInput($model, $attr.'Apply', [
            'class' => 'apply-image-input',
        ]) ?>
        <?php $pjax->end(); ?>
    </div>
</div>
