<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;

/* @var $model common\models\Company */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $creating boolean */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width',
    ],
    'inputOptions' => [
        'class' => 'form-control ',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'form-group required';
$inputConfigDetails = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$fioInitialsConfig = [
    'options' => [
        'class' => 'form-group required',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width initial-field',
    ],
];

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-address_legal_city, #company-address_legal_street", function () {
            var city = $.trim($("#company-address_legal_city").val());
            var street = $.trim($("#company-address_legal_street").val());
            if (!empty(city) && !empty(street)) {
                $.post("/company/ifns", {"city": city, "street": street}, function(data) {
                    if (!empty(data.oktmo)) {
                        $("#company-oktmo").val(data.oktmo);
                    }
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>

<div class="edit-profile__info">
    <?= $form->field($model, 'inn', [
        'options' => [
            'class' => 'form-group',
        ],
        'labelOptions' => [
            'class' => 'control-label col-md-8 label-width',
        ],
        'inputOptions' => [
            'class' => 'form-control field-width field-w',
        ],
    ])->textInput([
        'id' => 'self_employed-inn',
        'maxlength' => true,
    ]); ?>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption">Самозанятый</div>
        </div>
        <div class="portlet-body box-fio">
            <?= $form->field($model, 'ip_lastname', $inputConfigRequired)->label('Фамилия')->textInput(); ?>

            <div class="row">
                <div class="column">
                    <?= $form->field($model, 'ip_firstname', $inputConfigRequired)->label('Имя')->textInput(); ?>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <?= $form->field($model, 'ip_patronymic', $inputConfigRequired)->label('Отчество')->textInput([
                        'readonly' => (boolean)$model->has_chief_patronymic,
                    ]); ?>
                </div>
            </div>
            <div style="margin-left: 15px;">
                <?= $form->field($model, 'has_chief_patronymic', ['validateOnChange' => true])->label('Нет отчества')->checkbox(false); ?>
            </div>
        </div>
    </div>

    <?= $this->render('_partial_description', [
        'model' => $model,
        'form' => $form,
        'creating' => $creating,
    ]); ?>
</div>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'id' => 'add_tabs',
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => 'col-xs-3'],
        'items' => [
            [
                'label' => 'Банковские счета',
                'encode' => false,
                'content' => $this->render('_partial_checking_account', [
                    'model' => $model,
                    'form' => $form,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'banks' => $banks,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Кассы',
                'content' => $this->render('_partial_cashbox', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'Склады',
                'content' => $this->render('_partial_store', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'E-money',
                'content' => $this->render('_partial_emoney', [
                    'model' => $model,
                    'form' => $form,
                    'inputConfig' => $inputConfig,
                ]),
            ],
        ],
    ]); ?>
</div>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => 'col-xs-4'],
        'items' => [
            [
                'label' => 'Реквизиты',
                'encode' => false,
                'content' => $this->render('_self_requisites', [
                    'form' => $form,
                    'model' => $model,
                    'inputConfigDetails' => $inputConfigDetails,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Реквизиты на английском',
                'encode' => false,
                'content' => $this->render('_requisites_en', [
                    'form' => $form,
                    'model' => $model,
                    'inputConfig' => [
                        'options' => [
                            'class' => 'form-group',
                        ],
                        'inputOptions' => [
                            'class' => 'form-control',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ]
                ]),
                'active' => false,
            ],
            [
                'label' => 'Реквизиты вашей налоговой',
                'content' => $this->render('_partial_ifns', [
                    'form' => $form,
                    'model' => $model,
                    'ifns' => $ifns,
                ]),
            ],
        ],
    ]); ?>
</div>

<div style="margin-bottom: 20px;">
    <?php echo $this->render('_partial_files', [
        'model' => $model,
    ]); ?>
</div>
