<?php

use common\models\company\ForeignCurrencyAccount;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Html;
use common\models\Company;
use common\models\currency\Currency;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $foreignCurrencyAccountForm yii\bootstrap\ActiveForm */
/* @var $company Company|null */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label bold-text',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$foreignCurrencyAccountForm = ActiveForm::begin([
    'action' => $foreignCurrencyAccount->isNewRecord ? [
        'create-foreign-currency-account',
        'companyId' => $company ? $company->id : null
    ] : [
        'update-foreign-currency-account',
        'id' => $foreignCurrencyAccount->id,
        'companyId' => $company ? $company->id : null
    ],
    'options' => [
        'class' => 'form-foreign-currency-account',
        'id' => 'form-foreign-currency-account-' . $foreignCurrencyAccount->id,
        'is_new_record' => $foreignCurrencyAccount->isNewRecord ? 1 : 0,
        'labelOptions' => [
            'class' => 'control-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'inp_one_line-product',
        ],
    ],
    'enableAjaxValidation' => true,
]);
?>
<br class="form-body">
    <div class="row">
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'rs', ArrayHelper::merge($config, [
                'enableClientValidation' => false,
            ]))->textInput(['placeholder' => 'Пример: 01234567890123456789']); ?>
        </div>
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'currency_id', $config)
                ->dropdownList(Currency::find()->select(['name', 'id'])->indexBy('id')->column(),
                    ['prompt'=>'Выберите валюту']);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'swift', $config)
                ->textInput(['placeholder' => 'Пример: ALFARUMM']); ?>
        </div>
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'bank_name', $config)
                ->textInput(['placeholder' => 'Пример: ALFA-BANK']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'bank_address', $config)
                ->textInput(['placeholder' => 'Пример: 27 Kalanchevskaya str., Moscow, 107078']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 left-column">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'corr_rs',
                [
                    'labelOptions' => [
                        'class' => 'control-label bold-text',
                    ],
                    'horizontalCssClasses' => [
                        'offset' => 'col-xs-offset-6',
                        'wrapper' => 'col-xs-6',
                    ],
                ],
            )->textInput(['placeholder' => 'Пример: 36310481']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'iban', ArrayHelper::merge($config, [
                'enableClientValidation' => false,
            ]))->textInput(); ?>
        </div>
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'corr_swift', $config)
                ->textInput(['placeholder' => 'Пример: CITIUS33']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'corr_bank_name', $config)
                ->textInput(['placeholder' => 'Пример: CITIBANK NA']); ?>
        </div>
        <div class="col-md-12">
            <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'corr_bank_address', $config)
                ->textInput(['placeholder' => 'Пример: 399 Park Avenue, New York, NY 10043, USA']); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
            <?php if ($foreignCurrencyAccount->type !== ForeignCurrencyAccount::TYPE_MAIN) : ?>
                <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'isMain', $config)->checkbox([], false); ?>

                <?php if (!$foreignCurrencyAccount->isNewRecord && count(Yii::$app->user->identity->company->foreignCurrencyAccounts) !== 1) : ?>
                    <?= $foreignCurrencyAccountForm->field($foreignCurrencyAccount, 'isClosed', $config)->checkbox([], false) ?>
                <?php endif ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:59.45%;">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue gray-darkblue widthe-100 hidden-lg back',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $foreignCurrencyAccountForm->end(); ?>
