<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\Company;

/* @var $foreignCurrencyAccount ForeignCurrencyAccount */
/* @var $title string */
/* @var $id string */
/* @var $company Company|null */

?>
<div class="modal fade bd-example-modal-lg <?= $foreignCurrencyAccount->isNewRecord ? 'new-foreign-account' : 'exists-foreign-account'; ?>" id="<?= $id; ?>" tabindex="-1" role="modal"
     aria-hidden="true" style="min-width:720px">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0"><?= $title ?></h3>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', [
                    'foreignCurrencyAccount' => $foreignCurrencyAccount,
                    'company' => $company,
                ]); ?>
            </div>
        </div>
    </div>
</div>
