<?php

use frontend\models\StoreSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new StoreSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

$this->registerJs(<<<JS
$(document).on('submit', '#store-form', function(e){
    var ajaxModal = $('#ajax-modal-box');
    if (ajaxModal && $(this, ajaxModal).length) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#ajax-modal-content', ajaxModal).html(data);
        });
    }
});
JS
);
?>


<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'store-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                    <?= $this->render('/store/_table', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]) ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-left">
                    <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить склад', [
                        'class' => 'btn yellow ajax-modal-btn',
                        'data-title' => 'Добавить склад',
                        'data-url' => Url::to(['/store/create']),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>