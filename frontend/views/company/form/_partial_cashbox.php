<?php

use common\models\company\CheckingAccountant;
use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\models\CashboxSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new CashboxSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

$this->registerJs(<<<JS
$(document).on('submit', '#cashbox-form', function(e){
    var ajaxModal = $('#ajax-modal-box');
    if (ajaxModal && $(this, ajaxModal).length) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#ajax-modal-content', ajaxModal).html(data);
        });
    }
});
JS
);
?>


<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'cashbox-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                    <?= $this->render('/cashbox/_table', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]) ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-left">
                    <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить кассу', [
                        'class' => 'btn yellow ajax-modal-btn',
                        'data-title' => 'Добавить кассу',
                        'data-url' => Url::to(['/cashbox/create']),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>