<?php

use common\components\date\DateHelper;
use common\components\TextHelper;

/**
 * @var $form
 * @var $inputConfig
 * @var $inputConfigRequired
 * @var $model
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'ogrn', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'kpp', $inputConfigRequired)->textInput([
                    'maxlength' => true,
                    'ifns-exist' => $model->getIfns()->exists() ? 'true' : 'false',
                ]); ?>
                <?= $form->field($model, 'okpo', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okud', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfig)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfig)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'okved', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okato', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
            <div class="col-md-6 left-column">
                <?= $form->field($model, 'oktmo', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okogu', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okfs', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'okopf', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfig)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?php $model->tax_authority_registration_date = DateHelper::format($model->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                <?= $form->field($model, 'tax_authority_registration_date', array_merge($inputConfig, [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'col-md-8 control-label label-width',
                        'style' => 'width: 370px;',
                    ],
                    'wrapperOptions' => [
                        'class' => 'col-md-4 field-width inp_one_line',
                        'style' => 'width: 120px;',
                    ],
                ]))->textInput([
                    'class' => 'form-control date-picker m_b',
                ]); ?>
                <?= $form->field($model, 'capital', array_merge($inputConfig, [
                    'options' => [
                        'class' => 'form-group',
                    ],
                    'labelOptions' => [
                        'class' => 'col-md-8 control-label label-width',
                        'style' => 'width: 165px;',
                    ],
                    'inputOptions' => [
                        'class' => 'form-control js_input_to_money_format',
                    ],
                    'wrapperOptions' => [
                        'class' => 'col-md-4 field-width inp_one_line',
                        'style' => 'width: 239px;',
                    ],
                ]))->textInput([
                    'maxlength' => true,
                    'value' => TextHelper::invoiceMoneyFormat($model->capital, 2, '.', ''),
                ]); ?>
            </div>
        </div>
    </div>
</div>
