<?php

use yii\helpers\Html;

/**
 * @var $form
 * @var $model
 * @var $inputConfigDetails
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'egrip', array_merge($inputConfigDetails, [
                    'options' => [
                        'class' => 'form-group',
                    ],
                ]))->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okpo', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okud', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>

                <?= $form->field($model, 'okved', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'address_legal', $inputConfigDetails)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
                <?= $form->field($model, 'address_actual', $inputConfigDetails)->textInput([
                    'placeholder' => 'Индекс, Адрес',
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'oktmo', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'fss', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_ip', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
                <?= $form->field($model, 'pfr_employer', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>

                <div class="form-group field-company-ip_certificate_number">
                    <?= Html::activeLabel($model, 'ip_certificate_number', [
                        'class' => 'col-sm-4 control-label',
                    ]) ?>
                    <div class="col-sm-8">
                        <table>
                            <tr>
                                <td style="padding-right: 10px;">
                                    <?= Html::activeTextInput($model, 'ip_certificate_number', [
                                        'class' => 'form-control',
                                    ]) ?>
                                </td>
                                <td>
                                    <?= Html::activeLabel($model, 'certificateDate') ?>
                                </td>
                                <td style="position: relative;">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'certificateDate', [
                                        'class' => 'form-control date-picker',
                                    ]) ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?= $form->field($model, 'ip_certificate_issued_by', $inputConfigDetails)->textInput([
                    'maxlength' => true,
                ]); ?>
            </div>
        </div>
    </div>
</div>
