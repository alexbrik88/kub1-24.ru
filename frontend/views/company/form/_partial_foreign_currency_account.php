<?php

use common\models\company\ForeignCurrencyAccount;
use common\components\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\widgets\BtnConfirmModalWidget;
use backend\models\Bank;
use common\components\ImageHelper;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $banks Bank[] */

?>
<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'fca-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'swift',
                        'label' => 'SWIFT',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->swift;
                        },
                    ],
                    [
                        'attribute' => 'currency_id',
                        'label' => 'Валюта',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->currency->name;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Счет',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount  $model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return ArrayHelper::getValue($model->typeAccount, $model->type, '');
                        },
                    ],
                    [
                        'class' => \yii\grid\ActionColumn::className(),
                        'template' => '{update} {delete}',
                        'headerOptions' => [
                            'width' => '10%',
                        ],
                        'buttons' => [
                            'update' => function ($url, ForeignCurrencyAccount $data) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#update-foreign-account-' . $data->id, [
                                    'data-toggle' => 'modal',
                                    'data-target'=> '#update-foreign-account-' . $data->id,
                                    'title' => Yii::t('yii', 'Обновить'),
                                    'aria-label' => Yii::t('yii', 'Обновить'),
                                ]);
                            },
                            'delete' => function ($url, ForeignCurrencyAccount $data) {
                                if ($data->type !== ForeignCurrencyAccount::TYPE_MAIN) {
                                    return BtnConfirmModalWidget::widget([
                                        'toggleButton' => [
                                            'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                            'class' => '',
                                            'tag' => 'a',
                                        ],
                                        'confirmUrl' => $url,
                                        'markAsDeleteClass' => 'delete-foreign-currency-account',
                                        'message' => 'Вы уверены, что хотите удалить валютный счет?',
                                    ]);
                                }

                                return '';
                            },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            $url = 0;
                            switch ($action) {
                                case 'update':
                                    $url = 'update';
                                    break;
                                case 'delete':
                                    $url = 'delete-foreign-currency-account';
                                    break;
                            }

                            return Url::to([$url, 'id' => $model->id]);
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
        <div class="portlet box">
            <div class="btn-group">
                <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить существующий валютный счет', [
                    'class' => 'btn yellow',
                    'data-toggle' => 'modal',
                    'data-target'=> '#add-foreign-account',
                    'href' => '#add-foreign-account',
                ]); ?>
            </div>
        </div>
    </div>
</div>
