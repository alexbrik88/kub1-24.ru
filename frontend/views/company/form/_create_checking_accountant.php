<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.07.2016
 * Time: 19:09
 */

use common\models\company\CheckingAccountant;

/* @var $this yii\web\View */
/* @var $model CheckingAccountant */

?>
<div class="bank-create">
    <div class="portlet box">
        <h3 class="page-title">Добавить расчетный счет</h3>
    </div>

    <?= $this->render('_partial/_form', [
        'model' => $model,
    ]) ?>
</div>