<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:24
 */

use backend\models\Bank;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\models\company\ApplicationToBank;
use common\models\Company;

/* @var $bank Bank */
/* @var $checkingAccountantForm yii\bootstrap\ActiveForm */
/* @var $applicationToBank ApplicationToBank */
/* @var $redirectUrl string */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'horizontalCssClasses' => [
        'offset' => 'col-md-offset-4',
        'hint' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$bankForm = ActiveForm::begin([
    'action' => Url::to(['apply-to-bank', 'bankId' => $bank->id, 'redirectAction' => $redirectUrl]),
    'options' => [
        'class' => 'form-horizontal',
        'id' => 'form-apply-bank-' . $bank->id,
    ],
    //'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]);
?>
<div class="form-body">
    <div class="description-bank">
        <?= nl2br($bank->description); ?>
    </div>
    <?= $bankForm->field($applicationToBank, 'company_name', $config); ?>

    <?= $bankForm->field($applicationToBank, 'inn', $config); ?>

    <?= $bankForm->field($applicationToBank, 'kpp', $config); ?>

    <?= $bankForm->field($applicationToBank, 'legal_address', $config); ?>

    <?= $bankForm->field($applicationToBank, 'fio', $config); ?>

    <?= $bankForm->field($applicationToBank, 'contact_phone', $config)->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control contact_phone',
            'placeholder' => '+7(XXX) XXX-XX-XX',
        ],
    ]); ?>

    <?= $bankForm->field($applicationToBank, 'contact_email', $config); ?>

    <div class="license-bank">
        <?= 'Нажимая кнопку «Открыть счет», вы даете свое согласие на ',
            Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank']),
            " и отправку этих данных в банк «{$bank->bank_name}»."; ?>
    </div>
    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-2 col-xs-2">
                <?= Html::submitButton('Открыть счет', [
                    'class' => 'btn darkblue btn-save darkblue hidden-md hidden-sm hidden-xs',
                    'style' => 'width: 120px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue hidden-lg',
                    'title' => 'Открыть счет',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'style' => 'width: 120px!important;',
                ]); ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg back',
                    'title' => 'Отменить',
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php $bankForm->end(); ?>
