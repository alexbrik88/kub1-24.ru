<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 5:22
 */

use backend\models\Bank;
use common\models\company\ApplicationToBank;
use common\models\Company;

/* @var $bank Bank */
/* @var $title string */
/* @var $id string */
/* @var $applicationToBank ApplicationToBank */
/* @var $redirectUrl string */

?>
<div class="modal fade" id="apply-to-the-bank-<?= $bank->id; ?>" tabindex="-1"
     role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Заявка на открытие расчетного счета</h3>
            </div>
            <div class="modal-body">
                <?= $this->render('_partial/_form', [
                    'bank' => $bank,
                    'applicationToBank' => $applicationToBank,
                    'redirectUrl' => $redirectUrl,
                ]); ?>
            </div>
        </div>
    </div>
</div>
