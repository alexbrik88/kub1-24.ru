<?php

use common\models\company\CompanyType;

$house = \common\models\address\AddressHouseType::TYPE_HOUSE;
$flat = \common\models\address\AddressApartmentType::TYPE_APARTMENT;

$typeOOO = CompanyType::TYPE_OOO;
$typeZAO = CompanyType::TYPE_ZAO;
$typePAO = CompanyType::TYPE_PAO;
$typeOAO = CompanyType::TYPE_OAO;
$typeIp = CompanyType::TYPE_IP;

$this->registerJs(<<<JS

var companyType = {
    'ООО' : $typeOOO,
    'ЗАО' : $typeZAO,
    'ПАО' : $typePAO,
    'ОАО' : $typeOAO,
    'ИП': $typeIp
};
var body = $('.form-body');
$('#company-inn').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            $('#company-inn').val(suggestion.data.inn);
            if (suggestion.data.name !== null) {
                body.find('#company-name_short').val(suggestion.data.name.short);
                $('#company-name_full').val(suggestion.data.name.full);
            }
            if (suggestion.data.address) {
                if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                    var address = '';
                    if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                        address += suggestion.data.address.data.postal_code + ', ';
                    }
                    address += suggestion.data.address.value;
                    $('#company-address_legal').val(address);
                    $('#company-address_actual').val(address);
                } else {
                    $('#company-address_legal').val('000000, ' + suggestion.data.address.value);
                    $('#company-address_actual').val('000000, ' + suggestion.data.address.value);
                }
            } else {
                $('#company-address_legal').val('');
                $('#company-address_actual').val('');
            }

            if (suggestion.data.address && suggestion.data.address.data !== null) {
                $('#company-oktmo').val(suggestion.data.address.data.oktmo);
            }

            $('#company-okpo').val(suggestion.data.okpo);
            $('#company-okved').val(suggestion.data.okved);
            if ($('#company-taxregistrationdate').length) {
                $('#company-taxregistrationdate').val(moment(suggestion.data.state.registration_date).format("DD.MM.YYYY"));
            }

            if (companyType[suggestion.data.opf.short] == $typeIp) {
                var nameArray = suggestion.data.name.full.split(' ');
                $('#company-ip_lastname').val(nameArray[0] || '');
                $('#company-ip_firstname').val(nameArray[1] || '');
                $('#company-ip_patronymic').val(nameArray[2] || '');
                $('#company-egrip').val(suggestion.data.ogrn);
            } else {
                body.find('#company-company_type_id').val(companyType[suggestion.data.opf.short]);
                $('#company-kpp').val(suggestion.data.kpp).trigger('change');
                $('#company-ogrn').val(suggestion.data.ogrn);
                if (suggestion.data.management) $('#company-chief_post_name').val(suggestion.data.management.post);
                var nameArray = suggestion.data.management.name.split(' ');
                $('#company-chief_lastname').val(nameArray[0] || '');
                $('#company-chief_firstname').val(nameArray[1] || '');
                $('#company-chief_patronymic').val(nameArray[2] || '');
            }
            if (typeof isTaxRobot !== "undefined") {
                $.each(['ip_lastname', 'ip_firstname', 'ip_patronymic', 'address_legal', 'egrip', 'okved'], function(i,attr) {
                    $('#company-' + attr).addClass('edited').parent().removeClass('has-error');
                });
                if (suggestion.data.address && suggestion.data.address.data !== null) {
                   $('#company-oktmo').val(suggestion.data.address.data.oktmo).addClass('edited').parent().removeClass('has-error').addClass('has-success');
                   $('#company-ifns_ga').val(suggestion.data.address.data.tax_office_legal).addClass('edited').parent().removeClass('has-error').addClass('has-success');
                } else {
                    $('#company-oktmo').val('');
                    $('#company-ifns_ga').val('');
                }
                if (typeof suggestion.data.state !== "undefined") {
                    var registration_date = suggestion.data.state.registration_date;
                    if (registration_date) {
                        $('#company-taxregistrationdate').val(moment(registration_date).format("DD.MM.YYYY")).addClass('edited').parent().removeClass('has-error');
                    }
                } else {
                    $('#company-taxregistrationdate').val('');
                }

            }
        }
    });

JS
);