<?php

use common\assets\JCropAsset;
use common\models\Company;
use devgroup\dropzone\DropZoneAsset;
use yii\bootstrap\Tabs;

DropZoneAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];
JCropAsset::register($this)->jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

if (empty($action)) {
    $action = 'view';
}
?>

<div style="border-top: 1px solid #4276a4;">
    <?= $this->render('_partial_files_tabs_item', [
        'model' => $model,
        'attr' => 'chiefAccountantSignatureImage',
        'action' => $action,
    ]); ?>
</div>
