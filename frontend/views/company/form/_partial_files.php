<?php
/* @var $model common\models\Company */
use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\models\Company;
use yii\helpers\Html;

/* @var $form yii\widgets\ActiveForm */

?>

<div class="row small-boxes">
    <div class="col-md-4">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Логотип компании</div>
            </div>
            <div class="portlet-body">
                <div id="logoImage-image-result">
                    <table class="company-image-preview">
                        <tr>
                            <td>
                                <?php $imgPath = $model->getImage('logoImage', true) ? : $model->getImage('logoImage');
                                if (is_file($imgPath)) : ?>
                                    <?= EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                <?php else : ?>
                                    <div class="col-md-12">
                                        <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 545х185</div>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 20px auto 10px;">
                    <?= Html::button('Добавить логотип', [
                        'class' => 'btn yellow',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-companyImages',
                        'style' => 'margin-right: 20px;',
                        'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab0"]\').tab(\'show\');',
                    ]) ?>

                    <?= is_file($imgPath) ? \yii\helpers\Html::activeCheckbox($model, 'deleteLogoImage', [
                        'label' => 'Удалить',
                        'class' => 'radio-inline p-o radio-padding',
                    ]) : ''; ?>
                </div>
                <div>
                    <?php
                    $modificationDate = ImageHelper::getModificationDate($imgPath);
                    if ($modificationDate) {
                        echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$model->self_employed) : ?>
        <div class="col-md-4">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Печать компании</div>
                </div>
                <div class="portlet-body">
                    <div id="printImage-image-result">
                        <table class="company-image-preview">
                            <tr>
                                <td>
                                    <?php $imgPath = $model->getImage('printImage', true) ? : $model->getImage('printImage');
                                    if (is_file($imgPath)) : ?>
                                        <?= EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                    <?php else : ?>
                                        <div class="col-md-12">
                                            <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 400х400</div>
                                        </div>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="margin: 20px auto 10px;">
                        <?= Html::button('Добавить печать', [
                            'class' => 'btn yellow',
                            'data-toggle' => 'modal',
                            'data-target' => '#modal-companyImages',
                            'style' => 'margin-right: 20px;',
                            'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab1"]\').tab(\'show\');',
                        ]) ?>

                        <?php if (is_file($imgPath)) : ?>
                            <?= Html::activeCheckbox($model, 'deletePrintImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding has_depend',
                                'data-target' => '#deletePrintImageAll_wrap',
                            ]) ?>
                            <div id="deletePrintImageAll_wrap" class="collapse">
                                <?= Html::activeCheckbox($model, 'deletePrintImageAll', [
                                    'label' => 'Удалить из всех документов',
                                    'class' => 'radio-inline p-o radio-padding',
                                    'labelOptions' => [
                                        'style' => 'margin-top: 10px;',
                                    ],
                                ]) ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($imgPath);
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
    <div class="col-md-4">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Подпись руководителя</div>
            </div>
            <div class="portlet-body">
                <div id="chiefSignatureImage-image-result">
                    <table class="company-image-preview">
                        <tr>
                            <td>
                                <?php $imgPath = $model->getImage('chiefSignatureImage', true) ? : $model->getImage('chiefSignatureImage');
                                if (is_file($imgPath)) : ?>
                                    <?= EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                <?php else : ?>
                                    <div class="col-md-12">
                                        <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 165х50</div>
                                    </div>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="margin: 20px auto 10px;">
                    <?= Html::button('Добавить подпись', [
                        'class' => 'btn yellow',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-companyImages',
                        'style' => 'margin-right: 20px;',
                        'onclick' => '$(\'#company-image-tabs a[href="#company-image-tabs-tab2"]\').tab(\'show\');',
                    ]) ?>

                    <?php if (is_file($imgPath)) : ?>
                        <?= Html::activeCheckbox($model, 'deleteChiefSignatureImage', [
                            'label' => 'Удалить',
                            'class' => 'radio-inline p-o radio-padding has_depend',
                            'data-target' => '#deleteChiefSignatureImageAll_wrap',
                        ]) ?>
                        <div id="deleteChiefSignatureImageAll_wrap" class="collapse">
                            <?= Html::activeCheckbox($model, 'deleteChiefSignatureImageAll', [
                                'label' => 'Удалить из всех документов',
                                'class' => 'radio-inline p-o radio-padding',
                                'labelOptions' => [
                                    'style' => 'margin-top: 10px;',
                                ],
                            ]) ?>
                        </div>
                    <?php endif ?>
                </div>
                <div>
                    <?php
                    $modificationDate = ImageHelper::getModificationDate($imgPath);
                    if ($modificationDate) {
                        echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 30px;">
        <?php if ($model->self_employed) : ?>
            Не получается загрузить логотип или подпись?
        <?php else : ?>
            Не получается загрузить логотип, печать или подпись?
        <?php endif ?>
        <br>
        Пришлите нам ваши файлы на
        <a href="mailto:<?= \Yii::$app->params['emailList']['support'] ?>"><?= \Yii::$app->params['emailList']['support'] ?></a>
        и мы самостоятельно загрузим их.
    </div>
</div>

<?php $this->render('_company_update_files'); ?>