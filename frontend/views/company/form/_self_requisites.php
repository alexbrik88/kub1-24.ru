<?php
/**
 * @var $form
 * @var $model
 * @var $inputConfigDetails
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <?= $form->field($model, 'address_legal', $inputConfigDetails)->textInput([
            'placeholder' => 'Индекс, Адрес',
        ]); ?>
    </div>
</div>