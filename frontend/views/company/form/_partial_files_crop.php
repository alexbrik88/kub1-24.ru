<?php

use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;

$dataArray = Company::$imageDataArray;
$tmp = in_array(parse_url(Yii::$app->request->referrer, PHP_URL_PATH), [
    '/company/update',
    '/company/continue-create',
]);
$imgUrl = Url::toRoute(['/company/img-file', 'id' => $model->id, 'attr' => $attr, 'v' => time(), 'tmp' => $tmp ? 1: null]);
$width = $dataArray[$attr]['width'];
$height = $dataArray[$attr]['height'];
$ratio = $height / $width * 100;
?>
<div class="jcrop-image-container">
    <div style="margin-bottom: 10px;"><?= $dataArray[$attr]['crop_text'] ?></div>
    <table class="form-group jcrop-cropper-table" style="width: 100%; vertical-align: top;">
        <tr>
            <td style="width: 70%; padding-right: 10px;">
                <div id="<?= $attr ?>-image-pane" class="jcrop-image-pane">
                    <?php
                    echo Html::hiddenInput($attr . '_x', 0, ['class' => 'coords', 'id' => $attr . '_x']);
                    echo Html::hiddenInput($attr . '_y', 0, ['class' => 'coords', 'id' => $attr . '_y']);
                    echo Html::hiddenInput($attr . '_w', 0, ['class' => 'coords', 'id' => $attr . '_w']);
                    echo Html::hiddenInput($attr . '_h', 0, ['class' => 'coords', 'id' => $attr . '_h']);
                    echo Html::hiddenInput($attr . '_x2', 0, ['class' => 'coords', 'id' => $attr . '_x2']);
                    echo Html::hiddenInput($attr . '_y2', 0, ['class' => 'coords', 'id' => $attr . '_y2']);
                    echo Html::img($imgUrl, [
                        'id' => $attr,
                        'alt' => 'Crop this image',
                        'style' => 'width: 100%; height: auto;'
                    ]);
                    ?>
                </div>
            </td>
            <td style="width: 30%;">
                <div class="jcrop-preview-pane">
                    <div id="<?= $attr ?>-preview-wrapper" class="jcrop-preview-wrapper">
                        <div id="<?= $attr ?>-preview-container" class="jcrop-preview-container" style="padding-top: <?= $ratio ?>%;">
                            <div id="<?= $attr ?>-preview-inner" class="jcrop-preview-inner">
                                <img src="<?= $imgUrl ?>" class="jcrop-preview" alt="Preview" />
                            </div>
                        </div>
                    </div>
                </div>
                <span class="jcrop-preview-text"><?= $dataArray[$attr]['preview_text'] ?></span>
            </td>
        </tr>
    </table>

    <div class="form-group <?= $attr == 'logoImage' ? 'hidden' : '' ?>" id="<?= $attr ?>-apply-box">
        <?= Html::radioList('apply', 0, Company::$applyNewImage, [
            'unselect' => 0,
            'itemOptions' => [
                'class' => 'apply-image-radio',
                'labelOptions' => [
                    'class' => 'control-label',
                    'style' => 'display: block;',
                ]
            ]
        ]); ?>
    </div>

    <div class="jcrop-buttons" id="<?= $attr ?>_buttons">
        <?= Html::tag('span', 'Применить', [
            'id' => "crop_{$attr}",
            'class' => "btn darkblue text-white jcrop-crop",
            'onclick' => "jcrop_ajaxRequest('/company/img-crop', '/company/img-form', '{$model->id}', '{$attr}', this)",
        ]) ?>
        <?= Html::tag('span', 'Отменить', [
            'id' => "cancel_{$attr}",
            'class' => "btn darkblue text-white jcrop-crop pull-right",
            'onclick' => "jcropImageDelete('/company/img-delete', '/company/img-form', '{$model->id}', '{$attr}')",
        ]) ?>
    </div>
    <script type="text/javascript">
        jcrop_initByAttr('<?= $attr ?>');
    </script>
</div>