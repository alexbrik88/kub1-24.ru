<?php

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\widgets\Modal;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\MaskedInput;
use common\components\TextHelper;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $admin boolean */
/* @var $creating boolean */

$inputConfig = $inputConfigRequired = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
$inputConfigRequired['options']['class'] = 'form-group required';
$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-8 label-width',
    ],
    'inputOptions' => [
        'class' => 'form-control field-width field-w inp_one_line_company',
        'placeholder' => 'Автозаполнение по ИНН',
    ],
];

if ($model->strict_mode == Company::ON_STRICT_MODE) {
    $this->registerJs('
        $(document).on("blur", "#company-kpp", function () {
            var kpp = $.trim($(this).val());
            if (kpp.length == 9) {
                $.post("/company/ifns", {"kpp": $(this).val()}, function(data) {
                    if (!empty(data.ifns)) {
                        $("#ifns-ga").val(data.ifns.ga);
                        $("#ifns-gb").val(data.ifns.gb);
                        $("#ifns-g1").val(data.ifns.g1);
                        $("#ifns-g2").val(data.ifns.g2);
                        $("#ifns-g4").val(data.ifns.g4);
                        $("#ifns-g6").val(data.ifns.g6);
                        $("#ifns-g7").val(data.ifns.g7);
                        $("#ifns-g8").val(data.ifns.g8);
                        $("#ifns-g9").val(data.ifns.g9);
                        $("#ifns-g11").val(data.ifns.g11);
                    }
                });
            }
        });
    ');
}
?>


<div class="edit-profile__info">
    <?= $form->field($model, 'inn', $textInputConfig)->textInput([
        'maxlength' => true,
        'placeholder' => 'Автозаполнение реквизитов по ИНН',
    ]); ?>

    <?= $this->render('_partial_description', [
        'model' => $model,
        'form' => $form,
        'creating' => $creating,
    ]); ?>
</div>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'id' => 'add_tabs',
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => 'col-xs-3'],
        'items' => [
            [
                'label' => ' Банковские счета',
                'encode' => false,
                'content' => $this->render('_partial_checking_account', [
                    'model' => $model,
                    'form' => $form,
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'banks' => $banks,
                ]),
                'active' => true,
            ],
            [
                'label' => 'Кассы',
                'content' => $this->render('_partial_cashbox', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'Склады',
                'content' => $this->render('_partial_store', [
                    'model' => $model,
                    'form' => $form,
                ]),
            ],
            [
                'label' => 'E-money',
                'content' => $this->render('_partial_emoney', [
                    'model' => $model,
                    'form' => $form,
                    'inputConfig' => $inputConfig,
                ]),
            ],
        ],
    ]); ?>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Руководитель (для документов)</div>
            </div>
            <div class="portlet-body buh-info-edit">
                <?= $form->field($model, 'chief_post_name', $inputConfigRequired)->label('Должность')->textInput(); ?>
                <?= $form->field($model, 'chief_lastname', $inputConfigRequired)->label('Фамилия')->textInput(); ?>
                <?= $form->field($model, 'chief_firstname', $inputConfigRequired)->label('Имя')->textInput(); ?>
                <?= $form->field($model, 'chief_patronymic', $inputConfigRequired)->label('Отчество')->textInput([
                    'readonly' => (boolean)$model->has_chief_patronymic,
                ]); ?>
                <?= $form->field($model, 'has_chief_patronymic', $inputConfig)->label('Нет отчества')->checkbox(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box box-name darkblue">
            <div class="portlet-title">
                <div class="caption">Главный бухгалтер (для документов)</div>
            </div>
            <div class="portlet-body buh-info-edit">
                <div class="form-group" style="padding-bottom: 5px;">
                    <label class="checkbox-inline match-with-leader">
                        <?= \yii\helpers\Html::activeCheckbox($model, 'chief_is_chief_accountant', [
                            'id' => 'chief_is_chief_accountant_input',
                            'label' => false,
                        ]); ?>
                        совпадает с руководителем
                    </label>
                </div>

                <div class="change-disabled-inputs">
                    <?= $form->field($model, 'chief_accountant_lastname', $inputConfig)->label('Фамилия')->textInput([
                        'disabled' => $model->chief_is_chief_accountant ? true : false,
                    ]); ?>
                    <?= $form->field($model, 'chief_accountant_firstname', $inputConfig)->label('Имя')->textInput([
                        'disabled' => $model->chief_is_chief_accountant ? true : false,
                    ]); ?>
                    <?= $form->field($model, 'chief_accountant_patronymic', $inputConfig)->label('Отчество')->textInput([
                        'disabled' => $model->chief_is_chief_accountant ? true : false,
                    ]); ?>
                    <div class="col-md-8">
                        <?= $form->field($model, 'has_chief_accountant_patronymic')->label('Нет отчества')->checkbox([
                            'disabled' => $model->chief_is_chief_accountant ? true : false,
                        ]); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div id="chiefAccountantSignature-container" class="row <?= $model->chief_is_chief_accountant ? 'hidden' : '' ?>">
                    <div class="col-md-8 label-width">
                        <label class="control-label bold-text">
                            Подпись
                        </label>
                    </div>
                    <div class="col-md-4 field-width inp_one_line">
                        <?php $imgPath = $model->getImage('chiefAccountantSignatureImage'); ?>
                        <div id="chiefAccountantSignatureImage-image" class="<?= is_file($imgPath) ? '' : 'hidden'; ?>">
                            <div id="chiefAccountantSignatureImage-image-result" style="max-width: 156px;">
                                <table class="company-image-preview">
                                    <tr>
                                        <td>
                                            <?php if (is_file($imgPath)) : ?>
                                                <?= EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET); ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 155px;">
                                <?= $form->field($model, 'deleteChiefAccountantSignatureImage', [
                                    'options' => [
                                        'class' => '',
                                    ]
                                ])->checkbox([
                                    'class' => 'has_depend',
                                    'data-target' => '#deleteChiefAccountantSignatureImageAll_wrap',
                                ]); ?>
                            </div>
                            <div id="deleteChiefAccountantSignatureImageAll_wrap" class="collapse">
                                <?= $form->field($model, 'deleteChiefAccountantSignatureImageAll', [
                                    'options' => [
                                        'class' => '',
                                    ]
                                ])->checkbox(); ?>
                            </div>
                        </div>
                        <div>
                            <?= Html::button('Добавить подпись', [
                                'class' => 'btn yellow',
                                'data-toggle' => 'modal',
                                'data-target' => '#modal-chiefAccountantSignatureImage',
                                'style' => 'margin-right: 20px;',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="profile-form-tabs">
    <?= Tabs::widget([
        'options' => ['class' => 'nav-form-tabs row'],
        'headerOptions' => ['class' => 'col-xs-4'],
        'items' => [
            [
                'label' => 'Реквизиты',
                'encode' => false,
                'content' => $this->render('_ooo_requisites', [
                    'form' => $form,
                    'model' => $model,
                    'inputConfig' => $inputConfig,
                    'inputConfigRequired' => $inputConfigRequired
                ]),
                'active' => true,
            ],
            [
                'label' => 'Реквизиты на английском',
                'encode' => false,
                'content' => $this->render('_requisites_en', [
                    'form' => $form,
                    'model' => $model,
                    'inputConfigRequired' => $inputConfigRequired,
                    'inputConfig' => [
                            'options' => [
                                    'class' => 'form-group',
                                ],
                        'inputOptions' => [
                                'class' => 'form-control',
                            ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                        ]
                ]),
                'active' => false,
            ],
            [
                'label' => 'Реквизиты вашей налоговой',
                'content' => $this->render('_partial_ifns', [
                    'form' => $form,
                    'model' => $model,
                    'ifns' => $ifns,
                ]),
            ],
        ],
    ]); ?>
</div>

<div style="margin-bottom: 20px;">
    <?php echo $this->render('_partial_files', [
        'model' => $model,
    ]); ?>
</div>

<?php
Modal::begin([
    'id' => 'modal-chiefAccountantSignatureImage',
    'header' => '<h1>Загрузить подпись главного бухгалтера</h1>',
    'toggleButton' => false,
]);

echo $this->render('_partial_files_chief_accountant', [
    'model' => $model,
]);

Modal::end();
?>

<?php $this->registerJs('
    $(document).on("change", "#chief_is_chief_accountant_input", function () {
        $(".change-disabled-inputs input").prop("disabled", $(this).is(":checked"));
        $.uniform.update(".change-disabled-inputs input:checkbox");
        if ($(this).is(":checked")) {
            $("#chiefAccountantSignature-container").addClass("hidden");
        } else {
            $("#chiefAccountantSignature-container").removeClass("hidden");
        }
    });
') ?>
