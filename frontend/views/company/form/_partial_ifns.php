<?php

use common\components\widgets\IfnsTypeahead;
use common\models\company\CompanyType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $model common\models\Company */
/* @var $ifns common\models\Ifns */
/* @var $form \yii\bootstrap\ActiveForm */

$inputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-8 control-label label-width',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

?>

<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'ifns_ga', $inputConfig)->widget(IfnsTypeahead::className(), [
                    'kppSelector' => '#' . Html::getInputId($model, 'kpp'),
                    'related' => [
                        '#' . Html::getInputId($ifns, 'gb') => 'gb',
                        '#' . Html::getInputId($ifns, 'g1') => 'g1',
                        '#' . Html::getInputId($ifns, 'g2') => 'g2',
                        '#' . Html::getInputId($ifns, 'g4') => 'g4',
                        '#' . Html::getInputId($ifns, 'g6') => 'g6',
                        '#' . Html::getInputId($ifns, 'g7') => 'g7',
                        '#' . Html::getInputId($ifns, 'g8') => 'g8',
                        '#' . Html::getInputId($ifns, 'g9') => 'g9',
                        '#' . Html::getInputId($ifns, 'g11') => 'g11',
                    ],
                ])->textInput([
                    'placeholder' => 'Автозаполнение по Коду ИФНС' .
                        ($model->company_type_id != CompanyType::TYPE_IP ? ' (Первые четыре цифры вашего КПП)' : ''),
                ]); ?>
                <?= $form->field($ifns, 'gb', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g1', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g2', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($ifns, 'g4', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g6', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g7', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($ifns, 'g8', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g9', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
                <?= $form->field($ifns, 'g11', $inputConfig)->textInput([
                    'maxlength' => true,
                    'disabled' => true,
                ]); ?>
            </div>
        </div>
    </div>
</div>