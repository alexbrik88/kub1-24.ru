<?php
use common\models\Company;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CompanyType;
use backend\models\Bank;
use common\models\company\ApplicationToBank;
use frontend\assets\ContractorDossierAsset;
use frontend\assets\InterfaceCustomAsset;
use frontend\models\AffiliateProgramForm;
use frontend\rbac\UserRole;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */
/* @var $applicationToBank ApplicationToBank */
/* @var $affiliateProgramForm AffiliateProgramForm */
/* @var $outInvoiceSearch frontend\models\OutInvoiceSearch */
/* @var $outInvoiceProvider yii\data\ActiveDataProvider */



$this->title = 'Профиль компании';
$this->context->layoutWrapperCssClass = 'edit-profile';
ContractorDossierAsset::register($this);
InterfaceCustomAsset::register($this);

$view = $company->self_employed ? '_type_self' : (
    $company->company_type_id == CompanyType::TYPE_IP ?
    Company::$companyFormView[CompanyType::TYPE_IP] :
    Company::$companyFormView[CompanyType::TYPE_OOO]
);
?>

<div class="form-horizontal">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption bold-text">
                <?= Html::encode($company->shortTitle); ?>
            </div>

            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Company::PROFILE)): ?>
                <a class="caption promo-org"
                   href="<?= \yii\helpers\Url::to(['company/visit-card']); ?>">Визитка
                    компании</a>
            <?php endif; ?>

            <div class="actions">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Company::UPDATE)) {
                    $linkOptions = [
                        'id' => 'company-edit-link',
                        'class' => 'darkblue btn-sm',
                    ];

                    if ($company->strict_mode == Company::ON_STRICT_MODE) {
                        $linkOptions = \yii\helpers\ArrayHelper::merge($linkOptions, [
                            'data' => [
                                'toggle' => 'popover',
                                'placement' => 'left',
                                'content' => 'Внесите все данные по Вашей компании, чтобы использовать все возможности сервиса.',
                                'template' => '<div class="strict-mode-popover popover m-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
                            ],
                        ]);
                        $this->registerJs('$("#company-edit-link").popover("show");');
                    } else {
                        $linkOptions['title'] = 'Редактировать';
                    }

                    echo \yii\helpers\Html::a(' <i class="icon-pencil"></i>', ['update'], $linkOptions);
                } ?>
            </div>
        </div>
        <?php echo $this->render('view-profile/' . $view, [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'banks' => $banks,
            'affiliateProgramForm' => $affiliateProgramForm,
        ]) ?>

        <?php foreach ($banks as $bank) {
            echo $this->render('form/modal_bank/_create', [
                'bank' => $bank,
                'applicationToBank' => $applicationToBank,
                'redirectUrl' => Yii::$app->controller->action->id,
            ]);
        }; ?>
    </div>

</div>
<div class="modal fade" id="bill-invoice-remuneration" tabindex="-1"
     role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Получить
                    вознаграждение на счет</h3>
            </div>
            <div class="modal-body">
                <?php $affiliateForm = ActiveForm::begin([
                    'action' => Url::to(['withdraw-money', 'page' => 'company']),
                    'options' => [
                        'class' => 'form-horizontal form-checking-accountant',
                        'id' => 'form-bill-invoice-remuneration',
                    ],
                    'enableClientValidation' => true,
                ]); ?>


                <span>
                    Доступно к выводу: <b><?= $company->affiliate_sum; ?>
                        руб.</b>
                </span>

                <?= $affiliateForm->field($affiliateProgramForm, 'withdrawal_amount', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'margin-top: 15px; margin-bottom: 0;',
                    ],
                    'labelOptions' => [
                        'class' => 'col-md-2 control-label bold-text',
                    ],
                    'wrapperOptions' => [
                        'class' => 'col-md-10 inp_one_line-product',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                ])->textInput([
                    'placeholder' => 'Укажите сумму для вывода вознаграждения',
                ])->label('Вывести:'); ?>

                <?= $affiliateForm->field($affiliateProgramForm, 'type', [
                    'options' => [
                        'class' => 'form-group',
                        'style' => 'margin-bottom: 10px;',
                    ],
                    'labelOptions' => [
                        'class' => 'col-md-2 control-label bold-text',
                    ],
                    'wrapperOptions' => [
                        'class' => 'col-md-7 inp_one_line-product',
                    ],
                    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                ])->radioList([
                    AffiliateProgramForm::RS_TYPE => 'на расчетный счет компании',
                    AffiliateProgramForm::INDIVIDUAL_ACCOUNT_TYPE => 'на счет физического лица',
                ])->label(''); ?>

                <?php if (!$company->phone): ?>
                    <?= $affiliateForm->field($affiliateProgramForm, 'phone', [
                        'options' => [
                            'class' => 'form-group',
                            'style' => 'margin-bottom: 15px;',
                        ],
                        'labelOptions' => [
                            'class' => 'col-md-2 control-label bold-text',
                        ],
                        'wrapperOptions' => [
                            'class' => 'col-md-10 inp_one_line-product',
                        ],
                        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                    ])->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                        'options' => [
                            'class' => 'form-control field-width field-w inp_one_line_company',
                            'placeholder' => '+7(XXX) XXX-XX-XX',
                        ],
                    ]); ?>
                <?php endif; ?>

                <div class="form-actions" style="margin-bottom: 10px;">
                    <div class="row action-buttons">
                        <div class="col-sm-4 col-xs-4">
                            <?= Html::submitButton('Оформить заказ', [
                                'class' => 'btn darkblue text-white hidden-md hidden-sm hidden-xs',
                                'style' => 'width: 100% !important',
                            ]); ?>
                            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                'class' => 'btn darkblue text-white hidden-lg',
                                'title' => 'Оформить заказ',
                                'style' => 'width: 100% !important',
                            ]); ?>
                        </div>
                        <div class="col-sm-4 col-xs-4"></div>
                        <div class="col-sm-4 col-xs-4">
                            <?= Html::a('Отменить', null, [
                                'class' => 'btn darkblue hidden-md hidden-sm hidden-xs close-modal-button',
                                'style' => 'width: 100% !important',
                            ]); ?>
                            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', null, [
                                'class' => 'btn darkblue hidden-lg close-modal-button',
                                'title' => 'Отменить',
                                'style' => 'width: 100% !important',
                            ]); ?>
                        </div>
                    </div>
                </div>
                <span>После оформления заказа, наш сотрудник свяжется с вами и обсудит детали.</span>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
