<?php
use common\models\Company;
use common\models\company\CheckingAccountant;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var Company $company */

$this->title = $company->getTitle(true) . ' - Визитка компании';

$checkingAccountant = new CheckingAccountant;
$mainCheckingAccountant = $company->mainCheckingAccountant;
?>

<div class="page-content-in p-center pad-pdf-p">
    <h3 class="page-title">Визитка</h3>

    <h3>
        <b><?php echo Html::encode($company->getTitle(true, true)); ?></b>
    </h3>

    <table class="b-none">
        <tbody>
        <tr>
            <th>Система налогообложения:</th>
            <td><?= Html::encode($company->companyTaxationType->name); ?></td>
        </tr>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left">Юридический адрес:</th>
            <td><?= Html::encode($company->getAddressLegalFull()); ?></td>
        </tr>
        <tr>
            <th class="text-left">Фактический адрес:</th>
            <td><?= Html::encode($company->getAddressActualFull()); ?></td>
        </tr>
        </tbody>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('egrip'); ?>:</th>
                <td><?= Html::encode($company->egrip); ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('inn'); ?>:</th>
                <td><?= Html::encode($company->inn); ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('okved'); ?>:</th>
                <td><?= Html::encode($company->okved); ?></td>
            </tr>
        <?php else: ?>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('ogrn'); ?>:</th>
                <td><?= Html::encode($company->ogrn); ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('inn'); ?>:</th>
                <td><?= Html::encode($company->inn); ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('kpp'); ?>:</th>
                <td><?= Html::encode($company->kpp); ?></td>
            </tr>
            <tr>
                <th class="text-left"><?= $company->getAttributeLabel('okved'); ?>:</th>
                <td><?= Html::encode($company->okved); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('rs'); ?>:</th>
            <td><?= Html::encode($mainCheckingAccountant? $mainCheckingAccountant->rs: ''); ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('bank_name'); ?>:</th>
            <td><?= Html::encode($mainCheckingAccountant? $mainCheckingAccountant->bank_name: ''); ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('ks'); ?>:</th>
            <td><?= Html::encode($mainCheckingAccountant? $mainCheckingAccountant->ks: ''); ?></td>
        </tr>
        <tr>
            <th class="text-left"><?= $checkingAccountant->getAttributeLabel('bik'); ?>:</th>
            <td><?= Html::encode($mainCheckingAccountant? $mainCheckingAccountant->bik: ''); ?></td>
        </tr>

        <?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
            <tr>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            <?php if ($company->chief_post_name) : ?>
                <tr>
                    <th class="text-left"><?= mb_convert_case($company->chief_post_name, MB_CASE_TITLE, 'UTF-8'); ?>:</th>
                    <td><?= Html::encode($company->getChiefFio()); ?></td>
                </tr>
            <?php endif; ?>
            <tr>
                <th class="text-left">Главный бухгалтер:</th>
                <td><?= Html::encode($company->getChiefAccountantFio()); ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <th class="text-left"><?= $company->getAttributeLabel('phone'); ?>:</th>
            <td><?= Html::encode($company->phone); ?></td>
        </tr>

    </table>
</div>

