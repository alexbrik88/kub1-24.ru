<?php

use common\models\company\ForeignCurrencyAccount;
use common\models\company\ForeignCurrencyAccountSearch;
use common\components\grid\GridView;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use frontend\widgets\BtnConfirmModalWidget;
use backend\models\Bank;
use common\components\ImageHelper;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $foreignCurrencyAccountProvider ForeignCurrencyAccount */
/* @var $foreignCurrencyAccountSearch ForeignCurrencyAccountSearch */
/* @var $banks Bank[] */

?>
<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?php \yii\widgets\Pjax::begin([
                'id' => 'fca-pjax-container',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
                    'id' => 'datatable_ajax',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",

                'columns' => [
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Банк',
                        'headerOptions' => [
                            'width' => '25%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->bank_name;
                        },
                    ],
                    [
                        'attribute' => 'swift',
                        'label' => 'SWIFT',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->swift;
                        },
                    ],
                    [
                        'attribute' => 'currency_id',
                        'label' => 'Валюта',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return $model->currency->name;
                        },
                    ],
                    [
                        'attribute' => 'rs',
                        'label' => 'Счет',
                        'headerOptions' => [
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis;',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount  $model) {
                            return $model->rs;
                        },
                    ],
                    [
                        'attribute' => 'type',
                        'label' => 'Тип',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (ForeignCurrencyAccount $model) {
                            return ArrayHelper::getValue($model->typeAccount, $model->type, '');
                        },
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>
