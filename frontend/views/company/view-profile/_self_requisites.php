<?php
use yii\helpers\Html;
/**
 * @var $company
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-title">
        <div class="caption">Реквизиты</div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <label for="adress" class="control-label col-md-6 label-width">
                Фактический адрес:
            </label>

            <div class="col-md-6 field-width">
                <p class="form-control-static"><?php echo Html::encode($company->getAddressLegalFull()); ?></p>
            </div>
        </div>
    </div>
</div>
