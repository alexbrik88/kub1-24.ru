<?php
/** @var common\models\Company $company */
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\models\Company;

?>
<div class="row small-boxes">
    <div class="col-md-4">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Логотип компании</div>
            </div>
            <div class="portlet-body">
                <div class="form-group signature">
                    <div class="portlet align-center signature_block-image">
                        <?php
                        $imgPath = $company->getImage('logoImage');
                        if (is_file($imgPath)) {
                            echo EasyThumbnailImage::thumbnailImg($imgPath, 545, 186, EasyThumbnailImage::THUMBNAIL_INSET, [
                                'style' => 'max-width: 100%; max-height: 100%;'
                            ]);
                        } else {
                            echo '<img src="" class="signature_image">';
                        }; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!$company->self_employed) : ?>
        <div class="col-md-4">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Печать компании</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group signature">
                        <div class="portlet align-center signature_block-image">
                            <?php
                            $imgPath = $company->getImage('printImage');
                            if (is_file($imgPath)) {
                                echo EasyThumbnailImage::thumbnailImg($imgPath, 200, 200, EasyThumbnailImage::THUMBNAIL_INSET, [
                                    'style' => 'max-width: 100%; max-height: 100%;'
                                ]);
                            } else {
                                echo '<img src="" class="signature_image">';
                            }; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
    <div class="col-md-4">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Подпись руководителя</div>
            </div>
            <div class="portlet-body">
                <div class="form-group signature">
                    <div class="portlet align-center signature_block-image">
                        <?php
                        $imgPath = $company->getImage('chiefSignatureImage');
                        if (is_file($imgPath)) {
                            echo EasyThumbnailImage::thumbnailImg($imgPath, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET, [
                                'style' => 'max-width: 100%; max-height: 100%;'
                            ]);
                        } else {
                            echo '<img src="" class="signature_image">';
                        }; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
