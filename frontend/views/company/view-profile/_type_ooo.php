<?php
/** @var common\models\Company $company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */

/* @var $affiliateProgramForm AffiliateProgramForm */

use common\models\Company;
use common\models\TimeZone;
use common\models\company\CheckingAccountantSearch;
use backend\models\Bank;
use frontend\models\AffiliateProgramForm;
use frontend\rbac\UserRole;
use yii\bootstrap\Tabs;
use yii\helpers\Html;

?>

<div class="edit-profile__info" style="width: 68%;display: inline-block;">
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Краткое
            наименование организации:</label>

        <div class="col-md-6">
            <p class="form-control-static"><?php echo Html::encode($company->getTitle(true)); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="col-md-4 control-label label-width">Система
            налогообложения:</label>

        <div class="col-md-6">
            <p class="form-control-static"><?php echo Html::encode($company->companyTaxationType->name); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Полное
            наименование организации:</label>

        <div class="col-md-6 name_organiz_md">
            <p class="form-control-static"><?php echo Html::encode($company->title); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Часовой
            пояс:</label>

        <div class="col-md-6 name_organiz_md">
            <p class="form-control-static"><?php $timeZone = TimeZone::findOne($company->time_zone_id);
                echo $timeZone['out_time_zone'] ?></p>
        </div>
    </div>
    <?php if (($nds = \common\models\NdsOsno::findOne($company->nds)) !== null): ?>
        <div class="form-group">
            <label for="name" class="control-label col-md-4 label-width">В
                счетах цену за товар/услугу указывать:</label>

            <div class="col-md-6">
                <p class="form-control-static"><?= Html::encode($nds->name); ?></p>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Формировать
            PDF с печатью и подписью:</label>

        <div class="col-md-6">
            <p class="form-control-static">
                <?= join(', ', array_filter([
                    $company->pdf_signed ? $company->getAttributeLabel('pdf_signed') : '',
                    $company->pdf_act_signed ? $company->getAttributeLabel('pdf_act_signed') : '',
                ])) ?>
            </p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Отправлять
            с печатью и подписью:</label>

        <div class="col-md-6">
            <p class="form-control-static">
                <?= join(', ', array_filter([
                    $company->pdf_send_signed ? $company->getAttributeLabel('pdf_send_signed') : '',
                    $company->pdf_act_send_signed ? $company->getAttributeLabel('pdf_act_send_signed') : '',
                ])) ?>
            </p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">
            <?= $company->getAttributeLabel('is_additional_number_before') ?>:
        </label>

        <div class="col-md-4">
            <p class="form-control-static">
                <?= (isset(Company::$addNumPositions[$company->is_additional_number_before])) ?
                    Company::$addNumPositions[$company->is_additional_number_before] : ''; ?>
            </p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Телефон:</label>

        <div class="col-md-6">
            <p class="form-control-static"><?php echo Html::encode($company->phone); ?></p>
        </div>
    </div>
    <div class="form-group">
        <label for="name" class="control-label col-md-4 label-width">Электронная
            почта:</label>

        <div class="col-md-6">
            <p class="form-control-static">
                <a href="mailto:<?php echo $company->email; ?>"><?php echo Html::encode($company->email); ?></a>
            </p>
        </div>
    </div>
</div>
<?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
    <div class="affiliate-program-ooo">
        <?= $this->render('_affiliate_program', [
            'company' => $company
        ]); ?>
    </div>
<?php endif ?>
<?php if (!isset($admin)): ?>
    <div class="profile-form-tabs">
        <?= Tabs::widget([
            'options' => ['class' => 'nav-form-tabs row'],
            'headerOptions' => ['class' => 'col-xs-2'],
            'items' => [
                [
                    'label' => 'Банковские счета',
                    'encode' => false,
                    'content' => $this->render('_partial_checking_accountant', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'banks' => $banks,
                    ]),
                    'active' => true,
                    'headerOptions' => [
                        'style'=>'min-width:185px;'
                    ]
                ],
                [
                    'label' => 'Кассы',
                    'content' => $this->render('_partial_cashbox', ['model' => $company]),
                ],
                [
                    'label' => 'E-money',
                    'content' => $this->render('_partial_emoney', ['model' => $company]),
                ],
                [
                    'label' => 'Склады',
                    'content' => $this->render('_partial_store', ['model' => $company]),
                ],
            ],
        ]); ?>
    </div>
<?php endif; ?>

<div class="row">
    <div class="col-md-6">
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">Руководитель (для документов)</div>
            </div>
            <div class="portlet-body">
                <div>
                    <?= Html::encode(join(', ', array_filter([
                        $company->chief_post_name,
                        $company->getChiefFio(),
                    ]))); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box box-name darkblue">
            <div class="portlet-title">
                <div class="caption">Главный бухгалтер (для документов)
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <?php echo Html::encode($company->getChiefAccountantFio()); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?= Tabs::widget([
    'options' => ['class' => 'nav-form-tabs row'],
    'headerOptions' => ['class' => 'col-xs-3'],
    'items' => [
        [
            'label' => 'Реквизиты',
            'encode' => false,
            'content' => $this->render('_ooo_requisites', [
                'company' => $company
            ]),
            'active' => !(bool)Yii::$app->request->get('tab')
        ],
        [
            'label' => 'Реквизиты на английском',
            'encode' => false,
            'content' => $this->render('_requisites_en', [
                'company' => $company,
            ]),
            'active' => false,
        ],
        [
            'label' => 'Реквизиты налоговой',
            'content' => $this->render('_partial_ifns', [
                'company' => $company,
            ]),
            'headerOptions' => [
                'style'=>'min-width:229px;'
            ]
        ],
        /*[
            'label' => 'Досье',
            'id' => 'dossier',
            'content' => $this->render('_dossier'),
            'active' => (bool)Yii::$app->request->get('tab')
        ]*/
    ],
]); ?>

<?php echo $this->render('_partial_files', [
    'company' => $company,
]); ?>
