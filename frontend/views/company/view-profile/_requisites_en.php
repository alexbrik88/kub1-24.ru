<?php

use yii\helpers\Html;

/**
 * @var $form
 * @var $inputConfig
 * @var $model
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div style="padding: 15px 15px 0px 15px">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Название организации:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->name_short_en); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Полное наименование:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->name_full_en); ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Юридический адрес:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->address_legal_en); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Фактический адрес:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->address_actual_en); ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Фамилия:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->lastname_en); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Имя:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->firstname_en); ?></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Отчество:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->patronymic_en); ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_short"
                               class="control-label col-md-6">Форма:</label>
                        <p class="form-control-static"><?php echo Html::encode($company->form_legal_en); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
