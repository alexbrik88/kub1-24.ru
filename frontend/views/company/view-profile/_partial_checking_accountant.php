<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.08.2016
 * Time: 8:30
 */

use backend\models\Bank;
use common\models\company\CheckingAccountantSearch;
use common\models\company\CheckingAccountant;
use common\components\grid\GridView;
use common\components\ImageHelper;
use common\models\employee\EmployeeRole;
use frontend\rbac\UserRole;
use yii\helpers\Html;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CheckingAccountantSearch */
/* @var $banks Bank[] */

?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],

                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],

                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],

                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => "{items}\n{pager}",

                    'columns' => [
                        [
                            'attribute' => 'bank_name',
                            'label' => 'Банк',
                            'headerOptions' => [
                                'width' => '25%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return $model->bank_name;
                            },
                        ],
                        [
                            'attribute' => 'bik',
                            'label' => 'БИК',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return $model->bik;
                            },
                        ],
                        [
                            'attribute' => 'ks',
                            'label' => 'К/с',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return $model->ks;
                            },
                        ],
                        [
                            'attribute' => 'rs',
                            'label' => 'Р/с',
                            'headerOptions' => [
                                'width' => '20%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return $model->rs;
                            },
                        ],
                        [
                            'attribute' => 'type',
                            'label' => 'Тип',
                            'headerOptions' => [
                                'width' => '15%',
                            ],
                            'value' => function (CheckingAccountant $model) {
                                return $model->typeText[$model->type];
                            },
                        ],
                    ],
                ]); ?>
            </div>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                <div class="table-container" style="padding-top: 20px;">
                    <table
                        class="table table-striped table-bordered table-hover dataTable"
                        role="grid">
                        <thead>
                        <tr class="heading">
                            <th rowspan=>Открыть расчетный счет со скидкой</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <?= Bank::getSpecialOfferLogoBankList(4); ?>
            <?php endif ?>
        </div>
    </div>
</div>



