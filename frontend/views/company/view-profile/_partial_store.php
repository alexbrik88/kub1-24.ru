<?php

use common\models\EmployeeCompany;
use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use frontend\models\StoreSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $model common\models\Company */
/* @var $form \yii\bootstrap\ActiveForm */

$searchModel = new StoreSearch(['company_id' => $model->id]);
$dataProvider = $searchModel->search(Yii::$app->request->get());

?>


<div class="portlet box darkblue">
    <div class="portlet-body accounts-list">
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-container">
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'store-pjax-container',
                        'enablePushState' => false,
                    ]); ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'tableOptions' => [
                                'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
                                'id' => 'datatable_ajax',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],

                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],

                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],

                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => "{items}\n{pager}",

                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'headerOptions' => [
                                    ],
                                ],
                                [
                                    'attribute' => 'responsible_employee_id',
                                    'class' => DropDownSearchDataColumn::class,
                                    'headerOptions' => [
                                    ],
                                    'value' => function ($model) {
                                        $e = EmployeeCompany::find()->andWhere([
                                            'employee_id' => $model->responsible_employee_id,
                                            'company_id' => $model->company_id,
                                        ])->one();

                                        return $e ? $e->getFio(true) : '';
                                    },
                                ],
                                [
                                    'label' => 'Тип',
                                    'value' => function ($model) {
                                        return $model->is_main ? 'Основной' : ($model->is_closed ? 'Закрыт' : 'Дополнительный');
                                    },
                                ],
                            ],
                        ]); ?>

                    <?php \yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>