<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */

$ifns = $company->ifns;

?>

<?php if ($ifns) : ?>

<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                    <?= Html::activeLabel($company, 'ifns_ga', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($company->ifns_ga) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'gb', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->gb) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g1', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g1) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g2', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g2) ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g4', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g4) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g6', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g6) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g7', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g7) ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g8', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g8) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g9', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g9) ?></p>
                    </div>
                </div>
                <div class="form-group row">
                    <?= Html::activeLabel($ifns, 'g11', [
                        'class' => 'control-label col-md-6 label-width',
                    ]) ?>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?= Html::encode($ifns->g11) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif ?>
