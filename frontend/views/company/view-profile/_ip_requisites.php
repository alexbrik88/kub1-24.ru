<?php

use common\components\date\DateHelper;
use yii\helpers\Html;

/**
 * @var $company
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="KPP"
                           class="control-label col-md-6 label-width">ИНН:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->inn); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="INN"
                           class="control-label col-md-6 label-width">ОГРНИП:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->egrip); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">ОКПО:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okpo); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">Юридический адрес:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->getAddressLegalFull()); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">Фактический адрес:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->getAddressActualFull()); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">ОКВЭД:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okved); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 left-column">
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ОКТМО:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->oktmo); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="correspondent-account"
                           class="control-label col-md-6 label-width">ФСС:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->fss); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width"
                           style="width:215px;">ПФР (как ИП):</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->pfr_ip); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width"
                           style="width:215px;">ПФР (как
                        работодателя):</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->pfr_employer); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">Дата
                        постановки на учёт в налоговом органе:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo DateHelper::format($company->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>