<?php

use yii\helpers\Url;

?>
<div class="portlet box darkblue details">
    <div class="portlet-body" id="dossier">
        <script>
            $('#dossier').load('<?=Url::toRoute([
                '/dossier-company/' . Yii::$app->request->get('tab', 'index'),
                'ajax' => 1
            ])?>',function() {
            });
        </script>
    </div>
</div>
<script>
    function loadDossierTab(link) {
        $('#dossierContainer').html('<div class="loader"><span></span><span></span><span></span></div>').load(link, function () {
            linkReplace();
            try {
                let lnk = link.split('/');
                history.pushState(null, null, '<?=Url::toRoute(['index', 'tab' => ''])?>' + lnk[lnk.length - 1]);
            } catch (e) {
            }
        });
        return false;
    }
</script>