<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
use yii\helpers\Html;

/**
 * @var $company
 */
?>
<div class="portlet box darkblue details">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="KPP"
                           class="control-label col-md-6 label-width">ИНН:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->inn); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="INN"
                           class="control-label col-md-6 label-width">ОГРН:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->ogrn); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="OGRN"
                           class="control-label col-md-6 label-width">КПП:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->kpp); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">ОКПО:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okpo); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">Юридический адрес:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->getAddressLegalFull()); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">Фактический адрес:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->getAddressActualFull()); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="adress"
                           class="control-label col-md-6 label-width">ОКВЭД:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okved); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ОКАТО:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okato); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 left-column">
                <div class="form-group">
                    <label for="BIC"
                           class="control-label col-md-6 label-width">ОКТМО:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->oktmo); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="BIC"
                           class="control-label col-md-6 label-width">ОКОГУ:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okogu); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ОКФС:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okfs); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ОКОПФ:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->okopf); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ПФР:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->pfr); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width">ФСС:</label>

                    <div class="col-md-6 field-width">
                        <p class="form-control-static"><?php echo Html::encode($company->fss); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank"
                           class="control-label col-md-6 label-width" style="width: 370px;">Дата
                        постановки на учёт в налоговом органе:</label>

                    <div class="col-md-6 field-width" style="width: 120px;">
                        <p class="form-control-static"><?php echo DateHelper::format($company->tax_authority_registration_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank" class="control-label col-md-6 label-width" style="width: 170px;">
                        Уставный капитал:
                    </label>

                    <div class="col-md-6 field-width" style="width: 350px;">
                        <p class="form-control-static">
                            <?= Html::encode(TextHelper::invoiceMoneyFormat($company->capital, 2)); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>