<?php
use yii\helpers\Html;
/** @var common\models\Company $company */
?>
<div class="row">
    <div class="col-md-6">
        <div class="portlet box box-adress darkblue">
            <div class="portlet-title">
                <div class="caption">Юридический адрес</div>
            </div>
            <div class="portlet-body">
                <div><?php echo Html::encode($company->getAddressLegalFull()); ?></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet box box-adress darkblue">
            <div class="portlet-title">
                <div class="caption">Фактический адрес</div>
            </div>
            <div class="portlet-body">
                <div><?php echo Html::encode($company->getAddressActualFull()); ?></div>
            </div>
        </div>
    </div>
</div>
