<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var \yii\base\View $this */
/* @var $sendForm \frontend\models\VisitCardSendForm */
/* @var $message string */

?>

<?php $form = ActiveForm::begin([
    'id' => 'send-visit-card-form',
    'action' => Url::to(['send-visit-card',]),
    'method' => 'post',
    'enableAjaxValidation' => true,
]); ?>
<div class="form-body">
    <?= $form->field($sendForm, 'email')->label()->textInput(); ?>
</div>
<div class="form-actions">
    <?= Html::submitButton('ОК', [
        'class' => 'btn yellow pull-right',
    ]); ?>
</div>
<div class="clr"></div>
<?php $form->end(); ?>

