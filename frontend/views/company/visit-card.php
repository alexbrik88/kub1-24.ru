<?php
use common\models\Company;
use common\components\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var Company $company */

$this->title = 'Визитка компании';
$mainCheckingAccountant = $company->mainCheckingAccountant ? $company->mainCheckingAccountant : new \common\models\company\CheckingAccountant();
?>

<h3 class="page-title">Визитка</h3>

<h3>
    <b><?php echo Html::encode($company->getTitle(true, true)); ?></b>
</h3>

<table style="border-collapse: separate; border-spacing: 5px;" class="table_marg">
    <tbody>
    <tr>
        <th>Система налогообложения:</th>
        <td><?= Html::encode($company->companyTaxationType->name); ?></td>
    </tr>

    <tr><th>&nbsp;</th></tr>

    <?php if (!$company->self_employed) : ?>
        <tr>
            <th>Юридический адрес:</th>
            <td><?= Html::encode($company->getAddressLegalFull()); ?></td>
        </tr>
    <?php endif ?>
    <tr>
        <th>Фактический адрес:</th>
        <td><?= Html::encode($company->getAddressActualFull()); ?></td>
    </tr>
    </tbody>

    <tr><th>&nbsp;</th></tr>

    <?php if ($company->self_employed): ?>
        <tr>
            <th><?= $company->getAttributeLabel('inn'); ?>:</th>
            <td><?= Html::encode($company->inn); ?></td>
        </tr>
    <?php elseif ($company->getIsLikeIP()): ?>
        <tr>
            <th><?= $company->getAttributeLabel('egrip'); ?>:</th>
            <td><?= Html::encode($company->egrip); ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('inn'); ?>:</th>
            <td><?= Html::encode($company->inn); ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('okved'); ?>:</th>
            <td><?= Html::encode($company->okved); ?></td>
        </tr>
    <?php else: ?>
        <tr>
            <th><?= $company->getAttributeLabel('ogrn'); ?>:</th>
            <td><?= Html::encode($company->ogrn); ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('inn'); ?>:</th>
            <td><?= Html::encode($company->inn); ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('kpp'); ?>:</th>
            <td><?= Html::encode($company->kpp); ?></td>
        </tr>
        <tr>
            <th><?= $company->getAttributeLabel('okved'); ?>:</th>
            <td><?= Html::encode($company->okved); ?></td>
        </tr>
    <?php endif; ?>

    <tr><th>&nbsp;</th></tr>

    <tr>
        <th><?= $mainCheckingAccountant->getAttributeLabel('rs'); ?>:</th>
        <td><?= Html::encode($mainCheckingAccountant->rs); ?></td>
    </tr>
    <tr>
        <th><?= $mainCheckingAccountant->getAttributeLabel('bank_name'); ?>:</th>
        <td><?= Html::encode($mainCheckingAccountant->bank_name); ?></td>
    </tr>
    <tr>
        <th><?= $mainCheckingAccountant->getAttributeLabel('ks'); ?>:</th>
        <td><?= Html::encode($mainCheckingAccountant->ks); ?></td>
    </tr>
    <tr>
        <th><?= $mainCheckingAccountant->getAttributeLabel('bik'); ?>:</th>
        <td><?= Html::encode($mainCheckingAccountant->bik); ?></td>
    </tr>

    <?php if ($company->getIsLikeOOO()): ?>
        <tr><th>&nbsp;</th></tr>

        <tr>
            <th><?= !empty($company->chief_post_name) ? $company->chief_post_name : 'Генеральный директор'; ?>:</th>
            <td><?= Html::encode($company->getChiefFio()); ?></td>
        </tr>
        <tr>
            <th>Главный бухгалтер:</th>
            <td><?= Html::encode($company->getChiefAccountantFio()); ?></td>
        </tr>
    <?php endif; ?>

    <tr><th>&nbsp;</th></tr>

    <tr>
        <th><?= $company->getAttributeLabel('phone'); ?>:</th>
        <td><?= Html::encode($company->phone); ?></td>
    </tr>

</table>

<div class="row action-buttons">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php Modal::begin([
            'options' => [
                'id' => 'send-visit-card',
            ],
            'header' => '<h4>Отправка визитки</h4>',
            'toggleButton' => [
                'label' => 'Отправить',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ],
        ]);

        echo $this->render('_modal_send_visit_card', [
            'sendForm' => new \frontend\models\VisitCardSendForm,
        ]);

        Modal::end(); ?>
        <?php Modal::begin([
            'options' => [
                'id' => 'send-visit-card',
            ],
            'header' => '<h4>Отправка визитки</h4>',
            'toggleButton' => [
                'label' => '<span class="ico-Send-smart-pls fs"></span>',
                'Title' => 'Отправить',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ],
        ]);

        echo $this->render('_modal_send_visit_card', [
            'sendForm' => new \frontend\models\VisitCardSendForm,
        ]);

        Modal::end(); ?>

    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('Печать', ['document-print', 'actionType' => 'print'], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]); ?>
        <?= Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print'], [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('PDF', ['document-print', 'actionType' => 'pdf'], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]); ?>
        <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', ['document-print', 'actionType' => 'pdf'], [
            'target' => '_blank',
            'title' => 'PDF',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>
