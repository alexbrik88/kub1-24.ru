<?php

use common\components\date\DateHelper;
use common\models\document\DocumentType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\widgets\Modal;
use common\components\image\EasyThumbnailImage;

/* @var yii\web\View $this */
/* @var common\models\employee\Employee $model */

/* @var \yii\bootstrap\ActiveForm $form */
/* @var $completeRegistration boolean */

$initialConfig = [
    'wrapperOptions' => [
        'class' => 'col-md-2 inp_one_line',
    ],
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);

$showChBxRoles = EmployeeCompany::$addFlowRolesNeedConfig;
$payInvoiceCss = in_array($model->employee_role_id, $showChBxRoles) ? '' : 'hidden';
$roleArray = \common\models\employee\EmployeeRole::find()->actual()->orderBy([
    'sort' => SORT_ASC,
])->all();
?>

<div class="portlet box darkblue">
    <div class="portlet-body">
        <?= $form->field($model, 'time_zone_id')->dropDownList(
            ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>

        <?= $form->field($model, 'sex')->radioList(Employee::$sex_message, [
            'uncheck' => null,
            'item' => function ($index, $label, $name, $checked, $value) {
                $radio = Html::radio($name, $checked, [
                    'checked' => $checked,
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline m-l-n-md',
                    ],
                ]);

                return Html::tag('div', $radio, [
                    'class' => 'col-r col-xs-4 m-l-n',
                ]);
            },
        ]); ?>

        <?= $form->field($model, 'birthday', [
            'wrapperOptions' => [
                'class' => 'col-md-2 inp_one_line',
            ],
            'template' => Yii::$app->params['formDatePickerTemplate'],
        ])->textInput([
            'class' => 'form-control date-picker min_wid_picker',
            'value' => DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
        ]); ?>

        <div class="form-group">
            <?= $form->field($model, 'date_hiring', [
                'options' => [
                    'class' => '',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>

            <?= $form->field($model, 'date_dismissal', [
                'options' => [
                    'class' => 'one_line_date_dismissal',
                ],
                'labelOptions' => [
                    'class' => 'col-md-3 control-label bold-text label-width',
                ],
                'wrapperOptions' => [
                    'class' => 'col-md-2 inp_one_line',
                ],
                'template' => Yii::$app->params['formDatePickerTemplate'],
            ])->textInput([
                'class' => 'form-control date-picker min_wid_picker',
                'value' => DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
        </div>

        <?= $form->field($model, 'position')->textInput([
            'placeholder' => $model->getAttributeLabel('position'),
        ]); ?>
        <?= $form->field($model, 'employee_role_id', [
            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n" .
                Html::tag('span', '', [
                    'class' => 'tooltip2  ico-question valign-middle',
                    'data-tooltip-content' => '#tooltip_employye_role',
                    'style' => 'position: absolute; top: 2px; right: -20px;'
                ]) . "\n{endWrapper}",
        ])->dropDownList(
            ArrayHelper::map($roleArray, 'id', 'name'),
            [
                'prompt' => '',
                'disabled' => $model->scenario === Employee::SCENARIO_CONTINUE_REGISTRATION,
            ]
        ); ?>
        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
            'options' => [
                'class' => 'form-control',
                'placeholder' => '+7(XXX) XXX-XX-XX',
            ],
        ]); ?>

        <div class="form-group">
            <label class="col-md-2 control-label bold-text width-212">
                Видит ТОЛЬКО счета, которые ВЫСТАВИЛ САМ
            </label>
            <div class="col-md-7 inp_one_line" style="line-height: 35px;">
                <?= Html::activeCheckbox($model, 'document_access_own_only', [
                    'label' => false,
                    'data-initvalue' => $model->document_access_own_only,
                    'data-default-on' => EmployeeCompany::$docAccessOwnOnly,
                    'data-always-off' => EmployeeCompany::$docAccessAll,
                    'disabled' => in_array($model->employee_role_id, EmployeeCompany::$docAccessAll) ? true : null,
                ]); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label bold-text width-212">
                Право подписи на документах
            </label>
            <div class="col-md-7 inp_one_line" style="line-height: 35px;">
                <?= Html::activeCheckbox($model, 'can_sign', ['label' => false, 'style' => 'margin-right: 10px;']); ?>
                <span class="employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>">
                        <?= Html::activeDropDownList($model, 'sign_document_type_id', DocumentType::find()->select(['name', 'id'])->indexBy('id')->column(), [
                            'prompt' => '',
                            'class' => 'form-control ',
                            'style' => 'display: inline-block; width: 170px; margin-right: 10px;'
                        ]); ?>
                    <span>№</span>
                    <?= Html::activeTextInput($model, 'sign_document_number', [
                        'class' => 'form-control ',
                        'style' => 'display: inline-block; width: 170px; margin-right: 10px;'
                    ]); ?>
                    <span>от</span>
                        <div class="input-icon" style="display: inline-block;">
                            <i class="fa fa-calendar"></i>
                            <?= Html::activeTextInput($model, 'sign_document_date', [
                                'class' => 'form-control date-picker min_wid_picker',
                                'style' => 'display: inline-block; width: 170px;',
                                'value' => DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>
                    </span>
            </div>
        </div>
        <div class="form-group employee_signature_element<?= $model->can_sign ? '' : ' hidden_element'; ?>">
            <label class="col-md-2 control-label bold-text width-212">
                Подпись
            </label>
            <div class="col-md-3 inp_one_line" style="padding-left: 30px;">
                <div class="form-group" id="signature_file-image-result">
                    <?php if ($model->employeeSignature): ?>
                        <?= EasyThumbnailImage::thumbnailImg($model->employeeSignature->file, 165, 60, EasyThumbnailImage::THUMBNAIL_INSET, [
                            'class' => 'signature_image',
                        ]); ?>
                    <?php else: ?>
                        <img class="signature_image" src="" alt="">
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <?= Html::button('Добавить подпись', [
                        'class' => 'btn yellow',
                        'data-toggle' => 'modal',
                        'data-target' => '#modal-employeeSignatureImage',
                        'style' => 'margin-right: 20px;',
                    ]); ?>
                </div>
                <div class="form-group" style="margin-bottom: 0;">
                    <?= $model->employeeSignature ? Html::activeCheckbox($model, 'deleteSignature_file', [
                        'label' => 'Удалить',
                        'class' => 'radio-inline p-o radio-padding',
                    ]) : ''; ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label bold-text width-212">
                Видит закупочные цены
            </label>
            <div class="col-md-7 inp_one_line" style="line-height: 35px;">
                <?= Html::activeCheckbox($model, 'can_view_price_for_buy', ['label' => false]); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label bold-text width-212">
                Может добавлять номенклатуру
            </label>
            <div class="col-md-7 inp_one_line" style="line-height: 35px;">
                <?= Html::activeCheckbox($model, 'is_product_admin', ['label' => false]); ?>
            </div>
        </div>
        <div class="form-group employeecompany-can_invoice_add_flow <?= $payInvoiceCss; ?>">
            <label class="col-md-2 control-label bold-text width-212">
                Может добавлять оплату по счетам
            </label>
            <div class="col-md-7 inp_one_line" style="line-height: 35px;">
                <?= Html::activeCheckbox($model, 'can_invoice_add_flow', ['label' => false]); ?>
            </div>
        </div>
    </div>
</div>