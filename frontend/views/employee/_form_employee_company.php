<?php

use common\components\date\DateHelper;
use common\models\document\DocumentType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\TimeZone;
use yii\helpers\Url;
use common\components\ImageHelper;
use common\components\widgets\EmployeeTypeahead;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use common\widgets\Modal;
use common\components\image\EasyThumbnailImage;

/* @var yii\web\View $this */
/* @var EmployeeCompany $model */

/* @var \yii\bootstrap\ActiveForm $form */
/* @var $completeRegistration boolean */

$initialConfig = [
    'wrapperOptions' => [
        'class' => 'col-md-2 inp_one_line',
    ],
];

$checkboxConfig = [
    'labelOptions' => [
        'class' => '',
        'style' => ''
    ],
    'wrapperOptions' => [
        'class' => '',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{input}\n{error}\n{endWrapper}",
];

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
    ],
]);

$showChBxRoles = EmployeeCompany::$addFlowRolesNeedConfig;
$payInvoiceCss = in_array($model->employee_role_id, $showChBxRoles) ? '' : 'hidden';
$formId = 'employee-form';
?>

<?php $form = \yii\bootstrap\ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => $formId,
    'method' => 'POST',
    'options' => [
        'class' => 'form-horizontal b-shadow-hide',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
        'data' => [
            'calc-url' => Url::to(['/salary/config-calc', 'id' => $model->employee_id]),
        ]
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-2 control-label bold-text width-212',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-7 inp_one_line',
        ],
        'hintOptions' => [
            'tag' => 'p',
            'class' => 'text-muted',
        ],
        'horizontalCssClasses' => [
            'offset' => 'col-md-offset-2',
            'hint' => 'col-md-7',
        ],
        'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
    ],

    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => true,
])); ?>

    <div class="form-body form-body_width">

        <div class="col-md-8 pad0">

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'email', [
                'hintOptions' => ['tag' => 'div',],
            ])->widget(\common\components\widgets\EmployeeTypeahead::className(), [
                'remoteUrl' => Url::to(['/dictionary/employee']),
                'related' => [
                    '#' . Html::getInputId($model, 'lastname') => 'lastname',
                    '#' . Html::getInputId($model, 'firstname') => 'firstname',
                    '#' . Html::getInputId($model, 'firstname_initial') => 'firstname_initial',
                    '#' . Html::getInputId($model, 'patronymic') => 'patronymic',
                    '#' . Html::getInputId($model, 'patronymic_initial') => 'patronymic_initial',
                    '#' . Html::getInputId($model, 'time_zone_id') => 'time_zone_id',
                    '#' . Html::getInputId($model, 'sex') => 'sex',
                    '#' . Html::getInputId($model, 'birthday') => 'birthday_format',
                    '#' . Html::getInputId($model, 'date_hiring') => 'date_hiring_format',
                    '#' . Html::getInputId($model, 'date_dismissal') => 'date_dismissal_format',
                    '#' . Html::getInputId($model, 'position') => 'position',
                    '#' . Html::getInputId($model, 'employee_role_id') => 'employee_role_id',
                    '#' . Html::getInputId($model, 'phone') => 'phone',
                ],
            ])->textInput([
                'placeholder' => 'Автозаполнение по email',
                'disabled' => $model->scenario !== 'create',
            ]); ?>

            <?= $form->field($model, 'lastname')->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'firstname')->textInput([
                'maxlength' => true,
            ]); ?>
            <?= $form->field($model, 'patronymic')->textInput([
                'maxlength' => true,
            ]); ?>

            <div class="form-group field-employeecompany-has_no_patronymic">
                <label class="col-md-2 control-label bold-text width-212" for="employeecompany-has_no_patronymic"></label>
                <div class="col-md-7">
                    <?= $form->field($model, 'has_no_patronymic')
                        ->label('Нет отчества', ['style' => 'font-weight:normal!important'])
                        ->checkbox(['template' => '{beginLabel}{input}{labelTitle}{endLabel}{error}{hint}'], true); ?>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>

        <?= \yii\bootstrap\Tabs::widget([
            'options' => ['class' => 'nav-form-tabs row'],
            'headerOptions' => ['class' => 'col-xs-3'],
            'linkOptions' => ['style' => 'white-space: nowrap;overflow: hidden;text-overflow: ellipsis;'],
            'items' => [
                [
                    'label' => 'Общая информация',
                    'encode' => false,
                    'content' => $this->render('_tab_general_info', [
                        'model' => $model,
                        'form' => $form
                    ]),
                    'active' => true,
                    'linkOptions' => [
                        'title' => 'Общая информация',
                    ],
                ],
                [
                    'label' => 'Данные паспорта для доверенности',
                    'encode' => false,
                    'content' => $this->render('_tab_passport', [
                        'model' => $model,
                        'form' => $form
                    ]),
                    'linkOptions' => [
                        'title' => 'Данные паспорта для доверенности',
                    ],
                ],
                [
                    'label' => 'Данные для отчета C3B-M',
                    'encode' => false,
                    'content' => $this->render('_tab_c3b', [
                        'model' => $model,
                        'form' => $form
                    ]),
                    'linkOptions' => [
                        'title' => 'Данные для отчета C3B-M',
                    ],
                ],
            ],
        ]); ?>
    </div>

    <?php /*
    <?= \frontend\widgets\NewResponsibleEmployeeWidget::widget([
        'form' => $form,
        'model' => $model,
        'formId' => $formId,
        'dateDismissalId' => Html::getInputId($model, 'date_dismissal'),
        'modalVisibleClass' => 'in',
    ]) ?>
    */ ?>
<?php \yii\bootstrap\ActiveForm::end(); ?>
<?php Modal::begin([
    'id' => 'modal-employeeSignatureImage',
    'header' => '<h1>Загрузить подпись</h1>',
    'toggleButton' => false,
]);
echo $this->render('partial/_partial_files_signature', [
    'model' => $model,
    'attr' => 'signature_file',
]);
Modal::end(); ?>
    <div class="tooltip_templates">
    <span id="tooltip_employye_role"
          style="display: inline-block;">
        <?php $roleArray = \common\models\employee\EmployeeRole::find()->actual()->orderBy([
            'sort' => SORT_ASC,
        ])->all() ?>
        <?php foreach ($roleArray as $role) : ?>
            <b><?= $role->name ?></b> – <?= nl2br($role->description) ?><br>
        <?php endforeach ?>
    </span>
    </div>

<?php $this->registerJs('
var showChBxRoles = ' . json_encode($showChBxRoles) . ';
var roleAccountant = ' . EmployeeRole::ROLE_ACCOUNTANT . ';

$(document).on("change", "#employeecompany-can_sign", function () {
    if ($(this).is(":checked")) {
        $(".employee_signature_element").removeClass("hidden_element");
    } else {
        $(".employee_signature_element").addClass("hidden_element");
    }
});
$(document).on("change", "#employeecompany-employee_role_id", function () {
    var canViewPriceForBuy = $("#employeecompany-can_view_price_for_buy");
    let form = this.form;
    let docAccessInput = $("#employeecompany-document_access_own_only", form);
    if (docAccessInput.length > 0) {
        let value = parseInt(this.value);
        let valOn = docAccessInput.data("default-on");
        let valOff = docAccessInput.data("always-off");
        docAccessInput.prop("checked", valOn.includes(value)).uniform("refresh");
        if (valOff.includes(value)) {
            docAccessInput.prop("checked", false).prop("disabled", true).uniform("refresh");
        } else {
            docAccessInput.prop("disabled", false).uniform("refresh");
        }
    }

    if (showChBxRoles.indexOf($(this).val()*1) === -1) {
        $(".employeecompany-can_invoice_add_flow").addClass("hidden");
        $("#employeecompany-can_invoice_add_flow").prop("checked", false).uniform("refresh");
    } else {
        $(".employeecompany-can_invoice_add_flow").removeClass("hidden");
    }
    if ($(this).val() == roleAccountant) {
        if (!canViewPriceForBuy.is(":checked")) {
            canViewPriceForBuy.prop("checked", true).uniform("refresh");
        }
    } else {
        if (canViewPriceForBuy.is(":checked")) {
            canViewPriceForBuy.prop("checked", false).uniform("refresh");
        }
    }
});

') ?>