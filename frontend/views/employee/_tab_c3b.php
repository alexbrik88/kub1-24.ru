<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.06.2019
 * Time: 12:28
 */

use yii\widgets\MaskedInput;

/* @var $model \common\models\EmployeeCompany */
/* @var $form yii\widgets\ActiveForm */

$textInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-6 label-width',
        'style' => 'width: 175px;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-6 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
?>
<div class="portlet box darkblue employeecompany-passport-info-wrapper">
    <div class="portlet-body min-width-container-passport">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group field-employeecompany-inn">
                    <?=
                    $form->field($model, 'inn', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->textInput([
                        'maxlength' => false,
                    ]);
                    ?>
                </div>
                <div class="form-group field-employeecompany-snils">
                    <?=
                    $form->field($model, 'snils', array_merge($textInputConfig, ['options' => ['class' => '']]))
                        ->widget(MaskedInput::className(), [
                            'mask' => '9{3}-9{3}-9{3} 9{2}',
                            'options' => [
                                'class' => 'form-control',
                                'placeholder' => 'ХХХ-ХХХ-ХХХ ХХ',
                            ],
                        ]);
                    ?>
                </div>
                <div class="form-group field-employeecompany-in_szvm">
                    <?=
                    $form->field($model, 'in_szvm', array_merge($textInputConfig, [
                        'options' => [
                            'class' => '',
                        ],
                    ]))->checkbox([], false);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
