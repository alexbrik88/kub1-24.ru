<?php

use common\models\employee\Employee;
use common\models\EmployeeCompany;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\employee\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $prompt backend\models\Prompt */

$this->title = 'Сотрудники';
$countEmployee = Employee::find()->where([
    'author_id' => Yii::$app->user->identity->id,
])->count();

$isRemoteEmployee = EmployeeCompany::find()->where([
    'company_id' => Yii::$app->user->identity->company->id,
    'employee_id' => Yii::$app->params['service']['remote_employee_id'] ?? -1,
])->exists();

?>
<div class="portlet box">

    <div class="btn-group pull-right title-buttons">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Employee::CREATE)): ?>
            <a href="<?= Url::to(['create']) ?>" class="btn yellow">
                <i class="fa fa-plus"></i> ДОБАВИТЬ
            </a>
        <?php endif; ?>
    </div>
    <h3 class="page-title" style="display: inline-block">
        <?= $this->title; ?>
    </h3>
    <div style="display: inline-block;margin-left: 25px;margin-right: 10px; vertical-align: middle">
        <label class="rounded-switch"
               for="activate_remote_employee"
               style="">
            <?= Html::checkbox('remote_employee', $isRemoteEmployee, [
                'id' => 'activate_remote_employee',
                'class' => 'switch'
            ]); ?>
            <span class="sliderr no-gray yes-yellow round"></span>
        </label>
    </div>
    <div style="display: inline-block;vertical-align: top;padding-top: 7px;">
        <span style="font-size: 14px">Включить доступ сотруднику технической поддержки</span>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Список сотрудников
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-container clearfix" style="">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'dataColumnClass' => \common\components\grid\DataColumn::className(),

                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                'columns' => [
                    [
                        'attribute' => 'fio',
                        'label' => 'Фамилия Имя Отчество',
                        'format' => 'html',
                        'value' => function ($data) {
                            $data = $data->employee->getEmployeeCompany()->andWhere(['employee_company.company_id' => Yii::$app->user->identity->company_id])->one();
                            $name = trim($data->fio);
                            if (empty($name)) {
                                $name = '(не задано)';
                            }
                            return (Yii::$app->user->can(frontend\rbac\permissions\Employee::VIEW))
                                ? Html::a(Html::encode($name), ['view', 'id' => $data->employee_id])
                                : $name;
                        },
                    ],

                    'position',

                    [
                        'attribute' => 'employee_role_id',
                        'filter' => ArrayHelper::merge(['' => 'Все'], $searchModel->getEmployeeRoleArray()),
                        'value' => 'employeeRole.name',
                    ],

                    [
                        'attribute' => 'is_working',
                        'label' => 'Статус',
                        'filter' => $searchModel->getEmployeeStatusArray(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter dropdown-left',
                        ],
                        'value' => function ($data) {
                            return Employee::$status_message[$data->is_working];
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?php if ($countEmployee == 0) : ?>
    <?= \frontend\widgets\PromptWidget::widget([
        'prompt' => $prompt,
    ]); ?>
<?php endif; ?>

<?php $this->registerJs(<<<JS

$(document).on('change', '#activate_remote_employee', function() {
    $(this).attr('disabled', true);
    var isChecked = $(this).prop('checked');
    $.post('change-remote-employee', {'isChecked': isChecked ? 1:0}, function(data) { /* refresh page */ });
});

JS
);