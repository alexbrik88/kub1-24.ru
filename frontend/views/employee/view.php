<?php

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\TimeZone;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model EmployeeCompany */

$this->title = $model->fio;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
$editMode = \Yii::$app->request->isPost;

if (in_array($model->employee_role_id, EmployeeCompany::$addFlowRoles)) {
    $canAddFlow = true;
} elseif (!in_array($model->employee_role_id, EmployeeCompany::$addFlowRolesNeedConfig)) {
    $canAddFlow = false;
} else {
    $canAddFlow = (bool) $model->can_invoice_add_flow;
}
?>

<div class="page-content-in">
    <?php if (Yii::$app->user->can(frontend\rbac\permissions\Employee::INDEX)): ?>
        <a class="back-to-customers" href="<?= Url::to(['index']) ?>">Назад к списку</a>
    <?php endif; ?>

    <?php if (!$model->is_working): ?>
        <h2>Уволен</h2>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-8">
            <div class="portlet customer-info customer-info-cash <?= $editMode ? 'hide' : '' ?>">
                <div class="portlet-title">
                    <div class="col-md-10 caption">
                        <?= Html::encode($model->fio); ?>
                    </div>

                    <div class="actions">
                        <?= \frontend\modules\documents\widgets\CreatedByWidget::widget([
                            'createdAt' => date("d.m.Y", $model->created_at),
                            'author' => $model->employee->author === null ? '' : $model->employee->author->getFio(),
                        ]); ?>

                        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Employee::UPDATE)): ?>
                            <a href="#" class="btn darkblue btn-sm edit" id="edit-employee" title="Редактировать"><i
                                        class="icon-pencil"></i></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="portlet-body no_mrg_bottom">
                    <table id="datatable_ajax" class="table">
                        <tr>
                            <td class="bold-text border-space">Пол:</td>
                            <td class=""><?= ArrayHelper::getValue(Employee::$sex_message, $model->sex, 'не указано'); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата рождения:</td>
                            <td><?= DateHelper::format($model->birthday, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Часовой пояс:</td>
                            <td><?php $timeZone = TimeZone::findOne($model->time_zone_id);
                                echo $timeZone['out_time_zone'] ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата приёма на работу:</td>
                            <td><?= DateHelper::format($model->date_hiring, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Дата увольнения:</td>
                            <td><?= DateHelper::format($model->date_dismissal, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Должность:</td>
                            <td><?= Html::encode($model->position); ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Роль:</td>
                            <td><?= $model->employeeRole->name; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Телефон:</td>
                            <td><?= $model->phone; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">E-mail:</td>
                            <td><?= $model->email; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Право подписи на документах:</td>
                            <td>
                                <?php if ($model->can_sign && $model->signBasisDocument) : ?>
                                    <?= Html::encode($model->signBasisDocument->name); ?>
                                    №
                                    <?= Html::encode($model->sign_document_number); ?>
                                    от
                                    <?= DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                <?php else: ?>
                                    Нет
                                <?php endif ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Видит закупочные цены:</td>
                            <td><?= $model->can_view_price_for_buy ? 'Да' : 'Нет'; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Может добавлять номенклатуру:</td>
                            <td><?= $model->is_product_admin ? 'Да' : 'Нет'; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Может добавлять оплату по счетам:</td>
                            <td><?= $canAddFlow ? 'Да' : 'Нет'; ?></td>
                        </tr>
                        <tr>
                            <td class="bold-text border-space">Видит ТОЛЬКО счета, которые ВЫСТАВИЛ САМ:</td>
                            <td><?= $model->document_access_own_only ? 'Да' : 'Нет'; ?></td>
                        </tr>
                    </table>
                    <span class="uneditable table-link-info-1">Указанный e-mail является также логином для доступа в систему </span>
                </div>
            </div>
            <div class="edit-employee-form<?= $editMode ? '' : ' hide' ?>" id="edit-cash-order">
                <div class="portlet">
                    <div class="portlet-body">
                        <?= $this->render('_form_employee_company', [
                            'model' => $model,
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 pull-right <?= $editMode ? 'hide' : '' ?>">
            <?php if (false && Yii::$app->user->can(frontend\rbac\UserRole::ROLE_CHIEF)): ?>
                <div class="row form-group">
                    <div class="col-sm-12">
                        <?= Html::a('Настройка расчета ЗП', ['/salary/config', 'id' => $model->employee_id], [
                            'class' => 'btn yellow text-right pull-right clearfix',
                        ]); ?>
                    </div>
                </div>
                <?php if ($model->getHasSalary()) : ?>
                    <div style="margin-bottom: 5px;">
                        Это видит только пользователь с ролью "Руководитель"
                    </div>
                    <div class="custom-info-box">
                        <?php if ($model->has_salary_1) : ?>
                            <div>
                                Оклад 1 = <?= number_format($model->salary_1_amount / 100, 2, ',', ' ') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($model->has_bonus_1) : ?>
                            <div>
                                Премия 1 = <?= number_format($model->bonus_1_amount / 100, 2, ',', ' ') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($model->has_salary_2) : ?>
                            <div>
                                Оклад 2 = <?= number_format($model->salary_2_amount / 100, 2, ',', ' ') ?>
                            </div>
                        <?php endif ?>
                        <?php if ($model->has_bonus_2) : ?>
                            <div>
                                Премия 2 = <?= number_format($model->bonus_2_amount / 100, 2, ',', ' ') ?>
                            </div>
                        <?php endif ?>
                        <div>
                            Итого = <?= number_format($model->getTotalAmount() / 100, 2, ',', ' ') ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row action-buttons" id="buttons-fixed">
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button' . ($editMode ? '' : ' hide'),
            'form' => 'employee-form',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
            'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg' . ($editMode ? '' : ' hide'),
            'title' => 'Сохранить',
            'form' => 'employee-form',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 55%;">
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <button type="button"
                class="btn darkblue btn-cancel darkblue <?= $editMode ? '' : 'hide' ?> widthe-100 hidden-md hidden-sm hidden-xs">
            Отменить
        </button>
        <button type="button"
                class="btn darkblue btn-cancel darkblue <?= $editMode ? '' : 'hide' ?> widthe-100 hidden-lg"
                title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->id != $model->employee_id && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE)) {
            echo \frontend\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['delete', 'id' => $model->employee_id, 'companyId' => Yii::$app->user->identity->company->id]),
                'message' => 'Вы уверены, что хотите удалить сотрудника?',
            ]);
            echo \frontend\widgets\ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                    'title' => 'Удалить',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['delete', 'id' => $model->employee_id, 'companyId' => Yii::$app->user->identity->company->id]),
                'message' => 'Вы уверены, что хотите удалить сотрудника?',
            ]);
        } ?>
    </div>
</div>
