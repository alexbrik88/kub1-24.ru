<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\employee\Employee */

$this->title = 'Редактировать сотрудника';
?>
<div class="employee-create">

    <?= $this->render('_form_employee_company', [
        'model' => $model,
        'salaryModel' => null
    ]); ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'form' => 'employee-form',
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                    'form' => 'employee-form',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <a href="<?= Url::to(['index']) ?>">
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
                </a>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
        </div>
    </div>

</div>
