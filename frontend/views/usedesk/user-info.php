<?php

use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariffGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Company */

$tariffArray = [];
if ($model !== null) {
    $groupArray = SubscribeTariffGroup::find()->where([
        'is_active' => true,
    ])->orderBy('id')->indexBy('id')->all();
    foreach ($groupArray as $group) {
        $subscribes = SubscribeHelper::getPayedSubscriptions($model->id, $group->id);
        if ($subscribes) {
            $name = $group->name;
            $expDate = SubscribeHelper::getExpireDate($subscribes);
            $date = date('d.m.Y', $expDate);
            $days = Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => SubscribeHelper::getExpireLeftDays($expDate),
            ]);

            $tariffArray[] = Html::tag('div', "\"{$name}\" активна до $date (осталось: $days)");
        }
    }
}
?>

<?php if ($model !== null) : ?>
    <?= $tariffArray ? implode("\n", $tariffArray) . "<br>" : '' ?>
    <div>Тел: <?= $model->phone ?></div>
    <div>ИНН: <?= $model->inn ?></div>
    <div><?= $model->getTitle(true) ?></div>
<?php else : ?>
    Нет данных
<?php endif ?>
