<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\out\OutInvoice */
/* @var $backUrl string */

$this->title = 'Ссылка на создание счета';
?>
<div class="out-invoice-create">
    <div class="portlet box">
        <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'backUrl' => $backUrl,
    ]) ?>
</div>
