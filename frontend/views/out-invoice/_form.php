<?php

use common\models\Company;
use common\models\out\OutInvoice;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\out\OutInvoice */
/* @var $form yii\widgets\ActiveForm */
/* @var $backUrl string */
/* @var $company Company */

$this->registerJs('
    $("#outinvoice-is_outer_shopcart").on("change", function() {
        if ($(this).prop("checked")) {
            $("#selected-product-list .selected-product-row").remove();
            $("#new-product-row").removeClass("hidden");
            $(".outer_shopcart-collapse").collapse("hide");
            $(".outer_shopcart-collapse_in").collapse("show");
            $(".outer_shopcart-collapse input[type=checkbox]").prop("checked", false).trigger("change").uniform("refresh");
        } else {
            $(".outer_shopcart-collapse").collapse("show");
            $(".outer_shopcart-collapse_in").collapse("hide");
        }
    });
    $("#add_num").on("change", function() {
        if ($(this).prop("checked")) {
            $("#add_num_collapse").collapse("show");
        } else {
            $("#add_num_collapse").collapse("hide");
            $("#outinvoice-additional_number").val("");
            $("#outinvoice-is_additional_number_before input[value=0]").prop("checked", true);
            $("#outinvoice-is_additional_number_before input[type=radio]:not(.md-radiobtn)").uniform("refresh");
        }
    });
    $("#invoice_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#invoice_comment_collapse").collapse("show");
        } else {
            $("#invoice_comment_collapse").collapse("hide");
            $("#outinvoice-invoice_comment").val("");
        }
    });
    $("#add_comment").on("change", function() {
        if ($(this).prop("checked")) {
            $("#add_comment_collapse").collapse("show");
        } else {
            $("#add_comment_collapse").collapse("hide");
            $("#outinvoice-add_comment").val("");
        }
    });
    $(".check-collapse").on("change", function() {
        if ($(this).prop("checked")) {
            $($(this).data("target")).collapse("show");
        } else {
            $($(this).data("target")).collapse("hide");
            $("input", $($(this).data("target"))).val("");
        }
    });
    $(".tooltip-hint").tooltipster({
        "theme": ["tooltipster-kub"],
        "trigger": "click",
        "contentAsHTML": true,
    });
');

$checkboxTemplate = "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}";
$dataHint = Html::tag('span', '', [
    'class' => 'tooltip-hint ico-question',
    'data-tooltip-content' => '#tooltip_strict_data',
    'style' => 'margin: 0; cursor: pointer;'
]);
$resultHint = Html::tag('span', '', [
    'class' => 'tooltip-hint ico-question',
    'data-tooltip-content' => '#tooltip_send_result',
    'style' => 'margin: 0; cursor: pointer;'
]);
$hintOptions = [
    'style' => 'position: absolute; top: -5px; left: 40px;'
];
$company = Yii::$app->user->identity->company;

$code = '<?php
$orderList = [
    [
        "type" => "Укажите ТИП товара \'PRODUCT\' или \'SERVICE\'",
        "title" => "Укажите НАЗВАНИЕ ТОВАРА",
        "unit_code" => "Укажите код ОКЕИ ЕДИНИЦЫ ИЗМЕРЕНИЯ (796)",
        "quantity" => "Укажите КОЛИЧЕСТВО",
        "price" => "Укажите ЦЕНУ ЗА ЕДИНИЦУ в рублях",
        "nds" => "Укажите НДС в процентах, если есть",
    ],
];
?>
<form action="'.$model->outUrl.'" method="post">
    <?php foreach ($orderList as $key => $orderParams) : ?>
        <?php foreach ($orderParams as $name => $value) : ?>
            <input type="hidden" name="order_list[<?= $key ?>][<?= $name ?>]" value="<?= $value ?>">
        <?php endforeach ?>
    <?php endforeach ?>
    <input type="submit" name="form" value="Выставить счет" style="
        cursor: pointer;
        background: #4276a4;
        color: #fff;
        border-width: 0;
        padding: 7px 14px;
        font-size: 14px;">
</form>';

$openAdditionalOptions = false;

if ($isB2B = (strpos($backUrl, '/b2b/') !== false) && 'create' == $this->context->action->id) {
    $typeB2B = Yii::$app->request->get('type');
    if ($typeB2B == 'landing') {
        $model->is_autocomplete = 1;
        $model->is_bik_autocomplete = 1;
        $openAdditionalOptions = true;
    } else {
        $model->is_outer_shopcart = 1;
    }
}

?>

<style type="text/css">
    #out-invoice-form label {
        color: #333333;
    }

    #out-invoice-form select,
    #out-invoice-form input[type="text"] {
        width: 100%;
    }
</style>

<?php $form = ActiveForm::begin([
    'id' => 'out-invoice-form',
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            'offset' => '',
            'wrapper' => 'col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<div class="out-invoice-form" style="max-width: 600px;">

    <?= $form->errorSummary($model); ?>

    <?= Html::hiddenInput('out_id', $model->id) ?>

    <?= $form->field($model, 'status')->dropDownList(OutInvoice::$statusAvailable, [
        'disabled' => !$company->canAddOutInvoice() && $model->status == OutInvoice::INACTIVE,
    ]); ?>

    <?= $form->field($model, 'note')->textInput() ?>

    <?= $form->field($model, 'return_url')->textInput([
        'placeholder' => 'http://name.com',
    ]) ?>

    <?= $form->field($model, 'is_outer_shopcart', [
        'labelOptions' => [
            'style' => 'padding-top: 0;',
        ]
    ])->checkbox([], false); ?>

    <div class="outer_shopcart-collapse collapse <?= $model->is_outer_shopcart ? '' : 'in'; ?>">
        <?= $form->field($model, 'productId', [
            'parts' => [
                '{input}' => $this->render('_form_product_table', ['model' => $model]),
            ],
            'horizontalCssClasses' => [
                'label' => 'col-sm-12',
                'offset' => '',
                'wrapper' => 'col-sm-12',
                'error' => '',
                'hint' => '',
            ],
            'labelOptions' => [
                'style' => 'margin-bottom: 5px;',
            ]
        ])->render(); ?>
    </div>
</div>

<div class="outer_shopcart-collapse_in collapse <?= $model->is_outer_shopcart ? 'in' : ''; ?>">
   <div style="margin: 0 0 15px;">
        <?= Html::A('Пример кода для вставки на сайт <span class="caret"></span>', '#outer_shopcart_code', [
            'class' => 'collapse-toggle-link',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'true',
            'aria-controls' => 'outer_shopcart_code',
        ]) ?>
    </div>
    <div class="collapse in" id="outer_shopcart_code" style="padding-bottom: 15px;">
        <xmp style="border: 1px solid #ccc; margin: 0; padding: 5px 10px; background-color: #f5f5f5;"><?= $code ?></xmp>
    </div>
</div>

<div class="out-invoice-form" style="max-width: 600px;">
    <div style="margin: 0 0 15px;">
        <?= Html::A('Дополнительные настройки <span class="caret"></span>', '#additionalSettings', [
            'class' => 'collapse-toggle-link',
            'data-toggle' => 'collapse',
            'aria-expanded' => ($isB2B && $openAdditionalOptions) ? 'true' : 'false',
            'aria-controls' => 'additionalSettings',
        ]) ?>
    </div>
    <div class="collapse <?= ($isB2B && $openAdditionalOptions) ? 'in':'' ?>" id="additionalSettings">
        <div class="">
            <?= $form->field($model, 'show_article', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>
            <?= $form->field($model, 'is_autocomplete', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>
            <?= $form->field($model, 'is_bik_autocomplete', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>
            <?= $form->field($model, 'send_with_stamp', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>

            <div class="form-group field-outinvoice-additional_number" style="overflow: hidden;">
                <label class="control-label col-sm-4" style="padding-top: 0;" for="add_num">
                    К номеру счета Доп номер
                </label>

                <div class="col-sm-8" style="margin-bottom: -20px;">
                    <?= Html::checkbox('add_num', (boolean)$model->additional_number, [
                        'id' => 'add_num',
                    ]) ?>
                    <div id="add_num_collapse" class="collapse <?= $model->additional_number ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= $form->field($model, 'is_additional_number_before', [
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-12',
                                'offset' => '',
                                'wrapper' => 'col-sm-12',
                                'error' => 'col-sm-12',
                                'hint' => 'col-sm-12',
                            ],
                        ])->radioList(Company::$addNumPositions, [
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                    'style' => 'margin: 0 !important;'
                                ]);
                            },
                        ])->label(false)->error(false)->hint($form->field($model, 'additional_number', [
                            'labelOptions' => [
                                'style' => 'width: 95px; padding-right: 0;'
                            ],
                            'inputOptions' => [
                                'style' => 'width: 150px;'
                            ],
                        ])); ?>
                    </div>
                </div>
            </div>

            <div class="form-group field-outinvoice-invoice_comment" style="overflow: hidden;">
                <?= Html::activeLabel($model, 'invoice_comment', [
                    'class' => 'control-label col-sm-4',
                    'style' => 'padding-top: 0;',
                    'for' => 'invoice_comment',
                ]) ?>

                <div class="col-sm-8">
                    <?= Html::checkbox('invoice_comment', (boolean)$model->invoice_comment, [
                        'id' => 'invoice_comment',
                    ]) ?>
                    <div id="invoice_comment_collapse" class="collapse <?= $model->invoice_comment ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= Html::activeTextarea($model, 'invoice_comment', [
                            'maxlength' => true,
                            'style' => 'width: 100%;',
                        ]); ?>
                    </div>
                </div>
            </div>

            <div class="form-group field-outinvoice-add_comment" style="overflow: hidden;">
                <?= Html::activeLabel($model, 'add_comment', [
                    'class' => 'control-label col-sm-4',
                    'style' => 'padding-top: 0;',
                    'for' => 'add_comment',
                ]) ?>

                <div class="col-sm-8">
                    <?= Html::checkbox('add_comment', (boolean)$model->add_comment, [
                        'id' => 'add_comment',
                    ]) ?>
                    <div id="add_comment_collapse" class="collapse <?= $model->add_comment ? 'in' : ''; ?>"
                         style="margin-top: 10px;">
                        <?= Html::activeTextarea($model, 'add_comment', [
                            'maxlength' => true,
                            'style' => 'width: 100%;',
                        ]); ?>
                    </div>
                </div>
            </div>

            <?= $form->field($model, 'notify_to_email', [
                'labelOptions' => [
                    'style' => 'padding-top: 0;',
                ]
            ])->checkbox([], false); ?>

            <span style="font-weight: 600;font-size: 14px;">Для интеграции по API (информация для программиста):</span>

            <div class="outer_shopcart-collapse collapse <?= $model->is_outer_shopcart ? '' : 'in'; ?>">
                <?= $form->field($model, 'strict_data', [
                    'template' => $checkboxTemplate,
                    'hintOptions' => $hintOptions,
                    'wrapperOptions' => [
                        'style' => 'padding-top: 7px;'
                    ]
                ])->hint($dataHint)->checkbox([
                    'class' => 'check-collapse',
                    'data-target' => '#data_url_collapse'
                ], false) ?>
                <div id="data_url_collapse" class="collapse <?= $model->strict_data ? 'in' : ''; ?>">
                    <?= $form->field($model, 'data_url')->textInput([
                        'placeholder' => 'http://name.com/get-data',
                    ]) ?>
                </div>
            </div>

            <?= $form->field($model, 'send_result', [
                'template' => $checkboxTemplate,
                'hintOptions' => $hintOptions,
                'wrapperOptions' => [
                    'style' => 'padding-top: 7px;'
                ]
            ])->hint($resultHint)->checkbox([
                'class' => 'check-collapse',
                'data-target' => '#result_url_collapse'
            ], false) ?>
            <div id="result_url_collapse" class="collapse <?= $model->send_result ? 'in' : ''; ?>">
                <?= $form->field($model, 'result_url')->textInput([
                    'placeholder' => 'http://name.com/result',
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'btn darkblue ladda-button',
                'data-style' => 'expand-right',
                'style' => 'color: #fff; width: 140px;'
            ]) ?>
            <?= Html::a('Отменить', $backUrl, [
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; width: 140px;',
            ]) ?>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>

<div style="display: none;">
    <span id="tooltip_strict_data">
        Выберите и укажите "URL данных",
        <br>
        чтобы запросить количество и
        <br>
        цену товаров со своего сайта.
        <br>
        При перенаправлении пользователя
        <br>
        необходимо добавлять параметры
        <br>
        "uid" и "hash",
        <br>
        которые будут использованы в
        <br>
        запросе для получения данных.
        <br>
        Остались вопросы? Пишите нам
        <br>
        на support@kub-24.ru
    </span>
    <span id="tooltip_send_result">
        Выберите и укажите "URL результата",
        <br>
        чтобы отправить на свой сайт
        <br>
        данные созданного счета.
        <br>
        Остались вопросы? Пишите нам
        <br>
        на support@kub-24.ru
    </span>
</div>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_existed', [
    'contentCssClass' => 'to-outInvoice-content',
]); ?>
