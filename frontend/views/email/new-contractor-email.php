<?php

use common\components\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\document\UserEmail $userEmail */

$html = '<div class="row who-send-container">';
$html .= '<div class="container-who-send-label" style="width: 100%!important;">';
$html .= '<div class="form-group" style="margin-bottom: 0;">';
$html .= '<div class="checkbox" style="display: inline-block;width: 81%;">';
$html .= Html::checkbox('InvoiceSendForm[sendToUser][' . $userEmail->id . ']', false, [
    'id' => 'invoicesendform-sendtouser',
    'data-value' => $userEmail->email,
    'label' => $userEmail->fio ? '<span class="email-label">' . $userEmail->fio .
        '<br><span style="padding-left: 25px">' . $userEmail->email . '</span></span>' :
        '<span class="email-label">' . $userEmail->email . '</span>',
    'labelOptions' => [
        'style' => 'font-size: 13px;',
    ],
]);
$html .= '</div>';
$html .= '<div class="update-user-email-block" style="display: none;">';
$html .= Html::textInput('fio', $userEmail->fio, [
    'class' => 'form-control',
    'id' => 'user-fio',
    'placeholder' => 'Фамилия И.О.',
]);
$html .= Html::textInput('email', $userEmail->email, [
    'class' => 'form-control',
    'id' => 'user-email',
    'placeholder' => 'E-mail',
]);
$html .= '</div>';
$html .= '<div class="email-actions">';
$html .= '<span class="glyphicon glyphicon-pencil update-user-email"
                                            data-url="' . Url::to(['/email/update-user-email', 'id' => $userEmail->id]) . '"></span>';
$html .= '<span class="glyphicon glyphicon-trash delete-user-email user-email-' . $userEmail->id . '"
                                            data-url="' . Url::to(['/email/delete-user-email', 'id' => $userEmail->id]) . '"
                                            data-id="' . $userEmail->id . '" data-toggle="modal"
                                            data-target="#delete-confirm-user-email"></span>';
$html .= '<span class="fa fa-reply fa-2x undo-user-email" style="display: none;"></span>';
$html .= '<span class="fa fa-floppy-o fa-2x save-user-email" style="display: none;"
                                            data-url="' . Url::to(['/email/update-user-email', 'id' => $userEmail->id]) . '"></span>';
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';
$html .= '</div>';

echo $html;