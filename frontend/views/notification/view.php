<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2017
 * Time: 19:52
 */

use common\models\notification\Notification;
use php_rutils\RUtils;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;

/* @var $this yii\web\View
 * @var $model Notification
 *
 * @var $user Employee
 */

$user = Yii::$app->user->identity;
$isPassed = ArrayHelper::getValue($model, 'companyNotification.status') == Notification::STATUS_PASSED;
?>
<div class="row notification-list"
     style="margin-left: 10px;margin-right: 10px;">
    <span class="event-date">
        <?= RUtils::dt()->ruStrFTime([
            'date' => $model->event_date,
            'format' => 'd F Y',
            'monthInflected' => true,
        ]); ?>
    </span>

    <div class="row" style="margin-left: 0;margin-right: 0;">
        <span class="title">
            <?= $model->title; ?>
        </span>
        <?php if (in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_ACCOUNTANT])): ?>
            <div class="btn-group btn-toggle switch">
                <?= Html::button('Сделано', [
                    'class' => 'btn btn-default passed ' . ($isPassed ? 'active' : ''),
                    'data-url' => Url::to([
                        'change-status',
                        'id' => $model->id,
                    ]),
                    'status' => Notification::STATUS_PASSED,
                ]); ?>
                <?= Html::button('Не сделано', [
                    'class' => 'btn btn-default not-passed ' . ($isPassed ? '' : 'active'),
                    'data-url' => Url::to([
                        'change-status',
                        'id' => $model->id,
                    ]),
                    'status' => Notification::STATUS_NOT_PASSED,
                ]); ?>
            </div>
        <?php endif; ?>
    </div>
    <span class="term">
        <b><i>Срок:</i></b> до <?= RUtils::dt()->ruStrFTime([
            'date' => $model->event_date,
            'format' => 'd F Y',
            'monthInflected' => true,
        ]); ?>
    </span>
    <span class="text">
        <b><i>Кто:</i></b> <?= $model->text; ?>
    </span>
    <span class="fine">
        <b><i>Штраф:</i></b> <?= $model->fine; ?>
    </span>

    <div class="box-title-info-txt clear-box"
         style="margin-bottom: 15px;"></div>
</div>
