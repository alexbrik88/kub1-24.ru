<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.07.2017
 * Time: 18:58
 */

use common\models\notification\Notification;
use yii\data\ActiveDataProvider;
use yii\bootstrap\Html;
use yii\helpers\Url;
use php_rutils\RUtils;
use yii\widgets\ListView;

/* @var $this yii\web\View
 * @var $period []
 * @var $dataProvider ActiveDataProvider
 */

$this->title = 'Налоговый календарь';
?>
<div class="notification-index">
    <?= Html::a('Назад', Url::to(['/site/index'])); ?>
    <div class="portlet box row "
         style="margin:10px 0 10px 0;">
        <div class="col-md-9 col-sm-9 p-l-0">
            <h3 class="page-title"><?= $this->title; ?></h3>
        </div>

        <div class="col-md-3 col-sm-3">
            <div class="fc-toolbar">
                <div class="fc-left"></div>
                <div class="fc-right"></div>
                <div class="fc-center">
                    <?= Html::a('<span class="fc-icon fc-icon-left-single-arrow"></span>',
                        Url::to(['index', 'month' => $period['prevMonth'], 'year' => $period['prevYear']]), [
                            'class' => 'fc-prev-button fc-button fc-state-default fc-corner-left fc-corner-right',
                        ]); ?>
                    <h2>
                        <?= RUtils::dt()->ruStrFTime([
                            'date' => '01-' . $period['month'] . '-' . $period['year'],
                            'format' => 'F Y',
                        ]); ?>
                    </h2>
                    <?= Html::a('<span class="fc-icon fc-icon-right-single-arrow"></span>',
                        Url::to(['index', 'month' => $period['nextMonth'], 'year' => $period['nextYear']]), [
                            'class' => 'fc-next-button fc-button fc-state-default fc-corner-left fc-corner-right',
                        ]); ?>
                </div>
                <div class="fc-clear"></div>
            </div>
        </div>
    </div>

    <div class="portlet box darkblue">
        <div class="portlet-title row-fluid">
        </div>
        <div class="portlet-body accounts-list">
            <?= ListView::widget([
                'options' => [
                    'class' => 'list-view',
                ],
                'layout' => "{items}\n{pager}",
                'dataProvider' => $dataProvider,
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'emptyText' => 'В ' . Notification::$monthHelper[(int) $period['month']] . ' нет событий по налоговому календарю.',
                'itemView' => function (Notification $model) {
                    return $this->render('view', [
                        'model' => $model,
                    ]);
                },
            ]); ?>
        </div>
    </div>
</div>
