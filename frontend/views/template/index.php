<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $templates common\models\template\Template[] */
$defaultCssClasses = 0; // hidden
$cssClasses = [
    0 => [
        'link' => 'expand',
        'body' => 'display:none;',
    ],
    1 => [
        'link' => 'collapse',
        'body' => '',
    ],
];

$this->title = 'Шаблоны';
$this->params['breadcrumbs'][] = $this->title;
?>

    <h3 class="page-title"><?= Html::encode($this->title) ?></h3>

    <br/>
    <div class="portlet box darkblue">
        <div class="portlet-title row">
            <div class="col-md-3"
                 style=" font-size: 18px;line-height: 18px;font-weight: 300; padding: 12px 14px;">
                Поиск по шаблонам
            </div>
            <div class="col-md-9">
                <form action="find" method="GET">
                    <div class=" field-invoicesearch-bynumber"
                         style="margin-top: 4px;">
                        <input type="text" class="form-control" name="q"
                               placeholder="Поиск"
                               style="float: left; width: 90%;">
                        <button type="submit"
                                class="btn btn__ins btn-sm default btn_marg_down green-haze"
                                style="float: right; width: 10%; margin-top: 0;">
                            НАЙТИ
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <?php if (isset($queryModels)) { ?>
            <div class="portlet-body">
                <ul>
                    <?php if ($queryModels->all() != null) { ?>
                        <?php foreach ($queryModels->all() as $queryModel) { ?>
                            <li>
                                <?= Html::a($queryModel->file_name, [
                                    'get-file',
                                    'id' => $queryModel->id,
                                    'filename' => $queryModel->file_name,
                                ], [
                                    'class' => 'padding-left-10',
                                    'target' => '_blank',
                                ]) ?>
                            </li>
                        <?php } ?>
                    <?php } elseif ($queryModels->all() == null) { ?>
                        <li>Ничего не найдено</li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
<?php if (!empty($templates)) : ?>
    <?php foreach ($templates as $template) : ?>
        <div class="portlet box darkblue">
            <div class="portlet-title">
                <div class="caption">
                    <?= $template->title; ?>
                </div>

                <div class="tools arrow-tools">
                    <a href="javascript:;"
                       class="<?= $cssClasses[$defaultCssClasses]['link']; ?>"
                       data-original-title="" title=""
                       style="visibility:visible;"></a>
                </div>
                <?php if ($template->title == 'Мои шаблоны') : ?>
                    <div class="addTemplate">
                        <a href="<?= \Yii::$app->urlManager->createAbsoluteUrl(['template/create']) ?>">
                            <button>
                                + Добавить
                            </button>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="portlet-body"
                 style="<?= $cssClasses[$defaultCssClasses]['body']; ?>">

                <ul>
                    <?php foreach ($template->templateFiles as $file): ?>
                        <li>
                            <a href="<?= \yii\helpers\Url::to(['get-file', 'id' => $file->id, 'filename' => $file->file_name,]); ?>"
                               class="padding-left-10"
                               target="_blank"><?= $file->file_name; ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>