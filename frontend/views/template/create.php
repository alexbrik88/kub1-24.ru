<?php
use yii\helpers\Html;

$this->title = ' Добавление Шаблона';
$this->params['breadcrumbs'][] = $this->title;
?>
<a href="<?= \Yii::$app->urlManager->createAbsoluteUrl(['template/index'])?>">
    Назад к списку
</a>
<h1>
    Создание шаблона
</h1>
<?php
$form = \yii\widgets\ActiveForm::begin([
    'id' => 'template-form',
    'action' => ['create'],
    'method' => 'post',
//    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
//    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => '',
        ],
    ],
]);
?>
<?= $form->field($modelTemplateFile, 'file_name')->textInput(['placeholder' => 'Введите название шаблона'])->label(''); ?>
<?= $form->field($modelTemplateFile, 'hat_document')->checkbox(); ?>
<?php $form->end(); ?>