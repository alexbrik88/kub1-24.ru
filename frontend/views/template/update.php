<?php
use yii\helpers\Html;

$this->title = ' Добавление Шаблона';
$this->params['breadcrumbs'][] = $this->title;
?>
<a href="<?= \Yii::$app->urlManager->createAbsoluteUrl(['template/index'])?>">
    Назад к списку
</a>
<h1>
    Изменение шаблона
</h1>
<?php
$form = \yii\widgets\ActiveForm::begin([
    'id' => 'one-s-export-form',
    'action' => ['create'],
    'method' => 'post',
//    'enableClientValidation' => true,
//    'enableAjaxValidation' => true,
//    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'options' => [
            'class' => '',
        ],
    ],
]);
?>
<?php $form->end(); ?>