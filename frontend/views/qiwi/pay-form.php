<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $data array */

?>

<?php if (empty($data)) : ?>
    <h4 style="color: red;">Ошибка оплаты счета</h4>
<?php else : ?>
    <?= Html::beginForm('https://oplata.qiwi.com/create', 'get', [
        'id' => 'qiwi-form',
    ]); ?>
    <?php foreach ($data as $key => $value) : ?>
        <?= Html::hiddenInput($key, $value) ?>
    <?php endforeach; ?>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function(){
            document.forms["qiwi-form"].submit();
        });
    </script>
<?php endif; ?>