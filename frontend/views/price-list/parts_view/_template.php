<?php

use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\AgreementTemplate;
use common\components\date\DateHelper;
use common\models\product\PriceList;
use common\models\product\Product;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\AgreementType;
use common\components\image\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */

$signatureLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
$printLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);
$logoLink = EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

$images = [
    'logo' => $logoLink,
    'print' => $printLink,
    'signature' => $signatureLink
];

$isProduct = $model->production_type == Product::PRODUCTION_TYPE_GOODS || $model->production_type == Product::PRODUCTION_TYPE_ALL;
$sortAttr = $model->getSortAttributeName();
$priceListOrders = $model->getPriceListOrders()
    ->joinWith('productGroup')
    ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
    ->all();
$homeUrl = Yii::$app->user->isGuest && YII_ENV_PROD ? Yii::$app->params['serviceSite'] : Yii::$app->homeUrl;

$author = $model->priceListContact ?: $model->author;
$contacts = [];
if (!empty($author->phone)) $contacts[] = 'Тел: ' . $author->phone;
if (!empty($author->email)) $contacts[] = 'E-mail: ' . $author->email;
if (!empty($author->site))  $contacts[] = 'Сайт: ' . $author->site;
$contacts = implode(', ', $contacts);

?>

<div class="page-content-in m-size-div container-first-account-table no_min_h pad0" style="border:1px solid #4276a4; margin-top:3px;">

    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height:25px;">
            <div class="col-xs-12 pad0 font-bold" style="height: inherit">
                <div class="actions" style="margin-top: -8px;">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                    ]); ?>

                    <?php if (!$model->is_archive && Yii::$app->user->can(frontend\rbac\permissions\Product::UPDATE, [
                            'model' => $model,
                        ])
                    ): ?>
                        <a href="<?= Url::to(['update-card', 'id' => $model->id]); ?>"
                           title="Редактировать" class="btn darkblue btn-sm"
                           style="padding-bottom: 4px !important;">
                            <i class="icon-pencil"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

            <table class="header-price-list no-border" style="width: 100%">
                <tbody>
                <tr>
                    <td style="width:20%"></td>
                    <td class="p0ad doc-title text-center main-text" style="width: 60%;">
                        <?= $model->company->getTitle(true); ?><br>
                        <?= $author->getFio(); ?><br>
                        <?= $contacts ?>
                    </td>
                    <td class="p0ad text-right" style="vertical-align: top">
                        <?php if (is_file($path = $model->company->getImage('logoImage'))) : ?>
                            <?= ImageHelper::getThumb($path, [200, 54.4], [
                                'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                            ]); ?>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="document">
                <?php if ($model->is_deleted): ?>
                    <div class="text-center price-list_deleted">
                        Данный прайс-лист не действителен. <br>
                        Запросите новый.
                    </div>
                <?php else: ?>
                    <div class="main-text" style="margin-top: 30px;">
                        Данные на <?= date('d.m.Y', $model->updated_at) ?>
                    </div>
                    <table class="table-price-list" style="width: 100%;border: 2px solid #000000;margin-top:2px">
                        <thead>
                        <tr>
                            <th>№</th>
                            <?php if ($model->include_name_column): ?>
                                <th>Наимено&shy;вание</th>
                            <?php endif; ?>
                            <?php if ($isProduct && $model->include_article_column): ?>
                                <th>Артикул</th>
                            <?php endif; ?>
                            <?php if ($model->include_product_group_column): ?>
                                <th>Группа <?= $isProduct ? 'товаров' : 'услуг'; ?></th>
                            <?php endif; ?>
                            <?php if ($isProduct && $model->include_reminder_column): ?>
                                <th>Остаток</th>
                            <?php endif; ?>
                            <?php if ($model->include_product_unit_column): ?>
                                <th>Ед. измере&shy;ния</th>
                            <?php endif; ?>
                            <?php /* if ($isProduct && $model->include_description_column): ?>
                                <th>Описа&shy;ние</th>
                            <?php endif; */ ?>
                            <?php if ($model->include_price_column): ?>
                                <th>Цена</th>
                            <?php endif; ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($priceListOrders as $key => $priceListOrder): ?>
                            <tr>
                                <td style="text-align: center;">
                                    <?= ++$key; ?>
                                </td>
                                <?php if ($model->include_name_column): ?>
                                    <td>
                                        <?= $priceListOrder->name; ?>
                                    </td>
                                <?php endif; ?>
                                <?php if ($isProduct && $model->include_article_column): ?>
                                    <td>
                                        <?= $priceListOrder->article; ?>
                                    </td>
                                <?php endif; ?>
                                <?php if ($model->include_product_group_column): ?>
                                    <td>
                                        <?= $priceListOrder->productGroup ? $priceListOrder->productGroup->title : '---'; ?>
                                    </td>
                                <?php endif; ?>
                                <?php if ($isProduct && $model->include_reminder_column): ?>
                                    <td style="text-align: right;">
                                        <?php $isDecimal = ($priceListOrder->quantity != (int)$priceListOrder->quantity); ?>
                                        <?= TextHelper::numberFormat($priceListOrder->quantity, $isDecimal ? 3 : 0); ?>
                                    </td>
                                <?php endif; ?>
                                <?php if ($model->include_product_unit_column): ?>
                                    <td style="text-align: right;">
                                        <?= $priceListOrder->productUnit ? $priceListOrder->productUnit->name : '---'; ?>
                                    </td>
                                <?php endif; ?>
                                <?php /* if ($isProduct && $model->include_description_column): ?>
                                    <td>
                                        <?= $priceListOrder->description; ?>
                                    </td>
                                <?php endif; */ ?>
                                <?php if ($model->include_price_column): ?>
                                    <td style="text-align: right; white-space: nowrap">
                                        <?= TextHelper::invoiceMoneyFormat($priceListOrder->price_for_sell, 2); ?>
                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>

        </div>



    </div>
</div>