<?php

use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use common\widgets\Modal;
use frontend\models\Documents;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\product\PriceListNotification;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */

$changeStatusDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->created_at,
]);

if ($model->discount == round($model->discount))
    $model->discount = round($model->discount);
if ($model->markup == round($model->markup))
    $model->markup = round($model->markup);
?>

    <div class="control-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg darkblue"
                 style="padding-left:0px; padding-right:0px;text-align: center;"
                 title="Дата изменения статуса">

                <?= date(DateHelper::FORMAT_USER_DATE, $model->created_at) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-7 pad3">
                <div class="btn full_w marg darkblue"
                     title="Статус">

                    <?php
                    if ($model->is_archive)
                        echo 'В архиве';
                    else
                        echo $model->status->name
                    ?>
                </div>
            </div>
            <div class="col-xs-5 pad3">
                <div class="btn full_w marg darkblue btn-status"
                     style="text-align: center; "
                     title="Отправлено прайс-листов">

                    <?= (int)$model->send_count ?>
                </div>
            </div>
        </div>
    </div>

    <!-- _main_info -->
    <!--col-md-5 block-left-->
    <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
        <div class="portlet">
            <div class="customer-info bord-dark" style="margin:10px 0">

                <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <tbody>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Вид прайс-листа для клиента:</span><br/>
                                <?= Html::a($model->name,
                                    ['out-view', 'uid' => $model->uid],
                                    ['target' => '_blank']) ?>
                            </td>
                        </tr>
                        <?php if ($model->has_discount): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Скидка:</span>
                                    <?= $model->discount ?>%
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($model->has_markup): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Наценка:</span>
                                    <?= $model->markup ?>%
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($model->auto_update_prices): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Цены обновляются автоматически</span>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($model->auto_update_quantity): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Остатки обновляются автоматически</span>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <?php if ($model->use_notifications): ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Уведомления об открытии</span>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="">
                <div style="margin: 15px 0 0;">
                    <span style="font-weight: bold;">Комментарий</span>
                    <?= Html::tag('span', '', [
                        'id' => 'comment_internal_update',
                        'class' => 'glyphicon glyphicon-pencil',
                        'style' => 'cursor: pointer;',
                    ]); ?>
                </div>
                <div id="comment_internal_view" class="">
                    <?= Html::encode($model->comment_internal) ?>
                </div>
                <?php if ($canUpdate) : ?>
                    <?= Html::beginTag('div', [
                        'id' => 'comment_internal_form',
                        'class' => 'hidden',
                        'style' => 'position: relative;',
                        'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
                    ]) ?>
                    <?= Html::tag('i', '', [
                        'id' => 'comment_internal_save',
                        'class' => 'fa fa-floppy-o',
                        'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                    ]); ?>
                    <?= Html::textarea('comment_internal', $model->comment_internal, [
                        'id' => 'comment_internal_input',
                        'rows' => 3,
                        'maxlength' => true,
                        'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                    ]); ?>
                    <?= Html::endTag('div') ?>
                <?php endif ?>
            </div>

            <?php $viewedNotifications = PriceListNotification::find()->where(['price_list_id' => $model->id, 'is_viewed' => 1])->all() ?>
            <?php if ($viewedNotifications): ?>
            <div class="customer-info bord-dark" style="margin:10px 0">
                <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <tbody>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Прайс-лист был открыт:</span><br/>
                                <?php /** @var PriceListNotification $notification */ ?>
                                <?php foreach ($viewedNotifications as $notification): ?>
                                    <div><?= $notification->contractor ?
                                        Html::a($notification->contractor->getShortName(), ['/contractor/view', 'type' => 2, 'id' => $notification->contractor_id]) :
                                        $notification->contractor_email ?></div>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>


<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>