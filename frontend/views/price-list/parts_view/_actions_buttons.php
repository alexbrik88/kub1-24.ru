<?php

use common\models\AgreementTemplate;
use frontend\rbac\permissions;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Html;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var $model \common\models\product\PriceList;
 * @var $actualSubscription \common\models\service\Subscribe
 * @var $lastActiveSubscribe \common\models\service\Subscribe
 */

$canUpdate = Yii::$app->user->can(permissions\Product::CREATE);
$sendUrl = Url::to(['/price-list/send', 'id' => $model->id]);
?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate && !$model->is_deleted && !$model->is_archive): ?>
            <span>
                <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger" data-url="<?= $sendUrl ?>">
                    Отправить
                </button>
                <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить" data-url="<?= $sendUrl ?>">
                    <span class="ico-Send-smart-pls fs"></span>
                </button>
            </span>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && !$model->is_archive): ?>
            <?php $printUrl = ['print', 'id' => $model->id,
                'filename' => $model->name.'.html'];
            echo \yii\helpers\Html::a('Печать', $printUrl, [
                'target' => '_blank',
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ]);
            echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
                'target' => '_blank',
                'title' => 'Печать',
                'class' => 'btn darkblue widthe-100 hidden-lg',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && !$model->is_archive): ?>
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>

        <span class="dropup">
            <?= \yii\helpers\Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs',
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['download', 'type' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Excel</span> файл',
                        'encode' => false,
                        'url' => ['download', 'type' => 'xls', 'id' => $model->id, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => 'Отправить на мой e-mail',
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'send-message-panel-trigger send-to-me',
                            'data-url' => $sendUrl
                        ]
                    ],
                ],
            ]); ?>
        </span>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && !$model->is_archive): ?>
        <?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать этот прайс-лист?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать этот прайс-лист?',
            ]); ?>
        <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($actualSubscription): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => (!$model->is_archive) ?
                        'В архив' :
                        'Из архива',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to([
                        'update-is-archive',
                        'id' => $model->id,
                        'value' => (!$model->is_archive) ? 1 : 0
                ]),
                'message' => (!$model->is_archive) ?
                    'Вы уверены, что хотите перенести прайс-лист в архив?' :
                    'Вы уверены, что хотите извлечь прайс-лист из архива?'
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => (!$model->is_archive) ?
                        '<i class="fa fa-archive fa-2x"></i>' :
                        '<i class="fa fa-archive fa-2x"></i>',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => (!$model->is_archive) ? 'В архив' : 'Из архива',
                ],
                'confirmUrl' => Url::to([
                    'update-is-archive',
                    'id' => $model->id,
                    'value' => (!$model->is_archive) ? 1 : 0
                ]),
                'message' => (!$model->is_archive) ?
                    'Вы уверены, что хотите перенести прайс-лист в архив?' :
                    'Вы уверены, что хотите извлечь прайс-лист из архива?'
            ]); ?>
        <?php else: ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Из архива',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['/subscribe']),
                'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                    'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                    'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-archive fa-2x"></i>',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => (!$model->is_archive) ? 'В архив' : 'Из архива',
                ],
                'confirmUrl' => Url::to(['/subscribe']),
                'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                    'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                    'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\Product::CREATE)): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<?php if (Yii::$app->user->can(permissions\Product::CREATE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот прайс-лист?",
    ]);
}; ?>