<?php
use yii\web\View;
use common\components\TextHelper;
use frontend\modules\subscribe\forms\PaymentForm;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\web\JsExpression;
use common\models\Company;
use common\models\service\SubscribeTariffGroup;
use common\models\service\SubscribeTariff;
use common\models\product\PriceList;

/**
 * @var View  $this
 * @var Company $company
 * @var SubscribeTariffGroup $tariffGroup
 * @var SubscribeTariff[] $tariffList
 */

$tariffClass = [];
foreach ($tariffList as $id => $tariff) {
    $tariffClass[$id] = 'duration-month month-' . $tariff->duration_month . ' ';
    if (in_array($id, [43, 44, 45])) {
        $tariffClass[$id] .= 'active ';
    }
    if ($tariff->duration_month && $tariff->duration_month != 1) {
        $tariffClass[$id] .= 'hidden ';
    }
}

$tariffName = [];
foreach ($tariffList as $id => $tariff) {
    $tariffName[$id] = '';
    if (in_array($id, [39]))
        $tariffName[$id] .= 'Пробный';
    if (in_array($id, [40, 41, 42]))
        $tariffName[$id] .= 'Простой';
    if (in_array($id, [43, 44, 45]))
        $tariffName[$id] .= 'Онлайн';
    if (in_array($id, [46, 47, 48]))
        $tariffName[$id] .= 'Продающий';
}

$icoPlus = '<span class="big-ico plus">+</span>';
$icoMinus = '<span class="big-ico minus">—</span>';

$model = new PaymentForm($company);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-pay',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
        'interactive' => true,
        'functionBefore' => new JsExpression('function(instance, helper) {
            var content = $($(helper.origin).data("tooltip-content"));
            instance.content(content);
        }'),
    ],
]);

?>

<div class="tariff-months-block">
    <div class="tmb-item active" data-month="1">
        1 месяц
    </div>
    <div class="tmb-item" data-month="4">
        4 месяца
        <div class="discount">-10%</div>
    </div>
    <div class="tmb-item" data-month="12">
        12 месяцев
        <div class="discount">-20%</div>
    </div>
</div>

<table class="tariff-table">
    <thead>
    <tr>
        <th></th>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <th class="<?= $tariffClass[$id] ?? '' ?>" style="width: 20%">
                <div class="name"><?= $tariffName[$id] ?? '' ?></div>
            </th>
        <?php endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Количество прайс-листов</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>"><?= $tariff->tariff_limit ?: 'Безлимит' ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Количество товаров в прайс-листе</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $value = $tariff->getNamedParamValue('products_count', SubscribeTariffGroup::PRICE_LIST); ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>"><?= $value ? ('до ' . $value): $icoMinus ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Онлайн обновления цены и остатков</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $value = $tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST); ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>"><?= $value ? $icoPlus : $icoMinus ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Просмотр карточки товара</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $value = $tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST); ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>"><?= $value ? $icoPlus : $icoMinus ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Онлайн оформление заказов</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $value = $tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST); ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>">
                <?php if ($tariff->price > 0): ?>
                    <?= $value ? $icoPlus : $icoMinus ?>
                <?php else: ?>
                    <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> заказов
                <?php endif; ?>
            </td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Получение уведомлений</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $value = $tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST); ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>"><?= $value ? $icoPlus : $icoMinus ?></td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td>Тариф "Выставление счетов"</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <?php $tariffStandart = $tariff->containsStandartTariff; ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>">
                <?php if ($tariff->price > 0): ?>
                    <?= $tariffStandart ? (($tariffStandart->tariff_limit ? ($tariffStandart->tariff_limit . ' счетов') : 'Безлимит') . ' в месяц') : $icoMinus ?>
                <?php else: ?>
                    <?= PriceList::FREE_TARIFF_MAX_ORDERS_COUNT ?> счетов
                <?php endif; ?>
            </td>
        <?php endforeach; ?>
    </tr>
    <tr>
        <td class="tariff-price-td">Стоимость</td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <td class="tariff-price-td <?= $tariffClass[$id] ?? '' ?>">
                <div>
                    <span class="tariff-price"><?= TextHelper::numberFormat($tariff->price, 0) ?></span>
                    <span class="tariff-r">₽ / </span>
                    <span class="tariff-durability">
                        <span style="white-space: nowrap;">
                            <?= $tariff->duration_day ?
                            \php_rutils\RUtils::numeral()->getPlural($tariff->duration_day, [ 'день', 'дня', 'дней']) :
                            \php_rutils\RUtils::numeral()->getPlural($tariff->duration_month, [ 'месяц', 'месяца', 'месяцев']) ?>
                        </span>
                    </span>
                </div>
                <?php if ($tariff->duration_month > 0): ?>
                    <div class="tariff-average-price">
                        <?= TextHelper::numberFormat(round($tariff->price / $tariff->duration_month), 0) ?> ₽ / месяц
                    </div>
                <?php endif; ?>
            </td>
        <?php endforeach; ?>
    </tr>
    </tbody>
    <tfoot>
        <td></td>
        <?php foreach ($tariffList as $id => $tariff): ?>
            <td class="<?= $tariffClass[$id] ?? '' ?>">
                <?= $this->render('parts_tariff/_btn_buy', [
                    'model' => $model,
                    'tariff' => $tariff,
                    'tariffGroup' => $tariffGroup
                ]) ?>
            </td>
        <?php endforeach; ?>
    </tfoot>

</table>

<?= $this->render('parts_tariff/_modal', [
    'model' => $model,
    'tariff' => $tariff,
    'tariffGroup' => $tariffGroup
]) ?>

<?php $this->registerJs(<<<JS

    // change month
    $(document).on('click', '.tmb-item', function() {
        var table = $('.tariff-table');
        $('.tmb-item').removeClass('active');
        $(this).addClass('active');
        $(table).find('.duration-month').not('.month-0').addClass('hidden');
        $(table).find('.duration-month').filter('.month-' + $(this).data('month')).removeClass('hidden');
    });

    // submit
    $(document).on('submit', 'form.tariff-group-payment-form', function (e) {
        e.preventDefault();
        var form = this;
        $.post(form.action, $(form).serialize(), function(data) {
            console.log(data);
            Ladda.stopAll();
            if (data.content) {
                $('.submit-response-content', form).first().html(data.content);
            } else if (data.done) {
                if (data.email && (data.type == '2' || data.type == '3')) {
                    $("#send-to-email").html(data.email);
                    $("#info-after-pay").modal("show");
                }
                if (data.link) {
                    var tab = window.open(data.link, '_blank');
                    if (tab) {
                        tab.focus();
                    } else {
                        alert('Please allow popups for this website');
                    }
                }
            }
            $('.tooltipstered').tooltipster('close');
        });
    });
    
JS
) ?>

<style>
    table.tariff-table {
        border-collapse: separate;
        border-spacing: 0;
        width: 100%;
    }
    table.tariff-table th,
    table.tariff-table td:not(:first-child) {
        text-align: center;
        font-size: 16px;
        padding: 0 10px;
    }
    table.tariff-table th .name {
        background-color: #f6f7f8;
        color: #9198a0;
        font-size: 19px;
        padding: 12px;
        text-align: center;
        border-radius: 25px!important;
        display: inline-block;
        height: 50px;
        width: 90%;
        margin: 15px 0;
        max-width: 200px;
        min-width: 120px;
    }
    table.tariff-table td {
        vertical-align: middle;
        border-bottom: 1px solid #eeeff0;
        border-left: 1px solid #eeeff0;
        padding: 15px 0;
        text-align: center;
    }
    table.tariff-table td:first-child {
        border-left: none;
        text-align: left;
        font-size: 14px;
        color: #9198a0;
        padding-right: 10px;
    }
    table.tariff-table tr:last-child td {
        border-bottom: none;
    }
    table.tariff-table .tariff {
        width: 90%;
        max-width: 200px;
        margin-bottom: 15px;
    }
    table.tariff-table .big-ico.plus {
        font-size: 32px;
        color: #4679ae;
    }
    table.tariff-table .big-ico.minus {
        font-size: 16px;
    }
    table.tariff-table th.active,
    table.tariff-table td.active {
        border-left: 1px solid #488fd9;
        border-right: 1px solid #488fd9;
    }
    table.tariff-table th.active {
        border-top: 1px solid #488fd9;
        border-top-left-radius: 15px!important;
        border-top-right-radius: 15px!important;
    }
    table.tariff-table tfoot td.active {
        border-bottom: 1px solid #488fd9;
        border-bottom-left-radius: 15px!important;
        border-bottom-right-radius: 15px!important;
    }
    table.tariff-table th.active .name {
        background-color: #488fd9;
        color: #fff;
    }
    table.tariff-table td.tariff-price-td {
        vertical-align: top;
        padding: 20px 10px;
    }
    table.tariff-table td .tariff-price {
        font-size: 26px;
        font-weight: bold;
    }
    table.tariff-table td .tariff-r {
        font-size: 16px;
    }
    table.tariff-table td .tariff-average-price {
        text-align: center;
        color: #9198a0;
        padding-top: 15px;
    }
    table.tariff-table td .tariff-durability {
        color: #488fd9;
    }
    div.tariff-months-block {
        margin-left: auto;
        margin-right: auto;
        width: 345px;
        height: 40px;
        border: 1px solid #eeeff0;
        border-radius: 20px!important;
        margin-bottom: 15px;
    }
    div.tariff-months-block .tmb-item {
        height: 34px;
        border-radius: 17px!important;
        background-color: #fff;
        color: #0097fd;
        display: inline-block;
        width:110px;
        padding: 6px 0 0 0;
        font-size: 16px;
        text-align: center;
        position: relative;
        margin:2px 0;
        cursor: pointer;
    }
    div.tariff-months-block .tmb-item.active {
        background-color: #4679ae;
        color: #fff;
    }
    div.tariff-months-block .tmb-item .discount {
        position: absolute;
        top: -10px;
        right: -10px;
        font-size: 11px;
        background-color: #488fd9;
        color: #fff;
        height: 18px;
        border-radius: 9px!important;
        padding: 0 5px;
    }
</style>