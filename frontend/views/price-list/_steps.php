<?php

use yii\helpers\Url;
use common\models\company\CompanyType;
use common\models\Company;

function getStepClass($itemStep, $step) {
    return $itemStep < $step ? 'finish' : ($itemStep == $step ? 'active' : '');
}
?>

<div class="sbs-menu">
    <!-- Step 1 -->
    <a class="sbs-el sbs-step-1 <?= getStepClass(1, $step) ?>" href="<?= Url::to(['step-instruction']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i><img class="m-r-sm" src="/img/icons/instructions<?= $step > 1 ? '-black':'' ?>.png?" style="width: 30px;"></i>
                </td>
                <td class="text-block">
                    Инструкция
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 2 -->
    <a class="sbs-el sbs-step-2 <?= getStepClass(2, $step) ?>" href="<?= Url::to(['step-company']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-gear">
                </td>
                <td class="text-block">
                    Реквизиты вашего <?= $company->companyType->name_short ?>
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 3 -->
    <a class="sbs-el sbs-step-3 <?= getStepClass(3, $step) ?>" href="<?= Url::to(['step-products']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-cubes">
                </td>
                <td class="text-block">
                    Товары
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 4 -->
    <a class="sbs-el sbs-step-4 <?= getStepClass(4, $step) ?>" href="<?= Url::to(['step-services']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i class="fa fa-pencil"></i>
                    <i class="fa fa-wrench fa-pencil-wrench"></i>
                </td>
                <td class="text-block">
                    Услуги
                </td>
            </tr>
        </table>
    </a>
    <!-- Step 5 -->
    <a class="sbs-el sbs-step-5 <?= getStepClass(5, $step) ?>" href="<?= Url::to(['step-price-lists']) ?>" data-pjax="0">
        <table>
            <tr>
                <td class="image-block">
                    <i><img class="m-r-sm" src="/img/icons/price-list<?= $step < 5 ? '-black':'' ?>.png" style="width: 25px;"></i>
                </td>
                <td class="text-block">
                    Прайс-листы
                </td>
            </tr>
        </table>
    </a>
</div>