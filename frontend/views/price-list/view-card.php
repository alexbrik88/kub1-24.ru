<?php

use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;
use common\widgets\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */
/* @var $actualSubscription \common\models\service\Subscribe */
/* @var $lastActiveSubscribe \common\models\service\Subscribe */

$this->title = $model->name;

$canUpdate = Yii::$app->user->can(permissions\Product::UPDATE, [
    'model' => $model,
]);

$backUrl = Yii::$app->request->referrer ?: 'index';
?>

<div class="page-content-in" style="padding-bottom: 30px;">

    <div>
        <?= \yii\helpers\Html::a('Назад к списку', ['index'], [
                'class' => 'back-to-customers',
            ]) ?>
    </div>

    <div class="col-xs-12 pad0">

        <div class="col-xs-12 col-lg-7 pad0">
            <?= $this->render('parts_view/_template', [
                'model' => $model,
                'canUpdate' => $canUpdate
            ]); ?>
        </div>

        <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">

            <div class="col-xs-12" style="padding-right:0px !important;">

                <?= $this->render('parts_view/_status', [
                    'model' => $model,
                    'canUpdate' => $canUpdate
                ]); ?>

            </div>

        </div>

    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('parts_view/_actions_buttons', [
            'model' => $model,
            'canUpdate' => $canUpdate,
            'actualSubscription' => $actualSubscription,
            'lastActiveSubscribe' => $lastActiveSubscribe
        ]); ?>
    </div>

</div>

<?= $this->render('@frontend/modules/documents/views/invoice/view/_send_message', [
    'model' => $model,
    'useContractor' => null,
]); ?>