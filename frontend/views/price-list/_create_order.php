<?php
$isOwnerLook = !Yii::$app->user->isGuest && Yii::$app->user->identity->company && Yii::$app->user->identity->company->id == $model->company->id;
$downloadPdfUrl = \yii\helpers\Url::to(($isOwnerLook) ? ['download', 'type' =>'pdf', 'id' => $model->id] : ['download', 'type' =>'pdf', 'uid' => $model->uid]);
$downloadXlsUrl = \yii\helpers\Url::to(($isOwnerLook) ? ['download', 'type' =>'xls', 'id' => $model->id] : ['download', 'type' =>'xls', 'uid' => $model->uid]);
?>
<style>
    table { border-collapse: collapse; }
    .price-list-checkout:hover .cls-1 { fill: #000; }
    .svg-class { width: auto; height: 28px; margin: 11px; }
    .img-class { width:42px;  height: 42px; margin: 11px; }
</style>
<div id="buttons-bar-fixed-top-right">
    <div class="row">
        <div class="col-xs-12">
            <a class="price-list-checkout" href="<?= \yii\helpers\Url::to(["/out/order-document/{$model->uid}"])?>" title="Оформить заказ">
                <table>
                    <tr>
                        <td class="image-block">
                            <div class="img-class" style=""></div>
                        </td>
                        <td class="text-block">
                            Оформить заказ
                        </td>
                    </tr>
                </table>
            </a>
            <a class="price-list-checkout" href="<?= $downloadPdfUrl ?>" title="Скачать в PDF">
                <table>
                    <tr>
                        <td class="image-block" style="text-align: center;">
                            <svg class="svg-class" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 550.801 550.801" style="enable-background:new 0 0 550.801 550.801;" xml:space="preserve" stroke-width="0">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                            stroke: #fff;
                                            fill-rule: evenodd;
                                        }
                                    </style>
                                </defs>
                                <path class="cls-1" d="M160.381,282.225c0-14.832-10.299-23.684-28.474-23.684c-7.414,0-12.437,0.715-15.071,1.432V307.6 c3.114,0.707,6.942,0.949,12.192,0.949C148.419,308.549,160.381,298.74,160.381,282.225z"></path> <path class="cls-1" d="M272.875,259.019c-8.145,0-13.397,0.717-16.519,1.435v105.523c3.116,0.729,8.142,0.729,12.69,0.729 c33.017,0.231,54.554-17.946,54.554-56.474C323.842,276.719,304.215,259.019,272.875,259.019z"></path> <path class="cls-1" d="M488.426,197.019H475.2v-63.816c0-0.398-0.063-0.799-0.116-1.202c-0.021-2.534-0.827-5.023-2.562-6.995L366.325,3.694 c-0.032-0.031-0.063-0.042-0.085-0.076c-0.633-0.707-1.371-1.295-2.151-1.804c-0.231-0.155-0.464-0.285-0.706-0.419 c-0.676-0.369-1.393-0.675-2.131-0.896c-0.2-0.056-0.38-0.138-0.58-0.19C359.87,0.119,359.037,0,358.193,0H97.2 c-11.918,0-21.6,9.693-21.6,21.601v175.413H62.377c-17.049,0-30.873,13.818-30.873,30.873v160.545 c0,17.043,13.824,30.87,30.873,30.87h13.224V529.2c0,11.907,9.682,21.601,21.6,21.601h356.4c11.907,0,21.6-9.693,21.6-21.601 V419.302h13.226c17.044,0,30.871-13.827,30.871-30.87v-160.54C519.297,210.838,505.47,197.019,488.426,197.019z M97.2,21.605 h250.193v110.513c0,5.967,4.841,10.8,10.8,10.8h95.407v54.108H97.2V21.605z M362.359,309.023c0,30.876-11.243,52.165-26.82,65.333 c-16.971,14.117-42.82,20.814-74.396,20.814c-18.9,0-32.297-1.197-41.401-2.389V234.365c13.399-2.149,30.878-3.346,49.304-3.346 c30.612,0,50.478,5.508,66.039,17.226C351.828,260.69,362.359,280.547,362.359,309.023z M80.7,393.499V234.365 c11.241-1.904,27.042-3.346,49.296-3.346c22.491,0,38.527,4.308,49.291,12.928c10.292,8.131,17.215,21.534,17.215,37.328 c0,15.799-5.25,29.198-14.829,38.285c-12.442,11.728-30.865,16.996-52.407,16.996c-4.778,0-9.1-0.243-12.435-0.723v57.67H80.7 V393.499z M453.601,523.353H97.2V419.302h356.4V523.353z M484.898,262.127h-61.989v36.851h57.913v29.674h-57.913v64.848h-36.593 V232.216h98.582V262.127z"></path>
                            </svg>
                        </td>
                        <td class="text-block">
                            Скачать в PDF
                        </td>
                    </tr>
                </table>
            </a>
            <a class="price-list-checkout" href="<?= $downloadXlsUrl ?>" title="Скачать в Excel">
                <table>
                    <tr>
                        <td class="image-block" style="text-align: center;">
                            <svg class="svg-class" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 550.801 550.801" style="enable-background:new 0 0 550.801 550.801;" xml:space="preserve" stroke-width="0">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill: #fff;
                                            stroke: #fff;
                                            fill-rule: evenodd;
                                        }
                                    </style>
                                </defs>
                                <path class="cls-1" d="M488.432,197.019h-13.226v-63.816c0-0.398-0.063-0.799-0.111-1.205c-0.021-2.531-0.833-5.021-2.568-6.992L366.325,3.694 c-0.032-0.031-0.063-0.042-0.085-0.076c-0.633-0.707-1.371-1.295-2.151-1.804c-0.231-0.155-0.464-0.285-0.706-0.422 c-0.676-0.366-1.393-0.675-2.131-0.896c-0.2-0.053-0.38-0.135-0.58-0.188C359.87,0.119,359.037,0,358.193,0H97.2 c-11.918,0-21.6,9.693-21.6,21.601v175.413H62.377c-17.049,0-30.873,13.818-30.873,30.873v160.545 c0,17.038,13.824,30.87,30.873,30.87h13.224V529.2c0,11.907,9.682,21.601,21.6,21.601h356.4c11.907,0,21.6-9.693,21.6-21.601 V419.302h13.226c17.044,0,30.871-13.827,30.871-30.87v-160.54C519.297,210.832,505.48,197.019,488.432,197.019z M97.2,21.601 h250.193v110.51c0,5.967,4.841,10.8,10.8,10.8h95.407v54.108H97.2V21.601z M339.562,354.344v31.324H236.509V220.704h37.46v133.64 H339.562z M74.25,385.663l47.73-83.458l-46.019-81.501h42.833l14.439,30.099c4.899,10.03,8.572,18.116,12.49,27.414h0.483 c3.926-10.529,7.101-17.872,11.259-27.414l13.954-30.099h42.588l-46.509,80.525l48.961,84.438h-43.081l-14.929-29.858 c-6.115-11.507-10.036-20.07-14.684-29.615h-0.49c-3.431,9.55-7.583,18.119-12.722,29.615l-13.711,29.858H74.25V385.663z M453.601,523.353H97.2V419.302h356.4V523.353z M401.963,388.125c-18.837,0-37.446-4.904-46.738-10.04l7.578-30.839 c10.04,5.136,25.46,10.283,41.375,10.283c17.139,0,26.188-7.099,26.188-17.867c0-10.283-7.831-16.157-27.659-23.256 c-27.411-9.55-45.282-24.722-45.282-48.718c0-28.15,23.498-49.684,62.427-49.684c18.594,0,32.305,3.927,42.093,8.322l-8.322,30.109 c-6.607-3.186-18.361-7.834-34.509-7.834c-16.152,0-23.983,7.338-23.983,15.913c0,10.525,9.291,15.18,30.591,23.258 c29.125,10.769,42.836,25.936,42.836,49.191C468.545,364.627,447.257,388.125,401.963,388.125z"></path>
                            </svg>
                        </td>
                        <td class="text-block">
                            Скачать в Excel
                        </td>
                    </tr>
                </table>
            </a>
        </div>
    </div>
</div>

<script>
    // Preload icons
    var _img = new Image();
    _img.src = '/img/icons/shopping-cart-b.png';
</script>