<?php

use common\models\product\Product;
use frontend\models\Documents;
use frontend\rbac\permissions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

$company = Yii::$app->user->identity->company;
$serviceCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale(false)->count();
$goodsCount = $company->getProducts()->byDeleted()->byUser()->byStatus(Product::ACTIVE)->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale(false)->count();
$productCount = $serviceCount + $goodsCount;
$firstCreate = Yii::$app->controller->action->id == 'first-create';
$canAdd = Yii::$app->user->can(permissions\Product::CREATE);
?>

<tr id="from-new-add-row" class="from-new-add disabled-row" role="row" <?= $hasOrders ? 'style="display: none;"': ''; ?> >
    <td class="delete-column-left">
        <span class="icon-close remove-product-from-invoice from-new"></span>
    </td>
    <td class="">
        <div class="add-exists-product<?= $productCount == 0 ? ' hidden' : '' ?>">
            <?php echo Select2::widget([
                'id' => 'order-add-select',
                'name' => 'addOrder',
                'initValueText' => '',
                'options' => [
                    'placeholder' => 'Выберите товар/услуги из списка',
                    'class' => 'form-control ',
                ],
                'pluginOptions' => [
                    'allowClear' => false,
                    'minimumInputLength' => 1,
                    'dropdownCssClass' => 'product-search-dropdown' . ($firstCreate ? ' no-search' : ''),
                    'ajax' => [
                        'url' => "/product/search",
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(product) { return product.text; }'),
                    'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                ],
            ]); ?>
            <div id="order-add-static-items" style="display: none;">
                <ul class="order-add-static-items select2-results__options">
                    <?php if ($canAdd) : ?>
                        <li class="select2-results__option add-modal-produc-new yellow" aria-selected="false">
                            [ + Добавить новый товар/услугу ]
                        </li>
                    <?php endif ?>
                    <li class="select2-results__option add-modal-services<?= $serviceCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                        Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                    </li>
                    <li class="select2-results__option add-modal-products<?= $goodsCount == 0 ? ' hidden' : '' ?>" aria-selected="false">
                        Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                    </li>
                </ul>
            </div>
            <script type="text/javascript">
                $('#order-add-select').on('select2:open', function (evt) {
                    var prodItems = $('#select2-order-add-select-results').parent().children('.order-add-static-items');
                    if (prodItems.length) {
                        prodItems.remove();
                    }
                    $('.order-add-static-items').clone().insertAfter('#select2-order-add-select-results');
                });
                $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function() {
                    $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                    $(this).addClass('select2-results__option--highlighted');
                });
            </script>
        </div>
    </td>
    <?php /*
    <td class="col_article">
        <div class="order-param-value">&nbsp;</div>
    </td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>
    <td class=""><div class="order-param-value">&nbsp;</div></td>*/ ?>
</tr>