<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\models\document\Invoice;
use common\models\document\NdsViewType;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\ImageHelper;
use common\components\TextHelper;
use frontend\modules\documents\components\InvoiceHelper;
use common\models\product\PriceList;

/* @var $this yii\web\View */
/* @var $model \common\models\product\PriceList */
/* @var $company common\models\Company */
/* @var $document string */

SortableAsset::register($this);

$priceOneCss = ($model->has_discount ? ' has_discount' : '') . ($model->has_markup ? ' has_markup' : '');

$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}

$js = <<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: "img.sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS;
$this->registerJs($js);
?>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
        'style' => 'margin-bottom: 0;',
    ],
]); ?>

<table id="table-for-invoice" class="table table-striped table-bordered account_table last-line-table sort_after_adding" style="margin-bottom: 0;">
    <thead>
    <tr class="heading" role="row">
        <th class="delete-column-left" width="5%" tabindex="0" rowspan="1" colspan="1">
        </th>
        <th class="col_name" width="30%" tabindex="0" rowspan="1" colspan="1">
            Наименование
        </th>
        <th class="col_article <?= $model->include_article_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Артикул
        </th>
        <th class="col_group <?= $model->include_product_group_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Группа
        </th>
        <th class="col_quantity <?= $model->include_reminder_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Остаток
        </th>
        <?php /*
        <th class="col_comment <?= $model->include_description_column ? '' : 'hidden' ?>" width="10%" tabindex="0" rowspan="1" colspan="1">
            Описание
        </th>*/ ?>
        <th width="8%"  tabindex="0" rowspan="1" colspan="1" style="min-width: 70px">
            Ед.измерения
        </th>
        <th class="col_price <?= $model->include_price_column ? '' : 'hidden' ?>" width="15%" tabindex="0" rowspan="1" colspan="1" style="position: relative;">
            Цена
        </th>
        <th class="col_discount <?= $model->has_discount ? '' : 'hidden'; ?>" width="5%" tabindex="0" rowspan="1" colspan="1">
            Цена со скидкой
        </th>
        <th class="col_markup <?= $model->has_markup ? '' : 'hidden'; ?>" width="5%" tabindex="0" rowspan="1" colspan="1">
            Цена с наценкой
        </th>
    </tr>
    </thead>

    <tbody id="table-product-list-body" data-number="<?= count($model->priceListOrders) ?>">
    <?php if (count($model->priceListOrders) > 0 && $model->priceListType == PriceList::FOR_SELECTIVELY) : ?>
        <?php
        $orders = $model->getPriceListOrders()->orderBy(['production_type' => SORT_DESC, 'name' => SORT_ASC])->all();
        foreach ($orders as $key => $order) {
            echo $this->render('_form_order_row', [
                'price' => $model,
                'order' => $order,
                'number' => $key,
                'model' => $model,
                'precision' => 2,
            ]);
        }
        ?>
    <?php endif; ?>
    <?= $this->render('_form_add_order_row', [
        'hasOrders' => count($model->priceListOrders) > 0,
    ]) ?>
    </tbody>

    <tfoot>
    <tr class="template disabled-row" role="row">
        <td class="product-delete delete-column-left" style="white-space: nowrap;">

            <input disabled="disabled" type="hidden" class="product-count" name="orderArray[][count]" value="1"/>
            <input disabled="disabled" type="hidden" class="order-id" name="orderArray[][id]" value=""/>
            <input disabled="disabled" type="hidden" class="product-id selected-product-id" name="orderArray[][product_id]" value=""/>
            <input disabled="disabled" type="hidden" class="product-production-type" name="orderArray[][production_type]" value=""/>

            <span class="icon-close remove-product-from-invoice"></span>
            <!-- <span class="glyphicon glyphicon-menu-hamburger sortable-row-icon"></span> -->
            <?= ImageHelper::getThumb('img/menu-humburger.png', [20, 14], [
                'class' => 'sortable-row-icon',
                'style' => 'padding-bottom: 9px;',
            ]); ?>
        </td>
        <td class="col_name">
            <input disabled="disabled" type="text" class="product-title form-control tooltip-product"
                   name="orderArray[][title]"
                   style="width: 100%;">
        </td>
        <td class="col_article <?= $model->include_article_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-article">&nbsp;</div>
        </td>
        <td class="col_group <?= $model->include_product_group_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-group-name">&nbsp;</div>
        </td>
        <td class="col_quantity <?= $model->include_reminder_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-quantity" data-store-all="0">&nbsp;</div>
        </td>
        <?php /*
        <td class="col_comment <?= $model->include_description_column ? '' : 'hidden' ?>">
            <div class="order-param-value product-comment">&nbsp;</div>
        </td>*/ ?>
        <td class="col_unit_name">
            <div class="order-param-value product-unit-name">&nbsp;</div>
        </td>
        <td class="col_price <?= $model->include_price_column ? '' : 'hidden' ?> price-one">
            <?= Html::input('number', 'orderArray[][price]', 0, [
                'class' => 'form-control price-input',
                'disabled' => 'disabled',
                'min' => 0,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="discount col_discount <?= $model->has_discount ? '' : ' hidden'; ?>">
            <div class="order-param-value product-discount">
                <?= Html::input('hidden', 'orderArray[][discount]', 0, [
                    'class' => 'discount-input'
                ]); ?>
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
        <td class="markup col_markup <?= $model->has_markup ? '' : ' hidden'; ?>">
            <div class="order-param-value product-markup">
                <?= Html::input('hidden', 'orderArray[][markup]', 0, [
                    'class' => 'markup-input'
                ]); ?>
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
    </tr>
    <tr class="button-add-line" role="row">
        <td class="delete-column-left" colspan="2" style="border-bottom: none; border-left: none; border-right: none; position: relative;display: table-cell;">
            <div class="portlet pull-left control-panel button-width-table" style="text-align:left">
                <div class="btn-group pull-right" style="display: inline-block; padding-top: 7px">
                    <span class="btn yellow btn-add-line-table">
                        <i class="pull-left fa icon fa-plus-circle" style="font-size: 23px"></i>
                    </span>
                </div>
            </div>
        </td>
    </tr>
    </tfoot>
</table>

<?php Pjax::end(); ?>