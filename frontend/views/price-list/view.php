<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.10.2018
 * Time: 13:08
 */

use common\models\product\PriceList;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\product\Product;
use common\components\helpers\Html;
use common\components\image\EasyThumbnailImage;
use yii\bootstrap\Nav;
use yii\bootstrap\Modal;

/* @var $this yii\web\View
 * @var $model PriceList
 * @var $priceListOrders \common\models\product\PriceListOrder[]
 */

$this->title = 'Прайс-лист "' . $model->name . '"';
$isProduct = $model->production_type == Product::PRODUCTION_TYPE_GOODS || $model->production_type == Product::PRODUCTION_TYPE_ALL;
$sortAttr = $model->getSortAttributeName();
$priceListOrders = $model->getPriceListOrders()
    ->joinWith('productGroup')
    ->orderBy(['production_type' => SORT_DESC, $sortAttr => SORT_ASC])
    ->all();
$homeUrl = Yii::$app->user->isGuest && YII_ENV_PROD ? Yii::$app->params['serviceSite'] : Yii::$app->homeUrl;

$author = $model->priceListContact ?: $model->author;
$contacts = [];
if (!empty($author->phone)) $contacts[] = 'Тел: ' . $author->phone;
if (!empty($author->email)) $contacts[] = 'E-mail: ' . $author->email;
if (!empty($author->site))  $contacts[] = 'Сайт: ' . $author->site;
$contacts = implode(', ', $contacts);

// changing columns
$isName = $model->include_name_column;
$isArticle = $model->include_article_column;
$isGroup = $model->include_product_group_column;
$isWeight = $model->include_weight_column;
$isNetto = $model->include_netto_column;
$isBrutto = $model->include_brutto_column;
$isCustomField = $model->include_custom_field_column;
$isReminder = $model->include_reminder_column;
$isCountInPackage = $model->include_count_in_package_column;
$isBoxType = $model->include_box_type_column;
$isUnit = $model->include_product_unit_column;
$isPrice = $model->include_price_column;

// for modal
$isDescription = $model->include_description_column;
$isImage = $model->include_image_column;

$jsColumns = [
    'name' => $isName ? '{"orderable": true, "searchable": true},' : '',
    'article' => $isArticle ? 'null,' : '',
    'group' => $isGroup ? 'null,' : '',
    'weight' => $isWeight ? 'null,' : '',
    'netto' => $isNetto ? 'null,' : '',
    'brutto' => $isBrutto ? 'null,' : '',
    'custom_field' => $isCustomField ? 'null,' : '',
    'reminder' => $isReminder ? 'null,' : '',
    'count_in_package' => $isCountInPackage ? 'null,' : '',
    'box_type' => $isBoxType ? 'null,' : '',
    'unit' => $isUnit ? '{"orderable": false, "searchable": false},' : '',
    //'description' => $isDescription ? '{"orderable": false, "searchable": false},' : '',
    'price' => $isPrice ? '{"type": "formatted-num", "searchable": false},' : ''
];

$paging = count($priceListOrders) > 10 ? 'true' : 'false';

$js = <<<JS

// per page
window.productsPerPage = localStorage.getItem("productsPerPage") || 10;
$(document).on('change', '#produc-table_length select', function() {
    localStorage.setItem("productsPerPage", $(this).val());
});

var priceValueFormat = function (value) {
    return new Intl.NumberFormat('ru-RU', {minimumFractionDigits: 2, maximumFractionDigits: 2}).format(value);
}

$.extend( $.fn.dataTableExt.oSort, {
    "formatted-num-pre": function ( a ) {
        a = (a === "-" || a === "") ? 0 : a.replace( /[^\d,]/g, "" );
        return parseFloat( a );
    },
    "formatted-num-asc": function ( a, b ) {
        return a - b;
    },
    "formatted-num-desc": function ( a, b ) {
        return b - a;
    }
} );

var prodTable = $("#produc-table").DataTable({
    "paging": $paging,
    "info": false,
    "dom": "t<'row'<'col-sm-12'pl>>",
    "order": [[ 1, "asc" ]],
    "columns": [
        { "orderable": false, "searchable": false },
        {$jsColumns['name']}
        {$jsColumns['article']}
        {$jsColumns['group']}
        {$jsColumns['weight']}
        {$jsColumns['netto']}
        {$jsColumns['brutto']}
        {$jsColumns['custom_field']}
        {$jsColumns['reminder']}
        {$jsColumns['count_in_package']}        
        {$jsColumns['box_type']}
        {$jsColumns['unit']}
        //{jsColumns['description']}
        {$jsColumns['price']}
    ],
    "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Все"]],
    "language": {
        "lengthMenu": "Выводить по _MENU_",
        "decimal": ",",
        "thousands": "&nbsp;",
    },
    "pageLength": window.productsPerPage
});
prodTable.on('order.dt', function () {
    prodTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        cell.innerHTML = i+1;
    } );
}).draw();
$(document).on('keyup paste', '#product-title-search', function() {
    prodTable.search(this.value).draw();
})
JS;

$this->registerJs($js);

$totalWidth = 675;
$width = [
    'num' => 40,
    'name' => 300,
    'article' => 85,
    'group' => 85,
    'weight' => 85,
    'netto' => 85,
    'brutto' => 85,
    'custom_field' => 85,
    'reminder' => 85,
    'count_in_package' => 85,
    'box_type' => 85,
    'units' => 85,
    //'description' => 100,
    'price' => 100
];
foreach ($width as $k=>$v) {
    $width[$k] = round($v * 100 / $totalWidth) . '%';
}

?>
<div class="header">
    <div class="container-fluid">
        <div class="company-logo">
            <?php if (is_file($path = $model->company->getImage('logoImage'))) : ?>
                <?= ImageHelper::getThumb($path, [200, 54.4], [
                    'cutType' => EasyThumbnailImage::THUMBNAIL_INSET
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="main-text">
            <?= $model->company->getTitle(true); ?><br>
            <?= $author->getFio(); ?><br>
            <?= $contacts ?>
        </div>
        <div style="width:24%; display: inline-block">
        </div>
    </div>
</div>
<div class="document">
    <?php if ($model->is_deleted || $model->is_archive): ?>
        <div class="text-center price-list_deleted">
            Данный прайс-лист не действителен. <br>
            Запросите новый.
        </div>
    <?php else: ?>
        <div class="main-text" style="margin-top: 60px;">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div style="line-height:1; padding-top:10px;padding-bottom:5px;">Данные на <?= date('d.m.Y', $model->updated_at) ?></div>
                </div>
                <div class="col-sm-12 col-md-6">
                <?php if (count($priceListOrders) > 10) : ?>
                    <?= \yii\helpers\Html::input('search', 'title', '', [
                        'placeholder' => 'Поиск по названию' . ($isArticle ? ' и артикулу' : ''),
                        'class' => 'form-control',
                        'id' => 'product-title-search',
                        'style' => 'width: 100%'
                    ]) ?>
                <?php endif; ?>
                </div>
            </div>
        </div>

        <table id="produc-table" style="width: 99.75%;border: 2px solid #000000;margin-top:2px; border-collapse: collapse;">
            <col width="<?= $width['num'] ?>">
            <?php if ($isName): ?><col width="<?= $width['name'] ?>"><?php endif; ?>
            <?php if ($isArticle): ?><col width="<?= $width['article'] ?>"><?php endif; ?>
            <?php if ($isGroup): ?><col width="<?= $width['group'] ?>"><?php endif; ?>
            <?php if ($isWeight): ?><col width="<?= $width['weight'] ?>"><?php endif; ?>
            <?php if ($isNetto): ?><col width="<?= $width['netto'] ?>"><?php endif; ?>
            <?php if ($isBrutto): ?><col width="<?= $width['brutto'] ?>"><?php endif; ?>
            <?php if ($isCustomField): ?><col width="<?= $width['custom_field'] ?>"><?php endif; ?>
            <?php if ($isReminder): ?><col width="<?= $width['reminder'] ?>"><?php endif; ?>
            <?php if ($isCountInPackage): ?><col width="<?= $width['count_in_package'] ?>"><?php endif; ?>
            <?php if ($isBoxType): ?><col width="<?= $width['box_type'] ?>"><?php endif; ?>
            <?php if ($isUnit): ?><col width="<?= $width['units'] ?>"><?php endif; ?>
            <?php /* if ($isDescription): ?><col width="<?= $width['description'] ?>"><?php endif; */ ?>
            <?php if ($isPrice): ?><col width="<?= $width['price'] ?>"><?php endif; ?>
            <thead>
            <tr>
                <th>№</th>
                <?php if ($model->include_name_column): ?>
                    <th>Наимено&shy;вание</th>
                <?php endif; ?>
                <?php if ($isProduct && $model->include_article_column): ?>
                    <th>Артикул</th>
                <?php endif; ?>
                <?php if ($model->include_product_group_column): ?>
                    <th>Группа <?= $isProduct ? 'товаров' : 'услуг'; ?></th>
                <?php endif; ?>
                <?php if ($model->include_weight_column): ?>
                    <th>Вес (кг)</th>
                <?php endif; ?>
                <?php if ($model->include_netto_column): ?>
                    <th>Масса нетто</th>
                <?php endif; ?>
                <?php if ($model->include_brutto_column): ?>
                    <th>Масса брутто</th>
                <?php endif; ?>
                <?php if ($model->include_custom_field_column): ?>
                    <th><?= $productField->title ?? 'Ваше название' ?></th>
                <?php endif; ?>
                <?php if ($isProduct && $model->include_reminder_column): ?>
                    <th>Остаток</th>
                <?php endif; ?>
                <?php if ($model->include_box_type_column): ?>
                    <th>Вид упаков&shy;ки</th>
                <?php endif; ?>
                <?php if ($model->include_count_in_package_column): ?>
                    <th>Коли&shy;чество в упаков&shy;ке</th>
                <?php endif; ?>
                <?php if ($model->include_product_unit_column): ?>
                    <th>Ед. измер-я</th>
                <?php endif; ?>
                <?php /* if ($isProduct && $model->include_description_column): ?>
                    <th>Описание</th>
                <?php endif; */ ?>
                <?php if ($model->include_price_column): ?>
                    <th>Цена</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($priceListOrders as $key => $priceListOrder): ?>
                <tr class="<?= ($isDescription || $isImage) ? "show-pricelist-modal" : '' ?>" data-key="<?= $key ?>">
                    <td style="text-align: center;">
                        <?= 1+$key; ?>
                    </td>
                    <?php if ($isName): ?>
                        <td>
                            <?= $priceListOrder->name ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isProduct && $isArticle): ?>
                        <td>
                            <?= $priceListOrder->article; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isGroup): ?>
                        <td>
                            <?= $priceListOrder->productGroup ? $priceListOrder->productGroup->title : '---'; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isWeight): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->weight ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isNetto): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->mass_net ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isBrutto): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->mass_gross ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isCustomField): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->custom_field_value ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isProduct && $isReminder): ?>
                        <td style="text-align: right;">
                            <?= ($priceListOrder->quantity > 0) ? $priceListOrder->quantity : 0; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isBoxType): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->box_type ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isCountInPackage): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->product->count_in_package ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($isUnit): ?>
                        <td style="text-align: right;">
                            <?= $priceListOrder->productUnit ? $priceListOrder->productUnit->name : '---'; ?>
                        </td>
                    <?php endif; ?>
                    <?php /* if ($isDescription): ?>
                        <td>
                            <?= $priceListOrder->description; ?>
                        </td>
                    <?php endif; */ ?>
                    <?php if ($isPrice): ?>
                        <td style="text-align: right;">
                            <?= TextHelper::invoiceMoneyFormat($priceListOrder->price_for_sell, 2); ?>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>

<?= $this->render('_create_order', ['model' => $model]) ?>
<?= $this->render('_clickable_product_modal', [
    'priceList' => $model,
    'priceListOrders' => $priceListOrders,
    'showModalBtn' => '.show-pricelist-modal'
]) ?>

<style type="text/css">
    table#produc-table {
        width: 100%;
        position: relative;
        margin-bottom: 5px;
        table-layout: fixed;
        border-bottom: 1px solid #ddd;
    }
    table#produc-table th {
        padding: 10px;
    }
    table#produc-table td {
        vertical-align: middle;
        box-sizing: border-box;
    }
    table.quantity-box td {
        vertical-align: middle;
        text-align: center;
    }
    table.quantity-box button.quantity-btn,
    table.quantity-box button.quantity-btn:hover {
        margin: 0;
        padding: 6px 10px;
        border-radius: 4px !important;
        background-color: #e3e8ee;
        color: #333333;
    }
    .dataTables_wrapper .dataTables_paginate .paginate_button,
    .dataTables_wrapper .dataTables_paginate .paginate_button:hover,
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled,
    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover {
        padding: 0;
        border-width: 0;
    }
</style>