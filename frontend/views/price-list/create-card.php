<?php
/** @var $model \common\models\product\PriceList */
/** @var $modelContact \common\models\product\PriceListContact */
/** @var $actualSubscription \common\models\service\Subscribe */
/** @var $lastActiveSubscribe \common\models\service\Subscribe */

use common\models\product\PriceList;
use common\models\product\PriceListStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = ($model->isNewRecord) ? 'Создать Прайс-Лист' : 'Редактировать Прайс-Лист';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
        'contentCloning' => true,
    ],
]);

$form = ActiveForm::begin([
    'id' => 'price-list-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
    'fieldConfig' => [
        'template' => "{input}\n{label}\n{error}\n",
    ],
    'options' => [
        'class' => 'form-md-underline'
    ],
]);

$storeList = $user->getStores()
    ->select(['name'])
    ->orderBy(['is_main' => SORT_DESC])
    ->indexBy('id')
    ->column();

$contactModel = $model->contact ?: $modelContact;

if ($model->discount == round($model->discount))
    $model->discount = round($model->discount);
if ($model->markup == round($model->markup))
    $model->markup = round($model->markup);

$disableChangeDiscountMarkup = $model->status_id == PriceListStatus::STATUS_SEND && !$model->auto_update_prices;

?>

<div class="form-body form-body_sml form-price-list">
    <?= $form->errorSummary($model); ?>

    <?= Html::hiddenInput('documentType', Documents::IO_TYPE_OUT, ['id' => 'documentType']); // for products table ?>
    <?= Html::hiddenInput('PriceList[is_custom_price_list]', 1) ?>

    <div class="portlet">
        <div class="portlet-title">
            <div class="caption" style="width: 100%;">
                <div class="col-lg-6 col-sm-9 col-xs-12 pad0">
                    <table style="width: 100%; font-weight: bold!important;">
                        <tr>
                            <td style="width: 120px">Название</td>
                            <td>
                                <?= $form->field($model, 'name', ['options' => ['style' => 'padding-top: 9px']])->textInput([
                                    'maxlength' => true,
                                ])->label(false) ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="portlet-body">
            <div class="main-caption">
                Контактные данные
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                                'options' => [
                                    'class' => 'form-control',
                                    'placeholder' => '+7(XXX) XXX-XX-XX',
                                ],
                            ]) ?>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'email')->textInput([
                                'maxlength' => true,
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'site')->textInput([
                                'maxlength' => true,
                            ]) ?>
                        </div>
                    </div>
                </div>

                <div class="clearfix m-b-10 m-t-10"></div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'lastname')->label('Фамилия')->textInput(); ?>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'firstname')->label('Имя')->textInput(); ?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'patronymic')->label('Отчество')->textInput(); ?>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <?= $form->field($contactModel, 'no_patronymic', [
                                'template' => "{label}\n{input}",
                                'options' => [
                                    'class' => 'form-group form-md-line-input form-md-floating-label',
                                    'style' => 'display: inline-block;',
                                ],
                                'labelOptions' => [
                                    'class' => 'control-label',
                                    'style' => 'padding: 6px 0; margin-right: 9px;',
                                ]
                            ])->checkbox([], true) ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row m-t-10 m-b-10">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12 col-md-3">
                            <div class="include_columns">
                                <div class="price-list-label main-caption">
                                    Столбцы в прайс-листе:
                                </div>
                                <div class="include-columns_list form-group">
                                    <?= \common\components\helpers\Html::activeCheckbox($model, 'include_name_column', [
                                        'label' => 'Наименование',
                                        'disabled' => true
                                    ]); ?>
                                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS || $productionType == Product::PRODUCTION_TYPE_ALL): ?>
                                        <?= Html::activeCheckbox($model, 'include_article_column', [
                                            'label' => 'Артикул',
                                            'class' => 'toggle_products_column',
                                            'data-target' => '.col_article'
                                        ]); ?>
                                    <?php endif; ?>
                                    <?= Html::activeCheckbox($model, 'include_product_group_column', [
                                        'label' => 'Группа ',
                                        'class' => 'toggle_products_column',
                                        'data-target' => '.col_group'
                                    ]); ?>
                                    <?php if ($productionType == Product::PRODUCTION_TYPE_GOODS || $productionType == Product::PRODUCTION_TYPE_ALL): ?>
                                        <?= Html::activeCheckbox($model, 'include_reminder_column', [
                                            'id' => 'include_reminder_column',
                                            'label' => 'Остаток',
                                            'class' => 'toggle_products_column',
                                            'data-target' => '.col_quantity'
                                        ]); ?>
                                    <?php endif; ?>
                                    <?= Html::activeCheckbox($model, 'include_product_unit_column', [
                                        'label' => 'Ед. измерения',
                                        'disabled' => true
                                    ]); ?>
                                    <?= Html::activeCheckbox($model, 'include_price_column', [
                                        'label' => 'Цена (продажи)',
                                        'class' => 'toggle_products_column',
                                        'data-target' => '.col_price'
                                    ]); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <div class="sort_type">
                                <div class="price-list-label main-caption">
                                    Сортировка по столбцу:
                                </div>
                                <?= $form->field($model, 'sort_type')
                                    ->radioList([
                                        PriceList::SORT_BY_NAME => 'Наименование',
                                        PriceList::SORT_BY_GROUP => 'Группа ',
                                    ])->label(false); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6 price-list-options">
                            <div class="main-caption">
                                Дополнительно
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="max-width: 310px;  margin-top: 10px; margin-bottom: 5px;">
                                    <label style="width: 100%;">
                                        <?= Html::activeCheckbox($model, 'can_checkout', [
                                            'label' => 'Оформление заказа из прайс-листа',
                                            'disabled' => !$tariffRules['has_checkout']
                                        ]); ?>
                                    </label>
                                    <div style="position:absolute; top:-3px; right:-18px;"
                                         class="tooltip2 ico-question"
                                         data-tooltip-content="#tooltip_can_checkout"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="max-width: 310px;">
                                    <label style="width: 100%;">
                                        <?= Html::activeCheckbox($model, 'include_description_column', [
                                            'label' => 'Добавить описание',
                                            'class' => 'toggle_products_column',
                                            'data-target' => '.col_comment',
                                            'disabled' => !$tariffRules['has_product_view']
                                        ]); ?>
                                    </label>
                                    <div style="position:absolute; top:-3px; right:-18px;"
                                         class="tooltip2 ico-question"
                                         data-tooltip-content="#tooltip_image_description"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="max-width: 310px;">
                                    <label style="width: 100%; margin-top: 5px;">
                                        <?= Html::activeCheckbox($model, 'include_image_column', [
                                            'label' => 'Добавить изображение',
                                            'class' => 'toggle_products_column',
                                            'data-target' => '.col_image',
                                            'disabled' => !$tariffRules['has_product_view']
                                        ]); ?>
                                    </label>
                                    <div style="position:absolute; top:1px; right:-18px;"
                                         class="tooltip2 ico-question"
                                         data-tooltip-content="#tooltip_image_description"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="max-width: 310px;">
                                    <label style="width: 100%; margin-top: 5px;">
                                        <?= Html::activeCheckbox($model, 'has_discount', [
                                            'label' => 'Добавить скидку',
                                            'class' => 'toggle_products_column discount-percent',
                                            'data-target' => '.col_discount',
                                            'data-value' => (int)$model->has_discount,
                                            'disabled' => $disableChangeDiscountMarkup
                                        ]); ?>
                                    </label>
                                </div>
                                <div class="col-xs-6 discount_block <?= (!$model->has_discount) ? 'hide' : '' ?>" style=" padding-top: 5px; padding-left: 0;">
                                    <div style="max-width: 120px;">
                                        <?= $form->field($model, 'discount', ['options' => ['tag' => false]])
                                            ->textInput([
                                                'id' => 'all-discount',
                                                'disabled' => $disableChangeDiscountMarkup,
                                                'data-value' => $model->discount,
                                                'style' => 'width:65px; text-align:right;'
                                            ])
                                            ->label('%%', ['style' => 'display: block; position: absolute; left: 75px; top: 6px;']) ?>
                                    </div>
                                    <div style="position: absolute; left: 115px; top: 7px;">В прайс-листе цена будет с учетом скидки</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" style="max-width: 310px; margin-top: 5px;">
                                    <label style="width: 100%;">
                                        <?= Html::activeCheckbox($model, 'has_markup', [
                                            'label' => 'Добавить наценку',
                                            'class' => 'toggle_products_column markup-percent',
                                            'data-target' => '.col_markup',
                                            'data-value' => (int)$model->has_markup,
                                            'disabled' => $disableChangeDiscountMarkup
                                        ]); ?>
                                    </label>
                                </div>
                                <div class="col-xs-6 markup_block <?= (!$model->has_markup) ? 'hide' : '' ?>" style="padding-top: 5px; padding-left: 0;">
                                    <div style="max-width: 120px;">
                                        <?= $form->field($model, 'markup', ['options' => ['tag' => false]])
                                            ->textInput([
                                                'id' => 'all-markup',
                                                'disabled' => $disableChangeDiscountMarkup,
                                                'data-value' => $model->markup,
                                                'style' => 'width:65px; text-align:right;'
                                            ])
                                            ->label('%%', ['style' => 'display: block; position: absolute; left: 75px; top: 6px;']) ?>
                                    </div>
                                    <div style="position: absolute; left: 115px; top: 7px;">В прайс-листе цена будет с учетом наценки</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="margin-top: 5px;">
                                    <label>
                                        <?= Html::activeCheckbox($model, 'auto_update_prices', [
                                            'label' => 'Автоматически обновлять цены',
                                            'class' => 'form-control',
                                            'disabled' => !$tariffRules['has_auto_update']
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label style="margin-top: 5px;">
                                        <?= Html::activeCheckbox($model, 'auto_update_quantity', [
                                            'label' => 'Автоматически обновлять остатки',
                                            'class' => 'form-control',
                                            'disabled' => !$tariffRules['has_auto_update']
                                        ]); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="max-width: 310px; ">
                                    <label style="margin-top: 5px;">
                                        <?= Html::activeCheckbox($model, 'use_notifications', [
                                            'label' => 'Получать уведомления об открытии',
                                            'class' => 'form-control',
                                            'disabled' => !$tariffRules['has_notification']
                                        ]); ?>
                                    </label>
                                    <div style="position:absolute; top:1px; right:-18px;"
                                         class="tooltip2 ico-question"
                                         data-tooltip-content="#tooltip_notifications"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-3">
                    <div class="include_type_id">
                        <div class="price-list-label main-caption">
                            Включить в прайс-лист:
                        </div>
                        <div style="margin-top: 13px; max-width: 225px;">
                        <?= $form->field($model, '_price_list_type_id')
                            ->widget(\kartik\select2\Select2::class, [
                                'hideSearch' => true,
                                'data' => PriceList::$includeTypes,
                                'options' => [
                                    'tag' => false,
                                    'value' => $model->getPriceListType(),
                                ]
                            ])
                            ->label(false); ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 collapse <?= $model->include_reminder_column ? 'in' : '' ?>" id="stockCollapse">
                    <div>
                        <div class="price-list-label main-caption">
                            Учитывать остатки товара на складах:
                        </div>
                        <div class="form-group">
                            <?= Html::checkbox('storeId[]', true, [
                                'class' => 'choose_store store_all',
                                'label' => 'Все склады',
                                'value' => 'all'
                            ]); ?>
                            <?php foreach ($storeList as $id => $storeName): ?>
                                <?= Html::checkbox('storeId[]', false, [
                                    'class' => 'choose_store',
                                    'label' => $storeName,
                                    'value' => $id
                                ]); ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix m-b-10 m-t-10"></div>

            <div class="col-xs-12 pad0 m-t-10 custom_products_block <?= $model->priceListType != PriceList::FOR_SELECTIVELY ? 'hidden' : '' ?>" style="margin-bottom: 50px;">
                <?= $this->render('parts_create/_product_table', [
                    'model' => $model,
                    'company' => $company,
                ]); ?>
            </div>

        </div>

    </div>
</div>

<div class="clearfix"></div>

<div id="buttons-bar-fixed">
    <div class="row action-buttons">
        <div class="spinner-button button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if ($actualSubscription || !$lastActiveSubscribe): ?>
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button' : 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]) ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn darkblue widthe-100 hidden-lg'
                ]) ?>
            <?php else: ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Сохранить',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['/subscribe']),
                    'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                        'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                        'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                ]); ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-floppy-o fa-2x"></i>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ],
                    'confirmUrl' => Url::to(['/subscribe']),
                    'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                        'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                        'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <a href="<?= Yii::$app->request->referrer ?: 'index' ?>" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal" aria-hidden="true">Отменить</a>
            <a href="<?= Yii::$app->request->referrer ?: 'index' ?>" class="btn darkblue widthe-100 hidden-lg" title="Отменить" data-dismiss="modal" aria-hidden="true"><i class="fa fa-reply fa-2x"></i></a>
        </div>
</div>

<?php $form->end(); ?>

<div class="tooltip-templates" style="display: none">
    <div id="tooltip_image_description">
        При клике на строку прайс-листа откроется<br/>
        карточка товара с описанием и изображением
    </div>
    <div id="tooltip_can_checkout">
        Это позволит вашему клиенту из прай-листа сделать заказ и выставить счет.<br/>
        Заказ и счет создадутся у вас в КУБе
    </div>
    <div id="tooltip_notifications">
        При открытии прайс-листа клиентом<br/>
        появится уведомление
    </div>
</div>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_new'); ?>

<?= $this->render('@frontend/modules/documents/views/invoice/form/_form_product_existed', [
    'model' => $model,
]); ?>

<?= $this->render('parts_create/_confirm_modals') ?>

<script>
    $('#include_reminder_column').on('click', function() {
        $('#stockCollapse').collapse($(this).prop('checked') ? 'show' : 'hide');
    });
    $('#pricelist-has_discount').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.row').find('.discount_block').removeClass('hide');
            $('#pricelist-has_markup').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.row').find('.discount_block').addClass('hide');
            $(this).closest('.row').find('input').val('');
        }
    });
    $('#pricelist-has_markup').on('change', function() {
        if ($(this).prop('checked')) {
            $(this).closest('.row').find('.markup_block').removeClass('hide');
            $('#pricelist-has_discount').prop('checked', false).uniform('refresh').trigger('change');
        } else {
            $(this).closest('.row').find('.markup_block').addClass('hide');
            $(this).closest('.row').find('input').val('');
        }
    });

    $('.choose_store').on('click', function() {
        if ($('.choose_store').filter(':checked').length == 0) {
            if ($(this).hasClass('store_all'))
                $('.choose_store').not('.store_all').first().prop('checked', true).uniform('refresh');
            else
                $('.store_all').prop('checked', true).uniform('refresh');
        } else if ($(this).val() == 'all') {
            $('.choose_store').not('.store_all').prop('checked', false).uniform('refresh');
        } else {
            $('.store_all').prop('checked', false).uniform('refresh');
        }
        window.PriceListForm.recalculateQuantities();
    });

    $('#pricelist-_price_list_type_id').on('change', function() {
        var $productBlock = $('.custom_products_block');
        if ($(this).val() == "<?= PriceList::FOR_SELECTIVELY ?>")
            $productBlock.removeClass('hidden');
        else
            $productBlock.addClass('hidden');
    });
    $('.toggle_products_column').on('change', function() {
        var targetBlock = $(this).data('target');
        if ($(this).prop('checked')) {
            $(targetBlock).removeClass('hidden');
        } else {
            $(targetBlock).addClass('hidden');
        }
    });

    $('#add-from-exists').on("hidden.bs.modal", function () {
        setTimeout("window.PriceListForm.recalculateQuantities()", 500);
    });
    $('#add-new').on("hidden.bs.modal", function () {
        setTimeout("window.PriceListForm.recalculateQuantities()", 500);
    });

    window.PriceListForm = {
        recalculateQuantities: function()
        {
            $('.order-param-value.product-quantity').each(function(j, column) {
                var totalQuantity = 0;
                $('.choose_store').filter(':checked').each(function(i, checkbox) {
                    var storeId = $(checkbox).val();
                    var quantity = $(column).attr('data-store-' + storeId);
                    totalQuantity += parseFloat(quantity);
                });
                $(column).text(totalQuantity);
            });
        },
        refreshMarkupDiscount: function()
        {
            $("#table-for-invoice tbody .discount-input").val($('#all-discount').val()).each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
            $("#table-for-invoice tbody .markup-input").val($('#all-markup').val()).each(function () {
                INVOICE.recalculateItemPrice($(this));
            });
        }
    }


    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });

    $('#all-discount').on('change', function() {
        var $input = $("input#all-discount");
        var val = Math.max(Math.min(parseFloat($input.val()), 99.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#table-for-invoice tbody .markup-input").val(0);
        $("#table-for-invoice tbody .discount-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        // INVOICE.recalculateInvoiceTable();
    });

    $('#all-markup').on('change', function() {
        var $input = $("input#all-markup");
        var val = Math.max(Math.min(parseFloat($input.val()), 9999.9999), 0);
        if (val != $input.val()) {
            $input.val(val);
        }
        $("#table-for-invoice tbody .discount-input").val(0);
        $("#table-for-invoice tbody .markup-input").val(val).each(function () {
            INVOICE.recalculateItemPrice($(this));
        });
        // INVOICE.recalculateInvoiceTable();
    });

    $('#pricelist-has_discount').on('change', function() {
        window.PriceListForm.refreshMarkupDiscount();
    });
    $('#pricelist-has_markup').on('change', function() {
        window.PriceListForm.refreshMarkupDiscount();
    });

    <?php if ($model->status_id == PriceListStatus::STATUS_SEND): ?>
        $('#pricelist-auto_update_prices').on('change', function() {
            var has_discount = $('#pricelist-has_discount');
            var has_markup = $('#pricelist-has_markup');
            var discount = $('#all-discount');
            var markup = $('#all-markup');

            if ($(this).prop('checked')) {
                $('#modal-auto-update-prices').modal('show');
            } else {
                $(has_discount).prop('checked', $(has_discount).data('value')).trigger('change').attr('disabled', 'disabled');
                $(has_markup).prop('checked', $(has_markup).data('value')).trigger('change').attr('disabled', 'disabled');
                $(discount).val($(discount).data('value')).attr('disabled', 'disabled');
                $(markup).val($(markup).data('value')).attr('disabled', 'disabled');
            }
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
        $('#modal-auto-update-prices button.yes').on('click', function() {
            $('#pricelist-has_discount').removeAttr('disabled');
            $('#pricelist-has_markup').removeAttr('disabled');
            $('#all-discount').removeAttr('disabled');
            $('#all-markup').removeAttr('disabled');
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
        $('#modal-auto-update-prices button.no').on('click', function() {
            $('#pricelist-has_discount').attr('disabled', 'disabled');
            $('#pricelist-has_markup').attr('disabled', 'disabled');
            $('#all-discount').attr('disabled', 'disabled');
            $('#all-markup').attr('disabled', 'disabled');
            $('#pricelist-auto_update_prices').prop('checked', false);
            $('#price-list-form .price-list-options [type="checkbox"]').uniform('refresh');
        });
    <?php endif; ?>
</script>