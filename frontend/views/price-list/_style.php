<style>
    .form-md-underline .select2-container .select2-selection--single {
        height:28px!important;
    }

    .sbs-menu {
        height: 70px;
        margin-bottom:25px;
    }
    .sbs-menu > a, .sbs-menu > a:hover, .sbs-menu > a:focus { text-decoration: none; }
    .sbs-el {
        width: 20%;
        display: block;
        float:left;
        height: 70px;
        margin: 0;
        border: 1px solid #eee;
    }
    .sbs-el > table {
        width:100%;
        height:100%;
    }
    .sbs-el td.image-block {
        position:relative;
        width: 50px;
        height: 50px;
        border-radius: 25px !important;
        background: #eee;
        float:left;
        margin-top: 10px;
        margin-left: 10px;
    }
    .sbs-step-1 td.image-block { padding:6px 0 0 11px; }
    .sbs-step-2 td.image-block { padding:12px 0 0 15px; }
    .sbs-step-3 td.image-block { padding:10px 0 0 10px; }
    .sbs-step-4 td.image-block { padding:12px 0 0 16px; }
    .sbs-step-5 td.image-block { padding:5px 0 0 11px; }

    @media (max-width: 1250px) {
        .sbs-el td.image-block {
            display:none;
        }
    }
    .sbs-el td.text-block {
        width:100%;
        vertical-align: middle;
        text-align: center;
        padding: 0 10px;
        font-size: 14px;
        font-weight: 700;
        color: #000 !important;
    }

    .sbs-el .image-block > div {
        background-repeat: no-repeat;
        background-size: 70% !important;
    }

    .sbs-el .image-block > i {
        font-size:24px;
        color:#333;
    }
    .sbs-el .image-block [class^="flaticon-"]:before,
    .sbs-el .image-block [class*=" flaticon-"]:before,
    .sbs-el .image-block [class^="flaticon-"]:after,
    .sbs-el .image-block [class*=" flaticon-"]:after {
        font-size: 24px;
    }
    .sbs-el .image-block .fa-pencil-wrench {
        position: relative;
        margin-left: -25px !important;
        -ms-transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg);
        transform: rotate(-90deg);
        top: -2px;
        left: -1px;
    }

    /* active */
    .sbs-menu .active {
        border: 1px solid #67d77d;
    }

    .sbs-menu .active .image-block {
        background-color: #67d77d;
    }
    .sbs-menu .active .image-block > i {
        color: #fff;
    }

    .sbs-menu .active .text-block {
        color: #000 !important;
    }

    /* finish */
    .sbs-menu .finish .image-block {
        border-color: #67d77d;
    }
    .sbs-menu .finish .image-block > i {
        color: #333;
    }

    .sbs-menu .finish {
        background-color: #67d77d !important;
        border: 1px solid #67d77d;
        border-right: 1px solid #fff !important;
    }
    .sbs-menu .finish .text-block {
        color: #fff !important;
    }


    .step-by-step-form > .row > .col-xs-6,
    .step-by-step-form > .row > .col-xs-9,
    .step-by-step-form > .row > .col-xs-12,
    .step-by-step-form > .row > .new-account > .col-xs-6 {
        margin-bottom:5px;
    }

    .step-by-step-form .main-caption {
        display: block;
        font-size:16px;
        font-weight: bold;
        margin-top:10px;
    }

    .step-by-step-form .caption {
        display: block;
        margin-top: 25px;
        margin-bottom: 10px;
        font-size:16px;
        font-weight: bold;
    }

    .step-by-step-form .bold {
        font-weight: bold;
    }

    .buttons-block .btn {
        color:#eee;
    }

    .step-by-step .out-invoice-index > .row > .col-sm-8 > div {
        font-size:16px!important;
        font-weight: bold!important;
    }

    .darkblue-invert {
        color:#4276a4!important;
        border:1px solid #4276a4!important;
        background-color:#fff!important;
    }
    .darkblue-invert:hover {
        color: #426997 !important;
        border:1px solid #426997!important;
    }
    select.input-sm {
        height:34px;
    }
    .form-md-underline .form-group.field-company-company_type_id > label {
        top:3px!important;
    }
    /* style borders*/
    .bord-light-b {
        border-bottom: 1px solid #ececec;
    }
    /* errors */
    .form-md-underline .form-group.form-md-line-input .form-control:focus:not([readonly]) ~ .help-block {
        color: red;
    }
    .form-group.form-md-line-input.has-error .help-block {
        color: red !important;
        opacity: 1 !important;
    }
    #get-fias-ittem.error,
    #not_use_account.error {
        border-color: red !important;
    }
    .form-group.form-md-line-input.has-error .form-control {
        border-bottom: 1px solid red;
    }
    .form-group.form-md-line-input.has-error .form-control.edited:not([readonly]) ~ label,
    .form-group.form-md-line-input.has-error .form-control.form-control-static ~ label,
    .form-group.form-md-line-input.has-error .form-control:focus:not([readonly]) ~ label,
    .form-group.form-md-line-input.has-error .form-control.focus:not([readonly]) ~ label {
        color: red;
    }
    .has-error .help-block,
    .has-error .help-inline,
    .has-error .control-label,
    .has-error .radio,
    .has-error .checkbox,
    .has-error .radio-inline,
    .has-error .checkbox-inline {
        color: red;
    }

    #company-nds {
        margin-top:15px;
    }

    .b2b-description-block > ol {
        padding-left: 15px;
    }
    .b2b-description-block ul {
        padding-left: 15px;
        list-style: square;
    }
    .help-book {
        margin-left: 5px;
        margin-top: -3px;
        cursor: pointer;
    }

    /** INSTRUCTION PAGE **/

    /* blocks */
    .t-tile .title { min-height: 60px; font-size: 18px; color: #4679ae; }
    .t-tile .title > .img { float:left; }
    .t-tile .title > .txt {  }
    .t-tile .title > span {}
    .t-tile .list {}
    .t-tile .list ul { padding-left: 0; }
    .t-tile .list ul li { margin-left: 15px; }
    .st-tile { border-radius: 4px!important; border:1px solid #ddd; padding:20px; }
    .st-tile .title { font-size: 16px; color: #4679ae; font-weight: bold; }
    .st-tile .subtitle { width: 50%; font-size: 16px; }
    .bblock { margin: 0 -5px; }
    .bblock .col-sm-3, .bblock .col-sm-4, .bblock .col-sm-6 { padding: 0 5px; }
    .subcaption { font-size:18px; font-weight: bold; }
    .st-tile { margin-bottom: 10px; line-height: 1.2; }
    .st-tile .ico { position: absolute; top:20px; left:30px; }
    .st-tile .title { margin-bottom: 20px; }
    .st-tile .ico + .title { margin-left: 55px; min-height: 40px; }
    .st-tile .subtitle { width: 100%; }
    .st-tile.small .subtitle {font-size: 12px; color: #9198a0; }
    .st-tile.medium .subtitle {font-size: 13px; color: #9198a0; }
    .st-tile .subtitle { font-size: 15px; }
    .st-tile .bottom-img { text-align: center; margin: 20px -20px -30px -20px; min-height: 178px; }
    .st-tile .bottom-img > img { width: 100%; max-width: 198px; }
    .st-tile .title-top-pad { padding-top: 9px; }
    .bg_1 {
        border: 1px solid #ddd;
        background-image: url(/img/price_list/man_flying_money.png);
        background-repeat: no-repeat;
        background-position: 100% 100%;
    }
</style>