<?php

use common\models\document\Act;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\models\AgreementTemplate;
use common\models\employee\Employee;
use frontend\widgets\BtnConfirmModalWidget;
use yii\grid\ActionColumn;
use common\models\product\PriceListStatus;
use frontend\rbac\permissions;

/* @var $actualSubscription \common\models\service\Subscribe */
/* @var $lastActiveSubscribe \common\models\service\Subscribe */

$this->title = 'Прайс-листы';

$emptyMessage = 'У вас пока не создано ни одного прайс-листа.';

$canDelete = Yii::$app->getUser()->can(permissions\Product::DELETE);
$canSend = FALSE; // todo: сделать модалку отправки; Yii::$app->getUser()->can(permissions\Product::UPDATE);
?>

<?= $this->render('_style') ?>

<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 5, 'company' => $company]) ?>
        <div class="col-xs-12 col-lg-12 pad0">

            <div class="portlet box">
                <div class="btn-group pull-right">
                    <?php if ($actualSubscription || !$lastActiveSubscribe): ?>
                        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create-card']), [
                            'class' => 'btn yellow',
                        ]); ?>
                    <?php else: ?>
                        <?= ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<i class="fa fa-plus"></i> ДОБАВИТЬ',
                                'class' => 'btn yellow',
                            ],
                            'confirmUrl' => Url::to(['/subscribe']),
                            'message' => ($lastActiveSubscribe && $lastActiveSubscribe->tariff_id == 39) ?
                                'У вас закончился срок пробного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?' :
                                'У вас закончился срок оплаченного периода, необходимо оплатить тариф "Прайс-лист". Перейти к оплате?'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <span style="font-size:16px; font-weight: bold"><?= $this->title; ?></span>
            </div>

            <div class="portlet box darkblue price-list-index">
                <div class="portlet-title row-fluid">
                    <div class="caption">
                        Список прайс-листов: <?= $dataProvider->totalCount ?>
                    </div>
                    <div class="tools search-tools tools_button col-md-4 col-sm-4">
                        <div class="form-body">
                            <?php $form = \yii\widgets\ActiveForm::begin([
                                'method' => 'GET',
                                'fieldConfig' => [
                                    'template' => "{input}\n{error}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ],
                            ]); ?>
                            <div class="search_cont">
                                <div class="wimax_input">
                                    <?= $form->field($searchModel, 'find_by')->textInput([
                                        'placeholder' => 'Поиск...',
                                    ]); ?>
                                </div>
                                <div class="wimax_button">
                                    <?= Html::submitButton('НАЙТИ', [
                                        'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                                    ]) ?>
                                </div>
                            </div>
                            <?php $form->end(); ?>
                        </div>
                    </div>
                    <div class="actions joint-operations col-md-4 col-sm-4" style="display:none;">
                        <?php if ($canDelete) : ?>
                            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                                'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                                'data-toggle' => 'modal',
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                                'class' => 'btn btn-default btn-sm hidden-lg',
                                'title' => 'Удалить',
                                'data-toggle' => 'modal',
                            ]); ?>

                            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                                 tabindex="-1" aria-hidden="true"
                                 style="display: none; margin-top: -51.5px;">
                                <div class="modal-dialog ">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="form-body">
                                                <div class="row">Вы уверены, что хотите удалить выбранные прайс-листы?</div>
                                            </div>
                                            <div class="form-actions row">
                                                <div class="col-xs-6">
                                                    <?= Html::a('ДА', null, [
                                                        'class' => 'btn darkblue pull-right modal-many-delete',
                                                        'data-url' => Url::to(['many-delete']),
                                                    ]); ?>
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                        <?php if ($canSend) : ?>
                            <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                                'class' => 'btn btn-default btn-sm document-many-send hidden-md hidden-sm hidden-xs',
                                'data-url' => Url::to(['many-send']),
                            ]); ?>
                            <?= Html::a('<i class="glyphicon glyphicon-envelope"></i>', null, [
                                'class' => 'btn btn-default btn-sm document-many-send hidden-lg',
                                'title' => 'Отправить',
                                'data-url' => Url::to(['many-send']),
                            ]); ?>
                            <div class="modal fade confirm-modal" id="many-send-error"
                                 tabindex="-1"
                                 role="modal"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                    data-dismiss="modal"
                                                    aria-hidden="true"></button>
                                            <h3 style="text-align: center; margin: 0">Ошибка при отправке прайс-листов</h3>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-body">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <div class="portlet-body">
                    <div class="table-container">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'emptyText' => $emptyMessage,
                            'tableOptions' => [
                                'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap agreements-templates-table fix-thead',
                                'aria-describedby' => 'datatable_ajax_info',
                                'role' => 'grid',
                            ],

                            'headerRowOptions' => [
                                'class' => 'heading',
                            ],

                            'options' => [
                                'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                            ],

                            'pager' => [
                                'options' => [
                                    'class' => 'pagination pull-right',
                                ],
                            ],
                            'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount, 'scroll' => true]),
                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                            'columns' => [
                                [
                                    'header' => Html::checkbox('', false, [
                                        'class' => 'joint-operation-main-checkbox',
                                    ]),
                                    'headerOptions' => [
                                        'class' => 'text-center',
                                        'width' => '5%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'text-center',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return Html::checkbox('PriceList[' . $model->id . '][checked]', false, [
                                                'class' => 'joint-operation-checkbox',
                                            ]) . Html::hiddenInput('price', 0, ['class' => 'price', 'disabled' => true]);

                                    },
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => 'Дата',
                                    'headerOptions' => ['style' => 'width:10%;'],
                                    'format' => ['date', 'php:d.m.Y'],
                                ],
                                [
                                    'attribute' => 'name',
                                    'label' => 'Название',
                                    'headerOptions' => [
                                        'class' => 'sorting',
                                        'width' => '20%',
                                    ],
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return Html::a($model->name, ['view-card', 'id' => $model->id], ['data-pjax' => 0]);
                                    },
                                ],
                                [
                                    'attribute' => 'comment_internal',
                                    'label' => 'Комментарий',
                                    'headerOptions' => [
                                        'width' => '40%',
                                    ],
                                ],
                                [
                                    'attribute' => 'status_id',
                                    'label' => 'Статус',
                                    'headerOptions' => [
                                        'width' => '10%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'text-left text-ellipsis',
                                    ],
                                    'class' => DropDownSearchDataColumn::className(),
                                    'value' => function ($model) {
                                        return ($model->is_archive) ? 'В архиве' : $model->status->name;
                                    },
                                    'format' => 'raw',
                                    'filter' => [
                                        '' => 'Все',
                                        PriceListStatus::STATUS_CREATED => 'Действующий',
                                        PriceListStatus::STATUS_SEND => 'Отправлен',
                                        PriceListStatus::STATUS_ARCHIVE => 'В архиве',
                                    ]
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'controller' => 'price-list',
                                    'template' => '{update} {delete}',
                                    'headerOptions' => [
                                        'width' => '5%',
                                    ],
                                    'contentOptions' => [
                                        'class' => 'text-left'
                                    ],
                                    'buttons' => [
                                        'update' => function ($url, $model, $key) {
                                            return Html::a("<span class='glyphicon glyphicon-pencil'></span>",
                                                Url::to(['/price-list/update-card', 'id' => $model->id]), [
                                                    'title' => 'Изменить',
                                                    'aria-label' => 'Изменить',
                                                    'data-pjax' => '0',
                                                ]);
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            return \frontend\widgets\ConfirmModalWidget::widget([
                                                'toggleButton' => [
                                                    'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                                    'title' => 'Удалить',
                                                    'aria-label' => 'Удалить',
                                                    'data-pjax' => '0',
                                                    'tag' => 'a',
                                                ],
                                                'confirmUrl' => Url::to(['/price-list/delete', 'id' => $model->id, 'productionType' => $model->production_type]),
                                                'confirmParams' => [],
                                                'message' => 'Вы уверены, что хотите удалить прайс-лист?',
                                            ]);
                                        }
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>

                <?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
                    'buttons' => [
                        $canSend ? Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                            'class' => 'btn btn-sm darkblue text-white document-many-send',
                            'data-url' => Url::to(['many-send']),
                        ]) : null,
                        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                            'class' => 'btn btn-sm darkblue text-white',
                            'data-toggle' => 'modal',
                        ]) : null,
                    ],
                ]); ?>

            </div>

        </div>
    </div>
</div>

