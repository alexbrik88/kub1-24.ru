<?php
use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\dictionary\address\AddressDictionary;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

?>

<?= Html::activeHiddenInput($model, 'chief_post_name'); ?>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'inn')->textInput([
                'maxlength' => true,
            ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'egrip')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
    </div>
</div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'taxRegistrationDate')->textInput([
                'maxlength' => true,
                'class' => 'form-control input-sm date-picker' . ($model->taxRegistrationDate ? ' edited' : ''),
                'autocomplete' => 'off',
                'value' => $model->taxRegistrationDate,
                'disabled' => !empty($model->taxRegistrationDate),
            ])->label('Дата регистрации ИП') ?>
            <?= Html::tag('i', '', [
                'id' => 'show-taxregistrationdate',
                'class' => 'fa fa-calendar',
                'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
            ]) ?>
        </div>
        <div class="col-xs-12 col-md-6">
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'ip_lastname')->label('Фамилия')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'ip_firstname')->label('Имя')->textInput(); ?>
        </div>
    </div>
</div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'ip_patronymic')->label('Отчество')->textInput(); ?>
        </div>
        <div class="col-xs-12 col-md-6">
            <?= $form->field($model, 'has_chief_patronymic', [
                'template' => "{label}\n{input}",
                'options' => [
                    'class' => 'form-group form-md-line-input form-md-floating-label',
                    'style' => 'display: inline-block;',
                ],
                'labelOptions' => [
                    'class' => 'control-label',
                    'style' => 'padding: 6px 0; margin-right: 9px;',
                ]
            ])->checkbox([], true) ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?= $form->field($model, 'address_legal')->label('Адрес по прописке')->textInput(); ?>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12 col-md-6" style="margin-top:10px;">
            <?= $form->field($model, 'phone', [
                'options' => [
                    'class' => 'form-group form-md-line-input form-md-floating-label'
                ]
            ])->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => '+7(XXX) XXX-XX-XX',
                ],
            ])->label('Телефон', ['style' => 'top:0!important;font-size:13px!important']); ?>
        </div>
        <div class="col-xs-12 col-md-6">
        </div>
    </div>
</div>

<div class="clearfix"></div>