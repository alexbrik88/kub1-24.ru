<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\dictionary\address\AddressDictionary;
use common\models\service\SubscribeTariffGroup;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\web\View;

/**
 * @var View  $this
 * @var Company $company
 * @var SubscribeTariffGroup $tariffGroup
 * @var array $tariffList
 */

$this->title = 'Инструкция по созданию Прайс-листа';
?>

<?= $this->render('_style') ?>

<div class="row">
    <div class="col-xs-12 step-by-step">
        <?= $this->render('_steps', ['step' => 1, 'company' => $company]) ?>
        <div class="col-xs-12 col-lg-12 pad0">

            <div class="col-xs-12 pad0">
                <span style="font-size:20px; font-weight: bold">
                    Прайс-лист
                </span>
            </div>

            <br/><br/>

            <div class="col-xs-12 pad0">
                <span style="font-size:14px;">
                    Создайте уникальный прайс-лист, который <b>ПРОДАЁТ за ВАС!</b>
                </span>
            </div>

            <br/><br/><br/>

            <div class="col-xs-12 pad0">
                <span class="subcaption">
                    Ваши выгоды
                </span>
            </div>

            <br/><br/>

            <div class="row bblock autoheight-1">
                <div class="col-sm-4 col-xs-12">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/1.svg">
                        <div class="title title-top-pad">УДИВЛЯЙ</div>
                        <div class="subtitle">
                            Вместо скучных Excel таблиц, отправляйте клиентам продающий прайс-лист с описанием товара, количеством, артикулом, стоимостью и фото. Ваш прайс-лист — это мини интернет-магазин!
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/2.svg">
                        <div class="title title-top-pad">ВЫДЕЛЯЙСЯ</div>
                        <div class="subtitle">
                            Клиент получит ссылку на ваш прайс-лист с логотипом и вашими контактами. Он сможет открыть или скачать прайс-лист в любом удобном ему формате - WEB, PDF, XLS.
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="st-tile medium">
                        <img class="ico" src="/img/price_list/3.svg">
                        <div class="title title-top-pad">ПРОДАВАЙ</div>
                        <div class="subtitle">
                            Ваш клиент сможет выбрать необходимое кол-во товара и оформить заказ сразу в прайс-листе! Он автоматически получит счет на оплату с возможностью мгновенно его оплатить. А вы получите информацию о заказе.
                        </div>
                    </div>
                </div>
            </div>

            <br/>

            <div class="col-xs-12 pad0">
                <span class="subcaption">
                    Ваши преимущества
                </span>
            </div>

            <br/><br/>

            <div class="row bblock autoheight-2">
                <div class="col-sm-8 col-xs-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="st-tile medium">
                                <img class="ico" src="/img/price_list/21.svg">
                                <div class="title">Уведомление <br/> о прочтении</div>
                                <div class="subtitle">
                                    Как только клиент откроет прайс-лист, вам придет уведомление! Позвоните клиенту в тот момент, когда он изучает ваш прайс-лист. Вы его приятно удивите и, сразу сможете ответить на все его вопросы и проконсультировать по товару.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="st-tile medium">
                                <img class="ico" src="/img/price_list/22.svg">
                                <div class="title">Прайс-лист <br/> онлайн</div>
                                <div class="subtitle">
                                    Клиент может пользоваться им постоянно, пока вы его не отключите. Нужно поменять цену, кол-во, описание? Меняйте всё онлайн — данные в отправленном прайс-листе обновятся автоматически. Теперь не нужно после любой правки рассылать новые прайс-листы!
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="st-tile medium">
                                <img class="ico" src="/img/price_list/23.svg">
                                <div class="title">Продающий <br/> прайс-лист</div>
                                <div class="subtitle">
                                    Вам не нужно оформлять заказ и выставлять счета! Теперь заказ товара и счет на его оплату формируется из прайс-листа. Клиент это может сделать самостоятельно в любое время суток. Потенциальные клиенты обязательно оценят такой подход.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="st-tile medium">
                                <img class="ico" src="/img/price_list/24.svg">
                                <div class="title">Индивидуальные <br/> предложения</div>
                                <div class="subtitle">
                                    Создавайте индивидуальные прайс-листы под конкретного клиента с наценкой или скидкой. Добавляйте нужные столбцы и убирайте лишние.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 hidden-xs">
                    <div class="bg_1"></div>
                </div>
            </div>

            <br/>

            <div class="col-xs-12 pad0">
                <span class="subcaption">
                    Как сделать <b>Прайс-лист</b>
                </span>
            </div>

            <br/><br/>

            <div class="row bblock autoheight-3">
                <div class="col-sm-3 col-xs-12">
                    <div class="st-tile">
                        <div class="title"><b>ЛЕГКО</b><br/>создать</div>
                        <div class="subtitle">
                            Достаточно один раз загрузить ваши товары и услуги в КУБ. Загрузите список товаров с ценами, остатками, артикулами, описанием и даже фотографиями.
                        </div>
                        <div class="bottom-img">
                            <img  src="/img/price_list/women_1.png" style="margin-top:15px;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="st-tile">
                        <div class="title"><b>УДОБНО</b><br/>редактировать</div>
                        <div class="subtitle">
                            Загруженные товары и услуги можно использовать постоянно, включая или исключая нужные позиции в конкретном прайс-листе.
                        </div>
                        <div class="bottom-img">
                            <img  src="/img/price_list/women_2.png" style="margin-top:25px;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="st-tile">
                        <div class="title"><b>БЫСТРО</b><br/>добавить индивидуальности</div>
                        <div class="subtitle">
                            Загрузите ваш логотип, добавьте реквизиты компании и вашу контактную информацию.
                        </div>
                        <div class="bottom-img">
                            <img  src="/img/price_list/women_3.png">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="st-tile">
                        <div class="title"><b>ПРОСТО</b><br/>отправить</div>
                        <div class="subtitle">
                            Отправьте удобным для клиента способом: по e-mail, в мессенджере или в соцсети. Прайс-лист будет аккуратен, и читаем как на компьютере, так и на телефоне.
                        </div>
                        <div class="bottom-img">
                            <img  src="/img/price_list/women_4.png">
                        </div>
                    </div>
                </div>
            </div>

            <br/><br/>

            <div class="col-xs-12 pad0">
                <span class="subcaption">
                    Тарифы
                </span>
            </div>

            <br/><br/>

            <?= $this->render('_tariff', [
                'user' => $user,
                'company' => $user->company,
                'tariffGroup' => $tariffGroup,
                'tariffList' => $tariffList,
            ]) ?>

            <br/><br/><br/><br/><br/>

        </div>
    </div>
</div>

<script>
    autoheightBlock = function() {
        var b = $('.autoheight-1');
        var row1 = $(b).find('> div').eq(0);
        var row2 = $(b).find('> div').eq(1);
        var row3 = $(b).find('> div').eq(2);
        $(row2).find('.st-tile').height($(row3).find('.st-tile').height());
        $(row1).find('.st-tile').height($(row3).find('.st-tile').height());
        window.isAutoheightBlock = false;
    }
    autoheightBlock2 = function() {
        // right block
        var b = $('.autoheight-2');
        var row1 = $(b).find('> div').eq(0);
        var row2 = $(b).find('> div').eq(1);
        $(row2).find('.bg_1').height($(row1).height() - 10);
        // middle block
        var row3 = (b).find('> div').eq(0).find('> div').eq(0).find('> div').eq(0);
        var row4 = (b).find('> div').eq(0).find('> div').eq(0).find('> div').eq(1);

        if ($(row3).height() > $(row4).height())
            $(row4).find('.st-tile').height($(row3).find('.st-tile').height());
        else
            $(row3).find('.st-tile').height($(row4).find('.st-tile').height());

        // middle block
        var row5 = (b).find('> div').eq(0).find('> div').eq(1).find('> div').eq(0);
        var row6 = (b).find('> div').eq(0).find('> div').eq(1).find('> div').eq(1);
        $(row6).find('.st-tile').height($(row5).find('.st-tile').height());
        window.isAutoheightBlock2 = false;
    }
    autoheightBlock3 = function() {
        var b = $('.autoheight-3');
        var row1 = $(b).find('> div').eq(0);
        var row2 = $(b).find('> div').eq(1);
        var row3 = $(b).find('> div').eq(2);
        var row4 = $(b).find('> div').eq(3);
        $(row4).find('.title').height($(row3).find('.title').height());
        $(row2).find('.title').height($(row3).find('.title').height());
        $(row1).find('.title').height($(row3).find('.title').height());
        $(row3).find('.subtitle').height($(row4).find('.subtitle').height());
        $(row2).find('.subtitle').height($(row4).find('.subtitle').height());
        $(row1).find('.subtitle').height($(row4).find('.subtitle').height());
        window.isAutoheightBlock3 = false;
    }
    $(document).ready(function() {
        window.isAutoheightBlock = true;
        window.isAutoheightBlock2 = true;
        window.isAutoheightBlock3 = true;
        autoheightBlock();
        autoheightBlock2();
        autoheightBlock3();
    });
    $(window).on('resize', function() {
        if (!window.isAutoheightBlock) {
            window.isAutoheightBlock = true;
            setTimeout(function () {
                autoheightBlock();
            }, 100);
        }
        if (!window.isAutoheightBlock2) {
            window.isAutoheightBlock2 = true;
            setTimeout(function () {
                autoheightBlock2();
            }, 125);
        }
        if (!window.isAutoheightBlock3) {
            window.isAutoheightBlock3 = true;
            setTimeout(function () {
                autoheightBlock3();
            }, 150);
        }
    });
</script>