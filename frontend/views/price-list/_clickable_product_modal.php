<?php
use common\models\product\PriceList;
use common\widgets\Modal;
use yii\web\View;
use common\components\TextHelper;

/* @var $this yii\web\View
 * @var $priceList PriceList
 * @var $priceListOrders \common\models\product\PriceListOrder[]
 * @var $showModalBtn string
 */

// for modal
$isDescription = $priceList->include_description_column;
$isImage = $priceList->include_image_column;


if ($isImage || $isDescription): ?>
    <script>
        window.priceListOrder = {};
        <?php foreach ($priceListOrders as $key => $priceListOrder): ?>
        <?php $images = [];
        // new theme images
        for ($i=1; $i<=4; $i++)
            if ($src = $priceListOrder->product->getPictureThumb($i, PriceList::IMAGE_THUMB_WIDTH, PriceList::IMAGE_THUMB_HEIGHT))
                $images[] = urlencode($src);
        // old theme image
        if (empty($images)) {
            if ($src = $priceListOrder->product->getImageThumb(PriceList::IMAGE_THUMB_WIDTH, PriceList::IMAGE_THUMB_HEIGHT))
                $images[] = urlencode($src);
        }

        ?>
        window.priceListOrder['<?=($key)?>'] = {
            'image': <?= json_encode($images) ?>,
            'name':  <?= json_encode((string)$priceListOrder->name) ?>,
            'group': <?= json_encode($priceListOrder->productGroup ? (string)$priceListOrder->productGroup->title : '---') ?>,
            'price': <?= json_encode((string)TextHelper::invoiceMoneyFormat($priceListOrder->price_for_sell)) ?>,
            'description': <?= json_encode((string)$priceListOrder->description) ?>
        };
        <?php endforeach; ?>
        $(document).on('click', '<?=($showModalBtn)?>', function(e) {

            if ($(e.target).closest('td.td-no-clickable').length)
                return;

            var key = $(this).attr('data-key');
            var pOrder = window.priceListOrder[key];
            var $modal = $('#pricelist-image-modal');

            var isImage = <?= (int)$isImage ?>;
            var isDescription = <?= (int)$isDescription ?>;

            if (isImage && isDescription) {
                $modal.find('.block').hide().filter('.pic-desc-block').show();
            } else if (isImage) {
                $modal.find('.block').hide().filter('.pic-block').show();
            } else if (isDescription) {
                $modal.find('.block').hide().filter('.desc-block').show();
            } else {
                $modal.find('.block').hide();
                return;
            }

            if (pOrder.image.length > 0) {
                // one pic
                if (pOrder.image.length === 1) {
                    $modal.find('.product-single-image').show();
                    $modal.find('.product-many-image').hide();
                    $modal.find('.product-single-image img').attr('src', decodeURIComponent(pOrder.image[0]))
                    // many pics
                } else {
                    $modal.find('.product-many-image').show();
                    $modal.find('.product-single-image').hide();
                    for (i=0; i<=4; i++) {
                        if (pOrder.image[i])
                            $modal.find('.product-many-image .image-small[data-id="' + i + '"] img').attr('src', decodeURIComponent(pOrder.image[i])).show();
                        else
                            $modal.find('.product-many-image .image-small[data-id="' + i + '"] img').hide();
                    }
                }
                // no pic
            } else {
                $modal.find('.block').hide().end().find('.desc-block').show();
            }

            $modal.find('.product-image').html(pOrder.image);
            $modal.find('.product-name').html(pOrder.name);
            $modal.find('.product-group').html(pOrder.group);
            $modal.find('.product-price').html(pOrder.price);
            $modal.find('.product-description').html(pOrder.description);

            if (pOrder.description)
                $modal.find('.caption-description').show();
            else
                $modal.find('.caption-description').hide();

            $modal.modal('show');
        });
    </script>

    <?php Modal::begin(['id' => 'pricelist-image-modal', 'toggleButton' => false]); ?>
    <div class="pic-desc-block block" style="display: none">
        <div class="pic-part">
            <div class="col-sm-6">
                <div class="product-single-image">
                    <div class="image"><img src=""/></div>
                </div>
                <div class="product-many-image" style="display: none">
                    <div style="width: 100%">
                        <div class="image-small" data-id="0"><img src=""/></div>
                        <div class="image-small" data-id="1"><img src=""/></div>
                    </div>
                    <div style="width: 100%">
                        <div class="image-small" data-id="2"><img src=""/></div>
                        <div class="image-small" data-id="3"><img src=""/></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="product-name"></div>
                <div class="product-caption">Группа: </div><div class="product-group"></div><br/>
                <div class="product-caption">Цена: </div><div class="product-price"></div><div class="product-price-r">₽</div><br/>
                <div class="product-caption caption-description">Описание</div><br/>
                <div class="product-description"></div>
            </div>
        </div>
    </div>

    <div class="desc-block block" style="display: none">
        <div class="desc-part">
            <div class="col-sm-12">
                <div class="product-name"></div>
                <div class="product-caption">Группа: </div><div class="product-group"></div><br/>
                <div class="product-caption">Цена: </div><div class="product-price"></div><div class="product-price-r">₽</div><br/>
                <div class="product-caption caption-description">Описание</div><br/>
                <div class="product-description"></div>
            </div>
        </div>
    </div>

    <div class="pic-block block" style="display: none">
        <div class="pic-part">
            <div class="col-sm-12">
                <div class="product-single-image">
                    <div class="image"><img src=""/></div>
                </div>
                <div class="product-many-image" style="display: none">
                    <div style="width: 100%">
                        <div class="image-small" data-id="0"><img src=""/></div>
                        <div class="image-small" data-id="1"><img src=""/></div>
                    </div>
                    <div style="width: 100%">
                        <div class="image-small" data-id="2"><img src=""/></div>
                        <div class="image-small" data-id="3"><img src=""/></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php Modal::end(); ?>

<?php endif; ?>