<?php

use backend\models\Bank;
use common\components\widgets\BikTypeahead;
use common\components\widgets\AddressTypeahead;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\dictionary\address\AddressDictionary;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\company\CompanyType;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model Company */
/* @var $account CheckingAccountant */

$this->title = 'Заполните реквизиты';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-click',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$taxation = $model->companyTaxationType;
$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');

$this->registerJsFile( '@web/scripts/jquery.autocomplete.js', ['depends' => 'yii\web\JqueryAsset'] );

$view = $model->company_type_id == CompanyType::TYPE_IP ?
    Company::$companyFormView[CompanyType::TYPE_IP] :
    Company::$companyFormView[CompanyType::TYPE_OOO];
?>

<?= $this->render('_style') ?>
    <div class="row">
        <div class="col-xs-12 step-by-step">
            <?= $this->render('_steps', ['step' => 2, 'company' => $model]) ?>
                <div class="col-xs-12 col-lg-12 pad0">
                    <?php $form = ActiveForm::begin([
                        'id' => 'company-update-form',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => true,
                        'fieldClass' => 'common\components\floatLabelField\FloatLabelActiveField',
                        'fieldConfig' => [
                            'template' => "{input}\n{label}\n{error}\n",
                        ],
                        'options' => [
                            'class' => 'form-md-underline'
                        ],
                    ]); ?>
                    <?= $form->errorSummary($model); ?>
                    <!-- step 1 -->
                    <div class="col-xs-12 pad0 step-by-step-form">

                        <div class="row">

                            <div class="col-xs-12">
                                <div class="col-xs-12 main-caption bord-light-b pad0">
                                    <div>Шаг 1: Заполните данные по вашему <?= $model->companyType->name_short ?></div>
                                </div>
                            </div>

                            <div class="col-xs-12 caption">
                                <span>Реквизиты <?= $model->companyType->name_short ?></span>
                            </div>

                            <?= $this->render("parts_company/{$view}", [
                                'form' => $form,
                                'model' => $model,
                            ]); ?>

                            <?= $this->render('parts_company/_additional_accounts', [
                                'form' => $form,
                                'model' => $model,
                                'accounts' => $accounts,
                            ]) ?>

                            <div class="col-xs-12 caption">
                                <span>Система налогообложения</span>
                            </div>

                            <div class="col-xs-12 mb5 taxation-system">
                                <div class="row">
                                    <div class="col-xs-12 col-md-12">
                                        <?= $form->field($taxation, 'osno', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => (boolean) $taxation->usn]) ?>
                                        <?= $form->field($taxation, 'usn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => (boolean) $taxation->osno]) ?>
                                        <?= $form->field($taxation, 'envd', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox([]) ?>
                                        <?= $form->field($taxation, 'psn', [
                                            'template' => "{label}\n{input}",
                                            'options' => [
                                                'class' => 'form-group form-md-line-input form-md-floating-label',
                                                'style' => 'display: inline-block; margin-right:25px;',
                                            ],
                                            'labelOptions' => [
                                                'class' => 'control-label',
                                            ]
                                        ])->checkbox(['disabled' => $model->company_type_id != CompanyType::TYPE_IP]) ?>

                                        <div class="tax-usn-config collapse <?=($taxation->usn ? ' in' : '')?>">
                                            <?= $form->field($taxation, 'usn_type', [
                                                'options' => [
                                                    'class' => '',
                                                    'style' => 'max-width: 300px',
                                                ],
                                            ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                                                'class' => 'md-radio-list',
                                                'style' => 'margin-top: 10px;display: inline-block;',
                                                'item' => function ($index, $label, $name, $checked, $value) use ($taxation) {
                                                    if ($index == 0) $label = 'Доходы';
                                                    if ($index == 1) $label = 'Доходы минус Расходы';
                                                    $item  = Html::beginTag('div', ['class'=>'md-radio']);
                                                    $item .= Html::radio($name, $checked, ['id' => 'usn_radio_'.$index, 'class' => 'md-radiobtn', 'style' => 'display: inline-block;', 'value' => $value]);
                                                    $item .= Html::tag('label', '<span class="inc"></span><span class="check"></span><span class="box"></span>'. $label, ['for' => 'usn_radio_'.$index, 'style' => 'width:190px']);
                                                    $item .= Html::textInput('tmp_usn_percent_'.$index,
                                                            ($checked && $taxation->usn_percent) ? $taxation->usn_percent : (($index == 1) ? 15 : 6), [
                                                                'class' => 'form-control',
                                                                'style' => 'display: inline-block; width: 50px; margin-left: 30px;text-align:right;padding-right:5px;',
                                                            ]) . ' %';
                                                    $item .= Html::endTag('div');

                                                    return $item;
                                                },
                                            ])->label(false) . "\n";
                                            ?>
                                        </div>
                                        <div class="nds-view-osno collapse <?= $taxation->osno ? ' in' : ''; ?>">
                                            <?= $form->field($model, 'nds', [
                                                'options' => [
                                                    'class' => 'form-group',
                                                ],
                                            ])->radioList($ndsViewOsno, [
                                                'class' => 'inp_one_line_company',
                                                'item' => function ($index, $label, $name, $checked, $value) {
                                                    return Html::tag('label',
                                                            Html::radio($name, $checked, ['value' => $value]) . $label,
                                                            [
                                                                'class' => 'radio-inline p-o radio-padding',
                                                            ]) . Html::a('Пример счета', [Url::to('/company/view-example-invoice'), 'nds' => $value == 1 ? true : false], [
                                                            'class' => 'radio-inline p-o radio-padding',
                                                            'target' => '_blank',
                                                        ]);
                                                },
                                            ])->label(false);?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div id="buttons-bar-fixed">
                                <div class="col-xs-12 pad0 buttons-block">
                                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                                        'data-style' => 'expand-right',
                                        'style' => 'width: 130px!important; color: #fff;',
                                    ]); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style="clear:both"></div>

                    <?php $form->end(); ?>
                </div>
        </div>
    </div>

<?= $this->render('parts_company/_company_inn_api') ?>


<?php
Modal::begin([
    'id' => 'modal-companyImages',
    'header' => '<h1>Загрузить логотип, печать и подпись</h1>',
    'toggleButton' => false,
    'options' => [
        'data' => [
            'backdrop' => 'static',
            'keyboard' => 'false',
        ],
    ],
]);

echo $this->render('//company/form/_partial_files_tabs', [
    'model' => $model,
]);

Modal::end();
?>

<div class="row" style="margin-top:15px; margin-bottom:50px;">
    <div class="col-xs-12">
        <?= Html::button('<i class="glyphicon glyphicon-plus-sign"></i> Добавить логотип, печать и подпись', [
            'class' => 'btn darkblue darkblue-invert',
            'data-toggle' => 'modal',
            'data-target' => '#modal-companyImages',
            'style' => 'padding-right: 20px;',
            'onclick' => '$("#company-image-tabs a[href="#company-image-tabs-tab2"]").tab("show");',
        ]) ?>
    </div>
</div>

<?php
$this->registerJs(<<<JS
    $('.dictionary-bik').devbridgeAutocomplete({
        serviceUrl: function() {
            return $(this).data('url');
        },
        minLength: 1,
        paramName: 'q',
        dataType: 'json',
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.',
        transformResult: function (response) {
            return {
                suggestions: $.map(response, function (item, key) {
                    item.value = item.name;
                    return item;
                }),
            };
        },
        onSelect: function (suggestion) {
            $(this).val(suggestion.bik);
            $($(this).data('target-name')).val(suggestion.name);
            $($(this).data('target-city')).val(suggestion.city);
            $($(this).data('target-ks')).val(suggestion.ks);
            $($(this).data('target-collapse')).collapse('show');
            $('#add-new-account').prop('disabled', false);
        }
    });

    window.isB2B = true;

    $('#company-has_chief_patronymic').change(function() {
       if ($(this).prop('checked'))
           $('#company-ip_patronymic').val('').removeClass('edited').parent().removeClass('has-error');
    });

    // Система налогообложения
    $(document).ready(function() {
        if (0 === $(".nds-view-osno").find('input[type="radio"]:checked').length)
            $(".nds-view-osno").find('input[type="radio"]').first().prop('checked', 'checked').uniform();
    });

    function resetDisabledCheckboxes() {
        $('.taxation-system').find('.form-group').removeClass('has-success').removeClass('has-error');
    }

    $(document).on("change", "#companytaxationtype-osno", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true);

            $(".nds-view-osno", form).collapse("show");
        } else {
            $("#companytaxationtype-usn", form).prop("disabled", false);
            $(".nds-view-osno", form).collapse("hide");
            $(".nds-view-osno input[type=radio]", form).prop("checked", false);
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-usn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true);
            $(".tax-usn-config", form).collapse("show");
        } else {
            $("#companytaxationtype-osno", form).prop("disabled", false);
            $(".tax-usn-config", form).collapse("hide");
            $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true);
            $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false);
            $("#companytaxationtype-usn_percent", form).val("");
        }
        resetDisabledCheckboxes();
    });
    $(document).on("change", "#companytaxationtype-psn", function() {
        var form = this.form;
        if (this.checked) {
            $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true);
            $(".tax-patent-config", form).collapse("show");
        } else {
            $("#companytaxationtype-envd", form).prop("disabled", false);
            $(".tax-patent-config", form).collapse("hide");
        }
        resetDisabledCheckboxes();
    });

JS
);

// FILES UPLOAD
$this->registerJS(<<<JS
$('#company-logoimage').change(function () {
        var preview = document.querySelector('div[id=company_logo] img');
        var file    = document.querySelector('input[id=company-logoimage]').files[0];
        previewFile(preview, file);
});
$('#company-printimage').change(function () {
        var preview = document.querySelector('div[id=company_print] img');
        var file    = document.querySelector('input[id=company-printimage]').files[0];
        previewFile(preview, file);
});
$('#company-chiefsignatureimage').change(function () {
        var preview = document.querySelector('div[id=company_chief] img');
        var file    = document.querySelector('input[id=company-chiefsignatureimage]').files[0];
        previewFile(preview, file);
});
function previewFile(preview, file) {
    var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
}
JS
);