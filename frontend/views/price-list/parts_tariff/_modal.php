<div class="modal fade" id="info-after-pay" tabindex="-1" role="modal" aria-hidden="true" style="border: none;">
    <div class="modal-dialog" style="width: 350px !important;margin: 0 auto;">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none; padding: 20px 10px 0px 0px">
                <button type="button" class="close" data-dismiss="modal" style="height: 20px;width: 20px;"
                        aria-hidden="true"></button>
            </div>
            <div class="modal-body" style="padding: 0px 35px 20px 35px;font-size: 1.3em;text-align: center;margin: 0 auto;line-height: 1.8;">
                <div class="text-center pad5">
                    <img style="height: 4em;" src="/img/service/ok-2.1.png"> <br>
                    <div>
                        <p>
                            Счет отправлен на
                            <br>
                            <strong id="send-to-email"></strong>.
                        </p>
                        <p>
                            Если письмо не придет в течении 5 минут - ищите в спаме или напишите на
                            <br>
                            <span>support@kub-24.ru</span>
                        </p>
                    </div>

                    <a class="btn" data-dismiss="modal" style="background-color: #45B6AF; color: #fff;"> ОК </a>
                </div>
            </div>
        </div>
    </div>
</div>