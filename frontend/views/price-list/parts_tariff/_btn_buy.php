<?php

use common\models\service\PaymentType;
use yii\helpers\Html;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;

/** @var $tariff SubscribeTariff */
/** @var $tariffGroup SubscribeTariffGroup */

?>

<?php if ($tariff->price == 0): ?>
    <?= Html::a('ПОПРОБОВАТЬ', ['/price-list/step-company'], [
        'class' => 'btn tariff darkblue-invert',
    ]) ?>
<?php else: ?>
    <?= Html::button('ОПЛАТИТЬ', [
        'class' => 'btn tariff tooltip2-pay darkblue text-white',
        'data-tooltip-content' => '#tooltip_pay_' . $tariff->id,
    ]) ?>
<?php endif; ?>


<div class="hidden">
    <div id="tooltip_pay_<?= $tariff->id ?>" class="tooltip_pay_type" style="display: inline-block;">
        <div class="pay-tariff-wrap">
            <strong>
                Тариф:
            </strong>
            "<span class="pay-tariff-name"><?= $tariffGroup->name ?></span>"
        </div>
        <div class="pay-tariff-wrap">
            <strong>
                Период:
            </strong>
            <span class="pay-tariff-period"><?= $tariff->getReadableDuration() ?></span>
        </div>
        <div class="pay-tariff-wrap">
            <strong>
                Количество прайс-листов:
            </strong>
            <span class="pay-tariff-period"><?= $tariff->tariff_limit ?></span>
        </div>
        <div style="font-size: 16px; margin: 10px 0;  text-align: center;">
            Способ оплаты
        </div>
        <div style="display: flex; height: 57px;">
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'id' => 'tariff-pay-form-' . $tariff->id . '-' . PaymentType::TYPE_ONLINE,
                'class' => 'tariff-group-payment-form',
                'style' => 'margin-right: 1px;'
            ]) ?>
            <?= Html::activeHiddenInput($model, 'tariffId', [
                'value' => $tariff->id,
            ]) ?>
            <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                'value' => PaymentType::TYPE_ONLINE,
            ]) ?>
            <?= Html::submitButton('Картой', [
                'class' => 'btn darkblue text-white ladda-button',
                'data-style' => 'expand-down',
                'style' => 'width: 140px; height: auto; background: #45b6af;',
            ]) ?>
            <div class="hidden submit-response-content"></div>
            <?= Html::endForm() ?>
            <?= Html::beginForm(['/subscribe/default/payment'], 'post', [
                'id' => 'tariff-pay-form-' . $tariff->id . '-' . PaymentType::TYPE_INVOICE,
                'class' => 'tariff-group-payment-form',
                'style' => 'margin-left: 1px;'
            ]) ?>
            <?= Html::activeHiddenInput($model, 'tariffId', [
                'value' => $tariff->id,
            ]) ?>
            <?= Html::activeHiddenInput($model, 'paymentTypeId', [
                'value' => PaymentType::TYPE_INVOICE,
            ]) ?>
            <?= Html::submitButton('Выставить счет', [
                'class' => 'btn darkblue text-white ladda-button',
                'data-style' => 'expand-down',
                'style' => 'width: 140px; height: auto; background: #ffaa24;',
            ]) ?>
            <div class="hidden submit-response-content"></div>
            <?= Html::endForm() ?>
        </div>
    </div>
</div>
