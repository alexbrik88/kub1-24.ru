<?php

use common\models\employee\Employee;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$employeeArray = $model->company->getEmployeeCompanies()->andWhere([
    'or',
    ['is_working' => true],
    ['employee_id' => $model->responsible_employee_id],
])->orderBy([
    'lastname' => SORT_ASC,
    'firstname' => SORT_ASC,
    'patronymic' => SORT_ASC,
])->all();
$accessibleList = ['all' => 'Всем'] + ArrayHelper::map($employeeArray, 'employee_id', 'fio')
?>

<?php if (Yii::$app->request->isAjax && $model->saved) : ?>
    <?= Html::script('
        $.pjax.reload("#cashbox-pjax-container", {timeout: 5000});
        $(".modal.in").modal("hide");
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>

<div class="cashbox-form">
    <?php $form = ActiveForm::begin([
        'id' => 'cashbox-form',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => '',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->is_main) : ?>
        <?= $form->field($model, 'accessible')->dropDownList($accessibleList, [
            'prompt' => '',
        ]) ?>
    <?php endif ?>

    <?= $form->field($model, 'is_accounting', [
        'labelOptions' => [
            'class' => 'col-sm-4',
        ],
    ])->checkbox([], false) ?>

    <?php if (!$model->isNewRecord && !$model->is_main) : ?>
        <?= $form->field($model, 'is_closed', [
            'labelOptions' => [
                'class' => 'col-sm-4',
            ],
        ])->checkbox([], false) ?>
    <?php endif ?>

    <?php if ($model->isNewRecord) : ?>
        <?= $form->field($model, 'createStartBalance', [
            'labelOptions' => [
                'class' => 'col-sm-4',
            ],
        ])->checkbox([], false) ?>

        <div class="row start-balance-block <?= !$model->createStartBalance ? 'hidden' : null; ?>">
            <?= $form->field($model, 'startBalanceAmount', [
                'options' => [
                    'class' => 'form-group col-md-8',
                    'style' => 'width: 70%',
                ],
                'labelOptions' => [
                    'class' => 'control-label col-sm-6',
                    'style' => 'width: 50.845%;',
                ],
                'wrapperOptions' => [
                    'class' => 'col-sm-6',
                    'style' => 'width: 49.16%;',
                ],
            ])->textInput(['class' => 'form-control js_input_to_money_format']) ?>

            <?= $form->field($model, 'startBalanceDate', [
                'options' => [
                    'class' => 'form-group col-md-4 p-l-0 p-r-0',
                ],
                'labelOptions' => [
                    'class' => 'control-label col-sm-5',
                ],
                'wrapperOptions' => [
                    'class' => 'col-sm-7 p-l-0 p-r-0',
                ],
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>'
            ])->textInput([
                'class' => 'form-control date-picker',
                'style' => 'width: 100%; max-width: 125px;',
                'data' => [
                    'date-viewmode' => 'years',
                ],
            ]) ?>
        </div>
    <?php endif ?>


    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue text-white ladda-button',
                'data-style' => 'expand-right',
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white pull-right',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Html::script('
$("#cashbox-form input:checkbox:not(.md-check)").uniform();

$("#cashbox-startbalancedate").datepicker({
    language: "ru",
    autoclose: true,
}).on("change.dp", dateChanged);

function dateChanged(ev) {
    if (ev.bubbles == undefined) {
        var $input = $("#" + ev.currentTarget.id);
        if (ev.currentTarget.value == "") {
            if ($input.data("last-value") == null) {
                $input.data("last-value", ev.currentTarget.defaultValue);
            }
            var $lastDate = $input.data("last-value");
            $input.datepicker("setDate", $lastDate);
        } else {
            $input.data("last-value", ev.currentTarget.value);
        }
    }
}

$("#cashbox-createstartbalance").on("change", function (e) {
    $(".start-balance-block").toggleClass("hidden");
});
', [
    'type' => 'text/javascript',
]); ?>
