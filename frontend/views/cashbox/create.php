<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\cash\Cashbox */

$this->title = 'Добавить кассу';
$this->params['breadcrumbs'][] = ['label' => 'Кассы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
