<?php

use yii\helpers\Html;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\company\CompanySite */

$this->title = 'Добавить ваш сайт';
?>
<div class="company-site-create">
    <?= Html::a('Назад в настройки', ['/contractor/sale-increase', 'activeTab' => Contractor::TAB_SETTINGS]) ?>
    <div class="portlet box">
        <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
