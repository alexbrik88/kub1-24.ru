<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\company\CompanySite */

$this->title = 'Форма входа для сайта';
?>
<div class="company-site-view">

    <?= Html::a('Назад в настройки', ['/contractor/sale-increase', 'activeTab' => Contractor::TAB_SETTINGS]) ?>

    <div class="portlet box">
        <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
    </div>

    <div style="max-width: 600px;">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'url:url',
                'created_at:date',
            ],
        ]) ?>

    </div>

    <div style="margin-top: 30px;">
        <h3>Код для вставки на сайт формы входа в "Личный кабинет покупателя"</h3>
        <pre>
<?= Html::encode($this->render('_store_form')) ?>
        </pre>
    </div>
    <div>
        Скопируйте и установите этот код в HTML-верстку вашего сайта в том месте, где хотите вывести форму входа в личный кабинет покупателя.
    </div>

    <div class="row" style="margin-top: 30px;">
        <div class="col-sm-12">
            <?= Html::a('Изменить', ['update', 'id' => $model->id], [
                'class' => 'btn darkblue text-white',
                'style' => 'width: 140px;',
            ]) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn darkblue text-white pull-right',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
                'style' => 'width: 140px;',
            ]) ?>
        </div>
    </div>
</div>
