<?php

$storeUrl = trim(Yii::$app->params['serviceSiteStore'], '/');

?>
<!--Разместите перед закрывающим тегом </head>. Если библиотека jQuery на странице сайта уже подключена, эту строку кода можно не копировать.-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!--Разместите код в том месте, где хотите вывести форму-->
<div id="store-login-form-container" style="display: none;"></div>
<script>(function (w, d, s) {
    var c = d.getElementById('store-login-form-container'),
        j = d.createElement(s);
    j.async = true;
    j.src = '<?= $storeUrl ?>/js/login-form.min.js?v=' + new Date().getTime();
    c.parentNode.insertBefore(j, c);
})(window, document, 'script');</script>