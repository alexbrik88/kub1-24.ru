<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\company\CompanySite */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="company-site-form" style="max-width: 600px; margin-top: 30px;">

    <?php $form = ActiveForm::begin([
        'id' => 'company-site-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => '',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <div class="row" style="margin-top: 50px;">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span>', [
                'class' => 'btn darkblue ladda-button',
                'data-style' => 'expand-right',
                'style' => 'color: #fff; width: 140px;'
            ]) ?>
            <?= Html::a('Отменить', ['/contractor/sale-increase', 'activeTab' => Contractor::TAB_SETTINGS], [
                'class' => 'btn darkblue pull-right',
                'style' => 'color: #fff; width: 140px;',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
