<?php

use common\components\grid\GridView;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\CompanySiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $header string */

?>

<div class="company-site-index">
    <?= Alert::widget([
        'alertTypes' => [
            'site-error' => 'alert-danger',
            'site-danger' => 'alert-danger',
            'site-success' => 'alert-success',
            'site-info' => 'alert-info',
            'site-warning' => 'alert-warning'
        ],
    ]); ?>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-8">
            <?php if (isset($header)): ?>
                <?= $header; ?>
            <?php else: ?>
                <div style="font-size: 23px;">Форма входа для сайта</div>
            <?php endif; ?>
        </div>
        <div class="col-sm-4">
            <?= Html::a('Добавить сайт', ['/company-site/create'], ['class' => 'btn yellow pull-right']) ?>
        </div>
    </div>

    <?php $pjax = Pjax::begin([
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'url:url',
            'created_at:date',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'company-site',
                'template' => '{view} {update} {delete}',
                'headerOptions' => [
                    'width' => '60',
                ],
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-eye-open'></span>", $url, [
                            'title' => 'Просмотр',
                            'aria-label' => 'Просмотр',
                            'data-pjax' => '0',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a("<span class='glyphicon glyphicon-pencil'></span>", $url, [
                            'title' => 'Изменить',
                            'aria-label' => 'Изменить',
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return \frontend\widgets\ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span class="glyphicon glyphicon-trash"></span>',
                                'title' => 'Удалить',
                                'aria-label' => 'Удалить',
                                'data-pjax' => '0',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите удалить сайт?',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php $pjax->end(); ?>
</div>
