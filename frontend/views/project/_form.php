<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\date\DateHelper;
use kartik\select2\Select2;
use common\models\Contractor;
use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use common\models\document\InvoiceExpenditureItem;
use common\models\EmployeeCompany;
use common\models\project\Project;

/* @var $this yii\web\View */
/* @var $model common\models\project\Project */
/* @var $form yii\widgets\ActiveForm */
/* @var $user Employee */
/* @var string $backUrl */

$user = Yii::$app->user->identity;
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'project-form',
    'options' => [
        'enctype' => 'multipart/form-data',
    ],
    'enableClientValidation' => false,
])); ?>
    <div class="form-body form-body_sml form-body-project">
        <?= $form->errorSummary($model); ?>
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption" style="width: 100%">
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td valign="middle" style="width:1%; white-space:nowrap;">
                                <span class="invoice-block">
                                    Проект №
                                    <?= Html::activeTextInput($model, 'project_number', [
                                        'id' => 'project-number',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                    ]); ?>
                                    <br class="box-br">
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-xs-12 pad0 col-lg-6 col-md-12">
                        <div class="col-lg-12" style="max-width: 590px">
                            <?= $form->field($model, 'name', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->label('Название проекта <span class="required" aria-required="true">*</span>:'); ?>

                            <?= $form->field($model, 'start_date', [
                                'wrapperOptions' => [
                                    'class' => 'col-md-4 inp_one_line',
                                ],
                                'template' => Yii::$app->params['formDatePickerTemplate'],
                            ])->textInput([
                                'class' => 'form-control date-picker min_wid_picker',
                                'value' => DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label('Начало <span class="required" aria-required="true">*</span>:'); ?>

                            <?= $form->field($model, 'end_date', [
                                'wrapperOptions' => [
                                    'class' => 'col-md-4 inp_one_line',
                                ],
                                'template' => Yii::$app->params['formDatePickerTemplate'],
                            ])->textInput([
                                'class' => 'form-control date-picker min_wid_picker',
                                'value' => DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ])->label('Окончание:'); ?>

                            <?= $form->field($model, 'responsible_employee_id', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->dropDownList(ArrayHelper::map(Employee::find()
                                ->joinWith('employeeCompany')
                                ->andWhere(['and',
                                    [EmployeeCompany::tableName() . '.company_id' => $user->company_id],
                                    [EmployeeCompany::tableName() . '.is_working' => Employee::STATUS_IS_WORKING],
                                ])
                                ->byIsActive(Employee::ACTIVE)
                                ->byIsDeleted(Employee::NOT_DELETED)
                                ->orderBy([
                                    'lastname' => SORT_ASC,
                                    'firstname' => SORT_ASC,
                                    'patronymic' => SORT_ASC,
                                ])
                                ->all(), 'id', 'shortFio'))->label('Ответственный <span class="required" aria-required="true">*</span>:'); ?>

                            <?= $form->field($model, 'clients', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}
                                \n{beginWrapper}
                                \n{input}
                                \n{error}
                                <ul class='select2-selection__rendered selected-items-list' data-input_id='project-clients'>" .
                                    $model->getProjectCompaniesList() . "</ul>
                                \n{hint}
                                \n{endWrapper}",
                            ])->widget(Select2::classname(), [
                                'data' => Project::getALLContractorList(Contractor::TYPE_CUSTOMER),
                                'showToggleAll' => false,
                                'options' => [
                                    'class' => 'form-control project-multiple-select',
                                    'multiple' => 'multiple',
                                ],
                            ])->label('Клиент <span class="required" aria-required="true">*</span>:'); ?>

                            <?= $form->field($model, 'contractors', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}
                                \n{beginWrapper}
                                \n{input}
                                \n{error}
                                <ul class='select2-selection__rendered selected-items-list' data-input_id='project-contractors'>" .
                                    $model->getProjectContractorList() . "</ul>
                                \n{hint}
                                \n{endWrapper}",
                            ])->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Contractor::getSorted()
                                    ->andWhere(['type' => Contractor::TYPE_SELLER])
                                    ->byCompany($user->company_id)
                                    ->byDeleted()
                                    ->all(), 'id', 'nameWithType'),
                                'showToggleAll' => false,
                                'options' => [
                                    'class' => 'form-control project-multiple-select',
                                    'multiple' => 'multiple',
                                ]
                            ])->label('Поставщик:'); ?>

                            <?= $form->field($model, 'expenseItems', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}
                                \n{beginWrapper}
                                \n{input}
                                \n{error}
                                <ul class='select2-selection__rendered selected-items-list' data-input_id='project-expenseitems'>" .
                                    $model->getProjectExpenseItemList() . "
                                </ul>
                                \n{hint}
                                \n{endWrapper}",
                            ])->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                                    ->andWhere(['or',
                                        ['company_id' => null],
                                        ['company_id' => $user->company_id],
                                    ])
                                    ->orderBy('name')
                                    ->all(), 'id', 'name'),
                                'showToggleAll' => false,
                                'options' => [
                                    'class' => 'form-control project-multiple-select',
                                    'multiple' => 'multiple',
                                ]
                            ])->label('Статьи расходов:'); ?>

                            <?= $form->field($model, 'additional_expenses_month', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->label('Доп. затраты (ежемесячные):'); ?>

                            <?= $form->field($model, 'additional_expenses', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-8 inp_one_line-product',
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->label('Доп. затраты (разовые):'); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 pad0 col-lg-6 col-md-12">
                        <div class="col-lg-12" style="max-width: 590px">
                            <?= $form->field($model, 'description', [
                                'labelOptions' => [
                                    'class' => 'col-md-4 control-label bold-text',
                                ],
                                'wrapperOptions' => [
                                    'class' => 'col-md-12 inp_one_line-product' . ($model->description ? '' : ' hidden'),
                                ],
                                'hintOptions' => [
                                    'tag' => 'small',
                                    'class' => 'text-muted',
                                ],
                                'horizontalCssClasses' => [
                                    'offset' => 'col-md-offset-4',
                                    'hint' => 'col-md-8',
                                ],
                                'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
                            ])->textarea(['rows' => 6])->label('Описание: <span id="description_project_update" class="glyphicon glyphicon-pencil" style="cursor: pointer;"></span>'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg create-invoice',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="spinner-button col-sm-1 col-xs-1">
                    <?= Html::a('Отменить', $backUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                    ]); ?>
                    <?= Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $backUrl, [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'title' => 'Отменить',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<div class="modal fade t-p-f modal_scroll_center" id="add-new" tabindex="-1" role="modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>
