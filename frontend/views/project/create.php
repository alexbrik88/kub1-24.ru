<?php

/* @var $this yii\web\View */
/* @var $model common\models\project\Project */
/* @var string $backUrl */

$this->title = 'Создать Проект';
?>
<div class="project-create">
    <?= $this->render('_form', [
        'model' => $model,
        'backUrl' => $backUrl,
    ]); ?>
</div>
