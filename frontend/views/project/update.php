<?php

/* @var $this yii\web\View */
/* @var $model common\models\project\Project */
/* @var string $backUrl */

$this->title = 'Обновление проекта: ' . $model->name;
?>
<div class="project-update">
    <?= $this->render('_form', [
        'model' => $model,
        'backUrl' => $backUrl,
    ]); ?>
</div>
