<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\TextHelper;
use kartik\checkbox\CheckboxX;
use yii\bootstrap\Dropdown;
use frontend\models\ProjectSearch;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $this yii\web\View */
/* @var $model common\models\project\Project */
/* @var $searchModel ProjectSearch */
/* @var $data [] */

$this->title = $model->name;

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="project-view">
    <div style="col-xs-12 pad3">
        <?= Html::a('Назад к списку', ['index'], [
            'class' => 'back-to-customers',
        ]); ?>
    </div>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-6 pad0">
            <div class="col-xs-12 pad3" style="max-width: 720px;">
                <div class="portlet">
                    <div class="customer-info" style="border: 1px solid #4276a4;">
                        <div class="portlet-title row" style="margin: 0;">
                            <div class="col-md-9 pad0 caption">
                                <?= $model->name; ?>
                            </div>
                            <div class="actions">
                                <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                                    'class' => 'btn darkblue btn-sm pull-right',
                                    'title' => 'Редактировать',
                                    'style' => 'height: 27px;margin-left: 3px;',
                                ]); ?>
                            </div>
                        </div>
                        <div class="portlet-body" style="padding: 10px 20px 0 10px;display: grid;">
                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">Номер:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= $model->project_number; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">Дата
                                    начала:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">Дата
                                    окончания:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15" style="margin-bottom: 10px;">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">Клиент:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?php foreach ($model->projectCompanies as $projectCompany): ?>
                                        <?= Html::a($projectCompany->contractor->nameWithType, Url::to(['/contractor/view',
                                            'id' => $projectCompany->contractor_id,
                                            'type' => $projectCompany->contractor->type]), [
                                                'style' => 'display:block;',
                                            ]
                                        ); ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15" style="margin-bottom: 10px;">
                                <label class="width-label-prod-r pad-top-7 bold"
                                       for="under-date">Поставщики:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?php foreach ($model->projectContractors as $projectContractor): ?>
                                        <?= Html::a($projectContractor->contractor->nameWithType, Url::to(['/contractor/view',
                                            'id' => $projectContractor->contractor_id,
                                            'type' => $projectContractor->contractor->type]), [
                                                'style' => 'display:block;',
                                            ]
                                        ); ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15" style="margin-bottom: 10px;">
                                <label class="width-label-prod-r pad-top-7 bold"
                                       for="under-date">Статьи затрат:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?php foreach ($model->projectExpenditureItems as $projectExpenditureItem): ?>
                                        <span style="display: block;">
                                            <?= $projectExpenditureItem->expenditureItem->name; ?>
                                        </span>
                                    <?php endforeach; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">
                                    Ежемес. затраты:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= $model->additional_expenses_month ?
                                        TextHelper::moneyFormat($model->additional_expenses_month) . ' руб.' :
                                        $model->additional_expenses_month; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">
                                    Разовые затраты:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= $model->additional_expenses ?
                                        TextHelper::moneyFormat($model->additional_expenses) . ' руб.' :
                                        $model->additional_expenses; ?>
                                </div>
                            </div>

                            <div class="form-group disp-block-prod-r pad-l-r-15 m-b-0">
                                <label class="width-label-prod-r pad-top-7 bold" for="under-date">
                                    Ответственный:</label>

                                <div class="col-md-8 pad-top-7" style="float:right;">
                                    <?= $model->responsibleEmployee->getShortFio(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6 pad0">
            <div class="col-xs-12 pad3" style="max-width: 720px;">
                <div class="portlet">
                    <div class="customer-info project-info">
                        <div class="portlet-title row pad0" style="margin: 0;">
                            <div class="col-md-9 pad0 caption" style="font-weight: normal!important;">
                                Ключевые показатели
                            </div>
                            <div class="col-md-3 pad0">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div style="position: relative;">
                                            <div class="btn default p-t-7 p-b-7" data-toggle="dropdown"
                                                 style="width: 100%">
                                                <?= $searchModel->periodName ?> <b class="fa fa-angle-down"></b>
                                            </div>
                                            <?= Dropdown::widget([
                                                'items' => $searchModel->periodItems,
                                                'options' => [
                                                    'class' => 'col-sm-12 col-xs-12',
                                                    'style' => 'min-width:1px;',
                                                ],
                                            ]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body" style="margin-top: 20px;">
                            <table
                                class="table table-striped table-bordered table-hover project-analysis-table overfl_text_hid">
                                <tbody>
                                <tr class="heading">
                                    <th style="background-color: #fff !important;" width="45%">По выставленным счетам
                                    </th>
                                    <th style="background-color: #fff !important;" width="20%">
                                        <?= $searchModel->getPeriodColumnsName()['previousPeriod']; ?>
                                    </th>
                                    <th style="background-color: #fff !important;" width="20%">
                                        <?= $searchModel->getPeriodColumnsName()['currentPeriod']; ?>
                                    </th>
                                    <th style="background-color: #fff !important;" width="15%">%%</th>
                                </tr>
                                <tr>
                                    <td>
                                        <?= CheckboxX::widget([
                                            'id' => 'income',
                                            'name' => 'income',
                                            'value' => true,
                                            'options' => [
                                                'class' => 'project-analyse-checkbox',
                                            ],
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'threeState' => false,
                                                'inline' => false,
                                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                            ],
                                        ]); ?>
                                        Выручка
                                        <span id="tooltip_remind_project-expense"
                                              class="tooltip2 ico-question valign-middle"
                                              data-tooltip-content="#tooltip_expense_project"></span>
                                    </td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($previousIncome = $data['previousPeriod']['totalIncome']); ?>
                                    </td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($currentIncome = $data['currentPeriod']['totalIncome']); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (!$previousIncome && !$currentIncome): ?>
                                            0.00
                                        <?php else: ?>
                                            <?php $incomePercent = $previousIncome ?
                                                (($currentIncome * 100 / $previousIncome) - 100) : 100;
                                            $color = null;
                                            $sign = null;
                                            if ($incomePercent > 0) {
                                                $color = '#00da00';
                                                $sign = '+';
                                            } elseif ($incomePercent < 0) {
                                                $color = 'red';
                                                $sign = '-';
                                            }
                                            $incomePercent = number_format((float)abs($incomePercent), 2, '.', ''); ?>
                                            <span style="color: <?= $color; ?>;">
                                                <?= $sign . $incomePercent; ?>
                                            </span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php if (isset($data['contractors']['income'])): ?>
                                    <?php foreach ($data['contractors']['income'] as $contractorID => $oneDate): ?>
                                        <tr class="income-item hidden">
                                            <td>
                                                <span
                                                    title="<?= htmlspecialchars($data['contractors']['income'][$contractorID]['name']); ?>"
                                                    style="padding-left: 40px;">
                                                    <?= $data['contractors']['income'][$contractorID]['name']; ?>
                                                </span>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($previousIncomeContractor = $data['contractors']['income'][$contractorID]['previousPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($currentIncomeContractor = $data['contractors']['income'][$contractorID]['currentPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php if (!$previousIncomeContractor && !$currentIncomeContractor): ?>
                                                    0.00
                                                <?php else: ?>
                                                    <?php $incomePercentCompany = $previousIncomeContractor ?
                                                        (($currentIncomeContractor * 100 / $previousIncomeContractor) - 100) : 100;
                                                    $color = null;
                                                    $sign = null;
                                                    if ($incomePercentCompany > 0) {
                                                        $color = '#00da00';
                                                        $sign = '+';
                                                    } elseif ($incomePercentCompany < 0) {
                                                        $color = 'red';
                                                        $sign = '-';
                                                    }
                                                    $incomePercentCompany = number_format((float)abs($incomePercentCompany), 2, '.', ''); ?>
                                                    <span style="color: <?= $color; ?>;">
                                                        <?= $sign . $incomePercentCompany; ?>
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <tr>
                                    <td>
                                        <?= CheckboxX::widget([
                                            'id' => 'expense',
                                            'name' => 'expense',
                                            'value' => true,
                                            'options' => [
                                                'class' => 'project-analyse-checkbox',
                                            ],
                                            'pluginOptions' => [
                                                'size' => 'xs',
                                                'threeState' => false,
                                                'inline' => false,
                                                'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                                'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                            ],
                                        ]); ?>
                                        Расход
                                        <span id="tooltip_remind_project-income"
                                              class="tooltip2 ico-question valign-middle"
                                              data-tooltip-content="#tooltip_income_project"></span>
                                    </td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($previousExpense = $data['previousPeriod']['totalExpense']); ?>
                                    </td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($currentExpense = $data['currentPeriod']['totalExpense']); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (!$previousExpense && !$currentExpense): ?>
                                            0.00
                                        <?php else: ?>
                                            <?php $expensePercent = $previousExpense ?
                                                (($currentExpense * 100 / $previousExpense) - 100) : 100;
                                            $color = null;
                                            $sign = null;
                                            if ($expensePercent > 0) {
                                                $color = '#00da00';
                                                $sign = '+';
                                            } elseif ($expensePercent < 0) {
                                                $color = 'red';
                                                $sign = '-';
                                            }
                                            $expensePercent = number_format((float)abs($expensePercent), 2, '.', ''); ?>
                                            <span style="color: <?= $color; ?>;">
                                                <?= $sign . $expensePercent; ?>
                                            </span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php if (isset($data['contractors']['expense'])): ?>
                                    <?php foreach ($data['contractors']['expense'] as $contractorID => $oneDate): ?>
                                        <tr class="expense-item hidden">
                                            <td>
                                                <span
                                                    title="<?= htmlspecialchars($data['contractors']['expense'][$contractorID]['name']); ?>"
                                                    style="padding-left: 40px;">
                                                    <?= $data['contractors']['expense'][$contractorID]['name']; ?>
                                                </span>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($previousIncomeContractor = $data['contractors']['expense'][$contractorID]['previousPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($currentIncomeContractor = $data['contractors']['expense'][$contractorID]['currentPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php if (!$previousIncomeContractor && !$currentIncomeContractor): ?>
                                                    0.00
                                                <?php else: ?>
                                                    <?php $incomePercentCompany = $previousIncomeContractor ?
                                                        (($currentIncomeContractor * 100 / $previousIncomeContractor) - 100) : 100;
                                                    $color = null;
                                                    $sign = null;
                                                    if ($incomePercentCompany > 0) {
                                                        $color = '#00da00';
                                                        $sign = '+';
                                                    } elseif ($incomePercentCompany < 0) {
                                                        $color = 'red';
                                                        $sign = '-';
                                                    }
                                                    $incomePercentCompany = number_format((float)abs($incomePercentCompany), 2, '.', ''); ?>
                                                    <span style="color: <?= $color; ?>;">
                                                            <?= $sign . $incomePercentCompany; ?>
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <?php if (isset($data['expenseItems'])): ?>
                                    <?php foreach ($data['expenseItems'] as $expenseItemID => $oneData): ?>
                                        <tr class="expense-item hidden">
                                            <td>
                                                <span
                                                    title="<?= htmlspecialchars($data['expenseItems'][$expenseItemID]['name']); ?>"
                                                    style="padding-left: 40px;">
                                                    <?= $data['expenseItems'][$expenseItemID]['name']; ?>
                                                </span>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($previousIncomeExpenseItem = $data['expenseItems'][$expenseItemID]['previousPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?= TextHelper::invoiceMoneyFormat($currentIncomeExpenseItem = $data['expenseItems'][$expenseItemID]['currentPeriod']['sum']); ?>
                                            </td>
                                            <td class="text-right">
                                                <?php if (!$previousIncomeExpenseItem && !$currentIncomeExpenseItem): ?>
                                                    0.00
                                                <?php else: ?>
                                                    <?php $incomePercentExpenseItem = $previousIncomeExpenseItem ?
                                                        (($currentIncomeExpenseItem * 100 / $previousIncomeExpenseItem) - 100) : 100;
                                                    $color = null;
                                                    $sign = null;
                                                    if ($incomePercentExpenseItem > 0) {
                                                        $color = '#00da00';
                                                        $sign = '+';
                                                    } elseif ($incomePercentExpenseItem < 0) {
                                                        $color = 'red';
                                                        $sign = '-';
                                                    }
                                                    $incomePercentExpenseItem = number_format((float)abs($incomePercentExpenseItem), 2, '.', ''); ?>
                                                    <span style="color: <?= $color; ?>;">
                                                        <?= $sign . $incomePercentExpenseItem; ?>
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <tr class="expense-item hidden">
                                    <td>
                                        <span style="padding-left: 40px;" title="Доп. затраты (ежемесячные)">
                                            Доп. затраты (ежемесячные)
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <?php if (isset($data['additionalExpenses']['previousPeriod'])): ?>
                                            <?= TextHelper::invoiceMoneyFormat($data['additionalExpenses']['previousPeriod']['sum'] * 100); ?>
                                        <?php else: ?>
                                            0.00
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (isset($data['additionalExpenses']['currentPeriod'])): ?>
                                            <?= TextHelper::invoiceMoneyFormat($data['additionalExpenses']['currentPeriod']['sum'] * 100); ?>
                                        <?php else: ?>
                                            0.00
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-right">
                                        0.00
                                    </td>
                                </tr>
                                <tr class="expense-item hidden">
                                    <td>
                                        <span style="padding-left: 40px;" title="Доп. затраты (ежемесячные)">
                                            Доп. затраты (разовые)
                                        </span>
                                    </td>
                                    <td class="text-right">
                                        <?php if (isset($data['additionalExpensesOne']['previousPeriod'])): ?>
                                            <?= TextHelper::invoiceMoneyFormat($previousOneExpense = $data['additionalExpensesOne']['previousPeriod']['sum'] * 100); ?>
                                        <?php else: ?>
                                            0.00
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (isset($data['additionalExpensesOne']['currentPeriod'])): ?>
                                            <?= TextHelper::invoiceMoneyFormat($currentOneExpense = $data['additionalExpensesOne']['currentPeriod']['sum'] * 100); ?>
                                        <?php else: ?>
                                            0.00
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (isset($currentOneExpense)): ?>
                                            <span style="color: #00da00;">
                                                +100.00
                                            </span>
                                        <?php elseif (isset($previousOneExpense)): ?>
                                            <span style="color: red;">
                                                -100.00
                                            </span>
                                        <?php else: ?>
                                            0.00
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Результат</td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($previousResult = $previousIncome - $previousExpense); ?>
                                    </td>
                                    <td class="text-right">
                                        <?= TextHelper::invoiceMoneyFormat($currentResult = $currentIncome - $currentExpense); ?>
                                    </td>
                                    <td class="text-right">
                                        <?php if (!$previousResult && !$currentResult): ?>
                                            0.00
                                        <?php else: ?>
                                            <?php $resultPercent = $previousResult ?
                                                (($currentResult * 100 / $previousResult) - 100) : 100;
                                            $color = null;
                                            $sign = null;
                                            if ($resultPercent > 0) {
                                                $color = '#00da00';
                                                $sign = '+';
                                            } elseif ($resultPercent < 0) {
                                                $color = 'red';
                                                $sign = '-';
                                            }
                                            $resultPercent = number_format((float)abs($resultPercent), 2, '.', '')?>
                                            <span style="color: <?= $color; ?>;">
                                                <?= $sign . $resultPercent; ?>
                                            </span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Маржа %</td>
                                    <td class="text-right">
                                        <?= $previousIncome ? round(($previousResult / $previousIncome * 100), 2) : 0; ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $currentIncome ? round(($currentResult / $currentIncome * 100), 2) : 0; ?>
                                    </td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="tooltip-template" style="display: none;">
    <span id="tooltip_expense_project" style="display: inline-block; text-align: center;">
            Отображает сумму счетов, выставленных по данному проекту, за период.
    </span>
    <span id="tooltip_income_project" style="display: inline-block; text-align: center;">
            Отображает сумму оплат, сделанных по данному проекту, за период.
    </span>
</div>