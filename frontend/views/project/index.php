<?php

use yii\bootstrap\Html;
use frontend\widgets\RangeButtonWidget;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use frontend\models\ProjectSearch;
use common\components\grid\GridView;
use common\models\project\Project;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\components\grid\DropDownSearchDataColumn;
use frontend\widgets\TableConfigWidget;
use common\models\employee\Config;
use common\components\TextHelper;
use common\models\employee\Employee;
use common\models\EmployeeCompany;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel ProjectSearch */
/* @var $userConfig Config */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> ДОБАВИТЬ', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>

    <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
</div>
<div class="row" id="widgets">
    <div class="col-md-9 col-sm-9"></div>
    <div class="col-md-3 col-sm-3">
        <div class="dashboard-stat" style="position: relative;">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="table-icons" style="margin-top: -20px;">
            <?= TableConfigWidget::widget([
                'items' => [
                    [
                        'attribute' => 'project_end_date',
                    ],
                    [
                        'attribute' => 'project_clients',
                    ],
                    [
                        'attribute' => 'project_contractor',
                    ],
                    [
                        'attribute' => 'project_expense',
                    ],
                    [
                        'attribute' => 'project_result',
                    ],
                    [
                        'attribute' => 'project_responsible_employee_id',
                    ],
                ],
            ]); ?>
            <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                'class' => 'get-xls-link pull-right',
                'title' => 'Скачать в Excel',
            ]); ?>
        </div>
    </div>
</div>
<div class="portlet box darkblue" id="invoices-table">
    <div class="portlet-title">
        <div class="caption">
            Список проектов
        </div>
        <div class="tools search-tools tools_button col-md-6 col-sm-6">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input">
                        <?= $form->field($searchModel, 'searchProject')->textInput([
                            'placeholder' => 'Название проекта, название контрагента или ответственный',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-md-4 col-sm-4" style="display: none;">
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                'class' => 'btn btn-default btn-sm',
                'data-toggle' => 'modal',
            ]); ?>
            <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                 tabindex="-1" aria-hidden="true"
                 style="display: none; margin-top: -51.5px;">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="form-body">
                                <div class="row">
                                    Вы уверены, что хотите удалить выбранные проекты?
                                </div>
                            </div>
                            <div class="form-actions row">
                                <div class="col-xs-6">
                                    <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                        'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                        'data-url' => Url::to(['many-delete']),
                                        'data-style' => 'expand-right',
                                    ]); ?>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn darkblue"
                                            data-dismiss="modal">НЕТ
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container">
            <div class="scroll-table-wrapper">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => [
                        'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap invoice-table fix-thead',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => (Yii::$app->controller->id === 'default') ? "{items}\n{pager}" : $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'header' => Html::checkbox('', false, [
                                'class' => 'joint-operation-main-checkbox',
                            ]),
                            'headerOptions' => [
                                'class' => 'text-center pad0',
                                'width' => '5%',
                            ],
                            'contentOptions' => [
                                'class' => 'text-center pad0-l pad0-r',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::checkbox('Project[' . $model['id'] . '][checked]', false, [
                                    'class' => 'joint-operation-checkbox',
                                ]);
                            },
                        ],
                        [
                            'attribute' => 'project_number',
                            'label' => '№ проекта',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::a($model['project_number'], Url::to(['view', 'id' => $model['id']]));
                            },
                        ],
                        [
                            'attribute' => 'start_date',
                            'label' => 'Дата начала',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return DateHelper::format($model['start_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'end_date',
                            'label' => 'Дата окончания',
                            'headerOptions' => [
                                'class' => 'col_project_end_date sorting' . ($userConfig->project_end_date ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_end_date' . ($userConfig->project_end_date ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return DateHelper::format($model['end_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                            },
                        ],
                        [
                            'attribute' => 'name',
                            'label' => 'Название',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model['name'];
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'clients',
                            'label' => 'Клиенты',
                            'headerOptions' => [
                                'class' => 'col_project_clients dropdown-filter' . ($userConfig->project_clients ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_clients' . ($userConfig->project_clients ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getClientsFilter(),
                            'value' => function ($model) {
                                $result = '';
                                foreach ($model['clients'] as $client) {
                                    $result .= Html::a($client['name'], Url::to(['/contractor/view',
                                            'id' => $client['id'],
                                            'type' => Contractor::TYPE_CUSTOMER,
                                        ])) . '<br>';
                                }

                                return $result;
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'contractors',
                            'label' => 'Поставщики',
                            'headerOptions' => [
                                'class' => 'col_project_contractor dropdown-filter' . ($userConfig->project_contractor ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_contractor' . ($userConfig->project_contractor ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getContractorFilter(),
                            'value' => function ($model) {
                                $result = '';
                                foreach ($model['contractors'] as $contractor) {
                                    $result .= Html::a($contractor['name'], Url::to(['/contractor/view',
                                            'id' => $contractor['id'],
                                            'type' => Contractor::TYPE_SELLER,
                                        ])) . '<br>';
                                }

                                return $result;
                            },
                        ],
                        [
                            'attribute' => 'incomeSum',
                            'label' => 'Приход',
                            'headerOptions' => [
                                'class' => 'sorting',
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return TextHelper::invoiceMoneyFormat($model['incomeSum']);
                            },
                        ],
                        [
                            'attribute' => 'expenseSum',
                            'label' => 'Расход',
                            'headerOptions' => [
                                'class' => 'col_project_expense sorting' . ($userConfig->project_expense ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_expense' . ($userConfig->project_expense ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return TextHelper::invoiceMoneyFormat($model['expenseSum']);
                            },
                        ],
                        [
                            'attribute' => 'resultSum',
                            'label' => 'Результат',
                            'headerOptions' => [
                                'class' => 'col_project_result sorting' . ($userConfig->project_result ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_result' . ($userConfig->project_result ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'value' => function ($model) {
                                return TextHelper::invoiceMoneyFormat($model['resultSum']);
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'responsible_employee_id',
                            'label' => 'Ответственный',
                            'headerOptions' => [
                                'class' => 'col_project_responsible_employee_id dropdown-filter' . ($userConfig->project_responsible_employee_id ? '' : ' hidden'),
                            ],
                            'contentOptions' => [
                                'class' => 'col_project_responsible_employee_id' . ($userConfig->project_responsible_employee_id ? '' : ' hidden'),
                            ],
                            'format' => 'raw',
                            'filter' => $searchModel->getResponsibleEmployeeFilter(),
                            'value' => function ($model) use ($userConfig) {
                                /* @var $employee EmployeeCompany */
                                $employee = EmployeeCompany::find()
                                    ->joinWith('employee')
                                    ->andWhere([
                                        EmployeeCompany::tableName() . '.company_id' => $userConfig->employee->company_id,
                                        EmployeeCompany::tableName() . '.employee_id' => $model['responsible_employee_id'],
                                        EmployeeCompany::tableName() . '.is_working' => Employee::STATUS_IS_WORKING,
                                        Employee::tableName() . '.is_active' => Employee::ACTIVE,
                                        Employee::tableName() . '.is_deleted' => Employee::NOT_DELETED,
                                    ])->one();

                                return $employee->getShortFio();
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
