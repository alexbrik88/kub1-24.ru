<?php

use yii\helpers\ArrayHelper;
use common\models\company\CompanyType;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */


$inputListConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];
?>
<?= $form->field($model, 'ITN', array_merge($textInputConfig, [
    'options' => [
        'class' => 'legal form-group required',
    ],
]))->label('ИНН:')->textInput([
    'placeholder' => 'Автозаполнение по ИНН',
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'prod' => YII_ENV_PROD ? 1 : 0,
    ],
]); ?>
<?= $form->field($model, 'name', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group required',
    ],
]))->label('Название контрагента:')->textInput([
    'placeholder' => 'Без ООО/ИП',
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
    ],
]); ?>

<?= $form->field($model, 'companyTypeId', array_merge($textInputConfig, [
    'options' => [
        'id' => 'contractor_company_type',
        'class' => 'form-group legal row required',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4 control-label_no-marg',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line width-inp',
    ],
]))->label('Форма:')->dropDownList($model->getTypeArray()); ?>

<?= $form->field($model, 'PPC', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group field-contractor-ppc required',
    ],
]))->label('КПП:')->textInput([
    'maxlength' => true,
]); ?>

<?= $form->field($model, 'director_email', $textInputConfig)
    ->label('Email покупателя:<br><span>(для отправки счетов):</span>')
    ->textInput([
        'maxlength' => true,
    ]); ?>
<?php $this->registerJs(<<<JS
    var companyType = {
        'ИП' : 1,
        'ООО' : 2,
        'ЗАО' : 3,
        'ПАО' : 4,
        'АО' : 5,
        'АНО' : 6,
        'ФГУП' : 7,
        'ОАО' : $('#contractor-itn').data('prod') == 1 ? 10 : 8
    };
    $('[id="contractor-itn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        onSelect: function(suggestion) {
            console.log(suggestion.data);
            var companyTypeId = '-1';
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            $('#contractor-legal_address').val(suggestion.data.address.value);
            $('#contractor-actual_address').val(suggestion.data.address.value);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId);
            if ($('#contractor-companytypeid').val() == companyType['ИП']) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
            } else {
                $('.field-contractor-ppc').show();
            }
        }
    });
JS
);
