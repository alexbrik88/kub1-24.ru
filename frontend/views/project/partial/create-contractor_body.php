<?php
use common\models\Contractor;
/* @var $this yii\web\View */
/* @var $model Contractor */
/* @var $documentType integer */
$model->contact_is_director = true;
$model->chief_accountant_is_director = true;


echo $this->render('_form-contractor', [
    'model' => $model,
    'face_type_opt' => 0,
    'documentType' => $documentType,
]);