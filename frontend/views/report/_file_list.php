<?php

use common\widgets\Modal;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var $model common\models\report\Report */

?>

<div id="report-file-list-<?= $model->id ?>" class="file-list">
    <?php foreach ($model->reportFiles as $file): ?>
        <div id="report-file-<?= $file->id; ?>" class="file" data-id="<?= $file->id; ?>" >
            <?php
            echo '<span class="pull-left icon icon-paper-clip"></span>';
            echo Html::a($file->file_name, ['get-file', 'id' => $file->id, 'filename' => $file->file_name,], [
                'class' => 'padding-left-10',
                'target' => '_blank',
                'data-pjax' => 0,
            ]);
            echo '<span class="icon icon-close m-l-10" style="cursor:pointer;"
                    data-toggle="modal"
                    data-target="#delete-file-modal-' . $file->id . '"
                    ></span>';
            echo Modal::widget([
                'id' => 'delete-file-modal-' . $file->id,
                'toggleButton' => false,
                'options' => [
                    'class' => 'confirm-modal',
                ],
                'header' => null,
                'closeButton' => false,
                'content' => join('', [
                    '<div class="form-body"><div class="row">Вы уверены, что хотите удалить файл?</div></div>',
                    Html::tag('div', join('', [
                        Html::tag('div', Html::button('ДА', [
                            'class' => 'delete-file-btn btn darkblue pull-right',
                            'data-url' => Url::to(['delete-file', 'id' => $file->id]),
                            'data-key' => $model->id,
                            'data-dismiss' => 'modal',
                        ]), ['class' => 'col-md-6',]),
                        Html::tag('div', Html::button('НЕТ', [
                            'class' => 'btn darkblue',
                            'data-dismiss' => 'modal',
                        ]), ['class' => 'col-md-6',]),
                    ]), [
                        'class' => 'form-actions row',
                    ]),
                ]),
            ]);
            ?>
        </div>
    <?php endforeach; ?>
</div>
