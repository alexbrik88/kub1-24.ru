<?php

use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $reportFile common\models\report\ReportFile */
/* @var $reports common\models\report\Report[] */

$this->title = 'Отчётность';
$this->context->layoutWrapperCssClass = 'out-document reports';
?>
<div class="portlet box">
    <div class="btn-group pull-right">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Report::CREATE)): ?>
            <a href="<?= Url::to(['create']) ?>" class="btn yellow add-new-report">
                <i class="fa fa-plus"></i> ДОБАВИТЬ
            </a>
        <?php endif; ?>
    </div>
    <h3 class="page-title"><?= $this->title; ?></h3>
</div>
<div class="table-container clearfix" style="">
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => null,
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'itemView' => '_view',
]) ?>
</div>

<?php
$this->registerJs(
<<<JS
$.pjax.defaults.timeout = 1000;

var uploaders = {};

$(function () {
    function initUploudBtns(){
        uploaders = {};
        $('.feport-file-upload-button').each(function(i){
            var key = $(this).attr('data-key');
            var url = $(this).attr('data-url');
            var listUrl = $(this).attr('data-list');
            var csrfParameter = $(this).attr('data-csrf-parameter');
            var csrfToken = $(this).attr('data-csrf-token');
            var uploadData = {};
            uploadData[csrfParameter] = csrfToken;
            uploaders[key] = new ss.SimpleUpload({
                button: $(this), // HTML element used as upload button
                url: url, // URL of server-side upload handler
                data: uploadData,
                multipart: true,
                multiple: true,
                encodeCustomHeaders: true,
                responseType: 'json',
                name: 'file', // Parameter name of the uploaded file
                onComplete: function (filename, response) {
                    $.pjax.reload('#report-file-list-' + key, {'url': listUrl, 'push': false, 'replace': false});
                    if (response['success'] === false) {
                        alert(response['msg']);
                    }
                }
            });

        });
    }

    $(document).on('pjax:complete', function() {
        initUploudBtns();
        $('.report_form').each(function(i, form){
            var reportForm = $(form);
            var maxFiles = parseInt(reportForm.attr('data-max-files'));
            if (reportForm.find('.file-list .file').length >= maxFiles) {
                reportForm.find('.feport-file-upload-button').hide();
            } else {
                reportForm.find('.feport-file-upload-button').show();
            }
            reportForm.yiiActiveForm('validateAttribute', 'report-has_file');
        });
    });
});

$(document).on('click', '.delete-file-btn', function(){
    var url = $(this).attr('data-url');
    var key = $(this).attr('data-key');
    $.pjax.reload('#report-file-list-' + key, {'url': url, 'push': false, 'replace': false, 'type': 'post'});
    return false;
});
JS
);
?>