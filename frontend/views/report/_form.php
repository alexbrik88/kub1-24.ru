<?php

use common\models\report\Report;
use common\widgets\Modal;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\report\Report */
/* @var $form yii\widgets\ActiveForm */

$deleteConfirmId = 'delete-confirm-' . $model->id;
?>

<div class="report-form">

    <?php $form = ActiveForm::begin([
        'id' => 'report-form-' . $model->id,
    //    'enableAjaxValidation' => true,
        'options' => [
            'enctype' => 'multipart/form-data',
            'data-pjax' => '',
            'data-max-files' => $model->maxFiles,
            'class' => 'report_form',
        ]
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>

    <?= $this->render('_file_list', ['model' => $model]) ?>

    <div class="form-group" style="margin-top: 10px;">
        <div style="display: inline-block; vertical-align: top; margin-right: 15px;">
        <?php if ($model->isNewRecord) {
            echo $form->field($model, 'uploadedFile[]')->fileInput([
                'class' => 'js_filetype',
                'data-button-text' => '<span class=\'upload-button glyphicon glyphicon-download-alt padding-right-5\' aria-hidden=\'true\'></span>Загрузить документ',
                'multiple' => true,
                'size' => $model->maxFiles,
            ])->label(false);
        } else {
            echo Html::button('
                <span class="upload-button glyphicon glyphicon-download-alt padding-right-5" aria-hidden="true"></span>
                <span class="title" data-default-title="Загрузить документ">Загрузить документ</span>
                ', [
                'class' => 'feport-file-upload-button btn yellow',
                'data-key' => $model->id,
                'data-url' => Url::to(['upload-file', 'id' => $model->id]),
                'data-list' => Url::to(['list-file', 'id' => $model->id]),
                'data-csrf-parameter' => Yii::$app->request->csrfParam,
                'data-csrf-token' => Yii::$app->request->csrfToken,
            ]);
        } ?>
        </div>
        <div style="display: inline-block;">
            <span>можно загрузить: <?= Report::$extensions ?></span>
            <br>
            <span>максимальный размер файла: 3МБ</span>
        </div>
        <?= $form->field($model, 'has_file')->hiddenInput(['value' => ''])->label(false); ?>
    </div>


    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <a href="<?= $model->isNewRecord? Url::to(['index']): Url::to(['view', 'id' => $model->id]) ?>" <?= $model->isNewRecord? 'data-pjax="0"': ''; ?>>
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
                </a>
            </div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?php if (Yii::$app->controller->action->id !== 'create' && !$model->isNewRecord && Yii::$app->user->can(frontend\rbac\permissions\Report::DELETE)): ?>
                    <?= Html::button('Удалить', [
                        'title' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'data' => [
                            'pjax' => 0,
                            'toggle' => 'modal',
                            'target' => '#' . $deleteConfirmId,
                        ],
                    ]) ?>
                    <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                        'title' => 'Удалить',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'data' => [
                            'pjax' => 0,
                            'toggle' => 'modal',
                            'target' => '#' . $deleteConfirmId,
                        ],
                    ]) ?>
                    <?= \frontend\widgets\ConfirmModalWidget::widget([
                        'options' => [
                            'id' => $deleteConfirmId,
                        ],
                        'toggleButton' => false,
                        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить этот отчет?',
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
