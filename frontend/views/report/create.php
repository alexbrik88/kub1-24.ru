<?php

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\report\Report */
/* @var $reportFile common\models\report\ReportFile */

$this->title = 'Создать отчёт';
?>

<div class="report-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
$this->registerJs('
$("#report-uploadedfile").change(function(){
    $(".jq-file").css({"width": "auto"});
        var fileNameArray = $("#report-uploadedfile-styler .jq-file__name").html().split(", ");
        if (fileNameArray.length) {
            $("#report-uploadedfile-styler .jq-file__name").css({"height": "auto"}).html("");
            fileNameArray.forEach(function(item, i) {
                $("#report-uploadedfile-styler .jq-file__name").
                    append(\'<div class="jq-file-name-item">\' + item + \'</div>\');
            });
        }
});
') ?>

<?php
\yii\widgets\PjaxAsset::register($this);

$this->registerJs(
<<<JS
$.pjax.defaults.timeout = 1000;

$(function () {
    function initUploudBtns(){
        uploaders = {};
        $('.feport-file-upload-button').each(function(i){
            var key = $(this).attr('data-key');
            var url = $(this).attr('data-url');
            var listUrl = $(this).attr('data-list');
            var csrfParameter = $(this).attr('data-csrf-parameter');
            var csrfToken = $(this).attr('data-csrf-token');
            var uploadData = {};
            uploadData[csrfParameter] = csrfToken;
            uploaders[key] = new ss.SimpleUpload({
                button: $(this), // HTML element used as upload button
                url: url, // URL of server-side upload handler
                data: uploadData,
                multipart: true,
                multiple: true,
                encodeCustomHeaders: true,
                responseType: 'json',
                name: 'file', // Parameter name of the uploaded file
                onComplete: function (filename, response) {
                    $.pjax.reload('#report-file-list-' + key, {'url': listUrl, 'push': false, 'replace': false});
                    if (response['success'] === false) {
                        alert(response['msg']);
                    }
                }
            });

        });
    }

    $(document).on('pjax:complete', function() {
        initUploudBtns();
        var reportForm = $('#report-form-{$model->id}');
        var maxFiles = parseInt(reportForm.attr('data-max-files'));
        if (reportForm.find('.file-list .file').length >= maxFiles) {
            reportForm.find('.feport-file-upload-button').hide();
        } else {
            reportForm.find('.feport-file-upload-button').show();
        }
        console.log(reportForm);
        reportForm.yiiActiveForm('validateAttribute', 'report-has_file');
    });

    initUploudBtns();
});

$(document).on('click', '.delete-file-btn', function(){
    var url = $(this).attr('data-url');
    var key = $(this).attr('data-key');
    $.pjax.reload('#report-file-list-' + key, {'url': url, 'push': false, 'replace': false, 'type': 'post'});
    return false;
});
JS
);
?>