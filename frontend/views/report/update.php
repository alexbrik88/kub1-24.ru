<?php

use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\report\Report */

?>

<?php Pjax::begin([
    'id' => 'report-pjax-' . ($model->id? : 0),
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<div style="padding: 10px">
    <?=$this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

<?php Pjax::end(); ?>