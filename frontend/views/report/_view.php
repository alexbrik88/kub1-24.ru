<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $report \common\models\report\Report */
/* @var $reportFile common\models\report\ReportFile */
?>

<div class="portlet customer-info customer-info-cash">

    <?php Pjax::begin([
        'id' => 'report-pjax-' . ($model->id? : 0),
        'enablePushState' => false,
    ]); ?>

    <div class="portlet-title">
        <div class="col-md-10 caption" style="float: left; padding: 10px 0;">
            <?= $model->title; ?>
        </div>

        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Report::UPDATE, ['model' => $model,])): ?>
            <div>
                <a href="<?= Url::toRoute(['update', 'id' => $model->id]) ?>"
                    class="btn darkblue btn-sm pjax_link"
                    title="Редактировать отчет"
                    style="float:right">
                    <i class="icon-pencil"></i>
                </a>
            </div>
        <?php endif; ?>
    </div>
    <div class="portlet-body">
        <table id="datatable_ajax" class="table">
            <tbody>
            <tr>
                <td><span
                        class=""><?php
                        $findSpace = 0;
                        if (strlen($model->description) < 500) {
                            echo $model->description;
                        } elseif ($model->description{500} === ' ') {
                            echo substr($model->description, 0, 499) . '...';
                        } else {
                            for ($i = 501; $i < strlen($model->description); $i++) {
                                if ($model->description{$i} === ' ') {
                                    $findSpace = $i;
                                    break;
                                }
                            }
                            echo substr($model->description, 0, $findSpace) . '...';
                        }
                        ?></span>
                </td>
            </tr>
            <?php foreach ($model->reportFiles as $file): ?>
                <tr>
                    <td>
                        <span class="pull-left icon icon-paper-clip"></span>
                        <?= Html::a($file->file_name, ['get-file', 'rid' => $model->id, 'fid' => $file->id, 'filename' => $file->file_name,], [
                            'class' => 'padding-left-10',
                            'target' => '_blank',
                            'data-pjax' => 0,
                        ]); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php Pjax::end(); ?>
</div>
