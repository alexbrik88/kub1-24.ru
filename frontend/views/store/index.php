<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Склады';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div style="margin-bottom: 15px;">
        <?= $this->render('_table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>

    <?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> Добавить склад', [
        'class' => 'btn yellow ajax-modal-btn',
        'data-title' => 'Добавить склад',
        'data-url' => Url::to(['/store/create']),
    ]); ?>
</div>
