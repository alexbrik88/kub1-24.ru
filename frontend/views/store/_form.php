<?php

use common\models\employee\Employee;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\product\Store */
/* @var $form yii\widgets\ActiveForm */

?>

<?php if (Yii::$app->request->isAjax && $model->saved) : ?>
    <?= Html::script('
        $.pjax.reload("#store-pjax-container", {timeout: 5000});
        $(".modal.in").modal("hide");
    ', ['type' => 'text/javascript']); ?>
<?php endif ?>

<div class="store-form">
    <?php $form = ActiveForm::begin([
        'id' => 'store-form',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => '',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => [
            'data-isNewRecord' => (int)$model->isNewRecord,
            'data-modelid' => $model->id,
        ],
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if (!$model->is_main) : ?>
        <?= $form->field($model, 'responsible_employee_id')
        ->dropDownList(ArrayHelper::map($model->company->getEmployeeCompanies()->orderBy([
            'lastname' => SORT_ASC,
            'firstname' => SORT_ASC,
            'patronymic' => SORT_ASC,
        ])->all(), 'employee_id', 'fio'), [
            'prompt' => '',
        ]) ?>
    <?php endif ?>

    <?php if (!$model->isNewRecord && !$model->is_main) : ?>
        <?= $form->field($model, 'is_closed', [
            'labelOptions' => [
                'class' => 'col-sm-4',
            ],
        ])->checkbox([], false) ?>
    <?php endif ?>

    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue text-white ladda-button',
            ]) ?>
            <?= Html::button('Отменить', [
                'class' => 'btn darkblue text-white pull-right',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?= Html::script('$("#store-form input:checkbox").uniform();', ['type' => 'text/javascript']) ?>