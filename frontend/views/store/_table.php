<?php

use common\components\grid\GridView;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\models\company\CheckingAccountant;
use common\models\EmployeeCompany;
use frontend\models\StoreSearch;
use frontend\widgets\BtnConfirmModalWidget;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap',
        'id' => 'datatable_ajax',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],

    'headerRowOptions' => [
        'class' => 'heading',
    ],

    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],

    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => "{items}\n{pager}",

    'columns' => [
        [
            'attribute' => 'name',
            'headerOptions' => [
            ],
        ],
        [
            'attribute' => 'responsible_employee_id',
            'class' => DropDownSearchDataColumn::class,
            'headerOptions' => [
            ],
            'format' => 'raw',
            'value' => function ($model) {
                $e = EmployeeCompany::find()->andWhere([
                    'employee_id' => $model->responsible_employee_id,
                    'company_id' => $model->company_id,
                ])->one();

                return $e ? $e->getFio(true) : '';
            },
        ],
        [
            'label' => 'Тип',
            'value' => function ($model) {
                return $model->is_main ? 'Основной' : ($model->is_closed ? 'Закрыт' : 'Дополнительный');
            },
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'controller' => '/store',
            'template' => '{update} {delete}',
            'headerOptions' => [
                'width' => '10%',
            ],
            'buttons' => [
                'update' => function ($url, $data) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'class' => 'ajax-modal-btn',
                        'data-pjax' => '0',
                        'data-title' => 'Изменить склад',
                        'title' => Yii::t('yii', 'Изменить'),
                        'aria-label' => Yii::t('yii', 'Изменить'),
                    ]);
                },
                'delete' => function ($url, $data) {
                    if (!$data->is_main && $data->canDelete()) {
                        return BtnConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<span aria-hidden="true" class="glyphicon glyphicon-trash"></span>',
                                'class' => '',
                                'tag' => 'a',
                            ],
                            'confirmUrl' => $url,
                            'message' => 'Вы уверены, что хотите удалить склад?',
                        ]);
                    }

                    return '';
                },
            ],
        ],
    ],
]); ?>
