<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\product\Store */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Склады', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="cashbox-view" style="max-width: 600px;">
    <?= Html::a('Назад к списку', ['index']) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'name',
            'responsibleEmployee.fio',
            'is_main:boolean',
            'is_closed:boolean',
        ],
    ]) ?>

    <div>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn darkblue text-white']) ?>
        <?= BtnConfirmModalWidget::widget([
            'toggleButton' => [
                'label' => 'Удалить',
                'class' => 'btn darkblue text-white pull-right',
                'tag' => 'a',
            ],
            'confirmUrl' => Url::to(['delete', 'id' => $model->id]),
            'message' => 'Вы уверены, что хотите удалить склад?',
        ]); ?>
    </div>

</div>