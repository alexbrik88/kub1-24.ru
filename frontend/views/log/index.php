<?php

use common\components\date\DateHelper;
use frontend\components\PageSize;
use frontend\models\log\Log;
use frontend\models\log\LogSearch;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Действия';
?>

<?= Html::a('Назад', ['/site/index'], [
    'class' => 'back-to-customers',
]); ?>

<div class="widget-latest-actions portlet box darkblue">
    <div class="portlet-title">
        <div class="caption m-t-sm">
            <?= $this->title; ?>
        </div>

        <div class="actions">
            <div class="btn-group widget-home-popup">
                <?= $this->render('_form_entity', [
                    'searchModel' => $searchModel,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <?php $date = null; ?>
        <?= \yii\widgets\ListView::widget([
            'options' => [
                'class' => 'list-view',
            ],
            'pager' => [
                'options' => [
                    'class' => 'pagination pull-right',
                ],
            ],
            'layout' => "<ul class=\"feeds\">{items}</ul>" . $this->render('//layouts/grid/perPage', [
                'maxTitle' => $dataProvider->totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все'
            ]) . "{pager}",
            'dataProvider' => $dataProvider,
            'itemView' => function (Log $model) use (&$date) {
                $currentDate = date(DateHelper::FORMAT_USER_DATE, $model->created_at);

                $output = '';

                if ($date != $currentDate) {
                    $date = $currentDate;
                    $output .= '<span class="date vert-middle-pos">' . \php_rutils\RUtils::dt()->ruStrFTime([
                            'format' => 'd F',
                            'monthInflected' => true,
                            'date' => $model->created_at,
                        ]) . '</span>';
                }

                return $output . $this->render('_view', [
                    'model' => $model,
                ]);
            },
        ]); ?>
        <div class="clr"></div>
    </div>
</div>
