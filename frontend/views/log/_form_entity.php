<?php
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\models\log\LogEntityType;
use yii\widgets\ActiveForm;

/* @var \frontend\models\log\LogSearch $searchModel */
/* @var string $action */

$action = isset($action) ? $action : ['/log/index'];

$form = ActiveForm::begin([
    'action' => $action,
    'method' => 'GET',
    'options' => [
        'data-pjax' => true,
    ],
]);
$user = Yii::$app->user->identity;

$employeeArray = EmployeeCompany::find()->alias('e')->joinWith(['company c'], false)
    ->andWhere([
        'e.is_working' => true,
        'c.id' => $user->company->id,
    ])
    ->orderBy([
        'lastname' => SORT_ASC,
        'firstname' => SORT_ASC,
        'patronymic' => SORT_ASC,
    ])
    ->all();

if ($user->currentRole->id == EmployeeRole::ROLE_CHIEF) {
    echo $form->field($searchModel, 'author_id', ['options' => ['class' => '', 'style' => 'display: inline-block;']])
        ->label(false)
        ->dropDownList(
            \yii\helpers\ArrayHelper::map($employeeArray, 'employee_id', 'fio'),
            [
                'prompt' => 'ВСЕ сотрудники',
                'class' => 'form-control form-autosubmit',
            ]
        );
}

echo $form->field($searchModel, 'log_entity_type_id', ['options' => ['class' => '', 'style' => 'display: inline-block;']])
    ->label(false)
    ->dropDownList(
        \yii\helpers\ArrayHelper::map(LogEntityType::find()->all(), 'id', 'name'),
        [
            'prompt' => 'ВСЕ',
            'class' => 'form-control form-autosubmit',
        ]
    );
$form->end(); ?>