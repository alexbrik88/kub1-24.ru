<?php
/* @var \frontend\models\log\Log $model */
try {
    $logMessage = \frontend\models\log\LogHelper::getLogMessage($model);
} catch (Exception $e) {
    $logMessage = null;
}

$icon = \frontend\models\log\LogHelper::getLogIcon($model);
?>

<?php if ($logMessage !== null) : ?>
    <li>
        <div class="col1">
            <div class="cont">
                <div class="cont-col1">
                    <div class="label label-sm <?= $icon[0]; ?>">
                        <i class="<?= $icon[1]; ?>"></i>
                    </div>
                </div>
                <div class="cont-col2">
                    <div class="desc">
                        <span class="icon"></span>

                        <?= $logMessage; ?>

                        <span class="latest-actions-name">
                            <?= $model->employeeCompany ? $model->employeeCompany->getFio(true) : $model->author->getFio(true); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col2">
            <div class="date">
                <?= date('H:i', $model->created_at); ?>
            </div>
        </div>
    </li>
<?php endif; ?>