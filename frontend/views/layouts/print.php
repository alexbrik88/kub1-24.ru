<?php
/**
 * Created by konstantin.
 * Date: 08.10.15
 * Time: 14.07
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $asset \yii\web\AssetBundle */

$asset = ArrayHelper::getValue($this->params, 'asset');
if ($asset !== null) {
    $asset::register($this);
}
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
</head>
<body>

<div class="page-content">
    <?php echo $content; ?>
</div>

<?php $this->endBody(); ?>

<?= $this->render('_metrics'); ?>
</body>
</html>
<?php $this->endPage(); ?>
