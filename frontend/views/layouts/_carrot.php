<?php

use common\models\company\RegistrationPageType;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariffGroup;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

$apiKey = ArrayHelper::getValue(Yii::$app->params, ['carrotquest', 'api_key']);
$userAuthKey = ArrayHelper::getValue(Yii::$app->params, ['carrotquest', 'user_auth_key']);
$employee = Yii::$app->user->identity;
if ($userAuthKey && $employee && ($company = $employee->company)) {
    $tariffArray = [];
    $findirTariffArray = [];
    $groupArray = SubscribeTariffGroup::find()->where([
        'is_active' => true,
    ])->orderBy('id')->indexBy('id')->all();
    foreach ($groupArray as $group) {
        $subscribes = SubscribeHelper::getPayedSubscriptions($company->id, $group->id);
        if ($subscribes) {
            $isTrial = $group->id == SubscribeTariffGroup::STANDART &&
                ($price = array_sum(ArrayHelper::getColumn($subscribes, 'tariff.price'))) == 0;
            $name = $isTrial ? 'Пробный период' : $group->name;
            $paid = $isTrial ? '' : ' оплачено';
            $expDate = SubscribeHelper::getExpireDate($subscribes);
            $date = date('d.m.Y', $expDate);
            $days = Yii::t('yii', '{n, plural, =0{# дня} 1{# день} one{# день} few{# дня} other{# дней}}', [
                'n' => SubscribeHelper::getExpireLeftDays($expDate),
            ]);

            $tariffArray[] = "\"{$name}\"{$paid} до $date (осталось: $days)";
            if (in_array($group->id, SubscribeTariffGroup::$analyticsItems)) {
                $findirTariffArray[] = "\"{$name}\"{$paid} до $date (осталось: $days)";
            }
        }
    }

    if ($company->analytics_module_activated) {
        $isFindir = 'True';
        $findirTariff = $findirTariffArray ? implode('; ', $findirTariffArray) : 'Тарифы закончены';
    } else {
        $isFindir = 'False';
        $findirTariff = 'Нет тарифа';
    }

    $identify = Json::encode([
        '$email' => Html::encode($employee->email),
        '$phone' => Html::encode($employee->phone),
        '$name' => Html::encode($employee->getFio()),
        'ID компании' => $company->id,
        'ИНН' => Html::encode($company->inn),
        'Название компании' => Html::encode($company->getTitle(true)),
        'Текущие подписки' => $tariffArray ? implode('; ', $tariffArray) : 'БЕСПЛАТНО',
        'Регистрация в ФинДире' => $isFindir,
        'Тариф в ФинДире' => $findirTariff,
    ]);
} else {
    $identify = null;
}
?>

<?php if ($apiKey) : ?>
    <!-- Carrot quest BEGIN -->
    <script type="text/javascript">
    !function(){function t(t,e){return function(){window.carrotquestasync.push(t,arguments)}}if("undefined"==typeof carrotquest){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="//cdn.carrotquest.io/api.min.js",document.getElementsByTagName("head")[0].appendChild(e),window.carrotquest={},window.carrotquestasync=[],carrotquest.settings={};for(var n=["connect","track","identify","auth","oth","onReady","addCallback","removeCallback","trackMessageInteraction"],a=0;a<n.length;a++)carrotquest[n[a]]=t(n[a])}}(),carrotquest.connect("26916-1a49af8bf5c281881a56572bbc");
    <?php if ($identify) : ?>
    carrotquest.addCallback('user_replied', function(data) {
        carrotquest.auth('<?= $employee->id ?>', '<?= hash_hmac('sha256', $employee->id, $userAuthKey) ?>');
        carrotquest.identify(<?= $identify ?>);
    })
    <?php endif ?>
    </script>
<!-- Carrot quest END -->
<?php endif ?>
