<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$route = Yii::$app->controller->getRoute();
$mtsUser = ArrayHelper::getValue(\Yii::$app->user, 'identity.mtsUser');

if ($mtsUser && $mtsUser->getMtsSubscribes()->exists() && $route != 'mts/subscribe/index') {
    echo \yii\bootstrap\Alert::widget([
        'body' => 'У Вас есть оплаченные подписки, ожидающие активации. ' . Html::a('Активировать', [
            '/mts/subscribe/index',
        ]),
        'closeButton' => ['tag' => 'span'],
        'options' => [
            'id' => 'mts-subscribe-alert',
            'class' => 'alert-info',
        ],
    ]);
}
