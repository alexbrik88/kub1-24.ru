<?php
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<?php $this->beginBody() ?>

<?php echo $content ?>

<?php $this->endBody(); ?>

<?php $this->endPage(); ?>
