<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.09.2017
 * Time: 6:17
 */

use yii\bootstrap\Html;
use common\components\ImageHelper;
use common\models\ServiceMoreStock;
use yii\bootstrap\ActiveForm;
use common\models\company\Activities;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\employee\Employee;
use common\models\service\SubscribeTariff;
use common\models\service\PaymentType;

/* @var $company common\models\Company */

if (!$company->subscribe_payment_key) {
    $company->subscribe_payment_key = uniqid();
    $company->save(false, ['subscribe_payment_key']);
}

$route = [
    '/subscribe/default/create',
    'id' => $company->id,
    'key' => $company->subscribe_payment_key,
];
$url_1_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_INVOICE]));
$url_1_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_ONLINE]));
$url_2_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_INVOICE]));
$url_2_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_ONLINE]));
$url_3_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_INVOICE]));
$url_3_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_ONLINE]));


$serviceMoreStock = new ServiceMoreStock();
$inputConfig = [
    'options' => [
        'class' => 'form-group row',
    ],
    'labelOptions' => [
        'class' => 'col-md-2 control-label label-width',
        'style' => 'margin-top: 6px;',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$this->registerJs('
$(document).on("change", "#discount-switch", function() {
    $.post("/site/check-discount", $(this).serialize(), function(data) {
        if (typeof data.is_active !== "undefined") {
            $(this).prop("checked", data.is_active).trigger("change");
        } else {
            $(this).prop("checked", !this.checked).trigger("change");
        }
        console.log(data);
    });
});
');
?>

<div class="modal fade" id="service-stock" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= Html::button('', [
                    'class' => 'close',
                    'data' => [
                        'dismiss' => 'modal',
                    ],
                    'aria-hidden' => true,
                ]); ?>
                <h1>ТОЛЬКО ДЛЯ <?= $company->companyType->name_short; ?></h1>
            </div>
            <div class="modal-body">
                <div class="row text">
                    СКИДКА 20% при оплате КУБ сегодня
                </div>
                <div class="col-md-12 switch-block" style="margin-bottom: 10px;">
                    <div class="col-md-5 pay-now checked" data-value="0">ОПЛАТИТЬ СЕЙЧАС</div>
                    <div class="col-md-2 text-center">
                        <label class="rounded-switch">
                            <input id="discount-switch" name="discount" type="checkbox" class="switch">
                            <span class="sliderr round"></span>
                        </label>
                    </div>
                    <div class="col-md-5 default-tariff" data-value="1">ОПЛАТИТЬ ПОТОМ</div>
                </div>
                <div class="col-md-12 block-20stock" style="margin-bottom: 10px;">
                    <div class="discount">
                        20% СКИДКА
                    </div>
                </div>
                <div class="col-md-12 block-20stock" style="margin-bottom: 10px;">
                    <div class="more-discount">
                        <?= Html::a('УВЕЛИЧИТЬ СКИДКУ на 10%', null); ?>
                    </div>
                </div>
                <div class="col-md-12 block-no-stock" style="margin-bottom: 10px;display: none;">
                    <div class="free-trial">
                        14 ДНЕЙ бесплатно
                    </div>
                </div>
                <div class="col-md-12 block-more-stock" style="margin-bottom: 10px;text-align: center;display: none;">
                    <span class="how-get-more-stock">
                        Ответь на 6 вопросов и получи дополнительно скидку в 10%.
                    </span>
                </div>
                <div class="col-md-12 block-more-stock" style="margin-bottom: 20px;text-align: center;display: none;">
                    <span style="font-size: 16px;">Итого Ваша скидка составит 30%</span>
                </div>
                <div class="col-md-12 block-more-stock" style="margin-bottom: 10px;display: none;">
                    <?php $form = ActiveForm::begin([
                        'action' => 'javascript:;',
                        'id' => 'service-more-stock',
                        'options' => [
                            'data-url' => Url::to(['/site/create-service-more-stock']),
                        ],
                    ]); ?>

                    <?= $form->field($serviceMoreStock, 'activities_id', $inputConfig)
                        ->dropDownList(ArrayHelper::merge(
                            ArrayHelper::map(Activities::find()->all(), 'id', 'name'), [
                                null => 'Свой вариант',
                            ]
                        ))
                        ->label('1. Ваш вид деятельности:'); ?>

                    <?= $form->field($serviceMoreStock, 'company_type_text', array_merge($inputConfig, [
                        'options' => [
                            'class' => 'form-group row',
                            'style' => 'display:none;',
                        ],
                        'inputOptions' => [
                            'class' => 'form-control',
                            'placeholder' => 'Введите свой вариант',
                        ],
                    ]))->label(''); ?>

                    <?= $form->field($serviceMoreStock, 'specialization', $inputConfig)
                        ->dropDownList(ServiceMoreStock::$specializationArray)
                        ->label('2. Ваша специализация:'); ?>

                    <?= $form->field($serviceMoreStock, 'average_invoice_count', $inputConfig)
                        ->dropDownList(ServiceMoreStock::$averageInvoiceCountArray)
                        ->label('3.	Среднее кол-во счетов в месяц:'); ?>

                    <?= $form->field($serviceMoreStock, 'employees_count', $inputConfig)
                        ->dropDownList(ServiceMoreStock::$employeesCountArray)
                        ->label('4.	Кол-во сотрудников у вас в компании:'); ?>

                    <?= $form->field($serviceMoreStock, 'accountant_id', $inputConfig)
                        ->dropDownList(ServiceMoreStock::$accountantArray)
                        ->label('5.	Кто ведёт вашу бухгалтерию:'); ?>

                    <?= $form->field($serviceMoreStock, 'has_logo', $inputConfig)
                        ->radioList(ServiceMoreStock::$hasLogoArray, [
                            'value' => ServiceMoreStock::NO_LOGO,
                        ])
                        ->label('6.	У вас есть логотип?'); ?>

                    <div class="form-actions">
                        <div class="row action-buttons" id="buttons-fixed">
                            <div class="col-sm-4 col-xs-4"></div>
                            <div class="col-sm-4 col-xs-4 text-center">
                                <?= Html::submitButton('Отправить', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                                    'form' => 'service-more-stock',
                                ]); ?>
                                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                                    'title' => 'Сохранить',
                                    'form' => 'service-more-stock',
                                ]); ?>
                            </div>
                            <div class="col-sm-4 col-xs-4"></div>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-md-12 tariff-preview-img block-20stock">
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_1month_20.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_4month_20.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_12month_20.png', [
                            245, 145,
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 tariff-preview-img block-30stock" style="display: none;padding-left: 1px;">
                    <div class="col-md-4" style="top: 1px;">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_1month_30.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_4month_30.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/service') . DIRECTORY_SEPARATOR . 'service_12month_30.png', [
                            245, 145,
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 tariff-preview-img block-no-stock" style="display: none;padding-left: 1px;">
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_1_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_4_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_12_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 bill">
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_1_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_2_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_3_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 pay-card">
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_1_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_3_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_3_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                            'target' => '_blank',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reminder30stock" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="false"
     style="display: block; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">Скидка действует только <span style="color: red;">5 дней</span> !<br>Для оплаты
                        зайдите в раздел <?= Html::a('«Оплата сервиса»', Url::to(['/subscribe'])); ?>
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn darkblue" data-dismiss="modal">ОК</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
