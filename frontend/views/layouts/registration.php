<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;

yii\web\JqueryAsset::register($this);
yii\bootstrap4\BootstrapAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div style="width: 100%; max-width: 460px; margin: 45px auto;">
    <div>
        <a href="//kub-24.ru">
            <?= Html::img('//kub-24.ru/wp-content/themes/flat-bootstrap-child/img/for-login/logo.png', [
                'alt' => "logo",
                'width' => "139",
                'height' => "51",
                'style' => 'margin: 0 10px 10px 53px;'
            ]) ?>
        </a>
    </div>
    <div style="margin-top: 20px; border: 5px solid #1fb7f4; padding: 35px 50px; color: #333; font-family: 'IBM Plex Sans Bold';">

        <?= $content ?>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
