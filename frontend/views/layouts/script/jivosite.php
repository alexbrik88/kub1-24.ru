<?php

$this->registerJs(<<<JS
if ($.inArray(window.location.pathname.toString(), ['/site/login', '/site/request-password-reset', '/site/registration']) < 0) {
    (function () {
        var widget_id = 'lpvpwPMHmn';
        var d = document;
        var w = window;

        function l() {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = '//code.jivosite.com/script/widget/' + widget_id;
            var ss = document.getElementsByTagName('script')[0];
            ss.parentNode.insertBefore(s, ss);

        }

        if (d.readyState == 'complete') {
            l();
        } else {
            if (w.attachEvent) {
                w.attachEvent('onload', l);
            } else {
                w.addEventListener('load', l, false);
            }
        }
    })();
}

function jivo_onLoadCallback() {
    $.get('/site/get-jivo-site-params', function (response) {
        if (response.name !== null) {
            jivo_api.setContactInfo({
                name: response.name,
                description: response.companyName
            });
        }
    });
}
JS
);
?>
<div id="jivosite"></div>