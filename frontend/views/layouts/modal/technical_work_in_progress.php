<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$needShow = false;

$isViewed = Yii::$app->request->cookies->getValue('technical_work_in_progress');

if (!\Yii::$app->user->isGuest && !$isViewed) {

    $needShow = true;

    Yii::$app->response->cookies->add(new \yii\web\Cookie([
        'name' => 'technical_work_in_progress',
        'value' => 1,
        'expire' => time() + 3600 * 24
    ]));
}
?>

<?php if ($needShow): ?>


    <?php Modal::begin([
        'header' => '<h3>Ведутся технические работы по обновлению сервиса</h3>',
        'id' => 'modal_technical_work_in_progress',
        'footer' => Html::button('Ok', [
            'class' => 'btn darkblue text-white',
            'data' => [
                'dismiss' => 'modal',
            ],
        ]),
        'clientOptions' => [
            'show' => true,
        ],
    ]); ?>
        <div class="form-group">
            При работе с сервисом возможны временные ошибки.<br/>
            Если у вас выдает ошибку, нужно одновременно нажать кнопки "CTRL" и "F5".<br/>
            Приносим извинения за доставленные неудобства.
        </div>
    <?php Modal::end(); ?>

    <script>
        $('document').ready(function() { $('#modal_technical_work_in_progress').modal('show'); });
    </script>

    <style>
        #modal_technical_work_in_progress { z-index: 10051; }
    </style>

<?php endif; ?>