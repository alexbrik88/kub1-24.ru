<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$message = Yii::$app->session->remove('notify_modal');
?>

<?php if ($message !== null) : ?>

<?php Modal::begin([
    'header' => false,
    'id' => 'notify_modal',
    'footer' => Html::button('Ok', [
        'class' => 'btn darkblue text-white',
        'data' => [
            'dismiss' => 'modal',
        ],
    ]),
    'clientOptions' => [
        'show' => true,
    ],
]); ?>
    <div class="form-group">
        <?= $message ?>
    </div>
<?php Modal::end(); ?>

<?php endif ?>
