<?php

use common\models\company\CompanyType;
use common\widgets\Modal;
use yii\bootstrap4\Html;

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$company = (!\Yii::$app->user->isGuest && Yii::$app->user->identity) ? Yii::$app->user->identity->company : null;
?>

<?php if ($company && $company->getCanTaxModule() && $company->company_type_id == CompanyType::TYPE_IP): ?>
<?php if ((!\Yii::$app->request->cookies->getValue('ip_usn_6_declaration_modal_showed') || Yii::$app->request->get('show_declaration_modal'))): ?>

<?php

// Show 1 time at day
Yii::$app->response->cookies->add(new \yii\web\Cookie([
    'name' => 'ip_usn_6_declaration_modal_showed',
    'value' => '1',
    'expire' => time() + 3600 * 24
]));

Modal::begin([
    'id' => 'modal-ip-usn-6-declaration',
]); ?>
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 pl-25 text-gray">
                <div class="modal-ico">
                    <img width="44px" src="/img/ip-usn-6-icons-instruction.svg"/>
                </div>
                <div class="modal-txt modal-txt-inline bold">
                    Скачать ИНСТРУКЦИЮ <br/>
                    по заполнению налоговой декларации
                </div>
                <div class="modal-head red">
                    Минусы
                </div>
                <div class="modal-txt-small">
                    <table>
                        <tr><td class="red">1</td><td>Нужно разбираться и заполнять самостоятельно</td></tr>
                        <tr class="pb-15"><td class="red">2</td><td>Найти верные реквизиты</td></tr>
                        <tr><td class="red">3</td><td>Проверить правильность КБК и</td></tr>
                        <tr><td class="red">4</td><td>Не ошибиться в расчетах налогов</td></tr>
                        <tr class="pb-15"><td class="red">5</td><td>Не ошибиться в заполнении бланка</td></tr>
                        <tr><td class="red">6</td><td>РИСК "приятного" общения с налоговой, если будут ошибки в уплаченных налогах или декларации</td></tr>
                    </table>
                </div>
                <div class="modal-btn">
                    <?= Html::a('Скачать ИНСТРУКЦИЮ', '#', ['class' => 'btn darkblue-invert']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="modal-ico">
                    <img width="44px" src="/img/fav.svg"/>
                </div>
                <div class="modal-txt modal-txt-inline bold">
                    Использовать КУБ-24 для заполнения <br/>
                    налоговой декларации и расчета налогов
                </div>
                <div class="modal-head green">
                    Плюсы
                </div>
                <div class="modal-txt-small">
                    <table>
                        <tr><td class="green">1</td><td>Программа всё сделает сама <span class="green">за 10 минут!</span></td></tr>
                        <tr><td class="green">2</td><td>Не нужно ничего искать:<ul><li>Верные реквизиты налоговой</li><li>Правильные КБК и ОКТМО</li><li>Все формулы настроены.</li></ul></td></tr>
                        <tr><td class="green">3</td><td>Программа сама <span class="green">законно уменьшит ваши налоги</span></td></tr>
                        <tr><td class="green">4</td><td>Вы будете в <span class="green">ПЛЮСЕ!</span></td></tr>
                    </table>
                </div>
                <div class="modal-btn" style="padding-top: 21px;">
                    <?= Html::a('Заполнить декларацию за 10 минут', '/tax/robot/index', ['class' => 'btn darkblue text-white']) ?>
                </div>
            </div>
        </div>
    </div>
<?php Modal::end(); ?>

<style>
    #modal-ip-usn-6-declaration {
        width: 841px!important;
        left: 45%!important;
    }
    #modal-ip-usn-6-declaration tr.pb-15 td {
        padding-bottom: 15px;
    }
    #modal-ip-usn-6-declaration .pl-25 {
        padding-left: 25px;
    }
    #modal-ip-usn-6-declaration .modal-ico,
    #modal-ip-usn-6-declaration .modal-txt-inline {
        display: inline-block;
        vertical-align: top;
        padding-right: 10px;
    }
    #modal-ip-usn-6-declaration .modal-head{
        margin-top: 15px;
        margin-bottom: 5px;
        font-size: 14px;
    }
    #modal-ip-usn-6-declaration .modal-txt{
        margin: 5px 0;
        font-size: 14px;
    }
    #modal-ip-usn-6-declaration .modal-txt-small {
        font-size: 13px;
    }
    #modal-ip-usn-6-declaration td {
        vertical-align: top;
        padding: 2px 10px 2px 0;
    }
    #modal-ip-usn-6-declaration td:last-child {
        padding-top: 2.5px;
    }
    #modal-ip-usn-6-declaration ul {
        padding-left: 14px;
        margin-bottom: 0;
    }
    #modal-ip-usn-6-declaration ul li {
        padding: 3px;
    }
    #modal-ip-usn-6-declaration td.red,
    #modal-ip-usn-6-declaration td.green {
        font-size: 14px;
    }
    #modal-ip-usn-6-declaration .text-gray {
        color: #777;
    }
    #modal-ip-usn-6-declaration .red {
        color: #c6131c;
    }
    #modal-ip-usn-6-declaration .green {
        color: #159c0e;
    }
    #modal-ip-usn-6-declaration .modal-btn {
        margin-top: 25px;
        margin-bottom: 10px;
    }
    #modal-ip-usn-6-declaration .bold {
        font-weight: 700;
    }
    #modal-ip-usn-6-declaration .btn {
        min-width: 275px;
    }
</style>

<script>
    $(document).ready(function() {
        $('#modal-ip-usn-6-declaration').modal('show');
    })
</script>

<?php endif; ?>
<?php endif; ?>