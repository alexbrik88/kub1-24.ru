<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.01.2020
 * Time: 0:53
 */

use yii\bootstrap\Modal;
use common\components\helpers\Html;

Modal::begin([
    'id' => 'finished_job_modal',
    'header' => '<h2 class="job-header"></h2>',
]); ?>

<div class="job-transaction-block">
    Количество транзакций: <span class="job-transaction-count"></span> шт.
</div>

<div class="form-actions row">
    <div class="col-md-12 text-center">
        <?= Html::a('ОК', null, [
            'class' => 'btn darkblue job-after-link',
        ]) ?>
    </div>
</div>

<?php Modal::end(); ?>
