<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <?php NavBar::begin([
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'containerOptions' => [
            'style' => 'padding: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'id' => 'debt-report-menu',
        'items' => [
            [
                'label' => 'Сотрудники',
                'url' => ['/employee/index'],
                'active' => Yii::$app->controller->id == 'employee',
            ],
            [
                'label' => 'Рассчет ЗП',
                'url' => ['/salary/index'],
                'active' => Yii::$app->controller->id == 'salary',
                'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    NavBar::end();
    ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>