<?php

use common\models\Inquirer;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Inquirer */
/* @var $form ActiveForm */

$model = new Inquirer;
?>

<div class="site-inquirer">
    <?php $form = ActiveForm::begin([
        'id' => 'inquirer-form',
        'action' => Url::to(['/site/inquirer']),
    ]); ?>

    <div class="form-group">
        Меня зовут Арслан, я технический директор КУБ.
        Мы постоянно работаем над улучшением взаимодействия пользователей с нашим сайтом.
        Расскажите, пожалуйста, что вы думаете о нашем сайте ниже.
    </div>

    <div style="font-weight: bold;">Рейтинг</div>
    <?= $form->field($model, 'rating')->label(false)
        ->radioList([1=>1, 2=>2, 3=>3, 4=>4, 5=>5])
        ->hint(Html::tag(
            'div',
            Html::tag('span', 'Плохо', ['class' => 'pull-left']) .
            Html::tag('span', 'Отлично', ['class' => 'pull-right'])
        )) ?>

    <div class="inquirer-field-group low-rating-group">
        <div style="margin-top: -3px; padding-top: 10px; border-top: 1px solid #f1f1f1; font-weight: bold;">
            Будем признательны, если напишите, с какими трудностями вы столкнулись
        </div>
        <?= $form->field($model, 'difficulty')->textArea()->label(false) ?>
    </div>

    <div class="inquirer-field-group high-rating-group">
        <div style="margin-top: -3px; padding-top: 10px; border-top: 1px solid #f1f1f1; font-weight: bold;">
            Планируете ли вы использовать КУБ в будущем?
        </div>
        <?= $form->field($model, 'plan_id')->label(false)->radioList(Inquirer::$planLabels) ?>
    
        <div style="margin-top: -3px; padding-top: 10px; border-top: 1px solid #f1f1f1; font-weight: bold;">
            Сервис помогает решить вашу проблему? Кстати какую?
        </div>
        <?= $form->field($model, 'problem')->textArea(['placeholder' => 'Приятно будет услышать от вас пару слов:)'])->label(false) ?>
    </div>

    <div class="text-center">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary darkblue']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- site-inquirer -->