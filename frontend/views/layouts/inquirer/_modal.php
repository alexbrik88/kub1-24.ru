<?php
use common\models\Company;
use common\widgets\Modal;
use yii\helpers\Html;

$company = \Yii::$app->user->identity->company;

if (Yii::$app->session->remove('show_inquirer_popup') && !$company->getInquirers()->exists()) {
    echo Modal::widget([
        'id' => 'inquirer-modal',
        'toggleButton' => false,
        'header' => '<h3 id="inquirer-modal-header">Здравствуйте!</h3>',
        'options' => [
            'style' => 'right: 0; left: auto; max-width: 300px;',
        ],
        'content' => $this->render('_form'),
    ]);

    $this->registerJs('
        var needShowInquirer = true;
        $(document).mouseleave(function () {
            if (needShowInquirer) {
                $("#inquirer-modal").modal("show");
                needShowInquirer = false;
            }
        });
        $(document).on("change", "input[name=\"Inquirer[rating]\"]", function (e) {
            console.log(this.value);
            $(".inquirer-field-group").hide();
            if ($.inArray(this.value, ["1", "2", "3"]) > -1) {
                $(".low-rating-group").show();
                console.log("low");
            } else if ($.inArray(this.value, ["4", "5"]) > -1) {
                $(".high-rating-group").show();
                console.log("high");
            }
        });
    ');
}
