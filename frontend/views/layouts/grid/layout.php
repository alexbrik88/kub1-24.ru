<?php
use frontend\components\PageSize;

if (empty($pageSizeParam)) {
    $pageSizeParam = 'per-page';
}
?>

<?php if (!empty($scroll)) : ?>
    <div class="scroll-table-wrapper">{items}</div>
<?php else : ?>
    {items}
<?php endif ?>
<?= $this->render('perPage', [
    'maxTitle' => !empty($totalCount) && $totalCount > PageSize::$maxSize ? PageSize::$maxSize : 'Все',
    'pageSizeParam' => $pageSizeParam,
]) ?>
{pager}
<div class="clearfix"></div>