<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.09.2017
 * Time: 8:21
 */

use common\components\helpers\Html;
use common\components\ImageHelper;
use php_rutils\RUtils;
use common\models\service\SubscribeTariff;
use common\models\service\PaymentType;
use yii\helpers\Url;


$user = Yii::$app->user->identity;
$company = $user->company;

if (!$company->subscribe_payment_key) {
    $company->subscribe_payment_key = uniqid();
    $company->save(false, ['subscribe_payment_key']);
}

$route = [
    '/subscribe/default/create',
    'id' => $company->id,
    'key' => $company->subscribe_payment_key,
];
$url_1_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_1_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_2_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_2_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_3_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_3_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_ONLINE]), 'https');


/* @var $expireDays integer
 */
$variants = ['день', 'дня', 'дней'];
?>
<div class="modal fade" id="service-reminder" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <?= Html::button('', [
                    'class' => 'close',
                    'data' => [
                        'dismiss' => 'modal',
                    ],
                    'aria-hidden' => true,
                ]); ?>
                <h1>НАПОМИНАНИЕ</h1>
            </div>
            <div class="modal-body">
                <div class="row text">
                    Через <span class="days-left"><?= RUtils::numeral()->getPlural($expireDays, $variants) ?></span> ваш
                    оплаченный период закончится.<br>
                    Для подолжения работы без ограничений,<br>
                    сделайте оплату сегодня
                </div>
                <div class="col-md-12 tariff-preview-img">
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_1_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_4_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_12_month.png', [
                            245, 145,
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 bill">
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_1_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_2_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Выставить СЧЕТ', $url_3_invoice, [
                            'class' => 'btn darkblue',
                            'title' => 'Для оплаты со счета вашей компании',
                        ]); ?>
                    </div>
                </div>
                <div class="col-md-12 pay-card">
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_1_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_2_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                        ]); ?>
                    </div>
                    <div class="col-md-4">
                        <?= Html::a('Оплатить КАРТОЙ', $url_3_online, [
                            'class' => 'btn',
                            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

