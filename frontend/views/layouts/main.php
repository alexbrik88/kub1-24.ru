<?php

use frontend\assets\AppAsset;
use frontend\modules\documents\assets\TooltipAsset;
use common\widgets\AjaxModalWidget;
use frontend\widgets\SubscribeReminderWidget;
use kartik\select2\ThemeKrajeeAsset;
use kartik\select2\Select2Asset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use frontend\widgets\InvoicePaymentModal;
use common\components\notification\NotificationFlash;
use frontend\widgets\FinishedJobsWidget;
use frontend\widgets\PriceListNotificationsWidget;

/* @var $this \yii\web\View */
/* @var $content string */

Select2Asset::register($this)->addLanguage('ru', '', 'js/i18n');
TooltipAsset::register($this);
ThemeKrajeeAsset::register($this);
AppAsset::register($this);

$session = Yii::$app->session;
$sideBarStatus = Yii::$app->user->identity->config->minimize_side_menu ?? 0;
$status = $sideBarStatus ? 'page-sidebar-closed' : '';
$wrapperClass = isset($this->context->layoutWrapperCssClass) ? $this->context->layoutWrapperCssClass : '';
$hideCarrot = yii\helpers\ArrayHelper::getValue($this->params, 'hideCarrot', false);
$company = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company']);

/*
if (!\Yii::$app->user->isGuest) {
    $this->registerJs("
    var initChatPing = false;
    window.onblur = function () {
        if (window.timeoutID !== null) {
            initChatPing = true;
            clearTimeout(window.timeoutID);
        }
    }
    window.onfocus = function () {
        if (initChatPing) {
            pingSever();
        }
    }
    ");
}
*/
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
        <?php if (YII_ENV === 'prod') : ?>
            <script src="https://cdn.optimizely.com/js/6093251051.js"></script>
        <?php endif; ?>
        <script async src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=5a33a64a-0159-414f-8755-9785017383cc" type="text/javascript"></script>
        <?= (YII_ENV === 'prod' && !$hideCarrot) ? $this->render('_carrot') : null; ?>
    </head>
    <?php if (YII_ENV === 'prod') : ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-WBNX29');</script>
        <!-- End Google Tag Manager -->
    <?php endif; ?>
    <body class="theme-kub page-header-fixed page-quick-sidebar-over-content home-page <?= $status; ?>">
    <div class="black-screen"></div>
    <?php $this->beginBody() ?>

    <?php echo frontend\widgets\MainMenuWidget::widget(); ?>

    <div class="page-container">
        <?php if (Yii::$app->controller->module->id == 'documents' && Yii::$app->controller->action->id == 'preview-scans'): ?>
            <?php // fullscreen mode ?>
        <?php elseif (Yii::$app->controller->module->id == 'documents' && Yii::$app->request->get('mode') == 'previewScan'): ?>
            <?php echo frontend\modules\documents\widgets\SideMenuPreviewScanWidget::widget(); ?>
        <?php elseif (strpos(Yii::$app->request->url, 'upload-manager')): ?>
            <?php echo frontend\modules\documents\widgets\SideMenuDocumentsWidget::widget(); ?>
        <?php else: ?>
            <?php echo frontend\widgets\SideMenuWidget::widget(); ?>
        <?php endif; ?>

        <div class="page-content-wrapper">
            <div class="page-content <?= $wrapperClass ?>">
                <?= $this->render('_service_mts_subscribe'); ?>

                <?= NotificationFlash::widget([
                        'options' => [
                            'closeButton' => true,
                            'showDuration' => 1000,
                            'hideDuration' => 1000,
                            'timeOut' => 5000,
                            'extendedTimeOut' => 1000,
                            'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
                            'escapeHtml' => false,
                        ],
                ]); ?>
                <div id="js-alert"></div>
                <div class="vitala"></div>
                <?php echo $content ?>

                <div id="ajax-loading" class="ajax-loader-wrapper">
                    <img src="/img/loading.gif">
                </div>
            </div>
        </div>
    </div>

    <?php if ($company) : ?>
        <?= frontend\widgets\IfFreeTariffWidget::widget([
            'modalId' => 'if-free-tariff-xls',
            'linkSelector' => '.get-xls-link',
            'text' => 'Выгрузить данные в Excel можно только на платном тарифе.',
        ]); ?>

        <?= frontend\widgets\IfFreeTariffWidget::widget([
            'modalId' => 'if-free-tariff-word',
            'linkSelector' => '.get-word-link',
            'text' => 'Скачать документ в Word можно только на платном тарифе.',
        ]); ?>
    <?php endif ?>

    <div class="page-footer" id="page-footer">
        <div class="page-footer-inner">
            &copy; ООО "КУБ", <?= date('Y'); ?>
        </div>
        <div class="box-tech-support-tel">
            Техподдержка, тел: 8-800-500-54-36, e-mail: support@kub-24.ru
        </div>
        <div class="page-footer-inner pull-right box-l-t-c">
            <a href="http://kub-24.ru/TermsOfUseAll/TermsOfUseAll.pdf"
               target="_blank">Лицензионное соглашение</a>
            <a href="http://kub-24.ru/pomoshh" target="_blank" class="dis-hidden-700">Техническая
                поддержка</a>
            <a href="http://kub-24.ru/about" target="_blank" class="dis-hidden-700">Контакты</a>
        </div>
    </div>

    <style>
        .page-sidebar-closed .footer-for-invoice {
            display: none;
        }

        .footer-for-invoice {
            /*height: 57px;*/
            width: 234px;
            background-color: #4276a4;
            color: #c9dae9;
            position: absolute;
            bottom: 0;
            padding: 8px 20px 10px 20px;
            font-size: 12px;
            transition: margin 0.3s;
        }
    </style>

    <div class="footer-for-invoice hide" id="footer-for-invoice">
        &copy; ООО "КУБ", <?= date('Y'); ?>
        <br>
        Техподдержка, <br>
        тел: 8-800-500-54-36, <br>
        e-mail: support@kub-24.ru <br>
    </div>

    <?php if ($company) : ?>
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\Subscribe::INDEX)) {
            echo SubscribeReminderWidget::widget(['company' => Yii::$app->user->identity->company]);
        } ?>

        <?= $this->render('modal/modal_after_registration'); ?>

        <?= $this->render('modal/notify_modal'); ?>

        <?php // $this->render('_service_video_modal'); ?>

        <?= $this->render('@frontend/modules/tax/views/robot/parts_bank/modal-kub-worth', ['onStartPage' => true]) ?>

        <?php // (YII_ENV === 'prod') ? $this->render('modal/technical_work_in_progress') : null ?>

        <?= !\Yii::$app->user->isGuest ? $this->render('inquirer/_modal') : null; ?>

        <?= !\Yii::$app->user->isGuest ? $this->render('modal/finished_job_modal') : null ?>

        <?php /*<?= !\Yii::$app->user->isGuest ? FinishedJobsWidget::widget() : null */ // not used after 20-320 p.3 ?>

        <?= (false && YII_ENV === 'prod') ? $this->render('script/jivosite') : null; ?>
        <?= \frontend\widgets\ScanPopupWidget::widget(); ?>
        <?= AjaxModalWidget::widget(); ?>
        <?= InvoicePaymentModal::widget(); ?>
        <?= PriceListNotificationsWidget::widget() ?>
    <?php endif ?>

    <?php $this->endBody(); ?>

    <script>
        jQuery(document).ready(function () {
            Metronic.init(); // init metronic core components
            ChartsFlotcharts.init();
            ChartsFlotcharts.initCharts();
            ChartsFlotcharts.initBarCharts();
        });
    </script>

    <?= $this->render('_metrics'); ?>
    </body>
    <?php if (YII_ENV === 'prod') : ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="" https:
            //www.googletagmanager.com/ns.html?id=GTM-WBNX29""
            height=""0"" width=""0""
            style=""display:none;visibility:hidden""></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="" page"" class=""hfeed site"">
    <?php endif; ?>
    </html>

<?php $this->endPage(); ?>