<?php

use common\widgets\Modal;
use yii\helpers\Html;

$show = false;
$user = \Yii::$app->user;
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;

if (!$user->isGuest &&
    !$user->getIsMtsUser() &&
    $user->identity->need_look_service_video &&
    !($controller == 'product' && $action == 'index' && Yii::$app->request->get('modal')) &&
    !($module == 'tax' && $controller == 'robot') &&
    !Yii::$app->session->remove('showAgreementCreatePopup')) {
    $user->identity->need_look_service_video = false;
    $user->identity->save(false, ['need_look_service_video']);

    if (!$user->identity->company->getInvoices()->exists()) {
        $show = true;
    }
}

echo Modal::widget([
    'id' => 'service_video_review_modal',
    'toggleButton' => false,
    'size' => 'modal-lg',
    'header' => Html::tag('h1', Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-info-sign pull-left',
            'style' => 'font-size:inherit;line-height:inherit;',
        ]) . ' Возможности КУБ'),
    'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
    'clientOptions' => ['show' => $show],
    'content' => Html::tag('div', Html::tag('iframe', '', [
        'width' => 480,
        'height' => 370,
        'allowfullscreen' => true,
        'src' => 'https://player.vimeo.com/video/217134783',
    ]), ['class' => 'video-container']),
]);
