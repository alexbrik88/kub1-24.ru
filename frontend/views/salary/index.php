<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmployeeSalarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расчет  ЗП';
?>
<div class="employee-salary-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'dataColumnClass' => \common\components\grid\DataColumn::className(),

        'tableOptions' => [
            'class' => 'table table-bordered table-hover dataTable',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],

        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'attribute' => 'date',
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'nowrap',
                ],
                'value' => function ($model) {
                    return Html::a(mb_convert_case(Yii::$app->formatter->asDate($model->date, 'LLLL Y'), MB_CASE_TITLE), [
                        'summary',
                        'date' => $model->date,
                    ]);
                }
            ],
            [
                'attribute' => 'days_count',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
            [
                'attribute' => 'work_days_count',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
            [
                'attribute' => 'employees_count',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
            ],
            [
                'attribute' => 'total_amount',
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) {
                    return number_format($model->total_amount / 100, 2, '.', ' ');
                }
            ],
            [
                'attribute' => 'total_tax_ndfl',
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) {
                    return number_format($model->total_tax_ndfl / 100, 2, '.', ' ');
                }
            ],
            [
                'attribute' => 'total_tax_social',
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) {
                    return number_format($model->total_tax_social / 100, 2, '.', ' ');
                }
            ],
            [
                'attribute' => 'total_expenses',
                'contentOptions' => [
                    'class' => 'text-right',
                ],
                'value' => function ($model) {
                    return number_format($model->total_expenses / 100, 2, '.', ' ');
                }
            ],
        ],
    ]); ?>
</div>
