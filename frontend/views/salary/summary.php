<?php

use frontend\widgets\UpdateAttributeTooltipWidget;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\EmployeeSalarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$date = mb_convert_case(Yii::$app->formatter->asDate($model->date, 'LLLL Y'), MB_CASE_TITLE);

$this->title = 'Расчет ЗП за ' . $date;

$prepayDate = $model->prepayDate ? : '';
$payDate = $model->payDate ? : '';

$dayRange = range(0, (int) date_create($model->date)->format('t'));
$dayItems = array_combine($dayRange, $dayRange);
$updateSummaryAction = ['update-summary', 'date' => $model->date];

$js = <<<JS
$(document).on("click", ".update-attribute-tooltip", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
    $(this).tooltipster("open");
});
$(document).on("click", "form.update-attribute-tooltip-form .cancel", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
});
$(document).on("submit", "form.update-attribute-tooltip-form", function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: this.action,
        method: this.method,
        data: form.serialize(),
        success: function() {
            $(".update-attribute-tooltip").tooltipster("close");
            $.pjax.reload("#employee-salary-pjax");
        }
    })
});
$(document).on("pjax:complete", "#employee-salary-pjax", function(e) {
    $(".date-picker", this).datepicker({"language":"ru","autoclose":true});
});
JS;
$this->registerJs($js);
?>
<div class="employee-salary-index">
    <?= Html::a('Расчет ЗП список', ['index']) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'employee-salary-pjax',
    ]); ?>

    <div class="form-group">
        <table>
            <tr>
                <td class="text-bold">
                    Количество дней в <?= $date ?>
                </td>
                <td style="padding-left: 10px;">
                    <?= $model->days_count ?>
                </td>
            </tr>
            <tr>
                <td class="text-bold">
                    Рабочих дней в <?= $date ?>
                </td>
                <td style="padding-left: 10px;">
                    <?= $model->work_days_count ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-work_days_count',
                        'action' => $updateSummaryAction,
                        'input' => Html::activeDropDownList($model, 'work_days_count', $dayItems, [
                            'class' => 'form-control',
                            'style' => 'width: 100%;'
                        ]),
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td class="text-bold">
                    Дата аванса
                </td>
                <td style="padding-left: 10px;">
                    <?= $model->prepayDate ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-prepay_date',
                        'action' => $updateSummaryAction,
                        'input' => Html::activeTextInput($model, 'prepayDate', [
                            'class' => 'form-control date-picker',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
            </tr>
            <tr>
                <td class="text-bold">
                    Дата зарплаты
                </td>
                <td style="padding-left: 10px;">
                    <?= $model->payDate ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-pay_date',
                        'action' => $updateSummaryAction,
                        'input' => Html::activeTextInput($model, 'payDate', [
                            'class' => 'form-control date-picker',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
            </tr>
        </table>
    </div>

    <div class="hidden">
        <div>

        </div>
    </div>

    <?= common\components\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'dataColumnClass' => 'common\components\grid\DataColumn',
        'showFooter' => true,

        'tableOptions' => [
            'class' => 'table table-bordered table-hover',
            'role' => 'grid',
        ],

        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'footerRowOptions' => [
            'class' => 'text-bold',
        ],

        'pager' => [
            'options' => [
                'class' => 'pagination pull-right',
            ],
        ],

        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

        'columns' => [
            [
                'attribute' => 'employee_id',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'format' => 'html',
                'value' => function ($model) {
                    return Html::a($model->employeeCompany->getFio(true), [
                        'detail',
                        'id' => $model->employee_id,
                        'date' => $model->date,
                    ], [
                        'data' => [
                            'pjax' => '0',
                        ]
                    ]);
                },
                'footer' => 'Итого',
            ],
            [
                'class' => 'common\components\grid\DropDownSearchDataColumn',
                'attribute' => 'position',
                'label' => 'Должность',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'filter' => $searchModel->filterPosition($dataProvider->query),
                'value' => 'employeeCompany.position',
                'footer' => ' ',
            ],
            [
                'label' => 'Рабочих дней',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'value' => 'employeeSalarySummary.work_days_count',
                'footer' => ' ',
            ],
            [
                'attribute' => 'days_worked',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'value' => function ($model) use ($dayItems) {
                    return $model->days_worked . ' ' . UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-days_worked-' . $model->employee_id,
                        'action' => ['update-detail', 'id' => $model->employee_id, 'date' => $model->date],
                        'input' => Html::activeDropDownList($model, 'days_worked', $dayItems, [
                            'class' => 'form-control',
                            'style' => 'width: 100%;'
                        ]),
                    ]);
                },
                'format' => 'raw',
                'footer' => ' ',
            ],
            [
                'attribute' => 'total_amount',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_amount / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_amount') / 100, 2, '.', ' '),
            ],
            [
                'attribute' => 'total_tax_ndfl',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_tax_ndfl / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_tax_ndfl') / 100, 2, '.', ' '),
            ],
            [
                'attribute' => 'total_tax_social',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_tax_social / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_tax_social') / 100, 2, '.', ' '),
            ],
            [
                'label' => 'Аванс',
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'text-center',
                    'rowspan' => 1,
                    'colspan' => 2,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
            ],
            [
                'attribute' => 'salary_1_prepay_date',
                'label' => 'Дата',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-right-width: 1px;'
                ],
                'contentOptions' => [
                    'class' => 'text-center nowrap',
                ],
                'format' => 'date',
                'footer' => ' ',
            ],
            [
                'attribute' => 'total_prepay_sum',
                'label' => 'Сумма',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-right-width: 1px;'
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_prepay_sum / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_prepay_sum') / 100, 2, '.', ' '),
            ],
            [
                'label' => 'Зарплата',
                'headerOptions' => [
                    'type' => 'header',
                    'class' => 'text-center',
                    'rowspan' => 1,
                    'colspan' => 2,
                    'style' => 'border-bottom: 1px solid #ddd;',
                ],
            ],
            [
                'attribute' => 'salary_1_pay_date',
                'label' => 'Дата',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-right-width: 1px;'
                ],
                'contentOptions' => [
                    'class' => 'text-center nowrap',
                ],
                'format' => 'date',
                'footer' => ' ',
            ],
            [
                'attribute' => 'total_pay_sum',
                'label' => 'Сумма',
                'headerOptions' => [
                    'rowspan' => 1,
                    'colspan' => 1,
                    'style' => 'border-right-width: 1px;'
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_pay_sum / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_pay_sum') / 100, 2, '.', ' '),
            ],
            [
                'attribute' => 'total_expenses',
                'headerOptions' => [
                    'rowspan' => 2,
                    'colspan' => 1,
                ],
                'contentOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'footerOptions' => [
                    'class' => 'text-right nowrap',
                ],
                'value' => function ($model) {
                    return number_format($model->total_expenses / 100, 2, '.', ' ');
                },
                'footer' => number_format($dataProvider->query->sum('total_expenses') / 100, 2, '.', ' '),
            ],
        ],
    ]); ?>

    <?php \yii\widgets\Pjax::end(); ?>
</div>
