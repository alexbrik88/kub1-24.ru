<?php

use common\models\employee\EmployeeSalary;
use frontend\widgets\UpdateAttributeTooltipWidget;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\employee\EmployeeSalary */

$date = Yii::$app->formatter->asDate($model->date, 'LLLL Y');
$this->title = 'Сводные данные за ' . $date . ' - ' . $model->employeeCompany->getFio();

$updateAction = ['update-detail', 'id' => $model->employee_id, 'date' => $model->date];

$js = <<<JS
$(document).on("click", ".update-attribute-tooltip", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
    $(this).tooltipster("open");
});
$(document).on("click", "form.update-attribute-tooltip-form .cancel", function (e) {
    $(".update-attribute-tooltip").tooltipster("close");
});
$(document).on("submit", "form.update-attribute-tooltip-form", function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: this.action,
        method: this.method,
        data: form.serialize(),
        success: function() {
            $(".update-attribute-tooltip").tooltipster("close");
            $.pjax.reload("#employee-salary-pjax");
        }
    })
});
$(document).on("pjax:complete", "#employee-salary-pjax", function(e) {
    $(".date-picker", this).datepicker({"language":"ru","autoclose":true});
});
JS;
$this->registerJs($js);
?>
<div class="employee-salary-view">
    <?= Html::a('Расчет ЗП за ' . $date, [
        'summary',
        'date' => $model->date,
    ]) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'employee-salary-pjax',
    ]); ?>

    <table class="employee-salary-table table table-bordered">
        <thead>
            <tr class="heading">
                <th rowspan="2" style="">Вид расчета</th>
                <th rowspan="2" style="">Сумма на руки (руб.)</th>
                <th rowspan="2" style="">НДФЛ</th>
                <th rowspan="2" style="">Соц. налоги</th>
                <th colspan="2" style="border: 1px solid #ddd;">Аванс</th>
                <th colspan="2" style="border: 1px solid #ddd;">Зарплата</th>
                <th rowspan="2" style="">Итого затраты для компании</th>
            </tr>
            <tr class="heading">
                <th style="">Дата</th>
                <th style="">Сумма</th>
                <th style="">Дата</th>
                <th style="">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Оклад 1
                </td>
                <td class="text-right">
                    <?= $model->salary_1_amount === null ? '' : number_format($model->salary_1_amount / 100, 2, '.', ' '); ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-salary1Amount',
                        'action' => $updateAction,
                        'input' => Html::activeTextInput($model, 'salary1Amount', [
                            'class' => 'form-control text-right',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->salary_1_amount ? EmployeeSalary::TAX_NDFL . '%' : ''; ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->salary_1_amount ? EmployeeSalary::TAX_SOCIAL . '%' : ''; ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_1_amount) : ?>
                        <?= $model->salary1PrepayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary1PrepayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary1PrepayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_1_amount) : ?>
                        <?= $model->salary_1_prepay_sum === null ? '' : number_format($model->salary_1_prepay_sum / 100, 2, '.', ' '); ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary1PrepaySum',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary1PrepaySum', [
                                'class' => 'form-control text-right',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_1_amount) : ?>
                        <?= $model->salary1PayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary1PayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary1PayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?= $model->salary_1_amount ? number_format($model->salary_1_pay_sum / 100, 2, '.', ' ') : ''; ?>
                </td>
                <td class="text-right">
                    <?= $model->salary_1_amount ? number_format($model->salary_1_expenses / 100, 2, '.', ' ') : ''; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Премия 1
                </td>
                <td class="text-right">
                    <?= $model->bonus_1_amount ? number_format($model->bonus_1_amount / 100, 2, '.', ' ') : ''; ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-bonus1Amount',
                        'action' => $updateAction,
                        'input' => Html::activeTextInput($model, 'bonus1Amount', [
                            'class' => 'form-control text-right',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->bonus_1_amount ? EmployeeSalary::TAX_NDFL . '%' : ''; ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->bonus_1_amount ? EmployeeSalary::TAX_SOCIAL . '%' : ''; ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_1_amount) : ?>
                        <?= $model->bonus1PrepayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus1PrepayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus1PrepayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_1_amount) : ?>
                        <?= $model->bonus_1_prepay_sum === null ? '' : number_format($model->bonus_1_prepay_sum / 100, 2, '.', ' '); ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus1PrepaySum',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus1PrepaySum', [
                                'class' => 'form-control text-right',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_1_amount) : ?>
                        <?= $model->bonus1PayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus1PayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus1PayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?= $model->bonus_1_amount ? number_format($model->bonus_1_pay_sum / 100, 2, '.', ' ') : ''; ?>
                </td>
                <td class="text-right">
                    <?= $model->bonus_1_amount ? number_format($model->bonus_1_expenses / 100, 2, '.', ' ') : ''; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Оклад 2
                </td>
                <td class="text-right">
                    <?= $model->salary_2_amount ? number_format($model->salary_2_amount / 100, 2, '.', ' ') : ''; ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-salary2Amount',
                        'action' => $updateAction,
                        'input' => Html::activeTextInput($model, 'salary2Amount', [
                            'class' => 'form-control text-right',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->salary_2_amount ? '-' : ''; ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->salary_2_amount ? '-' : ''; ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_2_amount) : ?>
                        <?= $model->salary2PrepayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary2PrepayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary2PrepayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_2_amount) : ?>
                        <?= $model->salary_2_prepay_sum === null ? '' : number_format($model->salary_2_prepay_sum / 100, 2, '.', ' '); ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary2PrepaySum',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary2PrepaySum', [
                                'class' => 'form-control text-right',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->salary_2_amount) : ?>
                        <?= $model->salary2PayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-salary2PayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'salary2PayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?= $model->salary_2_amount ? number_format($model->salary_2_pay_sum / 100, 2, '.', ' ') : ''; ?>
                </td>
                <td class="text-right">
                    <?= $model->salary_2_amount ? number_format($model->salary_2_expenses / 100, 2, '.', ' ') : ''; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Премия 2
                </td>
                <td class="text-right">
                    <?= $model->bonus_2_amount ? number_format($model->bonus_2_amount / 100, 2, '.', ' ') : ''; ?>
                    <?= UpdateAttributeTooltipWidget::widget([
                        'id' => 'update-bonus2Amount',
                        'action' => $updateAction,
                        'input' => Html::activeTextInput($model, 'bonus2Amount', [
                            'class' => 'form-control text-right',
                            'style' => 'width: 100%;',
                            'autocomplete' => 'off',
                        ]),
                    ]) ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->bonus_2_amount ? '-' : ''; ?>
                </td>
                <td style="text-align: center;">
                    <?= $model->bonus_2_amount ? '-' : ''; ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_2_amount) : ?>
                        <?= $model->bonus2PrepayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus2PrepayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus2PrepayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_2_amount) : ?>
                        <?= $model->bonus_2_prepay_sum === null ? '' : number_format($model->bonus_2_prepay_sum / 100, 2, '.', ' '); ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus2PrepaySum',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus2PrepaySum', [
                                'class' => 'form-control text-right',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?php if ($model->bonus_2_amount) : ?>
                        <?= $model->bonus2PayDate ?>
                        <?= UpdateAttributeTooltipWidget::widget([
                            'id' => 'update-bonus2PayDate',
                            'action' => $updateAction,
                            'input' => Html::activeTextInput($model, 'bonus2PayDate', [
                                'class' => 'form-control date-picker',
                                'style' => 'width: 100%;',
                                'autocomplete' => 'off',
                            ]),
                        ]) ?>
                    <?php endif ?>
                </td>
                <td class="text-right">
                    <?= $model->bonus_2_amount ? number_format($model->bonus_2_pay_sum / 100, 2, '.', ' ') : ''; ?>
                </td>
                <td class="text-right">
                    <?= $model->bonus_2_amount ? number_format($model->bonus_2_expenses / 100, 2, '.', ' ') : ''; ?>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr class="summary">
                <td>
                    Итого
                </td>
                <td class="text-right">
                    <?= number_format($model->total_amount / 100, 2, '.', ' ') ?>
                </td>
                <td class="text-right">
                    <?= number_format($model->total_tax_ndfl / 100, 2, '.', ' ') ?>
                </td>
                <td class="text-right">
                    <?= number_format($model->total_tax_social / 100, 2, '.', ' ') ?>
                </td>
                <td>
                </td>
                <td class="text-right">
                    <?= number_format($model->total_prepay_sum / 100, 2, '.', ' ') ?>
                </td>
                <td>
                </td>
                <td class="text-right">
                    <?= number_format($model->total_pay_sum / 100, 2, '.', ' ') ?>
                </td>
                <td class="text-right" style="text-align: right;">
                    <?= number_format($model->total_expenses / 100, 2, '.', ' ') ?>
                </td>
            </tr>
        </tfoot>
    </table>

    <?php \yii\widgets\Pjax::end(); ?>
</div>
