<?php

use backend\models\Prompt;
use backend\models\PromptHelper;
use common\models\helpArticle\HelpArticle;
use common\widgets\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $articles HelpArticle[] */
/* @var $searchModel common\models\helpArticle\HelpArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Справочник';
$this->params['breadcrumbs'][] = $this->title;
$this->context->layoutWrapperCssClass = 'profile-setup';
?>
<h3 class="page-title"><?= $this->title; ?></h3>

<div class="portlet box darkblue">
    <?= Html::beginForm(['index'], 'GET'); ?>
    <div class="search-form-default">
        <div class="col-md-9 pull-right serveces-search"
             style="max-width: 595px;">
            <div class="input-group">
                <div class="input-cont">
                    <?= Html::activeTextInput($searchModel, 'title', [
                        'placeholder' => 'Поиск...',
                        'class' => 'form-control',
                    ]) ?>
                </div>
                <span class="input-group-btn">
                    <?= Html::submitButton('Найти', [
                        'class' => 'btn green-haze',
                    ]); ?>
                </span>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="portlet-title">
        <div class="caption caption_for_input" style="width: 21%!important;">
            Список разделов
        </div>
    </div>

    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],

                'headerRowOptions' => [
                    'class' => 'heading',
                ],

                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],

                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],

                'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),

                'columns' => [
                    [
                        'attribute' => 'sequence',
                        'label' => 'Номер',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '6%',
                        ],
                        'format' => 'text',
                    ],
                    [
                        'attribute' => 'title',
                        'label' => 'Раздел',
                        'headerOptions' => [
                            'class' => 'sorting',
                        ],
                        'format' => 'raw',
                        'value' => function (HelpArticle $data) {
                            return Html::a($data->title, Url::toRoute(['/help-article/view', 'id' => $data->id]));
                        },
                    ],
                    [
                        'label' => 'Видеотур',
                        'headerOptions' => [
                            'width' => '15%',
                        ],
                        'format' => 'raw',
                        'value' => function (HelpArticle $data) {
                            $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, \common\models\prompt\ForWhomPrompt::ALL, $data->sequence, true);
                            if ($prompt !== null) {
                                echo Modal::widget([
                                    'id' => 'view-video-modal-' . $data->sequence,
                                    'toggleButton' => false,
                                    'size' => 'modal-lg',
                                    'header' => null,
                                    'closeButton' => false,
                                    'content' => '<div class="video-container">
                                <iframe width="480" height="270"
                                        src=' . $prompt->video_link . '
                                        frameborder="0" allowfullscreen>
                                </iframe>
                            </div>',
                                ]);

                                return '<span class="glyphicon glyphicon-film" style="cursor:pointer;"
                                    data-toggle="modal"
                                    data-target="#view-video-modal-' . $data->sequence . '"
                                ></span><span style="cursor:pointer;"
                                    data-toggle="modal"
                                    data-target="#view-video-modal-' . $data->sequence . '">
                                     Как это работает</span>';
                            }

                            return '';
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
