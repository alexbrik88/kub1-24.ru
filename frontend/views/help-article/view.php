<?php

use common\models\helpArticle\HelpArticle;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model HelpArticle */
/* @var $previousArticle HelpArticle */
/* @var $nextArticle HelpArticle */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Help Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->context->layoutWrapperCssClass = 'info-page';
?>
<?= Html::a('Назад к списку', ['index'], [
    'class' => 'back-to-customers'
]); ?>
<div class="portlet box">
    <h3 class="page-title"><?= $model->sequence . '. ' . $this->title; ?></h3>
</div>
<div class="portlet box">
    <?= $model->text; ?>
    <ul class="pager">

        <?php if (!empty($previousArticle)) : ?>
            <li class="previous">
                <?= Html::a($previousArticle->sequence . '. ' . $previousArticle->title,
                    ['help-article/view', 'id' => $previousArticle->id]); ?>
            </li>
        <?php endif; ?>

        <li>
            <?= Html::a('Вернуться к оглавлению', Url::toRoute(['help-article/index'])); ?>
        </li>

        <?php if (!empty($nextArticle)) : ?>
            <li class="next">
                <?= Html::a($nextArticle->sequence . '. ' . $nextArticle->title,
                    ['help-article/view', 'id' => $nextArticle->id]);?>
            </li>
        <?php endif; ?>
    </ul>
</div>
