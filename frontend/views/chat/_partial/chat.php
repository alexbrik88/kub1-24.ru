<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.09.2017
 * Time: 6:03
 */

use yii\bootstrap\Html;
use common\models\employee\Employee;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\components\date\DateHelper;
use common\models\chat\CrmUser;

/* @var $user Employee
 * @var $userID integer
 * @var $friend CrmUser|Employee
 */

$messageDates = $user->getMessageDates($userID);
$chatVolume = $user->currentEmployeeCompany->getChatVolume($userID);
$isCrmUser = false;
if (stristr($userID, 'crm')) {
    $isCrmUser = true;
    $friend = CrmUser::find()->where(['user_id' => preg_replace("/[^0-9]/", '', $userID)])->one();
} else {
    $friend = Employee::findOne($userID);
}
?>
<div class="portlet light bordered chat">
    <div class="portlet-title">
        <div class="col-md-6 p-l-0 profile">
            <a href="<?= $isCrmUser ? null : Url::to(['/employee/view', 'id' => $userID]); ?>">
                <img class="contact-pic"
                     src="<?= $isCrmUser ? ('//' . $friend->chat_photo) : $friend->getChatPhotoSrc(); ?>">
                        <span class="contact-name">
                            <?= $isCrmUser ? $friend->user_fio : $friend->currentEmployeeCompany->getFio(); ?>
                            <br>
                            <span style="font-size: 10px;">
                                <?php if ($isCrmUser): ?>
                                    Бухгалтер CRM
                                <?php else: ?>
                                    <?= $friend->id == Employee::SUPPORT_KUB_EMPLOYEE_ID ?
                                        '' :
                                        ($friend->currentEmployeeCompany->employeeRole->name . ', ' . $friend->currentEmployeeCompany->company->getShortName()); ?>
                                <?php endif; ?>
                            </span>
                        </span>
                <span
                    class="contact-status <?= $isCrmUser ?
                        ($friend->is_online ? 'bg-green' : 'bg-red') :
                        ($friend->currentEmployeeCompany->employee->isOnline() ? 'bg-green' : 'bg-red'); ?>"></span>

            </a>
        </div>
        <div class="col-md-2 caption">
            <span
                class="glyphicon <?= ($chatVolume && $chatVolume->has_volume == Employee::HAS_VOLUME) ? 'glyphicon-volume-up' : 'glyphicon-volume-off'; ?> chat-volume"
                data-url="<?= Url::to(['change-volume-setting', 'friendID' => $userID]); ?>"
                style="float: right;"></span>
        </div>
        <div class="col-md-4 actions" style="padding: 3px 0 14px 15px;">
            <div class="portlet-input input-inline">
                <div class="input-icon right">
                    <i class="icon-magnifier" data-url="<?= Url::to(['search', 'userID' => $userID]); ?>"></i>
                    <?= Html::textInput('searchMessage', null, [
                        'class' => 'form-control input-circle search-message',
                        'placeholder' => 'Поиск...',
                        'data-url' => Url::to(['search', 'userID' => $userID]),
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet-body" id="chats">
        <div class="scroller" style="position:relative;height: 300px; overflow: hidden; width: auto;"
             data-always-visible="1"
             data-rail-visible1="1" data-initialized="1">
            <ul class="chats">
                <?php foreach ($user->getMessages($userID) as $key => $message): ?>
                    <?php $formattedDateDelimiterText = $message->getFormattedDateDelimiterText(); ?>
                    <?php if (in_array(date(DateHelper::FORMAT_DATE, $message->created_at), $messageDates)): ?>
                        <?php $deleteMessageDateKey = array_search(date(DateHelper::FORMAT_DATE, $message->created_at), $messageDates);
                        unset($messageDates[$deleteMessageDateKey]);
                        ?>
                        <div class="date-delimiter" data-value="<?= $formattedDateDelimiterText ?>" style="padding-top: <?= !$deleteMessageDateKey ? '10px' : '0' ?>;">
                            <div class="box-title-info-txt clear-box"
                                 style="margin-right:0;width: 43%;display: inline-block;"></div>
                            <div
                                style="width: 12%;display: inline-block;text-align: center;">
                                <?= $formattedDateDelimiterText; ?>
                            </div>
                            <div class="box-title-info-txt clear-box"
                                 style="margin-right:0;width: 43%;display: inline-block;"></div>
                        </div>
                    <?php endif; ?>
                    <?= $this->render('message', [
                        'user' => $user,
                        'message' => $message,
                        'dateDelimiter' => $formattedDateDelimiterText,
                    ]); ?>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="chat-form">
            <?php ActiveForm::begin([
                'action' => 'javascript:;',
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'id' => 'chat-form',
                    'data' => [
                        'from-user-id' => $user->id,
                        'to-user-id' => $userID,
                    ],
                    'data-url' => Url::to(['send-message', 'id' => $userID])
                ],
            ]) ?>
            <div class="input-cont" style="margin-right: 0;display: inline-block;">
                <?= Html::textarea('message', null, [
                    'class' => 'form-control message',
                    'placeholder' => 'Ваше сообщение...',
                    'rows' => 3,
                    'style' => 'resize:vertical;',
                ]); ?>
            </div>
            <div class="btn-cont" style="display:inline-block;width: 51px;margin-top:0;">
                <?= Html::submitButton('<i class="glyphicon glyphicon-send" style="left: -1px;"></i>', [
                    'class' => 'btn blue icn-only',
                    'style' => 'margin-top:0;float:right;',
                ]); ?>
                <div class="upload-area-chat"
                     data-upload-url="<?= \yii\helpers\Url::to(['upload-file', 'toEmployeeID' => $userID]); ?>"
                     data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
                     data-csrf-token="<?= Yii::$app->request->csrfToken; ?>">
                    <?= Html::a('<i class="glyphicon glyphicon-paperclip" style="left: -1px;"></i>', 'javascript:;', [
                        'class' => 'btn grey icn-only upload-file-chat',
                        'style' => 'margin-top:5px;float:right;background-color: #cccccc;!important',
                    ]); ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>