<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.09.2017
 * Time: 16:46
 */

use yii\helpers\Html;
use common\models\employee\Employee;
use common\models\Chat;
use yii\helpers\Url;

/* @var $user Employee
 * @var $message Chat
 * @var $dateDelimiter string
 */

if ($message->is_file) {
    $content = Html::a($message->message, Url::to(['/chat/download-file', 'id' => $message->id]));
} else {
    $content = $message->message;
}
?>
<li class="<?= $message->from_employee_id == $user->id ? 'out' : 'in'; ?>">
    <img class="avatar" alt=""
         src="<?= $message->isFromEmployeeCrm() ? '//' . $message->fromEmployeeCrm->chat_photo : $message->fromEmployee->getChatPhotoSrc(); ?>">

    <div class="message">
        <span class="arrow"> </span>
        <?php if ($message->isFromEmployeeCrm()): ?>
            <?= Html::a($message->fromEmployeeCrm->user_fio, null, [
                'class' => 'name',
            ]); ?>
        <?php else: ?>
            <?= Html::a($message->fromEmployee->getFio(), Url::to(['/employee/view', 'id' => $message->fromEmployee->employee_id]), [
                'class' => 'name',
            ]); ?>
        <?php endif; ?>
        <span class="datetime"><?= date('H:i', $message->created_at); ?></span>
        <span class="body"><?= $content; ?></span>
    </div>
</li>
