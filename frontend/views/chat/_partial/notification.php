<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.10.2017
 * Time: 10:43
 */

use yii\helpers\Url;
use common\models\Chat;

/* @var $this yii\web\View
 * @var $newMessage Chat
 */
?>
<a href="<?= Url::to(['/chat/index', 'userID' => $newMessage->from_employee_id]); ?>">
    <span class="details">
        <span class="time">
            <?= \php_rutils\RUtils::dt()->ruStrFTime([
                'date' => $newMessage->created_at,
                'format' => 'd F',
                'monthInflected' => true,
            ]); ?>
        </span>
        <span
            class="details">
            <span
                class="label label-sm label-icon label-info">
                <i class="fa fa-bullhorn"></i>
            </span>
            <span
                class="details-title">
                <?= strip_tags($newMessage->message); ?>
            </span>
        </span>
    </span>
</a>
