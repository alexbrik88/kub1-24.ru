<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.09.2017
 * Time: 4:30
 */

use common\models\EmployeeCompany;
use common\models\employee\Employee;
use yii\helpers\Url;
use yii\bootstrap\Html;

/* @var $this yii\web\View
 * @var $employeeCompanies EmployeeCompany[]
 * @var $user Employee
 * @var $userID integer
 * @var $kubSupportEmployee Employee
 * @var $crmAccountant []
 */
$this->title = 'Чат';
?>
<div class="chat-box">
    <div class="portlet box">
        <h3 class="page-title"><?= $this->title; ?></h3>
    </div>
    <div class="row">
        <?= Html::a('Получать уведомления о новом сообщении?', 'javascript:;', [
            'class' => 'notification-request-permission col-md-12 hidden',
        ]); ?>
        <div class="col-md-4">
            <div class="portlet light bordered contacts">
                <div class="portlet-title">
                    <div class="portlet-input input-inline" style="width: 104%;">
                        <div class="input-icon right">
                            <i class="icon-magnifier" style="right: 12px;"></i>
                            <?= Html::textInput('searchMessage', null, [
                                'class' => 'form-control input-circle search-message',
                                'placeholder' => 'Поиск...',
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="inbox-sidebar">
                    <div class="scroller-chat-contacts" data-always-visible="1" data-rail-visible1="1"
                         data-initialized="1">
                        <ul class="inbox-contacts">
                            <?php if ($user->id == Employee::SUPPORT_KUB_EMPLOYEE_ID): ?>
                                <?php foreach ($kubSupportEmployee->getUsersWithMessages() as $userWithMessage): ?>
                                    <li>
                                        <a href="javascript:;" data-id="<?= $userWithMessage->id; ?>"
                                           class="<?= $userWithMessage->id == $userID ? 'active' : ''; ?>"
                                           data-url="<?= Url::to(['change-employee', 'id' => $userWithMessage->id]); ?>">
                                            <img class="contact-pic" src="<?= $userWithMessage->getChatPhotoSrc(); ?>">
                                            <span class="contact-name">
                                                <?php if ($employeeCompany = $userWithMessage->currentEmployeeCompany) : ?>
                                                    <?= $employeeCompany->getFio(); ?>
                                                    <br>
                                                    <span>
                                                        <?= $employeeCompany->employeeRole->name ?>,
                                                        <?= $employeeCompany->company->getShortName() ?>
                                                    </span>
                                                <?php else : ?>
                                                    <?= $userWithMessage->getFio() ?>
                                                <?php endif ?>
                                            </span>
                                            <span class="contact-status <?= $userWithMessage->isOnline() ? 'bg-green' : 'bg-red'; ?>"></span>
                                <span class="badge badge-danger"
                                      style="float: right;display: <?= $user->getUnreadMessagesCount($userWithMessage->id) ? 'block' : 'none'; ?> ;">
                                    <?= $user->getUnreadMessagesCount($userWithMessage->id); ?>
                                </span>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php foreach ($employeeCompanies as $employeeCompany): ?>
                                    <?php $addSupportKubEmployee = $employeeCompany->employee_id !== Employee::SUPPORT_KUB_EMPLOYEE_ID; ?>
                                    <li>
                                        <a href="javascript:;" data-id="<?= $employeeCompany->employee_id; ?>"
                                           class="<?= $employeeCompany->employee_id == $userID ? 'active' : ''; ?>"
                                           data-url="<?= Url::to(['change-employee', 'id' => $employeeCompany->employee_id]); ?>">
                                            <img class="contact-pic"
                                                 src="<?= $employeeCompany->employee->getChatPhotoSrc(); ?>">
                        <span class="contact-name">
                            <?= $employeeCompany->getFio(); ?>
                            <br>
                            <span>
                                <?= $employeeCompany->employee_id == Employee::SUPPORT_KUB_EMPLOYEE_ID ?
                                    'Техническая поддержка КУБ' :
                                    $employeeCompany->employeeRole->name; ?>
                            </span>
                        </span>
                            <span
                                class="contact-status <?= $employeeCompany->employee->isOnline() ? 'bg-green' : 'bg-red'; ?>"></span>
                                <span class="badge badge-danger"
                                      style="float: right;display: <?= $user->getUnreadMessagesCount($employeeCompany->employee_id) ? 'block' : 'none'; ?> ;">
                                    <?= $user->getUnreadMessagesCount($employeeCompany->employee_id); ?>
                                </span>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                                <?php if ($crmAccountant): ?>
                                    <li>
                                        <a href="javascript:;" data-id="crm-<?= $crmAccountant['user_id']; ?>"
                                           class="<?= 'crm-' . $crmAccountant['user_id'] == $userID ? 'active' : ''; ?>"
                                           data-url="<?= Url::to(['change-employee', 'id' => 'crm-' . $crmAccountant['user_id']]); ?>">
                                            <img class="contact-pic" src="//<?= $crmAccountant['chat_photo']; ?>">
                        <span class="contact-name">
                            <?= $crmAccountant['user_fio']; ?>
                            <br>
                            <span>
                                Бухгалтер CRM
                            </span>
                        </span>
                            <span
                                class="contact-status <?= $crmAccountant['is_online'] ? 'bg-green' : 'bg-red'; ?>"></span>
                                        <span class="badge badge-danger"
                                              style="float: right;display: <?= $user->getUnreadMessagesCount($employeeCompany->employee_id) ? 'block' : 'none'; ?> ;">
                                    <?= $user->getUnreadMessagesCount($crmAccountant['user_id']); ?>
                                </span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?= $this->render('_partial/chat', [
                'user' => $user,
                'userID' => $userID,
            ]); ?>
        </div>
        <div class="message-template" style="display: none;">
            <img class="avatar" alt="" src="">

            <div class="message">
                <span class="arrow"></span>
                <?= Html::a(null, 'javascript:;', [
                    'class' => 'name',
                ]); ?>
                <span class="datetime"></span>
                <span class="body"></span>
            </div>
        </div>
    </div>
</div>