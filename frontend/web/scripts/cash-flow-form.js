$(function () {
    $(document).on("change", "form.update-movement-form input.flow-type-toggle-input[type=radio]", function() {
        var form = this.form;
        var value = $("input.flow-type-toggle-input:checked", form).val();
        if ($(".flow-type-toggle", form).length > 0) {
            $(".has-error", form).removeClass("has-error");
            $(".help-block.help-block-error", form).html("");
            $('.flow-type-toggle', form).addClass('hidden');
            $('.flow-type-toggle input, .flow-type-toggle select', form).prop('disabled', true);
            if (value == $(form).data('type-expense')) {
                $('.flow-type-toggle.expense', form).removeClass('hidden');
                $('.flow-type-toggle.expense input, .flow-type-toggle.expense select', form).prop('disabled', false);
            } else if (value == $(form).data('type-income')) {
                $('.flow-type-toggle.income', form).removeClass('hidden');
                $('.flow-type-toggle.income input, .flow-type-toggle.income select', form).prop('disabled', false);
            }
        }
    });
    $(document).on("change", "#cashorderflows-cashbox_id", function() {
        var targetInput = $($(this).data("target"));
        var targetValue = $(this).data("prefix")+this.value;
        if (targetInput.val() == targetValue) {
            $("option[value='"+targetValue+"']", targetInput).prop("selected", false);
        }
        $("option:disabled", targetInput).prop("disabled", false);
        $("option[value='"+targetValue+"']", targetInput).prop("disabled", true);
        if (targetInput.data("select2")) {
            targetInput.select2("destroy");
        }
        $.when(targetInput.select2(eval(targetInput.data("krajee-select2"))))
            .done(initS2Loading("cashorderflows-contractorinput", eval(targetInput.data("s2-options"))));
    });
    $(document).on("change", "select[data-tax-items]", function() {
        var form = this.form;
        var value = this.value;
        var items = eval($(this).data('tax-items'));
        if (items.indexOf(value) === -1) {
            $('.tax-payment-fields', form).addClass('hidden');
        } else {
            $('.tax-payment-fields', form).removeClass('hidden');
        }
    });
    $(document).on("change", "select.contractor-items-depend", function(event) {
        const form = this.form;
        const selectIncomeItems = $("select.flow-income-items", form);
        const selectExpenseItems = $("select.flow-expense-items", form);
        const selectProject = $("select.flow-project-id", form);

        if (+$(this).val() > 0) {
            $.get($(this).data("items-url").replace("_cid_", $(this).val()), function (data) {
                if (selectIncomeItems.length) {
                    selectIncomeItems.val(data.income).trigger("change");
                }
                if (selectExpenseItems.length) {
                    selectExpenseItems.val(data.expense).trigger("change");
                }
                if (selectProject.length && !selectProject.prop('disabled')) {
                    selectProject.val(data.project).trigger('change');
                }
            });
        } else if (
            this.value === 'company' ||
            this.value.substr(0, 4) === "bank" ||
            this.value.substr(0, 5) === "order" ||
            this.value.substr(0, 6) === "emoney") {

            selectIncomeItems.val(9).trigger("change");
            selectExpenseItems.val(44).trigger("change");
            selectProject.val(null).trigger("change");

        } else {

            selectProject.val(null).trigger("change");
        }
    });
});