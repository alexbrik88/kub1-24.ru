var vidimusUploader1c; // banking
var vidimusUploader;   // excel
var vidimusUploader2;  // ofd
var vidimusUploader1cXls;
var vidimusFileNames = [];

function validateFile(fileExtension, fileSize, availableFileExtensions) {
    var maxUploadedFileSize = 15360;
    fileValid = true;

    if (fileSize > maxUploadedFileSize) {
        $('.vidimus-error').html('Превышен максимальный размер файла. Допустимый размер файла: 15 Мб');
        $('.vidimus-error').show();
        fileValid = false;
    }

    if ($.inArray(fileExtension, availableFileExtensions) == -1) {
        $('.vidimus-error').html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + availableFileExtensions.join(','));
        $('.vidimus-error').show();
        fileValid = false;
    }

    if (fileValid) {
        $('.vidimus-error').hide();
    }
    return fileValid;
}

function initialiseVidimusUploader() {
    vidimusUploader1c = new ss.SimpleUpload({
        button: $('.add-vidimus-file'),
        url: $('#vidimus-ajax-form').attr('action'), // server side handler
        progressUrl: '/cash/banking/default/file-upload-progress/', // enables cross-browser progress support (more info below)
        responseType: 'json',
        name: 'uploadfile',
        maxSize: 15360,
        allowedExtensions: ['txt'], // for example, if we were uploading pics
        hoverClass: 'ui-state-hover',
        focusClass: 'ui-state-focus',
        disabledClass: 'ui-state-disabled',
        form: $('#vidimus-ajax-form'),
        onSizeError: function ( filename, fileSize ) {
            var error = "Допустимый максимальный размер файла 2МБ.";
            $('.vidimus-error').html(error);
            $('.vidimus-error').show();
        },
        onSubmit: function (filename, extension) {
            var progress = document.createElement('div'), // container for progress bar
                bar = document.createElement('div'), // actual progress bar
                fileSize = document.createElement('div'), // container for upload file size
                wrapper = document.createElement('div'), // container for this progress bar
                progressBox = document.getElementById('progressBox'); //on page container for progress bars

            $('.vidimus-hr').hide();
            $('.vidimus-error').hide();

            // Assign each element its corresponding class
            progress.className = 'progress progress-striped';
            bar.className = 'progress-bar progress-bar-success';
            fileSize.className = 'size';
            wrapper.className = 'wrapper';

            // Assemble the progress bar and add it to the page
            progress.appendChild(bar);
            wrapper.innerHTML = '<div class="name">' + filename + '</div>'; // filename is passed to onSubmit()
            wrapper.appendChild(progress);
            progressBox.appendChild(wrapper); // just an element on the page to hold the progress bars

            // Assign roles to the elements of the progress bar
            this.setProgressBar(bar); // will serve as the actual progress bar
            this.setFileSizeBox(fileSize); // display file size beside progress bar
            this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
        },
        onChange: function (filename, extension, uploadBtn, fileSize, file) {
            $('.upload-xls').hide();
            $('.upload-1C-hide').hide();
            $('.upload-1C-show').show();
            var maxFilesCount = 1;
            var filesQueueKey = vidimusUploader1c.getQueueSize();
            var filesCount = filesQueueKey + 1;

            if (!validateFile(extension, fileSize, ['txt'])) {
                this.removeCurrent();
                return false;
            }

            $('.upload-button a').removeClass('disabled');

            if (filesCount == maxFilesCount) {
                $('.add-vidimus-file').addClass('disabled');
            }

            var fileItemHtml = $('<div class="item">'
                + '<div class="file-name">' + filename + '</div>'
                + '<a href="#" data-queue="' + filesQueueKey + '" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                + '<div></div>'
                + '</div>');

            $('#vidimus-ajax-form .file-list').append(fileItemHtml);
            $('#upload-1C-actions').show();
            $('#bank-info').html('');
            $('#vidimus-table').html('');
        },
        onComplete: function (filename, data) {
            if (data.success) {
                $('#vidimus-table').html(data.html);
                $('#bank-info').html('<div>' + data.bankInfo.name + '</div><div>р/c ' + data.bankInfo.number + '</div>');
                $('.upload-button').hide();
                $('#save').click(function (e) {
                    e.preventDefault();
                    $('#vidimus_form_ajax').submit();
                });
            } else {
                if (data.error_code == 100) {
                    $('.vidimus-error').html(data.error);
                }
                $('.vidimus-error').show();
            }
        }
    });
}

function initialiseVidimusXlsUploader() {
    vidimusUploader1cXls = new ss.SimpleUpload({
        button: $('.add-vidimus-xls-file'),
        url: $('#vidimus-ajax-form-xls').attr('action'), // server side handler
        progressUrl: '/cash/banking/default/file-upload-progress/', // enables cross-browser progress support (more info below)
        responseType: 'json',
        name: 'uploadfile',
        maxSize: 15360,
        allowedExtensions: ['xlsx'], // for example, if we were uploading pics
        hoverClass: 'ui-state-hover',
        focusClass: 'ui-state-focus',
        disabledClass: 'ui-state-disabled',
        form: $('#vidimus-ajax-form-xls'),
        onSizeError: function ( filename, fileSize ) {
            var error = "Допустимый максимальный размер файла 2МБ.";
            $('.vidimus-error').html(error);
            $('.vidimus-error').show();
        },
        onSubmit: function (filename, extension) {
            var progress = document.createElement('div'), // container for progress bar
                bar = document.createElement('div'), // actual progress bar
                fileSize = document.createElement('div'), // container for upload file size
                wrapper = document.createElement('div'), // container for this progress bar
                progressBox = document.getElementById('progressBox'); //on page container for progress bars

            $('.vidimus-hr').hide();
            $('.vidimus-error').hide();

            // Assign each element its corresponding class
            progress.className = 'progress progress-striped';
            bar.className = 'progress-bar progress-bar-success';
            fileSize.className = 'size';
            wrapper.className = 'wrapper';

            // Assemble the progress bar and add it to the page
            progress.appendChild(bar);
            wrapper.innerHTML = '<div class="name">' + filename + '</div>'; // filename is passed to onSubmit()
            wrapper.appendChild(progress);
            progressBox.appendChild(wrapper); // just an element on the page to hold the progress bars

            // Assign roles to the elements of the progress bar
            this.setProgressBar(bar); // will serve as the actual progress bar
            this.setFileSizeBox(fileSize); // display file size beside progress bar
            this.setProgressContainer(wrapper); // designate the containing div to be removed after upload
        },
        onChange: function (filename, extension, uploadBtn, fileSize, file) {
            $('.upload-txt').hide();
            $('.upload-1C-hide').hide();
            $('.upload-1C-show').show();
            var maxFilesCount = 1;
            var filesQueueKey = vidimusUploader1cXls.getQueueSize();
            var filesCount = filesQueueKey + 1;

            if (!validateFile(extension, fileSize, ['xlsx'])) {
                this.removeCurrent();
                return false;
            }

            $('.upload-button a').removeClass('disabled');

            if (filesCount == maxFilesCount) {
                $('.add-vidimus-xls-file').addClass('disabled');
            }

            var fileItemHtml = $('<div class="item">'
                + '<div class="file-name">' + filename + '</div>'
                + '<a href="#" data-queue="' + filesQueueKey + '" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                + '<div></div>'
                + '</div>');

            $('#vidimus-ajax-form-xls .file-list').append(fileItemHtml);
            $('#upload-1C-actions').show();
            $('#bank-info').html('');
            $('#vidimus-table').html('');
        },
        onComplete: function (filename, data) {
            if (data.success) {
                $('#vidimus-table').html(data.html);
                $('#bank-info').html('<div>' + data.bankInfo.name + '</div><div>р/c ' + data.bankInfo.number + '</div>');
                $('.upload-button').hide();
                $('#save').click(function (e) {
                    e.preventDefault();
                    $('#vidimus_form_ajax').submit();
                });
            } else {
                if (data.error_code == 100) {
                    $('.vidimus-error').html(data.error);
                }
                $('.vidimus-error').show();
            }
        }
    });
}

function initXlsUpload(modalID) {

    var $xlsError;
    var $xlsButton;
    var $createdModels;

    if (modalID) {
        $xlsError = $(modalID).find('.xls-error');
        $xlsButton = $(modalID).find('.xls-buttons');
        $createdModels = $(modalID).find('.created-models');
    } else {
        $xlsError = $('.xls-error');
        $xlsButton = $('.xls-buttons');
        $createdModels = $('.created-models');
    }

    vidimusUploader = new ss.SimpleUpload({
        button: $('.add-xls-file'),
        url: '/xls/upload/', // server side handler
        progressUrl: '/xls/progress/', // enables cross-browser progress support (more info below)
        responseType: 'json',
        name: 'uploadfile',
        allowedExtensions: ['xls', 'xlsx'], // for example, if we were uploading pics
        hoverClass: 'ui-state-hover',
        focusClass: 'ui-state-focus',
        disabledClass: 'ui-state-disabled',
        form: $('#xls-ajax-form'),
        onSubmit: function (filename, extension) {
            var progress = document.createElement('div'),
                bar = document.createElement('div'),
                fileSize = document.createElement('div'),
                wrapper = document.createElement('div'),
                progressBox = (modalID) ? $(modalID).find('.progressBox')[0] : $('#progressBox')[0];
            $xlsError.hide();
            progress.className = 'progress progress-striped';
            bar.className = 'progress-bar progress-bar-success';
            fileSize.className = 'size';
            wrapper.className = 'wrapper';
            progress.appendChild(bar);
            wrapper.innerHTML = '<div class="name">' + filename + '</div>';
            wrapper.appendChild(progress);
            progressBox.appendChild(wrapper);
            this.setProgressBar(bar);
            this.setFileSizeBox(fileSize);
            this.setProgressContainer(wrapper);
        },
        onChange: function (filename, extension, uploadBtn, fileSize, file) {
            var $maxFilesCount = 1;
            var $filesQueueKey = vidimusUploader.getQueueSize();
            var $filesCount = $filesQueueKey + 1;
            if (!validateXls($xlsError, extension, fileSize)) {
                this.removeCurrent();
                return false;
            }
            $xlsButton.hide();
            $('.upload-xls-template-url').hide();
            $('.xls-title').hide();
            $('.upload-xls-button a').removeClass('disabled');
            $('.upload-xls-button a.tooltipstered').tooltipster('disable');
            if ($filesCount == $maxFilesCount) {
                $('.add-xls-file').addClass('disabled');
            }
            var $fileItemHtml = $('<div class="item">'
                + '<div class="file-name">' + filename + '</div>'
                + '<a href="#" data-queue="0" class="delete-file"><i class="fa fa-times-circle"></i></a>'
                + '<div></div>'
                + '</div>');
            $('.file-list').append($fileItemHtml);
        },
        onComplete: function (filename, data) {
            if (data.invoiceData.items) {
                $('#import-xls').modal('hide');
                $('#add-new').modal('hide');
                INVOICE.addProductToTable(data.invoiceData.items);
            } else {
                $xlsError.html(data.message);
                if (data.result == true) {
                    $xlsError.css('background-color', '#45b6af');
                }
                //$xlsButton.show();
                $xlsError.show();
                $createdModels.val(data.successProducts);
                $('#import-xls').on('hidden.bs.modal', function (e) {
                    if ($('.price-list-panel').is(':visible')) {
                        $('.block-pricelist-add-products').hide();
                        $('#create-price-list-form .submit').removeClass('tooltip2').removeAttr('disabled');
                        location.href = location.href;
                    } else {
                        location.href = location.href;
                    }
                })
            }
        }
    });
}

// Upload from OFD
function initXlsUpload2(modalID) {

    var $xlsError;
    var $xlsButton;
    var $createdModels;

    if (modalID) {
        $xlsError = $(modalID).find('.xls-error-2');
        $xlsButton = $(modalID).find('.xls-buttons-2');
        $createdModels = $(modalID).find('.created-models-2');
    } else {
        $xlsError = $('.xls-error-2');
        $xlsButton = $('.xls-buttons-2');
        $createdModels = $('.created-models-2');
    }

    vidimusUploader2 = new ss.SimpleUpload({
        button: $('.add-xls-file-2'),
        url: '/xls/upload/', // server side handler
        progressUrl: '/xls/progress/', // enables cross-browser progress support (more info below)
        responseType: 'json',
        name: 'uploadfile',
        allowedExtensions: ['xls', 'xlsx'], // for example, if we were uploading pics
        hoverClass: 'ui-state-hover',
        focusClass: 'ui-state-focus',
        disabledClass: 'ui-state-disabled',
        form: $('#xls-ajax-form-2'),
        onSubmit: function (filename, extension) {
            var progress = document.createElement('div'),
                bar = document.createElement('div'),
                fileSize = document.createElement('div'),
                wrapper = document.createElement('div'),
                progressBox = (modalID) ? $(modalID).find('.progressBox2')[0] : $('#progressBox2')[0];
            $xlsError.hide();
            progress.className = 'progress progress-striped';
            bar.className = 'progress-bar progress-bar-success';
            fileSize.className = 'size';
            wrapper.className = 'wrapper';
            progress.appendChild(bar);
            wrapper.innerHTML = '<div class="name">' + filename + '</div>';
            wrapper.appendChild(progress);
            progressBox.appendChild(wrapper);
            this.setProgressBar(bar);
            this.setFileSizeBox(fileSize);
            this.setProgressContainer(wrapper);
        },
        onChange: function (filename, extension, uploadBtn, fileSize, file) {
            $('.upload-1C-hide').hide();
            $('.upload-1C-show').show();
            var $maxFilesCount = 1;
            var $filesQueueKey = vidimusUploader2.getQueueSize();
            var $filesCount = $filesQueueKey + 1;
            if (!validateXls($xlsError, extension, fileSize)) {
                this.removeCurrent();
                return false;
            }

            $('.upload-button a').removeClass('disabled');

            $xlsButton.hide();
            $('.upload-xls-template-url').hide();
            $('.xls-title').hide();
            $('.upload-xls-button-2 a').removeClass('disabled');
            if ($filesCount == $maxFilesCount) {
                $('.add-xls-file-2').addClass('disabled');
            }
            var $fileItemHtml = $('<div class="item">'
                + '<div class="file-name">' + filename + '</div>'
                + '<a href="#" data-queue="0" class="delete-file-2"><i class="fa fa-times-circle"></i></a>'
                + '<div></div>'
                + '</div>');
            $('.file-list-2').append($fileItemHtml);
        },
        onComplete: function (filename, data) {
            if (data.invoiceData.items) {
                $('#ofd-upload').modal('hide');
            } else {
                $xlsError.html(data.message);
                if (data.result == true) {
                    $xlsError.css('background-color', '#45b6af');
                }
                $xlsError.show();
                $createdModels.val(data.successProducts);
            }

            $('#ofd-module-modal').on('hidden.bs.modal', function (e) {
                location.href = location.href.replace('&modal=1', '');
            });

        }
    });
}

$('#import-xls').on('hidden', function (e) {
    $('.xls-title').show();
    $('.upload-xls-template-url').show();
    $xlsError.empty().hide();
    $xlsButton.hide();
    $createdModels.val('');
    $('.add-xls-file').removeClass('disabled');
    $('.upload-xls-button a').addClass('disabled');
    $('.upload-xls-button a.tooltipstered').tooltipster('enable');
    $('.file-list').empty();
});

function validateXls($xlsError, fileExtension, fileSize) {
    var maxUploadedFileSize = 3072;
    var $extensions = ['xls', 'xlsx'];
    var $validation = true;

    if (fileSize > maxUploadedFileSize) {
        $xlsError.html('Превышен максимальный размер файла. Допустимый размер файла: 3 Мб');
        $xlsError.show();
        $validation = false;
    }

    if ($.inArray(fileExtension, $extensions) == -1) {
        $xlsError.html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + $extensions.join(', '));
        $xlsError.show();
        $validation = false;
    }

    if ($validation) {
        $xlsError.hide();
    }
    return $validation;
}

$(document).ready(function () {
    $('body').on('click', '.vidimus-cancel', function (e) {
        e.preventDefault();

        $('#vidimus-table').remove();
        $('.close').click();
    });

    $(document).on('click', '.vidimus-type', function (e) {
        e.preventDefault();
        let $items = $('.vidimus-type-item', $(this).closest('.vidimus-type').parent()).first();
        if ($items.hasClass('hidden')) {
            $('.vidimus-type-item:not(.hidden)').toggleClass('hidden', true);
        }
        $items.toggleClass('hidden');
    });

    $('body').on('click', '#banking-module-modal', function(e) {
        if (e.target.closest('.vidimus-type')) return;
        $('.vidimus-type-item:not(.hidden)').toggleClass('hidden', true);
    });

    $(document).on('click', '.vidimus-type-item div[data-id]', function (e) {
        e.preventDefault();
        $(this).parent().prev().find('.vidimus-type-title').html($(this).html());
        $(this).parent().toggleClass('hidden', true);
        $(this).parent().prev().removeClass('red-vidimus');
        $(this).closest('tr').find('input.vidimus_type').val($(this).data('id'));
    });

    $('body').on('click', '#vidimus-ajax-form .delete-file', function (e) {
        e.preventDefault();

        $(this).parent().remove();
        vidimusUploader1c['_queue'].splice($(this).data('queue'), 1);
        $('.add-vidimus-file').removeClass('disabled');
        if (vidimusUploader1c.getQueueSize() == 0) {
            $('.upload-button a').addClass('disabled');
            $.pjax.reload({
                "container": "#banking-module-pjax",
                "timeout": 5000,
                "url": "/cash/bank/index-upload"
            });
        }
    });

    $('body').on('click', '#vidimus-ajax-form .delete-file', function (e) {
        e.preventDefault();

        $(this).parent().remove();
        vidimusUploader1cXls['_queue'].splice($(this).data('queue'), 1);
        $('.add-vidimus-xls-file').removeClass('disabled');
        if (vidimusUploader1cXls.getQueueSize() == 0) {
            $('.upload-button a').addClass('disabled');
            $.pjax.reload({
                "container": "#banking-module-pjax",
                "timeout": 5000,
                "url": "/cash/bank/index-upload"
            });
        }
    });

    $(document).on('click', '.upload-button a', function (e) {
        e.preventDefault();
        $('.file-list .item').remove();
        $('.upload-button a').addClass('disabled');
        $('.bank-info').html('');
        $('#vidimus-table').html('');
        var txtForm = $('#vidimus-ajax-form:visible');
        var xlsForm = $('#vidimus-ajax-form-xls:visible');
        if (txtForm.length) {
            $('.add-vidimus-file').removeClass('disabled');
            txtForm.submit();
        }
        if (xlsForm.length) {
            $('.add-vidimus-xls-file').removeClass('disabled');
            xlsForm.submit();
        }
    });

    $(document).on('click', '.upload-xls-button a', function (e) {
        e.preventDefault();
        $('.file-list .item').remove();
        $('.upload-xls-button a').addClass('disabled');
        $('.upload-xls-button a.tooltipstered').tooltipster('enable');
        $('.add-xls-file').removeClass('disabled');
        $('#xls-ajax-form').submit();
    });

    if ($('.add-vidimus-file').length) {
        initialiseVidimusUploader();
    }
    if ($('.add-vidimus-xls-file').length) {
        initialiseVidimusXlsUploader();
    }
    if ($('.add-xls-file').length) {
        $('.add-xls-file').each(function() {
            let modalID = $(this).data('modal-id') || null;
            initXlsUpload(modalID);
        });
    }
    if ($('.add-xls-file-2').length) {
        $('.add-xls-file-2').each(function() {
            console.log($(this), $(this).data('modal-id'));
            let modalID = $(this).data('modal-id') || null;
            initXlsUpload2(modalID);
        });
    }

    if ($('.add-xml-file-2').length) {
        $('.add-xml-file-2').each(function() {
            console.log($(this), $(this).data('modal-id'));
            const modalID = $(this).data('modal-id') || null;
            MoyskadImport.init(modalID);
        });
    }

    $('.file-list').on('click', '.delete-file', function (e) {
        const modal = $(this).closest('.modal') || $('body');
        e.preventDefault();
        $(this).parent().remove();
        $('.xls-title').show();
        if (vidimusUploader) {
            vidimusUploader['_queue'].splice(0, 1);
        }
        if (vidimusUploader1c) {
            vidimusUploader1c['_queue'].splice(0, 1);
        }
        if (vidimusUploader1cXls) {
            vidimusUploader1cXls['_queue'].splice(0, 1);
        }
        $(modal).find('.upload-xls-template-url').show();
        $(modal).find('.upload-xls-button a').addClass('disabled');
        $(modal).find('.upload-xls-button a.tooltipstered').tooltipster('enable');
        $(modal).find('.add-xls-file').removeClass('disabled');
    });
});

// Upload from Moysklad

MoyskadImport = {
    modalID: '#import-moysklad',
    importID: null,
    totalSteps: 8,
    progressTimer: {
        id: null,
        tick: 3000
    },
    pseudoProgressTimer: {
        id: null,
        tick: 1000
    },
    step: {
        percent: 0,
        description: '',
        diff: 0.1
    },
    init: function(modalID) {
        this.modalID = modalID;
        this.initXmlUpload2();
        this.bindEvents();
    },
    bindEvents: function() {
        const modalID = this.modalID;
        $(modalID).find(".upload-xml-button-2").on("click", function() {
            if ($(this).hasClass("disabled"))
                return;

            $("#xml-ajax-form-2").submit();
        });
        $(document).on('click', modalID + ' .delete-file-2', function (e) {
            e.preventDefault();
            $(this).parent().remove();
            vidimusUploader2['_queue'].splice(0, 1);
            $(modalID).find('.upload-xml-button-2 a').addClass('disabled');
            $(modalID).find('.add-xml-file-2').removeClass('disabled');
        });
    },
    showProgressBar: function() {
        const modalID = this.modalID;
        const progressBar = $(modalID).find('.progress-status');
        $(progressBar).show();
        // disable close modal
        $(modalID).find('.modal-close').prop('disabled', true);
    },
    hideProgressBar: function() {
        const modalID = this.modalID;
        const progressBar = $(modalID).find('.progress-status');
        $(progressBar).hide();
    },
    updateProgressBar: function(percent, description) {
        const that = this;
        const modalID = that.modalID;
        const progressBar = $(modalID).find('.progress-status');
        const progressDescription = $(modalID).find('.progress-description');
        if (percent !== undefined) {
            $(progressBar).find('.progress-bar').css({width: percent + '%'});
            that.step.percent = percent;
        }
        if (description !== undefined) {
            $(progressDescription).html(description);
            that.step.description = description;
        }
    },
    initXmlUpload2: function() {
        const that = this;
        const modalID = that.modalID;
        const $xmlError = $(modalID).find('.xml-error-2');
        const $xmlButton = $(modalID).find('.xml-buttons-2');

        vidimusUploader2 = new ss.SimpleUpload({
            button: $('.add-xml-file-2'),
            url: '/product-ajax/moysklad-upload-file/', // server side handler
            responseType: 'json',
            name: 'uploadfile',
            allowedExtensions: ['xml'], // for example, if we were uploading pics
            hoverClass: 'ui-state-hover',
            focusClass: 'ui-state-focus',
            disabledClass: 'ui-state-disabled',
            form: $('#xml-ajax-form-2'),
            onSubmit: function (filename, extension) {
                that.updateProgressBar(0, 'Загрузка файла...');
                that.showProgressBar();
                $xmlError.hide();
                $(modalID).find('.delete-file-2').each(function() {
                    $(this).parent().remove();
                });
                $('.hide-on-process').addClass('hidden');
                $('.show-on-process').removeClass('hidden');
                $(modalID).find('.add-xml-file-2').addClass('disabled');
                $(modalID).find('.upload-xml-button-2 a').addClass('disabled');
                $(modalID).find('#moysklad_store_id').prop('disabled', true);
                $(modalID).find('#moysklad_document_format_id').prop('disabled', true);
            },
            onProgress: function(percent) {
                that.updateProgressBar(percent / that.totalSteps);
            },
            onComplete: function (filename, data) {
                if (data.result == true) {
                    $xmlError.css('background-color', '#45b6af');
                    that.updateProgressBar(1 / that.totalSteps * 100, data.step_description);
                    that.startParseFile();
                } else {
                    $xmlError.html(data.message);
                    $xmlError.css('background-color', '#ff8795');
                    $xmlError.show();
                }
                // statistics /////////////////////////////////////////////////////
                if (data.html_statistics)
                    $(modalID).find('.html_statistics').html(data.html_statistics);
                ///////////////////////////////////////////////////////////////////
            },
            onChange: function (filename, extension, uploadBtn, fileSize, file) {
                var $maxFilesCount = 1;
                var $filesQueueKey = vidimusUploader2.getQueueSize();
                var $filesCount = $filesQueueKey + 1;
                if (!that.validateXml($xmlError, extension, fileSize)) {
                    this.removeCurrent();
                    return false;
                }

                $('.upload-button a').removeClass('disabled');

                $xmlButton.hide();
                $('.upload-xml-template-url').hide();
                $('.xml-title').hide();
                $('.upload-xml-button-2 a').removeClass('disabled');
                if ($filesCount == $maxFilesCount) {
                    $('.add-xml-file-2').addClass('disabled');
                }
                var $fileItemHtml = $('<div class="item">'
                    + '<div class="file-name">' + filename + '</div>'
                    + '<a href="#" data-queue="0" class="delete-file-2"><i class="fa fa-times-circle"></i></a>'
                    + '<div></div>'
                    + '</div>');
                $('.file-list-2').append($fileItemHtml);
                that.updateProgressBar(0, '');
                that.hideProgressBar();
                $(that.modalID).find('.html_statistics').html('');
                $(that.modalID).find('.html_validation').hide();
                $(that.modalID).find('#html_validation_collapser').collapse('hide');
                $(that.modalID).find('.html_validation_objects').html('');
                $(that.modalID).find('.html_validation_documents').html('');
            },
        });
    },
    validateXml: function($xmlError, fileExtension, fileSize) {
        var maxUploadedFileSize = 104857600; // 100Mb
        var $extensions = ['xml'];
        var $validation = true;

        if (fileSize > maxUploadedFileSize) {
            $xmlError.html('Превышен максимальный размер файла. Допустимый размер файла: 100 Мб');
            $xmlError.show();
            $validation = false;
        }

        if ($.inArray(fileExtension, $extensions) == -1) {
            $xmlError.html('Формат загружаемого файла не поддерживается. Допустимые форматы файла: ' + $extensions.join(', '));
            $xmlError.show();
            $validation = false;
        }

        if ($validation) {
            $xmlError.hide();
        }
        return $validation;
    },
    startParseFile: function() {
        const that = this;
        const modalID = that.modalID;
        const $xmlError = $(modalID).find('.xml-error-2');
        $.ajax({
            type: "POST",
            url: '/product-ajax/moysklad-parse-file',
            data: {
                'MoyskladImport[store_id]': $('#moysklad_store_id').val(),
                'MoyskladImport[document_format_id]': $('#moysklad_document_format_id').val(),
            },
            success: function(data) {

                if (data.result == true) {
                    that.importID = data.import_id;
                    $xmlError.css('background-color', '#45b6af');
                    that.updateProgressBar(2 / that.totalSteps * 100, data.step_description);
                    that.startImport();
                } else {
                    $xmlError.html(data.message);
                    $xmlError.css('background-color', '#ff8795');
                    $xmlError.show();
                }
                // statistics /////////////////////////////////////////////////////
                if (data.html_statistics)
                    $(modalID).find('.html_statistics').html(data.html_statistics);
                ///////////////////////////////////////////////////////////////////
            },
            error: function(xhr, ajaxOptions, thrownError) {
                that.stopAllProgress();

                //alert('Internal server error!');
            }
        });

        that.startMoveProgress();
    },
    startImport: function() {
        const that = this;
        const modalID = that.modalID;
        const $xmlError = $(modalID).find('.xml-error-2');
        
        $.ajax({
            type: "GET",
            url: '/product-ajax/moysklad-import',
            data: {'import_id': that.importID},
            success: function(data) {
                // SUCCESS
                if (data.result == true) {
                    $xmlError.css('background-color', '#45b6af');
                    $xmlError.html(data.message);
                    that.updateProgressBar(100, data.step_description);
                    //$('.hide-on-process').removeClass('hidden');
                    $('.show-on-success').removeClass('hidden');
                    $(modalID).find('.add-xml-file-2').removeClass('disabled');
                    // statistics /////////////////////////////////////////////////////
                    if (data.html_statistics)
                        $(modalID).find('.html_statistics').html(data.html_statistics);
                    if (data.html_validation_objects || data.html_validation_documents) {
                        $(modalID).find('.html_validation_objects').html(data.html_validation_objects);
                        $(modalID).find('.html_validation_documents').html(data.html_validation_documents);
                    }
                    ///////////////////////////////////////////////////////////////////
                } else {
                    $xmlError.html(data.message);
                    $xmlError.css('background-color', '#ff8795');
                    $xmlError.show();
                }

                that.stopAllProgress();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                that.stopAllProgress();
                alert('Internal server error!');
            }
        });

        that.startCheckImportProgress();
    },    
    startCheckImportProgress: function() {
        const that = this;
        const modalID = that.modalID;
        that.progressTimer.id = window.setInterval(function () {
            $.ajax({
                type: "GET",
                url: '/product-ajax/moysklad-progress',
                data: {'import_id': that.importID},
                success: function(data) {
                    if (data.result == true) {
                        // success
                        if (!data.step_num) {
                            MoyskadImport.stopAllProgress();
                            data.step_num = that.totalSteps;
                        }
                        that.updateProgressBar(data.step_num / that.totalSteps * 100, data.step_description);
                        // statistics /////////////////////////////////////////////////////
                        if (data.html_statistics)
                            $(modalID).find('.html_statistics').html(data.html_statistics);
                        if (data.html_validation_objects || data.html_validation_documents) {
                            $(modalID).find('.html_validation_objects').html(data.html_validation_objects);
                            $(modalID).find('.html_validation_documents').html(data.html_validation_documents);
                        }
                        ///////////////////////////////////////////////////////////////////
                    } else {
                        $xmlError.html(data.message);
                        $xmlError.css('background-color', '#ff8795');
                        $xmlError.show();
                    }
                },
                error: function() {
                    MoyskadImport.stopAllProgress();
                    alert('Internal server error!');
                }
            });
        }, that.progressTimer.tick);
    },
    startMoveProgress: function() {
        const that = this;
        that.pseudoProgressTimer.id = window.setInterval(function () {
            that.step.percent += that.step.diff;
            MoyskadImport.updateProgressBar(that.step.percent);
        }, that.pseudoProgressTimer.tick);
    },
    stopAllProgress: function() {
        if (this.progressTimer.id)
            clearInterval(this.progressTimer.id);
        if (this.pseudoProgressTimer.id)
            clearInterval(this.pseudoProgressTimer.id);

        // enable close modal
        $(modalID).find('.modal-close').prop('disabled', false);
    }
};

$(document).on('change', 'table.vidimus-table input.select_all_items', function () {
    $('table.vidimus-table input.select_item:not(:disabled)').prop('checked', this.checked).uniform('refresh');
});
