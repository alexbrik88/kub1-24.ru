
var productPriceDropdownItemsTemplate = function(data, container) {
    container.setAttribute('data-id', data.id);
    if ($(data.element).data('editable') == '1') {
        var content = '<div class="expenditure-item-name-label">';
        content += '<i class="icon icon-pencil pull-right edit-exp-item" data-id="'+data.id+'" title="Изменить"></i>';
        if ($(data.element).data('deletable') == '1') {
            content += '<i class="icon icon-close pull-right del-exp-item" data-id="'+data.id+'" title="Удалить"></i>';
        }
        content += '<div class="item-name">'+data.text+'</div>';
        content += '</div>';
        container.innerHTML = content;
    } else {
        container.innerHTML = data.text;
    }
    return container;
}
var dleteProductPrice = function(input, target) {
    editProductPriceCancel();
    var itemId = $(target).data("id");
    var modalSelector = '#' + $(input).attr('id') + '-del-modal';
    $(modalSelector).on('show.bs.modal', function() {
        $(input).select2("close");
    });
    $(modalSelector).modal('show');
    $(modalSelector + ' .item-name').text($(target).parent().children('.item-name').text());
    $(modalSelector + ' .js-item-delete').on('click', function() {
        $.post($(input).data('delurl'), {"id":itemId}, function(data) {
            if (data.success) {
                $(input).find('option[value="'+itemId+'"]').remove();
                $(target).closest(".select2-results__option").remove();
                if ($(input).val() == itemId) {
                    $(input).val(null).trigger("change");
                }
            }
        });
    });
}
var editProductPrice = function(input, target) {
    editProductPriceCancel();
    var itemId = $(target).data('id');
    var $container = $(target).closest('.select2-results__option');
    var text = $container.find('div.item-name').html();
    var form = '<form class="expenditure-item-name-form" data-id="'+itemId+'" data-input="'+input.id+'">'
             + '<i class="icon icon-action-undo pull-right edit-exp-item-cancel" title="Отмена"></i>'
             + '<i class="icon icon-check pull-right edit-exp-item-apply" title="Сохранить"></i>'
             + '<div class="input-wrapper">'
             + '<input type="hidden" name="id" value="'+itemId+'"/>'
             + '<input type="text" name="title" value="'+text+'" class="item-name-input"/>'
             + '</div>'
             + '</form>';
    $container.addClass('editable-expenditure-item');
    $container.children('.expenditure-item-name-label').hide();
    $container.append(form);
    $container.find('form').on('submit', function(e) {
        e.preventDefault();
        return productGroupFormSubmit(input, $(this));
    });
}
var editProductPriceCancel = function() {
    $('form.expenditure-item-name-form').remove();
    $('div.expenditure-item-name-label').show();
    $('.editable-expenditure-item').removeClass('editable-expenditure-item');
}
var productPriceFormSubmit = function(input, $form) {
    var itemId = $form.data('id')
    var $container = $form.parent();
    var $input = $(input);
    $.post($input.data('editurl'), $form.serialize(), function(data) {
        if (data.success) {
            var $option = $input.find('option[value="'+itemId+'"]');
            $container.find('.item-name').text(data.name);
            $option.text(data.name);
            editProductPriceCancel();
            if ($input.data('select2')) {
                $input.select2('destroy');
            }
            $input.select2(eval($input.attr('data-krajee-select2')));
            $.when($input.select2(eval($input.attr('data-krajee-select2')))).done(initS2Loading($input.attr('id'), $input.attr('data-s2-options')));
            $input.val(itemId).trigger('change');
        } else if (data.message) {
            alert(data.message);
        } else {
            alert('Ошибка сохранения изменений.');
        }
    });
    return false;
}
var editProductPriceApply = function(input, target) {
    return productPriceFormSubmit(input, $(target).parent());
}
