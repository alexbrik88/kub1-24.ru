(function ($) {
    $(document).on('pjax:complete', '#proxy-form-container', function() {
        let form = $('#proxy-add-employee-form');
        let select = $('#proxy-proxy_person_id');
        if (form.length) {
            if (form.length && select.length) {
                let employee = form.data('employee');
                if (employee && employee.id && employee.name) {
                    select.append(new Option(employee.name, employee.id)).val(employee.id).trigger("change");
                }
            }
            let modal = form.closest('.modal');
            if (modal.length) {
                modal.modal('hide');
            }
        }
    });
})(jQuery);
