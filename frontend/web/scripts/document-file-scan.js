function initDocFileScan (maxFileUpload) {
    window.DocFileScan = {
        _container: '.document-file-scan-container',
        _sourceBtn: '.document-file-scan-source-btn',
        _sourcePopover: '.select-source-popover',
        _buttonsArea: '.document-file-scan-buttons-area',
        _uploadBtn: '.document-file-upload-btn',
        _scanBindBtn: '.free-scan-bind-button',
        _scanIndex: '.scan-document-index-free',
        _scanListModal: '.free-scan-list-modal',
        _maxFilesUpload: parseInt(maxFileUpload),
        _filesUploaded: 0,
        toggleUploadButton: function (context) {
            $('.list-container', context).removeClass('hidden');
            if ($('.file-list .file, .document-scan-list .scan', context).length < this._maxFilesUpload ) {
                $('.upload-button', context).toggleClass('hide', false);
                $('.document-file-scan-buttons-area', context).removeClass('hidden');
            } else {
                $('.upload-button', context).toggleClass('hide', true);
                $('.document-file-scan-buttons-area', context).addClass('hidden');
            }
        },
        bindScan: function (btn) {
            var $modal = $(btn).closest('.modal');
            var $container = $($modal.data('container'));
            var postData = $('input.scan-id-checker', $modal).serialize() + "&model-id=" + $container.data('model-id');
            $('.document-file-loading', $container).show();
            $.ajax({
                type : 'POST',
                url : $container.data('bind-url'),
                data : postData,
                success: function(data) {
                    if (data.content) {
                        $('.document-scan-list', $container).html($(data.content).html());
                    }
                    $('.document-file-loading', $container).hide();
                    DocFileScan.toggleUploadButton($container);
                    DocFileScan.setAllowedFilesCount();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.document-file-loading', $container).hide();
                    DocFileScan.toggleUploadButton($container);
                },
            });
        },
        delBindScan: function (btn) {
            var $btn = $(btn);
            var $list = $($btn.data('list'));
            var $container = $list.closest(this._container);
            $('.document-file-loading', $container).show();
            $.ajax({
                type : 'POST',
                url : $container.data('bind-url'),
                data : eval($btn.data('params')),
                success: function(data) {
                    if (data.content) {
                        $list.replaceWith(data.content);
                    }
                    $('.document-file-loading', $container).hide();
                    DocFileScan.toggleUploadButton($container);
                    DocFileScan.setAllowedFilesCount();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.document-file-loading', $container).hide();
                    DocFileScan.toggleUploadButton($container);
                },
            });
        },
        updateFileList: function (context) {
            $('.document-file-loading', context).show();
            $.ajax({
                url : context.data('list-url'),
                success: function(data) {
                    $('.file-list', context).html($(data).html());
                    $('.document-file-loading', context).hide();
                    DocFileScan.toggleUploadButton(context);
                    DocFileScan.setAllowedFilesCount();
                    $(".file-link-preview").tooltipster({
                        theme: ["tooltipster-kub"],
                        contentCloning: true,
                        trigger: "hover",
                        side: "left"
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.document-file-loading', context).hide();
                    DocFileScan.toggleUploadButton(context);
                    $(".file-link-preview").tooltipster({
                        theme: ["tooltipster-kub"],
                        contentCloning: true,
                        trigger: "hover",
                        side: "left"
                    });
                },
            });
        },
        initUploadBtn: function (i, container) {
            var $container = $(container);
            var postData = {};
            postData[$container.data('csrf-parameter')] = $container.data('csrf-token');
            new ss.SimpleUpload({
                button: $(DocFileScan._uploadBtn, $container).get(0), // HTML element used as upload button
                url: $container.data('upload-url'), // URL of server-side upload handler
                data: postData,
                multipart: true,
                multiple: false,
                encodeCustomHeaders: true,
                responseType: 'json',
                name: 'file', // Parameter name of the uploaded file
                onSubmit: function() {
                    $('.document-file-loading', $container).show();
                },
                onComplete: function (filename, response) {
                    $('.document-file-loading', $container).hide();
                    if (response.success) {
                    }
                    if (!response.success && response.msg) {
                        alert(response.msg);
                    }
                    DocFileScan.updateFileList($container);
                }
            });
            DocFileScan.updateFileList($container);
        },
        deleteFile: function (btn) {
            var $btn = $(btn);
            var fileId = $btn.data('file');
            var $file = $('#file-' + fileId);
            var $container = $file.closest(DocFileScan._container);
            $('.document-file-loading', $container).show();
            $.ajax($container.data('delete-url'), {
                data: {'file-id': fileId},
                method: 'POST',
                success: function (data, textStatus, jqXHR) {
                    if (!data.success && data.msg) {
                        alert(data.msg);
                    }
                    DocFileScan.updateFileList($container);
                    DocFileScan.setAllowedFilesCount();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('.document-file-loading', $container).hide();
                    alert(jqXHR.responseText);
                    DocFileScan.updateFileList($container);
                }
            });
        },
        setAllowedFilesCount: function() {
            DocFileScan._filesUploaded = $('.document-file-scan-container').find('.scan, .file').length;
            var allowedFilesCount = DocFileScan._maxFilesUpload - DocFileScan._filesUploaded;
            $('.uploaded-scans-count').html(allowedFilesCount + ' ' + (allowedFilesCount > 1 ? 'файла' : 'файл'));
        }
    };

    if ($(DocFileScan._container).length > 0) {
        $(DocFileScan._container).each(DocFileScan.initUploadBtn);

        $(document).on('click', DocFileScan._sourceBtn, function() {
            $(this).closest(DocFileScan._buttonsArea).find(DocFileScan._sourcePopover).toggleClass('hidden');
        });

        $(document).on('hidden.bs.modal', DocFileScan._scanListModal, function() {
            $('.modal-body', this).html('');
        });
        $(document).on('change', 'input.scan-id-checker', function() {
            var $context = $(this).closest(DocFileScan._scanIndex);
            if ($('input.scan-id-checker:checked', $context).length > 0) {
                $('.free-scan-bind-button', $context).prop('disabled', false);
            } else {
                $('.free-scan-bind-button', $context).prop('disabled', true);
            }
            if ($('input.scan-id-checker:checked', $context).length >= DocFileScan._maxFilesUpload - DocFileScan._filesUploaded) {
                $('input.scan-id-checker:not(":checked")').prop('disabled', true).uniform();
            } else {
                $('input.scan-id-checker:not(":checked")').prop('disabled', false).uniform();
            }
        });
        $(document).on('show.bs.modal', DocFileScan._scanListModal, function() {
            var $container = $($(this).data('container'));
            $('.modal-body', this).load($container.data('free-url'), function() {
                $('input[type=checkbox]:not(.md-check)', this).uniform();
                $(DocFileScan._sourcePopover, $container).addClass('hidden');
                $('.scan_link', this).tooltipster({
                    theme: ['tooltipster-kub'],
                    contentCloning: true,
                    side: ['right', 'left', 'top', 'bottom'],
                });
            });
            DocFileScan._filesUploaded = $('.document-file-scan-container').find('.scan, .file').length;
        });
        $(document).on('pjax:success', '#free-scan-document-pjax', function() {
            $('.scan_link', this).tooltipster({
                theme: ['tooltipster-kub'],
                contentCloning: true,
                side: ['right', 'left', 'top', 'bottom'],
            });
        });
        $(document).on('click', '.free-scan-bind-button', function() {
            DocFileScan.bindScan(this);
            $('.modal.in').modal('hide');
        });
        $(document).on('click', '.scan-list-del-button', function() {
            DocFileScan.delBindScan(this);
            $('.modal.in').modal('hide');
        });
        if ($(DocFileScan._sourceBtn).length > 0) {
            $(document).mouseup(function (e) {
                if ($(e.target).hasClass('upl_btn')) return;
                var popover = $(DocFileScan._sourcePopover + ":not(hidden)");
                if (popover && !popover.is(e.target) && popover.has(e.target).length === 0) {
                    popover.addClass("hidden");
                }
            });
        }

        $(document).off('click', '.delete-file').on('click', '.delete-file', function () {
            console.log(123);
            $('.modal:visible').modal('hide');
            DocFileScan.deleteFile(this);
        });

        $(document).on('click', '.scan-directory', function(e) {
            e.preventDefault();
            $.pjax({
                type: 'post',
                url: '/documents/scan-document/index-free',
                container: '#free-scan-document-pjax',
                push: false,
                timeout: 5000,
                data: {
                    "uploads": $(this).attr('data-uploads'),
                    "all": $(this).attr('data-all'),
                    "directory": $(this).attr('data-directory')
                }
            });
        });
    }
}
