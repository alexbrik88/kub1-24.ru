$(function () {
    var itemsContainer;
    var selectedItems = [];
    var toggleVisibleToSelect = function (el) {
        let wrapper = $(el).closest('.composite_product_simple_items');
        $('.show_product_composite_item_select', wrapper).toggleClass('hidden', true);
        $('.composite_product_item_select', wrapper).toggleClass('hidden', false);
    };
    var toggleVisibleToAddBtn = function (el) {
        let wrapper = $(el).closest('.composite_product_simple_items');
        $('.show_product_composite_item_select', wrapper).toggleClass('hidden', false);
        $('.composite_product_item_select', wrapper).toggleClass('hidden', true);
    };
    var addCompositeItem = function (id, container) {
        $.get('/product/composite-item', {id:id}, function (data) {
            $(container).append(data);
            toggleVisibleToAddBtn(container);
        });
    };
    var addCreatedCompositeItem = function (data) {
        $(itemsContainer).append(data.item);
        toggleVisibleToAddBtn(itemsContainer);
        $('.service-count-value').text(data.services);
        $('.product-count-value').text(data.goods);
        $('#simple_product_form_modal').modal('hide');
    };
    var openSimpleProductForm = function (url, container) {
        itemsContainer = container;
        $.get(url, function (data) {
            let modal = $('#simple_product_form_modal');
            $('.modal-body', modal).html(data);
            modal.modal('show');
        });
    };
    var openSimpleProductList = function (url, container) {
        itemsContainer = container;
        selectedItems = [];
        $.get(url, function (data) {
            let modal = $('#simple_product_index_modal');
            $('.modal-body', modal).html(data);
            modal.modal('show');
        });
    };
    var addselectedSimpleProducts = function () {
        if (itemsContainer.length && selectedItems.length) {
            addCompositeItem(selectedItems, itemsContainer);
        }
        $('#simple_product_index_modal').modal('hide');
    };
    var compositeItemCalc = function (input) {
        let value = input.value;
        let item = $(input).closest('.composite_product_item');
        let price = $('.composite_product_item_price', item).data('price');
        let numValue = parseFloat(value);
        if (value != numValue) {
            input.value = numValue;
        }
        let costPrice = Math.round(numValue * price * 100) / 100;
        $('.composite_product_item_cost_price', item).text(costPrice.toFixed(2));
    };
    var compositeItemSelect = function (select) {
        let value = $('option:selected', select).val() || '';
        if (value.length) {
            if (isNaN(value)) {
                return;
            } else {
                let wrapper
                let container = $(select).closest('.composite_product_simple_items').find('.exist_item_list');
                if (container.length) {
                    addCompositeItem(value, container);
                }
                $(select).val('').trigger('change');
            }
        }
    };
    $(document).on("click", ".show_product_composite_item_select", function(e) {
        toggleVisibleToSelect(this);
    });
    $(document).on("click", "._create_simple_product", function(e) {
        let url = $(this).data('url');
        let $select = $($(this).data('select'));
        let $container = $select.closest('.composite_product_simple_items').find('.exist_item_list');
        $select.select2("close");
        openSimpleProductForm(url, $container);
    });
    $(document).on("click", "._add_simple_product", function(e) {
        let url = $(this).data('url');
        let $select = $($(this).data('select'));
        let $container = $select.closest('.composite_product_simple_items').find('.exist_item_list');
        $select.select2("close");
        openSimpleProductList(url, $container);
    });
    $(document).on("createdCompositeItemSuccess", "#simple_product_form_pjax", function(e) {
        addCreatedCompositeItem(window.createdCompositeItemData);
    });
    $(document).on("change", ".product_composite_item_select", function(e) {
        compositeItemSelect(this);
    });
    $(document).on("change", "input.composite_product_item_quantity", function(e) {
        compositeItemCalc(this);
    });
    $(document).on("change", "input.simple_product_selection", function(e) {
        let val = $(this).val();
        console.log(val);
        if ($(this).is(':checked')) {
            if (selectedItems.indexOf(val) === -1) {
                selectedItems.push(val);
            }
        } else {
            var index = selectedItems.indexOf(val);
            if (index !== -1) {
                selectedItems.splice(index, 1);
            }
        }
        console.log(selectedItems);
    });
    $(document).on('pjax:end', '#simple_product_index_pjax', function(event) {
        if (selectedItems.length > 0) {
            $('input.simple_product_selection', event.target).each(function() {
                let $input = $(this);
                if (selectedItems.indexOf($input.val()) !== -1) {
                    $input.prop('checked', true).uniform('refresh');
                }
            });
        }
    });
    $(document).on("click", "#add_simple_selected_to_composite", function(e) {
        addCompositeItem(selectedItems, itemsContainer);
        $('#simple_product_index_modal').modal('hide');
        itemsContainer = null;
        selectedItems = [];
    });
    $(document).on("click", "tr.composite_product_item .composite_product_item_remove", function(e) {
        console.log(this);
        $(this).closest('tr.composite_product_item').remove();
    });
    $(document).on("change", "#create_service_type_select", function(e) {
        let url = $('option:selected', this).data('url') || '';
        if (url.length > 0) {
            $(this).closest('.create_service_type_wrap').find('a.create_service_pjax_link').attr('href', url).trigger('click');
        }
    });

    $(document).on('select2:open', '.product_composite_item_select', function (e) {
        let selectResults = $('#select2-'+this.id+'-results');
        let staticSelectResultsWrap = $($(this).data('static'));
        if (selectResults.length && staticSelectResultsWrap.length) {
            let selectResultsWrap = selectResults.parent();
            if ($('.product_composite_item_select_static_items', selectResultsWrap).length == 0) {
                selectResultsWrap.append($('.product_composite_item_select_static_items', staticSelectResultsWrap).clone());
            }
        }
    });
    $(document).on('mouseenter', '.product_composite_item_select_static_items li.select2-results__option', function() {
        $('.product_composite_item_select_static_items li.select2-results__option').removeClass('select2-results__option--highlighted');
        $(this).addClass('select2-results__option--highlighted');
    });
    $(document).on('hidden.bs.moda', '#simple_product_form_modal, #simple_product_index_modal', function(e) {
        itemsContainer = null;
        selectedItems = [];
        $('.modal-body', e.target).html('');
    });
});
