
function checkTodayDelimiter() {
    var $delimiter = '';
    var $paddingTop = 0;
    if ($('.date-delimiter:last').text().trim() !== 'Сегодня') {
        if ($('.date-delimiter').length == 0) {
            $paddingTop = '10px';
        }
        $delimiter = '<div class="date-delimiter" style="padding-top: ' + $paddingTop + ';">' +
            '<div class="box-title-info-txt clear-box" style="margin-right:0;width: 43%;display: inline-block;"></div>' +
            '<div style="width: 12%;display: inline-block;text-align: center;">Сегодня</div>' +
            '<div class="box-title-info-txt clear-box" style="margin-right:0;width: 43%;display: inline-block;"></div>' +
            '</div>';
    }

    return $delimiter;
}

function pingSever() {
    if (socket.readyState !== 1) {
        if ($pingNewMessagesXhr !== null) {
            $pingNewMessagesXhr.abort();
            clearTimeout(window.timeoutID);
        }
        $.post('/chat/get-new-messages', {existsMessages: $('input#exists-messages').val()}, function (dataArray) {
            if (dataArray.result == true) {
                $('input#exists-messages').val(dataArray.existsID);
                var $notificationMenu = $('.top-menu #header_chat_bar');
                var $inboxContacts = $('.inbox-contacts');
                var $scroller = $('div.scroller');
                var $addNotification = false;
                var $countNotifications = 0;
                dataArray.messages.forEach(function (data, i, arr) {
                    if ($inboxContacts.length > 0) {
                        var $activeEmployee = $inboxContacts.find('a.active');
                        if ($activeEmployee.data('id') == data.from_employee_id || data.type == 'out') {
                            var $template = $('.message-template');
                            var $chat = $('ul.chats');
                            var $soundPath = 'chat-send';
                            $template.find('.name').empty().append('<a class="name" href="/employee/view?id=' + data.from_employee_id + '">' + data.name + '</a>');
                            $template.find('.datetime').text(data.createdAt);
                            $template.find('.body').text(data.message);
                            $template.find('.avatar').attr('src', data.fromEmployeePhoto);
                            $chat.append(checkTodayDelimiter());
                            $chat.append('<li class="in">' + $template.html() + '</li>');
                            $scroller.slimScroll({
                                scrollTo: $scroller[0].scrollHeight
                            });
                            if (data.type != 'out') {
                                $hasVolume = data.toEmployeeHasSong;
                                $soundPath = 'chat-get';
                                $.post('/chat/view-message', {messageID: data.messageID}, function () {
                                });
                            } else {
                                $hasVolume = data.fromEmployeeHasSong;
                            }
                            if ($hasVolume == 1) {
                                new Audio('/frontend/web/songs/chat/' + $soundPath + '.mp3').play();
                            }
                        } else {
                            var $formEmployeeContact = $inboxContacts.find('a[data-id="' + data.from_employee_id + '"]');
                            if ($formEmployeeContact.length > 0) {
                                $formEmployeeContact.find('.badge-danger').text(+$formEmployeeContact.find('.badge-danger').text() + 1).show();
                            }
                            $addNotification = true;
                        }
                    } else {
                        $addNotification = true;
                    }
                    if (!('Notification' in window)) {
                        console.log('This browser does not support desktop notification');
                    } else if (Notification.permission === 'granted') {
                        var notification = new Notification('', {
                            body: 'Новое сообщение от ' + data.companyName,
                            icon: '/img/notification-logo.png'
                        });
                    } else if (Notification.permission !== 'denied') {
                        Notification.requestPermission(function (permission) {
                            if (permission === 'granted') {
                                var notification = new Notification('', {
                                    body: 'Новое сообщение от ' + data.companyName,
                                    icon: '/img/notification-logo.png'
                                });
                            }
                        });
                    }
                    if ($addNotification && $notificationMenu.length > 0) {
                        var $notificationTemplate = $('.notification-message-template');
                        $notificationTemplate.find('a').attr('href', '/chat/index?userID=' + data.from_employee_id);
                        $notificationTemplate.find('.time').text(data.notificationTime);
                        $notificationTemplate.find('span.details-title').text(data.notificationMessage);
                        $notificationMenu.find('ul.dropdown-menu-list').prepend('<li class="chat-notification" style="width:100%!important;">' + $notificationTemplate.html() + '</li>');
                        if ($notificationMenu.find('.badge-default').text() !== '') {
                            $countNotifications = +$notificationMenu.find('.badge-default').text();
                        }
                        if ($countNotifications > 0) {
                            $notificationMenu.find('.badge-default').text($countNotifications + 1);
                        } else {
                            $countNotifications = 1;
                            $notificationMenu.find('.icon-speech-bubble').after('<span class="badge badge-default">' + $countNotifications + '</span>');
                        }
                        $notificationMenu.find('ul.dropdown-menu').removeAttr('style');
                        if (data.toEmployeeHasSong == 1) {
                            new Audio('/frontend/web/songs/chat/chat-get.mp3').play();
                        }
                    }
                });
            }
        });
        window.timeoutID = setTimeout(function () {
            pingSever();
        }, 10000);
    }
}

function resizeChatInput() {
    var $chatForm = $('form#chat-form');
    if ($chatForm.length > 0) {
        $chatForm = $chatForm[0].getBoundingClientRect();
        var width;
        if ($chatForm.width) {
            width = $chatForm.width;
        } else {
            width = $chatForm.right - $chatForm.left;
        }
        $('textarea.message').closest('.input-cont').css('width', width - 51 + 'px', 'important');
    }
}

if ($('input.identity_employee_id').val() !== undefined) {
    var protocol = (location.protocol === 'https:') ? 'wss:' : 'ws:';
    socket = new WebSocket(protocol + '//' + location.host + '/chart-ws');
}

$(document).on('submit', 'form#chat-form', function (e) {
    e.preventDefault();
    var $newMessage = $(this).find('textarea.message').val();
    var $scroller = $('div.scroller');
    if ($newMessage !== '') {
        if ($('input.search-message').val() !== '') {
            $('input.search-message').val('');
            $('.chat ul.chats li').show();
            $scroller.slimScroll({
                scrollTo: $scroller[0].scrollHeight
            });
        }
        $('.chat-form textarea.message').val('');
        if (socket.readyState == 1) {
            socket.send(JSON.stringify({
                action_type: 'send-message',
                message: $newMessage,
                to_user_id: $(this).data('to-user-id'),
                company_id: $('li.current-company').attr('id')
            }));
        } else {
            $.post('send-message', {
                message: $newMessage,
                to_user_id: $(this).data('to-user-id')
            }, function (data) {
                if (data.result == true) {
                    var $scroller = $('div.scroller');
                    var $template = $('.message-template');
                    var $chat = $('ul.chats');
                    $template.find('.name').empty().append('<a class="name" href="/employee/view?id=' + data.from_user_id + '">' + data.name + '</a>');
                    $template.find('.datetime').text(data.createdAt);
                    $template.find('.body').text(data.message);
                    $template.find('.avatar').attr('src', data.fromEmployeePhoto);
                    $chat.append(checkTodayDelimiter());
                    $chat.append('<li class="out">' + $template.html() + '</li>');
                    if (data.fromEmployeeHasSong == 1) {
                        new Audio('/frontend/web/songs/chat/chat-send.mp3').play();
                    }
                    $scroller.slimScroll({
                        scrollTo: $scroller[0].scrollHeight,
                    });
                }
            });
        }
    }
});
var $pingNewMessagesXhr = null;
window.timeoutID = null;
if ($('input.identity_employee_id').val() !== undefined) {
    socket.onmessage = function (e) {
        var data = JSON.parse(e.data);
        var $notificationMenu = $('.top-menu #header_chat_bar');
        var $inboxContacts = $('.inbox-contacts');
        var $scroller = $('div.scroller');
        var $addNotification = false;
        var $countNotifications = 0;
        if ($inboxContacts.length > 0) {
            var $activeEmployee = $inboxContacts.find('a.active');
            if ($activeEmployee.data('id') == data.from_employee_id || data.type == 'out') {
                var $template = $('.message-template');
                var $chat = $('ul.chats');
                var $soundPath = 'chat-get';
                var message = data.messageType == 'file_message' ?
                    '<a href="/chat/download-file?id=' + data.id + '">' + data.message + '</a>' :
                    data.message;
                $template.find('.name').empty().append('<a class="name" href="/employee/view?id=' + data.from_employee_id + '">' + data.name + '</a>');
                $template.find('.datetime').text(data.createdAt);
                $template.find('.body').html(message);
                $template.find('.avatar').attr('src', data.fromEmployeePhoto);
                if (data.type == 'out') {
                    $hasVolume = data.fromEmployeeHasSong;
                    $soundPath = 'chat-send';
                } else {
                    $hasVolume = data.toEmployeeHasSong;
                }
                if ($hasVolume == 1) {
                    new Audio('/frontend/web/songs/chat/' + $soundPath + '.mp3').play();
                }
                $chat.append(checkTodayDelimiter());
                $chat.append('<li class="' + data.type + '">' + $template.html() + '</li>');
                $scroller.slimScroll({
                    scrollTo: $scroller[0].scrollHeight
                });
                if (data.type != 'out') {
                    $.post('/chat/view-message', {messageID: data.messageID}, function () {
                    });
                }
            } else {
                var $formEmployeeContact = $inboxContacts.find('a[data-id="' + data.from_employee_id + '"]');
                if ($formEmployeeContact.length > 0) {
                    $formEmployeeContact.find('.badge-danger').text(+$formEmployeeContact.find('.badge-danger').text() + 1).show();
                }
                $addNotification = true;
            }
        } else {
            $addNotification = true;
        }
        if (data.type == 'in') {
            if (!('Notification' in window)) {
                console.log('This browser does not support desktop notification');
            } else if (Notification.permission === 'granted') {
                var notification = new Notification('', {
                    body: 'Новое сообщение от ' + data.companyName,
                    icon: '/img/notification-logo.png'
                });
            } else if (Notification.permission !== 'denied') {
                Notification.requestPermission(function (permission) {
                    if (permission === 'granted') {
                        var notification = new Notification('', {
                            body: 'Новое сообщение от ' + data.companyName,
                            icon: '/img/notification-logo.png'
                        });
                    }
                });
            }
        }
        if ($addNotification && $notificationMenu.length > 0) {
            var $notificationTemplate = $('.notification-message-template');
            $notificationTemplate.find('a').attr('href', '/chat/index?userID=' + data.from_employee_id);
            $notificationTemplate.find('.time').text(data.notificationTime);
            $notificationTemplate.find('span.details-title').text(data.notificationMessage);
            $notificationMenu.find('ul.dropdown-menu-list').prepend('<li class="chat-notification" style="width:100%!important;">' + $notificationTemplate.html() + '</li>');
            if ($notificationMenu.find('.badge-default').text() !== '') {
                $countNotifications = +$notificationMenu.find('.badge-default').text();
            }
            if ($countNotifications > 0) {
                $notificationMenu.find('.badge-default').text($countNotifications + 1);
            } else {
                $countNotifications = 1;
                $notificationMenu.find('.icon-speech-bubble').after('<span class="badge badge-default">' + $countNotifications + '</span>');
            }
            $notificationMenu.find('ul.dropdown-menu').removeAttr('style');
            if (data.toEmployeeHasSong == 1) {
                new Audio('/frontend/web/songs/chat/chat-get.mp3').play();
            }
        }
    };
    socket.onopen = function (e) {
        socket.send(JSON.stringify({
            action_type: 'open',
            from_user_id: $('input.identity_employee_id').val()
        }));
    };
    socket.onerror = function (error) {
        if (window.timeoutID !== null) {
            clearTimeout(window.timeoutID);
        }
        pingSever();
    };
}

var $changeEmployeXhr = null;

$('.scroller').slimScroll({
    height: '300px',
    start: 'bottom',
});

$('.scroller-chat-contacts').slimScroll({
    height: '430px',
    start: 'top',
});

$(document).on('click', '.chat .chat-volume', function () {
    var $value = 1;
    if ($(this).hasClass('glyphicon-volume-up')) {
        $value = 0;
        $(this).removeClass('glyphicon-volume-up');
        $(this).addClass('glyphicon-volume-off');
    } else {
        $(this).removeClass('glyphicon-volume-off');
        $(this).addClass('glyphicon-volume-up');
    }
    $.post($(this).data('url'), {volume: $value}, function () {
    });
});

$('.inbox-contacts a').click(function (e) {
    e.preventDefault();
    var $this = $(this);
    var $notificationMenu = $('li#header_chat_bar');
    if ($changeEmployeXhr !== null) {
        $changeEmployeXhr.abort();
    }
    if (!$this.hasClass('active')) {
        $('.inbox-contacts a').removeClass('active');
        $this.addClass('active');
        $changeEmployeXhr = $.post($this.data('url'), null, function (data) {
            if (data.result == true) {
                $this.find('.badge-danger').text(0).hide();
                $('.chat').replaceWith(data.html);
                initUploadFileChat($('.chat-form'));
                resizeChatInput();
                $('.scroller').slimScroll({
                    height: '300px',
                    start: 'bottom',
                });
                var $chatNotificationsCount = +$notificationMenu.find('li.chat-notification').length;
                if ($chatNotificationsCount > 0) {
                    $notificationMenu.find('li.chat-notification').remove();
                    for (key in data.notificationArray.html) {
                        $notificationMenu.find('ul.dropdown-menu-list')
                            .append('<li class="chat-notification" style="width:100%!important;">' + data.notificationArray.html[key] + '</li>');
                    }
                    $('input#exists-messages').val(data.notificationArray.idArray);
                    $notificationMenu.find('.badge').text(data.notificationArray.countNewMessages);
                    if (data.notificationArray.countNewMessages == 0) {
                        $notificationMenu.find('.badge').hide();
                    }
                }
            }
        });
    }
});

$(document).on('keyup change', '.chat .search-message', function (e) {
    e.preventDefault();
    var $message = $(this).val();
    var $wrapper = $(this).closest('.col-md-4.actions');
    if ($(this).val() !== '') {
        $('.chat ul.chats li').each(function () {
            if ($(this).find('.body').text().toLowerCase().search($message) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
        $('.date-delimiter').each(function () {
            if ($('ul.chats li[data-delimiter="' + $(this).data('value') + '"]:visible').length > 0) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
        $('.date-delimiter:visible:first').css('padding-top', '20px');
        if ($('.chat ul.chats li:visible').length > 0) {
            $wrapper.find('.not-found').remove();
        } else {
            if ($wrapper.find('.not-found').length == 0) {
                $wrapper.append('<span class="not-found">Совпадений не найдено</span>');
            }
        }
    } else {
        $('.chat ul.chats li').show();
        $('.date-delimiter').show().css('padding-top', '0');
        $('.date-delimiter:visible:first').css('padding-top', '20px');
        $wrapper.find('.not-found').remove();
    }
});

$(document).on('keyup change', '.contacts .search-message', function (e) {
    e.preventDefault();
    var $searchContact = $(this).val();
    if ($(this).val() !== '') {
        $('.contacts ul.inbox-contacts li').each(function () {
            if ($(this).find('.contact-name').text().toLowerCase().search($searchContact.toLowerCase()) == -1) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    } else {
        $('.contacts ul.inbox-contacts li').show();
    }
});

$(document).ready(function () {
    var $notificationRequestPremission = $('.notification-request-permission');
    if ($notificationRequestPremission.length > 0) {
        if ('Notification' in window && (Notification.permission == 'denied' || Notification.permission == 'default')) {
            $notificationRequestPremission.removeClass('hidden');
        }
    }
    $notificationRequestPremission.click(function () {
        Notification.requestPermission();
    });
    $(document).on('click', '#header_chat_bar > a', function () {
        location.href = $(this).attr('href');
    });
    $(document).on('mouseenter', '#header_chat_bar', function () {
        var notificationMenu = $(this);
        if (notificationMenu.find('.badge').length > 0) {
            $.post('/employee/view-messages', { id : $userId }, function() {
                notificationMenu.find(".badge").remove();
            });
        }
    });
    resizeChatInput();
});
