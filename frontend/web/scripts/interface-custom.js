/**
 * Открывает боковую панель
 * rightPanel.fromConent(html) - открывает панель, содержащую указанный HTML-код
 * rightPanel.fromContainer(container) - открывает панель и помещает в неё указанный HTML-элемент
 * rightPanel.fromURL(url) - открывает панель и загружает в неё содержимое по указанному URL-адресу
 *
 * @type {{fromContent: (function(*): boolean), fromContainer: (function(*=): boolean), fromURL: (function(*=): boolean)}}
 */
var rightPanel = (function (contentContainer) {
    var _panel;
    var _container;
    var _prevContainer;
    var _prevContent;
    var _loader;

    function _resetContainer() {
        if (_prevContainer !== undefined && _prevContent !== undefined) {
            _prevContent.style.display = 'none';
            _prevContainer.appendChild(_prevContent);
            _prevContainer = _prevContent = undefined;
        }
    }

    _createPanel();
    _loader.hide = function () {
        this.style.display = 'none';
    };
    _loader.show = function () {
        this.style.display = 'block';
    };
    _panel.hide = function () {
        $(this).hide('fast');
    };
    _panel.show = function () {
        $(this).show('fast');
    };

    var fromURL = function (url) {
        _resetContainer();
        _loader.show();
        _panel.show();
        _container.innerHTML = '';
        $(_container).load(url, function () {
            _loader.hide();
        });
        //$(".send-message-panel .main-block").scrollTop(0);
        return false;
    };
    var fromContent = function (content) {
        _resetContainer();
        _loader.hide();
        _container.innerHTML = content;
        _panel.show();
        return false;
    };
    var fromContainer = function (container) {
        _resetContainer();
        container.style.display = 'block';
        _container.innerHTML = '';
        _prevContainer = container.parentNode;
        _prevContent = container;
        _container.appendChild(container);
        _loader.hide();
        _panel.show();
        return false;
    };
    return {
        'fromURL': fromURL,
        'fromContent': fromContent,
        'fromContainer': fromContainer
    };

    function _createPanel() {
        _panel = document.createElement('div');
        _panel.className = 'rightPanel send-message-panel';
        _panel.innerHTML = '<div class="main-block" style="height:100%;"><div class="content"></div><div class="loader"><span></span><span></span><span></span></div></div><span class="side-panel-close" title="Закрыть"><span class="side-panel-close-inner"></span></span>';
        _loader = _panel.getElementsByClassName('loader')[0];
        _container = _panel.getElementsByClassName('content')[0];
        _panel.getElementsByClassName('side-panel-close')[0].onclick = function () {
            _panel.hide();
        };
        contentContainer.appendChild(_panel);
    }

})(document.getElementsByClassName('page-content')[0]);
