// Add folder
$(document).on('click', '.add-folder', function(e) {
    e.preventDefault();
    $('.directory-options').tooltipster('hide');
    $.pjax({url: '/documents/upload-manager/create-dir', container: "#add-directory-form", push: false});
    $("#add-directory-modal").modal("show")
        .find('.modal-header > h1').text('Новая папка');
});

// Edit folder
$('.tooltip_templates .edit-folder').on('click', function(e) {
    e.preventDefault();
    $('.directory-options').tooltipster('hide');
    var id = $(this).attr('data-id');
    $.pjax({url: '/documents/upload-manager/update-dir/?id=' + id, container: "#add-directory-form", push: false});
    $("#add-directory-modal").modal("show")
        .find('.modal-header > h1').text('Редактировать папку');
});

// Remove folder
$(document).on("click", "button.btn-confirm-yes", function(){
    var $this = $(this);
    $.ajax({
        url: $this.data("url"),
        type: $this.data("type"),
        data: $this.data("params"),
        success: function(data) {
            //$(".page-content").prepend(successFlashMessageDelete);
        }
    });
    return false;
});

// Move folder up/down
$('.tooltip_templates .move-folder').on('click', function(e) {
    e.preventDefault();
    var id = $(this).attr('data-id');
    var direction = $(this).attr('data-direction');
    $.post('/documents/upload-manager/move-dir', {id:id, direction:direction}, function (data) {
        if (data.swap_id) {
            if (direction === 'up')
                $('#dir-' + data.swap_id).before($('#dir-' + id));
            else
                $('#dir-' + data.swap_id).after($('#dir-' + id));
        }

    })
});

$('.documents-nav-link').on("click", function(e) {
    if ($(this).parent().hasClass('active_menu'))
        $('#side-menu-dirs').hide();
    else
        $('#side-menu-dirs').show();
});

$(document).ready(function () {
    $('#side-menu-docs').metisMenu();
});