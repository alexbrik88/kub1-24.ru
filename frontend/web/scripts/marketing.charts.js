////////////////////
//    Chart123    //
////////////////////

window.Chart123 = {
    _inProcess: false,
    channel: null,
    campaign: null,
    chart_11: {
        id: 'chart_11',
        data: {},
        labelsX: [],
        freeDays: [],
        currDayPos: null,
        offset: 0,
    },
    chart_12: {
        id: 'chart_12',
        data: {},
        labelsX: [],
        freeDays: [],
        currDayPos: null,
        offset: 0,
    },
    chart_13: {
        id: 'chart_13',
        data: {},
        labelsX: [],
        freeDays: [],
        currDayPos: null,
        offset: 0,
    },
    chart_21: {
        id: 'chart_21',
        data: {},
        labelsX: [],
        freeDays: [],
        currDayPos: null,
        offset: 0,
        period: 'days'
    },
    chart_22: {
        id: 'chart_22',
        html: null, // refresh whole table
    },
    init: function () {
        this.bindEvents();
        this.bindTopChartEvents();
        this.bindBottomChartEvents();
        this._initScatter();
        this._initPeriod();
        this._initChannelType();
    },
    bindEvents: function() {

        // change channel
        $(document).on("change", "#marketing-chart-select-channel", function() {

            const channel = $(this).val();

            if (Chart123._inProcess) {
                $(this).val(Chart123.channel).trigger('change');
                return false;
            }

            Chart123.channel = channel;
            Chart123.refreshAll();
        });

        // change campaign
        $(document).on("change", "#marketing-chart-select-campaign", function() {

            const campaign = $(this).val();

            if (Chart123._inProcess) {
                $(this).val(Chart123.campaign).trigger('change');
                return false;
            }

            Chart123.campaign = campaign;
            Chart123.refreshAll();
        });

        // prevent auto-close config popups
        $(document).on('click', '._chart_user_config_option', function(e) {
            e.stopPropagation();
        });

        // change chart defaults (top)
        $(document).on('change', '.chart_user_config_select', function(e) {
            const saved = Chart123._setChartConfigParam(this.name, this.value);
            const noPjax = $(this).hasClass('no_pjax');
            let refreshUrl = '';
            if (saved) {

                if (noPjax) {
                    location.reload();
                    return;
                }

                refreshUrl += '?campaign=' + (Chart123.campaign || '');
                refreshUrl += '&offset=' + (Chart123.chart_11.offset || 0);
                $.pjax.reload({
                    container: '#pjax_marketing_statistics_charts',
                    url: refreshUrl,
                    scrollTo: false,
                    push: false,
                    replace: false,
                    timeout: 10E3,
                });
                window.toastr.success('Настройки сохранены');
            } else {
                window.toastr.success('Ошибка сохранения #2');
            }
        });

        // change chart defaults (bottom)
        $(document).on('change', '.chart_user_config_radio', function(e) {
            const saved = Chart123._setChartConfigParam(this.name, this.value);
        });
    },
    bindTopChartEvents: function () {

        // move top charts
        $(document).on("click", ".marketing-chart-offset", function () {

            const chartId = $(this).data('chart');
            const offset = $(this).data('offset');

            if (Chart123._inProcess) {
                return false;
            }

            if (typeof Chart123[chartId] !== 'undefined') {
                Chart123.chart_11.offset += offset;
                Chart123.chart_12.offset += offset;
                Chart123.chart_13.offset += offset;
                Chart123.refreshTop();
            }
        });
    },
    bindBottomChartEvents: function() {

        // move bottom chart
        $(document).on("click", ".marketing-chart-offset-2", function () {

            const chartId = $(this).data('chart');
            const offset = $(this).data('offset');

            if (Chart123._inProcess) {
                return false;
            }

            if (typeof Chart123[chartId] !== 'undefined') {
                Chart123.chart_21.offset += offset;
                Chart123.refreshBottom();
            }
        });

        // change bottom chart visible serie
        $('#chart_21_nav a').on('click', function(e) {

            e.preventDefault();

            const $nav = $('#chart_21_nav');
            const $chart = $('#chart_21').highcharts();
            const serieId = Number($(this).data('serie'));

            $nav.find('li.active, a.active').removeClass('active');
            $(this).addClass('active').parent().addClass('active');

            $.each($chart.series, function(i, serie) {
                if (i === serieId)
                    serie.update({visible: true});
                else
                    serie.update({visible: false});
            });
        });

        // change bottom chart period
        $('#chart_21_period a').on('click', function(e) {

            e.preventDefault();

            const $nav = $('#chart_21_period');

            if (Chart123._inProcess) {
                return false;
            }

            $nav.find('li.active, a.active').removeClass('active');
            $(this).addClass('active').parent().addClass('active');

            Chart123.chart_21.period = $(this).data('period');
            Chart123.refreshBottom();
        });
    },
    refreshTop: function() {
        Chart123._inProcess = true;
        Chart123._getDataTop().done(function() {
            Chart123._repaintTop();
            Chart123._inProcess = false;
        });
    },
    refreshBottom: function() {
        Chart123._inProcess = true;
        Chart123._getDataBottom().done(function() {
            Chart123._repaintBottom();
            Chart123._inProcess = false;
        });
    },
    refreshAll: function() {
        Chart123._inProcess = true;
        Chart123._getDataTop().done(function() {
            Chart123._getDataBottom().done(function() {
                Chart123._getDataTable().done(function() {
                    Chart123._repaintTop();
                    Chart123._repaintBottom();
                    Chart123._repaintTable();
                    Chart123._inProcess = false;
                });
            });
        });
    },
    _getDataTop: function() {
        const that = this;
        return $.post('/analytics/marketing/chart-ajax/get-data', {
                "channel": Chart123.channel,
                "campaign": Chart123.campaign || null,
                "offset": Chart123.chart_11.offset || 0,
                "chart-group": "Chart123",
                "chart-position": "top",
            },
            function(data) {
                $('#chart-transmitter').append(data).html(null);
            }
        );
    },
    _getDataBottom: function() {
        return $.post('/analytics/marketing/chart-ajax/get-data', {
                "channel": Chart123.channel,
                "campaign": Chart123.campaign || null,
                "period": Chart123.chart_21.period || 'days',
                "offset": Chart123.chart_21.offset || 0,
                "chart-group": "Chart123",
                "chart-position": "bottom",

            },
            function(data) {
                $('#chart-transmitter').append(data).html(null);
            }
        );
    },
    _getDataTable: function() {
        return $.post('/analytics/marketing/chart-ajax/get-data', {
                "channel": Chart123.channel,
                "campaign": Chart123.campaign || null,
                "chart-group": "Chart123",
                "chart-position": "bottom-table",
            },
            function(data) {
                Chart123.chart_22.html = data;
            }
        );
    },
    _setChartConfigParam: function(name, value) {
        let saved = false;
        let param = {};
        param[name] = value;

        $.ajax({
            "type": "POST",
            "async": false,
            "url": "/analytics/marketing/chart-ajax/set-config",
            "data": param,
            "success": function (data) {
                saved = true;
            },
            "error": function (data) {
                window.toastr.success('Ошибка сохранения #1');
            }
        });

        return saved;
    },
    _repaintTop: function() {
        $('#' + Chart123.chart_11.id).highcharts().update(Chart123.chart_11.data);
        $('#' + Chart123.chart_12.id).highcharts().update(Chart123.chart_12.data);
        $('#' + Chart123.chart_13.id).highcharts().update(Chart123.chart_13.data);
    },
    _repaintBottom: function() {
        $('#' + Chart123.chart_21.id).highcharts().update(Chart123.chart_21.data);
    },
    _repaintTable: function() {
        $('#' + Chart123.chart_22.id).parent().html(Chart123.chart_22.html);
    },
    _initPeriod: function() {
        Chart123.chart_21.period = $('#chart_21_period a.active').data('period') || 'days';
    },
    _initChannelType: function() {
        Chart123.channel = $('#marketing-channel-type').val() || null;
    },
    _initScatter: function() {
        Highcharts.SVGRenderer.prototype.symbols['c-rect']
            = function (x, y, w, h) {
            return ['M', x, y + h / 2, 'L', x + w, y + h / 2];
        };
    }
};