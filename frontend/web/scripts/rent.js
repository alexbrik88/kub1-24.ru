function rentFilterPrepare() {
    $(".cash-filter .popup-dropdown-in").on("click", function (e) {
        e.stopPropagation();
        $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop.visible.show").removeClass("visible show");
        $(e.target).parents(".form-group").siblings(".form-group").find(".dropdown-drop-menu.visible.show").removeClass("visible show");
    });

    $(".cash_filters_reset").on("click", function () {
        $('select', this.parentNode.parentNode.parentNode).each(function () {
            this.value = '';
        });
    });
}

function rentConfirmManyDelete() {
}

function rentJointChange(btnsOne, btnsAll) {
    $('.joint-operation-checkbox').on('change', function () {
        let countChecked = 0;
        let checked = [];
        $('.joint-operation-checkbox:checked').each(function () {
            countChecked++;
            checked.push(this.value);
        });
        if (countChecked > 0) {
            let summary = document.getElementById('summary-container');
            $(summary).addClass('visible check-true');
            $('.total-count').html(countChecked);
            $('.selected-ids').val(checked.join(','));
            $('#rent-clone-element').toggleClass('hidden', countChecked !== 1);
        } else {
            $('#summary-container').removeClass('visible check-true');
        }
    });
}

function rentIndexPrepare() {
    let $grid = $('.rent-calendar');
    let current = null;

    function mergeCells($cell) {
        let thisRents = $cell.data('rents');
        let prevRents = $cell.prev().data('rents');
        let colspan = $cell.prev().attr('colspan') * 1;
        for (id1 in thisRents) {
            for (id2 in prevRents) {
                if (id1 === id2) {
                    $cell.data('rents', $.extend(prevRents, thisRents));
                    $cell.prev().remove();
                    $cell.attr('colspan', colspan + 1);
                    return;
                }
            }
        }
    }

    function setLinks($cell) {
        let rents = $cell.data('rents');
        let links = [];
        let tooltip = $('<div style="display: none" />')

        for (id in rents) {
            links.push('<a class="tooltip3" data-tooltip-content="#rent-tooltip-content_'+ id +'" href="' + rents[id].url + '">' + rents[id].title + '</a>');
            tooltip.append('<div id="rent-tooltip-content_'+ id +'">' +
                '<span>'+ rents[id].tooltip.title +'</span>' +
                '<br />' +
                '<span>'+ rents[id].tooltip.agreement +'</span>' +
                '<br />' +
                '<span>'+ rents[id].tooltip.period +'</span>' +
                '</div>');
        }

        $cell.html('<div class="d-flex justify-content-around">' + links.join('') + '</div>');
        $cell.append(tooltip);
    }

    if($grid.data('mode') === 'month') {
        let $quarters = $('<tr/>');
        let $months = $grid.find('thead tr');
        let $entities = $grid.find('tbody tr');

        $quarters.insertBefore($months);
        $quarters.append($months.children(':first').attr('rowspan', 2));

        $months.children().each(function() {
            if ($quarters.find('[data-quarter=' + $(this).data('quarter') + '][data-year=' + $(this).data('year') + ']').length === 0) {
                let $th = $('<th>' +
                    '<button class="table-collapse-btn button-clr ml-1 active" type="button">' +
                    '<span class="table-collapse-icon">&nbsp;</span>' +
                    '<span class="text-grey weight-700 ml-1 nowrap">' + $(this).data('quarter') + ' кв ' + $(this).data('year') + '</span>' +
                    '</button>' +
                '</th>')
                .attr({
                    rowspan: 2,
                    'data-quarter': $(this).data('quarter'),
                    'data-year': $(this).data('year')
                });

                $quarters.append($th);
                $th.data('months', $months.find('[data-quarter=' + $(this).data('quarter') + '][data-year=' + $(this).data('year') + ']').remove());
            }
        });

        $entities.each(function() {
            $(this).data('cells', $(this).children().not(':first').remove());
        });

        $quarters.find('button').click(function() {
            $(this).toggleClass('active');

            $months.children().remove();
            $entities.each(function() {
                $(this).children().not(':first').remove();
            });

            $quarters.children().not(':first').each(function() {
                let quarter = $(this).data('quarter');
                let year = $(this).data('year');

                if ($(this).children().hasClass('active')) {
                    $months.append($(this).data('months').clone());

                    $entities.each(function() {
                        let $cells = $(this).data('cells').filter('[data-quarter=' + quarter +'][data-year=' + year + ']').clone();
                        $(this).append($cells);

                        $cells.each(function() {
                            if ($(this).hasClass('rented') && $(this).prev().hasClass('rented')) {
                                mergeCells($(this));
                            }
                            setLinks($(this));
                        })
                    });

                    $(this).attr({
                        rowspan: 1,
                        colspan: $(this).data('months').length
                    });
                } else {
                    $entities.each(function() {
                        let $cells = $(this).data('cells').filter('[data-quarter=' + quarter +'][data-year=' + year + ']');
                        let $td = $('<td>').attr('colspan', 1).data('rents', {});

                        $cells.each(function() {
                            if ($(this).hasClass('rented')) {
                                $td.addClass('rented');
                                $td.data('rents', $.extend($td.data('rents'), $(this).data('rents')));
                            }
                        });

                        $(this).append($td);
                        if ($td.hasClass('rented') && $td.prev().hasClass('rented')) {
                            mergeCells($td)
                        }
                        setLinks($td);
                    });

                    $(this).attr({
                        rowspan: 2,
                        colspan: 1
                    });
                }
            })
        }).click();
    } else {
        $grid.find('tbody tr').each(function() {
            $(this).children('.rented').each(function() {
                if ($(this).prev().hasClass('rented')) {
                    mergeCells($(this))
                }
                setLinks($(this));
            });
        });
    }
}

function rentRentListPrepare() {
    rentFilterPrepare();

    rentJointChange(['btn-copy']);

    $('.btn-copy').click(function () {
        let loader = document.querySelector('#rent-add .loader');
        loader.style.display = 'flex';
        $.ajax({
            'url': this.href,
            'dataType': 'json',
            'global': false,
            'success': function (response) {
                let form = document.forms.rentItem;
                let agreement = document.getElementById('rent-agreementId');
                $(agreement).find('option').remove();
                for (let id in response.agreement) {
                    let option = document.createElement('option');
                    option.value = id;
                    option.innerHTML = response.agreement[id];
                    $(agreement).add(option, null);
                }
                Object.keys(response.item).forEach(function (attribute) {
                    let field = form['RentAgreement[' + attribute + ']'];
                    if (field != undefined) field.value = response.item[attribute];
                });
                loader.style.display = 'none';
                $('#rent-add').modal('show');
            }
        });

        return false;
    });
}

function rentRentFormPrepare() {
}

function rentRentFormClear() {
    $('.form-group input,.form-group select', document.forms.rentItem).val('');
}

function rentEntityListPrepare() {
    rentFilterPrepare();

    rentJointChange(['btn-copy']);

    rentConfirmManyDelete();

    $(document).on('click', '#confirm-many-retired', function (e) {
        if ($("input.product_checker:checked").length) {
            $("#product_checker_form").attr("action", this.dataset.action).submit();
        }
    });
}

function rentEntityFormPrepare() {
    $(document.forms.rentEntity['RentEntity[owner_id]']).change(function() {
        if (this.value === this.dataset.default_owner_id) {
            $('#block-additional-fields').show();
        } else {
            $('#block-additional-fields').hide();
        }
    });

    $(document.forms.rentEntity['RentEntity[category_id]']).change(function() {
        let categoryId = $(this).val();
        let entityId = document.forms.rentEntity['RentEntity[id]'].value;

        if (categoryId) {
            $.when(
                $.ajax({
                    url: $(document.forms.rentEntity).data('ajax-attribute-url'),
                    data: {
                        category: categoryId,
                        tab: 'info',
                        entity: entityId
                    },
                    success: function(response) {
                        $('#ajax-category').empty().append(response);
                    }
                }),
                $.ajax({
                    url: $(document.forms.rentEntity).data('ajax-attribute-url'),
                    data: {
                        category: categoryId,
                        tab: 'price',
                        entity: entityId
                    },
                    success: function(response) {
                        $('#ajax-price').empty().append(response);
                    }
                })
            ).done(function() {
                $('#ajax-category select').select2({
                    theme: 'krajee-bs4',
                    width: '100%',
                    placeholder: '',
                    minimumResultsForSearch: Infinity
                });
                $('#ajax-price select').select2({
                    theme: 'krajee-bs4',
                    width: '100%',
                    placeholder: '',
                    minimumResultsForSearch: Infinity
                });
            });
        }
    });

    $(document.forms.rentEntity['RentEntity[owner_id]']).change();
}

function rentAgreementFormPeriod(form) {
    var start = $('#rentagreementform-date_begin', form).val();
    var end = $('#rentagreementform-date_end', form).val();
    var days = 0;
    if (start.length && end.length) {
        var a = moment(start, 'DD.MM.YYYY');
        var b = moment(end, 'DD.MM.YYYY');
        var c = b.diff(a, 'days');
        if (!Number.isNaN(c) && c >= 0) {
            days = c + 1;
        }
    }
    $('#rentagreementform-period_days', form).data('value', days > 0 ? days : '');
    $('#rentagreementform-period_days', form).val(days > 0 ? days + ' дн.' : '');
    rentAgreementFormPaymentCount(form);
}

function rentAgreementFormPaymentCount(form) {
    var period = parseInt($('#rentagreementform-payment_period', form).val());
    var days = parseInt($('#rentagreementform-period_days', form).data('value'));
    var count = 1;
    if (!Number.isNaN(period) && !Number.isNaN(days)) {
        switch (period) {
            case 1:
                count = days;
                break;
            case 2:
                count = Math.round(days / 30 * 10) / 10;
                break;
            case 3:
                count = Math.round(days / 365 * 10) / 10;
                break;
            case 4:
            default:
                count = 1;
                break;
        }
    }
    $('#rentagreementform-pay_count', form).val(Math.max(1, count));
}

function rentAgreementFormCheckItemsKey(form) {
    $('.rent_agreement_item', form).each(function (key) {
        var $row = this;
        $('[name^=RentAgreementItemForm]', $row).each(function (i) {
            this.name = this.name.replace(/\[\d+\]/, '['+key+']');
        });
    });
}

function rentAgreementFormCalculate(form) {
    var hasDiscount = $('#rentagreementform-has_discount', form).is(':checked');
    var discountType = $('#rentagreementform-discount_type', form).val();
    var totalAmount = 0;
    $('.rent_agreement_item', form).each(function (key) {
        var $row = this;
        var price = parseFloat($('.price input', $row).val()) || 0;
        var discount = hasDiscount ? (parseFloat($('.discount input', $row).val()) || 0) : 0;
        var amount = discountType == 1 ? (price - discount) : (price - price / 100 * discount);
        amount = Math.round(Math.max(0, amount) * 100) / 100;
        $('.amount', $row).text(amount.toFixed(2));
        totalAmount += amount;
    });
    $('#rent-agreemment-summary .total_amount', form).text(totalAmount.toFixed(2));
}

$(document).on('change', '#rent_agreement_entity_select', function (e) {
    var form = this.form;
    var $input = $(this);
    var hasDiscount = $('#rentagreementform-has_discount', form).is(':checked') ? 1 : 0;
    if ($input.val() > 0) {
        $.ajax({
            method: 'get',
            url: $input.data('item-url'),
            data: {id: $input.val(), hasDiscount: hasDiscount},
            success: function (data) {
                $input.val('').trigger('change');
                $('#rent_agreemment_item_row').toggleClass('hidden', true);
                $('table#rent-agreemment-items>tbody').append(data);
                rentAgreementFormCheckItemsKey(form);
                rentAgreementFormCalculate(form);
            }
        });
    }
});
$(document).on('click', '#rent_agreement_item_add', function (e) {
    $('#rent_agreemment_item_row').toggleClass('hidden', false);
});
$(document).on('click', '.rent_agreement_item_remove', function (e) {
    var form = $(this).closest('form').get();
    $(this).closest('tr').remove();
    rentAgreementFormCheckItemsKey(form);
    rentAgreementFormCalculate(form);
});
$(document).on('change', 'input.rent_agreement_period_input', function (e) {
    rentAgreementFormPeriod(this.form);
});
$(document).on('change input', '.rent_agreement_item input', function (e) {
    rentAgreementFormCalculate(this.form);
});
$(document).on('change', '#rentagreementform-has_discount', function (e) {
    $('table .discount_column', this.form).toggleClass('hidden', !$(this).is(':checked'));
    rentAgreementFormCalculate(this.form);
});
$(document).on('change', '#config-discount_type input', function () {
    var box = $(this).closest('#invoice_all_discount');
    var value = $('#config-discount_type input:checked', box).val();
    var allDiscount = $('#all-discount', box);
    if (value == $('#config-discount_type', box).data('rub')) {
        allDiscount.prop('disabled', true).val('');
    } else {
        allDiscount.prop('disabled', false).val(allDiscount.data('old-value'));
    }
});
$(document).on('input change', '#invoice_all_discount input', function () {
    var changed = false;
    var box = $(this).closest('#invoice_all_discount');
    var allDiscountValue = parseFloat($('#all-discount', box).val()).toString();
    var discountTypeValue = $('#config-discount_type input[type=radio]:checked', box).val();
    var hiddenDiscountValue = $('#config-is_hidden_discount', box).is(":checked") ? '1' : '0';
    if (allDiscountValue != $('#all-discount', box).data('old-value')) {
        changed = true;
    } else if (discountTypeValue != $('#invoice-discount_type').val()) {
        changed = true;
    } else if (hiddenDiscountValue != $('#invoice-is_hidden_discount').val()) {
        changed = true;
    }

    $('#discount_submit', box).toggleClass('button-hover-grey', !changed).toggleClass('button-regular_red', changed);
});
$(document).on("input change", "input.discount-input", function (e) {
    var p = (parseFloat(this.value) * 1).precision();
    if (p > 2 && $('#config-discount_type input[type=radio]:checked').val() == '1') {
        $("#invoice_price_conf button[value=4]").click();
    }
});
$(document).on('click', '#discount_submit.rent_agreement', function (e) {
    console.log($(this).hasClass('button-regular_red'));
    if ($(this).hasClass('button-regular_red')) {
        var val = 0;
        var form = document.getElementById('rent-agreemment-form');
        var box = $(this).closest('#invoice_all_discount');
        var allDiscount = $("#all-discount", box);
        var allDiscountValue = allDiscount.val();
        var discountTypeValue = $('#config-discount_type input[type=radio]:checked', box).val() || '0';
        var hiddenDiscountValue = $('#config-is_hidden_discount', box).is(':checked') ? '1' : '0';
        $('#rentagreementform-discount_type', form).val(discountTypeValue);
        $('#rentagreementform-is_hidden_discount', form).val(hiddenDiscountValue);
        if (discountTypeValue == '1') {
            $('#all_discount_toggle').text('Скидка (руб.)');
        } else {
            val = Math.max(Math.min(parseFloat(allDiscountValue), 99.9999), 0);
            if (val.toString() != allDiscountValue) {
                allDiscount.val(val.toString());
            }
            allDiscount.data('old-value', val.toString());
            $("table .discount-input").val(val);
            $('#all_discount_toggle').text('Скидка %');
        }
        rentAgreementFormCalculate(form);
        $(this).removeClass('button-regular_red').addClass('button-hover-grey');
    }
    $('.tooltipstered').tooltipster('hide');
});
$(document).on('click', '#rent-many-delete', function (e) {
    if ($("input.joint-operation-checkbox:checked").length) {
        $.post($(this).data('url'), $('input.joint-operation-checkbox').serialize());
    }
});
$(document).on('click', '#rent-clone-element', function (e) {
    if ($("input.joint-operation-checkbox:checked").length == 1) {
        document.location.href = $("input.joint-operation-checkbox:checked").data('clone-url');
    }
});
$(document).on('change', '#rentagreementform-payment_period', function (e) {
    rentAgreementFormPaymentCount(this.form);
});
$(document).on('change', 'input.entity_attribute_validityPeriod', function (e) {
    var form = this.form;
    var data = $('#rent-entity-additional-status', form).data('list');
    var status = 0;
    var a = moment(this.value, 'DD.MM.YYYY');
    var b = moment();
    if (a.isValid() && b.isValid()) {
        if (b.diff(a, 'days') > 0) {
            status = 2;
        } else {
            status = 1;
        }
    }
    $('#rent-entity-additional-status', form).val(data[status] || '');
    $('#rent-entity-additional-status', form).toggleClass('color-red', status === 2);
});
