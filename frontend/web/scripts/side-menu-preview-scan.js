$(document).ready(function() {
    var container = $('.page-scan-preview');
    $(container).find('.file-name').html(window.currScan.filename);
    $(container).find('.num-page').html(window.previewScansPos + 1);
    if (!$(container).find('.slide').find('img').length) {
         $(container).find('.zoomIn, .zoomOut').hide();
    }

    previewScansRefreshPage();
});

function previewScansRefreshPage()
{
    var container = $('.page-scan-preview');
    window.currScan = window.previewScansList[window.previewScansPos];

    $('.issuu-container').hide().html("");
    $(window.currScan.showIn).html(window.currScan.html).show();

    $(container).find('.file-name').html(window.currScan.filename);
    $(container).find('.num-page').html(window.previewScansPos + 1);
    if (!$(container).find('.slide').find('img').length) {
        $(container).find('.zoomIn, .zoomOut').hide();
    } else {
        $(container).find('.zoomIn, .zoomOut').show();
    }
    $(container).find('.slide:visible').attr('data-id', window.currScan.file_id);

    $.post('/documents/upload-manager-ajax/can-attach-file?file_id=' + window.currScan.file_id, function(data) {
        if (data.result) {
            $('.btn-many-attach').show();
        } else {
            $('.btn-many-attach').hide();
        }
    });
}

// slide scans
$('.nextPage').on('click', function(e) {
    window.previewScansPos++;
    if (window.previewScansPos > window.previewScansTotal - 1) {
        window.previewScansPos = 0;
    }

    previewScansRefreshPage();
});
$('.prevPage').on('click', function(e) {
    window.previewScansPos--;
    if (window.previewScansPos < 0) {
        window.previewScansPos = window.previewScansTotal - 1;
    }

    previewScansRefreshPage();
});

// zoom
$('.zoomIn, .zoomOut').on('click', function(e) {
    var container = $('.page-scan-preview');
    var activeSlide = $(container).find('.slide:visible').first();
    var image = $(activeSlide).find('img');
    var currentZoom = $(image).css('width').replace('px', '');
    if ($(this).hasClass('zoomIn'))
        $(image).css({'width': Math.ceil(1.05 * currentZoom) + 'px'});
    if ($(this).hasClass('zoomOut'))
        $(image).css({'width': Math.ceil(0.95 * currentZoom) + 'px'});
});

// print
$('.print-scan').on('click', function(e) {
    e.preventDefault();
    var container = $('.page-scan-preview');
    var activeSlide = $(container).find('.slide:visible').first();
    var id = $(activeSlide).attr('id');
    if ($(activeSlide).hasClass('pdf')) {
        printPDF(id);
    } else {
        printImg(id);
    }
});

// download
$('.download-scan').on('mousedown', function(e) {
    var filename = window.currScan.src;
    $(this).attr('href', filename);
});

function printImg(id) {
    var divToPrint = document.getElementById(id);
    wnd = window.wnd && window.wnd.close() || window.open('', "Print-Window");
    wnd.document.open();
    wnd.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
    wnd.document.close();
}

function printPDF(id) {
    var printUrl = $('#'+id).find('embed').attr('src').replace('/file/', '/print/');
    wnd = window.wnd && window.wnd.close() || window.open(printUrl, "Print-Window");
}