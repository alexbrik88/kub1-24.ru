var buttonsScroll = (function () {
    var forMobileOffset = 60;
    var forDesktopOffset = 28;
    var startPosition = 10;
    var currentPosition = 10;
    var offsetHeightMonitor = 25;
    var offset = startPosition + forDesktopOffset;
    var idTimer = [];

    var init = function () {
        $('#page-footer').css({'display': 'none'});
        $('#footer-for-invoice').removeClass('hide');
        calculate();
    };

    var addStyle = function (forPix, toPix) {
        var increment = forPix <= toPix ? true : false;
        var current = forPix;
        var iteration = Math.abs(forPix - toPix);

        $.each(idTimer, function (def, id) {
            clearTimeout(id);
        });

        idTimer[0] = setTimeout(function run() {
            if (increment) {
                current++;
            } else {
                current--;
            }
            $('#buttons-scroll-fixed').css({'bottom': current + 'px'});

            if (iteration < 1) {
                currentPosition = current;
                $.each(idTimer, function (def, id) {
                    clearTimeout(id);
                });
            } else {
                idTimer[2] = setTimeout(run, 1);
            }

            iteration--;

        }, 200);
    };

    var calculate = function () {
        $('#buttons-scroll-fixed').css({'width': $('#buttons-container').width() + 'px'});
        if ($(window).width() < 992) {
            if ($('.buttons-container-white').length > 0) {
                $('.page-footer').hide();
            }
        } else {
            if ($('.buttons-container-white').lenth > 0) {
                $('.page-footer').show()
            }
        }

        if ($(window).width() < 425) {
            offset = startPosition + forMobileOffset + 24;
            offsetHeightMonitor + 38;
        } else {
            if ($(window).width() < 992) {
                offset = startPosition + forMobileOffset;
            } else {
                offset = startPosition + forDesktopOffset;
            }
        }
    };

    window.onscroll = function () {
        calculate();
    };
    return {init: init};
})
();