jQuery(document).ready(function () {
    var $pickers = $(".datetimepicker .datetimepicker-minutes, .datetimepicker .datetimepicker-hours");
    $pickers.find("thead").remove();
    $pickers.find("tbody tr td").css("width", "185px");

    $("input.paymentremindermessage-time").each(function () {
        $(this).siblings(".input-group-addon").click();
        $(".datetimepicker-hours:visible").find("tbody span.hour:contains(" + $(this).data("hour") + ")").click();
        $(".datetimepicker-minutes:visible").find("tbody span.minute:contains(" + $(this).data("minute") + ")").click();
    });
    $(document).on("click", ".activate-message-template", function () {
        var $modal = $($(this).data("target") + ".modal");
        $modal.find("#paymentremindermessage-activate").val(1);
        $modal.find(".warning-text").removeClass("hidden");
    });
    $(document).on("click", ".update-payment-reminder-message", function () {
        var $modal = $($(this).data("target") + ".modal");
        $modal.find("#paymentremindermessage-activate").val(0);
        $modal.find(".warning-text").addClass("hidden");
    });
    $(document).on("click", ".update-payment-reminder-message-contractor:not(.disabled)", function () {
        let form = $("#form-payment-reminder-message-contractor");
        $(this).siblings().toggleClass("hidden");
        $(this).addClass("hidden");
        $("#summary-container").removeClass("hidden");
        $("table:not(.scrollable-table-clone) .can_update input", form).prop("disabled", false).uniform("refresh");
        $('.tooltipstered', form).tooltipster('enable');
    });
    $(document).on("click", ".undo-payment-reminder-message-contractor", function () {
        let form = $("#form-payment-reminder-message-contractor");
        $(this).siblings().toggleClass("hidden");
        $(this).addClass("hidden");
        $("#summary-container").addClass("hidden");
        $("table:not(.scrollable-table-clone) .can_update input", form).prop("disabled", true).uniform("refresh");
        $('.tooltipstered', form).tooltipster('disable');
    });
    $(document).on("click", "#summary-container .undo-payment-reminder-message-contractor", function () {
        let form = $("#form-payment-reminder-message-contractor");
        $(this).closest("#summary-container").addClass("hidden");
        $(".actions button:not(.hidden)", form).addClass("hidden");
        $(".actions button.update-payment-reminder-message-contractor", form).removeClass("hidden");
        $("table:not(.scrollable-table-clone) .can_update input", form).prop("disabled", true).uniform("refresh");
        $('.tooltipstered', form).tooltipster('disable');
    });
    $(document).on("click", "#add_message_contractor_btn", function () {
        var data = $("#add_message_contractor_grid input:checked").serialize();
        $.post($(this).data("url"), data, function() {
            $.pjax.reload("#payment-reminder-message-contractor-pjax");
        });
    });
    $(document).on("submit", "form.reminder-comtractor-delete-form", function (e) {
        e.preventDefault();
        $.post(this.action, function(data) {
            setTimeout(function() {
                $.pjax.reload("#payment-reminder-message-contractor-pjax");
            }, 300);
        });
        $('.modal.in, .modal.show').modal('hide');
    });
    $(document).on("change", "input.paymentremindermessagecontractor-message", function (e) {
        let form = this.form;
        let selector = $(this).data("target");
        console.log($(selector+":not(:checked)", form));
        $(selector+"_all", form).prop('checked', $(selector+":not(:checked)", form).length == 0).uniform("refresh");
    });
    $(document).on("change", "input.paymentremindermessagecontractor-message-all", function () {
        let form = this.form;
        let selector = $(this).data("target");
        $(selector, form).prop('checked', $(this).is(":checked")).uniform("refresh");
    });
    $(document).on("change", "table .select-on-check-all", function () {
        let table = $(this).closest("table");
        $("input", table).uniform("refresh");
    });
    $(document).on("change", "input.template_status_switch", function () {
        var input = this;
        if (input.checked) {
            let $modal = $($(input).data("target"));
            $modal.find("#paymentremindermessage-activate").val(1);
            $modal.find(".warning-text").removeClass("hidden");
            $modal.modal("show");
            $modal.off('hidden.bs.modal');
            $modal.on('hidden.bs.modal', function () {
                $(input).prop('checked', false).uniform('refresh');
            });
        } else {
            input.form.submit();
        }
    });
    $(document).on("hidden.bs.modal", "#add_message_contractor_modal", function (e) {
        setTimeout(function() {
            $.pjax.reload("#add_message_contractor_pjax", {
                url: $('#add_message_contractor_grid', e.target).data('current-url'),
                push: false,
            });
        }, 500);
    });
    $(".payment-reminder-tooltip").tooltipster({
        "multiple": true,
        "theme": ["tooltipster-kub"],
        "contentAsHTML": true,
        "functionBefore": function (instance) {
            var $item = instance._$origin;
            var $tooltipText = "Дата последнего<br> отправленного письма:<br>" + $item.data("date");
            if ($item.data("contractor_email") !== "") {
                $tooltipText = $tooltipText + " на " + $item.data("contractor_email");
            }
            instance.content($tooltipText);
        }
    });
    $("#payment-reminder-message-contractor-pjax").on('pjax:complete', function() {
        $(".tooltip2", this).tooltipster({
            theme: ['tooltipster-user-help'],
            trigger: 'hover',
            contentAsHTML: true,
            maxWidth: 500,
        });

        $(".tooltip3", this).tooltipster({
            theme: ['tooltipster-kub'],
            trigger: 'hover',
            contentAsHTML: true,
            maxWidth: 500,
        });
    });
});