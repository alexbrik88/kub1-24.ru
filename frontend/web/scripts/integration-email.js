function loadLetterDetails(ids) {
    var id = ids.shift();
    $.get('/integration/email/' + id + '/detail', function (data) {
        var tdList = $('#datatable_ajax tr[data-key=' + id + '] td');
        var p = document.createElement('P');
        p.innerHTML = data.textPlain;
        tdList[2].appendChild(p);
        setTimeout(function () {
            loadLetterDetails(ids);
        }, 200);
    });
}

var sendMessagePanelOpen = function (emailId, action) {
    var link = '/integration/email';
    if (emailId !== undefined) link += '/' + emailId;
    if (action !== undefined) link += '/' + action;
    return rightPanel.fromURL(link);
};

function sendEmailFormReady() {
    $('.form-actions .ladda-button').click(function () {
        var form = document.forms.sendEMail;
        if (form.action.value !== $(this).attr('data-action')) {
            setEmailFormAction($(this).attr('data-action'));
            return false;
        }
    });
}

function setEmailFormAction(action) {
    var form = document.forms.sendEMail;
    if (action === form.action.value) return true;
    sendMessagePanelOpen(form.uid.value, action);
}

function prepareToField() {
    $(document).ready(function () {
        $("#emailletterform-to_tag").attr("placeholder", "Укажите e-mail").addClass("visible");
    });
}

function prepareUploader() {
    var _$uploadButton = $(".block-files-email span.upload-file");
    var _uploadUrl = _$uploadButton.data("url");
    var _csrfParameter = _$uploadButton.data("csrf-parameter");
    var _csrfToken = _$uploadButton.data("csrf-token");
    var uploadData = {};
    uploadData[_csrfParameter] = _csrfToken;

    var uploader = new ss.SimpleUpload({
        button: _$uploadButton[0], // HTML element used as upload button
        url: _uploadUrl, // URL of server-side upload handler
        data: uploadData,
        multipart: true,
        multiple: true,
        multipleSelect: true,
        encodeCustomHeaders: true,
        responseType: "json",
        name: "emailFile", // Parameter name of the uploaded file
        onSubmit: function () {
            $("#file-ajax-loading").css("display", "inline-block");
        },
        onComplete: function (filename, response) {
            if (response["result"] == true) {
                var $templateFile = $(".email-uploaded-files .one-file.template").clone();
                var $needUploadFiles = [];

                $templateFile.removeClass("template");
                $templateFile.attr("data-id", response["id"]);
                $templateFile.prepend(response["previewImg"]);
                $templateFile.find(".file-name").text(response["name"]).attr("title", response["name"]);
                $templateFile.find(".file-size").text(response["size"] + " КБ");
                $templateFile.find(".delete-file").attr("data-url", response["deleteUrl"]);
                if (response["downloadUrl"]) {
                    $templateFile.find(".download-file").attr("href", response["downloadUrl"]).show();
                }
                $templateFile.show();
                $(".email-uploaded-files").append($templateFile);
                $("#file-ajax-loading").hide();
                $(".email-uploaded-files").show();

                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#emailletterform-attachments").val($needUploadFiles.join(", "));
            } else {
                alert(response["msg"]);
                $("#file-ajax-loading").hide();
            }
        }
    });

    $(document).on("click", ".email-uploaded-files .one-file .delete-file", function () {
        var $oneFile = $(this).closest(".one-file");
        var $fileBlock = $(this).closest(".email-uploaded-files");
        $.post($(this).data("url"), null, function (data) {
            if (data.result) {
                var $needUploadFiles = [];

                $oneFile.remove();
                $(".email-uploaded-files .one-file:not(.template)").each(function () {
                    $needUploadFiles.push($(this).data("id"));
                });
                $("#emailletterform-attachments").val($needUploadFiles.join(", "));
                if ($fileBlock.find(".one-file:not(.template)").length === 0) {
                    $fileBlock.hide();
                }
            } else {
                alert(data.msg);
            }
        });
    });
}
