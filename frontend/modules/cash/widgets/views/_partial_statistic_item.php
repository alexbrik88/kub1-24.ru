<?php
use common\components\TextHelper;

/**
 * @var $text string
 * @var $class string
 * @var $amount int
 * @var $statistic_text string
 * @var $statistic_text_class string
 */
?>

<div class="col-md-2 col-md-new-5 col-sm-3">
    <div class="dashboard-stat <?= $class; ?>">
        <div class="visual">
            <i class="fa fa-comments"></i>
        </div>
        <div class="details">
            <div class="number fontSizeSmall">
                <?= TextHelper::$format($amount, $decimals); ?> <?= $icon ?>
            </div>
            <div class="desc">
                <?= $text; ?>
            </div>
        </div>
        <div class="<?= $statistic_text_class; ?>"><?= $statistic_text; ?></div>
    </div>
</div>