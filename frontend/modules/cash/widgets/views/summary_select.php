<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $widget frontend\modules\documents\widgets\SummarySelectWidget */


$css = <<<STYLE
.summary-info {
    font-weight: bold;
    width: calc(100% - 275px);
    border: 1px solid #4276a4;
    border-bottom-color: rgb(66, 118, 164);
    margin: 0 20px;
}
.buttons-scroll-fixed {
    position: fixed;
    bottom: 0px;
    z-index: 9999;
    background:#fff;
    margin-left: -20px;
}
@media (max-width: 992px) {
    .summary-info {  width: calc(100% - 40px);}
}
@media(max-width:767px){
    .summary-info {  width: calc(100% - 20px);}
}
STYLE;

$js = <<<SCRIPT
$('.joint-operation-checkbox').on('change', function(){
    var countChecked = 0;
    var inSum = 0;
    var outSum = 0;
    var diff = 0;
    $('.joint-operation-checkbox:checked').each(function(){
        countChecked++;
        inSum += parseFloat($(this).data('income'));
        outSum += parseFloat($(this).data('expense'));
    });
    diff = inSum - outSum;
    if (countChecked > 0) {
        $('#summary-container').removeClass('hidden');
        $('#summary-container span.total-count').text(countChecked);
        $('#summary-container span.total-income').text(number_format(inSum, 2, ',', ' '));
        $('#summary-container span.total-expense').text(number_format(outSum, 2, ',', ' '));
        if (outSum == 0 || inSum == 0) {
            $('#summary-container span.total-difference').closest("td").hide();
        } else {
            $('#summary-container span.total-difference').closest("td").show();
        }
        $('#summary-container span.total-difference').text(number_format(diff, 2, ',', ' '));
    } else {
        $('#summary-container').addClass('hidden');
    }
});
SCRIPT;

$this->registerCss($css);
$this->registerJs($js);
?>

<div id="summary-container" class=" hidden buttons-scroll-fixed col-xs-12 col-sm-12 col-lg-12  pad0">
    <div class="summary-info">
        <div style="padding: 5px 10px;">
            <table class="pull-left">
                <tbody>
                    <tr>
                        <td style="width: 50px; height: 34px; text-align: center;">
                            <input type="checkbox" class="joint-operation-main-checkbox" name="" value="1">
                        </td>
                        <td style="padding: 0 10px;">
                            Выбрано:
                            <span class="total-count">0</span>
                        </td>
                        <td style="padding: 0 10px;">
                            Приход:
                            <span class="total-income">0</span>
                        </td>
                        <td style="padding: 0 10px;">
                            Расход:
                            <span class="total-expense">0</span>
                        </td>
                        <td style="padding: 0 10px;">
                            Разница:
                            <span class="total-difference">0</span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="actions-many-items pull-right">
                <tbody>
                    <tr>
                        <?php foreach ($widget->buttons as $key => $button) : ?>
                            <?= Html::tag('td', $button, [
                                'style' => 'width: 80px; height: 34px; color: #4276a4;' . ($key ? ' padding-left: 15px;' : ''),
                            ]) ?>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<?php $this->registerCss($css) ?>
<?php $this->registerJs($js) ?>
