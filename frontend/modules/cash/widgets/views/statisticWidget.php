<?php
/**
 * @var CashFlowsBase $model
 */

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use frontend\components\StatisticPeriod;

$date = StatisticPeriod::getSessionPeriod();

$dateStart = $model->getPeriodStartDate();
$dateEnd = $model->getPeriodEndDate();
$balanceStart = $model->getBalanceAtBegin(date_create_from_format(DateHelper::FORMAT_DATE, $date['from']));
$balanceEnd = $model->getBalanceAtEnd(date_create_from_format(DateHelper::FORMAT_DATE, $date['to']));
$totalIncome = $model->getPeriodIncome();
$totalExpense = $model->getPeriodExpense();

echo frontend\modules\cash\widgets\Statistic::widget([
    'items' => [
        [
            'class' => '_yellow',
            'text' => 'Баланс на начало периода',
            'amount' => $balanceStart,
            'statistic_text' => $dateStart,
            'statistic_text_class' => '_yellow_statistic',
        ],

        [
            'class' => '_green',
            'text' => 'Приход',
            'amount' => $totalIncome,
            'statistic_text' => 'ВСЕГО ПОСТУПЛЕНИЙ ' . $model->getCountIncome(),
            'statistic_text_class' => '_green_statistic',
        ],

        [
            'class' => '_red',
            'text' => 'Расход',
            'amount' => $totalExpense,
            'statistic_text' => 'ВСЕГО СПИСАНИЙ ' . $model->getCountExpense(),
            'statistic_text_class' => '_red_statistic',
        ],

        [
            'class' => '_yellow',
            'text' => 'Баланс на конец периода',
            'amount' => $balanceEnd,
            'statistic_text' => $dateEnd,
            'statistic_text_class' => '_yellow_statistic',
        ],
    ],

    'model' => $model,
]);