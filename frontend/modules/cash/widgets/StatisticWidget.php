<?php

namespace frontend\modules\cash\widgets;

use yii\base\Widget;

class StatisticWidget extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('statisticWidget', [ 'model' => $this->model, ]);
    }
}
