<?php

namespace frontend\modules\cash\widgets;

use common\models\cash\CashFlowsBase;
use common\models\currency\Currency;
use frontend\modules\cash\models\CashBankForeignCurrencySearch;
use yii\base\Widget;

class Statistic extends Widget
{
    /**
     * @var array
     */
    public $items = [];
    public $format = null;
    public $icon = '₽';
    public $decimals = 2;

    /**
     * @var CashFlowsBase $model
     */
    public $model;

    /**
     * @return string
     */
    public function run()
    {
        $content = '';
        if (!empty($this->items)) {
            if ($this->model) {
                $this->icon = Currency::$currencySymbols[$this->model->currency_name] ?? null;
            }
            if(isset($this->format)) {
                $this->decimals = ' ';
            } else {
                $this->format = 'invoiceMoneyFormat';
            }
            (isset($this->icon)) ?: $this->icon = '<i class="fa fa-rub"></i>';
            $lastKey = array_key_last($this->items);
            foreach ($this->items as $key => $item) {
                $content .= $this->render('_partial_statistic_item', [
                    'class'  => $item['class'],
                    'text'   => $item['text'],
                    'amount' => $item['amount'],
                    'last_item' => $key === $lastKey,
                    'decimals' => $this->decimals,
                    'format' => $this->format,
                    'icon' => $this->icon,
                    'dataValue' => $item['dataValue'] ?? '',
                    'statistic_text' => isset($item['statistic_text'])? $item['statistic_text']: '',
                    'statistic_text_class' => isset($item['statistic_text_class'])? $item['statistic_text_class']: '',
                ]);
            }
        }

        return $content;
    }
}
