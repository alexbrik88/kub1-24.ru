<?php

namespace frontend\modules\cash\widgets;

use yii\base\Widget;

class SummarySelectWidget extends Widget
{
    const VIEW_INLINE = 1;
    const VIEW_DROPDOWN = 2;

    public $buttonsViewType;

    public $beforeButtons = [];
    public $buttons = [];
    public $afterButtons = [];

    public function run()
    {
        return $this->render('summary_select', ['widget' => $this]);
    }
}
