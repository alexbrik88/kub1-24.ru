<?php

namespace frontend\modules\cash;

use frontend\rbac\UserRole;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\cash\controllers';

    public function init()
    {
        parent::init();

        $this->modules = [
            'banking' => [
                'class' => 'frontend\modules\cash\modules\banking\Module',
            ],
            'ofd' => [
                'class' => 'frontend\modules\cash\modules\ofd\Module',
            ],
        ];

        \Yii::setAlias('@banking', '@frontend/modules/cash/modules/banking');
        \Yii::setAlias('@ofd', '@frontend/modules/cash/modules/ofd');
    }
}
