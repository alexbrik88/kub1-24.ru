<?php

namespace frontend\modules\cash\components;

use Yii;
use frontend\modules\cash\models\InternalTransferForm;
use yii\base\Action;
use yii\base\Exception;
use yii\base\UserException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class UpdateInternalTransferAction extends Action
{
    public $redirectUrl;
    public $view;
    public $layout;
    public $findModel;
    public $modelClass;

    /**
     * Set true if you need to copy an existing transfer
     * @var boolean
     */
    public $copyMode = false;

    /**
     * Runs the action.
     *
     * @return string result content
     */
    public function run($id)
    {
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if ($this->layout !== null) {
            $this->controller->layout = $this->layout;
        }

        $model = new InternalTransferForm($company, $employee, $model, [
            'copyMode' => $this->copyMode,
        ]);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Операция перевода между счетами добавлена');

            if ($this->redirectUrl !== null) {
                $redirectUrl = call_user_func($this->redirectUrl, $model, $this);
            }

            return $this->controller->redirect($redirectUrl ?? Yii::$app->request->referrer);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->controller->renderAjax($this->view ?: $this->id, [
                    'model' => $model,
                ]);
            } else {
                return $this->controller->render($this->view ?: $this->id, [
                    'model' => $model,
                ]);
            }
        }
    }

    public function findModel($id)
    {
        if ($this->findModel !== null) {
            return call_user_func($this->findModel, $id, $this);
        }

        /* @var $modelClass ActiveRecordInterface */
        $modelClass = $this->modelClass;
        $keys = $modelClass::primaryKey();
        if (count($keys) > 1) {
            $values = explode(',', $id);
            if (count($keys) === count($values)) {
                $model = $modelClass::findOne(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $model = $modelClass::findOne($id);
        }

        if (isset($model)) {
            return $model;
        }

        throw new NotFoundHttpException("Object not found: $id");
    }
}
