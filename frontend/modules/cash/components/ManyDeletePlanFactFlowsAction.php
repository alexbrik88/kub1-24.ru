<?php

namespace frontend\modules\cash\components;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogHelper;
use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class ManyDeletePlanFactFlowsAction extends Action
{
    public function run()
    {
        $data = Yii::$app->request->post('flowId');
        foreach ((array)$data as $tb => $idArray) {
            foreach ((array)$idArray as $id) {
                if ($model = $this->findFlowItem($tb, $id, true)) {
                    if ($model instanceof PlanCashFlows || $model instanceof PlanCashForeignCurrencyFlows) {
                        $model->delete();
                    } else {
                        LogHelper::delete($model, LogEntityType::TYPE_CASH);
                    }
                }
            }
        }

        Yii::$app->session->setFlash('success', 'Операции удалены.');

        return $this->controller->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $tableName
     * @param $id
     * @param $skipNotFound
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id, $skipNotFound = false)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::class;
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::class;
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::class;
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::class;
                break;
            case CashBankForeignCurrencyFlows::tableName():
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case CashOrderForeignCurrencyFlows::tableName():
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case CashEmoneyForeignCurrencyFlows::tableName():
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                $className = PlanCashForeignCurrencyFlows::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist!');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }
}
