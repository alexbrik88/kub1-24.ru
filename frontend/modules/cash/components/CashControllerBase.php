<?php

namespace frontend\modules\cash\components;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Contractor;
use common\models\document\Invoice;
use frontend\components\FrontendController;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogHelper;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\models\InternalTransferForm;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CashControllerBase
 * @package frontend\modules\cash\components
 */
abstract class CashControllerBase extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'strictAccess' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'create',
                            'update',
                            'many-item',
                            'delete',
                            'many-delete',
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => [permissions\document\Document::STRICT_MODE],
                    ],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'many-item',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                    [
                        'actions' => ['delete', 'many-delete'],
                        'allow' => true,
                        'roles' => [permissions\Cash::DELETE],
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Contractor::CREATE],
                        'roleParams' => [
                            'type' => Yii::$app->request->getQueryParam('type'),
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function getIsForeign()
    {
        return (bool) Yii::$app->getRequest()->get('foreign');
    }

    /**
     * @return mixed
     */
    abstract public function actionIndex();

    /**
     * @return mixed
     */
    abstract public function actionCreate();

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        if (Yii::$app->request->get('is_plan_flow')) {
            return $this->runAction('update-plan', ['flowId' => $id]);
        }

        $model = $this->findModel($id);

        if ($this->_deleteModel($model)) {
            if ($model instanceof CashBankFlowsForm) {
                Yii::$app->session->setFlash('success', 'Операция по банку удалена');
            } elseif ($model instanceof CashOrderFlows) {
                $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
                Yii::$app->session->setFlash('success', $message . ' кассовый ордер удален');
            } elseif ($model instanceof CashEmoneyFlows) {
                Yii::$app->session->setFlash('success', 'Операция по E-money удалена');
            }
            // prevent redirect to deleted page
            if (strpos(Yii::$app->request->referrer, '/view') !== false) {
                return $this->redirect(['/cash/order/index']);
            }
        } else {
            Yii::$app->session->setFlash('success', 'Операция не удалена');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param integer $type
     * @return mixed
     */
    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        foreach ($idArray as $id) {
            if ($model = $this->findModel($id)) {
                $this->_deleteModel($model);
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionManyItem()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');

        foreach ($idArray as $id) {
            try {
                $model = $this->getIsForeign() ? $this->findForeignModel($id) : $this->findModel($id, false);
                if ($model->is_internal_transfer) {
                    continue;
                }
            } catch (\Exception $e) {
                continue;
            }
            $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
            $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
            if ($model instanceof CashBankFlows) {
                $model->setScenario('update');
            }
            $model->$attribute = $value;
            $model->save(true, [$attribute]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param CashFlowsBase $flowModel
     * @return boolean
     */
    protected function _deleteModel($flowModel)
    {
        return LogHelper::delete($flowModel, LogEntityType::TYPE_CASH);
    }

    /**
     * @param integer $id
     * @return CashFlowsBase the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    abstract protected function findModel($id);
}
