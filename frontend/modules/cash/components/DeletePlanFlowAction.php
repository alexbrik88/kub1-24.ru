<?php

namespace frontend\modules\cash\components;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class DeletePlanFlowAction extends Action
{
    public $walletType;

    public function run($flowId)
    {
        $model = $this->_findPlanModel($this->walletType, $flowId, true);

        if (!$model || $model->delete()) {
            Yii::$app->session->setFlash('success', 'Плановая операция удалена.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении плановой операции.');
        }

        return $this->controller->redirect(
            Yii::$app->request->post('redirect', Yii::$app->request->referrer ?? ['index'])
        );
    }

    private function _findPlanModel($walletType, $id, $skipNotFound = false)
    {
        $company = Yii::$app->user->identity->company;
        $model = null;

        switch ($walletType) {
            case CashFlowsBase::WALLET_BANK:
            case CashFlowsBase::WALLET_CASHBOX:
            case CashFlowsBase::WALLET_EMONEY:
                $model = PlanCashFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
            case CashFlowsBase::WALLET_FOREIGN_BANK:
            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                $model = PlanCashForeignCurrencyFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
        }

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }
}
