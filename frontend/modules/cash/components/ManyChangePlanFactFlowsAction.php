<?php

namespace frontend\modules\cash\components;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class ManyChangePlanFactFlowsAction extends Action
{
    public function run()
    {
        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                if ($model instanceof PlanCashFlows) {
                    $model->save(true, [$attribute]);
                } else {
                    LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, function ($model) use ($attribute) {
                        return $model->save(true, [$attribute]);
                    });
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Статьи по операциям изменены.');

        return $this->controller->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $tableName
     * @param $id
     * @param $skipNotFound
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id, $skipNotFound = false)
    {
        switch ($tableName) {
            case CashBankFlows::tableName():
                $className = CashBankFlows::class;
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::class;
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::class;
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::class;
                break;
            case CashBankForeignCurrencyFlows::tableName():
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case CashOrderForeignCurrencyFlows::tableName():
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case CashEmoneyForeignCurrencyFlows::tableName():
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                $className = PlanCashForeignCurrencyFlows::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist!');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }
}
