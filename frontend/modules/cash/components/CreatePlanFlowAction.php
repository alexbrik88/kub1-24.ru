<?php

namespace frontend\modules\cash\components;

use Yii;
use yii\base\Action;
use yii\db\Exception;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\cash\form\CashBankFlowsForm;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class CreatePlanFlowAction extends Action
{
    public $walletType;
    public $view;

    public function run($flowType, $walletId)
    {
        $model = $this->_getNewModel($flowType, $this->walletType, $walletId);

        if (Yii::$app->request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($this->_saveNewPlanModel($model)) {
                \frontend\components\facebook\EventTracker::event('connectbank');
            }

            return $this->controller->redirect(
                Yii::$app->request->post('redirect', Yii::$app->request->referrer ?? ['index'])
            );
        }

        if (Yii::$app->request->isAjax) {
            return $this->controller->renderAjax($this->view, [
                'model' => $model,
            ]);
        } else {
            return $this->controller->render($this->view, [
                'model' => $model,
            ]);
        }
    }
    
    private function _getNewModel($flowType, $walletType, $walletId)
    {
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;

        switch ($walletType) {

            case CashFlowsBase::WALLET_BANK:
                $account =
                    $company->getCheckingAccountants()->andWhere(['rs' => $walletId])->orderBy(['type' => SORT_ASC])->one() ?:
                        $company->mainCheckingAccountant;

                $model = new CashBankFlowsForm([
                    'scenario' => 'create',
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'rs' => $account->rs ?? null,
                    'bank_name' => $account->bank_name ?? null,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'has_invoice' => false,
                ]);
                break;

            case CashFlowsBase::WALLET_FOREIGN_BANK:
                $account =
                    $company->getCheckingAccountants()->andWhere(['rs' => $walletId])->one() ?:
                        $company->mainForeignCurrencyAccount;

                $model = new CashBankForeignCurrencyFlows([
                    //'scenario' => 'create',
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'rs' => $account->rs ?? null,
                    'swift' => $account->swift ?? null,
                    'bank_name' => $account->bank_name ?? null,
                    'bank_address' => $account->bank_address ?? null,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('Y-m-d'),
                    'has_invoice' => false,
                ]);
                break;

            case CashFlowsBase::WALLET_CASHBOX:
                $cashbox =
                    $user->getCashboxes()->andWhere(['id' => $walletId])->one() ?:
                        $user->getCashboxes()->andWhere(['is_main' => 1])->one();

                $model = new CashOrderFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'author_id' => $user->id,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'cashbox_id' => $cashbox->id ?? null,
                    'is_accounting' => $cashbox->is_accounting ?? null,
                ]);
                break;

            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
                $cashbox =
                    $user->getCashboxes()->andWhere(['id' => $walletId])->one() ?:
                        null;

                $model = new CashOrderForeignCurrencyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'author_id' => $user->id,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'cashbox_id' => $cashbox->id ?? null,
                    'is_accounting' => $cashbox->is_accounting ?? null,
                ]);

                $model->setNextNumber();
                break;

            case CashFlowsBase::WALLET_EMONEY:
                $emoney =
                    $company->getEmoneys()->andWhere(['id' => $walletId])->one() ?:
                        $company->getEmoneys()->andWhere(['id' => $company->emoney])->one();

                $model = new CashEmoneyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'emoney_id' => $emoney->id ?? null,
                    'is_accounting' => $emoney->is_accounting ?? null,
                    'date' => date('d.m.Y'),
                ]);

                break;

            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                $emoney =
                    $company->getEmoneys()->andWhere(['id' => $walletId])->one() ?:
                        null;

                $model = new CashEmoneyForeignCurrencyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'emoney_id' => $emoney->id ?? null,
                    'is_accounting' => $emoney->is_accounting ?? null,
                    'date' => date('d.m.Y'),
                ]);
                break;

            default:

                throw new Exception('Wallet type not found:' . $walletType);
        }

        // Contractor Tab
        if ($cid = Yii::$app->request->get('contractorId')) {
            /** @var Contractor $contractor */
            if ($contractor = Contractor::findOne(['id' => $cid, 'company_id' => $company->id])) {
                $model->contractor_id = $contractor->id;
                $model->expenditure_item_id = $contractor->invoice_expenditure_item_id;
                $model->income_item_id = $contractor->invoice_income_item_id;
                $model->flow_type = ($contractor->type == Contractor::TYPE_CUSTOMER) ?
                    CashFlowsBase::FLOW_TYPE_INCOME :
                    CashFlowsBase::FLOW_TYPE_EXPENSE;
            }
        }

        return $model;
    }

    private function _saveNewPlanModel($model)
    {
        switch ($model->walletType) {
            case CashFlowsBase::WALLET_BANK:
                $flowData = Yii::$app->request->post('CashBankFlowsForm', []);
                $flowData['checking_accountant_id'] = (int)CheckingAccountant::find()->where(['rs' => $flowData['rs'], 'company_id' => $model->company_id])->select('id')->scalar();
                break;
            case CashFlowsBase::WALLET_FOREIGN_BANK:
                $flowData = Yii::$app->request->post('CashBankForeignCurrencyFlows', []);
                $flowData['checking_accountant_id'] = (int)CheckingAccountant::find()->where(['rs' => $flowData['rs'], 'company_id' => $model->company_id])->select('id')->scalar();
                break;
            case CashFlowsBase::WALLET_CASHBOX:
                $flowData = Yii::$app->request->post('CashOrderFlows', []);
                $flowData['contractor_id'] = \common\components\helpers\ArrayHelper::getValue($flowData, 'contractorInput');
                break;
            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
                $flowData = Yii::$app->request->post('CashOrderForeignCurrencyFlows', []);
                break;
            case CashFlowsBase::WALLET_EMONEY:
                $flowData = Yii::$app->request->post('CashEmoneyFlows', []);
                break;
            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                $flowData = Yii::$app->request->post('CashEmoneyForeignCurrencyFlows', []);
                break;
            default:
                $flowData = [];
                break;
        }

        $planFlowData = ($model->isForeign) ?
            ['PlanCashForeignCurrencyFlows' => array_merge($flowData, Yii::$app->request->post('PlanCashForeignCurrencyFlows', []))] :
            ['PlanCashFlows' => array_merge($flowData, Yii::$app->request->post('PlanCashFlows', []))];

        $planModel = ($model->isForeign) ?
            (new PlanCashForeignCurrencyFlows) :
            (new PlanCashFlows);

        $planModel->company_id = $model->company_id;
        $planModel->payment_type = $model->walletType;
        $planModel->flow_type = $model->flow_type;

        if ($planModel->load($planFlowData) && $planModel->_save()) {
            Yii::$app->session->setFlash('success', 'Плановая операция добавлена');

            return true;
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка добавления плановой операции');
            // debug
            //if (YII_ENV_DEV) {
            //    $planModel->validate();
            //    var_dump($planModel->getErrors());
            //    exit;
            //}
        }

        return false;
    }
}
