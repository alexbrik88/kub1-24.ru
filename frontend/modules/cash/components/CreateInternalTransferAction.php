<?php

namespace frontend\modules\cash\components;

use Yii;
use frontend\modules\cash\models\InternalTransferForm;
use yii\base\Action;
use yii\base\Exception;
use yii\base\UserException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CreateInternalTransferAction extends Action
{
    public $redirectUrl;
    public $view;
    public $layout;

    /**
     * Runs the action.
     *
     * @return string result content
     */
    public function run()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $rs = Yii::$app->getRequest()->get('rs');
        $account = $rs ? $company->getCheckingAccountants()->andWhere([
            'rs' => $rs,
        ])->orderBy(['type' => SORT_ASC])->one() : null;

        if ($this->layout !== null) {
            $this->controller->layout = $this->layout;
        }

        $model = new InternalTransferForm($company, $employee);

        if ($account) {
            $model->account_from = $account->getInternalTransferId();
        }

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Операция перевода между счетами добавлена');

            if ($this->redirectUrl !== null) {
                $redirectUrl = call_user_func($this->redirectUrl, $model, $this);
            }

            return $this->controller->redirect($redirectUrl ?? Yii::$app->request->referrer);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->controller->renderAjax($this->view ?: $this->id, [
                    'model' => $model,
                ]);
            } else {
                return $this->controller->render($this->view ?: $this->id, [
                    'model' => $model,
                ]);
            }
        }
    }
}
