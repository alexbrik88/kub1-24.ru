<?php

namespace frontend\modules\cash\components;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class UpdatePlanFlowAction extends Action
{
    public $walletType;
    public $view;

    public function run($flowId)
    {
        $model = $this->_findPlanModel($this->walletType, $flowId);

        if (Yii::$app->request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }



        if ($model->load(Yii::$app->request->post())) {

            if ($model->_update())
                Yii::$app->session->setFlash('success', 'Плановая операция обновлена');
            else {

                var_dump($model->getErrors());
                exit;

                Yii::$app->session->setFlash('success', 'Не удалось обновить плановую операцию');
            }

            return $this->controller->redirect(
                Yii::$app->request->post('redirect', Yii::$app->request->referrer ?? ['index'])
            );
        }

        if (Yii::$app->request->isAjax) {
            return $this->controller->renderAjax($this->view, [
                'model' => $model,
                'isPlan' => true
            ]);
        } else {
            return $this->controller->render($this->view, [
                'model' => $model,
                'isPlan' => true
            ]);
        }
    }

    private function _findPlanModel($walletType, $id, $skipNotFound = false)
    {
        $company = Yii::$app->user->identity->company;
        $model = null;

        switch ($walletType) {
            case CashFlowsBase::WALLET_BANK:
            case CashFlowsBase::WALLET_CASHBOX:
            case CashFlowsBase::WALLET_EMONEY:
                $model = PlanCashFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
            case CashFlowsBase::WALLET_FOREIGN_BANK:
            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                $model = PlanCashForeignCurrencyFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
        }

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }
}
