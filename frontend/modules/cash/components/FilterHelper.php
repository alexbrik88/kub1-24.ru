<?php
namespace frontend\modules\cash\components;

use common\models\Contractor;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class FilterHelper
 * @package frontend\modules\documents\components
 */
class FilterHelper
{
    /**
     * @return array
     */
    public static function getContractorFilterItems($searchQuery)
    {
        $tContractor = Contractor::tableName();
        $tFlow = key($searchQuery->from);

        $contractorIdArray = $searchQuery
                        ->distinct()
                        ->select("$tFlow.contractor_id")
                        ->column();

        $contractorArray1 = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["name" => $contractorIdArray])
                ->all(),
            'name',
            'text'
        );

        $contractorArray2 = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return (['' => 'Все контрагенты'] + $contractorArray1 + $contractorArray2);
    }
}
