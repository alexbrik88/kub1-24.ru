<?php
namespace frontend\modules\cash\components;

use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardAccountRepository;
use common\modules\cards\models\CardBill;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\CardOperationRepository;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use DateTime;
use DateTimeInterface;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use frontend\rbac\permissions;
use yii\helpers\Url;

/**
 * Class CashStatisticInfo
 * @package frontend\modules\cash\components
 */
class CashStatisticInfo
{
    protected static bool $calcBankTinChildren = false;

    public static function calcBankTinChildren($byTinChildren = true): void {
        self::$calcBankTinChildren = (bool)$byTinChildren;
    }

    /**
     * @param $ioType
     * @param $company
     * @param $dateRange
     * @param $projectId
     * @return array
     */
    public static function getStatisticInfo($ioType, $company, $dateRange, $projectId)
    {
        $cashBankQuery = CashBankFlows::find()
            ->asArray()
            ->andWhere(['flow_type' => $ioType])
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']]);

        if (self::$calcBankTinChildren) {
            $cashBankQuery->select([
                'COUNT(id) AS count',
                "SUM( 
                    IF(parent_id IS NULL, amount, tin_child_amount) 
                ) AS sum",
            ])->andWhere(['has_tin_children' => 0]);
        } else {
            $cashBankQuery->select([
                'COUNT(id) AS count',
                "SUM(amount) AS sum",
            ]);
        }

        $cashBank = $cashBankQuery->one();

        $cashOrder = CashOrderFlows::find()
            ->asArray()
            ->select([
                'COUNT(id) AS count',
                'SUM(amount) AS sum',
            ])
            ->andWhere(['flow_type' => $ioType])
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->one();

        $cashEmoney = CashEmoneyFlows::find()
            ->asArray()
            ->select([
                'COUNT(id) AS count',
                'SUM(amount) AS sum',
            ])
            ->andWhere(['flow_type' => $ioType])
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->one();

        $cashAcquiring = AcquiringOperation::find()
            ->asArray()
            ->select([
                'COUNT(id) AS count',
                'SUM(amount) AS sum',
            ])
            ->andWhere(['flow_type' => $ioType])
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->one();

        $cashCard = CardOperation::find()
            ->asArray()
            ->select([
                'COUNT(id) AS count',
                'SUM(amount) AS sum',
            ])
            ->andWhere(['flow_type' => $ioType])
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->one();

        return [
            'count' => $cashBank['count'] + $cashOrder['count'] + $cashEmoney['count'] + $cashAcquiring['count'] + $cashCard['count'],
            'sum' => $cashBank['sum'] + $cashOrder['sum'] + $cashEmoney['sum'] + $cashAcquiring['sum'] + $cashCard['sum'],
        ];
    }

    /**
     * @param $company
     * @param $projectId
     * @param $beforeDate
     * @return int
     */
    public static function getStartBalanceInfo($company, $beforeDate, $projectId)
    {
        $balanceBank = CashBankFlows::find()
            ->select(new Expression('SUM(IF(flow_type = 0, -amount, amount)) AS balance'))
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['<', 'date', $beforeDate])
            ->scalar();

        $balanceOrder = CashOrderFlows::find()
            ->select(new Expression('SUM(IF(flow_type = 0, -amount, amount)) AS balance'))
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['<', 'date', $beforeDate])
            ->scalar();

        $balanceEmoney = CashEmoneyFlows::find()
            ->select(new Expression('SUM(IF(flow_type = 0, -amount, amount)) AS balance'))
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['<', 'date', $beforeDate])
            ->scalar();

        $balanceAcquiring = AcquiringOperation::find()
            ->select(new Expression('SUM(IF(flow_type = 0, -amount, amount)) AS balance'))
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['<', 'date', $beforeDate])
            ->scalar();

        $balanceCard = CardOperation::find()
            ->select(new Expression('SUM(IF(flow_type = 0, -amount, amount)) AS balance'))
            ->andWhere(['project_id' => $projectId])
            ->andWhere(['company_id' => $company])
            ->andWhere(['<', 'date', $beforeDate])
            ->scalar();

        return $balanceBank + $balanceOrder + $balanceEmoney + $balanceAcquiring + $balanceCard;
    }

    /**
     * @param array $period
     * @param bool $showBanks
     * @return array
     * @throws \yii\base\ErrorException
     */
    public static function getPeriodStatisticInfo($period = [], $showBanks = true)
    {
        /** @var Employee $employee */
        $user = Yii::$app->user;
        $employee = $user->identity;
        $company = $employee->company;
        $config = $employee->config;
        $hideZero = boolval($config->debts_cash_config_hide_zero);
        $byAccount = boolval($config->debts_cash_config_show_by_account);
        $periodFrom = ArrayHelper::getValue($period, 'from');
        $periodTo = ArrayHelper::getValue($period, 'to');

        if ($byAccount) {
            $bankArray = $company->getRubleAccounts()->select('name')->orderBy(['type' => SORT_ASC, 'bank_name' => SORT_ASC])->indexBy('rs')->column();
        } else {
            $bankArray = $company->getRubleAccounts()->select('bank_name')->groupBy('bik')->orderBy(['type' => SORT_ASC, 'bank_name' => SORT_ASC])->indexBy('bik')->column();
        }
        $cashboxArray = $employee->getCashboxes()->andWhere(['currency_id' => Currency::DEFAULT_ID])->select('name')->orderBy(['is_main' => SORT_DESC])->indexBy('id')->column();
        $emoneyArray = $company->getRubleEmoneys()->select('name')->orderBy(['is_main' => SORT_DESC])->indexBy('id')->column();
        $acquiringArray = Acquiring::find()->andWhere(['company_id' => $employee->currentEmployeeCompany->company_id])->all();
        $_rsArray = $company->getRubleAccounts()->select('rs')->groupBy('bik')->indexBy('bik')->column();
        $cardArray = CardBill::find()->alias('cb')
            ->leftJoin(CardAccount::tableName().' ca', 'ca.id = cb.account_id')
            ->where(['ca.company_id' => $employee->currentEmployeeCompany->company_id])
            ->select('cb.name')
            ->indexBy('cb.id')
            ->column();

        $data = [];

        $bikArray = ($showBanks) ?
            (count($bankArray) > 1 ? array_merge([''], array_keys($bankArray)) : ['']) : [];
        $isOne = count($bikArray) == 1;

        foreach ($bikArray as $bik) {
            $bankModel = new CashBankFlows([($byAccount ? 'rs' : 'bik') => $bik]);

            $endBalance = $bankModel->getBalanceAtEnd($periodTo, $company);
            if ($hideZero && $endBalance == 0) continue;

            $data[] = [
                'typeName' => $bik ? $bankArray[$bik] : 'Банк',
                'bik' => $bik ?: null,
                'beginBalance' => $bankModel->getBalanceAtBegin($periodFrom, $company),
                'income' => $bankModel->getPeriodIncome($periodFrom, $periodTo, $company),
                'expense' => $bankModel->getPeriodExpense($periodFrom, $periodTo, $company),
                'endBalance' => $endBalance,
                'inTotal' => $bik ? false : true,
                'target' => $bik ? null : '.cash-bank-substring',
                'cssClass' => $bik ? 'cash-bank-substring' : ($isOne ? 'single-string' : 'toggle-string'),
                'url' => $bik ? Url::to(['/cash/bank/index', 'rs' => $byAccount ? $bik : $_rsArray[$bik]]) : null,
            ];
        }

        $cashboxIds = array_keys($cashboxArray);
        $cashboxItems = $cashboxArray ? (count($cashboxArray) > 1 ? array_merge([''], $cashboxIds) : ['']) : [];
        $isOne = count($cashboxItems) == 1;
        foreach ($cashboxItems as $cashbox_id) {
            $orderModel = new CashOrderFlows(['cashbox_id' => $cashbox_id ?: $cashboxIds]);

            $endBalance = $orderModel->getBalanceAtEnd($periodTo, $company);
            if ($hideZero && $endBalance == 0) continue;

            $data[] = [
                'typeName' => $cashbox_id ? $cashboxArray[$cashbox_id] : 'Касса',
                'cashboxId' => $cashbox_id ?: null,
                'beginBalance' => $orderModel->getBalanceAtBegin($periodFrom, $company),
                'income' => $orderModel->getPeriodIncome($periodFrom, $periodTo, $company),
                'expense' => $orderModel->getPeriodExpense($periodFrom, $periodTo, $company),
                'endBalance' => $endBalance,
                'inTotal' => $cashbox_id ? false : true,
                'target' => $cashbox_id ? null : '.cash-order-substring',
                'cssClass' => $cashbox_id ? 'cash-order-substring' : ($isOne ? 'single-string' : 'toggle-string'),
                'url' => $cashbox_id ? Url::to(['/cash/order/index', 'cashbox' => $cashbox_id]) : null,
            ];
        }

        //$data = array_merge($data, self::getCardsStatistic($user->identity->company, $periodFrom, $periodTo));

        $acquiringItems = $acquiringArray ? (count($acquiringArray) > 1 ? array_merge([''], $acquiringArray) : ['']) : [];
        $isOne = count($acquiringItems) == 1;
        /** @var Acquiring $acquiring */
        foreach ($acquiringItems as $acquiring) {
            if ($acquiring) {
                $acquiringModel = new AcquiringOperation([
                    'company_id' => $company->id,
                    'acquiring_type' => $acquiring->type,
                    'acquiring_identifier' => $acquiring->identifier,
                ]);
            } else {
                $acquiringModel = new AcquiringOperation(['company_id' => $company->id]);
            }

            $endBalance = $acquiringModel->getBalanceAtEnd($periodTo, $company);
            if ($hideZero && $endBalance == 0) continue;

            $data[] = [
                'typeName' => $acquiring ? $acquiring->getFullName() : 'Эквайринг',
                'emoneyId' => $acquiring ? $acquiring->id : null,
                'beginBalance' => $acquiringModel->getBalanceAtBegin($periodFrom, $company),
                'income' => $acquiringModel->getPeriodIncome($periodFrom, $periodTo, $company),
                'expense' => $acquiringModel->getPeriodExpense($periodFrom, $periodTo, $company),
                'endBalance' => $endBalance,
                'inTotal' => $acquiring ? false : true,
                'target' => $acquiring ? null : '.cash-acquiring-substring',
                'cssClass' => $acquiring ? 'cash-acquiring-substring' : ($isOne ? 'single-string' : 'toggle-string'),
                'url' => $acquiring ? Url::to(['/acquiring', 'activeItem' => $acquiring->id]) : null,
            ];
        }

        $cardIds = array_keys($cardArray);
        $cardItems = $cardArray ? (count($cardArray) > 1 ? array_merge([''], $cardIds) : ['']) : [];
        $isOne = count($cardItems) == 1;

        foreach ($cardItems as $bill_id) {
            if ($bill_id) {
                $cardModel = new CardOperation([
                    'company_id' => $company->id,
                    'bill_id' => $bill_id,
                ]);
            } else {
                $cardModel = new CardOperation(['company_id' => $company->id]);
            }

            $endBalance = $cardModel->getBalanceAtEnd($periodTo, $company);
            if ($hideZero && $endBalance == 0) continue;

            $data[] = [
                'typeName' => $bill_id ? $cardArray[$bill_id] : 'Карты',
                'emoneyId' => $bill_id ? $bill_id : null,
                'beginBalance' => $cardModel->getBalanceAtBegin($periodFrom, $company),
                'income' => $cardModel->getPeriodIncome($periodFrom, $periodTo, $company),
                'expense' => $cardModel->getPeriodExpense($periodFrom, $periodTo, $company),
                'endBalance' => $endBalance,
                'inTotal' => $bill_id ? false : true,
                'target' => $bill_id ? null : '.cash-card-substring',
                'cssClass' => $bill_id ? 'cash-card-substring' : ($isOne ? 'single-string' : 'toggle-string'),
                'url' => $bill_id ? Url::to(['/cards', 'account_id' => $cardModel->bill->account_id, 'CardOperationRepository[bill_name]' => $cardModel->bill->name]) : null,
            ];
        }

        $emoneyIds = array_keys($emoneyArray);
        $emoneyItems = $emoneyArray ? (count($emoneyArray) > 1 ? array_merge([''], $emoneyIds) : ['']) : [];
        $isOne = count($emoneyItems) == 1;
        foreach ($emoneyItems as $emoney_id) {
            $emoneyModel = new CashEmoneyFlows(['emoney_id' => $emoney_id ?: $emoneyIds]);

            $endBalance = $emoneyModel->getBalanceAtEnd($periodTo, $company);
            if ($hideZero && $endBalance == 0) continue;

            $data[] = [
                'typeName' => $emoney_id ? $emoneyArray[$emoney_id] : 'E-money',
                'emoneyId' => $emoney_id ?: null,
                'beginBalance' => $emoneyModel->getBalanceAtBegin($periodFrom, $company),
                'income' => $emoneyModel->getPeriodIncome($periodFrom, $periodTo, $company),
                'expense' => $emoneyModel->getPeriodExpense($periodFrom, $periodTo, $company),
                'endBalance' => $endBalance,
                'inTotal' => $emoney_id ? false : true,
                'target' => $emoney_id ? null : '.cash-emoney-substring',
                'cssClass' => $emoney_id ? 'cash-emoney-substring' : ($isOne ? 'single-string' : 'toggle-string'),
                'url' => $emoney_id ? Url::to(['/cash/e-money/index', 'emoney' => $emoney_id]) : null,
            ];
        }

        // Т.к. Yii-шный GridView не поддерживает ряд итогов, то симулируем его сами
        // Хитровыделаная логика ниже результат простого выпедривания и желания сделать
        // код настолько универсальным, насколько это возможно - такие дела
        $index = count($data);
        $data[$index] = [
            'typeName' => 'Итого',
            'target' => null,
        ];

        foreach ($data as $row) {
            if (!empty($row['inTotal'])) {
                foreach ($row as $column => $value) {
                    if (in_array($column, ['beginBalance', 'income', 'expense', 'endBalance'])) {
                        if (array_key_exists($column, $data[$index])) {
                            $data[$index][$column] += $value;
                        } else {
                            $data[$index][$column] = $value;
                        }
                    }
                }
            }
        }

        $data[$index]['cssClass'] = 'summ';

        return $data;
    }

    /**
     * @param array $period
     * @param bool $showBanks
     * @return array
     */
    public static function getForeignCurrensyStatistic($period = [], $showBanks = true)
    {
        if (!$showBanks)
            return [];

        $foreignData = [];

        /** @var Employee $employee */
        $user = Yii::$app->user;
        $employee = $user->identity;
        $company = $employee->company;
        $config = $employee->config;
        $hideZero = boolval($config->debts_cash_config_hide_zero);
        $byAccount = boolval($config->debts_cash_config_show_by_account);
        $currensyArray = \common\models\currency\Currency::getAllCurrency();
        $periodFrom = ArrayHelper::getValue($period, 'from', StatisticPeriod::getSessionPeriod()['from']);
        $periodTo = ArrayHelper::getValue($period, 'to', StatisticPeriod::getSessionPeriod()['to']);
        if (is_string($periodFrom)) {
            $periodFrom = date_create($periodFrom);
        }
        if (is_string($periodTo)) {
            $periodTo = date_create($periodTo);
        }

        $foreignCurrencyAccountArray = $company->getForeignCurrencyAccounts()->orderBy(['type' => SORT_ASC, 'bank_name' => SORT_ASC])->indexBy('id')->all();
        $foreignCurrencyCashboxArray = $employee->getCashboxes()->andWhere(['not', ['currency_id' => Currency::DEFAULT_ID]])->orderBy(['is_main' => SORT_DESC])->indexBy('id')->all();
        $foreignCurrencyEmoneyArray = $company->getForeignCurrencyEmoneys()->orderBy(['is_main' => SORT_DESC])->indexBy('id')->all();

        if (!empty($foreignCurrencyAccountArray)) {
            $accountRsId = [];
            $accountIdArray = [];
            $currencyIdArray = [];
            foreach ($foreignCurrencyAccountArray as $id => $account) {
                $bank_id = mb_strtoupper(strval($account->swift ?: $account->bik));
                $account_id = $account->iban ?: $account->rs;
                $itemKey = $byAccount ? $account_id : $bank_id;
                $currencyId = $account->currency_id;
                if ($account->iban) {
                    $accountIdArray[] = $account->iban;
                    $accountRsId[$account->iban] = $bank_id;
                }
                if ($account->rs) {
                    $accountIdArray[] = $account->rs;
                    $accountRsId[$account->rs] = $bank_id;
                }
                $currencyIdArray[] = $currencyId;
                if (!isset($foreignData[$currencyId])) {
                    $foreignData[$currencyId]['name'] = $currensyArray[$currencyId];
                    $foreignData[$currencyId]['balance'] = 0;
                    $foreignData[$currencyId]['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['bank'])) {
                    $foreignData[$currencyId]['items']['bank']['name'] = 'Банк';
                    $foreignData[$currencyId]['items']['bank']['balance'] = 0;
                    $foreignData[$currencyId]['items']['bank']['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['bank']['items'][$itemKey])) {
                    $foreignData[$currencyId]['items']['bank']['items'][$itemKey] = [
                        'name' => $byAccount ? $account->name : strval($account->bank_name ?: $account->bank_name_en),
                        'balance' => 0,
                    ];
                }
            }
            $query = $company->getCashBankForeignCurrencyFlows()->select([
                'currency_id',
                'rs',
                'balance' => 'SUM(IF([[flow_type]]=:typeIncome, [[amount]], -[[amount]]))',
            ])->andWhere([
                'currency_id' => array_filter(array_unique($currencyIdArray)),
                'rs' => array_filter(array_unique($accountIdArray)),
            ])->andWhere([
                '<=',
                'date',
                $periodTo->format('Y-m-d'),
            ])->params([
                ':typeIncome' => CashFlowsBase::FLOW_TYPE_INCOME,
            ])->groupBy('[[currency_id]], [[rs]] WITH ROLLUP');

            $queryResult = $query->asArray()->all();

            foreach ($queryResult as $row) {
                if ($row['currency_id']) {
                    if ($row['rs'] === null) {
                        $foreignData[$row['currency_id']]['balance'] += $row['balance'];
                    } else {
                        $bank_id = $byAccount ? $row['rs'] : ($accountRsId[$row['rs']] ?? '');
                        $foreignData[$row['currency_id']]['items']['bank']['balance'] += $row['balance'];
                        $foreignData[$row['currency_id']]['items']['bank']['items'][$bank_id]['balance'] = $row['balance'];
                    }
                }
            }
        }

        if (!empty($foreignCurrencyCashboxArray)) {
            $accountIdArray = [];
            $currencyIdArray = [];
            foreach ($foreignCurrencyCashboxArray as $account) {
                $currencyId = $account->currency_id;
                $accountIdArray[] = $account->id;
                $currencyIdArray[] = $currencyId;
                if (!isset($foreignData[$currencyId])) {
                    $foreignData[$currencyId]['name'] = $currensyArray[$currencyId];
                    $foreignData[$currencyId]['balance'] = 0;
                    $foreignData[$currencyId]['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['order'])) {
                    $foreignData[$currencyId]['items']['order']['name'] = 'Касса';
                    $foreignData[$currencyId]['items']['order']['balance'] = 0;
                    $foreignData[$currencyId]['items']['order']['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['order']['items'][$account->id])) {
                    $foreignData[$currencyId]['items']['order']['items'][$account->id] = [
                        'name' => $account->name,
                        'balance' => 0,
                    ];
                }
            }
            $query = $company->getCashOrderForeignCurrencyFlows()->select([
                'currency_id',
                'cashbox_id',
                'balance' => 'SUM(IF([[flow_type]]=:typeIncome, [[amount]], -[[amount]]))',
            ])->andWhere([
                'currency_id' => $currencyIdArray,
                'cashbox_id' => $accountIdArray,
            ])->andWhere([
                '<=',
                'date',
                $periodTo->format('Y-m-d'),
            ])->params([
                ':typeIncome' => CashFlowsBase::FLOW_TYPE_INCOME,
            ])->groupBy('[[currency_id]], [[cashbox_id]] WITH ROLLUP');

            $queryResult = $query->asArray()->all();

            foreach ($queryResult as $row) {
                if ($row['currency_id']) {
                    if ($row['cashbox_id'] === null) {
                        $foreignData[$row['currency_id']]['balance'] += $row['balance'];
                    } else {
                        $foreignData[$row['currency_id']]['items']['order']['balance'] += $row['balance'];
                        $foreignData[$row['currency_id']]['items']['order']['items'][$row['cashbox_id']]['balance'] += $row['balance'];
                    }
                }
            }
        }

        if (!empty($foreignCurrencyEmoneyArray)) {
            $accountIdArray = [];
            $currencyIdArray = [];
            foreach ($foreignCurrencyEmoneyArray as $account) {
                $currencyId = $account->currency_id;
                $accountIdArray[] = $account->id;
                $currencyIdArray[] = $currencyId;
                if (!isset($foreignData[$currencyId])) {
                    $foreignData[$currencyId]['name'] = $currensyArray[$currencyId];
                    $foreignData[$currencyId]['balance'] = 0;
                    $foreignData[$currencyId]['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['emoney'])) {
                    $foreignData[$currencyId]['items']['emoney']['name'] = 'E-money';
                    $foreignData[$currencyId]['items']['emoney']['balance'] = 0;
                    $foreignData[$currencyId]['items']['emoney']['items'] = [];
                }
                if (!isset($foreignData[$currencyId]['items']['emoney']['items'][$account->id])) {
                    $foreignData[$currencyId]['items']['emoney']['items'][$account->id] = [
                        'name' => $account->name,
                        'balance' => 0,
                    ];
                }
            }
            $query = $company->getCashEmoneyForeignCurrencyFlows()->select([
                'currency_id',
                'emoney_id',
                'balance' => 'SUM(IF([[flow_type]]=:typeIncome, [[amount]], -[[amount]]))',
            ])->andWhere([
                'currency_id' => $currencyIdArray,
                'emoney_id' => $accountIdArray,
            ])->andWhere([
                '<=',
                'date',
                $periodTo->format('Y-m-d'),
            ])->params([
                ':typeIncome' => CashFlowsBase::FLOW_TYPE_INCOME,
            ])->groupBy('[[currency_id]], [[emoney_id]] WITH ROLLUP');

            $queryResult = $query->asArray()->all();

            foreach ($queryResult as $row) {
                if ($row['currency_id']) {
                    if ($row['emoney_id'] === null) {
                        $foreignData[$row['currency_id']]['balance'] += $row['balance'];
                    } else {
                        $foreignData[$row['currency_id']]['items']['emoney']['balance'] += $row['balance'];
                        $foreignData[$row['currency_id']]['items']['emoney']['items'][$row['emoney_id']]['balance'] += $row['balance'];
                    }
                }
            }
        }

        if ($hideZero) {
            foreach ($foreignData as $key1 => $value1) {
                if ($value1['balance'] == 0) {
                    unset($foreignData[$key1]);
                    continue;
                }
                foreach ($value1['items'] as $key2 => $value2) {
                    if ($value2['balance'] == 0) {
                        unset($foreignData[$key1]['items'][$key2]);
                        continue;
                    }
                    foreach ($value2['items'] as $key3 => $value3) {
                        if ($value3['balance'] == 0) {
                            unset($foreignData[$key1]['items'][$key2]['items'][$key3]);
                        }
                    }
                }
            }
        }

        return $foreignData;
    }

    /**
     * @param $dateFrom
     * @param $dateTo
     * @param $company
     * @return array
     */
    public static function getSumByDayForPeriod($dateFrom, $dateTo, Company $company)
    {
        if ($dateFrom instanceof DateTime) {
            $dateFrom = $dateFrom->format('Y-m-d');
        }
        if ($dateTo instanceof DateTime) {
            $dateTo = $dateTo->format('Y-m-d');
        }

        $select = ['pay.amount', 'flow.date'];
        $where = ['between', 'flow.date', $dateFrom, $dateTo];
        $query = new Query;
        $query1 = self::getInvoiceQuery($company)->select($select)
            ->innerJoin(['pay' => CashBankFlowToInvoice::tableName()], '{{pay}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin(['flow' => CashBankFlows::tableName()], '{{pay}}.[[flow_id]] = {{flow}}.[[id]]')
            ->andWhere($where);
        $query2 = self::getInvoiceQuery($company)->select($select)
            ->innerJoin(['pay' => CashEmoneyFlowToInvoice::tableName()], '{{pay}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin(['flow' => CashEmoneyFlows::tableName()], '{{pay}}.[[flow_id]] = {{flow}}.[[id]]')
            ->andWhere($where);
        $query3 = self::getInvoiceQuery($company)->select($select)
            ->innerJoin(['pay' => CashOrderFlowToInvoice::tableName()], '{{pay}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin(['flow' => CashOrderFlows::tableName()], '{{pay}}.[[flow_id]] = {{flow}}.[[id]]')
            ->andWhere($where);

        return $query
            ->select('SUM([[amount]]), [[date]]')
            ->from(['t' => $query1->union($query2, true)->union($query3, true)])
            ->groupBy(['date'])
            ->orderBy(['date' => SORT_ASC])
            ->indexBy('date')
            ->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getInvoiceQuery(Company $company)
    {
        return $company->getInvoices()
            ->andWhere([
                'type' => Documents::IO_TYPE_OUT,
                'is_deleted' => false,
                'invoice_status_id' => InvoiceStatus::$validInvoices,
            ]);
    }

//    /**
//     * @param Company $company
//     * @param DateTimeInterface|null $dateFrom
//     * @param DateTimeInterface|null $dateTo
//     * @return array
//     */
//    private static function getCardsStatistic(
//        Company $company,
//        ?DateTimeInterface $dateFrom,
//        ?DateTimeInterface $dateTo
//    ): array {
//        $rows = [];
//        $accounts = CardAccountRepository::findAccounts($company);
//        $isOne = (count($accounts) == 1);
//
//        foreach (array_merge([null], $accounts) as $account) {
//            $repository = new CardOperationRepository([
//                'company' => $company,
//                'account' => $account,
//                'dateFrom' => $dateFrom,
//                'dateTo' => $dateTo,
//            ]);
//            $summary = $repository->getSummary();
//            $rows[] = [
//                'typeName' => $account ? $account->getFullName() : 'Карты',
//                'emoneyId' => $account ? $account->id : null,
//                'beginBalance' => $summary->getStartBalance(),
//                'income' => $summary->getTotalIncome(),
//                'expense' => $summary->getTotalExpense(),
//                'endBalance' => $summary->getEndBalance(),
//                'inTotal' => ($account === null),
//                'target' => $account ? null : '.cash-cards-substring',
//                'cssClass' => $account ? 'cash-cards-substring' : ($isOne ? 'single-string' : 'toggle-string'),
//                'url' => $account ? Url::to(['/card', 'account_id' => $account->id]) : null,
//            ];
//
//            if ($isOne) {
//                break;
//            }
//        }
//
//        return $rows;
//    }
}
