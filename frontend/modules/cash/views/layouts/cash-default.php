<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2018
 * Time: 7:02
 */

use common\models\Company;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use frontend\rbac\permissions;


$this->beginContent('@frontend/views/layouts/main.php');

$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
$module = Yii::$app->controller->module->id;
?>
<div class="debt-report-content container-fluid" style="padding: 0; margin-top: -10px;">
    <?php NavBar::begin([
        'options' => [
            'class' => 'navbar-report navbar-default',
        ],
        'brandOptions' => [
            'style' => 'margin-left: 0;'
        ],
        'containerOptions' => [
            'style' => 'padding: 0;'
        ],
        'innerContainerOptions' => [
            'class' => 'container-fluid',
            'style' => 'padding: 0;'
        ],
    ]);
    echo Nav::widget([
        'id' => 'debt-report-menu',
        'items' => [
            [
                'label' => 'Итого',
                'url' => ['/cash',],
                'active' => $module == 'cash' && $controller == 'default' && $action == 'index',
                'visible' => Yii::$app->user->can(permissions\Cash::INDEX),

            ],
            [
                'label' => 'ОДДС',
                'url' => ['/cash/default/odds',],
                'active' => $module == 'cash' && $controller == 'default' && $action == 'odds',
                'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
            ],
        ],
        'options' => ['class' => 'navbar-nav'],
    ]);
    NavBar::end(); ?>
    <?= $content; ?>
</div>
<?php $this->endContent(); ?>
