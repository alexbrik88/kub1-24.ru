<?php
/**
 * @var yii\web\View $this
 * @var CashOrderFlows $model
 * @var yii\bootstrap\ActiveForm $form
 */

use common\components\date\DateHelper;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\widgets\InvoiceListInputWidget;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use common\models\document\InvoiceExpenditureItem;

$company = Yii::$app->user->identity->company;
if ($model->amount) {
    $model->amount /= 100;
}

$flowTypeItems = CashOrderFlows::getFlowTypes();
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
    if (!$model->number) {
        $model->number = $model::getNextNumber($model->company_id, $model->flow_type, $model->date);
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}


$rsArray = $company->getCheckingAccountants()
    ->select('rs')
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' =>SORT_ASC])
    ->indexBy('id')
    ->column();

$userCashboxList = Yii::$app->user->identity->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$cashboxList = $company->getCashboxes()
    ->select(['name'])
    ->andWhere(['is_closed' => false])
    ->orderBy([
        'is_main' => SORT_DESC,
        'name' => SORT_ASC,
    ])
    ->indexBy('id')
    ->column();

$selfFlowQuery = CashContractorType::find()
    ->andWhere(['not', ['name'=> CashContractorType::COMPANY_TEXT]])
    ->andWhere(['not', ['name'=> CashContractorType::ORDER_TEXT]]);
if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $selfFlowQuery->andWhere(['not', ['name'=> CashContractorType::BALANCE_TEXT]]);
}
$selfFlowResult = $selfFlowQuery->select(['text', 'name'])->indexBy('name')->column();

$contractorListOptions = [];
$contractorData = [];
if (Yii::$app->request->get('canAddContractor')) {
    $contractorData += ['add-modal-contractor' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) ?
        '[ + Добавить поставщика ]' :
        '[ + Добавить покупателя ]'];
}

if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
    $contractorData = array_merge($contractorData, ArrayHelper::map(CashContractorType::find()
        ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
        ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
        ->all(), 'name', 'text'));
} else {
    $contractorData = array_merge($contractorData, ArrayHelper::map(CashContractorType::find()
        ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
        ->all(), 'name', 'text'));
}

switch ($model->flow_type) {
    case CashOrderFlows::FLOW_TYPE_INCOME:
        $contractor = 'Покупатель';
        $contractorType = Contractor::TYPE_CUSTOMER;
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_INCOME);
        //$contractorData += $company->sortedContractorList(Contractor::TYPE_CUSTOMER);
        //$contractorListOptions += Contractor::getAllContractorSelect2Options(Contractor::TYPE_CUSTOMER);
        break;
    case CashOrderFlows::FLOW_TYPE_EXPENSE:
        $contractor = 'Поставщик';
        $contractorType = Contractor::TYPE_SELLER;
        $reasonTypeArray = CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_EXPENSE);
        //$contractorData += $company->sortedContractorList(Contractor::TYPE_SELLER);
        //$contractorListOptions += Contractor::getAllContractorSelect2Options(Contractor::TYPE_SELLER);
        break;

    default:
        $contractor = 'Контрагент';
        $reasonTypeArray = [];
        $contractorData = [];
        break;
}

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($contractorData as $key=>$seller) {

        if ('add-modal-contractor' == $key)
            continue;

        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($contractorData[$key]);
        else
            $model->contractor_id = $key;
    }
}

$income = 'income' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' операцию по кассе';
?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'cash-order-form',
    'action' => empty($action) ? '' : $action,
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'form-horizontal',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
    ],
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ],
])); ?>

    <div class="form-body">
        <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
            <?= Html::hiddenInput('redirect', $redirect) ?>
        <?php endif ?>
        <?= $form->field($model, 'flow_type')->radioList($typeItems, [
            'encode' => false,
            'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                $input = Html::radio($name, $checked, [
                    'class' => 'flow-type-toggle-input',
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'class' => 'radio-inline m-l-n-md',
                        'style' => 'font-size: 14px;'
                    ]
                ]);
                if ($model->isNewRecord) {
                    return Html::a($input, ['create', 'type' => $value, 'id' => $model->cashbox_id], [
                        'class' => 'col-xs-5 m-l-n ajax-modal-btn',
                        'data-pjax' => '0',
                        'style' => 'padding: 0; color: #333333; text-decoration: none;'
                    ]);
                } else {
                    return Html::tag('div', $input, [
                        'class' => 'col-xs-5 m-l-n',
                        'style' => 'padding: 0;'
                    ]);
                }
            }
        ])->label('Тип'); ?>

        <?php if (count($userCashboxList) == 1) : ?>
            <?= $form->field($model, 'cashbox_id')->hiddenInput(['value' => key($userCashboxList)])->label(false) ?>
        <?php else : ?>
            <?= $form->field($model, 'cashbox_id')->widget(Select2::classname(), [
                'data' => $userCashboxList,
                'options' => [
                    'placeholder' => '',
                    'data' => [
                        'prefix' => CashContractorType::ORDER_TEXT . '.',
                        'target' => '#' . Html::getInputId($model, 'contractorInput'),
                    ],
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
        <?php endif; ?>

        <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => $contractorType,
            'staticData' => $contractorData,
            'data' => $contractorData,
            'options' => [
                'placeholder' => '',
                'options' => $contractorListOptions,
                'class' => 'contractor-items-depend cash_contractor_id_select ' . (
                    $model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? 'seller' : 'customer'
                ),
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label($contractor); ?>

        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
                echo $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group required',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(Select2::className(), [
                    'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                                ->where(['id' => [28, 46, 48, 53]])
                                ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                                ->all(), 'id', 'name'),
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                    ]
                ]);
            } ?>
        <?php else: ?>
            <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE) {
                echo $form->field($model, 'expenditure_item_id', [
                    'enableClientValidation' => false,
                    'options' => [
                        'class' => 'form-group required',
                        'jsValue' => $model->expenditure_item_id
                    ],
                ])->widget(ExpenditureDropdownWidget::classname(), [
                    'options' => [
                        'class' => 'flow-expense-items',
                        'prompt' => '--',
                    ],
                    'pluginOptions' => [
                    ]
                ]);
            } ?>

            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashorderflows-expenditure_item_id',
            ]); ?>
        <?php endif; ?>

        <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) {
            echo $form->field($model, 'income_item_id', [
                'enableClientValidation' => false,
                'options' => [
                    'class' => 'form-group required cash_income_item_id_select',
                    'jsValue' => $model->income_item_id
                ],
            ])->widget(ExpenditureDropdownWidget::classname(), [
                'income' => true,
                'options' => [
                    'class' => 'flow-income-items cash_income_item_id_select',
                    'prompt' => '--',
                ],
                'pluginOptions' => [
                ]
            ]);
        } ?>

        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashorderflows-income_item_id',
            'type' => 'income',
        ]); ?>

        <?= $form->field($model, 'amount')->textInput([
            'class' => 'form-control js_input_to_money_format',
            'style' => 'width: 100%; max-width: 125px;',
            'readonly' => $model->hasInvoice(),
        ]); ?>

        <?= $form->field($model, 'date', [
            'inputTemplate' => '<div class="input-icon input-calendar"><i class="fa fa-calendar"></i>{input}</div>',
        ])->textInput([
            'class' => 'form-control date-picker',
            'style' => 'width: 100%; max-width: 125px;',
            'value' => DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'data' => [
                'date-viewmode' => 'years',
            ],
        ]); ?>

        <?= $form->field($model, 'number')->textInput(['style' => 'width: 100%; max-width: 125px;',]); ?>

        <?= $form->field($model, 'description')->textarea(['style' => 'width: 100%;',]); ?>

        <?= $form->field($model, 'invoices_list', [
            'labelOptions' => [
                'class' => 'col-md-3 control-label width-label bold-text',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-9',
            ],
        ])->widget(InvoiceListInputWidget::className(), [
            'contractor_attribute' => 'contractorInput',
            'invoicesListUrl' => ['/cash/order/un-payed-invoices'],
        ]); ?>

        <?= $form->field($model, 'reason_id')->dropDownList(ArrayHelper::map($reasonTypeArray, 'id', 'name'), [
            'class' => 'form-control flow-reason-field',
            'style' => 'width: 100%;',
            'prompt' => 'Необходимо для печатного документа',
            'jsValue' => $model->reason_id,
        ]); ?>

        <?= $form->field($model, 'other', [
            'options' => [
                'class' => 'form-group other-reason-box',
                'data-other' => CashOrdersReasonsTypes::VALUE_OTHER,
                'style' => 'display:' . ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER ? 'block;' : 'none;'),
            ]
        ])->textInput(['style' => 'width: 100%;',]); ?>

        <?= $form->field($model, 'application')->textInput([
            'placeholder' => 'Необходимо для печатного документа',
            'style' => 'width: 100%;',
        ]); ?>

        <?= $form->field($model, 'is_accounting')->checkbox(); ?>

        <?= $form->field($model, 'recognitionDateInput', [
            'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
            'labelOptions' => [
                'class' => 'col-md-3 control-label width-label bold-text',
                'style' => 'width: 47.5%',
            ],
            'options' => [
                'class' => 'form-group',
                'style' => 'display: inline-block; width: 55%',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-6',
            ],
        ])->textInput([
            'class' => 'form-control date-picker',
            'style' => 'width: 100%; max-width: 125px;',
            'data' => [
                'date-viewmode' => 'years',
            ],
            'disabled' => (bool)$model->is_prepaid_expense,
        ]); ?>

        <?= $form->field($model, 'is_prepaid_expense', [
            'options' => [
                'class' => 'form-group',
                'style' => 'display: inline-block; width: 50%',
                'disabled' => (bool)$model->is_prepaid_expense,
            ],
        ])->checkbox(); ?>

        <div class="form-actions">
            <div class="row action-buttons" id="buttons-fixed">
                <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                    <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                        'data-style' => 'expand-right',
                        'style' => 'width: 130px!important;',
                    ]); ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                        'title' => 'Сохранить',
                    ]); ?>
                </div>
                <div class="spinner-button col-sm-2 col-xs-1 pull-right" style="width: 24%;">
                    <?= Html::button('Отменить', [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                        'data-dismiss' => 'modal',
                        'aria-hidden' => 'true',
                        'style' => 'width: 130px!important;',
                    ]) ?>
                    <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                        'data-dismiss' => 'modal',
                        'aria-hidden' => 'true',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">

    var currentModalContent = $("#ajax-modal-box:visible .modal-content");

    if (currentModalContent) {
        currentModalContent.find(".modal-header > h1").html("<?= $header ?>");

        currentModalContent.find(":checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
        currentModalContent.find(":radio:not(.md-radiobtn)").uniform();
        currentModalContent.find(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

        function dateChanged(ev) {
            if (ev.bubbles == undefined) {
                var $input = $("[name='" + ev.currentTarget.name +"']");
                if (ev.currentTarget.value == "") {
                    if ($input.data("last-value") == null) {
                        $input.data("last-value", ev.currentTarget.defaultValue);
                    }
                    var $lastDate = $input.data("last-value");
                    $input.datepicker("setDate", $lastDate);
                } else {
                    $input.data("last-value", ev.currentTarget.value);
                }
            }
        }
    }
    $(document).on("change", "#cashorderflows-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashorderflows-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });
</script>

<?php \yii\widgets\Pjax::end(); ?>
