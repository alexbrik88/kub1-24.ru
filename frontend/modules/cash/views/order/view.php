<?php
/**
 * @var \yii\web\View $this
 * @var CashOrderFlows $model
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use frontend\widgets\ConfirmModalWidget;
use php_rutils\RUtils;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = RUtils::dt()->ruStrFTime([
    'date' => $model->date,
    'format' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? 'Приходный' : 'Расходный') . " кассовый ордер №{$model->number} от j F Y г.",
    'monthInflected' => true,
    'preposition' => true,]);

?>
<?= \yii\helpers\Html::a('Назад к списку', [
    'index',
    'cashbox' => $model->cashbox_id,
], [
    'class' => 'back-to-customers',
]); ?>

<div class="row">
    <div class="<?= $model->invoice !== null ? 'col-md-8' : 'col-md-12' ?>">
        <div class="portlet customer-info customer-info-cash">
            <div class="portlet-title">
                <div class="col-md-10 caption">
                    <?= RUtils::dt()->ruStrFTime([
                        'date' => $model->date,
                        'format' => ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ? 'Приходный' : 'Расходный') . " кассовый ордер №{$model->number} от j F Y г.",
                        'monthInflected' => true,
                        'preposition' => true,
                    ]); ?>
                </div>
                <div class="actions">
                    <?= \frontend\modules\documents\widgets\CreatedByWidget::widget([
                        'createdAt' => date(DateHelper::FORMAT_USER_DATE, $model->created_at),
                        'author' => $model->author->fio,
                    ]); ?>

                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE)): ?>
                        <?= Html::a('<i class="icon-pencil"></i>', [
                            '/cash/order/update',
                            'id' => $model->id,
                        ], [
                            'class' => 'btn darkblue btn-sm ajax-modal-btn',
                            'title' => 'Редактировать',
                            'data' => [
                                'title' => 'Редактировать операцию по кассе',
                                'pjax' => '0',
                            ],
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="portlet-body">
                <table id="datatable_ajax" class="table">
                    <tr>
                        <td width="18%" class="bold-text">Сумма:</td>
                        <td><?= TextHelper::invoiceMoneyFormat($model->amount, 2, '.', ''); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%" class="customer-characteristic">Контрагент:</td>
                        <td><?= $model->contractorText; ?></td>
                    </tr>
                    <tr>
                        <td width="18%" class="customer-characteristic">Основание:</td>
                        <td><?php
                            echo $model->reason ? $model->reason->name : '';
                            if ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER) {
                                echo ': ' . $model->other;
                            }
                            ?></td>
                    </tr>
                    <tr>
                        <td width="18%" class="customer-characteristic">Приложение:</td>
                        <td><?= $model->application; ?></td>
                    </tr>
                    <?php if ($model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE): ?>
                        <tr>
                            <td width="18%" class="customer-characteristic">Статья расхода:</td>
                            <td><?= $model->flow_type == CashOrderFlows::FLOW_TYPE_EXPENSE ? $model->expenditureReason->name : '(не задано)'; ?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td width="18%" class="customer-characteristic">Статья прихода:</td>
                            <td><?= $model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME ?
                                    ($model->incomeReason ? $model->incomeReason->name : '(не задано)') : '(не задано)'; ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <td width="18%" class="customer-characteristic">Учитывать в бухгалтерии:</td>
                        <td><?= $model->is_accounting ? 'Да' : 'Нет'; ?></td>
                    </tr>
                    <?php if ($model->company->getCashboxes()->count() > 1) : ?>
                        <tr>
                            <td width="18%" class="customer-characteristic">Касса:</td>
                            <td><?= $model->cashbox ? $model->cashbox->name : '---'; ?></td>
                        </tr>
                    <?php endif ?>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-3 control-panel control-panel-pre-edit" style="margin-bottom: 20px;">
        <?php if ($model->invoice !== null): ?>
            <a href="<?= Url::to(['@documentsUrl/invoice/view', 'type' => $model->invoice->type, 'id' => $model->invoice->id,]); ?>"
               class="btn btn-account yellow pull-right">
                <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                СЧЕТ
            </a>
        <?php endif; ?>
    </div>
</div>

<div class="row action-buttons buttons-fixed" id="buttons-fixed">
    <div class="row">
        <div class="col-md-12">
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <button type="submit" class="btn btn-save darkblue hide widthe-100 hidden-md hidden-sm hidden-xs"
                        form="cash-order">Сохранить
                </button>
                <button type="submit" class="btn btn-save darkblue hide widthe-100 hidden-lg" title="Сохранить"
                        form="cash-order"><i class="fa fa-floppy-o fa-2x"></i></button>
            </div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1">

            </div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'filename' => $model->getPrintTitle(),];
                echo Html::a('Печать', $printUrl, [
                    'target' => '_blank',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]);
                echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
                    'target' => '_blank',
                    'title' => 'Печать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?php $pdfUrl = Url::to(['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),]);
                echo Html::a('PDF', $pdfUrl, [
                    'target' => '_blank',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ]);
                echo Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', $pdfUrl, [
                    'target' => '_blank',
                    'title' => 'PDF',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE)): ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Копировать',
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ],
                        'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                        'message' => 'Вы уверены, что хотите скопировать этот КО?',
                    ]);
                    ?>
                    <?= ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<i class="fa fa-files-o fa-2x"></i>',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                        ],
                        'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                        'message' => 'Вы уверены, что хотите скопировать этот КО?',
                    ]);
                    ?>
                <?php endif; ?>
            </div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <button type="button"
                        class="btn darkblue btn-cancel darkblue hide widthe-100 hidden-md hidden-sm hidden-xs"
                        form="edit-invoice-facture">Отменить
                </button>
                <button type="button" class="btn darkblue btn-cancel darkblue hide widthe-100 hidden-lg"
                        title="Отменить" form="edit-invoice-facture"><i class="fa fa-reply fa-2x"></i></button>
            </div>

            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Cash::DELETE)): ?>
                    <?= \frontend\widgets\ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Удалить',
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ],
                        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id,]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить кассовый ордер?',
                    ]); ?>
                    <?= \frontend\widgets\ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                            'title' => "Удалить",
                        ],
                        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id,]),
                        'confirmParams' => [],
                        'message' => 'Вы уверены, что хотите удалить кассовый ордер?',
                    ]); ?>
                <?php endif; ?>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).on("shown.bs.modal", "#ajax-modal-box", function() {
        $("#ajax-modal-box :checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
        $("#ajax-modal-box :radio:not(.md-radiobtn)").uniform();
        $("#ajax-modal-box .date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

        function dateChanged(ev) {
            if (ev.bubbles == undefined) {
                var $input = $("[name='" + ev.currentTarget.name +"']");
                if (ev.currentTarget.value == "") {
                    if ($input.data("last-value") == null) {
                        $input.data("last-value", ev.currentTarget.defaultValue);
                    }
                    var $lastDate = $input.data("last-value");
                    $input.datepicker("setDate", $lastDate);
                } else {
                    $input.data("last-value", ev.currentTarget.value);
                }
            }
        }
    });
</script>