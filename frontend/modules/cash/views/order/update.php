<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashBankFlows */

$this->title = 'Изменение кассового ордера';
$this->params['breadcrumbs'][] = ['label' => 'Касса', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-order-flows-update">

    <?php if (!Yii::$app->request->isAjax):?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif;?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
