<?php
/* @var \yii\web\View $this */
/* @var \common\models\cash\CashOrderFlows $model */

use common\components\date\DateHelper;
use common\models\cash\CashOrdersReasonsTypes;
use php_rutils\RUtils;
use yii\helpers\Html;

$this->title = $model->printTitle;

$sum = number_format($model->amount/100, 2, '-', '');
$sumArray = explode('-', $sum);
$rubString = RUtils::numeral()->getInWordsInt($sumArray[0]);
$nds = $model->company->hasNds() ? number_format(round($model->amount/120*20)/100, 2, ',', '') : '0,00';
?>
<div class="page-content-in p-center pad-pdf-p pad-top-paym-order">

    <div class="width-b float-l" style="padding-right: 3px;position: relative;">
        <div class="font-size-6 line-h-small float-r med-p">
            <p>Унифицированная форма № КО-1</p>
            <p>Утверждена постановлением Госкомстата</p>
            <P>России от 18.08.98 № 88</P>
        </div>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td colspan="2"></td>
                <td width="15%" class="font-size-7 text-center" style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">Код</td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6 text-right">Форма по ОКУД</td>
                <td class="font-size-6-bold text-center" style="border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">0310002</td>
            </tr>
            <tr>
                <td width="70%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getTitle(true)) ?>
                </td>
                <td width="15%" class="font-size-6 text-right ver-bottom" style="padding: 1px 2px; border-bottom: 1px solid #ffffff">по ОКПО</td>
                <td class="font-size-6-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"></td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-5 text-center ver-top m-l" style="padding: 1px 0">(организация)</td>

                <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid black;padding: 1px 0" class="font-size-6">
                    <br/>
                </td>
                <td class="font-size-6-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 2px solid black;padding: 1px 2px"></td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-5 text-center ver-top m-l" style="padding: 1px 2px">(структурное подразделение)</td>
                <td></td>
            </tr>
        </table>

        <table class="table no-border" style="">
            <tr>
                <td width="70%"></td>
                <td width="15%" class="font-size-6 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-left: 1px solid black;border-top: 1px solid black;">Номер документа</td>
                <td width="15%" class="font-size-6 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-top: 1px solid black;">Дата составления</td>
            </tr>
            <tr>
                <td class="font-size-9-bold text-center">ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР</td>
                <td class="font-size-7-bold text-center" style="padding: 1px 2px;border: 2px solid black;">
                    <?= Html::encode($model->number)?>
                </td>
                <td class="font-size-7-bold text-center" style="padding: 1px 2px;border-top: 2px solid black;border-right: 2px solid black;border-bottom: 2px solid black;">
                    <?= DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </td>
            </tr>
        </table>

        <table class="table" style="margin-bottom: 5px !important;">
            <tr>
                <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top">Дебет</td>
                <td colspan="4" class="font-size-6 text-center">Кредит</td>
                <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top">Сумма <br>руб. коп.</td>
                <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top">Код целевого назначения</td>
                <td rowspan="2" class="font-size-6 text-center"></td>
            </tr>
            <tr>
                <td class="font-size-6 text-center"></td>
                <td class="font-size-6 text-center m-l">код струк-<br>турного подразде-<br>ления</td>
                <td class="font-size-6 text-center m-l">корреспон-<br>дирующий счет, субсчет</td>
                <td class="font-size-6 text-center m-l">код аналити-<br>ческого учета</td>
            </tr>
            <tr>
                <td width="10%" class="font-size-6 text-center"></td>
                <td width="4%" class="font-size-6 text-center"></td>
                <td width="14%" class="font-size-6 text-center"></td>
                <td width="18%" class="font-size-6 text-center"></td>
                <td width="18%" class="font-size-6 text-center"><br></td>
                <td width="14%" class="font-size-6 text-center">
                    <?= $sum; ?>
                </td>
                <td width="18%" class="font-size-6 text-center"></td>
                <td width="4%" class="font-size-6 text-center"></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 3px !important;">
            <tr>
                <td width="14%" class="font-size-6" style="padding: 2px 4px 2px 0;">Принято от</td>
                <td width="86%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->contractor ? $model->contractor->getShortName() : '') ?>
                </td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="10%" class="font-size-6" style="padding: 2px 4px 2px 0;">Основание:</td>
                <td width="90%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?php if ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER) : ?>
                        <?= Html::encode(Html::encode($model->other)) ?>
                    <?php else : ?>
                        <?= Html::encode($model->reason ? $model->reason->name : ''); ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid black;"><br></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 5px !important;">
            <tr>
                <td width="10%" class="font-size-6" style="padding: 2px 4px 2px 0;">Сумма</td>
                <td colspan="4" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= $rubString ?>
                </td>
            </tr>
            <tr>
                <td class="font-size-6"></td>
                <td colspan="4" class="font-size-6 text-center ver-top m-l">(прописью)</td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"></td>
                <td width="8%" class="font-size-6 text-center">руб.</td>
                <td width="17%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= $sumArray[1] ?>
                </td>
                <td width="8%" class="font-size-6 text-center"> коп.</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 3px !important;">
            <tr>
                <td width="15%" class="font-size-6" style="padding: 2px 4px 2px 0;">В том числе</td>
                <td width="85%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= ($model->company->hasNds() ? 'НДС ' : 'Без налога (НДС) ') . $nds . ' руб.' ?>
                </td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 7px !important;">
            <tr>
                <td width="15%" class="font-size-6" style="padding: 2px 4px 2px 0;">Приложение</td>
                <td width="85%" class="font-size-6" style="border-bottom: 2px solid black;"></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 2px !important;">
            <tr>
                <td width="25%" class="font-size-6" style="padding: 2px 4px 2px 0;">Главный бухгалтер</td>
                <td width="25%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
                <td width="5%"></td>
                <td width="30%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getChiefFio(true)) ?>
                </td>
                <td width="8%"></td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
                <td></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 2px !important;">
            <tr>
                <td width="25%" class="font-size-6" style="padding: 2px 4px 2px 0;">Получил кассир</td>
                <td width="25%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
                <td width="5%"></td>
                <td width="30%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getChiefFio(true)) ?>
                </td>
                <td width="8%"></td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
                <td></td>
            </tr>
        </table>
    </div>

    <div class="float-l" style="width: 6%">
        <img src="/img/pdf-line.png">
    </div>

    <div class="width-s float-r" style="margin-top: 2%;">
        <table class="table no-border" style="margin-bottom: 13px !important;">
            <tr>
                <td width="100%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getTitle(true)) ?>
                </td>
            </tr>
            <tr>
                <td class="font-size-5 text-center ver-top m-l">(организация) </td>
            </tr>
        </table>

        <div class="font-size-10-bold text-center" style="margin-bottom: 23px;">
            КВИТАНЦИЯ
        </div>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="77%" class="font-size-6" style="padding: 2px 4px 2px 0;">к приходному кассовому ордеру №</td>
                <td width="23%" style="border-bottom: 1px solid black;"><?= Html::encode($model->number)?></td>
            </tr>
        </table>

        <table class="table no-border"  style="margin-bottom: 7px !important;">
            <tr>
                <td width="9%" class="font-size-6" style="padding: 2px 4px 2px 0;">от</td>
                <td width="5%" class="font-size-6">"</td>
                <td width="10%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= date('d', strtotime($model->date)) ?>
                </td>
                <td width="8%" class="font-size-6">"</td>
                <td width="32%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= \Yii::$app->formatter->asDate($model->date, 'MMMM'); ?>
                </td>
                <td width="2%" class="font-size-6"></td>
                <td width="23%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= date('Y', strtotime($model->date)) ?>
                </td>
                <td width="7%" class="font-size-6">г.</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 5px !important;">
            <tr>
                <td width="20%" class="font-size-6" style="padding: 2px 4px 2px 0;">Принято от</td>
                <td width="80%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->contractor ? $model->contractor->getShortName() : '') ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6"  style="border-bottom: 1px solid black;"><br></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 10px !important;">
            <tr>
                <td width="20%" class="font-size-6" style="padding: 2px 4px 2px 0;">Основание:</td>
                <td width="80%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?php if ($model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER) : ?>
                        <?= Html::encode($model->other) ?>
                    <?php else : ?>
                        <?= Html::encode($model->reason ? $model->reason->name : ''); ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"><br></td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"><br></td>
            </tr>
            <tr>
                <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"><br></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="17%" class="font-size-6" style="padding: 2px 4px 2px 0;">Сумма</td>
                <td width="38%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= $sumArray[0] ?>
                </td>
                <td width="14%" class="font-size-6 text-center">руб.</td>
                <td width="14%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= $sumArray[1] ?>
                </td>
                <td width="17%" class="font-size-6">коп.</td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(цифрами)</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="100%" class="font-size-6" style="border-bottom: 1px solid black;padding-top: 0">
                    <?= $rubString ?>
                </td>
            </tr>
            <tr>
                <td class="font-size-5 text-center ver-top m-l">(прописью)</td>
            </tr>
            <tr>
                <td width="100%" class="font-size-6" style="border-bottom: 1px solid black;padding-top: 0"><br></td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="54%" class="font-size-6" style="border-bottom: 1px solid black;"></td>
                <td width="15%" class="font-size-6 text-center">руб.</td>
                <td width="15%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= $sumArray[1] ?>
                </td>
                <td class="font-size-8">коп.</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 4px !important;">
            <tr>
                <td width="21%" class="font-size-6" style="padding: 2px 4px 2px 0;">В том числе</td>
                <td width="79%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= ($model->company->hasNds() ? 'НДС ' : 'Без налога (НДС) ') . $nds . ' руб.' ?>
                </td>
            </tr>
        </table>

        <table class="table no-border"  style="margin-bottom: 25px !important;">
            <tr>
                <td width="5%" class="font-size-6">"</td>
                <td width="10%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= date('d', strtotime($model->date)) ?>
                </td>
                <td width="8%" class="font-size-6">"</td>
                <td width="32%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= \Yii::$app->formatter->asDate($model->date, 'MMMM'); ?>
                </td>
                <td width="2%" class="font-size-6"></td>
                <td width="23%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= date('Y', strtotime($model->date)) ?>
                </td>
                <td width="7%" class="font-size-6">г.</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 10px !important;">
            <tr>
                <td width="100%" class="font-size-6" style="padding-left: 20px">М.П. (штампа)</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 2px !important;">
            <tr>
                <td width="36%" class="font-size-6" style="padding: 2px 4px 2px 0;">Главный бухгалтер</td>
                <td width="19%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
                <td width="1%"></td>
                <td width="40%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getChiefFio(true)) ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
            </tr>
        </table>

        <table class="table no-border" style="margin-bottom: 2px !important;">
            <tr>
                <td width="17%" class="font-size-6" style="padding: 2px 4px 2px 0;">Кассир</td>
                <td width="19%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
                <td width="5%"></td>
                <td width="46%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                    <?= Html::encode($model->company->getChiefFio(true)) ?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
                <td></td>
                <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
                <td></td>
            </tr>
        </table>
    </div>

</div>