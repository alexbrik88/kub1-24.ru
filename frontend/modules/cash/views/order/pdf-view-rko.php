<?php

use common\components\date\DateHelper;
use common\models\Contractor;

/* @var \yii\web\View $this */
/* @var \common\models\cash\CashOrderFlows $model */

$this->title = $model->printTitle;
$sum = number_format($model->amount/100, 2, '-', '');
$sumArray = explode('-', $sum);
?>
<div class="page-content-in p-center pad-pdf-p pad-top-paym-order">
    <div class="font-size-6 line-h-small float-r med">
        <p>Унифицированная форма № КО-2</p>
        <p>Утверждена постановлением Госкомстата России от 18.08.98 № 88</p>
    </div>

    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td colspan="2"></td>
            <td width="15%" class="font-size-7 text-center" style="border-right: 1px solid black;border-top: 1px solid black;border-left: 1px solid black;padding: 1px 2px;">Код</td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-6 text-right">Форма по ОКУД</td>
            <td class="font-size-6-bold text-center" style="border-left: 2px solid black; border-right: 2px solid black ;border-top: 2px solid black; border-bottom: 1px solid black;padding: 1px 2px;">0310002</td>
        </tr>
        <tr>
            <td width="77%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"><?= $model->company->getTitle(true); ?></td>
            <td width="8%" class="font-size-6 text-right ver-bottom" style="padding: 1px 2px; border-bottom: 1px solid #ffffff">по ОКПО</td>
            <td class="font-size-6-bold text-center" style="padding: 1px 2px;border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;"><?= $model->company->okpo; ?></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-5 text-center ver-top m-l" style="padding: 1px 0">(организация)</td>

            <td style="border-right: 2px solid black;border-left: 2px solid black;padding: 1px 2px"></td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black;padding: 0" class="font-size-5 text-center">-
                <br/>
            </td>
            <td style="border-bottom: 1px solid #ffffff;padding: 0"></td>
            <td class="font-size-6-bold text-center" style="border-right: 2px solid black;border-left: 2px solid black;border-bottom: 1px solid black;padding: 0 2px"></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-5 text-center ver-top m-l" style="padding: 1px 2px">(структурное подразделение)</td>
            <td></td>
        </tr>
    </table>

    <table class="table no-border" style="">
        <tr>
            <td width="70%"></td>
            <td width="15%" class="font-size-6 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-left: 1px solid black;border-top: 1px solid black;">Номер документа</td>
            <td width="15%" class="font-size-6 text-center" style="padding: 1px 2px;border-right: 1px solid black;border-top: 1px solid black;">Дата составления</td>
        </tr>
        <tr>
            <td style="padding-left: 14%;" class="font-size-9-bold text-center">РАСХОДНЫЙ КАССОВЫЙ ОРДЕР</td>
            <td class="font-size-6-bold text-center"style="padding: 1px 2px;border: 2px solid black;"><?= $model->number; ?></td>
            <td class="font-size-6-bold text-center" style="padding: 1px 2px;border-top: 2px solid black;border-right: 2px solid black;border-bottom: 2px solid black;">
                <?= DateHelper::format($model->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
            </td>
        </tr>
    </table>

    <table class="table" style="margin-bottom: 5px !important;">
        <tr>
            <td colspan="4" class="font-size-6 text-center">Дебет</td>
            <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top;">Кредит</td>
            <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top;">Сумма, руб. коп.</td>
            <td rowspan="2" class="font-size-6 text-center" style="vertical-align: top;">Код целевого назначения</td>
            <td rowspan="2" class="font-size-6 text-center"></td>
        </tr>
        <tr>
            <td class="font-size-6 text-center"></td>
            <td class="font-size-6 text-center">код структурного подразделения</td>
            <td class="font-size-6 text-center">корреспондирующий счет, субсчет</td>
            <td class="font-size-6 text-center">код аналитического учета</td>
        </tr>
        <tr>
            <td width="6%" class="font-size-6 text-center"></td>
            <td width="15%" class="font-size-6 text-center"></td>
            <td width="21%" class="font-size-6 text-center"></td>
            <td width="20%" class="font-size-6 text-center"></td>
            <td width="7%" class="font-size-6 text-center"><br></td>
            <td width="12%" class="font-size-6 text-center"><?= number_format($model->amount/100, 2, '.', ' ') ?></td>
            <td width="12%" class="font-size-6 text-center"></td>
            <td width="7%" class="font-size-6 text-center"></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;">
            <tr>
                <td width="5%" class="font-size-6" style="padding: 2px 4px 2px 0;">Выдать</td>
                <td width="95%" class="font-size-6" style="border-bottom: 1px solid black;">
                    <?= $model->contractor && $model->contractor->is_seller ? $model->contractor->getShortName() : ''; ?>
                </td>
            </tr>
        <tr>
            <td style="padding: 0;"></td>
            <td class="font-size-5 text-center ver-top m-l">(фамилия, имя, отчество)</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 6px !important;">
        <tr>
            <td width="6%" class="font-size-6" style="padding: 2px 4px 2px 0;">Основание:</td>
            <td width="94%" class="font-size-6" style="border-bottom: 1px solid black;">
                <?= $model->reason ? $model->reason->name : ''; ?>
            </td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 5px !important;">
        <tr>
            <td width="5%" class="font-size-6" style="padding: 2px 4px 2px 0;">Сумма</td>
            <td colspan="4" class="font-size-6" style="border-bottom: 1px solid black;"><?= $model->amountRubString; ?></td>
        </tr>
        <tr>
            <td class="font-size-6"></td>
            <td colspan="4" class="font-size-5 text-center ver-top m-l">(прописью)</td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"></td>
            <td width="5%" class="font-size-6 text-center">руб.</td>
            <td width="17%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"><?= $sumArray[1] ?></td>
            <td width="5%" class="font-size-6 text-center"> коп.</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 3px !important;">
        <tr>
            <td width="8%" class="font-size-5" style="padding: 2px 4px 2px 0;">Приложение</td>
            <td width="92%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"><?= $model->application ?></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-5" style="border-bottom: 1px solid black;"><br></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 0 !important;">
        <tr>
            <td width="18%" class="font-size-6" style="padding: 2px 4px 2px 0;">Руководитель организации</td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                <?= $model->company->chief_post_name ?>
            </td>
            <td width="3%"></td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
            <td width="3%"></td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                <?= $model->company->getChiefFio(true) ?>
            </td>
            <td width="9%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(должность)</td>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
            <td></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 2px !important;">
        <tr>
            <td width="15%" class="font-size-6" style="padding: 2px 4px 2px 0;">Главный бухгалтер</td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
            <td width="3%"></td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                <?= $model->company->getChiefAccountantFio(true) ?>
            </td>
            <td width="39%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
            <td></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 4px !important;">
        <tr>
            <td width="6%" class="font-size-6" style="padding: 2px 4px 2px 0;">Получил</td>
            <td colspan="4" class="font-size-6 text-center" style="border-bottom: 1px solid black;"><?= $model->amountRubString ?></td>
        </tr>
        <tr>
            <td class="font-size-7"></td>
            <td colspan="4" class="font-size-5 text-center ver-top m-l">(сумма прописью)</td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-7" style="border-bottom: 1px solid black;"></td>
            <td width="5%" class="font-size-7 text-center">руб.</td>
            <td width="17%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"><?= $sumArray[1] ?></td>
            <td width="5%" class="font-size-7 text-center"> коп.</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 3px !important;">
        <tr>
            <td width="2%" class="text-right font-size-6">"</td>
            <td width="5%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"> <?=
                \php_rutils\RUtils::dt()->ruStrFTime([
                    'format' => 'd',
                    'monthInflected' => true,
                    'date' => $model->date,
                ]); ?></td>
            <td width="3%" class="text-left font-size-6">"</td>
            <td width="18%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"> <?=
                \php_rutils\RUtils::dt()->ruStrFTime([
                    'format' => 'F',
                    'monthInflected' => true,
                    'date' => $model->date,
                ]); ?></td>
            <td width="1%"></td>
            <td width="7%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"> <?=
                \php_rutils\RUtils::dt()->ruStrFTime([
                    'format' => 'Y',
                    'monthInflected' => true,
                    'date' => $model->date,
                ]); ?></td>
            <td width="3%" class="font-size-6 text-left">г.</td>
            <td width="22%" class="text-right font-size-6">Подпись</td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
            <td width="19%"></td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 2px !important;">
        <tr>
            <td width="2%" class="font-size-6" style="padding: 2px 4px 2px 0;">По</td>
            <td width="98%" class="font-size-6" style="border-bottom: 1px solid black;">
                <?= $model->contractor ? $model->contractor->getPassportText() : ''; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(наименование, номер, дата и место выдачи документа,</td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-6" style="border-bottom: 1px solid black;"><br></td>
        </tr>
        <tr>
            <td colspan="2" class="font-size-5 text-center ver-top m-l">удостоверяющего личность получателя)</td>
        </tr>
    </table>

    <table class="table no-border" style="margin-bottom: 2px !important;">
        <tr>
            <td width="12%" class="font-size-6" style="padding: 2px 4px 2px 0;">Выдал кассир</td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;"></td>
            <td width="3%"></td>
            <td width="20%" class="font-size-6 text-center" style="border-bottom: 1px solid black;">
                <?= $model->company->getChiefFio(true) ?>
            </td>
            <td width="43%"></td>
        </tr>
        <tr>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(подпись)</td>
            <td></td>
            <td class="font-size-5 text-center ver-top m-l">(расшифровка подписи)</td>
            <td></td>
        </tr>
    </table>

</div>
