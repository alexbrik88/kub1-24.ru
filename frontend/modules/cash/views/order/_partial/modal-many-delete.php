<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.06.2018
 * Time: 5:41
 */

use common\components\helpers\Html;
use yii\helpers\Url;

?>
<div id="many-delete" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true" style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">Вы уверены, что хотите удалить выбранные движения по кассе?</div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <?= Html::a('ДА', null, [
                            'class' => 'btn darkblue pull-right modal-many-delete',
                            'data-url' => Url::to(['many-delete']),
                        ]); ?>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
