<?php
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $model CashEmoneyFlows
 * @var $emoney string
 */

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\widgets\Modal;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\widgets\StatisticWidget;
use frontend\modules\cash\widgets\SummarySelectWidget;
use frontend\rbac\permissions;
use frontend\widgets\EmoneyFilterWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\web\JsExpression;

$this->title = 'E-money';
$this->params['breadcrumbs'][] = $this->title;

$canCreate = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
$canUpdate = Yii::$app->user->can(frontend\rbac\permissions\Cash::UPDATE);
$canDelete = Yii::$app->getUser()->can(permissions\Cash::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

/** @var \common\models\cash\Emoney $monetaRs */
$monetaRs = $company->getEmoneys()->andWhere(['LIKE', 'name', 'Монета'])->one();

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'zIndex' => 10000,
    ],
]);

$model->contractor_ids = (!empty($model->contractor_ids)) ? reset($model->contractor_ids) : null;
$model->reason_ids = (!empty($model->reason_ids)) ? reset($model->reason_ids) : null;
?>
    <div class="cash-emoney-flows-index">
        <div class="portlet box">
            <div class="btn-group pull-right title-buttons">
                <?php if ($canCreate): ?>
                    <?php if (Yii::$app->user->identity->company->strict_mode == Company::ON_STRICT_MODE): ?>
                        <a href="<?= Url::to(['create']); ?>"
                           class="btn yellow">
                            <i class="fa fa-plus"></i>
                            ДОБАВИТь
                        </a>
                    <?php else: ?>
                        <button class="btn yellow" data-toggle="modal"
                                href="#add-movement">
                            <i class="fa fa-plus"></i> ДОБАВИТЬ
                        </button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="pull-left">
                <h3 class="page-title">
                    <?= EmoneyFilterWidget::widget([
                        'pageTitle' => $this->title,
                        'emoney' => $emoney,
                        'company' => $company,
                    ]); ?>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-md-offset-<?= ($showMonetaIntegration ?? false) ? 6 : 9 ?>">
                        <?= frontend\widgets\RangeButtonWidget::widget([
                            'cssClass' => 'btn_select_days btn_no_right cash-btn-select-days',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <?= StatisticWidget::widget([
                'model' => $model
            ]); ?>
        </div>

        <div class="portlet box darkblue">

            <div class="portlet-title">
                <div class="caption capt_non">
                    Движения по e-money
                </div>
                <div class="tools" style="padding-top: 7px!important;">
                    <div class="">
                        <div class="form-body">
                            <?php $form = ActiveForm::begin([
                                'action' => ['index'],
                                'enableClientValidation' => false,
                                'method' => 'GET',
                            ]); ?>
                            <div>
                                <div
                                        class="radio-list pull-left radio_form form-group-m-b-0 btn_pad_down">
                                    <?= $form->field($model, 'is_accounting', [
                                        'labelOptions' => ['class' => 'col-md-4 control-label label-width'],
                                    ])->label(false)->radioList([
                                        -1 => 'Все',
                                        CashFlowsBase::ACCOUNTING => 'Для учёта в бухгалтерии',
                                        CashFlowsBase::NON_ACCOUNTING => 'Не для учёта в бухгалтерии',
                                    ], [
                                        'item' => function ($index, $label, $name, $checked, $value) {
                                            return Html::radio($name, $checked, [
                                                'value' => $value,
                                                'label' => $label,
                                                'labelOptions' => [
                                                    'class' => 'radio-inline',
                                                ],
                                            ]);
                                        },
                                    ]); ?>
                                </div>
                                <div class="form-actions pull-left">
                                    <?= Html::submitButton('ПРИМЕНИТЬ', ['class' => 'btn btn-sm default green-haze']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
                <div class="actions joint-operations col-md-3 col-sm-3"
                     style="display:none; width: 220px;padding-right: 5px!important;">
                    <?php if ($canDelete) : ?>
                        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                            'class' => 'btn btn-default btn-sm',
                            'data-toggle' => 'modal',
                            'style' => 'margin-right: 0;',
                        ]); ?>

                        <?= $this->render('_partial/modal-many-delete'); ?>
                    <?php endif ?>
                    <?php if ($canUpdate) : ?>
                        <?= Html::a('<i class="fa fa-list" style="padding-right: 3px;"></i>Статья', '#many-item', [
                            'class' => 'btn btn-default btn-sm',
                            'data-toggle' => 'modal',
                        ]); ?>

                        <?= $this->render('_partial/modal-many-item', [
                            'model' => $model,
                            'foreign' => $foreign,
                        ]); ?>
                    <?php endif ?>
                </div>
            </div>
            <div class="portlet-body accounts-list">
                <div class="table-container" style="">
                    <?= common\components\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $model,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable documents_table fix-thead',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading line-height-1em',
                        ],
                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right',
                            ],
                        ],
                        'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                        'columns' => [
                            [
                                'header' => Html::checkbox('', false, [
                                    'class' => 'joint-operation-main-checkbox',
                                ]),
                                'headerOptions' => [
                                    'class' => 'text-center',
                                    'width' => '5%',
                                ],
                                'contentOptions' => [
                                    'class' => 'text-center',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                                        $typeCss = 'income-item';
                                        $income = round($data['amount'] / 100, 2);
                                        $expense = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $income : 0;
                                    } else {
                                        $typeCss = 'expense-item';
                                        $expense = round($data['amount'] / 100, 2);
                                        $income = 0; //($data['is_internal_transfer'] && $data['wallet_id'] != 'plan') ? $expense : 0;
                                    }

                                    return Html::checkbox("flowId[{$data['tb']}][]", false, [
                                        'class' => 'joint-operation-checkbox ' . $typeCss,
                                        'value' => $data['id'],
                                        'data' => [
                                            'income' => $income,
                                            'expense' => $expense,
                                        ],
                                    ]);
                                },
                            ],
                            [
                                'attribute' => 'date',
                                'label' => 'Дата',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                ],
                                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'amountIncome',
                                'label' => 'Приход',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '9%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME)
                                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                                        : '-';
                                },
                            ],
                            [
                                'attribute' => 'amountExpense',
                                'label' => 'Расход',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '9%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    $formattedAmount = TextHelper::invoiceMoneyFormat($data['amount'], 2);
                                    return ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_EXPENSE)
                                        ? Html::tag('span', $formattedAmount, ['class' => $data['wallet_id'] == 'plan' ? 'plan':''])
                                        : '-';
                                },
                            ],
                            [
                                'class' => DropDownSearchDataColumn::className(),
                                'attribute' => 'contractor_ids',
                                'label' => 'Контрагент',
                                'headerOptions' => [
                                    'width' => '30%',
                                    'class' => 'nowrap-normal',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    if ($data['is_internal_transfer'])
                                        return '';

                                    if ($data['contractor_id'] > 0 && ($contractor = Contractor::findOne($data['contractor_id'])) !== null) {
                                        return Html::a(Html::encode($contractor->nameWithType), [
                                            '/contractor/view',
                                            'type' => $contractor->type,
                                            'id' => $contractor->id,
                                        ], ['target' => '_blank', 'title' => $contractor->nameWithType]);
                                    } else {
                                        $model = CashEmoneyFlows::findOne($data['id']);
                                        if ($model && $model->cashContractor)
                                            return Html::tag('span', Html::encode($model->cashContractor->text),
                                                ['title' => $model->cashContractor->text]);
                                    }

                                    return '---';
                                },
                                'filter' => ['' => 'Все контрагенты'] + $model->getContractorFilterItems()
                            ],
                            [
                                'attribute' => 'description',
                                'label' => 'Назначение',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '12%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) use ($foreign) {

                                    if ($data['description']) {
                                        $description = mb_substr($data['description'], 0, 50) . '<br>' . mb_substr($data['description'], 50, 50);

                                        return Html::label(strlen($data['description']) > 100 ? $description . '...' : $description, null, ['title' => $data['description']]);
                                    }

                                    return '';
                                },
                            ],
                            [
                                'attribute' => 'billPaying',
                                'label' => 'Опл. счета',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '10%',
                                    'style' => 'max-width:50px'
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {

                                    switch ($data['tb']) {
                                        case CashOrderFlows::tableName():
                                            $model = CashOrderFlows::findOne($data['id']);
                                            return '<div style="max-width:50px">' . $model->billPaying . '</div>';
                                        case PlanCashFlows::tableName():
                                            $model = PlanCashFlows::findOne($data['id']);
                                            $currDate = date('Y-m-d');
                                            if ($model->first_date < $currDate && $model->date != $model->first_date)
                                                return 'Перенос';
                                            elseif ($model->date >= $currDate)
                                                return 'План';
                                            elseif ($model->date < $currDate)
                                                return 'Просрочен';
                                    }

                                    return '';
                                }
                            ],
                            [
                                'class' => DropDownSearchDataColumn::className(),
                                'attribute' => 'reason_ids',
                                'label' => 'Статья',
                                'headerOptions' => [
                                    'width' => '10%',
                                ],
                                'filter' => array_merge(['' => 'Все статьи', 'empty' => '-'], $model->reasonFilterItems),
                                'format' => 'raw',
                                'value' => function ($data) {

                                    $reason = $data['flow_type'] == CashEmoneyFlows::FLOW_TYPE_INCOME ?
                                        (($item = \common\models\document\InvoiceIncomeItem::findOne($data['income_item_id'])) ?
                                            $item->fullName : "id={$data['income_item_id']}") :
                                        (($item = \common\models\document\InvoiceExpenditureItem::findOne($data['expenditure_item_id'])) ?
                                            $item->fullName : "id={$data['expenditure_item_id']}");

                                    return $reason ? Html::tag('span', $reason, ['title' => htmlspecialchars($reason)]) : '-';

                                },
                            ],
                            [
                                'class' => ActionColumn::className(),
                                'template' => '{update}<br>{delete}',
                                'headerOptions' => [
                                    'width' => '2%'
                                ],
                                'visible' => Yii::$app->user->can(\frontend\rbac\permissions\Cash::DELETE),
                                'urlCreator' => function ($action, $data, $key, $index, $actionColumn) use ($foreign) {
                                    if ($action == 'update' && $data['is_internal_transfer']) {
                                        $action = 'update-internal';
                                    }
                                    $params = [
                                        'id' => $data['id'],
                                        'foreign' => $foreign,
                                        'is_plan_flow' => ($data['wallet_id'] == 'plan') ? '1' : ''
                                    ];
                                    $params[0] = $actionColumn->controller ? $actionColumn->controller . '/' . $action : $action;

                                    return Url::toRoute($params);
                                },
                                'buttons' => [
                                    'update' => function ($url, $model, $key) {
                                        $options = [
                                            'title' => 'Изменить',
                                            'data' => [
                                                'toggle' => 'modal',
                                                'target' => '#update-movement',
                                            ],
                                        ];

                                        return Html::a("<span aria-hidden='true' class='icon-pencil'></span>", $url, $options);
                                    },
                                    'delete' => function ($url) {
                                        return \frontend\widgets\ConfirmModalWidget::widget([
                                            'toggleButton' => [
                                                'label' => '<span aria-hidden="true" class="icon-close"></span>',
                                                'class' => '',
                                                'tag' => 'a',
                                            ],
                                            'confirmUrl' => $url,
                                            'confirmParams' => [],
                                            'message' => 'Вы уверены, что хотите удалить операцию?',
                                        ]);
                                    }
                                ]
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-movement" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h1>Добавить движение по e-money</h1>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-movement" tabindex="-1" role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true"></button>
                    <h1>Редактировать движение по e-money</h1>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>
    <div class="tooltip_templates container-tooltip_templates">
        <div id="tooltip_import-dialog-process" class="box-tooltip-templates">
            Операции загружаются. Это может занять несколько минут. После загрузки мы вас уведомим.
        </div>
    </div>

<?php Modal::begin([
    'id' => 'import-dialog-widget',
    'header' => '<h1>Запрос выписки из E-money</h1>',
]);
Pjax::begin([
    'id' => 'import-dialog-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]);
Pjax::end();
Modal::end();
?>

<?= SummarySelectWidget::widget([
    'buttons' => [
        $canUpdate ? Html::a('<i class="fa fa-list" style="padding-right: 3px;"></i>Статья', '#many-item', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
    ],
]); ?>

<?php $this->registerJs('
$(document).on("show.bs.modal", "#update-movement", function(event) {
    $(".alert-success").remove();
    if (event.target.id === "update-movement") {
        $(this).find(".modal-body").empty();
        $(this).find("#js-modal_update_title").empty();
    }
});

$(document).on("hide.bs.modal", "#update-movement", function(event) {
    if (event.target.id === "update-movement") {
        $("#update-movement .modal-body").empty();
    }
});
$(document).on("show.bs.modal", "#add-movement", function(e) {
    $(".alert-success").remove();
    var button = $(e.relatedTarget);
    var form = $("#cash-emoney-form");
    var modal = $(this);
    if (!form.length)
        modal.find(".modal-body").load("/cash/e-money/create/?type=1&showForm=1&id=' . (int)$emoney . '");
});
$(document).on("shown.bs.modal", "#add-movement", function() {
    $("#add-movement :checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
    $("#add-movement :radio:not(.md-radiobtn)").uniform();
    $("#add-movement .date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            if (ev.currentTarget.value == "") {
                if ($input.data("last-value") == null) {
                    $input.data("last-value", ev.currentTarget.defaultValue);
                }
                var $lastDate = $input.data("last-value");
                $input.datepicker("setDate", $lastDate);
            } else {
                $input.data("last-value", ev.currentTarget.value);
            }
        }
    }
});
$(document).on("hide.bs.modal", "#add-movement", function(event) {
    if (event.target.id === "add-movement") {
        $("#add-movement .modal-body").empty();
    }
});
$(document).on("shown.bs.modal", "#many-item", function () {
    var $includeExpenditureItem = $(".joint-operation-checkbox.expense-item:checked").length > 0;
    var $includeIncomeItem = $(".joint-operation-checkbox.income-item:checked").length > 0;
    var $modal = $(this);

    if ($includeExpenditureItem) {
        $(".expenditure-item-block").removeClass("hidden");
    }
    if ($includeIncomeItem) {
        $(".income-item-block").removeClass("hidden");
    }
    $(".joint-operation-checkbox:checked").each(function() {
        $modal.find("form#js-cash_flow_update_item_form").prepend($(this).clone().hide());
    });
});
$(document).on("hidden.bs.modal", "#many-item", function () {
    $(".expenditure-item-block").addClass("hidden");
    $(".income-item-block").addClass("hidden");
});
$(document).on("submit", "form#js-cash_flow_update_item_form", function (e) {
    var l = Ladda.create($(this).find(".btn-save")[0]);
    var $hasError = false;

    l.start();
    $(".field-cashemoneysearch-incomeitemidmanyitem:visible, .field-cashemoneysearch-expenditureitemidmanyitem:visible").each(function () {
        $(this).removeClass("has-error");
        $(this).find(".help-block").text("");
        if ($(this).find("select").val() == "") {
            $hasError = true;
            $(this).addClass("has-error");
            $(this).find(".help-block").text("Необходимо заполнить.");
        }
    });
    if ($hasError) {
        return false;
    }
});
');

if ($hasNotFinishedJob ?? false) {
    $this->registerJs('
        Ladda.create($(".import-dialog-process")[0]).start();
    ');
}