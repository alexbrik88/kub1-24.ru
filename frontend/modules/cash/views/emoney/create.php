<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\cash\CashEmoneyFlows */

$this->title = 'Create Cash Emoney Flows';
$this->params['breadcrumbs'][] = ['label' => 'Cash Emoney Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-emoney-flows-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
