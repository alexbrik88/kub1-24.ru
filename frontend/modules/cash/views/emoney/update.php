<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashOrderFlows */
?>
<div class="cash-emoney-flows-update">
    <?php if (!Yii::$app->request->isAjax): ?>
        <h1><?= Html::encode($this->title) ?></h1>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>
</div>