<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashEmoneyFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\components\date\DateHelper;
use common\models\cash\CashEmoneyFlows;
use common\models\Contractor;
use frontend\modules\cash\models\CashContractorType;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$company = Yii::$app->user->identity->company;
if (empty($model)) {
    $model = new CashEmoneyFlows([
        'company_id' => $company->id,
        'emoney_id' => $company->emoney->id,
        'flow_type' => CashEmoneyFlows::FLOW_TYPE_INCOME,
        'date' => date(DateHelper::FORMAT_USER_DATE),
        'is_accounting' => 1,
    ]);
}
$emoneyData = $company->getEmoneys()->select('name')->orderBy([
    'is_closed' => SORT_ASC,
    'is_main' => SORT_DESC,
    'name' => SORT_ASC,
])->indexBy('id')->column();

switch ($model->flow_type) {
    case CashEmoneyFlows::FLOW_TYPE_INCOME:
        $contractorLabel = 'Покупатель';
        $contractorType = Contractor::TYPE_CUSTOMER;
        $contractorStaticItems = ArrayHelper::map(CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->all(), 'name', 'text');
        break;
    case CashEmoneyFlows::FLOW_TYPE_EXPENSE:
        $contractorLabel = 'Поставщик';
        $contractorType = Contractor::TYPE_SELLER;
        $contractorStaticItems = ArrayHelper::map(CashContractorType::find()
            ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
            ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
            ->all(), 'name', 'text');
        break;

    default:
        $contractorLabel = 'Контрагент';
        $contractorType = null;
        $contractorStaticItems = [];
        break;
}

$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' движение по e-money';
$fieldConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-3 control-label width-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-9',
    ],
];

$income = 'income' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');

if ($fromEmoney = Yii::$app->request->get('fromEmoney'))
    $model->emoney_id = $fromEmoney;
?>

<?php Pjax::begin([
    'id' => $model->isNewRecord ? 'add_emoney_flow_pjax' : 'update_emoney_flow_pjax',
    'enablePushState' => false,
    'timeout' => 5000,
]); ?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'action' => $model->isNewRecord ? ['create', 'emoney' => $fromEmoney] : ['update', 'id' => $model->id, 'emoney' => $fromEmoney],
    'id' => 'cash-emoney-form',
    'options' => [
        'class' => 'form-horizontal',
        'data' => [
            'pjax' => true,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ],
])); ?>

<div class="form-body">

    <?= $form->field($model, 'flow_type')->label('Тип')->radioList($model->isNewRecord ? CashEmoneyFlows::getFlowTypes() : [$model->flow_type => CashEmoneyFlows::getFlowTypes()[$model->flow_type]], [
        'class' => 'row',
        'encode' => false,
        'uncheck' => null,
        'item' => function ($index, $label, $name, $checked, $value) use ($model) {
            $input = Html::radio($name, $checked, [
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'class' => 'radio-inline m-l-n-md',
                    'style' => 'font-size: 14px;'
                ]
            ]);

            return Html::a($input, ['create', 'type' => $value], [
                'class' => 'col-xs-6',
                'style' => 'padding-left:4px; max-width: 140px; color: #333333; text-decoration: none;'
            ]);
        }
    ]); ?>

    <?= $form->field($model, 'emoney_id', $fieldConfig)->widget(Select2::class, [
        'data' => $emoneyData,
        'options' => [
            'id' => 'cashemoneyflows-emoney_id-' . $model->id,
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'placeholder' => '',
            'allowClear' => true,
        ],
    ]); ?>

    <?= $form->field($model, 'contractor_id', array_merge($fieldConfig, [
        'options' => [
            'class' => 'form-group cash-contractor_input',
        ],
    ]))->label($contractorLabel)->widget(ContractorDropdown::class, [
        'company' => $company,
        'contractorType' => $contractorType,
        'staticData' => $contractorStaticItems,
        'options' => [
            'id' => 'cashemoneyflows-contractor_id-' . $model->id,
            'class' => 'contractor-items-depend cash_contractor_id_select',
            'placeholder' => '',
            'data' => [
                'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
            ],
        ],
    ]); ?>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_EXPENSE): ?>
        <?= $form->field($model, 'expenditure_item_id', array_merge($fieldConfig, [
            'options' => [
                'class' => 'form-group required',
            ],
        ]))->widget(ExpenditureDropdownWidget::class, [
            'options' => [
                'id' => 'cashemoneyflows-expenditure_item_id-' . $model->id,
                'class' => 'flow-expense-items',
                'prompt' => '--',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ]
        ]); ?>
    <?php endif; ?>

    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'cashemoneyflows-expenditure_item_id-' . $model->id,
    ]); ?>

    <?php if ($model->flow_type == CashEmoneyFlows::FLOW_TYPE_INCOME): ?>
        <?= $form->field($model, 'income_item_id', array_merge($fieldConfig, [
            'options' => [
                'class' => 'form-group cash-income-item-id_input required cash_income_item_id_select',
            ],
        ]))->widget(ExpenditureDropdownWidget::class, [
            'income' => true,
            'options' => [
                'id' => 'cashemoneyflows-income_item_id-' . $model->id,
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '--',
            ],
            'pluginOptions' => [
                'width' => '100%',
            ]
        ]); ?>
    <?php endif; ?>

    <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
        'inputId' => 'cashemoneyflows-income_item_id-' . $model->id,
        'type' => 'income',
    ]); ?>

    <?= $form->field($model, 'amount', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
        'class' => 'form-control js_input_to_money_format',
        'style' => 'width: 100%; max-width: 125px;',
    ]); ?>

    <?= $form->field($model, 'date', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
    ])->label('Дата оплаты'); ?>

    <?= $form->field($model, 'date_time', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '9{2}:9{2}:9{2}',
        'options' => [
            'class' => 'form-control',
            'style' => 'width: 100%; max-width: 125px;',
            'value' => ($model->date_time) ? date('H:i:s', strtotime($model->date_time)) : date('H:i:s')
        ],
    ])->label('Время оплаты'); ?>

    <?= $form->field($model, 'number', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput(['style' => 'width: 100%; max-width: 125px;',]); ?>

    <?= $form->field($model, 'description')->textarea([
        'style' => 'resize: none; width: 100%;',
        'rows' => '2',
    ]); ?>

    <?= $form->field($model, 'invoices_list')
        ->widget(\frontend\modules\cash\widgets\InvoiceListInputWidget::class, [
            'invoicesListUrl' => ['/cash/order/un-payed-invoices'],
        ]); ?>

    <?= $form->field($model, 'is_accounting', [
        'options' => [
            'class' => ' col-md-9 col-sm-offset-3 m-l-n ',
            'style' => 'padding-left: 5px; padding-bottom: 5px;',
        ],

        'horizontalCssClasses' => [
            'offset' => '',
        ],
        'wrapperOptions' => [
            'class' => '',
        ],
        'template' => "{label}\n{input}\n{hint}\n{error}",
    ])->checkbox(); ?>

    <?= $form->field($model, 'recognitionDateInput', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                    'class' => 'flow-type-toggle ' . $income,
                ]) . Html::tag('span', 'расхода', [
                    'class' => 'flow-type-toggle ' . $expense,
                ]),
            'style' => 'width: 47.5%',
        ],
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block; width: 55%',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-6',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
        'disabled' => (bool)$model->is_prepaid_expense,
    ]); ?>

    <?= $form->field($model, 'is_prepaid_expense', [
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block; width: 40%; vertical-align: top',
        ],
        'horizontalCssClasses' => [
            'offset' => '',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
        'labelOptions' => [
            'style' => 'margin-top: 5px'
        ],
        'template' => "{label}\n{input}\n{hint}\n{error}",
    ])->checkbox(); ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%; float: right;">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'data' => [
                        'dissmis' => 'modal',
                    ],
                    'style' => 'width: 130px!important;',
                ]) ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg back',
                    'data' => [
                        'dissmis' => 'modal',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

</div>

<?php $form->end(); ?>

<?php Pjax::end(); ?>
<script type="text/javascript">
    var currentScript = document.currentScript || (function() {
        var scripts = document.getElementsByTagName("script");
        return scripts[scripts.length - 1];
    })();
    var currentModalContent = $(currentScript.parentNode).closest(".modal-content");

    if (currentModalContent) {
        currentModalContent.find(".modal-header > h1").html("<?= $header ?>");
    }
</script>
<?php
$this->registerJs('
$(document).on("pjax:complete", "#add_emoney_flow_pjax", function() {
  //$("#add_emoney_flow_pjax :checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
  //$("#add_emoney_flow_pjax :radio:not(.md-radiobtn)").uniform();
  $("#add_emoney_flow_pjax .date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            if (ev.currentTarget.value == "") {
                if ($input.data("last-value") == null) {
                    $input.data("last-value", ev.currentTarget.defaultValue);
                }
                var $lastDate = $input.data("last-value");
                $input.datepicker("setDate", $lastDate);
            } else {
                $input.data("last-value", ev.currentTarget.value);
            }
        }
    }
});

$(document).on("change", "#cashemoneyflows-is_prepaid_expense", function (e) {
    var $dateInput = $(this).closest(\'form\').find(\'#cashemoneyflows-recognitiondateinput\');
    if ($(this).is(":checked")) {
        $dateInput.val(\'\').attr(\'disabled\', true);
    } else {
        $dateInput.removeAttr(\'disabled\');
    }
});

$(document).ready(function () {
  $("#update_emoney_flow_pjax :checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
  $("#update_emoney_flow_pjax :radio:not(.md-radiobtn)").uniform();
  $("#update_emoney_flow_pjax .date-picker").datepicker({
        keyboardNavigation: false,
        forceParse: false,
        language: "ru",
        autoclose: true
    }).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            if (ev.currentTarget.value == "") {
                if ($input.data("last-value") == null) {
                    $input.data("last-value", ev.currentTarget.defaultValue);
                }
                var $lastDate = $input.data("last-value");
                $input.datepicker("setDate", $lastDate);
            } else {
                $input.data("last-value", ev.currentTarget.value);
            }
        }
    }
});
');
?>
