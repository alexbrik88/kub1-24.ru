<?php
/**
 * @var $this  yii\web\View
 * @var $model common\models\cash\CashBankFlows
 * @var $form  yii\bootstrap\ActiveForm
 */

use common\models\cash\CashBankFlows;
use common\models\Contractor;
use common\models\cash\CashBankReasonType;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ExpenditureDropdownWidget;
use frontend\modules\cash\models\CashContractorType;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

$company = Yii::$app->user->identity->company;
$accounArray = $company->getCheckingAccountants()
    ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC])->all();

$rsArray = ArrayHelper::map($accounArray, 'rs', function ($data) {
    return $data->rs . ', ' . $data->bank_name;
});

if (Yii::$app->request->get('canAddAccount'))
    $rsArray = ['add-checking-accountant' => '[ + ДОБАВИТЬ расч / счет ]'] + $rsArray;

$flowTypeItems = CashBankFlows::getFlowTypes();
if ($model->isNewRecord) {
    if (($flow_type = Yii::$app->request->get('flow_type')) !== null && isset($flowTypeItems[$flow_type])) {
        $typeItems = [$flow_type => $flowTypeItems[$flow_type]];
        $model->flow_type = $flow_type;
    } else {
        $typeItems = $flowTypeItems;
    }
} else {
    $typeItems = [$model->flow_type => $flowTypeItems[$model->flow_type]];
}

if (Yii::$app->request->get('canAddContractor')) {
    $sellerStaticItems = ['add-modal-contractor' => '[ + Добавить поставщика ]'];
    $customerStaticItems = ['add-modal-contractor' => '[ + Добавить покупателя ]'];
} else {
    $sellerStaticItems = [];
    $customerStaticItems = [];
}

$sellerStaticItems = array_merge($sellerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->andWhere(['not', ['id' => CashContractorType::BALANCE]])
    ->all(), 'name', 'text'));
$customerStaticItems = array_merge($customerStaticItems, ArrayHelper::map(CashContractorType::find()
    ->andWhere(['not', ['id' => CashContractorType::$internalTransferId]])
    ->all(), 'name', 'text'));

if (Yii::$app->request->get('onlyFNS')) {
    foreach ($sellerStaticItems as $key => $seller) {
        $seller = mb_strtolower($seller);
        if (strpos($seller, 'инспекция') === false || strpos($seller, 'налог') === false)
            unset($sellerStaticItems[$key]);
        else
            $model->contractor_id = $key;
    }
}

if ($model->flow_type === null) {
    $model->flow_type = CashBankFlows::FLOW_TYPE_INCOME;
}
$income = 'income' . ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? '' : ' hidden');
$expense = 'expense' . ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE ? '' : ' hidden');
$header = ($model->isNewRecord ? 'Добавить' : 'Изменить') . ' движение по банку';

$taxItemsIds = InvoiceExpenditureItem::taxIds();
$taxFields = in_array($model->expenditure_item_id, $taxItemsIds) ? '' : 'hidden';

$taxpayersStatus = TaxpayersStatus::find()->select('name')->andWhere(['not', ['id' => TaxpayersStatus::$deprecated]])->indexBy('id')->column();
$paymentDetails = PaymentDetails::find()->select('name')->indexBy('id')->column();
$paymentType = PaymentType::find()->select('name')->indexBy('id')->column();

?>

<?php \yii\widgets\Pjax::begin([
    'id' => 'update-movement-pjax',
    'enablePushState' => false,
    'linkSelector' => false,
]); ?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'js-cash_flow_update_form',
    'options' => [
        'class' => 'form-horizontal update-movement-form',
        'is_new_record' => $model->isNewRecord ? 1 : 0,
        'data' => [
            'type-income' => CashBankFlows::FLOW_TYPE_INCOME,
            'type-expense' => CashBankFlows::FLOW_TYPE_EXPENSE,
        ]
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-9',
        ],
    ],
])); ?>

<div class="form-body">
    <?php if ($redirect = Yii::$app->request->get('redirect')) : ?>
        <?= Html::hiddenInput('redirect', $redirect) ?>
    <?php endif ?>

    <?= $form->field($model, 'flow_type')->radioList($typeItems, [
        'class' => 'row',
        'uncheck' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::radio($name, $checked, [
                'class' => 'flow-type-toggle-input',
                'value' => $value,
                'label' => $label,
                'labelOptions' => [
                    'class' => 'col-xs-6',
                    'style' => 'padding-top: 7px; max-width: 140px;',
                ],
            ]);
        },
    ])->label('Тип'); ?>

    <?= $form->field($model, 'rs')->widget(Select2::classname(), [
        'data' => $rsArray,
        'options' => [
            'placeholder' => '',
        ],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]); ?>

    <div class="flow-type-toggle <?= $expense ?>">
        <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_SELLER,
            'staticData' => $sellerStaticItems,
            'options' => [
                'id' => 'seller_contractor_id',
                'class' => 'contractor-items-depend seller',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Поставщик'); ?>

        <?php if (Yii::$app->request->get('onlyFNS')): ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(InvoiceExpenditureItem::find()
                    ->where(['id' => [28, 46, 48, 53]])
                    ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
                    ->all(), 'id', 'name'),
                'options' => [
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'tax-items' => Json::encode($taxItemsIds),
                        'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                    ],
                ],
                'pluginOptions' => [
                ]
            ]); ?>
        <?php else: ?>
            <?= $form->field($model, 'expenditure_item_id')->widget(ExpenditureDropdownWidget::classname(), [
                'options' => [
                    'class' => 'flow-expense-items',
                    'prompt' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'data' => [
                        'tax-items' => Json::encode($taxItemsIds),
                        'tax-kbk' => common\models\TaxKbk::itemToKbk(),
                    ],
                ],
                'pluginOptions' => [
                ]
            ]); ?>
            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                'inputId' => 'cashbankflowsform-expenditure_item_id',
            ]) ?>
        <?php endif; ?>
    </div>

    <div class="flow-type-toggle <?= $income ?>">
        <?= $form->field($model, 'contractor_id')->widget(ContractorDropdown::class, [
            'company' => $company,
            'contractorType' => Contractor::TYPE_CUSTOMER,
            'staticData' => $customerStaticItems,
            'options' => [
                'id' => 'customer_contractor_id',
                'class' => 'contractor-items-depend customer cash_contractor_id_select',
                'placeholder' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
                'data' => [
                    'items-url' => Url::to(['/cash/default/items', 'cid' => '_cid_']),
                ],
            ],
        ])->label('Покупатель'); ?>

        <?= $form->field($model, 'income_item_id')->widget(ExpenditureDropdownWidget::classname(), [
            'income' => true,
            'options' => [
                'class' => 'flow-income-items cash_income_item_id_select',
                'prompt' => '',
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE,
            ],
            'pluginOptions' => [
            ]
        ]); ?>
        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
            'inputId' => 'cashbankflowsform-income_item_id',
            'type' => 'income',
        ]) ?>
    </div>

    <?= $form->field($model, 'amount', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'value' => !empty($model->amount) ? str_replace('.', ',', $model->amount / 100) : '',
        'class' => 'form-control js_input_to_money_format',
        'style' => 'width: 100%; max-width: 125px;',
    ]); ?>

    <?= $form->field($model, 'date', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
    ]); ?>

    <?= $form->field($model, 'payment_order_number', [
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label font-bold',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-5',
        ],
    ])->textInput(['style' => 'width: 100%; max-width: 125px;',]); ?>

    <div class="flow-type-toggle <?= $expense ?>">
        <div class="tax-payment-fields <?= $taxFields ?>">
            <?= $form->field($model, 'taxpayers_status_id')->widget(Select2::classname(), [
                'data' => $taxpayersStatus,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
            <?= $form->field($model, 'kbk', [
                'wrapperOptions' => [
                    'class' => 'col-sm-5',
                    'style' => 'max-width: 230px;',
                ],
                'hintOptions' => [
                    'class' => 'col-sm-4',
                    'style' => 'color: red;',
                ],
            ])->textInput([
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'style' => 'width: 100%; max-width: 200px;',
            ])->hint('Необходим для точного расчета налогов к уплате'); ?>
            <?= $form->field($model, 'oktmo_code')->textInput([
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'style' => 'width: 100%; max-width: 200px;',
            ]); ?>
            <?= $form->field($model, 'payment_details_id')->widget(Select2::classname(), [
                'data' => $paymentDetails,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
            <?= $form->field($model, 'tax_period_code')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '**.99.9999',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XX.XX.XXXX',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                    'style' => 'width: 100%; max-width: 125px;',
                ],
            ]) ?>
            <?= $form->field($model, 'document_number_budget_payment')->textInput([
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'style' => 'width: 100%; max-width: 125px;',
            ]); ?>
            <?= $form->field($model, 'dateBudgetPayment', [
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
            ])->textInput([
                'class' => 'form-control date-picker',
                'style' => 'width: 100%; max-width: 125px;',
                'data' => [
                    'date-viewmode' => 'years',
                ],
            ]); ?>
            <?= $form->field($model, 'payment_type_id')->widget(Select2::classname(), [
                'data' => $paymentType,
                'options' => [
                    'placeholder' => '',
                    'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]); ?>
            <?= $form->field($model, 'uin_code')->textInput([
                'disabled' => $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME,
                'style' => 'width: 100%; max-width: 125px;',
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea([
        'style' => 'resize: none;',
        'rows' => '2',
    ]); ?>

    <?= $form->field($model, 'invoices_list')
        ->widget(\frontend\modules\cash\widgets\InvoiceListInputWidget::className()); ?>

    <?= $form->field($model, 'recognitionDateInput', [
        'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
        'labelOptions' => [
            'class' => 'col-md-3 control-label width-label bold-text',
            'label' => 'Дата признания ' . Html::tag('span', 'дохода', [
                    'class' => 'flow-type-toggle ' . $income,
                ]) . Html::tag('span', 'расхода', [
                    'class' => 'flow-type-toggle ' . $expense,
                ]),
            'style' => 'width: 47.5%',
        ],
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block; width: 55%',
        ],
        'wrapperOptions' => [
            'class' => 'col-md-6',
        ],
    ])->textInput([
        'class' => 'form-control date-picker',
        'style' => 'width: 100%; max-width: 125px;',
        'data' => [
            'date-viewmode' => 'years',
        ],
        'disabled' => (bool)$model->is_prepaid_expense,
    ]); ?>

    <?= $form->field($model, 'is_prepaid_expense', [
        'options' => [
            'class' => 'form-group',
            'style' => 'display: inline-block; width: 50%',
        ],
    ])->checkbox(); ?>

    <div class="form-actions">
        <div class="row action-buttons" id="buttons-fixed">
            <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'style' => 'width: 130px!important;',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                    'title' => 'Сохранить',
                ]); ?>
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            </div>
            <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%; float: right;">
                <?= Html::button('Отменить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                    'data' => [
                        'dissmis' => 'modal',
                    ],
                    'style' => 'width: 130px!important;',
                ]) ?>
                <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg back',
                    'data' => [
                        'dissmis' => 'modal',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
<?php $form->end(); ?>

<script type="text/javascript">
    var currentScript = document.currentScript || (function () {
        var scripts = document.getElementsByTagName("script");
        return scripts[scripts.length - 1];
    })();
    var currentModalContent = $(currentScript.parentNode).closest(".modal-content");

    if (currentModalContent) {
        currentModalContent.find(".modal-header > h1").html("<?= $header ?>");

        $(".date-picker", currentModalContent).datepicker({
            keyboardNavigation: false,
            forceParse: false,
            language: "ru",
            format: "dd.mm.yyyy",
            autoclose: true
        });

        $("input[type=radio], input[type=checkbox]", currentModalContent).uniform();
    }
    $(document).on("change", "#cashbankflowsform-is_prepaid_expense", function (e) {
        var $dateInput = $(this).closest('form').find('#cashbankflowsform-recognitiondateinput');
        if ($(this).is(":checked")) {
            $dateInput.val('').attr('disabled', true);
        } else {
            $dateInput.removeAttr('disabled');
        }
    });
</script>

<?php \yii\widgets\Pjax::end(); ?>
