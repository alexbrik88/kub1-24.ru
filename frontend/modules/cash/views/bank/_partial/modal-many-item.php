<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2018
 * Time: 17:09
 */

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use common\models\cash\CashBankFlows;
use frontend\widgets\ExpenditureDropdownWidget;

/* @var $model CashBankFlows
 */
?>
<div class="modal fade" id="many-item" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h1>Изменить статью</h1>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
                    'action' => Url::to(['many-item', 'foreign' => $foreign ?? null]),
                    'options' => [
                        'class' => 'form-horizontal',
                    ],
                    'id' => 'js-cash_flow_update_item_form',
                ])); ?>
                <div class="form-body">
                    <div class="income-item-block hidden">
                        <div class="form-group">
                            <label class="col-xs-12 col-md-3 control-label width-label bold-text"
                                   for="cashbankflowsform-flow_type">
                                Для типа
                            </label>

                            <div class="col-xs-10 col-md-8 margin-left-15">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Приход <span style="padding-left: 30px;">изменить на:</span>',
                                            'labelOptions' => [
                                                'class' => 'radio-inline m-l-n-md',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'incomeItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label width-label font-bold',
                            ],
                            'wrapperOptions' => [
                                'class' => 'col-md-9',
                            ],
                            'options' => [
                                'id' => 'js-income_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'income' => true,
                            'options' => [
                                'prompt' => '',
                                'name' => 'incomeItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '97%',
                            ]
                        ])->label('Статья прихода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-incomeitemidmanyitem',
                            'type' => 'income',
                        ]); ?>
                    </div>

                    <div class="expenditure-item-block hidden">
                        <div class="form-group">
                            <label class="col-xs-12 col-md-3 control-label width-label bold-text"
                                   for="cashbankflowsform-flow_type">
                                Для типа
                            </label>

                            <div class="col-xs-10 col-md-8 margin-left-15">
                                <div id="cashbankflowsform-flow_type" aria-required="true">
                                    <div class="col-xs-12 m-l-n">
                                        <?= Html::radio(null, true, [
                                            'label' => 'Расход <span style="padding-left: 30px;">изменить на:</span>',
                                            'labelOptions' => [
                                                'class' => 'radio-inline m-l-n-md',
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?= $form->field($model, 'expenditureItemIdManyItem', [
                            'labelOptions' => [
                                'class' => 'col-md-3 control-label width-label font-bold',
                            ],
                            'wrapperOptions' => [
                                'class' => 'col-md-9',
                            ],
                            'options' => [
                                'id' => 'js-expenditure_item_id_wrapper required',
                                'class' => 'form-group',
                            ],
                        ])->widget(ExpenditureDropdownWidget::classname(), [
                            'options' => [
                                'prompt' => '',
                                'name' => 'expenditureItemIdManyItem',
                            ],
                            'pluginOptions' => [
                                'width' => '97%',
                            ]
                        ])->label('Статья расхода'); ?>
                        <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                            'inputId' => 'cashbanksearch-expenditureitemidmanyitem',
                        ]); ?>
                    </div>

                    <div class="form-actions">
                        <div class="row action-buttons" id="buttons-fixed">
                            <div class="spinner-button col-sm-1 col-xs-1 text-left" style="width: 24%;">
                                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                                    'data-style' => 'expand-right',
                                    'style' => 'width: 130px!important;',
                                ]); ?>
                                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                                    'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                                    'title' => 'Сохранить',
                                ]); ?>
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                            </div>
                            <div class="spinner-button col-sm-2 col-xs-1 text-right" style="width: 24%;">
                                <?php if (Yii::$app->request->isAjax): ?>
                                    <?= Html::button('Отменить', [
                                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                                        'data-dismiss' => 'modal',
                                        'style' => 'width: 130px!important;',
                                    ]) ?>
                                    <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                                        'data-dismiss' => 'modal',
                                    ]) ?>
                                <?php else: ?>
                                    <?= Html::a('Отменить', ['index'], [
                                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                                        'data-dismiss' => 'modal',
                                        'style' => 'width: 130px!important;',
                                    ]); ?>
                                    <?= Html::a('<i class="fa fa-reply fa-2x"></i></button>', ['index'], [
                                        'class' => 'btn darkblue widthe-100 hidden-lg back',
                                        'data-dismiss' => 'modal',
                                    ]); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
</div>