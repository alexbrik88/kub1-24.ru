<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashFlowLinkage */
/* @var $success bool */

?>

<?php Pjax::begin([
    'id' => 'cash-flow-linkage-form-pjax',
    'enablePushState' => false,
]); ?>

<?= Html::beginForm('', 'post', [
    'data-pjax' => true,
]) ?>

<div class="text-center">
    <?= Html::submitButton('Да', [
        'class' => 'button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>
    <?= Html::button('Нет', [
        'class' => 'button-regular button-hover-transparent button-width-medium ml-1 ajax-modal-hide',
    ]) ?>
</div>

<?= Html::endForm() ?>

<?php if ($success) : ?>
<script type="text/javascript">
    $.pjax.reload('#cash-flow-linkage-pjax', {replace: false, url: '<?= Url::to(['index']) ?>'});
    $('#linkage-ajax-modal-box').modal('hide');
</script>
<?php endif ?>

<?php Pjax::end(); ?>
