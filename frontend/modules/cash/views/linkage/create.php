<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $model common\models\cash\CashFlowLinkage */
/* @var $success bool */

?>
<div class="cash-flow-linkage-create">

    <?= $this->render('_form', [
        'company' => $company,
        'model' => $model,
        'success' => $success,
    ]) ?>

</div>
