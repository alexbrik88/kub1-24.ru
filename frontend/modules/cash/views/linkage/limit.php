<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

?>

<div>
    На вашем тарифе можно иметь только одну активную связку.
</div>
<div class="mt-2">
    Создавать связки без ограничений можно, если у вас любой из тарифов "ФинДиректор".
</div>
<div class="my-2">
    Перейти к выбору тарифа "ФинДиректор"?
</div>
<div class="text-center mt-4">
    <?= Html::a('Да', ['/subscribe/default/index'], [
        'class' => 'button-regular button-hover-transparent button-width-medium mr-2',
    ]) ?>
    <?= Html::button('Нет', [
        'class' => 'button-regular button-hover-transparent button-width-medium ml-1 ajax-modal-hide',
    ]) ?>
</div>
