<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashFlowLinkage */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cash Flow Linkages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cash-flow-linkage-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_id',
            'number',
            'status',
            'is_deleted',
            'for_flow_type',
            'for_wallet_id',
            'for_wallet_table',
            'for_contractor_id',
            'create_flow_type',
            'create_wallet_id',
            'create_wallet_table',
            'create_contractor_id',
            'date_diff',
            'amount_diff',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
        ],
    ]) ?>

</div>
