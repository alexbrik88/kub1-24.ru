<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\cash\CashFlowLinkage */

?>

<div>
    <?= $model->getThenFlowType() ?> в <?= Html::encode($model->thenWallet->name) ?>
</div>
<div>
    От: <?= Html::encode($model->thenContractor->shortTitle) ?>
</div>
<div>
    Дата: <?= $model->thenDateDiff ?>
</div>
<div>
    Сумма: <?= mb_strtolower($model->thenAmountAction) ?> <?= $model->then_amount_action_value * 1; ?>%
</div>
<div>
    Назначение: <?= Html::encode($model->then_description ?? '---') ?>
</div>
