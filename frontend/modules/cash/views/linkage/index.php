<?php

use common\models\cash\CashFlowLinkage;
use frontend\components\Icon;
use frontend\widgets\AjaxModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\cash\models\LinkageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cash Flow Linkages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-flow-linkage-index">
    <div class="mb-2">
        Варианты связок:
    </div>
    <div class="d-flex align-items-start">
        <div style="width: 75px;">
            Тип №1:
        </div>
        <div>
            <div class="mb-2">
                <strong>
                    Вы платите на какое-то ИП или ООО и потом хотите, чтобы эти деньги отобразились в кассе за минусом, например 10%.
                </strong>
            </div>
            <div class="mb-2">
                Имеющиеся связки:
            </div>

            <?php Pjax::begin([
                'id' => 'cash-flow-linkage-pjax',
                'enablePushState' => false,
            ]); ?>

            <?= common\components\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-style table-count-list',
                ],
                'rowOptions' => function ($model, $key, $index, $grid) {
                    if ($model->status == CashFlowLinkage::STATUS_INACTIVE) {
                        return [
                            'class' => 'text-muted',
                        ];
                    }

                    return [];
                },
                'layout' => $this->render('//layouts/grid/layout_no_perpage'),
                'columns' => [
                    'number',
                    [
                        'label' => 'Если есть операция',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $this->render('_if_flow', ['model' => $model]);
                        }
                    ],
                    [
                        'label' => 'То создается операция',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $this->render('_then_flow', ['model' => $model]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']);

                                return Html::a($icon, '#', [
                                    'class' => 'linkage-ajax-modal-btn',
                                    'data-title' => 'Редактирование связки тип 1',
                                    'data-url' => $url,
                                    'data-pjax' => '0',
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                $icon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']);

                                return Html::a($icon, '#', [
                                    'class' => 'linkage-ajax-modal-btn',
                                    'data-title' => 'Вы уверены, что хотите удалить связку?',
                                    'data-url' => $url,
                                    'data-pjax' => '0',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

            <?php Pjax::end(); ?>

            <div class="mt-3 d-flex justify-content-between">
                <?= Html::button(Icon::get('add-icon', ['class' => 'mr-2']).' Создать', [
                    'class' => 'linkage-ajax-modal-btn button-regular button-hover-content-red button-width',
                    'data-title' => 'Создание связки тип 1',
                    'data-url' => Url::to(['create']),
                ]) ?>
            </div>
        </div>
    </div>

    <?= AjaxModalWidget::widget([
        'id' => 'linkage-ajax-modal-box',
        'linkSelector' => '.linkage-ajax-modal-btn',
    ]) ?>

    <div class="mt-3 text-center">
        <?= Html::button('Закрыть', ['class' => 'button-width button-regular button-hover-transparent ajax-modal-hide mx-auto']) ?>
    </div>
</div>
