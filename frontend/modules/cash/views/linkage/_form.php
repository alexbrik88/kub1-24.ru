<?php

use common\models\Contractor;
use common\models\cash\CashBankFlows;
use frontend\widgets\ContractorDropdown;
use kartik\select2\Select2;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cash\models\Linkage */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $success bool */

$income = CashBankFlows::FLOW_TYPE_INCOME;
$expense = CashBankFlows::FLOW_TYPE_EXPENSE;

$this->registerJs(<<<JS
    $(document).on('change', '.cash-flow-linkage-flow_type', function () {
        var value = $(this).closest('.form-filter').find(':checked').val();
        var itemsBox = $(this).closest('.contractor_select_row').find('.contractor_select_col');
        $('.contractor_select_item', itemsBox).toggleClass('hidden', true).find('select').prop('disabled', true);
        $('.contractor_select_item.flow_type_'+value, itemsBox).toggleClass('hidden', false).find('select').prop('disabled', false);
    });
JS
);
?>

<?php Pjax::begin([
    'id' => 'cash-flow-linkage-form-pjax',
    'enablePushState' => false,
]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'cash-flow-linkage-form',
    'enableAjaxValidation' => true,
    'fieldConfig' => Yii::$app->params['kubFieldConfig'],
    'options' => [
        'data-pjax' => true,
    ],
]); ?>

<div class="mb-2">
    <strong>
        ЕСЛИ есть операция
    </strong>
</div>
<div class="row contractor_select_row">
    <div class="col-4">
        <?= $form->field($model, 'if_flow_type')->radioList($model->getFlowTypeItems(), [
            'class' => 'd-flex align-items-center',
            'style' => 'min-height: 44px;',
            'itemOptions' => [
                'class' => 'cash-flow-linkage-flow_type',
                'wrapperOptions' => [
                    'class' => 'd-flex mr-3',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
            ],
        ]) ?>
    </div>
    <div class="col-4">
        <?= $form->field($model, 'if_wallet_id')->widget(Select2::classname(), [
            'data' => $model->getIfWalletItems(),
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ]
        ]) ?>
    </div>
    <div class="col-4 contractor_select_col">
        <div class="contractor_select_item flow_type_<?= $expense ?><?= $model->if_flow_type == $income ? ' hidden' : '' ?>">
            <?= $form->field($model, 'if_contractor_id')->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_SELLER,
                'options' => [
                    'id' => 'if_seller_contractor_id',
                    'placeholder' => '',
                    'disabled' => $model->if_flow_type == $income,
                    'value' => $model->if_flow_type == $income ? '' : $model->if_contractor_id,
                ],
            ])->label('Поставщик'); ?>
        </div>
        <div class="contractor_select_item flow_type_<?= $income ?><?= $model->if_flow_type == $expense ? ' hidden' : '' ?>">
            <?= $form->field($model, 'if_contractor_id')->widget(ContractorDropdown::class, [
                'company' => $company,
                'contractorType' => Contractor::TYPE_CUSTOMER,
                'options' => [
                    'id' => 'if_customer_contractor_id',
                    'placeholder' => '',
                    'disabled' => $model->if_flow_type == $expense,
                    'value' => $model->if_flow_type == $expense ? '' : $model->if_contractor_id,
                ],
            ])->label('Покупатель'); ?>
        </div>
    </div>
</div>

<div class="mb-2">
    <strong>
        ТО создается операция
    </strong>
</div>
<div class="row contractor_select_row">
    <div class="col-4">
        <?= $form->field($model, 'then_flow_type')->radioList($model->getFlowTypeItems(), [
            'class' => 'd-flex align-items-center',
            'style' => 'min-height: 44px;',
            'itemOptions' => [
                'class' => 'cash-flow-linkage-flow_type',
                'wrapperOptions' => [
                    'class' => 'd-flex mr-3',
                ],
                'labelOptions' => [
                    'class' => '',
                ],
            ],
        ]) ?>
    </div>
    <div class="col-4">
        <?= $form->field($model, 'thenWalletId')->widget(Select2::classname(), [
            'data' => $model->getThenWalletItems(),
            'hideSearch' => true,
            'pluginOptions' => [
                'width' => '100%',
            ],
        ]) ?>
    </div>
    <div class="col-4">
        <div class="contractor_select_col">
            <div class="contractor_select_item flow_type_<?= $expense ?><?= $model->then_flow_type == $income ? ' hidden' : '' ?>">
                <?= $form->field($model, 'then_contractor_id')->widget(ContractorDropdown::class, [
                    'company' => $company,
                    'contractorType' => Contractor::TYPE_SELLER,
                    'options' => [
                        'id' => 'then_seller_contractor_id',
                        'placeholder' => '',
                        'disabled' => $model->then_flow_type == $income,
                        'value' => $model->then_flow_type == $income ? '' : $model->then_contractor_id,
                    ],
                ])->label('Поставщик'); ?>
            </div>
            <div class="contractor_select_item flow_type_<?= $income ?><?= $model->then_flow_type == $expense ? ' hidden' : '' ?>">
                <?= $form->field($model, 'then_contractor_id')->widget(ContractorDropdown::class, [
                    'company' => $company,
                    'contractorType' => Contractor::TYPE_CUSTOMER,
                    'options' => [
                        'id' => 'then_customer_contractor_id',
                        'placeholder' => '',
                        'disabled' => $model->then_flow_type == $expense,
                        'value' => $model->then_flow_type == $expense ? '' : $model->then_contractor_id,
                    ],
                ])->label('Покупатель'); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4"></div>
    <div class="col-8">
        <div class="row">
            <div class="col-5">
                <?= $form->field($model, 'then_date_diff')->widget(Select2::classname(), [
                    'data' => $model->getDateDiffItems(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]) ?>
            </div>
            <div class="col-5">
                <?= $form->field($model, 'then_amount_action')->widget(Select2::classname(), [
                    'data' => $model->getAmountActionItems(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'width' => '100%',
                    ]
                ]) ?>
            </div>
            <div class="col-2">
                <?= $form->field($model, 'then_amount_action_value')->textInput() ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-4"></div>
    <div class="col-8">
        <?= $form->field($model, 'then_description')->textarea(['maxlength' => true]) ?>
    </div>
</div>

<div class="mt-3 d-flex justify-content-between">
    <?= Html::submitButton('Сохранить', ['class' => 'button-regular button-width button-regular_red ladda-button']) ?>
    <?= Html::button('Отменить', ['class' => 'button-width button-regular button-hover-transparent ajax-modal-hide']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php if ($success) : ?>
<script type="text/javascript">
    $.pjax.reload('#cash-flow-linkage-pjax', {replace: false, url: '<?= Url::to(['index']) ?>'});
    $('#linkage-ajax-modal-box').modal('hide');
</script>
<?php endif ?>

<?php Pjax::end(); ?>
