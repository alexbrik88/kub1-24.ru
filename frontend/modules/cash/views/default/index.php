<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

use common\components\TextHelper;
use yii\grid\DataColumn;
use yii\helpers\Html;

$this->title = 'Итого';
?>
<!-- default/index -->
<div class="portlet box">
    <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button']); ?>
    <div class="page-title"><?= $this->title; ?></div>
</div>
<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption pad-top-txt-2">
            Обороты
        </div>
    </div>
    <div class="portlet-body overl-auto">
        <?= common\components\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'layout' => '{items}',
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover cash-flow-table',
            ],
            'headerRowOptions' => [
                'class' => 'heading',
            ],
            'rowOptions' => function($model, $key, $index, $grid) {
                return ['class' => $model['cssClass']];
            },
            'columns' => [
                [
                    'attribute' => 'typeName',
                    'label'     => '',
                    'headerOptions' => [
                        'width' => '4%',
                    ],
                    'value' => function($model, $key, $index, DataColumn $column) {
                        return $model['cssClass'] ?
                                Html::tag('span', $model[$column->attribute], ['class' => $model['cssClass']]) :
                                $model[$column->attribute];
                    },
                    'format' => 'html',
                ],
                [
                    'attribute' => 'beginBalance',
                    'label'     => 'Баланс на начало периода',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function($model, $key, $index, DataColumn $column) {
                        return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                    },
                    'contentOptions' => [
                        'class' => 'text-right'
                    ],
                ],
                [
                    'attribute' => 'income',
                    'label'     => 'Приход',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function($model, $key, $index, DataColumn $column) {
                        return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                    },
                    'contentOptions' => [
                        'class' => 'text-right'
                    ],
                ],
                [
                    'attribute' => 'expense',
                    'label'     => 'Расход',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function($model, $key, $index, DataColumn $column) {
                        return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                    },
                    'contentOptions' => [
                        'class' => 'text-right'
                    ],
                ],
                [
                    'attribute' => 'endBalance',
                    'label'     => 'Баланс на конец периода',
                    'headerOptions' => [
                        'width' => '10%',
                    ],
                    'value' => function($model, $key, $index, DataColumn $column) {
                        return TextHelper::invoiceMoneyFormat($model[$column->attribute], 2);
                    },
                    'contentOptions' => [
                        'class' => 'text-right'
                    ],
                ],
            ]
        ]); ?>
    </div>
</div>