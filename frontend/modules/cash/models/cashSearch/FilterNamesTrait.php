<?php
namespace frontend\modules\cash\models\cashSearch;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase as Cash;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\cash\models\CashContractorType;
use common\models\project\Project;
use frontend\modules\cash\models\CashSearch;

trait FilterNamesTrait {

    public function getFlowTypeFilterName()
    {
        if (strlen($this->flow_type) && $this->flow_type > -1) {
            switch ($this->flow_type) {
                case Cash::FLOW_TYPE_EXPENSE:
                    return 'Расход';
                case Cash::FLOW_TYPE_INCOME:
                    return 'Приход';
                case Cash::INTERNAL_TRANSFER:
                    return 'Между счетами';
            }
        }

        return 'Все';
    }

    public function getOperationTypeFilterName()
    {
        switch ($this->operation_type) {
            case Cash::OPERATION_TYPE_FACT:
                return 'Факт';
            case Cash::OPERATION_TYPE_PLAN:
                return 'План';
        }

        return ($this->_userConfigShowPlan) ? 'Все' : 'Факт';
    }

    public function getWalletFilterName()
    {
        if (count($this->wallet_ids) > 1) {
            return 'Мои счета ' . count($this->wallet_ids);
        }
        elseif ($walletId = reset($this->wallet_ids)) {
            @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            switch ($cashBlock) {
                case Cash::WALLET_BANK:
                    return ArrayHelper::getValue($this->bankItems, $currentWalletId, 'Банк');
                case Cash::WALLET_CASHBOX:
                    return ArrayHelper::getValue($this->cashboxItems, $currentWalletId, 'Касса');
                case Cash::WALLET_EMONEY:
                    return ArrayHelper::getValue($this->emoneyItems, $currentWalletId, 'E-money');
                case Cash::WALLET_ACQUIRING:
                    return ArrayHelper::getValue($this->acquiringItems, $currentWalletId, 'Эквайринг');
                case Cash::WALLET_CARD:
                    return ArrayHelper::getValue($this->cardItems, $currentWalletId, 'Карты');
            }
        }

        return 'Все счета';
    }

    public function getContractorFilterName()
    {
        if (count($this->contractor_ids) > 1) {
            return 'Контрагенты ' . count($this->contractor_ids);
        }
        elseif ($contractorId = reset($this->contractor_ids)) {
            if (in_array($contractorId, [
                CashContractorType::BANK_TEXT,
                CashContractorType::ORDER_TEXT,
                CashContractorType::EMONEY_TEXT,
                CashContractorType::BALANCE_TEXT,
                CashContractorType::COMPANY_TEXT,
                CashContractorType::CUSTOMERS_TEXT,
            ])) {
                $contractor = CashContractorType::findOne(['name' => $contractorId]);

                return $contractor->text;
            }
            return ($contractor = Contractor::findOne($contractorId)) ? $contractor->nameWithType : 'Неизвестный Контрагент';
        }

        return 'Все контрагенты';
    }

    public function getReasonFilterName()
    {
        if (count($this->reason_ids) > 1) {
            return 'Статьи ' . count($this->reason_ids);
        }
        elseif ($reasonId = reset($this->reason_ids)) {
            if ($reasonId == 'empty') {
                return 'Без статьи';
            } elseif (substr($reasonId, 0, 2) === 'i_') {
                return ($reason = InvoiceIncomeItem::findOne((int)substr($reasonId, 2))) ? $reason->name : 'Неизвестная статья';
            }
            elseif (substr($reasonId, 0, 2) === 'e_') {
                return ($reason = InvoiceExpenditureItem::findOne((int)substr($reasonId, 2))) ? $reason->name : 'Неизвестная статья';
            }
        }

        return 'Все статьи';
    }

    public function getProjectFilterName()
    {
        if ($this->project_id) {

            return ($project = Project::findOne($this->project_id)) ? $project->name : 'Неизвестный проект';
        }

        return 'Все';
    }

    public function getFlowTypeFilterName2()
    {
        if (strlen($this->flow_type_2) && $this->flow_type_2 > -1) {
            switch ($this->flow_type_2) {
                case CashSearch::FILTER_FLOW_TYPE_EXPENSE:
                    return 'Расход';
                case CashSearch::FILTER_FLOW_TYPE_INCOME:
                    return 'Приход';
            }
        }

        return 'Приход и Расход';
    }

    public function getContractorFilterName2()
    {
        return '<b>Контрагент: </b>' . htmlspecialchars($this->contractor_name_2);
    }

    public function getDescriptionFilterName2()
    {
        return '<b>Назначение: </b>' . htmlspecialchars($this->description_2);
    }

    public function getAmountFilterName2()
    {
        switch ($this->amount_type_2) {
            case self::FILTER_AMOUNT_TYPE_LESS_THAN:
                return '<b>Сумма меньше: </b>' . number_format($this->amount_2, 2, ',', ' ');
            case self::FILTER_AMOUNT_TYPE_MORE_THAN:
                return '<b>Сумма больше: </b>' . number_format($this->amount_2, 2, ',', ' ');
            case self::FILTER_AMOUNT_TYPE_EQUAL:
            default:
                return '<b>Сумма: </b>' . number_format($this->amount_2, 2, ',', ' ');
        }
    }
}