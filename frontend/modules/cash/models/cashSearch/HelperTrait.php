<?php
namespace frontend\modules\cash\models\cashSearch;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\Company;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use yii\helpers\Url;

trait HelperTrait {

    public function getBankRsById($id)
    {
        return array_search($id, $this->_bankRs2Id) ?: null;
    }

    public function getBankIdByRs($rs)
    {
        return $this->_bankRs2Id[$rs] ?? null;
    }

    /**
     * @param $company
     * @param $formattedFlowAmount
     * @param $flows
     * @param $returnFormatted
     * @return string
     */
    public static function getUpdateFlowLink($formattedFlowAmount, $flows = [], $returnFormatted = true)
    {
        $flowTable = ArrayHelper::getValue($flows, 'tb');
        $flowId = ArrayHelper::getValue($flows, 'id');
        $isPlanFlow = substr(ArrayHelper::getValue($flows, 'wallet_id'), 0, 4) == 'plan';

        $url = 'javascript:;';

        switch ($flowTable) {

            // RUB
            case CashBankFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-bank-flow', 'originWallet' => self::WALLET_BANK, 'originFlow' => $flowId]);
                break;
            case CashOrderFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-order-flow', 'originWallet' => self::WALLET_ORDER, 'originFlow' => $flowId]);
                break;
            case CashEmoneyFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-emoney-flow', 'originWallet' => self::WALLET_EMONEY, 'originFlow' => $flowId]);
                break;
            case PlanCashFlows::tableName():
                if ($planFlow = PlanCashFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/multi-wallet/update-plan-flow', 'originWallet' => $planFlow->payment_type, 'originFlow' => $flows['id']]);
                break;
            case AcquiringOperation::tableName():
                $url = Url::to(['/acquiring/acquiring/update', 'id' => $flowId]);
                break;
            case CardOperation::tableName():
                $url = Url::to(['/cards/operation/update', 'id' => $flowId, 'redirectUrl' => Url::current()]);
                break;

            // FOREIGN
            case CashBankForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-bank-flow', 'originWallet' => self::WALLET_FOREIGN_BANK, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
            case CashOrderForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-order-flow', 'originWallet' => self::WALLET_FOREIGN_ORDER, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
            case CashEmoneyForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/multi-wallet/update-emoney-flow', 'originWallet' => self::WALLET_FOREIGN_EMONEY, 'originFlow' => $flowId, 'isForeign' => 1]);
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                if ($planFlow = PlanCashForeignCurrencyFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/multi-wallet/update-plan-flow', 'originWallet' => $planFlow->payment_type, 'originFlow' => $flows['id'], 'isForeign' => 1]);
                break;
        }

        if ($flows['is_internal_transfer']) {

            // RUB
            switch ($flowTable) {
                case CashBankFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_BANK, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key']]);
                    break;
                case CashOrderFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_ORDER, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key']]);
                    break;
                case CashEmoneyFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_EMONEY, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key']]);
                    break;
            }

            // FOREIGN
            switch ($flowTable) {
                case CashBankForeignCurrencyFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_FOREIGN_BANK, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key'], 'isForeign' => 1]);
                    break;
                case CashOrderForeignCurrencyFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_FOREIGN_ORDER, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key'], 'isForeign' => 1]);
                    break;
                case CashEmoneyForeignCurrencyFlows::tableName():
                    $url = Url::to(['/cash/multi-wallet/update-internal', 'wallet' => self::WALLET_FOREIGN_EMONEY, 'flow' => $flows['id'], 'transferKey' => $flows['transfer_key'], 'isForeign' => 1]);
                    break;
            }
        }

        if (!$returnFormatted)
            return $url;

        return \yii\helpers\Html::a($formattedFlowAmount, $url, [
                'title' => 'Изменить',
                'class' => 'update-flow-item link link-bleak' . ($isPlanFlow ? ' plan' : ''),
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#update-movement',
                    'pjax' => 0
                ],
            ]);
    }

    public static function getUpdateFlowSpan($formattedFlowAmount, $flows = [], $returnFormatted = true)
    {
        $link = self::getUpdateFlowLink($formattedFlowAmount, $flows, $returnFormatted);
        return str_replace(
            [
                '<a', '/a>',
                'update-flow-item',
                'data-toggle=',
                'title='
            ],
            [
                '<span',
                '/span>',
                '',
                'data-_toggle',
                '_title'
            ], $link);
    }
    
    public static function getDeleteFlowLink($flows = []) 
    {
        switch ($flows['tb']) {
            // RUB
            case CashBankFlows::tableName():
                return Url::to(['/cash/bank/delete', 'id' => $flows['id']]);
            case CashOrderFlows::tableName():
                return Url::to(['/cash/order/delete', 'id' => $flows['id']]);
            case CashEmoneyFlows::tableName():
                return Url::to(['/cash/e-money/delete', 'id' => $flows['id']]);
            case PlanCashFlows::tableName():
                return Url::to(['/cash/plan/delete', 'id' => $flows['id']]);
            case AcquiringOperation::tableName():
                return Url::to(['/acquiring/acquiring/delete', 'id' => $flows['id']]);
            case CardOperation::tableName():
                return Url::to(['/cards/operation/delete', 'id' => $flows['id'], 'OperationDeleteForm[delete]' => 1, 'redirectUrl' => Url::current()]);
            // FOREIGN
            case CashBankForeignCurrencyFlows::tableName():
                return Url::to(['/cash/bank/delete', 'id' => $flows['id'], 'foreign' => 1]);
            case CashOrderForeignCurrencyFlows::tableName():
                return Url::to(['/cash/order/delete', 'id' => $flows['id'], 'foreign' => 1]);
            case CashEmoneyForeignCurrencyFlows::tableName():
                return Url::to(['/cash/e-money/delete', 'id' => $flows['id'], 'foreign' => 1]);
            case PlanCashForeignCurrencyFlows::tableName():
                return Url::to(['/cash/plan/delete', 'id' => $flows['id'], 'foreign' => 1]);
        }
        
        return 'javascript:;';
    }

}