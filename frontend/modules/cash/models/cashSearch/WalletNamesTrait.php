<?php
namespace frontend\modules\cash\models\cashSearch;

use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use yii\db\Expression;

trait WalletNamesTrait {

    // wallets names
    public $companyItems = [];
    public $bankItems = [];
    public $emoneyItems = [];
    public $cashboxItems = [];
    public $acquiringItems = [];
    public $cardItems = [];
    private $_bankRs2Id = [];

    public function initWalletNames($companiesIds, $onlyWallet = null)
    {
        foreach ((array)$companiesIds as $companyId) {
            if ($company = Company::findOne(['id' => $companyId])) {
                $this->companyItems[$companyId] = $company->getShortName();
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_BANK)
                    $this->bankItems = $this->bankItems + $this->_getBankNames($company);
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_EMONEY)
                    $this->emoneyItems = $this->emoneyItems + $this->_getEmoneyNames($company);
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_CASHBOX)
                    $this->cashboxItems = $this->cashboxItems + $this->_getCashboxNames($company);
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_ACQUIRING)
                    $this->acquiringItems = $this->acquiringItems + $this->_getAcquiringNames($company);
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_CARD)
                    $this->cardItems = $this->cardItems + $this->_getCardNames($company);
                //
                if (!$onlyWallet || $onlyWallet == CashFlowsBase::WALLET_BANK)
                    $this->_bankRs2Id = $this->_bankRs2Id + $this->_getRs2Id($company);
            }
        }
    }

    private function _getBankNames(Company $company)
    {
        return $company->getCheckingAccountants()->select('name')->orderBy(['type' => SORT_ASC, 'name' => SORT_ASC])->indexBy('rs')->column();
    }

    private function _getEmoneyNames(Company $company)
    {
        return $company->getEmoneys()->select('name')->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])->indexBy('id')->column();
    }

    private function _getCashboxNames(Company $company)
    {
        return $company->getCashboxes()->select('name')->orderBy(['is_main' => SORT_DESC, 'name' => SORT_ASC])->indexBy('id')->column();
    }

    private function _getAcquiringNames(Company $company)
    {
        return Acquiring::find()->where(['company_id' => $company->id])->select(new Expression('CONCAT(CASE WHEN type=0 THEN "Монета" WHEN type=1 THEN "ЮKassa" END, " (",identifier ,")")'))->orderBy('type')->indexBy('identifier')->column();
    }

    private function _getCardNames(Company $company)
    {
        return CardAccount::find()->where(['company_id' => $company->id])->select(new Expression('CONCAT("Дзен мани", " (", identifier, ")")'))->orderBy('identifier')->indexBy('id')->column();
    }

    private function _getRs2Id(Company $company)
    {
        return $company->getCheckingAccountants()->select('id')->orderBy(['type' => SORT_ASC, 'bank_name' => SORT_ASC])->indexBy('rs')->column();
    }
}