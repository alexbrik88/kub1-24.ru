<?php
namespace frontend\modules\cash\models\cashSearch;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\db\Expression;
use yii\db\Query;
use frontend\modules\cash\models\CashContractorType;

trait ChartTrait {

    /**
     * @param array $dates
     * @param int $purse
     * @param string $period
     * @return array
     */
    public function getPlanFactSeriesData($dates, $purse = null, $period = "days")
    {
        $resultFact = [];
        $resultPlan = [];
        $resultBalance = [];
        $resultCurrMonthPlanBalance = 0;

        $INCOME = CashFlowsBase::FLOW_TYPE_INCOME;
        $OUTCOME = CashFlowsBase::FLOW_TYPE_EXPENSE;

        $dateStart = $dates[0]['from'];
        $dateEnd = $dates[count($dates)-1]['to'];

        // 0. Zeroes
        $dateKeyLength = ($period == "months") ? 7 : 10;
        foreach ($dates as $date) {

            $pos = substr($date['from'], 0, $dateKeyLength);

            $resultFact[$INCOME][$pos] = 0;
            $resultFact[$OUTCOME][$pos] = 0;
            $resultPlan[$INCOME][$pos] = 0;
            $resultPlan[$OUTCOME][$pos] = 0;
            $resultBalance[$pos] = 0;
        }

        // 1. Fact
        $flows = $this->_getChartFlowsFact($dateStart, $dateEnd);
        foreach ($flows as $f) {
            $pos = substr($f['date'], 0, $dateKeyLength);
            $resultFact[$f['flow_type']][$pos] += round($f['amount'] / 100, 2);
        }

        // 2. Plan
        $flows = $this->_getChartFlowsPlan($dateStart, $dateEnd);
        foreach ($flows as $f) {
            $pos = substr($f['date'], 0, $dateKeyLength);
            $resultPlan[$f['flow_type']][$pos] += round($f['amount'] / 100, 2);

            // curr month (future days) plan sum
            if ("months" == $period
                && date('Ym', strtotime($f['date'])) == date('Ym')
                && date('d', strtotime($f['date'])) >= date('d')
            ) {
                $resultCurrMonthPlanBalance += round(($f['flow_type'] == $INCOME ? 1 : -1) * $f['amount'] / 100, 2);
            }
        }

        // 3. Balance
        $factStartAmount = ($_tmp = (clone $this->_filterQueryFullPeriod)
            ->andWhere(['<', 'date', $dateStart])
            ->andWhere(['not', ['wallet_id' => 'plan']])
            ->sum(new Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END'))) ? round($_tmp / 100, 2) : 0;

        $keys = array_keys($resultBalance);
        foreach ($dates as $date) {
            $pos = substr($date['from'], 0, $dateKeyLength);
            $resultBalance[$pos] += ($resultFact[$INCOME][$pos] - $resultFact[$OUTCOME][$pos]);
            if (array_search($pos, $keys) == 0)
                $resultBalance[$pos] += $factStartAmount;
            else
                $resultBalance[$pos] += $resultBalance[$keys[array_search($pos, $keys)-1]];
        }

        return [
            'incomeFlowsFact' => array_values($resultFact[$INCOME]),
            'outcomeFlowsFact' => array_values($resultFact[$OUTCOME]),
            'incomeFlowsPlan' => array_values($resultPlan[$INCOME]),
            'outcomeFlowsPlan' => array_values($resultPlan[$OUTCOME]),
            'balanceFact' => array_values($resultBalance),
            'balancePlanCurrMonth' => $resultCurrMonthPlanBalance
        ];
    }

    /**
     * @param $toDate
     * @param null $purse
     * @return float
     * @throws \yii\base\ErrorException
     */
    function getChartPlanBalanceFuturePoint($toDate, $purse = null)
    {
        $factStartAmount = (clone $this->_filterQueryFullPeriod)
            ->andWhere(['<', 'date', date('Y-m-d')])
            ->andWhere(['not', ['wallet_id' => 'plan']])
            ->sum(new Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END'));

        /** @var $planCashFlow $planCashFlow */
        $t = \frontend\modules\reports\models\PlanCashFlows::tableName();
        $i = CashFlowsBase::FLOW_TYPE_INCOME;
        $select = "IF({{{$t}}}.[[flow_type]] = {$i}, {{{$t}}}.[[amount]], - {{{$t}}}.[[amount]])";
        $query = PlanCashFlows::find()
            ->select(['flow_sum' => $select])
            ->where(['company_id' => $this->_companiesIds])
            ->andFilterWhere(['payment_type' => $purse]) // !
            ->andWhere(['>=', $t.'.date', date('Y-m-d')])
            ->andWhere(['<=', $t.'.date', $toDate]);

        $planStartAmount = (new Query)->from(['flow' => $query])->sum('flow_sum');

        return round(($factStartAmount + $planStartAmount) / 100, 2);
    }

    /**
     * @param bool $name
     * @return array
     */
    public function getTooltipStructureByContractors($dateStart, $dateEnd, $purse = null, $flowType = 0)
    {
        $amountsFact = [];
        $amountsPlan = [];
        $countsFact = [];
        $countsPlan = [];
        $items = [];

        // 1. Fact
        $flows = $this->_getChartFlowsFact($dateStart, $dateEnd);
        foreach ($flows as $f) {

            if ($f['flow_type'] != $flowType)
                continue;

            $DATE = $f['date'];
            if (!isset($amountsFact[$DATE])) {
                $amountsFact[$DATE] = [];
                $countsFact[$DATE] = [];
            }
            if (!isset($amountsFact[$DATE][$f['contractor_id']])) {
                $amountsFact[$DATE][$f['contractor_id']] = 0;
                $countsFact[$DATE][$f['contractor_id']] = 0;
            }
            $amountsFact[$DATE][$f['contractor_id']] += $f['amount'];
            $countsFact[$DATE][$f['contractor_id']] += 1;
        }

        // 2. Plan
        $flows = $this->_getChartFlowsPlan($dateStart, $dateEnd);
        foreach ($flows as $f) {

            if ($f['flow_type'] != $flowType)
                continue;

            $DATE = $f['date'];
            if (!isset($amountsPlan[$DATE])) {
                $amountsPlan[$DATE] = [];
                $countsPlan[$DATE] = [];
            }
            if (!isset($amountsPlan[$DATE][$f['contractor_id']])) {
                $amountsPlan[$DATE][$f['contractor_id']] = 0;
                $countsPlan[$DATE][$f['contractor_id']] = 0;
            }
            $amountsPlan[$DATE][$f['contractor_id']] += $f['amount'];
            $countsPlan[$DATE][$f['contractor_id']] += 1;
        }

        // 3. Names + merge amounts
        foreach ($amountsFact as $date => $itemsAmounts) {
            $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                $items[$date][$itemId] = [
                    'date' => $date,
                    'name' => $this->_getChartContractorName($itemId),
                    'amountFact' => round($amount / 100, 2),
                    'amountPlan' => 0,
                    'countFact' => $countsFact[$date][$itemId],
                    'countPlan' => 0
                ];
            }
        }
        foreach ($amountsPlan as $date => $itemsAmounts) {
            if (!isset($items[$date]))
                $items[$date] = [];
            foreach ($itemsAmounts as $itemId => $amount) {
                if (!isset($items[$date][$itemId])) {
                    $items[$date][$itemId] = [
                        'date' => $date,
                        'name' => $this->_getChartContractorName($itemId),
                        'amountFact' => 0,
                        'amountPlan' => 0,
                        'countFact' => 0,
                        'countPlan' => 0
                    ];
                }
                $items[$date][$itemId]['amountPlan'] = round($amount / 100, 2);
                $items[$date][$itemId]['countPlan'] = $countsPlan[$date][$itemId];
            }
        }

        foreach ($items as $date => &$itemsByDay) {
            uasort($itemsByDay, function ($a, $b) {
                if ($a['amountFact'] == $b['amountFact'])
                    return $a['amountPlan'] > $b['amountPlan'] ? -1 : 1;

                return ($a['amountFact'] > $b['amountFact']) ? -1 : 1;
            });
        }

        return $items;
    }

    private function _getChartFlowsFact($dateStart, $dateEnd)
    {
        $hash = md5($dateStart, $dateEnd);
        if (!isset($this->_chartDataFact[$hash])) {
            $this->_chartDataFact[$hash] =
                (clone $this->_filterQueryFullPeriod)
                    ->andWhere(['between', 'date', $dateStart, $dateEnd])
                    ->andWhere(['not', ['wallet_id' => 'plan']])
                    ->select(['amount', 'flow_type', 'date', 'contractor_id', 'is_internal_transfer', 'wallet_id'])
                    ->all();
        }

        return $this->_chartDataFact[$hash];
    }

    private function _getChartFlowsPlan($dateStart, $dateEnd)
    {
        $hash = md5($dateStart, $dateEnd);
        if (!isset($this->_chartDataPlan[$hash])) {
            $this->_chartDataPlan[$hash] =
                (clone $this->_filterQueryFullPeriod)
                    ->andWhere(['between', 'date', $dateStart, $dateEnd])
                    ->andWhere(['wallet_id' => 'plan'])
                    ->select(['amount', 'flow_type', 'date', 'contractor_id', 'is_internal_transfer', 'wallet_id'])
                    ->all();
        }

        return $this->_chartDataPlan[$hash];
    }

    private function _getChartContractorName($id)
    {
        if ($id > 0 && ($itemModel = Contractor::findOne($id)))
            return $itemModel->getTitle(true);
        elseif ($id == CashContractorType::CUSTOMERS_TEXT)
            return 'Физ. лица';
        elseif ($id == CashContractorType::BALANCE_TEXT)
            return 'Баланс начальный';
        elseif ($id == CashContractorType::COMPANY_TEXT)
            return '';
        elseif (in_array($id, [CashContractorType::BANK_TEXT, CashContractorType::EMONEY_TEXT, CashContractorType::ORDER_TEXT]))
            return 'Перевод м.с.';

        return '---';
    }

    public function getChartSubTitle($declination = false)
    {
        $wallets = [];
        $walletsNames = [];

        if ($this->widget_wallet_id) {
            $wallets = [$this->widget_wallet_id];
        }
        elseif ($this->wallet_ids) {
            $wallets = $this->wallet_ids;
        }

        if ($wallets) {
            foreach ($wallets as $walletId) {
                if (strpos($walletId, '_') !== false) {
                    @list($cashBlock, $currentWalletId) = explode('_', $walletId);
                    switch ($cashBlock) {
                        case self::WALLET_BANK:
                            $walletsNames[] = ArrayHelper::getValue($this->bankItems, $currentWalletId);
                            break;
                        case self::WALLET_ORDER:
                            $walletsNames[] = ArrayHelper::getValue($this->cashboxItems, $currentWalletId);
                            break;
                        case self::WALLET_EMONEY:
                            $walletsNames[] = ArrayHelper::getValue($this->emoneyItems, $currentWalletId);
                            break;
                    }
                } else {
                    $cashBlock = $walletId;
                    $currentWalletId = null;
                    switch ($cashBlock) {
                        case self::WALLET_BANK:
                            $walletsNames[] = 'БАНК';
                            break;
                        case self::WALLET_ORDER:
                            $walletsNames[] = 'КАССА';
                            break;
                        case self::WALLET_EMONEY:
                            $walletsNames[] = 'E-MONEY';
                            break;
                    }
                }
            }

            $walletsNames = array_filter($walletsNames);
        }

        return (implode(', ', $walletsNames) ?: ($declination ? 'ВСЕМ СЧЕТАМ' : 'ВСЕХ СЧЕТАХ'));
    }

}