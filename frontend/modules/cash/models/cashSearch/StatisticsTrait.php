<?php
namespace frontend\modules\cash\models\cashSearch;

use common\components\date\DateHelper;
use yii\db\Expression;

trait StatisticsTrait {

    public function isPlan()
    {
        return ($this->operation_type == self::OPERATION_TYPE_PLAN);
    }

    public function calcStatistics()
    {
        $startDate = DateHelper::format($this->periodStartDate, 'Y-m-d', 'd.m.Y');
        $endDate = DateHelper::format($this->periodEndDate, 'Y-m-d', 'd.m.Y');
            
        return (clone $this->_filterQueryFullPeriod)->select(
            new Expression('
                SUM(IF (t.date < "'.$startDate.'", IF (t.flow_type = 0, -amount, amount), 0)) AS balanceAtBegin,
                SUM(IF (t.date <= "'.$endDate.'", IF (t.flow_type = 0, -amount, amount), 0)) AS balanceAtEnd,
                SUM(IF (t.date >= "'.$startDate.'", IF (t.flow_type = 0, 0, amount), 0)) AS totalIncome,
                SUM(IF (t.date >= "'.$startDate.'", IF (t.flow_type = 0, amount, 0), 0)) AS totalExpense,
                SUM(IF (t.date >= "'.$startDate.'" AND t.flow_type = 0, 0, 1)) AS countIncome,  
                SUM(IF (t.date >= "'.$startDate.'" AND t.flow_type = 0, 1, 0)) AS countExpense
            '))
            ->andWhere(['<=', 'date', $endDate])
            ->andWhere($this->isPlan() ?
                ['wallet_id' => 'plan'] :
                ['not', ['wallet_id' => 'plan']]
            )->one();
    }

}