<?php
namespace frontend\modules\cash\models\cashSearch;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\modules\cash\models\CashBankSearch;
use frontend\modules\cash\models\CashSearch;
use frontend\rbac\permissions\Service;
use Yii;
use frontend\modules\cash\models\CashContractorType;
use yii\db\Expression;

trait FilterItemsTrait {

    public $operation_type;
    public $wallet_ids;
    public $contractor_ids;
    public $reason_ids;
    public $contractor_name;
    public $payment_priority;
    public $project_id;
    public $sale_point_id;
    public $industry_id;

    public $contractor_name_2;
    public $description_2;
    public $amount_type_2;
    public $amount_2;
    public $flow_type_2;

    private function setFilters($query, $tableAlias = 't')
    {
        if (strlen($this->flow_type) && $this->flow_type > -1) {
            if ($this->flow_type == CashFlowsBase::INTERNAL_TRANSFER) {
                $query->andWhere([$tableAlias . '.is_internal_transfer' => true]);
            } else {
                $query->andWhere([$tableAlias . '.flow_type' => $this->flow_type]);
            }
        }

        if ($this->contractor_ids) {
            $query->andWhere([$tableAlias . '.contractor_id' => $this->contractor_ids]);
        }
        if ($this->contractor_name) {
            $query->andWhere([$tableAlias . '.contractor_id' => $this->_getContractorFilterIdsByName($this->contractor_name)]);
        }
        if ($this->company_id) {
            $query->andWhere([$tableAlias . '.company_id' => $this->company_id]);
        }

        if ($this->reason_ids) {
            $incomeItems = [];
            $expenditureItems = [];
            $emptyItems = false;
            foreach ((array)$this->reason_ids as $reason_id) {
                if ($reason_id == 'empty') {
                    $emptyItems = true;
                } elseif (substr($reason_id, 0, 2) === 'i_') {
                    $incomeItems[] = substr($reason_id, 2);
                }
                elseif (substr($reason_id, 0, 2) === 'e_') {
                    $expenditureItems[] = substr($reason_id, 2);
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $childrenFlowsIds = null;
            if ($this instanceof CashBankSearch) {
                $childrenFlowsIds = CashBankFlows::find()
                    ->select('parent_id')
                    ->andWhere(['company_id' => $this->_companiesIds ?? $this->company_id]) // todo: CashSearch ?? CashBankSearch
                    ->andWhere(['not', ['parent_id' => null]])
                    ->andFilterWhere(['or',
                        ['income_item_id' => $incomeItems],
                        ['expenditure_item_id' => $expenditureItems]
                    ])
                    ->column();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $query->andFilterWhere(['or',
                [$tableAlias . '.income_item_id' => $incomeItems],
                [$tableAlias . '.expenditure_item_id' => $expenditureItems],
                ($emptyItems) ? ['or',
                    ['and', [$tableAlias . '.flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE], new Expression($tableAlias . '.expenditure_item_id IS NULL')],
                    ['and', [$tableAlias . '.flow_type' => CashFlowsBase::FLOW_TYPE_INCOME], new Expression($tableAlias . '.income_item_id IS NULL')],
                ] : [],
                [$tableAlias . '.id' => $childrenFlowsIds]
            ]);
        }

        if ($this->payment_priority) {
            $query->andWhere(['payment_priority' => $this->payment_priority]);
        }

        if ($this->operation_type) {
            if ($this->operation_type == CashSearch::OPERATION_TYPE_FACT) {
                $query->andWhere(['not', [$tableAlias . '.wallet_id' => 'plan']]);
            }
            if ($this->operation_type == CashSearch::OPERATION_TYPE_PLAN) {
                $query->andWhere([$tableAlias . '.wallet_id' => 'plan']);
            }
        }

        if (property_exists($this, 'project_id') && strlen($this->project_id)) {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $childrenFlowsIds = null;
            if ($this instanceof CashBankSearch) {
                $childrenFlowsIds = CashBankFlows::find()
                    ->select('parent_id')
                    ->andWhere(['company_id' => $this->_companiesIds ?? $this->company_id]) // todo: CashSearch ?? CashBankSearch
                    ->andWhere(['not', ['parent_id' => null]])
                    ->andWhere(['project_id' => $this->project_id ?: null])
                    ->column();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $query->andWhere(['or',
                [$tableAlias . '.project_id' => $this->project_id ?: null],
                [$tableAlias . '.id' => $childrenFlowsIds]
            ]);
        }

        if (property_exists($this, 'sale_point_id') && strlen($this->sale_point_id)) {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $childrenFlowsIds = null;
            if ($this instanceof CashBankSearch) {
                $childrenFlowsIds = CashBankFlows::find()
                    ->select('parent_id')
                    ->andWhere(['company_id' => $this->_companiesIds ?? $this->company_id]) // todo: CashSearch ?? CashBankSearch
                    ->andWhere(['not', ['parent_id' => null]])
                    ->andWhere(['sale_point_id' => $this->sale_point_id ?: null])
                    ->column();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $query->andWhere(['or',
                [$tableAlias . '.sale_point_id' => $this->sale_point_id ?: null],
                [$tableAlias . '.id' => $childrenFlowsIds]
            ]);
        }

        if (property_exists($this, 'industry_id') && strlen($this->industry_id)) {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $childrenFlowsIds = null;
            if ($this instanceof CashBankSearch) {
                $childrenFlowsIds = CashBankFlows::find()
                    ->select('parent_id')
                    ->andWhere(['company_id' => $this->_companiesIds ?? $this->company_id]) // todo: CashSearch ?? CashBankSearch
                    ->andWhere(['not', ['parent_id' => null]])
                    ->andWhere(['industry_id' => $this->industry_id ?: null])
                    ->column();
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $query->andWhere(['or',
                [$tableAlias . '.industry_id' => $this->industry_id ?: null],
                [$tableAlias . '.id' => $childrenFlowsIds]
            ]);
        }

        if (property_exists($this, 'flow_type_2')) {
            if (strlen($this->flow_type_2) && $this->flow_type_2 > -1) {
                $query->andWhere([$tableAlias . '.flow_type' => $this->flow_type_2]);
            }
            if ($this->contractor_name_2) {
                $query->andWhere([$tableAlias . '.contractor_id' => $this->_getContractorFilterIdsByName($this->contractor_name_2)]);
            }
            if ($this->description_2) {
                $query->andWhere(['like', $tableAlias . '.description', $this->description_2]);
            }
            if (strlen($this->amount_2)) {
                $this->amount_2 = (float)str_replace(',','.', trim($this->amount_2));
                if ($this->amount_2 > 0) {
                    switch ($this->amount_type_2) {
                        case self::FILTER_AMOUNT_TYPE_LESS_THAN:
                            $query->andWhere(['<', $tableAlias . '.amount', 100 * $this->amount_2]);
                            break;
                        case self::FILTER_AMOUNT_TYPE_MORE_THAN:
                            $query->andWhere(['>', $tableAlias . '.amount', 100 * $this->amount_2]);
                            break;
                        case self::FILTER_AMOUNT_TYPE_EQUAL:
                        default:
                            $query->andWhere(['=', $tableAlias . '.amount', 100 * $this->amount_2]);
                            break;
                    }
                }
            }
            // todo
        }

        return $query;
    }

    public function getWalletFilterItems()
    {
        $showBanks = Yii::$app->user->can(Service::HOME_CASH);

        $query = clone ($this->_filterQuery);

        $currentWalletIdArray = $query
            ->distinct()
            ->select("wallet_id")
            ->orderBy("wallet_id")
            ->column();

        // add selected by user
        $currentWalletIdArray = array_unique(array_merge($currentWalletIdArray, (array)$this->wallet_ids));

        $ret = ['' => 'Все счета ' . $this->currency_name];

        $_addedCashblocks = [];

        foreach ($currentWalletIdArray as $walletId) {
            @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            switch ($cashBlock) {
                case CashSearch::WALLET_BANK:
                    $cashBlockName = 'Банк';
                    $name = ArrayHelper::getValue($this->bankItems, $currentWalletId);
                    if (!$showBanks) continue 2;
                    break;
                case CashSearch::WALLET_ORDER:
                    $cashBlockName = 'Касса';
                    $name = ArrayHelper::getValue($this->cashboxItems, $currentWalletId);
                    break;
                case CashSearch::WALLET_EMONEY:
                    $cashBlockName = 'E-money';
                    $name = ArrayHelper::getValue($this->emoneyItems, $currentWalletId);
                    break;
                case CashSearch::WALLET_ACQUIRING:
                    $cashBlockName = 'Эквайринг';
                    $name = ArrayHelper::getValue($this->acquiringItems, $currentWalletId);
                    break;
                case CashSearch::WALLET_CARD:
                    $cashBlockName = 'Карты';
                    $name = ArrayHelper::getValue($this->cardItems, $currentWalletId);
                    break;
                default:
                    continue 2;
                    break;
            }
            if (!in_array($cashBlock, $_addedCashblocks)) {
                $ret[$cashBlock] = $cashBlockName;
            }
            if ($name) {
                $ret[$walletId] = $name;
            }
        }

        return $ret;
    }

    public function getContractorFilterItems()
    {
        $query = clone ($this->_filterQuery);

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("contractor.id")
            ->column();

        // add selected by user
        $contractorIdArray = array_unique(array_merge($contractorIdArray, (array)$this->contractor_ids));

        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->andWhere(["not", ["$tCashContractor.name" => ['bank', 'order', 'emoney']]])
                ->all(),
            'name',
            'text'
        );

        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return $cashContractorArray + $contractorArray;
    }

    public function getReasonFilterItems()
    {
        $query = clone ($this->_filterQuery);

        $_selectedExpenditureItems = [];
        $_selectedIncomeItems = [];

        foreach ((array)$this->reason_ids as $reason_id) {
            if (substr($reason_id, 0, 2) === 'i_') {
                $_selectedIncomeItems[] = (int)substr($reason_id, 2);
            }
            elseif (substr($reason_id, 0, 2) === 'e_') {
                $_selectedExpenditureItems[] = (int)substr($reason_id, 2);
            }
        }

        $e_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("e_", `expenditure`.`id`) `reason_id`,
                IF (LENGTH(`expenditureParent`.`name`) > 0, CONCAT(`expenditureParent`.`name`, " - ", `expenditure`.`name`), `expenditure`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditureParent`', '`expenditureParent`.`id` = `expenditure`.`parent_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->orFilterWhere(['expenditure.id' => $_selectedExpenditureItems]) // add selected by user
            ->createCommand()
            ->queryAll();

        $query = clone ($this->_filterQuery);
        $i_reasonArray = $query
            ->distinct()
            ->select(new Expression('
                CONCAT("i_", `income`.`id`) `reason_id`,
                IF (LENGTH(`incomeParent`.`name`) > 0, CONCAT(`incomeParent`.`name`, " - ", `income`.`name`), `income`.`name`) `reason_name`
            '))
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `incomeParent`', '`incomeParent`.`id` = `income`.`parent_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->orFilterWhere(['income.id' => $_selectedIncomeItems]) // add selected by user
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    public function getProjectFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $projectArray = $query
            ->distinct()
            ->select('`project_id`, `project`.`name` `project_name`')
            ->leftJoin(Project::tableName() . ' `project`', '`project`.`id` = `project_id`')
            ->orderBy('project_name')
            ->andWhere(['not', ['project_id' => null]])
            ->createCommand()
            ->queryAll();

        return ['0' => 'Без проекта'] + ArrayHelper::map($projectArray, 'project_id', 'project_name');
    }

    public function getSalePointFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $salePointArray = $query
            ->distinct()
            ->select('`sale_point_id`, `sale_point`.`name` `sale_point_name`')
            ->leftJoin(SalePoint::tableName() . ' `sale_point`', '`sale_point`.`id` = `sale_point_id`')
            ->orderBy('sale_point_name')
            ->andWhere(['not', ['sale_point_id' => null]])
            ->createCommand()
            ->queryAll();

        return ['0' => 'Без точки продаж'] + ArrayHelper::map($salePointArray, 'sale_point_id', 'sale_point_name');
    }

    public function getCompanyIndustryFilterItems()
    {
        $query = clone ($this->_filterQuery);
        $salePointArray = $query
            ->distinct()
            ->select('`industry_id`, `company_industry`.`name` `company_industry_name`')
            ->leftJoin(CompanyIndustry::tableName() . ' `company_industry`', '`company_industry`.`id` = `industry_id`')
            ->orderBy('company_industry_name')
            ->createCommand()
            ->queryAll();

        return ['0' => 'Без направления'] + ArrayHelper::map($salePointArray, 'industry_id', 'company_industry_name');
    }    

    public function getFlowTypeFilterItems()
    {
        return [
            -1 => 'Все',
            CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
            CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
            CashFlowsBase::INTERNAL_TRANSFER => 'Между счетами'
        ];
    }

    public function getOperationTypeFilterItems()
    {
        return ($this->_userConfigShowPlan) ?
        [
            0 => 'Все',
            CashSearch::OPERATION_TYPE_FACT => 'Факт',
            CashSearch::OPERATION_TYPE_PLAN => 'План',
        ] :
        [
            0 => 'Факт',
            CashSearch::OPERATION_TYPE_PLAN => 'План',
        ];
    }

    private function _getStartDate() {
        return DateHelper::format($this->periodStartDate, 'Y-m-d', 'd.m.Y');
    }

    private function _getEndDate() {
        return DateHelper::format($this->periodEndDate, 'Y-m-d', 'd.m.Y');
    }

    private function _getContractorFilterIdsByName($name)
    {
        $contractorIdArray = Contractor::find()
            ->select("id")
            ->andWhere(['company_id' => $this->_companiesIds ?? $this->company_id]) // todo: CashSearch ?? CashBankSearch
            ->byName($name)
            ->column();

        return $contractorIdArray ?: [-1];
    }
}