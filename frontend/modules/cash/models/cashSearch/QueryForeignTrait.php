<?php
namespace frontend\modules\cash\models\cashSearch;

use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;

trait QueryForeignTrait {


    private function getForeignWalletsQuery($currencyName, $walletsList = [])
    {
        if (empty($walletsList)) {

            $pcf = $this->_getForeignPlanQuery($currencyName);

            if ($this->isPlan()) {
                return (new Query)->from(['t' => $pcf]);
            }

            $cbf = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_BANK, [], $currencyName);
            $cof = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_ORDER, [], $currencyName);
            $cef = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_EMONEY, [], $currencyName);

            return (new Query)->from(['t' => $cbf->union($cef, true)->union($cof, true)->union($pcf, true)]);
        }

        $selectedWallets = [];
        foreach ($walletsList as $walletId) {
            if (strpos($walletId, '_') !== false) {
                @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            } else {
                $cashBlock = $walletId;
                $currentWalletId = null;
            }

            $selectedWallets[$cashBlock][] = $currentWalletId ? ("'{$currentWalletId}'") : null;
        }

        $cbf = $cef = $cof = null;
        foreach ($selectedWallets as $walletId => $wallets) {
            switch ($walletId) {
                case self::WALLET_FOREIGN_BANK:
                    $cbf = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_BANK, implode(',', $wallets), $currencyName);
                    break;
                case self::WALLET_FOREIGN_ORDER:
                    $cof = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_ORDER, implode(',', $wallets), $currencyName);
                    break;
                case self::WALLET_FOREIGN_EMONEY:
                    $cef = $this->_getForeignFlowQuery(self::WALLET_FOREIGN_EMONEY, implode(',', $wallets), $currencyName);
                    break;
            }
        }

        if ($cbf && $cef && $cof) {
            $subQuery = $cbf->union($cef, true)->union($cof, true);
        } elseif ($cbf && $cef) {
            $subQuery = $cbf->union($cef, true);
        } elseif ($cbf && $cof) {
            $subQuery = $cbf->union($cof, true);
        } elseif ($cef && $cof) {
            $subQuery = $cef->union($cof, true);
        } elseif ($cbf) {
            $subQuery = $cbf;
        } elseif ($cof) {
            $subQuery = $cof;
        } elseif ($cef) {
            $subQuery = $cef;
        } else {
            throw new Exception('Wallet not found');
        }

        $planQuery = $this->_getForeignPlanQuery($currencyName, $selectedWallets);

        return (new Query)->from(['t' => $subQuery->union($planQuery, true)]);
    }

    private function _getForeignFlowQuery($cashBlock, $walletIDs, $currencyName)
    {
        $currentWalletCondition = new Expression('1 = 1');
        $currentCurrencyCondition = new Expression('t.currency_name = "' . $currencyName . '"'); // todo: add db index (company_id, currency_name)

        if ($cashBlock == self::WALLET_FOREIGN_BANK) {
            $className = CashBankForeignCurrencyFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "rs IN ($walletIDs)";
            }
        }
        elseif ($cashBlock == self::WALLET_FOREIGN_ORDER) {
            $className = CashOrderForeignCurrencyFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "cashbox_id IN ($walletIDs)";
            }
        }
        elseif ($cashBlock == self::WALLET_FOREIGN_EMONEY) {
            $className = CashEmoneyForeignCurrencyFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "emoney_id IN ($walletIDs)";
            }
        } else {
            throw new \Exception('Invalid wallet.');
        }

        /* @var $className CashBankForeignCurrencyFlows|CashOrderForeignCurrencyFlows|CashEmoneyForeignCurrencyFlows */
        $tableName = $className::tableName();

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) as amountIncome",
                $cashBlock == self::WALLET_FOREIGN_BANK ? 't.rs' : new Expression('null as rs'),
                $cashBlock == self::WALLET_FOREIGN_ORDER ? 't.cashbox_id' : new Expression('null as cashbox_id'),
                $cashBlock == self::WALLET_FOREIGN_EMONEY ? 't.emoney_id' : new Expression('null as emoney_id'),
                'null AS [[acquiring_id]]',
                'null AS [[card_id]]',
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                new Expression('NULL AS project_id'),
                't.sale_point_id',
                't.industry_id',
                new Expression(
                    ($cashBlock == self::WALLET_FOREIGN_BANK ? 'CONCAT("1_", t.rs) AS wallet_id' : (
                        $cashBlock == self::WALLET_FOREIGN_ORDER ? 'CONCAT("2_", t.cashbox_id) AS wallet_id' : (
                            $cashBlock == self::WALLET_FOREIGN_EMONEY ? 'CONCAT("3_", t.emoney_id) AS wallet_id' :
                                'null AS wallet_id'
                            )
                        )
                    )
                ),
                't.is_internal_transfer',
                new Expression('IF (flow_type = 0, 
                    CONCAT(id, "_", IFNULL(internal_transfer_flow_id, contractor_id)), 
                    CONCAT(IFNULL(internal_transfer_flow_id, contractor_id), "_", id)
                ) AS transfer_key'),
                new Expression('NULL AS has_tin_children')
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition)
            ->andWhere($currentCurrencyCondition);
    }

    private function _getForeignPlanQuery($currencyName, $selectedWallets = [])
    {
        $className = PlanCashForeignCurrencyFlows::class;
        $tableName = $className::tableName();

        $_condition = [];

        foreach ($selectedWallets as $walletId => $wallets) {
            $wallets = array_filter($wallets);
            switch ($walletId) {
                case self::WALLET_FOREIGN_BANK:
                    $wallets = array_map(function($rs) { return (int)$this->getBankIdByRs(trim($rs, "'")); }, $wallets);
                    $_condition[] = 'payment_type = ' . $walletId . (!empty($wallets) ? (' AND checking_accountant_id IN ('.implode(',', $wallets).')') : '');
                    break;
                case self::WALLET_FOREIGN_ORDER:
                    $wallets = array_map(function($rs) { return (int)trim($rs, "'"); }, $wallets);
                    $_condition[] = 'payment_type = ' . $walletId . (!empty($wallets) ? (' AND cashbox_id IN ('.implode(',', $wallets).')') : '');
                    break;
                case self::WALLET_FOREIGN_EMONEY:
                    $wallets = array_map(function($rs) { return (int)trim($rs, "'"); }, $wallets);
                    $_condition[] = 'payment_type = ' . $walletId . (!empty($wallets) ? (' AND emoney_id IN ('.implode(',', $wallets).')') : '');
                    break;
            }
        }

        $currentWalletCondition = $_condition ? ('('.implode(' OR ', $_condition).')') : '1 = 1';
        $currentCurrencyCondition = new Expression('t.currency_name = "' . $currencyName . '"'); // todo: add db index (company_id, currency_name)

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" as tb'),
                "IF({{t}}.[[flow_type]] = 0, {{t}}.[[amount]], 0) as amountExpense",
                "IF({{t}}.[[flow_type]] = 1, {{t}}.[[amount]], 0) as amountIncome",
                '{{t}}.[[checking_accountant_id]] as rs',
                't.cashbox_id',
                't.emoney_id',
                'null AS [[acquiring_id]]',
                'null AS [[card_id]]',
                't.created_at',
                't.date',
                't.date AS recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                '"plan" as [[wallet_id]]',
                't.is_internal_transfer',
                new Expression('IF (flow_type = 0, CONCAT("PLAN_", id, "_C"), CONCAT("PLAN_", "C_", id)) AS transfer_key'),
                new Expression('NULL AS has_tin_children')
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition)
            ->andWhere($currentCurrencyCondition);
    }
}