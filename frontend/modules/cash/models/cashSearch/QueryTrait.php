<?php
namespace frontend\modules\cash\models\cashSearch;

use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\PlanCashFlows;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;

trait QueryTrait {


    private function getWalletsQuery($walletsList = [])
    {
        if (empty($walletsList)) {

            $pcf = $this->_getPlanQuery();

            if ($this->isPlan()) {
                return (new Query)->from(['t' => $pcf]);
            }

            $cbf = $this->_getFlowQuery(self::WALLET_BANK);
            $cef = $this->_getFlowQuery(self::WALLET_EMONEY);
            $cof = $this->_getFlowQuery(self::WALLET_ORDER);
            $acq = $this->_getAcquiringQuery();
            $card = $this->_getCardQuery();

            return (new Query)->from([
                't' => $cbf
                    ->union($cef, true)
                    ->union($cof, true)
                    ->union($pcf, true)
                    ->union($acq, true)
                    ->union($card, true)
            ]);
        }

        $selectedWallets = [];
        foreach ($walletsList AS $walletId) {
            if (strpos($walletId, '_') !== false) {
                @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            } else {
                $cashBlock = $walletId;
                $currentWalletId = null;
            }

            $selectedWallets[$cashBlock][] = $currentWalletId ? "'{$currentWalletId}'" : null;
        }

//        $cbf = $cef = $cof = null;
//        foreach ($selectedWallets AS $walletId => $wallets) {
//            switch ($walletId) {
//                case self::WALLET_BANK:
//                    $cbf = $this->_getFlowQuery(self::WALLET_BANK, implode(',', $wallets));
//                    break;
//                case self::WALLET_ORDER:
//                    $cof = $this->_getFlowQuery(self::WALLET_ORDER, implode(',', $wallets));
//                    break;
//                case self::WALLET_EMONEY:
//                    $cef = $this->_getFlowQuery(self::WALLET_EMONEY, implode(',', $wallets));
//                    break;
//            }
//        }
//        if ($cbf && $cef && $cof) {
//            $subQuery = $cbf->union($cef, true)->union($cof, true);
//        } elseif ($cbf && $cef) {
//            $subQuery = $cbf->union($cef, true);
//        } elseif ($cbf && $cof) {
//            $subQuery = $cbf->union($cof, true);
//        } elseif ($cef && $cof) {
//            $subQuery = $cef->union($cof, true);
//        } elseif ($cbf) {
//            $subQuery = $cbf;
//        } elseif ($cof) {
//            $subQuery = $cof;
//        } elseif ($cef) {
//            $subQuery = $cef;
//        } else {
//            throw new Exception('Wallet not found');
//        }

        $_searchByWalletsTypes = [];
        foreach ($selectedWallets AS $walletId => $wallets) {
            switch ($walletId) {
                case self::WALLET_BANK:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_BANK, implode(',', $wallets));
                    break;
                case self::WALLET_ORDER:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_ORDER, implode(',', $wallets));
                    break;
                case self::WALLET_EMONEY:
                    $_searchByWalletsTypes[] = $this->_getFlowQuery(self::WALLET_EMONEY, implode(',', $wallets));
                    break;
                case self::WALLET_ACQUIRING:
                    $_searchByWalletsTypes[] = $this->_getAcquiringQuery(implode(',', $wallets));
                    break;
                case self::WALLET_CARD:
                    $_searchByWalletsTypes[] = $this->_getCardQuery(implode(',', $wallets));
                    break;
            }
        }

        $subQuery = null;
        foreach ($_searchByWalletsTypes as $q) {
            if (!empty($subQuery))
                $subQuery->union($q, true);
            else
                $subQuery = $q;
        }


        $planQuery = $this->_getPlanQuery($selectedWallets);

        return (new Query)->from(['t' => $subQuery->union($planQuery, true)]);
    }

    private function _getPlanQuery($selectedWallets = [])
    {
        $className = PlanCashFlows::class;
        $tableName = $className::tableName();

        $_condition = [];

        foreach ($selectedWallets AS $walletId => $wallets) {
            $wallets = array_filter($wallets);
            switch ($walletId) {
                case self::WALLET_BANK:
                    $wallets = array_map(function($rs) { return (int)$this->getBankIdByRs(trim($rs, "'")); }, $wallets);
                    $_condition[] = 'payment_type = 1' . (!empty($wallets) ? (' AND checking_accountant_id IN ('.implode(',', $wallets).')') : '');
                    break;
                case self::WALLET_ORDER:
                    $wallets = array_map(function($rs) { return (int)trim($rs, "'"); }, $wallets);
                    $_condition[] = 'payment_type = 2' . (!empty($wallets) ? (' AND cashbox_id IN ('.implode(',', $wallets).')') : '');
                    break;
                case self::WALLET_EMONEY:
                    $wallets = array_map(function($rs) { return (int)trim($rs, "'"); }, $wallets);
                    $_condition[] = 'payment_type = 3' . (!empty($wallets) ? (' AND emoney_id IN ('.implode(',', $wallets).')') : '');
                    break;
            }
        }

        $currentWalletCondition = $_condition ? ('('.implode(' OR ', $_condition).')') : '1 = 1';

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" AS tb'),
                "IF({{t}}.[[flow_type]] = 0, {{t}}.[[amount]], 0) AS amountExpense",
                "IF({{t}}.[[flow_type]] = 1, {{t}}.[[amount]], 0) AS amountIncome",
                '{{t}}.[[checking_accountant_id]] AS rs',
                't.cashbox_id',
                't.emoney_id',
                'null AS [[acquiring_id]]',
                'null AS [[card_id]]',
                't.created_at',
                't.date',
                't.date AS [[recognition_date]]',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                '"plan" AS [[wallet_id]]',
                't.is_internal_transfer',
                new Expression('IF (flow_type = 0, CONCAT("PLAN_", id, "_C"), CONCAT("PLAN_", "C_", id)) AS transfer_key'),
                new Expression('NULL AS has_tin_children')
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition);
    }

    private function _getFlowQuery($cashBlock, $walletIDs = null)
    {
        $currentWalletCondition = $parentIdNullCondition = new Expression('1 = 1');

        if ($cashBlock == self::WALLET_BANK) {
            $className = CashBankFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "rs IN ($walletIDs)";
            }
            $parentIdNullCondition = 'parent_id IS NULL';
        }
        elseif ($cashBlock == self::WALLET_ORDER) {
            $className = CashOrderFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "cashbox_id IN ($walletIDs)";
            }
        }
        elseif ($cashBlock == self::WALLET_EMONEY) {
            $className = CashEmoneyFlows::class;
            if ($walletIDs) {
                $currentWalletCondition = "emoney_id IN ($walletIDs)";
            }
        } else {
            throw new \Exception('Invalid wallet.');
        }

        /* @var $className CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        $tableName = $className::tableName();

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" AS tb'),
                "IF({{t}}.[[flow_type]] = 0 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) AS amountExpense",
                "IF({{t}}.[[flow_type]] = 1 OR {{t}}.[[is_internal_transfer]], {{t}}.[[amount]], 0) AS amountIncome",
                $cashBlock == self::WALLET_BANK ? 't.rs' : 'null AS [[rs]]',
                $cashBlock == self::WALLET_ORDER ? 't.cashbox_id' : 'null AS [[cashbox_id]]',
                $cashBlock == self::WALLET_EMONEY ? 't.emoney_id' : 'null AS [[emoney_id]]',
                'null AS [[acquiring_id]]',
                'null AS [[card_id]]',
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                new Expression(
                    ($cashBlock == self::WALLET_BANK ? 'CONCAT("1_", t.rs) AS wallet_id' : (
                        $cashBlock == self::WALLET_ORDER ? 'CONCAT("2_", t.cashbox_id) AS wallet_id' : (
                            $cashBlock == self::WALLET_EMONEY ? 'CONCAT("3_", t.emoney_id) AS wallet_id' :
                                'null AS wallet_id'
                            )
                        )
                    )
                ),
                't.is_internal_transfer',
                new Expression('IF (flow_type = 0, CONCAT(id, "_", IFNULL(internal_transfer_flow_id, "C")), CONCAT(IFNULL(internal_transfer_flow_id, "C"), "_", id)) AS transfer_key'),
                new Expression(
                    ($cashBlock == self::WALLET_BANK ? 't.has_tin_children' : (
                        $cashBlock == self::WALLET_ORDER ? '0 AS has_tin_children' : (
                            $cashBlock == self::WALLET_EMONEY ? '0 AS has_tin_children' :
                                '0 AS has_tin_children'
                            )
                        )
                    )
                ),
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition)
            ->andWhere($parentIdNullCondition);  // parent_id_null_condition
    }

    private function _getAcquiringQuery($walletIDs = null)
    {
        $className = AcquiringOperation::class;
        $tableName = $className::tableName();

        if ($walletIDs) {
            $currentWalletCondition = "t.acquiring_identifier IN ($walletIDs)";
        } else {
            $currentWalletCondition = '1 = 1';
        }

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" AS tb'),
                "IF({{t}}.[[flow_type]] = 0, {{t}}.[[amount]], 0) AS amountExpense",
                "IF({{t}}.[[flow_type]] = 1, {{t}}.[[amount]], 0) AS amountIncome",
                'null AS [[rs]]',
                'null AS [[cashbox_id]]',
                'null AS [[emoney_id]]',
                't.acquiring_identifier AS [[acquiring_id]]',
                'null AS [[card_id]]',
                'null AS [[created_at]]',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                'CONCAT("4_", t.acquiring_identifier) AS [[wallet_id]]',
                'null AS [[is_internal_transfer]]',
                new Expression('IF (flow_type = 0, CONCAT("ACQUIRING_", id, "_C"), CONCAT("ACQUIRING_", "C_", id)) AS transfer_key'),
                new Expression('NULL AS has_tin_children')
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere($currentWalletCondition);
    }

    private function _getCardQuery($walletIDs = null)
    {
        $className = CardOperation::class;
        $tableName = $className::tableName();

        if ($walletIDs) {
            $currentWalletCondition = "t.account_id IN ($walletIDs)";
        } else {
            $currentWalletCondition = '1 = 1';
        }

        return $className::find()
            ->select([
                't.id',
                't.company_id',
                new Expression('"' . $tableName . '" AS tb'),
                "IF({{t}}.[[flow_type]] = 0, {{t}}.[[amount]], 0) AS amountExpense",
                "IF({{t}}.[[flow_type]] = 1, {{t}}.[[amount]], 0) AS amountIncome",
                'null AS [[rs]]',
                'null AS [[cashbox_id]]',
                'null AS [[emoney_id]]',
                'null AS [[acquiring_id]]',
                't.account_id AS [[card_id]]',
                't.created_at',
                't.date',
                't.recognition_date',
                't.flow_type',
                't.amount',
                't.contractor_id',
                't.description',
                't.expenditure_item_id',
                't.income_item_id',
                't.project_id',
                't.sale_point_id',
                't.industry_id',
                'CONCAT("5_", t.account_id) AS [[wallet_id]]',
                'null AS [[is_internal_transfer]]',
                new Expression('IF (flow_type = 0, CONCAT("CARD_", id, "_C"), CONCAT("CARD_", "C_", id)) AS transfer_key'),
                new Expression('NULL AS has_tin_children')
            ])
            ->from(['t' => $tableName])
            ->andWhere(['t.company_id' => $this->_companiesIds])
            ->andWhere(['t.is_deleted' => 0]) // todo: remove after
            ->andWhere($currentWalletCondition);
    }
}