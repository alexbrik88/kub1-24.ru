<?php
/**
 * Created by konstantin.
 * Date: 1.7.15
 * Time: 15.41
 */

namespace frontend\modules\cash\models;


use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\company\CheckingAccountant;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\components\FilterHelper;
use frontend\modules\cash\models\query\CashPlanFactQuery;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\helpers\Url;

/**
 * Class CashBankSearch
 * @package frontend\modules\cash\models
 */
class CashBankSearch extends CashBankFlows
{
    use FilterItemsTrait;
    use FilterNamesTrait;
    use WalletNamesTrait;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    public $foreign;
    public $currency_name;
    public $currency_id;
    public $bik;
    public $duplicates;
    public $parent_id;

    private $_filterQuery;
    private $_userConfigShowPlan;
    private $_sql;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expenditure_item_id', 'flow_type'], 'integer'],
            [['rs'], 'string'],
            [['contractor_id', 'billPaying', 'bik', 'source'], 'safe'],
            [['foreign', 'currency_id'], 'safe'],
            [['project_id', 'wallet_ids', 'contractor_ids', 'reason_ids', 'contractor_name', 'operation_type'], 'safe'],
            [['sale_point_id', 'industry_id'], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSearchQuery()
    {
        return $this->_filterQuery ? clone $this->_filterQuery : null;
    }

    /**
     *
     */
    public function init()
    {
        $this->initWalletNames($this->company_id, self::WALLET_BANK);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = strlen($this->project_id) ? $this->project_id : null;
        $this->sale_point_id = strlen($this->sale_point_id) ? $this->sale_point_id : null;
        $this->industry_id = strlen($this->industry_id) ? $this->industry_id : null;
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return SqlDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $this->load($params);

        $query = (new CashPlanFactQuery())
            ->alias('cbf')
            ->from(['cbf' => $this->getPlanFactQuery()])
            ->leftJoin('contractor', 'contractor.id = cbf.contractor_id')
            ->select([
                'cbf.id',
                'cbf.company_id',
                'cbf.tb',
                'cbf.rs',
                'cbf.created_at',
                'cbf.payment_order_number',
                'cbf.date',
                'cbf.recognition_date',
                'cbf.flow_type',
                'cbf.amount',
                'cbf.contractor_id',
                'cbf.description',
                'cbf.expenditure_item_id',
                'cbf.income_item_id',
                'cbf.wallet_id',
                'cbf.is_internal_transfer',
                //
                'cbf.amountExpense',
                'cbf.amountIncome',
                'cbf.transfer_key',
                'cbf.project_id',
                'cbf.sale_point_id',
                'cbf.industry_id',
                //
                'cbf.has_tin_children'
            ]);

        if ($this->id) {

            $query->andWhere(['cbf.id' => $this->id]);
            $this->_filterQuery = clone $query;
            $this->_statisticQuery = clone $query;

        } else {

            $query->andFilterWhere([
                'cbf.source' => $this->source,
            ]);

            $this->setFilters($query, 'cbf')
                ->andFilterWhere([
                    'cbf.rs' => ($this->wallet_ids)
                        ? array_map(function($v) { return str_replace(self::WALLET_BANK.'_', '', $v); }, $this->wallet_ids)
                        : str_replace('all', '', $this->rs)
                ]);

            $this->_filterQuery = clone $query;
            $this->_statisticQuery = (clone $query)->andWhere(['not', ['wallet_id' => 'plan']]);

            $query->andWhere(['cbf.parent_id' => null]); // parent_id_null_condition

            $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
            $query->andWhere(['between', 'cbf.date', $dateRange['from'], $dateRange['to']]);

            if ($this->duplicates) {
                $groupBy = new \yii\db\Expression('[[date]], [[amount]], [[contractor_id]], IFNULL([[payment_order_number]], "")');
                $query2 = new \yii\db\Query;
                $fromQuery = clone $query;
                $fromQuery->orderBy = null;
                $query2->select([
                    'date',
                    'amount',
                    'contractor_id',
                    'payment_order_number',
                    'duplicates' => 'COUNT(*)'
                ])->from([
                    't' => $fromQuery,
                ])->groupBy($groupBy)->having('[[duplicates]] > 1');

                $query->innerJoin([
                    'tsum' => $query2,
                ], '
                    {{cbf}}.[[date]] = {{tsum}}.[[date]]
                    AND
                    {{cbf}}.[[amount]] = {{tsum}}.[[amount]]
                    AND
                    {{cbf}}.[[contractor_id]] = {{tsum}}.[[contractor_id]]
                    AND
                    IFNULL({{cbf}}.[[payment_order_number]], "") = IFNULL({{tsum}}.[[payment_order_number]], "")
                ');
            }
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $this->_sql = $query->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'flow_type',
                    'amount',
                    'date',
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    //'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'flow_type' => SORT_DESC,
                    'amount' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    public function getPlanFactQuery()
    {
        $classFact = ($this->foreign)
            ? CashBankForeignCurrencyFlows::class
            : CashBankFlows::class;
        $classPlan = ($this->foreign)
            ? PlanCashForeignCurrencyFlows::class
            : PlanCashFlows::class;

        $factQuery = $planQuery = null;

        if ($this->operation_type == self::OPERATION_TYPE_FACT || empty($this->operation_type)) {

            $factQuery = $classFact::find()
                ->alias('f')
                ->andWhere(['f.company_id' => $this->company_id])
                ->groupBy('f.id');

            $factQuery->addSelect([
                'f.id',
                'f.parent_id',
                'f.company_id',
                '"' . $classFact::tableName() . '" AS {{tb}}',
                'f.rs',
                'f.created_at',
                'f.payment_order_number',
                'f.date',
                new Expression('IF(f.is_prepaid_expense, null, f.recognition_date) recognition_date'),
                'f.flow_type',
                'f.amount',
                'f.contractor_id',
                'f.description',
                'f.expenditure_item_id',
                'f.income_item_id',
                'CONCAT("1_", f.rs) AS {{wallet_id}}',
                'f.is_internal_transfer',
                'f.source',
                //
                "IF({{f}}.[[flow_type]] = 0 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountExpense",
                "IF({{f}}.[[flow_type]] = 1 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountIncome",
                new Expression('IF (flow_type = 0, CONCAT(id, "_", IFNULL(internal_transfer_flow_id, "C")), CONCAT(IFNULL(internal_transfer_flow_id, "C"), "_", id)) AS transfer_key'),
                'f.project_id',
                'f.sale_point_id',
                'f.industry_id',
                //
                'f.has_tin_children'
            ]);
        }

        if ($this->operation_type == self::OPERATION_TYPE_PLAN || $this->_userConfigShowPlan) {

            $planQuery = $classPlan::find()
                ->alias('p')
                ->andWhere(['p.payment_type' => PlanCashFlows::PAYMENT_TYPE_BANK])
                ->andWhere(['p.company_id' => $this->company_id])
                ->groupBy('p.id');

            $planQuery->addSelect([
                'p.id',
                new Expression('NULL AS parent_id'),
                'p.company_id',
                '"' . $classPlan::tableName() . '" AS {{tb}}',
                'p.rs',
                'p.created_at',
                '"" AS [[payment_order_number]]',
                'p.date',
                new Expression('p.date AS recognition_date'),
                'p.flow_type',
                'p.amount',
                'p.contractor_id',
                'p.description',
                'p.expenditure_item_id',
                'p.income_item_id',
                '"plan" AS {{wallet_id}}',
                'p.is_internal_transfer',
                CashBankStatementUpload::SOURCE_MANUAL.' AS {{source}}',
                //
                'IF({{p}}.[[flow_type]] = 0, {{p}}.[[amount]], 0) AS amountExpense',
                'IF({{p}}.[[flow_type]] = 1, {{p}}.[[amount]], 0) AS amountIncome',
                new Expression('IF (flow_type = 0, CONCAT("PLAN_", id, "_C"), CONCAT("PLAN_", "C_", id)) AS transfer_key'),
                'p.project_id',
                'p.sale_point_id',
                'p.industry_id',
                //
                new Expression('0 AS has_tin_children'),
            ]);
        }

        if ($factQuery && $planQuery) {
            return $factQuery->union($planQuery, true);
        } else {
            return ($factQuery ?: $planQuery);
        }
    }

    /**
     * @return bool
     */
    public function getIsExists()
    {
        return self::find()->andWhere([
            'company_id' => $this->company_id
        ])->andFilterWhere([
            'rs' => $this->rs
        ])->exists();
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return CheckingAccountant::find()->select('bank_name')->andWhere([
            'company_id' => $this->company_id,
            'rs' => $this->rs,
        ])->orderBy(['type' => SORT_ASC])->scalar();
    }

    /**
     * @param $value
     */
    public function setShowPlan($value)
    {
        $this->_userConfigShowPlan = $value;
    }

    /**
     * @return array
     */
    public function getAccountsFilterItems()
    {
        $result = [];

        if (!empty($this->_sql)) {
            $query = (new \yii\db\Query)->select('rs')->distinct()->from([
                't' => sprintf('(%s)', $this->_sql),
            ]);

            if ($rsArray = $query->column()) {
                $company = $this->company;
                foreach ($rsArray as $rs) {
                    if ($account = $company->accountByRs($rs)) {
                        $result[$rs] = $account->name;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $company
     * @param $formattedFlowAmount
     * @param $flows
     * @param $returnFormatted
     * @return string
     */
    public static function getUpdateFlowLink($formattedFlowAmount, $flows = [], $returnFormatted = true)
    {
        $flowTable = ArrayHelper::getValue($flows, 'tb');
        $flowId = ArrayHelper::getValue($flows, 'id');
        $isPlanFlow = ArrayHelper::getValue($flows, 'wallet_id') == 'plan';

        $url = 'javascript:;';

        switch ($flowTable) {

            // RUB
            case CashBankFlows::tableName():
                $url = Url::to(['/cash/bank/update', 'id' => $flowId]);
                break;
            case PlanCashFlows::tableName():
                if ($planFlow = PlanCashFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/bank/update', 'id' => $flows['id'], 'is_plan_flow' => 1]);
                break;

            // FOREIGN
            case CashBankForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/bank/update', 'id' => $flowId, 'foreign' => 1]);
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                if ($planFlow = PlanCashForeignCurrencyFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/bank/update', 'id' => $flows['id'], 'is_plan_flow' => 1, 'foreign' => 1]);
                break;
        }

        if (!$returnFormatted)
            return $url;

        return \yii\helpers\Html::a($formattedFlowAmount, $url, [
            'title' => 'Изменить',
            'class' => 'update-flow-item link link-bleak' . ($isPlanFlow ? ' plan' : ''),
            'data' => [
                'toggle' => 'modal',
                'target' => '#update-movement',
                'pjax' => 0
            ],
        ]);
    }

}
