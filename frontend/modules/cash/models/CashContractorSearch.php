<?php
namespace frontend\modules\cash\models;

use Yii;
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\rbac\permissions\Service;

class CashContractorSearch extends CashSearch {

    public function searchByContractor($contractorId, $params = [])
    {
        if (isset($params['CashContractorSearch']['contractor_ids'])) {
            unset($params['CashContractorSearch']['contractor_ids']);
        }

        $this->contractor_ids = [$contractorId];
        return $this->search($params);
    }

    public function getWalletFilterItems()
    {
        $showBanks = Yii::$app->user->can(Service::HOME_CASH);

        $query = clone ($this->_filterQuery)->andWhere(['between', 't.date', $this->periodStartDate, $this->periodEndDate]);

        $currentWalletIdArray = $query
            ->distinct()
            ->select("wallet_id")
            ->orderBy("wallet_id")
            ->column();

        $ret = ['' => 'Все счета'];

        $_addedCashblocks = [];

        foreach ($currentWalletIdArray as $walletId) {
            @list($cashBlock, $currentWalletId) = explode('_', $walletId);
            switch ($cashBlock) {
                case self::WALLET_BANK:
                    $cashBlockName = 'Банк';
                    $name = ArrayHelper::getValue($this->bankItems, $currentWalletId, "rs={$walletId}");
                    if (!$showBanks) continue 2;
                    break;
                case self::WALLET_ORDER:
                    $cashBlockName = 'Касса';
                    $name = ArrayHelper::getValue($this->cashboxItems, $currentWalletId, "id={$walletId}");
                    break;
                case self::WALLET_EMONEY:
                    $cashBlockName = 'E-money';
                    $name = ArrayHelper::getValue($this->emoneyItems, $currentWalletId, "id={$walletId}");
                    break;
                default:
                    $cashBlockName = '-';
                    $name = '-';
                    break;
            }
            if (!in_array($cashBlock, $_addedCashblocks)) {
                $ret[$cashBlock] = $cashBlockName;
            }
            $ret[$walletId] = $name;
        }

        return $ret;
    }

    public function getContractorFilterItems()
    {
        $query = clone ($this->_filterQuery)->andWhere(['between', 't.date', $this->periodStartDate, $this->periodEndDate]);

        $tContractor = Contractor::tableName();
        $tCashContractor = CashContractorType::tableName();
        $contractorIdArray = $query
            ->distinct()
            ->select("t.contractor_id")
            ->column();
        $cashContractorArray = ArrayHelper::map(
            CashContractorType::find()
                ->andWhere(["$tCashContractor.name" => $contractorIdArray])
                ->andWhere(["not", ["$tCashContractor.name" => ['bank', 'order', 'emoney']]])
                ->all(),
            'name',
            'text'
        );
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->andWhere(["$tContractor.id" => $contractorIdArray])
                ->all(),
            'id',
            'shortName'
        );

        return $cashContractorArray + $contractorArray;
    }

    public function getReasonFilterItems()
    {
        $query = clone ($this->_filterQuery)->andWhere(['between', 't.date', $this->periodStartDate, $this->periodEndDate]);
        $e_reasonArray = $query
            ->distinct()
            ->select('CONCAT("e_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `expenditure_item_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = (clone $this->_filterQuery)->andWhere(['between', 't.date', $this->periodStartDate, $this->periodEndDate]);
        $i_reasonArray = $query
            ->distinct()
            ->select('CONCAT("i_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `income_item_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    public function getProjectFilterItems()
    {
        $query = clone ($this->_filterQuery)->andWhere(['between', 't.date', $this->periodStartDate, $this->periodEndDate]);
        $projectArray = $query
            ->distinct()
            ->select('`project_id`, `project`.`name` `project_name`')
            ->leftJoin(Project::tableName() . ' `project`', '`project`.`id` = `project_id`')
            ->orderBy('project_name')
            ->andWhere(['not', ['project_id' => null]])
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($projectArray, 'project_id', 'project_name');
    }

}