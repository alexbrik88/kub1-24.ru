<?php

namespace frontend\modules\cash\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Company;
use common\models\cash\CashFlowLinkage;

/**
 * LinkageSearch represents the model behind the search form of `common\models\cash\CashFlowLinkage`.
 */
class LinkageSearch extends CashFlowLinkage
{
    private Company $_company;

    /**
     * {@inheritdoc}
     */
    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CashFlowLinkage::find();

        $query->andWhere([
            'company_id' => $this->_company->id,
            'is_deleted' => false,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 0],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'number' => $this->number,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
