<?php

namespace frontend\modules\cash\models;

use common\models\cash\Cashbox;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\components\FilterHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class CashOrderForeignCurrencySearch
 * @package frontend\modules\cash\models
 */
class CashOrderForeignCurrencySearch extends CashOrderForeignCurrencyFlows
{
    public $reason_id;
    public $contractor_query;

    protected $_filterQuery;
    protected $_statisticQuery;
    protected $_period;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->_period = StatisticPeriod::getDefaultPeriod();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id', 'expenditure_item_id', 'is_accounting', 'flow_type'], 'safe'],
            [['reason_id', 'contractor_query'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSearchQuery()
    {
        return $this->_filterQuery ? clone $this->_filterQuery : null;
    }

    /**
     * @param array $params
     * @param null $dateRange
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $alias = 'cof';

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $query = $this->find()
            ->alias($alias)
            ->with('contractor', 'expenditureReason')
            ->joinWith(['contractor', 'cashbox'], false)
            ->andWhere([$alias . '.company_id' => $this->company_id])
            ->andWhere([$alias . '.cashbox_id' => $this->cashbox_id]);

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['or',
                [$alias . '.is_accounting' => true],
                ['and',
                    [$alias . '.is_accounting' => false],
                    ['or',
                        ['cashbox.responsible_employee_id' => $user->id],
                        ['cashbox.accessible_to_all' => true],
                    ],
                ],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date',
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'number' => [
                        'asc' => [
                            $alias . '.flow_type' => SORT_DESC,
                            $alias . '.`number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            $alias . '.flow_type' => SORT_ASC,
                            $alias . '.`number` * 1' => SORT_DESC,
                        ],
                    ],
                    'created_at',
                    'billPaying' => [
                        'asc' => ['(invoice.document_number * 1)' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC],
                        'desc' => ['(invoice.document_number * 1)' => SORT_DESC, 'invoice.document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

    //    // бухгалтер видит все по своей кассе или только для учета в бухгалтерии
    //    if ($this->employeeCompany && $this->employeeCompany->employee_role_id == EmployeeRole::ROLE_ACCOUNTANT) {
    //        $query->andWhere([
    //            'or',
    //            [$alias . '.is_accounting' => true],
    //            ['cashbox.responsible_employee_id' =>  $this->employeeCompany->employee_id],
    //        ]);
    //    }

        $this->load($params);

        if ($this->is_accounting == -1) {
            $this->is_accounting = null;
        }

        if ($this->flow_type == -1) {
            $this->flow_type = null;
        }

        if ($this->contractor_query) {
            $query->andFilterWhere(['or',
                ['like', 'contractor.name', $this->contractor_query],
                ['like', 'contractor.ITN', $this->contractor_query],
            ]);
        }

        if ($this->reason_id == 'empty') {
            $query->andWhere(['or',
                ['and',
                    ['flow_type' => CashOrderForeignCurrencyFlows::FLOW_TYPE_EXPENSE],
                    [$alias . '.expenditure_item_id' => null],
                ],
                ['and',
                    ['flow_type' => CashOrderForeignCurrencyFlows::FLOW_TYPE_INCOME],
                    [$alias . '.income_item_id' => null],
                ],
            ]);
        } elseif (!empty($this->reason_id)) {
            if (substr($this->reason_id, 0, 2) === 'e_') {
                $this->expenditure_item_id = substr($this->reason_id, 2);
            } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                $this->income_item_id = substr($this->reason_id, 2);
            }
            $query->andFilterWhere([
                $alias . '.expenditure_item_id' => $this->expenditure_item_id,
                $alias . '.income_item_id' => $this->income_item_id,
            ]);
        }

        $query->andFilterWhere([
            $alias . '.id' => $this->id,
            $alias . '.is_accounting' => $this->is_accounting,
            $alias . '.contractor_id' => $this->contractor_id,
            $alias . '.flow_type' => $this->flow_type,
        ]);

        $this->_statisticQuery = clone $query;

        if (empty($this->id)) {
            $query->andWhere(['between', 'date', $this->_period['from'], $this->_period['to']]);
        }

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        return FilterHelper::getContractorFilterItems($query);
    }

    /**
     * @return array
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select('CONCAT("e_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cof`.`expenditure_item_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select('CONCAT("i_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cof`.`income_item_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /**
     * @return bool
     */
    public function getIsExists()
    {
        return self::find()->andWhere([
            'company_id' => $this->company_id,
            'cashbox_id' => $this->cashbox_id,
        ])->exists();
    }

    /**
     * @return string
     */
    public function getCashboxName()
    {
        return is_array($this->cashbox_id) ? null : Cashbox::find()->andWhere([
            'id' => $this->cashbox_id,
            'company_id' => $this->company_id,
        ])->select('name')->scalar();
    }
}
