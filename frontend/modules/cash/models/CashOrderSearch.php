<?php
/**
 * Created by konstantin.
 * Date: 1.7.15
 * Time: 15.41
 */

namespace frontend\modules\cash\models;

use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\project\Project;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\components\FilterHelper;
use frontend\modules\cash\models\query\CashPlanFactQuery;
use Yii;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\helpers\Url;

/**
 * Class CashOrderSearch
 * @package frontend\modules\cash\models
 */
class CashOrderSearch extends CashOrderFlows
{
    use FilterItemsTrait;
    use FilterNamesTrait;
    use WalletNamesTrait;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    public $foreign;
    public $currency_name;
    public $currency_id;

    private $_filterQuery;
    private $_userConfigShowPlan;
    private $_sql;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cashbox_id', 'contractor_id', 'expenditure_item_id', 'is_accounting', 'flow_type'], 'safe'],
            [['foreign', 'currency_id', 'currency_name'], 'safe'],
            [['project_id'], 'safe'],
            [['project_id', 'wallet_ids', 'contractor_ids', 'reason_ids', 'contractor_name', 'operation_type'], 'safe'],
            [['sale_point_id', 'industry_id'], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSearchQuery()
    {
        return $this->_filterQuery ? clone $this->_filterQuery : null;
    }

    /**
     *
     */
    public function init()
    {
        $this->initWalletNames($this->company_id, self::WALLET_CASHBOX);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = strlen($this->project_id) ? $this->project_id : null;
        $this->sale_point_id = strlen($this->sale_point_id) ? $this->sale_point_id : null;
        $this->industry_id = strlen($this->industry_id) ? $this->industry_id : null;
    }

    /**
     * @param array $params
     * @param null $dateRange
     * @return SqlDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $cashbox_id = $this->cashbox_id;
        $this->cashbox_id = null;

        $this->load($params);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $query = (new CashPlanFactQuery())
            ->alias('cof')
            ->from(['cof' => $this->getPlanFactQuery($cashbox_id)])
            ->leftJoin('contractor', 'contractor.id = cof.contractor_id')
            ->leftJoin('cashbox', 'cashbox.id = cof.cashbox_id')
            ->select([
                'cof.id',
                'cof.company_id',
                'cof.tb',
                'cof.cashbox_id',
                'cof.created_at',
                'cof.date',
                'cof.flow_type',
                'cof.amount',
                'cof.contractor_id',
                'cof.description',
                'cof.expenditure_item_id',
                'cof.income_item_id',
                'cof.wallet_id',
                'cof.is_internal_transfer',
                'cof.number',
                'cof.is_accounting',
                //
                'cof.amountExpense',
                'cof.amountIncome',
                'cof.transfer_key',
                'cof.project_id',
                'cof.sale_point_id',
                'cof.industry_id'
            ]);

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['or',
                ['cof.is_accounting' => true],
                ['and',
                    ['cof.is_accounting' => false],
                    ['or',
                        ['cashbox.responsible_employee_id' => $user->id],
                        ['cashbox.accessible_to_all' => true],
                    ],
                ],
            ]);
        }

        if ($this->id) {

            $query->andWhere(['cof.id' => $this->id]);
            $this->_filterQuery = clone $query;
            $this->_statisticQuery = clone $query;

        } else {

            $this->setFilters($query, 'cof');

            if ($this->wallet_ids) {
                $query->andFilterWhere([
                    'cof.cashbox_id' => array_map(function ($v) {
                        return str_replace(self::WALLET_CASHBOX . '_', '', $v);
                    }, $this->wallet_ids),
                ]);
            } else {
                $query->andFilterWhere([
                    'cof.cashbox_id' => $this->cashbox_id ?: null,
                ]);
            }

            $this->_statisticQuery = (clone $query)
                ->andWhere(['not', ['wallet_id' => 'plan']]);

            $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
            $query->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']]);

            $this->_filterQuery = clone $query;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $this->_sql = $query->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'date',
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'number' => [
                        'asc' => [
                            'cof.flow_type' => SORT_DESC,
                            'cof.`number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            'cof.flow_type' => SORT_ASC,
                            'cof.`number` * 1' => SORT_DESC,
                        ],
                    ],
                    'created_at',
                    //'billPaying' => [
                    //    'asc' => ['(invoice.document_number * 1)' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC],
                    //    'desc' => ['(invoice.document_number * 1)' => SORT_DESC, 'invoice.document_additional_number' => SORT_ASC],
                    //    'default' => SORT_ASC
                    //],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    private function getPlanFactQuery($cashbox_id)
    {
        $classFact = ($this->foreign)
            ? CashOrderForeignCurrencyFlows::class
            : CashOrderFlows::class;
        $classPlan = ($this->foreign)
            ? PlanCashForeignCurrencyFlows::class
            : PlanCashFlows::class;

        $factQuery = $planQuery = null;

        if ($this->operation_type == self::OPERATION_TYPE_FACT || empty($this->operation_type)) {

            $factQuery = $classFact::find()
                ->alias('f')
                ->andWhere(['f.company_id' => $this->company_id])
                ->andWhere(['f.cashbox_id' => $cashbox_id])
                ->groupBy('f.id');

            $factQuery->addSelect([
                'f.id',
                'f.company_id',
                '"' . $classFact::tableName() . '" AS {{tb}}',
                'f.cashbox_id',
                'f.created_at',
                'f.date',
                'f.flow_type',
                'f.amount',
                'f.contractor_id',
                'f.description',
                'f.expenditure_item_id',
                'f.income_item_id',
                'CONCAT("2_", f.cashbox_id) AS {{wallet_id}}',
                'f.is_internal_transfer',
                'f.number',
                'f.is_accounting',
                //
                "IF({{f}}.[[flow_type]] = 0 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountExpense",
                "IF({{f}}.[[flow_type]] = 1 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountIncome",
                new Expression('IF (flow_type = 0, CONCAT(id, "_", IFNULL(internal_transfer_flow_id, "C")), CONCAT(IFNULL(internal_transfer_flow_id, "C"), "_", id)) AS transfer_key'),
                'f.project_id',
                'f.sale_point_id',
                'f.industry_id'
            ]);
        }

        if ($this->operation_type == self::OPERATION_TYPE_PLAN || $this->_userConfigShowPlan) {

            $planQuery = $classPlan::find()
                ->alias('p')
                ->andWhere(['p.payment_type' => PlanCashFlows::PAYMENT_TYPE_ORDER])
                ->andWhere(['p.company_id' => $this->company_id])
                ->andFilterWhere(['p.cashbox_id' => $this->cashbox_id])
                ->groupBy('id');

            $planQuery->addSelect([
                'p.id',
                'p.company_id',
                '"' . $classPlan::tableName() . '" AS {{tb}}',
                'p.cashbox_id',
                'p.created_at',
                'p.date',
                'p.flow_type',
                'p.amount',
                'p.contractor_id',
                'p.description',
                'p.expenditure_item_id',
                'p.income_item_id',
                '"plan" AS {{wallet_id}}',
                'p.is_internal_transfer',
                '"" AS {{number}}',
                '"" AS {{is_accounting}}',
                //
                'IF({{p}}.[[flow_type]] = 0, {{p}}.[[amount]], 0) AS amountExpense',
                'IF({{p}}.[[flow_type]] = 1, {{p}}.[[amount]], 0) AS amountIncome',
                new Expression('IF (flow_type = 0, CONCAT("PLAN_", id, "_C"), CONCAT("PLAN_", "C_", id)) AS transfer_key'),
                'p.project_id',
                'p.sale_point_id',
                'p.industry_id'
            ]);
        }

        if ($factQuery && $planQuery) {
            return $factQuery->union($planQuery, true);
        } else {
            return ($factQuery ?: $planQuery);
        }
    }

    /**
     * @return bool
     */
    public function getIsExists()
    {
        return self::find()->andWhere([
            'company_id' => $this->company_id,
            'cashbox_id' => $this->cashbox_id,
        ])->exists();
    }

    /**
     * @return string
     */
    public function getCashboxName()
    {
        return is_array($this->cashbox_id) ? null : Cashbox::find()->andWhere([
            'id' => $this->cashbox_id,
            'company_id' => $this->company_id,
        ])->select('name')->scalar();
    }

    /**
     * @param $value
     */
    public function setShowPlan($value)
    {
        $this->_userConfigShowPlan = $value;
    }

    /**
     * @return array
     */
    public function getAccountsFilterItems()
    {
        $result = [];

        if (!empty($this->_sql)) {
            $query = (new \yii\db\Query)->select('cashbox_id')->distinct()->from([
                't' => sprintf('(%s)', $this->_sql),
            ]);

            if ($idArray = $query->column()) {
                $result = $this->company->getCashboxes()->andWhere([
                    'id' => $idArray,
                ])->select('name')->indexBy('id')->column();
            }
        }

        return $result;
    }

    /**
     * @param $formattedFlowAmount
     * @param $flows
     * @param $returnFormatted
     * @return string
     */
    public static function getUpdateFlowLink($formattedFlowAmount, $flows = [], $returnFormatted = true)
    {
        $flowTable = ArrayHelper::getValue($flows, 'tb');
        $flowId = ArrayHelper::getValue($flows, 'id');
        $isPlanFlow = ArrayHelper::getValue($flows, 'wallet_id') == 'plan';

        $url = 'javascript:;';

        switch ($flowTable) {

            // RUB
            case CashOrderFlows::tableName():
                $url = Url::to(['/cash/order/update', 'id' => $flowId]);
                break;
            case PlanCashFlows::tableName():
                if ($planFlow = PlanCashFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/order/update', 'id' => $flows['id'], 'is_plan_flow' => 1]);
                break;

            // FOREIGN
            case CashOrderForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/order/update', 'id' => $flowId, 'foreign' => 1]);
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                if ($planFlow = PlanCashForeignCurrencyFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/order/update', 'id' => $flows['id'], 'is_plan_flow' => 1, 'foreign' => 1]);
                break;
        }

        if (!$returnFormatted)
            return $url;

        return \yii\helpers\Html::a($formattedFlowAmount, $url, [
            'title' => 'Изменить',
            'class' => 'update-flow-item link link-bleak' . ($isPlanFlow ? ' plan' : ''),
            'data' => [
                'toggle' => 'modal',
                'target' => '#update-movement',
                'pjax' => 0
            ],
        ]);
    }
}
