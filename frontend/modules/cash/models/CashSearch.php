<?php
namespace frontend\modules\cash\models;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use frontend\modules\cash\models\cashSearch\ChartTrait;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\HelperTrait;
use frontend\modules\cash\models\cashSearch\QueryForeignTrait;
use frontend\modules\cash\models\cashSearch\QueryTrait;
use frontend\modules\cash\models\cashSearch\StatisticsTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\base\Exception;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use Yii;

class CashSearch extends \yii\base\Model {

    use ChartTrait;
    use FilterItemsTrait;
    use FilterNamesTrait;
    use HelperTrait;
    use QueryTrait;
    use QueryForeignTrait;
    use StatisticsTrait;
    use WalletNamesTrait;

    const WALLET_BANK = 1;
    const WALLET_ORDER = 2;
    const WALLET_EMONEY = 3;
    const WALLET_ACQUIRING = 4;
    const WALLET_CARD = 5;
    const WALLET_FOREIGN_BANK = 11;
    const WALLET_FOREIGN_ORDER = 12;
    const WALLET_FOREIGN_EMONEY = 13;

    const OPERATION_TYPE_FACT = 1;
    const OPERATION_TYPE_PLAN = 2;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    // widget
    public $widget_wallet_id;
    public $widget_currency_name;

    // filters;
    public $company_id;
    public $contractor_name;
    public $income_item_id;
    public $expenditure_item_id;
    public $flow_type;
    public $project_id;
    public $payment_priority;
    public $operation_type;

    // multiple filters
    public $contractor_ids;
    public $reason_ids;
    public $wallet_ids;

    // stats
    public $periodStartDate;
    public $periodEndDate;

    // private
    protected $_employee;
    protected $_companiesIds;
    protected $_filterQuery;
    protected $_filterQueryFullPeriod;

    // modal #many-item, #many-plan-date
    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $incomeManyPlanDate;
    public $expenditureManyPlanDate;

    // chart
    private $_chartDataFact = [];
    private $_chartDataPlan = [];

    // plan table
    public static $paymentPriorityNames = [
        Contractor::PAYMENT_PRIORITY_HIGH => '1 - Большой',
        Contractor::PAYMENT_PRIORITY_MEDIUM => '2 - Средний',
        Contractor::PAYMENT_PRIORITY_LOW => '3 - Наименьший',
    ];

    // show plan
    protected $_userConfigShowPlan = true;

    // split internal transfer
    protected $_splitInternalTransfer = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [[
                'company_id',
                'wallet_ids',
                'flow_type',
                'contractor_ids',
                'reason_ids',
                'project_id',
                'contractor_name',
                'payment_priority',
                'operation_type',
                'sale_point_id',
                'industry_id',
            ], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ]);
    }

    /**
     *
     */
    public function init()
    {
        $this->_employee = Yii::$app->user->identity;
        $this->_companiesIds = \Yii::$app->multiCompanyManager->getCompaniesIds();
        $this->widget_wallet_id = Yii::$app->session->get('cash_widget_wallet');
        $this->widget_currency_name = Yii::$app->session->get('cash_widget_currency');
        $this->_userConfigShowPlan = !Yii::$app->user->identity->config->cash_index_hide_plan;
        $this->initWalletNames($this->_companiesIds);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = strlen($this->project_id) ? $this->project_id : null;
        $this->sale_point_id = strlen($this->sale_point_id) ? $this->sale_point_id : null;
        $this->industry_id = strlen($this->industry_id) ? $this->industry_id : null;
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     * @throws \Exception
     */
    public function search($params = [])
    {
        if (!$this->_companiesIds)
            throw new Exception('CashSearch params not initialized');

        $this->load($params);

        $dateRange = StatisticPeriod::getSessionPeriod();
        $this->periodStartDate = DateHelper::format($dateRange['from'], 'd.m.Y', 'Y-m-d');
        $this->periodEndDate = DateHelper::format($dateRange['to'], 'd.m.Y', 'Y-m-d');

        if ($this->widget_currency_name) {
            // FOREIGN
            if ($this->widget_wallet_id) {
                $query = $this->getForeignWalletsQuery($this->widget_currency_name, [$this->widget_wallet_id]);
            } elseif ($this->wallet_ids) {
                $query = $this->getForeignWalletsQuery($this->widget_currency_name, $this->wallet_ids);
            } else {
                $query = $this->getForeignWalletsQuery($this->widget_currency_name);
            }
        } else {
            // RUB
            if ($this->widget_wallet_id) {
                $query = $this->getWalletsQuery([$this->widget_wallet_id]);
            } elseif ($this->wallet_ids) {
                $query = $this->getWalletsQuery($this->wallet_ids);
            } else {
                $query = $this->getWalletsQuery([]);
            }
        }

        $query->addSelect([
            't.id',
            't.company_id',
            't.tb',
            't.amountExpense',
            't.amountIncome',
            't.rs',
            't.cashbox_id',
            't.emoney_id',
            't.acquiring_id',
            't.card_id',
            't.created_at',
            't.date',
            't.recognition_date',
            't.flow_type',
            't.amount',
            't.contractor_id',
            't.description',
            't.expenditure_item_id',
            't.income_item_id',
            't.wallet_id',
            't.project_id',
            't.sale_point_id',
            't.industry_id',
            't.is_internal_transfer',
            't.transfer_key',
            't.has_tin_children'
        ]);

        $query->leftJoin('contractor', 'contractor.id = t.contractor_id')->addSelect('contractor.payment_priority');

        $this->setFilters($query);

        $this->_filterQueryFullPeriod = clone $query;

        $query->andWhere(['between', 't.date', $dateRange['from'], $dateRange['to']]);

        if (!($this->_userConfigShowPlan || $this->isPlan())) {
            $query->andWhere(['not', ['wallet_id' => 'plan']]);
        }

        $query->groupBy('t.id');

        $this->_filterQuery = clone $query;

        if ($this->_splitInternalTransfer)
            $query->groupBy('t.transfer_key');

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    /*'billPaying' => [
                        'asc' => ['document_number' => SORT_ASC, 'document_additional_number' => SORT_ASC],
                        'desc' => ['document_number' => SORT_DESC, 'document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],*/
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    public function getIsForeign()
    {
        return (bool) $this->widget_currency_name == Currency::DEFAULT_NAME;
    }

    public function getCurrencySymbol()
    {
        return (string) ArrayHelper::getValue(Currency::$currencySymbols, $this->widget_currency_name);
    }

    public function getCurrency_name()
    {
        return $this->widget_currency_name;
    }

    public function setShowPlan($value)
    {
        $this->_userConfigShowPlan = $value;
    }

    public function setSplitInternalTransfer($value)
    {
        $this->_splitInternalTransfer = $value;
    }
}