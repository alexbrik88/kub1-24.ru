<?php

namespace frontend\modules\cash\models;

use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "widget_config_account_balance".
 *
 * @property int $company_id
 * @property int $employee_id
 * @property string|null $options
 *
 * @property Company $company
 * @property Employee $employee
 *
 * @property array $exceptRs
 * @property array $exceptCashboxes
 * @property array $exceptEmoneys
 */
class WidgetConfigAccountBalance extends \yii\db\ActiveRecord
{
    private $optionsArr = [
        'rs' => [],
        'cashbox' => [],
        'emoney' => []
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widget_config_account_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id'], 'required'],
            [['company_id', 'employee_id'], 'integer'],
            [['options'], 'string'],
            [['company_id', 'employee_id'], 'unique', 'targetAttribute' => ['company_id', 'employee_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'options' => 'Options',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->options) {
            $this->optionsArr = json_decode($this->options, true);
        }
    }

    /**
     * @return array
     */
    public function getExceptRs()
    {
        return $this->optionsArr['rs'] ?? [];
    }

    /**
     * @return array
     */
    public function getExceptCashboxes()
    {
        return $this->optionsArr['cashboxes'] ?? [];
    }

    /**
     * @return array
     */
    public function getExceptEmoneys()
    {
        return $this->optionsArr['emoneys'] ?? [];
    }
}
