<?php
/**
 * Created by konstantin.
 * Date: 1.7.15
 * Time: 15.41
 */

namespace frontend\modules\cash\models;

use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\Emoney;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\components\FilterHelper;
use frontend\modules\cash\models\query\CashPlanFactQuery;
use Yii;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * Class CashEmoneySearch
 * @package frontend\modules\cash\models
 */
class CashEmoneySearch extends CashEmoneyFlows
{
    use FilterItemsTrait;
    use FilterNamesTrait;
    use WalletNamesTrait;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    public $foreign;
    public $currency_name;
    public $currency_id;

    private $_filterQuery;
    private $_userConfigShowPlan;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id', 'expenditure_item_id', 'is_accounting', 'flow_type'], 'safe'],
            [['foreign', 'currency_id', 'currency_name'], 'safe'],
            [['project_id', 'wallet_ids', 'contractor_ids', 'reason_ids', 'contractor_name', 'operation_type'], 'safe'],
            [['sale_point_id', 'industry_id'], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSearchQuery()
    {
        return $this->_filterQuery ? clone $this->_filterQuery : null;
    }

    /**
     *
     */
    public function init()
    {
        $this->initWalletNames($this->company_id, self::WALLET_EMONEY);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = strlen($this->project_id) ? $this->project_id : null;
        $this->sale_point_id = strlen($this->sale_point_id) ? $this->sale_point_id : null;
        $this->industry_id = strlen($this->industry_id) ? $this->industry_id : null;
    }

    /**
     * @param array $params
     * @return SqlDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $this->load($params);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $query = (new CashPlanFactQuery())
            ->alias('cef')
            ->from(['cef' => $this->getPlanFactQuery()])
            ->leftJoin('contractor', 'contractor.id = cef.contractor_id')
            ->select([
                'cef.id',
                'cef.company_id',
                'cef.tb',
                'cef.emoney_id',
                'cef.created_at',
                'cef.date',
                'cef.flow_type',
                'cef.amount',
                'cef.contractor_id',
                'cef.description',
                'cef.expenditure_item_id',
                'cef.income_item_id',
                'cef.wallet_id',
                'cef.is_internal_transfer',
                'cef.number',
                'cef.is_accounting',
                //
                'cef.amountExpense',
                'cef.amountIncome',
                'cef.transfer_key',
                'cef.project_id',
                'cef.sale_point_id',
                'cef.industry_id'
            ]);

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['cef.is_accounting' => true]);
        }

        if ($this->id) {

            $query->andWhere(['cef.id' => $this->id]);
            $this->_filterQuery = clone $query;
            $this->_statisticQuery = clone $query;

        } else {

            $this->setFilters($query, 'cef')
                ->andFilterWhere([
                    'cef.is_accounting' => $this->is_accounting,
                    'cef.emoney_id' => ($this->wallet_ids)
                        ? array_map(function ($v) {
                            return str_replace(self::WALLET_EMONEY . '_', '', $v);
                        }, $this->wallet_ids)
                        : str_replace('all', '', $this->emoney_id)
                ]);

            $this->_statisticQuery = (clone $query)
                ->andWhere(['not', ['wallet_id' => 'plan']]);

            $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
            $query->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']]);

            $this->_filterQuery = clone $query;
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'date',
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);        
        
        return $dataProvider;
    }

    public function getPlanFactQuery()
    {
        $classFact = ($this->foreign)
            ? CashEmoneyForeignCurrencyFlows::class
            : CashEmoneyFlows::class;
        $classPlan = ($this->foreign)
            ? PlanCashForeignCurrencyFlows::class
            : PlanCashFlows::class;

        $factQuery = $planQuery = null;

        if ($this->operation_type == self::OPERATION_TYPE_FACT || empty($this->operation_type)) {

            $factQuery = $classFact::find()
                ->alias('f')
                ->andWhere(['f.company_id' => $this->company_id])
                ->andWhere(['f.emoney_id' => $this->emoney_id])
                ->groupBy('f.id');

            $factQuery->addSelect([
                'f.id',
                'f.company_id',
                '"' . $classFact::tableName() . '" AS {{tb}}',
                'f.emoney_id',
                'f.created_at',
                'f.date',
                'f.flow_type',
                'f.amount',
                'f.contractor_id',
                'f.description',
                'f.expenditure_item_id',
                'f.income_item_id',
                'CONCAT("3_", f.emoney_id) AS {{wallet_id}}',
                'f.is_internal_transfer',
                'f.number',
                'f.is_accounting',
                //
                "IF({{f}}.[[flow_type]] = 0 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountExpense",
                "IF({{f}}.[[flow_type]] = 1 OR {{f}}.[[is_internal_transfer]], {{f}}.[[amount]], 0) AS amountIncome",
                new Expression('IF (flow_type = 0, CONCAT(id, "_", IFNULL(internal_transfer_flow_id, "C")), CONCAT(IFNULL(internal_transfer_flow_id, "C"), "_", id)) AS transfer_key'),
                'f.project_id',
                'f.sale_point_id',
                'f.industry_id'
            ]);
        }

        if ($this->operation_type == self::OPERATION_TYPE_PLAN || $this->_userConfigShowPlan) {

            $planQuery = $classPlan::find()
                ->alias('p')
                ->andWhere(['p.payment_type' => PlanCashFlows::PAYMENT_TYPE_EMONEY])
                ->andWhere(['p.company_id' => $this->company_id])
                ->andWhere(['p.emoney_id' => $this->emoney_id])
                ->groupBy('id');

            $planQuery->addSelect([
                'p.id',
                'p.company_id',
                '"' . $classPlan::tableName() . '" AS {{tb}}',
                'p.emoney_id',
                'p.created_at',
                'p.date',
                'p.flow_type',
                'p.amount',
                'p.contractor_id',
                'p.description',
                'p.expenditure_item_id',
                'p.income_item_id',
                '"plan" AS {{wallet_id}}',
                'p.is_internal_transfer',
                '"" AS {{number}}',
                '"" AS {{is_accounting}}',
                //
                'IF({{p}}.[[flow_type]] = 0, {{p}}.[[amount]], 0) AS amountExpense',
                'IF({{p}}.[[flow_type]] = 1, {{p}}.[[amount]], 0) AS amountIncome',
                new Expression('IF (flow_type = 0, CONCAT("PLAN_", id, "_C"), CONCAT("PLAN_", "C_", id)) AS transfer_key'),
                'p.project_id',
                'p.sale_point_id',
                'p.industry_id'
            ]);
        }

        if ($factQuery && $planQuery) {
            return $factQuery->union($planQuery, true);
        } else {
            return ($factQuery ?: $planQuery);
        }
    }

    /**
     * @return bool
     */
    public function getIsExists()
    {
        return self::find()->andWhere([
            'company_id' => $this->company_id,
            'emoney_id' => $this->emoney_id,
        ])->exists();
    }

    /**
     * @return string
     */
    public function getEmoneyName()
    {
        return is_array($this->emoney_id) ? null : Emoney::find()->andWhere([
            'id' => $this->emoney_id,
            'company_id' => $this->company_id,
        ])->select('name')->scalar();
    }

    /**
     * @param $value
     */
    public function setShowPlan($value)
    {
        $this->_userConfigShowPlan = $value;
    }

    /**
     * @param $formattedFlowAmount
     * @param $flows
     * @param $returnFormatted
     * @return string
     */
    public static function getUpdateFlowLink($formattedFlowAmount, $flows = [], $returnFormatted = true)
    {
        $flowTable = ArrayHelper::getValue($flows, 'tb');
        $flowId = ArrayHelper::getValue($flows, 'id');
        $isPlanFlow = ArrayHelper::getValue($flows, 'wallet_id') == 'plan';

        $url = 'javascript:;';

        switch ($flowTable) {

            // RUB
            case CashEmoneyFlows::tableName():
                $url = Url::to(['/cash/e-money/update', 'id' => $flowId]);
                break;
            case PlanCashFlows::tableName():
                if ($planFlow = PlanCashFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/e-money/update', 'id' => $flows['id'], 'is_plan_flow' => 1]);
                break;

            // FOREIGN
            case CashEmoneyForeignCurrencyFlows::tableName():
                $url = Url::to(['/cash/e-money/update', 'id' => $flowId, 'foreign' => 1]);
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                if ($planFlow = PlanCashForeignCurrencyFlows::findOne(['id' => $flows['id']]))
                    $url = Url::to(['/cash/e-money/update', 'id' => $flows['id'], 'is_plan_flow' => 1, 'foreign' => 1]);
                break;
        }

        if (!$returnFormatted)
            return $url;

        return \yii\helpers\Html::a($formattedFlowAmount, $url, [
            'title' => 'Изменить',
            'class' => 'update-flow-item link link-bleak' . ($isPlanFlow ? ' plan' : ''),
            'data' => [
                'toggle' => 'modal',
                'target' => '#update-movement',
                'pjax' => 0
            ],
        ]);
    }
}
