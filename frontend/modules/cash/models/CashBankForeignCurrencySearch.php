<?php

namespace frontend\modules\cash\models;

use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\components\FilterHelper;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class CashBankForeignCurrencySearch
 * @package frontend\modules\cash\models
 */
class CashBankForeignCurrencySearch extends CashBankForeignCurrencyFlows
{
    public $reason_id;
    private $_filterQuery;
    public $contractor_query;
    public $bik;
    public $currency_name;
    public $duplicates;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['expenditure_item_id', 'flow_type'], 'integer'],
            [['reason_id', 'contractor_query', 'rs'], 'string'],
            [['contractor_id', 'billPaying', 'bik'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSearchQuery()
    {
        return $this->_filterQuery ? clone $this->_filterQuery : null;
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'reason_id' => 'Тип',
            'billPaying' => 'Опл. счета',
        ]);
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return ActiveDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $query = $this->find()
            ->alias('cbf')
            ->joinWith('contractor')
            ->joinWith('invoices')
            ->andWhere(['cbf.company_id' => $this->company_id])
            ->groupBy('cbf.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'cbf.date' => SORT_ASC,
                            'cbf.created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.date' => SORT_DESC,
                            'cbf.created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'cbf.flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'cbf.amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'cbf.amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'cbf.flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'cbf.amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'cbf.flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'cbf.amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                    'billPaying' => [
                        'asc' => ['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC],
                        'desc' => ['invoice.document_number' => SORT_DESC, 'invoice.document_additional_number' => SORT_ASC],
                        'default' => SORT_ASC
                    ],
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if ($this->id) {
            $query->andWhere([
                'cbf.id' => $this->id,
            ]);
        } else {
            if ($this->flow_type == -1) {
                $this->flow_type = null;
            }
            if ($this->bik == 'all') {
                $this->bik = null;
            }
            $rs = $this->rs;
            $currency_id = null;
            if ($this->rs == 'all') {
                $rs = null;
            } elseif (in_array($this->rs, Currency::$foreignArray)) {
                $rs = null;
                $currency_id = Currency::find()->select('id')->where([
                    'name' => $this->rs,
                ])->scalar() ?? -1;
            }
            if ($this->bik) {
                $query->joinWith('checkingAccountants account')->andWhere([
                    'account.bik' => $this->bik,
                ]);
            }

            $query->andFilterWhere(['or',
                ['like', 'contractor.name', $this->contractor_query],
                ['like', 'contractor.ITN', $this->contractor_query],
            ]);
            $query->andFilterWhere([
                'cbf.rs' => $rs,
                'cbf.currency_id' => $currency_id,
                'cbf.flow_type' => $this->flow_type,
                'cbf.contractor_id' => $this->contractor_id,
            ]);

            if ($this->reason_id == 'empty') {
                $query->andWhere(['or',
                    ['and',
                        ['flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE],
                        ['cbf.expenditure_item_id' => null],
                    ],
                    ['and',
                        ['flow_type' => CashBankFlows::FLOW_TYPE_INCOME],
                        ['cbf.income_item_id' => null],
                    ],
                ]);
            } elseif (!empty($this->reason_id)) {
                if (substr($this->reason_id, 0, 2) === 'e_') {
                    $this->expenditure_item_id = substr($this->reason_id, 2);
                } elseif (substr($this->reason_id, 0, 2) === 'i_') {
                    $this->income_item_id = substr($this->reason_id, 2);
                }
                $query->andFilterWhere([
                    'cbf.expenditure_item_id' => $this->expenditure_item_id,
                    'cbf.income_item_id' => $this->income_item_id,
                ]);
            }
        }

        $this->_statisticQuery = clone $query;

        if (!$this->id) {
            $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
            $query->andWhere(['between', 'cbf.date', $dateRange['from'], $dateRange['to']]);

            if ($this->duplicates) {
                $query2 = new \yii\db\Query;
                $query2->select([
                    'date',
                    'amount',
                    'contractor_id',
                    'duplicates' => 'COUNT(*)'
                ])->from([
                    't' => clone $query,
                ])->groupBy('[[date]], [[amount]], [[contractor_id]]')->having('[[duplicates]] > 1');
                $query->innerJoin([
                    'tsum' => $query2,
                ], '
                    {{cbf}}.[[date]] = {{tsum}}.[[date]]
                    AND
                    {{cbf}}.[[amount]] = {{tsum}}.[[amount]]
                    AND
                    {{cbf}}.[[contractor_id]] = {{tsum}}.[[contractor_id]]
                ');
            }
        }

        $this->_filterQuery = clone $query;

        return $dataProvider;
    }

    /**
     * @return array
     */
    public static function getReasonDropDown()
    {
        $result = [];

        // The next code searches for distinct and existing operation's type
        $expen_items = Yii::$app->db->createCommand("SELECT DISTINCT iei.id, iei.name FROM cash_bank_flows cbf LEFT JOIN invoice_expenditure_item iei ON cbf.expenditure_item_id = iei.id  WHERE cbf.expenditure_item_id IS not NULL AND cbf.company_id = '" . Yii::$app->user->identity->company->id . "'")->queryAll();

        foreach ($expen_items as $item) {
            $result_items['e_' . $item['id']] = $item['name'];
        }

        $income_items = Yii::$app->db->createCommand("SELECT DISTINCT iii.name, iii.id FROM cash_bank_flows cbf LEFT JOIN invoice_income_item iii ON cbf.income_item_id = iii.id WHERE cbf.income_item_id IS not NULL AND cbf.company_id = '" . Yii::$app->user->identity->company->id . "'")->queryAll();

        foreach ($income_items as $item) {
            $result_items['i_' . $item['id']] = $item['name'];
        }

        if (!empty($result_items)) {
            return $result_items;
        }
        // Old code

        /** @var InvoiceExpenditureItem[] $items */
        $items = InvoiceExpenditureItem::find()->orderBy(['sort' => 'asc'])->all();

        if (!empty($items)) {
            foreach ($items as $item) {
                $result['e_' . $item->id] = $item->name;
            }
        }

        /** @var InvoiceIncomeItem[] $items */
        $items = InvoiceIncomeItem::find()->orderBy(['sort' => 'asc'])->all();

        if (!empty($items)) {
            foreach ($items as $item) {
                $result['i_' . $item->id] = $item->name;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $query = clone $this->_filterQuery;

        return FilterHelper::getContractorFilterItems($query);
    }

    /**
     * @return array
     */
    public function getReasonFilterItems()
    {
        $query = clone $this->_filterQuery;
        $e_reasonArray = $query
            ->distinct()
            ->select('CONCAT("e_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cbf`.`expenditure_item_id`')
            ->andWhere(['not', ['expenditure.id' => null]])
            ->createCommand()
            ->queryAll();

        $query = clone $this->_filterQuery;
        $i_reasonArray = $query
            ->distinct()
            ->select('CONCAT("i_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cbf`.`income_item_id`')
            ->andWhere(['not', ['income.id' => null]])
            ->createCommand()
            ->queryAll();

        $reasonArray = ArrayHelper::map(array_merge($e_reasonArray, $i_reasonArray), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    /**
     * @return bool
     */
    public function getIsExists()
    {
        return self::find()->andWhere([
            'company_id' => $this->company_id
        ])->andFilterWhere([
            'rs' => $this->rs
        ])->exists();
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return CheckingAccountant::find()->select('bank_name')->andWhere([
            'company_id' => $this->company_id,
            'rs' => $this->rs,
        ])->orderBy(['type' => SORT_ASC])->scalar();
    }
}
