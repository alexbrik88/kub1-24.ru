<?php

namespace frontend\modules\cash\models;

use Yii;
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class CashMoneySearch
 * @package frontend\modules\cash\models
 */
class CashFlowsProjectSearch extends Model
{
    const TYPE_PAYMENT_BANK = 1;
    const TYPE_PAYMENT_ORDER = 2;
    const TYPE_PAYMENT_EMONEY = 3;
    const TYPE_PAYMENT_ACQUIRING = 4;
    const TYPE_PAYMENT_CARD = 5;
    const TYPE_PLAN = "plan";

    const TYPE_IO_IN = 1;
    const TYPE_IO_OUT = 0;

    public $byNumber;
    public $search;

    public $project_id;
    public $reason_id;
    public $type;
    public $contractor_id;
    public $sale_point_id;
    public $industry_id;

    // show plan
    protected bool $_userConfigShowPlan = true;

    /**
     *
     */
    public function init()
    {
        $this->_userConfigShowPlan = !Yii::$app->user->identity->config->cash_index_hide_plan;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [[
                'type', // wallet type
            ], 'safe'],
        ]);
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return SqlDataProvider
     */
    public function search(array $params = [], $dateRange = null)
    {
        $dateRange = empty($dateRange) ? StatisticPeriod::getDefaultPeriod() : $dateRange;
        $nameFilter = [];
        $cashContractor = [];

        $this->load($params);

        $reasonArray = explode('_', $this->reason_id);

        if (isset($params['search']) && ($search = trim($params['search']))) {
            $nameFilter = ['or'];
            foreach (explode(' ', $search) as $namePart) {
                $cashContractor = array_merge($cashContractor,
                    CashContractorType::find()
                        ->asArray()
                        ->select('name')
                        ->where(['like', 'text', $namePart])
                        ->column());
                $nameFilter[] = ['like', Contractor::tableName() . '.name', $namePart];
            }
        }

        $cbfSelect = (new Query())
            ->select([
                'cbf.id as id',
                'cbf.date as date',
                new Expression('IF(cbf.is_prepaid_expense, null, cbf.recognition_date) recognition_date'),
                'cbf.flow_type as flow_type',
                'IF(cbf.tin_child_amount IS NULL, cbf.amount, cbf.tin_child_amount) as amount',
                'if(cbf.flow_type = 1, IF(cbf.tin_child_amount IS NULL, cbf.amount, cbf.tin_child_amount), 0) as amountIncome',
                'if(cbf.flow_type = 0, IF(cbf.tin_child_amount IS NULL, cbf.amount, cbf.tin_child_amount), 0) as amountExpense',
                'cbf.contractor_id as contractor_id',
                'cbf.company_id as company_id',
                'cbf.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                //'cbfi.invoice_id as invoice_id',
                //'if(cbfi.amount < cbf.amount, 1, 0) as invoice_partial',
                'cbf.income_item_id as reason_id',
                'cbf.project_id as project_id',
                'cbf.sale_point_id as sale_point_id',
                'cbf.industry_id as industry_id',
                'cbf.created_at as created_at',
                new Expression('CONCAT("1_", cbf.rs) AS wallet_id'),
                '1 as [[type]]',
            ])
            ->from('cash_bank_flows as cbf')
            //->leftJoin('cash_bank_flow_to_invoice as cbfi', 'cbf.id = cbfi.flow_id')
            ->leftJoin('contractor', 'cbf.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cbf`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cbf`.`income_item_id`')
            ->andWhere(['cbf.company_id' => $params['company_id']])
            ->andWhere(['cbf.project_id' => $params['project_id']])
            ->andWhere(['cbf.has_tin_children' => 0])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $cbfSelect->andFilterWhere(['cbf.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $cbfSelect->andFilterWhere(['cbf.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $cbfSelect->andFilterWhere(['in', 'cbf.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $cbfSelect->andFilterWhere(['cbf.contractor_id' => $this->contractor_id]);
        }

        $cofSelect = (new Query())
            ->select([
                'cof.id as id',
                'cof.date as date',
                new Expression('IF(cof.is_prepaid_expense, null, cof.recognition_date) recognition_date'),
                'cof.flow_type as flow_type',
                'cof.amount as amount',
                'if(cof.flow_type = 1, cof.amount, "") as amountIncome',
                'if(cof.flow_type = 0, cof.amount, "") as amountExpense',
                'cof.contractor_id as contractor_id',
                'cof.company_id as company_id',
                'cof.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                //'cofi.invoice_id as invoice_id',
                //'if(cofi.amount < cof.amount, 1, 0) as invoice_partial',
                'cof.income_item_id as reason_id',
                'cof.project_id as project_id',
                'cof.sale_point_id as sale_point_id',
                'cof.industry_id as industry_id',
                'cof.created_at as created_at',
                new Expression('CONCAT("2_", cof.cashbox_id) AS wallet_id'),
                '2 as [[type]]',
            ])
            ->from('cash_order_flows as cof')
            //->leftJoin('cash_order_flow_to_invoice as cofi', 'cof.id = cofi.flow_id')
            ->leftJoin('contractor', 'cof.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cof`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cof`.`income_item_id`')
            ->andWhere(['cof.company_id' => $params['company_id']])
            ->andWhere(['cof.project_id' => $params['project_id']])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $cofSelect->andFilterWhere(['cof.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $cofSelect->andFilterWhere(['cof.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $cofSelect->andFilterWhere(['in', 'cof.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $cofSelect->andFilterWhere(['cof.contractor_id' => $this->contractor_id]);
        }

        $cefSelect = (new Query())
            ->select([
                'p.id as id',
                'p.date as date',
                new Expression('IF(p.is_prepaid_expense, null, p.recognition_date) recognition_date'),
                'p.flow_type as flow_type',
                'p.amount as amount',
                'if(p.flow_type = 1, p.amount, "") as amountIncome',
                'if(p.flow_type = 0, p.amount, "") as amountExpense',
                'p.contractor_id as contractor_id',
                'p.company_id as company_id',
                'p.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                //'cefi.invoice_id as invoice_id',
                //'if(cefi.amount < p.amount, 1, 0) as invoice_partial',
                'p.income_item_id as reason_id',
                'p.project_id as project_id',
                'p.sale_point_id as sale_point_id',
                'p.industry_id as industry_id',
                'p.created_at as created_at',
                new Expression('CONCAT("3_", p.emoney_id) AS wallet_id'),
                '3 as [[type]]',
            ])
            ->from('cash_emoney_flows as p')
            //->leftJoin('cash_emoney_flow_to_invoice as cefi', 'p.id = cefi.flow_id')
            ->leftJoin('contractor', 'p.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `p`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `p`.`income_item_id`')
            ->andWhere(['p.company_id' => $params['company_id']])
            ->andWhere(['p.project_id' => $params['project_id']])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $cefSelect->andFilterWhere(['p.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $cefSelect->andFilterWhere(['p.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $cefSelect->andFilterWhere(['in', 'p.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $cefSelect->andFilterWhere(['p.contractor_id' => $this->contractor_id]);
        }

        $cafSelect = (new Query())
            ->select([
                'caf.id as id',
                'caf.date as date',
                'caf.recognition_date',
                'caf.flow_type as flow_type',
                'caf.amount as amount',
                'if(caf.flow_type = 1, caf.amount, "") as amountIncome',
                'if(caf.flow_type = 0, caf.amount, "") as amountExpense',
                'caf.contractor_id as contractor_id',
                'caf.company_id as company_id',
                'caf.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                //'null as [[invoice_id]]',
                //'0 as [[invoice_partial]]',
                'caf.income_item_id as reason_id',
                'caf.project_id as project_id',
                'caf.sale_point_id as sale_point_id',
                'caf.industry_id as industry_id',
                'null as [[created_at]]',
                new Expression('CONCAT("4_", caf.acquiring_identifier) AS wallet_id'),
                '4 as [[type]]',
            ])
            ->from('acquiring_operation as caf')
            ->leftJoin('contractor', 'caf.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `caf`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `caf`.`income_item_id`')
            ->andWhere(['caf.company_id' => $params['company_id']])
            ->andWhere(['caf.project_id' => $params['project_id']])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $cafSelect->andFilterWhere(['caf.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $cafSelect->andFilterWhere(['caf.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $cafSelect->andFilterWhere(['in', 'caf.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $cafSelect->andFilterWhere(['caf.contractor_id' => $this->contractor_id]);
        }

        $ccfSelect = (new Query())
            ->select([
                'ccf.id as id',
                'ccf.date as date',
                'ccf.recognition_date',
                'ccf.flow_type as flow_type',
                'ccf.amount as amount',
                'if(ccf.flow_type = 1, ccf.amount, "") as amountIncome',
                'if(ccf.flow_type = 0, ccf.amount, "") as amountExpense',
                'ccf.contractor_id as contractor_id',
                'ccf.company_id as company_id',
                'ccf.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                //'null as [[invoice_id]]',
                 //'0 as [[invoice_partial]]',
                'ccf.income_item_id as reason_id',
                'ccf.project_id as project_id',
                'ccf.sale_point_id as sale_point_id',
                'ccf.industry_id as industry_id',
                'null as [[created_at]]',
                new Expression('CONCAT("5_", ccf.account_id) AS wallet_id'),
                '5 as [[type]]',
            ])
            ->from('card_operation as ccf')
            ->leftJoin('contractor', 'ccf.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `ccf`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `ccf`.`income_item_id`')
            ->andWhere(['ccf.company_id' => $params['company_id']])
            ->andWhere(['ccf.project_id' => $params['project_id']])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $ccfSelect->andFilterWhere(['ccf.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $ccfSelect->andFilterWhere(['ccf.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $ccfSelect->andFilterWhere(['in', 'ccf.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $ccfSelect->andFilterWhere(['ccf.contractor_id' => $this->contractor_id]);
        }

        // PLAN
        $planSelect = (new Query())
            ->select([
                'p.id as id',
                'p.date as date',
                'p.date AS [[recognition_date]]',
                'p.flow_type as flow_type',
                'p.amount as amount',
                'if(p.flow_type = 1, p.amount, "") as amountIncome',
                'if(p.flow_type = 0, p.amount, "") as amountExpense',
                'p.contractor_id as contractor_id',
                'p.company_id as company_id',
                'p.description as description',
                'expenditure.name as expenseName',
                'income.name as incomeName',
                'p.income_item_id as reason_id',
                'p.project_id as project_id',
                'p.sale_point_id as sale_point_id',
                'p.industry_id as industry_id',
                'p.created_at as created_at',
                'CONCAT("plan_", p.payment_type) as [[wallet_id]]',
                '"plan" AS [[type]]',
            ])
            ->from('plan_cash_flows as p')
            ->leftJoin('contractor', 'p.contractor_id = contractor.id')
            ->leftJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `p`.`expenditure_item_id`')
            ->leftJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `p`.`income_item_id`')
            ->andWhere(['p.company_id' => $params['company_id']])
            ->andWhere(['p.project_id' => $params['project_id']])
            ->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']])
            ->andFilterWhere(['p.payment_type' => $this->type])
            ->andFilterWhere($nameFilter);

        if (count((array)$reasonArray) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
            if ($prefix[0] === 'i') {
                $cefSelect->andFilterWhere(['p.income_item_id' => intval($reason)]);
            } else if ($prefix[0] === 'e') {
                $cefSelect->andFilterWhere(['p.expenditure_item_id' => intval($reason)]);
            }
        }

        if (!empty($cashContractor)) {
            $cefSelect->andFilterWhere(['in', 'p.contractor_id', $cashContractor]);
        }

        if (!empty($this->contractor_id)) {
            $cefSelect->andFilterWhere(['p.contractor_id' => $this->contractor_id]);
        }

        // WITHOUT PLAN
        if (!($this->_userConfigShowPlan || $this->isPlan())) {
            if ($this->type == self::TYPE_PAYMENT_BANK) {
                $query = $cbfSelect;
            } elseif ($this->type == self::TYPE_PAYMENT_ORDER) {
                $query = $cofSelect;
            } elseif ($this->type == self::TYPE_PAYMENT_EMONEY) {
                $query = $cefSelect;
            } elseif ($this->type == self::TYPE_PAYMENT_ACQUIRING) {
                $query = $cafSelect;
            } elseif ($this->type == self::TYPE_PAYMENT_CARD) {
                $query = $ccfSelect;
            } elseif ($this->type == self::TYPE_PLAN) {
                $query = $planSelect;
            } else {
                $query = $cbfSelect
                    ->union($cofSelect, true)
                    ->union($cefSelect, true)
                    ->union($cafSelect, true)
                    ->union($ccfSelect, true);
            }
        // WITH PLAN
        }  else {
            if ($this->type == self::TYPE_PAYMENT_BANK) {
                $query = $cbfSelect->union($planSelect, true);
            } elseif ($this->type == self::TYPE_PAYMENT_ORDER) {
                $query = $cofSelect->union($planSelect, true);
            } elseif ($this->type == self::TYPE_PAYMENT_EMONEY) {
                $query = $cefSelect->union($planSelect, true);
            } elseif ($this->type == self::TYPE_PAYMENT_ACQUIRING) {
                $query = $cafSelect->union($planSelect, true);
            } elseif ($this->type == self::TYPE_PAYMENT_CARD) {
                $query = $ccfSelect->union($planSelect, true);
            } elseif ($this->type == self::TYPE_PLAN) {
                $query = $planSelect;
            } else {
                $query = $cbfSelect
                    ->union($cofSelect, true)
                    ->union($cefSelect, true)
                    ->union($cafSelect, true)
                    ->union($ccfSelect, true)
                    ->union($planSelect, true);
            }
        }

        ArrayHelper::multisort($array, 'created_at', SORT_DESC);

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => [
                            'date' => SORT_ASC,
                            'created_at' => SORT_ASC,
                        ],
                        'desc' => [
                            'date' => SORT_DESC,
                            'created_at' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => ['date' => SORT_DESC],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        $tContractor = Contractor::tableName();

        $cbfSelect = (new Query())
            ->distinct()
            ->select('cbf.contractor_id')
            ->from('cash_bank_flows as cbf')
            ->andWhere(['cbf.project_id' => $this->project_id]);

        $cofSelect = (new Query())
            ->distinct()
            ->select('cof.contractor_id')
            ->from('cash_bank_flows as cof')
            ->andWhere(['cof.project_id' => $this->project_id]);

        $cefSelect = (new Query())
            ->distinct()
            ->select('p.contractor_id')
            ->from('cash_emoney_flows as p')
            ->andWhere(['p.project_id' => $this->project_id]);

        $cafSelect = (new Query())
            ->distinct()
            ->select('caf.contractor_id')
            ->from('acquiring_operation as caf')
            ->andWhere(['caf.project_id' => $this->project_id]);

        $ccfSelect = (new Query())
            ->distinct()
            ->select('ccf.contractor_id')
            ->from('card_operation as ccf')
            ->andWhere(['ccf.project_id' => $this->project_id]);

        $contractorIds = [];

        $contractorsArray = array_unique(array_merge($cbfSelect->all(), $cofSelect->all(), $cefSelect->all(), $cafSelect->all(), $ccfSelect->all()), SORT_REGULAR);
        foreach ($contractorsArray as $contractors) {
            $contractorIds[] = $contractors['contractor_id'];
        }

        $contractorsArray1 = [];
        /** @var CashContractorType $contractor */
        foreach(CashContractorType::find()
                ->where(['in', 'id', $contractorIds])
                ->all() as $contractor){
            $contractorsArray1[$contractor->name] = $contractor->text;
        }

        $contractorArray2 = [];
        /** @var Contractor $contractor */
        foreach (Contractor::find()
                ->andWhere(["$tContractor.id" => $contractorIds])
                ->all() as $contractor) {
            $contractorArray2[$contractor->id] = $contractor->nameWithType;
        }

        return (['' => 'Все контрагенты'] + array_replace($contractorsArray1, $contractorArray2));
    }

    /**
     * @param $type
     * @return array
     */
    public function getReasonFilterItems()
    {
        $prefix = '';
        $reason = '';

        if (count((array)$this->reason_id) >= 2) {
            list($prefix, $reason) = explode('_', $this->reason_id);
        }

        $eo_reasonArray = (new Query())
            ->distinct()
            ->select('CONCAT("eo_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->from('cash_order_flows as cof')
            ->innerJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cof`.`expenditure_item_id`');

        if ($reason) {
            $eo_reasonArray->andFilterWhere(['`expenditure`.id' => $reason]);
        }

        $io_reasonArray = (new Query())
            ->distinct()
            ->select('CONCAT("io_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->from('cash_order_flows as cof')
            ->innerJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cof`.`income_item_id`');

        if ($reason) {
            $io_reasonArray->andFilterWhere(['`income`.id' => $reason]);
        }

        $eb_reasonArray = (new Query())
            ->distinct()
            ->select('CONCAT("eb_", `expenditure`.`id`) `reason_id`, `expenditure`.`name` `reason_name`')
            ->from('cash_bank_flows as cbf')
            ->innerJoin(InvoiceExpenditureItem::tableName() . ' `expenditure`', '`expenditure`.`id` = `cbf`.`expenditure_item_id`');

        if ($reason) {
            $eb_reasonArray->andFilterWhere(['`expenditure`.id' => $reason]);
        }

        $ib_reasonArray = (new Query())
            ->distinct()
            ->select('CONCAT("ib_", `income`.`id`) `reason_id`, `income`.`name` `reason_name`')
            ->from('cash_bank_flows as cbf')
            ->innerJoin(InvoiceIncomeItem::tableName() . ' `income`', '`income`.`id` = `cbf`.`income_item_id`');

        if ($reason) {
            $ib_reasonArray->andFilterWhere(['`income`.id' => $reason]);
        }

        $reasonArray = ArrayHelper::map(array_merge($eo_reasonArray->all(),
            $io_reasonArray->all(),
            $eb_reasonArray->all(),
            $ib_reasonArray->all()), 'reason_id', 'reason_name');
        natsort($reasonArray);

        return $reasonArray;
    }

    public function getProjectFilterItems()
    {
        return []; // todo
    }

    public function getSalePointFilterItems()
    {
        return []; // todo
    }

    public function getCompanyIndustryFilterItems()
    {
        return []; // todo
    }

    public function isPlan()
    {
        return false; // todo
    }

    public function load($params, $forName = null)
    {
        foreach ($params as $property => $param) {
            if (is_array($param)) {
                foreach ($param as $item => $value){
                    if (property_exists($this, $item)) {
                        $this->{$item} = $value;
                    }
                }
            } else if (property_exists($this, $property)) {
                $this->{$property} = $param;
            }
        }

    }
}
