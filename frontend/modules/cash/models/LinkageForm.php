<?php

namespace frontend\modules\cash\models;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashFlowLinkage;
use common\models\company\CheckingAccountant;

/**
 * This is the model class for table "cash_flow_linkage".
 *
 * @property int $id
 * @property int $company_id
 * @property int $number
 * @property int $status
 * @property int $is_deleted
 * @property int $if_flow_type
 * @property int $if_wallet
 * @property string $if_wallet_table
 * @property int $if_contractor_id
 * @property int $then_flow_type
 * @property int $then_wallet_type
 * @property int $then_wallet_id
 * @property int $thenWallet
 * @property int $then_contractor_id
 * @property int $then_date_diff
 * @property float $then_amount_action_value
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Company $company
 */
class LinkageForm extends \yii\base\Model
{
    public $if_flow_type;
    public $if_wallet_id;
    public $if_contractor_id;
    public $then_flow_type;
    public $then_wallet_type;
    public $then_wallet_id;
    public $then_contractor_id;
    public $then_date_diff;
    public $then_amount_action;
    public $then_amount_action_value;
    public $then_description;

    public static $contractorLabels = [
        CashFlowsBase::FLOW_TYPE_INCOME => 'Покупатель',
        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Поставщик',
    ];

    private Company $company;
    private CashFlowLinkage $model;
    private $data = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(Company $company, ?CashFlowLinkage $model = null, ?array $config = [])
    {
        $this->company = $company;
        $this->model = $model ?: $this->newModel();

        $this->load($this->model->attributes, '');

        parent::__construct($config);
    }

    private function newModel() : CashFlowLinkage
    {
        $model = new CashFlowLinkage;
        $model->company_id = $this->company->id;
        $model->status = CashFlowLinkage::STATUS_ACTIVE;
        $model->if_flow_type = CashFlowsBase::FLOW_TYPE_EXPENSE;
        $model->then_flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        $model->if_wallet_id = array_key_first($this->getIfWalletItems());
        $model->thenWalletId = array_key_first($this->getThenWalletItems());
        $model->then_date_diff = array_key_first($this->getDateDiffItems());

        return $model;
    }

    public function setThenWalletId($value)
    {
        $value = strval($value);

        $this->then_wallet_type = substr($value, 0, 1) ?: null;
        $this->then_wallet_id = substr($value, 2) ?: null;
    }

    public function getThenWalletId()
    {
        return sprintf('%s_%s', $this->then_wallet_type, $this->then_wallet_id);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['then_amount_action_value'],
                'filter',
                'filter' => function ($value) {
                    return str_replace(',', '.', $value);
                }
            ],
            [['then_description'], 'trim'],
            [
                [
                    'if_flow_type',
                    'if_wallet_id',
                    'if_contractor_id',
                    'then_flow_type',
                    'thenWalletId',
                    'then_contractor_id',
                    'then_date_diff',
                    'then_amount_action',
                    'then_amount_action_value',
                    'then_description',
                ], 
                'required',
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'if_flow_type',
                    'if_wallet_id',
                    'then_flow_type',
                    'then_wallet_type',
                    'then_wallet_id',
                    'then_amount_action',
                ],
                'integer',
            ],
            [['then_description'], 'string', 'max' => 255],
            [
                [
                    'if_flow_type',
                    'then_flow_type',
                ],
                'in',
                'range' => [
                    CashFlowsBase::FLOW_TYPE_INCOME,
                    CashFlowsBase::FLOW_TYPE_EXPENSE,
                ],
            ],
            [
                [
                    'then_amount_action',
                ],
                'in',
                'range' => [
                    CashFlowLinkage::ACTION_MINUS_PERCENT,
                    CashFlowLinkage::ACTION_TAKE_PERCENT,
                ],
            ],
            [['then_date_diff'], 'integer', 'min' => 0, 'max' => 10],
            [['then_amount_action_value'], 'number', 'min' => 0, 'max' => 99.99],
            [
                [
                    'if_wallet_id',
                ], 
                'in',
                'range' => array_keys($this->getIfWalletItems()),
            ],
            [
                [
                    'then_wallet_type',
                ],
                'in',
                'range' => CashFlowLinkage::$thenWallets,
            ],
            [
                [
                    'then_wallet_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
                'when' => function ($model) {
                    return $model->then_wallet_type == CashFlowsBase::WALLET_BANK;
                }
            ],
            [
                [
                    'then_wallet_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cashbox::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
                'when' => function ($model) {
                    return $model->then_wallet_type == CashFlowsBase::WALLET_CASHBOX;
                }
            ],
            [
                [
                    'if_contractor_id',
                    'then_contractor_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company->id],
            ],
            [
                ['if_contractor_id'],
                'unique',
                'targetClass' => CashFlowLinkage::class,
                'targetAttribute' => [
                    'if_flow_type',
                    'if_wallet_id',
                    'if_contractor_id',
                ],
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->company->id,
                        'is_deleted' => false,
                    ])->andFilterWhere([
                        'not',
                        ['id' => $this->model->id],
                    ]);
                },
                'message' => 'Связка для такой операции уже существует',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'number' => '№№',
            'if_flow_type' => 'Тип',
            'if_wallet_id' => 'Тип кошелька',
            'if_contractor_id' => 'Контрагент',
            'then_flow_type' => 'Тип',
            'thenWalletId' => 'Тип кошелька',
            'then_contractor_id' => 'Контрагент',
            'then_date_diff' => 'Дата',
            'then_amount_action' => 'Операция',
            'then_amount_action_value' => '%',
            'then_description' => 'Назначение',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return array
     */
    public function getFlowTypeItems()
    {
        return CashFlowLinkage::$flowTypeItems;
    }

    /**
     * @return array
     */
    public function getIfWalletItems()
    {
        return [
            CashFlowsBase::WALLET_BANK => 'Банк',
        ];
    }

    /**
     * @return array
     */
    public function getThenWalletItems()
    {
        $items = [];
        foreach ($this->getCheckingAccountants() as $key => $model) {
            $items[CashFlowsBase::WALLET_BANK.'_'.$key] = $model->getIsForeign() ? $model->currencyName.' '.$model->name : $model->name;
        }
        foreach ($this->getCashboxes() as $key => $model) {
            $items[CashFlowsBase::WALLET_CASHBOX.'_'.$key] = $model->getIsForeign() ? $model->currencyName.' '.$model->name : $model->name;
        }

        return $items;
    }

    /**
     * @return array
     */
    public function getDateDiffItems()
    {
        return CashFlowLinkage::dateDiffItems();
    }

    /**
     * @return array
     */
    public function getAmountActionItems()
    {
        return CashFlowLinkage::$thenActionItems;
    }

    /**
     * @return array
     */
    public function getContractorLabel($flowType)
    {
        return self::$contractorLabels[$flowType] ?? '';
    }

    /**
     * @return array
     */
    public function getCheckingAccountants()
    {
        if (!array_key_exists('getCheckingAccountants', $this->data)) {
            $rubQuery = $this->company->getRubleAccounts()->andWhere([
                'type' => [
                    CheckingAccountant::TYPE_MAIN,
                    CheckingAccountant::TYPE_ADDITIONAL,
                ],
            ])->orderBy([
                'type' => SORT_ASC,
            ]);
            /*
            $foreignQuery = $this->company->getForeignCurrencyAccounts()->andWhere([
                'type' => [
                    CheckingAccountant::TYPE_MAIN,
                    CheckingAccountant::TYPE_ADDITIONAL,
                ],
            ])->orderBy([
                'currency_id' => SORT_ASC,
                'type' => SORT_ASC,
            ]);
            $accounts = array_merge($rubQuery->all(), $foreignQuery->all());
            */
            $accounts = $rubQuery->all();
            $this->data['getCheckingAccountants'] = [];
            foreach ($accounts as $item) {
                $this->data['getCheckingAccountants'][$item->id] = $item;
            }
        }

        return $this->data['getCheckingAccountants'];
    }

    /**
     * @return array
     */
    public function getCashboxes()
    {
        if (!array_key_exists('getCashboxes', $this->data)) {
            $rubQuery = $this->company->getRubleCashboxes()->andWhere([
                'is_closed' => false,
            ])->orderBy([
                'is_main' => SORT_DESC,
            ]);
            /*
            $foreignQuery = $this->company->getForeignCurrencyCashboxes()->andWhere([
                'is_closed' => false,
            ])->orderBy([
                'currency_id' => SORT_ASC,
                'is_main' => SORT_DESC,
            ]);
            $accounts = array_merge($rubQuery->all(), $foreignQuery->all());
            */
            $accounts = $rubQuery->all();
            $this->data['getCashboxes'] = [];
            foreach ($accounts as $item) {
                $this->data['getCashboxes'][$item->id] = $item;
            }
        }

        return $this->data['getCashboxes'];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->model->load($this->attributes, '');

        if ($this->model->save()) {
            return true;
        }

        \common\components\helpers\ModelHelper::logErrors($this->model, __METHOD__);

        return false;
    }
}
