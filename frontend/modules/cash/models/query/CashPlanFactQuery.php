<?php
namespace frontend\modules\cash\models\query;

use common\models\company\CompanyType;
use yii\db\Query;

class CashPlanFactQuery extends Query {

    private $_alias;

    public function getTableAlias() {
        return $this->_alias;
    }

    public function alias($alias) {
        $this->_alias = $alias;
        return $this;
    }

    public function byFlowType($flowType)
    {
        return $this->andWhere([
            $this->getTableAlias().'.flow_type' => $flowType,
        ]);
    }

    public function byCompany($companyId)
    {
        return $this->andWhere([
            $this->getTableAlias().'.company_id' => $companyId,
        ]);
    }

    public function byContractorName($name)
    {
        $nameParts = explode(' ', preg_replace('/\s{2,}/', ' ', trim($name)));

        if (count($nameParts) > 1) {

            foreach ($nameParts as $key => $namePart) {

                $companyTypeId = CompanyType::find()
                    ->select('id')
                    ->andWhere(['name_short' => $namePart])
                    ->scalar();

                if ($companyTypeId) {
                    $this->andWhere(['contractor.company_type_id' => $companyTypeId]);
                    unset($nameParts[$key]);
                    break;
                }
            }
        }

        return $this->andWhere(['or',
            ['like', 'contractor.ITN', $name],
            ['like', 'contractor.name', $name],
            ['like', 'contractor.name', implode(' ', $nameParts)],
        ]);
    }
}