<?php

namespace frontend\modules\cash\models\multiWallet;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\Company;
use common\models\currency\Currency;
use common\models\employee\Employee;
use yii\db\Expression;
use yii\db\Query;

class AccountSelectHelper {

    const FAVORITES_COUNT = 3;

    // ACCOUNTS LISTS

    public static function getBankList(Employee $user, $indexBy = 'rs')
    {
        return $user->company->getCheckingAccountants()->orderBy([
            'type' => SORT_ASC,
            'rs' => SORT_ASC,
        ])->indexBy($indexBy)->all();
    }

    public static function getCashboxList(Employee $user)
    {
        return $user->getCashboxes()->orderBy([
            'is_main' => SORT_DESC,
            'is_closed' => SORT_ASC,
            'name' => SORT_ASC,
        ])->indexBy('id')->all();
    }

    public static function getEmoneyList(Employee $user)
    {
        return $user->company->getEmoneys()->orderBy([
            'is_main' => SORT_DESC,
            'is_closed' => SORT_ASC,
            'name' => SORT_ASC,
        ])->indexBy('id')->all();
    }

    // BALANCE LISTS
    
    public static function getBankBalanceList(Employee $user, $groupBy = 'rs')
    {
        return self::_getBalanceList([
            'company_ids' => [$user->company->id], // todo
            'table' => CashBankFlows::tableName(),
            'tableForeign' => CashBankForeignCurrencyFlows::tableName(),
            'groupBy' => $groupBy
        ]);
    }

    public static function getCashboxBalanceList(Employee $user)
    {
        return self::_getBalanceList([
            'company_ids' => [$user->company->id], // todo            
            'table' => CashOrderFlows::tableName(),
            'tableForeign' => CashOrderForeignCurrencyFlows::tableName(),
            'groupBy' => 'cashbox_id'
        ]);
    }

    public static function getEmoneyBalanceList(Employee $user)
    {
        return self::_getBalanceList([
            'company_ids' => [$user->company->id], // todo
            'table' => CashEmoneyFlows::tableName(),
            'tableForeign' => CashEmoneyForeignCurrencyFlows::tableName(),
            'groupBy' => 'emoney_id'
        ]);
    }

    private static function _getBalanceList($params = [])
    {
        return (new Query)->from(
            (new Query)
            ->from($params['table'])
            ->where(['company_id' => $params['company_ids']])
            ->groupBy($params['groupBy'])
            ->select(new Expression('SUM(CASE WHEN flow_type = 1 THEN amount ELSE -amount END) balance, ' . $params['groupBy']))
            ->union(
                (new Query)
                ->from($params['tableForeign'])
                ->where(['company_id' => $params['company_ids']])
                ->groupBy($params['groupBy'])
                ->select(new Expression('SUM(CASE WHEN flow_type = 1 THEN amount ELSE -amount END) balance, ' . $params['groupBy'])),
            true)
        )
        ->indexBy($params['groupBy'])
        ->column();
    }

    // SORT BY RATE LIST

    public static function getBankSortList(Employee $user, $groupBy = 'rs')
    {
        return self::_getSortList([
            'company_ids' => [$user->company->id], // todo
            'table' => CashBankFlows::tableName(),
            'tableForeign' => CashBankForeignCurrencyFlows::tableName(),
            'groupBy' => $groupBy
        ]);
    }

    public static function getCashboxSortList(Employee $user)
    {
        return self::_getSortList([
            'company_ids' => [$user->company->id], // todo
            'table' => CashOrderFlows::tableName(),
            'tableForeign' => CashOrderForeignCurrencyFlows::tableName(),
            'groupBy' => 'cashbox_id'
        ]);
    }

    public static function getEmoneySortList(Employee $user)
    {
        return self::_getSortList([
            'company_ids' => [$user->company->id], // todo
            'table' => CashEmoneyFlows::tableName(),
            'tableForeign' => CashEmoneyForeignCurrencyFlows::tableName(),
            'groupBy' => 'emoney_id'
        ]);
    }

    private static function _getSortList($params = [])
    {
        return (new Query)->from(
            (new Query)
            ->from($params['table'])
            ->where(['company_id' => $params['company_ids']])
            ->andWhere(new Expression($params['table'] . '.date > DATE_SUB(NOW(), INTERVAL 10 day)'))
            ->groupBy($params['groupBy'])
            ->orderBy(['cnt' => SORT_DESC])
            ->select(new Expression('COUNT(*) cnt, ' . $params['groupBy']))
            ->union(
                (new Query)
                ->from($params['tableForeign'])
                ->where(['company_id' => $params['company_ids']])
                ->andWhere(new Expression($params['tableForeign'] . '.date > DATE_SUB(NOW(), INTERVAL 10 day)'))
                ->groupBy($params['groupBy'])
                ->orderBy(['cnt' => SORT_DESC])
                ->select(new Expression('COUNT(*) cnt, ' . $params['groupBy'])))
        )
        ->indexBy($params['groupBy'])
        ->column();
    }

    // OTHER

    public static function getItemName($itemName, $currencyName)
    {
        return ($currencyName == Currency::DEFAULT_NAME) ? $itemName : ($currencyName . ' ' . $itemName);
    }
}