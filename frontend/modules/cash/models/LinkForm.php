<?php

namespace frontend\modules\cash\models;


use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Class LinkForm
 * @package frontend\modules\cash\models
 */
class LinkForm extends \yii\base\Model
{
    public $flow_ids;
    public $invoice_ids;

    public $flowClass;
    public $linkClass;
    public $invoiceClass;

    private $_company;

    /**
     * @inheritdoc
     */
    public function __construct(Company $company,  $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flow_ids', 'invoice_ids'], 'safe'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'ids' => 'ids',
        ];
    }

    /**
     * @inheritdoc
     */
    public function query($flowType)
    {
        $invoiceType = $flowType == CashBankFlows::FLOW_TYPE_INCOME ? Documents::IO_TYPE_OUT : Documents::IO_TYPE_IN;
        $contractorType = $flowType == CashBankFlows::FLOW_TYPE_INCOME ? Contractor::TYPE_CUSTOMER : Contractor::TYPE_SELLER;
        $period = StatisticPeriod::getSessionPeriod();
        $statuses = array_diff(InvoiceStatus::$validInvoices, [
            InvoiceStatus::STATUS_PAYED,
            InvoiceStatus::STATUS_REJECTED,
        ]);
        $subQuery = $this->flowClass::find()->alias('unlinked')
            ->select([
                'unlinked.*',
                'paid_sum' => 'IFNULL(SUM({{paid}}.[[amount]]), 0)',
            ])
            ->leftJoin([
                'contractor' => Contractor::tableName(),
            ], '{{contractor}}.[[id]]={{unlinked}}.[[contractor_id]]')
            ->leftJoin([
                'paid' => $this->linkClass::tableName(),
            ], '{{paid}}.[[flow_id]]={{unlinked}}.[[id]]')
            ->andWhere([
                'unlinked.flow_type' => $flowType,
                'unlinked.company_id' => $this->_company->id,
            ])
            ->andWhere([
                'not',
                ['contractor.id' => null],
            ])
            ->andWhere([
                'between',
                'unlinked.date',
                $period['from'],
                $period['to'],
            ])
            ->having('{{unlinked}}.[[amount]] > [[paid_sum]]')
            ->groupBy('unlinked.id');

        $query = $this->flowClass::find()->select([
                'flow.*',
                'invoice_ids' => 'GROUP_CONCAT({{invoice}}.[[id]] ORDER BY {{invoice}}.[[document_date]] ASC SEPARATOR ",")'
            ])->from([
                'flow' => $subQuery,
            ])
            ->innerJoin([
                'invoice' => $this->invoiceClass::tableName(),
            ], '{{invoice}}.[[contractor_id]]={{flow}}.[[contractor_id]]')
            ->leftJoin([
                'link' => $this->linkClass::tableName(),
            ], '{{link}}.[[invoice_id]]={{invoice}}.[[id]] AND {{link}}.[[flow_id]]={{flow}}.[[id]]')
            ->andWhere([
                'invoice.type' => $invoiceType,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => $statuses,
                'invoice.company_id' => $this->_company->id,
                'link.flow_id' => null,
            ])
            ->andWhere([
                'not',
                ['invoice.id' => null],
            ])
            ->groupBy('flow.id');
        if ($this->invoiceClass == ForeignCurrencyInvoice::class) {
            $query->andWhere('{{invoice}}.[[currency_name]]={{flow}}.[[currency_name]]');
        }

        return $query;
    }

    /**
     * @param array $params
     * @param array $dateRange
     * @return ActiveDataProvider
     */
    public function formData()
    {
        $typeArray = [
            CashFlowsBase::FLOW_TYPE_INCOME,
            CashFlowsBase::FLOW_TYPE_EXPENSE,
        ];
        $formData = [];

        foreach ($typeArray as $flowType) {
            $formData[$flowType] = [];
            $flowArray = $this->query($flowType)->all();
            foreach ($flowArray as $flow) {
                if (!empty($flow->invoice_ids)) {
                    $invoices = $this->invoiceClass::find()->where(['id' => explode(',', $flow->invoice_ids)])->all();
                    list($paidInvoices, $unpaidInvoices) = VidimusUploader::findPaidInvoices(
                        $invoices,
                        $flow->description,
                        $flow->amount - $flow->paid_sum,
                        date_create($flow->date) ?: null
                    );
                    if (!empty($paidInvoices) || !empty($unpaidInvoices)) {
                        $formData[$flowType][] = [
                            'flow' => $flow,
                            'paidInvoices' => $paidInvoices,
                            'unpaidInvoices' => $unpaidInvoices,
                        ];
                    }
                }
            }
        }

        return $formData;
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if (!is_array($this->invoice_ids) || empty($this->invoice_ids)) {
            return;
        }
        $flowArray = $this->flowClass::find()->andWhere([
            'id' => array_keys($this->invoice_ids),
            'company_id' => $this->_company->id,
        ])->all();
        if ($flowArray) {
            $statuses = array_diff(InvoiceStatus::$validInvoices, [
                InvoiceStatus::STATUS_PAYED,
                InvoiceStatus::STATUS_REJECTED,
            ]);
            foreach ($flowArray as $flow) {
                $type = $flow->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? Documents::IO_TYPE_OUT :  Documents::IO_TYPE_IN;
                $invoiceIds = $this->invoice_ids[$flow->id] ?? [];
                if (!empty($invoiceIds)) {
                    $query = $this->invoiceClass::find()->andWhere([
                        'id' => $invoiceIds,
                        'type' => $type,
                        'company_id' => $this->_company->id,
                        'contractor_id' => $flow->contractor_id,
                        'is_deleted' => false,
                        'invoice_status_id' => $statuses,
                    ]);
                    if ($flow instanceof CashBankForeignCurrencyFlows) {
                        $query->andWhere([
                            'currency_name' => $flow->currency_name,
                        ]);
                    }
                    foreach ($query->indexBy('id')->all() as $invoice) {
                        $flow->linkInvoice($invoice);
                    }
                }
            }
        }
    }
}
