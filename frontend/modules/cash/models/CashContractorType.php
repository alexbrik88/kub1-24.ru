<?php

namespace frontend\modules\cash\models;

use common\models\employee\Employee;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cash_contractor_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 */
class CashContractorType extends ActiveRecord
{
    /**
     *
     */
    const BANK = 1;
    const ORDER = 2;
    const EMONEY = 3;
    const BALANCE = 4;
    const COMPANY = 5;
    const BANK_CURRENCY = 7;
    const ORDER_CURRENCY = 8;
    const EMONEY_CURRENCY = 9;

    /**
     *
     */
    const BANK_TEXT = 'bank';
    const ORDER_TEXT = 'order';
    const EMONEY_TEXT = 'emoney';
    const BALANCE_TEXT = 'balance';
    const COMPANY_TEXT = 'company';
    const CUSTOMERS_TEXT = 'customers';
    const BANK_CURRENCY_TEXT = 'bank_currency';
    const ORDER_CURRENCY_TEXT = 'order_currency';
    const EMONEY_CURRENCY_TEXT = 'emoney_currency';

    public static $inverseTypes = [
        self::BANK_TEXT,
        self::ORDER_TEXT,
        self::EMONEY_TEXT,
    ];

    public static $internalTransferId = [
        self::BANK,
        self::ORDER,
        self::EMONEY,
        self::COMPANY,
        self::BANK_CURRENCY,
        self::ORDER_CURRENCY,
        self::EMONEY_CURRENCY,
    ];

    public static $internalTransferText = [
        self::BANK_TEXT,
        self::ORDER_TEXT,
        self::EMONEY_TEXT,
        self::COMPANY_TEXT,
        self::BANK_CURRENCY_TEXT,
        self::ORDER_CURRENCY_TEXT,
        self::EMONEY_CURRENCY_TEXT,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_contractor_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['name', 'text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->id == self::COMPANY &&
            is_a(Yii::$app, 'yii\web\Application') &&
            Yii::$app->user->identity instanceof Employee &&
            Yii::$app->user->identity->company !== null
        ) {
            $this->text = Yii::$app->user->identity->company->getTitle(true);
        }

        parent::afterFind();
    }

    public static function getNameById(?string $id): string
    {
        switch ($id) {
            case self::BANK_TEXT:
            case self::BANK_CURRENCY_TEXT:
                return 'Банк';
            case self::ORDER_TEXT:
            case self::ORDER_CURRENCY_TEXT:
                return 'Касса';
            case self::EMONEY_TEXT:
            case self::EMONEY_CURRENCY_TEXT:
                return 'E-money';
            case self::BALANCE_TEXT:
                return 'Баланс начальный';
            case self::COMPANY_TEXT:
                return (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company)
                    ? Yii::$app->user->identity->company->getTitle(true)
                    : 'Своя Компания';
            case self::CUSTOMERS_TEXT:
                return 'Физ. лица';
        }

        return '---';
    }
}
