<?php

namespace frontend\modules\cash\models;

use Yii;
use common\components\cash\InternalTransferHelper;
use common\components\cash\InternalTransferInterface;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;
use common\models\cash\form\CashBankFlowsForm;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\Company;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use yii\helpers\ArrayHelper;

/**
 * Class InternalTransferForm
 */
class InternalTransferForm extends \yii\base\Model
{
    /**
     * form attributes
     */
    public $account_from;
    public $account_to;
    public $amount_from;
    public $amount_to;
    public $date_from;
    public $date_to;
    public $date;
    public $description;

    public $amount_from_currency;
    public $amount_to_currency;
    public $amount_from_suffix;
    public $amount_to_suffix;
    public $rate_value;
    public $curr_1_name;
    public $curr_2_name;

    /**
     * Set true if you need to copy an existing transfer
     * @var boolean
     */
    public $copyMode = false;

    private $_company;
    private $_employee;
    private $_model;
    private $_init_model_from;
    private $_init_model_to;
    private $_result_model_from;
    private $_result_model_to;
    private $_accounts;
    private $_account_from;
    private $_account_to;
    private $_account_list;
    private $_account_list_data;
    private $_currency_list;
    private $_old_models = [];

    public static $accountClassList = [
        CheckingAccountant::class,
        Cashbox::class,
        Emoney::class,
    ];

    public function __construct(Company $company, Employee $employee, CashFlowsBase $model = null, array $config = [])
    {
        $this->_company = $company;
        $this->_employee = $employee;
        $this->_model = $model;

        parent::__construct($config);
    }

    public function init() : void
    {
        parent::init();

        $this->initAccounts();
        $this->initAttributes();
        $this->initLists();
    }

    public function getModel() : CashFlowsBase
    {
        return $this->_model;
    }

    public function attributeLabels() : array
    {
        return [
            'account_from' => 'Со счета',
            'account_to' => 'На счет',
            'amount_from' => 'Сумма списания',
            'amount_to' => 'Сумма зачисления',
            'date_from' => 'Дата списания',
            'date_to' => 'Дата зачисления',
            'date' => 'Дата операции',
            'description' => 'Назначение платежа',
        ];
    }

    public function rules() : array
    {
        return [
            [
                [
                    'amount_from',
                    'amount_to',
                    //'date_from',
                    //'date_to',
                    'date',
                    'description',
                ],
                'trim',
            ],
            [
                [
                    'amount_from',
                    'amount_to',
                    'rate_value',
                ],
                'filter',
                'filter' => function ($value) {
                    if (is_scalar($value)) {
                        $value = strtr($value, [',' => '.']);
                    }

                    return $value;
                }
            ],
            [
                [
                    'account_from',
                    'account_to',
                    'amount_from',
                    'date',
                ],
                'required',
            ],
            [
                [
                    'amount_to',
                ],
                'required',
                'when' => function ($model) {
                    return !empty($model->account_to);
                },
            ],
            [
                [
                    'rate_value',
                ],
                'required',
                'when' => function ($model) {
                    return $model->amount_from_currency != $model->amount_to_currency;
                },
            ],
            [
                [
                    'account_from',
                    'account_to',
                ],
                'in',
                'range' => array_keys($this->_accounts),
            ],
            [
                'account_to',
                'compare',
                'compareAttribute' => 'account_from',
                'operator' => '!=',
            ],
            [
                [
                    'amount_from',
                    'amount_to',
                ],
                'number',
                'numberPattern' => '/^[0-9]+(\.[0-9]{0,2})?$/'
            ],
            [
                [
                    //'date_from',
                    //'date_to',
                    'date',
                ],
                'date',
                'format' => 'php:d.m.Y',
            ],
            [['description'], 'string'],
        ];
    }

    public function initAccounts() : void
    {
        $query = $this->_company->getCheckingAccountants()->active(true)->orderBy([
            'type' => SORT_ASC,
            'rs' => SORT_ASC,
        ]);
        foreach ($query->all() as $account) {
            $this->_accounts[$account->getInternalTransferId()] = $account;
        }
        $query = ($this->_employee->getCashboxes($this->_company->id) ?? $this->_company->getCashboxes())->orderBy([
            'is_main' => SORT_DESC,
            'is_closed' => SORT_ASC,
            'name' => SORT_ASC,
        ]);
        foreach ($query->all() as $account) {
            $this->_accounts[$account->getInternalTransferId()] = $account;
        }
        $query = $this->_company->getEmoneys()->orderBy([
            'is_main' => SORT_DESC,
            'is_closed' => SORT_ASC,
            'name' => SORT_ASC,
        ]);
        foreach ($query->all() as $account) {
            $this->_accounts[$account->getInternalTransferId()] = $account;
        }
    }

    public function initAttributes()
    {
        if ($this->_model !== null) {
            $this->description = $this->_model->description;
            if ($this->_model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $this->_init_model_to = $this->_model;
                $this->_init_model_from = $this->_model->internalTransferFlow;
            } elseif ($this->_model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $this->_init_model_from = $this->_model;
                $this->_init_model_to = $this->_model->internalTransferFlow;
            }
            if ($this->_init_model_from !== null) {
                if ($account = $this->_init_model_from->selfAccount ?? null) {
                    $this->account_from = $account->getInternalTransferId();
                    $this->amount_from_currency = $account->getCurrencyName();
                    $this->amount_from_suffix = ', '.$this->amount_from_currency;
                    $this->_accounts[$this->account_from] = $account;
                }
                $this->amount_from = bcdiv($this->_init_model_from->amount, 100, 2);
                $this->date = ($d = date_create($this->_init_model_from->date)) ? $d->format('d.m.Y') : null;
            }
            if ($this->_init_model_to !== null) {
                if ($account = $this->_init_model_to->selfAccount ?? null) {
                    $this->account_to = $account->getInternalTransferId();
                    $this->amount_to_currency = $account->getCurrencyName();
                    $this->amount_to_suffix = ', '.$this->amount_to_currency;
                    $this->_accounts[$this->account_to] = $account;
                }
                $this->amount_to = bcdiv($this->_init_model_to->amount, 100, 2);
                $this->date = ($d = date_create($this->_init_model_to->date)) ? $d->format('d.m.Y') : null;
            }
            if ($this->_init_model_from !== null &&
                $this->_init_model_to !== null &&
                $this->amount_from_currency != $this->amount_to_currency
            ) {
                if ($this->amount_to_currency == Currency::DEFAULT_NAME) {
                    $rate_value = $this->amount_to / $this->amount_from;
                    $this->curr_1_name = $this->amount_to_currency;
                    $this->curr_2_name = $this->amount_from_currency;
                } else {
                    $rate_value = $this->amount_from / $this->amount_to;
                    $this->curr_1_name = $this->amount_from_currency;
                    $this->curr_2_name = $this->amount_to_currency;
                }
                $this->rate_value = round($rate_value, 4);
            }
        } else {
            $this->description = '';
        }
        if ($this->copyMode || empty($this->date)) {
            $this->date = date('d.m.Y');
        }
    }

    public function initLists()
    {
        foreach ($this->_accounts as $key => $value) {
            $currencyName = $value->getCurrencyName();
            $currencySymbol = ArrayHelper::getValue(Currency::$currencySymbols, $currencyName);
            $this->_account_list[$key] = $value->getInternalTransferName();
            $this->_account_list_data[$key] = [
                'data-currency' => $currencyName,
                'data-symbol' => $currencySymbol,
                'data-balance' => $value->getBalance(),
                'data-rs' => $value->rs ?? '',
                'data-label-suffix' => ', '.$currencyName,
            ];
            $this->_currency_list[$key] = $currencyName;
        }
    }

    public function getAccountList()
    {
        return $this->_account_list;
    }

    public function getAccountListData()
    {
        return $this->_account_list_data;
    }

    public function getCurencyList()
    {
        return $this->_currency_list;
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->prepareModels();

        $modelFrom = $this->_result_model_from;
        $modelTo = $this->_result_model_to;

        if ($modelFrom === null || $modelTo === null) {
            return false;
        }

        if ($modelFrom->selfAccount->currency_id != $modelTo->selfAccount->currency_id) {
            $modelFrom->expenditure_item_id = InvoiceExpenditureItem::ITEM_CURRENCY_CONVERSION;
            $modelTo->income_item_id = InvoiceIncomeItem::ITEM_CURRENCY_CONVERSION;
        }

        $saveClosure = function ($db) use ($modelFrom, $modelTo) {
            if (LogHelper::save($modelFrom, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                $modelTo->internal_transfer_flow_id = $modelFrom->id;
                if ($modelTo->hasAttribute('cash_id')) {
                    $modelTo->cash_id = $modelFrom->id;
                }
                if (LogHelper::save($modelTo, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    if ($modelFrom->hasAttribute('cash_id')) {
                        $attributes = [
                            'internal_transfer_flow_id' => $modelTo->id,
                            'cash_id' => $modelTo->id,
                        ];
                    } else {
                        $attributes = [
                            'internal_transfer_flow_id' => $modelTo->id,
                        ];
                    }

                    if ($modelFrom->internal_transfer_flow_id == $modelTo->id || $modelFrom->updateAttributes($attributes)) {
                        if ($this->checkInitModels($modelFrom, $modelTo)) {
                            return true;
                        }
                    } else {
                        \common\components\helpers\ModelHelper::logErrors(null, __METHOD__, 'Не удалось обновить $modelFrom->internal_transfer_flow_id');
                    }
                }else {
                    \common\components\helpers\ModelHelper::logErrors($modelTo, __METHOD__);
                }
            } else {
                \common\components\helpers\ModelHelper::logErrors($modelFrom, __METHOD__);
            }
            $db->getTransaction()->rollBack();

            return false;
        };

        return Yii::$app->db->transaction($saveClosure);
    }

    private function prepareModels() : void
    {
        $this->prepareModelFrom();
        $this->prepareModelTo();

        $this->_result_model_from->setInternalTransferAttributes($this->_result_model_to);
        $this->_result_model_to->setInternalTransferAttributes($this->_result_model_from);
    }

    private function prepareModelFrom() : void
    {
        $account = $this->_accounts[$this->account_from];
        if (!$this->copyMode && $this->_init_model_from && $this->_init_model_from->selfAccount == $account) {
            $model = $this->_init_model_from;
            if ($model instanceof CashBankFlowsForm) {
                $model->scenario = 'update';
            }
        } else {
            $model = $account->getCashFlowNewModel();
            if ($model instanceof CashBankFlowsForm) {
                $model->scenario = 'create';
            }
        }
        $model->is_internal_transfer = true;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_EXPENSE;
        if ($model->getIsForeign()) {
            $model->amount_input = $this->amount_from;
        } else {
            $model->amount = $this->amount_from;
        }
        $model->date = $this->date;
        $model->description = $this->description;
        $model->expenditure_item_id = InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
        $model->income_item_id = null;
        if ($model->hasAttribute('author_id')) {
            $model->author_id = $this->_employee->id;
        }

        $this->_result_model_from = $model;
    }

    private function prepareModelTo() : void
    {
        $account = $this->_accounts[$this->account_to];
        if (!$this->copyMode && $this->_init_model_to && $this->_init_model_to->selfAccount == $account) {
            $model = $this->_init_model_to;
            if ($model instanceof CashBankFlowsForm) {
                $model->scenario = 'update';
            }
        } else {
            $model = $account->getCashFlowNewModel();
            if ($model instanceof CashBankFlowsForm) {
                $model->scenario = 'create';
            }
        }
        $model->is_internal_transfer = true;
        $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
        if ($model->getIsForeign()) {
            $model->amount_input = $this->amount_to;
        } else {
            $model->amount = $this->amount_to;
        }
        $model->date = $this->date;
        $model->description = $this->description;
        $model->expenditure_item_id = null;
        $model->income_item_id = InvoiceIncomeItem::ITEM_OWN_FOUNDS;
        if ($model->hasAttribute('author_id')) {
            $model->author_id = $this->_employee->id;
        }

        $this->_result_model_to = $model;
    }

    private function checkInitModels(CashFlowsBase $modelFrom, CashFlowsBase $modelTo) : bool
    {
        if (!$this->copyMode) {
            if ($this->_init_model_from && $this->_init_model_from !== $modelFrom) {
                $source = $this->_init_model_from->source ?? 0;
                if ($source == 0) {
                    if (!$this->_init_model_from->delete()) {
                        \common\components\helpers\ModelHelper::logErrors(null, __METHOD__, 'Не удалось выполнить $this->_init_model_from->delete()');
                        return false;
                    }
                } else {
                    if ($this->_init_model_from->hasAttribute('cash_id')) {
                        $attributes = [
                            'internal_transfer_flow_id' => null,
                            'cash_id' => null,
                        ];
                    } else {
                        $attributes = [
                            'internal_transfer_flow_id' => null,
                        ];
                    }
                    $this->_init_model_from->updateAttributes($attributes);
                }
            }

            if ($this->_init_model_to && $this->_init_model_to !== $modelTo) {
                $source = $this->_init_model_to->source ?? 0;
                if ($source == 0) {
                    if (!$this->_init_model_to->delete()) {
                        \common\components\helpers\ModelHelper::logErrors(null, __METHOD__, 'Не удалось выполнить $this->_init_model_to->delete()');
                        return false;
                    }
                } else {
                    if ($this->_init_model_to->hasAttribute('cash_id')) {
                        $attributes = [
                            'internal_transfer_flow_id' => null,
                            'cash_id' => null,
                        ];
                    } else {
                        $attributes = [
                            'internal_transfer_flow_id' => null,
                        ];
                    }
                    $this->_init_model_to->updateAttributes($attributes);
                }
            }
        }

        return true;
    }
}
