<?php
/**
 * Created by konstantin.
 * Date: 1.7.15
 * Time: 16.22
 */

use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\models\Contractor;
use frontend\modules\cash\models\CashContractorType;
use yii\helpers\ArrayHelper;

/* @var \common\models\cash\CashFlowsBase $model */


$nextFlowNumber = [];
if (method_exists($model, 'getNextNumber')) {
    $nextFlowNumber = [
        CashOrderFlows::FLOW_TYPE_EXPENSE => $model::getNextNumber($model->company_id, CashOrderFlows::FLOW_TYPE_EXPENSE, $model->date),
        CashOrderFlows::FLOW_TYPE_INCOME => $model::getNextNumber($model->company_id, CashOrderFlows::FLOW_TYPE_INCOME, $model->date),
    ];
}

$cashContractorArray = [
    CashOrderFlows::FLOW_TYPE_EXPENSE => ArrayHelper::map(CashContractorType::find()
                                        ->andWhere(['not in', 'name', [
                                            $model->getNewCashContractorId(),
                                            CashContractorType::BALANCE_TEXT,
                                            CashContractorType::COMPANY_TEXT,
                                        ]])->all(), 'name', 'text'),
    CashOrderFlows::FLOW_TYPE_INCOME => ArrayHelper::map(CashContractorType::find()
                                        ->andWhere(['not in', 'name', [
                                            $model->getNewCashContractorId(),
                                            CashContractorType::COMPANY_TEXT,
                                        ]])->all(), 'name', 'text'),
];

$contractorArray = [
    CashOrderFlows::FLOW_TYPE_EXPENSE => Contractor::getAllContractorListForJs(\common\models\Contractor::TYPE_SELLER),
    CashOrderFlows::FLOW_TYPE_INCOME => Contractor::getAllContractorListForJs(\common\models\Contractor::TYPE_CUSTOMER),
];

$reasonTypeArray = [
    CashOrderFlows::FLOW_TYPE_EXPENSE => CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_EXPENSE),
    CashOrderFlows::FLOW_TYPE_INCOME => CashOrdersReasonsTypes::getArray(CashOrderFlows::FLOW_TYPE_INCOME),
];

// (раз)Блокировка селекта статьи расходов для смены типа влияния
$this->registerJs('
    var $expenditureDropDown = $("select[name=\'' . Html::getInputName($model, 'expenditure_item_id') . '\']");
    var $number = $("[name=\'' . Html::getInputName($model, 'number') . '\']");
    var $contractorDropDown = $("#' . Html::getInputId($model, 'contractor_id') . '");
    var $reasonTypeDropDown = $("select[name=\'' . Html::getInputName($model, 'reason_id') . '\']");

    var nextNumber = ' . \yii\helpers\Json::encode($nextFlowNumber) . ';
    var contractorArray = ' . \yii\helpers\Json::encode($contractorArray) . ';
    var reasonTypeArray = ' . \yii\helpers\Json::encode($reasonTypeArray) . ';
    var cashContractorArray = ' . \yii\helpers\Json::encode($cashContractorArray) . ';

    $("[name=\'' . Html::getInputName($model, 'flow_type') . '\']").on("change init", function() {
        if (this.tagName != "SELECT" && !this.checked) {
            return;
        }

        $number.val(nextNumber[this.value]);
        var clearExpenditure = this.value == ' . CashOrderFlows::FLOW_TYPE_INCOME . ';
        $expenditureDropDown.attr("disabled", clearExpenditure);
        if (clearExpenditure) {
            $expenditureDropDown.val("");
        }
        setOptions(contractorArray[this.value], $contractorDropDown, true, this.value);
        setOptions(reasonTypeArray[this.value], $reasonTypeDropDown, false, this.value);
    }).trigger("init");

    function setOptions(optionArray, $dropDown, contractor, typeValue) {
        var value = $dropDown.attr("jsValue");
        $dropDown.empty();
        if ($dropDown.length == 0 || optionArray == undefined || optionArray.length == 0) {
            return;
        }
        var select = $dropDown[0];
        var i = 0;

        select.options = [];
        select.options[i++] = new Option("", "");
        if (contractor) {
            for (id in cashContractorArray[typeValue]) {
                select.options[i++] = new Option(cashContractorArray[typeValue][id], id);
            }
        }
        $.each(optionArray, function (key, value) {
        if (value[0] != undefined) {
            for (id in value[0]) {
                select.options[i++] = new Option(value[0][id], id);
            }
        } else {
            select.options[i++] = new Option(value.name, value.id);
        }
        });

        $dropDown.val(value);
    }


');