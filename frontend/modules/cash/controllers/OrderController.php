<?php

namespace frontend\modules\cash\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\models\cash\excel\CashOrderXlsHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\SubscribeTariffGroup;
use Exception;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\components\CashControllerBase;
use frontend\modules\cash\models\CashBankSearch;
use frontend\modules\cash\models\CashOrderForeignCurrencySearch;
use frontend\modules\cash\models\CashOrderSearch;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\bootstrap\Dropdown;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class OrderController
 * @package frontend\modules\cash\controllers
 */
class OrderController extends CashControllerBase
{
    const TYPE_CREATE_ONE = 1;
    const TYPE_CREATE_MANY = 2;
    const MANY_CREATE_LIMIT = 20;

    /**
     * @var array
     */
    protected $_models = [];
    /**
     * @var
     */
    protected $employee;
    /**
     * @var
     */
    protected $company;
    /**
     * @var
     */
    protected $userCashboxArray;
    /**
     * @var
     */
    protected $allCurrencyList;
    /**
     * @var
     */
    protected $foreignCurrencyList;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'strictAccess' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'create',
                            'many-create',
                            'create-internal',
                            'copy',
                            'create-copy',
                            'copy-internal',
                            'view',
                            'document-print',
                            'update',
                            'update-internal',
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => [permissions\document\Document::STRICT_MODE],
                    ],
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'many-delete', 'un-payed-invoices', 'get-xls'],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::INDEX],
                    ],
                    [
                        'actions' => ['create-cashbox', 'update-cashbox'],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                    [
                        'actions' => ['view', 'document-print'],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::VIEW],
                        'roleParams' => function($rule) {
                            return ['model' => $this->findModel(null, null, false)];
                        },
                    ],
                    [
                        'actions' => [
                            'create',
                            'many-create',
                            'create-internal',
                            'copy-internal',
                            'create-plan',
                            'update-plan'
                        ],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::CREATE],
                    ],
                    [
                        'actions' => [
                            'update',
                            'update-internal',
                        ],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::UPDATE],
                        'roleParams' => function($rule) {
                            return ['model' => $this->findModel(null, null, false)];
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::DELETE],
                        'roleParams' => function($rule) {
                            return ['model' => $this->findModel(null, null, false)];
                        },
                    ],
                    [
                        'actions' => ['copy', 'create-copy'],
                        'allow' => true,
                        'roles' => [permissions\CashOrder::CREATE],
                        'roleParams' => function($rule) {
                            return ['model' => $this->findModel(null, null, false)];
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'create-internal' => [
                'class' => 'frontend\modules\cash\components\CreateInternalTransferAction',
                'view' => '/bank/create-internal',
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'update-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'findModel' => function ($id, $action) {
                    return $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'copy-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'copyMode' => true,
                'findModel' => function ($id, $action) {
                    return $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'create-plan' => [
                'class' => 'frontend\modules\cash\components\CreatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_ORDER,
                'view' => '/order/create',
            ],
            'update-plan' => [
                'class' => 'frontend\modules\cash\components\UpdatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_ORDER,
                'view' => '/order/update',
            ],
            'delete-plan' => [
                'class' => 'frontend\modules\cash\components\DeletePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_ORDER,
            ],
            'many-delete' => [
                'class' => 'frontend\modules\cash\components\ManyDeletePlanFactFlowsAction',
            ],
            'many-item' => [
                'class' => 'frontend\modules\cash\components\ManyChangePlanFactFlowsAction',
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->employee = Yii::$app->user->identity;
            $this->company = $this->employee->company ?? null;
            $this->userCashboxArray = $this->employee ? $this->employee->getCashboxes()->with('currency')->orderBy([
                'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])->indexBy('id')->all() : [];
            $this->allCurrencyList = $this->foreignCurrencyList = Currency::find()->select([
                'name',
                'id'
            ])->indexBy('id')->column();
            unset($this->foreignCurrencyList[Currency::DEFAULT_ID]);

            return true;
        }

        return false;
    }

    /**
     * @param null $cashbox
     * @return mixed|string
     */
    public function actionIndex($cashbox = null)
    {
        if ($this->company->strict_mode) {
            Yii::$app->session->setFlash('error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' .
                Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => Url::current(),
                ])) . '.'
            );
        }

        $cashboxModel = $this->getCashboxModel($cashbox);

        if (in_array($cashbox, $this->foreignCurrencyList) || ($cashboxModel && $cashboxModel->getIsForeign())) {
            return $this->foreignCurrencyIndex($cashbox, $cashboxModel);
        }

        $model = new CashOrderSearch([
            'company_id' => $this->company->id,
            'cashbox_id' => ($cashboxModel !== null) ? $cashboxModel->id : array_keys($this->userCashboxArray),
            'showPlan' => !Yii::$app->user->identity->config->cash_index_hide_plan
        ]);

        if ($flow_id = Yii::$app->request->post('flow_id')) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($model->is_accounting == null) {
            $model->is_accounting = -1;
        }

        \common\models\company\CompanyFirstEvent::checkEvent($this->company, 72);
        $showImportModal = Yii::$app->request->get('modal', 0);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'cashbox' => $cashbox,
            'company' => $this->company,
            'showImportModal' => $showImportModal,
            'foreign' => null,
            'foreignItems' => [],
            'currentCashboxId' => ($cashboxModel !== null) ? (array)$cashboxModel->id : array_keys($this->userCashboxArray)
        ]);
    }

    private function getCashboxModel(&$cashbox): ?Cashbox
    {
        $cashboxModel = null;

        if (empty($cashbox)) {
            $cashboxModel = reset($this->userCashboxArray);
            $cashbox = key($this->userCashboxArray);
        } else {
            $cashboxModel = $this->userCashboxArray[$cashbox] ?? null;
        }

        if (is_numeric($cashbox)) {
            if ($cashboxModel === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        if ($cashboxModel === null && !in_array($cashbox, ['all', Currency::DEFAULT_NAME])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $cashboxModel;
    }

    /**
     * @param null $cashbox
     * @return mixed|string
     */
    private function foreignCurrencyIndex($cashbox, $cashboxModel)
    {
        $model = new CashOrderSearch([
            'foreign' => true,
            'company_id' => $this->company->id,
            'cashbox_id' => $cashboxModel->id ?? array_keys($this->userCashboxArray),
            'currency_id' => $cashboxModel->currency_id ?? array_search($cashbox, $this->foreignCurrencyList),
        ]);

        if ($flow_id = Yii::$app->request->post('flow_id')) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($model->is_accounting == null) {
            $model->is_accounting = -1;
        }

        \common\models\company\CompanyFirstEvent::checkEvent($this->company, 72);
        $showImportModal = Yii::$app->request->get('modal', 0);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'cashbox' => $cashbox,
            'company' => $this->company,
            'showImportModal' => $showImportModal,
            'foreign' => 1,
            'foreignItems' => $this->foreignCurrencyList,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $this->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate($type = null, $id = null)
    {
        if ($type && !in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if (Yii::$app->request->get('internal')) {
            return $this->runAction('create-internal');
        }
        if (Yii::$app->request->post('is_plan_flow')) {
            return $this->runAction('create-plan', ['flowType' => $type, 'walletId' => $id]);
        }

        $cashbox = $this->userCashboxArray[$id] ?? reset($this->userCashboxArray);

        if ($cashbox === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $cashbox->getIsForeign() ? $this->foreignCreate($type, $cashbox) : $this->create($type, $cashbox);
    }

    /**
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    private function create($type = null, Cashbox $cashbox)
    {
        $model = new CashOrderFlows([
            'flow_type' => $type ?? CashFlowsBase::FLOW_TYPE_INCOME,
            'company_id' => $this->company->id,
            'author_id' => $this->employee->id,
            'date' => Yii::$app->request->get('skipDate') ? null : date(DateHelper::FORMAT_USER_DATE),
            'cashbox_id' => $cashbox->id,
            'is_accounting' => $cashbox->is_accounting,
        ]);

        if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($fillByFlow = Yii::$app->request->get('fill_by_flow')) {
            self::fillByFlow($model, $fillByFlow);
        }

        if ($model->load(Yii::$app->request->post())) {
            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                LogHelper::$useTransaction = false;
                if ($model->checkContractor() && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    $model->linkSelectedInvoices();

                    return true;
                } else {
                    $db->transaction->rollBack();

                    return false;
                }
            });

            if ($saved) {
                $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
                Yii::$app->session->setFlash('success', $message . ' кассовый ордер создан');
                $defaultRedirect = $this->_createdFromIndex()
                    ? (Yii::$app->request->referrer ?: ['index', 'cashbox' => $model->cashbox_id])
                    : ['view', 'id' => $model->id];

                return $this->redirect(Yii::$app->request->post('redirect', $defaultRedirect));
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
            ]);
        }
    }

    /**
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    private function foreignCreate($type = null, Cashbox $cashbox)
    {
        $model = new CashOrderForeignCurrencyFlows([
            'flow_type' => $type ?? CashFlowsBase::FLOW_TYPE_INCOME,
            'company_id' => $this->company->id,
            'author_id' => $this->employee->id,
            'date' => Yii::$app->request->get('skipDate') ? null : date(DateHelper::FORMAT_USER_DATE),
            'cashbox_id' => $cashbox->id,
            'is_accounting' => $cashbox->is_accounting,
        ]);
        $model->setNextNumber();

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($fillByFlow = Yii::$app->request->get('fill_by_flow')) {
            self::fillByFlowForeign($model, $fillByFlow);
        }

        if ($model->load(Yii::$app->request->post())) {
            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                LogHelper::$useTransaction = false;
                if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    $model->linkSelectedInvoices();

                    return true;
                } else {
                    $db->transaction->rollBack();

                    return false;
                }
            });

            if ($saved) {
                $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
                Yii::$app->session->setFlash('success', $message . ' кассовый ордер создан');
                $defaultRedirect = $this->_createdFromIndex() ? ['index', 'cashbox' => $model->cashbox_id] : ['view', 'id' => $model->id];

                return $this->redirect(Yii::$app->request->post('redirect', $defaultRedirect));
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('foreign-create', [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
            ]);
        } else {
            return $this->render('foreign-create', [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
            ]);
        }
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionManyCreate($type = null, $id = null)
    {
        $cashbox = $this->userCashboxArray[$id] ?? null;
        $type = $type ?? CashFlowsBase::FLOW_TYPE_INCOME;

        if ($cashbox === null || $type && !in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return $this->manyCreate($type, $cashbox);
    }

    /**
     * Create Rub&Foreign flows
     *
     * @param $type
     * @param Cashbox $cashbox
     * @return array|string|Response
     * @throws \Throwable
     */
    private function manyCreate($type, Cashbox $cashbox)
    {
        $isForeign = $cashbox->getIsForeign();

        /** @var CashOrderFlows|CashOrderForeignCurrencyFlows $cashOrderFlows */
        $cashOrderFlows = ($isForeign)
            ? CashOrderForeignCurrencyFlows::class
            : CashOrderFlows::class;
        /** @var PlanCashFlows|PlanCashForeignCurrencyFlows $planCashFlows */
        $planCashFlows = ($isForeign)
            ? PlanCashForeignCurrencyFlows::class
            : PlanCashFlows::class;

        // Enable multi-models in form
        $cashOrderFlows::$formNameModify = true;

        $_nextNumber = $cashOrderFlows::getNextNumber($this->company->id, $type, date(DateHelper::FORMAT_USER_DATE));

        $models = [];
        $firstModelPos = 1;
        for ($pos = $firstModelPos; $pos <= self::MANY_CREATE_LIMIT; $pos++) {
            $models[$pos] = new $cashOrderFlows([
                'id' => "_{$pos}",
                'isNewRecord' => false,
                'flow_type' => $type,
                'company_id' => $this->company->id,
                'author_id' => $this->employee->id,
                'date' => date(DateHelper::FORMAT_DATE),
                'recognition_date' => date(DateHelper::FORMAT_DATE),
                'cashbox_id' => $cashbox->id,
                'is_accounting' => $cashbox->is_accounting,
                'number' => $_nextNumber + ($pos - 1)
            ]);
        }

        // check first model pos
        foreach (Yii::$app->request->post() as $k => $v) {
            if (substr($k, 0, 9) == 'CashOrder') {
                @list($modelName, $pos) = explode('_', $k);
                if (isset($models[$pos])) {
                    $firstModelPos = $pos;
                    break;
                }
            }
        }

        /** @var CashOrderFlows $requiredFirstModel */
        $requiredFirstModel = $models[$firstModelPos];

        if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax') !== null) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $_validatedCount = 0;
            $manyValidation = [];
            foreach ($models as $pos => $model) {
                if ($model->load(Yii::$app->request->post()) && $model->amount > 0) {
                    $manyValidation = array_merge($manyValidation, ActiveForm::validate($model));
                    $_validatedCount++;
                }
            }

            if ($_validatedCount === 0) {
                return ActiveForm::validate($requiredFirstModel);
            }

            return $manyValidation;
        }

        if ($requiredFirstModel->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($isForeign, $models, $planCashFlows)
            {
                LogHelper::$useTransaction = false;
                foreach ($models as $pos => $model) {
                    if ($model->load(Yii::$app->request->post()) && $model->amount > 0 && $model->checkContractor()) {

                        $model->id = null;
                        $model->isNewRecord = true;

                        if ($model->isPlanDate()) {
                            $planModel = $planCashFlows::fact2Plan($model);
                            if ($planModel->_save()) {
                                continue;
                            }

                            $db->transaction->rollBack();
                            return false;
                        } else {

                            // correct amount
                            if ($isForeign)
                                $model->amount_input = $model->amount;

                            if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                                $model->linkSelectedInvoices();
                                continue;
                            }

                            $db->transaction->rollBack();
                            return false;
                        }
                    }
                }

                return true;
            });

            if ($isSaved) {
                Yii::$app->session->setFlash('success', ($requiredFirstModel->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходные' : 'Приходные') . ' кассовые ордера созданы');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось создать кассовые ордеры');
            }

            $defaultRedirect = $this->_createdFromIndex() ? ['index', 'cashbox' => $requiredFirstModel->cashbox_id] : ['view', 'id' => $requiredFirstModel->id];

            return $this->redirect(Yii::$app->request->post('redirect', $defaultRedirect));
        }

        $_renderParams = [
            'models' => $models,
            'company' => $this->company,
            'userCashboxArray' => $this->userCashboxArray,
            '_controlModel' => new CashOrderFlows([
                'flow_type' => $type,
                'company_id' => $this->company->id,
                'cashbox_id' => $cashbox->id
            ]),
            'foreign' => $isForeign
        ];

        return (Yii::$app->request->isAjax) ?
            $this->renderAjax('many_create', $_renderParams) :
            $this->render('many_create', $_renderParams);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        return $this->getIsForeign() ? $this->foreignUpdate($id) : $this->update($id);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    private function update($id)
    {
        if (Yii::$app->request->get('is_plan_flow')) {
            return $this->runAction('update-plan', ['flowId' => $id]);
        }

        $model = $this->findModel($id);

        if ($model->is_internal_transfer) {
            return $this->runAction('update-internal', ['id' => $id]);
        }

        $cashboxExists = Yii::$app->user->identity->getCashboxes()->andWhere([
            'cashbox.id' => $model->cashbox_id,
        ])->exists();
        if (!$cashboxExists) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->linkSelectedInvoices();
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }
            $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
            Yii::$app->session->setFlash('success', $message . ' кассовый ордер изменен');

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                    'company' => $this->company,
                    'userCashboxArray' => $this->userCashboxArray,
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'company' => $this->company,
                    'userCashboxArray' => $this->userCashboxArray,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    private function foreignUpdate($id)
    {
        $model = $this->findModel($id, true);

        if ($model->is_internal_transfer) {
            return $this->runAction('update-internal', ['id' => $id]);
        }

        $cashboxExists = Yii::$app->user->identity->getCashboxes()->andWhere([
            'cashbox.id' => $model->cashbox_id,
        ])->exists();
        if (!$cashboxExists) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->linkSelectedInvoices();
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }
            $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
            Yii::$app->session->setFlash('success', $message . ' кассовый ордер изменен');

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('foreign-update', [
                    'model' => $model,
                    'company' => $this->company,
                    'userCashboxArray' => $this->userCashboxArray,
                ]);
            } else {
                return $this->render('foreign-update', [
                    'model' => $model,
                    'company' => $this->company,
                    'userCashboxArray' => $this->userCashboxArray,
                ]);
            }
        }
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws Exception
     */
    public function actionCreateCashbox()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $cashbox = Yii::$app->request->post('cashbox');
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            $model = new Cashbox([
                'company_id' => $company->id,
            ]);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(['/cash/order/index', 'cashbox' => $model->id]);

                /*
                $user = Yii::$app->user->identity;
                $cashboxList = $user->getCashboxes()
                    ->select(['name', 'id'])
                    ->orderBy([
                        'is_main' => SORT_DESC,
                        'name' => SORT_ASC,
                    ])->indexBy('id')->column();
                $cashboxItems = [];
                if ($cashboxList) {
                    if (count($cashboxList) > 1) {
                        $cashboxList = $cashboxList + ['all' => 'Все кассы'];
                    }
                    foreach ($cashboxList as $key => $name) {
                        $cashboxItems[] = [
                            'label' => $name .
                                ($key !== 'all' ? '<span class="glyphicon glyphicon-pencil update-cashbox ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/cashbox/update', 'id' => $key]) . '"></span>' : null),
                            'url' => Url::to(['index', 'cashbox' => $key]),
                            'linkOptions' => [
                                'class' => $cashbox == $key ? 'active' : '',
                            ],
                        ];
                    }
                }
                $cashboxItems[] = [
                    'label' => '[ + Добавить кассу ]',
                    'options' => [
                        'class' => 'add-cashbox ajax-modal-btn',
                        'data-title' => 'Добавить кассу',
                        'data-url' => Url::to(['/cashbox/create']),
                    ],
                ];

                return [
                    'result' => true,
                    'id' => $model->id,
                    'name' => $model->name,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $cashboxItems,
                    ]),
                ];
                */
            }

            return [
                'result' => false,
                'html' => $this->renderPartial('@frontend/views/cashbox/create', [
                    'model' => $model,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @return array|Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionUpdateCashbox($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $cashbox = Yii::$app->request->post('cashbox');
            $model = Cashbox::findOne($id);
            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /* @var $user \common\models\employee\Employee */
                $user = Yii::$app->user->identity;
                $cashboxList = $user->getCashboxes()
                    ->select(['name', 'id'])
                    ->orderBy([
                        'is_main' => SORT_DESC,
                        'name' => SORT_ASC,
                    ])->indexBy('id')->column();
                $cashboxItems = [];
                if ($cashboxList) {
                    if (count($cashboxList) > 1) {
                        $cashboxList = $cashboxList + ['all' => 'Все кассы'];
                    }
                    foreach ($cashboxList as $key => $name) {
                        $cashboxItems[] = [
                            'label' => $name .
                                ($key !== 'all' ? '<span class="glyphicon glyphicon-pencil update-cashbox ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/cashbox/update', 'id' => $key]) . '"></span>' : null),
                            'url' => Url::to(['index', 'cashbox' => $key]),
                            'linkOptions' => [
                                'class' => $cashbox == $key ? 'active' : '',
                            ],
                        ];
                    }
                }
                $cashboxItems[] = [
                    'label' => '[ + Добавить кассу ]',
                    'options' => [
                        'class' => 'add-cashbox ajax-modal-btn',
                        'data-title' => 'Добавить кассу',
                        'data-url' => Url::to(['/cashbox/create']),
                    ],
                ];

                return [
                    'result' => true,
                    'label' => $cashbox == $model->id ? $model->name : null,
                    'id' => $model->id,
                    'name' => $model->name,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $cashboxItems,
                    ]),
                ];
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/cashbox/update', [
                    'model' => $model,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneModel = $this->findModel($id)->cloneSelf();

        if ($cloneModel->save(false)) {
            $message = $cloneModel->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
            Yii::$app->session->setFlash('success', $message . ' кассовый ордер скопирован');

            return $this->redirect(['view', 'id' => $cloneModel->id,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionCreateCopy($id)
    {
        $sourceModel = $this->findModel($id);

        if ($sourceModel->is_internal_transfer) {
            return $this->runAction('copy-internal', ['id' => $id]);
        }

        $model = $sourceModel->cloneSelf();
        $cashboxExists = Yii::$app->user->identity->getCashboxes()->andWhere([
            'cashbox.id' => $model->cashbox_id,
        ])->exists();
        if (!$cashboxExists) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->author_id = Yii::$app->user->id;

        if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, function ($model) {
                if ($model->save()) {
                    $model->linkSelectedInvoices();

                    return true;
                }

                return false;
            })
        ) {
            $message = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? 'Расходный' : 'Приходный';
            Yii::$app->session->setFlash('success', $message . ' кассовый ордер скопирован');

            return $this->redirect(Yii::$app->request->post('redirect', Yii::$app->request->get('redirect', [
                'view',
                'id' => $model->id,
                'foreign' => $model->isForeign ? 1 : null,
            ])));
        }

        $view = $model->isForeign ? 'foreign-create' : 'create';

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($view, [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
                'isClone' => true,
            ]);
        } else {
            return $this->render($view, [
                'model' => $model,
                'company' => $this->company,
                'userCashboxArray' => $this->userCashboxArray,
                'isClone' => true,
            ]);
        }
    }

    /**
     * @param int $contractorId
     * @param int $type
     * @param int|null $flowId
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUnPayedInvoices($contractorId, $type, $flowId = null, $flowDate = null)
    {
        $company = Yii::$app->user->identity ? Yii::$app->user->identity->company : null;
        $contractor = Contractor::findOne(['id' => $contractorId]);
        $result = [];

        if ($company && $contractor) {
            $invoiceArray = Invoice::find()->andWhere([
                'company_id' => $company->id,
                'contractor_id' => $contractor->id,
                'is_deleted' => false,
                'invoice_status_id' => InvoiceStatus::$payAllowed,
            ])->orderBy(['document_number' => SORT_ASC])->all();

            if ($invoiceArray) {
                $date = $flowDate ? \DateTime::createFromFormat('d.m.Y|', $flowDate) : false;
                foreach ($invoiceArray as $invoice) {
                    if ($date) {
                        $invoice->recalculateAmountOnDate($date);
                    }
                    $result[] = [
                        'id' => $invoice->id,
                        'number' => $invoice->fullNumber,
                        'date' => ($d = date_create_from_format('Y-m-d', $invoice->document_date)) ? $d->format('d.m.Y') : '',
                        'amount' => $invoice->total_amount_with_nds / 100,
                        'amountPrint' => \common\components\TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2),
                    ];
                }
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $result;
    }

    /**
     * @return mixed
     */
    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        $isForeign = Yii::$app->request->post('isForeign', false);
        foreach ($idArray as $id) {
            try {
                if ($model = $this->findModel($id, $isForeign)) {
                    $this->_deleteModel($model);
                }
            } catch (\Exception $e) {
                //
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($actionType, $id, $filename)
    {
        $model = $this->findModel($id);
        if ($model->flow_type == CashOrderFlows::FLOW_TYPE_INCOME) {
            return $this->_documentPrint($model, $actionType, 'pdf-view-pko');
        } else {
            return $this->_documentPrint($model, $actionType, 'pdf-view-rko');
        }
    }

    public function actionGetXls($cashbox = null, $foreign = null, $currency = null): Response
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if (!($company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE_PLUS)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_ALL_INCLUSIVE))) {
            Yii::$app->session->setFlash('error',
                'Выгрузить данные в Excel можно только на платном тарифе ФинДиректор, ФинДиректор PRO, ФинДиректор Всё включено.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $cashboxModel = $this->getCashboxModel($cashbox);

        $model = new CashOrderSearch([
            'company_id' => $company->id,
            'cashbox_id' => $cashboxModel->id ?? array_keys($this->userCashboxArray),
            'currency_id' => $cashboxModel->currency_id ?? array_search($cashbox, $this->foreignCurrencyList),
            'showPlan' => !Yii::$app->user->identity->config->cash_index_hide_plan
        ]);

        $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
        $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        $dataProvider->pagination->pageSize = 0;
        $models = $dataProvider->getModels();
        $period = StatisticPeriod::getSessionPeriod();

        try {
            CashOrderXlsHelper::init($employee);
            CashOrderXlsHelper::generateXls($period, $models);
            exit;
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', 'Не удалось выгрузить данные в Excel.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }


    /**
     * @param CashOrderFlows $model
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|void
     * @throws Exception
     */
    protected function _documentPrint(CashOrderFlows $model, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }

        /** @var CashOrderFlows $model */

        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'ioType' => ($model->hasAttribute('type') ? $model->type : null),
            ], $params),

            'destination' => PdfRenderer::DESTINATION_STRING,
            'filename' => $model->getPdfFileName(),
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                return $renderer->renderHtml();
        }
    }

    /**
     * @param integer $id
     * @return CashOrderFlows|CashOrderForeignCurrencyFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = null, $foreign = null, $throw = true)
    {
        $foreign = (int) boolval($foreign ?? Yii::$app->request->get('foreign'));
        $id = intval($id ?? Yii::$app->request->get('id'));
        $employee = Yii::$app->user->identity;
        $company_id = $employee ? $employee->company->id : null;
        $employeeCompany = $employee ? $employee->currentEmployeeCompany : null;
        $key = "{$foreign}_{$id}";

        if (!array_key_exists($key, $this->_models)) {
            $class = $foreign ? CashOrderForeignCurrencyFlows::class : CashOrderFlows::class;
            $query = $class::find()->alias('order')->andWhere([
                'order.id' => $id,
                'order.company_id' => $company_id,
            ]);
            // бухгалтер видит все по своей кассе или только для учета в бухгалтерии
            if ($employeeCompany && $employeeCompany->employee_role_id == EmployeeRole::ROLE_ACCOUNTANT) {
                $query->joinWith('cashbox', false)->andWhere([
                    'or',
                    ['order.is_accounting' => true],
                    ['cashbox.responsible_employee_id' => $employeeCompany->employee_id],
                ]);
            }

            $this->_models[$key] = $query->one();
        }

        if ($this->_models[$key] !== null) {
            return $this->_models[$key];
        } else {
            if ($throw) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }

    /**
     * @param integer $id
     * @return CashOrderForeignCurrencyFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findForeignModel($id)
    {
        if (($model = CashOrderForeignCurrencyFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function _createdFromIndex()
    {
        return (strpos(Yii::$app->request->referrer, '/cash/order/index') !== false) ? true : false;
    }

    /**
     * @param CashOrderFlows $model
     * @param $copyModelId
     * @return bool
     */
    protected static function fillByFlow(CashOrderFlows &$model, $copyModelId)
    {
        /** @var CashOrderFlows $copyModel */
        if ($copyModel = CashOrderFlows::findOne(['id' => $copyModelId, 'company_id' => Yii::$app->user->identity->company->id])) {
            $model->setAttributes([
                'flow_type' => $copyModel->flow_type,
                'contractor_id' => $copyModel->contractor_id,
                'income_item_id' => $copyModel->income_item_id,
                'expenditure_item_id' => $copyModel->expenditure_item_id,
                'amount' => $copyModel->amount,
                'is_accounting' => $copyModel->is_accounting,
                'description' => $copyModel->description,
                'other_cashbox_id' => $copyModel->other_cashbox_id,
                'other_rs_id' => $copyModel->other_rs_id,
                'other_emoney_id' => $copyModel->other_emoney_id,
                'project_id' => $copyModel->project_id
            ]);

            return true;
        }

        return false;
    }

    /**
     * @param CashOrderForeignCurrencyFlows $model
     * @param $copyModelId
     * @return bool
     */
    protected static function fillByFlowForeign(CashOrderForeignCurrencyFlows &$model, $copyModelId)
    {
        /** @var CashOrderFlows $copyModel */
        if ($copyModel = CashOrderFlows::findOne(['id' => $copyModelId, 'company_id' => Yii::$app->user->identity->company->id])) {
            $model->setAttributes([
                'flow_type' => $copyModel->flow_type,
                'contractor_id' => $copyModel->contractor_id,
                'income_item_id' => $copyModel->income_item_id,
                'expenditure_item_id' => $copyModel->expenditure_item_id,
                'amount' => $copyModel->amount,
                'is_accounting' => $copyModel->is_accounting,
                'description' => $copyModel->description,
                'project_id' => $copyModel->project_id
            ]);

            return true;
        }

        return false;
    }
}
