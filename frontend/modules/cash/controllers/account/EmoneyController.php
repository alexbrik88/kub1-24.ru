<?php

namespace frontend\modules\cash\controllers\account;

use common\models\Company;
use common\models\cash\Emoney;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class EmoneyController
 * @package frontend\modules\cash\controllers
 */
class EmoneyController extends \common\components\CommonController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
            ]
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreate()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $model = new Emoney([
            'company_id' => $company->id,
        ]);
        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/cash/e-money/index', 'emoney' => $model->id]);
        }

        return $this->renderAjax('create', [
            'company' => $company,
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $model = $this->findModel($id);
        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->renderAjax('update', [
                'company' => $company,
                'model' => $model,
                'save' => true,
            ]);
        }

        return $this->renderAjax('update', [
            'company' => $company,
            'model' => $model,
            'save' => false,
        ]);
    }

    /**
     * @param integer $id
     * @return Emoney the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Emoney::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
