<?php

namespace frontend\modules\cash\controllers\account;

use common\models\Company;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;
use common\models\company\CheckingAccountant;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CreateController
 * @package frontend\modules\cash\controllers
 */
class CreateController extends \common\components\CommonController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
            ]
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionBank()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $model = new CheckingAccountant([
            'company_id' => $company->id,
        ]);
        $model->populateRelation('company', $company);
        $view = '//company/form/modal_rs/_partial/_form';
        $save = false;

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $save = true;
        }

        return $this->renderAjax('create', [
            'view' => $view,
            'save' => $save,
            'model' => $model,
            'data' => [
                'checkingAccountant' => $model,
                'company' => $company,
                'actionUrl' => Url::current(),
            ],
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCashbox()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $model = new Cashbox([
            'company_id' => $company->id,
        ]);
        $model->populateRelation('company', $company);
        $view = '//cashbox/_form';
        $save = false;

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $save = true;
        }

        return $this->renderAjax('create', [
            'view' => $view,
            'save' => $save,
            'model' => $model,
            'data' => [
                'model' => $model,
            ],
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionEmoney()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $model = new Emoney([
            'company_id' => $company->id,
        ]);
        $model->populateRelation('company', $company);
        $view = '//emoney/_form';
        $save = false;

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $save = true;
        }

        return $this->renderAjax('create', [
            'view' => $view,
            'save' => $save,
            'model' => $model,
            'data' => [
                'model' => $model,
            ],
        ]);
    }
}
