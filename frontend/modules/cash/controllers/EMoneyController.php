<?php

namespace frontend\modules\cash\controllers;

use common\models\Company;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\Emoney;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\components\CashControllerBase;
use frontend\modules\cash\models\CashEmoneyForeignCurrencySearch;
use frontend\modules\cash\models\CashEmoneySearch;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\widgets\EmoneyFilterWidget;
use frontend\rbac\UserRole;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\Dropdown;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class EMoneyController extends CashControllerBase
{
    /**
     * @var array
     */
    protected $_models = [];
    /**
     * @var
     */
    protected $employee;
    /**
     * @var
     */
    protected $company;
    /**
     * @var
     */
    protected $emoneyArray;
    /**
     * @var
     */
    protected $allCurrencyList;
    /**
     * @var
     */
    protected $foreignCurrencyList;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'strictAccess' => [
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::INDEX],
                    ],
                    [
                        'actions' => [
                            'view',
                            'has-manual',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::VIEW],
                    ],
                    [
                        'actions' => [
                            'create',
                            'create-internal',
                            'create-copy',
                            'copy-internal',
                            'create-plan'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::CREATE],
                    ],
                    [
                        'actions' => [
                            'update',
                            'update-internal',
                            'update-plan'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                    [
                        'actions' => ['create-emoney', 'update-emoney'],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                ],
            ],
        ]);
    }

    public function getViewPath()
    {
        return Yii::getAlias('@frontend/modules/cash/views/emoney');
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'create-internal' => [
                'class' => 'frontend\modules\cash\components\CreateInternalTransferAction',
                'view' => '/bank/create-internal',
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'update-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'findModel' => function ($id, $action) {
                    return $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'copy-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'copyMode' => true,
                'findModel' => function ($id, $action) {
                    return $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'create-plan' => [
                'class' => 'frontend\modules\cash\components\CreatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_EMONEY,
                'view' => '/emoney/create',
            ],
            'update-plan' => [
                'class' => 'frontend\modules\cash\components\UpdatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_EMONEY,
                'view' => '/emoney/update',
            ],
            'delete-plan' => [
                'class' => 'frontend\modules\cash\components\DeletePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_EMONEY,
            ],
            'many-delete' => [
                'class' => 'frontend\modules\cash\components\ManyDeletePlanFactFlowsAction',
            ],
            'many-item' => [
                'class' => 'frontend\modules\cash\components\ManyChangePlanFactFlowsAction',
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->employee = Yii::$app->user->identity;
            $this->company = $this->employee->company ?? null;
            $this->emoneyArray = $this->company ? $this->company->getEmoneys()->with('currency')->orderBy([
                'IF([[currency_id]]="643", 0, 1)' => SORT_ASC,
                'is_main' => SORT_DESC,
                'is_closed' => SORT_ASC,
                'name' => SORT_ASC,
            ])->indexBy('id')->all() : [];
            $this->allCurrencyList = $this->foreignCurrencyList = Currency::find()->select([
                'name',
                'id'
            ])->indexBy('id')->column();
            unset($this->foreignCurrencyList[Currency::DEFAULT_ID]);

            return true;
        }

        return false;
    }

    public function actionIndex($emoney = null, $strict = false)
    {
        if ($this->company->strict_mode) {
            Yii::$app->session->setFlash('error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' .
                Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => Url::current(),
                ])) . '.'
            );
        }

        $employeeCompany = $this->employee->currentEmployeeCompany;

        if ($emoney === null) {
            $emoney = $employeeCompany->last_emoney_id ? : null;
        }

        if (empty($emoney)) {
            $emoneyModel = reset($this->emoneyArray);
            $emoney = key($this->emoneyArray);
        } else {
            $emoneyModel = $this->emoneyArray[$emoney] ?? null;
        }

        if (isset($this->emoneyArray[$emoney]) && $employeeCompany->last_emoney_id != $emoney) {
            $employeeCompany->updateAttributes(['last_emoney_id' => $emoney]);
        }

        if (is_numeric($emoney)) {
            if ($emoneyModel === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        if (in_array($emoney, $this->foreignCurrencyList) || ($emoneyModel && $emoneyModel->getIsForeign())) {
            return $this->foreignCurrencyIndex($emoney, $emoneyModel);
        }

        if ($emoneyModel === null && !in_array($emoney, ['all', Currency::DEFAULT_NAME])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new CashEmoneySearch([
            'company_id' => $this->company->id,
            'emoney_id' => $emoneyModel !== null ? $emoneyModel->id : array_keys($this->emoneyArray),
            'showPlan' => !Yii::$app->user->identity->config->cash_index_hide_plan
        ]);
        if (Yii::$app->request->isPost && ($flow_id = Yii::$app->request->post('flow_id'))) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($model->is_accounting == null) {
            $model->is_accounting = -1;
        }

        \common\models\company\CompanyFirstEvent::checkEvent($this->company, 81);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'company' => $this->company,
            'emoney' => $emoney,
            'foreign' => null,
        ]);
    }

    private function foreignCurrencyIndex($emoney, $emoneyModel)
    {
        $model = new CashEmoneySearch([
            'foreign' => true,
            'company_id' => $this->company->id,
            'emoney_id' => $emoneyModel->id ?? array_keys($this->emoneyArray),
            'currency_id' => $emoneyModel->currency_id ?? array_search($emoney, $this->foreignCurrencyList),
        ]);

        if (Yii::$app->request->isPost && ($flow_id = Yii::$app->request->post('flow_id'))) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($model->is_accounting == null) {
            $model->is_accounting = -1;
        }

        \common\models\company\CompanyFirstEvent::checkEvent($this->company, 81);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'company' => $this->company,
            'emoney' => $emoney,
            'foreign' => 1,
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate($type = null, $id = null)
    {
        if ($type && !in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if (Yii::$app->request->get('internal')) {
            return $this->runAction('create-internal');
        }
        if (Yii::$app->request->post('is_plan_flow')) {
            return $this->runAction('create-plan', ['flowType' => $type, 'walletId' => $id]);
        }

        $emoney = $this->emoneyArray[$id] ?? null;

        if ($emoney === null) {
            $id = $this->employee->currentEmployeeCompany->last_emoney_id;
            $emoney = $this->emoneyArray[$id] ?? null;
            if ($emoney === null) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        return $emoney->getIsForeign() ? $this->foreignCreate($type, $emoney) : $this->create($type, $emoney);
    }

    private function create($type, $emoney)
    {
        $model = new CashEmoneyFlows([
            'flow_type' => $type ?? CashFlowsBase::FLOW_TYPE_INCOME,
            'company_id' => $this->company->id,
            'emoney_id' => $emoney->id,
            'is_accounting' => $emoney->is_accounting,
            'date' => date('d.m.Y'),
        ]);

        if ($fillByFlow = Yii::$app->request->get('fill_by_flow')) {
            self::fillByFlow($model, $fillByFlow);
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            Yii::$app->session->setFlash('success', 'Операция по E-money добавлена');
            $model->linkSelectedInvoices();

            $defaultRedirect = Yii::$app->request->referrer ?: ['index', 'emoney' => $model->emoney_id];

            return $this->redirect(Yii::$app->request->post('redirect', $defaultRedirect));
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'company' => $this->company,
        ]);
    }

    private function foreignCreate($type, $emoney)
    {
        $model = new CashEmoneyForeignCurrencyFlows([
            'flow_type' => $type ?? CashFlowsBase::FLOW_TYPE_INCOME,
            'company_id' => $this->company->id,
            'emoney_id' => $emoney->id,
            'is_accounting' => $emoney->is_accounting,
            'date' => date('d.m.Y'),
        ]);

        if ($fillByFlow = Yii::$app->request->get('fill_by_flow')) {
            self::fillByFlow($model, $fillByFlow);
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            Yii::$app->session->setFlash('success', 'Операция по E-money добавлена');
            $model->linkSelectedInvoices();

            $defaultRedirect = ['index', 'emoney' => $model->emoney_id];

            return $this->redirect(Yii::$app->request->post('redirect', $defaultRedirect));
        }

        return $this->renderAjax('foreign-create', [
            'model' => $model,
            'company' => $this->company,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->get('is_plan_flow')) {
            return $this->runAction('update-plan', ['flowId' => $id]);
        }

        $isForeign = $this->getIsForeign();

        $model = $this->findModel($id, $isForeign);
        //$model->setScenario('update');

        if ($model->is_internal_transfer) {
            return $this->runAction('update-internal', ['id' => $id]);
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax($isForeign ? 'foreign-update' : 'update', [
                    'model' => $model,
                    'company' => $this->company,
                ]);
            } else {
                return $this->render($isForeign ? 'foreign-update' : 'update', [
                    'model' => $model,
                    'company' => $this->company,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidRouteException
     */
    public function actionCreateCopy($id)
    {
        $sourceModel = $this->findModel($id);

        if ($sourceModel->is_internal_transfer) {
            return $this->runAction('copy-internal', ['id' => $id]);
        }

        $model = $sourceModel->cloneSelf();

        if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, function ($model) {
                if ($model->save()) {
                    $model->linkSelectedInvoices();

                    return true;
                }

                return false;
            })
        ) {
            Yii::$app->session->setFlash('success', 'Операция по E-money скопирована');

            return $this->redirect(Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']));
        }

        $view = $model->isForeign ? 'foreign-create' : 'create';

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($view, [
                'model' => $model,
                'isClone' => true
            ]);
        } else {
            return $this->render($view, [
                'model' => $model,
                'isClone' => true
            ]);
        }
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws Exception
     */
    public function actionCreateEmoney()
    {
        if (Yii::$app->request->isAjax) {
            $company = Yii::$app->user->identity->company;
            $model = new Emoney([
                'company_id' => $company->id,
            ]);
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(['/cash/e-money/index', 'emoney' => $model->id]);

                /*
                Yii::$app->response->format = Response::FORMAT_JSON;
                $widget = new EmoneyFilterWidget([
                    'emoney' => Yii::$app->request->post('emoney'),
                    'company' => $company,
                ]);

                return [
                    'result' => true,
                    'id' => $model->id,
                    'name' => $model->name,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $widget->getItems(),
                    ]),
                ];
                */
            }

            return $this->renderAjax('@frontend/views/emoney/create', [
                'model' => $model,
            ]);
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @return array|Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws Exception
     */
    public function actionUpdateEmoney($id)
    {
        if (Yii::$app->request->isAjax) {
            $emoney = Yii::$app->request->post('emoney');
            $company = Yii::$app->user->identity->company;
            $model = Emoney::findOne($id);
            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $widget = new EmoneyFilterWidget([
                    'emoney' => Yii::$app->request->post('emoney'),
                    'company' => $company,
                ]);

                return [
                    'result' => true,
                    'id' => $model->id,
                    'name' => $model->name,
                    'label' => $emoney == $model->id ? $model->name : null,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $widget->getItems(),
                    ]),
                ];
            }

            return $this->renderAjax('@frontend/views/emoney/update', [
                'model' => $model,
            ]);
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param integer $id
     * @return CashOrderFlows|CashOrderForeignCurrencyFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id = null, $foreign = null)
    {
        $foreign = (int) boolval($foreign ?? Yii::$app->request->get('foreign'));
        $id = intval($id ?? Yii::$app->request->get('id'));
        $company_id = $this->company->id;
        $key = "{$foreign}_{$id}";

        if (!array_key_exists($key, $this->_models)) {
            $class = $foreign ? CashEmoneyForeignCurrencyFlows::class : CashEmoneyFlows::class;
            $query = $class::find()->andWhere([
                'id' => $id,
                'company_id' => $company_id,
            ]);

            $this->_models[$key] = $query->one();
        }

        if ($this->_models[$key] !== null) {
            return $this->_models[$key];
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     * @return CashEmoneyForeignCurrencyFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findForeignModel($id)
    {
        if (($model = CashEmoneyForeignCurrencyFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param CashEmoneyFlows $model
     * @param $copyModelId
     * @return bool
     */
    protected static function fillByFlow(CashEmoneyFlows &$model, $copyModelId)
    {
        /** @var CashEmoneyFlows $copyModel */
        if ($copyModel = CashEmoneyFlows::findOne(['id' => $copyModelId, 'company_id' => Yii::$app->user->identity->company->id])) {
            $model->setAttributes([
                'flow_type' => $copyModel->flow_type,
                'contractor_id' => $copyModel->contractor_id,
                'income_item_id' => $copyModel->income_item_id,
                'expenditure_item_id' => $copyModel->expenditure_item_id,
                'amount' => $copyModel->amount,
                'is_accounting' => $copyModel->is_accounting,
                'description' => $copyModel->description,
                'other_cashbox_id' => $copyModel->other_cashbox_id,
                'other_rs_id' => $copyModel->other_rs_id,
                'other_emoney_id' => $copyModel->other_emoney_id,
                'project_id' => $copyModel->project_id
            ]);

            return true;
        }

        return false;
    }
}
