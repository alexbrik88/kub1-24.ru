<?php

namespace frontend\modules\cash\controllers;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\excel\CashOperationsXlsHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\ContractorAutoProject;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\service\SubscribeTariffGroup;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\components\BusinessAnalyticsAccess;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\log\ILogMessage;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\AnalyticsMultiCompanyManager;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\analytics\models\credits\CreditFlow;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\cash\models\CashSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use yii\data\ArrayDataProvider;
use yii\db\Connection;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use frontend\rbac\UserRole;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DefaultController
 * @package frontend\modules\cash\controllers
 */
class DefaultController extends FrontendController
{
    public $layout = 'cash-default';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'get-xls'],
                        'allow' => true,
                        'roles' => [permissions\Cash::INDEX],
                    ],
                    [
                        'actions' => ['items', 'items-full'],
                        'allow' => true,
                        'roles' => [permissions\Cash::CREATE],
                    ],
                    [
                        'actions' => ['many-delete-flow-item'],
                        'allow' => true,
                        'roles' => [permissions\Cash::DELETE],
                    ],
                    [
                        'actions' => [
                            'many-change-flow-item',
                            'many-change-flow-project',
                            'many-change-flow-cashbox',
                            'many-change-flow-sale-point',
                            'many-change-flow-industry',
                            'many-change-flow-credit',
                            'many-change-plan-flow-date'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Contractor::CREATE],
                        'roleParams' => [
                            'type' => Yii::$app->request->getQueryParam('type'),
                        ],
                    ],
                    [
                        'actions' => [
                            'operations',
                            'onboarding-off',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\BusinessAnalytics::INDEX,
                        ],
                        'matchCallback' => function($rule, $action) {
                            return BusinessAnalyticsAccess::canAccess(BusinessAnalyticsAccess::SECTION_FINANCE);
                        },
                    ],
                    [
                        'actions' => ['odds'],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_SUPERVISOR,
                        ],
                    ],
                    [
                        'actions' => [
                            'add-modal-company',
                            'create-checking-accountant'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'onboarding-off' => ['POST'],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => [
                    'onboarding-off',
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\ErrorException
     */
    public function actionIndex()
    {
        $period = ['from' => StatisticPeriod::getDateFrom(), 'to' => StatisticPeriod::getDateTo()];
        $provider = new ArrayDataProvider([
            'allModels' => CashStatisticInfo::getPeriodStatisticInfo($period),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'dataProvider' => $provider,
        ]);
    }

    /**
     * @param $cid
     * @return array
     */
    public function actionItems($cid)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractor = $company->getContractors()->andWhere(['id' => $cid])->one();
        $contractorAutoProject = ($contractor)
            ? ContractorAutoProject::findOne([
                'contractor_id' => $cid,
                'project_status' => Project::STATUS_INPROGRESS,
                'new_flows' => true
            ]) : null;

        return [
            'expense' => ArrayHelper::getValue($contractor, 'invoice_expenditure_item_id'),
            'income' => ArrayHelper::getValue($contractor, 'invoice_income_item_id'),
            'project' => ArrayHelper::getValue($contractorAutoProject, 'project_id')
        ];
    }

    /**
     * @param $cid
     * @param $flowType
     * @return array
     */
    public function actionItemsFull($cid, $flowType)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractor = $company->getContractors()->andWhere(['id' => $cid])->one();
        $projects = $invoices = [];

        if ($company && $contractor) {

            $invoiceArray = Invoice::find()->andWhere([
                'company_id' => $company->id,
                'contractor_id' => $contractor->id,
                'is_deleted' => false,
                'invoice_status_id' => InvoiceStatus::$payAllowed,
            ])->orderBy(['document_number' => SORT_ASC])->all();

            if ($invoiceArray) {
                $flowDate = Yii::$app->request->get('date');
                $date = $flowDate ? \DateTime::createFromFormat('d.m.Y|', $flowDate) : false;
                foreach ($invoiceArray as $invoice) {
                    if ($date) {
                        $invoice->recalculateAmountOnDate($date);
                    }
                    $invoices[$invoice->id] = [
                        'id' => $invoice->id,
                        'number' => $invoice->fullNumber,
                        'date' => ($d = date_create_from_format('Y-m-d', $invoice->document_date)) ? $d->format('d.m.Y') : '',
                        'amount' => $invoice->total_amount_with_nds / 100,
                        'amountPrint' => \common\components\TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2),
                    ];
                    $invoices[$invoice->id]['text'] =
                        '№' . $invoice->fullNumber .
                        ' от ' . (($d = date_create_from_format('Y-m-d', $invoice->document_date)) ? $d->format('d.m.Y') : '') .
                        ' на ' . \common\components\TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2);
                }
            }

            $projects = ProjectCustomer::find()
                ->innerJoin(Project::tableName(), 'project.id = project_customer.project_id')
                ->select(['project.name', 'project.id'])
                ->andWhere(['project.company_id' => $company->id])
                ->andWhere(['project.status' => Project::STATUS_INPROGRESS])
                //->andWhere('start_date <= ' . (new Expression('current_date()')))
                //->andWhere('end_date >= ' . (new Expression('current_date()')))
                // filter by contractor_id
                ->andFilterWhere(['project_customer.customer_id' => ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) ? $cid : null])
                ->groupBy('project.id')
                ->indexBy('project.id')
                ->orderBy('project.name')
                ->column();
        }

        return [
            'expense' => ArrayHelper::getValue($contractor, 'invoice_expenditure_item_id'),
            'income' => ArrayHelper::getValue($contractor, 'invoice_income_item_id'),
            'projects' => $projects,
            'invoices' => $invoices
        ];
    }

    /**
     * @param int $activeTab
     * @return string
     * @throws \yii\base\Exception|\Exception
     */
    public function actionOdds($activeTab = OddsSearch::TAB_ODDS, $periodSize = 'months')
    {
        $searchModel = new OddsSearch();

        // change year
        if (\Yii::$app->request->get('OddsSearch')) {
            $year = \common\components\helpers\ArrayHelper::getValue(\Yii::$app->request->get('OddsSearch'), 'year');
            if ($year > 2010 && $year < 2100) {
                \Yii::$app->session->set('modules.reports.finance.year', $year);
                $getParams = \Yii::$app->request->queryParams;
                unset($getParams['OddsSearch']);
                return $this->redirect(Url::toRoute([Url::base()] + $getParams));
            }
        }

        $dataFull = ($periodSize == 'days') ?
            $searchModel->searchByDays($activeTab, Yii::$app->request->get()) :
            $searchModel->search($activeTab, Yii::$app->request->get());
        $data = &$dataFull['data'];
        $growingData = &$dataFull['growingData'];
        $totalData = &$dataFull['totalData'];

        $warnings = $searchModel->generateWarnings(ArrayHelper::getValue($totalData, 'growing.data'), $activeTab == OddsSearch::TAB_ODDS ? $data : []);
        CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 60);

        return $this->render('@frontend/modules/reports/views/finance/odds2', [
            'searchModel' => $searchModel,
            'data' => $data,
            'growingData' => $growingData,
            'totalData' => $totalData,
            'activeTab' => $activeTab,
            'periodSize' => $periodSize,
            'warnings' => $warnings,
        ]);
    }

    ////////////////
    // OPERATIONS //
    ////////////////

    public function beforeAction($action)
    {
        if ($action->id == 'operations') {
            $widgetWallet = Yii::$app->request->get('wallet');
            $widgetCurrency = Yii::$app->request->get('currency');

            if ($widgetWallet || $widgetCurrency) {
                Yii::$app->session->set('cash_widget_wallet', ($widgetWallet === 'all') ? null : $widgetWallet);
                Yii::$app->session->set('cash_widget_currency', ($widgetCurrency === Currency::DEFAULT_NAME) ? null : $widgetCurrency);
                return $this->redirect(Yii::$app->request->referrer ?: ['operations']);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $wallet string
     * @return string
     * @throws \Exception
     */
    public function actionOperations()
    {
        $this->layout = null;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $widgetWallet = Yii::$app->session->get('cash_widget_wallet');
        $widgetCurrency = Yii::$app->session->get('cash_widget_currency');

        $searchModel = new CashSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 50);
        $multiCompanyManager = \Yii::$app->multiCompanyManager;

        \common\models\company\CompanyFirstEvent::checkEvent($user->company, 100, true);

        return $this->render('operations', [
            'user' => $user,
            'company' => $user->company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'widgetWallet' => $widgetWallet,
            'widgetCurrency' => $widgetCurrency,
            'multiCompanyManager' => $multiCompanyManager
        ]);
    }

    public function actionGetXls($rs = null, $foreign = null, $currency = null): Response
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if (!($company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE_PLUS)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_ALL_INCLUSIVE))) {
            Yii::$app->session->setFlash('error',
                'Выгрузить данные в Excel можно только на платном тарифе ФинДиректор, ФинДиректор PRO, ФинДиректор Всё включено.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $widgetWallet = Yii::$app->session->get('cash_widget_wallet');
        $widgetCurrency = Yii::$app->session->get('cash_widget_currency');

        $searchModel = new CashSearch();
        $searchModel->setSplitInternalTransfer(false);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = 0;
        $models = $dataProvider->getModels();
        $period = StatisticPeriod::getSessionPeriod();

        try {
            CashOperationsXlsHelper::init($employee);
            CashOperationsXlsHelper::generateXls($period, $models);
            exit;
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', 'Не удалось выгрузить данные в Excel.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['operations']);
    }


    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDeleteFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                if ($model = $this->findFlowItem($tb, $id, true)) {
                    if (in_array(ILogMessage::class, class_implements($model))) {
                        LogHelper::delete($model, LogEntityType::TYPE_CASH);
                    } else {
                        $model->delete();
                    }
                }
            }
        }

        Yii::$app->session->setFlash('success', 'Операции удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyChangeFlowItem()
    {
        $data = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');
        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }
                $model->$attribute = $value;
                if (!$model->isForeign) {
                    $model->amount = $model->amount / 100;
                }
                if (in_array(ILogMessage::class, class_implements($model))) {
                    LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                } else {
                    $model->save();
                }
            }
        }

        Yii::$app->session->setFlash('success', 'Статьи по операциям изменены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyChangePlanFlowDate()
    {
        $data = Yii::$app->request->post('flowId', []);
        $searchModel = Yii::$app->request->post('CashSearch');
        $incomeDate = ArrayHelper::getValue($searchModel, 'incomeManyPlanDate');
        $expenseDate = ArrayHelper::getValue($searchModel, 'expenditureManyPlanDate');
        foreach ($data as $tb => $idArray) {
            // update plan operations only
            if ($tb != 'plan_cash_flows')
                continue;

            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                $attribute = 'date';
                $value = $model->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? $incomeDate : $expenseDate;
                $model->$attribute = DateHelper::format($value, 'Y-m-d', 'd.m.Y');
                if (!$model->save(true, [$attribute])) {
                    Yii::$app->session->setFlash('error', 'Ошибка при обновлении операций.');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
        }

        Yii::$app->session->setFlash('success', 'Плановые даты по операциям изменены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param bool $ajax
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyChangeFlowSalePoint($ajax = false)
    {
        $salePointID = (int)Yii::$app->request->post('salePointIdManyItem');

        if (!($data = Yii::$app->request->post('flowId', []))) {
            // for CardsOperations page
            if ($_tmp = Yii::$app->request->post('OperationSelectForm', [])) {
                if (isset($_tmp['id']) && is_array($_tmp['id'])) {
                    $data[CardOperation::tableName()] = [];
                    foreach ($_tmp['id'] as $id) {
                        $data[CardOperation::tableName()][] = $id;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Операции не найдены');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }

                if (!$model || !$model->hasAttribute('sale_point_id'))
                    continue;

                $model->sale_point_id = $salePointID ?: null;
                if ($model instanceof PlanCashFlows) {
                    $model->amount = (int)$model->amount / 100 / 100;
                } else {
                    //$model->amount = $model->amount / 100;
                }

                if ($model->validate(['sale_point_id'])) {
                    $model->save(false, ['sale_point_id']);
                    if (in_array(ILogMessage::class, class_implements($model))) {
                        LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                    }
                    // tin children
                    if ($model instanceof CashBankFlows) {
                        if ($model->has_tin_children) {
                            foreach ($model->children as $m)
                                $m->updateAttributes(['sale_point_id' => $model->sale_point_id]);
                        }
                    }
                } else {
                    return $this->returnAs($ajax, 0, 'Ошибка обновления операций');
                }
            }
        }

        return $this->returnAs($ajax, 1, 'Операции обновлены');
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyChangeFlowIndustry($ajax = false)
    {
        $industryID = (int)Yii::$app->request->post('industryIdManyItem');

        if (!($data = Yii::$app->request->post('flowId', []))) {
            // for CardsOperations page
            if ($_tmp = Yii::$app->request->post('OperationSelectForm', [])) {
                if (isset($_tmp['id']) && is_array($_tmp['id'])) {
                    $data[CardOperation::tableName()] = [];
                    foreach ($_tmp['id'] as $id) {
                        $data[CardOperation::tableName()][] = $id;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Операции не найдены');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }

                if (!$model || !$model->hasAttribute('industry_id'))
                    continue;

                $model->industry_id = $industryID ?: null;
                if ($model instanceof PlanCashFlows) {
                    $model->amount = (int)$model->amount / 100 / 100;
                } else {
                    //$model->amount = $model->amount / 100;
                }

                if ($model->validate(['industry_id'])) {
                    $model->save(false, ['industry_id']);
                    if (in_array(ILogMessage::class, class_implements($model))) {
                        LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                    }

                    // tin children
                    if ($model instanceof CashBankFlows) {
                        if ($model->has_tin_children) {
                            foreach ($model->children as $m)
                                $m->updateAttributes(['industry_id' => $model->industry_id]);
                        }
                    }

                } else {

                    return $this->returnAs($ajax, 0, 'Ошибка обновления операций');
                }
            }
        }

        return $this->returnAs($ajax, 1, 'Операции обновлены');
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyChangeFlowProject($ajax = false)
    {
        $projectID = (int)Yii::$app->request->post('projectIdManyItem');

        if (!($data = Yii::$app->request->post('flowId', []))) {
            // for CardsOperations page
            if ($_tmp = Yii::$app->request->post('OperationSelectForm', [])) {
                if (isset($_tmp['id']) && is_array($_tmp['id'])) {
                    $data[CardOperation::tableName()] = [];
                    foreach ($_tmp['id'] as $id) {
                        $data[CardOperation::tableName()][] = $id;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Операции не найдены');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);
                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }

                if (!$model || !$model->hasAttribute('project_id'))
                    continue;

                $model->project_id = $projectID ?: null;
                $model->amount = ($model instanceof CardOperation)
                    ? $model->amount
                    : $model->amount / 100;

                if ($model->validate(['project_id'])) {
                    $model->save(false, ['project_id']);
                    if (in_array(ILogMessage::class, class_implements($model))) {
                        LogHelper::log($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                    }

                    // tin children
                    if ($model instanceof CashBankFlows) {
                        if ($model->has_tin_children) {
                            foreach ($model->children as $m)
                                $m->updateAttributes(['project_id' => $model->project_id]);
                        }
                    }

                } else {

                    return $this->returnAs($ajax, 0, 'Ошибка обновления операций');
                }
            }
        }

        return $this->returnAs($ajax, 1, 'Операции обновлены');
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyChangeFlowCredit($ajax = false)
    {
        $creditID = Yii::$app->request->post('creditIdManyItem');

        if (!strlen($creditID)) {
            Yii::$app->session->setFlash('error', 'Договор не найден');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $creditID = (int)$creditID;

        $isValidCredit = $creditID === 0 || Credit::find()->where([
            'credit_id' => $creditID,
            'company_id' => Yii::$app->user->identity->company->id
        ])->exists();

        if (!$isValidCredit) {
            Yii::$app->session->setFlash('error', 'Договор не найден');
            return $this->redirect(Yii::$app->request->referrer);
        }

        if (!($data = Yii::$app->request->post('flowId', []))) {
            // for CardsOperations page
            if ($_tmp = Yii::$app->request->post('OperationSelectForm', [])) {
                if (isset($_tmp['id']) && is_array($_tmp['id'])) {
                    $data[CardOperation::tableName()] = [];
                    foreach ($_tmp['id'] as $id) {
                        $data[CardOperation::tableName()][] = $id;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Операции не найдены');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        foreach ($data as $tb => $idArray) {
            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);

                if ($model instanceof PlanCashFlows) {
                    continue;
                }

                if ($model instanceof CashBankFlows) {
                    $model->setScenario('update');
                }

                switch ($model->walletType) {
                    case CashFlowsBase::WALLET_BANK:
                        $attribute = 'cash_bank_flow_id';
                        break;
                    case CashFlowsBase::WALLET_CASHBOX:
                        $attribute = 'cash_order_flow_id';
                        break;
                    case CashFlowsBase::WALLET_EMONEY:
                        $attribute = 'cash_emoney_flow_id';
                        break;
                    default:
                        $attribute = null;
                        break;
                }

                if (!$attribute)
                    continue; // there no relations with credits

                if ($creditID > 0 && !$model->credit_id) {
                    $newRelation = new CreditFlow([
                        'credit_id' => $creditID,
                        'olap_flow_id' => $model->id,
                        'wallet_id' => $model->walletType,
                        "{$attribute}" => $model->id,
                    ]);
                    if ($newRelation->save()) {
                        $newRelation->credit->save(false); // refresh
                    }
                } elseif ($creditID === 0 && $model->credit_id) {
                    $oldRelation = (CreditFlow::findOne([
                        'credit_id' => $model->credit_id,
                        'olap_flow_id' => $model->id,
                        'wallet_id' => $model->walletType,
                        "{$attribute}" => $model->id,
                    ]));
                    if ($oldRelation) {
                        $oldRelation->delete();
                        $oldRelation->credit->save(false); // refresh
                    }
                }
            }
        }

        return $this->returnAs($ajax, 1, 'Операции обновлены');
    }    
    
    /**
     * @return Response|array
     * @throws NotFoundHttpException
     */
    public function actionManyChangeFlowCashbox($ajax = 0)
    {
        $cashboxID = [
            CashFlowsBase::FLOW_TYPE_INCOME => (int)Yii::$app->request->post('cashboxIdManyItemIncome'),
            CashFlowsBase::FLOW_TYPE_EXPENSE => (int)Yii::$app->request->post('cashboxIdManyItemExpense'),
        ];

        if (!($data = Yii::$app->request->post('flowId', []))) {

            if ($ajax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ['success' => 0, 'msg' => 'Операции не найдены'];
            }

            Yii::$app->session->setFlash('error', 'Операции не найдены');
            return $this->redirect(Yii::$app->request->referrer);
        }

        foreach ($data as $tb => $idArray) {

            foreach ($idArray as $id) {
                $model = $this->findFlowItem($tb, $id);

                if (!$model || !$model->hasAttribute('cashbox_id'))
                    continue;

                $model->cashbox_id = $cashboxID[$model->flow_type] ?? null;

                if ($model instanceof PlanCashFlows) {
                    $model->amount /= 10000;
                }

                if (!$model->validate()) {

                    if ($ajax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ['success' => 0, 'msg' => 'Ошибка обновления операций'];
                    }

                    Yii::$app->session->setFlash('error', 'Ошибка обновления операций');
                    return $this->redirect(Yii::$app->request->referrer);
                }

                if (in_array(ILogMessage::class, class_implements($model))) {
                    LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE);
                } else {
                    $model->save();
                }
            }
        }

        if ($ajax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => 1, 'msg' => 'Операции изменены.'];
        }

        Yii::$app->session->setFlash('success', 'Операции изменены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return array|mixed|string
     * @throws \Throwable
     */
    public function actionAddModalCompany($inn = null)
    {
        $model = Yii::$app->user->identity->company;
        $scenario = Company::getScenarioForModal();
        $model->setScenario($scenario);

        if ($inn && Yii::$app->request->isGet && $attributes = DadataClient::getCompanyAttributes($inn)) {
            $model->setAttributes($attributes, false);
            $model->save(false, ['company_type_id']);
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $modelValidate = $model->validate();
            if ($modelValidate) {
                $result = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    $model->chief_is_chief_accountant = true;
                    if ($model->setDirectorInitials() && $model->save()) {

                        return true;
                    }

                    $db->transaction->rollBack();
                    return false;
                });

                if ($result) {
                    \frontend\components\facebook\EventTracker::event('addcompany');
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ['success' => 1];
                }
            } else {
                // debug
                var_dump($model->getErrors());
                exit;
            }
        }

        return $this->renderAjax('_company/create-company_body', [
            'model' => $model,
        ]);
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreateCheckingAccountant()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $rs = Yii::$app->request->post('rs');
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            $model = new CheckingAccountant([
                'company_id' => $company->id,
            ]);
            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return [
                    'result' => true,
                    'id' => 'checking_accountant'.$model->rs,
                    'bank_name' => $model->bank_name,
                    'data_wallet' => CashFlowsBase::WALLET_BANK,
                    'data_currency' => Currency::DEFAULT_NAME,
                    'data_symbol' => ArrayHelper::getValue(Currency::$currencySymbols, Currency::DEFAULT_NAME),
                    'data_balance' => 0,
                    'data_id' => $model->id,
                    'data_rs' => $model->rs
                ];
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_create', [
                    'checkingAccountant' => $model,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @throws \Exception
     */
    public function actionOnboardingOff()
    {
        Yii::$app->user->identity->company->updateAttributes(['show_cash_operations_onboarding' => 0]);
    }

    /**
     * @param $tableName
     * @param $id
     * @param $skipNotFound
     * @return CashBankFlows|CashOrderFlows|CashEmoneyFlows|PlanCashFlows
     * @throws NotFoundHttpException
     */
    private function findFlowItem($tableName, $id, $skipNotFound = false)
    {
        switch ($tableName) {
            // RUB
            case CashBankFlows::tableName():
                $className = CashBankFlows::class;
                break;
            case CashOrderFlows::tableName():
                $className = CashOrderFlows::class;
                break;
            case CashEmoneyFlows::tableName():
                $className = CashEmoneyFlows::class;
                break;
            case PlanCashFlows::tableName():
                $className = PlanCashFlows::class;
                break;
            case AcquiringOperation::tableName():
                $className = AcquiringOperation::class;
                break;
            case CardOperation::tableName():
                $className = CardOperation::class;
                break;
            // FOREIGN
            case CashBankForeignCurrencyFlows::tableName():
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case CashOrderForeignCurrencyFlows::tableName():
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case CashEmoneyForeignCurrencyFlows::tableName():
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                $className = PlanCashForeignCurrencyFlows::class;
                break;
            default:
                throw new NotFoundHttpException('The requested page does not exist!');
        }
        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }

    public function returnAs($ajax, $success, $message)
    {
        if ($ajax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['success' => $success, 'message' => $message];
        } else {
            Yii::$app->session->setFlash(($success) ? 'success' : 'error', $message);
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
}