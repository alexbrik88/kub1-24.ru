<?php

namespace frontend\modules\cash\controllers;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\db\Exception;
use yii\web\Response;
use yii\db\Connection;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Contractor;
use frontend\rbac\permissions;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use yii\web\NotFoundHttpException;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use frontend\models\log\LogEntityType;
use common\models\cash\CashEmoneyFlows;
use frontend\components\FrontendController;
use common\components\filters\AccessControl;
use common\models\company\CheckingAccountant;
use common\models\cash\form\CashBankFlowsForm;
use frontend\modules\analytics\models\PlanCashFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use frontend\modules\cash\models\InternalTransferForm;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;

class MultiWalletController extends FrontendController {

    const WALLET_BANK = 1;
    const WALLET_CASHBOX = 2;
    const WALLET_EMONEY = 3;

    const WALLET_FOREIGN_BANK = 11;
    const WALLET_FOREIGN_CASHBOX = 12;
    const WALLET_FOREIGN_EMONEY = 13;

    const _walletsRu = [
        self::WALLET_BANK,
        self::WALLET_CASHBOX,
        self::WALLET_EMONEY
    ];

    const _walletsForeign = [
        self::WALLET_FOREIGN_BANK,
        self::WALLET_FOREIGN_CASHBOX,
        self::WALLET_FOREIGN_EMONEY
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['create-internal', 'create-bank-flow', 'create-order-flow', 'create-emoney-flow'],
                        'allow' => true,
                        'roles' => [permissions\Cash::CREATE],
                    ],
                    [
                        'actions' => ['update-internal', 'update-bank-flow', 'update-order-flow', 'update-emoney-flow', 'update-plan-flow'],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                ],
            ],
        ]);
    }

    // UPDATE

    public function actionUpdateBankFlow($originWallet, $originFlow, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');
        @list($newWalletType, $newWalletId) = explode('_', Yii::$app->request->get('wallet', $originWallet));
        $isFlowsInDifferentTables = $newWalletType != $originWallet;

        if ($isFlowsInDifferentTables) {
            $originModel = $this->_findModel($originWallet, $originFlow, true);
            $model = $this->_copyModel($newWalletType, $originModel);
            $model->rs = $newWalletId;
        } else {
            $model = $this->_findModel($originWallet, $originFlow);
            $originModel = null;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////
        if ($model->parent_id || $model->children) {
            return (new BankController($originFlow, $this->module))->runAction('update-pieces', [
                'id' => $model->parent_id ?: $model->id
            ]);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////

        if ($newWalletType == self::WALLET_BANK)
            $model->setScenario('update');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_updatePlanModel($model, $originModel) :
                $this->_updateModel($model, $originModel);

            return $this->redirect(Yii::$app->request->referrer ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/bank/foreign-update' : '/bank/update', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionUpdateOrderFlow($originWallet, $originFlow, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');
        @list($newWalletType, $newWalletId) = explode('_', Yii::$app->request->get('wallet', $originWallet));
        $isFlowsInDifferentTables = $newWalletType != $originWallet;

        if ($isFlowsInDifferentTables) {
            $originModel = $this->_findModel($originWallet, $originFlow, true);
            $model = $this->_copyModel($newWalletType, $originModel);
            $model->cashbox_id = $newWalletId;
        } else {
            $model = $this->_findModel($originWallet, $originFlow);
            $originModel = null;
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_updatePlanModel($model, $originModel) :
                $this->_updateModel($model, $originModel);

            return $this->redirect(Yii::$app->request->referrer ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/order/foreign-update' : '/order/update', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionUpdateEmoneyFlow($originWallet, $originFlow, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');
        @list($newWalletType, $newWalletId) = explode('_', Yii::$app->request->get('wallet', $originWallet));
        $isFlowsInDifferentTables = $newWalletType != $originWallet;

        if ($isFlowsInDifferentTables) {
            $originModel = $this->_findModel($originWallet, $originFlow, true);
            $model = $this->_copyModel($newWalletType, $originModel);
            $model->emoney_id = $newWalletId;
        } else {
            $model = $this->_findModel($originWallet, $originFlow);
            $originModel = null;
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_updatePlanModel($model, $originModel) :
                $this->_updateModel($model, $originModel);

            return $this->redirect(Yii::$app->request->referrer ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/emoney/foreign-update' : '/emoney/update', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionUpdatePlanFlow($originWallet, $originFlow, $isForeign = false)
    {


        $company = Yii::$app->user->identity->company;

        /** @var PlanCashFlows|PlanCashForeignCurrencyFlows $planModel */
        $planModel = $this->_findPlanModel($originWallet, $originFlow);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($planModel);
        }

        if ($planModel->load(Yii::$app->request->post())) {

            @list($newWalletType, $newWalletId) = explode('_', Yii::$app->request->post('wallet', $originWallet));
            $isFlowsInDifferentTables =
                in_array($originWallet, self::_walletsRu) && in_array($newWalletType, self::_walletsForeign) ||
                in_array($originWallet, self::_walletsForeign) && in_array($newWalletType, self::_walletsRu);

            switch ($newWalletType) {
                case self::WALLET_BANK:
                case self::WALLET_FOREIGN_BANK:
                    $realCheckingAccountID = $company->getCheckingAccountants()->where(['rs' => $newWalletId])->select('id')->scalar();
                    $planModel->payment_type = $newWalletType;
                    $planModel->checking_accountant_id = $realCheckingAccountID;
                    break;
                case self::WALLET_CASHBOX:
                case self::WALLET_FOREIGN_CASHBOX:
                    $planModel->payment_type = $newWalletType;
                    $planModel->cashbox_id = $newWalletId;
                    break;
                case self::WALLET_EMONEY:
                case self::WALLET_FOREIGN_EMONEY:
                    $planModel->payment_type = $newWalletType;
                    $planModel->emoney_id = $newWalletId;
                    break;
            }

            if ($isFlowsInDifferentTables) {

                $newModel = $this->_copyPlanModel($planModel);
                if ($newModel->_save()) {

                    Yii::$app->session->setFlash('success', 'Плановая операция обновлена');
                    $planModel->delete();
                } else {
                    // debug
                    //if (YII_ENV_DEV) {
                    //    $planModel->validate();
                    //    var_dump($planModel->getErrors());
                    //    exit;
                    //}
                    Yii::$app->session->setFlash('success', 'Не удалось обновить плановую операцию');
                }

            } else {

                if ($planModel->_update())
                    Yii::$app->session->setFlash('success', 'Плановая операция обновлена');
                else {

                    // debug
                    //if (YII_ENV_DEV) {
                    //    $planModel->validate();
                    //    var_dump($planModel->getErrors());
                    //    exit;
                    //}

                    Yii::$app->session->setFlash('success', 'Не удалось обновить плановую операцию');
                }
            }

            return $this->redirect(Yii::$app->request->referrer ?: ['/cash/operations/default']);
        }

        switch ($originWallet) {
            case self::WALLET_BANK:
                $viewWallet = '/bank/update';
                break;
            case self::WALLET_CASHBOX:
                $viewWallet = '/order/update';
                break;
            case self::WALLET_EMONEY:
                $viewWallet = '/emoney/update';
                break;
            case self::WALLET_FOREIGN_BANK:
                $viewWallet = '/bank/foreign-update';
                break;
            case self::WALLET_FOREIGN_CASHBOX:
                $viewWallet = '/order/foreign-update';
                break;
            case self::WALLET_FOREIGN_EMONEY:
                $viewWallet = '/emoney/foreign-update';
                break;
            default:
                throw new NotFoundHttpException('The requested page view does not exist.');
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($viewWallet, [
                'company' => $company,
                'model' => $planModel,
                'multiWallet' => true,
                'isPlan' => true
            ]);
        } else {
            return $this->render($viewWallet, [
                'company' => $company,
                'model' => $planModel,
                'multiWallet' => true,
                'isPlan' => true
            ]);
        }
    }

    public function actionUpdateInternal($wallet, $flow, $transferKey)
    {
        @list($id1, $id2) = explode('_', $transferKey);
        $model = $this->_findModel($wallet, $id1, true) ?: $this->_findModel($wallet, $id2);

        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if ($this->layout !== null) {
            $this->controller->layout = $this->layout;
        }

        $model = new InternalTransferForm($company, $employee, $model);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Операция перевода между счетами обновлена');
            return $this->redirect(Yii::$app->request->referrer ?: ['/cash/default/operations']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('/bank/update-internal', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('/bank/update-internal', [
                    'model' => $model,
                ]);
            }
        }
    }

    // CREATE

    public function actionCreateBankFlow($type, $rs = null, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $walletType = ($isForeign) ? self::WALLET_FOREIGN_BANK : self::WALLET_BANK;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');

        $model = $this->_getNewModel($type, $walletType, $rs);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_saveNewPlanModel($model) :
                $this->_saveNewModel($model);

            if ($isSaved) {
                if (parse_url(Yii::$app->request->referrer, PHP_URL_PATH) == '/cash/default/operations') {
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 101, true);
                }
                \frontend\components\facebook\EventTracker::event('connectbank');
                Yii::$app->session->set('multi_wallet_last_create_url', [
                    '/' . Yii::$app->controller->route,
                    'type' => $model->flow_type,
                    'rs' => $model->rs,
                    'isForeign' => $isForeign
                ]);
            }

            $redirectUrl = Yii::$app->request->get('redirectUrl', Yii::$app->request->referrer);

            return $this->redirect($redirectUrl ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/bank/foreign-create' : '/bank/create', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionCreateOrderFlow($type, $id = null, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $walletType = ($isForeign) ? self::WALLET_FOREIGN_CASHBOX : self::WALLET_CASHBOX;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');

        $model = $this->_getNewModel($type, $walletType, $id);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_saveNewPlanModel($model) :
                $this->_saveNewModel($model);

            if ($isSaved) {
                if (parse_url(Yii::$app->request->referrer, PHP_URL_PATH) == '/cash/default/operations') {
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 101, true);
                }
                Yii::$app->session->set('multi_wallet_last_create_url', [
                    '/' . Yii::$app->controller->route,
                    'type' => $model->flow_type,
                    'id' => $model->cashbox_id,
                    'isForeign' => $isForeign
                ]);
            }

            $redirectUrl = Yii::$app->request->get('redirectUrl', Yii::$app->request->referrer);

            return $this->redirect($redirectUrl ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/order/foreign-create' : '/order/create', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionCreateEmoneyFlow($type, $id = null, $isForeign = false)
    {
        $company = Yii::$app->user->identity->company;
        $walletType = ($isForeign) ? self::WALLET_FOREIGN_EMONEY : self::WALLET_EMONEY;
        $isPlanFlow = Yii::$app->request->post('is_plan_flow');

        $model = $this->_getNewModel($type, $walletType, $id);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $isSaved = ($isPlanFlow) ?
                $this->_saveNewPlanModel($model) :
                $this->_saveNewModel($model);

            if ($isSaved) {
                if (parse_url(Yii::$app->request->referrer, PHP_URL_PATH) == '/cash/default/operations') {
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 101, true);
                }
                Yii::$app->session->set('multi_wallet_last_create_url', [
                    '/' . Yii::$app->controller->route,
                    'type' => $model->flow_type,
                    'id' => $model->emoney_id,
                    'isForeign' => $isForeign
                ]);
            }

            $redirectUrl = Yii::$app->request->get('redirectUrl', Yii::$app->request->referrer);

            return $this->redirect($redirectUrl ?: ['/cash/default/operations']);
        }

        return $this->renderAjax(($isForeign) ? '/emoney/foreign-create' : '/emoney/create', [
            'company' => $company,
            'model' => $model,
            'multiWallet' => true
        ]);
    }

    public function actionCreateInternal()
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $model = new InternalTransferForm($company, $employee);

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (parse_url(Yii::$app->request->referrer, PHP_URL_PATH) == '/cash/default/operations') {
                \common\models\company\CompanyFirstEvent::checkEvent($company, 101, true);
            }
            Yii::$app->session->setFlash('success', 'Операция перевода между счетами добавлена');
            $redirectUrl = Yii::$app->request->get('redirectUrl', Yii::$app->request->referrer);
            return $this->redirect($redirectUrl ?: ['/cash/default/operations']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('/bank/create-internal', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('/bank/create-internal', [
                    'model' => $model,
                ]);
            }
        }
    }

    // _CREATE / _UPDATE

    private function _getNewModel($flowType, $walletType, $walletId)
    {
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;

        switch ($walletType) {

            case self::WALLET_BANK:
                $account =
                    $company->getCheckingAccountants()->andWhere(['rs' => $walletId])->orderBy(['type' => SORT_ASC])->one() ?:
                        $company->mainCheckingAccountant;

                $model = new CashBankFlowsForm([
                    'scenario' => 'create',
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'rs' => $account->rs ?? null,
                    'bank_name' => $account->bank_name ?? null,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'has_invoice' => false,
                ]);
                break;

            case self::WALLET_FOREIGN_BANK:
                $account =
                    $company->getCheckingAccountants()->andWhere(['rs' => $walletId])->one() ?:
                        $company->mainForeignCurrencyAccount;

                $model = new CashBankForeignCurrencyFlows([
                    //'scenario' => 'create',
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'rs' => $account->rs ?? null,
                    'swift' => $account->swift ?? null,
                    'bank_name' => $account->bank_name ?? null,
                    'bank_address' => $account->bank_address ?? null,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('Y-m-d'),
                    'has_invoice' => false,
                ]);
                break;

            case self::WALLET_CASHBOX:
                $cashbox =
                    $user->getCashboxes()->andWhere(['id' => $walletId])->one() ?:
                        $user->getCashboxes()->andWhere(['is_main' => 1])->one();

                $model = new CashOrderFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'author_id' => $user->id,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'cashbox_id' => $cashbox->id ?? null,
                    'is_accounting' => $cashbox->is_accounting ?? null,
                ]);
                break;

            case self::WALLET_FOREIGN_CASHBOX:
                $cashbox =
                    $user->getCashboxes()->andWhere(['id' => $walletId])->one() ?:
                        null;

                $model = new CashOrderForeignCurrencyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'author_id' => $user->id,
                    'date' => Yii::$app->request->get('skipDate') ? null : date('d.m.Y'),
                    'cashbox_id' => $cashbox->id ?? null,
                    'is_accounting' => $cashbox->is_accounting ?? null,
                ]);

                $model->setNextNumber();
                break;

            case self::WALLET_EMONEY:
                $emoney =
                    $company->getEmoneys()->andWhere(['id' => $walletId])->one() ?:
                        $company->getEmoneys()->andWhere(['id' => $company->emoney])->one();

                $model = new CashEmoneyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'emoney_id' => $emoney->id ?? null,
                    'is_accounting' => $emoney->is_accounting ?? null,
                    'date' => date('d.m.Y'),
                ]);

                break;

            case self::WALLET_FOREIGN_EMONEY:
                $emoney =
                    $company->getEmoneys()->andWhere(['id' => $walletId])->one() ?:
                        null;

                $model = new CashEmoneyForeignCurrencyFlows([
                    'flow_type' => $flowType,
                    'company_id' => $company->id,
                    'emoney_id' => $emoney->id ?? null,
                    'is_accounting' => $emoney->is_accounting ?? null,
                    'date' => date('d.m.Y'),
                ]);
                break;

            default:

                throw new Exception('Wallet type not found:' . $walletType);
        }

        // Contractor Tab
        if ($cid = Yii::$app->request->get('contractorId')) {
            /** @var Contractor $contractor */
            if ($contractor = Contractor::findOne(['id' => $cid, 'company_id' => $company->id])) {
                $model->contractor_id = $contractor->id;
                $model->expenditure_item_id = $contractor->invoice_expenditure_item_id;
                $model->income_item_id = $contractor->invoice_income_item_id;
            }if ($cid == CashContractorType::BALANCE_TEXT) {
                $model->contractor_id = CashContractorType::BALANCE_TEXT;
                $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
                $model->expenditure_item_id = null;
            }
        }

        // Credit Page
        if ($cid = Yii::$app->request->get('creditId')) {
            /** @var Credit $credit */
            if ($credit = Credit::findOne(['credit_id' => $cid, 'company_id' => $company->id])) {
                $model->credit_id = $credit->credit_id;
                $model->income_item_id = ($credit->credit_type == Credit::TYPE_LOAN)
                    ? InvoiceIncomeItem::ITEM_LOAN
                    :InvoiceIncomeItem::ITEM_CREDIT;
                $model->expenditure_item_id = ($credit->credit_type == Credit::TYPE_LOAN)
                    ? InvoiceExpenditureItem::ITEM_LOAN_REPAYMENT :
                      InvoiceExpenditureItem::ITEM_REPAYMENT_CREDIT;
            }
        }

        // Project Page
        if ($pid = Yii::$app->request->get('projectId')) {
            /** @var Contractor $contractor */
            if ($project = Project::findOne(['id' => $pid, 'company_id' => $company->id])) {
                $model->project_id = $project->id;
                if ($project->customers) {
                    if ($projectContractor = $project->customers[0]->customer) {
                        $model->contractor_id = $projectContractor->id;
                        $model->expenditure_item_id = $projectContractor->invoice_expenditure_item_id;
                        $model->income_item_id = $projectContractor->invoice_income_item_id;
                    }
                }
            }
        }

        return $model;
    }

    private function _saveNewPlanModel($model)
    {
        switch ($model->walletType) {
            case CashFlowsBase::WALLET_BANK:
                $flowData = Yii::$app->request->post('CashBankFlowsForm', []);
                $flowData['checking_accountant_id'] = (int)CheckingAccountant::find()->where(['rs' => $flowData['rs'], 'company_id' => $model->company_id])->select('id')->scalar();
                break;
            case CashFlowsBase::WALLET_FOREIGN_BANK:
                $flowData = Yii::$app->request->post('CashBankForeignCurrencyFlows', []);
                $flowData['checking_accountant_id'] = (int)CheckingAccountant::find()->where(['rs' => $flowData['rs'], 'company_id' => $model->company_id])->select('id')->scalar();
                break;
            case CashFlowsBase::WALLET_CASHBOX:
                $flowData = Yii::$app->request->post('CashOrderFlows', []);
                $flowData['contractor_id'] = \common\components\helpers\ArrayHelper::getValue($flowData, 'contractorInput');
                break;
            case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
                $flowData = Yii::$app->request->post('CashOrderForeignCurrencyFlows', []);
                break;
            case CashFlowsBase::WALLET_EMONEY:
                $flowData = Yii::$app->request->post('CashEmoneyFlows', []);
                break;
            case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                $flowData = Yii::$app->request->post('CashEmoneyForeignCurrencyFlows', []);
                break;
            default:
                $flowData = [];
                break;
        }

        $planFlowData = ($model->isForeign) ?
            ['PlanCashForeignCurrencyFlows' => array_merge($flowData, Yii::$app->request->post('PlanCashForeignCurrencyFlows', []))] :
            ['PlanCashFlows' => array_merge($flowData, Yii::$app->request->post('PlanCashFlows', []))];

        $planModel = ($model->isForeign) ?
            (new PlanCashForeignCurrencyFlows) :
            (new PlanCashFlows);

        $planModel->company_id = $model->company_id;
        $planModel->payment_type = $model->walletType;
        $planModel->flow_type = $model->flow_type;

        if ($planModel->load($planFlowData) && $planModel->_save()) {
            Yii::$app->session->setFlash('success', 'Плановая операция добавлена');

            return true;
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка добавления плановой операции');
            // debug
            //if (YII_ENV_DEV) {
            //    $planModel->validate();
            //    var_dump($planModel->getErrors());
            //    exit;
            //}
        }

        return false;
    }

    private function _saveNewModel($model)
    {
        if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {

            Yii::$app->session->setFlash('success', 'Операция добавлена');
            $model->linkSelectedInvoices();

            return true;

        } else {

            Yii::$app->session->setFlash('error', 'Ошибка добавления операции');

            return false;
        }
    }

    private function _updateModel($model, $originModel)
    {
        LogHelper::$useTransaction = false;

        $isUpdated = Yii::$app->db->transaction(function (Connection $db) use ($model, $originModel) {

            if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE)) {
                if ($originModel && $originModel->invoices_list ?? null) {
                    $model->linkSelectedInvoices();
                }
                if (!$originModel || LogHelper::delete($originModel, LogEntityType::TYPE_CASH)) {
                    Yii::$app->session->setFlash('success', 'Операция обновлена');

                    return true;
                }
            }

            Yii::$app->session->setFlash('error', 'Не удалось обновить операцию');
            $db->transaction->rollBack();

            return false;
        });

        return $isUpdated;
    }

    private function _updatePlanModel($model, $originModel)
    {
        LogHelper::$useTransaction = false;

        $isUpdated = Yii::$app->db->transaction(function (Connection $db) use ($model, $originModel) {
            if ($this->_saveNewPlanModel($model)) {
                if (LogHelper::delete($originModel ?: $model, LogEntityType::TYPE_CASH)) {

                    Yii::$app->session->setFlash('success', 'Операция изменена');
                    return true;
                }
            }
            $db->transaction->rollBack();
            Yii::$app->session->setFlash('success', 'Не удалось обновить операцию');

            return false;
        });

        return $isUpdated;
    }

    // _FIND

    private function _findModel($walletType, $id, $skipNotFound = false)
    {
        $company = Yii::$app->user->identity->company;

        switch ($walletType) {
            case self::WALLET_BANK:
                $className = CashBankFlowsForm::class;
                break;
            case self::WALLET_CASHBOX:
                $className = CashOrderFlows::class;
                break;
            case self::WALLET_EMONEY:
                $className = CashEmoneyFlows::class;
                break;
            case self::WALLET_FOREIGN_BANK:
                $className = CashBankForeignCurrencyFlows::class;
                break;
            case self::WALLET_FOREIGN_CASHBOX:
                $className = CashOrderForeignCurrencyFlows::class;
                break;
            case self::WALLET_FOREIGN_EMONEY:
                $className = CashEmoneyForeignCurrencyFlows::class;
                break;
            default:
                throw new NotFoundHttpException('The requested flow type does not exist!');
        }

        $model = $className::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => $company->id])
            ->one();

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }

    private function _findPlanModel($walletType, $id, $skipNotFound = false)
    {
        $company = Yii::$app->user->identity->company;
        $model = null;

        switch ($walletType) {
            case self::WALLET_BANK:
            case self::WALLET_CASHBOX:
            case self::WALLET_EMONEY:
                $model = PlanCashFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
            case self::WALLET_FOREIGN_BANK:
            case self::WALLET_FOREIGN_CASHBOX:
            case self::WALLET_FOREIGN_EMONEY:
                $model = PlanCashForeignCurrencyFlows::find()
                    ->andWhere(['id' => $id])
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['payment_type' => $walletType])
                    ->one();
                break;
        }

        if ($model === null) {

            if ($skipNotFound)
                return null;

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }

    // _COPY

    private function _copyPlanModel($originFlow)
    {
        $model = ($originFlow->isForeign) ?
            (new PlanCashFlows) :
            (new PlanCashForeignCurrencyFlows);

            $model->company_id = $originFlow->company_id;
            $model->payment_type = $originFlow->payment_type;
            $model->checking_accountant_id = $originFlow->checking_accountant_id;
            $model->cashbox_id = $originFlow->cashbox_id;
            $model->emoney_id = $originFlow->emoney_id;
            $model->contractor_id = $originFlow->contractor_id;
            $model->date = $originFlow->date;
            $model->first_date = $originFlow->first_date;
            $model->flow_type = $originFlow->flow_type;
            $model->amount = $originFlow->amount;
            $model->income_item_id = $originFlow->income_item_id;
            $model->expenditure_item_id = $originFlow->expenditure_item_id;
            $model->description = $originFlow->description;
            $model->is_internal_transfer = $originFlow->is_internal_transfer;
            $model->is_repeated = 0;
            //$model->currency_id = $originFlow->currency_id; // in model
            //$model->currency_name = $originFlow->currency_name; // in model

        return $model;
    }

    private function _copyModel($walletId, $originFlow)
    {
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;

        switch ($walletId) {
            case self::WALLET_BANK:
                $model = new CashBankFlowsForm(['company_id' => $company->id, 'rs' => null, 'has_invoice' => false]);
                break;
            case self::WALLET_CASHBOX:
                $model = new CashOrderFlows(['company_id' => $company->id, 'author_id' => $user->id, 'cashbox_id' => null, 'is_accounting' => null]);
                break;
            case self::WALLET_EMONEY:
                $model = new CashEmoneyFlows(['company_id' => $company->id, 'emoney_id' => null, 'is_accounting' => null]);
                break;
            case PlanCashFlows::tableName():
                $model = new PlanCashFlows(['company_id' => $company->id]);
                break;
            case self::WALLET_FOREIGN_BANK:
                $model = new CashBankForeignCurrencyFlows(['company_id' => $company->id, 'rs' => null, 'has_invoice' => false]);
                break;
            case self::WALLET_FOREIGN_CASHBOX:
                $model = new CashOrderForeignCurrencyFlows(['company_id' => $company->id, 'author_id' => $user->id, 'cashbox_id' => null, 'is_accounting' => null]);
                break;
            case self::WALLET_FOREIGN_EMONEY:
                $model = new CashEmoneyForeignCurrencyFlows(['company_id' => $company->id, 'emoney_id' => null, 'is_accounting' => null,]);
                break;
            case PlanCashForeignCurrencyFlows::tableName():
                $model = new PlanCashForeignCurrencyFlows(['company_id' => $company->id]);
                break;
            default:
                throw new NotFoundHttpException('The requested flow type does not exist!');
        }

        if ($originFlow) {
            $model->flow_type = $originFlow->flow_type;
            $model->contractor_id = $originFlow->contractor_id;
            $model->income_item_id = $originFlow->income_item_id;
            $model->expenditure_item_id = $originFlow->expenditure_item_id;
            $model->amount = $originFlow->amount;
            $model->date = $originFlow->date;
            $model->recognition_date = $originFlow->recognition_date;
            $model->is_prepaid_expense = $originFlow->is_prepaid_expense;
            $model->description = $originFlow->description;

            $diffAttributes = ['number', 'invoices_list'];
            foreach ($diffAttributes as $attr) {
                if (isset($originFlow->{$attr}) && array_key_exists($attr, $model->attributes)) {
                    $model->{$attr} = $originFlow->{$attr};
                }
            }
        }

        return $model;
    }
}