<?php

namespace frontend\modules\cash\controllers;

use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\rbac\permissions;
use yii\helpers\ArrayHelper;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\analytics\models\PlanCashFlows;

/**
 * Class PlanController
 */
class PlanController extends FrontendController
{
    public $layout = null;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'create-copy'],
                        'allow' => true,
                        'roles' => [permissions\Cash::CREATE],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [permissions\Cash::DELETE],
                    ],
                ],
            ],
        ]);
    }


    /**
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $company = Yii::$app->user->identity->company;
        $model = new PlanCashFlows([
            'company_id' => $company->id
        ]);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_update()) {
            Yii::$app->session->setFlash('success', 'Плановая операция добавлена.');

            return $this->redirect(Yii::$app->request->referrer ?: '/cash/default/operations');
        }

        return $this->renderAjax('plan_item_form', [
            'model' => $model,
            'company' => $company
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionCreateCopy($id)
    {
        $company = Yii::$app->user->identity->company;
        $sourceModel = $this->findPlanFlow($id);
        $model = $sourceModel->cloneSelf();

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_update()) {
            Yii::$app->session->setFlash('success', 'Плановая операция обновлена.');

            return $this->redirect(Yii::$app->request->referrer ?: '/cash/default/operations');
        }

        return $this->renderAjax('plan_item_form', [
            'model' => $model,
            'company' => $company,
            'isClone' => true
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $company = Yii::$app->user->identity->company;
        $model = $this->findPlanFlow($id);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_update()) {
            Yii::$app->session->setFlash('success', 'Плановая операция обновлена.');

            return $this->redirect(Yii::$app->request->referrer ?: '/cash/default/operations');
        }

        return $this->renderAjax('plan_item_form', [
            'model' => $model,
            'company' => $company
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findPlanFlow($id);

        if ($model == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Плановая операция удалена.');
        } else {
            Yii::$app->session->setFlash('error', 'Произошла ошибка при удалении плановой операции.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: '/cash/default/operations');
    }

    /**
     * @param $id
     * @return PlanCashFlows
     * @throws NotFoundHttpException
     */
    private function findPlanFlow($id)
    {
        /** @var PlanCashFlows $model */
        $model = PlanCashFlows::find()
            ->andWhere(['id' => $id])
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->one();

        if ($model === null) {

            throw new NotFoundHttpException('The requested page does not exist!');
        }

        return $model;
    }
}