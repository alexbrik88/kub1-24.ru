<?php

namespace frontend\modules\cash\controllers;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankForeignCurrencyFlowsToInvoice;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\form\CashBankFlowsForm;
use common\models\currency\Currency;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\components\CashControllerBase;
use frontend\modules\cash\models\CashBankSearch;
use frontend\modules\cash\models\CashBankForeignCurrencySearch;
use frontend\modules\cash\models\LinkForm;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\Dropdown;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;
use frontend\modules\cash\modules\banking\modules\alfabank_email\models\BankModel as AlphaBankEmailModel;
use frontend\modules\cash\modules\banking\modules\bspb_email\models\BankModel as BspbEmailModel;
use common\models\cash\excel\CashBankXlsHelper;

/**
 * Class BankController
 * @package frontend\modules\cash\controllers
 */
class BankController extends CashControllerBase
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authAccess' => [
                'rules' => new \yii\helpers\ReplaceArrayValue([
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]),
            ],
            'strictAccess' => [
                'rules' => [
                    [
                        'actions' => [
                            'link-invoices',
                        ],
                        'allow' => true,
                        'roles' => [permissions\document\Document::STRICT_MODE],
                    ],
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'access' => [
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'get-xls'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::INDEX],
                    ],
                    [
                        'actions' => [
                            'view',
                            'has-manual',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::VIEW],
                    ],
                    [
                        'actions' => [
                            'create',
                            'create-internal',
                            'create-copy',
                            'copy-internal',
                            'create-plan'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::CREATE],
                    ],
                    [
                        'actions' => [
                            'update',
                            'update-internal',
                            'link-invoices',
                            'update-plan',
                            'update-pieces'
                        ],
                        'allow' => true,
                        'roles' => [permissions\Cash::UPDATE],
                    ],
                    [
                        'actions' => ['un-payed-invoices'],
                        'allow' => true,
                        'roles' => [permissions\document\Invoice::ADD_CASH_FLOW],
                    ],
                    [
                        'actions' => [
                            'create-checking-accountant',
                            'update-checking-accountant',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                    [
                        'actions' => ['banking'],
                        'allow' => true,
                        'roles' => [\frontend\rbac\UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @var bool
     */
    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'create-internal' => [
                'class' => 'frontend\modules\cash\components\CreateInternalTransferAction',
                'view' => '/bank/create-internal',
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'update-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'findModel' => function ($id, $action) {
                    return $this->getIsForeign() ? $this->findForeignModel($id) : $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'copy-internal' => [
                'class' => 'frontend\modules\cash\components\UpdateInternalTransferAction',
                'view' => '/bank/update-internal',
                'copyMode' => true,
                'findModel' => function ($id, $action) {
                    return $this->findModel($id);
                },
                'redirectUrl' => function ($form, $action) {
                    return Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']);
                }
            ],
            'create-plan' => [
                'class' => 'frontend\modules\cash\components\CreatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_BANK,
                'view' => '/bank/create',
            ],
            'update-plan' => [
                'class' => 'frontend\modules\cash\components\UpdatePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_BANK,
                'view' => '/bank/update',
            ],
            'delete-plan' => [
                'class' => 'frontend\modules\cash\components\DeletePlanFlowAction',
                'walletType' => PlanCashFlows::PAYMENT_TYPE_BANK,
            ],
            'many-delete' => [
                'class' => 'frontend\modules\cash\components\ManyDeletePlanFactFlowsAction',
            ],
            'many-item' => [
                'class' => 'frontend\modules\cash\components\ManyChangePlanFactFlowsAction',
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $company = Yii::$app->user->identity->company;
            if ($company->hasBankingEmail) {
                AlphaBankEmailModel::checkNewStatements($company);
                BspbEmailModel::checkNewStatements($company);
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $strict
     * @param null $bik
     * @return mixed|string
     * @throws NotFoundHttpException
     */
    public function actionIndex($rs = null, $strict = false)
    {
        $company = Yii::$app->user->identity->company;
        $employeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
        $foreignItems = Currency::find()->select('name')->where([
            'not',
            ['id' => Currency::DEFAULT_ID],
        ])->column();

        \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 61);

        if (!isset($rs)) {
            $rs = $employeeCompany->last_rs ?: 'all';
        }
        $currentRsModel = null;
        if ($rs != 'all' && !in_array($rs, $foreignItems)) {
            $currentRsModel = $company->getRs($rs);
            if ($currentRsModel === null) {
                $employeeCompany->updateAttributes(['last_rs' => 'all']);
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        if ($employeeCompany->last_rs != $rs) {
            $employeeCompany->updateAttributes(['last_rs' => $rs]);
        }
        if (in_array($rs, $foreignItems) || ($currentRsModel && $currentRsModel->getIsForeign())) {
            return $this->foreignCurrencyIndex($company, $employeeCompany, $rs, $currentRsModel, $foreignItems);
        }

        $checkingAccountant = new CheckingAccountant([
            'company_id' => $company->id,
        ]);
        $model = new CashBankSearch([
            'company_id' => $company->id,
            'duplicates' => Yii::$app->request->get('duplicates'),
            'showPlan' => !Yii::$app->user->identity->config->cash_index_hide_plan
        ]);

        if ($flow_id = Yii::$app->request->post('flow_id')) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $model->rs = $rs;
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($strict !== false) {
            Yii::$app->session->setFlash(
                'error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' . Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => $strict,
                ])) . '.'
            );
        }

        if ($model->flow_type == null) {
            $model->flow_type = -1;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'company' => $company,
            'rs' => $rs,
            'checkingAccountant' => $checkingAccountant,
            'currentRsModel' => $currentRsModel,
            'foreign' => null,
            'foreignItems' => [],
        ]);
    }

    private function foreignCurrencyIndex($company, $employeeCompany, $rs, $currentRsModel, $foreignItems)
    {
        $checkingAccountant = new CheckingAccountant([
            'company_id' => $company->id,
        ]);

        $model = new CashBankSearch([
            'foreign' => true,
            'company_id' => $company->id,
            'currency_name' => $currentRsModel->currency->name ?? $rs,
            'duplicates' => Yii::$app->request->get('duplicates'),
        ]);

        if ($flow_id = Yii::$app->request->post('flow_id')) {
            $model->id = $flow_id;
            $dataProvider = $model->search();
        } else {
            $model->rs = $rs;
            $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
            $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        }

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($model->flow_type == null) {
            $model->flow_type = -1;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'company' => $company,
            'rs' => $rs,
            'checkingAccountant' => $checkingAccountant,
            'currentRsModel' => $currentRsModel,
            'foreign' => 1,
            'foreignItems' => $foreignItems,
        ]);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate($type = null, $rs = null)
    {
        if ($type && !in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if (Yii::$app->request->get('internal')) {
            return $this->runAction('create-internal');
        }
        if (Yii::$app->request->post('is_plan_flow')) {
            return $this->runAction('create-plan', ['flowType' => $type, 'walletId' => $rs]);
        }

        return $this->getIsForeign() ? $this->foreignCreate($type, $rs) : $this->create($type, $rs);
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    private function create($type, $rs)
    {
        $company = Yii::$app->user->identity->company;
        $account = $rs ? $company->getCheckingAccountants()->andWhere([
            'rs' => $rs,
        ])->orderBy(['type' => SORT_ASC])->one() : null;
        if ($account === null) {
            $account = $company->mainCheckingAccountant;
        }
        $model = new CashBankFlowsForm([
            'scenario' => 'create',
            'company_id' => $company->id,
            'rs' => $account->rs ?? null,
            'bank_name' => $account->bank_name ?? null,
            'date' => Yii::$app->request->get('skipDate') ? null : date(DateHelper::FORMAT_USER_DATE),
            'has_invoice' => false,
        ]);

        $model->load([
            'flow_type' => Yii::$app->request->get('flow_type', $type ?? CashBankFlowsForm::FLOW_TYPE_INCOME),
        ], '');

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($fillByFlow = Yii::$app->request->get('fill_by_flow')) {
            self::fillByFlow($model, $fillByFlow);
        }

        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            Yii::$app->session->setFlash('success', 'Операция по банку добавлена');

            return $this->redirect(Yii::$app->request->post('redirect', Yii::$app->request->referrer ?: ['index']));
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @return mixed|string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    private function foreignCreate($type, $rs)
    {
        $company = Yii::$app->user->identity->company;
        $account = $rs ? $company->getForeignCurrencyAccounts()->andWhere([
            'rs' => $rs,
        ])->orderBy(['type' => SORT_ASC])->one() : null;
        if ($account === null) {
            $account = $company->mainForeignCurrencyAccount;
        }
        $model = new CashBankForeignCurrencyFlows([
            //'scenario' => 'create',
            'company_id' => $company->id,
            'rs' => $account->rs ?? null,
            'swift' => $account->swift ?? null,
            'bank_name' => $account->bank_name ?? null,
            'bank_address' => $account->bank_address ?? null,
            'date' => Yii::$app->request->get('skipDate') ? null : date('Y-m-d'),
            'has_invoice' => false,
            'flow_type' => $type ?? CashBankFlowsForm::FLOW_TYPE_INCOME,
        ]);
        if ($account) {
            $model->populateRelation('foreignCurrencyAccount', $account);
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                Yii::$app->session->setFlash('success', 'Операция по банку добавлена');

                return $this->redirect(Yii::$app->request->post('redirect', [
                    'index',
                    'rs' => $model->rs,
                ]));
            } else {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('foreign-create', [
                'model' => $model,
            ]);
        } else {
            return $this->render('foreign-create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        return $this->getIsForeign() ? $this->foreignUpdate($id) : $this->update($id);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    private function update($id)
    {
        if (Yii::$app->request->get('is_plan_flow')) {
            return $this->runAction('update-plan', ['flowId' => $id]);
        }

        $model = $this->findModel($id);
        $model->setScenario('update');

        if ($model->children || $model->parent_id) {
            return $this->runAction('update-pieces', [
                'id' => $model->parent_id ?: $model->id
            ]);
        }

        if ($model->is_internal_transfer) {
            return $this->runAction('update-internal', ['id' => $id]);
        }

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }

            Yii::$app->session->setFlash('success', 'Операция по банку изменена');

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdatePieces($id)
    {
        return $this->getIsForeign()
            ? 'In development' // todo
            : $this->updatePieces($id);
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    private function updatePieces($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('update');

        // Enable multi-models in form
        CashBankFlowsForm::$formNameModify = true;

        $pieces = [];
        $firstModelPos = 1;
        for ($pos = $firstModelPos; $pos <= 10; $pos++) {
            /** @var $pieces CashBankFlowsForm[] */
            $pieces[$pos] = new CashBankFlowsForm([
                'scenario' => 'create',
                'id' => "_new_{$pos}",
                'parent_id' => $model->id,
                'isNewRecord' => false,
                'company_id' => $model->company_id,
                'contractor_id' => $model->contractor_id,
                'amount' => 0,
                'rs' => $model->rs,
                'date' => $model->date,
                'recognition_date' => $model->recognition_date,
                'description' => $model->description,
                'bank_name' => $model->bank_name,
                'flow_type' => $model->flow_type,
                'income_item_id' => $model->income_item_id,
                'expenditure_item_id' => $model->expenditure_item_id,
                'project_id' => $model->project_id,
                'sale_point_id' => $model->sale_point_id,
                'industry_id' => $model->industry_id,
                'source' => $model->source,
            ]);
        }

        // check first model pos
        foreach (Yii::$app->request->post() as $k => $v) {
            if (substr($k, 0, 8) == 'CashBank') {
                @list($modelName, $pos) = explode('_new_', $k);
                if (isset($pieces[$pos])) {
                    $firstModelPos = $pos;
                    break;
                }
            }
        }

        /** @var CashBankFlowsForm $requiredFirstModel */
        $requiredFirstModel = $pieces[$firstModelPos];

        if ($model->children) {
            // update pieces
            foreach ($model::getTinChildrenQuery($model->id, true)->all() as $child) {
                $child->scenario = 'update';
                $child->amount = $child->tin_child_amount / 100;
                array_unshift($pieces, $child);
            }
        } else {
            // first run (no pieces)
            $requiredFirstModel->setAttributes([
                'amount' => $model->amount / 100,
                'project_id' => $model->project_id,
                'sale_point_id' => $model->sale_point_id,
                'industry_id' => $model->industry_id,
                'income_item_id' => $model->income_item_id,
                'expenditure_item_id' => $model->expenditure_item_id,
            ]);
        }

        if (Yii::$app->request->post('ajax') !== null && $model->load(Yii::$app->request->post())) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            $manyValidation = [];
            $_validatedCount = 0;
            $_summaryAmount = 0;
            $_uniqueLinkedInvoices = [];

            foreach ($pieces as $pos => $m) {
                if ($m->load(Yii::$app->request->post()) && $m->amount > 0) {
                    // validate each row (sum >= invoice_sum)
                    $m->invoices_list = array_unique(array_filter((array)$m->invoices_list));
                    if (!empty($m->invoices_list)) {
                        // todo: task-20-477
                        $leftSum = round((float)$m->amount, 2);
                        $rightSum = round($m->getInvoicesListSum(), 2);
                        if ($leftSum > $rightSum) {
                            $m->addError('invoices_list', "Сумма слева больше сумм неоплаченных счетов на " . ($leftSum - $rightSum));
                        }
                        if (!empty(array_intersect($m->invoices_list, $_uniqueLinkedInvoices))) {
                            $m->addError('invoices_list', 'Счет оплачен в строке выше');
                        } else {
                            $_uniqueLinkedInvoices = array_merge($_uniqueLinkedInvoices, $m->invoices_list);
                        }
                    }

                    $manyValidation = array_merge($manyValidation, CashBankFlowsForm::ajaxValidateForm($m));
                    $_validatedCount++;
                    $_summaryAmount += $m->amount / 100;
                }
            }

            if ($_validatedCount === 0) {
                // drop all children
                if ($model->children) {
                    return ActiveForm::validate($model);
                } else {
                    // at least one model required
                    $manyValidation = ActiveForm::validate($requiredFirstModel);
                }
            } elseif (round((float)$_summaryAmount, 2) > round((float)$model->amount, 2)) {
                $model->addError('amount', 'Сумма строк больше на ' . TextHelper::numberFormat(abs($_summaryAmount - $model->amount)));
            } elseif (round((float)$_summaryAmount, 2) < round((float)$model->amount, 2)) {
                $model->addError('amount', 'Сумма строк меньше на ' . TextHelper::numberFormat(abs($_summaryAmount - $model->amount)));
            }

            // its like ActiveForm::validate($model, $clearErrors = false)
            $mainModelValidation = CashBankFlowsForm::ajaxValidateForm($model);

            return array_merge($mainModelValidation, $manyValidation);
        }

        if ($model->load(Yii::$app->request->post())) {

            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $pieces) {

                LogHelper::$useTransaction = false;
                if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_UPDATE)) {

                    // todo: task-20-477
                    // invoices array always equal to merged pieces invoices array
                    $model->invoices_list = [];
                    // links amount for parent and sum children are equal too
                    $model->unlinkAll('invoices', true);

                    foreach ($pieces as $pos => $m) {
                        if ($m->load(Yii::$app->request->post())) {

                            if ('_new_' === substr($m->id, 0, 5)) {
                                if ($m->amount > 0) {
                                    $m->id = null;
                                    $m->isNewRecord = true;
                                } else {
                                    //
                                    continue;
                                }

                            } else {
                                if ($m->amount > 0) {
                                    //
                                } else {
                                    $m->delete();
                                    continue;
                                }
                            }

                            $m->description = $model->description;

                            if ($m->save()) {
                                // todo: task-20-477
                                if (!empty($m->invoices_list)) {
                                    $m->linkSelectedTinChildInvoices();
                                    $model->invoices_list =
                                        array_filter(
                                            array_unique(
                                                array_merge((array)$model->invoices_list, (array)$m->invoices_list)));
                                }
                                continue;
                            }

                            Yii::$app->session->setFlash('error', 'Не удалось изменить операцию по банку #2');
                            $db->transaction->rollBack();
                            return false;
                        }
                    }

                    $model->checkTinChildren();

                    $model->linkSelectedTinParentInvoices($pieces);

                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось изменить операцию по банку #1');
                    $db->transaction->rollBack();
                    return false;
                }

                return true;
            });

            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return (bool)$isSaved;
            }

            if ($isSaved) {
                Yii::$app->session->setFlash('success', 'Операция по банку изменена');

                /** @var CashBankFlows $firstTinChild */
                if ($firstTinChild = $model::getTinChildrenQuery($model->id)->one()) {
                    $model->updateAttributes(['recognition_date' => $firstTinChild->recognition_date]);
                }
            }

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);

        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('@frontend/modules/cash/views/bank/update-pieces', [
                    'model' => $model,
                    'piecesModels' => $pieces
                ]);
            } else {
                return $this->render('@frontend/modules/cash/views/bank/update-pieces', [
                    'model' => $model,
                    'piecesModels' => $pieces
                ]);
            }
        }
    }


    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    private function foreignUpdate($id)
    {
        $model = $this->findForeignModel($id);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }
        if ($model->load(Yii::$app->request->post())
            && LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)
        ) {
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }

            Yii::$app->session->setFlash('success', 'Операция по банку изменена');

            return $this->redirect(Yii::$app->request->referrer ?? Yii::$app->request->post('redirect', [
                'index',
                'rs' => $model->rs,
            ]));
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('foreign-update', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('foreign-update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidRouteException
     */
    public function actionCreateCopy($id)
    {
        $sourceModel = $this->findModel($id);

        if ($sourceModel->is_internal_transfer) {
            return $this->runAction('copy-internal', ['id' => $id]);
        }

        $model = $sourceModel->cloneSelf();
        $model->scenario = 'create';
/*
        if (Yii::$app->request->isAjax && Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) &&
            LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, function ($model) {
                if ($model->save()) {
                    $model->linkSelectedInvoices();

                    return true;
                }

                return false;
            })
        ) {
            Yii::$app->session->setFlash('success', 'Банковкая операция скопирована');

            return $this->redirect(Yii::$app->request->get('redirect', Yii::$app->request->referrer ?? ['index']));
        }
*/
        Yii::$app->request->setUrl(\yii\helpers\Url::current([0 => $model->isForeign ? 'foreign-create' : 'create']));

        $view = $model->isForeign ? 'foreign-create' : 'create';

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax($view, [
                'model' => $model,
                'isClone' => true
            ]);
        } else {
            return $this->render($view, [
                'model' => $model,
                'isClone' => true
            ]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id, $foreign = null)
    {
        if (Yii::$app->request->get('is_plan_flow')) {
            return $this->runAction('delete-plan', ['flowId' => $id]);
        }

        $model = $foreign ? $this->findForeignModel($id) : $this->findModel($id);

        if (LogHelper::delete($model, LogEntityType::TYPE_CASH)) {
            Yii::$app->session->setFlash('success', 'Операция по банку удалена');
        } else {
            Yii::$app->session->setFlash('success', 'Операция не удалена');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param int $contractorId
     * @param int $type
     * @param int|null $flowId
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUnPayedInvoices($contractorId, $type, $flowId = null, $flowDate = null)
    {
        $isIn = ($type != CashBankFlows::FLOW_TYPE_INCOME);
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Employee $employer */
        $employer = Yii::$app->user->identity;

        if ($employer instanceof Employee) {
            $company = $employer->company;

            if ($contractor = Contractor::findOne(['id' => $contractorId])) {
                $flowModel = $flowId > 0 ? $this->findModel($flowId) : null;

                return CashBankFlowsForm::getAvailableInvoices($company, $contractor, $isIn, $flowModel, $flowDate);
            }
        }

        return [];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionCreateCheckingAccountant()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $rs = Yii::$app->request->post('rs');
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            $model = new CheckingAccountant([
                'company_id' => $company->id,
            ]);
            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                return $this->redirect(Yii::$app->request->referrer ?: ['/cash/bank/index', 'rs' => $model->rs]);

                /*
                $bankArray = $company->getCheckingAccountants()
                    ->groupBy('rs')
                    ->indexBy('rs')
                    ->orderBy('type')
                    ->all();
                $bankItems = [];
                if ($bankArray) {
                    foreach ($bankArray as $account) {
                        $bankItems[] = [
                            'label' => Html::tag('span', $account->bank_name, [
                                    'class' => 'tooltip_rs',
                                    'title' => "р/с: {$account->rs}",
                                ]) . '<span class="glyphicon glyphicon-pencil update-account" data-toggle="modal" title="Обновить" aria-label="Обновить"
                                    data-target="#update-company-rs-' . $account->id . '"></span>',
                            'url' => ['index', 'rs' => $account->rs],
                            'linkOptions' => [
                                'class' => $rs == $account->rs ? 'active' : '',
                            ],
                        ];
                    }
                    $bankCount = count($bankItems);
                    if ($bankCount > 1) {
                        $bankItems[] = [
                            'label' => 'Все счета',
                            'url' => ['index', 'rs' => 'all'],
                            'linkOptions' => [
                                'class' => $rs == 'all' ? 'active' : '',
                            ],
                        ];
                    }
                    $bankItems[] = [
                        'label' => '[ + Добавить расч / счет ]',
                        'options' => [
                            'class' => 'add-checking-accountant',
                        ],
                    ];
                }
                $company->populateRelation('checkingAccountants', $company->getCheckingAccountants()->all());

                return [
                    'result' => true,
                    'id' => $model->id,
                    'rs' => $model->rs,
                    'name' => $model->bank_name,
                    'html' => Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $bankItems,
                    ]),
                    'updateModals' => '<div id="company_rs_update_form_list">' .
                        $this->renderAjax('@frontend/views/company/form/_rs_update_form_list', [
                            'model' => $company,
                        ]) .
                        '</div>',
                ];
                */
            }

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_create', [
                    'checkingAccountant' => $model,
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param $id
     * @return array|Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdateCheckingAccountant($id)
    {
        if (Yii::$app->request->isAjax || true) {
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            /* @var $model CheckingAccountant */
            $model = $company->getCheckingAccountants()->andWhere(['id' => $id])->one();
            if ($model == null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }
            if (Yii::$app->request->post('ajax')) {
                return $this->ajaxValidate($model);
            }
            $rs = Yii::$app->request->post('rs');
            $hasActiveRs = $rs == 'all';
            $oldAccountRs = $model->rs;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                /** @var EmployeeCompany $employeeCompany */
                $employeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
                $employeeCompany->updateAttributes(['last_rs' => $model->rs]);
                /* @var $bankArray CheckingAccountant[] */
                $bankArray = $company->getCheckingAccountants()
                    ->groupBy('rs')
                    ->indexBy('rs')
                    ->orderBy('type')
                    ->all();
                $bankItems = [];
                if ($bankArray) {
                    foreach ($bankArray as $account) {
                        $bankItems[] = [
                            'label' => Html::tag('span', $account->bank_name, [
                                    'class' => 'tooltip_rs',
                                    'title' => "р/с: {$account->rs}",
                                ]) . '<span class="glyphicon glyphicon-pencil update-account" data-toggle="modal" title="Обновить" aria-label="Обновить"
                                    data-target="#update-company-rs-' . $account->id . '"></span>',
                            'url' => ['index', 'rs' => $account->rs],
                            'linkOptions' => [
                                'class' => $rs == $account->rs ? 'active' : '',
                            ],
                        ];
                        if ($rs == $account->rs) {
                            $hasActiveRs = true;
                        }
                    }
                    $bankCount = count($bankItems);
                    if ($bankCount > 1) {
                        $bankItems[] = [
                            'label' => 'Все счета',
                            'url' => ['index', 'rs' => 'all'],
                            'linkOptions' => [
                                'class' => $rs == 'all' ? 'active' : '',
                            ],
                        ];
                    }
                    $bankItems[] = [
                        'label' => '[ + Добавить расч / счет ]',
                        'options' => [
                            'class' => 'add-checking-accountant',
                        ],
                    ];
                    $company->populateRelation('checkingAccountants', $company->getCheckingAccountants()->all());

                    if ($hasActiveRs) {
                        Yii::$app->response->format = Response::FORMAT_JSON;

                        return [
                            'result' => true,
                            'id' => $model->id,
                            'rs' => $model->rs,
                            'name' => $model->bank_name,
                            'label' => $id == $model->id ? $model->bank_name : null,
                            'html' => Dropdown::widget([
                                'id' => 'user-bank-dropdown',
                                'encodeLabels' => false,
                                'items' => $bankItems,
                            ]),
                            'updateModals' => '<div>' .
                                $this->renderAjax('@frontend/views/company/form/_rs_update_form_list', [
                                    'model' => $company,
                                ]) . '</div>',
                        ];
                    }

                    return $this->redirect(Yii::$app->request->referrer ?: ['/cash/bank', 'rs' => ($rs == $oldAccountRs) ? $model->rs : null]);
                }
            }
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'result' => false,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_update', [
                    'checkingAccountant' => $model,
                    'company' => $company
                ]),
            ];
        } else {
            throw new BadRequestHttpException();
        }
    }

    /**
     * @param integer $id
     */
    public function actionBanking()
    {
        $this->layout = 'banking';

        return $this->render('banking');
    }

    /**
     * @param integer $id
     */
    public function actionLinkInvoices()
    {
        $foreign = (bool) Yii::$app->request->get('foreign');
        $company = Yii::$app->user->identity->company;
        $model = new LinkForm($company, [
            'flowClass' => $foreign ? CashBankForeignCurrencyFlows::class : CashBankFlows::class,
            'linkClass' => $foreign ? CashBankForeignCurrencyFlowsToInvoice::class : CashBankFlowToInvoice::class,
            'invoiceClass' => $foreign ? ForeignCurrencyInvoice::class : Invoice::class,
        ]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->save();

            return $this->redirect(Yii::$app->request->referrer ?: ['index']);
        }

        return $this->renderAjax('_link_invoices_form', [
            'model' => $model,
            'formData' => $model->formData(),
            'foreign' => $foreign,
        ]);
    }

    /**
     * @param integer $id
     */
    public function actionHasManual()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $period = StatisticPeriod::getSessionPeriod();

        $query = Yii::$app->user->identity->company->getCashBankFlows()->andWhere([
            'between',
            'date',
            $period['from'],
            $period['to'],
        ])->andWhere([
            'source' => CashBankStatementUpload::SOURCE_MANUAL,
        ]);

        return [
            'hasManual' => $query->exists(),
        ];
    }

    public function actionGetXls($rs = null, $foreign = null, $currency = null): Response
    {
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        if (!($company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_FINANCE_PLUS)
            || $company->getActualSubscription(SubscribeTariffGroup::BI_ALL_INCLUSIVE))) {
            Yii::$app->session->setFlash('error',
                'Выгрузить данные в Excel можно только на платном тарифе ФинДиректор, ФинДиректор PRO, ФинДиректор Всё включено.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $model = new CashBankSearch([
            'company_id' => $company->id,
            'foreign' => $foreign,
            'currency_name' => $currency,
            'showPlan' => !Yii::$app->user->identity->config->cash_index_hide_plan
        ]);
        $model->rs = $rs;

        $params = ArrayHelper::merge($_POST, Yii::$app->request->queryParams);
        $dataProvider = $model->search($params, StatisticPeriod::getSessionPeriod());
        $dataProvider->pagination->pageSize = 0;
        $models = $dataProvider->getModels();
        $period = StatisticPeriod::getSessionPeriod();

        try {
            CashBankXlsHelper::init($employee);
            CashBankXlsHelper::generateXls($period, $models);
            exit;
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', 'Не удалось выгрузить данные в Excel.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * @param integer $id
     * @return CashBankFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CashBankFlowsForm::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     * @return CashBankFlows the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findForeignModel($id)
    {
        if (($model = CashBankForeignCurrencyFlows::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param CashBankFlows $model
     * @param $copyModelId
     * @return bool
     */
    protected static function fillByFlow(CashBankFlows &$model, $copyModelId)
    {
        if ($copyModel = CashBankFlows::findOne(['id' => $copyModelId, 'company_id' => Yii::$app->user->identity->company->id])) {
            $model->setAttributes([
                'flow_type' => $copyModel->flow_type,
                'rs' => $copyModel->rs,
                'contractor_id' => $copyModel->contractor_id,
                'income_item_id' => $copyModel->income_item_id,
                'expenditure_item_id' => $copyModel->expenditure_item_id,
                'amount' => $copyModel->amount,
                'description' => $copyModel->description,
                'project_id' => $copyModel->project_id
            ]);

            return true;
        }

        return false;
    }
}
