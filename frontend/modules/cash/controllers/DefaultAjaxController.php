<?php

namespace frontend\modules\cash\controllers;

use common\models\employee\Employee;
use frontend\modules\cash\models\CashSearch;
use Yii;
use yii\web\Response;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;
use common\models\employee\Config;
use frontend\components\FrontendController;
use common\components\filters\AccessControl;

/**
 * Class DefaultAjaxController
 * @package frontend\modules\cash\controllers
 */
class DefaultAjaxController extends FrontendController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['get-plan-fact-data'],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string|null
     */
    public function actionGetPlanFactData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->request->post('chart-plan-fact-ajax')) {

            $period = Yii::$app->request->post('period');
            $offset = Yii::$app->request->post('offset');
            $purse = Yii::$app->request->post('purse');
            $onPage = Yii::$app->request->post('onPage');
            $filters = Yii::$app->request->post('filters');

            $this->_savePeriodState($period);

            $searchModel = new CashSearch();
            $dataProvider = $searchModel->search($filters); // build main query

            return $this->render('@frontend/modules/cash/views/default/_partial/head-chart', [
                'model' => $searchModel,
                'customOffset' => $offset,
                'customPeriod' => $period,
                'customPurse' => $purse,
                'userConfig' => Yii::$app->user->identity->config,
            ]);
        }

        return null;
    }

    /**
     * @param $period
     */
    protected function _savePeriodState($period)
    {
        /** @var Config $config */
        $config = Yii::$app->user->identity->config;
        $attr = 'cash_operations_chart_period';

        if (isset($config->{$attr})) {
            if ($period == "days" && $config->{$attr} != 0)
                $config->updateAttributes([$attr => 0]);
            elseif ($period == "months" && $config->{$attr} != 1)
                $config->updateAttributes([$attr => 1]);
        }
    }
}