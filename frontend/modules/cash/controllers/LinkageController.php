<?php

namespace frontend\modules\cash\controllers;

use Yii;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashFlowLinkage;
use frontend\modules\cash\models\LinkageSearch;
use frontend\modules\cash\models\LinkageForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LinkageController implements the CRUD actions for CashFlowLinkage model.
 */
class LinkageController extends Controller
{
    private $canCreate = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Cash::LINKAGE,
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\permissions\Cash::LINKAGE,
                        ],
                        'matchCallback' => function () {
                            $this->canCreate = ArrayHelper::getValue(Yii::$app->user, [
                                'identity',
                                'company',
                                'canCreateCashFlowLinkage',
                            ], false);

                            return true;
                        }
                    ],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'except' => [
                    'delete',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id != 'delete') {
                $this->canCreate = ArrayHelper::getValue(Yii::$app->user, [
                    'identity',
                    'company',
                    'canCreateCashFlowLinkage',
                ], false);
            }

            return true;
        }

        return false;
    }

    /**
     * Lists all CashFlowLinkage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LinkageSearch(Yii::$app->user->identity->company);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CashFlowLinkage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!$this->canCreate) {
            return $this->renderAjax('limit');
        }

        $company = Yii::$app->user->identity->company;
        $model = new LinkageForm(Yii::$app->user->identity->company);
        $success = false;

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
        }

        return $this->renderAjax('create', [
            'company' => $company,
            'model' => $model,
            'success' => $success,
        ]);
    }

    /**
     * Updates an existing CashFlowLinkage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $company = Yii::$app->user->identity->company;
        $linkage = $this->findModel($id);

        if (!$this->canCreate && $linkage->status == CashFlowLinkage::STATUS_INACTIVE) {
            return $this->renderAjax('limit');
        }

        $model = new LinkageForm($company, $linkage);
        $success = false;

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = true;
        }

        return $this->renderAjax('update', [
            'company' => $company,
            'model' => $model,
            'success' => $success,
        ]);
    }

    /**
     * Deletes an existing CashFlowLinkage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $success = false;

        if (Yii::$app->request->isPost && $model->delete()) {
            $success = true;
        }

        return $this->renderAjax('delete', [
            'model' => $model,
            'success' => $success,
        ]);
    }

    /**
     * Finds the CashFlowLinkage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CashFlowLinkage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $company_id = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);
        if (($model = CashFlowLinkage::findOne(['id' => $id, 'company_id' => $company_id, 'is_deleted' => false])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
