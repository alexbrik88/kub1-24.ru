<?php
namespace frontend\modules\cash\modules\banking\components;

use common\components\ImageHelper;
use common\models\bank\Bank;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use kartik\select2\Select2;
use Yii;
use yii\base\Exception;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class AccountSelectWidget extends Widget
{
    /**
     * @var yii\widgets\ActiveForm
     */
    public $form;
    /**
     * @var AbstractBankModel
     */
    public $bankModel;
    /**
     * @var string
     */
    public $action = 'index';
    /**
     * CheckingAccountant[]
     * @var array
     */
    public $accounts;
    /**
     * @var array
     */
    public $urlParams = [];

    public $linkOptions = [];
    public $itemOptions = [];

    public function init()
    {
        parent::init();

        if (!$this->bankModel instanceof AbstractBankModel) {
            throw new Exception("The bankModel must by instanceof AbstractBankModel");
        }
    }

    public function run()
    {
        /** @var CheckingAccountant[] $accountArray */
        $accountArray = $this->accounts ? : $this->bankModel->company->bankingAccountants;
        $items = ArrayHelper::map($accountArray, 'id', function ($acc) { return $acc['rs'] . ' ' . $acc['bank_name']; });
        $itemsData = [];
        $p = Yii::$app->request->get('p');

        foreach ($accountArray as $account) {
            $alias = Banking::aliasByBik($account->bik);
            $url = [
                "/cash/banking/{$alias}/default/{$this->action}",
                'account_id' => $account->id,
                'p' => $p,
            ];

            $bank = Bank::find()->andWhere(['bik' => $account->bik, 'is_blocked' => 0])->one();
            if ($bank !== null && $bank->little_logo_link) {
                $logo = ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                    'class' => 'little_logo_bank',
                    'style' => 'display: inline-block;',
                ]);
            } else {
                $logo = '/img/icons/bank1.png';
            }

            $itemsData[$account->id] = [
                'data-url' => Url::to(array_merge($url, $this->urlParams)),
                'data-logo' => ($account->sysBank && $account->sysBank->little_logo_link) ?
                    \common\components\ImageHelper::getThumbSrc($account->sysBank->getUploadDirectory() . $account->sysBank->little_logo_link) : null
            ];
        }

        if ($this->form) {
            return $this->form->field($this->bankModel, 'account_id', [
                'labelOptions' => [
                    'class' => 'control-label bold-text',
                ]
            ])->dropDownList($items, [
                'options' => $itemsData,
                'style' => 'width: 100%;'
            ])->hint(Html::tag('span', Html::img('/img/loader/ajax-loader.gif'), [
                'class' =>'banking-accoun-select-loader hidden',
                'style' => 'position: absolute; right: -5px; top: 32px;',
            ]));
        } else {
            $content = Html::beginTag('div', ['class' => 'form-group']);
            $content .= Html::activeLabel($this->bankModel, 'account_id', ['class' => 'control-label bold-text']);
            $content .= Html::activeDropDownList($this->bankModel, 'account_id', $items, [
                'options' => $itemsData,
                'class' => 'form-control',
                'style' => 'width: 100%;'
            ]);
            $content .= Html::tag('span', Html::img('/img/loader/ajax-loader.gif'), [
                'class' =>'banking-accoun-select-loader hidden',
                'style' => 'position: absolute; right: -5px; top: 6px;',
            ]);
            $content .= Html::endTag('div');

            return $content;
        }
    }
}
