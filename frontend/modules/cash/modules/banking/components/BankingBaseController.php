<?php

namespace frontend\modules\cash\modules\banking\components;

use Yii;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Default controller for the `banking` module
 */
class BankingBaseController extends Controller
{
    public $layoutWrapperCssClass;
    public $layout = '@frontend/modules/cash/modules/banking/views/layouts/main';

    /**
     * Run this route if the request is not Ajax
     * @var array|string
     */
    public $pageRoute = 'cash/bank/index';
    public $pageRouteParams = [];

    protected $renderMethod = 'render';

    /**
     * @var array
     */
    public $notAjaxAction = [
        'return',
        'upload',
        'pay-bill',
        'registration',
        'login',
        'auth',
        'delete',
        'sso',
    ];

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
            Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

            if (Yii::$app->getRequest()->getIsAjax() &&
                !Yii::$app->getRequest()->getIsPjax() &&
                !Yii::$app->getRequest()->post('ajax')
            ) {
                $this->renderMethod = 'renderAjax';
            }

            if (strpos(Yii::$app->request->referrer, 'cash/banking') === false)
                Yii::$app->session->set('bankingRealReferrer', Yii::$app->request->referrer);

            return true;
        }

        return false;
    }

    /**
     * This method is invoked right after an action is executed.
     * @param  yii\base\Action $action
     * @param  mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        /**
         * If the request is not Ajax rendering parent page
         */
        if (!Yii::$app->request->isAjax && !in_array($action->id, $this->notAjaxAction)) {
            $domDocument = new \DOMDocument();
            @$domDocument->loadHTML($result);

            if ($bankingElement = $domDocument->getElementById('banking-module-pjax')) {
                $bankingContent = "";
                $innerNodes = $bankingElement->childNodes;
                for ($i = 0; $i < $innerNodes->length; $i++) {
                    $bankingContent .= $domDocument->saveHTML($innerNodes->item($i));
                }
                $this->view->params['bankingContent'] = $bankingContent;
            }

            try {
                $params = Banking::routeDecode(Yii::$app->request->get('p'));
                $route = ArrayHelper::remove($params, 0);
                if (is_array($params)) {
                    Yii::$app->request->setQueryParams(array_merge(Yii::$app->request->getQueryParams(), $params));
                }
                $result = Yii::$app->runAction($route, $params);
            } catch (\Exception $e) {
                $result = Yii::$app->runAction($this->pageRoute, $this->pageRouteParams);
            }
        }

        return $result;
    }
}
