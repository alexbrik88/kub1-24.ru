<?php

namespace frontend\modules\cash\modules\banking\components;

use common\models\bank\Bank;
use common\models\bank\BankingParams;
use common\models\Company;
use frontend\modules\cash\modules\banking\Module;
use frontend\modules\cash\modules\banking\modules;
use Yii;

/**
 * Default controller for the `banking` module
 */
class Banking
{
    const ROUTE_BANK_INDEX = 0;
    const ROUTE_TAX_ROBOT_BANK = 1;
    const ROUTE_TAX_ROBOT_PARAMS = 2;
    const ROUTE_DOCUMENTS_PAYMENT_ORDER = 3;
    const ROUTE_ANALYTICS_OPTIONS = 4;
    const ROUTE_CASH_OPERATIONS = 5;

    /**
     * Модели интегрированных API банков, для которых доступно получение выписки
     * @var array
     */
    public static $modelClassArray = [
        modules\sberbank\models\BankModel::class,
        modules\otkrytiye\models\BankModel::class,
        modules\tinkoff\models\BankModel::class,
        modules\tochka\models\BankModel::class,
        modules\modulbank\models\BankModel::class,
        modules\alfabank_email\models\BankModel::class,
        modules\uralsib\models\BankModel::class,
        modules\bspb_email\models\BankModel::class,
    ];

    /**
     * Модели интегрированных API банков, для которых доступна отправка платежного поручения
     * @var array
     */
    public static $paymentModelClassArray = [
        modules\modulbank\models\BankModel::class,
        modules\otkrytiye\models\BankModel::class,
        modules\tinkoff\models\BankModel::class,
        modules\tochka\models\BankModel::class,
        modules\sberbank\models\BankModel::class,
    ];

    /**
     * Модели интегрированных API банков, через которые доступна регистрация
     * @var array
     */
    public static $registerModelClassArray = [
        modules\otkrytiye\models\BankModel::class,
        modules\sberbank\models\BankModel::class
    ];

    /**
     * БИК банков, для которых доступна подача заявки на РКО
     * @var array
     */
    public static $bikListApplicationApi = [
        '044525974', // АО «Тинькофф Банк»
    ];

    /**
     * Страницы, где есть API банков
     * @var array
     */
    public static $routeArray = [
        self::ROUTE_BANK_INDEX => '/cash/bank/index',
        self::ROUTE_TAX_ROBOT_BANK => '/tax/robot/bank',
        self::ROUTE_TAX_ROBOT_PARAMS => '/tax/robot/params',
        self::ROUTE_DOCUMENTS_PAYMENT_ORDER => '/documents/payment-order/view',
        self::ROUTE_ANALYTICS_OPTIONS => '/analytics/options/levels',
        self::ROUTE_CASH_OPERATIONS => '/cash/default/operations'
    ];

    /**
     * БИК банков, для которых доступно получение выписки
     *
     * @return array
     */
    public static function bikList()
    {
        $bikList = [];
        foreach (self::$modelClassArray as $class) {
            $bikList = array_merge($bikList, $class::$bikList);
        }

        return $bikList;
    }

    /**
     * БИК банков, для которых доступна отправка платежного поручения
     *
     * @return array
     */
    public static function bikListPaymentApi()
    {
        $bikList = [];
        foreach (self::$paymentModelClassArray as $class) {
            $bikList = array_merge($bikList, $class::$bikList);
        }

        return $bikList;
    }

    public static function classByBik($bik, $default = null)
    {
        foreach (self::$modelClassArray as $class) {
            if (in_array($bik, $class::$bikList)) {
                return $class;
            }
        }

        return $default;
    }

    public static function aliasByBik($bik, $default = null)
    {
        if (($class = self::classByBik($bik)) !== null) {
            return $class::$alias;
        }

        return $default;
    }

    /**
     * @param string $bik
     * @return Bank
     */
    public static function getBankByBik($bik, $default = null)
    {
        $bank = ($class = self::classByBik($bik)) ? Bank::findOne([
            'bik' => $class::BIK,
            'is_blocked' => false,
        ]) : null;

        return $bank ? : $default;
    }

    public static function routeEncode(array $url)
    {
        if (isset($url[0]) && ($i = array_search($url[0], self::$routeArray)) !== false) {
            $url[0] = $i;
        }

        return base64_encode(json_encode($url));
    }

    public static function currentRouteEncode()
    {
        $route = Yii::$app->getRequest()->getQueryParams();

        if (isset($route['p'])) {
            return $route['p'];
        }

        $route[0] = '/' . Yii::$app->controller->getRoute();

        return self::routeEncode($route);
    }

    public static function currentRoute()
    {
        $route = Yii::$app->getRequest()->getQueryParams();

        if (isset($route['p'])) {
            return self::routeDecode($route['p']);
        }

        $route[0] = '/' . Yii::$app->controller->getRoute();

        return $route;
    }

    public static function routeDecode($url, $default = null)
    {
        $url = json_decode(base64_decode($url), true);
        if (isset($url[0])) {
            if (is_numeric($url[0]) && isset(self::$routeArray[$url[0]])) {
                $url[0] = self::$routeArray[$url[0]];
            }

            return $url;
        }

        return $default;
    }

    public static function hasIntegration($company_id, $bank_alias)
    {
        return BankingParams::find()->where(['company_id' => $company_id, 'bank_alias' => $bank_alias])->exists();
    }

    public static function isTaxrobotPage()
    {
        if ($params = (array)self::routeDecode(\Yii::$app->request->get('p'))) {
            if (isset($params[0]) && strpos($params[0], 'tax/robot') !== false) {
                return true;
            }
        }

        return false;
    }

    public static function isCashOperationsPage()
    {
        if ($params = (array)self::routeDecode(\Yii::$app->request->get('p'))) {
            if (isset($params[0]))
                if (strpos($params[0], 'cash/default') !== false || strpos($params[0], 'analytics/finance') !== false) {
                    return true;
            }
        }

        return false;
    }
}
