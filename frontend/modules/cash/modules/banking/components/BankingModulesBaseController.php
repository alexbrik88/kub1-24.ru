<?php

namespace frontend\modules\cash\modules\banking\components;

use common\models\Company;
use common\models\bank\BankingParams;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\models\BankRegistrationForm;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `banking` module
 */
class BankingModulesBaseController extends BankingBaseController
{
    /**
     * @var string
     */
    public $layout = '@banking/views/layouts/bank';
    /**
     * @var frontend\modules\cash\modules\banking\models\AbstractBankModel
     */
    public $bankModelClass;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => ['set-autoload'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => ['return', 'pay-bill'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Redirect to needed page after authorize on bank page
     * @param $action
     * @return bool|\yii\web\Response
     */
    public function beforeAction($action)
    {
        $urlKey = 'bankingAfterLoginRedirectUrl';

        if ($action->id == 'index') {
            if (Yii::$app->request->get($urlKey)) {

                Yii::$app->session->set($urlKey, Yii::$app->request->get($urlKey));

            } elseif (Yii::$app->session->get($urlKey)) {

                return $this->redirect(Yii::$app->session->get($urlKey));

            } else {

                Yii::$app->session->remove($urlKey);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * set statement autoload mode
     */
    public function actionSetAutoload($accountId)
    {
        $account = Yii::$app->user->identity->company->getCheckingAccountants()->andWhere(['id' => $accountId])->one();
        if ($account !== null) {
            $account->autoload_mode_id = Yii::$app->request->post('autoload') ? : null;
            $account->save(true, ['autoload_mode_id']);
        }

        return;
    }

    /**
     * @return string
     */
    public function actionPayment($account_id = null, $po_id = null)
    {
        $this->layout = '@banking/views/layouts/payment';
        $model = $this->getModel($this->bankModelClass::SCENARIO_DEFAULT);
        $model->account_id = $account_id;
        $model->paymentOrder = $this->findPaymentOrder($po_id);

        if ($model->isValidToken()) {
            $model->setScenario($this->bankModelClass::SCENARIO_PAYMENT_ORDER);
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return $this->render('@banking/views/all-banks/payment', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                Url::remember(Url::current(), $this->bankModelClass::$alias.'ReturnRedirect');
                $model->setAuthState();

                return $this->redirect($model->getAuthUrl());
            }
        }

        return $this->render('@banking/views/all-banks/payment', ['model' => $model, 'sent' => false]);
    }

    /**
     * @return string
     */
    public function actionPayBill($uid)
    {
        $this->layout = '@banking/views/layouts/pay-bill';

        /* @var Invoice $model */
        $invoice = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->byDeleted(false)
            ->one();

        if ($invoice === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->getModel($this->bankModelClass::SCENARIO_DEFAULT, new Company(['inn' => $invoice->contractor_inn]));
        $model->payBill = $invoice;

        if ($model->getTmpToken()) {
            $model->setScenario($this->bankModelClass::SCENARIO_PAY_BILL);
            if ($model->validate()) {
                return $this->render('@banking/views/all-banks/pay-bill', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        } else {
            Url::remember(Url::current(), $this->bankModelClass::$alias.'ReturnRedirect');
            $model->setAuthState();

            return $this->redirect($model->getAuthUrl());
        }
    }

    /**
     * @return string
     */
    public function actionRegistration()
    {
        /* @var $model AbstractBankModel */
        $model = $this->getModel($this->bankModelClass::SCENARIO_REGISTRATION, new Company());

        $queryParams = Yii::$app->getRequest()->getQueryParams();
        $queryParams[0] = "/cash/banking/{$this->bankModelClass::$alias}/default/auth";
        $route = ArrayHelper::merge($queryParams, ['action' => $this->action->id]);

        Url::remember(Url::toRoute($route), $this->bankModelClass::$alias.'ReturnRedirect');
        $model->setAuthState();

        return $this->redirect($model->getAuthUrl());
    }

    /**
     * @return string
     */
    public function actionLogin($page = null)
    {
        /* @var $model AbstractBankModel */
        $model = $this->getModel($this->bankModelClass::SCENARIO_REGISTRATION, new Company());

        $queryParams = Yii::$app->getRequest()->getQueryParams();
        $queryParams[0] = "/cash/banking/{$this->bankModelClass::$alias}/default/auth";
        $route = ArrayHelper::merge($queryParams, ['action' => $this->action->id]);

        Url::remember(Url::toRoute($route), $this->bankModelClass::$alias.'ReturnRedirect');
        $model->setAuthState();

        return $this->redirect($model->getAuthUrl());
    }

    /**
     * @return string
     */
    public function actionAuth()
    {
        $this->layout = '//main';
        if (!Yii::$app->user->isGuest) {
            Yii::$app->user->logout(false);
        }

        $queryParams = Yii::$app->getRequest()->getQueryParams();
        $redirect = [
            'invoice' => ['/documents/invoice/first-create', 'type' => Documents::IO_TYPE_OUT],
            'taxrobot' => ['/tax/robot/index'],
            'analytics' => ['/analytics/options/start'],
        ];
        $redirectUrl = ArrayHelper::getValue($redirect, ArrayHelper::getValue($queryParams, 'page'), ['/site/index']);

        /* @var $model AbstractBankModel */
        $model = $this->getModel($this->bankModelClass::SCENARIO_REGISTRATION, new Company());

        $form = new BankRegistrationForm($model, $queryParams);
        if ($form->getIsInitialized()) {
            if ($form->needConfirm) {
                if (!$form->load(Yii::$app->request->post()) || !$form->save()) {
                    $model->afterRegistration($form, [
                        'failMessage' => 'Пользователь с таким email уже существует.'
                    ]);
                    $method = Yii::$app->request->isAjax ? 'renderAjax' : 'render';

                    return $this->$method('@banking/views/all-banks/password', [
                        'model' => $form,
                        'email' => $form->getUser()->email,
                    ]);
                }
            } else {
                $form->save();
            }

            if ($form->getIsSaved()) {
                $form->sendRegistrationEmails();
                Yii::$app->user->login($form->getUser());
                $model->afterRegistration($form, $queryParams);

                if ($form->needConfirm) {
                    $model->updateCompanyAccounts($form);
                    $model->updateIntegrationData($form);
                }

            } else {
                $model->afterRegistration($form, $queryParams);
            }
        }

        if ($model->getTmpToken()) {
            $model->removeTmpToken();
        }

        if (!Yii::$app->user->isGuest) {
            return $this->redirect($redirectUrl);
        } else {
            throw new NotFoundHttpException('Во время регистрации пользователя произошла ошибка. Попробуйте позже еще раз.');
        }
    }

    /**
     * @return string
     */
    public function actionDelete()
    {
        $condition = [
            'company_id' => Yii::$app->user->identity->company->id,
            'bank_alias' => $this->bankModelClass::$alias,
        ];
        $hasData = BankingParams::find()->where($condition)->exists();

        if (Yii::$app->request->isPost) {
            if ($hasData) {
                if (BankingParams::deleteAll($condition)) {
                    $hasData = false;
                    Yii::$app->session->setFlash('success', 'Интеграция с банком успешно удалена.');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось удалить интеграцию с банком.');
                }
            } else {
                Yii::$app->session->setFlash('success', 'Данные интеграции с банком отсутствуют.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?? ['index']);
    }

    /**
     * @return BankModel
     */
    public function getModel($scenario, Company $company = null)
    {
        $model = $this->view->params['bankingModel'] = new $this->bankModelClass(
            $company ? : Yii::$app->user->identity->company,
            ['scenario' => $scenario]
        );

        return $model;
    }

    /**
     * @return PaymentOrder|null
     */
    public function findPaymentOrder($po_id, $skipPaid = true)
    {
        $model = PaymentOrder::findOne([
            'id' => $po_id,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        if ($model === null) {
            throw new NotFoundHttpException('Платежное поручение не найдено.');
        }

        if ($skipPaid && $model->payment_order_status_id == PaymentOrderStatus::STATUS_PAID) {
            throw new ForbiddenHttpException('Платежное поручение уже оплачено.');
        }

        return $model;
    }

    /**
     * @return string
     */
    public static function generateGUID()
    {
        mt_srand((double)microtime() * 10000);

        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = substr($charid, 0, 8) . $hyphen
            . substr($charid, 8, 4) . $hyphen
            . substr($charid, 12, 4) . $hyphen
            . substr($charid, 16, 4) . $hyphen
            . substr($charid, 20, 12);

        return $uuid;
    }
}
