<?php

namespace frontend\modules\cash\modules\banking\components\vidimus;

use common\models\Contractor;
use common\models\ContractorAccount;
use common\models\company\CompanyType;
use yii\helpers\ArrayHelper;

/**
 * Class ContractorHelper
 * @package frontend\modules\cash\modules\banking\components\vidimus
 */
class ContractorHelper
{
    protected static $_companyTypeArray;

    protected static $_findData = [
        'companyId' => '', // ID компании
        'contragent' => '', // Наименование контрагента из выписки
        'isPhysical' => false, // Признак физлица
        'currentAccount' => '', // Р/с контрагента
        'inn' => '', // ИНН контрагента
        'kpp' => '', // КПП контрагента
        'opf' => '', // ID организационно правовой формы контрагента
        'name' => '', // Название контрагента
    ];

    public static function getIsPhysical($rs, $contragent, $target = null) : bool
    {
        if ($rs) {
            if (strpos($rs, '40803') === 0 ||
                strpos($rs, '40810') === 0 ||
                strpos($rs, '40813') === 0 ||
                strpos($rs, '40817') === 0 ||
                strpos($rs, '40820') === 0 ||
                strpos($rs, '40824') === 0 ||
                strpos($rs, '423') === 0 ||
                strpos($rs, '426') === 0 ||
                strpos($rs, '455') === 0 ||
                strpos($rs, '457') === 0
            ) {
                return true;
            }
            if (strpos($rs, '30233') === 0 && $contragent && strpos($contragent, '//') !== false) {
                return true;
            }
        }
        return false;
    }

    public static function getIsPhysicalVidimus(Vidimus $vidimus) : bool
    {
        return self::getIsPhysical($vidimus->currentAccount, $vidimus->contragent, $vidimus->target);
    }

    public static function getIsPhysicalPostData(array $postData) : bool
    {
        $data = $postData['contragent'] ?? [];

        return self::getIsPhysical($data['currentAccount'] ?? '', $data['name'] ?? '', $postData['target'] ?? '');
    }

    /**
     * @param $name
     * @param $kpp
     * @return int
     */
    public static function companyTypeArray()
    {
        if (!isset(self::$_companyTypeArray)) {
            self::$_companyTypeArray = CompanyType::find()->orderBy([
                'LENGTH([[name_short]])' => SORT_DESC,
            ])->all();
        }

        return self::$_companyTypeArray;
    }

    /**
     * @param $name
     * @param $kpp
     * @return int
     */
    public static function getOpfAndName($name)
    {
        $opfId = null;
        $contractorName = '';
        $typeArray = self::companyTypeArray();

        $nameWords = preg_split("/[\s\"]+/", $name);

        foreach ($typeArray as $type) {
            if ($type->name_short && in_array($type->name_short, $nameWords)) {
                $opfId = $type->id;
                $contractorName = mb_ereg_replace($type->name_short, '', $name);
                break;
            } elseif ($type->name_full && mb_stristr($name, $type->name_full) !== false) {
                $opfId = $type->id;
                $contractorName = mb_eregi_replace($type->name_full, '', $name);
                break;
            }
        }

        if ($contractorName === '') {
            $contractorName = $name;
        }

        $contractorName = mb_substr(trim($contractorName, ' \t\n\r\0\x0B\"\''), 0, 255);

        return [$opfId, $contractorName];
    }

    public static function getFindDataFromVidimus(Vidimus $vidimus) : array
    {
        return self::getFindData([
            'companyId' => $vidimus->company->id,
            'contragent' => $vidimus->contragent ?? '',
            'rs' => $vidimus->currentAccount ?? '',
            'inn' => $vidimus->inn ?? '',
            'kpp' => $vidimus->kpp ?? '',
            'target' => $vidimus->target ?? '',
        ]);
    }

    public static function getFindDataFromPostData(array $data, int $companyId) : array
    {
        return self::getFindData([
            'companyId' => $companyId,
            'contragent' => $data['contragent']['name'] ?? '',
            'rs' => $data['contragent']['currentAccount'] ?? '',
            'inn' => $data['contragent']['inn'] ?? '',
            'kpp' => $data['contragent']['kpp'] ?? '',
            'target' => $data['target'] ?? '',
        ]);
    }

    public static function getFindData(array $data) : array
    {
        $contragent = $data['contragent'] ?? '';
        $rs = $data['rs'] ?? '';
        $inn = $data['inn'] ?? '';
        $kpp = $data['kpp'] ?? '';
        $target = $data['target'] ?? '';

        list($opf, $name) = self::getOpfAndName($contragent);
        $isPhysical = self::getIsPhysical($rs, $contragent, $target);
        if ($isPhysical) {
            $opf = null;
            if (strlen($inn) != 12) {
                $inn = '';
            }
            $kpp = '';
            $nameItems = explode('//', $contragent);
            if (strpos($rs, '30233') === 0 && count($nameItems) == 5) {
                $name = $nameItems[1];
            } elseif (count($nameItems) > 1) {
                $name = $nameItems[0];
            }
        }

        return [
            'companyId' => $data['companyId'],
            'contragent' => $contragent,
            'isPhysical' => $isPhysical,
            'rs' => $rs,
            'inn' => $inn,
            'kpp' => $kpp,
            'opf' => $opf,
            'name' => $name,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function findQuery($data) : \yii\db\ActiveQuery
    {
        $companyId = $data['companyId'];
        $contragent = $data['contragent'];
        $isPhysical = $data['isPhysical'];
        $rs = $data['rs'];
        $inn = $data['inn'];
        $kpp = $data['kpp'];
        $opf = $data['opf'];
        $name = $data['name'];

        $query = Contractor::find()->andWhere([
            'contractor.is_deleted' => 0
        ])->andWhere([
            'or',
            ['contractor.company_id' => $companyId],
            ['contractor.company_id' => null],
        ]);

        $name = mb_substr($name, 0, 255);

        if ($isPhysical) {
            $query->andFilterWhere([
                'contractor.face_type' => Contractor::TYPE_PHYSICAL_PERSON,
            ]);
            if (intval($inn) > 0) {
                $query->andWhere([
                    'or',
                    ['contractor.name' => $name],
                    ['contractor.ITN' => $inn],
                ]);
                $query->addOrderBy([
                    '({{contractor}}.[[ITN]] = :inn)' => SORT_DESC
                ])->addParams([':inn' => $inn]);
            } else {
                $query->andWhere([
                    'contractor.name' => $name
                ]);
            }
        } else {
            if (intval($inn) > 0) {
                $query->andWhere([
                    'contractor.ITN' => $inn,
                ]);
                /*
                $query->andFilterWhere([
                    'contractor.PPC' => intval($kpp) > 0 ? $kpp : null,
                ]);
                */
                if (intval($kpp) > 0) {
                    $query->addOrderBy([
                        '({{contractor}}.[[PPC]] = :kpp)' => SORT_DESC
                    ])->addParams([':kpp' => $kpp]);
                } else {
                    $query->addOrderBy([
                        '(IFNULL({{contractor}}.[[PPC]], "") = "")' => SORT_DESC
                    ]);
                }
            } else {
                if ($opf) {
                    $name = mb_substr($name, 0, 255);
                    $query->andWhere([
                        'or',
                        ['contractor.name' => $name],
                        ['contractor.name' => '"'.$name.'"'],
                    ]);
                    $query->addOrderBy([
                        '({{contractor}}.[[company_type_id]] = :opf)' => SORT_DESC
                    ])->addParams([':opf' => $opf]);
                } else {
                    $query->andWhere(['contractor.name' => mb_substr($contragent, 0, 255)]);
                }
            }
        }

        $query->leftJoin(ContractorAccount::tableName(), '{{contractor_account}}.[[contractor_id]]={{contractor}}.[[id]]');
        $query->addOrderBy([
            '({{contractor_account}}.[[rs]] = :rs)' => SORT_DESC,
        ])->addParams([':rs' => $rs]);

        return $query;
    }

    /**
     * @return Contractor|null
     */
    public static function findContractor($findData) : ?Contractor
    {
        return self::findQuery($findData)->one();
    }

    /**
     * @return Contractor|null
     */
    public static function findContractorByVidimus(Vidimus $vidimus) : ?Contractor
    {
        $findData = self::getFindDataFromVidimus($vidimus);

        return self::findQuery($findData)->one();
    }

    /**
     * @return Contractor|null
     */
    public static function contractorByVidimusExists(Vidimus $vidimus) : bool
    {
        $findData = self::getFindDataFromVidimus($vidimus);

        return self::findQuery($findData)->exists();
    }

    /**
     * @return Contractor|null
     */
    public static function findContractorByPostData(array $postData, int $companyId) : ?Contractor
    {
        $findData = self::getFindDataFromPostData($postData, $companyId);

        return self::findContractor($findData);
    }
}
