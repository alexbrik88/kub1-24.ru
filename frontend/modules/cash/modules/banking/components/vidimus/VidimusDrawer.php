<?php
namespace frontend\modules\cash\modules\banking\components\vidimus;

use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashBankStatementUpload;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\TaxKbk;
use frontend\modules\cash\modules\banking\widgets\StatementSummaryWidget;
use Yii;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * Class VidimusDrawer
 * @package frontend\modules\cash\modules\banking\components\vidimus
 */
class VidimusDrawer
{
    private $company;
    private $vidimuses;
    private $accountId;
    private $accountBik;
    private $accountCurrency;
    private $isValidCompany = false;
    private $html = '';

    public $dateStart;
    public $dateEnd;
    public $balanceStart;
    public $balanceEnd;
    public $totalIncome;
    public $totalExpense;
    public $parentPage;
    public $selectButtons;
    public $cancelButton;
    public $uploadData = [
        'isValidCompany' => 0,
        'companyData' => [],
        "dateStart" => '',
        "dateEnd" => '',
        "balanceStart" => 0,
        "balanceEnd" => 0,
        "totalIncome" => 0,
        "totalExpense" => 0,
        'accounts' => [],
    ];

    public static $incomeTargetStrArray = [
        'личные средства' => 2,
        'внесение наличных' => 2,
        'перевод собственных средств' => 9,
        'комиссия' => 8,
        'комиссии' => 8,
    ];

    /**
     * VidimusDrawer constructor.
     * @param $vidimuses
     */
    public function __construct(Company $company, $vidimuses, $uploadData)
    {
        $this->company = $company;
        $this->vidimuses = (array) $vidimuses;
        $this->uploadData = array_merge($this->uploadData, $uploadData);
        $this->uploadData['companyData']['inn'] = $company->inn;
        $this->uploadData['companyData']['ifns_ga'] = $company->ifns_ga;
        $this->uploadData['companyData']['oktmo'] = $company->oktmo;
        foreach ($vidimuses as $key => $value) {
            if ($value->company_inn) {
                $this->uploadData['isValidCompany'] = 1;
                break;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function setPeriodData($data)
    {
        $this->dateStart = $data['dateStart'] ?? '';
        $this->dateEnd = $data['dateEnd'] ?? '';
        $this->balanceStart = $data['balanceStart'] ?? '';
        $this->balanceEnd = $data['balanceEnd'] ?? '';
        $this->totalIncome = $data['totalIncome'] ?? '';
        $this->totalExpense = $data['totalExpense'] ?? '';
        $this->detailingByRs = $data['detailingByRs'] ?? [];
    }

    /**
     * @return string
     */
    public function draw($bankInfo, $action = null)
    {
        $this->html .= $this->head($action);
        $this->html .= $this->tableBegin();
        $this->html .= $this->tableBody($bankInfo);
        $this->html .= $this->tableEnd();

        return $this->html;
    }

    /**
     * @return string
     */
    public function uploadDataInputs()
    {
        $html = '';
        foreach ($this->uploadData as $key1 => $value1) {
            if (is_array($value1)) {
                foreach ($value1 as $key2 => $value2) {
                    if (is_array($value2)) {
                        foreach ($value2 as $key3 => $value3) {
                            $html .= Html::hiddenInput(sprintf('uploadData[%s][%s][%s]', $key1, $key2, $key3), $value3)."\n";
                        }
                    } else {
                        $html .= Html::hiddenInput(sprintf('uploadData[%s][%s]', $key1, $key2), $value2)."\n";
                    }
                }
            } else {
                $html .= Html::hiddenInput(sprintf('uploadData[%s]', $key1), $value1)."\n";
            }
        }

        return $html;
    }

    /**
     * @return string
     */
    private function head($action = null)
    {
        if ($action === null) {
            $action = [
                '/cash/banking/default/upload',
                'p' => $this->parentPage,
            ];
        }

        $html = '';
        if (!$this->uploadData['isValidCompany']) {
            if (count($this->vidimuses) > 0) {
                $msg = 'Загружаемая выписка не содержит операций по вашей компании.<br>Возможно вы загружаете не тот файл.';
            } else {
                $msg = 'Загружаемая выписка не содержит операций по счету.';
            }
            $html .= Html::tag('div', $msg, [
                'style' => 'color: red;'
            ]);
        }
        $scrollDownImage = Html::tag('img', null, [
            'class' => 'banking-modal-scroll-down pull-right',
            'src' => '/img/chevron-sign-down.png',
            'width' => '32',
            'height' => '32',
        ]);
        $html .= '<hr/>';

        $html .= Html::beginForm($action, 'post', ['id' => 'vidimus_form_ajax'])."\n";
        $html .= Html::beginTag('div', ['class' => 'vidimus_form_ajax_content'])."\n";
        $html .= '<div class="vidimus-head">Проверка на дубли' . $scrollDownImage  .'</div>';
        $html .= Html::hiddenInput('source', CashBankStatementUpload::SOURCE_FILE)."\n";

        $html .= $this->uploadDataInputs();

        $html .= StatementSummaryWidget::widget([
            "dateStart" => $this->uploadData['dateStart'],
            "dateEnd" => $this->uploadData['dateEnd'],
            "balanceStart" => $this->uploadData['balanceStart'],
            "balanceEnd" => $this->uploadData['balanceEnd'],
            "totalIncome" => $this->uploadData['totalIncome'],
            "totalExpense" => $this->uploadData['totalExpense'],
        ]);

        return $html;
    }

    /**
     * @return string
     */
    public function tableBegin()
    {
        $html = '<div class="vidimus-table-wrap">'
            .'<table class="table vidimus-table" style="table-layout: fixed; width: 646px;">'
            .'<tr class="vidimus-table-head">'
            .'<td style="word-wrap: break-word; width: 3%; padding-left: 4px !important;"><input type="checkbox" class="select_all_items" name="" value="1"></td>'
            .'<td style="word-wrap: break-word; width: 15%">Дата</td>'
            .'<td style="word-wrap: break-word; width: 12%">№ П/П</td>'
            .'<td style="word-wrap: break-word; width: 15%">Сумма</td>'
            .'<td style="word-wrap: break-word; width: 20%">Контрагент</td>'
            .'<td style="word-wrap: break-word; width: 20%">Назначение</td>'
            .'<td style="word-wrap: break-word; width: 15%">Тип</td>'
            .'</tr>';

        return $html;
    }

    /**
     * @return string
     */
    public function tableBody($bankInfo)
    {
        $html = '';
        $company = $this->company;
        $expenditures = InvoiceExpenditureItem::getSorted($company->id);
        $incomes = InvoiceIncomeItem::getList($company->id);
        $actionItems = '';
        foreach (Vidimus::$contractorActions as $key => $value) {
            $actionItems .= Html::tag('div', $value, [
                'class' => 'item',
                'data-id' => $key,
            ]) . "\n";
        }
        $actions = Html::tag('div', Vidimus::$contractorActions[Vidimus::CONTRACTOR_UPDATE], [
            'class' => 'vidimus-select-title',
            'title' => Vidimus::$contractorActions[Vidimus::CONTRACTOR_UPDATE],
            'data-tooltipster' => ['maxWidth' => 300],
        ]);
        $actions .= Html::tag('div', $actionItems, ['class' => 'vidimus-select-items hidden']);
        $actions = Html::tag('div', $actions, ['class' => "vidimus-select red-vidimus"]);

        foreach ((array) $this->vidimuses as $key => $vidimus) {
            $actionTarget = 'action-' . $key;
            $vidimus->checkRequiredParam();

            $isSelf = $company->inn == $vidimus->inn;

            $vidimusTypeId = '';
            $vidimusTypeTittle = '';
            $vidimusTypeCss = '';
            if ($vidimus->flowType == Vidimus::FLOW_IN) {
                $itemId = $vidimus->incomeItemId();
                if (isset($incomes[$itemId])) {
                    $vidimusTypeId = $incomes[$itemId]->id;
                    $vidimusTypeTittle = $incomes[$itemId]->name;
                }
                $vidimusTypeCss = $vidimus->specify ? 'red-vidimus' : '';
            } elseif ($vidimus->flowType == Vidimus::FLOW_OUT) {
                if (($itemId = $vidimus->expenseItemId()) && isset($expenditures[$itemId])) {
                    $vidimusTypeId = $expenditures[$itemId]->id;
                    $vidimusTypeTittle = $expenditures[$itemId]->name;
                    $vidimusTypeCss = $vidimus->specify ? 'red-vidimus' : '';
                } else {
                    $vidimusTypeId = '';
                    $vidimusTypeTittle = 'Уточнить';
                    $vidimusTypeCss = 'red-vidimus';
                }
            }

            $expenditures_html = '';
            if ($vidimus->flowType == 'in') {
                foreach ($incomes as $income) {
                    $expenditures_html .= '<div data-id="' . $income->id . '">' . $income->name . '</div>';
                }
            } else {
                foreach ($expenditures as $expenditure) {
                    $expenditures_html .= '<div data-id="' . $expenditure->id . '">' . $expenditure->name . '</div>';
                }
            }

            $contractorAction = null;
            $new = '';
            if ($vidimus->new) {
                $new .= Html::tag('div', $vidimus->innExist ? 'Конрагент есть, но отличается КПП' : 'Новый', [
                    'class' => 'new-contractor',
                ]) . "\n";
                if ($vidimus->innExist) {
                    $new .= Html::hiddenInput("vidimus[{$key}][contragent][action]", Vidimus::CONTRACTOR_UPDATE, [
                        'id' => 'vidimus-select-input-' . $actionTarget,
                    ]) . "\n";
                    $new .= self::propSelect(
                        $actionTarget,
                        Vidimus::$contractorActions[Vidimus::CONTRACTOR_UPDATE],
                        Vidimus::$contractorActions
                    );
                }
            }
            $checked = !$vidimus->dublicate && (!$vidimus->error || (
                count($vidimus->failedAttributes) == 1 &&
                ($vidimus->failedAttributes[0] == 'inn' || $vidimus->failedAttributes[0] == 'currentAccount')
            ));
            $dublicated = ($vidimus->dublicate) ? '<div class="vidimus-diblicate">Дубль</div>' : '';

            $inputs = Html::checkbox("vidimus[{$key}][upload]", $checked, [
                'disabled' => $vidimus->disabled,
                'class' => 'select_item item_' . $vidimus->flowType . ($checked ? ' checked_default' : ''),
            ]) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][type]", $vidimus->type) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][date]", $vidimus->date) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][recognitionDate]", $vidimus->recognitionDate) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][projectName]", $vidimus->projectName) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][salePoint]", $vidimus->salePoint) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][number]", $vidimus->number) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][flow_type]", $vidimus->flowType) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][total]", $vidimus->total) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][target]", $vidimus->target) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][vidimus_type]", $vidimusTypeId, ['class' => 'vidimus_type']) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][doInsert]", $vidimus->doInsert) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][name]", $vidimus->contragent) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][inn]", $vidimus->inn) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][kpp]", $vidimus->kpp) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][currentAccount]", $vidimus->currentAccount) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][bik]", $vidimus->bik) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][bankName]", $vidimus->bankName) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][correspAccount]", $vidimus->correspAccount) . "\n";
            if ($vidimus->isPhysical !== null) {
                $inputs .= Html::hiddenInput("vidimus[{$key}][contragent][isPhysical]", $vidimus->isPhysical) . "\n";
            }

            $inputs .= Html::hiddenInput("vidimus[{$key}][account_rs]", $vidimus->company_rs ?? null) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][bank][name]", $bankInfo['name'] ?? null) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][bank][number]", $bankInfo['number'] ?? null) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][taxpayers_status]", $vidimus->taxpayers_status) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][kbk]", $vidimus->kbk) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][oktmo_code]", $vidimus->oktmo_code) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][payment_details]", $vidimus->payment_details) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][tax_period_code]", $vidimus->tax_period_code) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][document_number_budget_payment]", $vidimus->document_number_budget_payment) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][document_date_budget_payment]", $vidimus->document_date_budget_payment) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][payment_type]", $vidimus->payment_type) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][uin_code]", $vidimus->uin_code) . "\n";

            $trErrorStyle = '';
            $tdErrorStyle = '';
            if ($vidimus->error) {
                $inputs .= Html::tag('div', $vidimus->error, [
                    'style' => '
                        position: absolute;
                        left: 5px;
                        bottom: 0;
                        color: #860000;
                        width: 415px;
                        text-align: left;
                    ',
                ]);
                $trErrorStyle = 'background-color: #ffb7b7;';
                $tdErrorStyle = 'padding-bottom: 17px;';
            }
            $flowSum = "<span class=\"$vidimus->flowType\"></span><span>$vidimus->total</span>";
            $flowTarget = mb_strlen($vidimus->target) > 100 ? mb_substr($vidimus->target, 0, 100) . '...' : $vidimus->target;
            $flowTarget = Html::tag('div', $flowTarget, [
                'title' => $vidimus->target,
                'data-tooltipster' => ['maxWidth' => 300],
            ]);
            $vidimusType = Html::tag('div', $vidimusTypeTittle, ['class' => 'vidimus-type-title', 'title' => $vidimusTypeTittle]);
            $vidimusType = Html::tag('div', $vidimusType, ['class' => "vidimus-type $vidimusTypeCss"]);
            $vidimusType .= Html::tag('div', $expenditures_html, ['class' => 'vidimus-type-item hidden']);

            $html .= Html::beginTag('tr', $vidimus->error ? ['style' => $trErrorStyle] : []);
            $html .= Html::tag('td', $inputs, ['style' => 'position: relative; width: 3%; padding-left: 4px;' . $tdErrorStyle]);
            $html .= Html::tag('td', $vidimus->date . $dublicated, ['style' => 'word-wrap: break-word; width: 15%']);
            $html .= Html::tag('td', $vidimus->number, ['style' => 'word-wrap: break-word; width: 12%']);
            $html .= Html::tag('td', $flowSum, ['style' => 'word-wrap: break-word; width: 15%']);
            $html .= Html::tag('td', $vidimus->contragent ? $vidimus->contragent . $new : '', [
                'style' => 'word-wrap: break-word; width: 20%',
            ]);
            $html .= Html::tag('td', $flowTarget, ['style' => 'word-wrap: break-word; width: 20%']);
            $html .= Html::tag('td', $vidimusType, ['style' => 'word-wrap: break-word; width: 15%; position: relative;']);
            $html .= Html::endTag('tr');
        }

        return $html;
    }

    /**
     * @return string
     */
    private function tableEnd()
    {
        $html = "</table>\n</div>\n</div>\n</form>";

        $html .= Html::tag('div', $this->renderButtons(), [
            'class' => 'd-flex pt-2',
            'style' => 'position: -webkit-sticky; position: sticky; bottom: 0; background-color: #fff;'
        ]);

        $html .= $this->tableScripts();

        return $html;
    }

    /**
     * Renders the statement upload buttons.
     * @return string the rendering result
     */
    protected function renderButtons()
    {
        return Html::submitButton('Принять и Сохранить', [
            'class' => 'btn btn-primary statement-form-submit ladda-button',
            'form' => 'vidimus_form_ajax',
            'data-style' => 'expand-right',
            'disabled' => !$this->uploadData['isValidCompany'],
        ]) . $this->renderSelectButtons() . $this->renderCancelButton();
    }

    /**
     * Renders the select operations by type buttons.
     * @return string the rendering result
     */
    protected function renderSelectButtons()
    {
        if (($selectButtons = $this->selectButtons) !== false) {
            return $this->selectButtons ?? Html::button('Только приход', [
                'class' => 'btn yellow ml-3 banking_select_items_by_flow_type',
                'data-target' => 'input.select_item.checked_default.item_in',
            ]).Html::button('Только расход', [
                'class' => 'btn yellow ml-3 banking_select_items_by_flow_type',
                'data-target' => 'input.select_item.checked_default.item_out',
            ]).Html::button('Все', [
                'class' => 'btn yellow ml-3 banking_select_items_by_flow_type px-4',
                'data-target' => 'input.select_item.checked_default',
            ]);
        } else {
            return null;
        }
    }

    /**
     * Renders the close button.
     * @return string the rendering result
     */
    protected function renderCancelButton()
    {
        if (($cancelButton = $this->cancelButton) !== false) {
            return $this->cancelButton ?? Html::a('Отменить', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn btn-primary banking-module-link banking-cancel ml-auto',
            ]);
        } else {
            return null;
        }
    }

    public function tableScripts()
    {
        return Html::script('
            $(".vidimus-select-label").tooltipster({
               animation: "fade",
               delay: 200,
               theme: "tooltipster-kub",
               trigger: "click",
               interactive: true,
               functionReady: function (origin, tooltipEl) {
                    $(".vidimus-select-item").on("click", function (e) {
                        console.log($(this).html());
                        var popup = $(this).closest(".vidimus-select-popup");
                        $(popup.data("input")).val($(this).data("value"));
                        $(popup.data("label")).html($(this).html()).removeClass("red-vidimus").tooltipster("hide");
                    });
                }
            });
        ');
    }

    /**
     * @return string
     */
    public static function propSelect($target, $label, $items, $form = '#vidimus_form_ajax')
    {
        $selectItems = '';
        foreach ($items as $id => $val) {
            $selectItems .= Html::tag('div', $val, [
                'class' => 'vidimus-select-item',
                'data-value' => $id,
            ]) . "\n";
        }
        $html = Html::tag('div', $label, [
            'id' => 'vidimus-select-label-' . $target,
            'class' => 'red-vidimus vidimus-select-label ' . $target,
            'data-tooltip-content' => '#vidimus-select-popup-' . $target,
            'title' => $label,
        ]);
        $html .= Html::tag('div', Html::tag('div', $selectItems, [
            'id' => 'vidimus-select-popup-' . $target,
            'class' => 'vidimus-select-popup ' . $target,
            'data-input' => '#vidimus-select-input-' . $target,
            'data-label' => '#vidimus-select-label-' . $target,
            'data-form' => $form,
        ]), [
            'class' => 'hidden'
        ]) . "\n";

        return $html;
    }

    public static function getDismissModalCancelButton()
    {
        return Html::a('Отменить', 'javascript:void(0)', [
            'class' => 'btn btn-primary banking-module-link banking-cancel pull-right',
            'data-dismiss' => 'modal',
        ]);
    }
}
