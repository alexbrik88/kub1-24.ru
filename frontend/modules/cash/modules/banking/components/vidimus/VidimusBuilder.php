<?php

namespace frontend\modules\cash\modules\banking\components\vidimus;

use Carbon\Carbon;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\ContractorAccount;
use common\models\dictionary\bik\BikDictionary;
use Exception;
use frontend\components\XlsHelper;
use frontend\models\ContractorSearch;
use frontend\modules\cash\models\CashContractorType;
use yii\base\BaseObject;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * Class VidimusBuilder
 * @package frontend\modules\cash\modules\banking\components\vidimus
 */
class VidimusBuilder extends BaseObject
{
    /**
     * Document's parser parameters
     */
    const DOCUMENT_START_TAG = 'СекцияДокумент=';
    const DOCUMENT_START_TAG_PAY = 'СекцияДокумент=Банковский ордер';
    const DOCUMENT_END_TAG = 'КонецДокумента';
    const EXPLODE_SEPARATOR = '=';

    const TAG_RS = 'РасчСчет';
    const TAG_ACCOUNT_START = 'СекцияРасчСчет';
    const TAG_ACCOUNT_END = 'КонецРасчСчет';
    const TAG_DATE_START = 'ДатаНачала';
    const TAG_DATE_END = 'ДатаКонца';
    const TAG_BALANCE_START = 'НачальныйОстаток';
    const TAG_BALANCE_END = 'КонечныйОстаток';
    const TAG_TOTAL_INCOME = 'ВсегоПоступило';
    const TAG_TOTAL_EXPENSE = 'ВсегоСписано';

    /**
     * Error texts
     */
    const NO_INN_PAYMENT = 'В выписке не найдены платежи по Вашей компании. Пожалуйста, проверьте ИНН, расчетный счет и другие реквизиты, которые введены в "Профиль компании". Так же, убедитесь что вы загружаете выписку по Вашей компании.';
    const NO_COMPANY_INN = 'У вашей компании не задан ИНН. Заполните "Профиль компании"';

    /**
     * @var UploadedFile
     */
    private $file;
    /**
     * @var UploadedFile
     */
    private $_fileDescriptor;
    /**
     * @var Company
     */
    private $company;
    /**
     * The bank account number of the company
     * @var
     */
    private $companyRs;
    /**
     * The bank account numbers of the company
     * @var
     */
    private $companyRsArray = [];

    private $accountCurrency = null;

    /**
     * @var
     */
    private $vidimuses = [];

    /**
     * @var
     */
    public $bankInfo;

    /**
     * @var
     */
    public $bankMode;

    /**
     * @var
     */
    private $counter;

    /**
     * @var
     */
    private $insideVidimus;

    /**
     * @var
     */
    private $lastPayer;

    /**
     * @var array Данные по платежной операции
     */
    private $operation_data;

    private $currentLineNumber;
    private $documentLineNumber;

    private $account = [
        self::TAG_RS => '',
        self::TAG_DATE_START => '',
        self::TAG_DATE_END => '',
        self::TAG_BALANCE_START => '',
        self::TAG_BALANCE_END => '',
        self::TAG_TOTAL_INCOME => '',
        self::TAG_TOTAL_EXPENSE => '',
    ];

    private $uploadData = [];

    /**
     * @var array Данные по счетам в выписке
     */
    private $statementAccontsData = [];

    public $insideAccount = false;

    public $errors = [];
    public $findCompanyInn = false;
    public $findCompanyIfns = false;

    public static $replacemetData =[
        'Плательщик1' => 'Плательщик',
        'Получатель1' => 'Получатель',
        'ПлательщикБанк1' => 'ПлательщикБанк',
        'ПолучательБанк1' => 'ПолучательБанк',
        'ПлательщикРасчСчет' => 'ПлательщикСчет',
        'ПолучательРасчСчет' => 'ПолучательСчет',
    ];

    public static $emptyData =[
        'Номер' => '',
        'Сумма' => '',
        'Дата' => '',
        'ДатаСписано' => '',
        'ДатаПоступило' => '',
        'НазначениеПлатежа' => '',
        'НазначениеПлатежа1' => '',
        'НазначениеПлатежа2' => '',
        'Плательщик' => '',
        'ПлательщикИНН' => '',
        'ПлательщикКПП' => '',
        'ПлательщикСчет' => '',
        'ПлательщикБИК' => '',
        'ПлательщикБанк' => '',
        'ПлательщикКорсчет' => '',
        'Получатель' => '',
        'ПолучательИНН' => '',
        'ПолучательКПП' => '',
        'ПолучательСчет' => '',
        'ПолучательБИК' => '',
        'ПолучательБанк' => '',
        'ПолучательКорсчет' => '',
        'СтатусСоставителя' => '',
        'ПоказательКБК' => '',
        'ОКАТО' => '',
        'ПоказательОснования' => '',
        'ПоказательПериода' => '',
        'ПоказательНомера' => '',
        'ПоказательДаты' => '',
        'ПоказательТипа' => '',
        'Очередность' => '',
    ];

    /**
     * VidimusBuilder constructor.
     * @param $files
     * @param $company
     */
    public function __construct(UploadedFile $file = null, Company $company = null, $rs = null)
    {
        $this->file = $file;
        $this->company = $company;
        $this->companyRs = $rs;
    }

    /**
     * tmpfile descriptor
     * @param $descriptor
     */
    public function setFileDescriptor($descriptor)
    {
        $this->_fileDescriptor = $descriptor;
    }

    /**
     * Соответствие параметров контрагента атрибутам объекта операции
     * @param $flow_type
     * @return array
     */
    private function customerParams($flow_type)
    {
        switch ($flow_type) {
            case Vidimus::FLOW_OUT:
                return [
                    'contragent' => 'Получатель',
                    'inn' => 'ПолучательИНН',
                    'kpp' => 'ПолучательКПП',
                    'currentAccount' => 'ПолучательСчет',
                    'bik' => 'ПолучательБИК',
                    'bankName' => 'ПолучательБанк',
                    'correspAccount' => 'ПолучательКорсчет',
                ];
            case Vidimus::FLOW_IN:
                return [
                    'contragent' => 'Плательщик',
                    'inn' => 'ПлательщикИНН',
                    'kpp' => 'ПлательщикКПП',
                    'currentAccount' => 'ПлательщикСчет',
                    'bik' => 'ПлательщикБИК',
                    'bankName' => 'ПлательщикБанк',
                    'correspAccount' => 'ПлательщикКорсчет',
                ];
            default:
                return [];
        }
    }

    public function uploadData()
    {
        return $this->uploadData;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function build()
    {
        $this->uploadData = [
            'dateStart' => '',
            'dateEnd' => '',
            'balanceStart' => 0,
            'balanceEnd' => 0,
            'totalIncome' => 0,
            'totalExpense' => 0,
            'accounts' => [],
        ];

        if ($this->_fileDescriptor === null) {
            if (!is_file($this->file->tempName)) {
                throw new \Exception('Uploaded files could not be empty');
            }
            if (!$this->findCompanyInn && empty($this->company->inn)) {
                $link = Html::a('"Профиль компании"', ['/company/update']);
                throw new \Exception(strtr(self::NO_COMPANY_INN, ['"Профиль компании"' => $link]), 100);
            }

            $this->_fileDescriptor = fopen($this->file->tempName, 'r');
            if (!$this->_fileDescriptor) {
                throw new \Exception('Failed to open file');
            }
        }

        if ($this->file && $this->file->extension == 'xlsx') {
            $this->parseXls();
        } else {
            $this->insideVidimus = false;
            $this->bankMode = false;
            $this->counter = 0;
            $this->lastPayer = [];
            $this->currentLineNumber = 0;
            $this->documentLineNumber = 0;
            while (($line = fgets($this->_fileDescriptor)) !== false) {
                $this->currentLineNumber++;
                $line = trim(mb_convert_encoding($line, 'UTF-8', ['CP1251', 'UTF-8']));
                // Обработка строки
                $this->lineProcess($line);
            }
        }

        fclose($this->_fileDescriptor);

        $this->findDublicates();
        $this->processStatementAccontsData();

        return $this->vidimuses;
    }

    /**
     * @param array $vidimuses
     * @return array
     */
    public function handlingVidimus($vidimuses)
    {
        $this->vidimuses = (array) $vidimuses;
        $this->findDublicates();

        return $this->vidimuses;
    }

    /**
     *
     * Find flow dublicates
     */
    private function findDublicates()
    {
        foreach ((array) $this->vidimuses as $key => $vidimuse) {
            $vidimuse->findDublicates();
        }
    }

    /**
     *
     * Find flow dublicates
     */
    private function processStatementAccontsData()
    {
        if (empty($this->statementAccontsData)) {
            $this->uploadData = [
                'dateStart' => '',
                'dateEnd' => '',
                'balanceStart' => 0,
                'balanceEnd' => 0,
                'totalIncome' => 0,
                'totalExpense' => 0,
                'accounts' => [],
            ];

            return;
        }

        $dateStart = null;
        $dateEnd = null;
        $balanceStart = 0;
        $balanceEnd = 0;
        $totalIncome = 0;
        $totalExpense = 0;

        $accounts = [];

        foreach ($this->statementAccontsData as $rs => $data) {
            if (empty($this->companyRs)) {
                $this->companyRs = $rs;
            }
            $dStart = date_create_immutable($data[VidimusBuilder::TAG_DATE_START]);
            $dEnd = date_create_immutable($data[VidimusBuilder::TAG_DATE_END]);

            $dateStart = $dateStart ? min($dateStart, $dStart) : $dStart;
            $dateEnd = $dateEnd ? max($dateEnd, $dEnd) : $dEnd;
            $balanceStart += is_numeric($data[VidimusBuilder::TAG_BALANCE_START]) ? $data[VidimusBuilder::TAG_BALANCE_START] : 0;
            $balanceEnd += is_numeric($data[VidimusBuilder::TAG_BALANCE_END]) ? $data[VidimusBuilder::TAG_BALANCE_END] : 0;
            $totalIncome += is_numeric($data[VidimusBuilder::TAG_TOTAL_INCOME]) ? $data[VidimusBuilder::TAG_TOTAL_INCOME] : 0;
            $totalExpense += is_numeric($data[VidimusBuilder::TAG_TOTAL_EXPENSE]) ? $data[VidimusBuilder::TAG_TOTAL_EXPENSE] : 0;

            if (isset($accounts[$rs])) {
                $accounts[$rs]['balanceStart'] += is_numeric($data[VidimusBuilder::TAG_BALANCE_START]) ? $data[VidimusBuilder::TAG_BALANCE_START] : 0;
                $accounts[$rs]['balanceEnd'] += is_numeric($data[VidimusBuilder::TAG_BALANCE_END]) ? $data[VidimusBuilder::TAG_BALANCE_END] : 0;
                $accounts[$rs]['totalIncome'] += is_numeric($data[VidimusBuilder::TAG_TOTAL_INCOME]) ? $data[VidimusBuilder::TAG_TOTAL_INCOME] : 0;
                $accounts[$rs]['totalExpense'] += is_numeric($data[VidimusBuilder::TAG_TOTAL_EXPENSE]) ? $data[VidimusBuilder::TAG_TOTAL_EXPENSE] : 0;
            } else {
                $accounts[$rs]['balanceStart'] = is_numeric($data[VidimusBuilder::TAG_BALANCE_START]) ? $data[VidimusBuilder::TAG_BALANCE_START] : 0;
                $accounts[$rs]['balanceEnd'] = is_numeric($data[VidimusBuilder::TAG_BALANCE_END]) ? $data[VidimusBuilder::TAG_BALANCE_END] : 0;
                $accounts[$rs]['totalIncome'] = is_numeric($data[VidimusBuilder::TAG_TOTAL_INCOME]) ? $data[VidimusBuilder::TAG_TOTAL_INCOME] : 0;
                $accounts[$rs]['totalExpense'] = is_numeric($data[VidimusBuilder::TAG_TOTAL_EXPENSE]) ? $data[VidimusBuilder::TAG_TOTAL_EXPENSE] : 0;
            }
        }

        $this->uploadData = [
            'dateStart' => $dateStart->format('d.m.Y'),
            'dateEnd' => $dateEnd->format('d.m.Y'),
            'balanceStart' => $balanceStart,
            'balanceEnd' => $balanceEnd,
            'totalIncome' => $totalIncome,
            'totalExpense' => $totalExpense,
            'accounts' => $accounts,
        ];

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function contractorQuery($companyId, $contractorINN, $contractorName, $contractorKPP = null, $contractorType = null, $rs = null)
    {
        $query = Contractor::find()->andWhere([
            'and',
            ['contractor.is_deleted' => 0],
            [
                'or',
                ['contractor.company_id' => $companyId],
                ['contractor.company_id' => null],
            ],
        ]);
        if (intval($contractorINN) > 0) {
            $query->andWhere(['contractor.ITN' => $contractorINN]);
        } else {
            list($type_id, $name) = VidimusUploader::getTypeName($contractorName);
            if ($type_id) {
                $name = mb_substr($name, 0, 255);
                $query->andWhere([
                    'or',
                    ['contractor.name' => $name],
                    ['contractor.name' => '"'.$name.'"'],
                ]);
            } else {
                $query->andWhere(['contractor.name' => mb_substr($contractorName, 0, 255)]);
            }
        }
        if (!empty($rs)) {
            $query->leftJoin(ContractorAccount::tableName(), '{{contractor_account}}.[[contractor_id]]={{contractor}}.[[id]]');
            $query->andFilterWhere([
                'or',
                ['contractor.PPC' => intval($contractorKPP) > 0 ? $contractorKPP : null],
                ['contractor_account.rs' => $rs],
            ]);
        } else {
            $query->andFilterWhere([
                'contractor.PPC' => intval($contractorKPP) > 0 ? $contractorKPP : null,
            ]);
        }

        return $query;
    }

    /**
     * @return Contractor|null
     */
    public static function getVidimuseContractor($inn, $company_id, $name, $kpp = null, $contractorType = null, $rs = null)
    {
        return self::contractorQuery($company_id, $inn, $name, $kpp, $contractorType, $rs)->one();
    }

    /**
     * Обработка строк/блока операции из файла
     *
     * @param $line string Обрабатываемая строка
     */
    private function lineProcess($line)
    {
        // Вход в секцию платежной операции
        if (!$this->insideVidimus && strpos($line, self::DOCUMENT_START_TAG) !== false) {
            if (!$this->companyRs) {
                throw new \Exception('В загружаемом файле не указан номер расчетного счета, для которого загружается выписка.', 100);
            }
            $this->documentLineNumber = $this->currentLineNumber;
            $this->insideVidimus = true;
            $this->operation_data = self::$emptyData;
            return;
        }
        // Выход из секции платежной операции
        if ($line == self::DOCUMENT_END_TAG && $this->insideVidimus) {
            // Запись корректных данных, валидация
            $this->createFlow();
            $this->insideVidimus = false;
            $this->counter++;

            return;
        }
        // Обработка секции платежной операции
        if ($this->insideVidimus) {
            $explodedLine = explode(self::EXPLODE_SEPARATOR, $line, 2);

            if (isset($explodedLine[0], $explodedLine[1])) {
                $this->collectData(trim($explodedLine[0]), trim($explodedLine[1]));
            }

            return;
        }

        // Вход в секцию расчетного счета
        if (self::TAG_ACCOUNT_START === $line) {
            $this->insideAccount = true;

            return;
        }
        // Выход из секции расчетного счета
        if (self::TAG_ACCOUNT_END === $line && $this->insideAccount) {
            $this->insideAccount = false;
            $rs = $this->account[self::TAG_RS];
            //$this->companyRs = $this->account[self::TAG_RS];

            // multi-rs
            if (array_search($rs, $this->companyRsArray) === false) {
                $this->companyRsArray[] = $rs;
            }

            $this->statementAccontsData[$rs] = $this->account;

            return;
        }
        // Обработка секции расчетного счета
        if ($this->insideAccount) {
            $explodedLine = explode(self::EXPLODE_SEPARATOR, $line, 2);

            if (isset($explodedLine[0], $this->account[$explodedLine[0]], $explodedLine[1])) {
                $this->account[$explodedLine[0]] = $explodedLine[1];
            }

            return;
        }

        $explodedLine = explode(self::EXPLODE_SEPARATOR, $line, 2);
        if (isset($explodedLine[0], $explodedLine[1])) {
            if ($explodedLine[0] == self::TAG_RS && empty($this->companyRs) && !empty($explodedLine[1])) {
                $this->companyRs = $explodedLine[1];
                if (array_search($this->companyRs, $this->companyRsArray) === false)
                    $this->companyRsArray[] = $this->companyRs;
            }
        }
    }

    /**
     *
     * Сбор данных по-строчно
     *
     * @param $parameter string Ключ (до равно)
     * @param $value string Значение (после равно)
     * @throws \Exception
     */
    private function collectData($parameter, $value)
    {
        $key = strtr($parameter, self::$replacemetData);

        if (empty($this->operation_data[$key])) {
            $this->operation_data[$key] = $value;
        }
    }

    /**
     * Сохранение данных, валидация
     *
     */
    private function createFlow()
    {
        $company = $this->company;
        $flow = new Vidimus($company);
        $data = $this->operation_data;

        $flow->company_rs = null;
        $flow->company_bik = null;
        if (in_array($data['ПлательщикСчет'], $this->companyRsArray)) {
            if (!isset($this->uploadData['accounts'][$data['ПлательщикСчет']]['bik'])) {
                $this->uploadData['accounts'][$data['ПлательщикСчет']]['bik'] = $data['ПлательщикБИК'];
            }
            if ($this->findCompanyInn) {
                $company->inn = $data['ПлательщикИНН'];
                $this->findCompanyInn = false;
            }
            $flow->company_rs = $data['ПлательщикСчет'];
            $flow->company_bik = $data['ПлательщикБИК'];
            $flow->flowType = Vidimus::FLOW_OUT;
        }
        if (in_array($data['ПолучательСчет'], $this->companyRsArray)) {
            if (!isset($this->uploadData['accounts'][$data['ПолучательСчет']]['bik'])) {
                $this->uploadData['accounts'][$data['ПолучательСчет']]['bik'] = $data['ПолучательБИК'];
            }
            if ($this->findCompanyInn) {
                $company->inn = $data['ПолучательИНН'];
                $this->findCompanyInn = false;
            }
            if ($flow->company_rs === null) {
                $flow->company_rs = $data['ПолучательСчет'];
                $flow->company_bik = $data['ПолучательБИК'];
                $flow->flowType = Vidimus::FLOW_IN;
            }
        }

        if ($data['ПлательщикИНН'] != $company->inn && $data['ПолучательИНН'] != $company->inn) {
            $flow->company_inn = null;
        } else {
            $flow->company_inn = $company->inn;
        }

        if ($this->findCompanyIfns &&
            !empty($data['ПоказательКБК']) &&
            !empty($data['ПолучательКПП']) &&
            !empty($data['ОКАТО'])
        ) {
            $company->ifns_ga = substr($data['ПолучательКПП'], 0, 4);
            $company->oktmo = $data['ОКАТО'];
            $this->findCompanyIfns = false;
        }

        $flow->number = $data['Номер'];
        $flow->total = $data['Сумма'];
        $flow->date = $data['ДатаСписано'] ? : ($data['ДатаПоступило'] ? : $data['Дата']);
        $flow->target = $data['НазначениеПлатежа'] ?: implode('. ', array_filter([
            rtrim($data['НазначениеПлатежа1'], '.') ?: null,
            $data['НазначениеПлатежа2'] ?: null,
        ]));

        $customer = $this->customerParams($flow->flowType);
        if ($customer) {
            $flow->contragent = $data[$customer['contragent']];
            $flow->inn = $data[$customer['inn']];
            $flow->kpp = $data[$customer['kpp']];
            $flow->currentAccount = $data[$customer['currentAccount']];
            $flow->bik = $data[$customer['bik']];
            $flow->bankName = $data[$customer['bankName']];
            $flow->correspAccount = $data[$customer['correspAccount']];
        }

        $flow->taxpayers_status = $data['СтатусСоставителя'];
        $flow->ranking_of_payment = $data['Очередность'];
        $flow->kbk = $data['ПоказательКБК'];
        $flow->oktmo_code = $data['ОКАТО'];
        $flow->payment_details = $data['ПоказательОснования'];
        $flow->tax_period_code = $data['ПоказательПериода'];
        $flow->document_number_budget_payment = $data['ПоказательНомера'];
        $flow->document_date_budget_payment = $data['ПоказательДаты'];
        $flow->payment_type = $data['ПоказательТипа'];

        $this->vidimuses[$this->counter] = $flow;

        return true;
    }

    public function companyHasAccount($bik, $account)
    {
        return $this->company->getCheckingAccountants()->andWhere([
            'bik' => $bik,
            'rs' => $account,
        ])->exists();
    }

    public function getNameByBIK($bik)
    {
        return ($bank = BikDictionary::findOne(['bik' => $bik, 'is_active' => true])) ? $bank->name : '';
    }

    public function getAccountNumber()
    {
        return $this->companyRs;
    }

    public function getAccountCurrencyText() {
        return $this->accountCurrency;
    }

    private function parseXls() {
        $xlsHelper = new XlsHelper($this->file);
        $parsedData = $xlsHelper->readBankOperations();
        if (empty($parsedData) || empty($parsedData['items'])) {
            return null;
        }

        $this->companyRs = $parsedData['companyRs'];
        $this->accountCurrency = $parsedData['currency'];
        $totalExpense = 0;
        $totalIncome = 0;
        $startDate = null;
        $endDate = null;
        foreach ($parsedData['items'] as $key => $data) {
            $paymentDate = $this->parseDate($data['paymentDate']);
            $recognitionDate = $this->parseDate($data['recognitionDate']);
            $company = $this->company;
            $flow = new Vidimus($company);
            $flow->company_rs = $parsedData['companyRs'];
            $flow->company_inn = $this->company->inn;
            $flow->company_bik = $parsedData['companyBik'];
            if (empty($data['income'])) {
                $flow->flowType = Vidimus::FLOW_OUT;
                $flow->expenditureItemName = $data['itemName'];
                $amount = $this->formatXlsAmount($data['expense']);
                $totalExpense -= $amount;
            } else {
                $flow->flowType = Vidimus::FLOW_IN;
                $flow->incomeItemName = $data['itemName'];
                $amount = $this->formatXlsAmount($data['income']);
                $totalIncome += $amount;
            }

            $date = Carbon::parse($data['paymentDate']);
            if ($startDate === null) {
                $startDate = $date;
            } elseif ($date->lessThan($startDate)) {
                $startDate = $date;
            }

            if ($endDate === null) {
                $endDate = $date;
            } elseif ($date->greaterThan($endDate)) {
                $endDate = $date;
            }

            $flow->number = $data['paymentOrderNumber'];
            $flow->total = $amount;
            $flow->date = $paymentDate;
            $flow->recognitionDate = $recognitionDate;
            $flow->target = $data['purpose'];
            $flow->contragent = $data['contractorName'];
            $flow->inn = $data['contractorInn'];
            $flow->currentAccount = $data['contractorRs'];
            $flow->bik = $data['bankBik'];
            $flow->bankName = $data['bankName'];
            $flow->projectName = $data['project'];
            $flow->salePoint = $data['salePoint'];
            $flow->isPhysical = (int)(mb_strtolower($data['isContractorPhysical'] ?? '') === 'да');
            $this->vidimuses[$key] = $flow;
        }

        $this->account = [
            self::TAG_RS => $this->companyRs,
            self::TAG_DATE_START => $startDate->format('d.m.Y'),
            self::TAG_DATE_END => $endDate->format('d.m.Y'),
            self::TAG_BALANCE_START => $this->formatXlsAmount($parsedData['balanceStart']),
            self::TAG_BALANCE_END => $this->formatXlsAmount($parsedData['balanceEnd']),
            self::TAG_TOTAL_INCOME => $totalIncome,
            self::TAG_TOTAL_EXPENSE => $totalExpense,
        ];
    }

    private function formatXlsAmount($amount): float {
        if (strpos($amount, '.')) {
            return (float)str_replace(',', '', $amount);
        }

        return (float)str_replace(',', '.', $amount);
    }

    private function parseDate(string $date) {
        try {
            return Carbon::parse($date)->format('d.m.Y');
        } catch (Exception $e) {
            return null;
        }
    }

}
