<?php

namespace frontend\modules\cash\modules\banking\components\vidimus;

use common\components\TaxRobotHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\TaxKbk;
use common\models\cash\CashBankFlows;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\reference\FixedPaymentsIp;
use Webmozart\Assert\Assert;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class Vidimus
 * @package frontend\modules\cash\modules\banking\components\vidimus
 */
class Vidimus extends BaseObject
{
    const FLOW_IN = 'in';
    const FLOW_OUT = 'out';
    /**
     * Contractor actions IDs
     */
    const CONTRACTOR_UPDATE = 1;
    const CONTRACTOR_CREATE = 2;
    const CONTRACTOR_KEEP = 3;

    /**
     * @var flow attributes
     */
    public $number;
    public $total;
    public $date;
    public $recognitionDate;
    public $target;
    public $flowType;
    public $contragent;
    public $inn;
    public $kpp;
    public $currentAccount;
    public $bik;
    public $bankName;
    public $correspAccount;
    public $taxpayers_status;
    public $ranking_of_payment;
    public $kbk;
    public $oktmo_code;
    public $payment_details;
    public $tax_period_code;
    public $document_number_budget_payment;
    public $document_date_budget_payment;
    public $payment_type;
    public $uin_code;
    public $expenditureItemName;
    public $incomeItemName;
    public $projectName;
    public $salePoint;
    public $isPhysical;

    /**
     * @var
     */
    public $company_inn = true;

    /**
     * @var
     */
    public $company_rs = true;

    /**
     * @var
     */
    public $company_bik;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $new;

    /**
     * @var
     */
    public $innExist = false;

    /**
     * @var
     */
    public $dublicate;

    /**
     * @var
     */
    public $doInsert = true;

    /**
     * @var
     */
    public $error;

    /**
     * @var
     */
    public $disabled = false;

    /**
     * @var
     */
    public $specify = false;

    /**
     * @var
     */
    public $failedAttributes = [];

    /**
     * @var
     */
    private $_company;

    /**
     * @var
     */
    private $_contractor = false;

    /**
     * @var
     */
    private $_findData;

    /**
     * @var
     */
    private $_isSelfContractor;

    public static $attributeErrorList = [
        'company_inn' => 'ИНН не совпадает с ИНН вашей компании',
        'company_rs' => 'Р/с не совпадает с р/с вашей компании',
        'total' => 'Сумма не указана',
        'date' => 'Дата не указана',
        'target' => 'Назначение не указано',
        'flowType' => 'Не удалось определить тип платежа',
        'contragent' => 'Наименование контрагента не указано',
        'bik' => 'БИК банка контрагента не указан',
        'bankName' => 'Банк контрагента не указан',
        // can upload errors
        'currentAccount' => 'Р/с контрагента не указан',
        'number' => 'Номер не указан',
        'inn' => 'ИНН контрагента не указан',
    ];

    public static $disableIfEmpty = [
        'company_inn',
        'company_rs',
        'total',
        'date',
        'target',
        'flowType',
        'contragent',
        //'bik',
        //'bankName',
    ];

    public static $contractorActions = [
        self::CONTRACTOR_UPDATE => 'Обновить КПП контрагента',
        self::CONTRACTOR_CREATE => 'Создать нового контрагента',
        self::CONTRACTOR_KEEP => 'Загрузить для имеющегося контрагента',
    ];

    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    public function getCompany() : ?Company
    {
        return $this->_company;
    }

    public function getContractor() : ?Contractor
    {
        if ($this->_contractor === false) {
            $this->_contractor = $this->getIsSelfContractor() ? null : ContractorHelper::findQuery($this->getFindData())->one();
        }

        return $this->_contractor;
    }

    public function getFindData() : array
    {
        if (!isset($this->_findData)) {
            $this->_findData = ContractorHelper::getFindDataFromVidimus($this);
        }

        return $this->_findData;
    }

    /**
     * @inheritdoc
     */
    public function checkRequiredParam()
    {
        $this->failedAttributes = [];

        foreach (self::$attributeErrorList as $key => $value) {
            if ($key == 'currentAccount' && $this->contragent == $this->bankName) {
                continue;
            }
            if (empty($this->$key)) {
                $this->error = $value;
                $this->failedAttributes[] = $key;
                $this->disabled = in_array($key, self::$disableIfEmpty);
                break;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function incomeItemId()
    {
        if ($this->flowType == self::FLOW_IN) {
            if (!empty($this->incomeItemName)) {
                $incomeItem = InvoiceIncomeItem::find()
                    ->andWhere(['LIKE', 'name', $this->incomeItemName])
                    ->andWhere([
                        'OR',
                        ['company_id' => null],
                        ['company_id' => $this->_company->id],
                    ])->one();
                if ($incomeItem === null) {
                    $incomeItem = new InvoiceIncomeItem();
                    $incomeItem->name = $this->incomeItemName;
                    $incomeItem->company_id = $this->_company->id;
                    $incomeItem->can_be_controlled = true;
                    Assert::true($incomeItem->save(), 'Cannot save invoice income item record.');
                }

                return $incomeItem->id;
            } else {
                if ($this->getIsSelfContractor()) {
                    return InvoiceIncomeItem::ITEM_OWN_FOUNDS;
                } else {
                    return InvoiceIncomeItem::itemIdByReturn($this->target) ?: (
                        InvoiceIncomeItem::itemIdByAccount($this->currentAccount) ?: (
                            InvoiceIncomeItem::itemIdByPurpose($this->target) ?: $this->incomeItemIdByContractor()
                        )
                    );
                }
            }
        }

        return null;
    }

    /**
     * @return  integer|null
     */
    public function expenseItemId()
    {
        if ($this->flowType == self::FLOW_OUT) {
            if (!empty($this->expenditureItemName)) {
                $expenseItem = InvoiceExpenditureItem::find()
                    ->andWhere(['LIKE', 'name', $this->expenditureItemName])
                    ->andWhere([
                        'OR',
                        ['company_id' => null],
                        ['company_id' => $this->_company->id],
                    ])->one();
                if ($expenseItem === null) {
                    $expenseItem = new InvoiceExpenditureItem();
                    $expenseItem->name = $this->expenditureItemName;
                    $expenseItem->company_id = $this->_company->id;
                    $expenseItem->can_be_controlled = true;
                    $expenseItem->save();
                    Assert::true($expenseItem->save(), 'Cannot save invoice expense item record.');
                }

                return $expenseItem->id;
            } elseif ($this->kbk) {
                if ($this->kbk == TaxRobotHelper::$kbkPfr) {
                    return $this->getPfrItemId();
                } else {
                    return TaxKbk::itemId($this->kbk);
                }
            } elseif ($this->getIsSelfContractor()) {
                return InvoiceExpenditureItem::ITEM_OWN_FOUNDS;
            } elseif ($this->ranking_of_payment == 3 && $this->contragent == $this->bankName) {
                return InvoiceExpenditureItem::ITEM_SALARY;
            } else {
                return InvoiceIncomeItem::itemIdByReturn(
                    $this->target,
                    InvoiceExpenditureItem::itemIdByAccount(
                        $this->currentAccount,
                        InvoiceExpenditureItem::itemIdByPurpose(
                            $this->target,
                            $this->expenseItemIdByContractor()
                        )
                    )
                );
            }
        }

        return null;
    }

    /**
     * @param $company Company
     * @return  integer|null
     */
    public function getPfrItemId()
    {
        $taxYear = mb_substr($this->tax_period_code, -4);
        $payYear = mb_substr($this->date, -4);
        if ($taxYear == $payYear) {
            $amount = intval($this->total * 100);
            $pfr = ArrayHelper::getValue(FixedPaymentsIp::findOne($taxYear), 'pfr');
            if ($pfr && $amount <= $pfr) {
                $sumArray = [$pfr, $pfr/2, $pfr/4, $pfr/12];
                foreach ($sumArray as $sum) {
                    if ($amount >= floor($sum) && $amount <= ceil($sum)) {
                        return InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT;
                    }
                }
                foreach ($sumArray as $sum) {
                    if (abs($sum - $amount) <= $sum * 0.05) {
                        $this->specify = true;

                        return InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT;
                    }
                }
            }
        }

        return InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP;
    }

    /**
     * @return  integer|null
     */
    public function incomeItemIdByContractor()
    {
        $contractor = $this->getContractor();
        if ($contractor && $contractor->invoice_income_item_id) {
            return $contractor->invoice_income_item_id;
        }

        return $this->_company->default_contractor_income_item_id ?: InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
    }

    /**
     * @return  integer|null
     */
    public function expenseItemIdByContractor()
    {
        $contractor = $this->getContractor();
        if ($contractor && $contractor->invoice_expenditure_item_id) {
            return $contractor->invoice_expenditure_item_id;
        }

        return null;
    }

    /**
     *
     */
    public function getIsSelfContractor()
    {
        if (!isset($this->_isSelfContractor)) {
            $this->_isSelfContractor = $this->inn && $this->_company->inn == $this->inn;
            if ($this->_isSelfContractor &&
                $this->_company->getIsLikeIP() &&
                ContractorHelper::getIsPhysicalVidimus($this) &&
                !$this->_company->self_employed
            ) {
                $this->_isSelfContractor = false;
            }
        }

        return $this->_isSelfContractor;
    }

    /**
     *
     */
    public function findDublicates()
    {
        $this->findContractorDouble();
        $this->findCashBankFlowDouble();
    }

    /**
     *
     */
    public function findContractorDouble()
    {
        if ($this->getIsSelfContractor() || $this->getContractor()) {
            $this->new = false;
        } else {
            $this->new = true;
            if (intval($this->kpp) > 0) {
                $this->innExist = ContractorHelper::findQuery(array_merge($this->getFindData(), ['kpp' => null]))->exists();
            } else {
                $this->innExist = false;
            }
        }
    }

    /**
     *
     */
    public function findCashBankFlowDouble()
    {
        $this->dublicate = CashBankFlows::find()->andWhere([
            'flow_type' => $this->flowType == Vidimus::FLOW_IN ?
                            CashBankFlows::FLOW_TYPE_INCOME :
                            CashBankFlows::FLOW_TYPE_EXPENSE,
            'company_id' => $this->_company->id,
            'payment_order_number' => $this->number,
            'date' => date('Y-m-d', strtotime($this->date)),
            'amount' => $this->total * 100,
            'rs' => $this->company_rs,
        ])->exists();
    }
}
