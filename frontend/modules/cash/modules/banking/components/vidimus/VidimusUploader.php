<?php
namespace frontend\modules\cash\modules\banking\components\vidimus;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\CashFlowsBase;
use common\models\cash\form\CashBankFlowsForm;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\employee\Employee;
use common\models\project\Project;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class VidimusUploader
 * @package frontend\modules\cash\modules\banking\components\vidimus
 *
 * @property integer $uploadedCount
 * @property integer $errorsCount
 */
class VidimusUploader extends BaseObject
{
    public $from;
    public $till;
    public $uploadedBalanceData;

    /**
     * @var integer
     */
    protected $_errors_count = 0;
    protected $_uploaded_count = 0;

    protected $_company;
    protected $_uploadAccounts;
    protected $_allAccounts;

    /**
     * @var array
     */
    protected $_data = [];

    protected static $_companyTypeArray;
    protected static $prefixArray = [
        '№ ?',
        'no? ?',
        'номеру? ?',
        'счету? ?',
        'сч\.? ?',
    ];

    /**
     * VidimusUploader constructor.
     * @param $company Company
     */
    public function __construct(Company $company, array $uploadAccounts)
    {
        $this->_company = $company;
        $this->_uploadAccounts = [];
        foreach ($uploadAccounts as $account) {
            $this->_uploadAccounts[$account->rs] = $account;
        }
        $this->_allAccounts = $company->getCheckingAccountants()->indexBy('rs')->all();
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @param $vidimu
     * @return CheckingAccountant
     */
    public function getAccount($vidimu = null)
    {
        $account = null;
        if (!empty($vidimu['account_rs'])) {
            $account = ArrayHelper::getValue(
                $this->_uploadAccounts,
                $vidimu['account_rs'],
                ArrayHelper::getValue($this->_allAccounts, $vidimu['account_rs'])
            );
        }

        return $account;
    }

    /**
     * create Upload data
     */
    public static function formatUploadData(Company $company, CheckingAccountant $account, array $statistic) : array
    {
        $emptyData = [
            'dateStart' => '',
            'dateEnd' => '',
            'balanceStart' => 0,
            'balanceEnd' => 0,
            'totalIncome' => 0,
            'totalExpense' => 0,
        ];

        $data = array_merge($emptyData, $statistic);
        $data['isValidCompany'] = 1;
        $data['companyData'] = [
            'inn' => $company->inn,
            'ifns_ga' => $company->ifns_ga,
            'oktmo' => $company->oktmo,
        ];
        $data['accounts'] = [
            $account->rs => array_merge($data, ['bik' => $account->bik]),
        ];

        return $data;
    }

    /**
     * @return string
     */
    public static function invoiceSearchPattern($unpaidInvoice)
    {
        $numberPattern = quotemeta($unpaidInvoice->getFullNumber());
        $datePattern1 = quotemeta(Yii::$app->formatter->asDate($unpaidInvoice->document_date, 'php:d.m.Y'));
        $datePattern2 = quotemeta(Yii::$app->formatter->asDate($unpaidInvoice->document_date, 'long'));

        return '~(^|\W|N|№)' . $numberPattern . '\W.*(' . $datePattern1 . '|' . $datePattern2 . ')~ui';
    }

    /**
     * @return string
     */
    public static function invoiceNumberPattern($unpaidInvoice)
    {
        $numberPattern = quotemeta($unpaidInvoice->getFullNumber());

        return '~(^| |,|N|№)' . $numberPattern . '( |,|$)~ui';
    }

    public function upload($vidimus, $source, $uploadData)
    {
        $errors_count = [
            'isset_upload' => 0,
            'isset_contragent' => 0,
            'isset_bank' => 0,
            'model' => 0,
            'contractor' => 0,
            'contragent_type' => 0,
            'not_upload' => 0,
            'payment_exist' => 0,
            'all' => 0
        ];

        $dateFrom = isset($uploadData['dateStart']) ? date_create($uploadData['dateStart']) : null;
        $dateTill = isset($uploadData['dateEnd']) ? date_create($uploadData['dateEnd']) : null;
        $bankBalanceEnd = is_numeric($uploadData['balanceEnd']) ? bcmul($uploadData['balanceEnd'], 100, 0) : null;

        $statementUploadArray = [];
        foreach ((array) ArrayHelper::getValue($uploadData, 'accounts') as $rs => $data) {
            if ($account = $this->getAccount(['account_rs' => $rs])) {
                $statementUpload = new CashBankStatementUpload;
                $statementUpload->company_id = $this->company->id;
                $statementUpload->account_id = $account->id;
                $statementUpload->bik = $account->bik;
                $statementUpload->rs = $account->rs;
                $statementUpload->period_from = $dateFrom ? $dateFrom->format('Y-m-d') : null;
                $statementUpload->period_till = $dateTill ? $dateTill->format('Y-m-d') : null;
                $statementUpload->source = (int) $source;
                if (LogHelper::save($statementUpload, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    $statementUploadArray[$rs] = $statementUpload;
                    if (!is_a(Yii::$app, 'yii\console\Application')) {
                        Yii::$app->session->addFlash('success', "Выписка по р/с {$account->rs} загружена");
                    }
                } else {
                    \common\components\helpers\ModelHelper::logErrors($statementUpload, __METHOD__);
                }
            } else {
                Yii::error("Account not found rs: '{$rs}'");
            }
        }
        if ($statementUploadArray && !is_a(Yii::$app, 'yii\console\Application')) {
            \common\models\company\CompanyFirstEvent::checkEvent($this->company, 97); // Выписка загружена
        }

        // prevent update balances if multi-rs upload
        if (count(array_unique(array_column($vidimus, 'account_rs'))) > 1) {
            $statistic = [];
        }


        foreach ($vidimus as $vidimu) {

            $accountVidimu = $this->getAccount($vidimu);

            if (empty($accountVidimu)) {
                Yii::$app->session->addFlash('error', "Не найден р/с {$vidimu['account_rs']}");
                break;
            }

            if (empty($vidimu['upload'])) {
                continue;
            }

            if ($vidimu['upload']) {
                $contractorName = $vidimu['contragent']['name'] ?? '';
                $contractorBik = $vidimu['contragent']['bik'] ?? '';
                $contractorRs = $vidimu['contragent']['currentAccount'] ?? '';
                $model = null;
                $anotherModel = null;
                $isInterbankAccount = mb_strpos($contractorRs, '3') === 0;

                if ($isInternalTransfer = $this->getIsInternalTransfer($vidimu)) {
                    $contractor = null;
                    $unpaidInvoices = [];
                    $paidInvoices = [];
                    if (!$isInterbankAccount && ($anotherModel = $this->findInternalTransferFlow($vidimu)) !== null) {
                        if ($model = $anotherModel->internalTransferFlow) {
                            if ($model->payment_order_number == $vidimu['number'] &&
                                $model->date == date('Y-m-d', strtotime($vidimu['date'])) &&
                                $model->amount == bcmul($vidimu['total'], 100, 0) &&
                                $model->rs == $vidimu['account_rs']
                            ) {
                                continue;
                            }
                        } else {
                            $model = $this->getNewInternalTransferFlow($accountVidimu, $anotherModel);
                        }
                    }
                } else {
                    $contractor = $this->getContractor($vidimu);
                    if ($contractor && $contractor->hasErrors()) {
                        $errors_count['contractor']++;
                        continue;
                    }
                    $paySum = (int) bcmul($vidimu['total'] ?? 0, 100, 0);
                    $unpaidInvoices = $isInternalTransfer ? [] :
                        $contractor->getUnPayedInvoices(
                            $this->company,
                            ($vidimu['flow_type'] == 'out') ? true : false,
                            [
                                'document_date' => SORT_ASC,
                                'created_at' => SORT_ASC,
                            ],
                            $vidimu['date']
                        );
                    list($paidInvoices,) = self::findPaidInvoices($unpaidInvoices, $vidimu['target'] ?? '', $paySum);
                }

                if ($model) {
                    if ($model instanceof CashBankFlowsForm) {
                        $model->scenario = 'update';
                    }
                } else {
                    if ($accountVidimu->getIsForeign()) {
                        $model = new CashBankForeignCurrencyFlows([
                            'company_id' => $this->company->id,
                            'currency_id' => $accountVidimu->currency_id,
                            'swift' => $accountVidimu->swift ?? null,
                            'bank_address' => $accountVidimu->bank_address ?? null,
                            'is_internal_transfer' => $isInternalTransfer ? 1 : 0,
                            'date' => date('Y-m-d', strtotime($vidimu['date'])),
                            'recognition_date' => !empty($vidimu['recognitionDate'])
                                ? date('Y-m-d', strtotime($vidimu['recognitionDate']))
                                : null,
                            'amount_input' => $vidimu['total'],
                        ]);
                    } else {
                        $project = null;
                        if (!empty($vidimu['projectName'])) {
                            /** @var Project $project */
                            $project = Project::find()
                                ->byCompany($this->company->id)
                                ->andWhere(['LIKE', 'name', $vidimu['projectName']])
                                ->one();
                        }

                        $salePoint = null;
                        if (!empty($vidimu['salePoint'])) {
                            /** @var SalePoint $salePoint */
                            $salePoint = SalePoint::find()
                                ->andWhere(['company_id' => $this->company->id])
                                ->andWhere(['LIKE', 'name', $vidimu['salePoint']])
                                ->one();
                        }

                        $model = new CashBankFlowsForm([
                            'scenario' => 'create',
                            'company_id' => $this->company->id,
                            'is_internal_transfer' => $isInternalTransfer ? 1 : 0,
                            'date' => date('d.m.Y', strtotime($vidimu['date'])),
                            'recognition_date' => !empty($vidimu['recognitionDate'])
                                ? date('Y-m-d', strtotime($vidimu['recognitionDate']))
                                : null,
                            'amount' => $vidimu['total'],
                            'project_id' => $project ? $project->id : null,
                            'sale_point_id' => $salePoint ? $salePoint->id : null,
                        ]);
                    }

                    $model->flow_type = ($vidimu['flow_type'] == 'out') ? 0 : 1;
                    $model->payment_order_number = $vidimu['number'];
                    $model->contractor_id = $isInternalTransfer ? CashContractorType::BANK_TEXT : $contractor->id;
                    $model->bank_name = $accountVidimu->bank_name;
                    $model->rs = $accountVidimu->rs;
                    $model->description = mb_substr($vidimu['target']??'', 0, 255);
                    $model->invoices_list = ArrayHelper::getColumn($paidInvoices, 'id');

                    // TODO: fix so that it could correctly upload
                    // Ставим 1, т.к. тип - платежное поручение
                    if ($vidimu['vidimus_type'] == '') {
                        $vidimu['vidimus_type'] = 1;
                    }
                    // ENDTODO
                    $model->has_invoice = empty($paidInvoices) ? 0 : 1;

                    if ($vidimu['flow_type'] == 'in') {
                        $model->income_item_id = $vidimu['vidimus_type'];
                    } else {
                        $model->expenditure_item_id = $vidimu['vidimus_type'];
                    }

                    if (!$accountVidimu->getIsForeign()) {
                        $model->taxpayers_status_id = ($obj = TaxpayersStatus::findOne([
                            'code' => ArrayHelper::getValue($vidimu, 'taxpayers_status', ''),
                        ])) ? $obj->id : null;
                        $model->kbk = ArrayHelper::getValue($vidimu, 'kbk');
                        $model->oktmo_code = ArrayHelper::getValue($vidimu, 'oktmo_code');
                        $model->payment_details_id = ($obj = PaymentDetails::findOne([
                            'code' => ArrayHelper::getValue($vidimu, 'payment_details'),
                        ])) ? $obj->id : null;
                        $model->tax_period_code = self::normalizeTaxPeriodCode(ArrayHelper::getValue($vidimu, 'tax_period_code'));
                        $model->document_number_budget_payment = ArrayHelper::getValue($vidimu, 'document_number_budget_payment');
                        $model->document_date_budget_payment = ($d = date_create_from_format('d.m.Y', ArrayHelper::getValue($vidimu, 'document_date_budget_payment'))) ? $d->format('Y-m-d') : null;
                        $model->payment_type_id = ($obj = PaymentType::findOne([
                            'code' => ArrayHelper::getValue($vidimu, 'payment_type'),
                        ])) ? $obj->id : null;
                        $model->uin_code = ArrayHelper::getValue($vidimu, 'uin_code');
                    }
                }

                $model->source = (int) $source;
                $model->payment_order_number = $vidimu['number'];
                if (empty($model->description) && ($description = mb_substr($vidimu['target'], 0, 255))) {
                    $model->description = $description;
                }

                if ($isInternalTransfer && !$isInterbankAccount) {
                    if ($anotherModel === null) {
                        $anotherAccount = $this->getCheckingAccountant($contractorRs) ?:
                            $this->createCheckingAccountant($contractorRs, $contractorBik, $accountVidimu);
                        if ($anotherAccount) {
                            $anotherModel = $this->getNewInternalTransferFlow($anotherAccount, $model);
                        }
                    }
                    if ($anotherModel) {
                        $anotherModel->source = $model->source;
                        $model->setInternalTransferAttributes($anotherModel);
                        $anotherModel->setInternalTransferAttributes($model);
                    }
                }

                $save = function ($model) {
                    return $model->save($model->isNewRecord);
                };

                if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, $save)) {
                    $this->_uploaded_count++;
                    if ($contractor instanceof Contractor) {
                        if ($model->flow_type == CashBankFlows::FLOW_TYPE_INCOME && !$contractor->is_customer) {
                            $contractor->updateAttributes(['is_customer' => 1]);
                        }
                        if ($model->flow_type == CashBankFlows::FLOW_TYPE_EXPENSE && !$contractor->is_seller) {
                            $contractor->updateAttributes(['is_seller' => 1]);
                        }
                    }
                    if (isset($statementUploadArray[$model->rs])) {
                        $statementUploadArray[$model->rs]->saved_count++;
                    }
                    if ($anotherModel !== null) {
                        $anotherModel->internal_transfer_flow_id = $model->id;
                        if (LogHelper::save($anotherModel, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE, $save)) {
                            $model->updateAttributes(['internal_transfer_flow_id' => $anotherModel->id]);
                            if (isset($statementUploadArray[$anotherModel->rs])) {
                                $statementUploadArray[$anotherModel->rs]->saved_count++;
                            }
                        } else {
                            \common\components\helpers\ModelHelper::logErrors($anotherModel, __METHOD__);
                        }
                    }
                } else {
                    \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
                    $errors_count['model']++;
                }
            } else {
                $errors_count['not_upload']++;
            }
        }

        $account = count($this->_uploadAccounts) == 1 ? reset($this->_uploadAccounts) : null;

        if ($account && $dateFrom && !empty($statistic['balanceStart'])) {
            $getFlowsMethod = $account->getIsForeign() ? 'getCashBankForeignCurrencyFlows' : 'getCashBankFlows';
            $flowQuery = $this->company->$getFlowsMethod()
                ->andWhere(['<', 'date', $dateFrom->format('Y-m-d')])
                ->andWhere(['rs' => $account->rs])
                ->andWhere(['not', ['contractor_id' => CashContractorType::BALANCE_TEXT]]);

            if (!$flowQuery->exists()) {
                $balanceStartQuery = $this->company->$getFlowsMethod()
                    ->andWhere(['rs' => $account->rs])
                    ->andWhere(['contractor_id' => CashContractorType::BALANCE_TEXT]);
                $balanceStartFlows = $balanceStartQuery->all();
                if ($balanceStartFlows) {
                    foreach ($balanceStartFlows as $flow) {
                        $flow->delete();
                    }
                }
                if (!$balanceStartQuery->exists()) {
                    $dateFrom->modify('-1 day');
                    $startFlow = $this->getStartBalanceFlow($statistic['balanceStart'] * 100, $dateFrom, $source, $account);
                    $startFlow->save();
                }
            }
        }

        foreach ($statementUploadArray as $statementUpload) {
            if (!$statementUpload->isNewRecord && $statementUpload->saved_count > 0) {
                $statementUpload->save(false, ['saved_count']);
            }
        }

        foreach ($errors_count as $item) {
            $this->_errors_count += $item;
        }

        if ($account && $dateTill) {
            $kubBalanceEnd = CashBankFlows::find()->where([
                'company_id' => $account->company_id,
                'rs' => $account->rs,
            ])->andWhere([
                '<=',
                'recognition_date',
                $dateTill->format('Y-m-d'),
            ])->sum('IF([[flow_type]] = 1, [[amount]], -[[amount]])');

            $this->uploadedBalanceData = (YII_ENV_DEV || $this->company->id == 486) ? [
                'account' => $account->toArray(),
                'date' => $dateTill->format('d.m.Y'),
                'kubBalance' => isset($kubBalanceEnd) ? intval($kubBalanceEnd) : null,
                'bankBalance' => isset($bankBalanceEnd) ? intval($bankBalanceEnd) : null,
            ] : null;
        }

        $this->checkDepositAccounts($uploadData);
    }

    /**
     * Close deposit accounts with zero balance
     * @param  array $uploadData [description]
     */
    public function checkDepositAccounts($uploadData)
    {
        $accountsData = (array) ArrayHelper::getValue($uploadData, 'accounts');
        foreach ($accountsData as $rs => $data) {
            if (strpos($rs, '42109') === 0) {
                $account = $this->_uploadAccounts[$rs] ?? null;
                if ($account &&
                    isset($data['balanceEnd']) &&
                    (strval($data['balanceEnd']) === '0.00') &&
                    $account->type != CheckingAccountant::TYPE_CLOSED
                ) {
                    $account->updateAttributes(['type' => CheckingAccountant::TYPE_CLOSED]);
                }
            }
        }
    }

    public function getUploadedCount()
    {
        return $this->_uploaded_count;
    }

    public function getErrorsCount()
    {
        return $this->_errors_count;
    }

    public function getIsInternalTransfer($data)
    {
        $rs = $data['contragent']['currentAccount'] ?? '';
        $name = $data['contragent']['name'] ?? '';
        $inn = $data['contragent']['inn'] ?? '';
        $isPhysical = ContractorHelper::getIsPhysical($rs, $name);
        $isInternalTransfer = ($inn && $this->company->inn == $inn) || isset($this->_uploadAccounts[$rs]);

        if ($isInternalTransfer && $this->company->getIsLikeIP() && $isPhysical && !$this->company->self_employed ) {
            $isInternalTransfer = false;
        }

        return $isInternalTransfer;
    }

    public function findInternalTransferFlow($vidimu)
    {
        if ($this->getAccount($vidimu)->getIsForeign()) {
            return CashBankForeignCurrencyFlows::find()->andWhere([
                'company_id' => $this->company->id,
                'flow_type' => $vidimu['flow_type'] == 'out' ? CashBankFlows::FLOW_TYPE_EXPENSE : CashBankFlows::FLOW_TYPE_INCOME,
                'payment_order_number' => $vidimu['number'],
                'date' => date('Y-m-d', strtotime($vidimu['date'])),
                'amount' => bcmul($vidimu['total'], 100, 0),
                'rs' => $vidimu['account_rs'],
            ])->one();
        } else {
            return CashBankFlows::find()->andWhere([
                'company_id' => $this->company->id,
                'flow_type' => $vidimu['flow_type'] == 'out' ? CashBankFlows::FLOW_TYPE_EXPENSE : CashBankFlows::FLOW_TYPE_INCOME,
                'payment_order_number' => $vidimu['number'],
                'date' => date('Y-m-d', strtotime($vidimu['date'])),
                'amount' => bcmul($vidimu['total'], 100, 0),
                'rs' => $vidimu['account_rs'],
            ])->one();
        }
    }

    public function getNewInternalTransferFlow(CheckingAccountant $account, CashFlowsBase $flow) : CashFlowsBase
    {
        $model = $account->getCashFlowNewModel();
        $model->flow_type = $flow->flow_type == CashBankFlows::FLOW_TYPE_INCOME ? CashBankFlows::FLOW_TYPE_EXPENSE : CashBankFlows::FLOW_TYPE_INCOME;
        $model->payment_order_number = $flow->payment_order_number;
        $model->date = $flow->date;
        $model->amount = $flow->amount;
        $model->description = $flow->description;
        $model->expenditure_item_id = $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : null;
        $model->income_item_id = $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME ? InvoiceIncomeItem::ITEM_OWN_FOUNDS : null;

        return $model;
    }

    public function createCheckingAccountant(string $rs, string $bik, CheckingAccountant $account) : ?CheckingAccountant
    {
        if ($account->is_foreign_bank) {
            return null;
        }

        $model = new CheckingAccountant([
            'company_id' => $account->company_id,
            'currency_id' => $account->currency_id,
            'rs' => $rs,
            'bik' => $bik,
        ]);

        if ($model->save()) {
            $this->_data['companyAccount'][$rs] = $model;

            return $this->_data['companyAccount'][$rs];
        }

        return null;
    }

    /**
     * @param $name
     * @param $kpp
     * @return int
     */
    public function getCheckingAccountant(string $rs)
    {
        if (!isset($this->_data['companyAccount'][$rs])) {
            $this->_data['companyAccount'][$rs] = $this->company->getCheckingAccountants()->andWhere([
                'rs' => $rs,
            ])->orderBy([
                'type' => SORT_ASC,
            ])->one();
        }

        return $this->_data['companyAccount'][$rs];
    }

    /**
     * @param $name
     * @param $kpp
     * @return int
     */
    public static function getTypeName($name, $kpp = null)
    {
        return ContractorHelper::getOpfAndName($name);
    }

    /**
     * @param array $vidimu
     * @return Contractor
     */
    public function getContractor($vidimu)
    {
        $contractor_type = ($vidimu['flow_type'] == 'out') ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;
        $contragent = $vidimu['contragent'] ?? [];
        $bik = $contragent['bik'] ?? '';

        $findData = ContractorHelper::getFindDataFromPostData($vidimu, $this->company->id);
        $inn = $findData['inn'];
        $kpp = $findData['kpp'];
        $rs = $findData['rs'];

        $model = ContractorHelper::findQuery($findData)->one();
        $contractorAttributes = ($model === null && !$findData['isPhysical']) ? DadataClient::getContractorAttributes($inn, $kpp) : [];

        $action = ArrayHelper::getValue($contragent, 'action');
        if ($model === null && !$findData['isPhysical'] && $action) {
            if ($action == Vidimus::CONTRACTOR_UPDATE || $action == Vidimus::CONTRACTOR_KEEP) {
                $model = ContractorHelper::findQuery(array_merge($findData, ['kpp' => null]))->one();
                if ($model && $action == Vidimus::CONTRACTOR_UPDATE) {
                    $model->PPC = $kpp;
                    $model->legal_address = ArrayHelper::getValue($contractorAttributes, 'legal_address', $model->legal_address);
                    $model->save(false, ['PPC', 'legal_address']);
                }
            }
        }

        if ($model === null) {
            $model = new Contractor();
            $model->loadDefaultValues();
            $type_id = $findData['opf'];
            $contractorName = $findData['name'];
            $needDadata = $inn && ($this->company->inn != $inn || $type_id);

            if ($needDadata && $contractorAttributes) {
                $model->setAttributes($contractorAttributes);
            } else {
                $model->name = $contractorName;
                $model->PPC = $kpp;
                $model->ITN = $inn;
                $model->company_type_id = $type_id;

                $faceType = Contractor::TYPE_PHYSICAL_PERSON;
                if (($type_id || strlen($inn) === 10)) {
                    $faceType = Contractor::TYPE_LEGAL_PERSON;
                }

                if ($faceType === null && isset($contragent['isPhysical'])) {
                    $isPhysical = $contragent['isPhysical'];
                    if ($isPhysical) {
                        $faceType = Contractor::TYPE_PHYSICAL_PERSON;
                    } else {
                        $faceType = Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                    }
                }

                $model->face_type = $faceType;
                $model->director_name = ($type_id == CompanyType::TYPE_IP) ?
                                             $contractorName :
                                             Contractor::DEFAULT_DIRECTOR_NAME;
                $model->legal_address = Contractor::DEFAULT_LEGAL_ADDRESS;

                if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
                    $physical_name = explode(' ', $contractorName);
                    $model->physical_lastname = ArrayHelper::getValue($physical_name, 0, '');
                    $model->physical_firstname = ArrayHelper::getValue($physical_name, 1, '');
                    $model->physical_patronymic = ArrayHelper::getValue($physical_name, 2, '');
                    if (!$model->physical_patronymic) {
                        $model->physical_no_patronymic = true;
                    }
                }
            }

            $model->populateRelation('company', $this->company);
            $model->company_id = $this->company->id;
            $model->type = ($vidimu['flow_type'] == 'out') ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;

            $model->chief_accountant_is_director = true;
            $model->contact_is_director = true;
            $model->taxation_system = Contractor::WITHOUT_NDS;
        }
        if ($vidimu['flow_type'] == 'out' && $vidimu['vidimus_type']) {
            $model->invoice_expenditure_item_id = ((int) $vidimu['vidimus_type']) ? : null;
        }
        if ($model->company_id && ($model->isAttributeChanged('invoice_expenditure_item_id') || $model->getIsNewRecord())) {
            $model->save(false);
        }

        if (!$model->getIsNewRecord() && $bik && $rs && !$model->hasAccount($bik, $rs)) {
            $model->addAccount($bik, $rs);
        }

        return $model;
    }

    /**
     *
     */
    public function getStartBalanceFlow($amount, $date, $source, $account)
    {
        if ($account->getIsForeign()) {
            $model = new CashBankForeignCurrencyFlows([
                'company_id' => $this->company->id,
                'contractor_id' => CashContractorType::BALANCE_TEXT,
                'amount' => $amount,
                'rs' => $account->rs,
                'swift' => $account->swift ?? null,
                'bank_name' => $account->bank_name ?? null,
                'bank_address' => $account->bank_address ?? null,
                'date' => $date->format('d.m.Y'),
                'description' => "Остаток на конец {$date->format('d.m.Y')}г.",
                'bank_name' => $account->bank_name,
                'flow_type' => CashBankFlowsForm::FLOW_TYPE_INCOME,
                'income_item_id' => InvoiceIncomeItem::ITEM_STARTING_BALANCE,
                'source' => (int) $source,
            ]);
        } else {
            $model = new CashBankFlowsForm([
                'scenario' => 'create',
                'company_id' => $this->company->id,
                'contractor_id' => CashContractorType::BALANCE_TEXT,
                'amount' => $amount/100,
                'rs' => $account->rs,
                'date' => $date->format('d.m.Y'),
                'description' => "Остаток на конец {$date->format('d.m.Y')}г.",
                'bank_name' => $account->bank_name,
                'flow_type' => CashBankFlowsForm::FLOW_TYPE_INCOME,
                'income_item_id' => InvoiceIncomeItem::ITEM_STARTING_BALANCE,
                'source' => (int) $source,
            ]);
        }

        return $model;
    }

    /**
     *
     */
    public static function findPaidInvoices(array $unpaidInvoices, string $purpose, int $paySum, \DateTime $paymentDate = null)
    {
        $paidInvoices = [];
        $newInvoices = [];
        if ($paymentDate !== null) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                $invoiceDate = date_create_from_format('Y-m-d|', $unpaidInvoice->document_date);
                if ($invoiceDate > $paymentDate) {
                    $newInvoices[] = $unpaidInvoice;
                    unset($unpaidInvoices[$key]);
                }
            }
        }

        if (!empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if (preg_match(self::invoiceSearchPattern($unpaidInvoice), $purpose, $matches)) {
                    if ($paySum == $unpaidInvoice->total_amount_with_nds) {
                        $paidInvoices[] = $unpaidInvoice;
                        unset($unpaidInvoices[$key]);
                        $paySum = 0;
                        break;
                    }
                }
            }
        }
        if ($paySum > 0 && !empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if (preg_match(self::invoiceSearchPattern($unpaidInvoice), $purpose, $matches)) {
                    if ($paySum > 0) {
                        $paidInvoices[] = $unpaidInvoice;
                        $paySum = max(0, $paySum - $unpaidInvoice->total_amount_with_nds);
                        unset($unpaidInvoices[$key]);
                    } else {
                        break;
                    }
                }
            }
        }
        if ($paySum > 0 && !empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if (preg_match(self::invoiceNumberPattern($unpaidInvoice), $purpose, $matches)) {
                    if ($paySum == $unpaidInvoice->total_amount_with_nds) {
                        $paidInvoices[] = $unpaidInvoice;
                        unset($unpaidInvoices[$key]);
                        $paySum = 0;
                        break;
                    }
                }
            }
        }
        if ($paySum > 0 && !empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if (preg_match(self::invoiceNumberPattern($unpaidInvoice), $purpose, $matches)) {
                    if ($paySum > 0) {
                        $paidInvoices[] = $unpaidInvoice;
                        $paySum = max(0, $paySum - $unpaidInvoice->total_amount_with_nds);
                        unset($unpaidInvoices[$key]);
                    } else {
                        break;
                    }
                }
            }
        }
        if ($paySum > 0 && !empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if ($paySum == $unpaidInvoice->total_amount_with_nds) {
                    $paidInvoices[] = $unpaidInvoice;
                    unset($unpaidInvoices[$key]);
                    $paySum = 0;
                    break;
                }
            }
        }
        if ($paySum > 0 && !empty($unpaidInvoices)) {
            foreach ($unpaidInvoices as $key => $unpaidInvoice) {
                if ($paySum > 0) {
                    $paidInvoices[] = $unpaidInvoice;
                    $paySum = max(0, $paySum - $unpaidInvoice->total_amount_with_nds);
                    unset($unpaidInvoices[$key]);
                }
            }
        }

        return [$paidInvoices, array_merge($unpaidInvoices, $newInvoices)];
    }

    /**
     * @param $code
     * @return string
     */
    public static function normalizeTaxPeriodCode($code)
    {
        if (strlen($code) === 8 && ($date = date_create_from_format('d.m.y', $code))) {
            $code = $date->format('d.m.Y');
        }

        return $code;
    }
}
