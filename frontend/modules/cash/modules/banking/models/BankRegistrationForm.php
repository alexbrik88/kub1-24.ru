<?php

namespace frontend\modules\cash\modules\banking\models;

use common\models\bank\BankUser;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use frontend\models\RegistrationForm;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class BankRegistrationForm
 * @package frontend\modules\cash\modules\banking\models
 */
class BankRegistrationForm extends Model
{
    protected $_company;
    protected $_employee;
    protected $_accounts;
    protected $_bankUser;
    protected $_password;
    protected $_model;
    protected $_data;
    protected $_queryParams;
    protected $_isInitialized = false;
    protected $_isSaved = false;

    public $password;

    public $isNewCompany = true;
    public $isNewUser = true;
    public $isNewEmployee = true;
    public $needConfirm = false;

    /**
     * @param $type
     * @param $query
     * @param array $params
     * @throws InvalidConfigException
     */
    public function __construct(AbstractBankModel $model, array $queryParams, $config = [])
    {
        $this->_model = $model;
        $this->_queryParams = $queryParams;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->_model->isValidToken() && ($this->_data = $this->_model->getRegistrationData())) {
            if ($this->_employee === null) {
                if ($this->_employee = $this->findEmployeeByBankUser()) {
                    if ($bankUser = $this->_employee->getBankUser($this->_model::$alias)) {
                        $this->_bankUser = $bankUser;
                        $this->_company = $this->findUserCompanyByBankUser() ? : $this->findUserCompanyByData();
                    }
                } elseif ($this->_employee = $this->findEmployeeByLogin()) {
                    $this->needConfirm = true;
                    $this->_company = $this->findUserCompanyByData();
                } else {
                    $this->_employee = RegistrationForm::getNewEmployee();
                    $this->_employee->load($this->_data);
                }
            }
            if ($this->_company === null && ($this->_company = $this->findExistingCompanyByUid()) === null) {
                $this->_company = RegistrationForm::getNewCompany([
                    'scenario' => Company::SCENARIO_USER_UPDATE,
                    'chief_is_chief_accountant' => true,
                ]);
                $this->_company->load($this->_data);
                $this->_company->registration_page_type_id = $this->_model->registrationPageTypeId($this->_queryParams);
                $companyTaxationType = new CompanyTaxationType;
                $companyTaxationType->load($this->_data);
                $this->_company->populateRelation('companyTaxationType', $companyTaxationType);
                $this->_accounts = [];
                foreach ($this->_data['accounts'] as $key => $accountData) {
                    $checkingAccountant = new CheckingAccountant;
                    $checkingAccountant->load($accountData, '');
                    $this->_accounts[] = $checkingAccountant;
                }
                if (!empty($this->_accounts)) {
                    $this->_company->populateRelation('mainAccountant', reset($this->_accounts));
                    $this->_company->populateRelation('checkingAccountants', $this->_accounts);
                }
            }
            if ($this->_bankUser === null) {
                $this->_bankUser = new BankUser;
            }
            $this->_bankUser->load($this->_data);

            $this->_isInitialized = true;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'password',
                'required',
                'when' => function ($model) {
                    return $model->needConfirm;
                },
            ],
            [
                'password',
                function ($attribute) {
                    if (!$this->_employee->validatePassword($this->$attribute)) {
                        $this->addError($attribute, 'Не верный пароль. "'.$this->$attribute.'"');
                    }
                },
                'when' => function ($model) {
                    return $model->needConfirm;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * @return Employee
     */
    protected function findEmployeeByBankUser()
    {
        return Employee::findIdentityByBankUser($this->dataValue('BankUser'));
    }

    /**
     * @return Employee
     */
    protected function findEmployeeByLogin()
    {
        return Employee::findIdentityByLogin($this->dataValue('Employee', 'email'));
    }

    /**
     * @return Company
     */
    public function findExistingCompanyByUid()
    {
        return Company::find()->alias('company')
            ->rightJoin(['bank_user' => BankUser::tableName()], '{{bank_user}}.[[company_id]] = {{company}}.[[id]]')
            ->leftJoin(['employee' => Employee::tableName()], '{{employee}}.[[id]] = {{bank_user}}.[[employee_id]]')
            ->andWhere(['bank_user.company_uid' => $this->dataValue('BankUser', 'company_uid')])
            ->andWhere(['company.blocked' => false])
            ->andWhere(['employee.is_active' => true])
            ->andWhere(['employee.is_deleted' => false])
            ->one();
    }

    /**
     * @return Company
     */
    public function findUserCompanyByData()
    {
        return $this->_employee->getCompanies()->andWhere([
            'inn' => $this->dataValue('Company', 'inn'),
            'blocked' => false,
        ])->andFilterWhere([
            'kpp' => $this->dataValue('Company', 'kpp'),
        ])->one();
    }

    /**
     * @return Company
     */
    public function findUserCompanyByBankUser()
    {
        return $this->_employee->getCompanies()->andWhere([
            'id' => $this->_bankUser->company_id,
            'blocked' => false,
        ])->one();
    }

    /**
     * @return EmployeeCompany
     */
    public function findEmployeeCompany()
    {
        return EmployeeCompany::findOne([
            'employee_id' => $this->_employee->id,
            'company_id' => $this->_company->id,
        ]);
    }

    /**
     * @return mixed
     */
    public function dataValue($modelName, $attribute = null, $defaultValue = null)
    {
        if ($attribute === null) {
            return ArrayHelper::getValue($this->_data, $modelName, $defaultValue);
        } else {
            return ArrayHelper::getValue(ArrayHelper::getValue($this->_data, $modelName), $attribute, $defaultValue);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->_employee->getIsNewRecord() && !$this->_employee->validate(['email'])) {
                $this->addError('user', $this->_employee->getFirstError('email'));
            }
            if ($this->_company->getIsNewRecord()) {
                $this->_company->validate();
                $this->_company->clearErrors('phone');
            }

            return true;
        }

        return false;
    }

    /**
     * @return boolean
     * @throws \Exception
     * @throws \Throwable
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->isNewUser = $this->_employee->getIsNewRecord();
        $this->isNewCompany = $this->_company->getIsNewRecord();

        if ($this->isNewUser) {
            $this->_password = Employee::generatePassword(Employee::PASS_LENGTH);
            $this->_employee->setPassword($this->_password);
        }

        return \Yii::$app->db->transaction(function ($db) {
            $this->_isSaved = $this->isNewUser ? $this->_employee->save(false) : true;

            if ($this->_isSaved && $this->isNewCompany) {
                $this->_company->owner_employee_id = $this->_employee->id;
                if ($this->_isSaved = $this->_company->save(false)) {
                    $this->_company->companyTaxationType->company_id = $this->_company->id;
                    $this->_isSaved = $this->_company->createTrialSubscribe() &&
                               $this->_company->companyTaxationType->save() &&
                               Contractor::createFounder($this->_company);
                    foreach ($this->_company->checkingAccountants as $account) {
                        $account->company_id = $this->_company->id;
                        $this->_isSaved = $this->_isSaved && $account->save();
                    }
                    if (!$this->_company->main_id) {
                        $this->_company->updateAttributes(['main_id' => $this->_company->id]);
                    }
                }
            }

            if ($this->_isSaved) {
                if ($employeeCompany = $this->findEmployeeCompany()) {
                    if (!$employeeCompany->is_working) {
                        $employeeCompany->updateAttributes([
                            'is_working' => 1,
                            'date_dismissal' => null,
                        ]);
                    }
                    $this->isNewEmployee = false;
                } else {
                    $employeeCompany = new EmployeeCompany([
                        'company_id' => $this->_company->id,
                    ]);
                    $employeeCompany->employee = $this->_employee;
                    $employeeCompany->employee_role_id = $this->isNewCompany ? EmployeeRole::ROLE_CHIEF : (
                        $this->dataValue('BankUser', 'is_chief') ? EmployeeRole::ROLE_CHIEF : EmployeeRole::ROLE_ACCOUNTANT
                    );
                    $this->isNewEmployee = !$this->isNewCompany;
                    $this->_isSaved = $employeeCompany->save(false);
                }
            }

            if ($this->_isSaved) {
                $this->_bankUser->employee_id = $this->_employee->id;
                $this->_bankUser->company_id = $this->_company->id;
                if ($this->_bankUser->getDirtyAttributes()) {
                    $this->_isSaved = $this->_bankUser->save();
                }
                if ($this->_isSaved) {
                    $updateAttributes = [];
                    if ($this->_employee->company_id != $this->_company->id) {
                        $updateAttributes['company_id'] = $this->_company->id;
                    }
                    if ($this->isNewUser) {
                        $updateAttributes['main_company_id'] = $this->_company->id;
                    }
                    if ($updateAttributes) {
                        $this->_employee->updateAttributes($updateAttributes);
                    }

                    return $this->_isSaved;
                }
            }

            if ($db->getTransaction()->isActive) {
                $db->getTransaction()->rollBack();
            }

            if ($this->isNewUser) {
                $this->_employee->setIsNewRecord(true);
                $this->_employee->id = null;
            }
            if ($this->isNewCompany) {
                $this->_company->setIsNewRecord(true);
                $this->_company->id = null;
            }

            $this->addError('user', 'Во время регистрации пользователя произошла ошибка');

            return $this->_isSaved;
        });
    }

    /**
     * @return Employee
     */
    public function getUser()
    {
        return $this->_employee;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Company
     */
    public function getBankUser()
    {
        return $this->_bankUser;
    }

    /**
     * @return bool
     */
    public function getIsInitialized()
    {
        return $this->_isInitialized;
    }

    /**
     * @return bool
     */
    public function getIsSaved()
    {
        return $this->_isSaved;
    }

    /**
     * @inheritdoc
     */
    public function sendRegistrationEmails()
    {
        if ($this->_isSaved) {
            if ($this->isNewEmployee) {
                foreach ($this->_company->getEmployeeChief(true) as $employee) {
                    if ($employee->id != $user->id) {
                        \Yii::$app->mailer->compose([
                            'html' => 'system/add-to-company-notify/html',
                            'text' => 'system/add-to-company-notify/text',
                        ], [
                            'user' => $user,
                            'company' => $company,
                            'subject' => 'Новый сотрудник в Вашей компании',
                        ])
                            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                            ->setTo($employee->email)
                            ->setSubject('Новый сотрудник в Вашей компании')
                            ->send();
                    }
                }
            }
            if ($this->isNewUser) {
                $mailer = clone \Yii::$app->mailer;
                $mailer->htmlLayout = 'layouts/html2';
                $mailer->compose([
                    'html' => 'system/new-registration/html',
                    'text' => 'system/new-registration/text',
                ], [
                    'login' => $this->_employee->email,
                    'password' => $this->_password,
                    'subject' => 'Добро пожаловать в КУБ24',
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo($this->_employee->email)
                    ->setSubject('Добро пожаловать в КУБ24')
                    ->send();
            }
        }
    }

    /**
     * @return array
     */
    public function getAccountsData()
    {
        if ($this->_data) {
            return ArrayHelper::getValue($this->_data, 'accounts', []);
        }

        return [];
    }
}
