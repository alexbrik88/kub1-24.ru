<?php

namespace frontend\modules\cash\modules\banking\models;

use Yii;
use common\models\company\CheckingAccountant;

/**
 * This is the model class for table "banking_email_file".
 *
 * @property int $id
 * @property int $banking_email_id
 * @property string $date
 * @property string|null $period_from
 * @property string|null $period_to
 * @property int|null $statements_count
 * @property string $file_name
 * @property int|null $is_uploaded
 * @property int|null $uploaded_at
 *
 * @property BankingEmail $bankingEmail
 * @property CheckingAccountant $checkingAccountant
 */
class BankingEmailFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banking_email_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['banking_email_id', 'date', 'file_name'], 'required'],
            [['banking_email_id', 'statements_count', 'is_uploaded', 'uploaded_at'], 'integer'],
            [['date', 'period_from', 'period_to'], 'safe'],
            [['file_name'], 'string'],
            [['banking_email_id'], 'exist', 'skipOnError' => true, 'targetClass' => BankingEmail::className(), 'targetAttribute' => ['banking_email_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banking_email_id' => 'Banking Email ID',
            'date' => 'Date',
            'period_from' => 'Period From',
            'period_to' => 'Period To',
            'statements_count' => 'Statements Count',
            'file_name' => 'File Name',
            'is_uploaded' => 'Is Uploaded',
            'uploaded_at' => 'Uploaded At'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->is_uploaded)
            $this->__dropFile();
    }

    public function beforeDelete()
    {
        $this->__dropFile();

        return parent::beforeDelete();
    }

    /**
     * Gets query for [[BankingEmail]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBankingEmail()
    {
        return $this->hasOne(BankingEmail::className(), ['id' => 'banking_email_id']);
    }

    /**
     * @return string
     */
    public static function getUploadsDir()
    {
        return \Yii::getAlias('@common/uploads') . DIRECTORY_SEPARATOR . 'banking_email';
    }

    /**
     * @param $accountId
     * @param $now
     * @return string
     */
    public static function generateFileName($accountId, $now)
    {
        return "bef_{$accountId}_{$now}.txt";
    }

    private function __dropFile()
    {
        $path = $this->getUploadsDir() . DIRECTORY_SEPARATOR . $this->file_name;
        if (file_exists($path)) {
            unlink($path);
        }
    }

    ////// Summary tmp file

    public static function createSummaryTmpFile($destFile, $sourceFilesArr)
    {
        try {
            fopen($destFile, 'w');
            $dest = fopen($destFile, 'w');
            foreach ($sourceFilesArr as $sourceFile) {
                $source = fopen($sourceFile, 'r');
                stream_copy_to_stream($source, $dest);
                fclose($source);
            }
            fclose($dest);
        } catch (\Exception $e) {

            return false;
        }

        return true;
    }

    public static function deleteSummaryTmpFile($file)
    {
        if (file_exists($file)) {
            unlink($file);
        }
    }
}
