<?php

namespace frontend\modules\cash\modules\banking\models;

use common\models\company\CheckingAccountant;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "banking_email".
 *
 * @property int $id
 * @property int $company_id
 * @property int $checking_accountant_id
 * @property string $email
 * @property string $password
 * @property int $autoload_mode
 *
 * @property BankingEmailFile[] $bankingEmailFiles
 * @property Company $company
 * @property boolean $isAutoloadMode
 */
class BankingEmail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banking_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'checking_accountant_id', 'email'], 'required'],
            [['company_id', 'checking_accountant_id'], 'integer'],
            [['email'], 'email'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['checking_accountant_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckingAccountant::className(), 'targetAttribute' => ['checking_accountant_id' => 'id']],
            [['autoload_mode'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'checking_accountant_id' => 'Checking Accountant ID',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[BankingEmailFiles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBankingEmailFiles()
    {
        return $this->hasMany(BankingEmailFile::className(), ['banking_email_id' => 'id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[CheckingAccountant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['id' => 'checking_accountant_id']);
    }

    public function getIsAutoloadMode()
    {
        return (bool)$this->autoload_mode;
    }
}
