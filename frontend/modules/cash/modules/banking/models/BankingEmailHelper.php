<?php

namespace frontend\modules\cash\modules\banking\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * BankingEmailHelper
 */
class BankingEmailHelper
{

    static protected $inbox;
    static $lastErrorCode;

    /**
     * @param BankingEmail $model
     * @return bool
     */
    static public function createMailbox(BankingEmail $model)
    {
        if (!$model->email || !$model->password) {
            return false;
        }

        $vst_hostname = ArrayHelper::getValue(Yii::$app->params['vesta'], 'hostname');
        $vst_username = ArrayHelper::getValue(Yii::$app->params['vesta'], 'username');
        $vst_password = ArrayHelper::getValue(Yii::$app->params['vesta'], 'password');
        $vst_returncode = 'yes';
        $vst_command = 'v-add-mail-account';
        $vst_mail_user = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_user');
        $vst_mail_domain = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_domain');

        // New Email
        $account = current(explode('@', $model->email));
        $password = $model->password;

        // Prepare POST query
        $postvars = [
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $vst_mail_user,
            'arg2' => $vst_mail_domain,
            'arg3' => $account,
            'arg4' => $password,
        ];

        // Send POST query via cURL
        $postdata = http_build_query($postvars);

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postdata,
        ])->post('https://' . $vst_hostname . ':8083/api/');

        $postvars['username'] = $postvars['password'] = '******';

        // Check result
        if ($response == "0") {
            return true;
        } else {
            self::$lastErrorCode = $response;
        }

        return false;
    }

    /**
     * @param BankingEmail $model
     * @return bool
     */
    static public function dropMailbox(BankingEmail $model)
    {
        if (!$model->email) {
            return false;
        }

        $vst_hostname = ArrayHelper::getValue(Yii::$app->params['vesta'], 'hostname');
        $vst_username = ArrayHelper::getValue(Yii::$app->params['vesta'], 'username');
        $vst_password = ArrayHelper::getValue(Yii::$app->params['vesta'], 'password');
        $vst_returncode = 'yes';
        $vst_command = 'v-delete-mail-account';
        $vst_mail_user = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_user');
        $vst_mail_domain = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_domain');

        // New Email
        $account = current(explode('@', $model->email));
        $password = $model->password;

        // Prepare POST query
        $postvars = [
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $vst_mail_user,
            'arg2' => $vst_mail_domain,
            'arg3' => $account,
        ];

        // Send POST query via cURL
        $postdata = http_build_query($postvars);

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postdata,
        ])->post('https://' . $vst_hostname . ':8083/api/');

        $postvars['username'] = $postvars['password'] = '******';

        // Check result
        if ($response == "0") {
            return true;
        } else {
            self::$lastErrorCode = $response;
        }

        return false;
    }

    /**
     * @return array|bool
     */
    static public function getFiles() {

        if (!self::$inbox) {
            self::$lastErrorCode = 'No connection!';
            return false;
        }

        /* grab emails */
        $messages = imap_search(self::$inbox, 'ALL');

        $NEW_FILES = [];

        /* if emails are returned, cycle through each... */
        if($messages) {

            /* put the newest emails on top */
            rsort($messages);

            foreach($messages as $msg_number) {

                /* get information specific to this email */
                $header = imap_header(self::$inbox, $msg_number);
                $structure = imap_fetchstructure(self::$inbox,$msg_number);
                preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,10})/", $header->fromaddress, $matches);
                $fromEmail = ($matches) ? $matches[0] : 'unknown';

                $attachments = array();
                if(isset($structure->parts) && count($structure->parts)) {
                    for($i = 0; $i < count($structure->parts); $i++) {
                        $attachments[$i] = array(
                            'is_attachment' => false,
                            'filename' => '',
                            'name' => '',
                            'attachment' => '');

                        if($structure->parts[$i]->ifdparameters) {
                            foreach($structure->parts[$i]->dparameters as $object) {

                                if(strtolower($object->attribute) == 'filename') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['filename'] = self::decodeFilename($object->value);
                                }
                            }
                        }

                        if($structure->parts[$i]->ifparameters) {
                            foreach($structure->parts[$i]->parameters as $object) {
                                if(strtolower($object->attribute) == 'name') {
                                    $attachments[$i]['is_attachment'] = true;
                                    $attachments[$i]['name'] = $object->value;
                                }
                            }
                        }

                        if($attachments[$i]['is_attachment']) {
                            $attachments[$i]['attachment'] = imap_fetchbody(self::$inbox, $msg_number, $i+1);
                            if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                                $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                            }
                            elseif($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                                $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                            }
                        }
                    } // for($i = 0; $i < count($structure->parts); $i++)
                } // if(isset($structure->parts) && count($structure->parts))

                if(count($attachments) != 0) {
                    foreach($attachments as $at) {
                        if($at['is_attachment'] == 1) {

                            $tmpFilename = tempnam(sys_get_temp_dir(), 'bef_');
                            file_put_contents($tmpFilename, $at['attachment']);

                            $tmpFile = [
                                'from_email' => $fromEmail,
                                //'filename' => $at['filename'],
                                'filesize' => strlen($at['attachment']),
                                'type' => mime_content_type($tmpFilename),
                                'tmp_name' => $tmpFilename,
                                'msg_number' => $msg_number
                            ];

                            $NEW_FILES[] = $tmpFile;
                        }
                    }
                }
            }
        }

        return $NEW_FILES;
    }

    /**
     *
     */
    static public function expungeMessages()
    {
        imap_expunge(self::$inbox);
    }

    /**
     * @param BankingEmail $model
     * @return bool
     */
    static public function connect(BankingEmail $model)
    {
        $imap_host = ArrayHelper::getValue(Yii::$app->params['vesta'], 'imap_host');
        $imap_port = ArrayHelper::getValue(Yii::$app->params['vesta'], 'imap_port');
        $hostname = '{'.$imap_host.':'.$imap_port.'/novalidate-cert/tls}INBOX';
        $username = $model->email;
        $password = $model->password;

        /* try to connect */
        if (!(self::$inbox = @imap_open($hostname,$username,$password,0,1))) {
            Yii::warning(__METHOD__ . ' ' . imap_last_error() . ' ' . $model->email . "\n");
            self::$lastErrorCode = imap_last_error();

            return false;
        }

        return true;
    }

    /**
     *
     */
    static public function closeConnection()
    {
        imap_close(self::$inbox);
    }

    /**
     * @param $msgNumber
     */
    static public function markMessageAsDeleted($msgNumber)
    {
        imap_delete(self::$inbox, $msgNumber);
    }

    /**
     * @param $filename
     * @param string $encoding
     * @return string
     */
    static protected function decodeFilename($filename, $encoding = 'UTF-8')
    {
        $parts = imap_mime_header_decode($filename);
        $str = '';
        for ($p = 0; $p < count($parts); $p++) {
            $ch = $parts[$p]->charset;
            $part = $parts[$p]->text;
            if ($ch !== 'default') {
                $str .= mb_convert_encoding($parts[$p]->text, $encoding, $ch);
            } else {
                $str .= $part;
            }
        }

        return $str;
    }

    /**
     * @param int $length
     * @return bool|string
     */
    static public function generatePassword($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $length = rand(10, 16);
        $password = substr( str_shuffle(sha1(rand() . time()) . $chars ), 0, $length);

        return $password;
    }

    /**
     *
     */
    static public function setSuccessUploaded()
    {
        $sessionFilesIds = Yii::$app->session->get('uploadedBankingEmailFilesIds');
        foreach ($sessionFilesIds as $fileId) {
            if ($fileModel = BankingEmailFile::findOne($fileId)) {
                $fileModel->is_uploaded = 1;
                $fileModel->uploaded_at = date('Y-m-d H:i:s');
                $fileModel->save(false);
            }
        }
    }
}