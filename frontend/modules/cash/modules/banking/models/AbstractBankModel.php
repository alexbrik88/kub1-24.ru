<?php

namespace frontend\modules\cash\modules\banking\models;

use backend\models\Bank;
use common\components\curl\Curl;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\bank\BankingParams;
use common\models\bank\StatementAutoloadMode;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OperationType;
use common\models\document\PaymentOrder;
use common\models\document\TaxpayersStatus;
use common\models\employee\Employee;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\widgets\RenderStatementWidget;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `bank044525999` module
 */
class AbstractBankModel extends Model
{
    const SCENARIO_AUTOLOAD = 'autoload';

    public static $alias;
    public static $hasAutoload = false;
    public static $hasStatementsEmail = false;

    protected $_company;
    protected $_bank;
    protected $_partner;
    protected $_params;
    protected $_banking_params = [];
    protected $_currentAccount = false;
    protected $_paymentOrder;
    protected $_payBill;
    protected $_config;
    protected $_debugMode = false;

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [];

    /**
     * @var string
     */
    protected $_errorMessage = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';

    /**
     * @var boolean
     */
    protected $_error = false;

    /**
     * @var mixed
     */
    protected $_data;

    /**
     * @var boolean
     */
    public $showBankBatton = true;
    public $showSecureText = false;
    public $statement;
    public $account_id;
    public $start_date;
    public $end_date;

    public $needPreRequest = false;

    /**
     * Construct
     */
    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        $this->_debugMode = (bool) $this->getConfig('debugMode', false);

        if (($this->_partner = Bank::findOne(['bik' => static::BIK, 'is_blocked' => false])) == null) {
            //throw new NotFoundHttpException("Bank not found in partners list.");
        }
        $this->start_date = date('01.m.Y');
        $this->end_date = date('d.m.Y');
    }

    /**
     * @inheritdoc
     */
    public function checkStartDate()
    {
        $date = date_create('first day of this month');

        if (($lastDate = ArrayHelper::getValue($this, 'currentAccount.lastUploadDate')) &&
            ($lastDate = date_create_from_format('Y-m-d', $lastDate))
        ) {
            $date = min(max($date, $lastDate), date_create());

        } else if (Banking::isTaxrobotPage() && date('Ymd') <= '20200430') {

            $date = date_create_from_format('Y-m-d', '2019-01-01');
        }

        $this->start_date = $date->format('d.m.Y');
    }

    /**
     * @return boolean
     */
    public function getHasAutoload()
    {
        return static::$hasAutoload;
    }

    /**
     * @return string
     */
    public function getConfig($name, $default = null)
    {
        if ($this->_config === null) {
            $this->_config = ArrayHelper::getValue(Yii::$app->params['banking'], static::ALIAS, []);
        }

        return ArrayHelper::getValue($this->_config, $name, $default);
    }

    /**
     * @return string
     */
    public function setParam($name, $value, $save = true)
    {
        $this->_banking_params[$name] = $value;
        if ($save) {
            BankingParams::setValue($this->company, static::ALIAS, $name, $value);
        }
    }

    /**
     * @return string
     */
    public function getParam($name, $refresh = false)
    {
        if (!array_key_exists($name, $this->_banking_params) || $refresh) {
            $this->_banking_params[$name] = BankingParams::getValue($this->company, static::ALIAS, $name);
        }

        return $this->_banking_params[$name];
    }

    /**
     * @return string
     */
    public function unsetParam($name)
    {
        return BankingParams::deleteValue($this->company, static::ALIAS, $name);
    }

    /**
     * @return BikDictionary
     */
    public function getBank()
    {
        return $this->currentAccount ? $this->currentAccount->bank : null;
    }

    /**
     * @return Bank
     */
    public function getBankPartner()
    {
        return $this->_partner;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Company
     */
    public function getBik()
    {
        return static::BIK;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {

        return static::NAME;
    }

    /**
     * @return integer
     */
    public function registrationPageTypeId($queryParams = [])
    {
        return  null;
    }

    /**
     * @return bool
     */
    public function getHasError()
    {
        return $this->_error;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->_company->getCheckingAccountants()->andWhere(['bik' => static::$bikList])->orderBy('type');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAccounts()
    {
        return $this->_company->getCheckingAccountants();
    }

    /**
     * @return common\models\company\CheckingAccountant|null
     */
    public function getCurrentAccount()
    {
        if ($this->_currentAccount === false) {
            $this->_currentAccount = $this->company->getCheckingAccountants()->andWhere([
                'id' => $this->account_id,
                'bik' => static::$bikList,
            ])->one();
        }

        return $this->_currentAccount;
    }

    /**
     * @inheritdoc
     */
    public function setPaymentOrder(PaymentOrder $paymentOrder = null)
    {
        $this->_paymentOrder = $paymentOrder;
    }

    /**
     * @return common\models\document\PaymentOrder|null
     */
    public function getPaymentOrder()
    {
        return $this->_paymentOrder;
    }

    /**
     * @inheritdoc
     */
    public function setPayBill(Invoice $ivoice)
    {
        $this->_payBill = $ivoice;
        $this->_paymentOrder = $this->getNewPaymentOrder($ivoice);
        $this->_currentAccount = $this->getNewCheckingAccountant($ivoice);
    }

    /**
     * @return common\models\document\Invoice|null
     */
    public function getPayBill()
    {
        return $this->_payBill;
    }

    /**
     * @return string
     */
    public function getTmpTokenParamName()
    {
        return '_tmp_' . static::$alias;
    }

    /**
     * @inheritdoc
     */
    public function setTmpToken($value, $exp = null)
    {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => $this->getTmpTokenParamName(),
            'value' => $value,
            'expire' => $exp ?: time() + 60 * 10,
        ]));
    }

    /**
     * @return string
     */
    public function getTmpToken()
    {
        $cookies = Yii::$app->request->cookies;

        return $cookies->getValue($this->getTmpTokenParamName());
    }

    /**
     * @return string
     */
    public function removeTmpToken()
    {
        $cookies = Yii::$app->response->cookies;

        if ($cookies->has($this->getTmpTokenParamName())) {
            $cookies->remove($this->getTmpTokenParamName());
        }
    }

    /**
     * @inheritdoc
     */
    public function setAuthState()
    {
        return;
    }

    /**
     * @return \DateTime
     */
    public function getDateTimeFrom()
    {
        return \DateTime::createFromFormat('d.m.Y', $this->start_date)->setTime(0, 0, 0);
    }

    /**
     * @return \DateTime
     */
    public function getDateTimeTill()
    {
        return \DateTime::createFromFormat('d.m.Y', $this->end_date)->setTime(23, 59, 59);
    }

    /**
     * rendering content of statement
     */
    public function renderStatement()
    {
        list($vidimus, $statistic) = $this->vidimusPrepare();
        $vidimus = (array)$vidimus;
        $rs = $this->currentAccount->rs;
        foreach ($vidimus as $item) {
            $item->company_rs = $rs;
        }

        return RenderStatementWidget::widget([
            'bankBik' => static::BIK,
            'bankName' => $this->bank->name,
            'company' => $this->company,
            'currentAccount' => $this->currentAccount,
            'vidimus' => $vidimus,
            'statistic' => $statistic,
            'newRequestUrl' => $this->newRequestUrl,
        ]);
    }

    /**
     * Prepare vidimus from request data
     */
    public function vidimusPrepare()
    {
        return [[], []];
    }

    public function getCancelUrl()
    {
        return Url::to(['/cash/banking/default/index']);
    }

    public function getNewRequestUrl()
    {
        $alias = static::$alias;
        $request = Yii::$app->getRequest();

        return Url::to([
            "/cash/banking/{$alias}/default/index",
            'account_id' => $this->account_id,
            'p' => $request instanceof \yii\web\Request ? $request->get('p') : null,
        ]);
    }

    /**
     * @return array
     */
    public function getAutoloadItems()
    {
        return array_replace(StatementAutoloadMode::find()->select('name')->indexBy('id')->column(), ['' => 'Нет']);
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        return null;
    }

    public function sendPaymentOrder()
    {
        return false;
    }

    /**
     *
     */
    public function loadStatementData($vidimusArray = [], $statistic = [])
    {
        $company = $this->company;
        $rs = $this->currentAccount->rs;
        foreach ($vidimusArray as $item) {
            $item->company_rs = $rs;
            $item->findCashBankFlowDouble();
        }

        $vidimusData = [];
        foreach ($vidimusArray as $key => $vidimus) {
            if ($vidimus->dublicate) {
                continue;
            }
            $contragent = $vidimus->getContractor();

            if ($vidimus->flowType == Vidimus::FLOW_IN) {
                $vidimusTypeId = $vidimus->incomeItemId($company) ? : '';
            } elseif ($vidimus->flowType == Vidimus::FLOW_OUT) {
                $vidimusTypeId = $vidimus->expenseItemId($company, $contragent) ? : '';
            } else {
                $vidimusTypeId = '';
            }

            $vidimusData[$key]['upload'] = true;
            $vidimusData[$key]['date'] = (string) $vidimus->date;
            $vidimusData[$key]['number'] = (string) $vidimus->number;
            $vidimusData[$key]['flow_type'] = (string) $vidimus->flowType;
            $vidimusData[$key]['total'] = (string) $vidimus->total;
            $vidimusData[$key]['contragent']['name'] = htmlspecialchars($vidimus->contragent);
            $vidimusData[$key]['target'] = htmlspecialchars($vidimus->target);
            $vidimusData[$key]['vidimus_type'] = (string) $vidimusTypeId;
            $vidimusData[$key]['type'] = (string) $vidimus->type;
            $vidimusData[$key]['contragent']['inn'] = (string) $vidimus->inn;
            $vidimusData[$key]['contragent']['kpp'] = (string) $vidimus->kpp;
            $vidimusData[$key]['contragent']['bik'] = (string) $vidimus->bik;
            $vidimusData[$key]['contragent']['currentAccount'] = (string) $vidimus->currentAccount;
            $vidimusData[$key]['contragent']['correspAccount'] = (string) $vidimus->correspAccount;
            $vidimusData[$key]['contragent']['bankName'] = (string) htmlspecialchars($vidimus->bankName);
            $vidimusData[$key]['bank']['name'] = (string) htmlspecialchars($this->bank->name);
            $vidimusData[$key]['bank']['number'] = (string) static::BIK;
            $vidimusData[$key]['doInsert'] = (string) $vidimus->doInsert;
            $vidimusData[$key]['taxpayers_status'] = (string) $vidimus->taxpayers_status;
            $vidimusData[$key]['kbk'] = (string) $vidimus->kbk;
            $vidimusData[$key]['oktmo_code'] = (string) $vidimus->oktmo_code;
            $vidimusData[$key]['payment_details'] = (string) $vidimus->payment_details;
            $vidimusData[$key]['tax_period_code'] = (string) $vidimus->tax_period_code;
            $vidimusData[$key]['document_number_budget_payment'] = (string) $vidimus->document_number_budget_payment;
            $vidimusData[$key]['document_date_budget_payment'] = (string) $vidimus->document_date_budget_payment;
            $vidimusData[$key]['payment_type'] = (string) $vidimus->payment_type;
            $vidimusData[$key]['uin_code'] = (string) $vidimus->uin_code;
            $vidimusData[$key]['account_rs'] = (string) $rs;
        }

        $uploader = new VidimusUploader($company, [$this->currentAccount]);
        $source = $this->scenario == self::SCENARIO_AUTOLOAD ?
            CashBankStatementUpload::SOURCE_BANK_AUTO :
            CashBankStatementUpload::SOURCE_BANK;

        $uploader->upload($vidimusData, $source, VidimusUploader::formatUploadData($company, $this->currentAccount, $statistic));

        return $uploader->uploadedCount;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if ($this->hasErrors()) {
            Yii::warning(static::className() . "->getErrors() " . var_export($this->getErrors(), true), 'banking');
        }

        parent::afterValidate();
    }

    /**
     * @inheritdoc
     */
    public function getNewPaymentOrder(Invoice $invoice)
    {
        return new PaymentOrder([
            'document_number' => 1,
            'company_name' => $invoice->contractor_name_short,
            'company_inn' => $invoice->contractor_inn,
            'company_kpp' => $invoice->contractor_kpp,
            'company_rs' => $invoice->contractor_rs,
            'company_bank_name' => $invoice->contractor_bank_name,
            'company_bank_city' => $invoice->contractor_bank_city,
            'contractor_name' => $invoice->company_name_short,
            'contractor_inn' => $invoice->company_inn,
            'contractor_bank_name' => $invoice->company_bank_name,
            'contractor_bank_city' => $invoice->company_bank_city,
            'contractor_current_account' => $invoice->company_rs,
            'contractor_corresponding_account' => $invoice->company_ks,
            'contractor_bik' => $invoice->company_bik,
            'contractor_kpp' => $invoice->company_kpp,
            'sum' => $invoice->total_amount_with_nds,
            'ranking_of_payment' => 5,
            'operation_type_id' => OperationType::TYPE_PAYMENT_ORDER,
            'purpose_of_payment' => $invoice->getPurposeOfPayment(),
            'taxpayers_status_id' => $invoice->contractor->company_type_id == CompanyType::TYPE_IP ?
                TaxpayersStatus::DEFAULT_IP :
                TaxpayersStatus::DEFAULT_OOO,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getNewCheckingAccountant(Invoice $invoice)
    {
        return new CheckingAccountant([
            'bank_name' => $invoice->contractor_bank_name,
            'bank_city' => $invoice->contractor_bank_city,
            'bik' => $invoice->contractor_bik,
            'ks' => $invoice->contractor_ks,
            'rs' => $invoice->contractor_rs,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getRegistrationData()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function afterRegistration(BankRegistrationForm $form = null, $params = [])
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function updateCompanyAccounts(BankRegistrationForm $form = null)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function updateIntegrationData(BankRegistrationForm $form = null)
    {
        return;
    }

    /**
     * @return string
     */
    public static function curlLogMsg(Curl $curl)
    {
        if (method_exists($curl, 'getDump')) {
            return $curl->getDump();
        } else {
            $data = [
                'curl_info' => curl_getinfo($curl->curl),
                'curl_postfields' => ArrayHelper::getValue($curl->getOptions(), CURLOPT_POSTFIELDS),
                'response_headers' => $curl->responseHeaders,
                'response' => $curl->response,
            ];
            if ($curl->errorCode || $curl->errorText) {
                $data['error_code'] = $curl->errorCode;
                $data['error_text'] = $curl->errorText;
            }

            return VarDumper::dumpAsString($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function debugLog(Curl $curl, $method)
    {
        if ($this->_debugMode) {
            $logFile = Yii::getAlias('@runtime/logs/debug_' . static::ALIAS . '.log');
            file_put_contents($logFile, $method . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }
    }

    /**
     * @inheritdoc
     */
    public function errorLog(Curl $curl, $method)
    {
        Yii::warning($method . "\n" . self::curlLogMsg($curl) . "\n", 'banking');
    }
}
