<?php

namespace frontend\modules\cash\modules\banking;

use Yii;

/**
 * banking module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@banking', '@frontend/modules/cash/modules/banking');

        foreach (components\Banking::$modelClassArray as $class) {
            $alias = $class::$alias;
            $this->setModule($alias, [
                'class' => "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\Module",
            ]);
        }
    }
}
