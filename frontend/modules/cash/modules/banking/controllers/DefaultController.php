<?php

namespace frontend\modules\cash\modules\banking\controllers;

use backend\models\Bank;
use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\service\SubscribeTariffGroup;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\components\BankingBaseController;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\models\BankingEmailHelper;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * Default controller for the `banking` module
 */
class DefaultController extends BankingBaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'file',
                            'select',
                            'upload',
                            'file-upload',
                            'file-upload-progress',
                        ],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                        ],
                    ],
                    [
                        'actions' => [
                            'account'
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionFile()
    {
        return $this->render('file', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSelect()
    {
        if (Yii::$app->request->get('show_no_bank')) {
            return $this->render('select_account');
        }

        if (Yii::$app->user->identity->company->bankingAccountants) {
            return $this->render('select_bank');
        }

        return $this->render('select_account');
    }

    /**
     * @param $bankId
     * @param $redirectAction
     * @return mixed|string|Response
     * @throws NotFoundHttpException
     */
    public function actionAccount($bankId)
    {
        $bank = Bank::findOne($bankId);
        if ($bank == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        $company = Yii::$app->user->identity->company;

        $model = new ApplicationToBank([
            'company_name' => $company->getTitle(true),
            'inn' => $company->inn,
            'legal_address' => $company->addressLegalFull,
            'fio' => Yii::$app->user->identity->fio,
            'contact_phone' => Yii::$app->user->identity->phone,
            'contact_email' => Yii::$app->user->identity->email,
        ]);

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->bank_id = $bankId;
            $model->company_id = Yii::$app->user->identity->company->id;
            if ($model->save() && $model->send()) {
                $model->bank->request_count += 1;
                $model->bank->save(false, ['request_count']);
                Yii::$app->session->setFlash('success', 'Заявка на открытие расчетного счета отправлена.');

                return $this->redirect(['/cash/bank/index']);
            } else {
                if (!Yii::$app->session->hasFlash('error')) {
                    Yii::$app->session->setFlash('error', 'Произошла ошибка при отправке заявки на открытие расчетного счета.');
                }
            }
        }

        return $this->render('_bank_account_create', [
            'model' => $model,
            'bank' => $bank,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionUpload()
    {
        set_time_limit(90);

        $uploaded = false;
        $company = Yii::$app->user->identity->company;
        $redirectRoute = Banking::routeDecode(Yii::$app->request->get('p'), ['/cash/bank/index']);
        $uploadData = Yii::$app->request->post('uploadData');
        if (isset($uploadData['accounts'])) {
            $accounts = $this->findAccounts($company, $uploadData);
            if (!empty($accounts)) {
                $vidimus = Yii::$app->request->post('vidimus', []);
                $source = Yii::$app->request->post('source');
                $service = Yii::$app->request->post('service');

                if ($vidimus && in_array($source, [CashBankStatementUpload::SOURCE_FILE, CashBankStatementUpload::SOURCE_BANK])) {
                    $uploader = new VidimusUploader($company, $accounts);
                    $uploader->upload($vidimus, $source, $uploadData);
                    Yii::$app->session->set('__statementUploadBalanceData', $uploader->uploadedBalanceData);
                    $uploaded = true;
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось загрузить выписку.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Не найден р/с, для которого загружается выписка.');
            }
        }

        if ($uploaded && ArrayHelper::getValue($redirectRoute, 0) == '/tax/robot/bank') {
            $isTaxrobotPaid = $company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
                              $company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6);
            if (!$isTaxrobotPaid) {
                Yii::$app->session->set('show_bank_ready_modal', true);
            }
        }

        if ($uploaded && Yii::$app->request->get('is_from_banking_email')) {
            BankingEmailHelper::setSuccessUploaded();
        }

        if ($uploaded && isset($redirectRoute[0]) && $redirectRoute[0] == '/cash/default/operations') {
            \frontend\components\facebook\EventTracker::event('connectbank');
        }

        return $this->redirect($redirectRoute);
    }

    /**
     * @return array
     */
    public function actionFileUpload()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $file = UploadedFile::getInstanceByName('uploadfile');
        if ($file !== null) {
            $vidimusBuilder = new VidimusBuilder($file, $company);
            $vidimuses = $vidimusBuilder->build();
            $vidimusDrawer = new VidimusDrawer($company, $vidimuses, $vidimusBuilder->uploadData());
            $vidimusDrawer->parentPage = Yii::$app->request->get('p');
            if ('dismiss-modal' == Yii::$app->request->get('cancel_button')) {
                $vidimusDrawer->cancelButton = $vidimusDrawer::getDismissModalCancelButton();
            }

            $html = $vidimusDrawer->draw($vidimusBuilder->bankInfo);

            return ['success' => true, 'html' => $html, 'bankInfo' => $vidimusBuilder->bankInfo];
            try {
            } catch (\Exception $e) {
                return  ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];
            }
        }
    }

    /**
     * @return array
     */
    public function actionFileUploadProgress()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (isset($_REQUEST['progresskey'])) {
            $status = apc_fetch('upload_'.$_REQUEST['progresskey']);
        } else {
            exit(json_encode(array('success' => false)));
        }

        $pct = 0;
        $size = 0;

        if (is_array($status)) {
            if (array_key_exists('total', $status) && array_key_exists('current', $status)) {
                if ($status['total'] > 0) {
                    $pct = round(($status['current'] / $status['total']) * 100);
                    $size = round($status['total'] / 1024);
                }
            }
        }

        return ['success' => true, 'pct' => $pct, 'size' => $size];
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'file-upload' || $action->id == 'file-upload-progress') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @param string $account
     * @return bool
     */
    protected function findAccounts(Company $company, $uploadData)
    {
        $result = [];
        $isValid = ArrayHelper::getValue($uploadData, 'isValidCompany');
        if ($isValid == 1) {
            foreach ((array) ArrayHelper::getValue($uploadData, 'accounts') as $rs => $data) {
                if ($a = $this->findAccount($company, $rs, $data)) {
                    $result[$rs] = $a;
                }
            }
        }

        return $result;
    }

    /**
     * @param string $account
     * @return bool
     */
    protected function findAccount(Company $company, $rs, $data)
    {
        if (strlen($rs) == 20) {
            $account = $company->getCheckingAccountants()->andWhere([
                'rs' => $rs,
            ])->orderBy([
                'type' => SORT_ASC,
            ])->one();

            if ($account !== null) {
                return $account;
            } else {
                $bik = ArrayHelper::getValue($data, 'bik');
                $currency = mb_substr($rs, 5, 3);
                if (isset(Currency::$currencyList[$currency])) {
                    $account = new CheckingAccountant([
                        'company_id' => $company->id,
                        'rs' => $rs,
                        'bik' => $bik,
                        'type' => CheckingAccountant::TYPE_ADDITIONAL,
                        'currency_id' => $currency,
                    ]);

                    if ($account->save()) {
                        return $account;
                    } else {
                        \common\components\helpers\ModelHelper::logErrors($account, __METHOD__);
                    }
                }
            }
        }

        return null;
    }
}
