<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$submitText = ArrayHelper::getValue($_params_, 'submitText', 'Подтвердить');
$cancelUrl = ArrayHelper::getValue($_params_, 'cancelUrl', ['index', 'p' => Yii::$app->request->get('p')]);
?>

<div class="clearfix">
    <?= Html::submitButton($submitText, ['class' => 'btn btn-primary dis-none-bank']) ?>
    <?= Html::submitButton('<i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>', [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg',
        'style' => 'width: 205px !important; color: white; float: left;',
    ]) ?>

    <?= Html::a('Отменить', $cancelUrl, [
        'class' => 'btn btn-primary banking-module-link dis-none-bank banking-cancel pull-right',
    ]) ?>
    <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $cancelUrl, [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg banking-module-link banking-cancel',
        'style' => 'float:right; width: 100px !important; color: white;',
    ]) ?>
</div>
