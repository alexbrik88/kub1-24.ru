<?php
use yii\helpers\Html;
use yii\helpers\Url;

if (!isset($action)) {
    $action = Url::to([
        '/cash/banking/default/file-upload/',
        'p' => Yii::$app->request->get('p'),
    ]);
}
?>

<div class="row upload-1C-show" style="display: none;">
    <div class="col-sm-12" style="margin-top: 20px;">
        <div class="vidimus-error">Что-то пошло не так :( Попробуйте загрузить файлы еще раз</div>
        <div class="row">
            <?= Html::beginForm($action, 'post', [
                'id' => 'vidimus-ajax-form',
                'class' => 'upload-txt',
                'enctype' => 'multipart/form-data',
            ]) ?>
                <div class="file-list col-sm-12">
                </div>
            <?= Html::endForm() ?>
            <div class="vidimus-loader"></div>
        </div>
    </div>
    <div class="col-sm-12">
        <div id="progressBox"></div>
    </div>

    <div class="col-md-12">
        <hr class="vidimus-hr" />
    </div>
    <div class="col-sm-12 upload-1C-show">
        <div id="vidimus-table"></div>
    </div>
<?php if (\Yii::$app->request->isAjax) : ?>
    <script type="text/javascript">
        initialiseVidimusUploader();
    </script>
<?php endif; ?>
</div>