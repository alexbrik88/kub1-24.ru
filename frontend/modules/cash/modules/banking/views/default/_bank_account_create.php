<?php
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/* @var $bank backend\models\Bank */
/* @var $model common\models\company\ApplicationToBank */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line-product',
    ],
    'hintOptions' => [
        'tag' => 'small',
        'class' => 'text-muted',
    ],
    'horizontalCssClasses' => [
        'offset' => 'col-md-offset-4',
        'hint' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Заявка на открытие расчетного счета</h1>
</div>
<div class="modal-body">

    <?php $form = ActiveForm::begin([
        'action' => [
            'account',
            'bankId' => $bank->id,
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'class' => 'form-horizontal',
            'id' => 'form-apply-bank-' . $bank->id,
            'data' => [
                'pjax' => true,
            ]
        ],
    ]); ?>

        <div class="form-body">
            <div class="description-bank">
                <?= nl2br($bank->description); ?>
            </div>

            <?= Alert::widget(); ?>

            <?= $form->field($model, 'company_name', $config); ?>

            <?= $form->field($model, 'inn', $config); ?>

            <?= $form->field($model, 'legal_address', $config); ?>

            <?= $form->field($model, 'fio', $config); ?>

            <?= $form->field($model, 'contact_phone', $config)->textInput(['data-inputmask' => "'mask': '+7(9{3}) 9{3}-9{2}-9{2}'"]); ?>

            <?= $form->field($model, 'contact_email', $config); ?>
            <div class="license-bank">
                <?= 'Нажимая кнопку «Открыть счет», вы даете свое согласие на ',
                    Html::a('обработку персональных данных', 'https://kub-24.ru/SecurityPolicy/Security_Policy.pdf', ['target' => '_blank']),
                    " и отправку этих данных в банк «{$bank->bank_name}»."; ?>
            </div>
            <div class="form-actions">
                <div class="row action-buttons" id="buttons-fixed">
                    <div class="button-bottom-page-lg col-sm-2 col-xs-2">
                        <?= Html::submitButton('Открыть счет', [
                            'class' => 'btn darkblue btn-save darkblue hidden-md hidden-sm hidden-xs',
                            'style' => 'width: 120px!important;',
                        ]); ?>
                        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                            'class' => 'btn darkblue btn-save darkblue hidden-lg',
                            'title' => 'Открыть счет',
                        ]); ?>
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                        <?= Html::a('Отменить', [
                            '/cash/banking/default/index',
                            'p' => Yii::$app->request->get('p'),
                        ], [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs banking-module-link banking-cancel',
                            'style' => 'width: 120px!important;;',
                        ]); ?>
                        <?= Html::a('<i class="fa fa-reply fa-2x"></i>', [
                            '/cash/banking/default/index',
                            'p' => Yii::$app->request->get('p'),
                        ], [
                            'class' => 'btn darkblue widthe-100 hidden-lg banking-module-link banking-cancel',
                            'title' => 'Отменить',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

    <?php $form->end(); ?>

</div>