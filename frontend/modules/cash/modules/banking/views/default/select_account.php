<?php
/**
 * @var $this  yii\web\View
 * @var $model common\components\banking\AbstractService||null
 */

use backend\models\Bank;
use common\models\Contractor;
use common\models\cash\CashBankReasonType;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\Module;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$bankArray = Bank::find()->where([
    'is_blocked' => 0,
    'is_special_offer' => 1,
    'bik' => Banking::bikList(),
])->all();

$mainAccount = Yii::$app->user->identity->company->mainCheckingAccountant;
$bankName = ($mainAccount && $mainAccount->bank) ? $mainAccount->bank->name : null;
$bankUrl = ($mainAccount && $mainAccount->sysBank) ? $mainAccount->sysBank->url : null;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Загрузка выписки</h1>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div>
                <a href="#" class="btn yellow add-vidimus-file">
                    <i class="fa fa-plus-circle"></i>
                    Загрузка файла 1С
                </a>
            </div>
        </div>
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div class="upload-button upload-1C-show" style="text-align: right;">
                <a href="#" class="btn darkblue disabled">
                    <i class="fa fa-download"></i>
                    Загрузить
                </a>
            </div>
            <div class="upload-1C-hide" style="text-align: right;">
                <a href="<?= Url::to(['bank/statement']) ?>" class="btn yellow no-padding banking-module-link">
                    <i class="fa fa-bank m-r-sm"></i> Загрузить из банка
                </a>
            </div>
        </div>
    </div>
    <div class="upload-1C-hide">
        <div class="row">
            <div class="gray-alert" style="text-align: center;">
                <p>
                    УПС =(
                    <br>
                    <?php if ($bankName) : ?>
                        С <b><?= $bankName ?></b> мы еще не интегрировались
                    <?php else : ?>
                        С вашим банком мы еще не интегрировались
                    <?php endif ?>
                    <br>
                    Выгрузите из
                    <?php if ($bankUrl) : ?>
                        <a href="<?= $bankUrl ?>" target="_blank">Клиент-Банка</a>
                    <?php else : ?>
                        Клиент-Банка
                    <?php endif ?>
                    выписку в формате 1С и загрузите этот файл с помощью левой кнопки.
                </p>
                <div style="font-weight: bold;">
                    Мы уже интегрировались и автоматически загружаем выписки из банков партнеров:
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            foreach ($bankArray as $bank) {
                echo $this->render('_select_account_item', ['model' => $bank]);
            }
            ?>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= Html::a('Отменить', [
                    '/cash/banking/default/index',
                    'p' => Yii::$app->request->get('p'),
                ], [
                    'class' => 'btn darkblue text-white banking-module-link banking-cancel',
                    'style' => 'float:right;',
                ]); ?>
            </div>
        </div>
    </div>

    <?= $this->render('_vidimus_upload') ?>
</div>