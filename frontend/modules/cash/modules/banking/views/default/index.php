<?php
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use yii\helpers\Url;

$bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;
$bankIndex = 0;

// Show Bank by account_id
if ($account_id = Yii::$app->request->get('account_id')) {
    if ($bankingAccountsArray) {
        foreach ($bankingAccountsArray as $key => $bankingAccount) {
            if ($bankingAccount['id'] == $account_id) {
                $bankIndex = $key;
                break;
            }
        }
    }
}
$bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[$bankIndex] : null;
$bankingUrl = $bankingAccount && ($module = Banking::aliasByBik($bankingAccount->bik)) ?
    Url::to([
        "/cash/banking/{$module}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) :
    Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Загрузка выписки</h1>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div>
                <a href="#" class="btn yellow add-vidimus-file">
                    <i class="fa fa-plus-circle"></i>
                    Загрузка файла 1С
                </a>
            </div>
            <div class="upload-1C-hide" style="margin-top: 24px;">
                <!-- Если вы выгрузите из Клиент-Банка файл формата 1С,
                то его можно загрузить в КУБ и все счета,
                которые оплачены изменят статус на "Оплачен". -->
                Выгрузите из Клиент-Банка файл формата 1С и загрузите его в КУБ.
            </div>
        </div>
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div class="upload-button upload-1C-show" style="text-align: right;">
                <a href="#" class="btn darkblue disabled">
                    <i class="fa fa-download"></i>
                    Загрузить
                </a>
            </div>
            <div class="upload-1C-hide" style="text-align: right;">
                <a href="<?= $bankingUrl ?>" class="btn yellow no-padding banking-module-link">
                    <i class="fa fa-bank m-r-sm"></i> Загрузить из банка
                </a>
            </div>
            <div class="upload-1C-hide" style="margin-top: 24px;">
                КУБ уже интегрирован с несколькими банками.
                Если у вас есть счет в этом банке,
                то выписка будет загружаться напрямую из банка.
            </div>
            <div id="bank-info" style="text-align: right;"></div>
        </div>
    </div>

    <?= $this->render('_vidimus_upload') ?>
</div>