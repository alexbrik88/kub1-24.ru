<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\dictionary\bik\BikDictionary
 */

use common\components\ImageHelper;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-sm-12 bank-list-item" style="margin-top: 20px; margin-bottom: 20px;">
    <div class="cont-img_bank-logo" style="width: 150px; float: left;">
        <?= ImageHelper::getThumb(
            $model->getUploadDirectory() . $model->logo_link,
            [150, 90],
            [
                'class' => 'bank-logo',
                'style' => 'padding-right: 15px; cursor: pointer;',
            ]
        ); ?>
    </div>
    <div style="margin-left: 160px; padding: 5px 0;">
        <div class="bank-description bank-description-collaps" onclick="$(this).toggleClass('bank-description-collaps');">
            <?= nl2br($model->description); ?>
        </div>
        <?= Html::a('Открыть счет по акции?', [
            '/cash/banking/default/account',
            'bankId' => $model->id,
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'banking-module-link',
        ]) ?>
    </div>
</div>
