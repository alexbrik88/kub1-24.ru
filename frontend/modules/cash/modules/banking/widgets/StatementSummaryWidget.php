<?php

namespace frontend\modules\cash\modules\banking\widgets;

use yii\base\Widget;

class StatementSummaryWidget extends Widget
{
    public $dateStart;
    public $dateEnd;
    public $balanceStart;
    public $balanceEnd;
    public $totalIncome;
    public $totalExpense;

    public function run()
    {
        return $this->render('statement_summary', [
            "dateStart" => $this->dateStart,
            "dateEnd" => $this->dateEnd,
            "balanceStart" => $this->balanceStart,
            "balanceEnd" => $this->balanceEnd,
            "totalIncome" => $this->totalIncome,
            "totalExpense" => $this->totalExpense,
        ]);
    }
}
