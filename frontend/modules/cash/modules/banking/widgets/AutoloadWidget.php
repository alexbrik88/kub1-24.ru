<?php

namespace frontend\modules\cash\modules\banking\widgets;

use yii\base\Widget;

class AutoloadWidget extends Widget
{
    public $model;
    public $needPay = false;

    public function run()
    {
        return $this->render('_autoload', [
            'model' => $this->model,
            'needPay' => $this->needPay,
        ]);
    }
}
