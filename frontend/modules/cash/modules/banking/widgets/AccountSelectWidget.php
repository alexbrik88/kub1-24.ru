<?php

namespace frontend\modules\cash\modules\banking\widgets;

use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use Yii;
use yii\base\Exception;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

class AccountSelectWidget extends Widget
{
    /**
     * @var yii\widgets\ActiveForm
     */
    public $form;
    /**
     * @var frontend\modules\cash\modules\banking\models\AbstractBankModel
     */
    public $bankModel;
    /**
     * @var string
     */
    public $action = 'index';
    /**
     * CheckingAccountant[]
     * @var array
     */
    public $accounts;
    /**
     * @var array
     */
    public $urlParams = [];

    public $linkOptions = [];
    public $itemOptions = [];

    public function init()
    {
        parent::init();

        if (!$this->bankModel instanceof AbstractBankModel) {
            throw new Exception("The $bankModel must by instanceof AbstractBankModel");
        }
    }

    public function run()
    {
        $accountArray = $this->accounts ? : $this->bankModel->company->bankingAccountants;
        $items = ArrayHelper::map($accountArray, 'id', 'rs');
        $itemsData = [];
        $p = Yii::$app->request->get('p');

        foreach ($accountArray as $account) {
            $alias = Banking::aliasByBik($account->bik);
            $url = [
                "/cash/banking/{$alias}/default/{$this->action}",
                'account_id' => $account->id,
                'p' => $p,
            ];
            $itemsData[$account->id] = [
                'data-url' => Url::to(array_merge($url, $this->urlParams)),
            ];
        }

        return $this->render('accountSelectWidget', [
            'form' => $this->form,
            'bankModel' => $this->bankModel,
            'items' => $items,
            'itemsData' => $itemsData,
        ]);
    }
}
