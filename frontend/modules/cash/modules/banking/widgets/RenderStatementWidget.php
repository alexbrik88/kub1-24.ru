<?php

namespace frontend\modules\cash\modules\banking\widgets;

use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use yii\base\Widget;

class RenderStatementWidget extends Widget
{
    /**
     * @var string
     */
    public $bankBik;
    /**
     * @var string
     */
    public $bankName;
    /**
     * @var common\models\Company
     */
    public $company;
    /**
     * @var CheckingAccountant
     */
    public $currentAccount;
    /**
     * @var array
     */
    public $vidimus;
    /**
     * @var array
     */
    public $statistic;
    /**
     * @var array
     */
    public $newRequestUrl;

    public function run()
    {
        $vidimusBuilder = new VidimusBuilder(null, $this->company, $this->currentAccount->rs);
        $vidimus = $vidimusBuilder->handlingVidimus($this->vidimus);
        $statistic = $this->statistic;
        if (!isset($statistic['accounts'])) {
            $statistic['accounts'][$this->currentAccount->rs] = [
                'balanceStart' => $statistic['balanceStart'] ?? null,
                'balanceEnd' => $statistic['balanceEnd'] ?? null,
                'totalIncome' => $statistic['totalIncome'] ?? null,
                'totalExpense' => $statistic['totalExpense'] ?? null,
            ];
        }
        $vidimusDrawer = new VidimusDrawer($this->company, $vidimus, $statistic);
        $vidimusDrawer->parentPage = \Yii::$app->request->get('p');

        return $vidimusDrawer->draw([
            'name' => $this->currentAccount->bank_name,
            'number' => $this->currentAccount->bik,
        ]);
    }
}
