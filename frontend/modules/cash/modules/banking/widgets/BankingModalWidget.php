<?php

namespace frontend\modules\cash\modules\banking\widgets;

use Yii;
use frontend\modules\cash\modules\banking\assets\BankingAsset;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class BankingModalWidget extends Widget
{
    public static $rendered = false;

    /**
     * Show the modal on page load
     * @var boolean
     */
    public $showModal = false;
    /**
     * whether to enable push state
     * @var boolean
     */
    public $enablePushState = true;
    /**
     * Onclose page title
     * @var string
     */
    public $pageTitle;
    /**
     * Onclose page url
     * @var string
     */
    public $pageUrl;

    public function run()
    {
        return BankingModalWidget::$rendered ? null : $this->runWidget();
    }

    public function runWidget()
    {
        BankingModalWidget::$rendered = true;

        $enablePushState = $this->enablePushState ? 'true' : 'false';
        $view = $this->view;

        $modalOptions = [
            'id' => 'banking-module-modal',
            'class' => 'modal fade',
            'role' => 'modal',
            'data-push-state' => $enablePushState,
            'data-page-title' => $view->title, // $this->pageTitle,
            'data-page-url' => Url::to(Banking::currentRoute()), // $this->pageUrl,
        ];

        $pjaxOptions = [
            'id' => 'banking-module-pjax',
            'enablePushState' => $this->enablePushState,
            'enableReplaceState' => false,
            'timeout' => 10000,
            'linkSelector' => '.banking-module-link',
            'options' => [
                'data-push-state' => $enablePushState,
            ]
        ];

        $content = ArrayHelper::getValue($view->params, 'bankingContent', '');

        if ($content || $this->showModal) {
            $view->registerJs('
                $("#banking-module-modal").modal("show");


');
        }

        $uploadedBalanceData = Yii::$app->session->remove('__statementUploadBalanceData');

        return $this->render('BankingModalWidget', [
            'modalOptions' => $modalOptions,
            'pjaxOptions' => $pjaxOptions,
            'content' => $content,
            'uploadedData' => $uploadedBalanceData,
        ]);
    }
}
