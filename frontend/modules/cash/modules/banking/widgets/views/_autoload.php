<?php

use common\models\cash\CashBankStatementUpload;
use yii\bootstrap\Collapse;

?>

<?php if ($model->getHasAutoload() && $model->currentAccount) : ?>

<?php
$uploadedArray = $model->currentAccount ? $model->currentAccount->getStatementUploads()
    ->andWhere(['source' => CashBankStatementUpload::SOURCE_BANK_AUTO])
    ->orderBy(['created_at' => SORT_DESC])
    ->limit(5)
    ->all() : [];
?>

<div class="row banking-autoload-wrapper" style="min-height: 34px;">
    <div class="col-sm-6" style="width: 300px;">
        <?= Collapse::widget([
            'id' => 'autoload-form-container',
            'encodeLabels' => false,
            'options' => [
                'class' => 'autoload-form-collapse',
            ],
            'items' => [
                [
                    'label' => 'Автоматически загружать выписку <span class="caret"></span>',
                    'content' => $this->render('_autoload_form', [
                        'model' => $model,
                        'needPay' => $needPay,
                    ]),
                ],
            ]
        ]); ?>
    </div>
    <div id="statement-history" class="col-sm-8 panel-collapse collapse" style="width: 370px;">
        <label style="display: block; margin-top: 15px;">Последние автозагрузки</label>
        <table style="margin-top: 15px; border: 1px solid #e5e5e5;">
            <thead>
                <tr>
                    <th style="min-width: 120px; padding: 0 5px 2px; border: 1px solid #e5e5e5;">Период</th>
                    <th style="min-width: 120px; padding: 0 5px 2px; border: 1px solid #e5e5e5;">Дата загрузки</th>
                    <th style="min-width: 90px; padding: 0 5px 2px; border: 1px solid #e5e5e5;">Загружено операций</th>
                </tr>
            </thead>
            <tbody style="border: 1px solid #e5e5e5;">
                <?php if ($uploadedArray) : ?>
                    <?php foreach ($uploadedArray as $uploaded) : ?>
                        <?php
                        $from = new \DateTime($uploaded->period_from);
                        $till = new \DateTime($uploaded->period_till);
                        ?>
                        <tr>
                            <td style="padding: 0 5px 2px; border-right: 1px solid #e5e5e5;">
                                <?= ($from && $till) ? $from->format('d.m.y') . '-' . $till->format('d.m.y') : '' ?>
                            </td>
                            <td style="padding: 0 5px 2px; border-right: 1px solid #e5e5e5; text-align: center;">
                                <?= date('d.m.Y', $uploaded->created_at) ?>
                            </td>
                            <td style="padding: 0 5px 2px; text-align: center;">
                                <?= $uploaded->saved_count ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php else : ?>
                    <tr>
                        <td colspan="3" style="padding: 0 5px 2px;">
                            Автозагрузки не найдены
                        </td>
                    </tr>
                <?php endif ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).on('submit', '#autoload-mode-form', function(e) {
        e.preventDefault();
        $.post($(this).attr('action'), $(this).serialize(), function(data) {
            $('#autoload_save_report').show();
            setTimeout(function() {
                $('#autoload_save_report').hide();
            }, 3000);
        });
    });
    $('#autoload-form-container').on('show.bs.collapse', function () {
        $('#statement-history').collapse('show');
    })
    $('#autoload-form-container').on('hide.bs.collapse', function () {
        $('#statement-history').collapse('hide');
    })
</script>

<?php endif ?>
