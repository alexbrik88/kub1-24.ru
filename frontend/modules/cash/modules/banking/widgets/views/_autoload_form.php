<?php

use yii\helpers\Html;
use yii\helpers\Url;

$account = $model->currentAccount;
if ($needPay && $account->autoload_mode_id) {
    $account->updateAttributes(['autoload_mode_id' => null]);
}

$selected = $account->autoload_mode_id ? : '';
?>

<?= Html::beginForm(['set-autoload', 'accountId' => $account->id], 'post', [
    'id' => 'autoload-mode-form',
]) ?>
    <?= Html::radioList('autoload', $selected, $model->autoloadItems, [
        'id' => 'autoload-mode-selector',
        'style' => 'margin: 15px 0;',
        'item' => function ($index, $label, $name, $checked, $value) use ($needPay) {
            return Html::radio($name, $checked, [
                'value' => $value,
                'disabled' => $needPay && $value,
                'label' => Html::encode($label),
                'labelOptions' => [
                    'class' => $needPay && $value ? 'need-paid-tariff' : '',
                ],
            ]);
        },
    ]) ?>

    <?= Html::submitButton('Сохранить', [
        'class' => 'btn yellow save-tooltip',
        'data-tooltip-content' => '#tooltip_mode-selector',
        'style' => 'margin-right: 10px'
    ]) ?>
    <span id="autoload_save_report" style="display: none; color: green;">
        Настройки сохранены
    </span>
<?= Html::endForm() ?>
