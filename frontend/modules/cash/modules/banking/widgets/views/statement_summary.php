<?php

/* @var $this yii\web\View */
/* @var $dateStart string */
/* @var $dateEnd string */
/* @var $balanceStart integer */
/* @var $balanceEnd integer */
/* @var $totalIncome integer */
/* @var $totalExpense integer */

$isVisible = strval($balanceEnd) !== "";
?>

<?php if ($isVisible) : ?>
    <table class="mb-3">
        <tbody>
            <tr>
                <td colspan="4">
                    <b>Период:</b> с <?= $dateStart ?> по <?= $dateEnd ?>
                </td>
            </tr>
            <tr style="vertical-align: bottom;">
                <td>
                    <b>
                        Остаток на начало периода:
                    </b>
                </td>
                <td class="text-right pl-2">
                    <?= is_numeric($balanceStart) ? Yii::$app->formatter->asDecimal($balanceStart, 2) : '---'; ?>
                </td>
                <td class="text-right pl-4">
                    <b>
                        Приход:
                    </b>
                    <span class="vidimus-table">
                        <span class="in"></span>
                    </span>
                    <?= is_numeric($totalIncome) ? Yii::$app->formatter->asDecimal($totalIncome, 2) : '---'; ?>
                </td>
                <td class="text-right pl-4">
                    <b>
                        Расход:
                    </b>
                    <span class="vidimus-table">
                        <span class="out"></span>
                    </span>
                    <?= is_numeric($totalExpense) ? Yii::$app->formatter->asDecimal($totalExpense, 2) : '---'; ?>
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        Остаток на конец периода:
                    </b>
                </td>
                <td class="text-right pl-2">
                    <?= is_numeric($balanceEnd) ? Yii::$app->formatter->asDecimal($balanceEnd, 2) : '---'; ?>
                </td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
<?php endif ?>
