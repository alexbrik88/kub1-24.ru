<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $modalOptions array */
/* @var $pjaxOptions array */
/* @var $content mixed */

$js = <<<JS
var vidimusSubmitClicked = false;

$(document).on("click", ".banking-module-open-link", function(e) {
    e.preventDefault();
    $.pjax({url: this.href, container: "#banking-module-pjax", push: $("#banking-module-modal").data("push-state")});
    $("#banking-module-modal").modal("show");
});

$(document).on("change", "#bankmodel-account_id", function (e) {
    $('span.banking-accoun-select-loader').removeClass('hidden');
    $.pjax({
        url: $("option:selected", this).data("url"),
        container: "#banking-module-pjax",
        push : $("#banking-module-modal").data("push-state"),
    });
});
$(document).on("ajaxComplete", function(){
    $('span.banking-accoun-select-loader').addClass('hidden');
});
$(document).on("hide.bs.modal", "#banking-module-modal", function (e) {
    var bankingModal = $(this);
    if (bankingModal.data("push-state")) {
        history.pushState({}, bankingModal.data("page-title"), bankingModal.data("page-url"));
    }
});

$(document).on("hidden.bs.modal", "#banking-module-modal", function (e) {
    $("#banking-module-pjax", this).html("");
});

$(document).on("click", "button.statement-form-submit", function(e) {
    if (vidimusSubmitClicked) {
        e.preventDefault();
    } else {
        vidimusSubmitClicked = true;
    }
    setTimeout(vidimusSubmitClicked = false, 1000);
});

$(document).on("pjax:success", "#banking-module-pjax", function() {
    $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var input = $("[name='" + ev.currentTarget.name +"']");
            if (ev.currentTarget.value == "") {
                if (input.data("last-value") == null) {
                    input.data("last-value", ev.currentTarget.defaultValue);
                }
                var lastDate = input.data("last-value");
                input.datepicker("setDate", lastDate);
            } else {
                input.data("last-value", ev.currentTarget.value);
            }
        }
    };

    // Create Select2 in banking modal
    var select2_banking_modal = {
        "allowClear":false,
        "placeholder":"",
        "templateResult":formatBankingAccountResult,
        "templateSelection":formatBankingAccountSelection,
        "language":{},
        "theme":"krajee",
        "width":"100%",
        "minimumResultsForSearch":Infinity
    };

    var s2options_xxx_banking_modal = {
        "themeCss":".select2-container--krajee",
        "sizeCss":"",
        "doReset":true,
        "doToggle":false,
        "doOrder":false
    };

    function formatBankingAccountResult(data) {
        if (!data.id)
            return data.text;
        var logo = $(data.element).data("logo");
        if (!logo)
            return data.text;

        opt = $('<img width="16" style="float:right;margin:2px 21px 0 0;" src="'+logo+'"/><span>' + data.text + '</span>');
        return opt;
    };

    function formatBankingAccountSelection(data) {
        if (!data.id)
            return data.text;
        var logo = $(data.element).data("logo");
        if (!logo)
            return data.text;

        opt = $('<img width="16" style="float:right;margin:2px 3px 0 0;" src="'+logo+'"/><span>' + data.text + '</span>');
        return opt;
    };

    if (jQuery("#bankmodel-account_id").data("select2")) { jQuery("#bankmodel-account_id").select2("destroy"); }
    jQuery.when(jQuery("#bankmodel-account_id").select2(select2_banking_modal)).done(initS2Loading("bankmodel-account_id","s2options_xxx_banking_modal"));

});

JS;

$this->registerJs($js);
?>

<?= Html::beginTag('div', $modalOptions); ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <?php Pjax::begin($pjaxOptions); ?>

            <?= $content ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
<?= Html::endTag('div') ?>