<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $bankModel frontend\modules\cash\modules\banking\models\AbstractBankModel */
/* @var $items array */
/* @var $itemsData array */

?>

<?php if ($form) : ?>
    <?= $form->field($bankModel, 'account_id', [
        'labelOptions' => [
            'class' => 'control-label bold-text',
        ]
    ])->dropDownList($items, [
        'options' => $itemsData,
        'style' => 'width: 100%;'
    ])->hint(Html::tag('span', Html::img('/img/loader/ajax-loader.gif'), [
        'class' =>'banking-accoun-select-loader hidden',
        'style' => 'position: absolute; right: -5px; top: 32px;',
    ])) ?>
<?php else : ?>
    <div class="form-group">
        <?= Html::activeLabel($bankModel, 'account_id', [
            'class' => 'control-label bold-text',
        ]) ?>
        <?= Html::activeDropDownList($bankModel, 'account_id', $items, [
            'options' => $itemsData,
            'class' => 'form-control',
            'style' => 'width: 100%;'
        ]) ?>
        <?= Html::tag('span', Html::img('/img/loader/ajax-loader.gif'), [
            'class' =>'banking-accoun-select-loader hidden',
            'style' => 'position: absolute; right: -5px; top: 6px;',
        ]) ?>
    </div>
<?php endif ?>
