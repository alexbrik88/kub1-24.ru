<?php
/**
 * @var $this  yii\web\View
 * @var $model frontend\modules\cash\modules\banking\modules\sberbank\models\BankModel
 */

use common\models\service\SubscribeTariffGroup;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;
use frontend\modules\cash\modules\banking\components\Banking;

// start
$dateRows = [];
if (!empty($start_preload_statement)) {
    $daysOffset = 0;
    $daysCount = date_diff($model->getDateTimeTill(), $model->getDateTimeFrom())->days;
    do {
        $currDate = $model->getDateTimeFrom()->modify("+ {$daysOffset} days");
        $dateRows[] = $currDate->format('d.m.Y');
        $daysOffset++;
    } while ($daysOffset <= $daysCount);
} else {
    $start_preload_statement = null;
}

$account_id = (int)Yii::$app->request->get('account_id');
$isFreeTariff = !$model->company->getHasPaidActualSubscription();
$isTaxrobotPage = Banking::isTaxrobotPage();
$isTaxrobotPaid = $model->company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
                  $model->company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6);

$cssClassNottPaid = "";
if (!$isTaxrobotPage && $isFreeTariff) {
    $cssClassNottPaid = 'need-paid-tariff-form';
} elseif ($isTaxrobotPage && !$isTaxrobotPaid) {
    $cssClassNottPaid = 'taxrobot-not-paid-form';
}
?>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'statement-request-form',
        'action' => [
            'request',
            'account_id' => $account_id,
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'class' => $cssClassNottPaid,
            'data' => [
                'pjax' => true,
            ],
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'success_loaded', [
            'id' => 'success_loaded',
            'value' => (int)$model->success_loaded]) ?>

    <div class="row">
        <div class="col-sm-6">
            <?= AccountSelectWidget::widget([
                'form' => $form,
                'bankModel' => $model,
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'start_date', [
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
                'labelOptions' => [
                    'class' => 'control-label width-label bold-text',
                ],
                'wrapperOptions' => [
                  'class' => 'col-md-5',
                ],
            ])->textInput(['class' => 'form-control date-picker width-full']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date', [
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
                'labelOptions' => [
                    'class' => 'control-label width-label bold-text',
                ],
                'wrapperOptions' => [
                  'class' => 'col-md-5',
                ],
            ])->textInput(['class' => 'form-control date-picker width-full']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php if (!$isTaxrobotPage && $isFreeTariff) : ?>
                <?= Html::button('Отправить запрос', [
                    'class' => 'btn darkblue text-white visible-lg pull-left need-paid-tariff',
                ]); ?>
                <?= Html::button('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg pull-left need-paid-tariff',
                ]); ?>
            <?php elseif ($isTaxrobotPage && !$isTaxrobotPaid) : ?>
                <?= Html::button('Отправить запрос', [
                    'class' => 'btn darkblue text-white visible-lg pull-left taxrobot-not-paid',
                ]); ?>
                <?= Html::button('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg pull-left taxrobot-not-paid',
                ]); ?>
            <?php elseif (!$start_preload_statement) : ?>
                <?= Html::submitButton('Отправить запрос', [
                    'class' => 'btn darkblue text-white visible-lg pull-left',
                    'style' => '',
                ]); ?>
                <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                    'class' => 'btn darkblue text-white hidden-lg pull-left',
                    'title' => 'Отправить запрос',
                ]); ?>
            <?php endif; ?>
            <?= Html::a('Отменить', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn darkblue text-white visible-lg back banking-module-link banking-cancel pull-right',
                'style' => 'width: 120px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn darkblue text-white hidden-lg back banking-module-link banking-cancel pull-right',
                'title' => 'Отменить',
            ]); ?>
        </div>
    </div>
    <?php $form->end() ?>

    <div class="row">
        <div class="col-sm-12">
            <div class="progress-status process-shift-info" style="display:none;width:100%;margin-left:0;">
                <label><span class="export-progress-action"
                             data-in-progress="получение данных... "
                             data-completed="загрузка завершена"></span><span
                            class="progress-value"></span></label>

                <div class="progress progress-striped active export-progressbar">
                    <div class="progress-bar" role="progressbar"
                         aria-valuenow="0" aria-valuemin="0"
                         aria-valuemax="100" style="width:0%;">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= \frontend\modules\cash\modules\banking\widgets\AutoloadWidget::widget([
        'model' => $model,
        'needPay' => $isFreeTariff,
    ]); ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>

<?php if ($start_preload_statement): ?>

    <script>

        var Sberbank = {};

        Sberbank.currentRow = 0;
        Sberbank.rows = <?= (!empty($dateRows)) ? json_encode($dateRows) : '{}' ?>;

        console.log(Sberbank.rows);

        Sberbank.getInfo = function() {

            var process = $('#statement-request-form-container').find('.progress-status');
            var processAction = $(process).find('.export-progress-action');
            var processWidth = $(process).find('.progress-bar');

            if ("undefined" !== typeof Sberbank.rows[Sberbank.currentRow]) {

                $(processAction).html($(processAction).attr('data-in-progress'));

                $.post("/cash/banking/sberbank/default/ajax-request", {
                    "BankModel[success_loaded]": 0,
                    "BankModel[start_date]": Sberbank.rows[Sberbank.currentRow],
                    "BankModel[end_date]": Sberbank.rows[Sberbank.currentRow],
                    "BankModel[account_id]": <?= $account_id ?>
                }, function (data) {
                    if (data.result === true) {

                        Sberbank.currentRow++;

                        $(processWidth).css('width', Math.round(100 * Sberbank.currentRow / Sberbank.rows.length) + '%');

                        Sberbank.getInfo(); // RECURSIVE

                    } else {

                        if (data.message)
                            alert(data.message)
                        else
                            alert('Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.');

                        $('#banking-module-modal').modal('hide');
                    }
                });

            } else {

                $('#success_loaded').val(1);
                $(processAction).html($(processAction).attr('data-completed'));
                $('#statement-request-form').submit();
            }

        };

        $('.process-shift-info').show();
        Sberbank.getInfo();

    </script>
<?php endif; ?>
<?php if ($isFreeTariff) : ?>
    <script type="text/javascript">
        $(document).on("submit", "form.need-paid-tariff-form, form.taxrobot-not-paid-form", function (e) {
            e.preventDefault();
        });
        $(document).ready(function() {
            $("#statement-request-form-container .need-paid-tariff").tooltipster({
                content: 'Доступно только на платном тарифе',
                theme: ['tooltipster-kub'],
                trigger: 'click',
            });
        });
    </script>
<?php endif; ?>
