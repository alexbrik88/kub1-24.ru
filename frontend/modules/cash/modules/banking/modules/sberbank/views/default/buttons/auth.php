<?php

use yii\helpers\Html;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/* @var $cancelUrl array|string */

echo $this->registerJs('
        $(".tooltip-sberbank-token").tooltipster({
            theme: ["tooltipster-kub"],
            contentCloning: true,
            trigger: "click",
            side: "top"
        });');

?>
<style>
    .tooltipster-kub.tooltipster-top {margin-left:2px;}
</style>
<div class="clearfix">
    <?= Html::button('Подтвердить интеграцию', [
            'class' => 'btn btn-primary dis-none-bank tooltip-sberbank-token',
            'data-tooltip-content' => '#sberbank-choose-token'
    ]) ?>
    <?= Html::button('<i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>', [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg tooltip-sberbank-token',
        'style' => 'width: 205px !important; color: white; float: left;',
        'data-tooltip-content' => '#sberbank-choose-token'
    ]) ?>

    <?= Html::a('Отменить', $cancelUrl, [
        'class' => 'btn btn-primary banking-module-link banking-cancel dis-none-bank',
        'style' => 'width: 205px !important; float:right;',
    ]) ?>
    <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $cancelUrl, [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg banking-module-link banking-cancel',
        'style' => 'float:right; width: 100px !important; color: white;',
    ]) ?>

    <div style="display: none">
        <div id="sberbank-choose-token" class="update-attribute-tooltip-content" style="width: 175px;text-align:center">
            <div class="mar-b-5 text-bold text-center">У вас есть токен?</div>
            <div class="btn-group" style="border: 1px solid #ddd;">
                <button class="btn sberbank-token" style="width: 49px; color: #fff; background-color: #3379b5;" form="">Да</button>
                <button class="btn sberbank-web" style="width: 49px; color: #333; background-color: #fff;" form="">Нет</button>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $(".tooltip-sberbank-token").tooltipster({
                    theme: ["tooltipster-kub"],
                    contentCloning: true,
                    trigger: "click",
                    side: "top"
                });
                $(document).on('click', '.sberbank-token', function() {
                    $('#bankmodel-user_type').val('1');
                    $('#statement-request-form').submit();
                });
                $(document).on('click', '.sberbank-web', function() {
                    $('#bankmodel-user_type').val('0');
                    $('#statement-request-form').submit();
                });
            });
        </script>
    </div>

</div>
