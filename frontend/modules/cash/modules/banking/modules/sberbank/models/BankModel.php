<?php

namespace frontend\modules\cash\modules\banking\modules\sberbank\models;

use common\components\curl;
use common\models\bank\Bank;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\CompanyHelper;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\EmployeeRole;
use DateTime;
use frontend\modules\cash\modules\banking\models\BankRegistrationForm;
use Yii;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * BankModel
 *
 * https://developer.sberbank.ru/doc/v2/sbbol/mainserv-statement-transactions
 *
 * ПАО «Сбербанк»
 *
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525225';
    const NAME = 'ПАО «СберБанк»';
    const NAME_SHORT = 'СберБанк';
    const ALIAS = 'sberbank';

    public static $alias = 'sberbank';
    public static $hasPaymentApi = false;

    public static $hasAutoload = true;
    public static $tmpPath;

    const USER_TYPE_WEB = 0;
    const USER_TYPE_TOKEN = 1;

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [
        '040173604', // АЛТАЙСКОЕ ОТДЕЛЕНИЕ N8644 ПАО СБЕРБАНК
        '040349602', // КРАСНОДАРСКОЕ ОТДЕЛЕНИЕ N8619 ПАО СБЕРБАНК
        '040407627', // КРАСНОЯРСКОЕ ОТДЕЛЕНИЕ N 8646 ПАО СБЕРБАНК
        '040702615', // СТАВРОПОЛЬСКОЕ ОТДЕЛЕНИЕ N5230 ПАО СБЕРБАНК
        '040813608', // ДАЛЬНЕВОСТОЧНЫЙ БАНК ПАО СБЕРБАНК
        '041117601', // АРХАНГЕЛЬСКОЕ ОТДЕЛЕНИЕ N 8637 ПАО СБЕРБАНК
        '041203602', // АСТРАХАНСКОЕ ОТДЕЛЕНИЕ N8625 ПАО СБЕРБАНК
        '041403633', // БЕЛГОРОДСКОЕ ОТДЕЛЕНИЕ N8592 ПАО СБЕРБАНК
        '041501601', // БРЯНСКОЕ ОТДЕЛЕНИЕ N8605 ПАО СБЕРБАНК
        '041708602', // ВЛАДИМИРСКОЕ ОТДЕЛЕНИЕ №8611 ПАО СБЕРБАНК
        '041806647', // ВОЛГОГРАДСКОЕ ОТДЕЛЕНИЕ №8621 ПАО СБЕРБАНК
        '041909644', // ВОЛОГОДСКОЕ ОТДЕЛЕНИЕ N8638 ПАО СБЕРБАНК
        '042007681', // ЦЕНТРАЛЬНО-ЧЕРНОЗЕМНЫЙ БАНК ПАО СБЕРБАНК
        '042202603', // ВОЛГО-ВЯТСКИЙ БАНК ПАО СБЕРБАНК
        '042406608', // ИВАНОВСКОЕ ОТДЕЛЕНИЕ N 8639 ПАО СБЕРБАНК
        '042520607', // БАЙКАЛЬСКИЙ БАНК ПАО СБЕРБАНК
        '042748634', // КАЛИНИНГРАДСКОЕ ОТДЕЛЕНИЕ N8626 ПАО СБЕРБАНК
        '042809679', // ТВЕРСКОЕ ОТДЕЛЕНИЕ N8607 ПАО СБЕРБАНК
        '042908612', // КАЛУЖСКОЕ ОТДЕЛЕНИЕ N8608 ПАО СБЕРБАНК
        '043207612', // КЕМЕРОВСКОЕ ОТДЕЛЕНИЕ N8615 ПАО СБЕРБАНК
        '043304609', // КИРОВСКОЕ ОТДЕЛЕНИЕ N8612 ПАО СБЕРБАНК
        '043469623', // КОСТРОМСКОЕ ОТДЕЛЕНИЕ N 8640 ПАО СБЕРБАНК
        '043601607', // ПОВОЛЖСКИЙ БАНК ПАО СБЕРБАНК
        '043735650', // КУРГАНСКОЕ ОТДЕЛЕНИЕ N8599 ПАО СБЕРБАНК
        '043807606', // КУРСКОЕ ОТДЕЛЕНИЕ N8596 ПАО СБЕРБАНК
        '044030653', // СЕВЕРО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК
        '044206604', // ЛИПЕЦКОЕ ОТДЕЛЕНИЕ N8593 ПАО СБЕРБАНК
        '044442607', // СЕВЕРО-ВОСТОЧНОЕ ОТДЕЛЕНИЕ N8645 ПАО СБЕРБАНК
        '044525225', // ПАО СБЕРБАНК
        '044525366', // КУ ЗАО "ПРОМСБЕРБАНК" - ГК "АСВ"
        '044705615', // МУРМАНСКОЕ ОТДЕЛЕНИЕ N8627 ПАО СБЕРБАНК
        '044959698', // НОВГОРОДСКОЕ ОТДЕЛЕНИЕ N 8629 ПАО СБЕРБАНК
        '045004641', // СИБИРСКИЙ БАНК ПАО СБЕРБАНК
        '045209673', // ОМСКОЕ ОТДЕЛЕНИЕ N 8634 ПАО СБЕРБАНК
        '045354601', // ОРЕНБУРГСКОЕ ОТДЕЛЕНИЕ N8623 ПАО СБЕРБАНК
        '045402601', // ОРЛОВСКОЕ ОТДЕЛЕНИЕ N8595 ПАО СБЕРБАНК
        '045655635', // ПЕНЗЕНСКОЕ ОТДЕЛЕНИЕ N8624 ПАО СБЕРБАНК
        '045805602', // ПСКОВСКОЕ ОТДЕЛЕНИЕ N 8630 ПАО СБЕРБАНК
        '046015602', // ЮГО-ЗАПАДНЫЙ БАНК ПАО СБЕРБАНК
        '046126614', // РЯЗАНСКОЕ ОТДЕЛЕНИЕ N 8606 ПАО СБЕРБАНК
        '046577674', // УРАЛЬСКИЙ БАНК ПАО СБЕРБАНК
        '046614632', // СМОЛЕНСКОЕ ОТДЕЛЕНИЕ N8609 ПАО СБЕРБАНК
        '046850649', // ТАМБОВСКОЕ ОТДЕЛЕНИЕ N8594 ПАО СБЕРБАНК
        '046902606', // ТОМСКОЕ ОТДЕЛЕНИЕ N8616 ПАО СБЕРБАНК
        '047003608', // ТУЛЬСКОЕ ОТДЕЛЕНИЕ N8604 ПАО СБЕРБАНК
        '047102651', // ЗАПАДНО-СИБИРСКИЙ БАНК ПАО СБЕРБАНК
        '047308602', // УЛЬЯНОВСКОЕ ОТДЕЛЕНИЕ N8588 ПАО СБЕРБАНК
        '047501602', // ЧЕЛЯБИНСКОЕ ОТДЕЛЕНИЕ N8597 ПАО СБЕРБАНК
        '047601637', // ЧИТИНСКОЕ ОТДЕЛЕНИЕ N8600 ПАО СБЕРБАНК
        '048073601', // БАШКИРСКОЕ ОТДЕЛЕНИЕ N8598 ПАО СБЕРБАНК
        '048142604', // БУРЯТСКОЕ ОТДЕЛЕНИЕ N8601 ПАО СБЕРБАНК
        '048405602', // ГОРНО-АЛТАЙСКОЕ ОТДЕЛЕНИЕ N8558 ПАО СБЕРБАНК
        '048602673', // КАРЕЛЬСКОЕ ОТДЕЛЕНИЕ N8628 ПАО СБЕРБАНК
        '048702640', // КОМИ ОТДЕЛЕНИЕ N8617 ПАО СБЕРБАНК
        '048860630', // ОТДЕЛЕНИЕ МАРИЙ ЭЛ N8614 ПАО СБЕРБАНК
        '048952615', // МОРДОВСКОЕ ОТДЕЛЕНИЕ N8589 ПАО СБЕРБАНК
        '049205603', // ОТДЕЛЕНИЕ "БАНК ТАТАРСТАН" N8610 ПАО СБЕРБАНК
        '049401601', // УДМУРТСКОЕ ОТДЕЛЕНИЕ N8618 ПАО СБЕРБАНК
        '049514608', // АБАКАНСКОЕ ОТДЕЛЕНИЕ N8602 ПАО СБЕРБАНК
        '049706609', // ЧУВАШСКОЕ ОТДЕЛЕНИЕ N8613 ПАО СБЕРБАНК
        '049805609', // ЯКУТСКОЕ ОТДЕЛЕНИЕ N8603 ПАО СБЕРБАНК
    ];

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';
    const SCENARIO_REGISTRATION = 'registration';

    const SCOPE = 'openid KUB';

    public $auth_redirect;
    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $state;
    public $nonce;
    public $error;
    public $success_loaded;
    public $errorRequestMessage;

    // WEB or Token
    public $user_type;

    protected static $authHost = 'https://sbi.sberbank.ru:9443';
    protected static $tokenAuthHost = 'http://localhost:28016';
    protected static $apiHost = 'https://fintech.sberbank.ru:9443';

    // auth
    protected static $authPath = '/ic/sso/api/v1/oauth/authorize';

    // api
    protected static $tokenPath = '/ic/sso/api/v1/oauth/token';
    protected static $infoPath = '/ic/sso/api/v1/oauth/user-info';
    protected static $statementPath = '/fintech/api/v1/statement/transactions';
    protected static $clientInfoPath = '/fintech/api/v1/client-info';
    protected static $paymentOrderPath = '/fintech/api/v1/payments';

    private $_accountId = false;
    private $_accountInfo;

    /**
     * @var array
     */
    public static $regPageTypeId = [
        'default' => RegistrationPageType::PAGE_TYPE_BANK_SBERBANK,
        'invoice' => RegistrationPageType::PAGE_TYPE_BANK_SBERBANK,
        'taxrobot' => RegistrationPageType::PAGE_TYPE_SBERBANK_TAXROBOT,
    ];

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
                'user_type'
            ],
            static::SCENARIO_TOKEN => [
                'state',
                'nonce',
                'code',
                'error',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
                'success_loaded'
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
            static::SCENARIO_PAY_BILL => [
                'payBill',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'account_id',
                'start_date',
                'end_date',
                'code',
                'state',
                'nonce',
                'payBill',
                'paymentOrder',
                'currentAccount',
                'success_loaded',
                'user_type'
            ], 'required'],
            [['state', 'nonce', 'code', 'error'], 'string'],
            [['success_loaded', 'user_type'], 'integer'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:d.m.Y'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->company->id,
                    'bik' => static::$bikList,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
            'user_type' => 'Тип пользователя'
        ];
    }

    public function init() {
        parent::init();
        self::$tmpPath = sys_get_temp_dir();
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        $this->setAuthState();
        $this->setAuthNonce();

        $byToken =
            self::USER_TYPE_TOKEN == $this->user_type ||
            self::USER_TYPE_TOKEN == Yii::$app->session->get('banking.sberbank.user_type');

        return ($byToken ? self::$tokenAuthHost : self::$authHost) . self::$authPath . '?' . http_build_query([
                'client_id' => Yii::$app->params['banking'][static::ALIAS]['client_id'],
                'redirect_uri' => $this->getReturnUrl(),
                'state' => $this->getAuthState(),
                'nonce' => $this->getAuthNonce(),
                'scope' => self::SCOPE,
                'response_type' => 'code'
            ]);
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return Yii::$app->request->hostInfo . Url::to([
            '/cash/banking/' . static::ALIAS . '/default/return',
        ]);
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return 'banking.' . static::BIK . '.' . $this->_company->id ;
    }

    /**
     * @inheritdoc
     */
    public function setAuthState()
    {
        $session = Yii::$app->session;
        $session[$this->sessionKey . '.state'] = $this->uuid();
    }

    /**
     * @return string
     */
    public function getAuthState()
    {
        if (Yii::$app->id == 'app-frontend') {
            $session = Yii::$app->session;

            return $session[$this->sessionKey . '.state'];
        }

        return $this->uuid();
    }

    /**
     * @inheritdoc
     */
    public function setAuthNonce()
    {
        $session = Yii::$app->session;
        $session[$this->sessionKey . '.nonce'] = $this->uuid();
    }

    /**
     * @return string
     */
    public function getAuthNonce()
    {
        if (Yii::$app->id == 'app-frontend') {
            $session = Yii::$app->session;

            return $session[$this->sessionKey . '.nonce'];
        }

        return $this->uuid();
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return Yii::$app->params['banking'][static::ALIAS]['client_id'] ?? null;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return Yii::$app->params['banking'][static::ALIAS]['client_secret'] ?? null;
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return self::SCOPE;
    }


    /**
     * @inheritdoc
     */
    public function setAccessToken($access_token)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'access_token', $access_token);
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        return $this->company->id ?
            BankingParams::getValue($this->company, static::ALIAS, 'access_token') :
            $this->getTmpToken();
    }

    /**
     * @inheritdoc
     */
    public function setAccessTokenExpires($expires_at)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'access_token_expires', $expires_at);
    }

    /**
     * @inheritdoc
     */
    public function getAccessTokenExpires()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'access_token_expires');
    }

    /**
     * @inheritdoc
     */
    public function setRefreshToken($refresh_token)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'refresh_token', $refresh_token);
    }

    /**
     * @return string|null
     */
    public function getRefreshToken()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'refresh_token');
    }

    /**
     * @inheritdoc
     */
    public function setIdToken($refresh_token)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'id_token', $refresh_token);
    }

    /**
     * @return string|null
     */
    public function getIdToken()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'id_token');
    }

    /**
     * @inheritdoc
     */
    public function setAccountId($id)
    {
        /*if ($this->currentAccount) {
            BankingParams::setValue($this->company, static::ALIAS, $this->currentAccount->rs, $id);
        }*/
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->_accountId;
        /*return $this->currentAccount ? BankingParams::getValue($this->company, static::ALIAS, $this->currentAccount->rs) : null;*/
    }

    /**
     * @inheritdoc
     */
    public function checkAccount()
    {
        if ($this->_accountId === false) {
            if (isset($this->_accountInfo['accounts'])) {
                foreach ((array) $this->_accountInfo['accounts'] as $account) {
                    $accountNumber = ArrayHelper::getValue($account, 'number') ?:
                                     ArrayHelper::getValue($account, 'accountNumber');
                    if ($this->currentAccount && $this->currentAccount->rs == $accountNumber) {
                        $this->accountId = $this->account_id;

                        return true;
                    }
                }
            }

            $this->_accountId = null;
        }

        return false;
    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        if ($this->company->id) {
            if ($this->accessToken && $this->accessTokenExpires > (time() + 5)) {
                return true;
            } else {
                return $this->refreshToken ? $this->authTokenRefresh($this->refreshToken) : false;
            }
        }

        return $this->getTmpToken() !== null;
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest($isGuest = false)
    {
        $time = time();
        $requestHeader = [
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type' => 'authorization_code',
            'code' => $this->code,
            'client_id'=> $this->clientId,
            'client_secret'=> $this->clientSecret,
            'redirect_uri' => $this->returnUrl,
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
            CURLOPT_SSLKEY => Yii::$app->params['banking'][static::ALIAS]['ssl_key'],
            CURLOPT_SSLCERT => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
            CURLOPT_SSLKEYPASSWD => Yii::$app->params['banking'][static::ALIAS]['ssl_key_pass'],
            CURLOPT_CAINFO => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
        ])->post(self::$apiHost . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                if ($isGuest) {
                    $this->setTmpToken($requestResult['access_token']);
                    $this->setTmpIntegrationData([
                        'access_token' => $requestResult['access_token'],
                        'access_token_expires' => $time + $requestResult['expires_in'],
                        'refresh_token' => $requestResult['refresh_token'],
                        'id_token' => $requestResult['id_token']
                    ]);
                } else {
                    $this->accessToken = $requestResult['access_token'];
                    $this->accessTokenExpires = $time + $requestResult['expires_in'];
                    $this->refreshToken = $requestResult['refresh_token'];
                    $this->idToken = $requestResult['id_token'];
                }

                return true;

            } elseif (!empty($requestResult->error_description)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->error_description);
                }
                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for authToken
     * @return string|null
     */
    public function authTokenRefresh($refresh_token)
    {
        $time = time();
        $requestHeader = [
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type'=> 'refresh_token',
            'refresh_token' => $refresh_token,
            'client_id'=> $this->clientId,
            'client_secret'=> $this->clientSecret,
        ];

        $curl = new \common\components\curl\Curl();

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
            CURLOPT_SSLKEY => Yii::$app->params['banking'][static::ALIAS]['ssl_key'],
            CURLOPT_SSLCERT => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
            CURLOPT_SSLKEYPASSWD => Yii::$app->params['banking'][static::ALIAS]['ssl_key_pass'],
            CURLOPT_CAINFO => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
        ])->post(self::$apiHost . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                $this->accessToken = $requestResult['access_token'];
                $this->accessTokenExpires = $time + $requestResult['expires_in'];
                $this->refreshToken = $requestResult['refresh_token'];

                return true;

            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }

                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API
     *
     * @return bool
     */
    public function checkApiAccess()
    {
        throw new \yii\base\Exception('Method disabled! (mxfi:20.02.2020)');
    }

    public function statementRequest($byDate = null)
    {
        $this->_data = [];

        $requestHeader = [
            "authorization: Bearer {$this->getAccessToken()}",
        ];

        $currDate = $byDate ?: $this->getDateTimeFrom();

        if ($currDate) {

            $hasPages = false;
            $page = 1;

            do {

                if ($this->accessTokenExpires < (time()+5)) {
                    $this->authTokenRefresh($this->refreshToken);
                }

                $curl = new \common\components\curl\Curl();
                $response = $curl->setOptions([
                    CURLOPT_HTTPHEADER => $requestHeader,
                    CURLINFO_HEADER_OUT => true,
                    CURLOPT_SSLKEY => Yii::$app->params['banking'][static::ALIAS]['ssl_key'],
                    CURLOPT_SSLCERT => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
                    CURLOPT_SSLKEYPASSWD => Yii::$app->params['banking'][static::ALIAS]['ssl_key_pass'],
                    CURLOPT_CAINFO => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
                ])->get(self::$apiHost . self::$statementPath . '?' . implode('&', [
                    'accountNumber='.$this->getCurrentAccount()->rs,
                    'statementDate='.$currDate->format('Y-m-d'),
                    'page='.$page
                ]));

                $this->debugLog($curl, __METHOD__);

                if (!$curl->errorCode) {
                    $requestResult = json_decode($response, true);

                    if (ArrayHelper::getValue($requestResult, 'transactions')) {

                        // TO FILE
                        $this->_data = $this->loadTmpData();
                        $this->saveTmpData(array_merge($this->_data, $requestResult['transactions']));

                        $links = ArrayHelper::getValue($requestResult, '_links');
                        $hasPages = $links && ArrayHelper::getValue(ArrayHelper::getValue($links, '0'), 'rel') == 'next';

                        $page++;

                    }

                } else {
                    $this->errorRequestMessage = $this->_errorMessage;
                    $this->errorLog($curl, __METHOD__);
                    return false;
                }

            } while ($hasPages);
        }

        elseif (Yii::$app->id == 'app-frontend') {
            $this->errorRequestMessage = 'Неверный формат даты';
            return false;
        }

        return true;
    }

    /**
     * Prepare vidimus
     * @return array
     */
    public function vidimusPrepare()
    {
        $vidimuses = [];
        $data = $this->loadTmpData();

        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => null,
            'balanceEnd' => null,
            'totalIncome' => null,
            'totalExpense' => null,
        ];

        try {
            foreach ($data as $operation) {

                $ruTransfer = $operation['rurTransfer'];

                switch (ArrayHelper::getValue($operation, 'direction')) {
                    case 'CREDIT':
                        $flowType = Vidimus::FLOW_IN;
                        $contractor = "payer";
                        break;
                    case 'DEBIT':
                        $flowType = Vidimus::FLOW_OUT;
                        $contractor = "payee";
                        break;
                    default:
                        $flowType = null;
                        $contractor = null;
                        break;
                }
                if ($flowType !== null) {

                    $name = $contractor . 'Name';
                    $inn = $contractor . 'Inn';
                    $kpp = $contractor . 'Kpp';
                    $bic = $contractor . 'BankBic';
                    $bank = $contractor . 'BankName';
                    $rs = $contractor . 'Account';

                    $taxDetails = ArrayHelper::getValue($ruTransfer, 'departmentalInfo');

                    $vidimuses[] = new Vidimus($this->company, [
                        'date' => DateTime::createFromFormat('Y-m-d', $ruTransfer['valueDate'])->format('d.m.Y'),
                        'number' => (string) $operation['number'],
                        'total' => round($operation['amountRub']['amount'], 2),
                        'contragent' => (string) $ruTransfer[$name],
                        'target' => (string) $operation['paymentPurpose'],
                        'flowType' => $flowType,
                        'inn' => !empty($ruTransfer[$inn]) ? (string) $ruTransfer[$inn] : '',
                        'kpp' => !empty($ruTransfer[$kpp]) ? (string) $ruTransfer[$kpp] : '',
                        'bik' => !empty($ruTransfer[$bic]) ? (string) $ruTransfer[$bic] : '',
                        'bankName' => !empty($ruTransfer[$bank]) ? (string) $ruTransfer[$bank] : '',
                        'currentAccount' => !empty($ruTransfer[$rs]) ? (string) $ruTransfer[$rs] : '',
                        'taxpayers_status' => ArrayHelper::getValue($taxDetails, 'drawerStatus101', ''),
                        'ranking_of_payment' => ArrayHelper::getValue($operation, 'priority', ''),
                        'kbk' => ArrayHelper::getValue($taxDetails, 'kbk', ''),
                        'oktmo_code' => ArrayHelper::getValue($taxDetails, 'oktmo', ''),
                        'payment_details' => ArrayHelper::getValue($taxDetails, 'reasonCode106', ''),
                        'tax_period_code' => ArrayHelper::getValue($taxDetails, 'taxPeriod107', ''),
                        'document_number_budget_payment' => ArrayHelper::getValue($taxDetails, 'docNumber108', ''),
                        'document_date_budget_payment' => ArrayHelper::getValue($taxDetails, 'docDate109', ''),
                        'payment_type' => ArrayHelper::getValue($taxDetails, 'paymentKind110', ''),
                        'uin_code' => ArrayHelper::getValue($taxDetails, 'uip', ''),
                    ]);
                }
            }

            return [$vidimuses, $statistic];
        } catch (\Exception $e) {
            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            return [[], $statistic];
        }
    }

    /**
     * Платежка
     *
     * @return bool
     */
    public function sendPaymentOrder()
    {
        if (!$accountId = $this->getAccountId()) {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
            return false;
        }

        $requestHeader = [
            "Content-Type: application/json",
            "Authorization: Bearer {$this->accessToken}",
        ];

        /** @var PaymentOrder $paymentOrder */
        $paymentOrder = $this->paymentOrder;

        $requestData = [
            'amount' => bcdiv($paymentOrder->sum, 100, 2),
            'date' => date('Y-m-d'),
            'externalId' => (string) $paymentOrder->banking_guid,
            'number' => (string) $paymentOrder->document_number,
            'operationCode' => "01",
            'deliveryKind' => "0",
            'payeeInn' => $paymentOrder->contractor_inn,
            'payeeKpp' => $paymentOrder->contractor_kpp,
            'payeeAccount' => $paymentOrder->contractor_current_account,
            'payeeName' => $paymentOrder->contractor_name,
            'payeeBankBic' => $paymentOrder->contractor_bik,
            'payeeBankCorrAccount' => $paymentOrder->contractor_corresponding_account,
            'payerInn' => $paymentOrder->company_inn,
            'payerKpp' => $paymentOrder->company_kpp,
            'payerAccount' => $paymentOrder->company_rs,
            'payerName' => $paymentOrder->company_name,
            'payerBankBic' => $paymentOrder->company_bik,
            'payerBankCorrAccount' => $paymentOrder->company_ks,

            'priority' => (string) ($paymentOrder->ranking_of_payment ? : 5),
            'purpose' => (string) $paymentOrder->purpose_of_payment
        ];

        if ($paymentOrder->presence_status_budget_payment) {
            $requestData['departmentalInfo'] = [
                'uip' => (string) $paymentOrder->uin_code,
                'drawerStatus101' => (string) $paymentOrder->taxpayers_status_id,
                'kbk' => (string) $paymentOrder->kbk,
                'oktmo' => (string) $paymentOrder->oktmo_code,
                'reasonCode106' => (string) (($paymentOrder->paymentDetails) ? $paymentOrder->paymentDetails->code : 0),
                'taxPeriod107' => (string) ($paymentOrder->tax_period_code ? : 0),
                'docNumber108' => (string) $paymentOrder->document_number_budget_payment,
                'docDate109' => ($paymentOrder->document_date_budget_payment) ? date('d.m.Y', strtotime($paymentOrder->document_date_budget_payment)) : "0",
                'paymentKind110' => (string) (($paymentOrder->paymentType) ? $paymentOrder->paymentType->code : 0)
            ];
        }

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
            CURLOPT_SSLKEY => Yii::$app->params['banking'][static::ALIAS]['ssl_key'],
            CURLOPT_SSLCERT => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
            CURLOPT_SSLKEYPASSWD => Yii::$app->params['banking'][static::ALIAS]['ssl_key_pass'],
            CURLOPT_CAINFO => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
        ])->post(self::$apiHost . self::$paymentOrderPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($curl->responseCode == 200 && $requestResult) {
                if (isset($requestResult['bankStatus']) && $requestResult['bankStatus'] == 'CREATED') {
                    $this->paymentOrder->updateAttributes([
                        'payment_order_status_id' => PaymentOrderStatus::STATUS_SENT_TO_BANK,
                        'payment_order_status_updated_at' => time(),
                    ]);
                    if (Yii::$app->id == 'app-frontend') {
                        $bankUrl = $this->bankPartner ? $this->bankPartner->url : null;
                        $msg = 'Платежное поручение успешно отправлено в клиент банк и находится в разделе "Черновики". ';
                        $msg .= ($bankUrl ? Html::a('Подтвердите платежку', $bankUrl, ['target' => '_blank']) : 'Подтвердите платежку');
                        $msg .= ', чтобы оплатить по счету.';
                        Yii::$app->session->setFlash('success', $msg);
                    }
                    return true;

                } elseif (Yii::$app->id == 'app-frontend') {
                    $msg = $this->_errorMessage;
                    if (!empty($requestResult['checks'])) {
                        $msg = 'Обнаружены ошибки<br/>';
                        foreach ((array)$requestResult['checks'] as $check) {
                            $msg .= $check['message'] . '<br/>';
                        }
                    }
                    Yii::$app->session->setFlash('error', $msg);

                    return false;
                }
            } else {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('warning', 'Сервис Сбербанка временно недоступен. Попробуйте пожалуйста позже.');
                }
            }
        } else {
            $this->errorLog($curl, __METHOD__);
        }

        return false;
    }

    public function loadTmpData() {
        $filename = self::$tmpPath . "/sberbank_c{$this->_company->id}a{$this->account_id}.tmp";
        $data = @file_get_contents($filename);

        return ($data) ? json_decode($data, true) : [];
    }

    public function clearTmpData() {
        if (!is_dir(self::$tmpPath)) {
            mkdir(self::$tmpPath);
        }
        $filename = self::$tmpPath . "/sberbank_c{$this->_company->id}a{$this->account_id}.tmp";
        $bytes = @file_put_contents($filename, "");

        return ($bytes !== false);
    }

    private function saveTmpData(array $data) {
        if (!is_dir(self::$tmpPath)) {
            mkdir(self::$tmpPath);
        }
        $filename = self::$tmpPath . "/sberbank_c{$this->_company->id}a{$this->account_id}.tmp";
        $bytes = @file_put_contents($filename, json_encode($data));

        return ($bytes !== false);
    }

    /**
     * @param $str
     * @return array|mixed
     */
    private function getJWTPayload($str)
    {
        $m = explode('.', $str);

        if (!empty($m[1])) {
            $ret = json_decode(self::base64_url_decode($m[1]), true);

            return (is_array($ret) && isset($ret['inn'])) ? $ret : [];
        }

        return [];
    }

    private function base64_url_encode( $data ) {
        return strtr( base64_encode($data), '+/=', '-_,' );
    }

    private function base64_url_decode( $data ) {
        return base64_decode( strtr($data, '-_,', '+/=') );
    }

    /**
     * @return string
     */
    private function uuid(){
        $data = random_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        if ($this->isValidToken()) {

            if ($this->getDateTimeFrom()->getTimestamp() > $this->getDateTimeTill()->getTimestamp()) {
                Yii::warning("Sberbank: неверно указан период выписки: " . $this->getDateTimeFrom()->format('d.m.Y').'-'.$this->getDateTimeTill()->format('d.m.Y'), 'banking');
                return false;
            }

            $this->clearTmpData();

            $daysOffset = 0;
            $daysCount = date_diff($this->getDateTimeTill(), $this->getDateTimeFrom())->days;

            do {

                $currDate = $this->getDateTimeFrom()->modify("+ {$daysOffset} days");
                $this->statementRequest($currDate);

                $daysOffset++;

            } while ($daysOffset <= $daysCount);


            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function userInfoRequest()
    {
        $requestHeader = [
            "Content-Type: application/json",
            "Authorization: Bearer {$this->accessToken}",
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => null,
            CURLOPT_SSLKEY => Yii::$app->params['banking'][static::ALIAS]['ssl_key'],
            CURLOPT_SSLCERT => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
            CURLOPT_SSLKEYPASSWD => Yii::$app->params['banking'][static::ALIAS]['ssl_key_pass'],
            CURLOPT_CAINFO => Yii::$app->params['banking'][static::ALIAS]['ssl_cert'],
        ])->get(self::$apiHost . self::$infoPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {

            if ($curl->responseCode == 200 && $response) {

                return $this->getJWTPayload($response);

            } elseif ($curl->responseCode == 503) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('warning', 'Сервис Сбербанка временно недоступен. Попробуйте пожалуйста позже.');
                }
            }
        } else {
            $this->errorLog($curl, __METHOD__);
        }

        Yii::$app->session->setFlash('error', 'Нет доступа к АПИ Сбербанк.');

        return [];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getRegistrationData()
    {
        if ($data = $this->userInfoRequest()) {
            $is_chief = true;
            $email = ArrayHelper::getValue($data, 'email');
            $phone = ArrayHelper::getValue($data, 'phone_number');

            $inn = ArrayHelper::getValue($data, 'inn');
            $kpp = ArrayHelper::getValue($data, 'orgKpp');
            $addressLegal = ArrayHelper::getValue($data, 'orgJuridicalAddress');
            $addressActual = ArrayHelper::getValue($data, 'orgActualAddress');
            preg_match('/^[а-яё ]+/ui', $data['OrgName'], $matches);
            $companyType = ($matches) ? trim($matches[0]) : '';

            $userFio = explode(' ', $data['name']);
            $lastName = ArrayHelper::getValue($userFio, 0);
            $firstName = ArrayHelper::getValue($userFio, 1);
            $middleName = ArrayHelper::getValue($userFio, 2);

            $typeId = $companyType ? (CompanyType::find()->select('id')->where([
                'name_short' => $companyType,
            ])->scalar() ? : null) : null;

            if ($typeId === null) {
                switch (strlen((string) $inn)) {
                    case 10:
                        $typeId = CompanyType::TYPE_OOO;
                        break;
                    case 12:
                        $typeId = CompanyType::TYPE_IP;
                        break;

                    default:
                        $typeId = CompanyType::TYPE_EMPTY;
                        break;
                }
            }

            $isIp = $typeId == CompanyType::TYPE_IP;
            $companyData = array_merge([
                'company_type_id' => $typeId,
                'inn' => $inn,
                'kpp' => $kpp,
                'address_legal' => $addressLegal,
                'address_actual' => $addressActual ?: $addressLegal,
                'chief_lastname' => $is_chief ? $lastName : null,
                'chief_firstname' => $is_chief ? $firstName : null,
                'chief_patronymic' => $is_chief ? $middleName : null,
                'ip_lastname' => $is_chief && $isIp ? $lastName : null,
                'ip_firstname' => $is_chief && $isIp ? $firstName : null,
                'ip_patronymic' => $is_chief && $isIp ? $middleName : null,
                'nds' => $isIp ? null : \common\models\NdsOsno::WITH_NDS,
            ], (array) CompanyHelper::getCompanyData($inn, $kpp));

            $checkingAccountant = null;
            $accountArray = ArrayHelper::getValue($data, 'accounts', []);
            $allAccounts = [];
            foreach ((array)$accountArray as $account) {
                $allAccounts[] = [
                    'bik' => ArrayHelper::getValue($account, 'bic', static::BIK),
                    'rs' => ArrayHelper::getValue($account, 'accountNumber'),
                    'ks' => ArrayHelper::getValue($account, 'corrAccountNumber'),
                    'type' => CheckingAccountant::TYPE_MAIN,
                ];
            }

            if (!empty($allAccounts)) {
                $checkingAccountant = $allAccounts[0];
            }

            if ($checkingAccountant !== null) {
                if ($isIp) {
                    $taxationType = ['usn' => 1];
                } else {
                    $taxationType = ['osno' => 1];
                }

                $companyData['email'] = $email;
                $companyData['phone'] = $phone;

                return [
                    'Company' => $companyData,
                    'CompanyTaxationType' => $taxationType,
                    'CheckingAccountant' => $checkingAccountant,
                    'Employee' => [
                        'employee_role_id' => $is_chief ? EmployeeRole::ROLE_CHIEF :  EmployeeRole::ROLE_ACCOUNTANT,
                        'lastname' => $lastName,
                        'firstname' => $firstName,
                        'patronymic' => $middleName,
                        'position' => ArrayHelper::getValue($data, 'managingPost'),
                        'email' => $email,
                        'phone' => $phone,
                    ],
                    'BankUser' => [
                        'bank_alias' => static::$alias,
                        'user_uid' => (string) ArrayHelper::getValue($data, 'sub'),
                        'company_uid' => (string) ArrayHelper::getValue($data, 'HashOrgId'),
                        'inn' => (string) $inn,
                        'kpp' => (string) $kpp,
                        'is_chief' => (int) $is_chief,
                    ],
                    'accounts' => $allAccounts
                ];
            } else {
                Yii::warning(static::class . "::getRegistrationData()\nno checkingAccountant", 'banking');
            }
        }

        return [];
    }

    /**
     * @return integer
     */
    public function registrationPageTypeId($queryParams = [])
    {
        $page = ArrayHelper::getValue($queryParams, 'page');
        $authReferrer = Yii::$app->session->get('banking.sberbank.auth_referrer');

        if ($authReferrer && strpos($authReferrer, 'robot.kub-24') !== false) {
            $page = 'taxrobot';
        }

        return  ArrayHelper::getValue(self::$regPageTypeId, $page, self::$regPageTypeId['default']);
    }

    /**
     * @inheritdoc
     */
    public function updateCompanyAccounts(BankRegistrationForm $form = null)
    {
        $accounts = $form->getAccountsData();
        $company = $form->getCompany();

        if (!$company || !$accounts)
            return false;

        foreach ($accounts as $account) {
            if (!CheckingAccountant::find()->where([
                'company_id' => $company->id,
                'rs' => $account['rs'],
                'bik' => $account['bik']
            ])->exists()) {

                /** @var Bank $bank */
                $bank = Bank::findOne([
                    'bik' => $account['bik'],
                ]) ?: null;

                $newAccount = new CheckingAccountant();
                $newAccount->company_id = $company->id;
                $newAccount->bik = $account['bik'];
                $newAccount->rs = $account['rs'];
                $newAccount->ks = $account['ks'];
                $newAccount->type = CheckingAccountant::TYPE_ADDITIONAL;
                $newAccount->bank_name = ($bank) ? $bank->bank_name : 'ПАО СБЕРБАНК';

                if (!$newAccount->save()) {
                    throw new Exception('Не удалось сохранить р/с. Попробуйте пожалуйста еще раз.');
                }
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function updateIntegrationData(BankRegistrationForm $form = null)
    {
        $company = $form->getCompany();
        $data = $this->getTmpIntegrationData();

        if (!$company || !$data)
            return false;

        BankingParams::setValue($company, static::ALIAS, 'access_token', $data['access_token']);
        BankingParams::setValue($company, static::ALIAS, 'access_token_expires', $data['access_token_expires']);
        BankingParams::setValue($company, static::ALIAS, 'refresh_token', $data['refresh_token']);
        BankingParams::setValue($company, static::ALIAS, 'id_token', $data['id_token']);

        return true;
    }

    public function setTmpIntegrationData($data)
    {
        Yii::$app->session->set(self::ALIAS.'.auth_integration_data', $data);
    }

    public function getTmpIntegrationData()
    {
        return Yii::$app->session->get(self::ALIAS.'.auth_integration_data', []);
    }
}