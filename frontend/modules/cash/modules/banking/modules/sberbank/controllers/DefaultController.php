<?php

namespace frontend\modules\cash\modules\banking\modules\sberbank\controllers;

use common\models\Company;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\components\BankingModulesBaseController;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\modules\sberbank\models\BankModel;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `sberbank` module
 *
 * ПАО «Сбербанк»
 */
class DefaultController extends BankingModulesBaseController
{
    /**
     * @var BankModel
     */
    public $bankModelClass = BankModel::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'request',
                            'ajax-request',
                            'payment',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => [
                            'set-autoload',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'return',
                            'pay-bill',
                            'registration',
                            'login',
                            'auth',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($account_id = null, $redirect_url = null)
    {
        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;
        $model->account_id = $account_id;
        $model->checkStartDate();

        if ($model->isValidToken()) {
            $model->scenario = BankModel::SCENARIO_REQUEST;

            return $this->render('request', ['model' => $model]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Url::remember($redirect_url ?? Url::current(), BankModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getAuthUrl());
        }

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionReturn()
    {
        $guestRoutes = [
            BankModel::$alias . '/default/pay-bill',
            BankModel::$alias . '/default/auth',
        ];
        $pattern = implode('|', $guestRoutes);
        $redirectUrl = Url::previous(BankModel::$alias.'ReturnRedirect');
        $isGuest = (boolean) preg_match ("~($pattern)~" , $redirectUrl);

        if (Yii::$app->user->isGuest && !$isGuest) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_TOKEN, $isGuest ? new Company : null);

        if ($model->load(Yii::$app->request->get(), '') && $model->validate() && $model->authTokenRequest($isGuest)) {
            Yii::$app->session->setFlash('success', "Вы успешно авторизованы.");
            if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) {
                \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 98, true);
            }
        } else {
            Yii::$app->session->setFlash('error', "Во время авторизации произошла ошибка.");
            Yii::warning(static::className() . "->actionReturn() " . var_export($model->errors, true), 'banking');
        }

        return $this->redirect($redirectUrl ? : ['index']);
    }

    public function actionRequest()
    {
        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);

        if (!$model->company->getHasPaidActualSubscription() && !Banking::isTaxrobotPage()) {
            Yii::$app->session->setFlash('error', 'Загрузка выписки из Сберабанка доступна только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $model->load(Yii::$app->request->post());
        $model->showBankBatton = false;

        if ($model->success_loaded) {
            return $this->renderContent($model->renderStatement());
        }

        if ($model->isValidToken()) {

            if ($model->validate()) {

                if ($model->getDateTimeFrom()->getTimestamp() > $model->getDateTimeTill()->getTimestamp()) {
                    Yii::$app->session->setFlash('error', 'Неверно указан период выписки.');
                    return $this->render('request', ['model' => $model]);
                }

                $model->clearTmpData();

                return $this->render('request', [
                    'model' => $model,
                    'start_preload_statement' => true
                ]);

            } else {

                return $this->render('request', ['model' => $model]);
            }
        } else {

            return $this->render('index', [
                'model' => $this->getModel(BankModel::SCENARIO_DEFAULT),
                'action' => 'request',
            ]);
        }
    }

    public function actionAjaxRequest() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);
        $model->load(Yii::$app->request->post());

        if (!$model->company->getHasPaidActualSubscription() && !Banking::isTaxrobotPage()) {
            Yii::$app->session->setFlash('error', 'Загрузка выписки из Сберабанка доступна только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        if ($model->validate()) {
            if ($model->statementRequest())

                return ['result' => true];

        } else {
            $model->errorRequestMessage = 'Неверный формат данных';
        }

        return ['result' => false, 'message' => $model->errorRequestMessage];
    }

    /**
     * @return string
     */
    public function actionPayment($account_id = null, $po_id = null)
    {
        /** @var BankModel $model */
        $this->layout = '@banking/views/layouts/payment';
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->account_id = $account_id;
        $model->paymentOrder = $this->findPaymentOrder($po_id);
        if ($model->paymentOrder && !$model->paymentOrder->banking_guid) {
            $model->paymentOrder->banking_guid = self::generateGUID();
            $model->paymentOrder->updateAttributes(['banking_guid']);
        }

        $model->setScenario(BankModel::SCENARIO_PAYMENT_ORDER);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->isValidToken()) {
                return $this->render('@banking/views/all-banks/payment', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        }

        return $this->render('@banking/views/all-banks/payment', ['model' => $model, 'sent' => false]);
    }

    public function actionWebhook() {
    }

    /**
     * @return string
     */
    public function actionRegistration()
    {
        $byToken = Yii::$app->request->get('by_token', 0);
        Yii::$app->session->set('banking.sberbank.user_type', $byToken);
        Yii::$app->session->set('banking.sberbank.auth_referrer', Yii::$app->request->referrer);

        return parent::actionRegistration();
    }

    /**
     * @return string
     */
    public function actionLogin($page = null)
    {
        $byToken = Yii::$app->request->get('by_token', 0);
        Yii::$app->session->set('banking.sberbank.user_type', $byToken);
        Yii::$app->session->set('banking.sberbank.auth_referrer', Yii::$app->request->referrer);

        return parent::actionLogin($page);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAuth()
    {
        $authReferrer = Yii::$app->session->get('banking.sberbank.auth_referrer');
        $queryParams = Yii::$app->getRequest()->getQueryParams();

        if ($authReferrer && strpos($authReferrer, 'robot.kub-24') !== false) {
            Yii::$app->getRequest()->setQueryParams(array_merge($queryParams, ['page' => 'taxrobot']));
        }

        return parent::actionAuth();
    }

    /**
     * @return string
     */
    public function actionPayBill($uid)
    {
        $this->layout = '@banking/views/layouts/pay-bill';

        $byToken = Yii::$app->request->get('by_token', 0);
        Yii::$app->session->set('banking.sberbank.user_type', $byToken);

        /* @var Invoice $model */
        $invoice = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->byDeleted(false)
            ->one();

        if ($invoice === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->getModel($this->bankModelClass::SCENARIO_DEFAULT, new Company(['inn' => $invoice->contractor_inn]));
        $model->payBill = $invoice;

        if ($model->getTmpToken()) {
            $model->setScenario($this->bankModelClass::SCENARIO_PAY_BILL);
            if ($model->validate()) {
                return $this->render('@banking/views/all-banks/pay-bill', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        } else {
            Url::remember(Url::current(), $this->bankModelClass::$alias.'ReturnRedirect');
            $model->setAuthState();

            return $this->redirect($model->getAuthUrl());
        }
    }
}
