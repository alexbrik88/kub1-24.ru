<?php

namespace frontend\modules\cash\modules\banking\modules\sberbank;

/**
 * API - ПАО «СберБанк»
 *
 * sberbank module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\sberbank\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
