
<script type="text/javascript">
(function( $ ){
    window.StatementProgress = {};
    $.extend(StatementProgress, {
        progressValue: 0,
        progressTimerId: null,
        statusTimerId: null,
        form: $('#statement-result-form'),
        submitted: false,
        tickPause: 1000,
        statusPause: 5000,
        progressUpdate: function() {
            if (StatementProgress.progressValue < 100) {
                StatementProgress.progressValue++;
                $(".progress-container .progress-bar").css("width", StatementProgress.progressValue + "%");
                $(".progress-container .progress-value").text(StatementProgress.progressValue + "%");
            }
        },
        progressTick: function() {
            StatementProgress.progressUpdate();
            if (StatementProgress.progressValue < 100) {
                StatementProgress.progressTimerId = setTimeout(StatementProgress.progressTick, StatementProgress.tickPause);
            }
            /*else if (!StatementProgress.submitted) {
                StatementProgress.progressStop();
                StatementProgress.submitted = true;
                StatementProgress.form.submit();
            }*/
        },
        progressStart: function() {
            StatementProgress.progressValue = 0;
            StatementProgress.progressTimerId = setTimeout(StatementProgress.progressTick, StatementProgress.tickPause);
            StatementProgress.statusTimerId = setTimeout(StatementProgress.statusRequest, StatementProgress.statusPause);
        },
        progressStop: function() {
            clearTimeout(StatementProgress.statusTimerId);
            clearTimeout(StatementProgress.progressTimerId);
            StatementProgress.progressValue = 0;
        },
        statusRequest: function() {
            $.ajax({
                url: StatementProgress.form.data('status-url'),
                type: 'post',
                data: StatementProgress.form.serialize(),
                success: function(data) {
                    if (data.status == StatementProgress.form.data('status-ready')) {
                        StatementProgress.progressValue = Math.max(StatementProgress.progressValue, 95);
                        StatementProgress.progressUpdate();
                        if (!StatementProgress.submitted) {
                            StatementProgress.progressStop();
                            StatementProgress.submitted = true;
                            StatementProgress.form.submit();
                        }
                    } else if (data.status == StatementProgress.form.data('status-progress')) {
                        StatementProgress.statusTimerId = setTimeout(StatementProgress.statusRequest, StatementProgress.statusPause);
                    } else if (data.redirect) {
                        StatementProgress.progressStop();
                        $.pjax({url: data.redirect, container: "#banking-module-pjax"});
                    }
                }
            });
        }
    });
    StatementProgress.progressStart();
})( jQuery );
</script>
