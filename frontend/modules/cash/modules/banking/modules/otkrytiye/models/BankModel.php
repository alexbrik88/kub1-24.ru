<?php

namespace frontend\modules\cash\modules\banking\modules\otkrytiye\models;

use DateTime;
use Yii;
use common\models\Company;
use common\models\CompanyHelper;
use common\models\company\ApplicationToBank;
use common\models\company\CompanyType;
use common\models\company\CompanyTaxationType;
use common\models\company\CheckingAccountant;
use common\models\company\RegistrationPageType;
use common\models\bank\BankingParams;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\models\BankRegistrationForm;
use common\components\curl;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * BankModel
 *
 * https://ocb-msb.demo.rooxteam.com/login
 * https://ocb-msb.demo.rooxteam.com/webapi-2.1/swagger-ui.html
 *
 * ПАО БАНК "ФК ОТКРЫТИЕ"
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525297';
    const NAME = 'ПАО Банк «ФК Открытие»';
    const NAME_SHORT = 'Открытие';
    const ALIAS = 'otkrytiye';

    public static $alias = 'otkrytiye';
    public static $hasPaymentApi = false;
    public static $hasAutoload = true;
    public static $jwtLiveDays = 45;

    public $needPreRequest = true;
    public $statusReady = 'SUCCESS';
    public $statusProgress = 'IN_PROGRESS';

    // Partial Request
    public $partialRequestPeriods = [];
    public $statusSendedPart = 'SENDED_PART';

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [
        '041117730', // АРХАНГЕЛЬСКИЙ-ПКБ Ф-Л ПАО БАНКА "ФК ОТКРЫТИЕ" — Архангельск, Архангельская область
        '048327780', // Ф-Л СЕВЕРО-КАВКАЗСКИЙ ПАО БАНК "ФК ОТКРЫТИЕ" — Нальчик, Кабардино-Балкарская Республика
        '043002989', // ФИЛИАЛ РАСЧЕТНЫЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Петропавловск-Камчатский, Камчатский край
        '040349732', // КРАСНОДАРСКИЙ ФИЛИАЛ ПАО БАНКА "ФК ОТКРЫТИЕ" — Краснодар, Краснодарский край
        '044525727', // ФИЛИАЛ ПЕТРОКОММЕРЦ ПАО БАНКА "ФК ОТКРЫТИЕ" — Москва
        '044525985', // ПАО БАНК "ФК ОТКРЫТИЕ" — Москва
        '044525297', // ФИЛИАЛ ЦЕНТРАЛЬНЫЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Москва
        '042282881', // Ф-Л ПРИВОЛЖСКИЙ ПАО БАНК "ФК ОТКРЫТИЕ" — Нижний Новгород, Нижегородская область
        '045004839', // НОВОСИБИРСКИЙ ФИЛИАЛ ПАО БАНКА "ФК ОТКРЫТИЕ" — Новосибирск, Новосибирская область
        '045004867', // Ф-Л СИБИРСКИЙ ПАО БАНК "ФК ОТКРЫТИЕ" — Новосибирск, Новосибирская область
        '048717773', // УХТИНСКИЙ-ПКБ ФИЛИАЛ ПАО БАНКА "ФК ОТКРЫТИЕ" — Сыктывкар, Республика Коми
        '046015061', // ФИЛИАЛ ЮЖНЫЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Ростов-на-Дону, Ростовская область
        '046015065', // РОСТОВСКИЙ ФИЛИАЛ ПАО БАНКА "ФК ОТКРЫТИЕ" — Ростов-на-Дону, Ростовская область
        '044030720', // ФИЛИАЛ С-ПЕТЕРБУРГ ПАО БАНКА "ФК ОТКРЫТИЕ" — Санкт-Петербург
        '044030795', // Ф-Л СЕВЕРО-ЗАПАДНЫЙ ПАО БАНК "ФК ОТКРЫТИЕ" — Санкт-Петербург
        '046311913', // ФИЛИАЛ САРАТОВСКИЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Саратов, Саратовская область
        '046577918', // ЕКАТЕРИНБУРГСКИЙ Ф-Л ПАО БАНКА "ФК ОТКРЫТИЕ" — Екатеринбург, Свердловская область
        '047144763', // КОГАЛЫМСКИЙ-ПКБ Ф-Л ПАО БАНКА "ФК ОТКРЫТИЕ" — Тюмень, Тюменская область
        '047162812', // Ф-Л ЗАПАДНО-СИБИРСКИЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Тюмень, Тюменская область
        '040813997', // РЕГИОБАНК-ФИЛИАЛ ПАО БАНКА "ФК ОТКРЫТИЕ" — Хабаровск, Хабаровский край
        '040813704', // Ф-Л ДАЛЬНЕВОСТОЧНЫЙ ПАО БАНКА "ФК ОТКРЫТИЕ" — Хабаровск, Хабаровский край
    ];

    /**
     * @var array
     */
    public static $regPageTypeId = [
        'default' => RegistrationPageType::PAGE_TYPE_BANK_OTKRITIE,
        'invoice' => RegistrationPageType::PAGE_TYPE_BANK_OTKRITIE,
        'taxrobot' => RegistrationPageType::PAGE_TYPE_BANK_OTKRITIE_TAXROBOT,
        'analytics' => RegistrationPageType::PAGE_TYPE_BANK_OTKRITIE_ANALYTICS,
    ];

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_RESULT = 'result';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';
    const SCENARIO_REGISTRATION = 'registration';

    public $auth_redirect;
    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $request_id;
    public $requestStatus;

    protected $apiHost = 'https://ocb-msb.demo.rooxteam.com';
    protected $authPath = '/sso/oauth2/authorize';
    protected $tokenPath = '/sso/oauth2/access_token';
    protected $tokenInfoPath = '/sso/oauth2/tokeninfo';
    protected $accountsPath = '/webapi-2.1/accounts';
    protected $statementPath = '/webapi-2.1/accounts/<account_id>/statement';
    protected $statementRequestPath = '/webapi-2.1/accounts/<account_id>/statement';
    protected $statementStatusPath = '/webapi-2.1/accounts/<account_id>/statement/<request_id>';
    protected $statementResultPath = '/webapi-2.1/accounts/<account_id>/statement/<request_id>/print';
    //protected $paymentOrderPath = '/webapi-2.1/paymentTemplates';
    protected $paymentOrderPath = '/webapi-2.1/payments';
    protected $userDataPath = '/webapi-2.1/clients/@me';
    protected $userInfoPath = '/webapi-2.1/persons/short-info/@me';
    protected $userFioPath = '/webapi-2.1/persons/@me/full-name';
    protected $notificationsPath = '/webapi-2.1/notifications';
    protected $serviceCallbackPath = '/webapi-2.1/openhub/partner-services/<service_name>/callback';

    protected $_returnUrl;
    protected $_accountId = false;
    protected $_userId = false;
    protected $_userInfo;
    protected $_accountInfo;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
            ],
            static::SCENARIO_TOKEN => [
                'code',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_RESULT => [
                'account_id',
                'request_id',
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
            static::SCENARIO_PAY_BILL => [
                'payBill',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'account_id',
                'start_date',
                'end_date',
                'code',
                'request_id',
                'payBill',
                'paymentOrder',
                'currentAccount',
            ], 'required'],
            [['code'], 'string'],
            [['start_date', 'end_date'], 'date'],
            [['end_date'], 'validatePeriod'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->company->id,
                    'bik' => static::$bikList,
                ],
            ],
        ];
    }

    public function validatePeriod($attribute, $params)
    {
        $start = date_create($this->start_date);
        $end = date_create($this->end_date);
        if ($start > $end) {
            $this->addError($attribute, 'Дата начала периода не может быть болшьше даты его окончания.');

            return;
        }
        if (date_diff($start, $end)->format('%a') > 365) {
            $this->addError($attribute, 'Период запроса выписки не должен превышать 1 год.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
        ];
    }

    /**
     * @return string
     */
    public function getBik()
    {
        return static::BIK;
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return 'banking.' . static::BIK . '.' . $this->_company->id ;
    }

    /**
     * @return integer
     */
    public function registrationPageTypeId($queryParams = [])
    {
        $page = ArrayHelper::getValue($queryParams, 'page');

        return  ArrayHelper::getValue(self::$regPageTypeId, $page, self::$regPageTypeId['default']);
    }

    /**
     * @inheritdoc
     */
    public function setJwtToken($JWTToken)
    {
        $this->setParam('JWTToken', $JWTToken);
    }

    /**
     * @inheritdoc
     */
    public function getJwtToken($refresh = false)
    {
        return $this->getParam('JWTToken', $refresh);
    }

    /**
     * @inheritdoc
     */
    public function setAccessToken($access_token)
    {
        $this->setParam('access_token', $access_token);
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        return $this->company->id ? $this->getParam('access_token') : $this->getTmpToken();
    }

    /**
     * @inheritdoc
     */
    public function setAccessTokenExpires($expires_at)
    {
        $this->setParam('access_token_expires', $expires_at);
    }

    /**
     * @inheritdoc
     */
    public function getAccessTokenExpires()
    {
        return $this->getParam('access_token_expires');
    }

    /**
     * @inheritdoc
     */
    public function setRefreshToken($refresh_token)
    {
        $this->setParam('refresh_token', $refresh_token);
    }

    /**
     * @return string|null
     */
    public function getRefreshToken($refresh = false)
    {
        return $this->getParam('refresh_token', $refresh);
    }

    /**
     * @return string
     */
    public function getApiHost()
    {
        return $this->getConfig('apiHost', $this->apiHost);
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->getApiHost() . Url::to([
            $this->authPath,
            'service' => 'external',
            'redirect_uri' => $this->getReturnUrl(),
            'realm' => '/customer',
            'response_type' => 'code',
            'client_id' => $this->getConfig('client_id'),
        ]);
    }

    /**
     * @param string $value
     */
    public function setReturnUrl($value)
    {
        $this->_returnUrl = $value;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        if ($this->_returnUrl === null) {
            $this->_returnUrl = Yii::$app->request->hostInfo . Url::to(['/cash/banking/otkrytiye/default/return']);
        }

        return $this->_returnUrl;
    }

    /**
     * @return string
     */
    public function getTokenUrl()
    {
        return $this->getApiHost() . $this->tokenPath;
    }

    /**
     * @return string
     */
    public function getTokenInfoUrl()
    {
        return $this->getApiHost() . $this->tokenInfoPath;
    }

    /**
     * @return string
     */
    public function getAccountsUrl()
    {
        return $this->getApiHost() . $this->accountsPath;
    }

    /**
     * @return string
     */
    public function statementUrl($accountId)
    {
        return $this->getApiHost() . strtr($this->statementPath, ['<account_id>' => $accountId]);
    }

    /**
     * @return string
     */
    public function statementRequestUrl($accountId)
    {
        return $this->getApiHost() . strtr($this->statementRequestPath, [
            '<account_id>' => $accountId,
        ]);
    }

    /**
     * @return string
     */
    public function statementStatusUrl($accountId, $requestId)
    {
        return $this->getApiHost() . strtr($this->statementStatusPath, [
            '<account_id>' => $accountId,
            '<request_id>' => $requestId,
        ]);
    }

    /**
     * @return string
     */
    public function statementResultUrl($accountId, $requestId)
    {
        return $this->getApiHost() . strtr($this->statementResultPath, [
            '<account_id>' => $accountId,
            '<request_id>' => $requestId,
        ]);
    }

    /**
     * @return string
     */
    public function serviceCallbackUrl($params = [])
    {
        $paramName = [
            'service_name',
            ArrayHelper::getValue($params, 'page'),
        ];
        $paramName = implode('.', array_filter($paramName));

        return $this->getApiHost() . strtr($this->serviceCallbackPath, [
            '<service_name>' => $this->getConfig($paramName, $this->getConfig('service_name')),
        ]);
    }

    /**
     * @return string
     */
    public function userDataUrl()
    {
        return $this->getApiHost() . $this->userDataPath;
    }

    /**
     * @return string
     */
    public function userInfoUrl()
    {
        return $this->getApiHost() . $this->userInfoPath;
    }

    /**
     * @return string
     */
    public function userFioUrl()
    {
        return $this->getApiHost() . $this->userFioPath;
    }

    /**
     * @return string
     */
    public function getPaymentOrderUrl()
    {
        return $this->getApiHost() . $this->paymentOrderPath;
    }

    /**
     * @return string
     */
    public function logMsg($message, $headers)
    {
        $data = [
            'headers' => $headers,
        ];

        return "{$message}\n\n" . var_export($data, true) . "\n\n";
    }

    public function getJwtDaysLeft()
    {
        if (!$this->company) {
            return 0;
        }
        $updatedAt = BankingParams::find()->select('updated_at')->andWhere([
            'bank_alias' => static::ALIAS,
            'company_id' => $this->company->id,
            'param_name' => 'JWTToken',
        ])->scalar();

        if ($updatedAt) {
            $passed = time() - $updatedAt;
            $day = 3600*24;

            return intval($passed/$day);
        } else {
            return 0;
        }
    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        if ($token = $this->getAccessToken()) {
            if (empty($this->userInfoRequest())) {
                if (($jwt = $this->getJwtToken()) && $this->jwtTokenRefresh($jwt)) {
                    return !empty($this->userInfoRequest());
                }
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest($isGuest = false)
    {
        $time = time();
        $requestHeader = [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        ];

        $requestData = [
            'realm' => '/customer',
            'service' => 'external',
            'redirect_uri' => $this->getReturnUrl(),
            'client_id' => $this->getConfig('client_id'),
            'client_secret' => $this->getConfig('client_secret'),
            'grant_type' => 'authorization_code',
            'code' => $this->code,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post($this->getApiHost() . $this->tokenPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = json_decode($response, true);
            if ($result && isset($result['access_token'], $result['expires_in'], $result['refresh_token'])) {
                if ($isGuest) {
                    $result['time'] = $time;
                    Yii::$app->session->set(static::$alias . '.token_data', $result);
                    $this->setTmpToken($result['access_token']);
                } else {
                    return $this->authTokenRequest2($result['access_token']);
                }

                return true;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest2($accessToken)
    {
        $time = time();
        $requestHeader = [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        ];

        $requestData = [
            'realm' => '/customer',
            'service' => 'dispatcher',
            'accessToken' => $accessToken,
            'client_id' => $this->getConfig('client_id_2'),
            'client_secret' => $this->getConfig('client_secret_2'),
            'grant_type' => 'urn:roox:params:oauth:grant-type:m2m',
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post($this->getApiHost() . $this->tokenPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = json_decode($response, true);
            if ($result && isset($result['access_token'], $result['expires_in'], $result['refresh_token'])) {
                $this->setAccessToken($result['access_token']);
                $this->setAccessTokenExpires($time + $result['expires_in']);
                $this->setRefreshToken($result['refresh_token']);
                $this->setJwtToken($result['JWTToken']);

                return true;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for refresh accessToken
     * @return string|null
     */
    public function authTokenRefresh($token)
    {
        $time = time();

        $requestHeader = [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        ];

        $requestData = [
            'realm' => '/customer',
            'client_id' => $this->getConfig('client_id_2'),
            'client_secret' => $this->getConfig('client_secret_2'),
            'grant_type' => 'refresh_token',
            'refresh_token' => $token,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
            CURLINFO_HEADER_OUT => true,
        ])->post($this->getApiHost() . $this->tokenPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if ($result && isset($result['access_token'], $result['expires_in'], $result['refresh_token'])) {
                $this->setAccessToken($result['access_token']);
                $this->setAccessTokenExpires($time + $result['expires_in']);
                $this->setRefreshToken($result['refresh_token']);
                $this->setJwtToken($result['JWTToken']);

                return true;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for refresh accessToken
     * @return string|null
     */
    public function jwtTokenRefresh($jwt)
    {
        $time = time();

        $requestHeader = [
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        ];

        $requestData = [
            'client_id' => $this->getConfig('client_id_2'),
            'client_secret' => $this->getConfig('client_secret_2'),
            'realm' => '/customer',
            'grant_type' => 'urn:roox:params:oauth:grant-type:m2m',
            'service' => 'dispatcher',
            'auto-login-jwt' => $jwt,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
            CURLINFO_HEADER_OUT => true,
        ])->post($this->getApiHost() . $this->tokenPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if ($result && isset($result['access_token'], $result['expires_in'], $result['refresh_token'])) {
                $this->setAccessToken($result['access_token']);
                $this->setAccessTokenExpires($time + $result['expires_in']);
                $this->setRefreshToken($result['refresh_token']);
                $this->setJwtToken($result['JWTToken']);

                return true;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for accessToken info
     * @return string|null
     */
    public function notificationsRequest()
    {
        $requestHeader = [
            "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLINFO_HEADER_OUT => true,
        ])->get($this->getApiHost() . $this->notificationsPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if (isset($result['data'])) {
                return true;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function accountInfo()
    {
        if ($this->_accountInfo === null) {
            $data = $this->userInfoRequest();
            $this->_accountInfo = ArrayHelper::index(ArrayHelper::getValue($data, 'accounts', []), 'number');
        }

        return $this->_accountInfo;
    }

    public function getAccountId($rs = null, $refresh = false)
    {
        if ($this->_accountId === false) {
            $this->_accountId = null;
            if ($rs === null) {
                $rs = $this->currentAccount->rs;
            }
            if (($account = ArrayHelper::getValue($this->accountInfo(), $rs)) !== null) {
                $this->_accountId = ArrayHelper::getValue($account, 'id');
            }
        }

        return $this->_accountId;
    }

    public function statementRequest()
    {
        if ($accountId = $this->getAccountId()) {
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLINFO_HEADER_OUT => true,
            ])->get($this->statementUrl($accountId).'?'.http_build_query([
                'from' => $this->getDateTimeFrom()->format('Y-m-d'),
                'to' => $this->getDateTimeTill()->format('Y-m-d'),
                'format' => 'TXT',
            ]));

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200) {
                $this->_data = tmpfile();
                fwrite($this->_data, $response);

                return true;
            }

            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            $this->errorLog($curl, __METHOD__);
        } else {
            Yii::warning("\n" . __METHOD__ . " no account data\n", 'banking');
        }

        return false;
    }

    /**
     * Send statement Pre-Request
     */
    public function autoloadPreRequest()
    {
        if ($this->isValidToken()) {
            return $this->sendRequest();
        }

        return false;
    }

    /**
     * Создание запроса на выписку, получение ID запроса
     *
     * @return boolean
     */
    public function sendRequest()
    {
        if ($accountId = $this->getAccountId()) {
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLINFO_HEADER_OUT => true,
            ])->post($this->statementRequestUrl($accountId).'?'.http_build_query([
                'format' => 'TXT',
                'from' => $this->getDateTimeFrom()->format('Y-m-d'),
                'to' => $this->getDateTimeTill()->format('Y-m-d'),
            ]));

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200 || $curl->responseCode == 201) {
                $result = json_decode($response, true);
                if ($result && isset($result['data']['statementId'])) {
                    $this->request_id = $result['data']['statementId'];

                    return true;
                }
            }

            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            $this->errorLog($curl, __METHOD__);
        } else {
            Yii::warning("\n" . __METHOD__ . " no account data\n", 'banking');
        }

        return false;
    }

    /**
     * Cтатус запроса на выписку
     *
     * @return boolean
     */
    public function checkRequestStatus()
    {
        if ($accountId = $this->getAccountId()) {
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLINFO_HEADER_OUT => true,
            ])->get($this->statementStatusUrl($accountId, $this->request_id));

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200 || $curl->responseCode == 201) {
                $result = json_decode($response, true);
                if ($result && isset($result['data']['status'])) {
                    $this->requestStatus = $result['data']['status'];

                    return true;
                }
            }

            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            $this->errorLog($curl, __METHOD__);
        } else {
            Yii::warning("\n" . __METHOD__ . " no account data\n", 'banking');
        }

        return false;
    }

    /**
     * Получение выписки
     *
     * @return boolean
     */
    public function takeResult()
    {
        if ($accountId = $this->getAccountId()) {
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLINFO_HEADER_OUT => true,
            ])->get($this->statementResultUrl($accountId, $this->request_id));

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200) {

                $this->saveTmpData($response);

                return true;
            }

            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            $this->errorLog($curl, __METHOD__);
        } else {
            Yii::warning(__METHOD__ . " no account data", 'banking');
        }

        return false;
    }

    public function sendPaymentOrder()
    {
        if ($accountId = $this->getAccountId()) {
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
                "Content-Type: application/json",
            ];

            $requestData = [
                'accountId' => $accountId,
                'amount' => bcdiv($this->paymentOrder->sum, 100, 2),
                'createDate' => date('Y-m-d'),
                'counterparty' => $this->paymentOrder->contractor_name,
                'counterpartyAccount' => $this->paymentOrder->contractor_current_account,
                'counterpartyBankName' => $this->paymentOrder->contractor_bank_name,
                'counterpartyBic' => $this->paymentOrder->contractor_bik,
                'counterpartyCorrAccount' => $this->paymentOrder->contractor_corresponding_account,
                'counterpartyInn' => $this->paymentOrder->contractor_inn,
                'counterpartyKpp' => $this->paymentOrder->contractor_kpp,
                'currency' => 'RUB',
                'paymentPriority' => (string) ($this->paymentOrder->ranking_of_payment ? : 5),
                'paymentNumber' => (string) $this->paymentOrder->document_number,
                'details' => (string) $this->paymentOrder->purpose_of_payment,
                'kbk' => (string) ($this->paymentOrder->kbk ? : 0),
                //'period' => (string) ($this->paymentOrder->tax_period_code ? : 0),
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POSTFIELDS => json_encode($requestData),
                CURLINFO_HEADER_OUT => true,
            ])->post($this->getPaymentOrderUrl());

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200 ||$curl->responseCode == 201) {
                $result = Json::decode($response);

                if (isset($result['data']['patternId'])) {
                    $this->paymentOrder->updateAttributes([
                        'payment_order_status_id' => PaymentOrderStatus::STATUS_SENT_TO_BANK,
                        'payment_order_status_updated_at' => time(),
                    ]);
                    if (Yii::$app->id == 'app-frontend') {
                        $bankUrl = $this->bankPartner ? $this->bankPartner->url : null;
                        $msg = 'Платежное поручение успешно отправлено в клиент банк и находится в разделе "Черновики". ';
                        $msg .= ($bankUrl ? Html::a('Подтвердите платежку', $bankUrl, ['target' => '_blank']) : 'Подтвердите платежку');
                        $msg .= ', чтобы оплатить по счету.';
                        Yii::$app->session->setFlash('success', $msg);
                    }

                    return true;
                } elseif (isset($result['errorText'])) {
                    $this->_errorMessage = $result['errorText'];
                }
            }

            $this->errorLog($curl, __METHOD__);
        }
        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        return false;
    }

    public function userDataRequest()
    {
        $requestHeader = [
            "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLINFO_HEADER_OUT => true,
        ])->get($this->userDataUrl());

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if (isset($result['data'])) {
                $this->_data = $result['data'];

                return true;
            } elseif (isset($result['error'])) {
                $this->_errorMessage = $result['error']['message'];
            }
        }

        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function userInfoRequest()
    {
        if ($this->_userInfo === null) {
            $this->_userInfo = [];
            $error = false;
            $requestHeader = [
                "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLINFO_HEADER_OUT => true,
            ])->get($this->userInfoUrl());

            $this->debugLog($curl, __METHOD__);

            if ($curl->responseCode == 200) {
                $result = Json::decode($response);
                if (isset($result['data'])) {
                    $this->_userInfo = $result['data'];
                } elseif (isset($result['error'])) {
                    $error = true;
                    $this->_errorMessage = $result['error']['message'];
                }
            } else {
                $error = true;
            }

            if ($error) {
                $this->errorLog($curl, __METHOD__);
            }
        }

        return $this->_userInfo;
    }

    public function userFioRequest()
    {
        $requestHeader = [
            "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLINFO_HEADER_OUT => true,
        ])->get($this->userFioUrl());

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if (isset($result['data'])) {
                return $result['data'];
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function tokenInfoRequest()
    {
        $requestHeader = [
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLINFO_HEADER_OUT => true,
        ])->post($this->getTokenInfoUrl().'?'.http_build_query([
            'access_token' => $this->getAccessToken(),
        ]));

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if (isset($result['data'])) {
                return $result['data'];
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * serviceCallBack
     * @param array $data
     * @return boolean
     */
    public function serviceCallback($data, $params = [])
    {
        $requestHeader = [
            "Authorization: Bearer sso_1.0_{$this->getAccessToken()}",
            "Content-Type: application/json",
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($data),
        ])->post($this->serviceCallbackUrl($params));

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $result = Json::decode($response);
            if (isset($result['data'])) {
                return true;
            }
        }
        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Prepare vidimus from XML
     */
    public function vidimusPrepare()
    {
        $builder = new VidimusBuilder(null, $this->company);
        if ($tmpFile = $this->getTmpDataHandler()) {
            $builder->setFileDescriptor($tmpFile);
        }

        $vidimuses = $builder->build();

        $statistic = $builder->uploadData();

        return [$vidimuses, $statistic];
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        $this->clearTmpData();

        if ($this->isValidToken() && $this->takeResult()) {
            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }

    /**
     * @return array
     */
    public function getRegistrationData()
    {
        if ($data = $this->userInfoRequest()) {
            $is_chief = ArrayHelper::getValue($data, 'eio', false);
            $organization = ArrayHelper::getValue($data, 'organization', []);
            $inn = ArrayHelper::getValue($organization, 'inn');
            $kpp = ArrayHelper::getValue($organization, 'kpp');
            $address = ArrayHelper::getValue($organization, 'address');
            $companyType = ArrayHelper::getValue($organization, 'companyType');

            $userFio = $this->userFioRequest();
            $lastName = ArrayHelper::getValue($userFio, 'lastName');
            $firstName = ArrayHelper::getValue($userFio, 'firstName');
            $middleName = ArrayHelper::getValue($userFio, 'middleName');

            $typeId = $companyType ? (CompanyType::find()->select('id')->where([
                'name_short' => $companyType,
            ])->scalar() ? : null) : null;

            if ($typeId === null) {
                switch (strlen((string) $inn)) {
                    case 10:
                        $typeId = CompanyType::TYPE_OOO;
                        break;
                    case 12:
                        $typeId = CompanyType::TYPE_IP;
                        break;

                    default:
                        $typeId = CompanyType::TYPE_EMPTY;
                        break;
                }
            }

            $isIp = $typeId == CompanyType::TYPE_IP;
            $companyData = array_merge([
                'company_type_id' => $typeId,
                'inn' => $inn,
                'kpp' => $kpp,
                'address_legal' => $address,
                'address_actual' => $address,
                'chief_lastname' => $is_chief ? $lastName : null,
                'chief_firstname' => $is_chief ? $firstName : null,
                'chief_patronymic' => $is_chief ? $middleName : null,
                'ip_lastname' => $is_chief && $isIp ? $lastName : null,
                'ip_firstname' => $is_chief && $isIp ? $firstName : null,
                'ip_patronymic' => $is_chief && $isIp ? $middleName : null,
                'nds' => $isIp ? null : \common\models\NdsOsno::WITH_NDS,
            ], (array) CompanyHelper::getCompanyData($inn, $kpp));
            $allAccounts = [];
            $hasMain = false;
            $accountArray = ArrayHelper::getValue($data, 'accounts', []);
            foreach ($accountArray as $account) {
                if ($account['state'] == 'WORK' && $account['currency'] == 'RUB') {
                    $allAccounts[] = [
                        'bik' => ArrayHelper::getValue($organization, 'bic', static::BIK),
                        'rs' => $account['number'],
                        'type' => $hasMain ? CheckingAccountant::TYPE_ADDITIONAL : CheckingAccountant::TYPE_MAIN,
                    ];
                    $hasMain = true;
                    break;
                }
            }

            if (!empty($allAccounts)) {
                if ($isIp) {
                    $taxationType = ['usn' => 1];
                } else {
                    $taxationType = ['osno' => 1];
                }
                $email = '';
                $phone = '';
                $contactArray = (array) ArrayHelper::getValue($data, 'contacts');
                foreach ($contactArray as $contact) {
                    if ($contact['contactSubType'] == 'MAIN') {
                        switch ($contact['contactType']) {
                            case 'EMAIL':
                                $email = $contact['address'];
                                break;
                            case 'PHONE':
                                $phone = $contact['address'];
                                break;
                        }
                    }
                }
                $companyData['email'] = $email;
                $companyData['phone'] = $phone;

                return [
                    'Company' => $companyData,
                    'CompanyTaxationType' => $taxationType,
                    'CheckingAccountant' => $allAccounts[0],
                    'Employee' => [
                        'employee_role_id' => $is_chief ? EmployeeRole::ROLE_CHIEF :  EmployeeRole::ROLE_ACCOUNTANT,
                        'lastname' => $lastName,
                        'firstname' => $firstName,
                        'patronymic' => $middleName,
                        'position' => ArrayHelper::getValue($data, 'managingPost'),
                        'email' => $email,
                        'phone' => $phone,
                    ],
                    'BankUser' => [
                        'bank_alias' => static::$alias,
                        'user_uid' => (string) ArrayHelper::getValue($data, 'personId'),
                        'company_uid' => (string) ArrayHelper::getValue($organization, 'organizationId'),
                        'inn' => (string) $inn,
                        'kpp' => (string) $kpp,
                        'is_chief' => (int) $is_chief,
                    ],
                    'accounts' => $allAccounts,
                ];
            } else {
                Yii::warning(static::class . "::getRegistrationData()\nno checkingAccountant", 'banking');
            }
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    public function updateCompanyAccounts(BankRegistrationForm $form = null)
    {
        if ($form === null) {
            return false;
        }
        $accounts = $form->getAccountsData();
        $company = $form->getCompany();

        if (!$company || !$accounts)
            return false;

        foreach ($accounts as $account) {
            if (!CheckingAccountant::find()->where([
                'company_id' => $company->id,
                'rs' => $account['rs'],
                'bik' => $account['bik']
            ])->exists()) {
                $newAccount = new CheckingAccountant();
                $newAccount->company_id = $company->id;
                $newAccount->bik = $account['bik'];
                $newAccount->rs = $account['rs'];
                $newAccount->type = CheckingAccountant::TYPE_ADDITIONAL;

                if (!$newAccount->save()) {
                    throw new Exception('Не удалось сохранить р/с. Попробуйте пожалуйста еще раз.');
                }
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterRegistration(BankRegistrationForm $form = null, $params = [])
    {
        $isSaved = $form->getIsSaved();
        if ($isSaved) {
            $tokenData = Yii::$app->session->remove(static::$alias . '.token_data');
            $accessToken = ArrayHelper::getValue($tokenData, 'access_token');
            if ($accessToken && !$form->company->isNewRecord) {
                $this->_company = $form->company;
                $this->authTokenRequest2($accessToken);
            }
        }

        if (ArrayHelper::getValue($params, 'action') != 'login' || !$isSaved) {
            if ($isSaved) {
                $data = [
                    "state" => "ENABLE",
                ];
            } else {
                $data = [
                    "msg" => ArrayHelper::getValue($params, 'failMessage', 'Во время регистрации произошла ошибка.'),
                    "state" => "ERROR",
                ];
            }

            $this->serviceCallback($data, $params);
        }
    }

    public static function bankingTokenRefresh($company_id = null)
    {
        /**
         * Deleting old data
         */
        BankingParams::deleteAll([
            'and',
            ['bank_alias' => static::ALIAS],
            ['<', 'updated_at', date_create('-30 days')->getTimestamp()],
        ]);

        $companyIds = $company_id ? [$company_id] : BankingParams::find()->select('company_id')->andWhere([
            'bank_alias' => static::ALIAS,
            'param_name' => 'JWTToken',
        ])->andWhere([
            '<',
            'updated_at',
            date_create('-20 days')->getTimestamp(),
        ])->orderBy([
            'updated_at' => SORT_ASC,
        ])->column();

        foreach ($companyIds as $id) {
            $model = new BankModel(new Company(['id' => $id]));
            if ($jwt = $model->getJwtToken(true)) {
                $model->jwtTokenRefresh($jwt);
            }
        }
    }

    public function setPartialRequestMode()
    {
        $start = $current = date_create_from_format('d.m.Y', $this->start_date);
        $end = date_create_from_format('d.m.Y', $this->end_date);
        $this->end_date = $start->format('t.m.Y');

        $i = 1;
        while (true) {
            $current->modify("+1 month");
            if ($current->format('Ym') > $end->format('Ym')) {
                break;
            }
            elseif ($current->format('Ym') == $end->format('Ym')) {
                $this->partialRequestPeriods[] = [
                    ['start_date' => $end->format('01.m.Y'),
                     'end_date' => $end->format('d.m.Y')]
                ];
                break;
            } else {
                $this->partialRequestPeriods[] = [
                    ['start_date' => $current->format('01.m.Y'),
                     'end_date' => $current->format('t.m.Y')]
                ];
            }
            // 1 year limit
            if (++$i > 12) break;
        }
    }

    private function getTmpDataHandler() {
        $filename = sys_get_temp_dir() . "/otkrytie_c{$this->_company->id}a{$this->account_id}.tmp";
        return @fopen($filename, 'r');
    }

    private function saveTmpData(string $data) {
        $filename = sys_get_temp_dir() . "/otkrytie_c{$this->_company->id}a{$this->account_id}.tmp";
        if (!is_file($filename)) {
            touch($filename);
        }
        $file = @fopen($filename, "a");
        @fwrite($file, "\n");
        return @fwrite($file, $data);
    }

    public function clearTmpData() {
        $filename = sys_get_temp_dir() . "/otkrytie_c{$this->_company->id}a{$this->account_id}.tmp";
        $file = @fopen($filename, "w");
        return @ftruncate($file, 0);
    }
}
