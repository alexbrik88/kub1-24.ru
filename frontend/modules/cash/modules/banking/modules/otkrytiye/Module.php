<?php

namespace frontend\modules\cash\modules\banking\modules\otkrytiye;

/**
 * API - ПАО Банк «ФК Открытие»
 *
 * otkrytiye module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\otkrytiye\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
