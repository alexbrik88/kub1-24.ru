<?php

namespace frontend\modules\cash\modules\banking\modules\otkrytiye\controllers;

use common\models\Company;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\BankingModulesBaseController;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\models\BankRegistrationForm;
use frontend\modules\cash\modules\banking\modules\otkrytiye\models\BankModel;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `otkrytiye` module
 *
 * ПАО Банк «ФК Открытие»
 */
class DefaultController extends BankingModulesBaseController
{
    /**
     * @var frontend\modules\cash\modules\banking\models\AbstractBankModel
     */
    public $bankModelClass = BankModel::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'request',
                            'continue-request',
                            'status',
                            'result',
                            'payment',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => [
                            'set-autoload',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'return',
                            'pay-bill',
                            'registration',
                            'login',
                            'auth',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($account_id = null, $redirect_url = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;
        $model->account_id = $account_id;
        $model->checkStartDate();

        if ($model->isValidToken()) {
            $model->scenario = BankModel::SCENARIO_REQUEST;

            return $this->render('request', ['model' => $model]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Url::remember($redirect_url ?? Url::current(), BankModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getAuthUrl());
        }

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionReturn()
    {
        $guestRoutes = [
            BankModel::$alias . '/default/pay-bill',
            BankModel::$alias . '/default/auth',
        ];
        $pattern = implode('|', $guestRoutes);
        $redirectUrl = Url::previous(BankModel::$alias.'ReturnRedirect');
        $isGuest = (boolean) preg_match("~($pattern)~", $redirectUrl);

        if (Yii::$app->user->isGuest && !$isGuest) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->getModel(BankModel::SCENARIO_TOKEN, $isGuest ? new Company : null);

        if ($model->load(Yii::$app->request->get(), '') && $model->validate() && $model->authTokenRequest($isGuest)) {
            Yii::$app->session->setFlash('success', "Вы успешно авторизованы.");
            if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) {
                \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 98, true);
            }
        } else {
            Yii::$app->session->setFlash('error', "Во время авторизации произошла ошибка.");
        }

        return $this->redirect($redirectUrl ? : ['index']);
    }

    /**
     * @return string
     */
    public function actionRequest($account_id = null)
    {
        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);
        $model->account_id = $account_id;

        if ($model->isValidToken()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->setScenario(BankModel::SCENARIO_RESULT);

                $model->clearTmpData();
                $model->setPartialRequestMode();

                if ($model->sendRequest()) {
                    return $this->render('result', ['model' => $model]);
                } else {
                    return $this->render('request', ['model' => $model]);
                }
            } else {
                return $this->render('request', ['model' => $model]);
            }
        } else {
            return $this->render('index', ['model' => $this->getModel(BankModel::SCENARIO_DEFAULT)]);
        }
    }

    /**
     * @param null $account_id
     * @return array|string
     */
    public function actionContinueRequest($account_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);
        $model->account_id = $account_id;

        if ($model->isValidToken()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->sendRequest()) {
                return [
                    'status' => $model->statusSendedPart,
                    'request_id' => $model->request_id
                ];
            } else {
                return [
                    'status' => null,
                    'redirect' => Url::to([
                        'index',
                        'account_id' => $model->account_id,
                        'p' => Yii::$app->request->get('p'),
                    ]),
                ];
            }
        } else {
            return $this->render('index', ['model' => $this->getModel(BankModel::SCENARIO_DEFAULT)]);
        }
    }

    /**
     * @param int $account_id
     * @return array
     */
    public function actionStatus($account_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var BankModel $model */
        $model = $this->getModel(BankModel::SCENARIO_RESULT);
        $model->account_id = $account_id;

        if ($model->isValidToken()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->checkRequestStatus()) {
                if ($model->requestStatus != $model->statusReady || $model->takeResult()) {
                    return [
                        'status' => $model->requestStatus,
                    ];
                }
            }
        }

        return [
            'redirect' => Url::to([
                'index',
                'account_id' => $model->account_id,
                'p' => Yii::$app->request->get('p'),
            ]),
        ];
    }

    /**
     * @return string
     */
    public function actionResult($account_id = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_RESULT);
        $model->account_id = $account_id;

        if ($model->isValidToken()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() /* && $model->takeResult()*/) {
                return $this->renderContent($model->renderStatement());
            } else {
                return $this->render('result', ['model' => $model]);
            }
        }

        return $this->render('index', ['model' => $this->getModel(BankModel::SCENARIO_DEFAULT)]);
    }
}
