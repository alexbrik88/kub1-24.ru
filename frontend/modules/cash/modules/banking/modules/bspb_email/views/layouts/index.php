<?php

use backend\models\Bank;
use common\components\ImageHelper;
use common\components\banking\AbstractService;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\banking\Module;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this  yii\web\View */

if ($bankModel = ArrayHelper::getValue($this->params, 'bankingModel')) {
    $bankingUrl = Url::to([
        "/cash/banking/{$bankModel::$alias}/default/index",
        'account_id' => $bankModel->account_id,
        'p' => Yii::$app->request->get('p'),
    ]);
} else {
    $bankingAccountsArray = Yii::$app->user->identity->company->bankingAccountants;
    $bankingAccount = $bankingAccountsArray ? $bankingAccountsArray[0] : null;
    $bankingUrl = $bankingAccount && ($alias = Banking::aliasByBik($bankingAccount->bik)) ? Url::to([
        "/cash/banking/{$alias}/default/index",
        'account_id' => $bankingAccount->id,
        'p' => Yii::$app->request->get('p'),
    ]) : Url::to([
        '/cash/banking/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);
}

$this->beginContent('@frontend/modules/cash/modules/banking/views/layouts/main.php');
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Запрос выписки из банка</h1>
</div>
<div class="modal-body">

    <div class="statement-service-content" style="position: relative; min-height: 110px;">
        <?php if ($bankModel && ($bankPartner = $bankModel->bankPartner)) : ?>
            <div style="margin: 0 -5px 10px;">
                <div class="cont-img_bank-logo" style="width: 150px; float: left;">
                    <?= ImageHelper::getThumb($bankPartner->getUploadDirectory() . $bankPartner->logo_link, [150, 90], [
                        'class' => 'bank-logo',
                        'style' => 'padding-right: 15px; cursor: pointer;',
                    ]); ?>
                </div>
                <?php $bankName = Html::encode($bankModel->bank ? $bankModel->bank->name : ''); ?>
                <div id="statement-bank-info" data-name="<?= $bankName ?>" style="margin-left: 150px; padding: 5px;">
                    <?php if ($bankModel->showSecureText) : ?>
                        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee;">
                            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
                            - надежный протокол для передачи конфиденциальной банковской информации
                            и соблудаются требования международного стандарта PCI DSS по хранению и передаче
                            конфиденциальной информации в банковской сфере.
                        </div>
                    <?php else : ?>
                        <div style="text-align: right;">
                            <?php if ($bankModel->showBankBatton) : ?>
                                <p>
                                    <a href="<?= $bankingUrl ?>" class="btn yellow no-padding banking-module-link">
                                        <i class="fa fa-bank m-r-sm"></i> Загрузить из банка
                                    </a>
                                </p>
                            <?php endif; ?>
                            <p style="font-weight: bold;">
                                <?= $bankName ?><br>
                                <?= $bankModel->currentAccount ? 'р/с ' . $bankModel->currentAccount->rs : ''; ?>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <hr/>
        <?php endif ?>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>
    </div>
</div>

<?php if (Yii::$app->request->get('start_load_urgently')): ?>
    <script>
        $(document).ready(function() {
            $('#statement-request-form').submit();
        });
    </script>
<?php endif; ?>

<?php $this->endContent(); ?>
