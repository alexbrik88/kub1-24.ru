<?php

namespace frontend\modules\cash\modules\banking\modules\bspb_email\models;

use Yii;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\Company;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\models\BankingEmail;
use frontend\modules\cash\modules\banking\models\BankingEmailFile;
use frontend\modules\cash\modules\banking\models\BankingEmailHelper;
use php_rutils\RUtils;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * BankModel
 *
 * ПАО «Банк «Санкт-Петербург»
 *
 */
class BankModel extends AbstractBankModel
{
    const REFRESH_LIST_SECONDS = 60;

    const BIK = '044030790';
    const NAME = 'ПАО «Банк «Санкт-Петербург»';
    const NAME_SHORT = 'Банк Санкт-Петербург';
    const ALIAS = 'bspb_email';

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_CONFIRM_EMAIL = 'email';

    public $email;
    public $account_id;
    public $is_confirmed;

    public static $hasStatementsEmail = true;
    public static $alias = self::ALIAS;
    public static $storageErrorCode = '';

    /**
     * All branches of the bank
     * @inheritdoc
     */
    public static $bikList = [
        '044030790', // ПАО "БАНК "САНКТ-ПЕТЕРБУРГ"
        '044525142', // Ф-Л ПАО "БАНК "САНКТ-ПЕТЕРБУРГ" В Г. МОСКВЕ
        '042748877', // Ф-Л "ЕВРОПЕЙСКИЙ" ПАО "БАНК "САНКТ-ПЕТЕРБУРГ"
        '045004888', // ф-л "Невский" ПАО "Банк "Санкт-Петербург"
        '044106871', // Киришский ф-л ПАО "Банк "Санкт-Петербург"
        '044106817', // ПРИОЗЕРСКИЙ Ф-Л ПАО "БАНК "САНКТ-ПЕТЕРБУРГ"
    ];

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => [
                'account_id'
            ],
            static::SCENARIO_CONFIRM_EMAIL => [
                'account_id',
                'email',
                'is_confirmed'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'is_confirmed', 'email'], 'required', 'message' => 'Необходимо заполнить'],
            [['is_confirmed'], 'compare', 'compareValue' => 1, 'message' => 'Подтвердите, что указали e-mail'],
            [['email'], 'email'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->_company->id,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Ваш e-mail для выписки из Банка Санкт-Петербург',
            'account_id' => 'Номер счета',
            'is_confirmed' => 'Подтверждаю получение выписки'
        ];
    }

    public function generateEmail()
    {
        $email = '';

        if ($this->_company->id) {
            $nextMailboxNumber = BankingEmail::find()->where(['company_id' => $this->_company->id])->count() + 1;
            $email .= (YII_ENV_DEV) ? 'test-bspb-' : 'bspb-';
            $email .= str_pad($this->_company->id, 5, "0", STR_PAD_LEFT);
            $email .= ($nextMailboxNumber > 1 ? "-{$nextMailboxNumber}" : "");
            $email .= '@';
            $email .= ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_domain');
        }

        return $email;
    }

    public function createMailbox()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $emailModel = new BankingEmail();
        $emailModel->company_id = $this->_company->id;
        $emailModel->checking_accountant_id = $this->account_id;
        $emailModel->email = $this->generateEmail();
        $emailModel->password = BankingEmailHelper::generatePassword();

        if ($emailModel->save()) {
            if (BankingEmailHelper::createMailbox($emailModel)) {
                $transaction->commit();

                return true;
            } else {
                $errorMessage = 'Не удалось включить Email.' . '<br/>Ошибка: ' . BankingEmailHelper::$lastErrorCode;
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } else {

            $errorMessage = 'Не удалось сохранить Email.';
        }

        Yii::$app->session->setFlash('error', $errorMessage);

        $transaction->rollBack();
        return false;
    }

    public function dropMailbox(BankingEmail $emailModel)
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($emailModel->delete()) {
            if (BankingEmailHelper::dropMailbox($emailModel)) {

                $transaction->commit();
                return true;

            } else {
                $errorMessage = 'Не удалось отключить Email.' . '<br/>Ошибка: ' . BankingEmailHelper::$lastErrorCode;
                Yii::$app->session->setFlash('error', $errorMessage);
            }
        } else {

            $errorMessage = 'Не удалось удалить Email.';
        }

        Yii::$app->session->setFlash('error', $errorMessage);

        $transaction->rollBack();
        return false;
    }

    public function refreshStatementsList(CheckingAccountant $account)
    {
        if (Yii::$app->session->get('bspb_refresh_statements_list') + self::REFRESH_LIST_SECONDS < time()) {
            try {
                $newFilesCount = $this->_refreshStatementsList($account);
            } catch (\Exception $e) {
                $error = 'Не удалось загрузить выписки:<br/>' . $e->getMessage();
                Yii::$app->session->setFlash('error', $error);
                $newFilesCount = 0;
            }
            Yii::$app->session->set('bspb_refresh_statements_list', time());
            
            return $newFilesCount;
        }
        
        return null;
    }

    public function _refreshStatementsList(CheckingAccountant $checkingAccount)
    {
        set_time_limit(90);

        $bankingEmail = $checkingAccount->bankingEmail;
        $newFiles = $this->_getFilesFromMailbox($bankingEmail);
        if ($newFiles === false)
            throw new Exception($this->getEmailError());

        if (!$this->_saveFiles($bankingEmail, $newFiles))
            throw new Exception($this->getStorageError());
        
        return count($newFiles);
    }

    private function _getFilesFromMailbox(BankingEmail $model)
    {
        $isConnected = BankingEmailHelper::connect($model);
        if ($isConnected)
        {
            return BankingEmailHelper::getFiles();
        }

        return false;
    }
    
    private function _saveFiles(BankingEmail $bankingEmail, array $files)
    {
        $uploadsDir = BankingEmailFile::getUploadsDir();
        if (!is_dir($uploadsDir))
            FileHelper::createDirectory($uploadsDir);

        $now = time() - count($files);

        foreach ($files as $file) {

            $storageFileName = BankingEmailFile::generateFileName($this->account_id, $now);
            $storageFilePath = $uploadsDir . DIRECTORY_SEPARATOR . $storageFileName;

            if (rename($file['tmp_name'], $storageFilePath)) {

                $_info = self::getStatementFileInfo($storageFilePath);

                $bankingFile = new BankingEmailFile([
                    'banking_email_id' => $bankingEmail->id,
                    'date' => date('Y-m-d'),
                    'file_name' => $storageFileName,
                    'period_from' => $_info['period_from'],
                    'period_to' => $_info['period_to'],
                    'statements_count' => $_info['statements_count']
                ]);

                if ($bankingFile->save()) {

                    BankingEmailHelper::markMessageAsDeleted($file['msg_number']);

                    $now++;
                    continue;
                }

                self::$storageErrorCode = 'Не удалось сохранить файл (БД)';
                break;
            }

            self::$storageErrorCode = 'Не удалось сохранить файл (HDD)';
            break;
        }

        BankingEmailHelper::expungeMessages();
        BankingEmailHelper::closeConnection();

        return !self::$storageErrorCode;
    }

    public static function getStatementFileInfo($tmpFile)
    {
        $periodFrom = $periodTo = null;
        $statementsCount = 0;

        if (is_file($tmpFile)) {

           if ($_fileDescriptor = fopen($tmpFile, 'r')) {

               while (($line = fgets($_fileDescriptor)) !== false) {
                   $line = trim(mb_convert_encoding($line, 'UTF-8', ['CP1251', 'UTF-8']));

                   // Обработка строки
                   if ($periodFrom === null || $periodTo === null) {
                       if (mb_substr($line, 0, 10) == 'ДатаНачала') {
                           $linePart = explode('=', $line);
                           $periodFrom = (count($linePart) > 0) ? $linePart[1] : false;
                       } elseif (mb_substr($line, 0, 9) == 'ДатаКонца') {
                           $linePart = explode('=', $line);
                           $periodTo = (count($linePart) > 0) ? $linePart[1] : false;
                       }
                   } elseif (mb_substr($line, 0, 14) == 'СекцияДокумент') {
                       $statementsCount++;
                   }
               }

               fclose($_fileDescriptor);
           }
        }

        return [
            'period_from' => ($periodFrom) ? DateHelper::format($periodFrom, 'Y-m-d', 'd.m.Y') : null,
            'period_to' => $periodTo ? DateHelper::format($periodTo, 'Y-m-d', 'd.m.Y') : null,
            'statements_count' => $statementsCount
        ];
    }

    public function getEmailError()
    {
        return "Ошибка: " . BankingEmailHelper::$lastErrorCode;
    }

    public function getStorageError()
    {
        return 'Ошибка: ' . self::$storageErrorCode;
    }

    //// Frontend Helpers ////

    public function getVidimusCancelButton()
    {
        return \yii\helpers\Html::a('Отменить', [
            '/cash/banking/bspb_email/default/statements-list',
            'account_id' => $this->account_id,
            'p' => Yii::$app->request->get('p')
        ], [
            'class' => 'btn btn-primary banking-module-link banking-cancel pull-right',
        ]);
    }

    public static function isEmailActivated(CheckingAccountant $account)
    {
        return BankingEmail::find()
            ->where(['checking_accountant_id' => $account->id])
            ->andWhere(['company_id' => $account->company_id])
            ->exists();
    }

    /**
     * @param CheckingAccountant $account
     * @param $route
     * @return string
     * @throws \Exception
     */
    public static function getStatementsListModalLink(CheckingAccountant $account, $route)
    {
        $bankingEmail = $account->bankingEmail;

        if ($bankingEmail->isAutoloadMode) {

            $count = Yii::$app->session->remove('bspb_uploaded_files_count', 0);

            if ($count) {
                $label = 'Загружено: ' . RUtils::numeral()->getPlural($count ? : 0, ['выписка', 'выписки', 'выписок']);
            } else {

                $lastStatementUpload = CashBankStatementUpload::find()->where([
                    'company_id' => $account->company_id,
                    'rs' => $account->rs,
                    'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
                ])->orderBy(['created_at' => SORT_DESC])->one();

                if ($lastStatementUpload) {
                    $label = Html::tag('span', 'Выписка загружена ' . date_timestamp_set(new \DateTime, $lastStatementUpload->created_at)->format('d.m.Y в H:i'), [
                        'class' => 'tooltip3',
                        'title' => 'Последняя ' . ($lastStatementUpload->source == CashBankStatementUpload::SOURCE_BANK ? 'выгрузка' : 'автовыгрузка') . ' выписки из Вашего клиент-банка',
                    ]);
                } else {
                    $label = 'Новых выписок нет';
                }
            }

        } else {

            $count = $bankingEmail->getBankingEmailFiles()->andWhere(['is_uploaded' => false])->count();

            if ($count) {
                $label = 'Есть выписки из банка';
                $label .= Html::tag('span', $count, ['class' => 'badge badge-blue badge-line']);
            } else {
                $label = 'Новых выписок нет';
            }
        }

        return Html::a($label, [
            '/cash/banking/'.self::ALIAS.'/default/statements-list',
            'account_id' => $account->id,
            'p' => Banking::routeEncode($route),
        ], [
            'class' => 'banking-module-open-link' . ($count ? '' : ' grey-link'),
            'style' => '
                display: block;
                position: relative;
                padding-right: 15px;            
            '
        ]);
    }

    /**
     * @param Company $company
     * @throws \Exception
     */
    public static function checkNewStatements(Company $company)
    {
        $bankEmailAccounts = $company->getCheckingAccountants()
            ->where(['bik' => self::$bikList])
            ->all();

        if ($bankEmailAccounts) {
            /** @var CheckingAccountant $account */
            foreach ($bankEmailAccounts as $account) {
                if (self::isEmailActivated($account)) {
                    $self = new self($company);
                    $self->account_id = $account->id;
                    $self->refreshStatementsList($account);
                    if ($account->bankingEmail->isAutoloadMode) {
                        $self->autoUploadStatements($account);
                    }
                }
            }
        }
    }

    /**
     * @param CheckingAccountant $checkingAccount
     * @return bool|void
     * @throws \Exception
     */
    public function autoUploadStatements(CheckingAccountant $checkingAccount)
    {
        $this->scenario = self::SCENARIO_AUTOLOAD;

        $bankingEmail = $checkingAccount->bankingEmail;
        $uploadsDir = BankingEmailFile::getUploadsDir();
        $fileModels = BankingEmailFile::find()->where([
            'banking_email_id' => $bankingEmail->id,
            'is_uploaded' => false
        ])
        ->all();

        $filesArr = [];
        foreach ($fileModels as $fileModel) {
            $file = $uploadsDir . DIRECTORY_SEPARATOR . $fileModel->file_name;
            if (is_file($file)) {
                $filesArr[$fileModel->id] = $file;
            }
        }

        if (!$filesArr) {
            // todo: log error 'Файл не найден'
            return false;
        }

        $_summaryFileName = $uploadsDir . DIRECTORY_SEPARATOR . 'tmp_'.$checkingAccount->id.'.txt';

        BankingEmailFile::createSummaryTmpFile($_summaryFileName, $filesArr);

        $file = new UploadedFile;
        $file->tempName = $_summaryFileName;

        // std vidimus
        $company = $this->_company;
        $vidimusBuilder = new VidimusBuilder($file, $company);
        $vidimuses = $vidimusBuilder->build();
        // todo: date, balance
        $statistic = [
            'dateStart' => null,
            'dateEnd' => null,
            'balanceStart' => null,
            'balanceEnd' => null,
            'totalIncome' => null,
            'totalExpense' => null,
        ];

        BankingEmailFile::deleteSummaryTmpFile($_summaryFileName);

        $this->loadStatementData($vidimuses, $statistic);

        foreach ($fileModels as $fileModel) {
            $fileModel->updateAttributes([
                'is_uploaded' => 1,
                'uploaded_at' => date('Y-m-d H:i:s')
            ]);
        }

        Yii::$app->session->set('bspb_uploaded_files_count', count($filesArr));

        return;
    }
}