<?php

namespace frontend\modules\cash\modules\banking\modules\bspb_email;

/**
 * API - ПАО «Банк «Санкт-Петербург»
 *
 * bspb_email module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\bspb_email\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
