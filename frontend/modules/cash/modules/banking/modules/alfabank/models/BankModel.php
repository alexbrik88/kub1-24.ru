<?php

namespace frontend\modules\cash\modules\banking\modules\alfabank\models;

use backend\models\Bank;
use common\models\bank\BankingParams;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\modules\alfabank\components\SoapClient;
use frontend\modules\cash\modules\banking\modules\alfabank\widgets\PaymentOrder;
use frontend\modules\cash\modules\banking\modules\alfabank\widgets\StatementRequest;
use frontend\modules\cash\modules\banking\modules\alfabank\widgets\StatementResult;
use frontend\modules\cash\modules\banking\modules\alfabank\widgets\StatementStatus;
use common\components\curl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * BankModel
 *
 * АО «Альфа-Банк», г. Москва
 *
 * Так же базовый класс для следующих филиалов:
 * 046577964 Филиал «Екатеринбургский» АО «АЛЬФА-БАНК»
 * 042202824 Филиал «Нижегородский» АО «АЛЬФА-БАНК»
 * 045004774 Филиал «Новосибирский» АО «АЛЬФА-БАНК»
 * 046015207 Филиал «Ростовский» АО «АЛЬФА-БАНК»
 * 044030786 Филиал «Санкт-Петербургский» АО «АЛЬФА-БАНК»
 * 040702752 Филиал «Ставропольский» АО «АЛЬФА-БАНК»
 * 040813770 Филиал «Хабаровский» АО «АЛЬФА-БАНК»
 *
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525593';
    const NAME = 'АО «Альфа-Банк»';
    const NAME_SHORT = 'Альфа-Банк';
    const ALIAS = 'alfabank';

    public static $alias = 'alfabank';
    public static $hasAutoload = true;
    public $needPreRequest = true;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_RESULT = 'result';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';

    public $auth_redirect;
    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $request_id;
    public $requestStatus;

    protected $_apiBank;

    protected static $apiHost = 'https://testjmb.alfabank.ru';
    protected static $clientBankPath = '/webclient/pages';
    protected static $statementRequestPath = '/CS/ALBO/WSCreateAccountMovementListRequest/WSCreateAccountMovementListRequest';
    protected static $statementStatusPath = '/CS/ALBO/WSGetAccountMovementList/WSGetAccountMovementList';
    protected static $statementResultPath = '/CS/ALBO/WSGetAccountMovementList/WSGetAccountMovementList';
    protected static $paymentOrderPath = '/CS/ALBO/WSCreatePaymentDocRUR/WSCreatePaymentDocRUR';

    /**
     * All branches of the bank
     * @inheritdoc
     */
    public static $bikList = [
        '044525593', // АО «Альфа-Банк», г. Москва
        '046577964', // Филиал «Екатеринбургский» АО «АЛЬФА-БАНК»
        '042202824', // Филиал «Нижегородский» АО «АЛЬФА-БАНК»
        '045004774', // Филиал «Новосибирский» АО «АЛЬФА-БАНК»
        '046015207', // Филиал «Ростовский» АО «АЛЬФА-БАНК»
        '044030786', // Филиал «Санкт-Петербургский» АО «АЛЬФА-БАНК»
        '040702752', // Филиал «Ставропольский» АО «АЛЬФА-БАНК»
        '040813770', // Филиал «Хабаровский» АО «АЛЬФА-БАНК»
    ];

    /**
     * @inheritdoc
     */
    public static $errorList = [
        'PUB0007' => 'Ошибка валидации прав клиента из внешней системы (клиенту не подключена услуга, счет другого клиента)',
        'PUB0008' => 'Клиент блокирован для работы в системе АЛБО',
        'CAM0003' => 'Период за который делается запрос превышает допустимый',
    ];

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_RESULT => [
                'account_id',
                'request_id',
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'account_id',
                'start_date',
                'end_date',
                'code',
                'request_id',
                'payBill',
                'paymentOrder',
                'currentAccount',
            ], 'required'],
            [['start_date'], 'date', 'format' => 'php:d.m.Y', 'max' => date('d.m.Y')],
            [['end_date'], 'date', 'format' => 'php:d.m.Y', 'max' => date('d.m.Y')],
            [['end_date'], 'dateComparison'],
            [['request_id'], 'string'],
            [['code'], 'string'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->_company->id,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getApiBank()
    {
        if ($this->_apiBank === null) {
            $this->_apiBank = Bank::findOne(['bik' => BankModel::BIK, 'is_blocked' => false]);
        }

        return $this->_apiBank;
    }

    /**
     * @return string
     */
    public function getExternalSystemCode()
    {
        return $this->getConfig('externalSystemCode');
    }

    /**
     * @return string
     */
    public function getExternalUserCode()
    {
        if ($this->getConfig('isTest', false)) {
            return $this->getConfig('externalUserCode');
        }

        return $this->getParam('externalUserCode') ? : (
            Yii::$app->id == 'app-frontend' && Yii::$app->user->identity ? Yii::$app->user->identity->email : null
        );
    }

    /**
     * @return string
     */
    public function getApiHost()
    {
        return $this->getConfig('apiHost', static::$apiHost);
    }

    /**
     * @return string
     */
    public function clientBankUrl()
    {
        return $this->getApiHost() . static::$clientBankPath;
    }

    /**
     * @return string
     */
    public function statementRequestUrl()
    {
        return $this->getApiHost() . static::$statementRequestPath;
    }

    /**
     * @return string
     */
    public function statementStatusUrl()
    {
        return $this->getApiHost() . static::$statementStatusPath;
    }

    /**
     * @return string
     */
    public function statementResultUrl()
    {
        return $this->getApiHost() . static::$statementResultPath;
    }

    /**
     * @return string
     */
    public function paymentOrderUrl()
    {
        return $this->getApiHost() . static::$paymentOrderPath;
    }

    /**
     * @return boolean
     */
    public function isValidToken()
    {
        return !empty($this->getParam('externalUserCode'));
    }

    /**
     * SOAP request headers
     * @return array
     */
    public static function requestHeaders(string $url, string $requestData)
    {
        return array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "SOAPAction: " . $url,
            "Content-length: ".strlen($requestData),
        );
    }

    /**
     * @param  string $msg SOAP message
     * @return string      Signed SOAP message
     */
    public function signMessage($msg)
    {
        $result = null;
        $signUtil = Yii::getAlias('@banking/modules/alfabank/components/sign/SignUtil.jar');
        $signConfig = $this->getConfig('signConfig');

        if (is_file($signUtil) && is_array($signConfig)) {
            $tmpDir = sys_get_temp_dir();
            $configFile = tempnam($tmpDir, 'SignUtil');
            $inFile = tempnam($tmpDir, 'SignUtil');
            $outFile = tempnam($tmpDir, 'SignUtil');
            foreach ($signConfig as $key => $value) {
                file_put_contents($configFile, "{$key}={$value}\n", FILE_APPEND);
            }
            file_put_contents($inFile, $msg);
            exec("java -jar {$signUtil} {$configFile} {$inFile} {$outFile}", $output);

            if (end($output) == 'true') {
                $result = file_get_contents($outFile);
            }

            unlink($inFile);
            unlink($outFile);
        }

        return $result;
    }

    /**
     * @param  string $msg SOAP message
     * @return boolean
     */
    public function verifyMessage($msg)
    {
        $result = null;
        $signUtil = Yii::getAlias('@banking/modules/alfabank/components/sign/SignUtil.jar');
        $signConfig = $this->getConfig('signConfig');

        if (is_file($signUtil) && is_array($signConfig)) {
            $tmpDir = sys_get_temp_dir();
            $configFile = tempnam($tmpDir, 'SignUtil');
            $inFile = tempnam($tmpDir, 'SignUtil');
            foreach ($signConfig as $key => $value) {
                file_put_contents($configFile, "{$key}={$value}\n", FILE_APPEND);
            }
            file_put_contents($inFile, $msg);
            exec("java -jar {$signUtil} {$configFile} {$inFile}", $output);

            unlink($inFile);

            return end($output) == 'true';
        }

        return false;
    }

    /**
     * @param  string $msg SOAP message
     * @return array
     */
    public function parseMessage($msg)
    {
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$3", $msg);
        $xml = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);

        return json_decode(json_encode($xml), true);
    }

    /**
     * @return mixed
     */
    public function soapRequest($message, $url, $method)
    {
        $result = null;
        $curl = null;
        $requestData = $this->signMessage($message);

        if ($requestData) {
            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => self::requestHeaders($url, $requestData),
                CURLOPT_POSTFIELDS => $requestData,
            ])->post($url);

            $this->debugLog($curl, $method);

            if (!$curl->errorCode && $curl->responseCode == 200 && $this->verifyMessage($response)) {
                $result = $this->parseMessage($response);
            }
        }

        return [$result, $curl];
    }

    /**
     * Создание запроса на выписку, получение ID запроса
     *
     * @return boolean
     */
    public function sendRequest()
    {
        $url = $this->statementRequestUrl();
        $message = StatementRequest::widget([
            'externalSystemCode' => $this->getExternalSystemCode(),
            'externalUserCode' => $this->getExternalUserCode(),
            'accountNumber' => $this->getCurrentAccount()->rs,
            'startDate' => $this->getDateTimeFrom()->format('Y-m-d'),
            'endDate' => $this->getDateTimeTill()->format('Y-m-d'),
        ]);

        list($result, $curl) = $this->soapRequest($message, $url, __METHOD__);

        if ($result && isset($result['Body']['WSCreateAccountMovementListRequestAddResponse']['outParms']['requestId'])) {
            $this->request_id = $result['Body']['WSCreateAccountMovementListRequestAddResponse']['outParms']['requestId'];
            if (!$this->getConfig('isTest', false) &&
                !$this->getParam('externalUserCode') &&
                Yii::$app->id == 'app-frontend' &&
                Yii::$app->user->identity !== null
            ) {
                $this->setParam('externalUserCode', Yii::$app->user->identity->email);
            }

            return true;
        }

        $this->_error = true;

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Cтатус запроса на выписку
     *
     * @return boolean
     */
    public function checkRequestStatus()
    {
        $url = $this->statementStatusUrl();
        $message = StatementStatus::widget([
            'externalSystemCode' => $this->getExternalSystemCode(),
            'externalUserCode' => $this->getExternalUserCode(),
            'requestId' => $this->request_id,
        ]);

        list($result, $curl) = $this->soapRequest($message, $url, __METHOD__);

        if ($result && isset($result['Body']['WSGetAccountMovementListStatusResponse']['outParms']['status'])) {
            $this->requestStatus = $result['Body']['WSGetAccountMovementListStatusResponse']['outParms']['status'] == 'true' ? 1 : 0;

            return true;
        }

        $this->_error = true;

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Получение выписки
     *
     * @return boolean
     */
    public function takeResult()
    {
        $url = $this->statementResultUrl();
        $message = StatementResult::widget([
            'externalSystemCode' => $this->getExternalSystemCode(),
            'externalUserCode' => $this->getExternalUserCode(),
            'requestId' => $this->request_id,
        ]);

        list($result, $curl) = $this->soapRequest($message, $url, __METHOD__);

        if ($result && isset($result['Body']['WSGetAccountMovementListDocResponse']['outParms']['result'])) {
            $this->_data = $result['Body']['WSGetAccountMovementListDocResponse']['outParms'];

            return true;
        }

        $this->_error = true;

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Платежное поручение
     *
     * @return boolean
     */
    public function sendPaymentOrder()
    {
        $url = $this->paymentOrderUrl();
        $message = PaymentOrder::widget([
            'externalSystemCode' => $this->getExternalSystemCode(),
            'externalUserCode' => $this->getExternalUserCode(),
            'docNumber' => (string) $this->paymentOrder->document_number,
            'docDate' => null,
            'docSum' => bcdiv($this->paymentOrder->sum, 100, 2),
            'payerAccount' => (string) ($this->currentAccount ? $this->currentAccount->rs : $this->paymentOrder->company_rs),
            'payerInn' => (string) $this->paymentOrder->company_inn,
            'payerKpp' => (string) $this->paymentOrder->company_kpp,
            'recipientName' => (string) $this->paymentOrder->contractor_name,
            'recipientInn' => (string) $this->paymentOrder->contractor_inn,
            'recipientKpp' => (string) $this->paymentOrder->contractor_kpp,
            'recipientAccount' => (string) $this->paymentOrder->contractor_current_account,
            'recipientBankBik' => (string) $this->paymentOrder->contractor_bik,
            'priority' => (int) ($this->paymentOrder->ranking_of_payment ? : 5),
            'paymentPurpose' => (string) $this->paymentOrder->purpose_of_payment,
            'budgetPayerStatus' => (string) ($this->paymentOrder->taxpayersStatus ? $this->paymentOrder->taxpayersStatus->code : '01'),
            'budgetKbk' => (string) ($this->paymentOrder->kbk ? : 0),
            'budgetOktmo' => (string) ($this->paymentOrder->oktmo_code ? : 0),
            'budgetPaymentBase' => (string) ($this->paymentOrder->payment_details_id ? : 0),
            'budgetPeriod' => (string) ($this->paymentOrder->tax_period_code ? : 0),
            'budgetDocNumber' => (string) ($this->paymentOrder->document_number_budget_payment ? : 0),
            'budgetDocDate' => (string) ($this->paymentOrder->document_date_budget_payment ? : 0),
            'budgetPaymentType' => (string) ($this->paymentOrder->payment_type_id ? : 0),
            'code' => (string) ($this->paymentOrder->uin_code ? : 0),
        ]);

        list($result, $curl) = $this->soapRequest($message, $url, __METHOD__);

        if ($result && isset($result['Body']['WSCreateAccountMovementListRequestAddResponse']['outParms']['requestId'])) {
            $this->request_id = $result['Body']['WSCreateAccountMovementListRequestAddResponse']['outParms']['requestId'];
            if (!$this->getConfig('isTest', false) &&
                !$this->getParam('externalUserCode') &&
                Yii::$app->id == 'app-frontend' &&
                Yii::$app->user->identity !== null
            ) {
                $this->setParam('externalUserCode', Yii::$app->user->identity->email);
            }

            return true;
        }

        $this->_error = true;

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * @param  array $array
     * @param  string $key
     * @return string
     */
    public function paymentValue($array, $key)
    {
        $value = ArrayHelper::getValue($array, $key);

        return is_array($value) ? '' : (string) $value;
    }

    /**
     * Prepare vidimus from XML
     */
    public function vidimusPrepare()
    {
        $data = (array) $this->_data;
        $result = ArrayHelper::getValue($data, 'result', []);
        $paymentArray = ArrayHelper::getValue(ArrayHelper::getValue($data, 'resultSet', []), 'row', []);
        $vidimuses = [];
        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => (string) ArrayHelper::getValue($result, 'incomingBalance'),
            'balanceEnd' => (string) ArrayHelper::getValue($result, 'outgoingBalance'),
            'totalIncome' => (string) ArrayHelper::getValue($result, 'creditTurnover'),
            'totalExpense' => (string) abs(ArrayHelper::getValue($result, 'debitTurnover') * 1),
        ];

        foreach ((array) $paymentArray as $payment) {
            $date = date_create_from_format('Y-m-d', ArrayHelper::getValue($payment, 'chargeDate', ''));
            switch (ArrayHelper::getValue($payment, 'operation')) {
                case 'C':
                    $flowType = Vidimus::FLOW_IN;
                    break;
                case 'D':
                    $flowType = Vidimus::FLOW_OUT;
                    break;

                default:
                    $flowType = null;
                    break;
            }

            $vidimuses[] = new Vidimus($this->company, [
                'date' => $date ? $date->format('d.m.Y') : '',
                'number' => $this->paymentValue($payment, 'docNumber'),
                'flowType' => $flowType,
                'total' => (string) abs(ArrayHelper::getValue($payment, 'amount') * 1),
                'contragent' => $this->paymentValue($payment, 'contractorName'),
                'target' => $this->paymentValue($payment, 'paymentPurpose'),
                'inn' => $this->paymentValue($payment, 'contractorInn'),
                'kpp' => $this->paymentValue($payment, 'contractorKpp'),
                'currentAccount' => $this->paymentValue($payment, 'contractorAccount'),
                'bik' => $this->paymentValue($payment, 'contractorBik'),
                'bankName' => $this->paymentValue($payment, 'contractorBankName'),
                'taxpayers_status' => $this->paymentValue($payment, 'budgetPayerStatus'),
                'ranking_of_payment' => $this->paymentValue($payment, 'priority'),
                'kbk' => $this->paymentValue($payment, 'budgetKbk'),
                'oktmo_code' => $this->paymentValue($payment, 'budgetOktmo'),
                'payment_details' => $this->paymentValue($payment, 'budgetPaymentBase'),
                'tax_period_code' => $this->paymentValue($payment, 'budgetPeriod'),
                'document_number_budget_payment' => $this->paymentValue($payment, 'budgetDocNumber'),
                'document_date_budget_payment' => $this->paymentValue($payment, 'budgetDocDate'),
                'payment_type' => $this->paymentValue($payment, 'budgetPaymentType'),
                'uin_code' => $this->paymentValue($payment, 'code'),
            ]);
        }

        return [$vidimuses, $statistic];
    }

    /**
     * Send statement Pre-Request
     */
    public function autoloadPreRequest()
    {
        if ($this->isValidToken()) {
            return $this->sendRequest();
        }

        return false;
    }

    /**
     * Check if the statement is ready
     */
    public function autoloadCheckStatus()
    {
        if ($this->isValidToken()) {
            return $this->checkRequestStatus();
        }

        return false;
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        if ($this->isValidToken() && $this->takeResult()) {
            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }
}
