<?php

namespace frontend\modules\cash\modules\banking\modules\alfabank\controllers;

use common\models\Company;
use frontend\modules\cash\modules\banking\components\BankingModulesBaseController;
use frontend\modules\cash\modules\banking\modules\alfabank\models\BankModel;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Default controller for the `alfabank` module
 *
 * АО «Альфа-Банк», г. Москва
 */
class DefaultController extends BankingModulesBaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'request',
                            'send',
                            'status',
                            'result',
                            'payment',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => [
                            'set-autoload',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($account_id = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;
        $model->account_id = $account_id && $model->getCompanyAccounts()->andWhere([
            'id' => $account_id,
        ])->exists() ? $account_id : null;
        $model->checkStartDate();

        $model->scenario = BankModel::SCENARIO_REQUEST;

        return $this->render('request', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionRequest($account_id = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);
        $model->account_id = $account_id && $model->getCompanyAccounts()->andWhere([
            'id' => $account_id,
        ])->exists() ? $account_id : null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) {
                \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 98, true);
            }
            return $this->render('result', ['model' => $model]);
        } else {
            return $this->render('request', ['model' => $model]);
        }
    }

    /**
     * @return string
     */
    public function actionSend($account_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->getModel(BankModel::SCENARIO_REQUEST);
        $model->account_id = $account_id && $model->getCompanyAccounts()->andWhere([
            'id' => $account_id,
        ])->exists() ? $account_id : null;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->sendRequest()) {
            return [
                'inputs' => Html::activeHiddenInput($model, 'account_id') .
                    "\n" . Html::activeHiddenInput($model, 'request_id'),
            ];
        }

        return [
            'redirect' => Url::to([
                'index',
                'account_id' => $model->account_id,
                'p' => Yii::$app->request->get('p'),
            ]),
        ];
    }

    /**
     * @return string
     */
    public function actionStatus($account_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->getModel(BankModel::SCENARIO_RESULT);
        $model->account_id = $account_id && $model->getCompanyAccounts()->andWhere([
            'id' => $account_id,
        ])->exists() ? $account_id : null;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->checkRequestStatus()) {
            return [
                'status' => $model->requestStatus,
            ];
        }

        return [
            'redirect' => Url::to([
                'index',
                'account_id' => $model->account_id,
                'p' => Yii::$app->request->get('p'),
            ]),
        ];
    }

    /**
     * @return string
     */
    public function actionResult($account_id = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_RESULT);
        $model->account_id = $account_id && $model->getCompanyAccounts()->andWhere([
            'id' => $account_id,
        ])->exists() ? $account_id : null;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->takeResult()) {
            return $this->renderContent($model->renderStatement());
        }

        return $this->render('index', ['model' => $this->getModel(BankModel::SCENARIO_DEFAULT)]);
    }

    /**
     * @return string
     */
    public function actionPayment($account_id = null, $po_id = null)
    {
        $this->layout = '@banking/views/layouts/payment';
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->account_id = $account_id;
        $model->paymentOrder = $this->findPaymentOrder($po_id);

        $model->setScenario(BankModel::SCENARIO_PAYMENT_ORDER);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->render('@banking/views/all-banks/payment', [
                'model' => $model,
                'sent' => $model->sendPaymentOrder(),
            ]);
        }

        return $this->render('@banking/views/all-banks/payment', ['model' => $model, 'sent' => false]);
    }

    /**
     * @return BankModel
     */
    public function getModel($scenario, Company $company = null)
    {
        $model = new BankModel(Yii::$app->user->identity->company, ['scenario' => $scenario]);
        $this->view->params['bankingModel'] = $model;

        return $model;
    }
}
