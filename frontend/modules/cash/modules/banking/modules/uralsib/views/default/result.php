<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\cash\modules\banking\modules\uralsib\models\BankModel */

?>

<?php $form = ActiveForm::begin([
    'id' => 'statement-result-form',
    'action' => [
        'result',
        'account_id' => Yii::$app->request->get('account_id'),
        'p' => Yii::$app->request->get('p'),
    ],
    'options' => [
        'data' => [
            'status-ready' => $model->statusReady,
            'status-progress' => $model->statusProgress,
            'status-url' => Url::to([
                'status',
                'p' => Yii::$app->request->get('p'),
            ]),
            'pjax' => true,
        ]
    ]
]); ?>

    <?= $form->field($model, 'request_id')->hiddenInput()->label(false); ?>
    <?= $form->field($model, 'account_id')->hiddenInput(['id' => 'hidden-account_id'])->label(false); ?>

    <div class="progress-container">
        <div class="formation-status form-group">
            <label class="control-label">
                <span>идет формирование выписки </span>
                <span class="progress-value"></span>
            </label>
            <div class="progress progress-striped active">
                <div class="progress-bar"
                     role="progressbar"
                     aria-valuenow="0" aria-valuemax="100" style="width: 0%;"></div>
            </div>
        </div>
        <?= Html::a('Отменить', [
            '/cash/banking/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'btn btn-primary banking-module-link banking-cancel',
            'style' => 'float:right;',
        ]); ?>
    </div>

<?php $form->end(); ?>

<script type="text/javascript">
(function( $ ){
    window.URALSIB = {};
    $.extend(URALSIB, {
        progressTimerId: null,
        progressValue: 0,
        statusTimerId: null,
        form: $('#statement-result-form'),
        statusUrl: $('#statement-result-form').data('status-url'),
        progressUpdate: function() {
            URALSIB.progressValue++;
            $(".progress-container .progress-bar").css("width", URALSIB.progressValue + "%");
            $(".progress-container .progress-value").text(URALSIB.progressValue + "%");
        },
        progressStart: function() {
            URALSIB.progressValue = 0;
            URALSIB.progressTimerId = setTimeout(function tick() {
                URALSIB.progressUpdate();
                if (URALSIB.progressValue < 100) {
                    URALSIB.progressTimerId = setTimeout(tick, 300);
                } else {
                    $('#statement-result-form').submit();
                }
            }, 300);
        },
        progressStop: function() {
            clearTimeout(URALSIB.progressTimerId);
            URALSIB.progressValue = 0;
        },
        statusRequest: function() {
            $.ajax({
                url: URALSIB.statusUrl,
                type: 'post',
                data: URALSIB.form.serialize(),
                success: function(data) {
                    console.log(data);
                    if (data.status == 'ready') {
                        URALSIB.progressValue = Math.max(URALSIB.progressValue, 95);
                        URALSIB.progressUpdate();
                        $('#statement-result-form').submit();
                    } else if (data.status == 'queued') {
                        URALSIB.statusTimerId = setTimeout(URALSIB.statusRequest, 5000);
                    } else if (data.redirect) {
                        clearTimeout(URALSIB.progressTimerId);
                        $.pjax({url: data.redirect, container: "#banking-module-pjax"});
                    }
                }
            });
        }
    });
    URALSIB.progressStart();
    URALSIB.statusTimerId = setTimeout(URALSIB.statusRequest, 5000);
})( jQuery );
</script>
