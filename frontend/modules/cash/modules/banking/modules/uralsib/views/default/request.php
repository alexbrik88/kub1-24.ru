<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Dropdown;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\cash\modules\banking\components\AccountSelectWidget;

/* @var $this yii\web\View */
/* @var $this frontend\modules\cash\modules\banking\modules\uralsib\models\BankModel */

?>

<div class="alert alert-danger mb-2" role="alert">
    Запрос выписки из банка ПАО «БАНК УРАЛСИБ» в стадии разработки и в настоящий момент не работает. Попробуйте позже.
</div>

<div id='statement-request-form-container'>
    <?php $form = ActiveForm::begin([
        'id' => 'uralsib-statement-request-form',
        'action' => [
            'request',
            'account_id' => Yii::$app->request->get('account_id'),
            'p' => Yii::$app->request->get('p'),
        ],
        'options' => [
            'data' => [
                'pjax' => true,
            ]
        ]
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= AccountSelectWidget::widget([
                'form' => $form,
                'bankModel' => $model,
                'linkOptions' => [
                    'class' => 'banking-module-link',
                ],
            ]); ?>
        </div>
    </div>

    <?php  if ($model->currentAccount) {
        echo Html::hiddenInput('account', $model->currentAccount->rs, ['id' => 'uralsib-currentAccount']);
    } ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'start_date', [
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
                'labelOptions' => [
                    'class' => 'control-label width-label bold-text',
                ],
                'wrapperOptions' => [
                  'class' => 'col-md-5',
                ],
            ])->textInput(['class' => 'form-control date-picker width-full']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'end_date', [
                'inputTemplate' => '<div class="input-icon"><i class="fa fa-calendar"></i>{input}</div>',
                'labelOptions' => [
                    'class' => 'control-label width-label bold-text',
                ],
                'wrapperOptions' => [
                  'class' => 'col-md-5',
                ],
            ])->textInput(['class' => 'form-control date-picker width-full']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= Html::submitButton('Отправить запрос', [
                'class' => 'btn darkblue text-white visible-lg pull-left',
                'style' => '',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => 'btn darkblue text-white hidden-lg pull-left',
                'title' => 'Отправить запрос',
            ]); ?>
            <?= Html::a('Отменить', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn darkblue text-white visible-lg back banking-module-link banking-cancel pull-right',
                'style' => 'width: 120px!important;',
            ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', [
                '/cash/banking/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn darkblue text-white hidden-lg back banking-module-link banking-cancel pull-right',
                'title' => 'Отменить',
            ]); ?>
        </div>
    </div>
    <?php $form->end() ?>

    <?= \frontend\modules\cash\modules\banking\widgets\AutoloadWidget::widget([
        'model' => $model,
    ]); ?>

    <?= $this->render('@banking/views/all-banks/delete_ask') ?>
</div>
