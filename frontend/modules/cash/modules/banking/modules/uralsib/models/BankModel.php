<?php

namespace frontend\modules\cash\modules\banking\modules\uralsib\models;

use backend\models\Bank;
use common\models\bank\BankingParams;
use common\models\company\CheckingAccountant;
use common\models\document\status\PaymentOrderStatus;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\components\curl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * BankModel
 *
 * ПАО «БАНК УРАЛСИБ»
 *
 * 044525787 ПАО «БАНК УРАЛСИБ»
 *
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525787';
    const NAME = 'ПАО «БАНК УРАЛСИБ»';
    const NAME_SHORT = 'Точка';
    const ALIAS = 'uralsib';

    public static $alias = 'uralsib';
    public static $hasPaymentApi = true;
    public static $hasAutoload = true;

    public $needPreRequest = true;
    public $statusReady = 'ready';
    public $statusProgress = 'queued';

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [
        '044525787', // ПАО «БАНК УРАЛСИБ»
        '048073770',
        '044030706',
        '046577446',
        '040702753',
        '045004725',
        '040349700',
    ];

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_RESULT = 'result';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';
    const SCENARIO_SSO = 'sso';

    public $auth_redirect;
    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $request_id;
    public $requestStatus;

    protected $_apiBank;
    protected $_returnUrl;

    protected static $paramKey = '044525787';
    protected static $host = 'https://place-prod.uralsib.ru';
    protected static $authPath = '/sso/connect/authorize';
    protected static $tokenPath = '/sso/connect/token';
    protected static $accountsPath = '/api/v1/account/list';
    protected static $statementPath = '/api/v1/statement';
    protected static $paymentOrderPath = '/api/v1/payment';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        \Yii::$app->i18n->translations['banking/uralsib'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@frontend/modules/cash/modules/banking/modules/uralsib/messages',
            'fileMap' => [
                'banking/uralsib' => 'text.php',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
            ],
            static::SCENARIO_TOKEN => [
                'code',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_RESULT => [
                'account_id',
                'request_id',
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
            static::SCENARIO_PAY_BILL => [
                'payBill',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'account_id',
                'start_date',
                'end_date',
                'code',
                'request_id',
                'payBill',
                'paymentOrder',
                'currentAccount',
            ], 'required'],
            [['start_date', 'end_date'], 'date'],
            [['request_id'], 'string'],
            [['code'], 'string'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->company->id,
                    'bik' => static::$bikList,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
        ];
    }

    /**
     * @return string
     */
    public function getParams()
    {
        if ($this->_params === null) {
            $this->_params = ArrayHelper::getValue(Yii::$app->params['banking'], static::$alias, []);
        }

        return $this->_params;
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return self::$host . self::$authPath . '?' . http_build_query([
            'scope' => 'ApiResource_Bank openid offline_access profile legal_info ApiResource_LK',
            'response_type' => 'code',
            'client_id' => ArrayHelper::getValue($this->getParams(), 'client_id', ''),
            'redirect_uri' => $this->getReturnUrl(),
        ]);
    }

    /**
     * @param string $value
     */
    public function setReturnUrl($value)
    {
        $this->_returnUrl = $value;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        if ($this->_returnUrl === null) {
            $this->_returnUrl = Yii::$app->request->hostInfo . Url::to([
                '/cash/banking/' . static::$alias . '/default/return',
            ]);
        }

        return $this->_returnUrl;
    }

    /**
     * @return string
     */
    public function getPaymentOrderUrl()
    {
        return self::$host . self::$paymentOrderPath;
    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        if ($this->company->id) {
            if ($this->accessToken && $this->accessTokenExpires > (time()+5)) {
                return true;
            } else {
                return $this->refreshToken ? $this->authTokenRefresh($this->refreshToken) : false;
            }
        }

        return $this->getTmpToken() !== null;
    }

    /**
     * @inheritdoc
     */
    public function getApiBank()
    {
        if ($this->_apiBank === null) {
            $this->_apiBank = Bank::findOne(['bik' => BankModel::BIK, 'is_blocked' => false]);
        }

        return $this->_apiBank;
    }

    /**
     * @inheritdoc
     */
    public function setAccessToken($access_token)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'access_token', $access_token);
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        return $this->company->id ?
               BankingParams::getValue($this->company, static::ALIAS, 'access_token') :
               $this->getTmpToken();
    }

    /**
     * @inheritdoc
     */
    public function setAccessTokenExpires($expires_at)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'access_token_expires', $expires_at);
    }

    /**
     * @inheritdoc
     */
    public function getAccessTokenExpires()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'access_token_expires');
    }

    /**
     * @inheritdoc
     */
    public function setRefreshToken($refresh_token)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'refresh_token', $refresh_token);
    }

    /**
     * @return string|null
     */
    public function getRefreshToken()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'refresh_token');
    }

    /**
     * @return string
     */
    public function getAuthBasic()
    {
        $username = Yii::$app->params['banking'][static::$alias]['client_id'] ?? '';
        $password = Yii::$app->params['banking'][static::$alias]['client_secret'] ?? '';

        return base64_encode($username . ':' . $password);
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest($isTmp = false)
    {
        $time = time();
        $requestHeader = [
            'Authorization: Basic ' . $this->getAuthBasic(),
            'Content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type'=> 'authorization_code',
            'code' => $this->code,
            'redirect_uri' => $this->getReturnUrl(),
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$host . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                if ($isTmp) {
                    $this->setTmpToken($requestResult['access_token']);
                } else {
                    $this->accessToken = $requestResult['access_token'];
                    $this->accessTokenExpires = $time + $requestResult['expires_in'];
                    $this->refreshToken = $requestResult['refresh_token'];
                }

                return true;
            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for authToken
     * @return string|null
     */
    public function authTokenRefresh($refresh_token)
    {
        $time = time();
        $requestHeader = [
            'authorization: Basic ' . $this->getAuthBasic(),
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type'=> 'refresh_token',
            'refresh_token' => $refresh_token,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$host . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                $this->accessToken = $requestResult['access_token'];
                $this->accessTokenExpires = $time + $requestResult['expires_in'];
                $this->refreshToken = $requestResult['refresh_token'];

                return true;
            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }
                $this->unsetParam(['access_token', 'access_token_expires', 'refresh_token']);
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Создание запроса на выписку, получение ID запроса
     *
     * @return boolean
     */
    public function sendRequest()
    {
        $requestHeader = [
            'Authorization: Bearer ' . $this->accessToken,
            'Content-Type: application/json',
        ];

        $requestData = [
            'account_code' => $this->getCurrentAccount()->rs,
            'bank_code' => $this->getCurrentAccount()->bik,
            'date_start' => $this->getDateTimeFrom()->format('Y-m-d'),
            'date_end' => $this->getDateTimeTill()->format('Y-m-d'),
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
        ])->post(self::$host . self::$statementPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $result = json_decode($response, true);
            if ($result && isset($result['request_id'])) {
                $this->request_id = $result['request_id'];

                return true;
            } elseif (isset($result['message'])) {
                $this->_errorMessage = Yii::t('banking/uralsib', $result['message']);
            }
        }

        $this->_error = true;

        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Cтатус запроса на выписку
     *
     * @return boolean
     */
    public function checkRequestStatus()
    {
        $requestHeader = [
            'Authorization: Bearer ' . $this->accessToken,
            'Content-Type: application/json',
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
        ])->get(self::$host . self::$statementPath . "/status/{$this->request_id}");

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $result = json_decode($response, true);
            if ($result && isset($result['status'])) {
                $this->requestStatus = $result['status']; // "queued" | "ready"

                return true;
            } elseif (isset($result['message'])) {
                $this->_errorMessage = Yii::t('banking/uralsib', $result['message']);
            }
        }

        $this->_error = true;

        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Получение выписки
     *
     * @return boolean
     */
    public function takeResult()
    {
        $requestHeader = [
            'Authorization: Bearer ' . $this->accessToken,
            'Content-Type: application/json',
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
        ])->get(self::$host . self::$statementPath . "/result/{$this->request_id}");

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $result = json_decode($response, true);
            if ($result && isset($result['payments'], $result['balance_opening'], $result['balance_closing'])) {
                $this->_data = $result;

                return true;
            } elseif (isset($result['message'])) {
                $this->_errorMessage = Yii::t('banking/uralsib', $result['message']);
            }
        }

        $this->_error = true;

        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function sendPaymentOrder()
    {
        $requestHeader = [
            "Authorization: Bearer {$this->getAccessToken()}",
            "Content-Type: application/json",
        ];

        $requestData = [
            'account_code' => (string) ($this->currentAccount ? $this->currentAccount->rs : $this->paymentOrder->company_rs),
            'bank_code' => (string) ($this->currentAccount ? $this->currentAccount->bik : $this->paymentOrder->company_bik),
            'counterparty_account_number' => (string) $this->paymentOrder->contractor_current_account,
            'counterparty_bank_bic' => (string) $this->paymentOrder->contractor_bik,
            'counterparty_inn' => (string) $this->paymentOrder->contractor_inn,
            'counterparty_kpp' => (string) $this->paymentOrder->contractor_kpp,
            'counterparty_name' => (string) $this->paymentOrder->contractor_name,
            'payment_amount' => (string) bcdiv($this->paymentOrder->sum, '100', 2),
            'payment_date' => (string) date('d.m.Y'),
            'payment_number' => (string) $this->paymentOrder->document_number,
            'payment_priority' => (string) ($this->paymentOrder->ranking_of_payment ? : 5),
            'payment_purpose' => (string) $this->paymentOrder->purpose_of_payment,
            'supplier_bill_id' => (string) ($this->paymentOrder->uin_code ? : 0),
            'tax_info_document_date' => (string) ($this->paymentOrder->document_date_budget_payment ? : 0),
            'tax_info_document_number' => (string) ($this->paymentOrder->document_number_budget_payment ? : 0),
            'tax_info_kbk' => (string) ($this->paymentOrder->kbk ? : 0),
            'tax_info_okato' => (string) ($this->paymentOrder->oktmo_code ? : 0),
            'tax_info_period' => (string) ($this->paymentOrder->tax_period_code ? : 0),
            'tax_info_reason_code' => (string) ($this->paymentOrder->paymentDetails ?
                                      $this->paymentOrder->paymentDetails->code : 0),
            'tax_info_status' => (string) ($this->paymentOrder->taxpayersStatus ?
                                 $this->paymentOrder->taxpayersStatus->code : '')
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
        ])->post($this->getPaymentOrderUrl());

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $requestResult = json_decode($response, true);

            if (isset($requestResult['request_id'])) {
                $this->paymentOrder->updateAttributes([
                    'payment_order_status_id' => PaymentOrderStatus::STATUS_SENT_TO_BANK,
                    'payment_order_status_updated_at' => time(),
                ]);
                if (Yii::$app->id == 'app-frontend') {
                    $bankUrl = $this->bankPartner ? $this->bankPartner->url : null;
                    $msg = 'Платежное поручение успешно отправлено в клиент банк и находится в разделе "Черновики". ';
                    $msg .= ($bankUrl ? Html::a('Подтвердите платежку', $bankUrl, ['target' => '_blank']) : 'Подтвердите платежку');
                    $msg .= ', чтобы оплатить по счету.';
                    Yii::$app->session->setFlash('success', $msg);
                }

                return true;
            } elseif (!empty($requestResult['message'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult['message']);
                }

                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }
        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Prepare vidimus from XML
     */
    public function vidimusPrepare()
    {
        $data = (array) $this->_data;
        $paymentArray = ArrayHelper::getValue($data, 'payments', []);
        $vidimuses = [];
        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => ArrayHelper::getValue($data, 'balance_opening', ''),
            'balanceEnd' => ArrayHelper::getValue($data, 'balance_closing', ''),
            'totalIncome' => null,
            'totalExpense' => null,
        ];

        foreach ((array) $paymentArray as $payment) {
            $amount = ArrayHelper::getValue($payment, 'payment_amount', '');
            $flowType = substr($amount, 0, 1) == '-' ? Vidimus::FLOW_OUT : Vidimus::FLOW_IN;
            $vidimuses[] = new Vidimus($this->company, [
                'date' => ArrayHelper::getValue($payment, 'payment_date', ''),
                'number' => ArrayHelper::getValue($payment, 'payment_number', ''),
                'total' => (string) abs($amount * 1),
                'contragent' => ArrayHelper::getValue($payment, 'counterparty_name', ''),
                'target' => ArrayHelper::getValue($payment, 'payment_purpose', ''),
                'inn' => ArrayHelper::getValue($payment, 'counterparty_inn', ''),
                'kpp' => ArrayHelper::getValue($payment, 'counterparty_kpp', ''),
                'currentAccount' => ArrayHelper::getValue($payment, 'counterparty_account_number', ''),
                'bik' => ArrayHelper::getValue($payment, 'counterparty_bank_bic', ''),
                'bankName' => ArrayHelper::getValue($payment, 'counterparty_bank_name', ''),
                'flowType' => $flowType,
                'taxpayers_status' => (string) ArrayHelper::getValue($payment, 'tax_info_status', ''),
                'ranking_of_payment' => (string) ArrayHelper::getValue($payment, 'payment_priority', ''),
                'kbk' => (string) ArrayHelper::getValue($payment, 'tax_info_kbk', ''),
                'oktmo_code' => (string) ArrayHelper::getValue($payment, 'tax_info_okato', ''),
                'payment_details' => (string) ArrayHelper::getValue($payment, 'tax_info_reason_code', ''),
                'tax_period_code' => (string) ArrayHelper::getValue($payment, 'tax_info_period', ''),
                'document_number_budget_payment' => (string) ArrayHelper::getValue($payment, 'tax_info_document_number', ''),
                'document_date_budget_payment' => (string) ArrayHelper::getValue($payment, 'tax_info_document_date', ''),
                'payment_type' => '',
                'uin_code' => (string) ArrayHelper::getValue($payment, 'supplier_bill_id', ''),
            ]);
        }

        return [$vidimuses, $statistic];
    }

    /**
     * Send statement Pre-Request
     */
    public function autoloadPreRequest()
    {
        if ($this->isValidToken()) {
            return $this->sendRequest();
        }

        return false;
    }

    /**
     * Check if the statement is ready
     */
    public function autoloadCheckStatus()
    {
        if ($this->isValidToken()) {
            return $this->checkRequestStatus();
        }

        return false;
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        if ($this->isValidToken() && $this->takeResult()) {
            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }

    /**
     * Get start/end date from request_id
     * "044525999.2021-06-14.2021-06-01.407000000000000000"
     */
    public function setDatePeriodFromRequest()
    {
        if ($this->request_id) {
            $params = explode('.', $this->request_id);
            $start = \DateTime::createFromFormat('Y-m-d', $params[2] ?? '-');
            $end = \DateTime::createFromFormat('Y-m-d', $params[1] ?? '-');
            if ($start && $end) {
                $this->start_date = $start->format('d.m.Y');
                $this->end_date = $end->format('d.m.Y');
            }
        }
    }

    public function getIdentity()
    {
        return null;
    }
}
