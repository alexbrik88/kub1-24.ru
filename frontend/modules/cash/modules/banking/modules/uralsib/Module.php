<?php

namespace frontend\modules\cash\modules\banking\modules\uralsib;

/**
 * ПАО «БАНК УРАЛСИБ»
 *
 * uralsib module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\uralsib\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
