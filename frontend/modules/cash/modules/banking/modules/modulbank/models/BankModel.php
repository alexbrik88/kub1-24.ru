<?php

namespace frontend\modules\cash\modules\banking\modules\modulbank\models;

use DateTime;
use Yii;
use common\components\curl;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use common\models\bank\BankingParams;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\components\CurlHelper;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * BankModel
 *
 * АО КБ «Модульбанк»
 *
 * https://api.modulbank.ru/
 *
 * 044525092 Московского Филиала АО КБ «Модульбанк»
 * 045004864 Сибирский Филиал АО КБ «Модульбанк»
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525092';
    const NAME = 'АО КБ «Модульбанк»';
    const NAME_SHORT = 'Модульбанк';
    const ALIAS = 'modulbank';

    public static $alias = 'modulbank';
    public static $hasPaymentApi = false;

    public static $hasAutoload = true;

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [
        '044525092', // Московского Филиала АО КБ «Модульбанк»
        '045004864', // Сибирский Филиал АО КБ «Модульбанк»
    ];

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';

    const SCOPE = 'account-info operation-history';

    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $error;
    public $description;

    protected static $apiHost = 'https://api.modulbank.ru';
    protected static $authPath = '/v1/oauth/authorize';
    protected static $tokenPath = '/v1/oauth/token';
    protected static $infoPath = '/v1/account-info';
    protected static $historyPath = '/v1/operation-history/<bankAccountId>';
    protected static $paymentOrderPath = '/v1/operation-upload/1c';

    private $_accountId = false;
    private $_accountInfo;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_DEFAULT => [
            ],
            static::SCENARIO_TOKEN => [
                'code',
                'error',
                'description',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
            static::SCENARIO_PAY_BILL => [
                'payBill',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'account_id',
                'start_date',
                'end_date',
                'code',
                'payBill',
                'paymentOrder',
                'currentAccount',
            ], 'required'],
            [['code', 'error', 'description'], 'string'],
            [['start_date', 'end_date'], 'date'],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->company->id,
                    'bik' => static::$bikList,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
        ];
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        $url = self::$apiHost . self::$authPath;
        if (Yii::$app->params['banking'][static::$alias]['sandbox']) {
            $url .= "?sandbox=on";
        }

        return $url;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return Yii::$app->request->hostInfo . Url::to([
            '/cash/banking/' . static::$alias . '/default/return',
        ]);
    }

    /**
     * @return string
     */
    public function getTokenUrl()
    {
        return self::$apiHost . self::$tokenPath;
    }

    /**
     * @return string
     */
    public function getInfoUrl()
    {
        return self::$apiHost . self::$infoPath;
    }

    /**
     * @return string
     */
    public function getHistoryUrl()
    {
        return strtr(self::$apiHost . self::$historyPath, ['<bankAccountId>' => $this->accountId]);
    }

    /**
     * @return string
     */
    public function getPaymentOrderUrl()
    {
        return self::$apiHost . self::$paymentOrderPath;
    }

    /**
     * @return string
     */
    public function getIsSandbox()
    {
        return (boolean) Yii::$app->params['banking'][static::$alias]['sandbox'];
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return Yii::$app->params['banking'][static::$alias]['clientId'];
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return Yii::$app->params['banking'][static::$alias]['clientSecret'];
    }

    /**
     * @return string
     */
    public function getScope()
    {
        return self::SCOPE;
    }

    /**
     * @inheritdoc
     */
    public function setAccessToken($accessToken)
    {
        BankingParams::setValue($this->company, static::ALIAS, 'accessToken', $accessToken);
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return BankingParams::getValue($this->company, static::ALIAS, 'accessToken');
    }

    /**
     * @inheritdoc
     */
    public function setAccountId($id)
    {
        /*if ($this->currentAccount) {
            BankingParams::setValue($this->company, static::ALIAS, $this->currentAccount->rs, $id);
        }*/
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->_accountId;
        // return $this->currentAccount ? BankingParams::getValue($this->company, static::ALIAS, $this->currentAccount->rs) : null;
    }

    /**
     * @inheritdoc
     */
    public function checkAccount()
    {
        if ($this->_accountId === false) {
            foreach ((array) $this->_accountInfo as $companyData) {
                $accountList = (array) ArrayHelper::getValue($companyData, 'bankAccounts');
                foreach ($accountList as $account) {
                    $rs = ArrayHelper::getValue($account, 'number');
                    $accountId = ArrayHelper::getValue($account, 'id');
                    $currentRs = ArrayHelper::getValue($this, 'currentAccount.rs');
                    if ($rs && $accountId && $currentRs && $currentRs == $rs) {
                        $this->_accountId = $accountId;

                        return true;
                    }
                }
            }

            $this->_accountId = null;
        }

        return false;
    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        if ($this->accessToken && $this->checkApiAccess()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Request to remote API for accessToken
     *
     * @return string|null
     */
    public function authTokenRequest($isPayBill = false)
    {
        if ($this->validate()) {
            $time = time();
            $requestHeader = [
                'content-Type: application/json',
            ];
            if ($this->isSandbox) {
                $requestHeader[] = 'sandbox: on';
            }

            $requestData = [
                'clientId'=> $this->clientId,
                'clientSecret'=> $this->clientSecret,
                'code' => $this->code,
                'redirectUri' => $this->returnUrl,
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POSTFIELDS => json_encode($requestData),
            ])->post($this->tokenUrl);

            $this->debugLog($curl, __METHOD__);

            if (!$curl->errorCode) {
                $requestResult = json_decode($response, true);
                if ($requestResult && isset($requestResult['accessToken'])) {
                    if ($isPayBill) {
                        $this->setTmpToken($requestResult['accessToken']);
                    } else {
                        $this->accessToken = $requestResult['accessToken'];
                    }
                    $this->debugLog($curl, __METHOD__);

                    return true;
                } else {
                    if (Yii::$app->id == 'app-frontend') {
                        Yii::$app->session->setFlash('error', 'Не удалось получить токен авторизации.');
                    }
                    $this->errorLog($curl, __METHOD__);

                    return false;
                }
            }

            if ($curl->errorText && Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $curl->errorText);
            }

            $this->errorLog($curl, __METHOD__);
        } else {
            if ($this->description && Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->description);
            }
            Yii::warning(static::className() . "->authTokenRequest():\n\$_GET = " . var_export($_GET, true), 'banking');
        }

        return false;
    }

    /**
     * Request to remote API
     *
     * @return bool
     */
    public function checkApiAccess()
    {
        $requestHeader = [
            "Content-Type: application/json",
            "Authorization: Bearer {$this->accessToken}",
        ];
        if ($this->isSandbox) {
            $requestHeader[] = 'sandbox: on';
        }

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => null,
        ])->post($this->infoUrl);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($curl->responseCode == 200 && $requestResult) {
                if (isset($requestResult[0]['bankAccounts'])) {
                    $this->_accountInfo = $requestResult;
                    $this->checkAccount();

                    $this->debugLog($curl, __METHOD__);

                    return true;
                }
            } elseif ($curl->responseCode == 503) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('warning', 'Сервис Модульбанка временно недоступен. Попробуйте пожалуйста позже.');
                }
                $this->errorLog($curl, __METHOD__);
            }
        } else {
            $this->errorLog($curl, __METHOD__);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function statementRequest()
    {
        $requestHeader = [
            "Content-Type: application/json",
            "Authorization: Bearer {$this->accessToken}",
        ];
        if ($this->isSandbox) {
            $requestHeader[] = 'sandbox: on';
        }

        $requestData = [
            'from' => $this->getDateTimeFrom()->format('Y-m-d'),
            'till' => $this->getDateTimeTill()->format('Y-m-d'),
            'records' => 50,
            'skip' => 0,
        ];

        $done = true;
        $data = [];

        do {
            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POSTFIELDS => json_encode($requestData),
            ])->post($this->historyUrl);

            $this->debugLog($curl, __METHOD__);

            if (!$curl->errorCode) {
                $requestResult = json_decode($response, true);
                if ($curl->responseCode == 200 && is_array($requestResult)) {
                    $data = array_merge($data, $requestResult);
                    $this->debugLog($curl, __METHOD__);
                } elseif ($curl->responseCode == 503) {
                    if (Yii::$app->id == 'app-frontend') {
                        Yii::$app->session->setFlash('warning', 'Сервис Модульбанка временно недоступен. Попробуйте пожалуйста позже.');
                    }
                    $done = false;
                    $this->errorLog($curl, __METHOD__);
                    break;
                }
            } else {
                $this->errorLog($curl, __METHOD__);
                break;
            }
            $requestData['skip'] += 50;
        } while (!empty($requestResult) && count($requestResult) == 50);

        if ($done) {
            $this->_data = $data;
        } elseif (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        return $done;
    }

    public function sendPaymentOrder()
    {
        $requestHeader = [
            "Authorization: Bearer {$this->getAccessToken()}",
            "Content-Type: application/json",
        ];

        $requestData = [
            'document' => Yii::$app->getView()->render('@frontend/modules/documents/views/payment-order/import', [
                'modelArray' => [$this->paymentOrder],
            ]),
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
        ])->post($this->getPaymentOrderUrl());

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $requestResult = json_decode($response, true);

            if (isset($requestResult['totalLoaded']) && $requestResult['totalLoaded'] > 0) {
                $this->debugLog($curl, __METHOD__);
                return true;
            } elseif (!empty($requestResult['errors']) && Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', reset($requestResult['errors']));
            }
        } elseif (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }
        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Prepare vidimus
     * @return array
     */
    public function vidimusPrepare()
    {
        $vidimuses = [];
        $data = $this->_data;
        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => null,
            'balanceEnd' => null,
            'totalIncome' => null,
            'totalExpense' => null,
        ];

        try {
            foreach ($data as $operation) {
                switch (ArrayHelper::getValue($operation, 'category')) {
                    case 'Debet':
                        $flowType = Vidimus::FLOW_IN;
                        break;
                    case 'Credit':
                        $flowType = Vidimus::FLOW_OUT;
                        break;
                    default:
                        $flowType = null;
                        break;
                }
                if ($flowType !== null) {
                    $date = date_create(ArrayHelper::getValue($operation, 'executed'));
                    $vidimuses[] = new Vidimus($this->company, [
                        'flowType' => $flowType,
                        'contragent' => ArrayHelper::getValue($operation, 'contragentName', ''),
                        'inn' => ArrayHelper::getValue($operation, 'contragentInn', ''),
                        'kpp' => ArrayHelper::getValue($operation, 'contragentKpp', ''),
                        'currentAccount' => ArrayHelper::getValue($operation, 'contragentBankAccountNumber', ''),
                        'bankName' => ArrayHelper::getValue($operation, 'contragentBankName', ''),
                        'bik' => ArrayHelper::getValue($operation, 'contragentBankBic', ''),
                        'total' => ArrayHelper::getValue($operation, 'amount', ''),
                        'target' => ArrayHelper::getValue($operation, 'paymentPurpose', ''),
                        'date' => $date ? $date->format('d.m.Y') : '',
                        'number' => ArrayHelper::getValue($operation, 'docNumber', ''),
                        'taxpayers_status' => ArrayHelper::getValue($operation, 'payerStatus', ''),
                        'kbk' => ArrayHelper::getValue($operation, 'kbk', ''),
                        'oktmo_code' => ArrayHelper::getValue($operation, 'oktmo', ''),
                        'payment_details' => ArrayHelper::getValue($operation, 'paymentBasis', ''),
                        'tax_period_code' => ArrayHelper::getValue($operation, 'taxCode', ''),
                        'document_number_budget_payment' => ArrayHelper::getValue($operation, 'taxDocNum', ''),
                        'document_date_budget_payment' => ArrayHelper::getValue($operation, 'taxDocDate', ''),
                        'payment_type' => '',
                        'uin_code' => ArrayHelper::getValue($operation, 'uin', ''),
                    ]);
                }
            }

            return [$vidimuses, $statistic];
        } catch (\Exception $e) {
            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            return [[], $statistic];
        }
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        if ($this->isValidToken() && $this->statementRequest()) {
            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }
}
