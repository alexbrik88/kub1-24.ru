<?php

namespace frontend\modules\cash\modules\banking\modules\modulbank;

/**
 * API - АО КБ «Модульбанк»
 *
 * modulbank module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\modulbank\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
