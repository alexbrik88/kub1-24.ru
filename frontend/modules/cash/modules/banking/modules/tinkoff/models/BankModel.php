<?php

namespace frontend\modules\cash\modules\banking\modules\tinkoff\models;

use DateTime;
use Yii;
use common\components\curl;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use common\models\bank\BankingParams;
use common\models\document\status\PaymentOrderStatus;
use frontend\modules\cash\modules\banking\components\vidimus\Vidimus;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\banking\components\CurlHelper;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * BankModel
 *
 * АО «Тинькофф Банк»
 *
 * 044525974 АО «Тинькофф Банк»
 *
 * https://business.tinkoff.ru/openapi/docs
 *
 * Application to bank - http://static2.tinkoff.ru/portal/business/Doc/API.pdf
 */
class BankModel extends AbstractBankModel
{
    const BIK = '044525974';
    const NAME = 'АО «Тинькофф Банк»';
    const NAME_SHORT = 'Тинькофф';
    const ALIAS = 'tinkoff';

    public static $alias = 'tinkoff';
    public static $hasPaymentApi = true;
    public static $hasAutoload = true;

    /**
     * Branches of the bank
     * @var array
     */
    public static $bikList = [
        '044525974', // АО «Тинькофф Банк»
    ];

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_PAYMENT_ORDER = 'payment_order';
    const SCENARIO_PAY_BILL = 'pay_bill';

    public $auth_redirect;
    public $account_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $state;

    /**
     * Auth
     */
    protected static $authHost = 'https://id.tinkoff.ru';
    protected static $authPath = '/auth/authorize';
    protected static $tokenPath = '/auth/token';
    protected static $checkPath = '/auth/introspect';

    /**
     * API
     */
    protected static $apiHost = 'https://business.tinkoff.ru';
    protected static $companyPath = '/openapi/api/v1/company';
    protected static $accountsPath = '/openapi/api/v1/bank-accounts';
    protected static $statementPath = '/openapi/api/v1/bank-statement';
    protected static $paymentOrderPath = '/openapi/api/v1/payment/create';
    protected static $applicationUrl = 'https://origination.tinkoff.ru/api/v1/public/partner/createApplication';

    protected $_returnUrl;

    /**
     * @return string
     */
    public function getAccountsUrl()
    {
        return sellf::$apiHost . self::$accountsPath;
    }

    /**
     * @return string
     */
    public function getStatementUrl()
    {
        return sellf::$apiHost . self::$statementPath;
    }

    /**
     * @return string
     */
    public function getPaymentOrderUrl()
    {
        return sellf::$apiHost . self::$paymentOrderPath;
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        $this->setAuthState();

        $query = http_build_query([
            'client_id' => Yii::$app->params['banking'][static::$alias]['client_id'],
            'redirect_uri' => $this->getReturnUrl(),
            'state' => $this->getAuthState(),
            'response_type' => 'code',
            'scope_parameters' => Json::encode($this->getScopeData()),
        ]);

        return self::$authHost . self::$authPath . '?' . $query;
    }

    /**
     * @param string $value
     */
    public function setReturnUrl($value)
    {
        $this->_returnUrl = $value;
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        if ($this->_returnUrl === null) {
            $this->_returnUrl = Yii::$app->request->hostInfo . Url::to([
                '/cash/banking/' . static::$alias . '/default/return',
            ]);
        }

        return $this->_returnUrl;
    }

    /**lk.kub-24.ru
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_STATEMENT => [
                'statement',
            ],
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
            ],
            static::SCENARIO_TOKEN => [
                'code',
                'state',
            ],
            static::SCENARIO_REQUEST => [
                'account_id',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_PAYMENT_ORDER => [
                'account_id',
                'paymentOrder',
            ],
            static::SCENARIO_PAY_BILL => [
                'payBill',
                'paymentOrder',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'account_id',
                'start_date',
                'end_date',
                'code',
                'state',
                'payBill',
                'paymentOrder',
                'currentAccount',
            ], 'required'],
            [['code', 'state'], 'string'],
            [['start_date', 'end_date'], 'date'],
            [['state'], 'compare', 'compareValue' => $this->getAuthState()],
            [
                ['account_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['account_id' => 'id'],
                'filter' => [
                    'company_id' => $this->company->id,
                    'bik' => static::$bikList,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Номер счета',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
        ];
    }

    /**
     * @return string
     */
    public function getBik()
    {
        return static::BIK;
    }

    /**
     * @return string
     */
    public function getScopeData()
    {
        $scope = [
            'inn' => $this->_company->inn,
            'kpp' => empty($this->_company->kpp) ? '0' : $this->_company->kpp,
        ];

        return $scope;
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return 'banking.' . static::BIK . '.' . $this->_company->id ;
    }

    /**
     * @inheritdoc
     */
    public function setAuthState()
    {
        $session = Yii::$app->session;
        $session[$this->sessionKey . '.state'] = uniqid();
    }

    /**
     * @return string
     */
    public function getAuthState()
    {
        if (Yii::$app->id == 'app-frontend') {
            $session = Yii::$app->session;

            return $session[$this->sessionKey . '.state'];
        }

        return uniqid();
    }

    /**
     * @inheritdoc
     */
    public function setAccessToken($access_token)
    {
        $this->setParam('access_token', $access_token);
    }

    /**
     * @inheritdoc
     */
    public function getAccessToken()
    {
        return $this->company->id ?
               $this->getParam('access_token') :
               $this->getTmpToken();
    }

    /**
     * @inheritdoc
     */
    public function setAccessTokenExpires($expires_at)
    {
        $this->setParam('access_token_expires', $expires_at);
    }

    /**
     * @inheritdoc
     */
    public function getAccessTokenExpires()
    {
        return $this->getParam('access_token_expires');
    }

    /**
     * @inheritdoc
     */
    public function setRefreshToken($refresh_token)
    {
        $this->setParam('refresh_token', $refresh_token);
    }

    /**
     * @return string|null
     */
    public function getRefreshToken()
    {
        return $this->getParam('refresh_token');
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        $session = Yii::$app->session;

        return $session[$this->sessionKey . '.token'];
    }

    /**
     * @return string
     */
    public function getAuthBasic()
    {
        $username = Yii::$app->params['banking'][static::$alias]['client_id'];
        $password = Yii::$app->params['banking'][static::$alias]['password'];

        return base64_encode($username . ':' . $password);
    }

    /**
     * @return string
     */
    public function logMsg($message, $headers)
    {
        $data = [
            'headers' => $headers,
        ];

        return "{$message}\n\n" . var_export($data, true) . "\n\n";
    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        if ($this->checkApiAccess()) {
            return true;
        } elseif ($refreshToken = $this->getRefreshToken()) {
            return $this->authTokenRefresh($refreshToken) && $this->checkApiAccess();
        }

        return false;
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest($isPayBill = false)
    {
        $time = time();
        $requestHeader = [
            'authorization: Basic ' . $this->getAuthBasic(),
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type'=> 'authorization_code',
            'code' => $this->code,
            'redirect_uri' => $this->getReturnUrl(),
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$authHost . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                if ($isPayBill) {
                    $this->setTmpToken($requestResult['access_token']);
                } else {
                    $this->accessToken = $requestResult['access_token'];
                    $this->accessTokenExpires = $time + $requestResult['expires_in'];
                    $this->refreshToken = $requestResult['refresh_token'];
                }

                return true;
            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function checkApiAccess()
    {
        $accessToken = $this->getAccessToken();

        if (!$accessToken) {
            return false;
        }

        $requestHeader = [
            'authorization: Basic ' . $this->getAuthBasic(),
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'token'=> $accessToken,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$authHost . self::$checkPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = Json::decode($response, true);
            if ($requestResult && isset($requestResult['active'], $requestResult['scope'])) {
                if ($requestResult['active']) {
                    $this->setParam('scope', Json::encode($requestResult['scope']));
                }

                return true;
            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for authToken
     * @return string|null
     */
    public function authTokenRefresh($refresh_token)
    {
        $time = time();
        $requestHeader = [
            'authorization: Basic ' . $this->getAuthBasic(),
            'content-Type: application/x-www-form-urlencoded',
        ];

        $requestData = [
            'grant_type'=> 'refresh_token',
            'refresh_token' => $refresh_token,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post(self::$authHost . self::$tokenPath);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'], $requestResult['refresh_token'])) {
                $this->accessToken = $requestResult['access_token'];
                $this->accessTokenExpires = $time + $requestResult['expires_in'];
                $this->refreshToken = $requestResult['refresh_token'];

                return true;
            } elseif (!empty($requestResult->errorText)) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult->errorText);
                }
                $this->unsetParam(['access_token', 'access_token_expires', 'refresh_token']);
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function statementRequest()
    {
        $requestHeader = [
            "authorization: Bearer {$this->getAccessToken()}",
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLINFO_HEADER_OUT => true,
        ])->get(self::$apiHost.self::$statementPath.'?'.http_build_query([
            'accountNumber' => $this->getCurrentAccount()->rs,
            'from' => $this->getDateTimeFrom()->format('Y-m-d'),
            'till' => $this->getDateTimeTill()->format('Y-m-d'),
        ]));

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $requestResult = json_decode($response, true);

            if (isset($requestResult['accountNumber'])) {
                $this->_data = $requestResult;

                return true;
            } elseif (!empty($requestResult['errorMessage'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult['errorMessage']);
                }
                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }
        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    public function sendPaymentOrder()
    {
        $requestHeader = [
            "Authorization: Bearer {$this->getAccessToken()}",
            "Content-Type: application/json",
        ];

        $requestData = [
            'documentNumber' => (string) $this->paymentOrder->document_number,
            'amount' => bcdiv($this->paymentOrder->sum, 100, 2) * 1,
            'recipientName' => (string) $this->paymentOrder->contractor_name,
            'inn' => (string) ($this->paymentOrder->contractor_inn ? : 0),
            'kpp' => (string) ($this->paymentOrder->contractor_kpp ? : 0),
            'bankAcnt' => (string) $this->paymentOrder->contractor_current_account,
            'bankBik' => (string) $this->paymentOrder->contractor_bik,
            'accountNumber' => (string) ($this->currentAccount ? $this->currentAccount->rs : $this->paymentOrder->company_rs),
            'paymentPurpose' => (string) $this->paymentOrder->purpose_of_payment,
            'executionOrder' => (int) ($this->paymentOrder->ranking_of_payment ? : 5),
            'taxPayerStatus' => (string) ($this->paymentOrder->taxpayersStatus ? $this->paymentOrder->taxpayersStatus->code : '01'),
            'kbk' => (string) ($this->paymentOrder->kbk ? : 0),
            'oktmo' => (string) ($this->paymentOrder->oktmo_code ? : 0),
            'taxEvidence' => (string) ($this->paymentOrder->paymentDetails->code ?? 0),
            'taxPeriod' => (string) ($this->paymentOrder->tax_period_code ? : 0),
            'uin' => (string) ($this->paymentOrder->uin_code ? : 0),
            'taxDocNumber' => (string) ($this->paymentOrder->document_number_budget_payment ? : 0),
            'taxDocDate' => (string) ($this->paymentOrder->document_date_budget_payment ? : 0),
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
        ])->post(self::$apiHost.self::$paymentOrderPath);

        $this->debugLog($curl, __METHOD__);

        if ($curl->responseCode == 200) {
            $requestResult = json_decode($response, true);

            if (isset($requestResult['documentId'])) {
                $this->paymentOrder->updateAttributes([
                    'payment_order_status_id' => PaymentOrderStatus::STATUS_SENT_TO_BANK,
                    'payment_order_status_updated_at' => time(),
                ]);
                if (Yii::$app->id == 'app-frontend') {
                    $bankUrl = $this->bankPartner ? $this->bankPartner->url : null;
                    $msg = 'Платежное поручение успешно отправлено в клиент банк и находится в разделе "Черновики". ';
                    $msg .= ($bankUrl ? Html::a('Подтвердите платежку', $bankUrl, ['target' => '_blank']) : 'Подтвердите платежку');
                    $msg .= ', чтобы оплатить по счету.';
                    Yii::$app->session->setFlash('success', $msg);
                }
                return true;
            } elseif (!empty($requestResult['errorText'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult['errorText']);
                }
                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }
        if (Yii::$app->id == 'app-frontend') {
            Yii::$app->session->setFlash('error', $this->_errorMessage);
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Prepare vidimus from XML
     */
    public function vidimusPrepare()
    {
        $vidimuses = [];
        $data = $this->_data;
        $accountNumber = ArrayHelper::getValue($data, 'accountNumber');
        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => ArrayHelper::getValue($data, 'saldoIn'),
            'balanceEnd' => ArrayHelper::getValue($data, 'saldoOut'),
            'totalIncome' => ArrayHelper::getValue($data, 'income'),
            'totalExpense' => ArrayHelper::getValue($data, 'outcome'),
        ];

        foreach ((array) ArrayHelper::getValue($data, 'operation', []) as $operation) {
            if ($accountNumber == ArrayHelper::getValue($operation, 'payerAccount')) {
                $contractor = 'recipient';
                $contractorName = ArrayHelper::getValue($operation, 'recipient');
            } elseif ($accountNumber == ArrayHelper::getValue($operation, 'recipientAccount')) {
                $contractor = 'payer';
                $contractorName =  ArrayHelper::getValue($operation, 'payerName');
            } else {
                continue;
            }
            $inn = $contractor . 'Inn';
            $kpp = $contractor . 'Kpp';
            $bic = $contractor . 'Bic';
            $bank = $contractor . 'Bank';
            $rs = $contractor . 'Account';
            $date  = DateTime::createFromFormat('Y-m-d', ArrayHelper::getValue($operation, 'date'));
            $vidimuses[] = new Vidimus($this->company, [
                'date' => $date ? $date->format('d.m.Y') : '',
                'number' => (string) ArrayHelper::getValue($operation, 'id'),
                'total' => round(ArrayHelper::getValue($operation, 'amount'), 2),
                'contragent' => (string) $contractorName,
                'target' => (string) ArrayHelper::getValue($operation, 'paymentPurpose'),
                'flowType' => ($contractor == 'recipient')? Vidimus::FLOW_OUT: Vidimus::FLOW_IN,
                'inn' => (string) ArrayHelper::getValue($operation, $inn),
                'kpp' => (string) ArrayHelper::getValue($operation, $kpp),
                'bik' => (string) ArrayHelper::getValue($operation, $bic),
                'bankName' => (string) ArrayHelper::getValue($operation, $bank),
                'currentAccount' => (string) ArrayHelper::getValue($operation, $rs),
                'taxpayers_status' => (string) ArrayHelper::getValue($operation, 'creatorStatus', ''),
                'ranking_of_payment' => (string) ArrayHelper::getValue($operation, 'executionOrder', ''),
                'kbk' => (string) ArrayHelper::getValue($operation, 'kbk', ''),
                'oktmo_code' => (string) ArrayHelper::getValue($operation, 'oktmo', ''),
                'payment_details' => (string) ArrayHelper::getValue($operation, 'taxEvidence', ''),
                'tax_period_code' => (string) ArrayHelper::getValue($operation, 'taxPeriod', ''),
                'document_number_budget_payment' => (string) ArrayHelper::getValue($operation, 'taxDocNumber', ''),
                'document_date_budget_payment' => (string) ArrayHelper::getValue($operation, 'taxDocDate', ''),
                'payment_type' => (string) ArrayHelper::getValue($operation, 'taxType', ''),
                'uin_code' => (string) ArrayHelper::getValue($operation, 'uin', ''),
            ]);
        }

        return [$vidimuses, $statistic];
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        if ($this->isValidToken() && $this->statementRequest()) {
            list($vidimuses, $statistic) = $this->vidimusPrepare();

            return $this->loadStatementData($vidimuses, $statistic);
        }
    }

    public static function applicationRequest(ApplicationToBank $model)
    {
        $apiKey = Yii::$app->params['banking'][static::$alias]['apiKey'] ?? '';
        $apiSecret = Yii::$app->params['banking'][static::$alias]['apiSecret'] ?? '';
        $agentId = Yii::$app->params['banking'][static::$alias]['agentId'] ?? '';
        $requestHeader = [
            'Authorization: Partner-Basic api-key="'.$apiKey.'", api-secret="'.$apiSecret.'", agent-id="'.$agentId.'"',
            'Content-Type: application/json',
        ];

        $nameItemsArray = explode(' ', mb_ereg_replace('\s+', ' ', trim($model->fio)));
        $requestData = [
            'product' => 'РКО',
            'source' => 'Федеральные партнеры',
            'subsource' => 'API',
            'firstName' => isset($nameItemsArray[1]) ? $nameItemsArray[1] : '',
            'middleName' => isset($nameItemsArray[2]) ? $nameItemsArray[2] : '',
            'lastName' => isset($nameItemsArray[0]) ? $nameItemsArray[0] : '',
            'phoneNumber' => '+' . mb_ereg_replace("[^0-9]", "", $model->contact_phone),
            'email' => $model->contact_email,
            'companyName' => $model->company_name,
            'innOrOgrn' => $model->inn,
        ];

        $curl = new curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => json_encode($requestData),
        ])->post(self::$applicationUrl);

        if (Yii::$app->params['banking'][static::$alias]['debugMode'] ?? false) {
            $logFile = Yii::getAlias('@runtime/logs/debug_' . static::ALIAS . '.log');
            file_put_contents($logFile, __METHOD__ . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && isset($requestResult['success'])) {
                if ($requestResult['success']) {
                    return true;
                } elseif (Yii::$app->id == 'app-frontend' && !empty($requestResult['errorMessage'])) {
                    Yii::$app->session->setFlash('error', $requestResult['errorMessage']);
                }
            }
        }

        Yii::warning(__METHOD__ . "\n" . self::curlLogMsg($curl) . "\n", 'banking');

        return false;
    }
}
