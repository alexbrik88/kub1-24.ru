<?php

namespace frontend\modules\cash\modules\banking\modules\tinkoff\controllers;

use common\models\Company;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\BankingModulesBaseController;
use frontend\modules\cash\modules\banking\modules\tinkoff\models\BankModel;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `tinkoff` module
 *
 * АО «Тинькофф Банк»
 */
class DefaultController extends BankingModulesBaseController
{
    /**
     * @var frontend\modules\cash\modules\banking\models\AbstractBankModel
     */
    public $bankModelClass = BankModel::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'delete',
                            'request',
                            'payment',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => [
                            'set-autoload',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'return',
                            'pay-bill',
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($account_id = null, $redirect_url = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;
        $model->account_id = $account_id;
        $model->checkStartDate();

        if ($model->isValidToken()) {
            $model->scenario = BankModel::SCENARIO_REQUEST;

            return $this->render('request', ['model' => $model]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setAuthState();
            Url::remember($redirect_url ?? Url::current(), BankModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getAuthUrl());
        }

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionReturn()
    {
        $redirectUrl = Url::previous(BankModel::$alias.'ReturnRedirect');
        $isPayBill = strpos($redirectUrl, '/default/pay-bill') !== false;

        if (Yii::$app->user->isGuest && !$isPayBill) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->getModel(BankModel::SCENARIO_TOKEN, $isPayBill ? new Company : null);

        if (($code = Yii::$app->request->get('code')) && ($state = Yii::$app->request->get('state'))) {
            if ($model->getAuthState() && $model->load(['code' => $code, 'state' => $state], '') && $model->validate() && $model->authTokenRequest($isPayBill)) {
                Yii::$app->session->setFlash('success', "Вы успешно авторизованы.");
                if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company) {
                    \common\models\company\CompanyFirstEvent::checkEvent(Yii::$app->user->identity->company, 98, true);
                }
            } else {
                Yii::$app->session->setFlash('error', "Во время авторизации произошла ошибка.");
            }
        }

        return $this->redirect($redirectUrl ? : ['index']);
    }

    /**
     * @return string
     */
    public function actionRequest()
    {
        $model = $this->getModel(BankModel::SCENARIO_REQUEST);

        if ($model->isValidToken()) {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->statementRequest()) {
                return $this->renderContent($model->renderStatement());
            } else {
                return $this->render('request', ['model' => $model]);
            }
        } else {
            return $this->render('index', ['model' => $this->getModel(BankModel::SCENARIO_DEFAULT)]);
        }
    }

    /**
     * @return string
     */
    public function actionPayment($account_id = null, $po_id = null)
    {
        $this->layout = '@banking/views/layouts/payment';
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->account_id = $account_id;
        $model->paymentOrder = $this->findPaymentOrder($po_id);

        if ($model->isValidToken()) {
            $model->setScenario(BankModel::SCENARIO_PAYMENT_ORDER);
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return $this->render('@banking/views/all-banks/payment', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        } else {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->setAuthState();
                Url::remember(Url::current(), BankModel::$alias.'ReturnRedirect');

                return $this->redirect($model->getAuthUrl());
            }
        }

        return $this->render('@banking/views/all-banks/payment', ['model' => $model, 'sent' => false]);
    }

    /**
     * @return string
     */
    public function actionPayBill($uid)
    {
        $this->layout = '@banking/views/layouts/pay-bill';

        /* @var Invoice $model */
        $invoice = Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->byDeleted(false)
            ->one();

        if ($invoice === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = $this->getModel(BankModel::SCENARIO_DEFAULT, new Company(['inn' => $invoice->contractor_inn]));
        $model->payBill = $invoice;

        if ($model->getTmpToken()) {
            $model->setScenario(BankModel::SCENARIO_PAY_BILL);
            if ($model->validate()) {
                return $this->render('@banking/views/all-banks/pay-bill', [
                    'model' => $model,
                    'sent' => $model->sendPaymentOrder(),
                ]);
            }
        } else {
            $model->setAuthState();
            Url::remember(Url::current(), BankModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getAuthUrl());
        }
    }
}
