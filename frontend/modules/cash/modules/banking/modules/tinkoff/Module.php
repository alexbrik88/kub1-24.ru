<?php

namespace frontend\modules\cash\modules\banking\modules\tinkoff;

/**
 * API - Тинькофф
 *
 * tinkoff module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\tinkoff\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
