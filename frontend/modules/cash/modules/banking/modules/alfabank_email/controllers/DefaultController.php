<?php

namespace frontend\modules\cash\modules\banking\modules\alfabank_email\controllers;

use Yii;
use common\models\Company;
use yii\web\UploadedFile;
use frontend\rbac\UserRole;
use yii\filters\AccessControl;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\models\BankingEmail;
use frontend\modules\cash\modules\banking\models\BankingEmailFile;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\banking\components\BankingModulesBaseController;
use frontend\modules\cash\modules\banking\modules\alfabank_email\models\BankModel;

/**
 * АО «Альфа-Банк», г. Москва
 * Рассылка выписки по email
 */
class DefaultController extends BankingModulesBaseController
{
    public $layout = '@banking/modules/alfabank_email/views/layouts/index';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'confirm-email',
                            'drop-email',
                            'statements-list',
                            'upload-file',
                            'delete-file',
                            'toggle-autoload-mode'
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex($account_id = null)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;
        if ($account_id && $model->getCompanyAccounts()->andWhere(['id' => $account_id])->exists()) {
            $model->account_id = $account_id;
        }

        return $this->render('index', ['model' => $model]);
    }

    public function actionConfirmEmail()
    {
        $model = $this->getModel(BankModel::SCENARIO_CONFIRM_EMAIL);
        $model->showSecureText = true;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->createMailbox())
            {
                Yii::$app->session->setFlash('success', 'Email включен.');
            }

            $redirectUrl = Yii::$app->session->get('bankingRealReferrer', ['/cash/bank/index']);

            return $this->redirect($redirectUrl);
        }

        return $this->render('index', ['model' => $model]);
    }

    public function actionDropEmail($account_id)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;

        $checkingAccount = $model->getCompanyAccounts()->andWhere(['id' => $account_id])->one();
        if (!$checkingAccount)
            return self::showError('Р/с не определен!');

        $bankingEmail = BankingEmail::findOne(['checking_accountant_id' => $checkingAccount->id]);
        if (!$bankingEmail)
            return self::showError('E-mail не определен!');

        if ($model->dropMailbox($bankingEmail))
        {
            Yii::$app->session->setFlash('success', 'Email отключен.');
        }

        $redirectUrl = Yii::$app->session->get('bankingRealReferrer', ['/cash/bank/index']);

        return $this->redirect($redirectUrl);

        return $this->render('index', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionStatementsList($account_id)
    {
        $this->layout = '@banking/modules/alfabank_email/views/layouts/statements-list';

        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = false;
        $model->account_id = $account_id;

        $checkingAccount = $model->getCompanyAccounts()->andWhere(['id' => $account_id])->one();
        if (!$checkingAccount)
            return self::showError('Р/с не определен!');

        $bankingEmail = BankingEmail::findOne(['checking_accountant_id' => $checkingAccount->id]);
        if (!$bankingEmail)
            return self::showError('E-mail не определен!');

        $filesList = BankingEmailFile::find()
            ->select(['id', 'date', 'period_from', 'period_to', 'statements_count'])
            ->where(['banking_email_id' => $bankingEmail->id])
            ->andWhere(['is_uploaded' => 0])
            ->asArray()
            ->all();

        return $this->render('statements-list', [
            'model' => $model,
            'filesList' => $filesList,
            'bankingEmail' => $bankingEmail
        ]);
    }

    public function actionUploadFile($account_id)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = false;
        $model->account_id = $account_id;

        $checkingAccount = $model->getCompanyAccounts()->andWhere(['id' => $account_id])->one();
        if (!$checkingAccount)
            return self::showError('Р/с не определен!');

        $bankingEmail = BankingEmail::findOne(['checking_accountant_id' => $checkingAccount->id]);
        if (!$bankingEmail)
            return self::showError('E-mail не определен!');

        $files_ids = (array)Yii::$app->request->get('file_id');
        $uploadsDir = BankingEmailFile::getUploadsDir();

        $filesArr = [];
        foreach ($files_ids as $k => $id) {
            if ($fileModel = BankingEmailFile::findOne(['id' => $id, 'banking_email_id' => $bankingEmail->id])) {
                $file = $uploadsDir . DIRECTORY_SEPARATOR . $fileModel->file_name;
                if (is_file($file)) {
                    $filesArr[$fileModel->id] = $file;
                }
            }
        }

        if (!$filesArr) {
            return self::showError('Файл не найден');
        }

        $_summaryFileName = $uploadsDir . DIRECTORY_SEPARATOR . 'tmp_'.$account_id.'.txt';

        BankingEmailFile::createSummaryTmpFile($_summaryFileName, $filesArr);

        $file = new UploadedFile;
        $file->tempName = $_summaryFileName;

        // std vidimus
        $p = Yii::$app->request->get('p');
        $company = $model->getCompany();
        $vidimusBuilder = new VidimusBuilder($file, $company);
        $vidimuses = $vidimusBuilder->build();
        $vidimusDrawer = new VidimusDrawer($company, $vidimuses, $vidimusBuilder->uploadData());
        $vidimusDrawer->parentPage = $p;
        $vidimusDrawer->cancelButton = $model->getVidimusCancelButton();

        BankingEmailFile::deleteSummaryTmpFile($_summaryFileName);

        Yii::$app->session->set('uploadedBankingEmailFilesIds', array_keys($filesArr));

        return $vidimusDrawer->draw($vidimusBuilder->bankInfo, [
            '/cash/banking/default/upload',
            'is_from_banking_email' => 1,
            'p' => $p
        ]);
    }

    public function actionDeleteFile($account_id)
    {
        $this->layout = '@banking/modules/alfabank_email/views/layouts/statements-list';

        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->showSecureText = false;
        $model->account_id = $account_id;

        /** @var CheckingAccountant $checkingAccount */
        $checkingAccount = $model->getCompanyAccounts()->andWhere(['id' => $account_id])->one();
        if (!$checkingAccount) {
            return self::showError('Р/с не найден');
        }

        $bankingEmail = BankingEmail::findOne(['checking_accountant_id' => $checkingAccount->id]);
        if (!$bankingEmail) {
            return self::showError('E-mail не найден');
        }

        $files_ids = (array)Yii::$app->request->get('file_id');

        foreach ($files_ids as $file_id) {
            $fileModel = BankingEmailFile::findOne(['id' => $file_id, 'banking_email_id' => $bankingEmail->id]);
            if ($fileModel) {
                if (!$fileModel->delete()) {
                    return self::showError('Не удалось удалить файл');
                }
            }
        }

        $filesList = BankingEmailFile::find()
            ->select(['id', 'date', 'period_from', 'period_to', 'statements_count'])
            ->where(['banking_email_id' => $bankingEmail->id])
            ->andWhere(['is_uploaded' => 0])
            ->asArray()
            ->all();

        return $this->render('statements-list', [
            'model' => $model,
            'filesList' => $filesList,
            'bankingEmail' => $bankingEmail
        ]);
    }

    public function actionToggleAutoloadMode($account_id)
    {
        $model = $this->getModel(BankModel::SCENARIO_DEFAULT);
        $model->account_id = $account_id;

        /** @var CheckingAccountant $checkingAccount */
        $checkingAccount = $model->getCompanyAccounts()->andWhere(['id' => $account_id])->one();
        if (!$checkingAccount) {
            return self::showError('Р/с не найден');
        }

        $bankingEmail = BankingEmail::findOne(['checking_accountant_id' => $checkingAccount->id]);
        if (!$bankingEmail) {
            return self::showError('E-mail не найден');
        }

        $bankingEmail->updateAttributes([
            'autoload_mode' => ($bankingEmail->isAutoloadMode) ? 0 : 1
        ]);

        Yii::$app->session->setFlash('success',
            'Автозагрузка ' . ($bankingEmail->isAutoloadMode ? 'включена' : 'отключена'));

        // permanent refresh
        Yii::$app->session->set('alfa_refresh_statements_list', 0);

        return $this->redirect(['/cash/bank/index', 'rs' => $checkingAccount->rs]);
    }

    /**
     * @return BankModel
     */
    public function getModel($scenario, Company $company = null)
    {
        $model = new BankModel(Yii::$app->user->identity->company, ['scenario' => $scenario]);
        $this->view->params['bankingModel'] = $model;

        return $model;
    }

    public static function showError($msg)
    {
        $supportEmail = Yii::$app->params['emailList']['support'];
        $msg .= '<br/>';
        $msg .= "Попробуйте повторить попытку или обратитесь в службу технической поддержки <a href=\"mailto:$supportEmail\">$supportEmail</a></p>";
        return \yii\helpers\Html::tag('div', $msg, ['class' => 'alert alert-danger mb-0']) .
            \yii\helpers\Html::tag('button',
                '<svg class="svg-icon"><use xlink:href="/img/svg/svgSprite.svg#close"></use></svg>',
                ['class' => 'modal-close close', 'data-dismiss' => 'modal']);
    }
}
