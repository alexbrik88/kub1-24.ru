<?php

namespace frontend\modules\cash\modules\banking\modules\alfabank_email;

/**
 * API - АО «Альфа-Банк», г. Москва
 *
 * alfabank module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\alfabank_email\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
