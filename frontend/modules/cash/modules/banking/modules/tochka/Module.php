<?php

namespace frontend\modules\cash\modules\banking\modules\tochka;

/**
 * API - Ф ТОЧКА БАНК КИВИ БАНК (АО)
 *
 * tochka module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\banking\modules\tochka\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
