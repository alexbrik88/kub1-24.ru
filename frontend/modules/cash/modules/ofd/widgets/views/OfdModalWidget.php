<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $modalOptions array */
/* @var $pjaxOptions array */
/* @var $content mixed */

$js = <<<JS
var vidimusSubmitClicked = false;

$(document).on("click", ".ofd-module-open-link", function(e) {
    e.preventDefault();
    $.pjax({url: this.href, container: "#ofd_module_pjax", push: $("#ofd-module-modal").data("push-state")});
    $("#ofd-module-modal").modal("show");
});

$(document).on("pjax:success", "#ofd_module_pjax",  function(event){
    if ($('.add-xls-file-2').length)
        initXlsUpload2();
  }
);

$(document).on("change", "#ofdmodel-account_id", function (e) {
    $('span.ofd-accoun-select-loader').removeClass('hidden');
    $.pjax({
        url: $("option:selected", this).data("url"),
        container: "#ofd_module_pjax",
        push : $("#ofd-module-modal").data("push-state"),
    });
});
$(document).on("ajaxComplete", function(){
    $('span.ofd-accoun-select-loader').addClass('hidden');
});
$(document).on("hide.bs.modal", "#ofd-module-modal", function (e) {
    var ofdModal = $(this);
    if (ofdModal.data("push-state")) {
        history.pushState({}, ofdModal.data("page-title"), ofdModal.data("page-url"));
    }
});

$(document).on("hidden.bs.modal", "#ofd-module-modal", function (e) {
    if (!$(e.target).attr('id') == 'delete-confirm') {
        $("#ofd_module_pjax", this).html("");
    }
});

$(document).on("click", "button.statement-form-submit", function(e) {
    if (vidimusSubmitClicked) {
        e.preventDefault();
    } else {
        vidimusSubmitClicked = true;
    }
    setTimeout(vidimusSubmitClicked = false, 1000);
});
JS;

$this->registerJs($js);
?>

<?= Html::beginTag('div', $modalOptions); ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <?php Pjax::begin($pjaxOptions); ?>

            <?= $content ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
<?= Html::endTag('div') ?>