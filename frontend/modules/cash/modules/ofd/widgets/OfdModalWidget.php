<?php

namespace frontend\modules\cash\modules\ofd\widgets;

use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class OfdModalWidget extends Widget
{
    public static $rendered = false;

    /**
     * Show the modal on page load
     * @var boolean
     */
    public $showModal = false;
    /**
     * whether to enable push state
     * @var boolean
     */
    public $enablePushState = true;
    /**
     * Onclose page title
     * @var string
     */
    public $pageTitle;
    /**
     * Onclose page url
     * @var string
     */
    public $pageUrl;

    public function run()
    {
        return OfdModalWidget::$rendered ? null : $this->runWidget();
    }

    public function runWidget()
    {
        OfdModalWidget::$rendered = true;

        $enablePushState = $this->enablePushState ? 'true' : 'false';

        $modalOptions = [
            'id' => 'ofd-module-modal',
            'class' => 'modal fade',
            'role' => 'modal',
            'data-push-state' => $enablePushState,
            'data-page-title' => $this->pageTitle,
            'data-page-url' => $this->pageUrl ?: Url::to(Ofd::currentRoute()),
        ];

        $pjaxOptions = [
            'id' => 'ofd_module_pjax',
            'enablePushState' => $this->enablePushState,
            'enableReplaceState' => false,
            'timeout' => 20000,
            'linkSelector' => '.ofd-module-link',
            'options' => [
                'data-push-state' => $enablePushState,
            ]
        ];

        $view = $this->view;
        $content = ArrayHelper::getValue($view->params, 'ofdContent', '');

        if ($content || $this->showModal) {
            $view->registerJs('
                $("#ofd-module-modal").modal("show");
            ');
        }

        return $this->render('OfdModalWidget', [
            'modalOptions' => $modalOptions,
            'pjaxOptions' => $pjaxOptions,
            'content' => $content,
        ]);
    }
}
