<?php

namespace frontend\modules\cash\modules\ofd;

use Yii;

/**
 * ofd module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\ofd\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@ofd', '@frontend/modules/cash/modules/ofd');

        foreach (components\Ofd::$modelClassArray as $class) {
            $alias = $class::$alias;
            $this->setModule($alias, [
                'class' => "frontend\\modules\\cash\\modules\\ofd\\modules\\{$alias}\\Module",
            ]);
        }

    }
}
