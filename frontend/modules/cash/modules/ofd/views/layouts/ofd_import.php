<?php
/**
 * @var $this  yii\web\View
 */

use common\components\ImageHelper;
use common\models\dictionary\bik\BikDictionary;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$ofdModel = ArrayHelper::getValue($this->params, 'ofdModel');

$ofdUrl =
    Url::to([
        "/cash/ofd/{$ofdModel::$alias}/default/import",
        'p' => Yii::$app->request->get('p'),
    ]);

$this->beginContent('@frontend/modules/cash/modules/ofd/views/layouts/main.php');
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Выберите провайдера отправки отчетности</h1>
</div>
<div class="modal-body">

    <div class="statement-service-content" style="position: relative; min-height: 110px;">
        <?php if ($ofdModel) : ?>
            <div style="margin: 0 -5px 10px;">
                <div class="cont-img_bank-logo" style="float:left">
                    <img class="bank-logo" style='padding-right: 15px;' src="/img/icons/<?=$ofdModel::$alias?>.png" width="150" height="100"/>
                </div>
                <?php $ofdName = Html::encode($ofdModel::NAME); ?>
                <div id="statement-bank-info" data-name="<?= $ofdName ?>" style="margin-left: 160px; padding: 5px;">
                    <?php if ($ofdModel->showSecureText) : ?>
                        <div style="padding: 6px 10px; font-size: 10px; line-height: 18px; background-color: #eee; min-height: 100px">
                            Для обеспечения безопасности данных используется протокол зашифрованного соединения SSL
                            - надежный протокол для передачи конфиденциальной банковской информации
                            и соблюдаются требования международного стандарта PCI DSS по хранению и передаче
                            конфиденциальной информации в банковской сфере.
                        </div>
                    <?php else : ?>
                        <div style="text-align: right;">
                            <p style="font-weight: bold;">
                                <?= $ofdName ?><br>
                            </p>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <hr/>
        <?php endif ?>

        <?= Alert::widget(); ?>

        <?php echo $content ?>

        <div class="statement-loader"></div>
    </div>
</div>

<?php $this->endContent(); ?>
