<?php

use yii\helpers\Html;

/* @var $cancelUrl array|string */

?>

<div class="clearfix">
    <?= Html::submitButton('Подтвердить интеграцию', ['class' => 'btn btn-primary dis-none-bank']) ?>
    <?= Html::submitButton('<i class="fa fa-sign-in fa-2x" aria-hidden="true"></i>', [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg',
        'style' => 'width: 205px !important; color: white; float: left;',
    ]) ?>

    <?= Html::a('Отменить', $cancelUrl, [
        'class' => 'btn btn-primary ofd-module-link dis-none-bank',
        'style' => 'width: 205px !important; float:right;',
    ]) ?>
    <?= Html::a('<i class="fa fa-reply fa-2x"></i>', $cancelUrl, [
        'class' => 'btn darkblue widthe-100 hidden-lg back dis-none-bank-lg ofd-module-link',
        'style' => 'float:right; width: 100px !important; color: white;',
    ]) ?>
</div>
