<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$cashbox_id = Yii::$app->request->get('cashbox_id');
$formData = Html::hiddenInput('className', 'CashOrderFlows') . Html::hiddenInput('cashbox', $cashbox_id);
?>

<div class="row">
    <div class="col-sm-12" style="margin-top: 20px;">
        <div class="xls-error-2"></div>
        <div class="row">
            <?php ActiveForm::begin([
                'action' => '#',
                'method' => 'post',
                'id' => 'xls-ajax-form-2',
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>
            <?= $formData; ?>
            <?= Html::hiddenInput('createdModels', null, [
                'class' => 'created-models-2',
            ]); ?>
            <div class="file-list-2 col-sm-12">
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
    <div class="row action-buttons buttons-fixed xls-buttons-2"
         style="padding-top: 25px;margin-right: 0px;margin-left: 0px;display: none;">
        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
             style="text-align: left;width: 50%">
            <?= Html::a('Сохранить', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs undelete-models',
            ]); ?>
            <?= Html::a('<i class="fa fa-floppy-o fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-lg undelete-models',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-6 col-xs-6"
             style="text-align: right;width: 50%;">
            <?= Html::a('Отменить', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs xls-close',
            ]); ?>
            <?= Html::a('<i class="fa fa-reply fa-2x"></i>', 'javascript:;', [
                'class' => 'btn darkblue widthe-100 hidden-lg xls-close',
            ]); ?>
        </div>
    </div>
    <div class="col-sm-12">
        <div id="progressBox2"></div>
    </div>
</div>