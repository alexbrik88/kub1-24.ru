<?php
use frontend\modules\cash\modules\ofd\components\Ofd;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\XlsHelper;

$uploadXlsTemplateUrl = Url::to(['/xls/download-template', 'type' => XlsHelper::CASH_ORDER]);

$ofdUrl = Url::to([
        '/cash/ofd/default/select',
        'p' => Yii::$app->request->get('p'),
    ]);

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h1>Загрузка из ОФД</h1>
</div>
<div class="modal-body" id="ofd-upload">
    <div class="row">
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div>
                <a href="#" class="btn yellow add-xls-file-2">
                    <i class="fa fa-plus-circle"></i>
                    Загрузка файла
                </a>
            </div>
            <div class="upload-1C-hide" style="margin-top: 24px;">
                Заполните шаблон данными из ОФД и загрузите его в КУБ.
            </div>
        </div>
        <div class="col-sm-6" style="margin-bottom: 24px">
            <div class="upload-xls-button-2 upload-1C-show" style="text-align: right;">
                <a href="#" class="btn darkblue disabled" data-pjax="0" style="color:#fff">
                    <i class="fa fa-download"></i>
                    Загрузить
                </a>
            </div>
            <div class="upload-1C-hide" style="text-align: right;">
                <a href="<?= $ofdUrl ?>" class="btn yellow no-padding ofd-module-link">
                    <i class="fa fa-money m-r-sm"></i> Загрузить из ОФД
                </a>
            </div>
            <div class="upload-1C-hide" style="margin-top: 24px;">
                КУБ уже интегрирован с несколькими ОФД.
                Если у вас касса в этом ОФД,
                то операции по кассе будут загружаться напрямую из ОФД.
            </div>
            <div id="bank-info" style="text-align: right;"></div>
        </div>
    </div>

    <?= Html::a('Скачать шаблон таблицы', $uploadXlsTemplateUrl, [
        'class' => 'upload-xls-template-url',
    ]); ?>

    <?= $this->render('_import_xls') ?>
</div>

<script>
    $(document).on('click', '.upload-xls-button-2 a', function (e) {
        e.preventDefault();
        $('#ofd-upload').find('.file-list-2 .item').remove();
        $('#ofd-upload').find('.upload-xls-button-2 a').addClass('disabled');
        $('#ofd-upload').find('.add-xls-file-2').removeClass('disabled');
        $('#ofd-upload').find('#xls-ajax-form-2').submit();
    });
    $('#ofd-upload .file-list').on('click', '.delete-file-2', function (e) {
        e.preventDefault();
        $(this).parent().remove();
        $('#ofd-upload').find('.xls-title').show();
        vidimusUploader['_queue'].splice(0, 1);
        $('#ofd-upload').find('.upload-xls-template-url').show();
        $('#ofd-upload').find('.upload-xls-button-2 a').addClass('disabled');
        $('#ofd-upload').find('.add-xls-file-2').removeClass('disabled');
    });
</script>