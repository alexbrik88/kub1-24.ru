<?php

namespace frontend\modules\cash\modules\ofd\components;

use Yii;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusUploader;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Default controller for the `ofd` module
 */
class OfdBaseController extends Controller
{
    public $layoutWrapperCssClass;
    public $layout = '@frontend/modules/cash/modules/ofd/views/layouts/main';

    /**
     * Run this route if the request is not Ajax
     * @var array|string
     */
    public $pageRoute = 'cash/order/index';
    public $pageRouteParams = [];

    /**
     * @var array
     */
    public $notAjaxAction = [
        'return',
        'upload',
        'pay-bill',
        'registration',
        'delete'
    ];

    /**
     * This method is invoked right after an action is executed.
     * @param  yii\base\Action $action
     * @param  mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        /**
         * If the request is not Ajax rendering parent page
         */
        if (!Yii::$app->request->isAjax && !in_array($action->id, $this->notAjaxAction)) {
            $domDocument = new \DOMDocument();
            @$domDocument->loadHTML($result);
            $ofdElement = $domDocument->getElementById('ofd_module_pjax');

            $ofdContent = "";
            $innerNodes = $ofdElement->childNodes;
            for ($i = 0; $i < $innerNodes->length; $i++) {
                $ofdContent .= $domDocument->saveHTML($innerNodes->item($i));
            }
            $this->view->params['ofdContent'] = $ofdContent;

            try {
                $params = Ofd::routeDecode(Yii::$app->request->get('p'));
                $route = array_shift($params);
                Yii::$app->request->setQueryParams(array_merge(Yii::$app->request->getQueryParams(), $params));
                $result = Yii::$app->runAction($route, $params);
            } catch (\Exception $e) {
                $result = Yii::$app->runAction($this->pageRoute, $this->pageRouteParams);
            }
        }

        return $result;
    }

    public function isUserCashbox($cashbox_id)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return $user->getCashboxes()->andWhere(['cashbox.id' => $cashbox_id])->exists();
    }

}
