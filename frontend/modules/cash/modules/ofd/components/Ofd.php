<?php

namespace frontend\modules\cash\modules\ofd\components;

use Yii;
use frontend\modules\cash\modules\ofd\modules;

/**
 * Default controller for the `ofd` module
 */
class Ofd
{
    const ROUTE_ORDER_INDEX = 0;
    const ROUTE_TAX_ROBOT_BANK = 1;
    const ROUTE_TAX_DECLARATION = 2;
    const ROUTE_REPORTS_OPTIONS = 3;
    const ROUTE_CASH_OPERATIONS = 4;

    /**
     * Модели интегрированных API, для которых доступно получение выписки
     * @var array
     */
    public static $modelClassArray = [
        modules\taxcom\models\OfdModel::class,
    ];

    /**
     * Страницы, где есть API ОФД
     * @var array
     */
    public static $routeArray = [
        self::ROUTE_ORDER_INDEX => '/cash/order/index',
        self::ROUTE_TAX_ROBOT_BANK => '/tax/robot/bank',
        self::ROUTE_TAX_DECLARATION => '/tax/declaration/view',
        self::ROUTE_REPORTS_OPTIONS => '/reports/options/statements',
        self::ROUTE_CASH_OPERATIONS => '/cash/default/operations'
    ];

    public static function routeEncode(array $url)
    {
        if (isset($url[0]) && ($i = array_search($url[0], self::$routeArray)) !== false) {
            $url[0] = $i;
        }

        return base64_encode(json_encode($url));
    }

    public static function routeDecode($url, $default = null)
    {
        $url = json_decode(base64_decode($url), true);
        if (isset($url[0])) {
            if (is_numeric($url[0]) && isset(self::$routeArray[$url[0]])) {
                $url[0] = self::$routeArray[$url[0]];
            }

            return $url;
        }

        return $default;
    }

    public static function currentRouteEncode()
    {
        $route = Yii::$app->getRequest()->getQueryParams();

        if (isset($route['p'])) {
            return $route['p'];
        }

        $route[0] = '/' . Yii::$app->controller->getRoute();

        return self::routeEncode($route);
    }

    public static function currentRoute()
    {
        $route = Yii::$app->getRequest()->getQueryParams();

        if (isset($route['p'])) {
            return self::routeDecode($route['p']);
        }

        $route[0] = '/' . Yii::$app->controller->getRoute();

        return $route;
    }

    public static function isCashOperationsPage()
    {
        if ($params = (array)self::routeDecode(\Yii::$app->request->get('p'))) {
            if (isset($params[0]))
                if (strpos($params[0], 'cash/default') !== false || strpos($params[0], 'analytics/finance') !== false) {
                    return true;
                }
        }

        return false;
    }
}