<?php

namespace frontend\modules\cash\modules\ofd\components;

use common\models\Company;
use frontend\modules\cash\modules\ofd\models\AbstractOfdModel;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;

/**
 * Default controller for the `ofd` module
 */
class OfdModulesBaseController extends OfdBaseController
{
    /**
     * @var string
     */
    public $layout = '@ofd/views/layouts/ofd';

    /**
     * @var AbstractOfdModel
     */
    public $ofdModelClass;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'delete'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => ['set-autoload'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * Redirect to needed page after authorize on ofd page
     * @param $action
     * @return bool|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $urlKey = 'ofdAfterLoginRedirectUrl';

        if ($action->id == 'index') {
            if (Yii::$app->request->get($urlKey)) {

                Yii::$app->session->set($urlKey, Yii::$app->request->get($urlKey));

            } elseif (Yii::$app->session->get($urlKey)) {

                return $this->redirect(Yii::$app->session->get($urlKey));

            } else {

                Yii::$app->session->remove($urlKey);
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * set statement autoload mode
     */
    public function actionSetAutoload($accountId)
    {
        $account = Yii::$app->user->identity->company->getCheckingAccountants()->andWhere(['id' => $accountId])->one();
        if ($account !== null) {
            $account->autoload_mode_id = Yii::$app->request->post('autoload') ? : null;
            $account->save(true, ['autoload_mode_id']);
        }

        return;
    }


    /**
     * @param $scenario
     * @param Company|null $company
     * @return OfdModel
     */
    public function getModel($scenario, Company $company = null)
    {
        $model = $this->view->params['ofdModel'] = new $this->ofdModelClass(
            $company ? : Yii::$app->user->identity->company,
            ['scenario' => $scenario]
        );

        return $model;
    }

    /**
     * @return string
     */
    public function actionDelete()
    {
        $condition = [
            'company_id' => Yii::$app->user->identity->company->id,
            'ofd_alias' => $this->ofdModelClass::$alias,
        ];
        $hasData = OfdParams::find()->where($condition)->exists();

        if (Yii::$app->request->isPost) {
            if ($hasData) {
                if (OfdParams::deleteAll($condition)) {
                    $hasData = false;
                    Yii::$app->session->setFlash('success', 'Интеграция успешно удалена.');
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось удалить интеграцию.');
                }
            } else {
                Yii::$app->session->setFlash('success', 'Данные интеграции отсутствуют.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
