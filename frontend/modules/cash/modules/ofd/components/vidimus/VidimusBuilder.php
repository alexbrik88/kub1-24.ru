<?php

namespace frontend\modules\cash\modules\ofd\components\vidimus;

use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use frontend\models\ContractorSearch;
use frontend\modules\cash\models\CashContractorType;
use yii\base\BaseObject;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * Class VidimusBuilder
 * @package frontend\modules\cash\modules\ofd\components\vidimus
 */
class VidimusBuilder extends BaseObject
{
    /**
     * @var UploadedFile
     */
    private $file;
    /**
     * @var UploadedFile
     */
    private $_fileDescriptor;
    /**
     * @var Company
     */
    private $company;
    /**
     * @var integer
     */
    private $cashbox_id;
    /**
     * @var
     */
    private $vidimuses;

    /**
     * @var
     */
    private $counter;

    /**
     * @var string | int ИНН текущей компании
     */
    private $company_inn;

    /**
     * @var array Данные по платежной операции
     */
    private $operation_data;

    private $currentLineNumber;
    private $documentLineNumber;

    /**
     * VidimusBuilder constructor.
     * @param $files
     * @param $company
     */
    public function __construct(UploadedFile $file = null, Company $company = null, $cashbox_id = null)
    {
        $this->file = $file;
        $this->company = $company;
        $this->cashbox_id = $cashbox_id;
    }

    /**
     * tmpfile descriptor
     * @param $descriptor
     */
    public function setFileDescriptor($descriptor)
    {
        $this->_fileDescriptor = $descriptor;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function build()
    {
        if ($this->_fileDescriptor === null) {
            if (!is_file($this->file->tempName)) {
                throw new \Exception('Uploaded files could not be empty');
            }

            $this->_fileDescriptor = fopen($this->file->tempName, 'r');

            if (!$this->_fileDescriptor) {
                throw new \Exception('Failed to open file');
            }
        }

        $this->counter = 0;
        $this->currentLineNumber = 0;
        $this->documentLineNumber = 0;

        while (($line = fgets($this->_fileDescriptor)) !== false) {
            $this->currentLineNumber++;
            $line = trim(mb_convert_encoding($line, 'UTF-8', ['CP1251', 'UTF-8']));
            // Обработка строки
            $this->lineProcess($line);
        }

        fclose($this->_fileDescriptor);

        $this->findDublicates();

        return $this->vidimuses;
    }

    /**
     * @param array $vidimuses
     * @return array
     */
    public function handlingVidimus($vidimuses)
    {
        $this->vidimuses = $vidimuses;
        $this->findDublicates();

        return $this->vidimuses;
    }

    /**
     *
     * Find flow dublicates
     */
    private function findDublicates()
    {
        foreach ((array) $this->vidimuses as $key => $vidimuse) {

            $this->vidimuses[$key]->new = false;

            $this->vidimuses[$key]->dublicate = CashOrderFlows::find()->andWhere([
                'flow_type' => $vidimuse->flowType == Vidimus::FLOW_IN ?
                                CashFlowsBase::FLOW_TYPE_INCOME :
                                CashFlowsBase::FLOW_TYPE_EXPENSE,
                'company_id' => $this->company->id,
                'cashbox_id' => $this->cashbox_id,
                'date' => date('Y-m-d', strtotime($vidimuse->date)),
                'amount' => $vidimuse->total,
            ])->exists();
        }
    }

    /**
     * Обработка строк/блока операции из файла
     *
     * @param $line string Обрабатываемая строка
     */
    private function lineProcess($line)
    {
        return;
    }

}
