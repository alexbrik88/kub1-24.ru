<?php

namespace frontend\modules\cash\modules\ofd\components\vidimus;

use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\TaxKbk;
use yii\base\BaseObject;

/**
 * Class Vidimus
 * @package frontend\modules\cash\modules\ofd\components\vidimus
 */
class Vidimus extends BaseObject
{
    const FLOW_IN = 'in';
    const FLOW_OUT = 'out';

    public $cahbox_id;
    public $number;
    public $total;
    public $date;
    public $flowType;

    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $new;

    /**
     * @var
     */
    public $dublicate;

    /**
     * @var
     */
    public $doInsert = true;

    /**
     * @var
     */
    public $error;

    /**
     * @var
     */
    public $disabled = false;

    public static $attributeErrorList = [
        'number' => 'Номер не указан',
        'total' => 'Сумма не указана',
        'date' => 'Дата не указана',
        'flowType' => 'Не удалось определить тип платежа',
    ];

    public static $disableIfEmpty = [
        'number',
        'total',
        'date',
        'flowType',
    ];

    /**
     * @inheritdoc
     */
    public function checkRequiredParam()
    {
        foreach (self::$attributeErrorList as $key => $value) {
            if (empty($this->$key)) {
                $this->error = $value;
                $this->disabled = in_array($key, self::$disableIfEmpty);
                break;
            }
        }
    }


}
