<?php
namespace frontend\modules\cash\modules\ofd\components\vidimus;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderStatementUpload;
use common\models\cash\form\CashBankFlowsForm;
use common\models\company\CompanyType;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\modules\ofd\components\Ofd;
use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * Class VidimusUploader
 * @package frontend\modules\cash\modules\ofd\components\vidimus
 *
 * @property integer $uploadedCount
 * @property integer $errorsCount
 */
class VidimusUploader extends BaseObject
{
    public $from;
    public $till;

    /**
     * @var integer
     */
    protected $_errors_count = 0;
    protected $_uploaded_count = 0;

    protected $_company;
    protected $_cashbox;
    protected $_cashbox_id;

    protected static $prefixArray = [
        '№ ?',
        'no? ?',
        'номеру? ?',
        'счету? ?',
        'сч\.? ?',
    ];

    /**
     * VidimusUploader constructor.
     * @param $company Company
     */
    public function __construct(Company $company, $cashbox_id)
    {
        $this->_company = $company;
        $this->_cashbox_id = $cashbox_id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Company
     */
    public function getCashboxId()
    {
        return $this->_cashbox_id;
    }

    public function upload($vidimus, $source, $statistic = [], $service)
    {
        $sources = array_combine(CashOrderStatementUpload::$sources, CashOrderStatementUpload::$sources);
        $services = [];
        foreach (Ofd::$modelClassArray as $class) {
            $services[$class::$alias] = $class::$alias;
        }
        $this->_uploaded_count = 0;
        $errors_count = [
            'isset_upload' => 0,
            'isset_contragent' => 0,
            'isset_bank' => 0,
            'model' => 0,
            'contractor' => 0,
            'contragent_type' => 0,
            'not_upload' => 0,
            'payment_exist' => 0,
            'all' => 0
        ];

        // LOG
        $dateFrom = isset($statistic['dateStart']) ? date_create_from_format('d.m.Y', $statistic['dateStart']) : null;
        $dateTill = isset($statistic['dateEnd']) ? date_create_from_format('d.m.Y', $statistic['dateEnd']) : null;
        $statementUpload = new CashOrderStatementUpload;
        $statementUpload->company_id = $this->company->id;
        $statementUpload->cashbox_id = $this->_cashbox_id;
        $statementUpload->service = ArrayHelper::getValue($services, $service);
        $statementUpload->source = ArrayHelper::getValue($sources, $source, CashOrderStatementUpload::SOURCE_OFD);
        $statementUpload->period_from = $dateFrom ? $dateFrom->format('Y-m-d') : null;
        $statementUpload->period_till = $dateTill ? $dateTill->format('Y-m-d') : null;
        $statementUpload->saved_count = 0;

        LogHelper::save($statementUpload, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE);

        foreach ($vidimus as $vidimu) {
            if (empty($vidimu['upload'])) {
                continue;
            }

            if ($vidimu['upload']) {
                $model = new CashOrderFlows([
                    'company_id' => $this->company->id,
                ]);

                $currNum = $model::getNextNumber($this->company->id, $vidimu['flow_type'] == 'out' ? 0 : 1, $vidimu['date']);

                $model->date = date('d.m.Y', strtotime($vidimu['date']));
                $model->author_id = Yii::$app->user->id;
                $model->cashbox_id = $this->_cashbox_id;
                $model->number = $currNum;
                $model->flow_type = ($vidimu['flow_type'] == 'out') ? 0 : 1;
                $model->is_accounting = 1;
                $model->has_invoice = 0;
                $model->contractorInput = 'customers';
                $model->amount = round ($vidimu['total'] / 100, 2);
                $model->description = 'Смена №' . $vidimu['number'] . ', Оплата от покупателей';
                $model->is_taxable = 1;

                if ($vidimu['flow_type'] == 'in') {
                    $model->income_item_id = 1; // Оплата от покупателя
                } else {
                    $model->expenditure_item_id = 14; // Возврат
                }

                if ($model->save()) {
                    $this->_uploaded_count++;
                } else {
                    $errors_count['model']++;
                }
            } else {
                $errors_count['not_upload']++;
            }

        }

        if (!$statementUpload->isNewRecord) {
            $statementUpload->updateAttributes([
                'saved_count' => $this->_uploaded_count,
            ]);
        }

        foreach ($errors_count as $item) {
            $this->_errors_count += $item;
        }

        return $this->_uploaded_count;
    }

    public function getUploadedCount()
    {
        return $this->_uploaded_count;
    }

    public function getErrorsCount()
    {
        return $this->_errors_count;
    }

    /**
     * @param $name
     * @param $kpp
     * @return int
     */
    private function getTypeName($name, $kpp = null)
    {
        $contractorType = null;
        $contractorName = '';
        $typeArray = CompanyType::find()->all();

        foreach ($typeArray as $type) {
            if ($type->name_short && mb_strstr($name, $type->name_short)) {
                $contractorType = $type->id;
                $contractorName = mb_ereg_replace($type->name_short, '', $name);
                break;
            } elseif ($type->name_full && mb_stristr($name, $type->name_full) !== false) {
                $contractorType = $type->id;
                $contractorName = mb_eregi_replace($type->name_full, '', $name);
                break;
            }
        }

        if ($contractorName === '') {
            $contractorName = $name;
        }

        $contractorName = mb_substr(trim($contractorName, ' \t\n\r\0\x0B\"\''), 0, 255);

        return [$contractorType, $contractorName];
    }

    /**
     * @param array $vidimu
     * @return Contractor
     */
    public function getContractor($vidimu)
    {
        $contractor_type = ($vidimu['flow_type'] == 'out') ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;
        $contragent = (array) ArrayHelper::getValue($vidimu, 'contragent', []);
        $name = trim(ArrayHelper::getValue($contragent, 'name', ''));
        $inn = trim(ArrayHelper::getValue($contragent, 'inn', ''));
        $kpp = trim(ArrayHelper::getValue($contragent, 'kpp', ''));

        $model = VidimusBuilder::getVidimuseContractor($inn, $this->company->id, $name, $kpp, $contractor_type);

        if ($model === null) {
            $model = new Contractor();
            $model->loadDefaultValues();

            if ($contractorAttributes = DadataClient::getContractorAttributes($inn, $kpp)) {
                $model->setAttributes($contractorAttributes);
            } else {
                list($type_id, $contractorName) = $this->getTypeName($name);
                $model->name = $contractorName;
                $model->PPC = $kpp;
                $model->ITN = $inn;
                $model->company_type_id = $type_id;
                $model->face_type = ($type_id || strlen($inn) === 10) ?
                                         Contractor::TYPE_LEGAL_PERSON :
                                         Contractor::TYPE_PHYSICAL_PERSON;
                $model->director_name = ($type_id == CompanyType::TYPE_IP) ?
                                             $contractorName :
                                             Contractor::DEFAULT_DIRECTOR_NAME;
                $model->legal_address = Contractor::DEFAULT_LEGAL_ADDRESS;

                if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
                    $physical_name = explode(' ', $contractorName);
                    $model->physical_lastname = ArrayHelper::getValue($physical_name, 0, '');
                    $model->physical_firstname = ArrayHelper::getValue($physical_name, 1, '');
                    $model->physical_patronymic = ArrayHelper::getValue($physical_name, 2, '');
                    if (!$model->physical_patronymic) {
                        $model->physical_no_patronymic = true;
                    }
                }
            }

            $model->populateRelation('company', $this->company);
            $model->company_id = $this->company->id;
            $model->type = ($vidimu['flow_type'] == 'out') ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;

            $model->bank_name = ArrayHelper::getValue($contragent, 'bankName', '');
            $model->BIC = ArrayHelper::getValue($contragent, 'bik', '');
            $model->corresp_account = ArrayHelper::getValue($contragent, 'correspAccount', '');
            $model->current_account = ArrayHelper::getValue($contragent, 'currentAccount', '');

            $model->chief_accountant_is_director = true;
            $model->contact_is_director = true;
            $model->taxation_system = Contractor::WITHOUT_NDS;
        }
        if ($vidimu['flow_type'] == 'out' && $vidimu['vidimus_type']) {
            $model->invoice_expenditure_item_id = ((int) $vidimu['vidimus_type']) ? : null;
        }
        if ($model->company_id && ($model->isAttributeChanged('invoice_expenditure_item_id') || $model->getIsNewRecord())) {
            $model->save(false);
        }

        return $model;
    }

    /**
     *
     */
    public function getStartBalanceFlow($amount, $date)
    {

        $model = new CashBankFlowsForm([
            'scenario' => 'create',
            'company_id' => $this->company->id,
            'contractor_id' => CashContractorType::BALANCE_TEXT,
            'amount' => $amount,
            'rs' => $this->account->rs,
            'date' => $date->format('d.m.Y'),
            'description' => "Остаток на конец {$date->format('d.m.Y')}г.",
            'bank_name' => $this->account->bank_name,
            'flow_type' => CashBankFlowsForm::FLOW_TYPE_INCOME,
            'income_item_id' => InvoiceIncomeItem::ITEM_OTHER,
        ]);

        return $model;
    }
}
