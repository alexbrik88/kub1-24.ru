<?php
namespace frontend\modules\cash\modules\ofd\components\vidimus;

use common\components\TextHelper;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashOrderStatementUpload;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\TaxKbk;
use Yii;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * Class VidimusDrawer
 * @package frontend\modules\cash\modules\ofd\components\vidimus
 */
class VidimusDrawer
{
    private $company;
    private $vidimuses;
    private $cashboxId;
    private $html = '';

    public $dateStart;
    public $dateEnd;
    public $balanceStart;
    public $balanceEnd;
    public $totalIncome;
    public $totalExpense;
    public $parentPage;

    /**
     * VidimusDrawer constructor.
     * @param $vidimuses
     */
    public function __construct(Company $company, $vidimuses, $cashboxId)
    {
        $this->company = $company;
        $this->vidimuses = $vidimuses;
        $this->cashboxId = $cashboxId;
    }

    /**
     * @inheritdoc
     */
    public function setPeriodData($data)
    {
        $this->dateStart = isset($data[VidimusBuilder::TAG_DATE_START]) ? $data[VidimusBuilder::TAG_DATE_START] : '';
        $this->dateEnd = isset($data[VidimusBuilder::TAG_DATE_END]) ? $data[VidimusBuilder::TAG_DATE_END] : '';
        $this->balanceStart = isset($data[VidimusBuilder::TAG_BALANCE_START]) ? $data[VidimusBuilder::TAG_BALANCE_START] : '';
        $this->balanceEnd = isset($data[VidimusBuilder::TAG_BALANCE_END]) ? $data[VidimusBuilder::TAG_BALANCE_END] : '';
        $this->totalIncome = isset($data[VidimusBuilder::TAG_TOTAL_INCOME]) ? $data[VidimusBuilder::TAG_TOTAL_INCOME] : '';
        $this->totalExpense = isset($data[VidimusBuilder::TAG_TOTAL_EXPENSE]) ? $data[VidimusBuilder::TAG_TOTAL_EXPENSE] : '';
    }

    /**
     * @return string
     */
    public function draw($ofdInfo)
    {
        $this->html .= $this->head();
        $this->html .= $this->tableBegin();
        $this->html .= $this->tableBody($ofdInfo);
        $this->html .= $this->tableEnd();

        return $this->html;
    }

    /**
     * @return string
     */
    private function head()
    {
        $action = Url::to([
            '/cash/ofd/default/upload',
            'p' => $this->parentPage,
        ]);
        $csrfParam = Yii::$app->request->csrfParam;
        $csrfToken = Yii::$app->request->csrfToken;
        $html = '<hr/>';
        $html .= '<form method="POST" action="' . $action . '" id="vidimus_form_ajax">';
        $html .= '<input type="hidden" name="' . $csrfParam . '" value="' . $csrfToken . '">';
        $html .= '<div class="vidimus-head">Проверка на дубли</div>';
        $html .= '<input type="hidden" name="cashboxId" value="' . $this->cashboxId .'"/>';
        $html .= '<input type="hidden" name="source" value="' . CashOrderStatementUpload::SOURCE_FILE .'"/>';
        $html .= '<input type="hidden" name="dateStart" value="' . $this->dateStart .'"/>';
        $html .= '<input type="hidden" name="dateEnd" value="' . $this->dateEnd .'"/>';
        $html .= '<input type="hidden" name="balanceStart" value="' . $this->balanceStart .'"/>';
        $html .= '<input type="hidden" name="balanceEnd" value="' . $this->balanceEnd .'"/>';
        $html .= '<input type="hidden" name="totalIncome" value="' . $this->totalIncome .'"/>';
        $html .= '<input type="hidden" name="totalExpense" value="' . $this->totalExpense .'"/>';

        return $html;
    }

    /**
     * @return string
     */
    public function tableBegin()
    {
        $html = '<table class="table vidimus-table" style="table-layout: fixed; width: 646px;">'
            .'<tr class="vidimus-table-head">'
            .'<td style="word-wrap: break-word; width: 3%"></td>'
            .'<td style="word-wrap: break-word; width: 15%">Дата</td>'
            //.'<td style="word-wrap: break-word; width: 12%">№ Смены</td>'
            .'<td style="word-wrap: break-word; width: 15%">Сумма</td>'
            .'<td style="word-wrap: break-word; width: 20%">Контрагент</td>'
            .'<td style="word-wrap: break-word; width: 20%">Назначение</td>'
            .'<td style="word-wrap: break-word; width: 15%">Тип</td>'
            .'</tr>';

        return $html;
    }

    /**
     * @return string
     */
    public function tableBody($cashbox_id)
    {
        $html = '';
        $company = $this->company;
        $incomes = [];
        $expenditures = [];
        //$incomes = CashOrdersReasonsTypes::find()->where(['flow_type' => 1])->asArray()->all();
        //$incomes = ArrayHelper::index($incomes, 'id');
        //$expenditures = CashOrdersReasonsTypes::find()->where(['flow_type' => 0])->asArray()->all();
        //$expenditures = ArrayHelper::index($expenditures, 'id');

        foreach ((array) $this->vidimuses as $key => $vidimus) {
            $vidimus->checkRequiredParam();

            $vidimusTypeId = '';
            $vidimusTypeTittle = '';
            $vidimusTypeCss = '';
            if ($vidimus->flowType == Vidimus::FLOW_IN) {
                $vidimusTypeTittle = 'Оплата';
                $vidimusTypeCss = '';
            } elseif ($vidimus->flowType == Vidimus::FLOW_OUT) {
                $vidimusTypeTittle = 'Возврат';
                $vidimusTypeCss = '';
            }

            $expenditures_html = '';
            //if ($vidimus->flowType == 'in') {
            //    foreach ($incomes as $income) {
            //        $expenditures_html .= '<div data-id="1">' . 'Оплата' . '</div>';
            //    }
            //} else {
            //    foreach ($expenditures as $expenditure) {
            //        $expenditures_html .= '<div data-id="14">' . 'Возврат' . '</div>';
            //    }
            //}

            $new = ($vidimus->new) ? '<div class="new-contractor">Новый</div>' : '';
            $checked = !$vidimus->dublicate && !$vidimus->error;
            $dublicated = ($vidimus->dublicate) ? '<div class="vidimus-diblicate">Дубль</div>' : '';

            $inputs = Html::checkbox("vidimus[{$key}][upload]", $checked, ['disabled' => $vidimus->disabled]) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][type]", $vidimus->type) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][date]", $vidimus->date) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][number]", $vidimus->number) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][flow_type]", $vidimus->flowType) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][total]", $vidimus->total) . "\n";
            //$inputs .= Html::hiddenInput("vidimus[{$key}][target]", $vidimus->target) . "\n";
            //$inputs .= Html::hiddenInput("vidimus[{$key}][vidimus_type]", $vidimusTypeId, ['class' => 'vidimus_type']) . "\n";
            $inputs .= Html::hiddenInput("vidimus[{$key}][doInsert]", $vidimus->doInsert) . "\n";


            $trErrorStyle = '';
            $tdErrorStyle = '';
            if ($vidimus->error) {
                $inputs .= Html::tag('div', $vidimus->error, [
                    'style' => '
                        position: absolute;
                        left: 5px;
                        bottom: 0;
                        color: #860000;
                        width: 415px;
                        text-align: left;
                    ',
                ]);
                $trErrorStyle = 'background-color: #ffb7b7;';
                $tdErrorStyle = 'padding-bottom: 17px;';
            }
            $flowSum = "<span class=\"$vidimus->flowType\"></span><span>".TextHelper::moneyFormat(round($vidimus->total / 100, 2), 2)."</span>";
            $flowTarget = 'Смена №'.$vidimus->number.', '.$vidimusTypeTittle . ' от покупателей';
            $flowTarget = Html::tag('div', $flowTarget, ['title' => '']);
            $vidimusType = Html::tag('div', $vidimusTypeTittle, ['class' => 'vidimus-type-title', 'title' => $vidimusTypeTittle]);
            $vidimusType = Html::tag('div', $vidimusType, ['class' => "vidimus-type $vidimusTypeCss"]);
            $vidimusType .= Html::tag('div', '<div data-id="0">Оплата</div>', ['class' => 'vidimus-type-item']);

            $html .= Html::beginTag('tr', $vidimus->error ? ['style' => $trErrorStyle] : []);
            $html .= Html::tag('td', $inputs, ['style' => 'position: relative; width: 3%;' . $tdErrorStyle]);
            $html .= Html::tag('td', $vidimus->date . $dublicated, ['style' => 'word-wrap: break-word; width: 15%']);
            //$html .= Html::tag('td', $vidimus->number, ['style' => 'word-wrap: break-word; width: 12%']);
            $html .= Html::tag('td', $flowSum, ['style' => 'word-wrap: break-word; width: 15%']);
            $html .= Html::tag('td', 'Физ. лица', [
                'style' => 'word-wrap: break-word; width: 20%',
            ]);
            $html .= Html::tag('td', $flowTarget, ['style' => 'word-wrap: break-word; width: 30%']);
            $html .= Html::tag('td', $vidimusType, ['style' => 'word-wrap: break-word; width: 15%']);
            $html .= Html::endTag('tr');
        }

        return $html;
    }

    /**
     * @return string
     */
    private function tableEnd()
    {
        $html = '</table>'
        .'</form>'
        .Html::submitButton('<span class="ladda-label">Принять и Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'btn btn-primary statement-form-submit ladda-button',
            'form' => 'vidimus_form_ajax',
            'data-style' => 'expand-right',
            ])
        .Html::a('Отменить', [
            '/cash/ofd/default/index',
            'p' => Yii::$app->request->get('p'),
        ], [
            'class' => 'btn btn-primary ofd-module-link',
            'style' => 'float:right;',
        ]);

        return $html;
    }
}
