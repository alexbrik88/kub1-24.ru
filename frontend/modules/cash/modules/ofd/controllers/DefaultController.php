<?php

namespace frontend\modules\cash\modules\ofd\controllers;

use backend\models\Bank;
use common\models\cash\CashBankStatementUpload;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use frontend\components\StatisticPeriod;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\components\OfdBaseController;
//use frontend\modules\cash\modules\ofd\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusUploader;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Request;
use yii\web\UploadedFile;

/**
 * Default controller for the `ofd` module
 */
class DefaultController extends OfdBaseController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'select', 'upload', 'file-upload', 'file-upload-progress', 'import'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => ['account'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionImport()
    {
        return $this->render('select_ofd_import');
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionSelect()
    {

        return $this->render('select_ofd');
    }

    /**
     * @return mixed
     */
    public function actionUpload()
    {
        set_time_limit(90);
        $company = Yii::$app->user->identity->company;

        $cashbox_id = Yii::$app->request->post('cashboxId');

        if ($cashbox_id && $this->isUserCashbox($cashbox_id)) {
            $vidimus = Yii::$app->request->post('vidimus', []);
            $source = Yii::$app->request->post('source');
            $service = Yii::$app->request->post('service');
            $statistic = [
                'dateStart' => Yii::$app->request->post('dateStart'),
                'dateEnd' => Yii::$app->request->post('dateEnd'),
                'balanceStart' => Yii::$app->request->post('balanceStart'),
                'balanceEnd' => Yii::$app->request->post('balanceEnd'),
                'totalIncome' => Yii::$app->request->post('totalIncome'),
                'totalExpense' => Yii::$app->request->post('totalExpense'),
            ];
            if ($vidimus) {

                $uploader = new VidimusUploader($company, $cashbox_id);
                $uploader->upload($vidimus, $source, $statistic, $service);

                $message = "Загружено {$uploader->uploadedCount} записей";
                if ($uploader->errorsCount > 0)
                    $message .= ", пропущено {$uploader->errorsCount} записей";

                Yii::$app->session->setFlash('success', $message);

            } else {
                Yii::$app->session->setFlash('error', 'Не удалось загрузить выписку.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Не найдена касса, для которой загружается выписка.');
        }

        return $this->redirect(Ofd::routeDecode(Yii::$app->request->get('p'), ['/cash/order/index', 'cashbox' => $cashbox_id]));
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if ($action->id == 'file-upload' || $action->id == 'file-upload-progress') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
}
