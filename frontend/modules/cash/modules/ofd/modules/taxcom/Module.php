<?php

namespace frontend\modules\cash\modules\ofd\modules\taxcom;

/**
 * API - Такском
 *
 * taxcom module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\cash\modules\ofd\modules\taxcom\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
