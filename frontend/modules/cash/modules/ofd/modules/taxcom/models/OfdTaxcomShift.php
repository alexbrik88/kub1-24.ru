<?php

namespace frontend\modules\cash\modules\ofd\modules\taxcom\models;

use Yii;

/**
 * This is the model class for table "ofd_taxcom_shift".
 *
 * @property integer $id
 * @property string $kktRegNumber
 * @property string $fnFactoryNumber
 * @property integer $shiftNumber
 * @property string $group
 * @property integer $openDateTime
 * @property integer $closeDateTime
 * @property integer $total
 * @property integer $cash
 * @property integer $electronic
 * @property integer $receiptCount
 * @property integer $nds0Total
 * @property integer $noNDSTotal
 * @property integer $nds10
 * @property integer $nds18
 * @property integer $nds10_110
 * @property integer $nds18_118
 * @property integer $prepayment
 * @property integer $credit
 * @property integer $exchange
 */
class OfdTaxcomShift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ofd_taxcom_shift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kktRegNumber', 'fnFactoryNumber', 'shiftNumber', 'group', 'openDateTime', 'closeDateTime'], 'required'],
            [['shiftNumber', 'openDateTime', 'closeDateTime', 'total', 'cash', 'electronic', 'receiptCount', 'nds0Total', 'noNDSTotal', 'nds10', 'nds18', 'nds10_110', 'nds18_118', 'prepayment', 'credit', 'exchange'], 'integer'],
            [['kktRegNumber', 'fnFactoryNumber', 'group'], 'string', 'max' => 21],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kktRegNumber' => 'Kkt Reg Number',
            'fnFactoryNumber' => 'Fn Factory Number',
            'shiftNumber' => 'Shift Number',
            'group' => 'Group',
            'openDateTime' => 'Open Date Time',
            'closeDateTime' => 'Close Date Time',
            'total' => 'Total',
            'cash' => 'Cash',
            'electronic' => 'Electronic',
            'receiptCount' => 'Receipt Count',
            'nds0Total' => 'Nds0 Total',
            'noNDSTotal' => 'No Ndstotal',
            'nds10' => 'Nds10',
            'nds18' => 'Nds18',
            'nds10_110' => 'Nds10 110',
            'nds18_118' => 'Nds18 118',
            'prepayment' => 'Prepayment',
            'credit' => 'Credit',
            'exchange' => 'Exchange',
        ];
    }
}
