<?php

namespace frontend\modules\cash\modules\ofd\modules\taxcom\models;

use common\components\curl;
use common\models\cash\CashOrderStatementUpload;
use common\models\cash\Cashbox;
use DateTime;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusDrawer;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use common\models\company\ApplicationToBank;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use frontend\modules\cash\modules\ofd\components\vidimus\Vidimus;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusUploader;
use frontend\modules\cash\modules\ofd\models\AbstractOfdModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * OfdModel
 * «Такском»
 */
class OfdModel extends AbstractOfdModel
{
    const NAME = 'Такском';
    const NAME_SHORT = 'Такском';
    const ALIAS = 'taxcom';

    public static $alias = 'taxcom';

    public static $importMode = false;
    public static $hasAutoload = false;

    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_TOKEN = 'token';
    const SCENARIO_STATEMENT = 'statement';
    const SCENARIO_EMPTY = 'empty';
    const SCENARIO_REQUEST = 'request';
    const SCENARIO_GET_SHIFT_LIST = 'get-shift-list';
    const SCENARIO_GET_SHIFT_INFO = 'get-shift-info';
    const SCENARIO_IMPORT = 'import';

    public static $shiftInfoGroup = [
        'income',
        'incomeReturn',
        'incomeCorrection',
        'expenditure',
        'expenditureReturn',
        'expenditureCorrection'
    ];

    public $integrator_id;
    public $auth_redirect;
    public $cashbox_id;
    public $start_date;
    public $end_date;
    public $statement;
    public $code;
    public $error;

    public $modeImport;
    public $declarationId;

    public $kktRegNumber;
    public $processShiftInfo = false;

    protected static $apiHost = 'https://api-lk-ofd.taxcom.ru';
    protected static $authHost = 'https://auth-lk-ofd.taxcom.ru';

    protected static $importAuthHost = 'https://auth-lk-ofd.taxcom.ru'; // prod
    protected static $importHost = 'https://api-online.taxcom.ru'; // prod

    // Lists for get vidimus:
    // Outlets -> kkt -> fn -> shifts -> shift_info
    public $outlet_list;
    public $kkt_list;
    public $fn_list;
    public $shift_list;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            static::SCENARIO_EMPTY => [
            ],
            static::SCENARIO_DEFAULT => [
                'auth_redirect',
            ],
            static::SCENARIO_TOKEN => [
                'code',
                'error',
            ],
            static::SCENARIO_REQUEST => [
                'kkt',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_GET_SHIFT_LIST => [
                'kktRegNumber',
                'start_date',
                'end_date',
            ],
            static::SCENARIO_STATEMENT => [
                'kktRegNumber',
            ],
            static::SCENARIO_IMPORT => [
                'declarationId',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'auth_redirect',
                'cashbox_id',
                'start_date',
                'end_date',
                'code',
                'payBill',
                'paymentOrder',
                'currentAccount',
                'declarationId'
            ], 'required'],
            [['code', 'error'], 'string'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:d.m.Y'],
            [
                ['cashbox_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Cashbox::className(),
                'targetAttribute' => ['cashbox_id' => 'id'],
            ],
            [
                [
                    'kktRegNumber',
                    'start_date',
                    'end_date'
                ],
                'required', 'on' => static::SCENARIO_GET_SHIFT_LIST
            ],
            [
                [
                    'kktRegNumber',
                ],
                'required', 'on' => static::SCENARIO_STATEMENT
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cashbox_id' => 'ID кассы',
            'start_date' => 'Начало периода',
            'end_date' => 'Конец периода',
            'kktRegNumber' => 'ККТ'
        ];
    }

    public function init() {

        parent::init();

        $this->integrator_id = Yii::$app->params['ofd'][static::$alias]['integratorId'];

        $this->outlet_list = $this->getJsonParam('outlet_list');
        $this->kkt_list = $this->getJsonParam('kkt_list');
        $this->fn_list = $this->getJsonParam('fn_list');
    }

    public function getCashboxOfdName() {

        if ($this->kktRegNumber) {
            $outlets = ArrayHelper::map((array)$this->outlet_list, 'id', 'name');
            $kkts = ArrayHelper::index((array)$this->kkt_list, 'kktRegNumber');

            foreach ($kkts as $value) {
                if ($value['kktRegNumber'] == $this->kktRegNumber) {
                    $kkt_name = $value['name'];
                    $outlet_name = isset($outlets[$value['outlet_id']]) ? $outlets[$value['outlet_id']] : '';

                    return $kkt_name . ' (' . $outlet_name . ')';
                }
            }

            return '№' . $this->kktRegNumber;
        }

        return 'не задано';
    }

    public function getOfdData($attr, $params = []) {

        $params['pn'] = 1;   // page number
        $params['ps'] = 100; // records per page
        $requestsLimit = 10; // max 1000 records per one ajax-request

        $paths = [
            'outlet_list' => 'OutletList',
            'kkt_list' => 'KKTList',
            'fn_list' => 'FnHistory',
            'shift_list' => 'ShiftList'];

        if (!isset($paths[$attr]))
            return false;

        $requestHeader = [
            'content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . $this->getParam('sessionToken'),
        ];

        while ($requestsLimit > 0) {

            $apiPath = '/API/v2/' . $paths[$attr] . (!empty($params) ? ('/?'.http_build_query($params)) : '');

            $curl = new \common\components\curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
            ])->get(self::$apiHost . $apiPath);

            $this->debugLog($curl, __METHOD__);

            $requestResult = json_decode($response, true);

            // Success exit (no more records)
            if (isset($requestResult['counts']) && $requestResult['counts']['recordInResponceCount'] == 0)
                return true;

            if ($requestsLimit == 0)
                return false;

            $params['pn']++;
            $requestsLimit--;

            if (!$curl->errorCode) {

                if ($requestResult && isset($requestResult['records'])) {

                    $items_list = $this->getJsonParam($attr);

                    if ('outlet_list' == $attr) {
                        foreach ($requestResult['records'] as $value) {
                            $items_list[] = [
                                'id' => $value['id'],
                                'name' => $value['name'],
                                'address' => $value['address'],
                                'problem' => $value['problemIndicator']
                            ];
                        }
                        $this->outlet_list = $items_list;

                    } elseif ('kkt_list' == $attr) {
                        foreach ($requestResult['records'] as $value) {
                            $items_list[] = [
                                'outlet_id' => $params['id'],
                                'name' => $value['name'],
                                'kktRegNumber' => $value['kktRegNumber'],
                                'problem' => $value['problemIndicator']
                            ];
                        }
                        $this->kkt_list = $items_list;

                    } elseif ('fn_list' == $attr) {
                        foreach ($requestResult['records'] as $value) {
                            $items_list[] = [
                                'fn' => $value['fn'],
                                'kktRegNumber' => $params['kktRegNumber']
                            ];
                        }
                        $this->fn_list = $items_list;

                    } elseif ('shift_list' == $attr) {
                        foreach ($requestResult['records'] as $value) {
                            $items_list[$params['fn']][] = $value['shiftNumber'];
                        }
                        $this->shift_list = $items_list;

                    } else {
                        return false;
                    }

                    $this->setJsonParam($attr, $items_list);

                    // Success exit (one page total)
                    if ($requestResult['counts']['recordCount'] == $requestResult['counts']['recordInResponceCount'])
                        return true;

                    continue;

                } elseif (!empty($requestResult['commonDescription'])) {
                    if (Yii::$app->id == 'app-frontend') {
                        Yii::$app->session->setFlash('error', $requestResult['commonDescription']);
                    }
                    $this->errorLog($curl, __METHOD__);

                    return false;
                }
            }

            $this->errorLog($curl, __METHOD__);

            return false;

        };

        return false;
    }

    public function getShiftInfo($fn, $shift) {

        if (!$fn || !$shift)
            return false;

        $apiPath = '/API/v2/ShiftInfo/?fn=' . $fn .'&shift=' . $shift;

        $requestHeader = [
            'content-Type: application/json; charset=utf-8',
            'Authorization: Bearer ' . $this->getParam('sessionToken'),
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
        ])->get(self::$apiHost . $apiPath);

        $this->debugLog($curl, __METHOD__);

        $requestResult = json_decode($response, true);

        if (!$curl->errorCode) {

            if ($requestResult && isset($requestResult['shift'])) {

                return $requestResult['shift'];

            } elseif (!empty($requestResult['commonDescription'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult['commonDescription']);
                }
                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;

    }

    /**
     * @return string
     */
    public function isValidToken()
    {
        $sessionToken = $this->getParam('sessionToken');
        $sessionTokenExpires = $this->getParam('sessionTokenExpires');

        if ($this->company->id) {
            if ($sessionToken && $sessionTokenExpires > (time()+5)) {
                return true;
            } else {
                return $this->authTokenRefresh(null);
            }
        }

        return $this->getTmpToken() !== null;
    }

    /**
     * Request to remote API for accessToken
     * @return string|null
     */
    public function authTokenRequest($host = null)
    {
        $time = time();
        $requestHeader = [
            'Authorization: Basic ' . base64_encode(urlencode($this->clientId) .':'. urlencode($this->clientSecret)),
        ];

        $requestData = [
            'grant_type' => 'authorization_code',
            'code' => $this->code,
            'redirect_uri' => $this->returnUrl,
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => http_build_query($requestData),
        ])->post($host ?: self::$authHost . '/core/connect/token');

        $this->debugLog($curl, __METHOD__);

        $requestResult = json_decode($response, true);

        if (!$curl->errorCode) {

            if ($requestResult && isset($requestResult['access_token'], $requestResult['expires_in'])) {

                $this->setParam('sessionToken', $requestResult['access_token']);
                $this->setParam('sessionTokenExpires', $time + $requestResult['expires_in']);
                //$this->setParam('refresh_token', $requestResult['refresh_token']);

                return true;

            } elseif (!empty($requestResult['commonDescription'])) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', $requestResult['commonDescription']);
                }
                $this->errorLog($curl, __METHOD__);

                return false;
            }
        }

        $this->errorLog($curl, __METHOD__);

        return false;
    }

    /**
     * Request to remote API for authToken
     * @return string|null
     */
    public function authTokenRefresh($refresh_token)
    {
        return false;
    }

    /**
     * Request to remote API
     *
     * @return bool
     */
    public function checkApiAccess()
    {
        $requestHeader = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->getParam('sessionToken'),
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => null,
        ])->get(self::$apiHost . '/API/v2/OutletList');

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {

            $requestResult = json_decode($response, true);

            if ($curl->responseCode == 200 && $requestResult) {
                if (isset($requestResult['counts'])) {

                    return true;

                } elseif (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('error', 'Нет доступа к ОФД Такском');

                    return false;
                }
            } else {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('warning', 'Сервис Такском временно недоступен. Попробуйте пожалуйста позже.');
                }
            }
        } else {
            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('warning', 'Сервис Такском временно недоступен. Попробуйте пожалуйста позже.');
            }
            $this->errorLog($curl, __METHOD__);
        }

        return false;
    }

    /**
     * rendering content of statement
     */
    public function renderStatement()
    {
        $cashbox = Cashbox::findOne([
            'company_id' => $this->company->id,
            'reg_number' => $this->kktRegNumber
        ]);

        if ($cashbox)
            $this->cashbox_id = $cashbox->id;

        $this->_data = OfdTaxcomShift::find()->where(['kktRegNumber' => $this->kktRegNumber])->asArray()->all();

        list($vidimus, $statistic) = $this->vidimusPrepare();
        $vidimus = (array)$vidimus;
        $content = '';

        if (count($vidimus) > 0) {

            $builder = new VidimusBuilder(null, $this->company, $this->cashbox_id);
            $vidimusDrawer = new VidimusDrawer($this->company, $builder->handlingVidimus($vidimus), $this->cashbox_id);

            $content .= Html::tag('div', "Проверка на дубли", ['class' => 'vidimus-head']);
            $content .= Html::beginForm(Url::to(['/cash/ofd/default/upload']), 'post', ['id' => 'vidimus_form_ajax']);
            $content .= Html::hiddenInput('source', CashOrderStatementUpload::SOURCE_OFD);
            $content .= Html::hiddenInput('service', static::$alias);
            $content .= Html::hiddenInput('cashboxId', $this->cashbox_id);
            $content .= Html::hiddenInput('dateStart', isset($statistic['dateStart']) ? $statistic['dateStart'] : null);
            $content .= Html::hiddenInput('dateEnd', isset($statistic['dateEnd']) ? $statistic['dateEnd'] : null);
            $content .= Html::hiddenInput('balanceStart', isset($statistic['balanceStart']) ? $statistic['balanceStart'] : null);
            $content .= Html::hiddenInput('balanceEnd', isset($statistic['balanceEnd']) ? $statistic['balanceEnd'] : null);
            $content .= Html::hiddenInput('totalIncome', isset($statistic['totalIncome']) ? $statistic['totalIncome'] : null);
            $content .= Html::hiddenInput('totalExpense', isset($statistic['totalExpense']) ? $statistic['totalExpense'] : null);
            $content .= $vidimusDrawer->tableBegin();
            $content .= $vidimusDrawer->tableBody($this->cashbox_id);
            $content .= '</table>';
            $content .= Html::submitButton('Принять и Сохранить', [
                'class' => 'btn btn-primary statement-form-submit',
                'form' => 'vidimus_form_ajax',
            ]);
            $content .= Html::a('Отменить', [
                '/cash/ofd/default/index',
                'p' => Yii::$app->request->get('p'),
            ], [
                'class' => 'btn btn-primary ofd-module-link',
                'style' => 'float:right;',
            ]);
            $content .= Html::endForm();
        } else {
            $message = Html::tag('div', "За указанный период не найдено операций по указанной ККТ.", ['class' => 'gray-alert',]);
            $content .= Html::tag('div', $message, ['class' => 'form-group',]);

            $content .= Html::a('Новый запрос', $this->newRequestUrl, [
                'class' => 'btn btn-primary ofd-module-link',
            ]);
        }

        return $content;
    }


    /**
     * Prepare vidimus
     * @return array
     */
    public function vidimusPrepare()
    {
        $vidimuses = [];
        $data = $this->_data;
        $statistic = [
            'dateStart' => $this->start_date,
            'dateEnd' => $this->end_date,
            'balanceStart' => null,
            'balanceEnd' => null,
            'totalIncome' => null,
            'totalExpense' => null,
        ];

        try {
            foreach ($data as $operation) {

                $group = $operation['group'];

                switch ($group) {
                    case 'expenditure':
                        $flowType = Vidimus::FLOW_OUT;
                        break;
                    case 'income':
                        $flowType = Vidimus::FLOW_IN;
                        break;
                    default:
                        $flowType = null;
                        $contractor = null;
                        break;
                }
                if ($flowType !== null && $operation['total'] > 0) {

                    $vidimuses[] = new Vidimus([
                        'date' => date('d.m.Y', $operation['openDateTime']),
                        'number' => (string) $operation['shiftNumber'],
                        'total' => round($operation['total']),
                        'flowType' => $flowType,
                    ]);
                }
            }

            return [$vidimuses, $statistic];

        } catch (\Exception $e) {
            if (Yii::$app->id == 'app-frontend') {
                Yii::$app->session->setFlash('error', $this->_errorMessage);
            }

            return [[], $statistic];
        }
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return self::$authHost . '/core/connect/authorize/' . '?' . http_build_query([
                'client_id' => Yii::$app->params['ofd'][static::$alias]['client_id'],
                'response_type' => 'code',
                'scope' => 'api',
                'redirect_uri' => $this->getReturnUrl(),
            ]);
    }

    /**
     * @return string
     */
    public function getImportAuthUrl()
    {
        return self::$importAuthHost . '/core/connect/authorize/' . '?' . http_build_query([
                'client_id' => Yii::$app->params['ofd_import'][static::$alias]['client_id'],
                'response_type' => 'code',
                'scope' => 'sprinter-integration-api',
                'redirect_uri' => $this->getReturnUrl(),
            ]);
    }

    /**
     * @return string
     */
    public function getReturnUrl()
    {
        return Yii::$app->request->hostInfo . Url::to([
                '/cash/ofd/' . static::$alias . '/default/return',
            ]);
    }

    /**
     * @return string
     */
    public function getSessionKey()
    {
        return 'ofd.' . static::$alias . '.' . $this->_company->id ;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        if (self::$importMode) {
            return Yii::$app->params['ofd_import'][static::$alias]['client_id'];
        }

        return Yii::$app->params['ofd'][static::$alias]['client_id'];
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        if (self::$importMode) {
            return Yii::$app->params['ofd_import'][static::$alias]['client_secret'];
        }

        return Yii::$app->params['ofd'][static::$alias]['client_secret'];
    }

    /**
     * @return string
     */
    public function setParam($name, $value, $save = true)
    {
        if (self::$importMode) {
            return parent::setOfdParam($name, $value, self::$alias.'_import', $save);
        }

        return parent::setOfdParam($name, $value, self::$alias, $save);
    }

    /**
     * @return string
     */
    public function getParam($name, $refresh = false)
    {
        if (self::$importMode) {
            return parent::getOfdParam($name, self::$alias.'_import', $refresh);
        }

        return parent::getOfdParam($name, self::$alias, $refresh);
    }

    /**
     * @return string
     */
    public function setJsonParam($name, $value, $save = true)
    {
        return parent::setOfdParam($name, json_encode($value), self::$alias, $save);
    }

    /**
     * @return array
     */
    public function getJsonParam($name, $refresh = false)
    {
        $ret = parent::getOfdParam($name, self::$alias, $refresh);

        return (!empty($ret)) ? json_decode($ret, true) : [];
    }

    public function setImportMode() {
        self::$importMode = true;
        self::$authHost = self::$importAuthHost;
        self::$apiHost  = self::$importHost;
    }

    public function importDocument($declaration) {

        if (!$declaration)
            return false;

        if ($id = $this->importDeclarationFile($declaration)) {

            if ($result = $this->importDeclarationDraft($id)) {

                return $result;

            } else {

                Yii::$app->session->setFlash('error', 'Не удалось произвести импорт документа в Черновики');
            }

        } else {

            Yii::$app->session->setFlash('error', 'Не удалось отправить файл декларации');
        }

        return false;
    }

    /**
     * @param TaxDeclaration $declaration
     * @return bool
     */
    protected function importDeclarationFile($declaration) {

        $requestHeader = [
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $this->getParam('sessionToken'),
            'Accept: application/json'
        ];

        $xml = $declaration->getXmlToImport();
        $filename = $xml['filename'].'.xml';
        $file = tempnam(sys_get_temp_dir(), 'POST');
        file_put_contents($file, $xml['output']);
        $cFile = new \CURLFile($file, 'text/xml', $filename);

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => [
                'data' => $cFile
            ],
        ])->post(self::$importHost . '/api/v1/import/upload');

        unlink($file);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {

            $requestResult = json_decode($response, true);

            if ($curl->responseCode == 200 && $requestResult) {

                if (is_string($requestResult) && strlen($requestResult) == 36) {

                    return $requestResult;

                } elseif (Yii::$app->id == 'app-frontend') {

                    $this->errorLog($curl, __METHOD__);

                    return false;
                }
            }
        } else {

            $this->errorLog($curl, __METHOD__);
        }

        return false;
    }

    protected function importDeclarationDraft($id) {

        $requestHeader = [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $this->getParam('sessionToken'),
            'Accept: application/json'
        ];

        $curl = new \common\components\curl\Curl();
        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => null
        ])->post(self::$importHost . "/api/v1/documents/importDraft?importItemId={$id}");

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {

            $requestResult = json_decode($response, true);

            if ($curl->responseCode == 200 && $requestResult) {
                if (isset($requestResult['PackageChainId']) && isset($requestResult['DocumentId'])) {

                    return $requestResult;

                } elseif (Yii::$app->id == 'app-frontend') {

                    $this->errorLog($curl, __METHOD__);

                    return false;
                }
            }
        } else {

            $this->errorLog($curl, __METHOD__);
        }

        return false;
    }
}
