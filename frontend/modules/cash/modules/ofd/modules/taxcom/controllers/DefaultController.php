<?php

namespace frontend\modules\cash\modules\ofd\modules\taxcom\controllers;

use common\models\cash\Cashbox;
use common\models\Company;
use frontend\modules\cash\modules\ofd\components\Ofd;
use frontend\modules\cash\modules\ofd\components\OfdModulesBaseController;
use frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdModel;
use frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdTaxcomShift;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `taxcom` module
 *
 * «Такском»
 */
class DefaultController extends OfdModulesBaseController
{
    /**
     * @var OfdModel
     */
    public $ofdModelClass = OfdModel::class;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'request',
                            'import',
                            'send-declaration',
                            'get-log',
                            'clear-log',
                            'delete'
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF, UserRole::ROLE_ACCOUNTANT],
                    ],
                    [
                        'actions' => [
                            'set-autoload',
                        ],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'return',
                            'get-shift-list',
                            'get-shift-info'
                        ],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }

    public function actionClearLog() {
        echo (bool)file_put_contents(Yii::getAlias('@frontend').'/runtime/logs/debug_taxcom.log', '');
        exit;
    }
    public function actionGetLog() {
        echo file_get_contents(Yii::getAlias('@frontend').'/runtime/logs/debug_taxcom.log');
        exit;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->session->set(OfdModel::$alias.'ImportMode', 0);

        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_DEFAULT);
        $model->showSecureText = true;

        if ($model->isValidToken() && $model->checkApiAccess()) {
            $model->scenario = OfdModel::SCENARIO_REQUEST;

            return $this->render('request', ['model' => $model]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            Url::remember(Url::current(), OfdModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getAuthUrl());

        }

        return $this->render('index', ['model' => $model]);
    }

    public function actionGetShiftList()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect('/cash/order');
        }

        if (Yii::$app->request->post('isSuccessLoaded')) {
            return $this->showStatement();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_GET_SHIFT_LIST);

        if ($model->isValidToken() && $model->checkApiAccess()) {

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $cashbox = Cashbox::findOne([
                    'company_id' => $model->company->id,
                    'reg_number' => $model->kktRegNumber
                ]);

                if (!$cashbox) {
                    $cashbox = new Cashbox;
                    $cashbox->company_id = $model->company->id;
                    $cashbox->is_accounting = true;
                    $cashbox->is_main = false;
                    $cashbox->is_closed = false;
                    $cashbox->name = $model->getCashboxOfdName();
                    $cashbox->reg_number = $model->kktRegNumber;
                    $cashbox->save(false);
                }

                $model->setParam('shift_list', null);

                $begin = $model->getDateTimeFrom()->format('Y-m-d\TH:i:s');
                $end   = $model->getDateTimeTill()->format('Y-m-d\TH:i:s');

                foreach ($model->fn_list as $fn) {
                    if ($fn['kktRegNumber'] == $model->kktRegNumber)
                        $model->getOfdData('shift_list', ['fn' => $fn['fn'], 'begin' => $begin, 'end' => $end]);
                }

                if (empty($model->shift_list)) {
                    Yii::$app->session->setFlash('error', "За указанный период смены не найдены.");
                } else {
                    $model->processShiftInfo = true;
                    OfdTaxcomShift::deleteAll(['kktRegNumber' => $model->kktRegNumber]);
                }

                return $this->render('request', ['model' => $model]);

            } else {

                return $this->render('request', ['model' => $model]);
            }
        } else {
            Yii::$app->session->setFlash('error', "Session token expires.");

            return $this->render('request', ['model' => $model]);
        }
    }

    public function actionGetShiftInfo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_GET_SHIFT_INFO);

        if ($model->isValidToken()) {

            $fn = Yii::$app->request->post('fn');
            $shiftNum = Yii::$app->request->post('shift');
            $kkt = Yii::$app->request->post('kkt');

            $shift = $model->getShiftInfo($fn, $shiftNum);

            if ($shift) {

                foreach ($model::$shiftInfoGroup as $group) {

                    if (isset($shift[$group])) {

                        $data = $shift[$group];

                        $openDateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $shift['openDateTime']);
                        $closeDateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $shift['closeDateTime']);

                        $modelShiftInfo = new OfdTaxcomShift([
                            'kktRegNumber' => $kkt,
                            'fnFactoryNumber' => $fn,
                            'shiftNumber' => $shiftNum,
                            'group' => $group,
                            'openDateTime' => $openDateTime ? $openDateTime->getTimestamp() : 0,
                            'closeDateTime' => $closeDateTime ? $closeDateTime->getTimestamp() : 0,
                            'total' =>  ArrayHelper::getValue($data, 'total', 0),
                            'cash' => ArrayHelper::getValue($data, 'cash', 0),
                            'electronic' => ArrayHelper::getValue($data, 'electronic', 0),
                            'receiptCount' => ArrayHelper::getValue($data, 'receiptCount', 0),
                            'nds0Total' => ArrayHelper::getValue($data, 'nds0Total', 0),
                            'noNDSTotal' => ArrayHelper::getValue($data, 'noNDSTotal', 0),
                            'nds10' => ArrayHelper::getValue($data, 'nds10', 0),
                            'nds18' => ArrayHelper::getValue($data, 'nds18', 0),
                            'nds10_110' => ArrayHelper::getValue($data, 'nds10_110', 0),
                            'nds18_118' => ArrayHelper::getValue($data, 'nds18_118', 0),
                            'prepayment' => ArrayHelper::getValue($data, 'prepayment', 0),
                            'credit' => ArrayHelper::getValue($data, 'credit', 0),
                            'exchange' => ArrayHelper::getValue($data, 'exchange', 0),
                        ]);

                        if (!$modelShiftInfo->save()) {

                            return ['result' => false, 'errors' => $modelShiftInfo->getErrors()];
                        }
                    }
                }

                return ['result' => true];
            }

            return ['result' => false, 'error' => 'Error shift info.'];

        } else {
            Yii::$app->session->setFlash('error', "Session token expires.");

            return ['result' => false, 'error' => "Session token expires."];
        }
    }

    /**
     * @return string
     */
    public function actionReturn()
    {
        /** @var OfdModel $model */
        $redirectUrl = Url::previous(OfdModel::$alias.'ReturnRedirect');

        $model = $this->getModel(OfdModel::SCENARIO_TOKEN);

        $importMode = Yii::$app->session->get(OfdModel::$alias.'ImportMode');

        // IMPORT DOCS
        if ($importMode) {

            $model->setImportMode();

            if ($model->load(Yii::$app->request->get(), '') && $model->validate() && $model->authTokenRequest()) {
                Yii::$app->session->setFlash('success', "Вы успешно авторизованы.");
            } else {
                Yii::$app->session->setFlash('error', "Во время авторизации произошла ошибка.");
                Yii::warning(static::className() . "->actionReturn() (import)" . var_export($model->errors, true), 'ofd');
            }
        }

        // GET VIDIMUS
        else {

            if ($model->load(Yii::$app->request->get(), '') && $model->validate() && $model->authTokenRequest()) {

                $model->setParam('outlet_list', null);
                $model->setParam('kkt_list', null);
                $model->setParam('fn_list', null);

                $model->getOfdData('outlet_list');

                foreach ((array)$model->outlet_list as $outlet)
                    $model->getOfdData('kkt_list', ['id' => $outlet['id']]);

                foreach ($model->kkt_list as $kkt)
                    $model->getOfdData('fn_list', ['kktRegNumber' => $kkt['kktRegNumber']]);

                Yii::$app->session->setFlash('success', "Вы успешно авторизованы.");

            } else {
                Yii::$app->session->setFlash('error', "Во время авторизации произошла ошибка.");
                Yii::warning(static::className() . "->actionReturn() " . var_export($model->errors, true), 'ofd');
            }
        }

        return $this->redirect($redirectUrl ? : ['index']);
    }

    public function showStatement()
    {
        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_STATEMENT);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return $this->renderContent($model->renderStatement());
        } else {
            return $this->render('index', [
                'model' => $this->getModel(OfdModel::SCENARIO_DEFAULT),
                'action' => 'request',
            ]);
        }

    }

    /**
     * Renders the documents import index view for the module
     * @return string
     */
    public function actionImport()
    {
        $this->layout = '@ofd/views/layouts/ofd_import';

        Yii::$app->session->set(OfdModel::$alias.'ImportMode', 1);

        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_DEFAULT);
        $model->setImportMode();
        $model->showSecureText = true;

        $declaration = null;
        $p = Ofd::routeDecode(Yii::$app->request->get('p'));
        if ($id = ArrayHelper::getValue($p, 'id')) {
            $declaration = TaxDeclaration::findOne([
                'company_id' => $model->company->id,
                'id' => $id
            ]);
        }

        if (!$declaration)
            throw new Exception('Декларация не найдена');

        if ($model->isValidToken()) {

            return $this->render('import_declaration', [
                'model' => $model,
                'declaration' => $declaration,
                'success' => false
                ]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            Url::remember(Url::current(), OfdModel::$alias.'ReturnRedirect');

            return $this->redirect($model->getImportAuthUrl());
        }

        return $this->render('import', [
            'model' => $model,
            'declaration' => $declaration,
        ]);
    }

    public function actionSendDeclaration() {

        $this->layout = '@ofd/views/layouts/ofd_import';

        /** @var OfdModel $model */
        $model = $this->getModel(OfdModel::SCENARIO_DEFAULT);
        $model->setImportMode();
        $model->showSecureText = true;

        $ids = false;
        $declaration = null;
        $p = Ofd::routeDecode(Yii::$app->request->get('p'));
        if ($id = ArrayHelper::getValue($p, 'id')) {
            $declaration = TaxDeclaration::findOne([
                'company_id' => $model->company->id,
                'id' => $id
            ]);
        }

        if (!$declaration)
            throw new Exception('Декларация не найдена');

        $model->scenario = OfdModel::SCENARIO_IMPORT;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->declarationId == $declaration->id) {

            if ($ids = $model->importDocument($declaration)) {

                Yii::$app->session->setFlash('success', "Декларация успешно загружена.");
            }

        }

        return $this->render('import_declaration', [
            'model' => $model,
            'declaration' => $declaration,
            'success' => (bool)$ids,
            'ids' => $ids
        ]);
    }

}
