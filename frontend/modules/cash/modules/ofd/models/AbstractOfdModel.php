<?php

namespace frontend\modules\cash\modules\ofd\models;

use common\components\curl\Curl;
use common\models\Company;
use common\models\company\CompanyType;
use frontend\modules\cash\modules\ofd\models\OfdParams;
use common\models\bank\StatementAutoloadMode;
use common\models\company\CheckingAccountant;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OperationType;
use common\models\document\PaymentOrder;
use common\models\document\TaxpayersStatus;
use common\models\employee\Employee;
use frontend\modules\cash\modules\ofd\Module;
use frontend\modules\cash\modules\ofd\components\vidimus\Vidimus;
//use frontend\modules\cash\modules\ofd\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusDrawer;
use frontend\modules\cash\modules\ofd\components\vidimus\VidimusUploader;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

class AbstractOfdModel extends Model
{
    public static $alias;
    public static $hasAutoload = false;

    protected $_company;
    protected $_config;
    protected $_debugMode = false;
    protected $_ofd_params = [];

    /**
     * @var string
     */
    protected $_errorMessage = 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.';

    /**
     * @var boolean
     */
    protected $_error = false;

    /**
     * @var mixed
     */
    protected $_data;

    /**
     * @var boolean
     */
    public $showOfdBatton = false;
    public $showSecureText = false;
    public $statement;
    public $cashbox_id;
    public $start_date;
    public $end_date;

    public $needPreRequest = false;

    /**
     * Construct
     */
    public function __construct(Company $company, $config = [])
    {
        $this->_company = $company;

        parent::__construct($config);
    }

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        $this->_debugMode = (bool) $this->getConfig('debugMode', false);

        $this->start_date = date('01.m.Y');
        $this->end_date = date('d.m.Y');
    }

    /**
     * @return boolean
     */
    public function getHasAutoload()
    {
        return static::$hasAutoload;
    }

    /**
     * @return string
     */
    public function getConfig($name, $default = null)
    {
        if ($this->_config === null) {
            $this->_config = ArrayHelper::getValue(Yii::$app->params['ofd'], static::ALIAS, []);
        }

        return ArrayHelper::getValue($this->_config, $name, $default);
    }

    /**
     * @return string
     */
    public function setOfdParam($name, $value, $alias, $save = true)
    {
        $this->_ofd_params[$name] = $value;
        if ($save) {
            return OfdParams::setValue($this->company, $alias, $name, $value);
        }

        return false;
    }

    /**
     * @return string
     */
    public function getOfdParam($name, $alias, $refresh = false)
    {
        if (!array_key_exists($name, $this->_ofd_params) || $refresh) {
            $this->_ofd_params[$name] = OfdParams::getValue($this->company, $alias, $name);
        }

        return $this->_ofd_params[$name];
    }

    /**
     * @return string
     */
    public function unsetParam($name)
    {
        return OfdParams::deleteValue($this->company, self::$alias, $name);
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return mixed
     */
    public function getOfdName()
    {

        return static::NAME;
    }

    /**
     * @return bool
     */
    public function getHasError()
    {
        return $this->_error;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }
    /**
     * @return \DateTime
     */
    public function getDateTimeFrom()
    {
        return \DateTime::createFromFormat('d.m.Y', $this->start_date)->setTime(0, 0, 0);
    }

    /**
     * @return \DateTime
     */
    public function getDateTimeTill()
    {
        return \DateTime::createFromFormat('d.m.Y', $this->end_date)->setTime(23, 59, 59);
    }

    public function getCancelUrl()
    {
        return Url::to(['/cash/ofd/default/index']);
    }

    public function getNewRequestUrl()
    {
        $alias = static::$alias;

        return Url::to(["/cash/ofd/{$alias}/default/index", 'cashbox_id' => $this->cashbox_id]);
    }

    /**
     * @return array
     */
    public function getAutoloadItems()
    {
        return ['' => 'Нет'];
    }

    /**
     * Request statement and load
     */
    public function autoloadStatement()
    {
        return null;
    }

    public function sendPaymentOrder()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if ($this->hasErrors()) {
            Yii::warning(static::className() . "->getErrors() " . var_export($this->getErrors(), true), 'ofd');
        }

        parent::afterValidate();
    }

    /**
     * @return string
     */
    public static function curlLogMsg(Curl $curl)
    {
        if (method_exists($curl, 'getDump')) {
            return $curl->getDump();
        } else {
            $data = [
                'curl_info' => curl_getinfo($curl->curl),
                'curl_postfields' => ArrayHelper::getValue($curl->getOptions(), CURLOPT_POSTFIELDS),
                'response_headers' => $curl->responseHeaders,
                'response' => $curl->response,
            ];
            if ($curl->errorCode || $curl->errorText) {
                $data['error_code'] = $curl->errorCode;
                $data['error_text'] = $curl->errorText;
            }

            return VarDumper::dumpAsString($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function debugLog(Curl $curl, $method)
    {
        if ($this->_debugMode) {
            $logFile = Yii::getAlias('@runtime/logs/debug_' . static::$alias . '.log');
            file_put_contents($logFile, $method . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }
    }

    /**
     * @inheritdoc
     */
    public function errorLog(Curl $curl, $method)
    {
        Yii::warning($method . "\n" . self::curlLogMsg($curl) . "\n", 'ofd');
    }

    /**
     * @return string
     */
    public function getTmpTokenParamName()
    {
        return '_tmp_ofd_' . static::$alias;
    }

    /**
     * @inheritdoc
     */
    public function setTmpToken($value, $exp = null)
    {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new \yii\web\Cookie([
            'name' => $this->getTmpTokenParamName(),
            'value' => $value,
            'expire' => $exp ?: time() + 60 * 10,
        ]));
    }

    /**
     * @return string
     */
    public function getTmpToken()
    {
        $cookies = Yii::$app->request->cookies;

        return $cookies->getValue($this->getTmpTokenParamName());
    }
}
