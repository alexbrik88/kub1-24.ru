<?php

namespace frontend\modules\cash\modules\ofd\models;

use common\models\Company;
use common\models\employee\Employee;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "ofd_params".
 *
 * @property integer $company_id
 * @property string $ofd_alias
 * @property string $param_name
 * @property string $param_value
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Employee $updatedBy
 * @property Company $company
 * @property Employee $createdBy
 */
class OfdParams extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ofd_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'ofd_alias', 'param_name', 'param_value'], 'required'],
            [['company_id', 'created_by', 'updated_by'], 'integer'],
            [['ofd_alias'], 'string'],
            [['param_name'], 'string'],
            [['param_value'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'ofd_alias' => 'OFD alias',
            'param_name' => 'Param Name',
            'param_value' => 'Param Value',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @inheritdoc
     */
    public static function setValue(Company $company, $ofdAlias, $paramName, $paramValue)
    {
        if (!empty($paramName) && !empty($company->id) && !empty($ofdAlias)) {
            $model = static::findOne([
                'company_id' => $company->id,
                'ofd_alias' => $ofdAlias,
                'param_name' => $paramName,
            ]);
            if ($model === null) {
                $model = new static([
                    'company_id' => $company->id,
                    'ofd_alias' => $ofdAlias,
                    'param_name' => $paramName,
                ]);
            }
            $model->param_value = (string) $paramValue;

            return $model->save();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public static function getValue(Company $company, $ofdAlias, $paramName, $default = null, $attribute = 'param_value')
    {
        $model = static::findOne([
            'company_id' => $company->id,
            'ofd_alias' => $ofdAlias,
            'param_name' => $paramName,
        ]);

        return $model !== null ? $model->$attribute : $default;
    }

    /**
     * @inheritdoc
     */
    public static function deleteValue(Company $company, $ofdAlias, $paramName)
    {
        return Yii::$app->db->createCommand()->delete(self::tableName(), [
            'company_id' => $company->id,
            'ofd_alias' => $ofdAlias,
            'param_name' => $paramName,
        ])->execute();
    }
}
