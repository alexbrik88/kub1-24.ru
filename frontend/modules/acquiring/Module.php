<?php

namespace frontend\modules\acquiring;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{
    /**
     * @inheritDoc
     */
    public $controllerNamespace = 'frontend\modules\acquiring\controllers';
}
