<?php

namespace frontend\modules\acquiring\controllers;

use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\modules\acquiring\commands\MonetaImportCommand;
use frontend\components\FrontendController;
use frontend\controllers\WebTrait;
use common\modules\acquiring\models\Acquiring;
use frontend\modules\acquiring\models\MonetaCredentialsForm;
use frontend\modules\acquiring\models\MonetaDisconnectForm;
use yii\helpers\Url;
use yii\web\Response;

class MonetaController extends FrontendController
{
    use WebTrait;

    /** @var string[] */
    private const REDIRECT_URL = ['/acquiring'];

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            \frontend\rbac\UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'disconnect' => [
                'class' => DisconnectAction::class,
                'formClass' => MonetaDisconnectForm::class,
                'commandClass' => MonetaImportCommand::class,
                'redirectUrl' => Url::to(self::REDIRECT_URL),
                'successMessage' => 'Интеграция с Moneta.ru отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с Moneta.ru.',
            ],
            'upload' => [
                'class' => ImportAction::class,
                'commandClass' => MonetaImportCommand::class,
                'acquiringType' => Acquiring::TYPE_MONETA,
                'redirectUrl' => Url::to(self::REDIRECT_URL),
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => MonetaImportCommand::class,
                'acquiringType' => Acquiring::TYPE_MONETA,
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionConnect(): Response
    {
        $form = new MonetaCredentialsForm($this->user->identity);

        if ($form->load($this->request->post()) && $form->validate() && $form->updateCredentials()) {
            $this->session->setFlash('success', 'Интеграция с Moneta.ru подключена.');

            return $this->redirect($this->request->get('redirectUrl', self::REDIRECT_URL));
        }

        $this->response->content = $this->renderAjax('connect', compact('form'));

        return $this->response;
    }
}
