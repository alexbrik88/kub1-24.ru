<?php

namespace frontend\modules\acquiring\controllers;

use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\DisconnectFormInterface;
use frontend\controllers\WebTrait;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\web\Response;

class DisconnectAction extends Action
{
    use WebTrait;

    /**
     * @var string
     */
    public $commandClass;

    /**
     * @var string
     */
    public $successMessage;

    /**
     * @var string
     */
    public $errorMessage;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @var string|DisconnectFormInterface
     */
    public $formClass;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->commandClass)) {
            throw new InvalidConfigException();
        }

        if (!isset($this->successMessage, $this->errorMessage)) {
            throw new InvalidConfigException();
        }

        if (!is_a($this->formClass, DisconnectFormInterface::class, true)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $identifier = $this->request->get('identifier');
        $form = $this->formClass::createDisconnectForm(
            $this->user->identity->company,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass),
            is_scalar($identifier) ? $identifier : ''
        );

        if ($form->load($this->request->get(), '') && $form->validate() && $form->removeIntegration()) {
            $this->session->setFlash('success', $this->successMessage);
        } else {
            $this->session->setFlash('error', $this->errorMessage);
        }

        return $this->controller->redirect($this->redirectUrl);
    }
}
