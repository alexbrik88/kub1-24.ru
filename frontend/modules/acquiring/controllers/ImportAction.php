<?php

namespace frontend\modules\acquiring\controllers;

use common\modules\import\components\ImportCommandManager;
use frontend\controllers\ActionInterface;
use frontend\controllers\WebTrait;
use frontend\modules\acquiring\models\ImportForm;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\Response;

class ImportAction extends Action implements ActionInterface
{
    use WebTrait;

    /**
     * @var string
     */
    public $commandClass;

    /**
     * @var string
     */
    public $acquiringType;

    /**
     * @var string
     */
    public $disconnectUrl;

    /**
     * @var string
     */
    public $redirectUrl;

    /**
     * @var string
     */
    public $viewFile = 'import';

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->redirectUrl, $this->commandClass, $this->viewFile, $this->acquiringType)) {
            throw new InvalidConfigException();
        }

        if ($this->disconnectUrl === null) {
            $this->disconnectUrl = Url::to(['disconnect']);
        }
    }

    /**
     * @inheritDoc
     */
    public function run(): Response
    {
        $identifier = $this->request->get('identifier');
        $redirectUrl = $this->request->get('redirectUrl', $this->redirectUrl);
        $disconnectUrl = sprintf('%s?%s', $this->disconnectUrl, http_build_query(compact('identifier', 'redirectUrl')));

        $form = new ImportForm(
            $this->user->identity,
            ImportCommandManager::getInstance()->getTypeByClass($this->commandClass),
            $this->acquiringType,
            is_scalar($identifier) ? $identifier : '',
        );

        if ($form->load($this->request->post()) && $form->validate()) {
            if (!$form->createJob()) {
                $this->session->setFlash('error', 'Произошла ошибка при создании задачи на выгрузку операций.');
            } else {
                $this->session->setFlash('success', 'Задача на загрузку операций создана.');
            }

            return $this->controller->redirect($redirectUrl);
        }

        $this->response->content = $this->controller
            ->renderAjax($this->viewFile, compact('form', 'disconnectUrl', 'identifier'));

        return $this->response;
    }
}
