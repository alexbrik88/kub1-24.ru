<?php

namespace frontend\modules\acquiring\controllers;

use common\modules\acquiring\components\YookassaComponent;
use common\modules\acquiring\commands\YookassaImportCommand;
use frontend\modules\acquiring\models\YookassaPaymentForm;
use frontend\modules\acquiring\models\YookassaTokenForm;
use frontend\controllers\WebTrait;
use common\modules\acquiring\models\Acquiring;
use frontend\modules\acquiring\models\YookassaDisconnectForm;
use frontend\rbac\UserRole;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class YookassaController extends Controller
{
    use WebTrait;

    /** @var string[] */
    private const REDIRECT_URL = ['/acquiring'];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'accessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                        ],
                        'actions' => [
                            'code',
                            'token',
                            'disconnect',
                            'payment',
                            'test',
                            'upload',
                            'set-auto-import',
                            'connect',
                        ],
                        'denyCallback' => function(): void {
                            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function actions()
    {
        return [
            'disconnect' => [
                'class' => DisconnectAction::class,
                'formClass' => YookassaDisconnectForm::class,
                'commandClass' => YookassaImportCommand::class,
                'redirectUrl' => Url::to(self::REDIRECT_URL),
                'successMessage' => 'Интеграция с ЮKassa отключена.',
                'errorMessage' => 'Произошла ошибка при отключении интеграции с ЮKassa.',
            ],
            'upload' => [
                'class' => ImportAction::class,
                'commandClass' => YookassaImportCommand::class,
                'acquiringType' => Acquiring::TYPE_YOOKASSA,
                'redirectUrl' => Url::to(self::REDIRECT_URL),
            ],
            'set-auto-import' => [
                'class' => SetAutoImportAction::class,
                'commandClass' => YookassaImportCommand::class,
                'acquiringType' => Acquiring::TYPE_YOOKASSA,
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionCode(): Response
    {
        $redirectUrl = $this->request->get('redirectUrl', self::REDIRECT_URL);
        $this->session->set('redirectUrl', $redirectUrl);

        return $this->redirect(YookassaComponent::getInstance()->getAuthorizationUrl());
    }

    /**
     * @return Response
     */
    public function actionToken(): Response
    {
        $form = new YookassaTokenForm($this->user->identity);

        if ($form->load($this->request->get(), '') && $form->validate()) {
            if ($form->updateToken()) {
                $this->session->setFlash('success', 'Интеграция с ЮKassa подключена.');
            } elseif ($form->hasErrors('code')) {
                $this->session->setFlash('error', $form->getFirstError('code'));
            }
        }

        $redirectUrl = $this->session->get('redirectUrl', self::REDIRECT_URL);

        return $this->redirect($redirectUrl);
    }

    /**
     * @return Response
     * @throws
     */
    public function actionPayment(): Response
    {
        $identifier = $this->request->get('identifier');
        $form = new YookassaPaymentForm(
            $this->user->identity->company,
            is_scalar($identifier) ? $identifier : ''
        );

        if ($form->createPayment()) {
            return $this->redirect($form->response->confirmation->getConfirmationUrl());
        }

        throw new BadRequestHttpException();
    }

    /**
     * @return Response
     */
    public function actionConnect(): Response
    {
        $redirectUrl = $this->request->get('redirectUrl', self::REDIRECT_URL);
        $this->response->content = $this->renderAjax('connect', compact('redirectUrl'));

        return $this->response;
    }
}
