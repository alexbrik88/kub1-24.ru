<?php

namespace frontend\modules\acquiring\controllers;

use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\models\employee\Employee;
use frontend\components\FrontendController;
use frontend\components\PageSize;
use frontend\controllers\WebTrait;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\acquiring\models\AcquiringOperationsSearch;
use frontend\modules\acquiring\models\ActiveAcquiringForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AcquiringController extends FrontendController
{
    use WebTrait;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex(): string
    {
        $activeAcquiringForm = new ActiveAcquiringForm($this->user->identity);

        if ($activeAcquiringForm->load($this->request->get(), '')) {
            if ($activeAcquiringForm->validate()) {
                $activeAcquiringForm->setActiveAcquiring();
            } else {
                throw new NotFoundHttpException();
            }
        }

        $activeItem = $activeAcquiringForm->getAcquiringItem();
        $searchModel = new AcquiringOperationsSearch([
            'company_id' => $this->user->identity->company->id,
            'acquiring_type' => $activeItem ? $activeItem->type : null,
            'acquiring_identifier' => $activeItem ? $activeItem->identifier : null,
        ]);
        $dataProvider = $searchModel->search($activeItem, Yii::$app->request->get());
        $dataProvider->pagination->pageSize = PageSize::get();

        // TODO: to view
        if (Acquiring::find()->andWhere(['company_id' => $this->user->identity->company->id])->exists()) {
            $emptyMessage = 'В указанном периоде нет операций по эквайрингу';
        } else {
            $emptyMessage = 'У вас не подключен интернет-эквайринг';
        }

        return $this->render('index', [
            'activeItem' => $activeItem,
            'company' => $this->user->identity->company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'emptyMessage' => $emptyMessage,
        ]);
    }

    /**
     * @param $id
     * @return mixed|string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findOperationModel($id);
        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->post('fromOdds', 0)) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return true;
            }

            return $this->redirect(Yii::$app->request->referrer ?? ['index']);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionManyItem()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        $incomeItemID = Yii::$app->request->post('incomeItemIdManyItem');
        $expenseItemID = Yii::$app->request->post('expenditureItemIdManyItem');

        foreach ($idArray as $id) {
            $model = $this->findOperationModel($id);
            $attribute = $model->flow_type == AcquiringOperation::FLOW_TYPE_INCOME ? 'income_item_id' : 'expenditure_item_id';
            $value = $model->flow_type == AcquiringOperation::FLOW_TYPE_INCOME ? $incomeItemID : $expenseItemID;
            $model->$attribute = $value;
            $model->save(true, [$attribute]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findOperationModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Операция удалена');
        } else {
            Yii::$app->session->setFlash('success', 'Операция не удалена');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDelete()
    {
        $idArray = Yii::$app->request->post('flowId', []);
        foreach ($idArray as $id) {
            if ($model = $this->findOperationModel($id)) {
                $model->delete();
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $id
     * @return AcquiringOperation
     * @throws NotFoundHttpException
     */
    private function findOperationModel($id): AcquiringOperation {
        /** @var Employee $user */
        $user = Yii::$app->user->identity;
        /** @var AcquiringOperation $acquiring */
        $acquiring = AcquiringOperation::find()
            ->andWhere(['company_id' => $user->currentEmployeeCompany->company_id])
            ->andWhere(['id' => $id])
            ->one();
        if ($acquiring === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $acquiring;
    }
}
