<?php

namespace frontend\modules\acquiring\models;

use common\models\Company;
use common\models\FormInterface;
use common\modules\acquiring\models\YookassaIdentity;
use yii\base\Model;
use YooKassa\Client;
use YooKassa\Request\Payments\CreatePaymentResponse;

class YookassaPaymentForm extends Model implements FormInterface
{
    /**
     * @var CreatePaymentResponse
     */
    public $response;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @param Company $company
     * @param string $identifier
     */
    public function __construct(Company $company, string $identifier)
    {
        $this->company = $company;
        $this->identifier = $identifier;

        parent::__construct();
    }

    /**
     * @return bool
     * @throws
     */
    public function createPayment(): bool
    {
        $client = new Client();
        $identity = YookassaIdentity::findOne(['company_id' => $this->company->id, 'account_id' => $this->identifier]);

        if ($identity === null) {
            return false;
        }

        $client->setAuthToken($identity->access_token);
        $me = $client->me();

        if (!$me['test']) {
            return false;
        }

        $this->response = $client->createPayment([
            'amount' => ['value' => 100.0, 'currency' => 'RUB'],
            'capture' => true,
            'description' => 'Тестовый платёж',
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => 'https://www.merchant-website.com/return_url',
            ],
        ], uniqid('', true));

        return true;
    }
}
