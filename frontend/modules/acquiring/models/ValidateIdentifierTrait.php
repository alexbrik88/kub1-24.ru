<?php

namespace frontend\modules\acquiring\models;

use common\models\Company;
use common\modules\acquiring\models\Acquiring;
use yii\helpers\ArrayHelper;

/**
 * @property-read string[] $identifierList
 */
trait ValidateIdentifierTrait
{
    /**
     * @var string[]
     */
    private $_identifierList;

    /**
     * @return string[]
     */
    public function getIdentifierList(): array
    {
        if ($this->_identifierList === null) {
            $acquiringList = Acquiring::find()
                ->indexBy('identifier')
                ->andWhere([
                    'company_id' => $this->getCompany()->id,
                    'type' => $this->getAcquiringType(),
                ])->all();

            $this->_identifierList = ArrayHelper::getColumn($acquiringList, 'identifier');
        }

        return $this->_identifierList;
    }

    /**
     * @inheritDoc
     */
    protected function identifierRules()
    {
        return [
            [['identifier'], 'required'],
            [['identifier'], 'string'],
            [['identifier'], 'in', 'range' => array_keys($this->identifierList)],
        ];
    }

    /**
     * @return int
     */
    abstract public function getAcquiringType(): int;

    /**
     * @return Company
     */
    abstract public function getCompany(): Company;
}
