<?php

namespace frontend\modules\acquiring\models;

use common\models\Company;
use common\models\FormInterface;
use common\modules\acquiring\models\AcquiringIdentifierList;
use common\modules\import\models\AbstractAutoImportForm;
use common\modules\import\components\ImportCommandType;

class AutoImportForm extends AbstractAutoImportForm implements FormInterface
{
    /**
     * @var int
     */
    private $acquiringType;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param int $acquiringType
     * @param string $identifier
     */
    public function __construct(
        Company $company,
        ImportCommandType $commandType,
        int $acquiringType,
        string $identifier
    ) {
        $this->acquiringType = $acquiringType;

        parent::__construct($company, $commandType, new AcquiringIdentifierList($company, $acquiringType), $identifier);
    }

    /**
     * @inheritDoc
     */
    public function getAcquiringType(): int
    {
        return $this->acquiringType;
    }
}
