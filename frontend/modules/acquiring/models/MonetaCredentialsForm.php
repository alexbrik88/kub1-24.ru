<?php

namespace frontend\modules\acquiring\models;

use common\models\employee\Employee;
use common\models\FormInterface;
use common\modules\acquiring\components\MonetaComponent;
use common\modules\acquiring\models\MonetaIdentity;
use GuzzleHttp\Exception\ClientException;
use yii\base\Model;

class MonetaCredentialsForm extends Model implements FormInterface
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Employee $employee, array $config = [])
    {
        $this->employee = $employee;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['username'], 'required'],
            [['username'], 'string'],
            [['username'], 'trim'],
            [['password'], 'required'],
            [['password'], 'string'],
        ];
    }

    /**
     * @return bool
     */
    public function updateCredentials(): bool
    {
        $accountId = $this->getAccountId();

        if ($accountId && $this->createIdentity($accountId)) {
            return true;
        }

        return false;
    }

    /**
     * @param int $accountId
     * @return bool
     */
    private function createIdentity(int $accountId): bool
    {
        $identity = MonetaIdentity::findByAccountId($accountId);

        if ($identity && $identity->company_id != $this->employee->company->id) {
            $this->addError('username', 'Аккаунт уже подключен к другой компании.');

            return false;
        }

        if ($identity === null) {
            $identity = new MonetaIdentity([
                'company_id' => $this->employee->company->id,
                'account_id' => $accountId,
                'username' => $this->username,
                'password' => $this->password,
            ]);
        }

        if ($identity->save()) {
            $activeAcquiringForm = new ActiveAcquiringForm($this->employee, [
                'activeItem' => $identity->acquiring->id,
            ]);
            $activeAcquiringForm->setActiveAcquiring();

            return true;
        }

        return false;
    }

    /**
     * @return int|null
     */
    private function getAccountId(): ?int
    {
        $moneta = MonetaComponent::getInstance();
        $moneta->setUsername($this->username);
        $moneta->setPassword($this->password);

        try {
            $response = $moneta->getProfileInfo();
            $attributes = $response['Envelope']['Body']['GetProfileInfoResponse']['attribute'] ?? [];

            foreach ($attributes as $attribute) {
                if ($attribute['key'] === 'primaryaccountid') {
                    return $attribute['value'];
                }
            }

            $this->addError('password', 'Не найдено ни одного счета.');
        } catch (ClientException $e) {
            $errorResponse = json_decode($e->getResponse()->getBody(), true);
            $errorCode = $errorResponse['Envelope']['Body']['fault']['detail']['faultDetail'] ?? null;

            if ($errorCode == '300.1') {
                $this->addError('password', 'Неверный логин или пароль.');
            }
        }

        return null;
    }
}
