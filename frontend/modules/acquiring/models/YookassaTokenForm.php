<?php

namespace frontend\modules\acquiring\models;

use common\models\employee\Employee;
use common\modules\acquiring\components\YookassaComponent;
use common\modules\acquiring\components\YookassaException;
use common\models\FormInterface;
use common\modules\acquiring\models\YookassaIdentity;
use yii\base\Model;
use yii\db\Expression;
use YooKassa\Client;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;

class YookassaTokenForm extends Model implements FormInterface
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Employee $employee, array $config = [])
    {
        $this->employee = $employee;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'string', 'min' => 1, 'max' => 256],
        ];
    }

    /**
     * @return bool
     */
    public function updateToken(): bool
    {
        try {
            $credentials = YookassaComponent::getInstance()->getCredentials($this->code);
            $client = new Client();
            $client->setAuthToken($credentials['access_token']);

            return $this->createIdentity(
                $client->me()['account_id'],
                $credentials['access_token'],
                $credentials['expires_in']
            );
        } catch (ApiException | ExtensionNotFoundException | YookassaException $exception) {
            $this->addError('code', $exception->getMessage());

            return false;
        }
    }

    /**
     * @param string $accountId
     * @param string $accessToken
     * @param int $expiresIn
     * @return bool
     */
    private function createIdentity(string $accountId, string $accessToken, int $expiresIn): bool
    {
        $identity = YookassaIdentity::findByAccountId($accountId);

        if ($identity && $identity->company_id != $this->employee->company->id) {
            $this->addError('code', 'Аккаунт уже подключен к другой компании.');

            return false;
        }

        if ($identity === null) {
            $identity = new YookassaIdentity([
                'company_id' => $this->employee->company->id,
                'access_token' => $accessToken,
                'account_id' => $accountId,
                'expired_at' => new Expression("DATE_ADD(NOW(), INTERVAL {$expiresIn} SECOND)"),
            ]);
        }

        if ($identity->save()) {
            $activeAcquiringForm = new ActiveAcquiringForm($this->employee, [
                'activeItem' => $identity->acquiring->id,
            ]);
            $activeAcquiringForm->setActiveAcquiring();

            return true;
        }

        return false;
    }
}
