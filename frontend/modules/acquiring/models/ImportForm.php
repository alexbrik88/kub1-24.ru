<?php

namespace frontend\modules\acquiring\models;

use common\models\employee\Employee;
use common\modules\acquiring\models\AcquiringIdentifierList;
use common\modules\import\components\ImportCommandType;
use common\modules\import\models\AbstractImportForm2;
use common\modules\import\models\AutoImportFormInterface;

class ImportForm extends AbstractImportForm2
{
    /**
     * @var string
     */
    private $acquiringType;

    /**
     * @param Employee $employee
     * @param ImportCommandType $commandType
     * @param int $acquiringType
     * @param string $identifier
     */
    public function __construct(
        Employee $employee,
        ImportCommandType $commandType,
        int $acquiringType,
        string $identifier
    ) {
        $this->acquiringType = $acquiringType;
        $identifierList = new AcquiringIdentifierList($employee->company, $acquiringType);

        parent::__construct($employee, $commandType, $identifierList, $identifier);
    }

    /**
     * @inheritDoc
     */
    public function createAutoImportForm(): AutoImportFormInterface
    {
        return new AutoImportForm(
            $this->getEmployee()->company,
            $this->getCommandType(),
            $this->acquiringType,
            $this->identifier
        );
    }
}
