<?php

namespace frontend\modules\acquiring\models;

use common\models\employee\Employee;
use common\models\FormInterface;
use common\modules\acquiring\models\Acquiring;
use yii\base\Model;

/**
 * @property-read Acquiring[] $acquiringItems
 */
class ActiveAcquiringForm extends Model implements FormInterface
{
    /**
     * @var int|null
     */
    public $activeItem;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Acquiring[]
     */
    private $_acquiringItems;

    /**
     * @param Employee $employee
     * @param array $config
     */
    public function __construct(Employee $employee, array $config = [])
    {
        $this->employee = $employee;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['activeItem'], 'default', 'value' => $this->employee->currentEmployeeCompany->last_acquiring_id],
            [['activeItem'], 'integer'],
            [['activeItem'], 'in', 'range' => array_keys($this->acquiringItems)],
        ];
    }

    /**
     * @return Acquiring[]
     */
    public function getAcquiringItems(): array
    {
        if ($this->_acquiringItems === null) {
            $this->_acquiringItems = Acquiring::find()
                ->where(['company_id' => $this->employee->company->id])
                ->indexBy('id')
                ->all();

            $this->_acquiringItems[0] = new Acquiring();
        }

        return $this->_acquiringItems;
    }

    /**
     * @return bool
     */
    public function setActiveAcquiring(): bool
    {
        $this->employee->currentEmployeeCompany->last_acquiring_id = $this->activeItem ?: null;

        return $this->employee->currentEmployeeCompany->save(true, ['last_acquiring_id']);
    }

    /**
     * @return Acquiring|null
     */
    public function getAcquiringItem(): ?Acquiring
    {
        if ($this->employee->currentEmployeeCompany->last_acquiring_id) {
            return $this->acquiringItems[$this->employee->currentEmployeeCompany->last_acquiring_id];
        }

        return null;
    }
}
