<?php

namespace frontend\modules\acquiring\models;

use common\models\Company;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringIdentifierList;
use common\modules\acquiring\models\YookassaIdentity;
use common\modules\import\components\ImportCommandType;
use common\modules\import\models\AbstractDisconnectForm;
use common\modules\import\models\DisconnectFormInterface;

class YookassaDisconnectForm extends AbstractDisconnectForm implements DisconnectFormInterface
{
    /**
     * @inheritDoc
     */
    public static function createDisconnectForm(
        Company $company,
        ImportCommandType $commandType,
        string $identifier
    ): DisconnectFormInterface {
        $identifierList = new AcquiringIdentifierList($company, Acquiring::TYPE_YOOKASSA);

        return new static($company, $commandType, $identifierList, $identifier);
    }

    /**
     * @return bool
     * @throws
     */
    protected function removeAccount(): bool
    {
        $identity = YookassaIdentity::findOne([
            'company_id' => $this->getCompany()->id,
            'account_id' => $this->identifier,
        ]);

        if ($identity && !$identity->delete()) {
            return false;
        }

        return true;
    }
}
