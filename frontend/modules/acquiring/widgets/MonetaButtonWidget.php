<?php

namespace frontend\modules\acquiring\widgets;

use common\models\employee\Employee;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringRepository;
use frontend\themes\kub\widgets\AbstractImportButtonWidget;
use yii\helpers\Url;

class MonetaButtonWidget extends AbstractImportButtonWidget
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @inheritDoc
     */
    protected function getConnectUrl(string $redirectUrl): string
    {
        return Url::to([
            '/acquiring/moneta/connect',
            'redirectUrl' => sprintf('%s#%s', $redirectUrl, $this->getButtonId('import')),
        ]);
    }

    /**
     * @inheritDoc
     */
    protected function getImportUrl(string $redirectUrl): string
    {
        return Url::to([
            '/acquiring/moneta/upload',
            'redirectUrl' => $redirectUrl,
            'identifier' => $this->identifier,
        ]);
    }

    /**
     * @inheritDoc
     * @throws
     */
    protected function hasAccounts(): bool
    {
        $employee = $this->getUser()->getIdentity();

        if ($employee instanceof Employee) {
            $models = AcquiringRepository::findActiveAcquiring($employee->company, Acquiring::TYPE_MONETA);

            if ($models) {
                return true;
            }
        }

        return false;
    }
}
