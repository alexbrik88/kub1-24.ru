<?php
namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\pdf\PdfRenderer;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderProxy;
use common\models\document\Proxy;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\ProxyStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file;
use common\models\product\Product;
use common\models\TimeZone;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\document\status\InvoiceStatus;
use frontend\modules\documents\models\InvoiceSearch;

/**
 * Proxy controller
 */
class ProxyController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_PROXY;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create-employee'],
                        'allow' => true,
                        'roles' => [permissions\Employee::CREATE],
                    ],
                    [
                        'actions' => ['generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['send'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['upd',],
                        'allow' => YII_ENV === 'dev',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id)
    {
        $model = $this->loadModel($id, $type);

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
        ]);
    }


    public function actionUpdate($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (Yii::$app->request->post('OrderProxy') != null) {
                foreach (Yii::$app->request->post('OrderProxy') as $key => $value) {
                    $opl = OrderProxy::findOne(['order_id' => $key, 'proxy_id' => $model->id]);
                    if ($value['quantity'] > 0 && $value['status'] == 'active') {
                        if ($opl != null) {
                            if ($opl->getAvailableQuantity() >= $value['quantity'] && $value['quantity'] > 0) {
                                OrderProxy::setQuantity($key, $model->id, $value['quantity']);
                            }
                        } else {

                            if ($value['quantity'] != 0 && $value['quantity'] <= OrderProxy::compareWithOrderByQuantity(Order::findOne(['id' => $key]))) {
                                OrderProxy::createFromOrder($model->id, Order::findOne(['id' => $key]))->save();
                            }

                        }
                    } elseif ($value['status'] == 'deleted' && OrderProxy::ifExist($key, $model->id)) {
                        OrderProxy::deleteAll(['order_id' => $key, 'proxy_id' => $model->id]);
                    }

                }

                Yii::$app->db->transaction(function ($db) use ($model, $type) {
                    $model->document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                    $model->ordinal_document_number = $model->document_number;
                    $model->orders_sum = $model->totalAmountWithNds;
                    if (!$model->save()) {
                        foreach ($model->getErrors() as $error) {
                            Yii::$app->session->setFlash('error', $error);
                        }
                    } else {
                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                        Yii::$app->session->setFlash('success', 'Доверенность изменена');
                    }
                    if ($model->errors) {
                        return $this->refresh();
                    }

                });

                return $this->redirect([
                    'view',
                    'type' => $type,
                    'id' => $model->id,
                ]);
            } else {
                $model->delete();
                return $this->render('index');
            }

            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid)
    {
        /* @var Invoice $model */
        $model = Proxy::find()
            ->byIOType(Documents::IO_TYPE_IN)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), 'print', 'pdf-view', [
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-P',
            'proxy_id' => $id,
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $proxiesID = explode(',', $multiple);
        /* @var $model Proxy */
        $model = $this->loadModel(current($proxiesID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-P',
            'addStamp' => ($actionType === 'pdf' && $model->invoice->company->pdf_signed) ? true : false,
            'multiple' => $proxiesID,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        /* @var Proxy $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = Proxy::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
                // $model->email_messages += $saved[0];
                // $model->save(true, ['email_messages']);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        $proxies = Yii::$app->request->post('Proxy');
        $send = 0;
        foreach ($proxies as $id => $proxy) {
            if ($proxy['checked']) {
                /* @var $model Proxy */
                $model = $this->loadModel($id, $type);
                if ($model->uid == null) {
                    $model->uid = Proxy::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = null;
                if ($model->invoice->contractor->director_email) {
                    $sendTo = $model->invoice->contractor->director_email;
                } elseif ($model->invoice->contractor->chief_accountant_email) {
                    $sendTo = $model->invoice->contractor->chief_accountant_email;
                } elseif ($model->invoice->contractor->contact_email) {
                    $sendTo = $model->invoice->contractor->contact_email;
                }
                if ($sendTo) {
                    /** @var Employee $sender */
                    $sender = \Yii::$app->user->identity;
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'Доверенность',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];

                    \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (\Yii::$app->mailer->compose([
                        'html' => 'system/documents/proxy-in/html',
                        'text' => 'system/documents/proxy-in/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('Доверенность No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(Proxy::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send += 1;
                        InvoiceSendForm::setProxySendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            \common\models\company\CompanyFirstEvent::checkEvent($company, 24);
            $message = 'Отправлено ' . $send . ' из ' . count($proxies) . ' писем.';
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки доверенностей, необхоидмо заполнить E-mail у покупателей:<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }
        if ($notSend) {
            Yii::$app->session->setFlash('error', $notSend);
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Proxy $model */
        $model = $this->loadModel($id, $type);

        $status = (int) Yii::$app->request->getBodyParam('status');

        $this->_updateStatus($model, $status);

        switch ($model->status_out_id) {
            case ProxyStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'Доверенность передана');
                break;
            case ProxyStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'Доверенность подписана');
                break;
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param Proxy $model
     * @param $status
     * @throws \yii\base\Exception
     */
    private function _updateStatus(Proxy $model, $status)
    {
        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS,
                function (Proxy $model) {
                    return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                });
        }
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId, $limitDate = null, $proxyPersonId = null)
    {
        /* @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');

        if ($invoice->canAddProxy) {
            if ($invoice->createProxy(null, $documentNumber, $limitDate, $proxyPersonId)) {
                $orderDocument = $invoice->orderDocument;
                if ($orderDocument) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                }
                Yii::$app->session->setFlash('success', 'Доверенность создана.');

                return $this->redirect([
                    'view',
                    'type' => $invoice->proxy->type,
                    'id' => $invoice->proxy->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'При создании доверенности возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать доверенность.');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        $company = Yii::$app->user->identity->company;
        $idArray = [];
        $documentDate = !empty(Yii::$app->request->post('Proxy')['document_date']) ?
            Yii::$app->request->post('Proxy')['document_date'] : null;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            /* @var $invoice Invoice */
            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddProxy) {
                    $orderDocument = $invoice->orderDocument;
                    if ($orderDocument) {
                        $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                        $orderDocument->save(true, ['status_id']);
                    }
                    $invoice->createProxy($documentDate);
                }
            }
        } else {
            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    ['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddProxy()) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                    $orderDocument->invoice->createProxy($documentDate);
                }
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }
        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }


    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if (LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT)) {
            Yii::$app->session->setFlash('success', 'Доверенность удалена');
        } else {
            Yii::$app->session->setFlash('error', 'Доверенность не удалось удалить');
        }

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $proxies = Yii::$app->request->post('Proxy');
        foreach ($proxies as $id => $proxy) {
            if ($proxy['checked']) {
                $model = $this->loadModel($id, $type);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @return mixed
     */
    public function actionUpd()
    {
        $renderer = new PdfRenderer([
            'view' => 'upd',
            'params' => [],

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => 'upd',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => ArrayHelper::getValue([], 'documentFormat', 'A4-P'),
        ]);

        return $renderer->renderHtml();
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $period = StatisticPeriod::getSessionPeriod();
        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $proxies = $searchModelOut->search(Yii::$app->request->queryParams, $period, true);

        Proxy::generateXlsTable($period, $proxies);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionAddStamp($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        return $model->updateAttributes(['add_stamp' => (boolean) Yii::$app->request->post('add_stamp')]);
    }

    /**
     * @return mixed
     */
    public function actionCreateEmployee()
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('index');
        }

        $company = Yii::$app->user->identity->company;

        $model = new EmployeeCompany([
            'scenario' => EmployeeCompany::SCENARIO_CREATE_PROXY,
            'send_email' => false,
            'company_id' => $company->id,
            'employee_role_id' => EmployeeRole::ROLE_EMPLOYEE,
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'is_product_admin' => false,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save(true)) {
            Yii::$app->session->setFlash('success', 'Сотрудник добавлен');

            return $this->render('_viewPartials/_create_employee_view', [
                'model' => $model,
            ]);
        }

        return $this->render('_viewPartials/_create_employee_form', [
            'model' => $model,
        ]);
    }

    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $companyId = \Yii::$app->user->identity->company->id;
        $ioType    = Documents::IO_TYPE_IN;
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');
        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'company_id' => $companyId,
            'can_add_proxy' => true,
            'withProductsOnly' => true
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        $fromInvoice = Yii::$app->request->get('from_invoice', null);
        $invoiceId = ($fromInvoice) ?: \Yii::$app->request->get('invoice_id', null);

        $model = new Proxy();
        $model->type = $ioType;
        $model->limit_date = date('Y-m-d', time() + 3600 * 24 * 10);
        $model->document_date = date('Y-m-d');
        $model->load(Yii::$app->request->get());

        $eventDateChange = \Yii::$app->request->get('event_date_change', -1);
        $eventFirstOpen  = $eventDateChange == -1;

        if ($eventDateChange || $eventFirstOpen) {

            $model->clearErrors();
            $invoiceId = 0;

        } else {

            if (!empty($invoiceId)) {

                $model->invoice_id = $invoiceId;

                if (!Invoice::find()->where(['id' => $invoiceId, 'company_id' => $companyId])->exists()) {
                    $model->addError('invoice_id', 'Необходимо выбрать');
                }

                if ($model->validate($attributeNames = ['invoice_id', 'proxy_person_id', 'limit_date_input'], false)) {

                    /* @var Invoice $invoice */
                    $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $ioType);
                    $documentNumber = Yii::$app->request->get('documentNumber');

                    if ($invoice->canAddProxy) {
                        // createProxy($date = null, $documentNumber = null, $limitDate = null, $proxyPersonId = null)
                        if ($invoice->createProxy(null, null, $model->limit_date, $model->proxy_person_id)) {
                            $orderDocument = $invoice->orderDocument;
                            if ($orderDocument) {
                                $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                                $orderDocument->save(true, ['status_id']);
                            }
                            Yii::$app->session->setFlash('success', 'Доверенность создана.');

                            return $this->redirect([
                                'view',
                                'type' => $invoice->proxy->type,
                                'id' => $invoice->proxy->id,
                                'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', 'При создании доверенности возникли ошибки.');
                        }
                    } else {
                        Yii::$app->session->setFlash('error', 'Нельзя создать доверенность.');
                    }
                }

            } else {
                if ($model->validate($attributeNames = ['proxy_person_id', 'limit_date_input'], $clearErrors = true)) {
                    return $this->redirect([
                        '/documents/invoice/create',
                        'type' => $ioType,
                        'document' => Documents::SLUG_PROXY,
                        'limit_date' => DateHelper::format($model->limit_date_input, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE),
                        'proxy_person_id' => $model->proxy_person_id
                    ]);
                }
            }

        }

        return $this->renderAjax('/proxy/_viewPartials/_invoices_modal_table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName,
            'ioType' => Documents::IO_TYPE_IN,
            'model' => $model,
            'invoiceId' => $fromInvoice ?: $invoiceId,
            'companyId' => $companyId,
            'fromInvoice' => $fromInvoice
        ]);
    }
}
