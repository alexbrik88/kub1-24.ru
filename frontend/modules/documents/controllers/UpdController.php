<?php

namespace frontend\modules\documents\controllers;

use common\models\address\Country;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\models\document\status\UpdStatus;
use common\models\document\OrderUpd;
use common\models\document\Upd;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\rbac\permissions\document\Document;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use frontend\rbac\permissions;

/**
 * UpdController implements the CRUD actions for Upd model.
 */
class UpdController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_UPD;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['get-xls', 'generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create-for-several-invoices',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            /* @var $user EmployeeCompany */
                            $user = Yii::$app->user->identity->currentEmployeeCompany;
                            return (Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) || $user->employee_role_id == EmployeeRole::ROLE_MANAGER)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'select-product', 'add-product', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(Document::VIEW, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-update-original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'ioType' => Yii::$app->request->get('type'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(Document::UPDATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['send-many-in-one'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(Document::STRICT_MODE);
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($type, $id)
    {
        /* @var Upd $model */
        $model = $this->loadModel($id, $type);

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionSelectProduct($type, $id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad Request');
        }
        /* @var Upd $model */
        $model = $this->loadModel($id, $type);
        $existOrdersId = array_keys(Yii::$app->request->post('orderParams', []));
        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->getPosibleOrders()->andWhere(['not', ['order.id' => $existOrdersId]])->all(),
            'sort' => false,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->renderAjax('viewPartials/_available_orders', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddProduct($type, $id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad Request');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var Upd $model */
        $model = $this->loadModel($id, $type);
        $invoiceOrdersId = Yii::$app->request->post('orders', []);
        $orderRows = [];
        $invoices = ArrayHelper::getColumn($model->invoices, 'id');
        $countryList = Country::find()->select('name_short')->indexBy('id')->column();
        foreach (Order::find()
                     ->andWhere(['in', 'order.invoice_id', $invoices])
                     ->andWhere(['id' => $invoiceOrdersId])->all() as $invoiceOrder) {
            if ($order = OrderUpd::createFromOrder($model->id, $invoiceOrder)) {
                $row = $model->type == Documents::IO_TYPE_IN ? '_order_list_in_row' : '_order_list_out_row';
                $orderRows[] = $this->renderPartial('viewPartials/' . $row, [
                    'model' => $model,
                    'orderUpd' => $order,
                    'precision' => $model->invoice->price_precision,
                    'countryList' => $countryList,
                ]);
            }
        }

        return ['rows' => $orderRows];
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /** @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        $documentDate = Yii::$app->request->get('documentDate');
        $isFromInvoiceForm = Yii::$app->request->get('isFromInvoiceForm');
        if (empty($documentDate) && $invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = $invoice->document_date;
            $documentNumber = $invoice->document_number;
        }
        if ($invoice->canAddUpd) {
            if (!$invoice->need_upd) {
                if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                    $invoice->need_upd = true;
                    $invoice->save(true, ['need_upd']);
                } else {
                    throw new BadRequestHttpException();
                }
            }
            if ($invoice->createUpd($documentDate, $documentNumber)) {

                Yii::$app->session->setFlash('success', 'УПД создан.');

                //// 20-204.2: Product::updateGrowingPriceForBuy($invoice);

                if ($this->isScanPreview)
                {
                    if (Yii::$app->request->get('returnToManager')) {
                        Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                        return $this->redirect(Url::previous('lastUploadManagerPage'));
                    }

                    return $this->redirect([
                        '/documents/upd/update',
                        'type' => $invoice->upd->type,
                        'id' => $invoice->upd->id,
                        'mode' => 'previewScan',
                        'files' => Yii::$app->request->get('files')
                    ]);
                }

                if ($invoice->upd->type == Documents::IO_TYPE_IN && !$isFromInvoiceForm) {
                    return $this->redirect([
                        '/documents/upd/update',
                        'type' => $invoice->upd->type,
                        'id' => $invoice->upd->id
                    ]);
                }

                return $this->redirect(['view',
                    'type' => $invoice->upd->type,
                    'id' => $invoice->upd->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'При создании УПД возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать УПД.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @return Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreateForSeveralInvoices($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];
        $canAdd = true;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        if (Invoice::find()
                ->andWhere(['id' => $idArray])
                ->groupBy('contractor_id')
                ->count() == 1) {
            /* @var $invoices Invoice[] */
            $invoices = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();
            foreach ($invoices as $invoice) {
                if (!$invoice->getCanAddUpd()) {
                    $canAdd = false;
                }
            }
            if ($canAdd) {
                foreach ($invoices as $invoice) {
                    if (!$invoice->need_upd) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_upd = true;
                            $invoice->save(true, ['need_upd']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                }
                if (($updID = Invoice::createUpdForSeveralInvoices($invoices, $type))) {
                    Yii::$app->session->setFlash('success', 'УПД создан.');

                    return $this->redirect([
                        'view',
                        'type' => $type,
                        'id' => $updID,
                        'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', 'При создании УПД возникли ошибки.');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws BadRequestHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];

        $documentDate = Yii::$app->request->post('documentDate');
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $createdUPDs = 0;
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddUpd) {
                    if (!$invoice->need_upd) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_upd = true;
                            $invoice->save(true, ['need_upd']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                    if ($invoice->createUpd($documentDate))
                        $createdUPDs++;
                }
            }
        } else {

            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    //['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();

            $noInvoice = false;
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddUpd()) {
                    if ($orderDocument->invoice->createUpd($documentDate))
                        $createdUPDs++;
                }
                if (!$orderDocument->invoice) {
                    $noInvoice = true;
                }
            }

            if ($createdUPDs > 0) {
                Yii::$app->session->setFlash('success', 'УПД создан');
            } else {
                Yii::$app->session->setFlash('error', ($noInvoice) ? 'Сначала создайте Счет' : 'Не удалось создать УПД');
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }

        if ($createdUPDs > 0) {
            Yii::$app->session->setFlash('success', 'УПД создан');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать УПД');
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }

    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($type, $id)
    {
        /* @var Upd $model */
        $model = $this->loadModel($id, $type);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            $model->documentDate = $model->document_date;

            if ($model->updateAndSave(
                    Yii::$app->request->post('orderParams'),
                    Yii::$app->request->post('payment'),
                    Yii::$app->request->post('surchargeProduct'),
            )) {
                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                foreach ($model->invoices as $invoice) {
                    InvoiceHelper::checkForUpd($invoice->id, $model->invoices);
                }

                GoodsCancellation::cancellationByUpd(Yii::$app->user->identity->currentEmployeeCompany, $model);

                Yii::$app->session->setFlash('success', 'УПД изменен');

                if ($this->isScanPreview) {
                    Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                    return $this->redirect(Url::previous('lastUploadManagerPage'));
                }

                return $this->redirect(['view', 'type' => $model->type, 'id' => $model->id]);
            }
        }

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * Deletes an existing Upd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $type)
    {
        $model = $this->loadModel($id, $type);
        if ($model) {
            $invoices = $model->invoices;
            $model->delete();
            foreach ($invoices as $invoice) {
                InvoiceHelper::checkForUpd($invoice->id, $invoices);
            }
            Yii::$app->session->setFlash('success', 'УПД удален');
        }

        return $this->redirect(['index', 'type' => $type]);
    }

    /**
     * @param integer $type
     * @return mixed
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $idArray = Yii::$app->request->post('Upd', []);
        foreach ($idArray as $id) {
            $model = $this->loadModel($id, $type);
            $invoices = $model->invoices;
            LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            foreach ($invoices as $invoice) {
                InvoiceHelper::checkForUpd($invoice->id, $invoices);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Act $model */
        $model = $this->loadModel($id, $type);
        $status = UpdStatus::findOne(Yii::$app->request->getBodyParam('status'));

        if ($status !== null && $status->id >= $model->status_out_id) {
            $model->status_out_id = $status->id;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                return $model->save(false, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
            });
        }

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        switch ($status->id) {
            case UpdStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'УПД подписан');
                break;
            case UpdStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'УПД передан');
                break;
            case UpdStatus::STATUS_REJECTED:
                Yii::$app->session->setFlash('success', 'УПД отменен');
                break;
        }

        if ($status->id == UpdStatus::STATUS_PRINTED) {
            return $this->redirect([
                'document-print',
                'actionType' => 'print',
                'type' => $model->type,
                'id' => $model->id,
                'filename' => $model->getPrintTitle(),
            ]);
        } else {
            return $this->redirect(['view',
                'type' => $type,
                'id' => $model->id,
                'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
            ]);
        }
    }

    /**
     * @param string $uid
     * @return mixed
     */
    public function actionOutView($uid, $view = 'print')
    {
        /* @var Invoice $model */
        $model = Upd::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $view, 'pdf-view', [
            'isOutView' => true,
            'documentFormat' => 'A4-L',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'layout' => '@frontend/modules/documents/views/layouts/upd',
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'layout' => '@frontend/modules/documents/views/layouts/upd',
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'documentFormat' => 'A4-L',
            'layout' => '@frontend/modules/documents/views/layouts/upd',
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $actsID = explode(',', $multiple);
        /* @var $model Invoice */
        $model = $this->loadModel(current($actsID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'multiple' => $actsID,
            'documentFormat' => 'A4-L',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'layout' => '@frontend/modules/documents/views/layouts/upd',
        ]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var Act $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = Upd::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        /** @var Employee $sender */
        $sender = Yii::$app->user->identity;
        $idArray = Yii::$app->request->post('Upd');
        $send = 0;
        $contractorUpds = [];
        foreach ($idArray as $id) {
            /* @var $model Upd */
            $model = $this->loadModel($id, $type);
            if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                continue;
            }
            $contractorUpds[$model->invoice->contractor_id][] = $model->id;
        }
        foreach ($contractorUpds as $contractorID => $upds) {
            if (count($upds) > 1) {
                $contractor = Contractor::findOne($contractorID);
                $sendTo = $contractor->someEmail;
                if ($sendTo) {
                    if (Upd::manySend($sender->currentEmployeeCompany, $sendTo, $type, $upds)) {
                        $send++;
                    }
                } else {
                    $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $contractor->type, 'id' => $contractor->id
                    ]));
                }
            } else {
                $updID = current($upds);
                /* @var $model Upd */
                $model = $this->loadModel($updID, $type);
                if ($model->uid == null) {
                    $model->uid = Upd::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = $model->invoice->contractor->someEmail;
                if ($sendTo) {
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'УПД',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];
                    Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (Yii::$app->mailer->compose([
                        'html' => 'system/documents/upd-out/html',
                        'text' => 'system/documents/upd-out/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('УПД No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(Upd::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send++;
                        InvoiceSendForm::setUpdSendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            CompanyFirstEvent::checkEvent($company, 34);
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorUpds) . ' писем.';
        if ($send) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки УПД, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $period = StatisticPeriod::getSessionPeriod();
        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $upds = $searchModelOut->search(Yii::$app->request->queryParams, $period, true);

        Upd::generateXlsTable($period, $upds);
    }

    /**
     * Finds the Upd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Upd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Upd::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType    = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_upd' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'УПД',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_UPD,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionAddStamp($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        return $model->updateAttributes(['add_stamp' => (boolean)Yii::$app->request->post('add_stamp')]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $ioType = Yii::$app->request->get('type', Documents::IO_TYPE_OUT);
        $period = StatisticPeriod::getSessionPeriod();
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $ioType);
        $upds = $searchModel->search(Yii::$app->request->queryParams, $period, true, true);

        Upd::generateXlsTable($period, $upds);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateStatus($type, $status)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('Upd', []);
        if (is_array($docs)) {
            foreach ($docs as $id) {

                /* @var $model Upd */
                $model = $this->loadModel($id, $type);

                if ($status !== null && $status != $model->status_out_id) {
                    $model->status_out_id = $status;

                    LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Upd $model) {
                        return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                    });
                }

                switch ($status) {
                    case UpdStatus::STATUS_SEND:
                        Yii::$app->session->setFlash('success', 'УПД переданы');
                        break;
                    case UpdStatus::STATUS_RECEIVED:
                        Yii::$app->session->setFlash('success', 'УПД подписаны');
                        break;
                }

            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateOriginal($type, $val)
    {
        if ($type != Documents::IO_TYPE_IN) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('Upd', []);
        if (is_array($docs)) {
            foreach ($docs as $id) {
                /* @var $model Upd */
                $model = $this->loadModel($id, $type);
                $model->is_original = intval($val) ? true : false;
                $model->save(false, ['is_original', 'is_original_updated_at']);
                Yii::$app->session->setFlash('success', 'УПД сохранены');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type file type
     * @param $uid document uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getByUid($uid);

        switch ($type) {
            case 'pdf':
            default:
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean) $model->company->pdf_signed,
                ], 'A4-L');
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Act
     * @throws NotFoundHttpException
     */
    public static function getByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = empty($uid) ? null : Upd::find()
            ->joinWith('invoices.company')
            ->andWhere([
                'upd.uid' => $uid,
                'upd.type' => $type,
                'invoice.is_deleted' => false,
                'company.blocked' => false,
            ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }
}
