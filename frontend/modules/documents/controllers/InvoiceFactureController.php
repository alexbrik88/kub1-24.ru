<?php

namespace frontend\modules\documents\controllers;

use common\models\address\Country;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\document\Order;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use DateTime;
use Exception;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\models\document\Autoinvoicefacture;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceFacturePaymentDocument;
use common\models\document\OrderInvoiceFacture;
use common\models\document\PackingList;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Invoice facture controller
 */
class InvoiceFactureController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_INVOICE_FACTURE;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['get-xls', 'generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create-for-several-invoices',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            /* @var $user EmployeeCompany */
                            $user = Yii::$app->user->identity->currentEmployeeCompany;
                            return (Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) || $user->employee_role_id == EmployeeRole::ROLE_MANAGER)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ])
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-update-original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'ioType' => Yii::$app->request->get('type'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'select-product', 'add-product', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['send-many-in-one'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['autoinvoicefacture'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'update-status' => ['post'],
                    'delete' => ['post'],
                    'select-product' => ['post'],
                    'add-product' => ['post'],

                    // files
                    'file-list' => ['get'],
                    'file-upload' => ['post'],
                    'file-delete    ' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    public function actionView($type, $id)
    {
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        $view = 'view';
        $contractorId = $useContractor ? $model->invoice->contractor_id : null;
        $backUrl = null;
        if ($useContractor && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::VIEW)) {
            $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
        } elseif (Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX)) {
            $backUrl = ['index', 'type' => $model->type,];
        }
        $paymentDocuments = [];
        foreach ($model->paymentDocuments as $doc) {
            $date = date('d.m.Y', strtotime($doc->payment_document_date));
            $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
        }
        return $this->render($view, [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
            'backUrl' => $backUrl,
            'paymentDocuments' => $paymentDocuments
        ]);
    }

    public function actionUpdate($type, $id)
    {
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        if ($model->load(Yii::$app->request->post())) {
            $model->document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
            $model->ordinal_document_number = $model->document_number;

            if ($model->updateAndSave(Yii::$app->request->post('orderParams'), Yii::$app->request->post('payment'))) {
                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                Yii::$app->session->setFlash('success', 'СФ изменена');

                if ($this->isScanPreview) {
                    Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                    return $this->redirect(Url::previous('lastUploadManagerPage'));
                }

                return $this->redirect(['view',
                    'type' => $type,
                    'id' => $model->id,
                    'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
                ]);
            }
            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionSelectProduct($type, $id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad Request');
        }
        /* @var InvoiceFacture $model */
        $model = $this->loadModel($id, $type);
        $existOrdersId = array_keys(Yii::$app->request->post('orderParams', []));
        $availableOrderArray = [];
        foreach ($model->invoice->getOrders()->andWhere(['not', ['id' => $existOrdersId]])->all() as $order) {
            if (OrderInvoiceFacture::availableQuantity($order)) {
                $availableOrderArray[] = $order;
            }
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $model->getPosibleOrders()->andWhere(['not', ['order.id' => $existOrdersId]])->all(),
            'sort' => false,
            'pagination' => [
                'pageSize' => 0,
            ],
        ]);

        return $this->renderAjax('_viewPartials/_available_orders', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAddProduct($type, $id)
    {
        if (!Yii::$app->request->isAjax) {
            throw new BadRequestHttpException('Bad Request');
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var InvoiceFacture $model */
        $model = $this->loadModel($id, $type);
        $invoiceOrdersId = Yii::$app->request->post('orders', []);
        $orderRows = [];
        $invoices = ArrayHelper::getColumn($model->invoices, 'id');
        $countryList = Country::find()->select('name_short')->indexBy('id')->column();
        foreach (Order::find()
                     ->andWhere(['in', 'order.invoice_id', $invoices])
                     ->andWhere(['id' => $invoiceOrdersId])->all() as $invoiceOrder) {
            if ($order = OrderInvoiceFacture::createFromOrder($model->id, $invoiceOrder)) {
                $row = $model->type == Documents::IO_TYPE_IN ? '_order_list_in_row' : '_order_list_out_row';
                $orderRows[] = $this->renderPartial('_viewPartials/' . $row, [
                    'model' => $model,
                    'order' => $order,
                    'precision' => $model->invoice->price_precision,
                    'countryList' => $countryList,
                ]);
            }
        }

        return ['rows' => $orderRows];
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'documentFormat' => 'A4-L',
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $invoiceFacturesID = explode(',', $multiple);
        /* @var $model InvoiceFacture */
        $model = $this->loadModel(current($invoiceFacturesID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf' && $model->invoice->company->pdf_signed) ? true : false,
            'multiple' => $invoiceFacturesID,
            'documentFormat' => 'A4-L',
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var InvoiceFacture $model */
        $model = $this->loadModel($id, $type);
        if ($model->uid == null) {
            $model->uid = InvoiceFacture::generateUid();
            $model->save(false, ['uid']);
        }
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        /** @var Employee $sender */
        $sender = \Yii::$app->user->identity;
        $invoiceFactures = Yii::$app->request->post('InvoiceFacture');
        $send = 0;
        $contractorInvoiceFactures = [];
        foreach ($invoiceFactures as $id => $invoiceFacture) {
            if ($invoiceFacture['checked']) {
                /* @var $model InvoiceFacture */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }
                $contractorInvoiceFactures[$model->invoice->contractor_id][] = $model->id;
            }
        }
        foreach ($contractorInvoiceFactures as $contractorID => $invoiceFactures) {
            if (count($invoiceFactures) > 1) {
                $contractor = Contractor::findOne($contractorID);
                $sendTo = $contractor->someEmail;
                if ($sendTo) {
                    if (InvoiceFacture::manySend($sender->currentEmployeeCompany, $sendTo, $type, $invoiceFactures)) {
                        $send++;
                    }
                } else {
                    $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $contractor->type, 'id' => $contractor->id
                    ]));
                }
            } else {
                $packignListID = current($invoiceFactures);
                /* @var $model InvoiceFacture */
                $model = $this->loadModel($packignListID, $type);
                if ($model->uid == null) {
                    $model->uid = InvoiceFacture::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = $model->invoice->contractor->someEmail;
                if ($sendTo) {
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'Счет фактура',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];
                    $oneCFilePath = $model->generateOneCFile();

                    \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (\Yii::$app->mailer->compose([
                        'html' => 'system/documents/invoice-facture-out/html',
                        'text' => 'system/documents/invoice-facture-out/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('Счет фактура No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(InvoiceFacture::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        ->attach($oneCFilePath, [
                            'fileName' => $model->getOneCFileName(),
                            'mime' => 'application/xml',
                        ])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send++;
                        InvoiceSendForm::setInvoiceFactureSendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            CompanyFirstEvent::checkEvent($company, 29);
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorInvoiceFactures) . ' писем.';
        if ($send == count($contractorInvoiceFactures)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки счет фактур, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid, $view = 'print')
    {
        /* @var Invoice $model */
        $model = InvoiceFacture::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $view, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var InvoiceFacture $model */
        $model = $this->loadModel($id, $type);

        $status = (int)Yii::$app->request->getBodyParam('status');

        $this->_updateStatus($model, $status);

        switch ($status) {
            case InvoiceFactureStatus::STATUS_DELIVERED:
                Yii::$app->session->setFlash('success', 'СФ передана');
                break;
            case InvoiceFactureStatus::STATUS_REJECTED:
                Yii::$app->session->setFlash('success', 'СФ отменена');
                break;
        }

        return $this->redirect(['view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param InvoiceFacture $model
     * @param $status
     * @throws \yii\base\Exception
     */
    private function _updateStatus(InvoiceFacture $model, $status)
    {
        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (InvoiceFacture $model) {
                return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
            });
        }
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /** @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        $documentDate = Yii::$app->request->get('documentDate');
        $isFromInvoiceForm = Yii::$app->request->get('isFromInvoiceForm');
        if (empty($documentDate) && $invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = DateHelper::format($invoice->document_date, 'd.m.Y', 'Y-m-d');
            $documentNumber = $invoice->document_number;
        }
        if ($invoice->canAddInvoiceFacture) {
            if ($invoice->createInvoiceFacture($documentDate, $documentNumber)) {
                Yii::$app->session->set('show_document_date_tooltip', 1);
                Yii::$app->session->setFlash('success', 'СФ создана.');

                if ($this->isScanPreview)
                {
                    if (Yii::$app->request->get('returnToManager')) {
                        Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                        return $this->redirect(Url::previous('lastUploadManagerPage'));
                    }

                    return $this->redirect([
                        '/documents/invoice-facture/update',
                        'type' => $invoice->invoiceFacture->type,
                        'id' => $invoice->invoiceFacture->id,
                        'mode' => 'previewScan',
                        'files' => Yii::$app->request->get('files')
                    ]);
                }

                if ($invoice->invoiceFacture->type == Documents::IO_TYPE_IN && !$isFromInvoiceForm) {
                    return $this->redirect([
                        '/documents/invoice-facture/update',
                        'type' => $invoice->invoiceFacture->type,
                        'id' => $invoice->invoiceFacture->id
                    ]);
                }

                return $this->redirect(['view',
                    'type' => $invoice->invoiceFacture->type,
                    'id' => $invoice->invoiceFacture->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'При создании СФ возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать СФ.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }


    /**
     * @param $type
     * @return Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreateForSeveralInvoices($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $idArray = [];
        $canAdd = true;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        if (Invoice::find()
                ->andWhere(['id' => $idArray])
                ->groupBy('contractor_id')
                ->count() == 1) {
            /* @var $invoices Invoice[] */
            $invoices = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();
            foreach ($invoices as $invoice) {
                if (!$invoice->getCanAddInvoiceFacture()) {
                    $canAdd = false;
                }
            }
            if ($canAdd) {
                if (($updID = Invoice::createInvoiceFactureForSeveralInvoices($invoices, $type))) {
                    Yii::$app->session->setFlash('success', 'Счет-фактура создана.');

                    return $this->redirect([
                        'view',
                        'type' => $type,
                        'id' => $updID,
                        'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', 'При создании Счет-фактуры возникли ошибки.');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $idArray = [];

        $documentDate = Yii::$app->request->post('InvoiceFacture')['document_date'];
        $documentDate = DateHelper::format($documentDate, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);

        $invoices = Yii::$app->request->post('Invoice');
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                $idArray[] = $id;
            }
        }

        $invoiceArray = $company->getInvoices()
            ->andWhere(['invoice.id' => $idArray])
            ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
            ->all();

        $createdIF = 0;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->canAddInvoiceFacture) {
                if ($invoice->createInvoiceFacture($documentDate))
                    $createdIF++;
            }
        }

        if ($createdIF > 0) {
            Yii::$app->session->setFlash('success', ($createdIF == 1) ? 'СФ создана' : 'СФ созданы');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать СФ');
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }

    /**
     * Deletes an existing InvoiceFacture model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($type, $id)
    {
        /** @var InvoiceFacture $model */
        $model = $this->loadModel($id, $type);
        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
        Yii::$app->session->setFlash('success', 'СФ удалена');

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $invoiceFactures = Yii::$app->request->post('InvoiceFacture');
        foreach ($invoiceFactures as $id => $invoiceFacture) {
            if ($invoiceFacture['checked']) {
                $model = $this->loadModel($id, $type);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $period = StatisticPeriod::getSessionPeriod();
        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $invoiceFactures = $searchModelOut->search(Yii::$app->request->queryParams, $period, true);

        InvoiceFacture::generateXlsTable($period, $invoiceFactures);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType    = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_invoice_facture' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'Счет-фактуру',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_INVOICE_FACTURE,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionAddStamp($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        return $model->updateAttributes(['add_stamp' => (boolean)Yii::$app->request->post('add_stamp')]);
    }

    /**
     * @throws mixed
     */
    public function actionAutoinvoicefacture()
    {
        $company = Yii::$app->user->identity->company;
        $model = $company->autoinvoicefacture ? : new Autoinvoicefacture([
            'company_id' => $company->id,
            'rule' => Autoinvoicefacture::MANUAL,
            'send_auto' => false,
        ]);

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if (Yii::$app->request->post('ajax') == 'autoinvoicefacture-form') {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            } elseif ($model->save()) {
                Yii::$app->session->setFlash('success', 'Правила выставления СФ сохранены.');

                return $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => Documents::IO_TYPE_OUT]);
            }
        }

        return $this->renderPartial('autoinvoicefacture', ['model' => $model]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $ioType = Yii::$app->request->get('type', Documents::IO_TYPE_OUT);
        $period = StatisticPeriod::getSessionPeriod();
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $ioType);
        $invoiceFactures = $searchModel->search(Yii::$app->request->queryParams, $period, true, true);

        InvoiceFacture::generateXlsTable($period, $invoiceFactures);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateStatus($type, $status)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('InvoiceFacture', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model InvoiceFacture */
                    $model = $this->loadModel($id, $type);

                    if ($status !== null && $status != $model->status_out_id) {
                        $model->status_out_id = $status;

                        LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (InvoiceFacture $model) {
                            return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                        });
                    }

                    switch ($status) {
                        case InvoiceFactureStatus::STATUS_DELIVERED:
                            Yii::$app->session->setFlash('success', 'СФ переданы');
                            break;
                    }
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateOriginal($type, $val)
    {
        if ($type != Documents::IO_TYPE_IN) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('InvoiceFacture', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model InvoiceFacture */
                    $model = $this->loadModel($id, $type);
                    $model->is_original = intval($val) ? true : false;
                    $model->save(false, ['is_original', 'is_original_updated_at']);
                    Yii::$app->session->setFlash('success', 'СФ сохранены');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type file type
     * @param $uid document uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getByUid($uid);

        switch ($type) {
            case 'pdf':
            default:
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean) $model->company->pdf_signed,
                ], 'A4-L');
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Act
     * @throws NotFoundHttpException
     */
    public static function getByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = empty($uid) ? null : InvoiceFacture::find()
            ->joinWith('invoices.company')
            ->andWhere([
                'invoice_facture.uid' => $uid,
                'invoice_facture.type' => $type,
                'invoice.is_deleted' => false,
                'company.blocked' => false,
            ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }
}
