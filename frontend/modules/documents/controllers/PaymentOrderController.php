<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Invoice;
use common\models\document\OperationType;
use common\models\document\PaymentOrder;
use common\models\document\PaymentOrderInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PaymentOrderStatus;
use common\models\document\TaxpayersStatus;
use Faker\Provider\nl_NL\Payment;
use frontend\components\filters\CompleteRegistrationFilter;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\models\PaymentOrderSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\Html;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PaymentOrderController implements the CRUD actions for PaymentOrder model.
 */
class PaymentOrderController extends DocumentBaseController
{
    /**
     * @var
     */
    public $typeDocument = Documents::DOCUMENT_PAYMENT_ORDER;

    /**
     * Whether to allow file upload for document.
     * @var bool
     */
    protected $allowFileUpload = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'get-invoices', 'get-invoice', 'get-contractor-data',
                              'add-modal-details-address', 'validate-company-modal',
                              'add-modal-contractor'],
                        'allow' => true,
                        'roles' => [permissions\document\PaymentOrder::INDEX],
                    ],
                    [
                        'actions' => ['create', 'copy', 'many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\document\PaymentOrder::CREATE)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => ['view', 'document-print', 'import', 'thumbnail'],
                        'allow' => true,
                        'roles' => [permissions\document\PaymentOrder::VIEW],
                    ],
                    [
                        'actions' => ['update', 'paid'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\document\PaymentOrder::UPDATE)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\document\PaymentOrder::DELETE)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'completeRegistration' => CompleteRegistrationFilter::className(),
        ];
    }

    /**
     * Lists all PaymentOrder models.
     * @param int $type
     * @param bool|false $strict
     * @return string
     */
    public function actionIndex($type = 0, $strict = false)
    {
        $searchModel = new PaymentOrderSearch();
        $dateRange = StatisticPeriod::getSessionPeriod();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        if ($strict !== false) {
            Yii::$app->session->setFlash('error',
                'Режим ограниченной функциональности. Вам необходимо заполнить данные в разделе ' . Html::a('«Профиль компании»', Url::to([
                    '/company/update',
                    'backUrl' => $strict,
                ])) . '.'
            );
        }

        if (Yii::$app->request->get('xls')) {
            if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
                Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
                return $this->redirect(['/subscribe/default/index']);
            }

            Yii::$app->response->format = Response::FORMAT_RAW;
            $filename = 'Платёжные поручения';

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
            header('Cache-Control: max-age=0');
            $writer = $searchModel->getExcelWriter($dataProvider);
            $writer->save('php://output');
            exit;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PaymentOrder model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * Displays a single PaymentOrder model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionImport()
    {
        $company = Yii::$app->user->identity->company;
        $modelArray = PaymentOrder::findAll([
            'id' => Yii::$app->request->get('id'),
            'company_id' => $company->id,
        ]);
        if ($modelArray) {
            /* @var $model PaymentOrder */
            foreach ($modelArray as $model) {
                $model->updateStatus(PaymentOrderStatus::STATUS_IMPORT_IN_BANK);
            }

            $bankCity = BikDictionary::find()
                ->select(['bik', 'city'])
                ->where(['bik' => [$model->company_bik, $model->contractor_bik]])
                ->indexBy('bik')
                ->asArray()
                ->all();

            return Yii::$app->response->sendContentAsFile(
                $this->renderPartial('import', [
                    'modelArray' => $modelArray,
                    'company' => $company,
                    'bankCity' => $bankCity
                ]),
                'Платежное_поручение.txt',
                ['mimeType' => 'text/plain']
            );
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionManyCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $createdCount = 0;
        $idArray = [];
        $data = [];
        $models = Yii::$app->request->post('Invoice');
        $error = [];
        unset($models['document_date']);
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $invoiceArray = $company->getInvoices()
            ->andWhere(['invoice.id' => $idArray])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]])
            ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
            ->all();
        /* @var $invoice Invoice */
        foreach ($invoiceArray as $invoice) {
            $data[$invoice->contractor_id][] = $invoice;
        }
        /* @var $contractorInvoices Invoice[] */
        foreach ($data as $contractorID => $contractorInvoices) {
            /* @var $contractor Contractor */
            $contractor = Contractor::findOne($contractorID);
            if ($contractor->ITN && $contractor->BIC) {
                $invoice = reset($contractorInvoices);
                $fullName = count($contractorInvoices) < 4;
                $sum = 0;
                $nds = 0;
                $purposeOfPayment = 'Оплата по счету';
                if (!$fullName) {
                    $purposeOfPayment .= ' №';
                }
                foreach ($contractorInvoices as $contractorInvoice) {
                    $nds += ($contractorInvoice->hasNds ? $contractorInvoice->total_amount_nds : 0);
                    $sum += $contractorInvoice->remaining_amount;
                    if ($fullName) {
                        $purposeOfPayment .= (' №' . $contractorInvoice->fullNumber . ' от ' .
                            DateHelper::format($contractorInvoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . ',');
                    } else {
                        $purposeOfPayment .= ' ' . $contractorInvoice->fullNumber . ',';
                    }

                }
                $purposeOfPayment .= (" за {$contractorInvoices[0]->orders[0]->product_title}.");
                $purposeOfPayment .= ($nds > 0 ? ' В том числе НДС ' . TextHelper::invoiceMoneyFormat($nds, 2) . ' руб.' : ' Без налога НДС');
                $paymentOrder = new PaymentOrder();
                $paymentOrder->company_id = $company->id;
                $paymentOrder->company_name = $company->companyType->name_short . ' ' . $company->name_full;
                $paymentOrder->company_inn = $company->inn;
                $paymentOrder->company_kpp = $company->kpp;
                $paymentOrder->payment_order_status_id = PaymentOrderStatus::STATUS_CREATED;
                $paymentOrder->document_author_id = Yii::$app->user->identity->id;
                $paymentOrder->document_date = date(DateHelper::FORMAT_DATE);
                $paymentOrder->document_number = PaymentOrder::getNextDocumentNumber($company, $paymentOrder->document_date);
                $paymentOrder->relation_with_in_invoice = true;
                $paymentOrder->company_bik = $invoice->company_bik;
                $paymentOrder->company_bank_name = $invoice->company_bank_name;
                $paymentOrder->company_bank_city = $invoice->company_bank_city;
                $paymentOrder->company_ks = $invoice->company_ks;
                $paymentOrder->company_rs = $invoice->company_rs;
                $paymentOrder->contractor_id = $contractor->id;
                $paymentOrder->contractor_name = $contractor->getTitle(true);
                $paymentOrder->contractor_inn = $contractor->ITN;
                $paymentOrder->contractor_bank_name = $contractor->bank_name;
                $paymentOrder->contractor_bank_city = $contractor->bank_city;
                $paymentOrder->contractor_current_account = $contractor->current_account;
                $paymentOrder->contractor_corresponding_account = $contractor->corresp_account;
                $paymentOrder->contractor_bik = $contractor->BIC;
                $paymentOrder->contractor_kpp = $contractor->PPC;
                $paymentOrder->sum = $sum / 100;
                $paymentOrder->ranking_of_payment = 5;
                $paymentOrder->operation_type_id = OperationType::TYPE_PAYMENT_ORDER;
                $paymentOrder->purpose_of_payment = $purposeOfPayment;
                $paymentOrder->taxpayers_status_id = TaxpayersStatus::NO_VALUE;

                $save = Yii::$app->db->transaction(function ($db) use ($paymentOrder, $contractorInvoices, $contractor, &$error) {
                    if ($paymentOrder->save()) {
                        foreach ($contractorInvoices as $invoice) {
                            $paymentOrder->link('invoices', $invoice);
                        }

                        return true;
                    }

                    foreach ((array)$paymentOrder->getErrors() as $errors_msg)
                        foreach ((array)$errors_msg as $error_msg)
                            $error[] = $error_msg . ' (' . $contractor->getTitle(true) . ')';

                    $db->transaction->rollBack();

                    return false;
                });

                if ($save) {
                    $createdCount++;

                    continue;
                }
            } else {
                $error[] = 'Платежное поручение по ' . $contractor->getTitle(true) .
                    ' не может быть создано. Заполните БИК и расчетный счет поставщика.';
            }
        }

        if ($error) {
            Yii::$app->session->setFlash('error', implode('<br>', $error));
        }
        if ($createdCount > 0) {
            Yii::$app->session->setFlash('success', 'Платежные поручения созданы: ' . $createdCount);

            return $this->redirect(['/documents/payment-order/index']);
        }

        return $this->redirect(Yii::$app->getRequest()->getReferrer() ? : ['/documents/payment-order/index']);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->findModel($id);
        if ($actionType == 'print') {
            $model->updateStatus(PaymentOrderStatus::STATUS_PRINTED);
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $paymentOrdersID = explode(',', $multiple);
        $model = $this->findModel(current($paymentOrdersID));
        /* @var $paymentOrder PaymentOrder */
        foreach (PaymentOrder::find()->where(['id' => $paymentOrdersID])->all() as $paymentOrder) {
            $paymentOrder->updateStatus(PaymentOrderStatus::STATUS_PRINTED);
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf' && $model->company->pdf_signed) ? true : false,
            'multiple' => $paymentOrdersID,
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * Creates a new PaymentOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = Yii::$app->user->identity->company;
        $account = $company->mainCheckingAccountant;
        $model = new PaymentOrder();
        $model->company_id = $company->id;
        $model->company_name = $company->companyType->name_short . ' ' . $company->name_full;
        $model->company_inn = $company->inn;
        $model->company_kpp = $company->kpp;
        if ($account) {
            $model->company_bik = $account->bik;
            $model->company_bank_name = $account->bank_name;
            $model->company_bank_city = $account->bank_city;
            $model->company_ks = $account->ks;
            $model->company_rs = $account->rs;
        }
        $model->document_number = PaymentOrder::getNextDocumentNumber($company);
        $model->document_date = date(DateHelper::FORMAT_DATE);

        // defaults:
        $model->ranking_of_payment = 5;
        $model->payment_order_status_id = PaymentOrderStatus::STATUS_CREATED;
        $model->taxpayers_status_id = $company->getIsLikeIP() ?
                                      TaxpayersStatus::DEFAULT_IP :
                                      TaxpayersStatus::DEFAULT_OOO;

        // reset for budjet payment
        $model->uin_code = '';
        $model->payment_details_id = null;
        $model->payment_type_id = null;
        $model->tax_period_code = '';
        $model->document_number_budget_payment = '';

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->loadInvoices();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => Company::findOne(['id' => $model->company_id]),
        ]);
    }

    /**
     * @return array
     */
    public function actionGetContractorData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $contractorID = Yii::$app->request->post('id');
        if ($contractorID) {
            /* @var $company Company */
            $company = Yii::$app->user->identity->company;
            /* @var $contractor Contractor */
            $contractor = Contractor::find()
                ->byCompany($company->id)
                ->byContractor(Contractor::TYPE_SELLER)
                ->andWhere(['id' => $contractorID])
                ->one();
            if ($contractor) {
                return [
                    'result' => true,
                    'contractor' => [
                        'id' => $contractor->id,
                        'contractor_name' => $contractor->getTitle(true),
                        'contractor_inn' => $contractor->ITN,
                        'contractor_bank_name' => $contractor->bank_name,
                        'contractor_bank_city' => $contractor->bank_city,
                        'contractor_current_account' => $contractor->current_account,
                        'contractor_corresponding_account' => $contractor->corresp_account,
                        'contractor_bik' => $contractor->BIC,
                        'contractor_kpp' => $contractor->PPC,
                    ],
                ];
            }
        }

        return [
            'result' => false,
        ];
    }

    /**
     * Updates an existing PaymentOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->loadInvoices();

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'company' => Company::findOne(['id' => $model->company_id]),
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneModel = $this->findModel($id)->cloneSelf();

        if ($cloneModel->save(false)) {
            return $this->redirect(['view', 'id' => $cloneModel->id,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionPaid()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $count = 0;
        $ids = (array) Yii::$app->request->post('id');
        $date = date_create_from_format('d.m.Y', Yii::$app->request->post('date')) ? : date_create();
        $company = Yii::$app->user->identity->company;

        foreach ($ids as $id) {
            $model = PaymentOrder::findOne([
                'id' => $id,
                'company_id' => $company->id,
            ]);
            if ($model !== null && $model->makePaid($date)) {
                $count++;
            }
        }

        return [
            'done' => $count,
            'of' => count($ids),
        ];
    }

    /**
     * Deletes an existing PaymentOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->_delete();

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $paymentOrders = Yii::$app->request->post('PaymentOrder');
        foreach ($paymentOrders as $id => $paymentOrder) {
            if ($paymentOrder['checked']) {
                $model = $this->loadModel($id);
                $model->_delete();
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index']);
    }

    /**
     * @param integer $id
     * @param integer $w
     * @param integer $h
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionThumbnail($id, $w, $h)
    {
        $model = $this->findModel($id);
        $w = min(600, (int) $w);
        $h = min(600, (int) $h);

        $image = new \Imagick();
        $image->readImageBlob(PaymentOrder::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false));
        $image->setImageFormat("png");
        $image->scaleimage($w, $h);

        Yii::$app->response->sendContentAsFile($image->getImageBlob(), $model->id . '.png');
    }

    /**
     * Finds the PaymentOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return PaymentOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentOrder::findOne(['id' => $id, 'company_id' => Yii::$app->user->identity->company->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return array|mixed
     */
    public function actionAddModalDetailsAddress()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if ($model->load(Yii::$app->request->post()) && $checkingAccountant->load(Yii::$app->request->post())) {
            $modelValidate = $model->validate();
            $accountValidate = $checkingAccountant->validate();
            if ($modelValidate && $accountValidate) {
                $result = Yii::$app->db->transaction(function (Connection $db) use ($checkingAccountant, $model) {
                    if ($checkingAccountant->_save($model)) {
                        $model->populateRelation('mainCheckingAccountant', $checkingAccountant);
                        $model->chief_is_chief_accountant = true;
                        if ($model->setDirectorInitials() && $model->save()) {

                            return true;
                        }
                    }
                    $db->transaction->rollBack();

                    return false;
                });

                if ($result && $model->strict_mode == Company::OFF_STRICT_MODE) {
                    return [
                        // texts
                        'is_payment_order_page' => 1,
                        'company_name' => $model->getTitle($model->company_type_id == CompanyType::TYPE_IP ? false : true),
                        'company_inn'  => $model->inn,
                        'company_kpp'  => $model->kpp,
                        // inputs
                        'company_bank_name' => $checkingAccountant->bank_name,
                        'company_bank_city' => $checkingAccountant->bank_city,
                        'company_bik' => $checkingAccountant->bik,
                        'company_rs' => $checkingAccountant->rs,
                        'company_ks' => $checkingAccountant->ks
                    ];
                }
            }
        }

        $header = $this->renderAjax('@frontend/modules/documents/views/invoice/partial/create-company_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('@frontend/modules/documents/views/invoice/partial/create-company_body', [
            'checkingAccountant' => $checkingAccountant,
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed
     */
    public function actionValidateCompanyModal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $checkingAccountant->load(Yii::$app->request->post());

            return ActiveForm::validate($model, $checkingAccountant);
        }
    }

    /**
     * @return array
     */
    public function actionAddModalContractor()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = null;
        $contractor_id = Yii::$app->request->post('contractor_id');

        if ($contractor_id) {
            $model = Contractor::findOne([
                'id' => (int)$contractor_id,
                'company_id' => Yii::$app->user->identity->company->id
            ]);
            $contractor['face_type'] = $model->face_type;
        }

        if (!$model) {
            $model = new Contractor();
            $model->status = Contractor::ACTIVE;
            $model->type = Contractor::TYPE_SELLER;
            $model->isNewRecord = true;
            $contractor = Yii::$app->request->getBodyParam('Contractor');
        }

        if (isset($contractor['face_type'])) {
            switch ($contractor['face_type']) {
                case 1:
                    $scenario = Contractor::SCENARIO_FIZ_FACE;
                    break;
                case 0:
                    $scenario = Contractor::SCENARIO_LEGAL_FACE;
                    break;
                case 2:
                    $scenario = Contractor::SCENARIO_FOREIGN_LEGAL_FACE;
            }
        } else {
            $scenario = Contractor::SCENARIO_LEGAL_FACE;
        }

        $model->setScenario($scenario);

        $isPhysical = intval(Yii::$app->request->post('Contractor')['face_type'] ?? 0) === Contractor::TYPE_PHYSICAL_PERSON;

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->post('ajax') !== null) {
                return $this->ajaxValidate($model);
            }
            if ($isPhysical) {
                $model->contact_is_director = 1;
                if (!empty($model->contact_email)) {
                    $model->director_email = $model->contact_email;
                }
            }
            if (empty($model->director_name)) {
                $model->director_name = 'ФИО руководителя';
            }
            $model->taxation_system = Contractor::WITHOUT_NDS;
            $model->chief_accountant_is_director = 1;
            $model->contact_is_director = 1;
            $model->setAttribute('company_id', Yii::$app->user->identity->company->id);
            $model->setAttribute('employee_id', Yii::$app->user->id);
            $model->setAttribute('object_guid', OneCExport::generateGUID());

            if ($model->save()) {
                return $data = [
                    'is_payment_order_page' => 1,
                    'id' => $model->id,
                    // inputs
                    'contractor_name' => ($isPhysical) ? $model->getPhysical_fio() : $model->getShortName(),
                    'contractor_inn' => $model->ITN,
                    'contractor_kpp' => $model->PPC,
                    'contractor_bank_name' => $model->bank_name,
                    'contractor_bank_city' => $model->bank_city,
                    'contractor_bik' => $model->BIC,
                    'contractor_current_account' => $model->current_account,
                    'contractor_corresponding_account' => $model->corresp_account
                ];
            }
        }

        $header = $this->renderAjax('partial_contractor/create-contractor_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('partial_contractor/create-contractor_body', [
            'model' => $model,
            'documentType' => $model->type,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }
}
