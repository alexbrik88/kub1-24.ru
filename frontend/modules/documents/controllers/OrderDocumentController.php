<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 16:59
 */

namespace frontend\modules\documents\controllers;


use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\phpWord\PhpWordOrderDocument;
use common\components\TextHelper;
use common\models\address\Country;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentInToOut;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderHelper;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\components\filters\CompleteRegistrationFilter;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\OrderDocumentHelper;
use frontend\modules\documents\components\OrderDocumentProductHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\forms\OrderDocumentSendForm;
use frontend\modules\documents\models\OrderDocumentSearch;
use frontend\modules\export\models\one_c\OneCExport;
use yii\bootstrap\Html;
use yii\db\Connection;
use yii\filters\VerbFilter;
use Yii;
use frontend\rbac\permissions;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class OrderDocumentController
 * @package frontend\modules\documents\controllers
 */
class OrderDocumentController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_ORDER;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['add-modal-contractor', 'add-modal-product', 'index'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]);
                        },
                    ],
                    [
                        'actions' => ['file-upload', 'file-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->findModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['create', 'copy'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Invoice::CREATE, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['view', 'file-get', 'file-list',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->findModel(),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['document-print', 'docx', 'document-png'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->findModel(),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'model' => $action->controller->findModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['update-status', 'update', 'comment-internal', 'send', 'update-contractor-fields'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                'model' => $action->controller->findModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-change-status'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
                            Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view', 'out-create-invoice'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['many-document-print'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'send-many-in-one', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS)
                            && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE_IN, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['basis-document',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'completeRegistration' => CompleteRegistrationFilter::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    public function beforeAction($action)
    {
        $getParams = \Yii::$app->request->queryParams;
        $class = 'PaymentOrderSearch';
        $param = 'detailingTableBy';

        if (isset($getParams[$class])) {
            if (isset($getParams[$class][$param])) {
                /* @var $user Employee */
                $user = Yii::$app->user->identity;
                $user->config->updateAttributes([
                    'order_document_detailing' => ($getParams[$class][$param]) ? 1 : 0
                ]);

                unset($getParams[$class]);
                return $this->redirect(Url::toRoute([Url::base()] + $getParams));
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionIndex($type = null)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $dashBoardData = OrderDocument::getDashBordData($type);
        $searchModel = new OrderDocumentSearch(['type' => $type]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'dashBoardData' => $dashBoardData,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    /**
     * @param $type
     * @return string|Response
     * @throws \Throwable
     */
    public function actionCreate($type = Documents::IO_TYPE_OUT)
    {
        if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Тип документа не найден');
        }

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $model = new OrderDocument();
        $model->type = $type;
        $model->company_id = $user->company->id;
        $model->author_id = $user->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->status_id = OrderDocumentStatus::STATUS_NEW;
        $model->document_number = OrderDocument::getNextDocumentNumber($company, null, $model->document_date, $model->type);
        // initial nds values
        $model->nds_view_type_id = ($type == Documents::IO_TYPE_IN)
            ? OrderDocument::NDS_VIEW_IN : $company->nds_view_type_id;
        $model->has_nds = ($model->nds_view_type_id == OrderDocument::NDS_VIEW_WITHOUT)
            ? OrderDocument::HAS_NO_NDS : OrderDocument::HAS_NDS;
        $model->populateRelation('company', $company);

        if (Yii::$app->request->get('out')) {
            OrderDocumentHelper::loadOutProducts($model);
        }

        $orderDocumentSaved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

            if (OrderDocumentHelper::load($model, Yii::$app->request->post())) {
                if (OrderDocumentHelper::save($model)) {
                    return true;
                }
            }

            $db->transaction->rollBack();

            return false;
        });

        if ($orderDocumentSaved) {
            Yii::$app->session->setFlash('success', 'Заказ создан.');

            return $this->redirect(['view', 'id' => $model->id, 'type' => $type]);
        }

        $addProduct = Yii::$app->request->post('selection');
        if ($addProduct) {
            $orderArray = [];
            foreach ($addProduct as $pid) {
                if ($product = Product::getById($model->company_id, $pid)) {
                    $order = OrderDocumentProductHelper::createOrderByProduct($product, $model, 1);
                    $orderArray[] = $order;
                }
            }
            $model->populateRelation('orderDocumentProducts', $orderArray);
            $model->beforeValidate();
        }

        if ($model->createFromOut && !empty($model->outProducts) && empty($model->orderDocumentProducts)) {
            $orderArray = [];
            foreach ($model->outProducts as $outProductId => $outPos) {
                if ($product = Product::findOne(['id' => $outProductId, 'company_id' => $user->company->id])) {
                    $order = OrderDocumentProductHelper::createOrderByProduct($product, $model, array_sum($outPos['quantity']));
                    $orderArray[] = $order;
                }
            }
            $model->populateRelation('orderDocumentProducts', $orderArray);
            $model->beforeValidate();
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'user' => $user,
            'ioType' => $type,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $model = $this->findModel($id);
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $previousStatus = $model->status_id;

        if ($model->hasOutOrderDocuments()) {
            OrderDocumentHelper::loadOutProducts($model);
        }

        if (OrderDocumentHelper::load($model, Yii::$app->request->post()) && OrderDocumentHelper::save($model)) {
            Yii::$app->session->setFlash('success', 'Заказ изменен');

            return $this->redirect(['view',
                'id' => $id
            ]);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'user' => $user,
            'ioType' => $model->type,
        ]);
    }

    /**
     * @param $id
     * @param int $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $type = 2)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return $this->render('view', [
            'model' => $this->findModel($id),
            'user' => $user,
            'type' => $type,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($id)
    {
        $model = $this->findModel($id);
        $statusID = Yii::$app->request->post('status_id');
        $model->status_id = $statusID;
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $previousStatus = $model->status_id;
            if ($model->save()) {
                $model->checkStatus($previousStatus);

                return [
                    'result' => true,
                    'status' => $model->status,
                    'date' => date(DateHelper::FORMAT_USER_DATE, $model->status_updated_at),
                ];
            }

            return ['result' => false];
        }

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Заказ отменён');
        }

        return $this->redirect(['view', 'id' => $model->id,]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneModel = $this->cloneOrderDocument($id);
        $cloneModel->status_id = OrderDocumentStatus::STATUS_NEW;
        $cloneModel->status_author_id = $cloneModel->document_author_id = Yii::$app->user->identity->id;
        if (OrderDocumentHelper::save($cloneModel, false)) {
            Yii::$app->session->setFlash('success', 'Заказ скопирован');

            return $this->redirect(['view', 'id' => $cloneModel->id,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @param $id
     * @return OrderDocument
     * @throws NotFoundHttpException
     */
    public function cloneOrderDocument($id)
    {
        $model = $this->findModel($id);

        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->document_number = $model->getNextDocumentNumber(Yii::$app->user->identity->company, null, $clone->document_date);
        $cloneOrders = array_map([$this, 'cloneOrderDocumentProducts'], $model->orderDocumentProducts);
        $clone->populateRelation('orderDocumentProducts', $cloneOrders);

        return $clone;
    }

    /**
     * @param OrderDocumentProduct $order
     * @return OrderDocumentProduct
     */
    public function cloneOrderDocumentProducts(OrderDocumentProduct $order)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->order_document_id = null;

        return $cloneOrder;
    }

    /**
     * @param $id
     * @param $type
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function actionDocx($id, $type)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Скачать документ в Word можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $model = $this->findModel($id);
        $word = new PhpWordOrderDocument($model);

        return $word->getFile();
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($id)
    {
        /* @var OrderDocument $model */
        $model = $this->findModel($id);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = OrderDocument::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'id' => $model->id,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function old_actionSend($id)
    {
        $model = $this->findModel($id);
        $sendForm = new OrderDocumentSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
            'textRequired' => true,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(['view', 'id' => $model->id,]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        Yii::$app->db->transaction(function (Connection $db) use ($model) {
            $model->is_deleted = true;
            if ($model->save(false, ['is_deleted'])) {
                $model->deleteRelated();
                return true;
            }
            Yii::$app->session->setFlash('error', "Ошибка при удалении заказа.");
            $db->transaction->rollBack();

            return false;
        });
        Yii::$app->session->setFlash('success', 'Заказ удален');

        return $this->redirect(['index']);
    }

    /**
     * @return array|mixed
     */
    public function actionAddModalProduct()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $product = Yii::$app->request->getBodyParam('Product');
        $productionType = $product['production_type'];
        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;
        if (isset($product['group_id'])) {
            $model->group_id = $product['group_id'];
        }
        $scenarioProd = 'prod';
        $scenarioUslug = $productionType == 1 ? 'tovar' : 'uslug';
        $scenario = $scenarioProd . $scenarioUslug;
        $model->setScenario($scenario);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if ($company->companyTaxationType->osno) {
            $model->price_for_sell_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
        } else {
            $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }

        if (!isset($product['flag'])) {
            if (Yii::$app->request->post('ajax') !== null) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->_prepareFromInvoice() && $model->save()) {
                return [
                    'items' => $model->getProductsByIds([$model->id], $company->id),
                    'services' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                    ])->count(),
                    'goods' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_GOODS,
                    ])->count(),
                ];
            }
            $attribute = 'price_for_sell_with_nds';
            if ($model->getAttribute($attribute) == 0) {
                $model->addError($attribute, 'Необходимо заполнить «' . $model->getAttributeLabel($attribute) . '».');
            }
        }
        $header = $this->renderPartial('partial/create-product_header');
        $body = $this->renderAjax('partial/create-product_body', [
            'model' => $model,
            'documentType' => Documents::IO_TYPE_OUT,
            'company' => $company,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $uid
     * @param $email
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid, $email = null)
    {
        $this->layout = 'out-view-order-document';

        /* @var OrderDocument $model */
        $model = OrderDocument::find()
            ->andWhere(['and',
                ['is_deleted' => false],
                ['uid' => $uid],
            ])
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        if ($model->status_id == OrderDocumentStatus::STATUS_SEND) {
            $model->status_updated_at = time();
            $model->save(false, ['status_id', 'status_updated_at',]);
        }

        return $this->render('out-view', [
            'orderDocument' => $model,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
        ]);

    }

    public function actionOutCreateInvoice($uid) {

        $orderDocument = OrderDocument::findOne(['uid' => $uid]);

        if ($orderDocument->invoice_id) {
            $invoice = Invoice::findOne($orderDocument->invoice_id);
            return $this->redirect(["/documents/invoice/out-view", 'uid' => $invoice->uid]);
        }

        if ($orderDocument) {
            $company = $orderDocument->company;
            /* @var Invoice $invoice */
            $invoice = new Invoice;
            $invoice->scenario = 'insert';
            $invoice->type = Documents::IO_TYPE_OUT;
            $invoice->document_date = date(DateHelper::FORMAT_DATE);
            $invoice->contractor_id = $orderDocument->contractor_id;
            $invoice->comment_internal = $orderDocument->comment_internal;
            $invoice->isAutoinvoice = 0;
            $invoice->company_id = $company->id;
            $invoice->populateRelation('company', $company);
            $invoice->populateRelation('orderDocument', $orderDocument);
            $invoice->document_number = (string)Invoice::getNextDocumentNumber($company->id, Documents::IO_TYPE_OUT, null, $invoice->document_date);
            $contractor = $orderDocument->contractor;
            $payment_delay = $contractor->type == Contractor::TYPE_SELLER ? $contractor->seller_payment_delay : $contractor->customer_payment_delay;
            $invoice->agreement = $orderDocument->agreement;
            $invoice->populateRelation('contractor', $orderDocument->contractor);
            $invoice->nds_view_type_id = $company->hasNds() ?
                ($company->isNdsExclude ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) :
                Invoice::NDS_VIEW_WITHOUT;
            $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
            $invoiceSaved = Yii::$app->db->transaction(function (Connection $db) use ($invoice, $orderDocument, $company) {
                if ($this->action->id === 'first-create') {
                    $company->save(true, ['nds']);
                }
                foreach ($orderDocument->orderDocumentProducts as $orderDocumentProduct) {
                    $product = Product::getById($invoice->company_id, $orderDocumentProduct->product_id);
                    if ($product !== null) {
                        $order = OrderHelper::createOrderByProduct(
                            $product,
                            $invoice,
                            $orderDocumentProduct->quantity,
                            $orderDocumentProduct->selling_price_with_vat,
                            ($invoice->has_discount ? $orderDocumentProduct->discount : 0)
                        );
                        $order->product_title = $orderDocumentProduct->product_title;
                        $orderArray[] = $order;
                    }
                }
                array_walk($orderArray, function ($order, $key) {
                    $order->number = $key + 1;
                });
                $invoice->populateRelation('orders', $orderArray);

                $invoice->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
                $invoice->total_place_count = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));
                if (!InvoiceHelper::save($invoice)) {
                    $db->transaction->rollBack();

                    return false;
                }
                $orderDocument->invoice_id = $invoice->id;
                $orderDocument->save(true, ['invoice_id']);

                return true;
            });

            if ($invoiceSaved) {

                return $this->redirect(["/documents/invoice/out-view", 'uid' => $invoice->uid]);
            }
        }

        Yii::$app->session->setFlash('error', 'Ошибка при создании счета.');

        return $this->redirect(["/documents/order-document/out-view", 'uid' => $uid]);
    }

    /**
     * Get an invoice file by uid for unauthorized users
     * @param $type file type
     * @param $uid invoice uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var OrderDocument $model */
        $model = OrderDocument::find()
            ->andWhere(['and',
                ['is_deleted' => false],
                ['uid' => $uid],
            ])
            ->one();

        if ($model === null) {
            throw new NotFoundHttpException();
        }

        switch ($type) {
            case 'pdf':
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'isOutView' => true,
                    'addStamp' => (boolean)$model->company->pdf_signed,
                ]);
                break;

            default:
                $doc = new PhpWordOrderDocument($model);
                return $doc->getFile();
                break;
        }
    }

    /**
     * @param $type
     * @return string[]
     * @throws \Throwable
     */
    public function actionManyCreate($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;

        $jsonData = Yii::$app->request->post('data', '{}');
        $data = json_decode($jsonData, true);

        foreach ($data as $post) {
            $model = new OrderDocument();
            $model->type = $type;
            $model->company_id = $user->company->id;
            $model->author_id = $user->id;
            $model->document_date = date(DateHelper::FORMAT_DATE);
            $model->has_nds = $company->companyTaxationType->osno ?
                OrderDocument::HAS_NDS :
                OrderDocument::HAS_NO_NDS;
            $model->status_id = OrderDocumentStatus::STATUS_NEW;
            $model->document_number = (string)OrderDocument::getNextDocumentNumber($company, null, $model->document_date, $model->type);
            $model->populateRelation('company', $company);

            $orderDocumentSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $post) {
                if (OrderDocumentHelper::load($model, $post)) {
                    if (OrderDocumentHelper::save($model)) {
                        return true;
                    }
                }

                $db->transaction->rollBack();

                return false;
            });

            if (!$orderDocumentSaved) {
                return ['success' => 'false'];
            }
        }

        return ['success' => 'true'];
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $orderDocumentsID = explode(',', $multiple);
        $model = $this->findModel(current($orderDocumentsID));

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf' && $model->company->pdf_signed) ? true : false,
            'multiple' => $orderDocumentsID,
        ]);
    }

    /**
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionManySend()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var EmployeeCompany $sender */
        $sender = Yii::$app->user->identity->currentEmployeeCompany;
        $contractors = null;
        $orderDocuments = Yii::$app->request->post('OrderDocument');
        $send = 0;
        foreach ($orderDocuments as $id => $orderDocument) {
            if ($orderDocument['checked']) {
                $model = $this->findModel($id);
                if (($sendTo = $model->contractor->someEmail) && $model->sendAsEmail($sender, $sendTo)) {
                    $send += 1;
                } else {
                    $contractors[$model->contractor->id] = Html::a($model->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->contractor->type, 'id' => $model->contractor->id
                    ]));
                }
            }
        }
        $message = 'Отправлено ' . $send . ' из ' . count($orderDocuments) . ' писем.';
        if ($send == count($orderDocuments)) {
            Yii::$app->session->setFlash('success', $message);
        }

        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки заказов, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ?: ['index']);
    }

    /**
     * @param $statusID
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManyUpdateStatus($statusID)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderDocuments = Yii::$app->request->post('OrderDocument', []);
        $noUpdatedOrderDocuments = [];
        $updatedOrderDocuments = [];
        foreach ((array)$orderDocuments as $id => $orderDocument) {
            if ($orderDocument['checked']) {
                $model = $this->findModel($id);
                $model->status_id = $statusID;
                if ($model->save()) {
                    $updatedOrderDocuments[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                } else {
                    $noUpdatedOrderDocuments[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                }
            }
        }
        if (count($noUpdatedOrderDocuments) > 0) {
            Yii::$app->session->setFlash('error', 'Статусы заказов не может быть изменен:<br>' . implode('<br>', $noUpdatedOrderDocuments));
        }
        if (count($updatedOrderDocuments) > 0) {
            Yii::$app->session->setFlash('success', 'Статусы заказов успешно изменены:<br>' . implode('<br>', $updatedOrderDocuments));
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderDocuments = Yii::$app->request->post('OrderDocument');
        $noDeletedOrderDocuments = [];
        $deletedOrderDocuments = [];
        foreach ($orderDocuments as $id => $orderDocument) {
            if ($orderDocument['checked']) {
                $model = $this->findModel($id);
                $deleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {
                    $model->is_deleted = true;
                    if ($model->save(false, ['is_deleted'])) {
                        $model->deleteRelated();
                        return true;
                    }
                    return false;
                });
                if ($deleted) {
                    $deletedOrderDocuments[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                } else {
                    $noDeletedOrderDocuments[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                }
            }
        }

        if (count($noDeletedOrderDocuments) > 0) {
            Yii::$app->session->setFlash('error', 'Заказы не могут быть удалены:<br>' . implode('<br>', $noDeletedOrderDocuments));
        }
        if (count($deletedOrderDocuments) > 0) {
            Yii::$app->session->setFlash('success', 'Заказы успешно удалены:<br>' . implode('<br>', $deletedOrderDocuments));
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBasisDocument()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $contractor = Contractor::findOne(['id' => Yii::$app->request->get('contractorId'), 'company_id' => $company->id]);
        $delay = 10;
        $refresh = false;

        $model = new OrderDocument();
        $model->scenario = 'insert';
        $model->company_id = $company->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        if ($contractor) {
            $delay = $contractor->type == Contractor::TYPE_SELLER ? $contractor->seller_payment_delay : $contractor->customer_payment_delay;
            $refresh = $company->getOrderDocuments()->andWhere([
                'contractor_id' => $contractor->id,
                'is_deleted' => false,
            ])->exists();
            $model->contractor_id = $contractor->id;
            $model->agreement = $contractor->last_basis_document;
            $model->populateRelation('contractor', $contractor);
        }

        return $this->renderPartial('form/basis_document', [
            'model' => $model,
            'delay' => $delay,
            'refresh' => $refresh,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateContractorFields($id)
    {
        /* @var $model OrderDocument */
        $model = $this->findModel($id);
        if ($model->contractor) {
            $model->setContractor($model->contractor);
        }
        $model->save(true, ['contractor_name_short', 'contractor_name_full', 'contractor_director_name',
            'contractor_director_post_name', 'contractor_address_legal_full', 'contractor_bank_name', 'contractor_bank_city',
            'contractor_bik', 'contractor_inn', 'contractor_kpp', 'contractor_ks', 'contractor_rs',]);

        Yii::$app->session->setFlash('success', 'Информация по покупателю обновлена');

        return $this->redirect(['update', 'id' => $id, 'type' => $model->type]);
    }

    /**
     * @param $id
     * @return OrderDocument
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->request->get('id');
        }
        $model = OrderDocument::findOne($id);
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}