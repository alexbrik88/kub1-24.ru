<?php
namespace frontend\modules\documents\controllers;

use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderProxy;
use common\models\document\Proxy;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use common\components\date\DateHelper;
use Yii;
use yii\web\Response;

/**
 * Class ProxyAjaxController
 */
class ProxyAjaxController extends FrontendController
{
    /**
     * @return array
     */
    public function actionGetProductList()
    {
        $result = [];
        if (\Yii::$app->request->isAjax) {
            $invoice_id = \Yii::$app->request->post('invoice_id');
            $invoice = Invoice::findOne(['id'=>$invoice_id]);
            $orders = $invoice->orders;

            foreach ($orders as $order) {
                if (OrderProxy::compareWithOrderByQuantity($order) != null) {
                    $result[] = $order;
                }
            }
            return $result;
        }
        return $result;
    }


    /**
     * @return bool
     */
    public function actionSubsitution()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $order_id = \Yii::$app->request->post('order_id');
        $proxy_id = \Yii::$app->request->post('proxy_id');
        $order = Order::findOne(['id'=>$order_id]);
        if (OrderProxy::findOne(['proxy_id'=>$proxy_id,'order_id'=>$order_id]) != null) {
            $opl = OrderProxy::findOne(['proxy_id'=>$proxy_id,'order_id'=>$order_id]);
        } else {
            $opl = OrderProxy::createFromOrder($proxy_id, $order);
        }

        return [
            'hasAvailable' => !empty(OrderProxy::getAvailable($proxy_id)),
            'content' => $this->renderAjax('/proxy/_viewPartials/_template_'.Documents::$ioTypeToUrl[$order->invoice->type], [
                'key' => count(\common\models\document\OrderProxy::findAll(['proxy_id' => $proxy_id])),
                'proxy_id'=>$proxy_id,
                'order'=> $opl,
                'plus'=>0,
                'hasNds'=>$order->invoice->hasNds,
                'precision' => $order->invoice->price_precision,
            ]),
        ];
    }
    /**
     * @param $key
     * @return string
     */
    public function actionAddNewRow()
    {
        $key = \Yii::$app->request->post('key');
        $invoice_id = \Yii::$app->request->post('invoice_id');
        $proxy_id = \Yii::$app->request->post('proxy_id');
        $active = \Yii::$app->request->post('active');
        $invoice = Invoice::findOne(['id'=>$invoice_id]);
        $orders = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS &&
                    $order->quantity > OrderProxy::find()
                        ->where(['order_id'=>$order->id])
                        ->andWhere(['!=','proxy_id',$proxy_id])
                        ->sum('quantity')
                ) {
                    $orders[] = $order;
                }
            }
        }
        $result = [];
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                    $result[$order->id] = $order->product_title;
            }
        }

        return  $this->renderAjax('/proxy/_viewPartials/_template_'.Documents::$ioTypeToUrl[$invoice->type], [
            'key' => $key,
            'result'=>$result,
            'proxy_id'=>$proxy_id,
            'plus'=>0,
            'invoice'=>$invoice,
            'precision' => $invoice->price_precision,
        ]);
    }
    /**
     * @return bool
     */
    public function actionDeleteRow()
    {
        $key = \Yii::$app->request->post('key');
        $result = [];
        $proxy_id = \Yii::$app->request->post('proxy_id');
        $orders = Proxy::findOne(['id'=>$proxy_id])->invoice->orders;
        $active = \Yii::$app->request->post('active');
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                $result[$order->id] = $order->product_title;
            }
        }
        if ($result != null) {
            return $plus = 1;
        } else {
            return $plus = 0;
        }
    }

    /**
     * @return bool
     */
    public function actionClose($proxy_id, $ioType)
    {
        $model = Proxy::findOne(['id'=>$proxy_id]);

        return $this->renderPartial('/proxy/_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $model->invoice->price_precision,
        ]);
    }

    /**
     * @return bool
     */
    public function actionEdit($proxy_id, $ioType)
    {
        $orders = OrderProxy::getAvailable($proxy_id);
        if (OrderProxy::getAvailable($proxy_id) == null) {
            return $is_editable = 0;
        } else {
            return $is_editable = 1;
        }
    }

}
