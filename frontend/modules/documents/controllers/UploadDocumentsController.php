<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 30.05.2017
 * Time: 9:47
 */

namespace frontend\modules\documents\controllers;


use common\components\filters\AccessControl;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\UploadedDocumentOrder;
use common\models\document\UploadedDocuments;
use frontend\components\FrontendController;
use frontend\models\Documents;
use frontend\rbac\permissions;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use Zend\Validator\File\Upload;

/**
 * Class UploadDocumentsController
 * @package frontend\modules\documents\controllers
 */
class UploadDocumentsController extends FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [permissions\document\Document::CREATE],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param null $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($id = null)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $uploadedDocumentOrder = new UploadedDocumentOrder();
        /* @var $currentUploadedDocument UploadedDocuments */
        if ($id) {
            $currentUploadedDocument = $this->findModel($id);
        } else {
            $currentUploadedDocument = $company->getUploadedDocuments()->one();
        }
        if ($currentUploadedDocument) {
            $existsContractor = Contractor::find()->andWhere(['and',
                ['ITN' => $currentUploadedDocument->inn],
                ['company_id' => $currentUploadedDocument->company_id],
                ['is_deleted' => false],
                ['type' => Contractor::TYPE_SELLER],
            ])->andFilterWhere(['PPC' => $currentUploadedDocument->kpp])->exists();
        } else {
            $existsContractor = false;
        }

        return $this->render('index', [
            'company' => $company,
            'uploadedDocumentOrder' => $uploadedDocumentOrder,
            'currentUploadedDocument' => $currentUploadedDocument,
            'existsContractor' => $existsContractor,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpload($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Company::findOne($id);
        if ($company === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if ($_FILES) {
            UploadedDocuments::uploadFiles();
        }

        return true;
    }

    /**
     * @param $id
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionSaveDocument($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $errors = [];
        $uploadedDocument = $this->findModel($id);
        if ($uploadedDocument->load(Yii::$app->request->post())) {
            $uploadedDocumentOrderData = Yii::$app->request->post('UploadedDocumentOrder');
            $uploadedDocument->total_amount_no_nds = intval(str_replace([',', ' '], '', $uploadedDocument->total_amount_no_nds));
            $uploadedDocument->total_amount_with_nds = intval(str_replace([',', ' '], '', $uploadedDocument->total_amount_with_nds));
            $uploadedDocument->total_amount_nds = (int) $uploadedDocument->nds_view_type_id !== Invoice::NDS_VIEW_WITHOUT ?
                intval(str_replace([',', ' '], '', $uploadedDocument->total_amount_nds)) : 0;
            if ($uploadedDocument->validate()) {
                if ($uploadedDocumentOrderData !== null) {
                    $errors = $this->validateUploadDocumentOrder($uploadedDocument, $uploadedDocumentOrderData);
                } else {
                    return false;
                }

                if (empty($errors) && $uploadedDocument->_save($uploadedDocumentOrderData)) {
                    $invoiceID = $uploadedDocument->createInvoice();
                    if ($invoiceID) {
                        return $this->redirect(['/documents/invoice/view', 'type' => Documents::IO_TYPE_IN, 'id' => $invoiceID]);
                    }
                    Yii::$app->session->setFlash('error', 'Ошибка при сохранении документа!');

                    return $this->redirect(['index', 'id' => $uploadedDocument->id, 'activeTab' => UploadedDocuments::TAB_REQUISITES]);
                } else {
                    $errors['result'] = false;

                    return $errors;
                }
            } else {
                if ($uploadedDocumentOrderData !== null) {
                    $errors = $this->validateUploadDocumentOrder($uploadedDocument, $uploadedDocumentOrderData);
                }
                $errors['result'] = false;
                $errors['uploadedDocument'] = $uploadedDocument->getErrors();

                return $errors;
            }
        }

        return false;
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionManySaveDocuments()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->post('UploadedDocuments');
        foreach ($data as $uploadedDocumentID => $isChecked) {
            if ($isChecked) {
                $uploadedDocument = $this->findModel($uploadedDocumentID);
                $uploadedDocument->createInvoice();
            }
        }

        return $this->redirect(['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionSaveUploadDocumentsRequisites($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $uploadedDocument = $this->findModel($id);
        $uploadedDocument->load(Yii::$app->request->post());
        if ($uploadedDocument->validate(['bank_name', 'bik', 'ks', 'inn', 'kpp', 'rs', 'contractor_type_id', 'contractor_name'])) {
            if ($uploadedDocument->save(true, ['bank_name', 'bik', 'ks', 'inn', 'kpp', 'rs', 'contractor_type_id', 'contractor_name']) &&
                $uploadedDocument->checkOnComplete()
            ) {
                return $this->redirect(['index', 'id' => $id]);
            }
        }
        $errors['result'] = false;
        $errors['uploadedDocument'] = $uploadedDocument->getErrors();

        return $errors;
    }

    /**
     * @param $id
     * @return array|bool|Response
     * @throws NotFoundHttpException
     */
    public function actionSaveUploadedDocumentsOrder($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $uploadedDocument = $this->findModel($id);
        $uploadedDocument->load(Yii::$app->request->post());
        $uploadedDocument->total_amount_no_nds = $uploadedDocument->convertAmount($uploadedDocument->total_amount_no_nds);
        $uploadedDocument->total_amount_with_nds = $uploadedDocument->convertAmount($uploadedDocument->total_amount_with_nds);
        $uploadedDocument->total_amount_nds = (int) $uploadedDocument->nds_view_type_id !== Invoice::NDS_VIEW_WITHOUT ?
            $uploadedDocument->convertAmount($uploadedDocument->total_amount_nds) : 0;
        $uploadedDocumentOrderData = Yii::$app->request->post('UploadedDocumentOrder');
        if ($uploadedDocument->validate(['nds_view_type_id', 'total_amount_no_nds', 'total_amount_with_nds', 'total_amount_nds'])) {
            if ($uploadedDocumentOrderData !== null) {
                $errors = $this->validateUploadDocumentOrder($uploadedDocument, $uploadedDocumentOrderData);
            } else {
                return false;
            }
        } else {
            if ($uploadedDocumentOrderData !== null) {
                $errors = $this->validateUploadDocumentOrder($uploadedDocument, $uploadedDocumentOrderData);
            }
            $errors['uploadedDocument'] = $uploadedDocument->getErrors();
        }
        if (empty($errors)) {
            if ($uploadedDocument->save(true, ['nds_view_type_id', 'total_amount_no_nds', 'total_amount_with_nds', 'total_amount_nds']) &&
                $uploadedDocument->updateOrders($uploadedDocumentOrderData) &&
                $uploadedDocument->checkOnComplete()
            ) {
                return $this->redirect(['index', 'id' => $id]);
            }

            return false;
        } else {
            $errors['result'] = false;

            return $errors;
        }
    }

    /**
     * @param UploadedDocuments $uploadedDocuments
     * @param $uploadedDocumentOrderData
     * @param array $errors
     * @return array
     * @throws NotFoundHttpException
     */
    public function validateUploadDocumentOrder(UploadedDocuments $uploadedDocuments, $uploadedDocumentOrderData, $errors = [])
    {
        if (isset($uploadedDocumentOrderData['new'])) {
            $newUploadedDocumentOrderData = $uploadedDocumentOrderData['new'];
            foreach ($newUploadedDocumentOrderData as $key => $oneUploadedDocumentData) {
                $newUploadedDocumentOrder = new UploadedDocumentOrder();
                $newUploadedDocumentOrder->name = $oneUploadedDocumentData['name'];
                $newUploadedDocumentOrder->count = $oneUploadedDocumentData['count'];
                $newUploadedDocumentOrder->product_unit_id = $oneUploadedDocumentData['product_unit_id'];
                $newUploadedDocumentOrder->price = $uploadedDocuments->convertAmount($oneUploadedDocumentData['price']);
                $newUploadedDocumentOrder->amount = $uploadedDocuments->convertAmount($oneUploadedDocumentData['amount']);
                $newUploadedDocumentOrder->production_type_id = $oneUploadedDocumentData['production_type_id'];

                if (!$newUploadedDocumentOrder->validate(['name', 'count', 'product_unit_id', 'price', 'amount', 'production_type_id'])) {
                    $errors['uploadedDocumentOrder']['new'][$key] = [
                        $newUploadedDocumentOrder->getErrors(),
                    ];
                }

                if (intval($newUploadedDocumentOrder->price) <= 0) {
                    $errors['uploadedDocumentOrder']['new'][$key][0]['price'] = ['Цена должна быть больше 0'];
                }
                if (intval($newUploadedDocumentOrder->amount) <= 0) {
                    $errors['uploadedDocumentOrder']['new'][$key][0]['amount'] = ['Цена должна быть больше 0'];
                }
            }
        }
        unset($uploadedDocumentOrderData['new']);
        foreach ($uploadedDocumentOrderData as $key => $oneUploadedDocumentOrderData) {
            $oldUploadedDocumentOrderData = UploadedDocumentOrder::findOne($key);
            if ($oldUploadedDocumentOrderData === null) {
                throw new NotFoundHttpException('Запрошенная страница не существует.');
            }
            $oldUploadedDocumentOrderData->name = $oneUploadedDocumentOrderData['name'];
            $oldUploadedDocumentOrderData->count = $oneUploadedDocumentOrderData['count'];
            $oldUploadedDocumentOrderData->product_unit_id = $oneUploadedDocumentOrderData['product_unit_id'];
            $oldUploadedDocumentOrderData->price = $uploadedDocuments->convertAmount($oneUploadedDocumentOrderData['price']);
            $oldUploadedDocumentOrderData->amount = $uploadedDocuments->convertAmount($oneUploadedDocumentOrderData['amount']);
            $oldUploadedDocumentOrderData->production_type_id = $oneUploadedDocumentOrderData['production_type_id'];

            if (!$oldUploadedDocumentOrderData->validate(['name', 'count', 'product_unit_id', 'price', 'amount', 'production_type_id'])) {
                $errors['uploadedDocumentOrder'][$key] = [
                    $oldUploadedDocumentOrderData->getErrors(),
                ];
            }
            if (intval($oldUploadedDocumentOrderData->price) <= 0) {
                $errors['uploadedDocumentOrder'][$key][0]['price'] = ['Цена должна быть больше 0'];
            }
            if (intval($oldUploadedDocumentOrderData->amount) <= 0) {
                $errors['uploadedDocumentOrder'][$key][0]['amount'] = ['Цена должна быть больше 0'];
            }
        }

        return $errors;
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionChangeDocument($id)
    {
        $model = $this->findModel($id);
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $uploadedDocumentOrder = new UploadedDocumentOrder();
        $existsContractor = Contractor::find()->andWhere(['and',
            ['ITN' => $model->inn],
            ['company_id' => $model->company_id],
            ['is_deleted' => false],
            ['type' => Contractor::TYPE_SELLER],
        ])->andFilterWhere(['PPC' => $model->kpp])->exists();
        if ($model->company_id === $company->id) {
            return $this->renderPartial('_partial/_form', [
                'uploadedDocument' => $model,
                'company' => $company,
                'uploadedDocumentOrder' => $uploadedDocumentOrder,
                'existsContractor' => $existsContractor,
                'activeTab' => UploadedDocuments::TAB_REQUISITES,
            ]);
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeDocumentNumber($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->document_number = Yii::$app->request->post('document_number');
        if ($model->validate(['document_number']) && $model->save(true, ['document_number'])) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'error' => $model->getErrors('document_number'),
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionChangeDocumentDate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->document_date = Yii::$app->request->post('document_date');
        if ($model->validate(['document_date']) && $model->save(true, ['document_date'])) {
            return [
                'result' => true,
            ];
        }

        return [
            'result' => false,
            'error' => $model->getErrors('document_date'),
        ];
    }

    /**
     * @param $id
     * @return bool
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        foreach ($model->uploadedDocumentOrders as $uploadedDocumentOrder) {
            $uploadedDocumentOrder->delete();
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionManyDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = Yii::$app->request->post('UploadedDocuments');
        foreach ($data as $uploadedDocumentID => $isChecked) {
            if ($isChecked) {
                $uploadedDocument = $this->findModel($uploadedDocumentID);
                foreach ($uploadedDocument->uploadedDocumentOrders as $uploadedDocumentOrder) {
                    $uploadedDocumentOrder->delete();
                }
                $uploadedDocument->delete();
            }
        }

        return true;
    }

    /**
     * @param $id
     * @return UploadedDocuments
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = UploadedDocuments::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        return $model;
    }
}