<?php

namespace frontend\modules\documents\controllers;

use DateTime;
use Yii;
use common\components\DadataClient;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\phpWord\PhpWordForeignCurrencyInvoice;
use common\models\Agreement;
use common\models\Company;
use common\models\CompanyHelper;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\NdsOsno;
use common\models\TaxRate;
use common\models\address\Country;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\AbstractDocument;
use common\models\document\EmailSignature;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\InvoiceContractorSignature;
use common\models\document\InvoiceEssence;
use common\models\document\NdsViewType;
use common\models\document\OperationType;
use common\models\document\ForeignCurrencyInvoiceOrder;
use common\models\document\PaymentOrder;
use common\models\document\PaymentOrderInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use common\models\out\OutInvoice;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\service\SubscribeTariffGroup;
use frontend\components\Icon;
use frontend\components\StatisticPeriod;
use frontend\components\filters\CompleteRegistrationFilter;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use frontend\modules\documents\forms\ForeignCurrencyInvoiceFlowForm;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\ForeignCurrencyInvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ForeignCurrencyInvoice controller
 */
class ForeignCurrencyInvoiceController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $type;

    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_FOREIGN_CURRENCY_INVOICE;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'upd',
                            'index',
                            'get-invoices',
                            'get-invoice',
                            'add-modal-contractor-fast',
                            'add-modal-details-address',
                            'add-modal-logo',
                            'validate-company-modal',
                            'first-create-2',
                            'generate-xls',
                            'get-xls',
                            'contractor-product',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::INDEX,
                        ],
                        'roleParams' => function ($rule) {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'add-modal-product',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Product::CREATE,
                        ],
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                                'type' => Yii::$app->request->post('documentType'),
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'index-auto',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create', 'create-from-order', 'create-from-agreement', 'update-from-order', 'autoinvoice', 'first-create', 'copy-invoice',
                            'get-contractor-expenditure-item', 'copy-auto', 'many-create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Invoice::CREATE, [
                                'ioType' => Yii::$app->request->getQueryParam('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['view', 'file-get', 'file-list', 'scan-list', 'change-contractor-signature', 'files'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModel(null, null, true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['view-auto', 'print-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModelAuto(null, true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['file-upload', 'file-delete', 'scan-bind'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['document-print', 'docx', 'document-png'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModel(null, Yii::$app->request->getQueryParam('type'), true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-document-print'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->getQueryParam('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['update', 'update-status', 'get-contractor-expenditure-item',
                            'comment-internal', 'set-currency-rate'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => [
                            'update-contractor-fields',
                            'update-company-fields',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $isAuto = Yii::$app->request->get('isAuto');

                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $isAuto ? $action->controller->loadModelAuto() : $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['update-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModelAuto(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['can-add-one-document'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE) && (
                                Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ]) || Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ])
                            );
                        },
                    ],
                    [
                        'actions' => ['update-status'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'model' => $action->controller->loadModel(), // throw exception if not found
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['send'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::VIEW,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->loadModel(),
                            ];
                        },
                    ],
                    [
                        'actions' => ['create-payment-order',],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\document\PaymentOrder::CREATE)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => [
                            'add-flow',
                            'add-flow-form',
                            'add-flow-validate',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => [
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => [
                            'delete-auto',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                    'model' => $action->controller->loadModelAuto(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'many-send-by-contractor', 'get-many-send-message-panel', 'send-many-in-one'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'many-send-by-contractor'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE) && (
                                Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ]) || Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ])
                            );
                        },
                    ],
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'out-view',
                            'out-view-login',
                            'out-view-reset',
                            'out-view-save',
                            'out-view-signup',
                        ],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['not-show', 'basis-document', 'amount-on-date', 'hide-scan-popup', 'run-scan-popup', 'get-contractor-info'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['unpaid', 'not-need-document'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'create-checking-accountant',
                            'update-company',
                            'update-account',
                        ],
                        'allow' => true,
                        'roles' => [permissions\Company::UPDATE],
                    ],
                    [
                        'actions' => [
                            'set-responsible',
                            'mass-set-responsible',
                        ],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
            'accessByTariff' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'create',
                            'update',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $company = Yii::$app->user->identity->company;

                            return Yii::$app->request->getQueryParam('type') == Documents::IO_TYPE_IN ||
                                !$company->getForeignCurrencyInvoices()->byIOType(Documents::IO_TYPE_OUT)->exists() ||
                                $company->getHasActualSubscription(SubscribeTariffGroup::FOREIGN_CURRENCY_INVOICE);
                        },
                    ],
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return !in_array($action->id, [
                                'create',
                                'update',
                            ]);
                        },
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    $link = Html::a('оплатить подписку', [
                        '/subscribe/default/index',
                    ]);
                    $message = "Необходимо $link \"Выставление Инвойсов\"";
                    throw new ForbiddenHttpException($message);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'set-currency-rate' => ['post'],
                ],
            ],
            'completeRegistration' => CompleteRegistrationFilter::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        $actions['add-modal-contractor'] = [
            'class' => 'frontend\components\AddModalContractorAction',
            'beforeRender' => function ($action, $model) {
                $model->face_type = Contractor::TYPE_FOREIGN_LEGAL_PERSON;
            }
        ];

        return $actions;
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->view->theme === null) {
                Yii::$app->params['bsVersion'] = '4';
                $bandle = Yii::$app->assetManager->getBundle('frontend\themes\kub\assets\KubAsset');
                Yii::$app->params['kubAssetBaseUrl'] = $bandle->baseUrl;
                Yii::$app->params['kubAssetBasePath'] = $bandle->basePath;
                Yii::$app->view->theme = Yii::createObject([
                    'class' => 'yii\base\Theme',
                    'basePath' => '@frontend/themes/kub',
                    'baseUrl' => '@web/themes/kub',
                    'pathMap' => [
                        '@common/models/file/widgets/views' => '@frontend/themes/kub/file',
                        '@frontend/views' => '@frontend/themes/kub/views',
                        '@frontend/modules' => '@frontend/themes/kub/modules',
                        '@frontend/widgets' => '@frontend/themes/kub/widgets',
                    ],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Lists models.
     *
     * @param integer $type
     * @return mixed
     */
    public function actionIndex($type, $currency = null)
    {
        if ($currency && !in_array($currency, Currency::$foreignArray)) {
            throw new NotFoundHttpException();
        }
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $currencyNameArray = array_filter($company->getForeignCurrencyInvoices()->select('currency_name')->distinct()->column());
        $currencyName = $currency ?: reset($currencyNameArray);
        $searchModel = new ForeignCurrencyInvoiceSearch([
            'type' => $type,
            'company_id' => $company->id,
            'currency_name' => $currencyName,
            'employeeCompany' => $employee->currentEmployeeCompany,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, StatisticPeriod::getSessionPeriod());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'currencyNameArray' => $currencyNameArray,
            'currencyName' => $currencyName,
            'ioType' => $type,
            'message' => new Message($this->typeDocument, $type),
        ]);
    }

    /**
     * @param $type
     * @param bool $isContract
     * @param null $createDocument
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionFirstCreate($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        if (Yii::$app->request->getIsGet() && $company->getForeignCurrencyInvoices()->exists()) {
            return $this->redirect(Url::current([0 => 'create']));
        }

        $nds = Yii::$app->request->post('Company')['nds'] ?? null;
        if (isset($nds) && in_array($nds, [NdsOsno::WITH_NDS, NdsOsno::WITHOUT_NDS])) {
            $company->updateAttributes(['nds' => $nds]);
        }

        return $this->actionCreate($type);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type)
    {
        self::checkType($type);
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $rsArray = $company->getForeignCurrencyAccounts()
            ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
            ->orderBy(['type' => SORT_ASC])
            ->indexBy('id')
            ->with('currency')
            ->all();

        $rs = reset($rsArray);

        if (Yii::$app->request->getIsGet() && $this->action->id != 'first-create' && !$company->getForeignCurrencyInvoices()->exists()) {
            return $this->redirect(Url::current([0 => 'first-create']));
        }

        $model = new ForeignCurrencyInvoice([
            'scenario' => ForeignCurrencyInvoice::SCENARIO_USER_CREATE,
            'type' => $type,
            'company_id' => $company->id,
            'contractor_id' => Yii::$app->request->get('contractorId'),
            'nds_view_type_id' => $company->nds_view_type_id,
            'show_article' => Yii::$app->user->identity->config->invoice_form_article,
        ]);
        if ($rs) {
            $model->checking_accountant_id = $rs->id;
            $model->populateRelation('checkingAccountant', $rs);
        }
        if (($fromId = Yii::$app->request->get('clone')) && ($fromModel = $this->loadModel($fromId, $type))) {
            $model = $this->copyModelData($model, $fromModel);
        }
        $model->document_author_id = $employee->id;
        $model->invoice_status_author_id = $employee->id;
        $model->invoice_status_id = InvoiceStatus::STATUS_CREATED;
        $model->document_date = date('Y-m-d');
        $model->payment_limit_date = date_create('+10 day')->format('Y-m-d');
        $model->document_number = $type == Documents::IO_TYPE_IN ? null :
                ForeignCurrencyInvoice::getNextDocumentNumber($company->id, $type);

        $invoiceEssence = $company->invoiceEssence ?? new InvoiceEssence([
            'company_id' => $company->id,
        ]);

        if ($from = Yii::$app->request->get('from')) {
            $fromInvoice = $company->getForeignCurrencyInvoices()->andWhere([
                'type' => Documents::IO_TYPE_IN,
                'is_deleted' => false,
                'id' => $from,
            ])->one();
            if ($fromInvoice !== null) {
                $addProduct = $fromInvoice->getOrders()->select('product_id')->column();
                if ($addProduct) {
                    $orderArray = [];
                    foreach ($addProduct as $pid) {
                        if ($product = Product::getById($model->company_id, $pid)) {
                            $order = ForeignCurrencyInvoiceOrder::createFromProduct($product, $model, 1);
                            $orderArray[] = $order;
                        }
                    }
                    $model->populateRelation('orders', $orderArray);
                    $model->beforeValidate();
                }
            }
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text_en = $model->comment;
                if (($invoiceEssence->is_checked_en && $invoiceEssence->text_en) ||
                    !$invoiceEssence->is_checked_en) {
                    $invoiceEssence->save();
                }
            }
            $this->redirect(['view', 'type' => $type, 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'rsArray' => $rsArray,
            'invoiceEssence' => $invoiceEssence,
            'company' => $company,
            'employee' => $employee,
            'ioType' => $type,
            'fixedContractor' => false,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($id, $type)
    {
        self::checkType($type);
        $employee = Yii::$app->user->identity;
        $company = $employee->company;
        $rsArray = $company->getForeignCurrencyAccounts()
            ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
            ->orderBy(['type' => SORT_ASC])
            ->indexBy('id')
            ->with('currency')
            ->all();

        $model = $this->loadModel($id, $type);
        $model->scenario = $model::SCENARIO_USER_UPDATE;

        $invoiceEssence = $company->invoiceEssence ?? new InvoiceEssence([
            'company_id' => $company->id,
        ]);

        if (!$model->updateAllowed()) {
            $this->redirect([
                'view',
                'type' => $type,
                'id' => $model->id,
                'contractorId' => Yii::$app->request->getQueryParam('contractorId'),
            ]);
        }

        $fixedContractor = false;
        $model->show_article = Yii::$app->user->identity->config->invoice_form_article;

        if ($model->load(Yii::$app->getRequest()->post()) && $model->save()) {
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text_en = $model->comment;
                if (($invoiceEssence->is_checked_en && $invoiceEssence->text_en) ||
                    !$invoiceEssence->is_checked_en) {
                    $invoiceEssence->save();
                }
            }
            $this->redirect(['view', 'type' => $type, 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'rsArray' => $rsArray,
            'invoiceEssence' => $invoiceEssence,
            'company' => $company,
            'employee' => $employee,
            'ioType' => $type,
            'fixedContractor' => $fixedContractor,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $invoiceContractorSignature = $model->company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $model->company_id;
        }
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->contractor_id;
        $contractorTab = Yii::$app->request->getQueryParam('contractorTab', null);

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
            'useContractor' => $useContractor,
            'contractorTab' => $contractorTab,
            'invoiceContractorSignature' => $invoiceContractorSignature,
        ]);
    }

    /**
     * @param null $id
     * @param null $ioType
     * @param bool $allowDeleted
     * @return Invoice|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function loadModel($id = null, $ioType = null, $allowDeleted = false)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }
        if ($ioType === null) {
            $ioType = Yii::$app->request->getQueryParam('type');
        }

        /* @var Invoice $model */
        $model = parent::loadModel($id, $ioType);

        if (!$allowDeleted && $model->is_deleted) {
            throw new NotFoundHttpException('Документ удален.');
        }

        return $model;
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        /* custom asset */

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf') ? $model->company->pdf_signed : 0,
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'addStamp' => $model->company->pdf_signed
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $invoicesID = explode(',', $multiple);
        /* @var $model Invoice */
        $model = $this->loadModel(current($invoicesID), $type);

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf' && $model->company->pdf_signed) ? true : false,
            'multiple' => $invoicesID,
        ]);
    }

    /**
     * @param $uid
     * @param null $email
     * @param null $payment
     * @return string|\yii\console\Response|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionOutView($uid, $payment = null)
    {
        $this->layout = 'out-view';
        $email = Yii::$app->request->get('email');

        if ($uid == 'obrasetc-silki-na-schet') {
            $uid = YII_ENV_PROD ? 'fGkg0' : '7ubrQ';
        }
        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        if ($invoice->invoice_status_id == InvoiceStatus::STATUS_SEND) {
            $invoice->updateAttributes(['invoice_status_updated_at' => time()]);
        }

        return $this->render('out-view', [
            'invoice' => $invoice,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
            'isDemo' => false
        ]);
    }

    /**
     * Get an invoice file by uid for unauthorized users
     * @param $type file type
     * @param $uid invoice uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getInvoiceByUid($uid);
        $model->out_download_count += 1;
        $model->save(true, ['out_download_count']);

        switch ($type) {
            case 'pdf':
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean)$model->company->pdf_signed,
                ]);
                break;

            default:
                $doc = new PhpWordForeignCurrencyInvoice($model);
                return $doc->getFile();
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Invoice
     * @throws NotFoundHttpException
     */
    public static function getInvoiceByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = ForeignCurrencyInvoice::find()
            ->byIOType($type)
            ->byUid($uid)
            ->byDeleted(false)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }

    /**
     * @param $type
     * @param $id
     * @return Invoice
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function copyModelData($model, $fromModel)
    {
        $model->setAttributes($fromModel->getAttributes());
        $cloneOrders = [];
        foreach ($fromModel->orders as $order) {
            $cloneOrders[] = $this->cloneOrders($order, $model);
        }
        $model->populateRelation('orders', $cloneOrders);

        return $model;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function cloneOrders(ForeignCurrencyInvoiceOrder $order, ForeignCurrencyInvoice $invoice)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->foreign_currency_invoice_id = null;
        $cloneOrder->populateRelation('invoice', $invoice);

        return $cloneOrder;
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $status = InvoiceStatus::findOne(Yii::$app->request->getBodyParam('status'));

        if ($status !== null && $status->id != $model->invoice_status_id) {
            $model->invoice_status_id = $status->id;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                if ($model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id'])) {
                    return true;
                }

                return false;
            });
        }
        if ($model->invoice_status_id == InvoiceStatus::STATUS_REJECTED) {
            Yii::$app->session->setFlash('success', 'Счет отменён');
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
            'textRequired' => true,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Company $sender */
        $company = Yii::$app->user->identity->company;
        /** @var EmployeeCompany $sender */
        $sender = Yii::$app->user->identity->currentEmployeeCompany;

        $sendWithDocuments = Yii::$app->request->get('send_with_documents');

        $contractors = null;
        $invoices = Yii::$app->request->post('Invoice');
        $send = 0;
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                /* @var $model Invoice */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }

                if (($sendTo = $model->contractor->someEmail) &&
                    $model->sendAsEmail($sender, $sendTo, null, null, null, null, null, false, $sendWithDocuments)) {
                    $send += 1;
                } else {
                    $contractors[$model->contractor->id] = Html::a($model->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->contractor->type, 'id' => $model->contractor->id
                    ]));
                }
            }
        }
        $message = 'Отправлено ' . $send . ' из ' . count($invoices) . ' писем.';
        if ($send == count($invoices)) {
            Yii::$app->session->setFlash('success', $message);
        }

        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки счетов, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @param $contractor
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionManySendByContractor($type, $contractor)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = null;
        $invoices = explode(', ', Yii::$app->request->post('invoices'));
        $send = 0;
        $all = 0;
        foreach ($invoices as $id) {
            /* @var Invoice $model */
            $model = $this->loadModel($id, $type);
            $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
                'model' => $model,
            ]);

            if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($sendForm);
            }

            if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
                $saved = $sendForm->send();
                if (is_array($saved)) {
                    $send += $saved[0];
                    $all += $saved[1];
                    $model->email_messages += $saved[0];
                    $model->save(true, ['email_messages']);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Отправлено ' . $send . ' из ' . $all . ' писем.');

        return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlowValidate($type, $id)
    {
        self::checkType($type);
        $employee = Yii::$app->user->identity;
        $company = $employee->company;

        $invoice = $this->loadModel($id, $type);

        $model = new ForeignCurrencyInvoiceFlowForm([
            'invoice' => $invoice,
            'employee' => Yii::$app->user->identity,
        ]);

        return $this->ajaxValidate($model);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlow($type, $id)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException();
        }
        $idArray = explode(',', $id);

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $modelArray = $company->getForeignCurrencyInvoices()->andWhere([
            'id' => $idArray,
            'type' => $type,
            'is_deleted' => false,
        ])->all();

        $invoiceArray = [];
        foreach ($modelArray as $model) {
            if ($model->availablePaymentAmount > 0) {
                $invoiceArray[] = $model;
            }
        }

        if ($invoiceArray) {
            if (count($invoiceArray) === 1) {
                $model = array_shift($invoiceArray);

                $flowForm = new ForeignCurrencyInvoiceFlowForm([
                    'invoice' => $model,
                    'employee' => Yii::$app->user->identity,
                ]);

                if ($flowForm->load(Yii::$app->request->post()) && $flowForm->validate()) {
                    $flowForm->save();
                }

                if ($flowForm->invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
                    Yii::$app->session->setFlash('success', 'Счет оплачен');
                } elseif ($flowForm->invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                    Yii::$app->session->setFlash('success', 'Счет оплачен частично');
                }
            } else {
                $paidCount = 0;
                $hasFlowsMessage = [];
                $flowForm = new ForeignCurrencyInvoiceFlowForm([
                    'invoice' => new ForeignCurrencyInvoice(['type' => $type, 'company' => $company]),
                    'employee' => Yii::$app->user->identity,
                ]);

                if (Yii::$app->request->post('ajax') && $flowForm->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return ActiveForm::validate($flowForm, ['flowType', 'date_pay', 'isAccounting']);
                }

                if ($flowForm->load(Yii::$app->request->post()) && $flowForm->validate(['flowType', 'date_pay', 'isAccounting'])) {
                    $modelArraySorted = [];
                    foreach ($invoiceArray as $invoice) {
                        $modelArraySorted[$invoice->contractor_id][] = $invoice;
                    }
                    foreach ($modelArraySorted as $contractorId => $invoiceArray) {
                        $invoice = $invoiceArray[0];
                        $contractor = $invoice->contractor;
                        $hasFlows = [];
                        if ($contractor->getNotLinkedBankFlows($company)->exists()) {
                            $hasFlows[] = 'банку';
                        }
                        if ($contractor->getNotLinkedOrderFlows($company)->exists()) {
                            $hasFlows[] = 'кассе';
                        }
                        if ($contractor->getNotLinkedEmoneyFlows($company)->exists()) {
                            $hasFlows[] = 'e-money';
                        }

                        if ($hasFlows) {
                            $contractorName = $invoice->contractor->nameWithType;
                            $flows = implode(', ', $hasFlows);
                            $numbers = implode(', ', array_map(function ($invoice) {
                                return '№' . $invoice->fullNumber;
                            }, $invoiceArray));
                            $message = "По {$contractorName} есть оплаты по {$flows} не привязанные к счетам.";
                            $message .= " Что бы не продублировать оплаты нужно проставить по каждому счету оплату отдельно.";
                            $message .= "<br>Счет {$numbers}.";
                            $hasFlowsMessage[] = $message;
                        } else {
                            $amount = array_sum(array_map(function ($invoice) {
                                return $invoice->availablePaymentAmount;
                            }, $invoiceArray));
                            $numbers = implode(', ', array_map(function ($invoice) {
                                return '№' . $invoice->fullNumber . ' от ' .
                                    DateTime::createFromFormat('Y-m-d', $invoice->document_date)->format('d.m.Y');
                            }, $invoiceArray));
                            if ($amount > 0) {
                                $flowForm->invoice = $invoice;
                                $flowForm->amount = $amount;
                                $flowModel = $flowForm->createFlow();
                                $flowModel->description = "Оплата счета {$numbers}";
                                $isSaved = Yii::$app->db->transaction(function ($db) use ($flowModel, $invoiceArray, $company) {
                                    if (LogHelper::save($flowModel, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                                        foreach ($invoiceArray as $invoice) {
                                            if (!$flowModel->linkInvoice($invoice)) {
                                                $db->transaction->rollBack();

                                                return false;
                                            }
                                        }

                                        return true;
                                    }

                                    $db->transaction->rollBack();

                                    return false;
                                });

                                if ($isSaved) {
                                    $paidCount += count($invoiceArray);
                                }
                            }
                        }
                    }
                }

                if ($paidCount) {
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 15);
                    Yii::$app->session->setFlash('success', "Оплачено счетов: {$paidCount}.");
                }
                if ($hasFlowsMessage) {
                    Yii::$app->session->setFlash('info', implode('<br><br>', $hasFlowsMessage));
                }
            }
        } else {
            Yii::$app->session->setFlash('info', "Нет доступных для оплаты счетов.");
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlowForm($type, $id)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException();
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $idArray = explode(',', $id);

        $model = (count($idArray) == 1) ? $company->getForeignCurrencyInvoices()->andWhere([
            'id' => $idArray[0],
            'type' => $type,
            'is_deleted' => false,
        ])->one() : null;

        if ($model === null) {
            $model = new ForeignCurrencyInvoice([
                'type' => $type,
                'company_id' => $company->id,
                'contractor_id' => Yii::$app->request->getQueryParam('contractorId', null),
                'price_precision' => 2,
            ]);
        }

        return $this->renderAjax('_add_flow_form', [
            'model' => $model,
            'idArray' => $idArray,
            'useContractor' => (boolean)Yii::$app->request->getQueryParam('contractorId', false),
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdateCompanyFields($id, $type, $isAuto = false)
    {
        /* @var $model Invoice */
        if ($isAuto) {
            $model = $this->loadModelAuto($id);
        } else {
            $model = $this->loadModel($id, $type);
        }
        $model->setCompany($model->company);

        $model->save(true, array_merge(Company::$invoiceRequisites, ['updated_at']));

        Yii::$app->session->setFlash('success', 'Информация по компании обновлена');

        if ($isAuto) {
            return $this->redirect(['update-auto', 'id' => $id]);
        } else {
            return $this->redirect(['update', 'id' => $id, 'type' => $model->type]);
        }
    }

    /**
     * @param $id
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdateContractorFields($id, $type, $isAuto = false)
    {
        /* @var $model Invoice */
        if ($isAuto) {
            $model = $this->loadModelAuto($id);
        } else {
            $model = $this->loadModel($id, $type);
        }
        if ($model->contractor) {
            $model->setContractor($model->contractor);
        }
        $model->save(true, ['contractor_name_short', 'contractor_name_full', 'contractor_director_name',
            'contractor_director_post_name', 'contractor_address_legal_full', 'contractor_bank_name', 'contractor_bank_city',
            'contractor_bik', 'contractor_inn', 'contractor_kpp', 'contractor_ks', 'contractor_rs']);

        Yii::$app->session->setFlash('success', 'Информация по покупателю обновлена');

        if ($isAuto) {
            return $this->redirect(['update-auto', 'id' => $id]);
        } else {
            return $this->redirect(['update', 'id' => $id, 'type' => $model->type]);
        }
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\web\ForbiddenHttpException
     *
     */
    public function actionDelete($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $deleted = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE, function ($model) {
            $model->is_deleted = true;
            return $model->save(false, ['is_deleted']);
        });

        if ($deleted) {
            Yii::$app->session->setFlash('success', 'Инвойс удален');
        } else {
            Yii::$app->session->setFlash('error', "Ошибка при удалении Инвойса.");
        }

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type, $contractor = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $invoices = Yii::$app->request->post('Invoice');
        $noDeletedInvoices = [];
        $deletedInvoices = [];
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                $model = ForeignCurrencyInvoice::findOne([
                    'id' => $id,
                    'company_id' => Yii::$app->user->identity->company->id,
                    'type' => $type,
                    'is_deleted' => false,
                ]);
                if ($model) {
                    $deleted = (bool) $model->delete();

                    if ($deleted) {
                        $deletedInvoices[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                    } else {
                        $reasonText = !in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed) ? ' (счет оплачен)' : '';
                        $noDeletedInvoices[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date)) . $reasonText;
                    }
                }
            }
        }

        if (count($noDeletedInvoices) > 0) {
            \Yii::$app->session->setFlash('error', 'Инвойсы не могут быть удалены:<br>' . implode('<br>', $noDeletedInvoices));
        }
        if (count($deletedInvoices) > 0) {
            \Yii::$app->session->setFlash('success', 'Инвойсы успешно удалены:<br>' . implode('<br>', $deletedInvoices));
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $searchModel = new ForeignCurrencyInvoiceSearch([
            'type' => Documents::IO_TYPE_IN,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dateRange = StatisticPeriod::getSessionPeriod();
        $contractorID = Yii::$app->request->post('contractor_id');

        $documentNumber = Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
        if ($contractorID) {
            $dataProvider->query->andWhere(['contractor_id' => $contractorID]);
        }
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('partial/_modal_invoice_table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentNumber' => $documentNumber,
        ]);
    }

    public function actionAddModalContractorFast()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new Contractor();
        $model->status = Contractor::ACTIVE;
        $scenario = Contractor::SCENARIO_LEGAL_FACE;
        $model->setScenario($scenario);
        $model->type = Yii::$app->request->post('documentType');

        $inn = Yii::$app->request->post('inn');
        $contractorAttributes = DadataClient::getContractorAttributes($inn);

        if ($contractorAttributes) {

            $model->setAttributes($contractorAttributes, false);

            if (empty($model->director_name)) {
                $model->director_name = 'ФИО руководителя';
            }

            $model->taxation_system = Contractor::WITHOUT_NDS;
            $model->chief_accountant_is_director = 1;
            $model->contact_is_director = 1;
            $model->setAttribute('company_id', Yii::$app->user->identity->company->id);
            $model->setAttribute('employee_id', Yii::$app->user->id);
            $model->setAttribute('object_guid', OneCExport::generateGUID());

            if ($model->save()) {

                return $data = [
                    'id' => $model->id,
                    'name' => $model->name,
                    'name-short' => $model->companyType ? $model->companyType->name_short : '',
                ];
            }
        }

        $header = $this->renderAjax('partial/create-contractor_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('partial/create-contractor_body', [
            'model' => $model,
            'documentType' => $model->type,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed|string
     */
    public function actionAddModalProduct()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;

        $documentType = Yii::$app->request->getBodyParam('documentType');
        $storeId = Yii::$app->request->getBodyParam('storeId');
        $product = Yii::$app->request->getBodyParam('Product');
        $document = Yii::$app->request->getBodyParam('document');
        $productionType = $product['production_type'];
        $model = $this->createHelper($productionType);
        if (isset($product['group_id'])) {
            $model->group_id = $product['group_id'];
        }
        $scenarioProd = $documentType == 1 ? 'zakup' : 'prod';
        $scenarioUslug = $productionType == 1 ? 'tovar' : 'uslug';
        $scenario = $scenarioProd . $scenarioUslug;
        $model->setScenario($scenario);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if ($documentType == Documents::IO_TYPE_OUT) {
            if ($company->companyTaxationType->osno) {
                $model->price_for_sell_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
            }
        } else {
            if ($company->companyTaxationType->osno) {
                $model->price_for_buy_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            }
        }

        if (!isset($product['flag'])) {
            if (Yii::$app->request->post('ajax') !== null) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->_prepareFromInvoice() && $model->save()) {
                return [
                    'items' => $model->getProductsByIds([$model->id], $company->id),
                    'services' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                    ])->count(),
                    'goods' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_GOODS,
                    ])->count(),
                ];
            }
            if ($documentType == Documents::IO_TYPE_OUT) {
                $attribute = 'price_for_sell_with_nds';
            } else {
                $attribute = 'price_for_buy_with_nds';
            }
            if ($model->getAttribute($attribute) == 0) {
                $model->addError($attribute, 'Необходимо заполнить «' . $model->getAttributeLabel($attribute) . '».');
            }
        }
        $header = $this->renderPartial('partial/create-product_header', ['productionType' => $productionType]);
        $body = $this->renderAjax('partial/create-product_body', [
            'model' => $model,
            'documentType' => $documentType,
            'document' => $document,
            'storeId' => $storeId
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @param $productionType
     * @return Product
     */
    protected function createHelper($productionType)
    {
        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;

        return $model;
    }

    /**
     * @return array|mixed
     */
    public function actionAddModalDetailsAddress()
    {
        $attributes = [
            'name_short_en',
            'name_full_en',
            'form_legal_en',
            'address_legal_en',
            'address_actual_en',
            'chief_post_name_en',
            'lastname_en',
            'firstname_en',
            'patronymic_en',
            'not_has_patronymic_en',
        ];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);
        $model->requiredEnAttributes = true;
        $model->chief_is_chief_accountant = true;

        if ($model->load(Yii::$app->request->post()) && $model->save(true, $attributes)) {
            return [
                'name' => $model->name_short,
                'type' => $model->companyType->name_short,
            ];
        }

        // First invoice
        if ($inn = Yii::$app->request->post('inn')) {
            $companyAttributes = DadataClient::getCompanyAttributes($inn);
            $model->setAttributes($companyAttributes, false);
        }

        $header = $this->renderAjax('partial/create-company_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('partial/create-company_body', [
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed
     */
    public function actionValidateCompanyModal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);
        $model->requiredEnAttributes = true;
        $model->chief_is_chief_accountant = true;

        if (Yii::$app->request->post('ajax')) {
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }
    }

    /**
     * @return array|mixed|string
     */
    public function actionAddModalLogo()
    {
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }
        if (CompanyHelper::load($model, true) && CompanyHelper::validate($model, true) && CompanyHelper::save($model, true)) {
            return json_encode(['success' => true]);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $header = $this->renderAjax('partial/create-logo_header');
        $body = $this->renderAjax('partial/create-logo_body', [
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array
     */
    public function actionGetContractorExpenditureItem()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contractor = Contractor::findOne(Yii::$app->request->post('id'));
        if ($contractor !== null && $contractor->invoice_expenditure_item_id !== null) {
            return $contractor->invoice_expenditure_item_id;
        }

        return 'null';
    }

    /**
     * @return string
     */
    public function actionBasisDocument()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        $company = Yii::$app->user->identity->company;
        $contractor = Contractor::findOne(['id' => Yii::$app->request->get('contractorId'), 'company_id' => $company->id]);
        $delay = 10;
        $refresh = false;
        $invoiceCount = 0;
        $invoiceSum = 0;

        $model = new Invoice;
        $model->scenario = 'insert';
        $model->company_id = $company->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->price_precision = 2;
        if ($contractor) {
            $delay = $contractor->type == Contractor::TYPE_SELLER ? $contractor->seller_payment_delay : $contractor->customer_payment_delay;
            $refresh = $company->getForeignCurrencyInvoices()->andWhere([
                'contractor_id' => $contractor->id,
                'is_deleted' => false,
            ])->exists();
            $model->type = $contractor->type;
            $model->contractor_id = $contractor->id;
            $model->agreement = $contractor->last_basis_document;
            $model->populateRelation('contractor', $contractor);
            $invoiceStatistic = Invoice::find()
                ->select([
                    'invoice_count' => 'COUNT({{invoice}}.[[id]])',
                    'invoice_sum' => 'SUM(IFNULL({{invoice}}.[[total_amount_with_nds]], 0))',
                ])->where([
                    'and',
                    ['invoice.is_deleted' => 0],
                    ['invoice.type' => (int)Documents::IO_TYPE_OUT],
                    ['invoice.contractor_id' => (int)$contractor->id],
                    ['invoice.invoice_status_id' => InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][(int)Documents::IO_TYPE_OUT]],
                ])->asArray()->one();
            if ($invoiceStatistic['invoice_count']) {
                $invoiceCount = $invoiceStatistic['invoice_count'];
                $invoiceSum = TextHelper::invoiceMoneyFormat($invoiceStatistic['invoice_sum'], 2);
            }
        }

        return $this->renderPartial('form/_basis_document', [
            'model' => $model,
            'delay' => $delay,
            'refresh' => $refresh,
            'invoiceCount' => $invoiceCount,
            'invoiceSum' => $invoiceSum,
        ]);
    }

    /**
     *
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $idArray = [];
        $period = StatisticPeriod::getSessionPeriod();
        $models = Yii::$app->request->post('Invoice');
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $invoices = Invoice::find()
            ->andWhere(['id' => $idArray])
            ->andWhere([Invoice::tableName() . '.company_id' => Yii::$app->user->identity->company_id])
            ->orderBy(['`' . Invoice::tableName() . '`.`document_date`' => SORT_DESC,
                '`' . Invoice::tableName() . '`.`document_number` * 1' => SORT_DESC])
            ->all();

        Invoice::generateXlsTable($period, $invoices);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls($type)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->getHeaders()->set('Content-Type', 'application/vnd.ms-excel');

        $period = StatisticPeriod::getSessionPeriod();

        $searchModel = new ForeignCurrencyInvoiceSearch([
            'type' => $type,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        $invoices = $searchModel->search(
            Yii::$app->request->queryParams,
            $period,
            $searchModel::SORT_PARAM_NAME_OUT,
            true
        );

        Invoice::generateXlsTable($period, $invoices);
        exit;
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUnpaid($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        if ($model->cashBankFlows) {
            foreach ($model->cashBankFlows as $cashFlow) {
                $cashFlow->unlinkInvoice($model);
            }
        }
        foreach ($model->cashEmoneyFlows as $cashFlow) {
            $cashFlow->unlinkInvoice($model);
            if (!$cashFlow->getInvoices()->exists()) {
                $cashFlow->delete();
            }
        }
        foreach ($model->cashOrderFlows as $cashFlow) {
            $cashFlow->unlinkInvoice($model);
            if (!$cashFlow->getInvoices()->exists()) {
                $cashFlow->delete();
            }
        }
        $redirectUrl = Yii::$app->request->post('redirect');

        return $this->redirect($redirectUrl ?: Yii::$app->request->referrer);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($type, $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);

        $model = $this->loadModel($id, $type);

        return ['value' => $model->comment_internal];
    }

    public function getIsPaid($company)
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'b2b-paid', false)) {
            return true;
        }

        return $company->getHasActualSubscription(SubscribeTariffGroup::B2B_PAYMENT);
    }

    public function actionGetContractorInfo()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contractor = Contractor::findOne(Yii::$app->request->post('contractor_id'));
        if ($contractor) {
            $type = Yii::$app->getRequest()->get('type') ?? $contractor->type;

            return ['value' => $type == Documents::IO_TYPE_IN ? $contractor->seller_discount : $contractor->customer_discount];
        }

        return ['value' => 0];
    }

    /**
     * @return array|string
     */
    public function actionCreateCheckingAccountant()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        /* @var $model CheckingAccountant */
        $model = new CheckingAccountant([
            'company_id' => $company->id,
            'currency_id' => Currency::DEFAULT_USD,
        ]);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $rsData = [];
            $currencyData = [];
            $companyRs = $company->getForeignCurrencyAccounts()
                ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
                ->orderBy(['type' => SORT_ASC])
                ->indexBy('id')
                ->with('currency')
                ->all();
            foreach ($companyRs as $rs) {
                $rsData[$rs->id] = $rs->bank_name . ", {$rs->rs} ({$rs->currency->name})";
                $currencyData[$rs->id] = Currency::name($rs->currency_id);
            }
            $rsData["add-modal-rs"] = \frontend\themes\kub\helpers\Icon::PLUS . ' Добавить валютный счет';

            return [
                'result' => true,
                'options' => $rsData,
                'companyRs' => $companyRs,
                'currencyData' => $currencyData,
                'val' => $model->id,
            ];
        }

        return $this->renderAjax('@frontend/views/company/form/modal_rs/_partial/_form', [
            'checkingAccountant' => $model,
            'company' => $company,
        ]);
    }

    /**
     * @return array|string
     */
    public function actionUpdateCompany($type, $id)
    {
        $invoice = $this->loadModel($id, $type);
        $model = Yii::$app->user->identity->company;
        $attributes = [
            'name_short_en',
            'name_full_en',
            'form_legal_en',
            'address_legal_en',
            'address_actual_en',
            'chief_post_name_en',
            'lastname_en',
            'firstname_en',
            'patronymic_en',
            'not_has_patronymic_en',
        ];

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save(true, $attributes)) {
            $invoice->setCompany($model);
            $invoice->save(false);

            return $this->redirect(Yii::$app->getRequest()->referrer);
        }

        return $this->renderAjax('update-company', [
            'model' => $model,
        ]);
    }

    /**
     * @return array|string
     */
    public function actionUpdateAccount($type, $id)
    {
        $invoice = $this->loadModel($id, $type);
        /* @var $company Company */
        $model = $invoice->checkingAccountant;

        if ($model === null) {
            throw new NotFoundHttpException('Банковский счет не найден.');
        }

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $invoice->setCheckingAccountant($model);
            $invoice->save(false);

            return $this->redirect(Yii::$app->getRequest()->referrer);
        }

        return $this->renderAjax('update-account', [
            'model' => $model,
            'company' => Yii::$app->user->identity->company,
        ]);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSetResponsible($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $emplComp = Yii::$app->user->identity->company->getEmployeeCompanies()->andWhere([
            'is_working' => true,
            'employee_id' => Yii::$app->request->post('responsible_employee_id'),
        ])->one();

        if ($emplComp !== null && $model->setResponsible($emplComp)) {
            Yii::$app->session->setFlash('success', 'Ответственный изменен');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось изменить ответственного');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMassSetResponsible()
    {
        $company = Yii::$app->user->identity->company;
        $ids = array_filter((array) Yii::$app->request->post('selected'), function ($val) {
            return $val === strval(intval($val));
        });

        $emplComp = $company->getEmployeeCompanies()->andWhere([
            'is_working' => true,
            'employee_id' => Yii::$app->request->post('responsible_employee_id'),
        ])->one();

        $invoiceArray = $emplComp === null ? [] : $company->getForeignCurrencyInvoices()->andWHere([
            'id' => $ids,
            'invoice_status_id' => InvoiceStatus::$validInvoices,
        ])->all();
        $result = 0;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->setResponsible($emplComp)) {
                $result++;
            }
        }

        if ($result > 0) {
            Yii::$app->session->setFlash('success', 'Ответственный изменен');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось изменить ответственного');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
