<?php

namespace frontend\modules\documents\controllers;

use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderWaybill;
use common\models\document\Waybill;
use common\models\file\File;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\models\Documents;
use frontend\modules\documents\components\UploadManagerHelper;
use yii\web\Response;

/**
 * Class UploadManagerAjaxController
 */
class UploadManagerAjaxController extends FrontendController
{
    public $enableCsrfValidation = false;

    /**
     * @param $file_id
     * @return array
     */
    public function actionCanAttachFile($file_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $file = File::findOne([
            'id' => $file_id,
            'company_id' => \Yii::$app->user->identity->company->id
        ]);

        if ($file) {
            return ['result' => !UploadManagerHelper::getAttachedDocument($file)];
        }

        return ['result' => false];
    }
}
