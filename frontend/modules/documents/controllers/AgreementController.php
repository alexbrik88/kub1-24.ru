<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\models\AgreementEssence;
use common\models\AgreementType;
use common\models\document\status\AgreementStatus;
use common\models\AgreementTemplate;
use common\models\Contractor;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\modules\documents\models\AgreementSearch;
use common\models\Agreement;
use frontend\rbac\permissions;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use yii\widgets\ActiveForm;
use frontend\rbac\permissions\Employee;
use yii\web\Response;
use common\models\file;
use yii\helpers\Url;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;


/**
 * Specific documents controller
 */
class AgreementController extends DocumentBaseController
{
    public $typeDocument = Documents::DOCUMENT_AGREEMENT;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return  ArrayHelper::merge([
            'accessAuth' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ], parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['add-modal-contractor'],
                        'allow' => true,
                        'roles' => [
                            permissions\Contractor::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'type' => Yii::$app->request->get('type', Yii::$app->request->post('type')),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'view',
                            'document-print',
                            'agreement-file-list',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::VIEW,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => Agreement::findOne(Yii::$app->request->get('id')),
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'update',
                            'update-contract',
                            'agreement-file-upload',
                            'agreement-file-delete',
                            'comment-internal',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::UPDATE,
                        ],
                        'roleParams' => function () {
                            $model = $this->findModel(Yii::$app->request->get('id'));

                            return [
                                'model' => $model,
                                'ioType' => $model->type,
                            ];
                        },
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['copy'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['get-next-number',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::DELETE,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['update-status', 'send'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::UPDATE_STATUS,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->findModel(Yii::$app->request->get('id')),
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => [
                            'create-page',
                            'update-page',
                            'agreement-create',
                            'agreement-update',
                            'agreement-file-get',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Contractor::CREATE_SELLER,
                            permissions\Contractor::CREATE_CUSTOMER,
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'agreement-file-get' => [
                'class' => file\actions\GetFileAction::className(),
                'model' => file\File::className(),
                'idParam' => 'file-id',
                'fileNameField' => 'filename_full',
                'folderPath' => function (file\File $model) {
                    return $model->getUploadPath();
                },
            ],
            'agreement-file-list' => [
                'class' => file\actions\FileListAction::className(),
                'model' => Agreement::className(),
                'fileLinkCallback' => function (file\File $file, Agreement $ownerModel) {
                    return Url::to(['agreement-file-get', 'id' => $ownerModel->id, 'file-id' => $file->id,]);
                },
            ],
            'agreement-file-delete' => [
                'class' => file\actions\FileDeleteAction::className(),
                'model' => Agreement::className(),
            ],
            'agreement-file-upload' => [
                'class' => file\actions\FileUploadAction::className(),
                'model' => Agreement::className(),
                'maxFileCount' => 5,
                'maxFileSize' => 5 * 1024 * 1024,
                'folderPath' => 'documents' . DIRECTORY_SEPARATOR . Agreement::$uploadDirectory,
            ],
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @param null $type
     * @return string
     */
    public function actionIndex($type = null)
    {
        $searchModel = new AgreementSearch();
        $searchModel->type = $type;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $backUrl = Url::previous('agreement_last_index_page') ?: ['index', 'type' => $model->type];
        $contractor_id = Yii::$app->request->get('contractor');

        if ($contractor_id) {
            $contractor = Contractor::findOne($contractor_id);
            $backUrl = "/contractor/view?type={$contractor->type}&id={$contractor->id}&activeTab=4";
        }

        return $this->render('view', [
            'model' => $model,
            'backUrl' => $backUrl
        ]);
    }

    /**
     * Creates a new Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['index', 'type' => $type]);
        }

        $returnTo = Yii::$app->request->get('returnTo');
        if ($returnTo) {
            Yii::$app->session->set('return_from_agreement', $returnTo);
        } else {
            Yii::$app->session->remove('return_from_agreement');
        }

        $model = Agreement::find()->where([
            'created_by' => Yii::$app->user->id,
            'company_id' => Yii::$app->user->identity->company->id,
            'is_created' => false,
        ])->one();

        if ($model === null) {
            $model = new Agreement([
                'is_created' => false,
                'created_at' => time(),
                'created_by' => Yii::$app->user->id,
                'company_id' => Yii::$app->user->identity->company->id,
                'document_date' => date(DateHelper::FORMAT_DATE),
                'document_number' => '',
                'document_name' => '',
            ]);

            $model->save(false);

        } else {
            foreach ($model->files as $file) {
                $file->delete();
            }
        }

        $fixed_template = Yii::$app->request->get('from_template');
        $fixed_contractor_id = Yii::$app->request->get('contractor_id');
        $fixed_type = Yii::$app->request->get('type');

        $model->create_agreement_from = 0;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_name = '';
        $model->document_number = '';
        $model->contractor_id = $fixed_contractor_id;
        $model->type = $fixed_type;

        if ($fixed_template) {
            $model->agreement_template_id = (int)$fixed_template;
            $model->create_agreement_from = 1;
        }

        if ($templateDocumentTypeId = Yii::$app->request->get('templateDocumentTypeId')) {
            $model->document_type_id = $templateDocumentTypeId;
            $model->create_agreement_from = 1;
        }

        return $this->renderAjax('partial/_createForm', [
            'model' => $model,
            'fixedContractor' => $fixed_contractor_id ? true : false,
            'fixedType' => $fixed_type ? true : false,
            'fixedTemplate' => $fixed_template,
            'newRecord' => true,
            'disableAllFields' => true,
            'returnTo' => $returnTo,
            'templateDocumentTypeId' => $templateDocumentTypeId
        ]);
    }

    /**
     * Update Agreement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id, $type = null)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect(['index', 'type' => $type]);
        }

        $newRecord = Yii::$app->request->get('old_record') ? false : true;
        /** @var $model Agreement */
        $model = $this->findModel($id);

        if ($fixed_contractor = Yii::$app->request->get('contractor_id', false))
            $model->contractor_id = $fixed_contractor;
        else
            $model->contractor_id = Yii::$app->request->post('contractor_id', false);

        if ($fixed_type = Yii::$app->request->get('type', false))
            $model->type = $fixed_type;
        else
            $model->type = Yii::$app->request->post('type', false);

        if ($fixed_template = Yii::$app->request->get('from_template', false)) {
            $model->create_agreement_from = 1;
            $model->agreement_template_id = (int)$fixed_template;
        } else {
            $postAgreement = Yii::$app->request->post('Agreement');
            if (isset($postAgreement['create_agreement_from']))
                $model->create_agreement_from = $postAgreement['create_agreement_from'];
            if (isset($postAgreement['agreement_template_id']))
                $model->agreement_template_id = $postAgreement['agreement_template_id'];
        }

        if ($model->agreement_template_id > 0) {
            $tplModel = AgreementTemplate::findOne([
                'id' => $model->agreement_template_id,
                'company_id' => Yii::$app->user->identity->company->id,
            ]);
            $model->document_type_id = ($tplModel) ? $tplModel->document_type_id : $model->document_type_id;
        }

        $returnTo = Yii::$app->session->get('return_from_agreement') ? : Yii::$app->request->get('returnTo');

        if ($model->load(Yii::$app->request->post()) && ($model->is_created = true) && $model->save()) {

            if ($model->create_agreement_from == Agreement::CREATE_AGREEMENT_FROM_TEMPLATE) {
                $model->generateEssence();
            }

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, ($newRecord ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE), function ($model) {
                return (boolean)$model->updateAttributes([
                    'status_id' => AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });

            switch ($returnTo) {
                case 'contractor':
                    Yii::$app->session->set('return_from_agreement', null);
                    Yii::$app->session->setFlash('success', 'Договор ' . ($newRecord ? 'добавлен' : 'обновлен'));
                    return $this->redirect([
                        '/contractor/view',
                        'type' => $model->type,
                        'id' => $model->contractor_id,
                        'tab' => 'agreements',
                    ]);
                case 'invoice':
                case 'order-document':
                case 'upd':
                case 'packing-list':
                case 'act':
                    return $this->render('partial/_createView', [
                        'model' => $model,
                        'isNewRecord' => (bool)$newRecord,
                        'agreementPjax' => '#agreement-pjax-container',
                    ]);
                case 'request':
                    if ($model->type == Documents::IO_TYPE_IN) {
                        $agreementPjax = '#carrierAgreement-pjax-container';
                    } else {
                        $agreementPjax = '#customerAgreement-pjax-container';
                    }

                    return $this->render('partial/_createView', [
                        'model' => $model,
                        'isNewRecord' => true,
                        'agreementPjax' => $agreementPjax,
                    ]);
                default:
                    break;
            }

            Yii::$app->session->setFlash('success', 'Договор ' . ($newRecord ? 'добавлен' : 'обновлен'));

            return ($fixed_template) ?
                $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]) :
                $this->redirect(Url::previous('agreement_last_index_page') ?: ['index', 'type' => $type]);
        }

        return $this->render('partial/_createForm', [
            'model' => $model,
            'fixedContractor' => (bool)$fixed_contractor,
            'fixedType' => (bool)$fixed_type,
            'fixedTemplate' => (bool)$fixed_template,
            'newRecord' => $newRecord,
            'disableAllFields' => false,
            'returnTo' => $returnTo
        ]);
    }

    public function actionGetNextNumber($agreement_template_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Agreement();
        $company_id = Yii::$app->user->identity->company->id;
        $tplModel = AgreementTemplate::findOne([
            'id' => $agreement_template_id,
            'company_id' => $company_id,
        ]);

        return [
            'document_type_id' => ($tplModel) ? $tplModel->document_type_id : null,
            'document_number' => $model->getNextDocumentNumber($company_id, $tplModel->id),
            'document_name' => ($tplModel) ? $tplModel->document_name : ''
        ];
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdateContract($id)
    {
        $model = $this->findModel($id);

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->essence->load(Yii::$app->request->post());
            $model->payment_limit_date = DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);

            if ($model->validate() && $model->essence->validate()) {

                $model->essence->save();
                $model->save();

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);

                Yii::$app->session->setFlash('success', 'Договор обновлён');

                return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'type' => $model->type
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id, $type)
    {
        $cloneModel = $this->cloneModel($id);

        if ($cloneModel) {

            Yii::$app->session->setFlash('success', 'Договор скопирован');

            LogHelper::save($cloneModel, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($cloneModel) {
                return (boolean)$cloneModel->updateAttributes([
                    'status_id' => AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });

            return $this->redirect(['view', 'id' => $cloneModel->id, 'type' => $type]);
        }

        Yii::$app->session->setFlash('error', 'Ошибка копирования');

        return $this->redirect(['view', 'id' => $id, 'type' => $type]);
    }

    /**
     * @param $id
     * @param $status
     * @return string|\yii\web\Response
     */
    public function actionUpdateStatus($id, $status)
    {
        $model = $this->findModel($id);

        if ('active' == $status)
            $model->is_completed = false;
        elseif ('ended' == $status)
            $model->is_completed = true;
        else
            return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);

        if ($model->save()) {

            Yii::$app->session->setFlash('success', 'Статус договора обновлён');

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                return (boolean)$model->updateAttributes([
                    'status_id' => ($model->is_completed) ? AgreementStatus::STATUS_FINISHED : AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });

        } else {

            Yii::$app->session->setFlash('error', 'Ошибка обновления статуса');
        }

        return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id, $type = null)
    {
        $model = $this->findModel($id);

        $model->delete();

        Yii::$app->session->setFlash('success', 'Договор удалён');

        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);

        $redirectUrl = Yii::$app->request->referrer && strpos(Yii::$app->request->referrer, 'documents/agreement') === false ?
            Yii::$app->request->referrer :
            ['index', 'type' => $model->type];

        return $this->redirect($redirectUrl);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);
        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);
        $model = $this->findModel($id);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return mixed
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->findModel($id);

        if (!$model->essence)
            throw new NotFoundHttpException('The requested page does not exist.');

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-P',
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSend($type, $id)
    {
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
            'textRequired' => true,
        ]);
        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'id' => $model->id,
            'type' => $model->type,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $id
     * @return Agreement null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Agreement::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }

    /**
     * @param $id
     * @return Agreement
     * @throws NotFoundHttpException
     */
    protected function cloneModel($id)
    {
        $clone = clone $this->findModel($id)->cloneSelf();

        return $clone;
    }

    public function actionCreatePage($type)
    {
        $returnTo = ($this->isScanPreview) ? Url::previous('lastPreviewScansPage') : Yii::$app->request->referrer;
        $fixed_template = Yii::$app->request->get('from_template');
        $fixed_contractor_id = Yii::$app->request->get('contractor_id');
        $newRecord = true;

        $model = new Agreement([
            'is_created' => true,
            'created_at' => time(),
            'created_by' => Yii::$app->user->id,
            'company_id' => Yii::$app->user->identity->company->id,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'type' => $type,
            'contractor_id' => $fixed_contractor_id,
            'document_type_id' => AgreementType::TYPE_AGREEMENT,
        ]);

        if ($model->load(Yii::$app->request->post()) && ($model->is_created = true) && $model->save()) {

            if ($model->create_agreement_from == Agreement::CREATE_AGREEMENT_FROM_TEMPLATE) {
                $model->generateEssence();
            }

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, ($newRecord ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE), function ($model) {
                return (boolean)$model->updateAttributes([
                    'status_id' => AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });


            Yii::$app->session->setFlash('success', 'Договор ' . ($newRecord ? 'добавлен' : 'обновлен'));

            if ($this->isScanPreview) {
                Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
            }

            return $this->redirect($returnTo);
        }

        return $this->render('partial/_createPageForm', [
            'model' => $model,
            'fixedContractor' => $fixed_contractor_id ? true : false,
            'fixedType' => true,
            'fixedTemplate' => $fixed_template,
            'newRecord' => true,
            'disableAllFields' => true,
            'returnTo' => $returnTo
        ]);
    }

    public function actionUpdatePage($id, $type = null)
    {
        $newRecord = false;
        $fixed_type = true;
        $fixed_contractor = false;
        $returnTo = ($this->isScanPreview) ? Url::previous('lastPreviewScansPage') : Yii::$app->request->referrer;
        $fixed_template = Yii::$app->request->get('from_template');

        /** @var $model Agreement */
        $model = Agreement::find()->where([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->one();

        $postAgreement = Yii::$app->request->post('Agreement');
        if (isset($postAgreement['create_agreement_from']))
            $model->create_agreement_from = $postAgreement['create_agreement_from'];
        if (isset($postAgreement['agreement_template_id']))
            $model->agreement_template_id = $postAgreement['agreement_template_id'];

        if ($model->load(Yii::$app->request->post()) && ($model->is_created = true) && $model->save()) {

            if ($model->create_agreement_from == Agreement::CREATE_AGREEMENT_FROM_TEMPLATE) {
                $model->essence->save();
            }

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, ($newRecord ? LogEvent::LOG_EVENT_CREATE : LogEvent::LOG_EVENT_UPDATE), function ($model) {
                return (boolean)$model->updateAttributes([
                    'status_id' => AgreementStatus::STATUS_CREATED,
                    'status_updated_at' => time(),
                    'status_author_id' => Yii::$app->user->id
                ]);
            });

            Yii::$app->session->setFlash('success', 'Договор ' . ($newRecord ? 'добавлен' : 'обновлен'));

            if ($this->isScanPreview) {
                Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
            }

            return $this->redirect($returnTo);
        }

        return $this->render('partial/_createPageForm', [
            'model' => $model,
            'fixedContractor' => (bool)$fixed_contractor,
            'fixedType' => (bool)$fixed_type,
            'fixedTemplate' => (bool)$fixed_template,
            'newRecord' => $newRecord,
            'disableAllFields' => false,
            'returnTo' => $returnTo
        ]);
    }


}
