<?php
namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use Yii;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use frontend\modules\documents\models\SpecificDocument;
use frontend\modules\documents\models\SpecificDocumentSearch;
use frontend\components\StatisticPeriod;
use yii\web\UploadedFile;
use common\models\file;
use common\components\filters\AccessControl;
use yii\base\ErrorException;


/**
 * Specific documents controller
 */
class SpecificDocumentController extends DocumentBaseController
{
    public $layout = 'my-documents';
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_SPECIFIC;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'file-get',
                            'create',
                            'update',
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['authenticated'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();

        if ($this->allowFileUpload) {
            $documentModel = Documents::getBaseModel($this->typeDocument);

            $actions = array_merge($actions, [
                'file-get' => [
                    'class' => file\actions\GetFileAction::className(),
                    'model' => file\File::className(),
                    'idParam' => 'file-id',
                    'fileNameField' => 'filename_full',
                    'folderPath' => function($model) {
                        return $model->getUploadPath();
                    },
                    'findModel' => [$this, 'findFileModel'],
                ],
                'file-list' => null,
                'file-delete' => null,
                'file-upload' => null,
                'scan-bind' => null,
                'scan-list' => null,
            ]);
        }

        return $actions;
    }

    public function actionIndex($type = null)
    {
        $searchModel = new SpecificDocumentSearch();
        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            null //StatisticPeriod::getSessionPeriod()
        );

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new SpecificDocument model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!\Yii::$app->user->identity->company->uploadAllowed()) {
            $link = Html::a('перейдите на платный тариф', ['/subscribe']);
            \Yii::$app->session->setFlash('error', "У вас закончилось место на диске для хранения сканов и документов. Использовано более 1 ГБ. Освободите место или $link");

            return $this->redirect(\Yii::$app->request->referrer);
        }

        $model = new SpecificDocument([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        $model->setScenario('create');

        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_author_id = Yii::$app->user->id;
        $model->load(Yii::$app->request->post());
        $model->file = UploadedFile::getInstance($model, 'file');
        if ($model->validate()) {

            $model->save();

            $file = new file\File();
            $file->owner_model = $model::className();
            $file->owner_id = $model->id;
            $file->uploadDirectoryPath = 'documents' . DIRECTORY_SEPARATOR . 'specific';
            $file->file = $model->file;

            if (!$file->save()) {
                throw new ErrorException('Не удалось сохранить файл.');
            } else {
                Yii::$app->session->setFlash('success', 'Документ добавлен');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return mixed|string|\yii\web\Response
     * @throws ErrorException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');

            $model->save();
            if($model->file)
            {
                /** @var File $file */
                $file = file\File::find()->where(['owner_id'=>$model->id])->one();
                if(!empty($file))
                {
                    $file->delete();
                }
                $file = new file\File();
                $file->owner_model = $model::className();
                $file->owner_id = $model->id;
                $file->uploadDirectoryPath = 'documents' . DIRECTORY_SEPARATOR . 'specific';
                $file->file = $model->file;

                if (!$file->save()) {
                    throw new ErrorException('Не удалось сохранить файл.');
                }
            }
            Yii::$app->session->setFlash('success', 'Документ изменен');

            return $this->redirect(['index']);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                    'model' => $model,
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Документ удален');

        return $this->redirect('index');
    }

    /**
     * @param integer $id
     * @return SpecificDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id, $action = null)
    {
        $model = SpecificDocument::find()->andWhere([
            'id' => $id,
        ])->andWhere([
            'or',
            ['document_author_id' => \Yii::$app->user->identity->id],
            ['company_id' => \Yii::$app->user->identity->company->id],
        ])->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     * @return File
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findFileModel($id)
    {
        $model = $this->findModel($id)->savedFile;

        if ($model === null) {
            throw new NotFoundHttpException('File not found.');
        }

        return $model;
    }
}
