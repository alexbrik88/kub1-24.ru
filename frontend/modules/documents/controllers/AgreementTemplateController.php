<?php
namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use common\models\AgreementType;
use Yii;
use common\components\filters\AccessControl;
use frontend\components\FrontendController;
use frontend\modules\documents\models\AgreementTemplateSearch;
use frontend\rbac\permissions;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use common\models\Company;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;

/**
 * Specific documents controller
 */
class AgreementTemplateController extends FrontendController
{
    public static $useLog = true;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::VIEW,
                        ],
                        'roleParams' => function () {
                            $model = AgreementTemplate::findOne(Yii::$app->request->get('id'));

                            return [
                                'model' => $model,
                                'ioType' => $model ? $model->type : Yii::$app->request->get('type'),
                            ];
                        },
                    ],
                    [
                        'actions' => ['copy'],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::CREATE,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                        'matchCallback' => function ($rule, $action) {
                            $model = AgreementTemplate::findOne(Yii::$app->request->get('id'));

                            return Yii::$app->getUser()->can(permissions\AgreementTemplate::VIEW, [
                                'ioType' => $model ? $model->type : Yii::$app->request->get('type'),
                                'model' => $model,
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'update',
                            'comment-internal',
                            'update-status',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::UPDATE,
                        ],
                        'roleParams' => function () {
                            $model = AgreementTemplate::findOne(Yii::$app->request->get('id'));

                            return [
                                'ioType' => $model ? $model->type : Yii::$app->request->get('type'),
                                'model' => $model,
                            ];
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [
                            permissions\AgreementTemplate::DELETE,
                        ],
                        'roleParams' => function () {
                            $model = AgreementTemplate::findOne(Yii::$app->request->get('id'));

                            return [
                                'ioType' => $model ? $model->type : Yii::$app->request->get('type'),
                                'model' => $model,
                            ];
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * @return string
     */
    public function actionIndex($type = null)
    {
        $searchModel = new AgreementTemplateSearch();
        $searchModel->type = $type;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'type' => $type
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate($type = null)
    {
        $model = new AgreementTemplate();
        $model->type = $type;

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->company_id = Yii::$app->user->identity->company->id;
            $model->other_patterns = json_encode(Yii::$app->request->post('pattern', null));

            if ($model->validate()) {

                $model->save();

                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);

                Yii::$app->session->setFlash('success', 'Шаблон документа добавлен');

                return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);
            }
        } else{
            $model->document_type_id = AgreementType::TYPE_AGREEMENT;
        }

        return $this->render('create', [
            'model' => $model,
            'type' => $type
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id, $type = null)
    {
        $model = $this->findModel($id);

        // Save
        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());
            $model->company_id = Yii::$app->user->identity->company->id;
            $model->other_patterns = json_encode(Yii::$app->request->post('pattern', null));

            if ($model->validate()) {

                $model->save();

                Yii::$app->session->setFlash('success', 'Шаблон документа обновлён');

                return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'type' => $type
        ]);
    }

    /**
     * @param $id
     * @param $status
     * @return string|\yii\web\Response
     */
    public function actionUpdateStatus($id, $status)
    {
        $model = $this->findModel($id);

        if ('active' == $status)
            $model->status = AgreementTemplate::STATUS_ACTIVE;
        elseif ('archived' == $status)
            $model->status = AgreementTemplate::STATUS_ARCHIVED;
        else
            return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);

        if ($model->save() && self::$useLog)
        {
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
        }

        Yii::$app->session->setFlash('success', 'Статус документа обновлён');

        return $this->redirect(['view', 'id' => $model->id, 'type' => $model->type]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $model->status = AgreementTemplate::STATUS_DELETED;

        if ($model->save() && self::$useLog)
        {
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE);
        }

        Yii::$app->session->setFlash('success', 'Шаблон документа удалён');

        return $this->redirect(['index', 'type' => $model->type]);
    }

    public function actionView($id, $type)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'backUrl' => ['index', 'type' => $model->type]
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id, $type)
    {
        $cloneModel = $this->cloneModel($id);

        if ($cloneModel->save(false)) {

            if (self::$useLog)
            {
                LogHelper::log($cloneModel, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);
            }

            Yii::$app->session->setFlash('success', 'Шаблон скопирован');

            return $this->redirect(['view', 'id' => $cloneModel->id, 'type' => $type]);

        } else {
            return $this->redirect(['view', 'id' => $id, 'type' => $type]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var AgreementTemplate $model */
        $model = $this->findModel($id);

        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);

        $model = $this->findModel($id);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $id
     * @return AgreementTemplate
     * @throws NotFoundHttpException
     */
    protected function cloneModel($id)
    {
        $model = $this->findModel($id);
        $company_id = Yii::$app->user->identity->company->id;
        $clone = new AgreementTemplate;
        $clone->setAttributes($model->attributes);
        $clone->isNewRecord = true;
        $clone->id = null;
        $clone->document_number = $clone->getNextDocumentNumber($company_id, $clone->document_date);
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->status = AgreementTemplate::STATUS_ACTIVE;
        $clone->created_by = Yii::$app->user->identity->id;
        $clone->company_id = $company_id;
        $clone->documents_created = 0;

        return $clone;
    }

    /**
     * @param $id
     * @return AgreementTemplate null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = AgreementTemplate::findOne([
            'id' => $id,
            'company_id' => ArrayHelper::getValue(Yii::$app->user->identity, ['company', 'id']),
        ]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
    }
}
