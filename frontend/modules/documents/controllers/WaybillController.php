<?php
namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\pdf\PdfRenderer;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderWaybill;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Waybill;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\WaybillStatus;
use common\models\employee\Employee;
use common\models\file;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Waybill controller
 */
class WaybillController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_WAYBILL;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type', null),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE)
                            && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE)
                            && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id)
    {
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }


    public function actionUpdate($type, $id)
    {
        /** @var Waybill $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->updateAndSave(
                    Yii::$app->request->post('OrderWaybill'),
            )) {
                LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                Yii::$app->session->setFlash('success', 'ТТН изменена');

                return $this->redirect([
                    'view',
                    'type' => $type,
                    'id' => $model->id,
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Waybill $model */
        $model = $this->loadModel($id, $type);

        $status = (int) Yii::$app->request->getBodyParam('status');

        $this->_updateStatus($model, $status);

        switch ($model->status_out_id) {
            case WaybillStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'ТТН передана');
                break;
            case WaybillStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'ТТН подписана');
                break;
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param Waybill $model
     * @param $status
     * @throws \yii\base\Exception
     */
    private function _updateStatus(Waybill $model, $status)
    {
        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS,
                function (Waybill $model) {
                    return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                });
        }
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /** @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        if ($invoice->canAddWaybill) {
            if ($invoice->createWaybill(null, $documentNumber)) {
                $orderDocument = $invoice->orderDocument;
                if ($orderDocument) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                }
                Yii::$app->session->setFlash('success', 'ТТН создана.');

                return $this->redirect([
                    'view',
                    'type' => $invoice->waybill->type,
                    'id' => $invoice->waybill->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', $invoice->lastCreatedDocumentError ?: 'При создании ТТН возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', $invoice->lastCreatedDocumentError ?: 'Нельзя создать ТТН.');
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        $company = Yii::$app->user->identity->company;
        $idArray = [];
        $documentDate = !empty(Yii::$app->request->post('Waybill')['document_date']) ?
            Yii::$app->request->post('Waybill')['document_date'] : null;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            /* @var $invoice Invoice */
            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddWaybill) {
                    $orderDocument = $invoice->orderDocument;
                    if ($orderDocument) {
                        $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                        $orderDocument->save(true, ['status_id']);
                    }
                    $invoice->createWaybill($documentDate);
                }
            }
        } else {
            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    ['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddWaybill()) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                    $orderDocument->invoice->createWaybill($documentDate);
                }
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }
        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }


    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if (LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT)) {
            Yii::$app->session->setFlash('success', 'ТТН удалена');
        } else {
            Yii::$app->session->setFlash('error', 'ТТН не удалось удалить');
        }

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $Waybills = Yii::$app->request->post('Waybill');
        foreach ($Waybills as $id => $Waybill) {
            if ($Waybill['checked']) {
                $model = $this->loadModel($id, $type);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'waybill_id' => $id,
        ]);
    }


    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var Waybill $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = Waybill::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
                // $model->email_messages += $saved[0];
                // $model->save(true, ['email_messages']);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        $waybills = Yii::$app->request->post('Waybill');
        $send = 0;
        foreach ($waybills as $id => $waybill) {
            if ($waybill['checked']) {
                /* @var $model Waybill */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }
                if ($model->uid == null) {
                    $model->uid = Waybill::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = null;
                if ($model->invoice->contractor->director_email) {
                    $sendTo = $model->invoice->contractor->director_email;
                } elseif ($model->invoice->contractor->chief_accountant_email) {
                    $sendTo = $model->invoice->contractor->chief_accountant_email;
                } elseif ($model->invoice->contractor->contact_email) {
                    $sendTo = $model->invoice->contractor->contact_email;
                }
                if ($sendTo) {
                    /** @var Employee $sender */
                    $sender = \Yii::$app->user->identity;
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'Товарно-транспортная накладная',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];

                    //$oneCFilePath = $model->generateOneCFile();

                    \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (\Yii::$app->mailer->compose([
                        'html' => 'system/documents/waybill-out/html',
                        'text' => 'system/documents/waybill-out/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['info'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('Товарно-транспортная накладная No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(Waybill::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        //->attach($oneCFilePath, [
                        //    'fileName' => $model->getOneCFileName(),
                        //    'mime' => 'application/xml',
                        //])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send += 1;
                        InvoiceSendForm::setWaybillSendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            \common\models\company\CompanyFirstEvent::checkEvent($company, 24);
        }
        $message = 'Отправлено ' . $send . ' из ' . count($waybills) . ' писем.';
        if ($send == count($waybills)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки товарно-транспортных накладных, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType    = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_waybill' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'транспортную накладную',
            'subTitle' => 'Выберите счет, к которому создать ТТН',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_WAYBILL,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $waybillID = explode(',', $multiple);
        /* @var $model Waybill */
        $model = $this->loadModel(current($waybillID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'addStamp' => ($actionType === 'pdf' && $model->invoice->company->pdf_signed) ? true : false,
            'multiple' => $waybillID,
        ]);
    }
}
