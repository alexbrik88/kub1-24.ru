<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\image\EasyThumbnailImage;
use common\components\pdf\PdfRenderer;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\PackingListStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Packing controller
 */
class PackingListController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_PACKING_LIST;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['get-xls', 'generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'many-create',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-update-original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'ioType' => Yii::$app->request->get('type'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['send-many-in-one'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id)
    {
        //if ($type == Documents::IO_TYPE_IN) {
        //    return $this->actionViewOld($type, $id);
        //}
        /** @var PackingList $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }


    public function actionUpdate($type, $id)
    {
        /** @var PackingList $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (Yii::$app->request->post('OrderPackingList') != null) {
                foreach (Yii::$app->request->post('OrderPackingList') as $key => $value) {
                    $opl = OrderPackingList::findOne(['order_id' => $key, 'packing_list_id' => $model->id]);
                    if ($value['quantity'] > 0 && $value['status'] == 'active') {
                        if ($opl != null) {
                            if ($opl->getAvailableQuantity() >= $value['quantity'] && $value['quantity'] > 0) {
                                OrderPackingList::setQuantity($key, $model->id, $value['quantity']);
                            }
                        } else {

                            if ($value['quantity'] != 0 && $value['quantity'] <= OrderPackingList::compareWithOrderByQuantity(Order::findOne(['id' => $key]))) {
                                OrderPackingList::createFromOrder($model->id, Order::findOne(['id' => $key]))->save();
                            }

                        }
                    } elseif ($value['status'] == 'deleted' && OrderPackingList::ifExist($key, $model->id)) {
                        OrderPackingList::deleteAll(['order_id' => $key, 'packing_list_id' => $model->id]);
                    }

                }

                $isSaved = Yii::$app->db->transaction(function ($db) use ($model, $type, $useContractor) {
                    $model->document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                    $model->ordinal_document_number = $model->document_number;
                    $model->orders_sum = $model->totalAmountWithNds;
                    $model->order_nds = $model->totalNds;
                    if (!$model->save()) {
                        foreach ($model->getErrors() as $error) {
                            Yii::$app->session->setFlash('error', $error);
                        }
                    } else {
                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                        Yii::$app->session->setFlash('success', 'ТН изменена');
                    }
                    if ($model->errors) {
                        return $this->refresh();
                    }

                    return true;
                });

                if ($isSaved) {
                    // refresh orders sum
                    $orders_sum = 0;
                    $order_nds = 0;
                    foreach ($model->getOrderPackingLists()->all() as $key => $value) {
                        /** @var $value OrderPackingList */
                        $orders_sum += $value->amountWithNds;
                        $order_nds += $value->amountNds;
                    }
                    $model->updateAttributes([
                        'orders_sum' => round($orders_sum),
                        'order_nds' => round($order_nds)
                    ]);
                    $model->invoice->updateDocumentsPaid();
                    PlanCashContractor::updateFlowByInvoice($model->invoice, Documents::DOCUMENT_PACKING_LIST);
                }

                if ($this->isScanPreview) {
                    Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                    return $this->redirect(Url::previous('lastUploadManagerPage'));
                }

                return $this->redirect([
                    'view',
                    'type' => $type,
                    'id' => $model->id,
                ]);
            } else {
                $model->delete();
                return $this->render('index');
            }

            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }


    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionViewOld($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->post('OrderPackingList') != null) {
                foreach (Yii::$app->request->post('OrderPackingList') as $key => $value) {
                    $opl = OrderPackingList::findOne(['order_id' => $key, 'packing_list_id' => $model->id]);
                    if ($value['quantity'] > 0 && $value['status'] == 'active') {
                        if ($opl != null) {
                            if ($opl->getAvailableQuantity() >= $value['quantity'] && $value['quantity'] > 0) {
                                OrderPackingList::setQuantity($key, $model->id, $value['quantity']);
                            }
                        } else {

                            if ($value['quantity'] != 0 && $value['quantity'] <= OrderPackingList::compareWithOrderByQuantity(Order::findOne(['id' => $key]))) {
                                OrderPackingList::createFromOrder($model->id, Order::findOne(['id' => $key]))->save();
                            }

                        }
                    } elseif ($value['status'] == 'deleted' && OrderPackingList::ifExist($key, $model->id)) {
                        OrderPackingList::deleteAll(['order_id' => $key, 'packing_list_id' => $model->id]);
                    }

                }
                Yii::$app->db->transaction(function ($db) use ($model) {
                    $model->document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                    $model->ordinal_document_number = $model->document_number;
                    $model->orders_sum = $model->totalAmountWithNds;
                    if (!$model->save()) {
                        foreach ($model->getErrors() as $error) {
                            Yii::$app->session->setFlash('error', $error);
                        }
                    } else {
                        Yii::$app->session->setFlash('success', 'ТН изменена');
                    }
                });
            } else {
                $model->delete();
            }

            return $this->refresh();
        }

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('view-old', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid, $view = 'print')
    {
        /* @var Invoice $model */
        $model = PackingList::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $view, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'packing_list_id' => $id,
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'documentFormat' => 'A4-L',
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $packingListsID = explode(',', $multiple);
        /* @var $model PackingList */
        $model = $this->loadModel(current($packingListsID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'addStamp' => ($actionType === 'pdf' && $model->invoice->company->pdf_signed) ? true : false,
            'multiple' => $packingListsID,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var PackingList $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = PackingList::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
                // $model->email_messages += $saved[0];
                // $model->save(true, ['email_messages']);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        $packingLists = Yii::$app->request->post('PackingList');
        /** @var Employee $sender */
        $sender = \Yii::$app->user->identity;
        $send = 0;
        $contractorPackingLists = [];
        foreach ($packingLists as $id => $packingList) {
            if ($packingList['checked']) {
                /* @var $model PackingList */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }
                $contractorPackingLists[$model->invoice->contractor_id][] = $model->id;
            }
        }
        foreach ($contractorPackingLists as $contractorID => $packingLists) {
            if (count($packingLists) > 1) {
                $contractor = Contractor::findOne($contractorID);
                $sendTo = $contractor->someEmail;
                if ($sendTo) {
                    if (PackingList::manySend($sender->currentEmployeeCompany, $sendTo, $type, $packingLists)) {
                        $send++;
                    }
                } else {
                    $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $contractor->type, 'id' => $contractor->id
                    ]));
                }
            } else {
                $packingListID = current($packingLists);
                /* @var $model PackingList */
                $model = $this->loadModel($packingListID, $type);
                if ($model->uid == null) {
                    $model->uid = PackingList::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = $model->invoice->contractor->someEmail;
                if ($sendTo) {
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'Товарная накладная',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];
                    $oneCFilePath = $model->generateOneCFile();

                    // render pdf file
                    $fileName = \Yii::getAlias('@frontend/runtime/pdf-cache/') . 'out-packing-list-' . $model->id;
                    $renderer = PackingList::getRenderer($fileName, $model);
                    $renderer->output(false);

                    \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (\Yii::$app->mailer->compose([
                        'html' => 'system/documents/packing-list-out/html',
                        'text' => 'system/documents/packing-list-out/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('Товарная накладная No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(PackingList::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        ->attach($oneCFilePath, [
                            'fileName' => $model->getOneCFileName(),
                            'mime' => 'application/xml',
                        ])
                        ->attach($oneCFilePath, [
                            'fileName' => $model->getOneCFileName(),
                            'mime' => 'application/xml',
                        ])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send++;
                        InvoiceSendForm::setPackingListSendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            CompanyFirstEvent::checkEvent($company, 24);
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorPackingLists) . ' писем.';
        if ($send == count($contractorPackingLists)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки товарных накладных, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var PackingList $model */
        $model = $this->loadModel($id, $type);

        $status = (int)Yii::$app->request->getBodyParam('status');

        $this->_updateStatus($model, $status);

        switch ($model->status_out_id) {
            case PackingListStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'ТН передана');
                break;
            case PackingListStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'ТН подписана');
                break;
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param PackingList $model
     * @param $status
     * @throws \yii\base\Exception
     */
    private function _updateStatus(PackingList $model, $status)
    {
        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS,
                function (PackingList $model) {
                    return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                });
        }
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        $documentDate = Yii::$app->request->get('documentDate');
        $isFromInvoiceForm = Yii::$app->request->get('isFromInvoiceForm');
        if (empty($documentDate) && $invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = DateHelper::format($invoice->document_date, 'd.m.Y', 'Y-m-d');
            $documentNumber = $invoice->document_number;

        }
        if ($invoice->canAddPackingList) {
            if (!$invoice->need_packing_list) {
                if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                    $invoice->need_packing_list = true;
                    $invoice->save(true, ['need_packing_list']);
                } else {
                    throw new BadRequestHttpException();
                }
            }
            if ($invoice->createPackingList($documentDate, $documentNumber)) {
                $orderDocument = $invoice->orderDocument;
                if ($orderDocument) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                }

                Yii::$app->session->setFlash('success', 'ТН создана.');

                //// 20-204.2: Product::updateGrowingPriceForBuy($invoice);

                if ($this->isScanPreview)
                {
                    if (Yii::$app->request->get('returnToManager')) {
                        Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                        return $this->redirect(Url::previous('lastUploadManagerPage'));
                    }

                    return $this->redirect([
                        '/documents/packing-list/update',
                        'type' => $invoice->packingList->type,
                        'id' => $invoice->packingList->id,
                        'mode' => 'previewScan',
                        'files' => Yii::$app->request->get('files')
                    ]);
                }

                if ($invoice->packingList->type == Documents::IO_TYPE_IN && !$isFromInvoiceForm) {
                    return $this->redirect([
                        '/documents/packing-list/update',
                        'type' => $invoice->packingList->type,
                        'id' => $invoice->packingList->id
                    ]);
                }

                return $this->redirect([
                    'view',
                    'type' => $invoice->packingList->type,
                    'id' => $invoice->packingList->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'При создании ТН возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать ТН.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }


    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws BadRequestHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];
        $documentDate = !empty(Yii::$app->request->post('PackingList')['document_date']) ?
            Yii::$app->request->post('PackingList')['document_date'] : null;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $createdPL = 0;
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            /* @var $invoice Invoice */
            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddPackingList) {
                    if (!$invoice->need_packing_list) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_packing_list = true;
                            $invoice->save(true, ['need_packing_list']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                    $orderDocument = $invoice->orderDocument;
                    if ($orderDocument) {
                        $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                        $orderDocument->save(true, ['status_id']);
                    }
                    if ($invoice->createPackingList($documentDate))
                        $createdPL++;
                }
            }
        } else {
            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    //['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();

            $noInvoice = false;
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddPackingList()) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                    if ($orderDocument->invoice->createPackingList($documentDate))
                        $createdPL++;
                }
                if (!$orderDocument->invoice) {
                    $noInvoice = true;
                }
            }

            if ($createdPL > 0) {
                Yii::$app->session->setFlash('success', ($createdPL == 1) ? 'ТН создана' : 'ТН созданы');
            } else {
                Yii::$app->session->setFlash('error', ($noInvoice) ? 'Сначала создайте Счет' : 'Не удалось создать ТН');
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }


    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if (LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT)) {
            Yii::$app->session->setFlash('success', 'ТН удалена');
        } else {
            Yii::$app->session->setFlash('error', 'ТН не удалось удалить');
        }

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $packingLists = Yii::$app->request->post('PackingList');
        foreach ($packingLists as $id => $packingList) {
            if ($packingList['checked']) {
                $model = $this->loadModel($id, $type);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @return mixed
     */
    public function actionUpd()
    {
        $renderer = new PdfRenderer([
            'view' => 'upd',
            'params' => [],

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => 'upd',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => ArrayHelper::getValue([], 'documentFormat', 'A4-P'),
        ]);

        return $renderer->renderHtml();
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $period = StatisticPeriod::getSessionPeriod();
        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $packingLists = $searchModelOut->search(Yii::$app->request->queryParams, $period, true);

        PackingList::generateXlsTable($period, $packingLists);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionAddStamp($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        return $model->updateAttributes(['add_stamp' => (boolean)Yii::$app->request->post('add_stamp')]);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType    = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_packing_list' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'товарную накладную',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_PACKING_LIST,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $ioType = Yii::$app->request->get('type', Documents::IO_TYPE_OUT);
        $period = StatisticPeriod::getSessionPeriod();
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $ioType);
        $packingLists = $searchModel->search(Yii::$app->request->queryParams, $period, true, true);

        PackingList::generateXlsTable($period, $packingLists);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateStatus($type, $status)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('PackingList', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model PackingList */
                    $model = $this->loadModel($id, $type);

                    if ($status !== null && $status != $model->status_out_id) {
                        $model->status_out_id = $status;

                        LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (PackingList $model) {
                            return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                        });
                    }

                    switch ($status) {
                        case PackingListStatus::STATUS_SEND:
                            Yii::$app->session->setFlash('success', 'ТН переданы');
                            break;
                        case PackingListStatus::STATUS_RECEIVED:
                            Yii::$app->session->setFlash('success', 'ТН подписаны');
                            break;
                    }
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateOriginal($type, $val)
    {
        if ($type != Documents::IO_TYPE_IN) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('PackingList', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model PackingList */
                    $model = $this->loadModel($id, $type);
                    $model->is_original = intval($val) ? true : false;
                    $model->save(false, ['is_original', 'is_original_updated_at']);
                    Yii::$app->session->setFlash('success', 'ТН сохранены');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type file type
     * @param $uid document uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getByUid($uid);

        switch ($type) {
            case 'pdf':
            default:
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean) $model->company->pdf_signed,
                ], 'A4-L');
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Act
     * @throws NotFoundHttpException
     */
    public static function getByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = empty($uid) ? null : PackingList::find()
            ->joinWith('invoice.company')
            ->andWhere([
                'packing_list.uid' => $uid,
                'packing_list.type' => $type,
                'invoice.is_deleted' => false,
                'company.blocked' => false,
            ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }
}
