<?php

namespace frontend\modules\documents\controllers;

use frontend\components\FrontendController;
use common\models\document\ScanDocument;
use frontend\modules\documents\models\ScanDocumentSearch;
use frontend\modules\documents\models\UploadManagerFileSearch;
use frontend\rbac\UserRole;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * ScanDocumentController implements the CRUD actions for ScanDocument model.
 */
class ScanDocumentController extends FrontendController
{
    public $layout = 'my-documents';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'index-free',
                            'create',
                            'update',
                            'delete',
                            'file',
                            'print'
                        ],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                            UserRole::ROLE_SUPERVISOR,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ScanDocument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScanDocumentSearch([
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ScanDocument models.
     * @return mixed
     */
    public function actionIndexFree()
    {
        $fromUploads = Yii::$app->request->post('uploads');
        $fromDirectory = Yii::$app->request->post('directory');
        $fromAllDocs = Yii::$app->request->post('all');

        if (!$fromUploads && !$fromDirectory && !$fromAllDocs)
            $fromUploads = 1;

        $searchModel = new ScanDocumentSearch([
            'company_id' => Yii::$app->user->identity->company->id,
            'free' => true,
            'fromUploads' => $fromUploads,
            'fromDirectory' => $fromDirectory
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('index-free', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'fromUploads' => $fromUploads,
            'fromDirectory' => $fromDirectory,
            'fromAllDocs' => $fromAllDocs,
        ]);
    }

    /**
     * Creates a new ScanDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ScanDocument([
            'scenario' => ScanDocument::SCENARIO_CREATE,
            'company_id' => Yii::$app->user->identity->company->id,
            'employee_id' => Yii::$app->user->id,
        ]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Скан документа загружен');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось загрузить скан документа.');
            }

            return $this->redirect(Yii::$app->request->referrer ? : ['index']);
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['index']);
    }

    /**
     * Updates an existing ScanDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->isPost || Yii::$app->request->isAjax) {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(Yii::$app->request->referrer ? : ['index']);
            } else {
                return $this->renderAjax('_form', [
                    'model' => $model,
                ]);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['index']);
    }

    /**
     * Deletes an existing ScanDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing ScanDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionFile($id, $name = null)
    {
        $model = $this->findModel($id);
        $file = $model->file ? $model->file->getPath() : null;

        if ($file && is_file($file)) {
            $options = ['inline' => true];
            if ($model->file->ext == 'pdf') {
                $options['mimeType'] = 'application/pdf';
            }

            return \Yii::$app->response->sendFile($file, $model->file->filename_full, $options);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $file = $model->file ? $model->file->getPath() : null;

        if ($file && is_file($file)) {

            $mpdf = new \mPDF();
            $mpdf->SetImportUse();
            $pagecount = $mpdf->SetSourceFile($file);
            for ($i=1; $i<=$pagecount; $i++) {
                $import_page = $mpdf->ImportPage();
                $mpdf->UseTemplate($import_page);

                if ($i < $pagecount)
                    $mpdf->AddPage();
            }
            $mpdf->SetJS('this.print();');
            $mpdf->Output();

        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the ScanDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ScanDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $company_id = Yii::$app->user->identity->company->id;

        if (($model = ScanDocument::findOne(['id' => $id, 'company_id' => $company_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
