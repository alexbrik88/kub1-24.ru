<?php

namespace frontend\modules\documents\controllers;

use backend\models\Bank;
use backend\models\Prompt;
use backend\models\PromptHelper;
use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\ImageHelper;
use common\components\phpWord\PhpWordInvoice;
use common\components\TextHelper;
use common\models\address\Country;
use common\models\Agreement;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFactory;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\CompanyHelper;
use common\models\Contractor;
use common\models\ContractorAutoProject;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\AbstractDocument;
use common\models\document\Autoinvoice;
use common\models\document\EmailSignature;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceContractEssence;
use common\models\document\InvoiceContractorSignature;
use common\models\document\InvoiceEmailText;
use common\models\document\InvoiceEssence;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\NdsViewType;
use common\models\document\OperationType;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderHelper;
use common\models\document\PaymentOrder;
use common\models\document\PaymentOrderInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PaymentOrderStatus;
use common\models\document\TaxpayersStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\out\OutInvoice;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\product\Store;
use common\models\project\Project;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectEstimate;
use common\models\prompt\PageType;
use common\models\service\SubscribeTariffGroup;
use common\models\TaxRate;
use DateTime;
use frontend\components\filters\CompleteRegistrationFilter;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\InvoiceLandingForm;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use frontend\modules\documents\forms\InvoiceFlowForm;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\AutoinvoiceForm;
use frontend\modules\documents\models\AutoinvoiceSearch;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\db\Connection;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Invoice controller
 */
class InvoiceController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $type;

    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_INVOICE;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'upd',
                            'index',
                            'get-invoices',
                            'get-invoice',
                            'add-modal-contractor-fast',
                            'add-modal-details-address',
                            'add-modal-logo',
                            'validate-company-modal',
                            'first-create-2',
                            'generate-xls',
                            'get-xls',
                            'contractor-product',
                            'update-checking-accountant',
                            'create-checking-accountant',
                        ],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'add-modal-product',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Product::CREATE);
                        },
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                                'type' => Yii::$app->request->post('documentType'),
                            ]);
                        },
                    ],
                    [
                        'actions' => [
                            'index-auto',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Documents::IO_TYPE_OUT,
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create', 'create-from-order', 'create-from-agreement', 'update-from-order', 'autoinvoice', 'first-create', 'copy-invoice',
                            'get-contractor-expenditure-item', 'copy-auto', 'many-create'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Invoice::CREATE, [
                                'ioType' => Yii::$app->request->getQueryParam('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['view', 'file-get', 'file-list', 'scan-list', 'change-contractor-signature', 'files'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModel(null, null, true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['view-auto', 'print-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModelAuto(null, true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['file-upload', 'file-delete', 'scan-bind'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['document-print', 'docx', 'document-png'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'model' => $action->controller->loadModel(null, Yii::$app->request->getQueryParam('type'), true),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-document-print'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->getQueryParam('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['update', 'update-status', 'get-contractor-expenditure-item',
                            'comment-internal', 'set-currency-rate'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => [
                            'update-contractor-fields',
                            'update-company-fields',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $isAuto = Yii::$app->request->get('isAuto');

                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $isAuto ? $action->controller->loadModelAuto() : $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => [
                            'many-update-company-fields',
                            'many-update-contractor-fields',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::STRICT_MODE
                        ],
                    ],
                    [
                        'actions' => ['update-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModelAuto(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['can-add-one-document'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE) && (
                                Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ]) || Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ])
                            );
                        },
                    ],
                    [
                        'actions' => ['send'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::VIEW,
                        ],
                        'roleParams' => function () {
                            return [
                                'model' => $this->loadModel(),
                            ];
                        },
                    ],
                    [
                        'actions' => ['update-status'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'model' => $action->controller->loadModel(), // throw exception if not found
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['create-payment-order',],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return (Yii::$app->getUser()->can(permissions\document\PaymentOrder::CREATE)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE));
                        },
                    ],
                    [
                        'actions' => ['add-flow'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Invoice::ADD_CASH_FLOW,
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::VIEW, [
                                'model' => Invoice::findOne(Yii::$app->request->get('id')),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['add-flow-form'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Invoice::ADD_CASH_FLOW,
                        ],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['delete-auto'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                    'model' => $action->controller->loadModelAuto(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'send-many-in-one', 'many-send-by-contractor'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->get('type'),
                            ];
                        },
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['get-many-send-message-panel'],
                        'allow' => true,
                        'roles' => [
                            permissions\document\Document::INDEX,
                        ],
                        'roleParams' => function () {
                            return [
                                'ioType' => Yii::$app->request->post('ioType', Yii::$app->request->post('typeDocument')),
                            ];
                        },
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['preview-invoice', 'register-user'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'out-view',
                            'out-view-login',
                            'out-view-reset',
                            'out-view-save',
                            'out-view-signup',
                            'download',
                        ],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'not-show',
                            'basis-document',
                            'field-project',
                            'field-project-estimate',
                            'amount-on-date',
                            'hide-scan-popup',
                            'run-scan-popup',
                            'get-contractor-info'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['unpaid', 'not-need-document'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => [
                            'set-responsible',
                            'mass-invoice-item',
                            'mass-set-responsible',
                            'mass-set-industry',
                            'mass-set-project'
                        ],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'set-currency-rate' => ['post'],
                    'preview-invoice' => ['post'],
                    'register-user' => ['post']
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'only' => ['preview-invoice', 'register-user'],
                'cors' => [
                    'Origin' => \Yii::$app->params['corsFilter'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['X-PJAX', 'X-PJAX-Container'],
                ],
            ],
            'completeRegistration' => CompleteRegistrationFilter::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
                'prepareModelAndData' => function ($action, $model, $company) {
                    $invoiceId = Yii::$app->request->post('invoiceId');
                    $contractorId = Yii::$app->request->post('contractorId');
                    $type = Yii::$app->request->post('documentType', Yii::$app->request->get('type'));

                    // Update contractor
                    if ($invoiceId && $contractorId) {
                        if ($updateContractor = Contractor::findOne(['id' => (int)$contractorId, 'company_id' => $company->id])) {
                            $model = $updateContractor;
                            $action->data['updateContractor'] = $updateContractor;
                            $action->data['invoice'] = $this->loadModel($invoiceId, $type);
                        }
                    }

                    $model->type = $type;

                    return $model;
                },
                'returnData' => function ($action, $model) {
                    if (isset($invoice) && $invoice->contractor && $invoice->contractor_id == $model->id) {
                        $invoice->setContractor($model);
                        $invoice->save(true, ['contractor_name_short', 'contractor_name_full', 'contractor_director_name',
                            'contractor_director_post_name', 'contractor_address_legal_full', 'contractor_bank_name',
                            'contractor_bank_city', 'contractor_bik', 'contractor_inn', 'contractor_kpp', 'contractor_ks', 'contractor_rs',]);
                    }

                    return [
                        'id' => $model->id,
                        'name' => $model->name,
                        'name-short' => $model->companyType ? $model->companyType->name_short : '',
                    ] + (isset($action->data['updateContractor']) ? ['update-contractor' => $model->id] : []);
                },
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['preview-invoice', 'register-user'])) {
            Yii::$app->controller->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $id
     * @return array|string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateCheckingAccountant($id)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        /* @var $model CheckingAccountant */
        $model = CheckingAccountant::findOne($id);

        if ($model == null) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        if ($model->load(Yii::$app->request->post()) && $model->_save(Company::findOne($company->id))) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $companyRs = $company->getCheckingAccountants()
                ->select(['bank_name', 'rs', 'id', 'bik'])
                ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
                ->orderBy(['type' => SORT_ASC])
                ->indexBy('rs')
                ->asArray()->all();
            foreach ($companyRs as $key => $rs) {
                $rsData[$rs['rs']] = $rs['bank_name'];
                $companyRs[$key]['bank_logo'] = null;
                /* @var $bank Bank */
                if (($bank = Bank::find()->andWhere(['bik' => $rs['bik']])->byStatus(!Bank::BLOCKED)->one()) !== null && $bank->little_logo_link) {
                    $companyRs[$key]['bank_logo'] = ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32]);
                }
            }
            $rsData["add-modal-rs"] = '[ + Добавить расчетный счет ]';

            return [
                'result' => true,
                'options' => $rsData,
                'companyRs' => $companyRs,
                'val' => $model->rs,
            ];
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('@frontend/views/company/form/modal_rs/_partial/_form', [
                'checkingAccountant' => $model,
                'company' => $company,
            ]);
        } else {
            return [
                'result' => true,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_partial/_form', [
                    'checkingAccountant' => $model,
                    'company' => $company,
                ]),
            ];
        }
    }

    /**
     * @return array|string
     */
    public function actionCreateCheckingAccountant()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->currentEmployeeCompany->company;
        /* @var $model CheckingAccountant */
        $model = new CheckingAccountant([
            'company_id' => $company->id,
            'currency_id' => Currency::DEFAULT_ID,
        ]);

        if (Yii::$app->request->post('ajax')) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->_save(Company::findOne($company->id))) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $companyRs = $company->getRubleAccounts()
                ->select(['bank_name', 'rs', 'id', 'bik'])
                ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
                ->orderBy(['type' => SORT_ASC])
                ->indexBy('rs')
                ->asArray()->all();
            foreach ($companyRs as $key => $rs) {
                $rsData[$rs['rs']] = $rs['bank_name'];
                $companyRs[$key]['bank_logo'] = null;
                /* @var $bank Bank */
                if (($bank = Bank::find()->andWhere(['bik' => $rs['bik']])->byStatus(!Bank::BLOCKED)->one()) !== null && $bank->little_logo_link) {
                    $companyRs[$key]['bank_logo'] = ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32]);
                }
            }
            $rsData["add-modal-rs"] = '[ + Добавить расчетный счет ]';

            return [
                'result' => true,
                'options' => $rsData,
                'companyRs' => $companyRs,
                'val' => $model->rs,
                'model' => $model->isForeign ? null : $model,
            ];
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('@frontend/views/company/form/modal_rs/_partial/_form', [
                'checkingAccountant' => $model,
                'company' => $company,
            ]);
        } else {
            return [
                'result' => true,
                'html' => $this->renderAjax('@frontend/views/company/form/modal_rs/_partial/_form', [
                    'checkingAccountant' => $model,
                    'company' => $company,
                ]),
            ];
        }
    }

    /**
     * Lists models.
     *
     * @param integer $type
     * @return mixed
     */
    public function actionIndex($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        $company = Yii::$app->user->identity->company;
        $searchModel = new InvoiceSearch([
            'type' => $type,
            'company_id' => $company->id,
        ]);
        $searchModel->populateRelation('company', $company);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, StatisticPeriod::getSessionPeriod());

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $prompt = null;
        if ($this->typeDocument === Documents::DOCUMENT_INVOICE) {
            $page_type = $type == Documents::IO_TYPE_IN ? PageType::TYPE_INVOICE_IN : PageType::TYPE_INVOICE_OUT;
            $prompt = (new PromptHelper())->getPrompts(Prompt::ACTIVE, Yii::$app->user->identity->company->company_type_id + 1, $page_type, true);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $type,
            'message' => new Message($this->typeDocument, $type),
            'prompt' => $prompt,
        ]);
    }

    /**
     * Lists models.
     *
     * @param integer $type
     * @return mixed
     */
    public function actionIndexAuto()
    {
        $company = Yii::$app->user->identity->company;

        $searchModel = new AutoinvoiceSearch([
            'company_id' => $company->id,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        return $this->render('index-auto', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'projectList' => [0 => 'Без проекта'] + $company->getProjects()->active()->select('name')->indexBy('id')->orderBy('name')->column(),
            'industryList' => [0 => 'Без направления'] + $company->getCompanyIndustries()->select('name')->indexBy('id')->orderBy('name')->column(),
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($type, $id)
    {
        if (($url = Yii::$app->request->referrer) && ($path = parse_url($url, PHP_URL_PATH))) {
            $excludedPaths = [
                '/documents/invoice/view',
                '/documents/invoice/update',
                '/documents/invoice/create',
                '/system/selling/create',
            ];
            if (!in_array(strtolower($path), $excludedPaths)) {
                Url::remember($url, 'invoiceViewReturnUrl');
            }
        } else {
            Yii::$app->getSession()->remove('invoiceViewReturnUrl');
        }
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $invoiceContractorSignature = $model->company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $model->company_id;
        }
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->contractor_id;
        $contractorTab = Yii::$app->request->getQueryParam('contractorTab', null);

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
            'useContractor' => $useContractor,
            'contractorTab' => $contractorTab,
            'invoiceContractorSignature' => $invoiceContractorSignature,
        ]);
    }

    /**
     * Displays a single InvoiceAuto model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @internal param null $contractorId
     */
    public function actionViewAuto($id)
    {
        /** @var InvoiceAuto $model */
        $model = $this->loadModelAuto($id);

        $status = Yii::$app->request->get('status');

        switch ($status) {
            case 'start':
                if ($model->contractor->status == Contractor::ACTIVE && $model->contractor->is_deleted == Contractor::NOT_DELETED) {
                    $model->auto->status = Autoinvoice::ACTIVE;
                    if ($model->auto->isAttributeChanged('status')) {
                        $model->auto->save(false, ['status', 'next_invoice_date']);
                        $model->invoice_status_updated_at = time();
                        $model->save(false, ['invoice_status_updated_at']);
                        LogHelper::log($model->auto, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
                        return $this->redirect(Url::current(['status' => null]));
                    }
                } else {
                    \Yii::$app->session->setFlash('error', "Нельзя запустить АвтоСчет для неактивного покупателя.");

                    return $this->redirect(['view-auto', 'id' => $model->id]);
                }
                break;
            case 'stop':
                $model->auto->status = Autoinvoice::CANCELED;
                if ($model->auto->isAttributeChanged('status')) {
                    $model->auto->save(false, ['status']);
                    $model->invoice_status_updated_at = time();
                    $model->save(false, ['invoice_status_updated_at']);
                    LogHelper::log($model->auto, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
                    return $this->redirect(Url::current(['status' => null]));
                }
                break;
        }

        return $this->render('view-auto', [
            'model' => $model,
            'useContractor' => (boolean) Yii::$app->request->getQueryParam('contractorId'),
        ]);
    }

    /**
     * @param null $id
     * @param null $ioType
     * @param bool $allowDeleted
     * @return Invoice|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    protected
    function loadModel($id = null, $ioType = null, $allowDeleted = false)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }
        if ($ioType === null) {
            $ioType = Yii::$app->request->getQueryParam('type');
        }

        /* @var Invoice $model */
        $model = parent::loadModel($id, $ioType);

        if (!$allowDeleted && $model->is_deleted) {
            throw new NotFoundHttpException('Документ удален.');
        }

        return $model;
    }

    /**
     * @param integer $id
     * @param bool $allowDeleted
     *
     * @return \yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function loadModelAuto($id = null, $allowDeleted = false)
    {
        if ($id === null) {
            $id = Yii::$app->request->getQueryParam('id');
        }

        $model = InvoiceAuto::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->andWhere(['id' => $id])
            ->one();

        if ($model === null || (!$allowDeleted && $model->is_deleted)) {
            throw new NotFoundHttpException();
        }

        return $model;
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf') ? Yii::$app->request->get('addStamp', $model->company->pdf_signed) : 0,
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'addStamp' => $model->company->pdf_signed
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionPrintAuto($id)
    {
        $model = $this->loadModelAuto($id, Documents::IO_TYPE_OUT);

        /* custom asset */

        return $this->_documentPrint($model, InvoicePrintAsset::className(), 'print', 'pdf-view', [
            'addStamp' => true,
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $invoicesID = explode(',', $multiple);
        /* @var $model Invoice */
        $model = $this->loadModel(current($invoicesID), $type);

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf') ? Yii::$app->request->get('addStamp', $model->company->pdf_signed) : false,
            'multiple' => $invoicesID,
        ]);
    }

    /**
     * @param $type
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionManyCreate($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $documentDate = !empty(Yii::$app->request->post('Invoice')['document_date'])
            ? Yii::$app->request->post('Invoice')['document_date']
            : date(DateHelper::FORMAT_DATE);

        $models = Yii::$app->request->post('OrderDocument');

        $idArray = [];
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        /* @var $orderDocuments OrderDocument[] */
        $orderDocuments = OrderDocument::find()
            ->andWhere(['and',
                ['is_deleted' => false],
                ['id' => $idArray],
                ['invoice_id' => null],
                ['company_id' => $company->id],
            ])->all();
        foreach ($orderDocuments as $orderDocument) {
            /* @var $user Employee */
            $user = Yii::$app->user->identity;
            $company = $user->company;
            /* @var Invoice $model */
            $model = $this->getNewModel($type);
            $model->scenario = 'insert';
            $model->type = $type;
            $model->document_date = $documentDate;
            $model->contractor_id = $orderDocument->contractor_id;
            $model->isAutoinvoice = 0;
            $model->company_id = $company->id;
            $model->populateRelation('company', $company);
            $model->document_number = (string)Invoice::getNextDocumentNumber($company->id, $type, null, $model->document_date);
            $payment_delay = $type == Documents::IO_TYPE_IN ? $orderDocument->contractor->seller_payment_delay : $orderDocument->contractor->customer_payment_delay;
            $model->agreement = $orderDocument->agreement;
            $model->populateRelation('contractor', $orderDocument->contractor);
            $model->nds_view_type_id = $company->hasNds() ?
                ($company->isNdsExclude ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) :
                Invoice::NDS_VIEW_WITHOUT;
            $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
            $model->invoice_expenditure_item_id = $type == Documents::IO_TYPE_IN
                ? InvoiceExpenditureItem::ITEM_SALARY
                : InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            Yii::$app->db->transaction(function (Connection $db) use ($model, $orderDocument, $company) {
                if ($this->action->id === 'first-create') {
                    $company->save(true, ['nds']);
                }
                foreach ($orderDocument->orderDocumentProducts as $orderDocumentProduct) {
                    $product = Product::getById($model->company_id, $orderDocumentProduct->product_id);
                    if ($product !== null) {
                        $order = OrderHelper::createOrderByProduct(
                            $product,
                            $model,
                            $orderDocumentProduct->quantity,
                            $orderDocumentProduct->selling_price_with_vat,
                            ($model->has_discount ? $orderDocumentProduct->discount : 0)
                        );
                        $order->product_title = $orderDocumentProduct->product_title;
                        $orderArray[] = $order;
                    }
                }
                array_walk($orderArray, function ($order, $key) {
                    $order->number = $key + 1;
                });
                $model->populateRelation('orders', $orderArray);

                $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orders, 'mass_gross'));
                $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orders, 'place_count'));
                if (!InvoiceHelper::save($model)) {
                    $db->transaction->rollBack();

                    return false;
                }
                $orderDocument->invoice_id = $model->id;
                $orderDocument->save(true, ['invoice_id']);

                return true;
            });
        }

        $url = Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index'];

        return $this->redirect($url);
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function actionPreviewInvoice()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $companyId = Yii::$app->request->post('OldCompany')['id'];
        if ($companyId !== null) {
            /* @var Company $company */
            $company = Company::findOne($companyId);
            if ($company !== null) {
                if (!$company->deleteCompanyFromLanding()) {
                    return ['result' => false, 'errors' => [
                        'system_error' => 'Ошибка при удалении старой компании',
                    ]];
                }
            }
        }
        $type = Documents::IO_TYPE_OUT;
        $company = Company::getBasicCompanyModel();
        $checkingAccountant = new CheckingAccountant();
        $contractor = Contractor::getBasicContractorModel();
        $invoice = $this->getNewModel($type);
        $invoice = $invoice->getBaseInvoice($company, $type);

        return Yii::$app->db->transaction(function (Connection $db) use ($company, $contractor, $checkingAccountant, $invoice) {
            if ($company->load(Yii::$app->request->post())) {
                $ndsViewTypeId = isset(Yii::$app->request->post('Invoice')['nds_view_type_id']) ? Yii::$app->request->post('Invoice')['nds_view_type_id'] : Invoice::NDS_VIEW_WITHOUT;
                if (is_array(($companyResult = $company->saveCompanyByLanding($checkingAccountant, $invoice, $contractor, $ndsViewTypeId)))) {
                    $db->transaction->rollBack();

                    return ['result' => false, 'errors' => $companyResult];
                }

                $taxationType = Yii::$app->request->post('Company')['taxation_type_id'];
                $companyTaxationType = new CompanyTaxationType([
                    'company_id' => $company->id,
                    'osno' => $taxationType == 1,
                    'usn' => $taxationType == 2 || $taxationType == 3,
                    'envd' => $taxationType == 4,
                    'psn' => false,
                ]);
                if (!$companyTaxationType->save()) {
                    $db->transaction->rollBack();

                    return ['result' => false];
                }
            }
            if (!($contractor->load(Yii::$app->request->post()) && $contractor->save())) {
                $db->transaction->rollBack();

                return ['result' => false, 'errors' => $contractor->getErrors()];
            }
            $productResult = Product::createProductByLanding(Yii::$app->request->post('orderArray'), $company, Yii::$app->request->post('Product')['product_type']);
            if ($productResult['result'] == false) {
                $db->transaction->rollBack();

                return $productResult;
            } else {
                unset($productResult['result']);
            }
            $invoice->invoice_status_author_id = 2;
            $invoice->contractor_id = $contractor->id;
            $invoice->company_id = $company->id;

            if ($invoice->load(Yii::$app->request->post())) {
                if (is_array(($invoiceResult = $invoice->createInvoiceByLanding($company, current($productResult))))) {
                    $db->transaction->rollBack();

                    return ['result' => false, 'errors' => $invoiceResult];
                }
            }

            return [
                'result' => true,
                'url' => Url::to(['/bill/invoice/' . $invoice->uid]),
                'companyId' => $company->id,
                'invoice' => [
                    'number' => $invoice->getFullNumber(),
                    'date' => DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    'sum' => TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2)
                ],
            ];
        });
    }

    /**
     * @return bool
     * @throws NotFoundHttpException
     */
    public function actionRegisterUser()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new InvoiceLandingForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->_save()) {
            return [
                'result' => true,
                'redirect' => $model->sendMessage ? 'schet-otpravlen' : 'login',
            ];
        }

        return [
            'result' => false,
            'errors' => $model->getErrors(),
        ];
    }

    /**
     * @param $uid
     * @param null $email
     * @param null $payment
     * @return string|\yii\console\Response|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionOutView($uid, $payment = null)
    {
        $this->layout = 'out-view';
        $email = Yii::$app->request->get('email');

        if ($uid == 'obrasetc-silki-na-schet') {
            $uid = YII_ENV_PROD ? 'fGkg0' : '7ubrQ';
        }
        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        // DEMO
        if ($isDemo = $invoice->from_demo_out_invoice) {
            $this->layout = 'out-view-demo';
        }

        if (!$invoice->out_view_count) {
            $invoice->updateAttributes(['out_view_count' => 1]);
        }

        if ($payment !== null) {
            $content = $this->renderPartial('1C_payment_order', [
                'model' => $invoice,
            ]);
            if (!$invoice->out_pay_count) {
                $invoice->updateAttributes(['out_pay_count' => 1]);
            }
            $name = $invoice->is_invoice_contract ? 'счет-договора' : 'счета';
            $fileName = 'Платежное_поручение_для_' . $name . '_№' .
                mb_ereg_replace("([^\w\s\d\-_])", '', $invoice->fullNumber) . '.txt';

            return Yii::$app->response->sendContentAsFile($content, $fileName);
        }

        if ($invoice->invoice_status_id == InvoiceStatus::STATUS_SEND) {
            $invoice->updateAttributes(['invoice_status_updated_at' => time()]);
        }

        return $this->render(($isDemo) ? 'out-view-demo' : 'out-view', [
            'invoice' => $invoice,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
            'isDemo' => $isDemo
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutViewLogin($uid, $email = null)
    {
        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        $model = new LoginForm(['username' => $email]);

        if (($post = Yii::$app->request->post('LoginForm')) && $model->load($post, '') && $model->login()) {
            return $this->actionOutViewSave($uid, $email);
        }

        return $this->renderAjax('out-view/login', [
            'model' => $model,
            'invoice' => $invoice,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutViewReset($uid, $email = null)
    {
        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        $model = new PasswordResetRequestForm;

        if (($post = Yii::$app->request->post('PasswordResetRequestForm')) && $model->load($post, '') && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'На Ваш e-mail: ' . $model->email . ' были высланы дальнейшие инструкции.');

                return $this->actionOutViewLogin($uid, $email);
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем сбросить пароль для указанного адреса электронной почты.');
            }
        }

        return $this->renderAjax('out-view/reset', [
            'model' => $model,
            'invoice' => $invoice,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutViewSignup($uid, $email = null)
    {
        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        $model = new InvoiceOutViewForm([
            'scenario' => InvoiceOutViewForm::SCENARIO_SIGNUP,
            'invoice' => $invoice,
            'username' => $email,
        ]);

        if (($post = Yii::$app->request->post('InvoiceOutViewForm')) && $model->load($post, '') && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Вы успешно зарегистрированны.');

            return $this->actionOutViewSave($uid, $email);
        }

        return $this->renderAjax('out-view/signup', [
            'model' => $model,
            'invoice' => $invoice,
            'userExists' => $email ? Employee::find()->isActual()->byEmail($email)->exists() : false,
            'email' => $email,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutViewSave($uid, $email = null)
    {
        $userExists = $email ? Employee::find()->isActual()->byEmail($email)->exists() : false;

        if (Yii::$app->user->isGuest) {
            return $userExists ? $this->actionOutViewLogin($uid, $email) : $this->actionOutViewSignup($uid, $email);
        }

        /* @var Invoice $invoice */
        $invoice = self::getInvoiceByUid($uid);

        $companyExists = Yii::$app->user->identity->getCompanies()->andWhere([
            'inn' => $invoice->contractor_inn,
            'blocked' => false,
        ])->andFilterWhere([
            'kpp' => $invoice->contractor_kpp,
        ])->exists();

        $model = new InvoiceOutViewForm([
            'scenario' => $companyExists ? InvoiceOutViewForm::SCENARIO_SAVE : InvoiceOutViewForm::SCENARIO_COMPANY,
            'invoice' => $invoice,
            'employee' => Yii::$app->user->identity,
        ]);

        if (($post = Yii::$app->request->post('InvoiceOutViewForm')) !== null) {
            if (!$companyExists && !Yii::$app->user->identity->getCanAddCompany()) {
                Yii::$app->session->set(
                    'notify_modal',
                    'Ваш аккаунт имеет компании на Бесплатном тарифе. ' .
                    'Чтобы добавить еще одну компанию, нужно ' .
                    Html::a('оплатить сервис', '/subscribe/default/index') . '.'
                );

                return $this->redirect('/site/index');
            }

            if (($post = Yii::$app->request->post('InvoiceOutViewForm')) && $model->load($post, '') && $model->save()) {
                if (!$invoice->out_save_count) {
                    $invoice->updateAttributes(['out_save_count' => 1]);
                }

                return $this->redirect($model->getInvoiceViewUrl());
            }
        }

        return $this->renderAjax('out-view/save', [
            'model' => $model,
            'invoice' => $invoice,
            'userExists' => $userExists,
            'email' => $email,
        ]);
    }

    /**
     * Get an invoice file by uid for unauthorized users
     * @param $type file type
     * @param $uid invoice uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getInvoiceByUid($uid);
        $model->out_download_count += 1;
        $model->save(true, ['out_download_count']);

        switch ($type) {
            case 'pdf':
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean)$model->company->pdf_signed,
                ]);
                break;

            default:
                $doc = new PhpWordInvoice($model);
                return $doc->getFile();
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Invoice
     * @throws NotFoundHttpException
     */
    public static function getInvoiceByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = Invoice::find()
            ->byIOType($type)
            ->byUid($uid)
            ->byDeleted(false)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCopyAuto($id)
    {
        $invoiceAuto = $this->loadModelAuto($id);

        $auto = clone $invoiceAuto->auto;
        unset($auto->id);
        $auto->isNewRecord = true;
        $auto->setNextNumber($invoiceAuto->company_id);
        $auto->last_invoice_date = null;
        $auto->next_invoice_date = null;

        /* @var $model Invoice */
        $model = clone $invoiceAuto;
        unset($model->id);
        $model->isNewRecord = true;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $orders = [];
        foreach ($invoiceAuto->orders as $order) {
            $orders[] = $this->cloneOrders($order, $invoiceAuto);
        }
        $model->populateRelation('orders', $orders);
        $model->populateRelation('auto', $auto);
        foreach ($orders as $order) {
            $order->populateRelation('invoice', $model);
        }
        if (InvoiceHelper::save($model, false)) {
            return $this->_redirectAfterSave($model);
        } else {
            return $this->redirect([
                'view-auto',
                'id' => $id,
                'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
            ]);
        }
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCopyInvoice($type, $id)
    {
        if (!\Yii::$app->user->identity->company->createInvoiceAllowed($type, true, true)) {
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $cloneModel = $this->cloneInvoice($type, $id);
        $cloneModel->invoice_status_id = InvoiceStatus::STATUS_CREATED;
        $cloneModel->invoice_status_author_id = $cloneModel->document_author_id = \Yii::$app->user->id;

        if (InvoiceHelper::save($cloneModel, false)) {
            $this->checkShowPopup();
            $this->checkShowScanPopup();
            Yii::$app->session->setFlash('success', 'Счет скопирован');

            return $this->_redirectAfterSave($cloneModel, true);
        } else {
            return $this->redirect([
                'view',
                'type' => $type,
                'id' => $id,
                'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
            ]);
        }
    }

    /**
     * @param $type
     * @param $id
     * @return Invoice
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function cloneInvoice($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $model->document_date = date(DateHelper::FORMAT_DATE);

        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->document_number = ($type == Documents::IO_TYPE_OUT) ?
            $model->getNextDocumentNumber(Yii::$app->user->identity->company->id, $type, null, $clone->document_date) : '';
        $clone->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 day'));
        $cloneOrders = [];
        foreach ($model->orders as $order) {
            $cloneOrders[] = $this->cloneOrders($order, $clone);
        }
        $clone->populateRelation('orders', $cloneOrders);

        // reset defaults
        $clone->can_add_act = 1;
        $clone->can_add_packing_list = 1;
        $clone->can_add_sales_invoice = 1;
        $clone->can_add_proxy = 1;
        $clone->can_add_invoice_facture = 1;
        $clone->can_add_upd = 1;
        $clone->has_act = 0;
        $clone->has_packing_list = 0;
        $clone->has_sales_invoice = 0;
        $clone->has_proxy = 0;
        $clone->has_invoice_facture = 0;
        $clone->has_upd = 0;
        $clone->has_file = 0;
        $clone->need_act = 1;
        $clone->need_upd = 1;
        $clone->need_packing_list = 1;
        $clone->need_sales_invoice = 1;

        return $clone;
    }

    /**
     * @inheritdoc
     */
    protected function checkShowPopup()
    {
        if (Yii::$app->user->identity->knows_about_sending_invoice == 0) {
            Yii::$app->session->set('show_send_popup', 1);
        }
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function cloneOrders(Order $order, Invoice $invoice)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->invoice_id = null;
        $cloneOrder->populateRelation('invoice', $invoice);

        return $cloneOrder;
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSetResponsible($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $emplComp = Yii::$app->user->identity->company->getEmployeeCompanies()->andWhere([
            'is_working' => true,
            'employee_id' => Yii::$app->request->post('responsible_employee_id'),
        ])->one();

        if ($emplComp !== null && $model->setResponsible($emplComp)) {
            Yii::$app->session->setFlash('success', 'Ответственный изменен');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось изменить ответственного');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMassSetResponsible()
    {
        $company = Yii::$app->user->identity->company;
        $ids = array_filter((array) Yii::$app->request->post('selected'), function ($val) {
            return $val === strval(intval($val));
        });

        $emplComp = $company->getEmployeeCompanies()->andWhere([
            'is_working' => true,
            'employee_id' => Yii::$app->request->post('responsible_employee_id'),
        ])->one();

        $invoiceArray = $emplComp === null ? [] : $company->getInvoices()->andWHere([
            'id' => $ids,
            'invoice_status_id' => InvoiceStatus::$validInvoices,
        ])->all();
        $result = 0;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->setResponsible($emplComp)) {
                $result++;
            }
        }

        if ($result > 0) {
            Yii::$app->session->setFlash('success', 'Ответственный изменен');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось изменить ответственного');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMassSetIndustry()
    {
        $company = Yii::$app->user->identity->company;
        $industryId = Yii::$app->request->post('industry_id');

        if ($industryId > 0) {
            if (!CompanyIndustry::find()->where(['id' => $industryId, 'company_id' => $company->id])->exists()) {
                Yii::$app->session->setFlash('error', 'Не удалось изменить счета, направление не найдено');
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            $industryId = null;
        }

        $ids = array_filter((array) Yii::$app->request->post('selected'), function ($val) {
            return $val === strval(intval($val));
        });

        $invoiceArray = $company->getInvoices()->andWHere([
            'id' => $ids,
            'company_id' => $company->id,
            'invoice_status_id' => InvoiceStatus::$validInvoices,
        ])->all();

        $result = 0;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->updateAttributes(['industry_id' => $industryId])) {
                LogHelper::log($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_INDUSTRY);
                $result++;
            }

            if ($result > 0) {
                Yii::$app->session->setFlash('success', 'Направление изменено');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось изменить направление');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionMassSetProject()
    {
        $company = Yii::$app->user->identity->company;
        $projectId = Yii::$app->request->post('project_id');

        if ($projectId > 0) {
            if (!Project::find()->where(['id' => $projectId, 'company_id' => $company->id])->exists()) {
                Yii::$app->session->setFlash('error', 'Не удалось изменить счета, проект не найден');
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            $projectId = null;
        }

        $ids = array_filter((array) Yii::$app->request->post('selected'), function ($val) {
            return $val === strval(intval($val));
        });

        $invoiceArray = $company->getInvoices()->andWHere([
            'id' => $ids,
            'company_id' => $company->id,
            'invoice_status_id' => InvoiceStatus::$validInvoices,
        ])->all();

        $result = 0;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->updateAttributes(['project_id' => $projectId])) {
                LogHelper::log($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_PROJECT);
                $result++;
            }

            if ($result > 0) {
                Yii::$app->session->setFlash('success', 'Проект изменен');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось изменить проект');
            }
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionMassInvoiceItem($type)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException();
        }
        $company = Yii::$app->user->identity->company;
        $itemId = Yii::$app->request->post('item_id');
        $ids = array_filter((array) Yii::$app->request->post('selected'), function ($val) {
            return $val === strval(intval($val));
        });
        $result = 0;
        $attribute = $type == Documents::IO_TYPE_OUT ? 'invoice_income_item_id' : 'invoice_expenditure_item_id';
        $query = $type == Documents::IO_TYPE_OUT ? InvoiceIncomeItem::find() : InvoiceExpenditureItem::find();
        $query->andWhere([
            'id' => $itemId,
        ])->andWhere([
            'OR',
            ['company_id' => null],
            ['company_id' => $company->id],
        ]);


        if ($itemId && $ids && $query->exists()) {
            $invoiceArray = $company->getInvoices()->andWHere([
                'id' => $ids,
                'company_id' => $company->id,
                'invoice_status_id' => InvoiceStatus::$validInvoices,
            ])->all();
            foreach ($invoiceArray as $invoice) {
                if ($invoice->updateAttributes([$attribute => $itemId])) {
                    LogHelper::log($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                    $result++;
                }

            }
        }

        $title = $type == Documents::IO_TYPE_OUT ? 'приходов' : 'расходов';

        if ($result > 0) {
            Yii::$app->session->setFlash('success', sprintf('Статья %s изменена', $title));
        } else {
            Yii::$app->session->setFlash('error', sprintf('Не уддалось изменить статью %s', $title));
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
    
    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $status = InvoiceStatus::findOne(Yii::$app->request->getBodyParam('status'));

        if ($status !== null && $status->id != $model->invoice_status_id) {
            $model->invoice_status_id = $status->id;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Invoice $model) {
                if ($model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id'])) {
                    if ($model->invoice_status_id == InvoiceStatus::STATUS_REJECTED) {
                        \frontend\modules\analytics\models\PlanCashContractor::updateFlowByInvoice($model);
                        return $model->rejectDacuments();
                    }

                    return true;
                }

                return false;
            });
        }
        if ($model->invoice_status_id == InvoiceStatus::STATUS_REJECTED) {
            Yii::$app->session->setFlash('success', 'Счет отменён');
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
            'textRequired' => true,
        ]);

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                $this->actionRunScanPopup();
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\base\ExitException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var EmployeeCompany $sender */
        $sender = Yii::$app->user->identity->currentEmployeeCompany;

        $sendWithDocuments = Yii::$app->request->get('send_with_documents');

        $contractors = null;
        $invoices = Yii::$app->request->post('Invoice');
        $send = 0;
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                /* @var $model Invoice */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, [
                    'ioType' => $model->type,
                    'model' => $model,
                ])) {
                    continue;
                }

                if (($sendTo = $model->contractor->someEmail) &&
                    $model->sendAsEmail($sender, $sendTo, null, null, null, null, null, false, $sendWithDocuments)) {
                    $send += 1;
                } else {
                    $contractors[$model->contractor->id] = Html::a($model->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->contractor->type, 'id' => $model->contractor->id
                    ]));
                }
            }
        }
        $message = 'Отправлено ' . $send . ' из ' . count($invoices) . ' писем.';
        if ($send == count($invoices)) {
            Yii::$app->session->setFlash('success', $message);
        }

        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки счетов, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @param $contractor
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionManySendByContractor($type, $contractor)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $contractors = null;
        $invoices = explode(', ', Yii::$app->request->post('invoices'));
        $send = 0;
        $all = 0;
        foreach ($invoices as $id) {
            /* @var Invoice $model */
            $model = $this->loadModel($id, $type);

            if (!Yii::$app->user->can(permissions\document\Document::VIEW, [
                'ioType' => $model->type,
                'model' => $model,
            ])) {
                continue;
            }

            $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
                'model' => $model,
            ]);

            if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($sendForm);
            }

            if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
                $saved = $sendForm->send();
                if (is_array($saved)) {
                    $send += $saved[0];
                    $all += $saved[1];
                    $model->email_messages += $saved[0];
                    $model->save(true, ['email_messages']);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Отправлено ' . $send . ' из ' . $all . ' писем.');

        return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlowOld($type, $id)
    {
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $flowForm = new InvoiceFlowForm([
            'invoice' => $model,
            'employee' => Yii::$app->user->identity,
        ]);

        if (Yii::$app->request->isAjax && $flowForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($flowForm);
        }

        if ($flowForm->load(Yii::$app->request->post()) && $flowForm->validate()) {
            $flowForm->save();
        }
        if ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
            Yii::$app->session->setFlash('success', 'Счет оплачен');
        } elseif ($model->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
            Yii::$app->session->setFlash('success', 'Счет оплачен частично');
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlow($type, $id)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException('Не выбран тип документов');
        }
        $idArray = explode(',', $id);

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $modelArray = $company->getInvoices()->andWhere([
            'id' => $idArray,
            'type' => $type,
            'is_deleted' => false,
        ])->all();

        $invoiceArray = [];
        foreach ($modelArray as $model) {
            if ($model->availablePaymentAmount > 0) {
                $invoiceArray[] = $model;
            }
        }

        $flowType = Yii::$app->request->post('InvoiceFlowForm')['flowType'] ?? null;
        $hasNotForBookkeeping = false;
        foreach ($invoiceArray as $invoice) {
            if ($invoice->not_for_bookkeeping) {
                $hasNotForBookkeeping = true;
                break;
            }
        }

        if (!Yii::$app->request->post('ajax') && $hasNotForBookkeeping && $flowType == CashFactory::TYPE_BANK) {
            Yii::$app->session->setFlash('error', 'Недопустимый тип оплаты для счетов "не для бухгалтерии".');
        } else if ($invoiceArray) {
            if (count($invoiceArray) === 1) {
                $model = array_shift($invoiceArray);

                $flowForm = new InvoiceFlowForm([
                    'invoice' => $model,
                    'employee' => Yii::$app->user->identity,
                ]);

                if (Yii::$app->request->post('ajax') && $flowForm->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return ActiveForm::validate($flowForm);
                }

                if ($flowForm->load(Yii::$app->request->post()) && $flowForm->validate()) {
                    $flowForm->save();
                }
                if ($flowForm->invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
                    Yii::$app->session->setFlash('success', 'Счет оплачен');
                } elseif ($flowForm->invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL) {
                    Yii::$app->session->setFlash('success', 'Счет оплачен частично');
                }
            } else {
                $paidCount = 0;
                $hasFlowsMessage = [];
                $flowForm = new InvoiceFlowForm([
                    'invoice' => new Invoice(['type' => $type, 'company' => $company]),
                    'employee' => Yii::$app->user->identity,
                ]);

                if (Yii::$app->request->post('ajax') && $flowForm->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return ActiveForm::validate($flowForm, ['flowType', 'date_pay', 'isAccounting']);
                }

                if ($flowForm->load(Yii::$app->request->post()) && $flowForm->validate(['flowType', 'date_pay', 'isAccounting'])) {
                    $modelArraySorted = [];
                    foreach ($invoiceArray as $invoice) {
                        $modelArraySorted[$invoice->contractor_id][] = $invoice;
                    }
                    foreach ($modelArraySorted as $contractorId => $invoiceArray) {
                        $invoice = $invoiceArray[0];
                        $contractor = $invoice->contractor;
                        $hasFlows = [];
                        if ($contractor->getNotLinkedBankFlows($company)->exists()) {
                            $hasFlows[] = 'банку';
                        }
                        if ($contractor->getNotLinkedOrderFlows($company)->exists()) {
                            $hasFlows[] = 'кассе';
                        }
                        if ($contractor->getNotLinkedEmoneyFlows($company)->exists()) {
                            $hasFlows[] = 'e-money';
                        }

                        if ($hasFlows) {
                            $contractorName = $invoice->contractor->nameWithType;
                            $flows = implode(', ', $hasFlows);
                            $numbers = implode(', ', array_map(function ($invoice) {
                                return '№' . $invoice->fullNumber;
                            }, $invoiceArray));
                            $message = "По {$contractorName} есть оплаты по {$flows} не привязанные к счетам.";
                            $message .= " Что бы не продублировать оплаты нужно проставить по каждому счету оплату отдельно.";
                            $message .= "<br>Счет {$numbers}.";
                            $hasFlowsMessage[] = $message;
                        } else {
                            $amount = array_sum(array_map(function ($invoice) {
                                return $invoice->availablePaymentAmount;
                            }, $invoiceArray));
                            $numbers = implode(', ', array_map(function ($invoice) {
                                return '№' . $invoice->fullNumber . ' от ' .
                                    DateTime::createFromFormat('Y-m-d', $invoice->document_date)->format('d.m.Y');
                            }, $invoiceArray));
                            if ($amount > 0) {
                                $flowForm->invoice = $invoice;
                                $flowForm->amount = $amount;
                                $flowModel = $flowForm->createFlow();
                                $flowModel->description = "Оплата счета {$numbers}";
                                $isSaved = Yii::$app->db->transaction(function ($db) use ($flowModel, $invoiceArray, $company) {
                                    if (LogHelper::save($flowModel, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                                        foreach ($invoiceArray as $invoice) {
                                            if (!$flowModel->linkInvoice($invoice)) {
                                                $db->transaction->rollBack();

                                                return false;
                                            }
                                        }

                                        return true;
                                    }

                                    $db->transaction->rollBack();

                                    return false;
                                });

                                if ($isSaved) {
                                    $paidCount += count($invoiceArray);
                                }
                            }
                        }
                    }
                }

                if ($paidCount) {
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 15);
                    Yii::$app->session->setFlash('success', "Оплачено счетов: {$paidCount}.");
                }
                if ($hasFlowsMessage) {
                    Yii::$app->session->setFlash('info', implode('<br><br>', $hasFlowsMessage));
                }
            }
        } else {
            Yii::$app->session->setFlash('info', "Нет доступных для оплаты счетов.");
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionAddFlowForm($type, $id)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException();
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $idArray = explode(',', $id);

        $model = (count($idArray) == 1) ? $company->getInvoices()->andWhere([
            'id' => $idArray[0],
            'type' => $type,
            'is_deleted' => false,
        ])->one() : null;

        if ($model === null) {
            $model = new Invoice([
                'type' => $type,
                'company_id' => $company->id,
                'contractor_id' => Yii::$app->request->getQueryParam('contractorId', null),
                'price_precision' => 2,
            ]);
        }

        return $this->renderAjax('_add_flow_form', [
            'model' => $model,
            'idArray' => $idArray,
            'useContractor' => (boolean)Yii::$app->request->getQueryParam('contractorId', false),
        ]);
    }

    /**
     * @param $type
     * @param null $contractorId
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionContractorProduct($type, $id, $nds = null)
    {
        if (!in_array($type *= 1, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException();
        }
        if (!Yii::$app->request->isPjax) {
            return $this->redirect(Yii::$app->request->referrer ?: ['create', 'type' => $type]);
        }
        $company = Yii::$app->user->identity->company;
        $lastInvoiceId = $company->getInvoices()->select('id')->andWhere([
            'type' => $type,
            'contractor_id' => $id,
            'is_deleted' => false,
        ])->orderBy(['id' => SORT_DESC])->scalar();

        if ($lastInvoiceId) {
            $model = $this->cloneInvoice($type, $lastInvoiceId);
        } else {
            $model = new Invoice([
                'type' => $type,
                'company_id' => $company->id,
                'contractor_id' => $lastInvoice ? $lastInvoice->contractor_id : null,
                'price_precision' => 2,
            ]);
        }

        return $this->renderAjax('form/_product_table_invoice', [
            'model' => $model,
            'ioType' => $type,
            'company' => $company,
            'ndsCellClass' => 'with-nds-item' . ($nds ? '' : ' hidden'),
        ]);
    }

    /**
     * @param $type
     * @param bool $isContract
     * @param null $createDocument
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionFirstCreate($type, $isContract = false, $createDocument = null)
    {
        if (!in_array($type, [Documents::IO_TYPE_OUT, Documents::IO_TYPE_IN])) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        if (Yii::$app->request->getIsGet() && $company->getInvoices()->exists()) {
            return $this->redirect(Url::current([0 => 'create']));
        }

        Yii::$app->user->identity->need_look_service_video = false;

        return $this->actionCreate($type, null, $isContract, $createDocument);

        $company->scenario = $company->getIsLikeIP() ?
            Company::SCENARIO_IP_UPDATE :
            Company::SCENARIO_OOO_UPDATE;

        if ($company->getInvoices()->byIOType($type)->byDeleted()->exists()) {
            // return $this->redirect(['create', 'type' => $type]);
        }

        /* @var CheckingAccountant $account */
        $account = $company->mainAccountant ?: new CheckingAccountant([
            'type' => CheckingAccountant::TYPE_MAIN,
            'company_id' => $company->id,
        ]);
        $company->populateRelation('mainAccountant', $account);

        /* @var Contractor $contractor */
        $contractor = new Contractor([
            'scenario' => 'insert',
            'type' => $type,
            'status' => Contractor::ACTIVE,
            'face_type' => 0,
            'company_id' => $company->id,
            'employee_id' => Yii::$app->user->id,
            'company_type_id' => -1,
            'taxation_system' => Contractor::WITHOUT_NDS,
            'chief_accountant_is_director' => 1,
            'contact_is_director' => 1,
            'object_guid' => OneCExport::generateGUID(),
        ]);

        /* @var Invoice $model */
        $model = new Invoice([
            'scenario' => 'insert',
            'type' => $type,
            'company_id' => $company->id,
            'isAutoinvoice' => 0,
            'price_precision' => 2,
            'document_date' => date(DateHelper::FORMAT_DATE),
            'show_article' => Yii::$app->user->identity->config->invoice_form_article,
            'payment_limit_date' => date(DateHelper::FORMAT_DATE, strtotime("+10 day")),
            'nds_view_type_id' => $company->nds_view_type_id,
        ]);
        if ($type == Documents::IO_TYPE_OUT) {
            $model->document_number = Invoice::getNextDocumentNumber($company->id, $type, null, $model->document_date);
        }
        $model->populateRelation('company', $company);

        if ($post = Yii::$app->request->post()) {
            switch (ArrayHelper::getValue((array)Yii::$app->request->post('Contractor', []), 'face_type')) {
                case 1:
                    $contractor->scenario = Contractor::SCENARIO_FIZ_FACE;
                    break;
                default:
                    $contractor->scenario = Contractor::SCENARIO_LEGAL_FACE;
                    break;
            }
            $company->address_actual = null;
            $company->load($post);
            $account->load($post);
            $contractor->load($post);
            InvoiceHelper::load($model, $post);
            if (empty($contractor->director_name)) {
                $contractor->director_name = 'ФИО руководителя';
            }
            $companyValidate = $company->validate();
            $accountValidate = $account->validate();
            $contractorValidate = $contractor->validate();
            $model->validate();
            $model->clearErrors('company_id');
            $model->clearErrors('contractor_id');
            if ($companyValidate && $accountValidate && $contractorValidate) {
                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $contractor, $company, $account) {
                    if ($account->save() && $company->save() && $contractor->save()) {
                        $model->contractor_id = $contractor->id;
                        if (InvoiceHelper::save($model)) {
                            return true;
                        }
                    }
                    $db->transaction->rollBack();

                    return false;
                });

                if ($isSaved) {
                    if (Yii::$app->user->identity->need_look_service_video == true) {
                        $user->identity->need_look_service_video = false;
                        $user->identity->save(false, ['need_look_service_video']);
                    }

                    $this->checkShowPopup();
                    $this->checkShowScanPopup();
                    $invoiceCount = $company->getInvoices()->count();
                    if ($invoiceCount == 1) {
                        $tooltips = [
                            'tooltip_send',
                            'tooltip_pdf',
                            'tooltip_paid',
                            'tooltip_account-view',
                            'tooltip_send-example',
                        ];

                        foreach ($tooltips as $key => $value) {
                            $name = "{$value}_{$model->company_id}";
                            if (!isset($_COOKIE[$name])) {
                                setcookie($name, "1", time() + (3600 * 24 * 365));
                            }
                        }
                    }
                    //Yii::$app->session->setFlash('success', 'Счет создан.');

                    return $this->_redirectAfterSave($model);
                } else {
                    $company->load($post);
                }
            }
        }


        return $this->render('first-create', [
            'company' => $company,
            'account' => $account,
            'contractor' => $contractor,
            'ioType' => $type,
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
        ]);
    }

    /**
     * @param $type
     * @param null $contractorId
     * @param bool $isContract
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionCreate($type, $contractorId = null, $example = false, $isContract = false)
    {
        if (($url = Yii::$app->request->referrer) && ($path = parse_url($url, PHP_URL_PATH))) {
            $excludedPaths = [
                '/documents/invoice/view',
                '/documents/invoice/update',
                '/documents/invoice/create'
            ];
            if (!in_array(strtolower($path), $excludedPaths)) {
                Url::remember($url, 'invoiceViewReturnUrl');
            }
        }

        if (!in_array($type, [Documents::IO_TYPE_IN, Documents::IO_TYPE_OUT])) {
            throw new NotFoundHttpException('Тип документа не найден');
        }

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        if (Yii::$app->request->getIsGet() && $this->action->id != 'first-create' && !$company->getInvoices()->exists()) {
            return $this->redirect(Url::current([0 => 'first-create']));
        }

        $returnUrl = Yii::$app->session->remove('documents.invoice.create-return') ? :
            Yii::$app->request->post('returnUrl', Yii::$app->request->get('returnUrl'));

        if (!$company->createInvoiceAllowed($type, true, true)) {
            return $this->redirect(\Yii::$app->request->referrer);
        }

        $create = Yii::$app->request->get('create');
        if ($create !== null && !in_array($create, Documents::$slugList)) {
            $create = null;
        }

        $projectId = (int)Yii::$app->request->get('project_id', null);
        $projectEstimateId = (int)Yii::$app->request->get('project_estimate_id', null);
        $hasProjectEstimate = !empty($projectEstimateId);

        /* @var $user Employee */
        $user = Yii::$app->user;

        $fixedContractor = false;
        $document = null;
        $documentNumber = null;
        $documentDate = null;
        $newDocFields = [
            'document_number' => null,
            'document_date' => date('Y-m-d')
        ];

        $postNewDoc = $postInvoice = null;

        /* @var Company $company */
        $company = Yii::$app->user->identity->company;
        if (Yii::$app->request->post('is_first_invoice') ||
            $company->strict_mode == Company::ON_STRICT_MODE ||
            !$company->getContractors()->byContractor($type)->byDeleted()->exists() ||
            !$company->getProducts()->exists()
        ) {
            $this->action->id = 'first-create';
            if (isset(Yii::$app->request->post('Company')['nds'])) {
                $company->nds = Yii::$app->request->post('Company')['nds'];
            }
        }

        if ($company->invoiceContractEssence) {
            $invoiceContractEssence = $company->invoiceContractEssence;
        } else {
            $invoiceContractEssence = new InvoiceContractEssence();
            $invoiceContractEssence->company_id = $company->id;
        }
        if ($company->invoiceEssence) {
            $invoiceEssence = $company->invoiceEssence;
        } else {
            $invoiceEssence = new InvoiceEssence();
            $invoiceEssence->company_id = $company->id;
        }

        $document = Yii::$app->request->get('document');
        if ($document && in_array($document, Documents::$slugList)) {
            if ($postInvoice = Yii::$app->request->post('Invoice')) {
                $documentNumber = $postInvoice['document_number'];
            } else {
                $documentNumber = Invoice::getNextCreatedDocumentNumber(Yii::$app->user->identity->company->id, $type, null, date(DateHelper::FORMAT_DATE));
            }
            // ADDING WITH INVOICE NUMBER
            if ($postNewDoc = Yii::$app->request->post('NewDoc')) {
                $newDocFields['document_number'] = $postNewDoc['document_number'];
                $newDocFields['document_date'] = DateHelper::format($postNewDoc['document_date'], DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
            }
        } else {
            $document = null;
        }

        /* @var Invoice $model */
        if ($fromId = Yii::$app->request->get('clone')) {
            $model = $this->cloneInvoice($type, $fromId);
            $model->invoice_status_id = InvoiceStatus::STATUS_CREATED;
            $model->invoice_status_author_id = $model->document_author_id = \Yii::$app->user->id;
            $model->isAutoinvoice = ($type == Documents::IO_TYPE_OUT && Yii::$app->request->get('auto')) ? 1 : 0;
            if ($model->isAutoinvoice) {
                $autoinvoice = new AutoinvoiceForm([
                    'company_id' => $company->id,
                    'period' => Autoinvoice::MONTHLY,
                    'day' => 1,
                    'payment_delay' => $model->contractor? $model->contractor->customer_payment_delay : 10,
                    'dateFrom' => date('d.m.Y'),
                    'send_to' => [AutoinvoiceForm::TO_DIRECTOR],
                ]);
                $autoinvoice->setNextNumber($company->id);
                $autoinvoice->populateRelation('invoice', $model);
                $model->populateRelation('auto', $autoinvoice);
            }
        } else {
            $model = $this->getNewModel($type);
            $model->scenario = 'insert';
            $model->type = $type;
            $model->price_precision = 2;
            $model->document_date = date(DateHelper::FORMAT_DATE);
            $model->is_invoice_contract = $isContract;
            $model->payment_limit_rule = Invoice::PAYMENT_LIMIT_RULE_INVOICE;
            $model->contract_essence_template = Invoice::CONTRACT_ESSENCE_TEMPLATE_GOODS;
            if ($type == Documents::IO_TYPE_IN) {
                $model->nds_view_type_id = Invoice::NDS_VIEW_IN;
            } else {
                $model->nds_view_type_id = $company->nds_view_type_id;
            }
            $model->isAutoinvoice = ($type == Documents::IO_TYPE_OUT && Yii::$app->request->get('auto')) ? 1 : 0;
            $model->company_id = $company->id;
            $model->populateRelation('company', $company);

            if ($lastInvoice = $company->getInvoices()->andWhere(['type' => $type, 'is_deleted' => false,])->orderBy(['id' => SORT_DESC])->one()) {
                $model->is_invoice_contract = $lastInvoice->is_invoice_contract;
            }

            // Set user store
            $userStores = $user->identity->getStores()
                ->orderBy(['is_main' => SORT_DESC])
                ->all();
            if ($userStores) {
                $model->store_id = $userStores[0]->id;
            }

            if ($type == Documents::IO_TYPE_OUT) {
                // DOC NUMBER
                if ($document)
                    $model->document_number = (string)$documentNumber;
                // INVOICE NUMBER
                else
                    $model->document_number = (string)Invoice::getNextDocumentNumber(Yii::$app->user->identity->company->id, $type, null, $model->document_date);
            }

            if ($type == Documents::IO_TYPE_IN && $postNewDoc) {

                if ($this->validateNewDocumentFields($company, $model, $document, $newDocFields)) {
                    $documentNumber = $newDocFields['document_number'];
                    $documentDate = $newDocFields['document_date'];
                }
            }

            // create from InInvoice
            if ($from = Yii::$app->request->get('from')) {
                $model->createdFromInInvoiceId = (int)$from;
            }

            $payment_delay = $type == Documents::IO_TYPE_IN ? $company->seller_payment_delay : $company->customer_payment_delay;;
            if ($contractorId) {
                /* @var $contractor Contractor */
                if (($contractor = $company->getContractors()->andWhere(['id' => $contractorId])->one()) === null) {
                    // OOO "KUB"
                    if ($contractorId == 1)
                        $contractor = Contractor::findOne(['id' => 1]);
                    else
                        throw new NotFoundHttpException('Контрагент не найден.');
                }
                $payment_delay = $type == Documents::IO_TYPE_IN ? $contractor->seller_payment_delay : $contractor->customer_payment_delay;
                $model->contractor_id = $contractor->id;
                $model->agreement = $contractor->last_basis_document;
                $model->populateRelation('contractor', $contractor);
                if (Yii::$app->request->isGet && $contractor->nds_view_type_id !== null) {
                    $model->nds_view_type_id = $contractor->nds_view_type_id;
                }
                if ($type == Documents::IO_TYPE_OUT) {
                    $model->industry_id = $contractor->customer_industry_id;
                    $model->sale_point_id = $contractor->customer_sale_point_id;
                } else {
                    $model->industry_id = $contractor->seller_industry_id;
                    $model->sale_point_id = $contractor->seller_sale_point_id;
                }
            }
            $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));

            $autoinvoice = new AutoinvoiceForm([
                'company_id' => $company->id,
                'period' => Autoinvoice::MONTHLY,
                'day' => 1,
                'payment_delay' => $payment_delay,
                'dateFrom' => date('d.m.Y'),
                'send_to' => [AutoinvoiceForm::TO_DIRECTOR],
            ]);
            $autoinvoice->setNextNumber($company->id);
            $autoinvoice->populateRelation('invoice', $model);
            $model->populateRelation('auto', $autoinvoice);
        }

        $model->show_article = Yii::$app->user->identity->config->invoice_form_article;

        if (Yii::$app->request->isPjax) {

            $model->load(Yii::$app->request->post());
            if (($contractor = $model->contractor) !== null && $contractor->nds_view_type_id !== null) {
                $model->nds_view_type_id = $contractor->nds_view_type_id;
            }

            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company,
                'fixedContractor' => $fixedContractor,
                'message' => new Message($this->typeDocument, $model->type),
                'ioType' => $type,
                'contractorId' => $contractorId,
                'projectId' => $projectId,
                'autoinvoice' => $model->auto,
                'invoiceContractEssence' => $invoiceContractEssence,
                'invoiceEssence' => $invoiceEssence,
                'create' => $create,
                'hasProjectEstimate' => $hasProjectEstimate,
                'projectEstimateId' => $projectEstimateId,
            ]);
        }

        $invoiceSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $contractorId, $company, $document, $type, $documentNumber, $postNewDoc) {
            if ($this->action->id === 'first-create') {
                $company->save(true, ['nds']);
            }
            if (InvoiceHelper::load($model, Yii::$app->request->post())) {
                if ($contractorId !== null) {
                    $model->contractor_id = $contractorId;
                }
                // REPLACE DOC NUMBER TO INVOICE NUMBER
                if ($document && !$postNewDoc) {
                    $model->document_number = (string)Invoice::getNextDocumentNumber(Yii::$app->user->identity->company->id, $type, null, $model->document_date);
                }

                LogHelper::$useTransaction = false;
                if (InvoiceHelper::save($model)) {
                    return true;
                }
                // REPLACE INVOICE NUMBER TO DOC NUMBER
                if ($document && !$postNewDoc) {
                    $model->document_number = (string)$documentNumber;
                }
            }

            $db->transaction->rollBack();

            return false;
        });

        if ($invoiceSaved) {
            if ($invoiceContractEssence->load(Yii::$app->request->post())) {
                $invoiceContractEssence->text = $model->contract_essence;
                if (($invoiceContractEssence->is_checked && $invoiceContractEssence->text) ||
                    !$invoiceContractEssence->is_checked) {
                    $invoiceContractEssence->save();
                }
            }
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text = $model->comment;
                if (($invoiceEssence->is_checked && $invoiceEssence->text) ||
                    !$invoiceEssence->is_checked) {
                    $invoiceEssence->save();
                }
            }
            if ($user->identity->need_look_service_video == true) {
                $user->identity->need_look_service_video = false;
                $user->identity->save(false, ['need_look_service_video']);
            }
            if ($fromId) {
                \common\models\company\CompanyFirstEvent::checkEvent($company, 16);
            }

            $this->checkShowPopup();
            $this->checkShowScanPopup();

            $invoiceCount = $company->getInvoices()->count();
            if ($invoiceCount == 1) {
                $tooltips = [
                    'tooltip_send',
                    'tooltip_pdf',
                    'tooltip_paid',
                    'tooltip_account-view',
                    'tooltip_send-example',
                ];

                foreach ($tooltips as $key => $value) {
                    $name = "{$value}_{$model->company_id}";
                    if (!isset($_COOKIE[$name])) {
                        setcookie($name, "1", time() + (3600 * 24 * 365));
                    }
                }
            }
            if ($invoiceCount == 3 || $invoiceCount == 6) {
                Yii::$app->session->set('show_inquirer_popup', 1);
            }

            // CREATE DOCUMENT REDIRECT
            $create = $create ?: $document;
            if (!$model->isAutoinvoice && $create) {

                $docNumber = ($document) ?
                    $documentNumber :
                    $model->document_number;
                $docDate = ($document) ?
                    DateHelper::format($documentDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                    null;

                $extraParams = [];

                if ($create == Documents::SLUG_PROXY) {
                    $extraParams = [
                        'limitDate' => Yii::$app->request->get('limit_date'),
                        'proxyPersonId' => Yii::$app->request->get('proxy_person_id'),
                    ];
                }

                if ($this->isScanPreview) {
                    $extraParams['mode'] = 'previewScan';
                    $extraParams['returnToManager'] = 1;
                    $extraParams['permanentAttachFiles'] = 1;
                    $extraParams['files'] = Yii::$app->request->get('files');
                }

                return $this->redirect([
                        "/documents/{$create}/create",
                        'type' => $type,
                        'invoiceId' => $model->id,
                        'documentNumber' => $docNumber,
                        'documentDate' => $docDate,
                        'isFromInvoiceForm' => 1
                    ] + $extraParams);
            }
            if ($model->is_invoice_contract) {
                $message = ($model->isAutoinvoice ? 'Шаблон АвтоСчёт-договора' : 'Счет-договор') . ' создан.';
            } else {
                $message = ($model->isAutoinvoice ? 'Шаблон АвтоСчёта' : 'Счет') . ' создан.';
            }
            if ($company->getInvoices()->count() > 1) {
                Yii::$app->session->setFlash('success', $message);
            }

            return $returnUrl ? $this->redirect($returnUrl) : $this->_redirectAfterSave($model);
        }

        $addProduct = Yii::$app->request->post('selection');

        // create from InInvoice
        if ($from = Yii::$app->request->get('from')) {
            $fromInvoice = $company->getInvoices()->andWhere([
                'type' => Documents::IO_TYPE_IN,
                'is_deleted' => false,
                'id' => $from,
            ])->one();
            if ($fromInvoice !== null) {
                $addProduct = $fromInvoice->getOrders()->select('product_id')->column();
            }
        }

        if ($addProduct) {
            $orderArray = [];
            foreach ($addProduct as $pid) {
                if ($product = Product::getById($model->company_id, $pid)) {
                    $order = OrderHelper::createOrderByProduct($product, $model, 1);
                    $orderArray[] = $order;
                }
            }
            $model->populateRelation('orders', $orderArray);
            $model->beforeValidate();
        }

        if ($projectId && $model->type == 2 && !$model->contractor_id) {
            if ($project = Project::findOne(['id' => $projectId, 'company_id' => $company->id])) {
                $model->contractor_id = $project->customer_id;
            }
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'fixedContractor' => $fixedContractor,
            'message' => new Message($create ? Documents::$slugToType[$create] : $this->typeDocument, $model->type),
            'ioType' => $type,
            'contractorId' => $contractorId,
            'example' => $example,
            'projectId' => $projectId,
            'autoinvoice' => $model->auto,
            'invoiceContractEssence' => $invoiceContractEssence,
            'invoiceEssence' => $invoiceEssence,
            'document' => $document,
            'returnUrl' => $returnUrl,
            'newDoc' => $newDocFields,
            'create' => $create,
            'hasProjectEstimate' => $hasProjectEstimate,
            'projectEstimateId' => $projectEstimateId,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCreatePaymentOrder($id)
    {
        /* @var $model Invoice */
        $model = $this->loadModel($id, Documents::IO_TYPE_IN);

        if (!$model->contractor->ITN || !$model->contractor->BIC) {
            Yii::$app->session->setFlash('error', 'Платежное поручение не может быть создано. Заполните БИК и расчетный счет поставщика.');

            return $this->redirect(Yii::$app->getRequest()->getReferrer() ?: [
                '/documents/invoice/view',
                'id' => $model->id,
                'type' => $model->type,
            ]);
        }

        $paymentOrder = PaymentOrder::newByInInvoice($model);

        if ($paymentOrder->save()) {
            $paymentOrderInvoice = new PaymentOrderInvoice();
            $paymentOrderInvoice->invoice_id = $model->id;
            $paymentOrderInvoice->payment_order_id = $paymentOrder->id;
            if ($paymentOrderInvoice->save()) {
                return $this->redirect(Url::to(['/documents/payment-order/view', 'id' => $paymentOrder->id]));
            }
        }

        $paymentOrderErrors = [];
        foreach ($paymentOrder->getFirstErrors() as $name => $message) {
            $paymentOrderErrors[] = $message;
        }

        Yii::$app->session->setFlash('error', 'Ошибка при создании платежки.<br/>' . implode(' ', $paymentOrderErrors));

        return $this->redirect(['/documents/invoice/view', 'id' => $model->id, 'type' => $model->type]);
    }

    /**
     * @param $orderDocumentID
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCreateFromOrder($type, $orderDocumentID)
    {
        $orderDocument = OrderDocument::findOne($orderDocumentID);
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        if ($orderDocument) {
            $contractor = $orderDocument->contractor;
            /* @var Invoice $model */
            $model = $this->getNewModel($type);
            $model->scenario = 'insert';
            $model->type = $type;
            $model->document_date = date(DateHelper::FORMAT_DATE);
            $model->contractor_id = $orderDocument->contractor_id;
            $model->comment_internal = $orderDocument->comment_internal;
            $model->isAutoinvoice = 0;
            $model->company_id = $company->id;
            $model->populateRelation('company', $company);
            $model->document_number = (string)Invoice::getNextDocumentNumber($company->id, $type, null, $model->document_date);
            $payment_delay = $type == Documents::IO_TYPE_IN ? $contractor->seller_payment_delay : $contractor->customer_payment_delay;
            $model->agreement = $orderDocument->agreement;
            $model->populateRelation('contractor', $contractor);
            $model->nds_view_type_id = $orderDocument->nds_view_type_id;
            $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
            if ($type == Documents::IO_TYPE_IN) {
                $model->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_SALARY;
            }
            $invoiceSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $orderDocument, $company) {
                if ($this->action->id === 'first-create') {
                    $company->save(true, ['nds']);
                }
                foreach ($orderDocument->orderDocumentProducts as $orderDocumentProduct) {
                    $product = Product::getById($model->company_id, $orderDocumentProduct->product_id);
                    if ($product !== null) {
                        $order = OrderHelper::createOrderByProduct(
                            $product,
                            $model,
                            $orderDocumentProduct->quantity,
                            ($orderDocument->type == Documents::IO_TYPE_OUT)
                                ? ($orderDocument->nds_view_type_id == OrderDocument::NDS_VIEW_OUT ? $orderDocumentProduct->selling_price_no_vat : $orderDocumentProduct->selling_price_with_vat)
                                : ($orderDocument->nds_view_type_id == OrderDocument::NDS_VIEW_OUT ? $orderDocumentProduct->purchase_price_no_vat : $orderDocumentProduct->purchase_price_with_vat),
                            ($model->has_discount ? $orderDocumentProduct->discount : 0),
                            ($model->has_markup ? $orderDocumentProduct->markup : 0),
                            ($model->type == Documents::IO_TYPE_OUT)
                                ? $orderDocumentProduct->sale_tax_rate_id
                                : $orderDocumentProduct->purchase_tax_rate_id
                        );
                        $order->product_title = $orderDocumentProduct->product_title;
                        $orderArray[] = $order;
                    }
                }
                array_walk($orderArray, function ($order, $key) {
                    $order->number = $key + 1;
                });
                $model->populateRelation('orders', $orderArray);

                $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orders, 'mass_gross'));
                $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orders, 'place_count'));

                if (!InvoiceHelper::save($model)) {
                    $db->transaction->rollBack();

                    return false;
                }
                $orderDocument->invoice_id = $model->id;
                $orderDocument->save(true, ['invoice_id']);

                return true;
            });
            if ($invoiceSaved) {
                if ($user->need_look_service_video == true) {
                    $user->need_look_service_video = false;
                    $user->save(false, ['need_look_service_video']);
                }

                $this->checkShowPopup();
                $invoiceCount = $company->getInvoices()->count();
                if ($invoiceCount == 1) {
                    $tooltips = [
                        'tooltip_send',
                        'tooltip_pdf',
                        'tooltip_paid',
                        'tooltip_account-view',
                        'tooltip_send-example',
                    ];

                    foreach ($tooltips as $key => $value) {
                        $name = "{$value}_{$model->company_id}";
                        if (!isset($_COOKIE[$name])) {
                            setcookie($name, "1", time() + (3600 * 24 * 365));
                        }
                    }
                }
                if ($invoiceCount == 3 || $invoiceCount == 6) {
                    Yii::$app->session->set('show_inquirer_popup', 1);
                }
                Yii::$app->session->setFlash('success', 'Счет создан.');

                return $this->_redirectAfterSave($model);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при создании счета.');

                return $this->redirect(['/documents/order-document/view', 'id' => $orderDocumentID]);
            }
        }
        throw new NotFoundHttpException('Order document not found');
    }

    /**
     * @param $agreementDocumentID
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCreateFromAgreement($agreementDocumentID)
    {
        $agreementDocument = Agreement::findOne($agreementDocumentID);
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        if ($agreementDocument) {
            /* @var Invoice $model */
            $model = $this->getNewModel(Documents::IO_TYPE_OUT);
            $model->scenario = 'insert';
            $model->type = Documents::IO_TYPE_OUT;
            $model->document_date = $agreementDocument->document_date;
            $model->contractor_id = $agreementDocument->contractor_id;
            $model->comment_internal = $agreementDocument->comment_internal;
            $model->isAutoinvoice = 0;
            $model->company_id = $company->id;

            $model->populateRelation('company', $company);

            $model->document_number = (string)Invoice::getNextDocumentNumber($company->id, Documents::IO_TYPE_OUT, null, $model->document_date);
            $payment_delay = $agreementDocument->seller_payment_delay;

            $model->populateRelation('contractor', $agreementDocument->contractor);

            $model->nds_view_type_id = $company->hasNds() ?
                ($company->isNdsExclude ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) :
                Invoice::NDS_VIEW_WITHOUT;
            $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
            $invoiceSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $agreementDocument, $company) {

                if (!InvoiceHelper::save($model)) {
                    $db->transaction->rollBack();

                    return false;
                }

                $agreementDocument->invoice_id = $model->id;
                $agreementDocument->save(true, ['invoice_id']);

                return true;
            });

            if ($invoiceSaved) {
                if ($user->need_look_service_video == true) {
                    $user->identity->need_look_service_video = false;
                    $user->identity->save(false, ['need_look_service_video']);
                }

                $this->checkShowPopup();
                $invoiceCount = $company->getInvoices()->count();
                if ($invoiceCount == 1) {
                    $tooltips = [
                        'tooltip_send',
                        'tooltip_pdf',
                        'tooltip_paid',
                        'tooltip_account-view',
                        'tooltip_send-example',
                    ];

                    foreach ($tooltips as $key => $value) {
                        $name = "{$value}_{$model->company_id}";
                        if (!isset($_COOKIE[$name])) {
                            setcookie($name, "1", time() + (3600 * 24 * 365));
                        }
                    }
                }
                if ($invoiceCount == 3 || $invoiceCount == 6) {
                    Yii::$app->session->set('show_inquirer_popup', 1);
                }
                Yii::$app->session->setFlash('success', 'Счет создан.');

                return $this->_redirectAfterSave($model);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при создании счета.');
                ////var_dump(ActiveForm::validate($model));die;

                return $this->redirect(['/documents/agreement/view', 'id' => $agreementDocumentID]);
            }
        }
        throw new NotFoundHttpException('Agreement document not found');
    }


    /**
     * @param $orderDocumentID
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     */
    public function actionUpdateFromOrder($orderDocumentID)
    {
        $orderDocument = OrderDocument::findOne($orderDocumentID);
        if ($orderDocument) {
            $invoice = $orderDocument->invoice;
            $invoice->contractor_id = $orderDocument->contractor_id;
            $invoice->comment_internal = $orderDocument->comment_internal;
            $invoice->agreement = $orderDocument->agreement;
            $modelArray = array_merge(
                $invoice->getActs()->all(),
                $invoice->getPackingLists()->all(),
                $invoice->getInvoiceFactures()->all(),
                $invoice->getUpds()->all(),
                $invoice->getPaymentOrder()->all()
            );
            if ($modelArray) {
                foreach ($modelArray as $model) {
                    LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                }
            }
            foreach ($invoice->orders as $order) {
                $order->delete();
            }

            foreach ($orderDocument->orderDocumentProducts as $orderDocumentProduct) {
                $product = Product::getById($invoice->company_id, $orderDocumentProduct->product_id);
                if ($product !== null) {
                    $order = OrderHelper::createOrderByProduct(
                        $product,
                        $invoice,
                        $orderDocumentProduct->quantity,
                        $orderDocumentProduct->selling_price_with_vat,
                        ($invoice->has_discount ? $orderDocumentProduct->discount : 0)
                    );
                    $order->product_title = $orderDocumentProduct->product_title;
                    $orderArray[] = $order;
                }
            }
            array_walk($orderArray, function ($order, $key) {
                $order->number = $key + 1;
            });
            $invoice->populateRelation('orders', $orderArray);

            $invoice->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
            $invoice->total_place_count = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));
            if (InvoiceHelper::save($invoice, true, true)) {
                Yii::$app->session->setFlash('success', 'Счет обновлен.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при обновлении счета.');
            }

            return $this->_redirectAfterSave($invoice);

        }
        throw new NotFoundHttpException('Order document not found');
    }

    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($type, $id)
    {
        /* @var Invoice $model
         * @var Company $company
         */
        $model = $this->loadModel($id, $type);
        $company = Yii::$app->user->identity->company;
        if ($model->type == Documents::IO_TYPE_OUT && $company->isFreeTariff && date_create('first day of this month') > date_create($model->document_date)) {
            Yii::$app->session->setFlash('error', 'На бесплатном тарифе вы не можете редактировать счета от даты ранее, чем '.date('01.m.Y'));

            return $this->_redirectAfterSave($model);
        }
        if ($company->invoiceContractEssence) {
            $invoiceContractEssence = $company->invoiceContractEssence;
        } else {
            $invoiceContractEssence = new InvoiceContractEssence();
            $invoiceContractEssence->company_id = $company->id;
        }
        if ($company->invoiceEssence) {
            $invoiceEssence = $company->invoiceEssence;
        } else {
            $invoiceEssence = new InvoiceEssence();
            $invoiceEssence->company_id = $company->id;
        }

        if ($model->is_subscribe_invoice) {
            \Yii::$app->session->setFlash('error', "Вы не можете редактировать системный счет.");

            return $this->redirect(\Yii::$app->request->referrer);
        }

        if (!$model->updateAllowed()) {
            $this->redirect([
                'view',
                'type' => $type,
                'id' => $model->id,
                'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
            ]);
        }
        $model->show_article = Yii::$app->user->identity->config->invoice_form_article;

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());

            if (($contractor = $model->contractor) !== null && $contractor->nds_view_type_id !== null) {
                $model->nds_view_type_id = $contractor->nds_view_type_id;
            }

            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company,
                'fixedContractor' => false,
                'message' => new Message($this->typeDocument, $model->type),
                'ioType' => $type,
                'autoinvoice' => null,
                'invoiceContractEssence' => $invoiceContractEssence,
                'invoiceEssence' => $invoiceEssence,
            ]);
        }
        if (InvoiceHelper::load($model, Yii::$app->request->post())) {
            $invoiceDocumentLimitDate = $company->getInvoiceLimitDateForFreeTariff();
            if ($company->isFreeTariff && $model->type == Documents::IO_TYPE_OUT &&
                (strtotime($model->document_date) < strtotime($invoiceDocumentLimitDate['from']) ||
                    strtotime($model->document_date) > strtotime($invoiceDocumentLimitDate['to']))) {
                Yii::$app->session->setFlash('error', 'Для текущего тарифа БЕСПЛАТНО вы можете создавать счета за период с ' . $invoiceDocumentLimitDate['from'] . ' по ' . $invoiceDocumentLimitDate['to'] . '.');
            } else {
                if (InvoiceHelper::save($model, true, true)) {
                    $model->checkPaymentStatus();
                    if ($model->is_invoice_contract) {
                        $message = ($model->isAutoinvoice ? 'Шаблон АвтоСчёт-договора' : 'Счет-договор') . ' изменен.';
                    } else {
                        $message = ($model->isAutoinvoice ? 'Шаблон АвтоСчёта' : 'Счет') . ' изменен.';
                    }
                    Yii::$app->session->setFlash('success', $message);

                    if ($invoiceContractEssence->load(Yii::$app->request->post())) {
                        $invoiceContractEssence->text = $model->contract_essence;
                        if (($invoiceContractEssence->is_checked && $invoiceContractEssence->text) ||
                            !$invoiceContractEssence->is_checked) {
                            $invoiceContractEssence->save();
                        }
                    }
                    if ($invoiceEssence->load(Yii::$app->request->post())) {
                        $invoiceEssence->text = $model->comment;
                        if (($invoiceEssence->is_checked && $invoiceEssence->text) ||
                            !$invoiceEssence->is_checked) {
                            $invoiceEssence->save();
                        }
                    }

                    return $this->_redirectAfterSave($model);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'fixedContractor' => !$model->isChangeContractorAllowed,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'autoinvoice' => null,
            'invoiceContractEssence' => $invoiceContractEssence,
            'invoiceEssence' => $invoiceEssence,
            'projectId' => $model->project_id,
        ]);
    }

    /**
     * @param $id
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdateCompanyFields($id, $type, $isAuto = false)
    {
        /* @var $model Invoice */
        if ($isAuto) {
            $model = $this->loadModelAuto($id);
        } else {
            $model = $this->loadModel($id, $type);
        }
        $model->setCompany($model->company);

        $model->save(true, array_merge(Company::$invoiceRequisites, ['updated_at']));

        Yii::$app->session->setFlash('success', 'Информация по компании обновлена');

        if ($isAuto) {
            return $this->redirect(['update-auto', 'id' => $id]);
        } else {
            return $this->redirect(['update', 'id' => $id, 'type' => $model->type]);
        }
    }

    /**
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyUpdateCompanyFields($type, $isAuto = false)
    {
        $updated = 0;
        $ids = Yii::$app->request->post('ids');

        foreach ((array) $ids as $id) {
            /* @var $model Invoice */
            if ($isAuto) {
                $model = $this->loadModelAuto($id);
            } else {
                $model = $this->loadModel($id, $type);
            }
            if (Yii::$app->getUser()->can(permissions\document\Document::UPDATE, ['model' => $model])) {
                $model->setCompany($model->company);
                if ($model->save(true, array_merge(Company::$invoiceRequisites, ['updated_at']))) {
                    $updated++;
                } else {
                    \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Информация по компании обновлена');
        }

        return $this->redirect(Yii::$app->request->referrer ?? [$isAuto ? 'index-auto' : 'index']);
    }

    /**
     * @param $id
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdateContractorFields($id, $type, $isAuto = false)
    {
        /* @var $model Invoice */
        if ($isAuto) {
            $model = $this->loadModelAuto($id);
        } else {
            $model = $this->loadModel($id, $type);
        }
        if ($model->contractor) {
            $model->setContractor($model->contractor);
        }
        $model->save(true, ['contractor_name_short', 'contractor_name_full', 'contractor_director_name',
            'contractor_director_post_name', 'contractor_address_legal_full', 'contractor_bank_name', 'contractor_bank_city',
            'contractor_bik', 'contractor_inn', 'contractor_kpp', 'contractor_ks', 'contractor_rs']);

        Yii::$app->session->setFlash('success', 'Информация по покупателю обновлена');

        if ($isAuto) {
            return $this->redirect(['update-auto', 'id' => $id]);
        } else {
            return $this->redirect(['update', 'id' => $id, 'type' => $model->type]);
        }
    }

    /**
     * @param $id
     * @param $type
     * @param bool $isAuto
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyUpdateContractorFields($type, $isAuto = false)
    {
        $updated = 0;
        $fields = [
            'contractor_name_short',
            'contractor_name_full',
            'contractor_director_name',
            'contractor_director_post_name',
            'contractor_address_legal_full',
            'contractor_bank_name',
            'contractor_bank_city',
            'contractor_bik',
            'contractor_inn',
            'contractor_kpp',
            'contractor_ks',
            'contractor_rs',
        ];
        $ids = Yii::$app->request->post('ids');

        foreach ((array) $ids as $id) {
            /* @var $model Invoice */
            if ($isAuto) {
                $model = $this->loadModelAuto($id);
            } else {
                $model = $this->loadModel($id, $type);
            }
            if ($model->contractor && Yii::$app->getUser()->can(permissions\document\Document::UPDATE, ['model' => $model])) {
                $model->setContractor($model->contractor);
                if ($model->save(true, $fields)) {
                    $updated++;
                } else {
                    \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Информация по покупателю обновлена');
        }

        return $this->redirect(Yii::$app->request->referrer ?? [$isAuto ? 'index-auto' : 'index']);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateAuto($id)
    {
        /** @var InvoiceAuto $model */
        $model = $this->loadModelAuto($id, true);
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $autoinvoice = AutoinvoiceForm::findOne($model->id);
        $autoinvoice->populateRelation('invoice', $model);
        $model->populateRelation('auto', $autoinvoice);

        if ($company->invoiceContractEssence) {
            $invoiceContractEssence = $company->invoiceContractEssence;
        } else {
            $invoiceContractEssence = new InvoiceContractEssence();
            $invoiceContractEssence->company_id = $company->id;
        }
        //if ($company->invoiceContractTemplate) {
        //    $invoiceContractTemplate = $company->invoiceContractTemplate;
        //} else {
        //    $invoiceContractTemplate = new InvoiceContractTemplate();
        //    $invoiceContractTemplate->company_id = $company->id;
        //}
        if ($company->invoiceEssence) {
            $invoiceEssence = $company->invoiceEssence;
        } else {
            $invoiceEssence = new InvoiceEssence();
            $invoiceEssence->company_id = $company->id;
        }

        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());

            if (($contractor = $model->contractor) !== null && $contractor->nds_view_type_id !== null) {
                $model->nds_view_type_id = $contractor->nds_view_type_id;
            }

            return $this->renderAjax('create', [
                'model' => $model,
                'company' => $company,
                'fixedContractor' => false,
                'message' => new Message($this->typeDocument, $model->type),
                'ioType' => $model->type,
                'autoinvoice' => $autoinvoice,
                'invoiceContractEssence' => $invoiceContractEssence,
                //'invoiceContractTemplate' => $invoiceContractTemplate,
                'invoiceEssence' => $invoiceEssence,
            ]);
        }

        if (InvoiceHelper::load($model, Yii::$app->request->post()) &&
            $model->setCheckingAccountant(Yii::$app->user->identity->company) &&
            InvoiceHelper::save($model, true, true)
        ) {
            if ($invoiceContractEssence->load(Yii::$app->request->post())) {
                $invoiceContractEssence->text = $model->contract_essence;
                if (($invoiceContractEssence->is_checked && $invoiceContractEssence->text) ||
                    !$invoiceContractEssence->is_checked) {
                    $invoiceContractEssence->save();
                }
            }
            if ($invoiceEssence->load(Yii::$app->request->post())) {
                $invoiceEssence->text = $model->comment;
                if (($invoiceEssence->is_checked && $invoiceEssence->text) ||
                    !$invoiceEssence->is_checked) {
                    $invoiceEssence->save();
                }
            }

            return $this->_redirectAfterSave($model);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'fixedContractor' => false,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $model->type,
            'autoinvoice' => $autoinvoice,
            'invoiceContractEssence' => $invoiceContractEssence,
            //'invoiceContractTemplate' => $invoiceContractTemplate,
            'invoiceEssence' => $invoiceEssence,
            'projectId' => $model->project_id,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\web\ForbiddenHttpException
     *
     */
    public function actionDelete($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        Yii::$app->db->transaction(function (Connection $db) use ($model) {

            if ($model->deletingAll()) {
                Yii::$app->session->setFlash('success', 'Счет удален');

                return true;
            }
            \Yii::$app->session->setFlash('error', $model->getFirstError('id') ?: "Ошибка при удалении счета.");
            $db->transaction->rollBack();

            return false;
        });

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * NOTE: Don't works because of relation constraint.
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $type
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public
    function actionDeleteAuto($id)
    {
        /** @var InvoiceAuto $model */
        $model = $this->loadModelAuto($id, true);

        if ($model->auto->getInvoices()->byDeleted(false)->exists()) {
            \Yii::$app->session->setFlash('error', "Шаблон АвтоСчета удалить нельзя, т.к. уже созданы счета по данному шаблону");

            return $this->redirect(\Yii::$app->request->referrer);
        }

        LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE, function (InvoiceAuto $model) {
            $model->is_deleted = true;
            $model->auto->status = Autoinvoice::DELETED;

            return ($model->auto->save(false, ['status']) && $model->save(false, ['is_deleted']));
        });

        return $this->redirectAfterDelete($model->type, $model);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public
    function actionManyDelete($type, $contractor = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $invoices = Yii::$app->request->post('Invoice');
        $noDeletedInvoices = [];
        $deletedInvoices = [];
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                $model = Invoice::findOne([
                    'id' => $id,
                    'company_id' => Yii::$app->user->identity->company->id,
                    'type' => $type,
                    'is_deleted' => false,
                ]);
                if ($model) {
                    $deleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {

                        if ($model->deletingAll()) {
                            return true;
                        }
                        $db->transaction->rollBack();

                        return false;
                    });
                    if ($deleted) {
                        $deletedInvoices[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date));
                    } else {
                        $reasonText = !in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed) ? ' (счет оплачен)' : '';
                        $noDeletedInvoices[] = '№ ' . $model->fullNumber . ' от ' . date('d.m.Y', strtotime($model->document_date)) . $reasonText;
                    }
                }
            }
        }

        if (count($noDeletedInvoices) > 0) {
            \Yii::$app->session->setFlash('error', 'Счета не могут быть удалены:<br>' . implode('<br>', $noDeletedInvoices));
        }
        if (count($deletedInvoices) > 0) {
            \Yii::$app->session->setFlash('success', 'Счета успешно удалены:<br>' . implode('<br>', $deletedInvoices));
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }

    /**
     * @param Invoice $model
     * @param boolean $openEdit
     *
     * @return Response
     */
    private
    function _redirectAfterSave(Invoice $model, $openEdit = false)
    {
        $contractorId = Yii::$app->request->getQueryParam('contractorId');
        $contractorId = $contractorId !== null && Yii::$app->user->can(permissions\Contractor::VIEW) ? $model->contractor_id : null;

        if ($this->isScanPreview) {
            Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
            return $this->redirect(Url::previous('lastUploadManagerPage'));
        }

        return Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model]) ?
            ($model->isAutoinvoice ?
                $this->redirect([$openEdit ? 'update-auto' : 'view-auto', 'id' => $model->id, 'contractorId' => $contractorId,]) :
                $this->redirect([$openEdit ? 'update' : 'view', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,])
            ) :
            $this->redirect(['/site/index',]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $searchModel = new InvoiceSearch([
            'type' => Documents::IO_TYPE_IN,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        $dateRange = StatisticPeriod::getSessionPeriod();
        $contractorID = Yii::$app->request->post('contractor_id');

        $documentNumber = Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);
        if ($contractorID) {
            $dataProvider->query->andWhere(['contractor_id' => $contractorID]);
        }
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('partial/_modal_invoice_table', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentNumber' => $documentNumber,
        ]);
    }

    /**
     * @return array
     */
    public function actionCanAddOneDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $invoiceIds = Yii::$app->request->post('invoices');
        $canAddAct = $canAddUpd = $canAddInvoiceFacture = $canAddPackingList = $canAddSalesInvoice = true;
        $notNeedAct = $notNeedPackingList = $notNeedSalesInvoice = $notNeedUpd = false;
        $hasAct = $hasPackingList = $hasInvoiceFacture = $hasUpd = false;
        $canAddSomeActs = $canAddSomeUpds = $canAddSomePackingLists = false;
        /* @var $invoices Invoice[] */
        $invoices = Invoice::findAll([
            'id' => $invoiceIds,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if (Invoice::find()
                ->andWhere(['id' => $invoiceIds])
                ->groupBy(['contractor_id', 'invoice_expenditure_item_id'])
                ->count() == 1) {
            foreach ($invoices as $invoice) {
                if (!$invoice->getCanAddAct()) {
                    $canAddAct = false;
                }
                if (!$invoice->getCanAddUpd()) {
                    $canAddUpd = false;
                }
                if (!$invoice->getCanAddInvoiceFacture()) {
                    $canAddInvoiceFacture = false;
                }
                if (!$invoice->getCanAddPackingList()) {
                    $canAddPackingList = false;
                }
                if (!$invoice->getCanAddSalesInvoice()) {
                    $canAddSalesInvoice = false;
                }
            }
        } else {
            $canAddAct = $canAddUpd = $canAddInvoiceFacture = $canAddPackingList = $canAddSalesInvoice = false;
        }
        foreach ($invoices as $invoice) {
            if ($invoice->getCanAddAct() && !$invoice->has_act && $invoice->need_act) {
                $notNeedAct = true;
            }
            if ($invoice->getCanAddPackingList() && !$invoice->has_packing_list && $invoice->need_packing_list) {
                $notNeedPackingList = true;
            }
            if ($invoice->getCanAddSalesInvoice() && !$invoice->has_sales_invoice && $invoice->need_sales_invoice) {
                $notNeedPackingList = true;
            }
            if ($invoice->getCanAddUpd() && !$invoice->has_upd && $invoice->need_upd) {
                $notNeedUpd = true;
            }

            if ($invoice->has_act)
                $hasAct = true;
            if ($invoice->has_packing_list)
                $hasPackingList = true;
            if ($invoice->has_invoice_facture)
                $hasInvoiceFacture = true;
            if ($invoice->has_upd)
                $hasUpd = true;

            if ($invoice->getCanAddAct())
                $canAddSomeActs = true;
            if ($invoice->getCanAddUpd())
                $canAddSomeUpds = true;
            if ($invoice->getCanAddPackingList())
                $canAddSomePackingLists = true;
        }

        return [
            'canAddAct' => $canAddAct,
            'canAddUpd' => $canAddUpd,
            'canAddInvoiceFacture' => $canAddInvoiceFacture,
            'canAddPackingList' => $canAddPackingList,
            'canAddSalesInvoice' => $canAddSalesInvoice,
            'notNeedAct' => $notNeedAct,
            'notNeedPackingList' => $notNeedPackingList,
            'notNeedUpd' => $notNeedUpd,
            'hasAct' => $hasAct,
            'hasPackingList' => $hasPackingList,
            'hasInvoiceFacture' => $hasInvoiceFacture,
            'hasUpd' => $hasUpd,
            'canAddSomeActs' => $canAddSomeActs,
            'canAddSomeUpds' => $canAddSomeUpds,
            'canAddSomePackingLists' => $canAddSomePackingLists
        ];
    }

    /**
     * @param $type
     * @param $param
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionNotNeedDocument($type, $param)
    {
        $invoices = Yii::$app->request->post('Invoice');
        if (!in_array($param, array_flip(Invoice::$needDocumentTypesAttribute))) {
            throw new BadRequestHttpException('Invalid param.');
        }
        $attribute = Invoice::$needDocumentTypesAttribute[$param];
        $getter = Invoice::$canAddDocumentGetter[$param];
        foreach ($invoices as $id => $invoice) {
            if ($invoice['checked']) {
                $model = $this->loadModel($id, $type);
                if ($model->$getter) {
                    $model->$attribute = false;
                    $model->save(true, [$attribute]);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Счета обновлены.');

        return $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => $type]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetTemplates()
    {
        $this->layout = '@frontend/views/layouts/empty';
        $searchModel = Autoinvoice::find()->where(['type' => $this->typeDocument]);
        $dateRange = StatisticPeriod::getSessionPeriod();


        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        return $this->render('partial/_modal_template_table', [
            'tempSearchModel' => $searchModel,
            'tempDataProvider' => $dataProvider,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetInvoice()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $invoiceIDs = Yii::$app->request->post('InvoiceSearch')['id'] ?? [];
        $data = [];
        $response = [];
        $totalSum = 0;
        $totalCount = 0;
        $invoiceArray = $invoiceIDs ? Yii::$app->user->identity->company->getInvoices()->andWhere([
            'id' => $invoiceIDs,
            'type' => Documents::IO_TYPE_IN,
            'is_deleted' => false,
        ])->all() : [];

        foreach ($invoiceArray as $invoice) {
            $data[$invoice->contractor_id][] = $invoice;
            $totalSum += $invoice->remaining_amount;
            $totalCount += 1;
        }
        if (count($data) > 1) {
            $response['result'] = true;
            $response['contractorCount'] = count($data);
            foreach ($data as $contractorID => $invoices) {
                /* @var $contractor Contractor */
                $contractor = Contractor::findOne($contractorID);
                $contractorInvoiceAmount = 0;
                /* @var $invoices Invoice[] */
                foreach ($invoices as $invoice) {
                    $contractorInvoiceAmount += $invoice->remaining_amount;
                }
                $response['contractor'][$contractorID] = '. ' . $contractor->getTitle(true) . ': счета ' . count($invoices) . ' шт., сумма ' . TextHelper::invoiceMoneyFormat($contractorInvoiceAmount, 2) . '<i class="fa fa-rub" style="padding-left: 5px;"></i>';
            }
            $response['total'] = 'Итого на оплату: Счета ' . $totalCount . ' шт., Сумма ' . TextHelper::invoiceMoneyFormat($totalSum, 2) . '<i class="fa fa-rub" style="padding-left: 5px;"></i>';
            $response['invoices'] = $invoiceIDs;
        } elseif (count($data) === 1) {
            $invoice = reset($invoiceArray);
            $response = [
                'contractorCount' => 1,
                'totalAmount' => $totalSum,
                'company_bik' => $invoice->company_bik,
                'company_bank_name' => $invoice->company_bank_name,
                'company_bank_city' => $invoice->company_bank_city,
                'company_ks' => $invoice->company_ks,
                'company_rs' => $invoice->company_rs,
                'contractor_id' => $invoice->contractor_id,
                'contractor_name' => $invoice->contractor_name_short,
                'contractor_inn' => $invoice->contractor_inn,
                'contractor_bank_name' => $invoice->contractor_bank_name,
                'contractor_bank_city' => $invoice->contractor_bank_city,
                'contractor_current_account' => $invoice->contractor_rs,
                'contractor_corresponding_account' => $invoice->contractor_ks,
                'contractor_bik' => $invoice->contractor_bik,
                'contractor_kpp' => $invoice->contractor_kpp,
                'invoiceIDs' => serialize($invoiceIDs),
                'purpose_of_payment' => Invoice::getPurposeOfPaymentForInvoiceArray($invoiceArray),
            ];
        }

        return json_encode($response);
    }

    public
    function actionAddModalContractorFast()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new Contractor();
        $model->status = Contractor::ACTIVE;
        $scenario = Contractor::SCENARIO_LEGAL_FACE;
        $model->setScenario($scenario);
        $model->type = Yii::$app->request->post('documentType');

        $inn = Yii::$app->request->post('inn');
        $contractorAttributes = DadataClient::getContractorAttributes($inn);

        if ($contractorAttributes) {

            $model->setAttributes($contractorAttributes, false);

            if (empty($model->director_name)) {
                $model->director_name = 'ФИО руководителя';
            }

            $model->taxation_system = Contractor::WITHOUT_NDS;
            $model->chief_accountant_is_director = 1;
            $model->contact_is_director = 1;
            $model->setAttribute('company_id', Yii::$app->user->identity->company->id);
            $model->setAttribute('employee_id', Yii::$app->user->id);
            $model->setAttribute('object_guid', OneCExport::generateGUID());

            if ($model->save()) {

                return $data = [
                    'id' => $model->id,
                    'name' => $model->name,
                    'name-short' => $model->companyType ? $model->companyType->name_short : '',
                ];
            }
        }

        $header = $this->renderAjax('partial/create-contractor_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('partial/create-contractor_body', [
            'model' => $model,
            'documentType' => $model->type,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed|string
     */
    public
    function actionAddModalProduct()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;

        $documentType = Yii::$app->request->getBodyParam('documentType');
        $storeId = Yii::$app->request->getBodyParam('storeId');
        $product = Yii::$app->request->getBodyParam('Product');
        $document = Yii::$app->request->getBodyParam('document');
        $productionType = $product['production_type'];
        $model = $this->createHelper($productionType);
        if (isset($product['group_id'])) {
            $model->group_id = $product['group_id'];
        }
        $scenarioProd = $documentType == 1 ? 'zakup' : 'prod';
        $scenarioUslug = $productionType == 1 ? 'tovar' : 'uslug';
        $scenario = $scenarioProd . $scenarioUslug;
        $model->setScenario($scenario);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if ($documentType == Documents::IO_TYPE_OUT) {
            if ($company->companyTaxationType->osno) {
                $model->price_for_sell_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
            }
        } else {
            if ($company->companyTaxationType->osno) {
                $model->price_for_buy_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            }
        }

        if (!isset($product['flag'])) {
            if (Yii::$app->request->post('ajax') !== null) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->_prepareFromInvoice() && $model->save()) {
                return [
                    'items' => $model->getProductsByIds([$model->id], $company->id),
                    'services' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                    ])->count(),
                    'goods' => $company->getProducts()->byDeleted()->andWhere([
                        'production_type' => Product::PRODUCTION_TYPE_GOODS,
                    ])->count(),
                ];
            }
            if ($documentType == Documents::IO_TYPE_OUT) {
                $attribute = 'price_for_sell_with_nds';
            } else {
                $attribute = 'price_for_buy_with_nds';
            }
            if ($model->getAttribute($attribute) == 0) {
                $model->addError($attribute, 'Необходимо заполнить «' . $model->getAttributeLabel($attribute) . '».');
            }
        }
        $header = $this->renderPartial('partial/create-product_header', ['productionType' => $productionType]);
        $body = $this->renderAjax('partial/create-product_body', [
            'model' => $model,
            'documentType' => $documentType,
            'document' => $document,
            'storeId' => $storeId
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @param $productionType
     * @return Product
     */
    protected
    function createHelper($productionType)
    {
        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;

        return $model;
    }

    /**
     * @return array|mixed
     */
    public function actionAddModalDetailsAddress()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if ($model->load(Yii::$app->request->post()) && $checkingAccountant->load(Yii::$app->request->post())) {
            $modelValidate = $model->validate();
            $accountValidate = $checkingAccountant->validate();
            if ($modelValidate && $accountValidate) {
                $result = Yii::$app->db->transaction(function (Connection $db) use ($checkingAccountant, $model) {
                    if ($checkingAccountant->_save($model)) {
                        $model->populateRelation('mainCheckingAccountant', $checkingAccountant);
                        $model->chief_is_chief_accountant = true;
                        if ($model->setDirectorInitials() && $model->save()) {

                            return true;
                        }
                    }
                    $db->transaction->rollBack();

                    return false;
                });

                if ($result && $model->strict_mode == Company::OFF_STRICT_MODE) {
                    return [
                        'name' => $model->name_short,
                        'type' => $model->companyType->name_short,
                    ];
                }
            }
        }

        // First invoice
        if ($inn = Yii::$app->request->post('inn')) {
            $companyAttributes = DadataClient::getCompanyAttributes($inn);
            $model->setAttributes($companyAttributes, false);
        }

        $header = $this->renderAjax('partial/create-company_header', [
            'model' => $model,
        ]);
        $body = $this->renderAjax('partial/create-company_body', [
            'checkingAccountant' => $checkingAccountant,
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array|mixed
     */
    public function actionValidateCompanyModal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (($checkingAccountant = $model->mainCheckingAccountant) === null) {
            $checkingAccountant = new CheckingAccountant(['company_id' => $model->id, 'type' => CheckingAccountant::TYPE_MAIN]);
        }

        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        if (Yii::$app->request->isAjax && !Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $checkingAccountant->load(Yii::$app->request->post());

            return ActiveForm::validate($model, $checkingAccountant);
        }
    }

    /**
     * @return array|mixed|string
     */
    public
    function actionAddModalLogo()
    {
        /* @var $model Company */
        $model = Yii::$app->user->identity->company;

        if (Yii::$app->request->post('ajax') !== null) {
            return $this->ajaxValidate($model);
        }
        if (CompanyHelper::load($model, true) && CompanyHelper::validate($model, true) && CompanyHelper::save($model, true)) {
            return json_encode(['success' => true]);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $header = $this->renderAjax('partial/create-logo_header');
        $body = $this->renderAjax('partial/create-logo_body', [
            'model' => $model,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    /**
     * @return array
     */
    public
    function actionGetContractorExpenditureItem()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $contractor = Contractor::findOne(Yii::$app->request->post('id'));
        if ($contractor !== null && $contractor->invoice_expenditure_item_id !== null) {
            return $contractor->invoice_expenditure_item_id;
        }

        return 'null';
    }

    /**
     * @return string
     */
    public function actionNotShow()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($notShow = Yii::$app->request->post('notShow')) {
            Yii::$app->user->identity->knows_about_sending_invoice = (int)$notShow;
            Yii::$app->user->identity->save(false, ['knows_about_sending_invoice']);
        }

        return $notShow;
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionBasisDocument()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        $company = Yii::$app->user->identity->company;

        $delay = 10;
        $refresh = false;
        $invoiceCount = 0;
        $invoiceSum = 0;

        $model = new Invoice;
        $model->scenario = 'insert';
        $model->company_id = $company->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->price_precision = 2;

        $contractor = Contractor::findOne(['id' => Yii::$app->request->get('contractorId'), 'company_id' => $company->id]);

        if ($contractor) {
            $documentIoType = Yii::$app->request->get('type', $contractor->type);
            $delay = ($documentIoType == Documents::IO_TYPE_OUT)
                ? $contractor->customer_payment_delay
                : $contractor->seller_payment_delay;
            $refresh = $company->getInvoices()->andWhere([
                'contractor_id' => $contractor->id,
                'is_deleted' => false,
            ])->exists();
            $model->type = $documentIoType;
            $model->contractor_id = $contractor->id;
            $model->agreement = $contractor->last_basis_document;
            $model->populateRelation('contractor', $contractor);
            $invoiceStatistic = Invoice::find()
                ->select([
                    'invoice_count' => 'COUNT({{invoice}}.[[id]])',
                    'invoice_sum' => 'SUM(IFNULL({{invoice}}.[[total_amount_with_nds]], 0))',
                ])->where([
                    'and',
                    ['invoice.is_deleted' => 0],
                    ['invoice.type' => (int)Documents::IO_TYPE_OUT],
                    ['invoice.contractor_id' => (int)$contractor->id],
                    ['invoice.invoice_status_id' => InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][(int)Documents::IO_TYPE_OUT]],
                ])->asArray()->one();
            if ($invoiceStatistic['invoice_count']) {
                $invoiceCount = $invoiceStatistic['invoice_count'];
                $invoiceSum = TextHelper::invoiceMoneyFormat($invoiceStatistic['invoice_sum'], 2);
            }
        }

        return $this->renderPartial('form/_basis_document', [
            'model' => $model,
            'delay' => $delay,
            'refresh' => $refresh,
            'invoiceCount' => $invoiceCount,
            'invoiceSum' => $invoiceSum,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFieldProject()
    {
        $contractorId = (int)Yii::$app->request->get('contractorId', null);
        $projectId = (int)Yii::$app->request->get('projectId', null);

        $hasProject = Yii::$app->user->identity->menuItem->project_item;

        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        if (!$hasProject) {
            return "";
        }

        $company = Yii::$app->user->identity->company;

        $model = new Invoice;
        $model->scenario = 'insert';
        $model->company_id = $company->id;

        $projects = [];
        $actualProjectId = 0;

        if ($contractorId && (Contractor::find()->where(['id' => $contractorId, 'company_id' => $company->id])->exists())) {

            $projectIds = ProjectCustomer::find()
                ->joinWith('project', false)
                ->where(['project.company_id' => $company->id])
                ->andWhere(['project_customer.customer_id' => $contractorId])
                ->select('project.id')
                ->column();

            if ($projectIds) {
                $projects = Project::find()
                    ->select('name')
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS))
                    ->andWhere('start_date <= ' . (new Expression('current_date()')))
                    ->andWhere('end_date >= ' . (new Expression('current_date()')))
                    ->andWhere(['id' => $projectIds])
                    ->indexBy('id')
                    ->column();

                $actualProjectId = in_array($projectId, $projects) ? $projectId : ($projects ? array_key_first($projects) : 0);

                if (!$actualProjectId) {
                    $contractorAutoProject = ContractorAutoProject::findOne([
                        'contractor_id' => $contractorId,
                        'project_status' => Project::STATUS_INPROGRESS,
                        'new_invoices' => true
                    ]);

                    $actualProjectId = ($contractorAutoProject) ? $contractorAutoProject->project_id : null;
                }
            }
        }

        return $this->renderPartial('form/_project_selecting', [
            'model' => $model,
            'projects' => $projects,
            'projectId' => $projectId ?: 0,
            'actualProjectId' => $actualProjectId,
            'contractorId' => (bool)$model->contractor_id,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFieldProjectEstimate()
    {
        $contractorId = Yii::$app->request->get('contractorId', null);
        $projectId = Yii::$app->request->get('projectId', null);

        $hasProject = Yii::$app->user->identity->menuItem->project_item;

        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        if (!$hasProject) {
            return "";
        }

        $company = Yii::$app->user->identity->company;
        //$contractor = Contractor::findOne(['id' => $id, 'company_id' => $company->id]);

        $model = new Invoice;
        $model->scenario = 'insert';
        $model->company_id = $company->id;

        $estimates = ProjectEstimate::find()
            ->select('name')
            ->andWhere(['project_id' => $projectId])
            ->andFilterWhere(['customer_id' => $contractorId])
            ->indexBy('id')
            ->column();

        $actualEstimateId = ($estimates) ? array_key_first($estimates) : 0;

        return $this->renderPartial('form/_project_estimate_selecting', [
            'model' => $model,
            'estimates' => $estimates,
            'projectId' => $projectId ?: 0,
            'actualEstimateId' => $actualEstimateId,
            'contractorId' => (bool)$model->contractor_id,
        ]);
    }

    /**
     * @return string
     */
    public function actionUpd()
    {
        $this->layout = 'upd';

        return $this->render('upd');
    }

    /**
     *
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $idArray = [];
        $period = StatisticPeriod::getSessionPeriod();
        $models = Yii::$app->request->post('Invoice');
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $invoices = Invoice::find()
            ->andWhere(['id' => $idArray])
            ->andWhere([Invoice::tableName() . '.company_id' => Yii::$app->user->identity->company_id])
            ->orderBy(['`' . Invoice::tableName() . '`.`document_date`' => SORT_DESC,
                '`' . Invoice::tableName() . '`.`document_number` * 1' => SORT_DESC])
            ->all();

        Invoice::generateXlsTable($period, $invoices);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls($type)
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        \Yii::$app->response->getHeaders()->set('Content-Type', 'application/vnd.ms-excel');

        $period = StatisticPeriod::getSessionPeriod();

        $searchModel = new InvoiceSearch([
            'type' => $type,
            'company_id' => Yii::$app->user->identity->company->id,
        ]);

        $invoices = $searchModel->search(
            Yii::$app->request->queryParams,
            $period,
            $searchModel::SORT_PARAM_NAME_OUT,
            true
        );

        Invoice::generateXlsTable($period, $invoices);
        exit;
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUnpaid($type, $id)
    {
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        if ($model->cashBankFlows) {
            foreach ($model->cashBankFlows as $cashFlow) {
                $cashFlow->unlinkInvoice($model);
            }
        }
        if ($model->cashEmoneyFlows) {
            foreach ($model->cashEmoneyFlows as $cashFlow) {
                $cashFlow->unlinkInvoice($model);
                if (!$cashFlow->getInvoices()->exists()) {
                    $cashFlow->delete();
                }
            }
        }
        if ($model->cashOrderFlows) {
            foreach ($model->cashOrderFlows as $cashFlow) {
                $cashFlow->unlinkInvoice($model);
                if (!$cashFlow->getInvoices()->exists()) {
                    $cashFlow->delete();
                }
            }
        }
        $redirectUrl = Yii::$app->request->post('redirect');

        return $this->redirect($redirectUrl ?: Yii::$app->request->referrer);
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($type, $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);

        $model = $this->loadModel($id, $type);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSetCurrencyRate($type, $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);

        $loadData = [];
        $currencyParams = [
            'currency_name',
            'currency_amount',
            'currency_rate',
            'currency_rate_type',
            'currency_rateDate',
            'currency_rate_amount',
            'currency_rate_value',
            'currencyAmountCustom',
            'currencyRateCustom',
        ];

        if ($data = (array)Yii::$app->request->post('Invoice', [])) {
            foreach ($currencyParams as $param) {
                $loadData[$param] = ArrayHelper::getValue($data, $param, '');
            }
        }

        if ($model->load($loadData, '')) {
            $isSaved = Yii::$app->db->transaction(function ($db) use ($model) {
                $orderArray = [];
                foreach ($model->orders as $order) {
                    $order->populateRelation('invoice', $model);
                    $order->price = $order->price;
                    if (!$order->save()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                    $orderArray[] = $order;
                }
                $model->populateRelation('orders', $orderArray);

                if (!$model->save()) {
                    $db->transaction->rollBack();

                    return false;
                }

                return true;
            });
        }

        return [
            'content' => $this->renderPartial('view/_currency_info', ['model' => $model]),
        ];
    }

    /**
     * @param $type
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAmountOnDate($type, $id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Invoice $model */
        $model = $this->loadModel($id, $type);
        $rateArray = [];

        if ($date = \DateTime::createFromFormat('d.m.Y|', Yii::$app->request->post('date'))) {
            $rateArray = CurrencyRate::getRateOnDate($date);
            if ($rateArray && isset($rateArray[$model->currency_name])) {
                if ($model->currency_name != Currency::DEFAULT_NAME && isset($rateArray[$model->currency_name])) {
                    $amount = $rateArray[$model->currency_name]['amount'];
                    $rate = $rateArray[$model->currency_name]['value'];
                    $model->recalculateByCarrencyRate($amount, $rate);

                    return [
                        'result' => true,
                        'rate' => $rateArray[$model->currency_name],
                        'available' => $model->availablePaymentAmount / 100,
                        'invoice' => $model,
                    ];
                }
            }
        }

        return [
            'result' => false,
        ];
    }

    public function actionHideScanPopup()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($showScan = Yii::$app->request->post('showed_scan')) {
            Yii::$app->user->identity->show_scan_popup = 0;
            Yii::$app->user->identity->save(false, ['show_scan_popup']);
            Yii::$app->session->remove(Employee::SHOW_SCAN_POPUP_SESSION_KEY);
        }
        return $showScan;
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionChangeContractorSignature($type, $id)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        /* @var Invoice $model */
        $model = $this->loadModel($id, $type, true);

        $status = Yii::$app->request->post('status');
        $invoiceContractorSignature = $company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $company->id;
        }
        $invoiceContractorSignature->is_active = (int)$status;
        $invoiceContractorSignature->save();

        return $this->redirect(['view', 'type' => $type, 'id' => $id,]);
    }

    public function actionRunScanPopup()
    {
        if (Yii::$app->user->identity->show_scan_popup) {
            Yii::$app->session->set(Employee::SHOW_SCAN_POPUP_SESSION_KEY, 1);
        }
    }

    private function checkShowScanPopup()
    {
        if (Yii::$app->user->identity->canShowScanPopup()) {
            Yii::$app->user->identity->show_scan_popup = 1;
            Yii::$app->user->identity->save(false, ['show_scan_popup']);
        }
    }

    public function getIsPaid($company)
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'b2b-paid', false)) {
            return true;
        }

        return $company->getHasActualSubscription(SubscribeTariffGroup::B2B_PAYMENT);
    }

    public function actionGetContractorInfo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $discount = 0;
        $paymentLimitRule = [
            'payment_limit_rule' => Invoice::PAYMENT_LIMIT_RULE_INVOICE,
            'payment_limit_days' => 10,
            'payment_limit_percent' => 100,
            'related_document_payment_limit_days' => 0,
            'related_document_payment_limit_percent' => 0,
        ];

        $contractor = Contractor::findOne(Yii::$app->request->post('contractor_id'));
        if ($contractor) {
            $type = Yii::$app->getRequest()->get('type') ?? $contractor->type;
            $discount = ($type == Documents::IO_TYPE_IN)
                ? $contractor->seller_discount
                : $contractor->customer_discount;

            if (Yii::$app->getRequest()->post('with_payment_rules')) {
                /** @var Invoice $lastContractorInvoice */
                $lastContractorInvoice = Invoice::find()
                    ->where([
                        'contractor_id' => $contractor->id,
                        //'is_deleted' => 0
                    ])
                    ->orderBy([
                        'created_at' => SORT_DESC
                    ])
                    ->one();
                if ($lastContractorInvoice) {
                    $paymentLimitRule = [
                        'date' => $lastContractorInvoice->document_date,
                        'number' => $lastContractorInvoice->document_number,
                        'payment_limit_rule' => $lastContractorInvoice->payment_limit_rule,
                        'payment_limit_days' => $contractor->customer_payment_delay,
                        'payment_limit_percent' => $lastContractorInvoice->payment_limit_percent,
                        'related_document_payment_limit_days' => $lastContractorInvoice->related_document_payment_limit_days,
                        'related_document_payment_limit_percent' => $lastContractorInvoice->related_document_payment_limit_percent,
                    ];
                } else {
                    $paymentLimitRule = [
                        'payment_limit_rule' => Invoice::PAYMENT_LIMIT_RULE_INVOICE,
                        'payment_limit_days' => $contractor->customer_payment_delay,
                        'payment_limit_percent' => 100,
                        'related_document_payment_limit_days' => 0,
                        'related_document_payment_limit_percent' => 0,
                    ];
                }
            }
        }

        return [
            'discount' => $discount,
            'paymentLimitRules' => $paymentLimitRule
        ];
    }

    protected function validateNewDocumentFields($company, $model, $documentSlug, $newDocFields)
    {
        if ($documentSlug == Documents::SLUG_ACT)
            $newDocClass = Documents::getModel(Documents::DOCUMENT_ACT);
        elseif ($documentSlug == Documents::SLUG_PACKING_LIST)
            $newDocClass = Documents::getModel(Documents::DOCUMENT_PACKING_LIST);
        elseif ($documentSlug == Documents::SLUG_INVOICE_FACTURE)
            $newDocClass = Documents::getModel(Documents::DOCUMENT_INVOICE_FACTURE);
        elseif ($documentSlug == Documents::SLUG_UPD)
            $newDocClass = Documents::getModel(Documents::DOCUMENT_UPD);
        else
            die('Unknown IN Document Type.');

        /** @var AbstractDocument $newDocModel */
        $newDocModel = new $newDocClass;

        $companyInvoicesIds = Invoice::find()
            ->where(['company_id' => $company->id])
            ->andWhere(['type' => Documents::IO_TYPE_IN])
            ->andWhere('YEAR([[document_date]]) = :year', [':year' => date('Y', strtotime($newDocFields['document_date']))])
            ->select('id')
            ->column();

        $newDocNumberExists = $newDocModel::find()
            ->andWhere(['invoice_id' => $companyInvoicesIds])
            ->andWhere(['document_number' => $newDocFields['document_number']])
            ->andWhere('YEAR([[document_date]]) = :year', [':year' => date('Y', strtotime($newDocFields['document_date']))])
            ->andWhere(['type' => Documents::IO_TYPE_IN])->exists();

        if (!$newDocFields['document_number']) {
            $newDocModel->addError('document_number', 'Необходимо заполнить номер документа');
        }
        if (!$newDocFields['document_date']) {
            $newDocModel->addError('document_date', 'Необходимо заполнить дату документа');
        }
        if (!($checkDate = DateTime::createFromFormat('Y-m-d', $newDocFields['document_date']))) {
            $newDocModel->addError('document_date', 'Неверно заполнена дата документа');
        }
        if ($newDocNumberExists) {
            $newDocModel->addError('document_number', 'Такой номер документа уже существует, одинаковые номера не допустимы');
        }

        if ($newDocErrors = $newDocModel->getErrors()) {
            foreach ($newDocErrors as $attr => $errors) {
                foreach ($errors as $error) {
                    if ($documentSlug == Documents::SLUG_ACT)
                        $error = str_replace('документа', 'акта', $error);
                    elseif ($documentSlug == Documents::SLUG_PACKING_LIST)
                        $error = str_replace('документа', 'ТН', $error);
                    elseif ($documentSlug == Documents::SLUG_INVOICE_FACTURE)
                        $error = str_replace('документа', 'СФ', $error);
                    elseif ($documentSlug == Documents::SLUG_UPD)
                        $error = str_replace('документа', 'УПД', $error);
                    $model->addError($attr, $error);
                }
            }

            return false;

        }

        return true;
    }
}
