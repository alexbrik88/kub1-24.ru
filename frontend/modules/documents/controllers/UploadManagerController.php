<?php

namespace frontend\modules\documents\controllers;


use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\UploadedFile;
use common\models\address\Country;
use common\models\Company;
use common\models\Contractor;
use common\models\document\AbstractDocument;
use common\models\document\ScanDocument;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\file\FileDir;
use common\models\file\FileUploadType;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\product\ProductUnit;
use common\models\scanRecognize\ScanRecognize;
use common\models\scanRecognize\ScanRecognizeContractor;
use common\models\scanRecognize\ScanRecognizeParams;
use common\models\scanRecognize\ScanRecognizeProduct;
use common\models\TaxRate;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadEmailHelper;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\models\AgreementSearch;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\documents\models\ScanUploadEmail;
use frontend\modules\documents\models\UploadManagerFileSearch;
use frontend\modules\documents\models\UploadManagerInvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use php_rutils\RUtils;
use Yii;
use yii\db\Connection;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * Class UploadManagerController
 * @package frontend\modules\documents\controllers
 */
class UploadManagerController extends FrontendController
{
    public $layout = 'upload-manager';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'upload',
                            'all-documents',
                            'directory',
                            'trash',
                            'create-dir',
                            'update-dir',
                            'remove-dir',
                            'move-dir',
                            'many-delete',
                            'many-restore',
                            'move-files-to',
                            'create-scan',
                            'get-invoices',
                            'redirect-to-create-document',
                            'attach-file',
                            'preview-scans',
                            'get-email-upload-modal',
                            'recognize-scan',
                            'check-recognize-status',
                            'get-products',
                            'get-products-table',
                            'add-modal-product',
                            'set-recognized-contractor-id',
                            'set-recognized-product-id',
                            'many-recognize',
                        ],
                        'allow' => true,
                        'roles' => [
                            UserRole::ROLE_CHIEF,
                            UserRole::ROLE_ACCOUNTANT,
                            UserRole::ROLE_SUPERVISOR,
                        ],
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\Contractor::CREATE, [
                                'type' => Yii::$app->request->post('documentType'),
                            ]);
                        },
                    ],
                ],
            ],
        ];
    }

    public function init()
    {
        parent::init();

        if (!Yii::$app->session->get('isTrashCleared') && !Yii::$app->user->isGuest) {
            $this->clearTrash();
            Yii::$app->session->set('isTrashCleared', true);
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    public function afterAction($action, $result)
    {
        $request = Yii::$app->getRequest();
        if (!$request->getIsAjax() && $action->id != 'preview-scans') {
            Url::remember(Yii::$app->request->url, 'lastPreviewScansPage');
        }

        return parent::afterAction($action, $result);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        Url::remember(Yii::$app->request->referrer, 'UploadManagerReturn');

        return $this->redirect('upload');
    }

    public function actionUpload()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        /** @var ScanUploadEmail $scanUploadEmail */
        $scanUploadEmail = ScanUploadEmail::findOne([
            'company_id' => $company->id,
            'enabled' => true
        ]) ?: new ScanUploadEmail();

        if ($scanUploadEmail->enabled) {
            if ($newScans = UploadEmailHelper::getScans($scanUploadEmail)) {

                $saved = UploadEmailHelper::saveScans($newScans);

                $msg = 'Загрузка сканов через email';
                if (UploadEmailHelper::$savedCount['success'] > 0) {
                    $msg .= '<br/>' . '- успешных загрузок: ' . RUtils::numeral()->getPlural( UploadEmailHelper::$savedCount['success'], ['файл', 'файла', 'файлов']);
                }
                if (UploadEmailHelper::$savedCount['error'] > 0) {
                    $msg .= '<br/>' . '- ошибок загрузки: ' . RUtils::numeral()->getPlural( UploadEmailHelper::$savedCount['success'], ['файл', 'файла', 'файлов']);
                }
                if ($saved) {
                    UploadEmailHelper::deleteEmails();
                    Yii::$app->session->setFlash('success', $msg);
                } else {
                    Yii::$app->session->setFlash('error', $msg);
                }
            }

            UploadEmailHelper::closeConnection();
        }

        if (!ScanDocument::getNewScansCount($company)) {

            return $this->render('upload-first-step', [
                'company' => $company,
                'scanUploadEmail' => $scanUploadEmail,
            ]);
        }

        $searchModel = new UploadManagerFileSearch([
            'company_id' => $company->id,
            'find_free_scans' => true,
            'find_new_scans' => true
        ]);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 20);

        return $this->render('files-list', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Распознавание',
            'directory' => null,
            'isUploads' => true
        ]);
    }

    public function actionRecognizeScan($scan_id)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $hasRecognize = ScanRecognize::find()->where(['scan_id' => $scan_id])->exists();
        $scan = ScanDocument::findOne(['id' => $scan_id, 'company_id' => $company->id]);

        if ($hasRecognize) {
            throw new NotFoundHttpException('Скан уже отправлен.');
        } elseif (!$scan) {
            throw new NotFoundHttpException('Скан не найден.');
        }

        $scanRecognize = new ScanRecognize();
        $scanRecognize->scan_id = $scan_id;
        if ($scanRecognize->send($scan->file)) {
            $scanRecognize->save();
        } else {
            Yii::$app->session->setFlash('error', 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.');
        }

        $searchModel = new UploadManagerFileSearch([
            'company_id' => $company->id,
            'find_free_scans' => true,
            'find_new_scans' => true
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 20);

        return $this->render('files-list', [
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Распознавание',
            'directory' => null,
            'isUploads' => true
        ]);
    }


    /**
     * @return Response
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyRecognize()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $isChief = $company->getEmployeeChief()->id == Yii::$app->user->id;

        $idArray = Yii::$app->request->post('File', []);
        foreach ($idArray as $id) {
            $file = File::findOne(['id' => $id, 'company_id' => $company->id]);
            $scan = UploadManagerHelper::getScanDocument($file);

            if ($scan) {
                $hasRecognize = ScanRecognize::find()->where(['scan_id' => $scan->id])->exists();

                if ($hasRecognize) {
                    continue;
                }

                $scanRecognize = new ScanRecognize();
                $scanRecognize->scan_id = $scan->id;
                if ($scanRecognize->send($scan->file)) {
                    $scanRecognize->save();
                } else {
                    Yii::$app->session->setFlash('error', 'Что-то пошло не так. Повторите действие позже или обратитесь в службу поддержки.');
                }
            }

        }

        Yii::$app->session->setFlash('success', 'Файлы распознаются.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionCheckRecognizeStatus($scan_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $scan = ScanDocument::findOne(['id' => $scan_id, 'company_id' => $company->id]);

        if ($scan) {
            if ($scan->recognize) {
                if ($scan->recognize->getData($company)) {
                    return ['result' => true];
                }
            } else {
                return ['result' => false, 'message' => 'Скан не отправлен на распознавание.'];
            }
        } else {
            return ['result' => false, 'message' => 'Скан не найден.'];
        }

        return ['result' => false];
    }

    public function actionAllDocuments()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $searchModel = new UploadManagerFileSearch([
            'company_id' => $company->id,
        ]);
        $searchModel->load(Yii::$app->request->post());
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 20);

        return $this->render('files-list', [
            'title' => 'Все документы',
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'directory' => null
        ]);
    }

    public function actionDirectory($id)
    {
        /** @var $model FileDir */
        $model = $this->findAllowedDirModel($id);

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $searchModel = new UploadManagerFileSearch([
            'company_id' => $company->id,
            'directory_id' => $model->id
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 20);

        return $this->render('files-list', [
            'title' => $model->name,
            'model' => $model,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'directory' => $model
        ]);
    }

    public function actionTrash()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $user = Yii::$app->user;

        $searchModel = new UploadManagerFileSearch([
            'company_id' => $company->id,
            'created_at_author_id' => $user->id,
            'is_deleted' => true,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get('per-page', 20);

        return $this->render('files-list', [
            'title' => 'Корзина',
            'isTrash' => true,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateDir()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $model = new FileDir([
            'company_id' => $company->id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

                if ($model->save())
                    if ($model->insertEmployersLinks())
                        return true;

                Yii::$app->session->setFlash('error', 'Ошибка при сохранении.');
                $db->transaction->rollBack();

                return false;
            });

            if ($saved) {
                Yii::$app->session->setFlash('success', 'Папка создана');
                return $this->redirect('/documents/upload-manager/directory/' . $model->id);
            }
        }

        return $this->renderAjax('_partial/_createDirectoryForm', [
            'action' => 'create-dir',
            'model' => $model,
        ]);
    }

    public function actionUpdateDir($id)
    {
        $model = $this->findAllowedDirModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $saved = Yii::$app->db->transaction(function (Connection $db) use ($model) {

                if ($model->save())
                    if ($model->updateEmployersLinks())
                        return true;

                Yii::$app->session->setFlash('error', 'Ошибка при обновлении.');
                $db->transaction->rollBack();

                return false;
            });

            if ($saved) {
                Yii::$app->session->setFlash('success', 'Папка обновлена');
                return $this->redirect('/documents/upload-manager/directory/' . $model->id);
            }
        }

        return $this->renderAjax('_partial/_createDirectoryForm', [
            'action' => 'update-dir',
            'model' => $model,
        ]);
    }

    public function actionRemoveDir($id)
    {
        /** @var FileDir $model */
        $model = $this->findAllowedDirModel($id);

        if ($model->files) {
            Yii::$app->session->setFlash('error', 'Перед удалением папки, просьба удалить все документы из папки или перенести их в другую папку');
            return $this->redirect(Yii::$app->request->referrer ?: '/documents/upload-manager/all-documents');
        }

        $deleted = Yii::$app->db->transaction(function (Connection $db) use ($model) {

            if ($model->deleteEmployersLinks())
                if ($model->delete())
                    return true;

            Yii::$app->session->setFlash('error', 'Ошибка при удалении.');
            $db->transaction->rollBack();

            return false;
        });

        if  ($deleted) {
            Yii::$app->session->setFlash('success', 'Папка удалена');
        }

        return $this->redirect('/documents/upload-manager/all-documents');
    }

    public function actionMoveDir()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        $model = $this->findAllowedDirModel($id);
        $direction = Yii::$app->request->post('direction');

        return ['swap_id' => $model->move($direction)];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyDelete()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $isChief = $company->getEmployeeChief()->id == Yii::$app->user->id;

        $idArray = Yii::$app->request->post('File', []);
        foreach ($idArray as $id) {
            $model = File::findOne(['id' => $id, 'company_id' => $company->id]);

            if ($model) {

                $canDelete = ($isChief || $model->created_at_author_id == Yii::$app->user->id);

                if (!$canDelete) {
                    Yii::$app->session->setFlash('error', 'Нет прав на удаление Файла <b>' . $model->filename_full.'</b>');
                    return $this->redirect(Yii::$app->request->referrer);
                }

                if ($model->is_deleted) {
                    $model->delete();
                } else {
                    UploadManagerHelper::moveFileToTrash($model);
                }
            }
        }
        Yii::$app->session->setFlash('success', 'Файлы удалены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionManyRestore()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $isChief = $company->getEmployeeChief()->id == Yii::$app->user->id;

        $idArray = Yii::$app->request->post('File', []);
        foreach ($idArray as $id) {
            $model = File::findOne(['id' => $id, 'company_id' => $company->id]);

            if ($model) {

                $canRestore = ($isChief || $model->created_at_author_id == Yii::$app->user->id);

                if (!$canRestore) {
                    Yii::$app->session->setFlash('error', 'Нет прав на восстановление Файла <b>' . $model->filename_full.'</b>');
                    return $this->redirect(Yii::$app->request->referrer);
                }

                $model->updateAttributes([
                    'is_deleted' => 0
                ]);
            }
        }
        Yii::$app->session->setFlash('success', 'Файлы восстановлены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionMoveFilesTo($directory_id)
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        if ($directory_id) {
            $dir = FileDir::findOne($directory_id);
            if (!$dir->checkEmployeeAccess()) {
                Yii::$app->session->setFlash('error', 'У Вас нет прав на данную папку');
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            $directory_id = null; // move to uploads
        }

        $idArray = Yii::$app->request->post('File', []);
        foreach ((array)$idArray as $id) {
            $model = File::findOne(['id' => $id, 'company_id' => $company->id]);
            if ($model) {
                $model->updateAttributes([
                    'directory_id' => $directory_id
                ]);
            }
        }
        Yii::$app->session->setFlash('success', 'Файлы перемещены.');

        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findAllowedDirModel($id)
    {
        $model = FileDir::findOne([
            'id' => $id,
            'company_id' => Yii::$app->user->identity->company->id
        ]);

        if ($model !== null) {
            if ($model->checkEmployeeAccess()) {

                return $model;
            }
            throw new ForbiddenHttpException('У Вас нет прав на данное действие.');
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }

    public function actionCreateScan()
    {
        $model = new ScanDocument([
            'scenario' => ScanDocument::SCENARIO_CREATE,
            'company_id' => Yii::$app->user->identity->company->id,
            'employee_id' => Yii::$app->user->id,
        ]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                if ($dir_id = Yii::$app->request->get('dir_id')) {

                    $dirModel = FileDir::findOne($dir_id);

                    if ($dirModel && $dirModel->checkEmployeeAccess() && $model->file) {
                        $model->file->directory_id = $dirModel->id;
                        $model->file->updateAttributes(['directory_id']);
                    }
                }


                Yii::$app->session->setFlash('success', 'Скан документа загружен');
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось загрузить скан документа.');
            }

            if (Yii::$app->request->post('dz')) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return true;
            }

            return $this->redirect(Yii::$app->request->referrer ? : ['all-documents']);
        } elseif (Yii::$app->request->isAjax) {
            return $this->renderAjax('_scan_document/_form', [
                'model' => $model,
            ]);
        }

        return $this->redirect(Yii::$app->request->referrer ? : ['all-documents']);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        // Agreements has own table
        if (Yii::$app->request->get('doc_type') == Documents::DOCUMENT_AGREEMENT) {
            return $this->actionGetAgreements();
        }

        $this->layout = '@frontend/views/layouts/empty';

        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom = \Yii::$app->request->get('date_from');
        $dateTo = \Yii::$app->request->get('date_to');

        $ioType  = \Yii::$app->request->get('type');
        $docType = \Yii::$app->request->get('doc_type');
        $files   = \Yii::$app->request->get('files');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to' => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new UploadManagerInvoiceSearch([
            'type' => $ioType,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange, $ioType, $docType);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        $view = ($ioType == Documents::IO_TYPE_IN) ? '_invoices_in_modal_table' : '_invoices_out_modal_table';

        return $this->renderAjax("_partial/{$view}", [
            'documentTypeName' => isset(Message::$messageAttachTo[$ioType][$docType]) ? Message::$messageAttachTo[$ioType][$docType] : '',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'docType' => $docType,
            'files' => $files,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName,
            'canChangePeriod' => true,
        ]);
    }

    /**
     * @return array
     */
    public function actionRedirectToCreateDocument()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ioType  = \Yii::$app->request->post('type');
        $docType = \Yii::$app->request->post('doc_type');
        $files   = \Yii::$app->request->post('files');

        if ($docType == Documents::DOCUMENT_AGREEMENT) {
            return [
                'redirect_url' => Url::to([
                        '/documents/agreement/create-page',
                        'mode' => 'previewScan',
                        'permanentAttachFiles' => 1,
                        'files' => $files,
                        'type' => $ioType,
                    ])
            ];
        }

        $extraParams = [];
        if ($docType == Documents::DOCUMENT_ACT) {
            $docSlug = Documents::SLUG_ACT;
        } elseif ($docType == Documents::DOCUMENT_PACKING_LIST) {
            $docSlug = Documents::SLUG_PACKING_LIST;
        } elseif ($docType == Documents::DOCUMENT_INVOICE_FACTURE) {
            $docSlug = Documents::SLUG_INVOICE_FACTURE;
        } elseif ($docType == Documents::DOCUMENT_UPD) {
            $docSlug = Documents::SLUG_UPD;
        } else {
            $docSlug = null;
            $extraParams['permanentAttachFiles'] = 1;
        }

        return [
            'redirect_url' => Url::to([
                '/documents/invoice/create',
                'mode' => 'previewScan',
                'files' => $files,
                'type' => $ioType,
                'document' => $docSlug
            ] + $extraParams)
        ];
    }

    public function actionAttachFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ioType  = \Yii::$app->request->post('type');
        $docType = \Yii::$app->request->post('doc_type');
        $files   = \Yii::$app->request->post('files');
        $id      = \Yii::$app->request->post('attached_doc_id');

        if ($model = Documents::loadModel($id, $docType, $ioType)) {
            if (UploadManagerHelper::attachFiles($model, $files)) {
                /** @var $model AbstractDocument */
                $model->updateHasFile();
                UploadManagerHelper::setAttachFilesMessage($model, $files);

                Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());

                return ['success' => true];
            }

            Yii::$app->session->setFlash('error', 'Ошибка при сохранении');
            return ['success' => false];
        }

        Yii::$app->session->setFlash('error', 'Ошибка при сохранении.');
        return ['success' => false];
    }

    public function actionPreviewScans($dir_id = null, $file_id = null)
    {
        $this->layout = 'preview-scan';
        if ($isUploads = $dir_id == 'upload')
            $dir_id = null;

        if ($dir_id && !$this->findAllowedDirModel($dir_id)) {
            throw new ForbiddenHttpException('У Вас нет прав на просмотр папки.');
        }

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $searchModel = new UploadManagerFileSearch([
            'by_directory' => (bool)$dir_id,
            'company_id' => $company->id,
            'directory_id' => $dir_id,
            'find_new_scans' => $isUploads,
            'find_free_scans' => $isUploads
        ]);

        return $this->render('preview-scans', [
            'filesIds' => $searchModel->searchFilesIds(),
            'startFileId' => $file_id
        ]);
    }

    public function actionGetEmailUploadModal()
    {
        $this->layout = '@frontend/views/layouts/empty';

        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $model = ScanUploadEmail::findOne([
            'company_id' => $company->id,
        ]);

        // First create
        if (!$model) {
            $model = new ScanUploadEmail();
            $model->enabled = false;
            $model->company_id = $company->id;
            do {
                $model->email = ScanUploadEmail::generateEmail($company->getShortName());
            } while (!$model->validate(['email']));

            $model->save(false);
        }

        $oldEmail = $model->email;
        if ($model->load(Yii::$app->request->post())) {
            $model->email = $model->customEmailPart .'.'. $model->basisEmailPart;
            if ($model->validate() && $model->validateBasisPart($oldEmail)) {
                $model->save();
                if (UploadEmailHelper::createMailbox($model)) {
                    Yii::$app->session->setFlash('success', 'Email включен.');
                    $model->updateAttributes(['enabled' => true]);
                } else {
                    Yii::$app->session->setFlash('error', 'Не удалось включить Email.<br/>Ошибка: ' . UploadEmailHelper::$lastErrorCode);
                    $model->updateAttributes(['enabled' => false]);
                }

                return $this->redirect(Yii::$app->request->referrer ?: '/documents/upload-manager/upload');
            }
        }

        return $this->renderAjax('_partial/_email_upload_modal_content', [
            'model' => $model,
            'oldEmail' => $oldEmail
        ]);
    }

    protected function clearTrash()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;

        $files = File::find()->where([
            'company_id' => $company->id,
            'is_deleted' => true,
        ])->andWhere(['<', 'created_at', time() - File::$lifetimeInTrash])->all();

        /** @var File $file */
        foreach ($files as $file) {
            if ($file->owner_model == ScanDocument::className()) {
                if ($scan = ScanDocument::findOne($file->owner_id))
                    $scan->delete();
            } else {
                $file->delete();
            }
        }
    }

    /**
     * @return string
     */
    public function actionGetAgreements()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom = \Yii::$app->request->get('date_from');
        $dateTo = \Yii::$app->request->get('date_to');

        $ioType  = \Yii::$app->request->get('type');
        $docType = \Yii::$app->request->get('doc_type');
        $files   = \Yii::$app->request->get('files');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to' => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new AgreementSearch([
            'type' => $ioType,
            'has_file' => false,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);

        $view = ($ioType == Documents::IO_TYPE_IN) ? '_agreements_in_modal_table' : '_agreements_out_modal_table';

        return $this->renderAjax("_partial/{$view}", [
            'documentTypeName' => isset(Message::$messageAttachTo[$ioType][$docType]) ? Message::$messageAttachTo[$ioType][$docType] : '',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'docType' => $docType,
            'files' => $files,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName,
            'canChangePeriod' => true,
        ]);
    }

    /**
     * @return string|NotFoundHttpException
     */
    public function actionGetProducts()
    {
        $company = Yii::$app->user->identity->company;
        $documentType = Yii::$app->request->get('documentType', -1);

        $title = Yii::$app->request->get('title');
        $searchTitle = UploadManagerHelper::getSimilarProductsTitle($company, $title);
        $customSearchTitle = Yii::$app->request->post('searchTitle');

        $searchModel = new ProductSearch();
        $searchModel->documentType = $documentType;
        $searchModel->production_type = [0,1];
        $searchModel->company_id = $company->id;
        $searchModel->title = $customSearchTitle ?: $searchTitle;
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->pagination->pageSize = 10;

        $this->layout = 'empty';

        return $this->render('_partial/_modal_find_product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentType' => $documentType,
            'productType' => null,
            'storeId' => null
        ]);
    }


    /**
     * @param $recognizedId
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionGetProductsTable($recognizedId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $inOrder = Yii::$app->request->post('in_order');
        $productId = ArrayHelper::getValue($inOrder, 0);
        $product = Product::findOne(['id' => $productId, 'company_id' => $company->id]);
        $rProduct = ScanRecognizeProduct::findOne($recognizedId);

        if (!$product)
            throw new NotFoundHttpException('Product not found');
        if (!$rProduct)
            throw new NotFoundHttpException('RecognizedProduct not found');

        $rProduct->updateAttributes(['product_id' => $product->id]);

        return $product->getProductsByIds([$productId], Yii::$app->user->identity->company->id);
    }

    /**
     * @param int $recognizedId
     * @return array|mixed
     * @throws NotFoundHttpException
     */
    public function actionAddModalProduct($recognizedId)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;

        $rProduct = ScanRecognizeProduct::findOne($recognizedId);

        if (!$rProduct)
            throw new NotFoundHttpException('RecognizedProduct not found');

        $documentType = Yii::$app->request->getBodyParam('document_type', 1);
        $product = Yii::$app->request->getBodyParam('Product');
        $document = Yii::$app->request->getBodyParam('document');
        $productionType = $product['production_type'];

        $model = $this->productCreateHelper($productionType);
        if (isset($product['group_id'])) {
            $model->group_id = $product['group_id'];
        }
        $scenarioProd = $documentType == 1 ? 'zakup' : 'prod';
        $scenarioUslug = $productionType == 1 ? 'tovar' : 'uslug';
        $scenario = $scenarioProd . $scenarioUslug;
        $model->setScenario($scenario);
        $model->product_unit_id = ProductUnit::UNIT_COUNT;

        if ($documentType == Documents::IO_TYPE_OUT) {
            if ($company->companyTaxationType->osno) {
                $model->price_for_sell_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
            }
        } else {
            if ($company->companyTaxationType->osno) {
                $model->price_for_buy_nds_id = (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18);
            } else {
                $model->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
            }
        }

        // Set recognized product data
        if (isset($product['flag'])) {
            $model->title = $rProduct->name;
            $model->article = $rProduct->code;
            if ($documentType == Documents::IO_TYPE_OUT) {
                $model->price_for_sell_nds_id = $rProduct->taxRateId;
                $model->price_for_sell_with_nds = bcmul(100, $rProduct->amount);
            } else {
                $model->price_for_buy_nds_id = $rProduct->taxRateId;
                $model->price_for_buy_with_nds = bcmul(100, $rProduct->amount);
            }
        }

        if (!isset($product['flag'])) {
            if (Yii::$app->request->post('ajax') !== null) {
                return $this->ajaxValidate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->_prepareFromInvoice() && $model->save()) {

                $rProduct->updateAttributes(['product_id' => $model->id]);

                return [
                    'items' => $model->getProductsByIds([$model->id], $company->id),
                ];
            }
            if ($documentType == Documents::IO_TYPE_OUT) {
                $attribute = 'price_for_sell_with_nds';
            } else {
                $attribute = 'price_for_buy_with_nds';
            }
            if ($model->getAttribute($attribute) == 0) {
                $model->addError($attribute, 'Необходимо заполнить «' . $model->getAttributeLabel($attribute) . '».');
            }
        }

        $header = $this->renderPartial('@frontend/modules/documents/views/invoice/partial/create-product_header', ['productionType' => $productionType]);
        $body = $this->renderAjax('@frontend/modules/documents/views/invoice/partial/create-product_body', [
            'model' => $model,
            'documentType' => $documentType,
            'document' => $document,
        ]);

        return [
            'header' => $header,
            'body' => $body,
        ];
    }

    public function actionSetRecognizedContractorId($id, $inn, $type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;

        if ($newContractor = Contractor::findOne(['company_id' => $company->id, 'id' => $id])) {
            if ($rContractors = ScanRecognizeContractor::find()->where(['company_id' => $company->id, 'inn' => $inn, 'type' => $type])->all()) {
                foreach ($rContractors as $rContractor) {
                    $rContractor->updateAttributes(['contractor_id' => $newContractor->id]);
                }
            }
        }

        return ['result' => true];
    }

    public function actionSetRecognizedProductId($id, $name = null, $code = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $company = Yii::$app->user->identity->company;
        $newProduct = Product::findOne(['company_id' => $company->id, 'id' => $id]);
        $rProducts = [];

        if ($newProduct && $code) {
            $rProducts = ScanRecognizeProduct::find()->where(['company_id' => $company->id])->andWhere(['code' => $code])->all();
        } elseif ($newProduct && $name) {
            $rProducts = ScanRecognizeProduct::find()->where(['company_id' => $company->id])->andWhere(['name' => $name])->all();
        }

        if ($rProducts) {
            foreach ($rProducts as $rProduct) {
                $rProduct->updateAttributes(['product_id' => $newProduct->id]);
            }
        }

        return ['result' => true];
    }

    /**
     * @param $productionType
     * @return Product
     */
    protected function productCreateHelper($productionType)
    {
        $model = new Product();
        $model->production_type = $productionType;
        $model->creator_id = Yii::$app->user->id;
        $model->company_id = Yii::$app->user->identity->company->id;
        $model->country_origin_id = Country::COUNTRY_WITHOUT;

        return $model;
    }
}