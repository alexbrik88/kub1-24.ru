<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 05.01.2017
 * Time: 12:44
 */
namespace frontend\modules\documents\controllers;

use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\models\Documents;

/**
 * Class AjaxController
 */
class AjaxController extends FrontendController
{
    /**
     * @return array
     */
    public function actionGetProductList()
    {
        $result = [];
        if (\Yii::$app->request->isAjax) {
            $invoice_id = \Yii::$app->request->post('invoice_id');
            $invoice = Invoice::findOne(['id'=>$invoice_id]);
            $orders = $invoice->orders;

            foreach ($orders as $order) {
                if (OrderPackingList::compareWithOrderByQuantity($order) != null) {
                    $result[] = $order;
                }
            }
            return $result;
        }
        return $result;
    }


    /**
     * @return bool
     */
    public function actionDeleteFromProductList($packing_list_id)
    {
        $plus = 0;
        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');
            OrderPackingList::deleteAll(['id' => $id]);
        }
        if (\common\models\document\OrderPackingList::getAvailable($packing_list_id) != null) {
            $plus = 1;
        }

        return $plus;
    }

    /**
     * @return bool
     */
    public function actionSubsitution()
    {
        $order_id = \Yii::$app->request->post('order_id');
        $packing_list_id = \Yii::$app->request->post('packing_list_id');
        $order = Order::findOne(['id'=>$order_id]);
        if (OrderPackingList::findOne(['packing_list_id'=>$packing_list_id,'order_id'=>$order_id]) != null) {
            $opl = OrderPackingList::findOne(['packing_list_id'=>$packing_list_id,'order_id'=>$order_id]);
        } else {
            $opl = OrderPackingList::createFromOrder($packing_list_id, $order);
        }

        return  $this->renderAjax('/packing-list/_viewPartials/_template_'.Documents::$ioTypeToUrl[$order->invoice->type], [
            'key' => count(\common\models\document\OrderPackingList::findAll(['packing_list_id' => $packing_list_id])),
            'packing_list_id'=>$packing_list_id,
            'order'=> $opl,
            'hasNds'=>$order->invoice->hasNds,
            'precision' => $order->invoice->price_precision,
        ]);
    }
    /**
     * @param $key
     * @return string
     */
    public function actionAddNewRow()
    {
        $key = \Yii::$app->request->post('key');
        $invoice_id = \Yii::$app->request->post('invoice_id');
        $packing_list_id = \Yii::$app->request->post('packing_list_id');
        $active = \Yii::$app->request->post('active');
        $invoice = Invoice::findOne(['id'=>$invoice_id]);
        $orders = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS &&
                    $order->quantity > OrderPackingList::find()
                        ->where(['order_id'=>$order->id])
                        ->andWhere(['!=','packing_list_id',$packing_list_id])
                        ->sum('quantity')
                ) {
                    $orders[] = $order;
                }
            }
        }
        $result = [];
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                    $result[$order->id] = $order->product_title;
            }
        }

        return  $this->renderAjax('/packing-list/_viewPartials/_template_'.Documents::$ioTypeToUrl[$invoice->type], [
            'key' => $key,
            'result'=>$result,
            'packing_list_id'=>$packing_list_id,
            'plus'=>0,
            'invoice'=>$invoice,
            'precision' => $invoice->price_precision,
        ]);
    }
    /**
     * @return bool
     */
    public function actionDeleteRow()
    {
        $key = \Yii::$app->request->post('key');
        $result = [];
        $packing_list_id = \Yii::$app->request->post('packing_list_id');
        $orders = PackingList::findOne(['id'=>$packing_list_id])->invoice->orders;
        $active = \Yii::$app->request->post('active');
        if ($active != null) {
            foreach ($orders as $order) {
                if (OrderPackingList::compareWithOrderByQuantity($order) != null && !in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                $result[$order->id] = $order->product_title;
            }
        }
        if ($result != null) {
            return $plus = 1;
        } else {
            return $plus = 0;
        }
    }

    /**
     * @return bool
     */
    public function actionClose($packing_list_id, $ioType)
    {
        $model = PackingList::findOne(['id'=>$packing_list_id]);

        return $this->renderPartial('/packing-list/_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $model->invoice->price_precision,
        ]);
    }

    /**
     * @return bool
     */
    public function actionEdit($packing_list_id, $ioType)
    {
        $orders = OrderPackingList::getAvailable($packing_list_id);
        if (OrderPackingList::getAvailable($packing_list_id) == null) {
            return $is_editable = 0;
        } else {
            return $is_editable = 1;
        }
    }
}
