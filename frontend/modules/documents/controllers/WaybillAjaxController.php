<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 05.01.2017
 * Time: 12:44
 */
namespace frontend\modules\documents\controllers;

use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderWaybill;
use common\models\document\Waybill;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\models\Documents;

/**
 * Class WaybillAjaxController
 */
class WaybillAjaxController extends FrontendController
{
    /**
     * @return array
     */
    public function actionGetProductList()
    {
        $result = [];
        if (\Yii::$app->request->isAjax) {
            $invoice_id = \Yii::$app->request->post('invoice_id');
            $invoice = Invoice::findOne(['id'=>$invoice_id]);
            $orders = $invoice->orders;

            foreach ($orders as $order) {
                if (OrderWaybill::compareWithOrderByQuantity($order) != null) {
                    $result[] = $order;
                }
            }
            return $result;
        }
        return $result;
    }


    /**
     * @return bool
     */
    public function actionSubsitution()
    {
        $order_id = \Yii::$app->request->post('order_id');
        $waybill_id = \Yii::$app->request->post('waybill_id');
        $order = Order::findOne(['id'=>$order_id]);
        if (OrderWaybill::findOne(['waybill_id'=>$waybill_id,'order_id'=>$order_id]) != null) {
            $opl = OrderWaybill::findOne(['waybill_id'=>$waybill_id,'order_id'=>$order_id]);
        } else {
            $opl = OrderWaybill::createFromOrder($waybill_id, $order);
        }
        if (OrderWaybill::getAvailable($waybill_id) == null) {
            $plus = 0;
        } else {
            $plus = 1;
        }
        return  $this->renderAjax('/waybill/_viewPartials/_template_'.Documents::$ioTypeToUrl[$order->invoice->type], [
            'key' => count(\common\models\document\OrderWaybill::findAll(['waybill_id' => $waybill_id])),
            'waybill_id'=>$waybill_id,
            'order'=> $opl,
            'plus'=>$plus,
            'hasNds'=>$order->invoice->hasNds,
            'precision' => $order->invoice->price_precision,
        ]);
    }
    /**
     * @param $key
     * @return string
     */
    public function actionAddNewRow()
    {
        $key = \Yii::$app->request->post('key');
        $invoice_id = \Yii::$app->request->post('invoice_id');
        $waybill_id = \Yii::$app->request->post('waybill_id');
        $active = \Yii::$app->request->post('active');
        $invoice = Invoice::findOne(['id'=>$invoice_id]);
        $orders = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS &&
                    $order->quantity > OrderWaybill::find()
                        ->where(['order_id'=>$order->id])
                        ->andWhere(['!=','waybill_id',$waybill_id])
                        ->sum('quantity')
                ) {
                    $orders[] = $order;
                }
            }
        }
        $result = [];
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                    $result[$order->id] = $order->product_title;
            }
        }

        return  $this->renderAjax('/waybill/_viewPartials/_template_'.Documents::$ioTypeToUrl[$invoice->type], [
            'key' => $key,
            'result'=>$result,
            'waybill_id'=>$waybill_id,
            'plus'=>0,
            'invoice'=>$invoice,
            'precision' => $invoice->price_precision,
        ]);
    }
    /**
     * @return bool
     */
    public function actionDeleteRow()
    {
        $key = \Yii::$app->request->post('key');
        $result = [];
        $waybill_id = \Yii::$app->request->post('waybill_id');
        $orders = Waybill::findOne(['id'=>$waybill_id])->invoice->orders;
        $active = \Yii::$app->request->post('active');
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                $result[$order->id] = $order->product_title;
            }
        }
        if ($result != null) {
            return $plus = 1;
        } else {
            return $plus = 0;
        }
    }

    /**
     * @return bool
     */
    public function actionClose($waybill_id, $ioType)
    {
        $model = Waybill::findOne(['id'=>$waybill_id]);

        return $this->renderPartial('/waybill/_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'precision' => $model->invoice->price_precision,
        ]);
    }

    /**
     * @return bool
     */
    public function actionEdit($waybill_id, $ioType)
    {
        $orders = OrderWaybill::getAvailable($waybill_id);
        if (OrderWaybill::getAvailable($waybill_id) == null) {
            return $is_editable = 0;
        } else {
            return $is_editable = 1;
        }
    }
}
