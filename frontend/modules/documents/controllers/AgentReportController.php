<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\Contractor;
use common\models\document\AgentReport;
use common\models\document\Invoice;
use common\models\document\AgentReportOrder;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderHelper;
use common\models\document\status\AgentReportStatus;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\AgentReportHelper;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * AgentReport controller
 */
class AgentReportController extends DocumentBaseController
{

    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_AGENT_REPORT;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create-invoice'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                    'ioType' =>  Documents::IO_TYPE_IN,
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['update', 'comment-internal'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['send'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                ],
            ],
        ]);
    }


    /**
     * @param null $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($type = null)
    {
        $company = Yii::$app->user->identity->company;
        $type = Documents::IO_TYPE_OUT;
        $agent_id = Yii::$app->request->get('agent');
        if (!$agent = Contractor::findOne(['id' => $agent_id, 'company_id' => $company->id]))
            throw new NotFoundHttpException('Необходимо указать агента');

        $searchModel = Documents::loadSearchProvider($this->typeDocument, $type);
        $searchModel->agent_id = $agent_id;
        $dateRange = StatisticPeriod::getSessionPeriod();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $dateRange);

        $dataProvider->pagination->pageSize = \frontend\components\PageSize::get();

        $message = new Message($this->typeDocument, $type);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $type,
            'message' => $message,
            'agent' => $agent,
            'company' => $company
        ]);
    }

    /**
     * Displays a single AgentReport model.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id)
    {
        /** @var AgentReport $model */
        $model = $this->loadModel($id, $type);
        $canUpdate  = Yii::$app->getUser()->can(permissions\document\Document::UPDATE)
                   && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

        $model->setStatusByInvoice();

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => false,
            'contractorId' => null,
            'canUpdate' => $canUpdate,
            'backUrl' => $this->getBackUrl($model)
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($type, $id)
    {
        /** @var AgentReport $model */
        $model = $this->loadModel($id, $type);
        AgentReportHelper::$orderParamName = 'AgentReportOrder';
        if (AgentReportHelper::load($model, Yii::$app->getRequest()->post()) && AgentReportHelper::save($model)) {
            Yii::$app->session->setFlash('success', 'Отчет изменен');
            return $this->redirect(['view', 'type' => $model->type, 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'company' => Yii::$app->user->identity->company,
            'useContractor' => false,
            'backUrl' => $this->getBackUrl($model)
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid, $view = 'print')
    {
        /* @var AgentReport $model */
        $model = AgentReport::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $view, 'pdf-view', [
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var AgentReport $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = AgentReport::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var EmployeeCompany $sender */
        $sender = Yii::$app->user->identity->currentEmployeeCompany;
        $contractors = null;
        $agentReports = Yii::$app->request->post('AgentReport');
        $send = 0;
        $contractorAgentReports = [];
        foreach ($agentReports as $id => $agentReport) {
            if ($agentReport['checked']) {
                /* @var $model AgentReport */
                $model = $this->loadModel($id, $type);
                $contractorAgentReports[$model->agent_id][] = $model->id;
            }
        }
        foreach ($contractorAgentReports as $contractorID => $agentReports) {
            $contractor = Contractor::findOne($contractorID);
            if ($sendTo = $contractor->someEmail) {
                if (AgentReport::manySend($sender, $sendTo, $type, $agentReports)) {
                    $send++;
                }
            } else {
                $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                    'type' => $contractor->type, 'id' => $contractor->id
                ]));
            }
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorAgentReports) . ' писем.';
        if ($send == count($contractorAgentReports)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки отчетов, необхоидмо заполнить E-mail у агента<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        /** @var AgentReport $model */
        $model = $this->loadModel($id, $type);

        if ($actionType === 'pdf' && $this->id == 'act') {
            $addStamp = $model->invoice->company->pdf_act_signed;
        } else {
            $addStamp = 0;
        }

        $model->setStatusPrinted();

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => $addStamp,
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $agentReportID = explode(',', $multiple);
        /* @var $model AgentReport */
        $model = $this->loadModel(current($agentReportID), $type);

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf' && $model->agent->company->pdf_act_signed) ? true : false,
            'multiple' => $agentReportID,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id, $type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->loadModel($id, $type);
        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);
        $model = $this->loadModel($id, $type);

        return ['value' => $model->comment_internal];
    }

    public function actionCreateInvoice($reportId)
    {
        /** @var AgentReport $model */
        $model = $this->loadModel($reportId, Documents::IO_TYPE_OUT);
        $company = Yii::$app->user->identity->company;
        $contractor = $model->agent;
        $invoice = AgentReportHelper::getReportInvoice($company, $contractor);
        $invoice->populateRelation('company', $company);
        $invoice->populateRelation('contractor', $contractor);

        $discount = 0;
        $price = $model->total_sum;
        $product = AgentReportHelper::getReportProduct($model, true);
        $order = OrderHelper::createOrderByProduct($product, $invoice, 1, $price, $discount);

        $invoice->populateRelation('orders', [$order]);
        $invoice->total_amount_has_nds = $invoice->total_amount_nds > 0;
        $invoice->total_mass_gross = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
        $invoice->total_place_count = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));

        $save = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        \common\components\helpers\ModelHelper::logErrors($order, __METHOD__);

                        return false;
                    }
                }

                return true;
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        });

        if ($save) {
            $model->updateAttributes(['invoice_id' => $invoice->id, 'has_invoice' => 1]);
            return $this->redirect(['/documents/invoice/view', 'type' => $invoice->type, 'id' => $invoice->id, 'contractorId' => $invoice->contractor_id, 'contractorTab' => 'agent_report']);
        }

        Yii::$app->session->setFlash('error', 'Ошибка при создании счета.');
        return $this->redirect(Yii::$app->request->referrer ?: ['/contractor/view', 'type' => $model->agent->type, 'id' => $model->agent_id, 'tab' => 'agent_report']);
    }


    /**
     * @param AgentReport $model
     * @return array
     */
    public function getBackUrl(AgentReport $model) {
        return ['/contractor/view', 'type' => Contractor::TYPE_SELLER, 'id' => $model->agent->id, 'tab' => 'agent_report'];
    }
}
