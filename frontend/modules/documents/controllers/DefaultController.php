<?php
namespace frontend\modules\documents\controllers;

use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\FrontendController;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use frontend\rbac\permissions;

/**
 * Document default controller
 */
class DefaultController extends FrontendController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_INVOICE;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [permissions\document\Document::DOCUMENTS],
                    ],
                    [
                        'actions' => ['expenditure-delete', 'income-delete'],
                        'allow' => true,
                        'roles' => [permissions\document\Document::DELETE],
                    ],
                    [
                        'actions' => ['expenditure-edit', 'income-edit'],
                        'allow' => true,
                        'roles' => [permissions\document\Document::UPDATE],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'expenditure-delete' => ['post'],
                    'expenditure-edit' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $company = Yii::$app->user->identity->company;

        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $searchModelOut->company_id = $company->id;
        $searchModelOut->populateRelation('company', $company);
        $dataProviderOut = $searchModelOut->search(Yii::$app->request->queryParams,
            StatisticPeriod::getDefaultPeriod(), $searchModelOut::SORT_PARAM_NAME_OUT);

        $searchModelIn = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_IN);
        $searchModelIn->company_id = $company->id;
        $searchModelIn->populateRelation('company', $company);
        $dataProviderIn = $searchModelIn->search(Yii::$app->request->queryParams,
            StatisticPeriod::getDefaultPeriod(), $searchModelIn::SORT_PARAM_NAME_IN);

        return $this->render('index', [
            'searchModelOut' => $searchModelOut,
            'dataProviderOut' => $dataProviderOut,
            'searchModelIn' => $searchModelIn,
            'dataProviderIn' => $dataProviderIn,
        ]);
    }

    /**
     * @return array
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionExpenditureDelete()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = InvoiceExpenditureItem::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Статья расхода не найдена',
            ];
        }

        if (!$model->getCanDelete()) {
            return [
                'success' => false,
                'message' => 'Статью расхода удалить нельзя, так как к ней привязаны счета и платежи.',
            ];
        }

        foreach ($model->expenseItemsFlowOfFunds as $expenseItemFlowOfFunds) {
            $expenseItemFlowOfFunds->delete();
        }

        foreach ($model->profitAndLossCompanyItems as $profitAndLossCompanyItem) {
            $profitAndLossCompanyItem->delete();
        }


        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Статья расхода успешно удалена.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Статья расхода не удалось удалить.',
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionExpenditureEdit()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = InvoiceExpenditureItem::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Статья расхода не найдена',
            ];
        }

        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->name,
            ];
        }

        return [
            'success' => false,
            'message' => $model->getFirstError('name'),
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionIncomeDelete()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = InvoiceIncomeItem::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Тип прихода не найден',
            ];
        }
        if ($model->getBankFlows()->exists()) {
            return [
                'success' => false,
                'message' => 'Тип прихода удалить нельзя, так как к нему привязаны платежи.',
            ];
        }
        foreach ($model->incomeItemsFlowOfFunds as $incomeItemFlowOfFunds) {
            $incomeItemFlowOfFunds->delete();
        }

        if ($model->delete()) {
            return [
                'success' => true,
                'message' => 'Тип прихода успешно удален.',
            ];
        }

        return [
            'success' => false,
            'message' => 'Тип прихода не удалось удалить.',
        ];
    }

    /**
     *
     * @return mixed
     */
    public function actionIncomeEdit()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = InvoiceIncomeItem::findOne([
            'id' => Yii::$app->request->post('id'),
            'company_id' => Yii::$app->user->identity->company->id,
        ]);
        if ($model === null) {
            return [
                'success' => false,
                'message' => 'Тип прихода не найден',
            ];
        }

        if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
            return [
                'success' => true,
                'name' => $model->name,
            ];
        }

        return [
            'success' => false,
            'message' => $model->getFirstError('name'),
        ];
    }

}
