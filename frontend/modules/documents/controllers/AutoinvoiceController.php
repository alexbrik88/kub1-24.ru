<?php

namespace frontend\modules\documents\controllers;

use Yii;
use common\models\document\Autoinvoice;
use common\models\document\InvoiceAuto;
use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Autoinvoice controller
 */
class AutoinvoiceController extends \frontend\components\FrontendController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'actions' => [
                            'many-update-day',
                            'many-update-dateto',
                            'many-update-industry',
                            'many-update-project',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'many-update-day' => ['post'],
                    'many-update-dateto' => ['post'],
                    'many-update-industry' => ['post'],
                    'many-update-project' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => [
                    'many-update-day',
                    'many-update-dateto',
                    'many-update-industry',
                    'many-update-project',
                ],
            ],
        ];
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionManyUpdateDay()
    {
        $user = Yii::$app->getUser();
        $company = $user->identity->company;
        $updated = 0;
        $ids = (array) Yii::$app->request->post('ids');
        $value = Yii::$app->request->post('value');

        if (in_array($value, range(1, 31)) && $ids) {
            foreach ($ids as $id) {
                $model = Autoinvoice::find()->joinWith('invoice')->andWhere([
                    'autoinvoice.id' => $id,
                    'autoinvoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                ])->one();

                if ($user->can(permissions\document\Document::UPDATE, ['model' => $model->invoice])) {
                    if ($model->updateAttributes(['day' => $value])) {
                        $updated++;
                    }
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Число месяца отправки изменено.');
        }

        return $this->redirect(Yii::$app->request->referrer ?? ['invoice/index-auto']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionManyUpdateDateto()
    {
        $user = Yii::$app->getUser();
        $company = $user->identity->company;
        $updated = 0;
        $ids = (array) Yii::$app->request->post('ids');
        $value = ($d = date_create_from_format('d.m.Y', Yii::$app->request->post('value'))) ? $d->format('Y-m-d') : null;

        if ($value && $ids) {
            foreach ($ids as $id) {
                $model = Autoinvoice::find()->joinWith('invoice')->andWhere([
                    'autoinvoice.id' => $id,
                    'autoinvoice.company_id' => $company->id,
                    'invoice.is_deleted' => false,
                ])->one();

                if ($user->can(permissions\document\Document::UPDATE, ['model' => $model->invoice])) {
                    if ($model->updateAttributes(['date_to' => $value])) {
                        $updated++;
                    }
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Дата окончания изменена.');
        }

        return $this->redirect(Yii::$app->request->referrer ?? ['invoice/index-auto']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionManyUpdateIndustry()
    {
        $user = Yii::$app->getUser();
        $company = $user->identity->company;
        $updated = 0;
        $ids = (array) Yii::$app->request->post('ids');
        $value = Yii::$app->request->post('value');
        if (isset($value) && is_numeric($value) && $ids) {
            if ($value == 0 || $company->getCompanyIndustries()->andWhere(['id' => $value])->exists()) {
                foreach ($ids as $id) {
                    $model = InvoiceAuto::find()->andWhere([
                        'invoice.id' => $id,
                        'invoice.company_id' => $company->id,
                        'invoice.is_deleted' => false,
                    ])->one();

                    if ($user->can(permissions\document\Document::UPDATE, ['model' => $model])) {
                        if ($model->updateAttributes(['industry_id' => $value ?: null])) {
                            $updated++;
                        }
                    }
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Направление изменено.');
        }

        return $this->redirect(Yii::$app->request->referrer ?? ['invoice/index-auto']);
    }

    /**
     * @return Response
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionManyUpdateProject()
    {
        $user = Yii::$app->getUser();
        $company = $user->identity->company;
        $updated = 0;
        $ids = (array) Yii::$app->request->post('ids');
        $value = Yii::$app->request->post('value');
        if (isset($value) && is_numeric($value) && $ids) {
            if ($value == 0 || $company->getProjects()->andWhere(['id' => $value])->exists()) {
                foreach ($ids as $id) {
                    $model = InvoiceAuto::find()->andWhere([
                        'invoice.id' => $id,
                        'invoice.company_id' => $company->id,
                        'invoice.is_deleted' => false,
                    ])->one();

                    if ($user->can(permissions\document\Document::UPDATE, ['model' => $model])) {
                        if ($model->updateAttributes(['project_id' => $value ?: null])) {
                            $updated++;
                        }
                    }
                }
            }
        }

        if ($updated > 0) {
            Yii::$app->session->setFlash('success', 'Проект изменен.');
        }

        return $this->redirect(Yii::$app->request->referrer ?? ['invoice/index-auto']);
    }
}
