<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\ActEssence;
use common\models\document\Autoact;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderDocument;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\product\Product;
use Exception;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\ActHelper;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\ActSearch;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Act controller
 */
class ActController extends DocumentBaseController
{

    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_ACT;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['download'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['generate-xls', 'get-xls', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['create-for-several-invoices',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            /* @var $user EmployeeCompany */
                            $user = Yii::$app->user->identity->currentEmployeeCompany;
                            return (Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) || $user->employee_role_id == EmployeeRole::ROLE_MANAGER)
                                && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-create',],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-update-original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'ioType' => Yii::$app->request->get('type'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['update', 'comment-internal'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'model' => $action->controller->loadModel(),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'send-many-in-one', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['autoact'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_CHIEF],
                    ],
                    [
                        'actions' => ['act-by-status', 'act-by-payment', 'act-by-calculate-nds'],
                        'allow' => true,
                        'roles' => [UserRole::ROLE_AUTHENTICATED],
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['autoact'],
            ]
        ]);
    }


    /**
     * Displays a single Act model.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($type, $id)
    {
        /** @var Act $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        $canUpdate =
            Yii::$app->getUser()->can(permissions\document\Document::UPDATE)
            &&
            Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
            'canUpdate' => $canUpdate,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionUpdate($type, $id)
    {
        /** @var Act $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        if ($model->invoice->company->actEssence) {
            $actEssence = $model->invoice->company->actEssence;
        } else {
            $actEssence = new ActEssence();
            $actEssence->company_id = $model->invoice->company_id;
        }
        ActHelper::$orderParamName = 'OrderAct';
        if (ActHelper::load($model, Yii::$app->getRequest()->post()) && ActHelper::save($model)) {
            if ($actEssence->load(Yii::$app->request->post())) {
                $actEssence->text = $model->comment;
                if (($actEssence->is_checked && $actEssence->text) ||
                    !$actEssence->is_checked) {
                    $actEssence->save();
                }
            }

            GoodsCancellation::cancellationByAct(Yii::$app->user->identity->currentEmployeeCompany, $model);

            if ($this->isScanPreview) {
                Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                return $this->redirect(Url::previous('lastUploadManagerPage'));
            }

            return $this->redirect(['view',
                'type' => $type,
                'id' => $model->id,
                'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
            ]);
        }
        $dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $canUpdate =
            Yii::$app->getUser()->can(permissions\document\Document::UPDATE)
            &&
            Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'dateFormatted' => $dateFormatted,
            'ioType' => $type,
            'precision' => $model->invoice->price_precision,
            'company' => Yii::$app->user->identity->company,
            'canUpdate' => $canUpdate,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
            'actEssence' => $actEssence,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var Act $model */
        $model = $this->loadModel($id, $type);
        $status = (int)Yii::$app->request->getBodyParam('status');

        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Act $model) {
                return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
            });
        }

        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        switch ($status) {
            case ActStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'Акт передан');
                break;
            case ActStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'Акт подписан');
                break;
        }
        if ($status == ActStatus::STATUS_PRINTED) {
            return $this->redirect([
                'document-print',
                'actionType' => 'print',
                'id' => $model->id,
                'type' => $model->type,
                'filename' => $model->getPrintTitle(),
            ]);
        } else {
            return $this->redirect(['view',
                'type' => $type,
                'id' => $model->id,
                'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
            ]);
        }
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid, $view = 'print')
    {
        /* @var Invoice $model */
        $model = Act::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $view, 'pdf-view', [
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \Exception
     */
    public function actionCreate($type, $invoiceId)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        $documentDate = Yii::$app->request->get('documentDate');
        $isFromInvoiceForm = Yii::$app->request->get('isFromInvoiceForm');
        if (empty($documentDate) && $invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = DateHelper::format($invoice->document_date, 'd.m.Y', 'Y-m-d');
            $documentNumber = $invoice->document_number;
        }
        if ($invoice->canAddAct) {
            if (!$invoice->need_act) {
                if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                    $invoice->need_act = true;
                    $invoice->save(true, ['need_act']);
                } else {
                    throw new BadRequestHttpException();
                }
            }
            if ($invoice->createAct($documentDate, $documentNumber)) {

                Yii::$app->session->setFlash('success', 'Акт создан.');

                //// 20-204.2: Product::updateGrowingPriceForBuy($invoice);

                if ($this->isScanPreview)
                {
                    if (Yii::$app->request->get('returnToManager')) {
                        Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                        return $this->redirect(Url::previous('lastUploadManagerPage'));
                    }

                    return $this->redirect([
                        '/documents/act/update',
                        'type' => $invoice->act->type,
                        'id' => $invoice->act->id,
                        'mode' => 'previewScan',
                        'files' => Yii::$app->request->get('files')
                    ]);
                }

                if ($invoice->act->type == Documents::IO_TYPE_IN && !$isFromInvoiceForm) {
                    return $this->redirect([
                        '/documents/act/update',
                        'type' => $invoice->act->type,
                        'id' => $invoice->act->id
                    ]);
                }

                return $this->redirect([
                    'view',
                    'type' => $invoice->act->type,
                    'id' => $invoice->act->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'При создании Акта возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать Акт.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @return Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreateForSeveralInvoices($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];
        $canAdd = true;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        if (Invoice::find()
                ->andWhere(['id' => $idArray])
                ->groupBy('contractor_id')
                ->count() == 1) {
            /* @var $invoices Invoice[] */
            $invoices = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();
            foreach ($invoices as $invoice) {
                if (!$invoice->getCanAddAct()) {
                    $canAdd = false;
                }
            }
            if ($canAdd) {
                foreach ($invoices as $invoice) {
                    if (!$invoice->need_act) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_act = true;
                            $invoice->save(true, ['need_act']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                }
                if (($actID = Invoice::createActForSeveralInvoices($invoices, $type))) {
                    Yii::$app->session->setFlash('success', 'Акт создан.');

                    return $this->redirect([
                        'view',
                        'type' => $type,
                        'id' => $actID,
                        'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', 'При создании Акта возникли ошибки.');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws \Exception
     */
    public function actionManyCreate($type, $contractor = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];
        $documentDate = !empty(Yii::$app->request->post('Act')['document_date']) ? Yii::$app->request->post('Act')['document_date'] : null;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $createdActs = 0;
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddAct) {
                    if (!$invoice->need_act) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_act = true;
                            $invoice->save(true, ['need_act']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                    if ($invoice->createAct($documentDate))
                        $createdActs++;
                }
            }
        } else {
            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    //['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();

            $noInvoice = false;
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddAct()) {
                    if ($orderDocument->invoice->createAct($documentDate))
                        $createdActs++;
                }
                if (!$orderDocument->invoice) {
                    $noInvoice = true;
                }
            }

            if ($createdActs > 0) {
                Yii::$app->session->setFlash('success', ($createdActs == 1) ? 'Акт создан' : 'Акты созданы');
            } else {
                Yii::$app->session->setFlash('error', ($noInvoice) ? 'Сначала создайте Счет' : 'Не удалось создать Акт');
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var Act $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = Act::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var EmployeeCompany $sender */
        $sender = Yii::$app->user->identity->currentEmployeeCompany;
        $contractors = null;
        $acts = Yii::$app->request->post('Act');
        $send = 0;
        $contractorActs = [];
        foreach ($acts as $id => $act) {
            if ($act['checked']) {
                /* @var $model Act */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }
                $contractorActs[$model->invoice->contractor_id][] = $model->id;
            }
        }
        foreach ($contractorActs as $contractorID => $acts) {
            if (count($acts) > 1) {
                $contractor = Contractor::findOne($contractorID);
                if ($sendTo = $contractor->someEmail) {
                    if (Act::manySend($sender, $sendTo, $type, $acts)) {
                        $send++;
                    }
                } else {
                    $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $contractor->type, 'id' => $contractor->id
                    ]));
                }
            } else {
                $actID = current($acts);
                /* @var $model Act */
                $model = $this->loadModel($actID, $type);
                if ($model->uid == null) {
                    $model->uid = Act::generateUid();
                    $model->save(false, ['uid']);
                }
                if ($sendTo = $model->invoice->contractor->someEmail) {
                    if ($model->sendAsEmail($sender, $sendTo)) {
                        $send++;
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorActs) . ' писем.';
        if ($send == count($contractorActs)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки актов, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $actsID = explode(',', $multiple);
        /* @var $model Act */
        $model = $this->loadModel(current($actsID), $type);

        return $this->_documentPrint($model, InvoicePrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => ($actionType === 'pdf') ? Yii::$app->request->get('addStamp', $model->invoice->company->pdf_act_signed) : false,
            'multiple' => $actsID,
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);
        $addStamp = $model->invoice->company->pdf_act_signed;

        return $this->_documentPng($model, $page, [
            'addStamp' => $addStamp,
        ]);
    }

    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param $type
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($type, $id)
    {
        /* @var $model Act */
        $model = $this->loadModel($id, $type);
        OrderAct::deleteByAct($model->id);
        $invoices = $model->invoices;
        LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
        foreach ($invoices as $invoice) {
            InvoiceHelper::checkForAct($invoice->id, $invoices);
        }

        Yii::$app->session->setFlash('success', 'Акт удален');

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $acts = Yii::$app->request->post('Act', []);
        if (is_array($acts)) {
            foreach ($acts as $id => $act) {
                if ($act['checked']) {
                    /* @var $model Act */
                    $model = $this->loadModel($id, $type);
                    $invoices = $model->invoices;
                    OrderAct::deleteByAct($model->id);
                    LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
                    foreach ($invoices as $invoice) {
                        InvoiceHelper::checkForAct($invoice->id, $invoices);
                    }
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $ioType = Yii::$app->request->get('type', Documents::IO_TYPE_OUT);
        $period = StatisticPeriod::getSessionPeriod();
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $ioType);
        $acts = $searchModel->search(Yii::$app->request->queryParams, $period, true, true);

        Act::generateXlsTable($period, $acts);
    }

    /**
     *
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $idArray = [];
        $period = StatisticPeriod::getSessionPeriod();
        $models = Yii::$app->request->post('Act');
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $query = Act::find()->byCompany(Yii::$app->user->identity->company->id);

        $query->joinWith([
            'statusOut',
            'invoice.contractor',
        ])->andWhere([
            Act::tableName() . '.id' => $idArray,
        ])->groupBy(Act::tableName() . '.id');

        $acts = $query->orderBy(['`' . Act::tableName() . '`.`document_date`' => SORT_DESC,
            '`' . Act::tableName() . '`.`document_number` * 1' => SORT_DESC,
            '`' . Act::tableName() . '`.`document_additional_number` * 1' => SORT_DESC])->all();

        $ioType = ($acts) ? $acts[0]->type : null;

        Act::generateXlsTable($period, $acts);
    }

    /**
     * @throws mixed
     */
    public function actionAutoact()
    {
        $model = Yii::$app->user->identity->company->autoact;

        if (Yii::$app->request->isPost) {
            $params = Yii::$app->request->post('Autoact');
            if (isset($params['rule']) && isset($params[$params['rule']])) {
                $params = array_merge($params, $params[$params['rule']]);
            }

            $model->loadDefaultValues(false)->load($params, '');

            if (Yii::$app->request->post('ajax') == 'autoact-form') {
                Yii::$app->response->format = Response::FORMAT_JSON;

                return ActiveForm::validate($model);
            } elseif ($model->save()) {
                Yii::$app->session->setFlash('success', 'Правила выставления актов сохранены.');

                return $this->redirect(Yii::$app->request->referrer ?: ['index', 'type' => Documents::IO_TYPE_OUT]);
            }
        }

        return $this->renderPartial('autoact', ['model' => $model]);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom = \Yii::$app->request->get('date_from');
        $dateTo = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to' => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_act' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'Акт',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_ACT,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCommentInternal($id, $type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->loadModel($id, $type);
        $model->comment_internal = Yii::$app->request->post('comment_internal', '');
        $model->save(true, ['comment_internal']);
        $model = $this->loadModel($id, $type);

        return ['value' => $model->comment_internal];
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateStatus($type, $status)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('Act', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model Act */
                    $model = $this->loadModel($id, $type);

                    if ($status !== null && $status != $model->status_out_id) {
                        $model->status_out_id = $status;

                        LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Act $model) {
                            return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                        });
                    }

                    switch ($status) {
                        case ActStatus::STATUS_SEND:
                            Yii::$app->session->setFlash('success', 'Акты переданы');
                            break;
                        case ActStatus::STATUS_RECEIVED:
                            Yii::$app->session->setFlash('success', 'Акты подписаны');
                            break;
                    }
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateOriginal($type, $val)
    {
        if ($type != Documents::IO_TYPE_IN) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('Act', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model Act */
                    $model = $this->loadModel($id, $type);
                    $model->is_original = intval($val) ? true : false;
                    $model->save(false, ['is_original', 'is_original_updated_at']);
                    Yii::$app->session->setFlash('success', 'Акты сохранены');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    public function actionActByStatus()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userConfig = Yii::$app->user->identity->config;

        try {
            $userConfig->updateAttributes([
                'act_stat_by_payment' => false,
                'act_stat_by_status' => true,
                'act_stat_by_calculate_nds' => false,
            ]);
        } catch (Exception $e) {
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    public function actionActByPayment()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userConfig = Yii::$app->user->identity->config;

        try {
            $userConfig->updateAttributes([
                'act_stat_by_payment' => true,
                'act_stat_by_status' => false,
                'act_stat_by_calculate_nds' => false,
            ]);
        } catch (Exception $e) {
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    public function actionActByCalculateNds()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $userConfig = Yii::$app->user->identity->config;

        try {
            $userConfig->updateAttributes([
                'act_stat_by_payment' => false,
                'act_stat_by_status' => false,
                'act_stat_by_calculate_nds' => true,
            ]);
        } catch (Exception $e) {
            return ['success' => 'false'];
        }

        return ['success' => 'true'];
    }

    /**
     * @param $type file type
     * @param $uid document uid
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDownload($type, $uid)
    {
        /* @var Invoice $model */
        $model = self::getByUid($uid);

        switch ($type) {
            case 'pdf':
            default:
                return $this->_pdf($model, 'pdf-view', [
                    'model' => $model,
                    'message' => new Message($this->typeDocument, $model->type),
                    'ioType' => $model->type,
                    'addStamp' => (boolean) $model->company->pdf_signed,
                ]);
                break;
        }
    }

    /**
     * @param string $uid
     * @param integer $type
     * @return Act
     * @throws NotFoundHttpException
     */
    public static function getByUid($uid, $type = Documents::IO_TYPE_OUT)
    {
        /* @var Invoice $model */
        $model = empty($uid) ? null : Act::find()
            ->joinWith('invoices.company')
            ->andWhere([
                'act.uid' => $uid,
                'act.type' => $type,
                'invoice.is_deleted' => false,
                'company.blocked' => false,
            ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }
}
