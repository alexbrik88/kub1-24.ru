<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 16.02.2017
 * Time: 13:11
 */

namespace frontend\modules\documents\controllers;


use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\product\Product;
use frontend\components\FrontendController;
use frontend\models\Documents;

/**
 * Class ActAjaxController
 * @package frontend\modules\documents\controllers
 */
class ActAjaxController extends FrontendController
{


    /**
     * @return array
     */
    public function actionGetProductList()
    {
        $result = [];
        if (\Yii::$app->request->isAjax) {
            $invoice_id = \Yii::$app->request->post('invoice_id');
            $invoice = Invoice::findOne(['id' => $invoice_id]);
            $orders = $invoice->orders;

            foreach ($orders as $order) {
                if (OrderAct::compareWithOrderByQuantity($order) != null) {
                    $result[] = $order;
                }
            }
            return $result;
        }
        return $result;
    }


    /**
     * @return bool
     */
    public function actionSubsitution()
    {
        $order_id = \Yii::$app->request->post('order_id');
        $act_id = \Yii::$app->request->post('act_id');
        $act = Act::findOne(['id' => $act_id]);
        $order = Order::findOne(['id' => $order_id]);
        if (OrderAct::findOne(['act_id' => $act_id, 'order_id' => $order_id]) != null) {
            $opl = OrderAct::findOne(['act_id' => $act_id, 'order_id' => $order_id]);
        } else {
            $opl = OrderAct::createFromOrder($act_id, $order);
        }
        if (OrderAct::getAvailable($act_id) == null) {
            $plus = 0;
        } else {
            $plus = 1;
        }
        return $this->renderAjax('/act/_viewPartials/_template_' . Documents::$ioTypeToUrl[$order->invoice->type], [
            'key' => count(OrderAct::findAll(['act_id' => $act_id])),
            'model' => $act,
            'order' => $opl,
            'plus' => $plus,
            'precision' => $act->invoice->price_precision,
        ]);
    }

    /**
     * @return string
     */
    public function actionAddNewRow()
    {
        $key = \Yii::$app->request->post('key');
        $invoice_id = \Yii::$app->request->post('invoice_id');
        $act_id = \Yii::$app->request->post('act_id');
        $active = \Yii::$app->request->post('active');
        $invoice = Invoice::findOne(['id' => $invoice_id]);
        /* @var $act Act */
        $act = Act::findOne(['id' => $act_id]);
        $orders = [];
        if ($invoice !== null) {
            if ($invoice->orders != null) {
                foreach ($invoice->orders as $order) {
                    if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE && $order->quantity > OrderAct::find()
                            ->where(['order_id' => $order->id])->andWhere(['!=', 'act_id', $act_id])
                            ->sum('quantity')) {
                        $orders[] = $order;
                    }
                }
            }
        } else {
            foreach ($act->invoices as $invoice) {
                foreach ($invoice->orders as $order) {
                    if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE && $order->quantity > OrderAct::find()
                            ->where(['order_id' => $order->id])->andWhere(['!=', 'act_id', $act_id])
                            ->sum('quantity')) {
                        $orders[] = $order;
                    }
                }
            }
        }
        $result = [];
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }
        } else {
            foreach ($orders as $order) {
                $result[$order->id] = $order->product_title;
            }
        }
        return $this->renderAjax('/act/_viewPartials/_template_' . Documents::$ioTypeToUrl[$invoice->type], [
            'key' => $key,
            'result' => $result,
            'model' => $act,
            'plus' => 0
        ]);
    }

    /**
     * @return bool
     */
    public function actionDeleteRow()
    {
        $key = \Yii::$app->request->post('key');
        $result = [];
        $act_id = \Yii::$app->request->post('act_id');
        $orders = Act::findOne(['id' => $act_id])->invoice->orders;
        $active = \Yii::$app->request->post('active');
        if ($active != null) {
            foreach ($orders as $order) {
                if (!in_array((string)$order->id, $active)) {
                    $result[$order->id] = $order->product_title;
                }
            }

        } else {
            foreach ($orders as $order) {
                $result[$order->id] = $order->product_title;
            }
        }
        if ($result != null) {
            return $plus = 1;
        } else {
            return $plus = 0;
        }


    }

    /**
     * @return bool
     */
    public function actionClose($act_id, $ioType)
    {

        return $this->renderPartial('/act/_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => Act::findOne(['id' => $act_id]),
        ]);

    }

    /**
     * @return bool
     */
    public function actionEdit($act_id, $ioType)
    {
        $orders = OrderAct::getAvailable($act_id);
        if (OrderAct::getAvailable($act_id) == null) {
            return $is_editable = 0;
        } else {
            return $is_editable = 1;
        }

    }

}