<?php

namespace frontend\modules\documents\controllers;

use common\models\document\GoodsCancellationStatus;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderGoodsCancellation;
use common\models\document\OrderHelper;
use common\models\document\PackingList;
use common\models\product\Product;
use common\models\project\ProjectCustomer;
use common\models\project\ProjectEstimate;
use frontend\components\StatisticPeriod;
use frontend\models\log\LogEntityType;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\GoodsCancellationHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\GoodsCancellationSearch;
use Yii;
use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\document\GoodsCancellation;
use common\models\employee\Employee;
use common\models\project\Project;
use frontend\models\Documents;
use frontend\models\log\LogHelper;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\GoodsCancellationHelper as ModelHelper;
use frontend\modules\documents\components\Message;
use yii\base\Exception;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\AjaxFilter;
use yii\helpers\ArrayHelper;
use frontend\rbac\permissions;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use frontend\components\PageSize;
use yii\web\Response;

class GoodsCancellationController extends DocumentBaseController {

    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_GOODS_CANCELLATION;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'many-delete',
                            'comment-internal',
                            'field-project',
                            'field-project-estimate',
                            'document-print',
                            'send',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Product::WRITE_OFF,
                        ],
                    ],
                    [
                        'actions' => [
                            'add-modal-contractor',
                        ],
                        'allow' => true,
                        'roles' => [
                            permissions\Contractor::CREATE,
                        ],
                        'roleParams' => function ($rule) {
                            return [
                                'type' => Contractor::TYPE_CUSTOMER,
                            ];
                        },
                    ],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::class,
                'only' => [
                    'field-project',
                    'field-project-estimate',
                ],
            ]
        ];
    }

    public function actionIndex($type = null)
    {
        /* @var Company $company */
        $company = Yii::$app->user->identity->company;

        $searchModel = new GoodsCancellationSearch(['company_id' => $company->id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, StatisticPeriod::getSessionPeriod());
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $type,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionView($id)
    {
        /** @var GoodsCancellation $model */
        $model = $this->loadModel($id);

        return $this->render('view', [
            'model' => $model,
            'useContractor' => false,
            'backUrl' => Url::to(['index']),
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionCreate()
    {
        $returnUrl = Url::to(['index']);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        /* @var Company $company */
        $company = $user->company;

        $projectEstimateId = (int)Yii::$app->request->get('project_estimate_id', null);
        $hasProjectEstimate = !empty($projectEstimateId);
        $byDocument = null;

        if ($fromId = Yii::$app->request->get('clone')) {
            $model = $this->cloneModel($fromId);
        } else {
            if ($pl_id = Yii::$app->request->get('pl_id')) {
                $byDocument = $this->findPackinList($company, $pl_id);
                if ($existModel = $byDocument->goodsCancellations[0] ?? null) {
                    return $this->redirect(['view', 'id' => $existModel->id]);
                }
                $ordersData = GoodsCancellation::ordersDataFromPackingList($byDocument);
            }
            try {
                $model = $this->getNewModel();
            } catch (\Exception $e) {
                \Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->redirect('index');
            }
            if (isset($ordersData)) {
                GoodsCancellationHelper::ordersLoad($model, $ordersData);
            } elseif ($selectedProducts = Yii::$app->request->post('selection')) {
                $this->addSelectedProducts($model, $selectedProducts);
            }
        }

        $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $company, $byDocument) {
        
            if (ModelHelper::load($model, Yii::$app->request->post())) {
                LogHelper::$useTransaction = false;
                if (ModelHelper::save($model)) {
                    if ($byDocument !== null) {
                        $byDocument->link('goodsCancellations', $model);
                    }
                    return true;
                }
            }
        
            $db->transaction->rollBack();
        
            return false;
        });

        if ($isSaved) {
            $returnUrl = Url::to(['view', 'id' => $model->id]);
            return $this->redirect($returnUrl);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'fixedContractor' => false,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => Documents::IO_TYPE_IN,
            'returnUrl' => $returnUrl,
            'hasProjectEstimate' => $hasProjectEstimate,
            'projectEstimateId' => $projectEstimateId,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $returnUrl = Url::to(['view', 'id' => $id]);

        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        /* @var Company $company */
        $company = $user->company;

        /* @var $model GoodsCancellation */
        $model = $this->loadModel($id);
        $projectEstimateId = $model->project_estimate_id;
        $hasProjectEstimate = (bool)$projectEstimateId;

        if (ModelHelper::load($model, Yii::$app->request->post())) {
            $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $company) {
                LogHelper::$useTransaction = false;
                if (ModelHelper::save($model)) {
                    return true;
                }

                $db->transaction->rollBack();

                return false;
            });
        }

        if ($isSaved ?? false) {
            return $this->redirect($returnUrl);
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'fixedContractor' => false,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => Documents::IO_TYPE_IN,
            'returnUrl' => $returnUrl,
            'hasProjectEstimate' => $hasProjectEstimate,
            'projectEstimateId' => $projectEstimateId,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\web\ForbiddenHttpException
     *
     */
    public function actionDelete($id)
    {
        $returnUrl = Url::to(['index']);
        
        /** @var GoodsCancellation $model */
        $model = $this->loadModel($id);

        $isDeleted = LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);

        if ($isDeleted) {
            Yii::$app->session->setFlash('success', 'Списание удалено');
            return $this->redirect($returnUrl);
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка при удалении списания.');
            return $this->redirect(Yii::$app->request->referrer ?: $returnUrl);
        }
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $deletedCount = 0;
        $models = Yii::$app->request->post('GoodsCancellation');
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $model = $this->loadModel($id);

                $isDeleted = LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);

                if ($isDeleted)
                    $deletedCount++;
            }
        }

        if ($deletedCount) {
            Yii::$app->session->setFlash('success', 'Списания успешно удалены.');
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index']);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFieldProject()
    {
        $contractorId = (int)Yii::$app->request->get('contractorId', null);
        $projectId = (int)Yii::$app->request->get('projectId', null);

        $hasProject = Yii::$app->user->identity->menuItem->project_item;

        if (!$hasProject) {
            return "";
        }

        $company = Yii::$app->user->identity->company;

        $model = new GoodsCancellation();
        $model->company_id = $company->id;

        $projectIds = ProjectCustomer::find()
            ->joinWith('project', false)
            ->where(['project.company_id' => $company->id])
            ->andWhere(['project_customer.customer_id' => $contractorId])
            ->select('project.id')
            ->column();

        if ($projectIds) {
            $projects = Project::find()
                ->select('name')
                ->andWhere(['company_id' => $company->id])
                ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS))
                ->andWhere('start_date <= ' . (new Expression('current_date()')))
                ->andWhere('end_date >= ' . (new Expression('current_date()')))
                ->andWhere(['id' => $projectIds])
                ->indexBy('id')
                ->column();

            $actualProjectId = in_array($projectId, $projects) ? $projectId : ($projects ? array_key_first($projects) : 0);

        } else {
            $projects = [];
            $actualProjectId = 0;
        }

        return $this->renderPartial('form/_project_selecting', [
            'model' => $model,
            'projects' => $projects,
            'projectId' => $projectId ?: 0,
            'actualProjectId' => $actualProjectId,
            'contractorId' => (bool)$model->contractor_id,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionFieldProjectEstimate()
    {
        $contractorId = Yii::$app->request->get('contractorId', null);
        $projectId = Yii::$app->request->get('projectId', null);

        $hasProject = Yii::$app->user->identity->menuItem->project_item;

        if (!$hasProject) {
            return "";
        }

        $company = Yii::$app->user->identity->company;
        $model = new GoodsCancellation();
        $model->company_id = $company->id;

        $estimates = ProjectEstimate::find()
            ->select('name')
            ->andWhere(['project_id' => $projectId])
            ->andFilterWhere(['customer_id' => $contractorId])
            ->indexBy('id')
            ->column();

        $actualEstimateId = ($estimates) ? array_key_first($estimates) : 0;

        return $this->renderPartial('form/_project_estimate_selecting', [
            'model' => $model,
            'estimates' => $estimates,
            'projectId' => $projectId ?: 0,
            'actualEstimateId' => $actualEstimateId,
            'contractorId' => (bool)$model->contractor_id,
        ]);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCommentInternal($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id);

        $model->comment = Yii::$app->request->post('comment', '');
        $model->save(true, ['comment']);

        $model = $this->loadModel($id);

        return ['value' => $model->comment];
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type = null, $filename = null)
    {
        $model = $this->loadModel($id);

        if ($actionType === 'pdf') {
            $addStamp = Yii::$app->request->get('addStamp');
        } else {
            $addStamp = 0;
        }

        return $this->_documentPrint($model, DocumentPrintAsset::class, $actionType, 'pdf-view', [
            'addStamp' => $addStamp,
        ]);
    }

    public function actionSend($id)
    {
        /* @var GoodsCancellation $model */
        $model = $this->loadModel($id);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);

        //if ($model->uid == null) {
        //    $model->uid = GoodsCancellation::generateUid();
        //    $model->save(false, ['uid']);
        //}

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * @param $id
     * @return GoodsCancellation
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    protected function cloneModel($id)
    {
        /** @var GoodsCancellation $model */
        $model = $this->loadModel($id);
        $user = Yii::$app->user->identity;
        $company = Yii::$app->user->identity->company;

        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $cloneOrders = [];
        foreach ($model->orders as $order) {
            $cloneOrders[] = $this->cloneOrders($order, $clone);
        }
        $clone->populateRelation('orders', $cloneOrders);

        // changed data
        $clone->status_id = GoodsCancellationStatus::STATUS_CREATED;
        $clone->document_author_id = $user->id;
        $clone->responsible_employee_id = $user->id;
        $clone->document_number = (string)GoodsCancellation::getNextDocumentNumber($company->id, null, $model->document_date);
        $clone->document_date = date('d.m.Y');

        return $clone;
    }

    /**
     * @param null $ioType
     * @return GoodsCancellation
     * @throws Exception
     */
    protected function getNewModel($ioType = null)
    {
        return GoodsCancellation::newModel(Yii::$app->user->identity->currentEmployeeCompany);
    }

    public function addSelectedProducts(&$model, $productsIds)
    {
        $orderArray = [];
        foreach ((array)$productsIds as $pid) {
            if ($product = Product::getById($model->company_id, $pid)) {
                $orderArray[] = GoodsCancellationHelper::createOrderByProduct($product, $model, 1);
            }
        }
        $model->populateRelation('orders', $orderArray);
        $model->beforeValidate();
    }

    /**
     * @param OrderGoodsCancellation $order
     * @param GoodsCancellation $model
     * @return OrderGoodsCancellation
     */
    public function cloneOrders(OrderGoodsCancellation $order, GoodsCancellation $model)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->goods_cancellation_id = null;
        $cloneOrder->populateRelation('goodsCancellation', $model);

        return $cloneOrder;
    }

    /**
     * Find an existing PackingList model
     * @param  Company $company
     * @param  int $id
     * @return PackingList
     * @throws NotFoundHttpException
     */
    private function findPackinList($company, $id)
    {
        /* @var PackingList $model */
        $model = PackingList::find()
            ->joinWith('invoice.company')
            ->andWhere([
                'packing_list.id' => $id,
                'invoice.company_id' => $company->id,
            ])->one();

        if ($model === null) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $model;
    }
}
