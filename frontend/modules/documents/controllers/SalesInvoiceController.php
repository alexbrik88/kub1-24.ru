<?php

namespace frontend\modules\documents\controllers;

use common\components\date\DateHelper;
use common\components\filters\AccessControl;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\image\EasyThumbnailImage;
use common\components\pdf\PdfRenderer;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderSalesInvoice;
use common\models\document\SalesInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\SalesInvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\DocumentBaseController;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\documents\models\InvoiceSearch;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class SalesInvoiceController
 * @package frontend\modules\documents\controllers
 */
class SalesInvoiceController extends DocumentBaseController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_SALES_INVOICE;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-xls', 'generate-xls', 'add-modal-contractor', 'get-invoices'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::INDEX, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-create'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-document-print', 'add-stamp'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::VIEW, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]);
                        },
                    ],
                    [
                        'actions' => ['many-delete'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-update-status', 'many-update-original'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS, [
                                    'ioType' => Yii::$app->request->get('type'),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['send-many-in-one'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                    'model' => $action->controller->loadModel(),
                                ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['many-send', 'get-many-send-message-panel'],
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->getUser()->can(permissions\document\Document::UPDATE, [
                                'ioType' => Yii::$app->request->get('type'),
                            ]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
                        },
                    ],
                    [
                        'actions' => ['out-view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['upd',],
                        'allow' => YII_ENV === 'dev',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return array_merge(parent::actions(), [
            'add-modal-contractor' => [
                'class' => 'frontend\components\AddModalContractorAction',
            ],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function beforeAction($action)
    {
        $type = Yii::$app->request->get('type', 2);

        if ($type != Documents::IO_TYPE_OUT) {
            throw new ForbiddenHttpException('Расходные накладные недоступны для выбранного типа');
        }

        return parent::beforeAction($action);
    }

    /**
     * @param $type
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws ForbiddenHttpException
     */
    public function actionView($type, $id)
    {
        /** @var SalesInvoice $model */
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;

        return $this->render('view', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => Documents::IO_TYPE_OUT,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }


    public function actionUpdate($type, $id)
    {
        $model = $this->loadModel($id, $type);
        $useContractor = Yii::$app->request->getQueryParam('contractorId', null) == $model->invoice->contractor_id;
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->post('OrderSalesInvoice') != null) {
                foreach (Yii::$app->request->post('OrderSalesInvoice') as $key => $value) {
                    $opl = OrderSalesInvoice::findOne(['order_id' => $key, 'sales_invoice_id' => $model->id]);
                    if ($value['quantity'] > 0 && $value['status'] == 'active') {
                        if ($opl != null) {
                            if ($opl->getAvailableQuantity() >= $value['quantity'] && $value['quantity'] > 0) {
                                OrderSalesInvoice::setQuantity($key, $model->id, $value['quantity']);
                            }
                        } else {

                            if ($value['quantity'] != 0 && $value['quantity'] <= OrderSalesInvoice::compareWithOrderByQuantity(Order::findOne(['id' => $key]))) {
                                OrderSalesInvoice::createFromOrder($model->id, Order::findOne(['id' => $key]))->save();
                            }

                        }
                    } elseif ($value['status'] == 'deleted' && OrderSalesInvoice::ifExist($key, $model->id)) {
                        OrderSalesInvoice::deleteAll(['order_id' => $key, 'sales_invoice_id' => $model->id]);
                    }

                }

                Yii::$app->db->transaction(function ($db) use ($model, $type, $useContractor) {
                    $model->document_date = DateHelper::format($model->document_date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                    $model->ordinal_document_number = $model->document_number;
                    $model->orders_sum = $model->totalAmountWithNds;
                    $model->order_nds = $model->totalNds;
                    if (!$model->save()) {
                        foreach ($model->getErrors() as $error) {
                            Yii::$app->session->setFlash('error', $error);
                        }
                    } else {
                        LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE);
                        Yii::$app->session->setFlash('success', 'Расходная накладная изменена');
                    }
                    if ($model->errors) {
                        return $this->refresh();
                    }

                });

                if ($this->isScanPreview) {
                    Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                    return $this->redirect(Url::previous('lastUploadManagerPage'));
                }

                return $this->redirect([
                    'view',
                    'type' => $type,
                    'id' => $model->id,
                ]);
            } else {
                $model->delete();
                return $this->render('index');
            }

            return $this->refresh();
        }

        return $this->render('update', [
            'model' => $model,
            'message' => new Message($this->typeDocument, $model->type),
            'ioType' => $type,
            'useContractor' => $useContractor,
            'contractorId' => $useContractor ? $model->invoice->contractor_id : null,
        ]);
    }

    /**
     * @param $uid
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionOutView($uid)
    {
        /* @var Invoice $model */
        $model = SalesInvoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byUid($uid)
            ->one();

        if ($model === null || !$model->company || $model->company->blocked) {
            throw new NotFoundHttpException('Запрошенная страница не существует');
        }

        return $this->_documentPrint($model, DocumentPrintAsset::className(), 'print', 'pdf-view', [
            'isOutView' => true,
            'addStamp' => true,
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $type
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionDocumentPrint($actionType, $id, $type, $filename)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'out-view', [
            'documentFormat' => 'A4-L',
            'sales_invoice_id' => $id,
        ]);
    }

    public function actionDocumentPng($id, $type, $page = 1)
    {
        $model = $this->loadModel($id, $type);

        return $this->_documentPng($model, $page, [
            'documentFormat' => 'A4-L',
        ]);
    }

    /**
     * @param $actionType
     * @param $type
     * @param $multiple
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionManyDocumentPrint($actionType, $type, $multiple)
    {
        $SalesInvoicesID = explode(',', $multiple);
        /* @var $model SalesInvoice */
        $model = $this->loadModel(current($SalesInvoicesID), $type);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'out-view', [
            'documentFormat' => 'A4-L',
            'addStamp' => ($actionType === 'pdf' && $model->invoice->company->pdf_signed) ? true : false,
            'multiple' => $SalesInvoicesID,
        ]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSend($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /* @var SalesInvoice $model */
        $model = $this->loadModel($id, $type);
        $sendForm = new InvoiceSendForm(Yii::$app->user->identity->currentEmployeeCompany, [
            'model' => $model,
        ]);
        if ($model->uid == null) {
            $model->uid = SalesInvoice::generateUid();
            $model->save(false, ['uid']);
        }

        if (Yii::$app->request->isAjax && $sendForm->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($sendForm);
        }

        if ($sendForm->load(Yii::$app->request->post()) && $sendForm->validate()) {
            $saved = $sendForm->send();
            if (is_array($saved)) {
                Yii::$app->session->setFlash('success', 'Отправлено ' . $saved[0] . ' из ' . $saved[1] . ' писем.');
                // $model->email_messages += $saved[0];
                // $model->save(true, ['email_messages']);
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка при отправке писем.');
            }
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param $type
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function actionManySend($type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $company = Yii::$app->user->identity->company;
        $contractors = null;
        $SalesInvoices = Yii::$app->request->post('SalesInvoice');
        /** @var Employee $sender */
        $sender = \Yii::$app->user->identity;
        $send = 0;
        $contractorSalesInvoices = [];
        foreach ($SalesInvoices as $id => $SalesInvoice) {
            if ($SalesInvoice['checked']) {
                /* @var $model SalesInvoice */
                $model = $this->loadModel($id, $type);
                if (!Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $model])) {
                    continue;
                }
                $contractorSalesInvoices[$model->invoice->contractor_id][] = $model->id;
            }
        }
        foreach ($contractorSalesInvoices as $contractorID => $SalesInvoices) {
            if (count($SalesInvoices) > 1) {
                $contractor = Contractor::findOne($contractorID);
                $sendTo = $contractor->someEmail;
                if ($sendTo) {
                    if (SalesInvoice::manySend($sender->currentEmployeeCompany, $sendTo, $type, $SalesInvoices)) {
                        $send++;
                    }
                } else {
                    $contractors[$contractor->id] = Html::a($contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $contractor->type, 'id' => $contractor->id
                    ]));
                }
            } else {
                $SalesInvoiceID = current($SalesInvoices);
                /* @var $model SalesInvoice */
                $model = $this->loadModel($SalesInvoiceID, $type);
                if ($model->uid == null) {
                    $model->uid = SalesInvoice::generateUid();
                    $model->save(false, ['uid']);
                }
                $sendTo = $model->invoice->contractor->someEmail;
                if ($sendTo) {
                    $params = [
                        'model' => $model,
                        'employee' => $sender,
                        'employeeCompany' => $sender->currentEmployeeCompany,
                        'subject' => 'Товарная накладная',
                        'pixel' => [
                            'company_id' => $model->invoice->company_id,
                            'email' => $sendTo,
                        ],
                    ];
                    $oneCFilePath = $model->generateOneCFile();

                    \Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                    if (\Yii::$app->mailer->compose([
                        'html' => 'system/documents/sales-invoice-out/html',
                        'text' => 'system/documents/sales-invoice-out/text',
                    ], $params)
                        ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                        ->setReplyTo([$sender->email => $sender->getFio(true)])
                        ->setSubject('Товарная накладная No ' . $model->fullNumber
                            . ' от ' . DateHelper::format($model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
                            . ' от ' . $model->invoice->company_name_short)
                        ->attachContent(SalesInvoice::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => $model->pdfFileName,
                            'contentType' => 'application/pdf',
                        ])
                        ->attach($oneCFilePath, [
                            'fileName' => $model->getOneCFileName(),
                            'mime' => 'application/xml',
                        ])
                        ->setTo($sendTo)
                        ->send()
                    ) {
                        $send++;
                        InvoiceSendForm::setSalesInvoiceSendStatus($model);
                    }
                } else {
                    $contractors[$model->invoice->contractor->id] = Html::a($model->invoice->contractor->nameWithType, Url::to(['/contractor/view',
                        'type' => $model->invoice->contractor->type, 'id' => $model->invoice->contractor->id
                    ]));
                }
            }
        }
        if ($send) {
            CompanyFirstEvent::checkEvent($company, 24);
        }
        $message = 'Отправлено ' . $send . ' из ' . count($contractorSalesInvoices) . ' писем.';
        if ($send == count($contractorSalesInvoices)) {
            Yii::$app->session->setFlash('success', $message);
        }
        $notSend = false;
        if ($contractors) {
            $notSend = 'Для отправки расходных накладных, необхоидмо заполнить E-mail у покупателей<br>';
            foreach ($contractors as $contractorName) {
                $notSend .= $contractorName . '<br>';
            }
        }

        return $notSend ? ['notSend' => $notSend, 'message' => $message] :
            $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($type, $id)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        /** @var SalesInvoice $model */
        $model = $this->loadModel($id, $type);

        $status = (int)Yii::$app->request->getBodyParam('status');

        $this->_updateStatus($model, $status);

        switch ($model->status_out_id) {
            case SalesInvoiceStatus::STATUS_SEND:
                Yii::$app->session->setFlash('success', 'Расходная накладная передана');
                break;
            case SalesInvoiceStatus::STATUS_RECEIVED:
                Yii::$app->session->setFlash('success', 'Расходная накладная подписана');
                break;
        }

        return $this->redirect([
            'view',
            'type' => $type,
            'id' => $model->id,
            'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
        ]);
    }

    /**
     * @param SalesInvoice $model
     * @param $status
     * @throws \yii\base\Exception
     */
    private function _updateStatus(SalesInvoice $model, $status)
    {
        if ($status !== null && $status != $model->status_out_id) {
            $model->status_out_id = $status;

            LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS,
                function (SalesInvoice $model) {
                    return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                });
        }
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \yii\web\ForbiddenHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type);
        $documentNumber = Yii::$app->request->get('documentNumber');
        $documentDate = Yii::$app->request->get('documentDate');
        $isFromInvoiceForm = Yii::$app->request->get('isFromInvoiceForm');
        if (empty($documentDate) && $invoice->type == Documents::IO_TYPE_IN) {
            $documentDate = DateHelper::format($invoice->document_date, 'd.m.Y', 'Y-m-d');
            $documentNumber = $invoice->document_number;

        }
        if ($invoice->canAddSalesInvoice) {
            if (!$invoice->need_sales_invoice) {
                if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                    $invoice->need_sales_invoice = true;
                    $invoice->save(true, ['need_sales_invoice']);
                } else {
                    throw new BadRequestHttpException();
                }
            }
            if ($invoice->createSalesInvoice($documentDate, $documentNumber)) {
                $orderDocument = $invoice->orderDocument;
                if ($orderDocument) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                }

                Yii::$app->session->setFlash('success', 'Расходная накладная создана.');

                Product::updateGrowingPriceForBuy($invoice);

                if ($this->isScanPreview)
                {
                    if (Yii::$app->request->get('returnToManager')) {
                        Yii::$app->session->setFlash('success', UploadManagerHelper::getAttachFilesMessage());
                        return $this->redirect(Url::previous('lastUploadManagerPage'));
                    }

                    return $this->redirect([
                        '/documents/sales-invoice/update',
                        'type' => $invoice->salesInvoice->type,
                        'id' => $invoice->salesInvoice->id,
                        'mode' => 'previewScan',
                        'files' => Yii::$app->request->get('files')
                    ]);
                }

                if ($invoice->salesInvoice->type == Documents::IO_TYPE_IN && !$isFromInvoiceForm) {
                    return $this->redirect([
                        '/documents/sales-invoice/update',
                        'type' => $invoice->salesInvoice->type,
                        'id' => $invoice->salesInvoice->id
                    ]);
                }

                return $this->redirect([
                    'view',
                    'type' => $invoice->salesInvoice->type,
                    'id' => $invoice->salesInvoice->id,
                    'contractorId' => Yii::$app->request->getQueryParam('contractorId', null),
                ]);
            } else {
                var_dump($invoice->errors);die;
                Yii::$app->session->setFlash('error', 'При создании расходной накладной возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать расходную накладную.');
        }

        return $this->redirect(Yii::$app->request->referrer ?: ['invoice/index', 'type' => $type,]);
    }

    /**
     * @param $type
     * @param null $contractor
     * @return Response
     * @throws BadRequestHttpException
     */
    public function actionManyCreate($type, $contractor = null)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $idArray = [];
        $documentDate = !empty(Yii::$app->request->post('SalesInvoice')['document_date']) ?
            Yii::$app->request->post('SalesInvoice')['document_date'] : null;
        $models = Yii::$app->request->post('Invoice');
        unset($models['document_date']);
        $fromInvoice = true;
        if (!$models) {
            $models = Yii::$app->request->post('OrderDocument');
            $fromInvoice = false;
        }
        foreach ($models as $id => $model) {
            if ($model['checked']) {
                $idArray[] = $id;
            }
        }
        $createdPL = 0;
        if ($fromInvoice) {
            $invoiceArray = $company->getInvoices()
                ->andWhere(['invoice.id' => $idArray])
                ->orderBy(['invoice.document_number' => SORT_ASC, 'invoice.document_additional_number' => SORT_ASC])
                ->all();

            /* @var $invoice Invoice */
            foreach ($invoiceArray as $invoice) {
                if ($invoice->canAddSalesInvoice) {
                    if (!$invoice->need_sales_invoice) {
                        if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
                            $invoice->need_sales_invoice = true;
                            $invoice->save(true, ['need_sales_invoice']);
                        } else {
                            throw new BadRequestHttpException();
                        }
                    }
                    $orderDocument = $invoice->orderDocument;
                    if ($orderDocument) {
                        $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                        $orderDocument->save(true, ['status_id']);
                    }
                    if ($invoice->createSalesInvoice($documentDate))
                        $createdPL++;
                }
            }
        } else {
            /* @var $orderDocuments OrderDocument[] */
            $orderDocuments = OrderDocument::find()
                ->andWhere(['and',
                    ['is_deleted' => false],
                    ['id' => $idArray],
                    ['not', ['invoice_id' => null]],
                    ['company_id' => $company->id],
                ])->all();
            foreach ($orderDocuments as $orderDocument) {
                if ($orderDocument->invoice && $orderDocument->invoice->getCanAddSalesInvoice()) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                    if ($orderDocument->invoice->createSalesInvoice($documentDate))
                        $createdPL++;
                }
            }

            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['order-document/index']);
        }

        if ($createdPL > 0) {
            Yii::$app->session->setFlash('success', ($createdPL == 1) ? 'Расходная накладная создана' : 'Расходные накладные созданы');
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось создать расходную накладную');
        }

        if ($contractor) {
            return $this->redirect(['/contractor/view', 'type' => $type, 'id' => $contractor]);
        } else {
            return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['invoice/index', 'type' => $type]);
        }
    }


    /**
     * Deletes an existing Act model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $type
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($type, $id)
    {
        $model = $this->loadModel($id, $type);
        if (LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT)) {
            Yii::$app->session->setFlash('success', 'Расходная накладная удалена');
        } else {
            Yii::$app->session->setFlash('error', 'Расходную накладную не удалось удалить');
        }

        return $this->redirectAfterDelete($type, $model);
    }

    /**
     * @param $type
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyDelete($type)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $SalesInvoices = Yii::$app->request->post('SalesInvoice');
        foreach ($SalesInvoices as $id => $SalesInvoice) {
            if ($SalesInvoice['checked']) {
                $model = $this->loadModel($id, $type);
                LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT);
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @return mixed
     */
    public function actionUpd()
    {
        $renderer = new PdfRenderer([
            'view' => 'upd',
            'params' => [],

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => 'upd',
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => ArrayHelper::getValue([], 'documentFormat', 'A4-P'),
        ]);

        return $renderer->renderHtml();
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGenerateXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $period = StatisticPeriod::getSessionPeriod();
        $searchModelOut = Documents::loadSearchProvider($this->typeDocument, Documents::IO_TYPE_OUT);
        $SalesInvoices = $searchModelOut->search(Yii::$app->request->queryParams, $period, true);

        SalesInvoice::generateXlsTable($period, $SalesInvoices);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionAddStamp($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->loadModel($id, Documents::IO_TYPE_OUT);

        return $model->updateAttributes(['add_stamp' => (boolean)Yii::$app->request->post('add_stamp')]);
    }

    /**
     * @return string
     */
    public function actionGetInvoices()
    {
        $this->layout = '@frontend/views/layouts/empty';

        $ioType    = \Yii::$app->request->get('type');
        $labelName = \Yii::$app->request->get('label_name');
        $dateFrom  = \Yii::$app->request->get('date_from');
        $dateTo    = \Yii::$app->request->get('date_to');

        if ($dateFrom && $dateTo && $labelName) {
            $dateRange = [
                'from' => $dateFrom,
                'to'   => $dateTo
            ];
        } else {
            $labelName = StatisticPeriod::getSessionName();
            $dateRange = StatisticPeriod::getSessionPeriod();
        }

        $searchModel = new InvoiceSearch([
            'type' => $ioType,
            'can_add_sales_invoice' => true,
            'company_id' => \Yii::$app->user->identity->company->id,
        ]);

        $documentNumber = \Yii::$app->request->post('documentNumber');
        if (!empty($documentNumber)) {
            $searchModel->document_number = $documentNumber;
        }

        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams, $dateRange);
        $dataProvider->query->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]]);

        return $this->renderAjax('@frontend/modules/documents/views/invoice/modal/_invoices_modal_table', [
            'documentTypeName' => 'расходную накладную',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ioType' => $ioType,
            'documentType' => Documents::SLUG_SALES_INVOICE,
            'canChangePeriod' => false,
            'documentNumber' => $documentNumber,
            'dateFrom' => DateHelper::format($dateRange['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'dateTo' => DateHelper::format($dateRange['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            'labelName' => $labelName
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionGetXls()
    {
        if (!Yii::$app->user->identity->company->getHasPaidActualSubscription()) {
            Yii::$app->session->setFlash('error', 'Выгрузить данные в Excel можно только на платном тарифе.');
            return $this->redirect(['/subscribe/default/index']);
        }

        $ioType = Yii::$app->request->get('type', Documents::IO_TYPE_OUT);
        $period = StatisticPeriod::getSessionPeriod();
        $searchModel = Documents::loadSearchProvider($this->typeDocument, $ioType);
        $SalesInvoices = $searchModel->search(Yii::$app->request->queryParams, $period, true, true);

        SalesInvoice::generateXlsTable($period, $SalesInvoices);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateStatus($type, $status)
    {
        if ($type != Documents::IO_TYPE_OUT) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('SalesInvoice', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model SalesInvoice */
                    $model = $this->loadModel($id, $type);

                    if ($status !== null && $status != $model->status_out_id) {
                        $model->status_out_id = $status;

                        LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (SalesInvoice $model) {
                            return $model->save(true, ['status_out_id', 'status_out_author_id', 'status_out_updated_at']);
                        });
                    }

                    switch ($status) {
                        case SalesInvoiceStatus::STATUS_SEND:
                            Yii::$app->session->setFlash('success', 'Расходные накладные переданы');
                            break;
                        case SalesInvoiceStatus::STATUS_RECEIVED:
                            Yii::$app->session->setFlash('success', 'Расходные накладные подписаны');
                            break;
                    }
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }

    /**
     * @param $type
     * @pararm $status
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionManyUpdateOriginal($type, $val)
    {
        if ($type != Documents::IO_TYPE_IN) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $docs = Yii::$app->request->post('SalesInvoice', []);
        if (is_array($docs)) {
            foreach ($docs as $id => $doc) {
                if ($doc['checked']) {
                    /* @var $model SalesInvoice */
                    $model = $this->loadModel($id, $type);
                    $model->is_original = intval($val) ? true : false;
                    $model->save(false, ['is_original', 'is_original_updated_at']);
                    Yii::$app->session->setFlash('success', 'Расходные накладные сохранены');
                }
            }
        }

        return $this->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : ['index', 'type' => $type]);
    }
}
