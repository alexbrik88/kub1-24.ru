<?php

namespace frontend\modules\documents\models;

use common\models\Company;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use common\models\file\File;

/**
 * This is the model class for table "specific_document".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $document_author_id
 * @property string $document_date
 * @property string $name
 * @property string $description
 */
class SpecificDocument extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public static $uploadDirectory = 'specific';

    /**
     * @var
     */
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specific_document';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required', 'message' => 'Необходимо заполнить'],
            [['file'], 'required', 'on' => 'create', 'message' => 'Необходимо выбрать файл'],
            [['created_at', 'updated_at', 'document_author_id'], 'integer'],
            [['document_date'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, jpeg, png, pdf, doc, docx, xls, xlsx, txt, xml', 'maxSize' => '3145728'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'document_author_id' => 'Document Author ID',
            'document_date' => 'Дата',
            'name' => 'Название',
            'description' => 'Описание',
            'file' => "файл",
            'updated_at' => 'Дата изменения'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getSavedFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => SpecificDocument::className()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return "";
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return '#';
    }
}
