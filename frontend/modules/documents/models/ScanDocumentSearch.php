<?php

namespace frontend\modules\documents\models;

use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\ScanDocument;
use common\models\employee\Employee;
use common\models\file\File;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ScanDocumentSearch represents the model behind the search form about `frontend\modules\documents\models\ScanDocument`.
 */
class ScanDocumentSearch extends ScanDocument
{
    public $free = false;
    public $fromDirectory;
    public $fromUploads;

    private $_query;
    private $_contractor_items;
    private $_type_items;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id', 'document_type_id'], 'integer'],
            [['name'], 'string'],
            [['free'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScanDocument::find();

        $query->with('file')->andWhere(['company_id' => $this->company_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        if ($this->free) {
            $query->andWhere(['owner_id' => null]);
        }

        if ($this->fromUploads || $this->fromDirectory) {
            $query2 = File::find();
            $query2->andWhere(['company_id' => $this->company_id]);
            $query2->andWhere(['owner_model' => ScanDocument::className()]);

            if ($this->fromDirectory) {
                $query2->andWhere(['directory_id' => (int)$this->fromDirectory]);
            } else {
                $query2->andWhere(['is', 'directory_id', null]);
            }

            if ($freeScansIds = $query2->asArray()->select('owner_id')->column()) {
                $query->andWhere(['id' => $freeScansIds]);
            } else {
                $query->where('0=1');
            }
        }

        $queryDeleted = File::find()->where([
            'company_id' => $this->company_id,
            'owner_model' => ScanDocument::className(),
            'is_deleted' => true
        ])->asArray()->select('owner_id')->column();

        $query->andFilterWhere([
            'contractor_id' => $this->contractor_id,
            'document_type_id' => $this->document_type_id,
        ]);
        $query->andWhere(['not in' , 'id', $queryDeleted]);

        $query->andFilterWhere([
            'or',
            ['like', 'name', $this->name],
            ['like', 'description', $this->name],
        ]);

        $this->_query = $query;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getContractorFilterItems()
    {
        if (!isset($this->_contractor_items)) {
            $query = clone $this->_query;
            $tContractor = Contractor::tableName();
            $tType = CompanyType::tableName();
            $this->_contractor_items = Contractor::getSorted()->select([
                'contractor_name' => "IF(
                    {{{$tType}}}.[[id]] IS NULl,
                    {{{$tContractor}}}.[[name]],
                    CONCAT({{{$tType}}}.[[name_short]], ' ', {{{$tContractor}}}.[[name]])
                )",
                "{$tContractor}.id",
            ])->andWhere([
                "{$tContractor}.id" => $query->select('contractor_id')->distinct()->column(),
            ])->indexBy('id')->column();
        }

        return ['' => 'Все'] + $this->_contractor_items;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getTypeFilterItems()
    {
        if (!isset($this->_type_items)) {
            $query = clone $this->_query;
            $typeIds = $query->select('document_type_id')->distinct()->column();
            $this->_type_items = [];
            foreach ($typeIds as $id) {
                if ($name = Documents::typeNameById($id)) {
                    $this->_type_items[$id] = $name;
                }
            }
            asort($this->_type_items);
        }

        return ['' => 'Все'] + $this->_type_items;
    }

    public function getEmployees() {

        return ['' => 'Все'] + ArrayHelper::map(
                Employee::find()
                    ->where(['company_id' => Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true);
            });
    }
}
