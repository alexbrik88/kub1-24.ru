<?php

namespace frontend\modules\documents\models;

use common\components\helpers\ModelHelper;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "scan_upload_email".
 *
 * @property integer $id
 * @property string $email
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $created_at
 * @property integer $enabled
 *
 * @property Company $company
 * @property Employee $employee
 */
class ScanUploadEmail extends \yii\db\ActiveRecord
{
    protected $_customEmailPart;
    protected $_basisEmailPart;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_upload_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'enabled', 'company_id', 'customEmailPart', 'basisEmailPart'], 'required'],
            [['customEmailPart'], 'match', 'pattern' => '/^[a-zA-Z0-9-]+$/', 'message' => 'Некорректный e-mail'],
            [['company_id', 'employee_id', 'created_at'], 'integer'],
            [['enabled'], 'boolean'],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['customEmailPart'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'created_at' => 'Created At',
            'enabled' => 'Включить',
            'customEmailPart' => 'Email',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            ModelHelper::HtmlEntitiesModelAttributes($this);

            if ($insert) {
                $this->created_at = time();
                $this->employee_id = Yii::$app->user->id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @param Company $company
     * @return string
     */
    public static function generateEmail($company_name) {

        $custom_part = self::translitRussianNames($company_name);
        $code_part = self::generateCodePart();

        $custom_part = preg_replace("/[[:space:]]/u", '-', $custom_part);
        $custom_part = preg_replace("/[^[:alnum:]-]/u", '', $custom_part);
        $email_domain = ArrayHelper::getValue(Yii::$app->params['vesta'], 'mail_domain');

        return $custom_part.'.'.$code_part.'@'.$email_domain;
    }

    protected static function generateCodePart($numCount = 3, $alphaCount = 4) {

        $numCharacters = '0123456789';
        $alphaCharacters = 'abcdefghijklmnopqrstuvwxyz';
        $randomString = '';
        for ($i = 0; $i < $numCount; $i++) {
            $randomString .= $numCharacters[rand(0, strlen($numCharacters) - 1)];
        }
        for ($i = 0; $i < $alphaCount; $i++) {
            $randomString .= $alphaCharacters[rand(0, strlen($alphaCharacters) - 1)];
        }

        return $randomString;
    }

    // The Yandex transliteration rules (http://www.translityandex.ru/)
    protected static function translitRussianNames( $name ) {
        $cyr = array(
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я',
            'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'ы', 'Ь', 'Э', 'Ю', 'Я' );
        $lat = array(
            'a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya',
            'a', 'b', 'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya');
        $name_translit = str_replace($cyr, $lat, $name);
        return $name_translit;
    }

    public function getCustomEmailPart() {
        return $this->_customEmailPart;
    }

    public function setCustomEmailPart($value) {
        $this->_customEmailPart = $value;
    }

    public function getBasisEmailPart() {
        return $this->_basisEmailPart;
    }

    public function setBasisEmailPart($value) {
        $this->_basisEmailPart = $value;
    }

    public function validateBasisPart($oldEmail) {
        $old = explode('.', $oldEmail);
        $new = explode('.', $this->email);
        array_shift($old);
        array_shift($new);

        if ($old == $new) {
            return true;
        }

        $this->addError('email', 'Некорректный email!');
        return false;
    }


    /**
     *  GET FROM EMAIL
     */


}
