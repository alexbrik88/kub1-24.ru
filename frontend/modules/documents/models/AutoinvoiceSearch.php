<?php

namespace frontend\modules\documents\models;

use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\document\InvoiceAuto;
use common\models\document\Order;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\document\Autoinvoice;

/**
 * AutoinvoiceSearch represents the model behind the search form about `common\models\document\Autoinvoice`.
 */
class AutoinvoiceSearch extends Autoinvoice
{
    public $product_id;
    public $amount;
    public $find_by;

    protected $_searchQuery;

    /**
     * @inheritdoc
     */
    public function setSearchQuery(ActiveQuery $query)
    {
        $this->_searchQuery = clone $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchQuery()
    {
        return $this->_searchQuery;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'contractor_id', 'product_id', 'find_by', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function getProductList()
    {
        $searchQuery = clone $this->getSearchQuery();
        $query = Order::find()
            ->select(['order.product_id', 'product.title'])
            ->distinct()
            ->joinWith('product')
            ->andWhere([
                'order.invoice_id' => $searchQuery->select(['autoinvoice.id'])->column(),
            ])
            ->orderBy(['product.title' => SORT_ASC])
            ->asArray();
        return ['' => 'Все'] + ArrayHelper::map($query->all(), 'product_id', 'title');
    }

    /**
     * @inheritdoc
     */
    public function getContractorList()
    {
        $searchQuery = clone $this->getSearchQuery();
        $query = Contractor::getSorted()->andWhere(['contractor.id' => $searchQuery->select(['autoinvoice.contractor_id'])->distinct()->column()]);
        return ['' => 'Все'] + ArrayHelper::map($query->all(), 'id', 'shortName');
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find()
            ->select(['autoinvoice.*', 'invoice.total_amount_with_nds AS amount'])
            ->joinWith('invoice')
            ->joinWith('contractor')
            ->andWhere([
                'invoice.is_deleted' => false,
                'autoinvoice.company_id' => $this->company_id,
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'created_at',
                    'document_number',
                    'last_invoice_date',
                    'next_invoice_date',
                    'amount',
                ],
                'defaultOrder' => [
                    'document_number' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'autoinvoice.period' => $this->period,
            'autoinvoice.contractor_id' => $this->contractor_id,
            'autoinvoice.status' => $this->status,
        ]);

        $query->andFilterWhere([
            'or',
            ['autoinvoice.document_number' => $this->find_by],
            ['like', 'contractor.name', $this->find_by]
        ]);

        if ($this->product_id) {
            $query->leftJoin([
                'o' => Order::find()
                    ->select(['invoice_id', 'id'])
                    ->where(['product_id' => $this->product_id])
            ], "{{invoice}}.[[id]] = {{o}}.[[invoice_id]]")
            ->andWhere(['not', ['o.id' => null]])
            ->groupBy('autoinvoice.id');
        }

        $this->searchQuery = $query;

        return $dataProvider;
    }
}
