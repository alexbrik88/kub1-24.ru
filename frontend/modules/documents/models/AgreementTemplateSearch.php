<?php

namespace frontend\modules\documents\models;

use frontend\rbac\permissions\Employee;
use Yii;
use common\models\AgreementTemplate;
use common\models\Contractor;
use common\models\employee\Employee as EmployeeModel;
use common\models\company\CompanyType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use yii\db\Query;
use common\models\AgreementType;

/**
 * AgreementTemplateSearch represents the model behind the search form about `common\models\AgreementTemplate`.
 */
class AgreementTemplateSearch extends AgreementTemplate
{
    protected $query;

    public $find_by;
    public $type;
    public $status;
    public $created_by;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_type_id', 'type', 'status', 'created_by', 'find_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $A = static::tableName();
        $AT = AgreementType::tableName();

        $query = new Query;
        $query->select([
            "$A.id",
            "$A.document_number",
            "$A.document_additional_number",
            "$A.document_date",
            "$A.document_name",
            "$A.type",
            "$A.documents_created",
            "$A.status",
            "$A.created_by",
            "$AT.name as agreementType"])
            ->from($A)
            ->leftJoin($AT, "{{%$AT}}.[[id]] = {{%$A}}.[[document_type_id]]")
            ->where(["$A.company_id" => Yii::$app->user->identity->company->id])
            ->andWhere(['and', "$A.status > 0"]);

        $query->andFilterWhere([
            "$A.type" => $this->type,
            "$A.status" => $this->status,
            "$A.created_by" => $this->created_by,
            "$A.document_type_id" => $this->document_type_id
        ]);

        $query->andFilterWhere([
            'or',
            ['agreement_template.document_number' => $this->find_by],
            ['like', 'agreement_template.document_name', $this->find_by]
        ]);

        $countQuery = clone $query;
        $count = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'document_number',
                    'document_date',
                    'document_name',
                    'documents_created'
                ],
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);
    }

    public function getCreators() {

        return ['' => 'Все'] + ArrayHelper::map(
                \common\models\employee\Employee::find()
                    ->innerJoin('agreement_template', 'agreement_template.created_by = employee.id')
                    ->where(['agreement_template.company_id'=>Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true);
            });
    }

    /**
     * @return array
     */
    public function getAgreementTypeFilterItems()
    {
        return ['' => 'Все'] + ArrayHelper::map(
                AgreementType::find()
                    ->innerJoin('agreement_template', 'agreement_template.document_type_id = agreement_type.id')
                    ->where(['agreement_template.company_id'=>Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', 'name');
    }
}
