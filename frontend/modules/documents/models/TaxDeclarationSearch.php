<?php

namespace frontend\modules\documents\models;

use common\components\helpers\Month;
use frontend\modules\tax\models\DeclarationOsnoHelper;
use frontend\modules\tax\models\TaxDeclarationQuarter;
use frontend\rbac\permissions\Employee;
use Yii;
use frontend\modules\tax\models\TaxDeclaration;
use common\models\Contractor;
use common\models\employee\Employee as EmployeeModel;
use common\models\company\CompanyType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use yii\db\Query;

/**
 * TaxDeclarationSearch represents the model behind the search form about `common\models\TaxDeclaration`.
 */
class TaxDeclarationSearch extends TaxDeclaration
{
    protected $query;

    public $find_by;
    public $type;
    public $status;
    public $document_author_id;
    // OOO
    public $declaration_name_id;
    public $period_name_id;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period_name_id', 'declaration_name_id', 'status', 'tax_year', 'document_correction_number', 'declaration_maker_code', 'status_updated_at', 'status_author_id', 'document_author_id', 'find_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $A = static::tableName();
        $B = TaxDeclarationQuarter::tableName();

        $query = new Query;
        $query->select([
            "$A.id",
            "$A.document_correction_number",
            "$A.document_date",
            "$A.tax_year",
            "$A.tax_quarter",
            "$A.tax_month",
            "$A.declaration_maker_code",
            "$A.status_id",
            "$A.document_author_id",
            "$A.created_at",
            "$A.nds",
            "$A.org",
            "$A.balance",
            "$A.szvm",
            "$A.company_id",
            "($B.tax_rate / 100 * $B.income_amount) AS full_year_tax"],
            (int)$this->declaration_name_id . " AS `declaration_name_id`,",
            "concat(COALESCE($A.tax_year,'0'),'-',COALESCE($A.tax_quarter,'0'),'-',COALESCE($A.tax_month,'0')) AS `period_name_id`,"
            )
            ->from($A)
            ->leftJoin($B, "$A.id = $B.tax_declaration_id AND $B.quarter_number = 4")
            ->where(["$A.company_id" => Yii::$app->user->identity->company->id]);

        $query->andFilterWhere([
            'tax_declaration.status_id' => $this->status,
            'tax_declaration.document_author_id' => $this->document_author_id,
            'tax_declaration.tax_year' => $this->find_by
        ]);

        if ($this->declaration_name_id == DeclarationOsnoHelper::TYPE_SINGLE) {
            $query->andWhere([
                'single' => 1,
            ]);
        } elseif ($this->declaration_name_id == DeclarationOsnoHelper::TYPE_NDS) {
            $query->andWhere([
                'nds' => 1,
            ]);
        } elseif ($this->declaration_name_id == DeclarationOsnoHelper::TYPE_ORG) {
            $query->andWhere([
                'org' => 1,
            ]);
        } elseif ($this->declaration_name_id == DeclarationOsnoHelper::TYPE_BALANCE) {
            $query->andWhere([
                'balance' => 1,
            ]);
        } elseif ($this->declaration_name_id == DeclarationOsnoHelper::TYPE_SZVM) {
            $query->andWhere([
                'szvm' => 1,
            ]);
        }

        if ($this->period_name_id) {
            @list($year, $quarter, $month) = explode('-', $this->period_name_id);
            if (strlen($year) && strlen($quarter) && strlen($month)) {
                $query->andWhere(["$A.tax_year" => (int)$year]);
                iF ($quarter) {
                    $query->andWhere(["$A.tax_quarter" => (int)$quarter]);
                } else {
                    $query->andWhere(['is', "$A.tax_quarter", null]);
                }
                if ($month) {
                    $query->andWhere(["$A.tax_month" => (int)$month]);
                } else {
                    $query->andWhere(['is', "$A.tax_month", null]);
                }
            }

        }

        $countQuery = clone $query;
        $count = (int) $countQuery->select('*')->limit(-1)->offset(-1)->orderBy([])->count('*');

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'tax_year',
                    'document_date',
                    'created_at',
                    'document_correction_number'
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);
    }

    public function getCreators() {

        return ['' => 'Все'] + ArrayHelper::map(
                \common\models\employee\Employee::find()
                    ->innerJoin('tax_declaration', 'tax_declaration.document_author_id = employee.id')
                    ->where(['tax_declaration.company_id'=>Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true);
            });
    }

    public function getPeriodFilterNames() {
        $query = new Query;
        $periods = $query->select(["distinct(concat(COALESCE(tax_year,'0'),'-',COALESCE(tax_quarter,'0'),'-',COALESCE(tax_month,'0')))"])
            ->from(static::tableName())
            ->where(["company_id" => Yii::$app->user->identity->company->id])
            ->column();

        $names = ['' => 'Все'];
        foreach ($periods as $period) {
            @list($year, $quarter, $month) = explode('-', $period);
            if (strlen($year) && strlen($quarter) && strlen($month)) {
                if ($month == 0 && $quarter == 0)
                    $names[$period] = $year . ' г.';
                elseif ($month == 0)
                    $names[$period] = $quarter . '-й квартал ' . $year . ' г.';
                else
                    $names[$period] = ArrayHelper::getValue(Month::$monthFullRU, (int)$month).' '.$year.' г.';
            }
        }
        ksort($names);
        return $names;
    }
}
