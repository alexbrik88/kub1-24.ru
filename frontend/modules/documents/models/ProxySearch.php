<?php

namespace frontend\modules\documents\models;

use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Proxy;
use common\models\document\status\ProxyStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ProxySearch represents the model behind the search form about `common\models\Proxy`.
 */
class ProxySearch extends Proxy
{
    /**
     * @var
     */
    public $contractor_id;
    public $byNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'status_out_id', 'contractor_id', 'proxy_person_id'], 'integer'],
            [['document_date', 'byNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange Array
     *
     * @param $forExport boolean
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $query = Documents::getSearchQuery(Documents::DOCUMENT_PROXY, $this->type);

        $query->joinWith([
            'status',
            'files',
            'scanDocuments',
            'orderProxies.order',
            'invoice.contractor',
        ])->andWhere([
            Invoice::tableName() . '.is_deleted' => false,
        ])->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'has_file' => [
                        'asc' => ['file.owner_id'=> SORT_ASC],
                        'desc' => ['file.owner_id' => SORT_DESC],
                    ],
                    'invoice.total_amount_with_nds',
                    'invoice_document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'document_date' => $this->document_date,
            'id' => $this->id,
            Invoice::tableName() . '.contractor_id' => $this->contractor_id,
            'status_out_id' => $this->status_out_id,
            'invoice_id' => $this->invoice_id,
            'proxy_person_id' => $this->proxy_person_id
        ]);

        $query->andWhere(['between', Proxy::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param],
                ]);
            }
        }

        if ($forExport) {
            return $query->orderBy(['`' . self::tableName() .'`.`ordinal_document_number` * 1' => SORT_DESC,
                '`' . self::tableName() .'`.`document_additional_number` * 1' => SORT_DESC])->all();
        }

        return $dataProvider;
    }

    /**
     * @return mixed
     */
    public function getStatusArray($type)
    {
        $statusArray[null] = 'Все';
        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = Contractor::find()
            ->distinct()
            ->select(['s.id as id', 's.name as name'])
            ->leftJoin(['invoice' => Invoice::tableName()],
                'invoice.contractor_id = contractor.id')
            ->leftJoin(['proxy' => Proxy::tableName()],
                'proxy.invoice_id = invoice.id')
            ->leftJoin(['s' => ProxyStatus::tableName()],
                'proxy.status_out_id = s.id')
            ->andWhere(['OR',
                ['=', Contractor::tableName() . '.company_id', Yii::$app->user->identity->company->id],
                ['IS', Contractor::tableName() . '.company_id', null],
            ])
            ->andWhere([
                Contractor::tableName() . '.type' => $type,
                Contractor::tableName() . '.is_deleted' => false,
                Contractor::tableName() . '.status' => Contractor::ACTIVE,
            ])
            ->andWhere(['invoice.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['between', 'proxy.document_date', $dateRange['from'], $dateRange['to']])
            ->orderBy('name')
            ->all();
        $statusArray += ArrayHelper::map(
            $query,
            'id', 'name'
        );

        return $statusArray;
    }

    public function getProxyPersonsArray() {

        return ['' => 'Все'] + ArrayHelper::map(
                Employee::find()
                    ->innerJoin('proxy', 'proxy.proxy_person_id = employee.id')
                    ->where(['employee.company_id'=>Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true) ?: '(не задан)';
            });
    }
}
