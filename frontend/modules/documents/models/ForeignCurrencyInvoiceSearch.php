<?php

namespace frontend\modules\documents\models;

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\NdsViewType;
use common\models\document\query\InvoiceQuery;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * ForeignCurrencyInvoiceSearch represents the model behind the search form about `common\models\ForeignCurrencyInvoice`.
 */
class ForeignCurrencyInvoiceSearch extends ForeignCurrencyInvoice
{
    public $search;
    public $payDate;
    public $byNumber;
    public $employeeCompany;

    protected $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'search',
                    'invoice_status_id',
                    'document_author_id',
                    'responsible_employee_id',
                    'has_file',
                    'can_add_proxy',
                    'fileCount',
                    'byNumber',
                ], 'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @param string $sortParamTitle
     *
     * @param boolean $forExport
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange = null, $sortParamTitle = null, $forExport = false)
    {
        $this->period = $dateRange;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var InvoiceQuery $query */
        $paidStatuses = implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]);
        $query = self::find()->alias('invoice')
            ->addSelect([
                'invoice.*',
                'payDate' => new \yii\db\Expression("
                    IF({{invoice}}.[[invoice_status_id]] in ({$paidStatuses}), {{invoice}}.[[invoice_status_updated_at]], NULL)
                "),
            ])
            ->andWhere([
                'invoice.company_id' => $this->company_id,
                'invoice.type' => $this->type,
                'invoice.currency_name' => $this->currency_name,
                'invoice.is_deleted' => false,
            ])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']]);

        $query->joinWith([
            'invoiceStatus',
            'company',
            'contractor.companyType',
        ])->with([
            'author',
        ])->groupBy('invoice.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.responsible_employee_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'invoice.responsible_employee_id' => Yii::$app->user->id,
            ]);
        }

        if ($this->employeeCompany && $this->employeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['or',
                [Contractor::tableName() . '.not_accounting' => false],
                ['and',
                    [Contractor::tableName() . '.not_accounting' => true],
                    [Contractor::tableName() . '.responsible_employee_id' => $this->employeeCompany->employee_id],
                ],
                ['and',
                    [Contractor::tableName() . '.not_accounting' => true],
                    ['invoice.responsible_employee_id' => $this->employeeCompany->employee_id],
                ],
            ]);
        }
        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    [self::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => (!empty($sortParamTitle)) ? $sortParamTitle : 'sort',
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            'invoice.document_date' => SORT_ASC,
                            '{{invoice}}.[[document_number]] * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            'invoice.document_date' => SORT_DESC,
                            '{{invoice}}.[[document_number]] * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payDate' => [
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => ['{{invoice}}.[[document_number]] * 1' => SORT_ASC],
                        'desc' => ['{{invoice}}.[[document_number]] * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds',
                    'payment_limit_date',
                    'has_file',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->joinWith([
                'files file',
            ])->addSelect([
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        $query->andFilterWhere([
            'invoice.document_author_id' => $this->document_author_id,
            'invoice.responsible_employee_id' => $this->responsible_employee_id,
            'invoice.has_file' => $this->has_file,
        ]);

        if ($this->type == Documents::IO_TYPE_OUT
            && $this->invoice_status_id == InvoiceStatus::STATUS_OVERDUE
        ) {
            $query->andWhere(['AND',
                ['<', 'payment_limit_date', date(DateHelper::FORMAT_DATE)],
                ['not in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED]],
            ]);
        } elseif (!empty($this->invoice_status_id)) {
            $statusIds = is_array($this->invoice_status_id) ? $this->invoice_status_id : explode(',', $this->invoice_status_id);
            $query->andFilterWhere([
                'invoice_status_id' => array_filter($statusIds),
            ]);
            if (in_array(CashContractorType::BANK_TEXT, $statusIds)) {
                $query->joinWith('bankPayments bankPay', false)->andWhere(['not', ['bankPay.invoice_id' => null]]);
            }
        }

        if (!empty($this->document_number)) {
            $query->andWhere(['like', 'invoice.document_number', $this->document_number]);
        }

        if (empty($this->period)) {
            $this->period = StatisticPeriod::getDefaultPeriod();
        }

        if ($search = trim($this->search)) {
            $paramArray = explode(' ', $search);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', 'invoice.document_number', $param],
                    ['like', 'invoice.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.ITN', $param],
                    ['like', Contractor::tableName() . '.name', $param],
                ]);
            }
        }
        $query->andFilterWhere(['invoice.contractor_id' => $this->contractor_id,]);

        if ($forExport) {
            return $query->orderBy([
                'invoice.document_date' => SORT_DESC,
                '{{invoice}}.[[document_number]] * 1' => SORT_DESC])->all();
        }

        return $dataProvider;
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItems()
    {
        $query = Contractor::getSorted()
            ->innerJoin(['invoice' => self::tableName()], '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ])
            ->andWhere([
                'invoice.company_id' => Yii::$app->user->identity->company->id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
                'contractor.face_type' => Contractor::TYPE_FOREIGN_LEGAL_PERSON,
            ])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->groupBy('contractor.id')
            ->indexBy('id');

        return (['' => 'Все'] + $query->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItemsByQuery($searchQuery)
    {
        $ids = (clone $searchQuery)->select('invoice.contractor_id')->distinct()->column();
        $query = Contractor::getSorted()
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ])
            ->andWhere([
                'contractor.id' => $ids,
            ])
            ->indexBy('id');

        return (['' => 'Все'] + $query->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getStatusItemsByQuery(ActiveQuery $query)
    {
        $statusQuery = clone $query;
        $statusQuery->select([InvoiceStatus::tableName() . '.name', 'invoice_status_id'])
            ->distinct()
            ->orderBy([InvoiceStatus::tableName() . '.name' => SORT_ASC])
            ->indexBy('invoice_status_id');

        return (['' => 'Все'] + $statusQuery->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getAuthorItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select('invoice.document_author_id')->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    /**
     * @param $type
     * @return array
     */
    public function getResponsibleEmployeeByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select('invoice.responsible_employee_id')->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }
}
