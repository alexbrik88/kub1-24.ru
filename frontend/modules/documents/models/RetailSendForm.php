<?php

namespace frontend\modules\documents\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\Model;

/**
 * Форма отправки письма
 */
class RetailSendForm extends Model
{

    /** @var string E-mail от кого */
    public $from;
    /** @var string E-mail кому */
    public $to;
    /** @var string Тема письма */
    public $subject;
    /** @var string Текст письма */
    public $body;
    /** @var string Вложение (контент) */
    public $attachment = [];
    /** @var string E-mail для ответа */
    public $replyTo;

    public function __construct(array $config = null)
    {
        if ($config !== null && isset($config['replyTo']) === true && is_array($config['replyTo']) === true) {
            $config['replyTo'] = array_shift($config['replyTo']);
        }
        parent::__construct($config);
    }

    /** @inheritDoc */
    public function rules(): array
    {
        return [
            [['from', 'to', 'subject', 'body'], 'required'],
            [['from', 'replyTo'], 'email'],
            [
                ['to'],
                'filter',
                'filter' => function ($value) {
                    return is_string($value) ? explode(',', $value) : $value;
                },
            ],
            ['to', 'each', 'rule' => ['email']],
            [['subject', 'body'], 'string'],
        ];
    }

    /** @inheritDoc */
    public function attributeLabels(): array
    {
        return [
            'from' => 'От кого (e-mail)',
            'to' => 'Кому (e-mail)',
            'subject' => 'Тема',
            'body' => 'Текст письма',
        ];
    }

    /**
     * @return bool
     * @throws InvalidArgumentException
     */
    public function send(): bool
    {
        if ($this->validate() === false) {
            return false;
        }
        $mailer = Yii::$app->mailer->compose()
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($this->subject)
            ->setTextBody($this->body);
        if ($this->replyTo) {
            $mailer->setReplyTo($this->replyTo);
        }
        if (is_array($this->to) && count($this->to) > 0) {
            $mailer->setCc($this->to);
        }
        $mailer->attachContent($this->attachment, [
            'fileName' => 'report.pdf',
            'contentType' => 'application/pdf',
        ]);
        return $mailer->send();
    }

}
