<?php

namespace frontend\modules\documents\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\SalesInvoice;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\SalesInvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class SalesInvoiceSearch
 * @package frontend\modules\documents\models
 */
class SalesInvoiceSearch extends SalesInvoice
{
    /**
     * @var
     */
    public $contractor_id;
    public $byNumber;
    public $sales_invoice_source;
    public $sales_invoice_responsible;
    public $payment_date;

    public $payment_status;

    protected $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'status_out_id', 'contractor_id'], 'integer'],
            [['document_date', 'byNumber', 'fileCount'], 'safe'],
            [['sales_invoice_source', 'sales_invoice_responsible'], 'safe'],
            [['payment_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return parent::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param null $dateRange array
     * @param $forExport boolean
     *
     * @return ActiveDataProvider
     * @throws \yii\web\NotFoundHttpException
     */
    public function search($params, $dateRange = null, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $query = Documents::getSearchQuery(Documents::DOCUMENT_SALES_INVOICE, $this->type);

        $dateRange = StatisticPeriod::getSessionPeriod();

        $query
            ->addSelect([
                SalesInvoice::tableName() . '.*',
                Invoice::tableName() . '.total_amount_no_nds as order_sum_without_nds',
                Invoice::tableName() . '.total_amount_nds as order_sum_nds',
                'IF('.CashBankFlows::tableName() . '.date , ' . CashBankFlows::tableName() . '.date, ' . CashOrderFlows::tableName() . '.date) as sales_invoice_payment_date',
            ])
            ->joinWith([
                'status',
                'files',
                'scanDocuments',
                'orderSalesInvoices.order',
                'invoice.contractor',
                'invoice.cashBankFlows',
                'invoice.cashOrderFlows',
            ])
            ->andWhere([
                Invoice::tableName() . '.is_deleted' => false,
            ])
            ->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'has_file' => [
                        'asc' => ['file.owner_id'=> SORT_ASC],
                        'desc' => ['file.owner_id' => SORT_DESC],
                    ],
                    'invoice.total_amount_with_nds',
                    'order_sum_without_nds',
                    'order_sum_nds',
                    'invoice_document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'sales_invoice_payment_date',
                ],
                'defaultOrder' => [($this->type == 1 ? 'document_date' : 'document_number') => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if ($this->payment_status) {
            $this->filterByPaymentStatus($query, $this->payment_status);
        }

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->addSelect([
                self::tableName() . '.*',
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        $query->andFilterWhere([
            'document_date' => $this->document_date,
            'id' => $this->id,
            Invoice::tableName() . '.contractor_id' => $this->contractor_id,
            'status_out_id' => $this->status_out_id,
            self::tableName() . '.invoice_id' => $this->invoice_id,
        ]);

        $query->andWhere(['between', SalesInvoice::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        if ($this->sales_invoice_responsible) {
            $query->andFilterWhere([SalesInvoice::tableName().'.document_author_id' => $this->sales_invoice_responsible]);
        }

        if (isset($this->sales_invoice_source) && 1 == $this->sales_invoice_source) {
            $query->andFilterWhere(['IS NOT', SalesInvoice::tableName() . '.object_guid', new Expression('NULL')]);
        } else if (isset($this->sales_invoice_source) && '' !== $this->sales_invoice_source) {
            $query->andFilterWhere(['IS', SalesInvoice::tableName() . '.object_guid', new Expression('NULL')]);
        }


        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param],
                ]);
            }
        }

        if ($forExport) {
            return $query->orderBy(['`' . self::tableName() .'`.`ordinal_document_number` * 1' => SORT_DESC,
                '`' . self::tableName() .'`.`document_additional_number` * 1' => SORT_DESC])->all();
        }

        $this->query = $query;

        return $dataProvider;
    }

    public function getQuery()
    {
        return clone $this->query;
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function getStatusArray()
    {
        if ($this->query === null) {
            $this->search([]);
        }

        $query = clone $this->getQuery();
        $statusArray = $query
            ->innerJoin(['status' => 'sales_invoice_status'], "{{sales_invoice}}.[[status_out_id]] = {{status}}.[[id]]")
            ->select(['status.name', 'status.id'])
            ->distinct()
            ->indexBy('id')
            ->column();

        return ['' => 'Все'] + $statusArray;
    }


    /**
     * @return Employee[]
     */
    public function filterSalesInvoiceResponsible()
    {
        $employee = Employee::find()->andWhere(['company_id' => Yii::$app->user->identity->company->id])->all();
        return ['' => 'Все'] + ArrayHelper::map($employee, 'id', 'shortFio');
    }

    /**
     * @return string[]
     */
    public function filterSalesInvoiceSource()
    {
        return [
            '' => 'Все',
        ];
    }

    /**
     * @param ActiveQuery $query
     * @param string $stat_by_status
     */
    private function filterByPaymentStatus(ActiveQuery &$query, $stat_by_status)
    {
        switch ($stat_by_status) {
            case 'all':
            default:
                break;
            case 'not_payment':
                $query->andFilterWhere(['NOT IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED]]);
                break;
            case 'payment':
                $query->andFilterWhere(['IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]]);
                break;

        }
    }

}
