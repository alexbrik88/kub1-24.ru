<?php

namespace frontend\modules\documents\models;

use common\models\evotor\ReceiptItem;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * @property ActiveQuery $query
 */
class RetailDataProvider extends ActiveDataProvider
{

    public $companyId;

    public static function instance(int $companyId, int $dateFrom, int $dateTo, string $keyword = null): self
    {
        $query = ReceiptItem::find()->alias('ri')
            ->innerJoinWith([
                'receipt' => function (ActiveQuery $query) {
                    $query->alias('r');
                },
            ])->onCondition(['r.company_id' => $companyId])
            ->andWhere(['between', 'r.date_time', $dateFrom, $dateTo]);
        if ($keyword !== null) {
            $query->andWhere([
                'OR',
                ['like', 'article_number', $keyword],
                ['like', 'ri.name', $keyword],
            ]);
        }
        return new self([
            'companyId' => $companyId,
            'query' => $query,
            'sort' => [
                'sortParam' => 'sum_price',
                'attributes' => [
                    'sum_price',
                ],
                'defaultOrder' => 'sum_price',
            ],
        ]);
    }

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->key = function (ReceiptItem $model) {
            return $model->uuid;
        };
    }

    public function prepareForReport($productIDs = null)
    {
        $this->query->select([
            'ri.product_id',
            'ri.uuid',
            'ri.name',
            'quantity' => 'SUM(ri.quantity)',
            'ri.measure_name',
            'price' => 'ROUND(SUM(ri.price),2)',
            'sum_price' => 'ROUND(SUM(ri.sum_price),2)',
            'discount' => 'ROUND(SUM(ri.discount),2)',
            'tax' => 'ROUND(SUM(ri.tax),2)',
        ])->joinWith('product', true)->groupBy(['ri.uuid', 'ri.price']); //Если товар был продан по разной цене (без скидки), то вывести отдельные строки для каждой цены
        if ($productIDs !== null) {
            $this->query->andWhere(['ri.uuid' => $productIDs]);
        }
    }

    public function prepareForFlat()
    {
        $this->query->innerJoinWith([
            'receipt.store' => function (ActiveQuery $query) {
                $query->alias('s');
            },
            'receipt.device' => function (ActiveQuery $query) {
                $query->alias('d');
            },
        ])->select([
            'ri.receipt_id',
            'ri.name',
            'ri.quantity',
            'ri.measure_name',
            'ri.price',
            'ri.sum_price',
            'ri.tax_percent',
        ]);
    }

    /**
     * Возвращает суммарные значения
     *
     * @return array [count], [amount], [tax]
     */
    public function total()
    {
        $total = ReceiptItem::find()->alias('ri')->select(['product_id', 'receipt_id', 'sum_price' => 'SUM(sum_price)', 'tax' => 'SUM(ri.tax)'])
            ->joinWith('product', true)
            ->innerJoinWith([
                'receipt' => function (ActiveQuery $query) {
                    $query->alias('r');
                },
            ], false)->onCondition(['r.company_id' => $this->companyId])
            ->andWhere($this->query->where)->asArray(true)->one();
        $total = [
            'amount' => round($total['sum_price']),
            'tax' => round($total['tax']),
        ];
        return $total;
    }

}