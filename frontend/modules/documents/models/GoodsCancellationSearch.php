<?php

namespace frontend\modules\documents\models;

use Yii;
use common\models\Contractor;
use common\models\document\GoodsCancellation;
use common\models\document\GoodsCancellationStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class GoodsCancellationSearch extends GoodsCancellation
{
    /**
     * @var
     */
    public $company_id;
    public $contractor_id;
    public $status_id;
    public $byNumber;

    private $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id', 'responsible_employee_id', 'status_id', 'project_id', 'project_estimate_id', 'profit_loss_type', 'byNumber'], 'safe'],
        ];
    }


    public function search($params, $dateRange)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $this->load($params);

        $query = GoodsCancellation::find()
            ->alias('g')
            ->andWhere(['g.company_id' => $this->company_id])
            ->joinWith('contractor c');

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere(['g.responsible_employee_id' => Yii::$app->user->id]);
        }

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', 'g.document_number', $param],
                    ['like', 'g.document_additional_number', $param],
                    ['like', 'c.name', $param],
                    ['like', 'c.ITN', $param],
                ]);
            }
        }

        $query->andWhere(['between', 'g.document_date', $dateRange['from'], $dateRange['to']]);

        $query->andFilterWhere([
            'g.contractor_id' => $this->contractor_id,
            'g.status_id' => $this->status_id,
            'g.project_id' => $this->project_id,
            'g.project_estimate_id' => $this->project_estimate_id,
            'g.profit_loss_type' => $this->profit_loss_type
        ]);

        $this->_query = $query;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            'g.`document_date`' => SORT_DESC,
                            'g.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'view_total_with_nds',
                    'document_number' => [
                        'asc' => [
                            'g.`document_number` * 1' => SORT_ASC,
                            'g.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            'g.`document_number` * 1' => SORT_DESC,
                            'g.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => ['document_date_init' => SORT_DESC],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return mixed
     */
    public function getStatusArray()
    {
        return [
            '' => 'Все',
            GoodsCancellationStatus::STATUS_CREATED => 'Создан',
            GoodsCancellationStatus::STATUS_PRINTED => 'Распечатан',
            GoodsCancellationStatus::STATUS_SEND => 'Отправлен',
        ];
    }

    /**
     * @return mixed
     */
    public function getProfitAndLossTypeArray()
    {
        return [
            '' => 'Все',
            self::VARIABLE_EXPENSE_TYPE => 'Переменные расходы',
            self::CONSTANT_EXPENSE_TYPE => 'Постоянные расходы',
            self::OPERATING_EXPENSE_TYPE => 'Операционные расходы',
            self::OTHER_EXPENSE_TYPE => 'Другие расходы',
            self::UNSET_EXPENSE_TYPE => 'Не указан',
        ];
    }

    public function getContractorsArray()
    {
        $query = clone $this->_query;

        return ['' => 'Все'] + ArrayHelper::map(
            Contractor::find()
                ->alias('c')
                ->where(['c.id' => $query->select('g.contractor_id')->distinct()->column()])
                ->indexBy('id')
                ->all(), 'id', function ($model) {
            return $model->getShortName() ?: '(не задан)';
        });
    }

    public function getEmployersArray()
    {
        $query = clone $this->_query;

        return ['' => 'Все'] + ArrayHelper::map(
                Employee::find()
                    ->alias('e')
                    ->where(['e.id' => $query->select('g.responsible_employee_id')->distinct()->column()])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true) ?: '(не задан)';
            });
    }

    public function getProjectsArray()
    {
        $query = clone $this->_query;

        return ['' => 'Все'] + ArrayHelper::map(
                Project::find()
                    ->alias('p')
                    ->where(['p.id' => $query->select('project_id')->distinct()->column()])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getFullName() ?: '(не задан)';
            });
    }

    public function getProjectsEstimatesArray()
    {
        $query = clone $this->_query;

        return ['' => 'Все'] + ArrayHelper::map(
                ProjectEstimate::find()
                    ->alias('p')
                    ->where(['p.id' => $query->select('project_estimate_id')->distinct()->column()])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getFullName() ?: '(не задан)';
            });
    }
}
