<?php

namespace frontend\modules\documents\models;

use common\components\date\DateHelper;
use common\models\AgreementTemplate;
use common\models\AgreementType;
use common\models\employee\Employee;
use Yii;
use common\models\Agreement;
use common\models\Contractor;
use common\models\company\CompanyType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use yii\db\Query;

/**
 * AgreementSearch represents the model behind the search form about `common\models\Agreement`.
 */
class AgreementSearch extends Agreement
{
    /**
     * @var
     */
    protected $query;

    /**
     * @var
     */
    public $c_id;
    /**
     * @var
     */
    public $name;
    /**
     * @var
     */
    public $contractor;
    /**
     * @var array
     */
    public $contractorArray = [];

    public $find_by;
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['c_id', 'document_type_id', 'contractor_id', 'created_by'], 'integer'],
            [['contractor', 'find_by'], 'string'],
            [['type', 'has_file', 'agreement_template_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params, $dateRange = null)
    {
        $this->load($params);

        $company_id = Yii::$app->user->identity->company->id;

        $A = static::tableName();
        $C = Contractor::tableName();
        $T = CompanyType::tableName();
        $AT = AgreementType::tableName();

        $query = new Query;
        $query->select([
            "$A.id",
            "$A.document_number",
            "$A.document_additional_number",
            "$A.document_date",
            "$A.document_author_id",
            "$A.type",
            "$A.agreement_template_id AS tid",
            "$A.created_by",
            "$A.company_id",
            "$AT.name as agreementType",
            "$A.document_name",
            "$C.id AS cid",
            "$C.name",
            "$T.name_short",
            "$A.has_file"
        ])->from($A)
            ->leftJoin($C, "{{%$A}}.[[contractor_id]] = {{%$C}}.[[id]]")
            ->leftJoin($T, "{{%$C}}.[[company_type_id]] = {{%$T}}.[[id]]")
            ->leftJoin($AT, "{{%$AT}}.[[id]] = {{%$A}}.[[document_type_id]]")
            ->where(["$A.company_id" => $company_id])
            ->andWhere(["$A.is_created" => 1])
            ->andFilterWhere(["$AT.id" => $this->document_type_id])
            ->andFilterWhere(["$A.contractor_id" => $this->contractor_id])
            ->andFilterWhere(["$A.created_by" => $this->created_by]);

        if ($this->agreement_template_id == 'no_tpl')
            $query->andFilterWhere(["AND", "$A.agreement_template_id IS NULL"]);
        elseif ($this->agreement_template_id > 0)
            $query->andFilterWhere(["$A.agreement_template_id" => $this->agreement_template_id]);

        $query->andFilterWhere([
            'or',
            ["$A.document_number" => $this->find_by],
            ['like', "$A.document_name", $this->find_by]
        ]);

        $query->andFilterWhere([
            "$A.type" => $this->type
        ]);

        $query->andFilterWhere([
            "$A.has_file" => $this->has_file
        ]);

        if ($dateRange) {
            $query->andWhere(['between', Agreement::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);
        }

        \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query, $A, $C);

        $countQuery = clone $query;
        $count = (int)$countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            self::tableName() . '.`document_date`' => SORT_ASC,
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_number',
                    'document_name',
                    'has_file',
                    'name' => [
                        'asc' => [
                            new \yii\db\Expression("ISNULL($T.name_short)"),
                            "$T.name_short" => SORT_ASC,
                            "$C.name" => SORT_ASC,
                        ],
                        'desc' => [
                            new \yii\db\Expression("IF(ISNULL($T.name_short), 0, 1)"),
                            "$T.name_short" => SORT_DESC,
                            "$C.name" => SORT_DESC,
                        ],
                    ]
                ],
                'defaultOrder' => [
                    'document_date' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @return array
     */
    public function getContractorFilterItems()
    {
        return ['' => 'Все'] + Contractor::getSorted()
            ->select([
                'contractor_name' => 'CONCAT_WS(" ", {{company_type}}.[[name_short]], {{contractor}}.[[name]])',
                'contractor.id',
            ])
            ->byIsDeleted(false)
            ->andFilterWhere(['contractor.type' => $this->type])
            ->andWhere(['or',
                [Contractor::tableName() . '.company_id' => Yii::$app->user->identity->company->id],
                [Contractor::tableName() . '.company_id' => null]
            ])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return array
     */
    public function getAgreementTypeFilterItems()
    {
        $setContractor = $this->contractor_id ? ['agreement.contractor_id' => $this->contractor_id] : [];

        return ['' => 'Все'] + ArrayHelper::map(
            AgreementType::find()
                ->innerJoin('agreement', 'agreement.document_type_id = agreement_type.id')
                ->where(['agreement.company_id'=>Yii::$app->user->identity->company->id] + $setContractor)
                ->indexBy('id')
                ->all(), 'id', 'name');
    }

    public function getAgreementEmployees() {

        $setContractor = $this->contractor_id ? ['agreement.contractor_id' => $this->contractor_id] : [];

        return ['' => 'Все'] + ArrayHelper::map(
            Employee::find()
                ->innerJoin('agreement', 'agreement.created_by = employee.id')
                ->where(['agreement.company_id'=>Yii::$app->user->identity->company->id] + $setContractor)
                ->indexBy('id')
                ->all(), 'id', function ($model) {
            return $model->getShortFio(true);
        });
    }

    public function getAgreementTemplates() {

        $setContractor = $this->contractor_id ? ['contractor_id' => $this->contractor_id] : [];

        $hasSimpleAgreements = Agreement::find()->where([
                'company_id' => Yii::$app->user->identity->company->id,
                'is_created' => true,
                'is_completed' => false,
            ] + $setContractor)
            ->andWhere(['AND', "agreement_template_id IS NULL"])->count();

        $retArray  = ['' => 'Все'];
        if ($hasSimpleAgreements)
            $retArray += ['no_tpl' => '---'];

        return $retArray + ArrayHelper::map(
            AgreementTemplate::find()
                ->where(['agreement.company_id' => Yii::$app->user->identity->company->id] + $setContractor)
                ->leftJoin('agreement', 'agreement.agreement_template_id = agreement_template.id')
                ->indexBy('id')
                ->all(), 'id', function ($model) {
                return '№ ' . $model->document_number . ' от ' . DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            });
    }
}
