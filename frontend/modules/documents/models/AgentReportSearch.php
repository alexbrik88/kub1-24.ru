<?php

namespace frontend\modules\documents\models;

use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\AgentReport;
use common\models\document\status\AgentReportStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * AgentReportSearch represents the model behind the search form about `common\models\AgentReport`.
 */
class AgentReportSearch extends AgentReport
{
    /**
     * @var
     */
    public $contractor_id;
    public $byNumber;
    public $agent_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'status_out_id', 'contractor_id', 'document_author_id', 'agent_id', 'total_sum'], 'integer'],
            [['document_date', 'byNumber', 'has_invoice'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange Array
     *
     * @param $forExport boolean
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $company = $user->company;
        $query = AgentReport::find()
            ->andWhere(['agent_report.company_id' => $company->id])
            ->andWhere(['agent_report.agent_id' => $this->agent_id])
            ->andWhere(['agent_report.type' => $this->type]);
        $query->joinWith([
            'statusOut',
            //'files',
            //'scanDocuments',
            //'agentReportOrders',
            'agent AS agent',
        ])->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['agent_report.document_author_id' => Yii::$app->user->id],
                ['agent.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'total_sum',
                    'document_date',
                    'pay_up_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'invoice_id'
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        $query->andWhere(['between', AgentReport::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere(['like', self::tableName() . '.document_number', $param]);
            }
        }

        $query->andFilterWhere(['has_invoice' => $this->has_invoice]);

        return $dataProvider;
    }

    /**
     * @param $type
     * @return array
     */
    public function getStatusArray()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = (new Query())
            ->select(['s.id as id', 's.name as name'])
            ->distinct()
            ->from(['a' => AgentReport::tableName()])
            ->leftJoin(['s' => AgentReportStatus::tableName()],
                'a.status_out_id = s.id')
            ->andWhere([
                'a.agent_id' => $this->agent_id
            ])
            ->andWhere(['between', 'a.document_date', $dateRange['from'], $dateRange['to']])
            ->orderBy('s.name')
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($query, 'id', 'name');
    }

    public function getResponsibleEmployees() {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $query = (new Query())
            ->select(['s.id as id', 's.lastname'])
            ->distinct()
            ->from(['a' => AgentReport::tableName()])
            ->leftJoin(['s' => Employee::tableName()],
                'a.document_author_id = s.id')
            ->andWhere([
                'a.agent_id' => $this->agent_id
            ])
            ->andWhere(['between', 'a.document_date', $dateRange['from'], $dateRange['to']])
            ->orderBy('s.lastname')
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($query, 'id', function($data) { $e = Employee::findOne(['id' => $data['id']]); return ($e) ? $e->getShortFio() : "(не задан)"; });
    }
}
