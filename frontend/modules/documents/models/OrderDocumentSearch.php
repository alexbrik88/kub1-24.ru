<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2018
 * Time: 19:38
 */

namespace frontend\modules\documents\models;


use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentInToOut;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use common\models\document\Upd;

/**
 * Class OrderDocumentSearch
 * @package frontend\modules\documents\models
 */
class OrderDocumentSearch extends OrderDocument
{
    const DOCUMENTS_ALL = 0;
    const DOCUMENTS_NOT_EXISTS = 1;
    const DOCUMENTS_EXISTS = 2;

    const DETAILING_BY_UNSET = 0;
    const DETAILING_BY_NOMENCLATURE = 1;

    public static $detailingBy = [
        self::DETAILING_BY_UNSET => 'Без детализации',
        self::DETAILING_BY_NOMENCLATURE => 'По номенклатуре',
    ];

    public $supplier_id;
    public $supply_document_number;

    public $invoiceState;
    public $actState;
    public $updState;
    public $packingListState;
    public $invoiceFactureState;

    /**
     * @var
     */
    public $byNumber;

    /**
     * @var
     */
    public $reserve;

    /**
     * @var
     */
    public $shipped;

    /**
     * @var
     */
    public $type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['invoiceState', 'actState', 'updState', 'packingListState', 'invoiceFactureState'], 'in', 'range' => [
                self::DOCUMENTS_ALL,
                self::DOCUMENTS_NOT_EXISTS,
                self::DOCUMENTS_EXISTS,
            ],],
            [['contractor_id', 'status_id', 'author_id', 'store_id', 'supplier_id'], 'integer'],
            [['byNumber', 'reserve', 'shipped', 'type'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param bool|false $forExport
     * @return ActiveDataProvider
     */
    public function search($params, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $query = $this->getBaseQuery();

        $query
            ->select([
                self::tableName() . '.*',
                'IFNULL(SUM(`bankFlow`.`amount`), 0) + IFNULL(SUM(`orderFlow`.`amount`), 0) + IFNULL(SUM(`emoneyFlow`.`amount`), 0) AS paymentSum',
                'SUM(' . OrderDocumentProduct::tableName() . '.quantity) * 1 - SUM(orderPackingLists.quantity) * 1 as reserve',
                'SUM(orderPackingLists.quantity) * 1 as shipped',
            ])
            ->joinWith([
                'contractor',
                'invoice',
                'orderDocumentProducts',
                'invoice.acts',
                'invoice.upds',
                'invoice.packingLists',
                'invoice.invoiceFactures',
            ])
            ->leftJoin(['bankFlow' => CashBankFlowToInvoice::tableName()], 'bankFlow.invoice_id = invoice.id')
            ->leftJoin(['orderFlow' => CashOrderFlowToInvoice::tableName()], 'orderFlow.invoice_id = invoice.id')
            ->leftJoin(['emoneyFlow' => CashEmoneyFlowToInvoice::tableName()], 'emoneyFlow.invoice_id = invoice.id')
            ->leftJoin(['packingLists' => PackingList::tableName()], 'packingLists.invoice_id = ' . Invoice::tableName() . '.id')
            ->leftJoin(['orderPackingLists' => OrderPackingList::tableName()], 'orderPackingLists.packing_list_id = packingLists.id')
            ->groupBy(OrderDocument::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                [self::tableName() . '.document_author_id' => Yii::$app->user->id],
                [Contractor::tableName() . '.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'paymentSum',
                    'document_date',
                    'total_amount',
                    'ship_up_to_date',
                    'reserve',
                    'shipped',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        $query->andWhere([OrderDocument::tableName().'.type' => $this->type]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        // filter default values
        foreach (['invoiceState', 'actState', 'updState', 'packingListState', 'invoiceFactureState'] as $attr)
            if (strlen($this->{$attr}) && $this->{$attr} == self::DOCUMENTS_ALL)
                $this->{$attr} = null;

        if ($this->invoiceState != self::DOCUMENTS_ALL) {
            if ($this->invoiceState == self::DOCUMENTS_NOT_EXISTS) {
                $query->andWhere(Invoice::tableName() . '.id IS NULL');
            } elseif ($this->invoiceState == self::DOCUMENTS_EXISTS) {
                $query->andWhere(Invoice::tableName() . '.id IS NOT NULL');
            }
        }

        if ($this->actState != self::DOCUMENTS_ALL) {
            if ($this->actState == self::DOCUMENTS_NOT_EXISTS) {
                $query->andWhere(Act::tableName() . '.id IS NULL');
            } elseif ($this->actState == self::DOCUMENTS_EXISTS) {
                $query->andWhere(Act::tableName() . '.id IS NOT NULL');
            }
        }

        if ($this->updState != self::DOCUMENTS_ALL) {
            if ($this->updState == self::DOCUMENTS_NOT_EXISTS) {
                $query->andWhere(Upd::tableName() . '.id IS NULL');
            } elseif ($this->updState == self::DOCUMENTS_EXISTS) {
                $query->andWhere(Upd::tableName() . '.id IS NOT NULL');
            }
        }

        if ($this->packingListState != self::DOCUMENTS_ALL) {
            if ($this->packingListState == self::DOCUMENTS_NOT_EXISTS) {
                $query->andWhere(PackingList::tableName() . '.id IS NULL');
            } elseif ($this->packingListState == self::DOCUMENTS_EXISTS) {
                $query->andWhere(PackingList::tableName() . '.id IS NOT NULL');
            }
        }

        if ($this->invoiceFactureState != self::DOCUMENTS_ALL) {
            if ($this->invoiceFactureState == self::DOCUMENTS_NOT_EXISTS) {
                $query->andWhere(InvoiceFacture::tableName() . '.id IS NULL');
            } elseif ($this->invoiceFactureState == self::DOCUMENTS_EXISTS) {
                $query->andWhere(InvoiceFacture::tableName() . '.id IS NOT NULL');
            }
        }

        if ($this->supplier_id) {
            $query
                ->leftJoin(['in2out' => OrderDocumentInToOut::tableName()], 'order_document.id = in2out.out_order_document')
                ->leftJoin(['in_order_document' => OrderDocument::tableName()], 'in2out.in_order_document = in_order_document.id')
                ->andWhere(['in_order_document.contractor_id' => $this->supplier_id]);
        }

        $query->andFilterWhere([self::tableName() . '.contractor_id' => $this->contractor_id]);

        $query->andFilterWhere([self::tableName() . '.status_id' => $this->status_id]);

        $query->andFilterWhere([self::tableName() . '.author_id' => $this->author_id]);

        $query->andFilterWhere([self::tableName() . '.store_id' => $this->store_id]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->joinWith('status')
            ->groupBy('status_id')
            ->all(), 'status_id', 'status.name'));
    }

    /**
     * @return array
     */
    public function getAuthorFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->joinWith('author')
            ->groupBy('author_id')
            ->all(), 'author_id', 'author.shortFio'));
    }

    /**
     * @return array
     */
    public function getStoreFilter() {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->andWhere(['not', ['store_id' => null]])
            ->joinWith('store')
            ->groupBy('store_id')
            ->all(), 'store_id', 'store.name'));
    }

    /**
     * @return array
     */
    public function getInvoiceFilter()
    {
        return [
            self::DOCUMENTS_ALL => 'Все',
            self::DOCUMENTS_EXISTS => 'Есть счет',
            self::DOCUMENTS_NOT_EXISTS => 'Нет счета',
        ];
    }

    /**
     * @return array
     */
    public function getUpdFilter()
    {
        return [
            self::DOCUMENTS_ALL => 'Все',
            self::DOCUMENTS_EXISTS => 'Есть УПД',
            self::DOCUMENTS_NOT_EXISTS => 'Нет УПД',
        ];
    }

    /**
     * @return array
     */
    public function getActFilter()
    {
        return [
            self::DOCUMENTS_ALL => 'Все',
            self::DOCUMENTS_EXISTS => 'Есть акт',
            self::DOCUMENTS_NOT_EXISTS => 'Нет акта',
        ];
    }

    /**
     * @return array
     */
    public function getPackingListFilter()
    {
        return [
            self::DOCUMENTS_ALL => 'Все',
            self::DOCUMENTS_EXISTS => 'Есть ТН',
            self::DOCUMENTS_NOT_EXISTS => 'Нет ТН',
        ];
    }

    /**
     * @return array
     */
    public function getInvoiceFactureFilter()
    {
        return [
            self::DOCUMENTS_ALL => 'Все',
            self::DOCUMENTS_EXISTS => 'Есть СФ',
            self::DOCUMENTS_NOT_EXISTS => 'Нет СФ',
        ];
    }

    public function getInToOutSupplierList()
    {
        $company = Yii::$app->user->identity->company;
        $dateRange = StatisticPeriod::getSessionPeriod();

        $query = Contractor::getSorted()
            ->distinct()
            ->leftJoin(['o' => OrderDocument::tableName()], 'o.contractor_id = contractor.id')
            ->innerJoin(['i2o' => OrderDocumentInToOut::tableName()], 'o.id = i2o.in_order_document')
            ->where(['contractor.company_id' => $company->id])
            ->andWhere(['between', 'o.document_date', $dateRange['from'], $dateRange['to']]);

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($query->all(), 'id', 'shortName'));
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $dateRange = StatisticPeriod::getSessionPeriod();

        return OrderDocument::find()->andWhere(['and',
            [self::tableName() . '.company_id' => $company->id],
            [self::tableName() . '.is_deleted' => false],
            ['between', self::tableName() . '.document_date', $dateRange['from'], $dateRange['to']],
        ]);
    }
}