<?php

namespace frontend\modules\documents\models;

use common\components\date\DateHelper;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceIncomeItem;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\document\NdsViewType;
use common\models\document\query\InvoiceQuery;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\Product;
use common\models\project\Project;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class InvoiceSearch extends Invoice
{
    const INVOICE_FACTURE_ALL = 0;
    const INVOICE_FACTURE_NOT_EXISTS = 1;
    const INVOICE_FACTURE_EXISTS = 2;

    const ACT_OR_PACKING_LIST_ALL = 0;
    const ACT_OR_PACKING_LIST_NOT_EXISTS = 1;
    const ACT_OR_PACKING_LIST_EXISTS = 2;


    const SORT_PARAM_NAME_IN = 'in_sort';
    const SORT_PARAM_NAME_OUT = 'out_sort';

    const CONTRACTOR_DUPLICATE = 'duplicate';

    const WITHOUT_DOCUMENT = 'without';

    public $invoceFactureState;
    public $actOrPackingListState;

    public $act;
    public $packingList;
    public $waybill;
    public $upd;
    public $byNumber;
    public $search;
    public $payDate;
    public $withProductsOnly = false;
    public $excludeIds = [];

    protected $period;

    public $float_payment_limit_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoceFactureState'], 'in', 'range' => [
                self::INVOICE_FACTURE_ALL,
                self::INVOICE_FACTURE_NOT_EXISTS,
                self::INVOICE_FACTURE_EXISTS,
            ],],
            [['act', 'packingList', 'waybill', 'upd'], 'in', 'range' => [
                self::ACT_OR_PACKING_LIST_ALL,
                self::ACT_OR_PACKING_LIST_NOT_EXISTS,
                self::ACT_OR_PACKING_LIST_EXISTS,
            ],],
            [['document_date', 'document_number', 'contractor_id'], 'integer'],
            [['total_amount_with_nds'], 'number'],
            [['withProductsOnly'], 'boolean'],
            [
                [
                    'search',
                    'byNumber',
                    'invoice_status_id',
                    'invoice_expenditure_item_id',
                    'invoice_income_item_id',
                    'document_author_id',
                    'responsible_employee_id',
                    'has_act',
                    'has_invoice_facture',
                    'has_packing_list',
                    'has_waybill',
                    'has_upd',
                    'has_file',
                    'can_add_act',
                    'can_add_invoice_facture',
                    'can_add_upd',
                    'can_add_packing_list',
                    'can_add_waybill',
                    'can_add_proxy',
                    'fileCount',
                    'responsible_employee_id',
                    'industry_id',
                    'sale_point_id',
                    'project_id'
                ], 'safe'
            ],
        ];
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + ['float_payment_limit_date' => 'Дата оплаты'];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @param string $sortParamTitle
     *
     * @param boolean $forExport
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange = null, $sortParamTitle = null, $forExport = false)
    {
        $this->period = $dateRange;
        $paidStatuses = implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]);

        /* @var InvoiceQuery $query */
        $query = self::find()
            ->addSelect([
                'invoice.*',
                new Expression('IF(invoice.payment_limit_rule = 1 OR invoice.related_document_payment_limit_date IS NULL, invoice.payment_limit_date, invoice.related_document_payment_limit_date) AS float_payment_limit_date'),
                'payDate' => new \yii\db\Expression("
                    IF({{invoice}}.[[invoice_status_id]] in ({$paidStatuses}), {{invoice}}.[[invoice_status_updated_at]], NULL)
                "),
            ])
            ->andWhere([
                'invoice.company_id' => $this->company_id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
                'invoice.from_demo_out_invoice' => false
            ]);

        $query->joinWith([
            'invoiceStatus',
            'company',
            'contractor.companyType',
        ])->with([
            'author',
            'acts',
            'packingLists',
            'waybills',
            'invoiceFactures',
            'upds',
            'industry',
            'project',
            'salePoint',
        ])->groupBy('invoice.id');

        self::searchByRoleFilter($query);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => (!empty($sortParamTitle)) ? $sortParamTitle : 'sort',
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            self::tableName() . '.`document_date`' => SORT_ASC,
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payDate' => [
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => [self::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [self::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds',
                    'float_payment_limit_date',
                    'has_file',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->joinWith([
                'files file',
            ])->addSelect([
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        self::searchByDocFilter($this, $query);

        $query->andFilterWhere([
            'invoice.document_date' => $this->document_date,
            'invoice.total_amount_with_nds' => $this->total_amount_with_nds,
            'invoice.document_author_id' => $this->document_author_id,
            'invoice.responsible_employee_id' => $this->responsible_employee_id,
            'invoice.has_file' => $this->has_file,
            'invoice.can_add_act' => $this->can_add_act,
            'invoice.can_add_invoice_facture' => $this->can_add_invoice_facture,
            'invoice.can_add_upd' => $this->can_add_upd,
            'invoice.can_add_packing_list' => $this->can_add_packing_list,
            'invoice.can_add_proxy' => $this->can_add_proxy,
            'invoice.can_add_waybill' => $this->can_add_waybill,
            'invoice.invoice_income_item_id' => $this->invoice_income_item_id,
            'invoice.invoice_expenditure_item_id' => $this->invoice_expenditure_item_id,
        ]);

        if ($this->type == Documents::IO_TYPE_OUT
            && $this->invoice_status_id == InvoiceStatus::STATUS_OVERDUE
        ) {
            $query->andWhere(['AND',
                ['<', 'IF(invoice.related_document_payment_limit_date IS NULL, invoice.payment_limit_date, invoice.related_document_payment_limit_date)', date(DateHelper::FORMAT_DATE)],
                ['not in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_REJECTED]],
            ]);
        } elseif (!empty($this->invoice_status_id)) {
            $statusIds = is_array($this->invoice_status_id) ? $this->invoice_status_id : explode(',', $this->invoice_status_id);
            $query->andFilterWhere([
                'invoice_status_id' => array_filter($statusIds),
            ]);
            if (in_array(CashContractorType::BANK_TEXT, $statusIds)) {
                $query->joinWith('bankPayments bankPay', false)->andWhere(['not', ['bankPay.invoice_id' => null]]);
            }
            if (in_array(CashContractorType::ORDER_TEXT, $statusIds)) {
                $query->joinWith('orderPayments orderPay', false)->andWhere(['not', ['orderPay.invoice_id' => null]]);
            }
            if (in_array(CashContractorType::EMONEY_TEXT, $statusIds)) {
                $query->joinWith('emoneyPayments emoneyPay', false)->andWhere(['not', ['emoneyPay.invoice_id' => null]]);
            }
        }

        if (!empty($this->document_number)) {
            $query->andWhere(['like', Invoice::tableName() . '.document_number', $this->document_number]);
        }

        if (empty($this->period)) {
            $this->period = StatisticPeriod::getDefaultPeriod();
        }

        $query->andWhere(['between', Invoice::tableName() . '.document_date', $this->period['from'], $this->period['to']]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',
                    [self::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        if ($search = trim($this->search)) {
            $nameFilter = ['and'];
            foreach (explode(' ', $search) as $param) {
                $nameFilter[] = ['like', Contractor::tableName() . '.name', $param];
            }

            $query->andFilterWhere([
                'or',
                ['like', 'CONCAT({{invoice}}.[[document_number]], {{invoice}}.[[document_additional_number]])', $search],
                count($nameFilter) == 2 ? $nameFilter[1] : $nameFilter,
            ]);
        }
        $query->andFilterWhere(['invoice.contractor_id' => $this->contractor_id]);
        $query->andFilterWhere(['invoice.responsible_employee_id' => $this->responsible_employee_id]);

        if (strlen($this->project_id)) {
            $query->andWhere(['invoice.project_id' => $this->project_id ?: null]);
        }
        if (strlen($this->industry_id)) {
            $query->andWhere(['invoice.industry_id' => $this->industry_id ?: null]);
        }
        if (strlen($this->sale_point_id)) {
            $query->andWhere(['invoice.sale_point_id' => $this->sale_point_id ?: null]);
        }

        if ($this->withProductsOnly) {
            $query->andFilterWhere([
                'or',
                ['can_add_packing_list' => 1],
                ['can_add_waybill' => 1],
                ['has_packing_list' => 1],
            ]);
        }

        if ($this->excludeIds) {
            $query->andWhere(['not', ['invoice.id' => $this->excludeIds]]);
        }

        if ($forExport) {
            return $query->orderBy(['`' . self::tableName() . '`.`document_date`' => SORT_DESC,
                '`' . self::tableName() . '`.`document_number` * 1' => SORT_DESC])->all();
        }

        return $dataProvider;
    }

    /**
     * using:
     * \frontend\modules\documents\models\InvoiceSearch::searchByRoleFilter($query);
     *
     * @param $query
     */
    public static function searchByRoleFilter($query, $invoiceAlias = 'invoice', $contractorAlias = 'contractor')
    {
        $employee = Yii::$app->user->identity;
        $employeeCompany = $employee->currentEmployeeCompany;
        $role = $employeeCompany->employee_role_id;
        if (!in_array($role, EmployeeRole::$roleViewAll)) {
            if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
                $query->andWhere([
                    'or',
                    [$invoiceAlias.'.document_author_id' => Yii::$app->user->id],
                    [$contractorAlias.'.responsible_employee_id' => Yii::$app->user->id],
                ]);
            }
            if ($employeeCompany->document_access_own_only) {
                $query->andWhere([
                    $invoiceAlias.'.document_author_id' => Yii::$app->user->id,
                ]);
            }
            if ($role == EmployeeRole::ROLE_ACCOUNTANT) {
                $query->andWhere(['or',
                    [$contractorAlias.'.not_accounting' => false],
                    ['and',
                        [$contractorAlias.'.not_accounting' => true],
                        ['or',
                            [$contractorAlias.'.responsible_employee_id' => $employee->id],
                            [$invoiceAlias.'.document_author_id' => $employee->id],
                        ],
                    ],
                ]);
            }
        }
    }

    /**
     * @param $type
     * @return array
     */
    public static function searchByDocFilter(self $model, $query)
    {
        if (($hasDoc = (string) $model->has_act) !== '') {
            $query->andWhere([
                'invoice.has_services' => true,
                'invoice.has_upd' => false,
            ]);
            if ($hasDoc == self::WITHOUT_DOCUMENT) {
                $query->andWhere(['invoice.need_act' => false]);
            } else {
                $query->andWhere([
                    'invoice.need_act' => true,
                    'invoice.has_act' => $hasDoc,
                ]);
            }
        }
        if (($hasDoc = (string) $model->has_packing_list) !== '') {
            $query->andWhere([
                'invoice.has_goods' => true,
                'invoice.has_upd' => false,
            ]);
            if ($hasDoc == self::WITHOUT_DOCUMENT) {
                $query->andWhere(['invoice.need_packing_list' => false]);
            } else {
                $query->andWhere([
                    'invoice.need_packing_list' => true,
                    'invoice.has_packing_list' => $hasDoc,
                ]);
            }
        }
        if (($hasDoc = (string) $model->has_invoice_facture) !== '') {
            $query->andWhere([
                'invoice.has_upd' => false,
                'invoice.has_invoice_facture' => $hasDoc,
            ])->andWhere(['<>', 'invoice.nds_view_type_id', NdsViewType::NDS_VIEW_WITHOUT]);
        }
        if (($hasDoc = (string) $model->has_upd) !== '') {
            $query->andWhere([
                'invoice.has_act' => false,
                'invoice.has_packing_list' => false,
                'invoice.has_invoice_facture' => false,
            ]);
            if ($hasDoc == self::WITHOUT_DOCUMENT) {
                $query->andWhere(['invoice.need_upd' => false]);
            } else {
                $query->andWhere([
                    'invoice.need_upd' => true,
                    'invoice.has_upd' => $hasDoc,
                ]);
            }
        }
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItems()
    {
        $query = Contractor::getSorted()
            ->innerJoin(Invoice::tableName(), '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ])
            ->andWhere([
                'invoice.company_id' => Yii::$app->user->identity->company->id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
            ])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->groupBy('contractor.id')
            ->indexBy('id');

        return (['' => 'Все'] + [self::CONTRACTOR_DUPLICATE => 'Показать дубли'] + $query->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItemsByQuery($searchQuery)
    {
        $ids = (clone $searchQuery)->select('invoice.contractor_id')->distinct()->column();
        $query = Contractor::getSorted()
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ])
            ->andWhere([
                'contractor.id' => $ids,
            ])
            ->indexBy('id');

        return (['' => 'Все'] + $query->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getStatusItemsByQuery(ActiveQuery $query)
    {
        $statusQuery = clone $query;
        $statusQuery->select([InvoiceStatus::tableName() . '.name', 'invoice_status_id'])
            ->distinct()
            ->orderBy([InvoiceStatus::tableName() . '.name' => SORT_ASC])
            ->indexBy('invoice_status_id');

        return (['' => 'Все'] + $statusQuery->column());
    }

    /**
     * @param $type
     * @return array
     */
    public function getAuthorItemsByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select('invoice.document_author_id')->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    /**
     * @param $type
     * @return array
     */
    public function getResponsibleEmployeeByQuery(ActiveQuery $query)
    {
        $searchQuery = clone $query;
        $dataArray = $this->company
            ->getEmployeeCompanies()
            ->andWhere([
                'employee_id' => $searchQuery->select('invoice.responsible_employee_id')->distinct()->column(),
            ])
            ->orderBy(['lastname' => SORT_ASC, 'firstname_initial' => SORT_ASC, 'patronymic_initial' => SORT_ASC])
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($dataArray, 'employee_id', function ($model) {
                return $model->getFio(true);
            });
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    public function getHasActFilter(ActiveQuery $query)
    {
        $filter = [
            '' => 'Все',
            '1' => 'Есть Акт',
            '0' => 'Нет Акта',
        ];
        $query = self::find()->andWhere([
            'company_id' => $this->company_id,
            'type' => $this->type,
            'is_deleted' => false,
            'from_demo_out_invoice' => false
        ])->andWhere([
            'between',
            'document_date',
            $this->period['from'],
            $this->period['to'],
        ]);
        if ($query->andWhere(['need_act' => false])->exists()) {
            $filter[self::WITHOUT_DOCUMENT] = 'Без Акта';
        }

        return $filter;
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    public function getHasPackingListFilter(ActiveQuery $query)
    {
        $filter = [
            null => 'Все',
            '1' => 'Есть ТН',
            '0' => 'Нет ТН',
        ];
        $query = self::find()->andWhere([
            'company_id' => $this->company_id,
            'type' => $this->type,
            'is_deleted' => false,
            'from_demo_out_invoice' => false
        ])->andWhere([
            'between',
            'document_date',
            $this->period['from'],
            $this->period['to'],
        ]);
        if ($query->andWhere(['need_packing_list' => false])->exists()) {
            $filter[self::WITHOUT_DOCUMENT] = 'Без ТН';
        }

        return $filter;
    }

    /**
     * @param ActiveQuery $query
     * @return array
     */
    public function getHasUpdFilter(ActiveQuery $query)
    {
        $filter = [
            null => 'Все',
            '1' => 'Есть УПД',
            '0' => 'Нет УПД',
        ];
        $query = self::find()->andWhere([
            'company_id' => $this->company_id,
            'type' => $this->type,
            'is_deleted' => false,
            'from_demo_out_invoice' => false
        ])->andWhere([
            'between',
            'document_date',
            $this->period['from'],
            $this->period['to'],
        ]);
        if ($query->andWhere(['need_upd' => false])->exists()) {
            $filter[self::WITHOUT_DOCUMENT] = 'Без УПД';
        }

        return $filter;
    }

    /**
     * @param $searchQuery
     * @return array
     */
    public function getInvoiceItemsByQuery($searchQuery)
    {
        $query = clone $searchQuery;

        $ids = $query->select([
            $this->type == Documents::IO_TYPE_OUT ? 'invoice.invoice_income_item_id' : 'invoice.invoice_expenditure_item_id',
        ])->distinct()->column();

        $itemsQuery = $this->type == Documents::IO_TYPE_OUT ? InvoiceIncomeItem::find() : InvoiceExpenditureItem::find();

        return ['' => 'Все'] + $itemsQuery
            ->where(['id' => $ids])
            ->select(['name', 'id'])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')->column();
    }

    /**
     * @param $searchQuery
     * @return array
     */
    public function getIndustryItemsByQuery($searchQuery)
    {
        $ids = (clone $searchQuery)->select('invoice.industry_id')->distinct()->column();
        $query = CompanyIndustry::find()
            ->select('name')
            ->where(['id' => $ids])
            ->orderBy('name')
            ->indexBy('id');

        return (['' => 'Все'] + [0 => 'Без направления'] + $query->column());
    }

    /**
     * @param $searchQuery
     * @return array
     */
    public function getSalePointItemsByQuery($searchQuery)
    {
        $ids = (clone $searchQuery)->select('invoice.sale_point_id')->distinct()->column();
        $query = SalePoint::find()
            ->select('name')
            ->where(['id' => $ids])
            ->orderBy('name')
            ->indexBy('id');

        return (['' => 'Все'] + [0 => 'Без точки продаж'] +  $query->column());
    }

    /**
     * @param $searchQuery
     * @return array
     */
    public function getProjectItemsByQuery($searchQuery)
    {
        $ids = (clone $searchQuery)->select('invoice.project_id')->distinct()->column();
        $query = Project::find()
            ->select('name')
            ->where(['id' => $ids])
            ->orderBy('name')
            ->indexBy('id');

        return (['' => 'Все'] + [0 => 'Без проекта'] +  $query->column());
    }
}
