<?php

namespace frontend\modules\documents\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\documents\models\SpecificDocument;

/**
 * SpecificDocumentSearch represents the model behind the search form about `frontend\modules\documents\models\SpecificDocument`.
 */
class SpecificDocumentSearch extends SpecificDocument
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'document_author_id'], 'integer'],
            [['document_date', 'name', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param $dateRange
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange = null)
    {
        $query = SpecificDocument::find();
        $query->andWhere([
            'or',
            ['document_author_id' => \Yii::$app->user->identity->id],
            ['company_id' => \Yii::$app->user->identity->company->id],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        if ($dateRange !== null) {
            $query->andFilterWhere(['>=', 'document_date', $dateRange['from']]);
            $query->andFilterWhere(['<=', 'document_date', $dateRange['to']]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
