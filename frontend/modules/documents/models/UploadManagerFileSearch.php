<?php

namespace frontend\modules\documents\models;

use common\components\date\DatePickerFormatBehavior;
use common\models\Agreement;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\ScanDocument;
use common\models\document\Upd;
use common\models\file\File;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use \common\models\employee\Employee;

/**
 * UploadManagerFileSearch represents the model behind the search form about `common\models\file\File`.
 */
class UploadManagerFileSearch extends File
{
    protected $query;

    public $find_by;
    public $find_free_scans;
    public $find_new_scans;
    public $by_directory;

    public $filter_contractor;
    public $filter_type;
    public $filter_doc_type;
    public $date_from;
    public $date_to;

    private $_query;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at_author_id', 'find_by', 'filter_contractor', 'filter_type', 'filter_doc_type', 'date_from', 'date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $company = Yii::$app->user->identity->company;

        $this->load($params);

        $F = static::tableName();

        $query = new Query;
        $query->select([
            "$F.id",
            "$F.filename",
            "$F.ext",
            "$F.filename_full",
            "$F.filepath",
            "$F.created_at",
            "$F.created_at_author_id",
            "$F.owner_model",
            "$F.owner_table",
            "$F.owner_id",
            "$F.directory_id",
            "$F.filesize",
            "$F.upload_type_id"
        ])->from($F)->where([
            "$F.company_id" => $company->id,
            "$F.is_deleted" => (int)$this->is_deleted
        ]);

        if ($this->find_new_scans) {
            $query->andWhere(['is', "$F.directory_id", null]);
            $query->andWhere(["$F.owner_table" => 'scan_document']);
        }

        if ($this->find_free_scans) {
            $query2 = ScanDocument::find();
            $query2->andWhere(['company_id' => $company->id]);
            $query2->andWhere(['owner_id' => null]);
            if ($freeScansIds = $query2->asArray()->select('id')->column()) {
                $query->andWhere(["$F.owner_id" => $freeScansIds, "$F.owner_model" => ScanDocument::className()]);
            } else {
                $query->where('0=1');
            }
        }

        $query->andFilterWhere([
            'or',
            ['like', "$F.filename", $this->find_by],
            ['like', "$F.filename_full", $this->find_by],
        ]);

        $query->andFilterWhere([
            "$F.directory_id" => $this->directory_id
        ]);

        if ($findBySender = $this->created_at_author_id) {

            if (strpos($findBySender, 'employee')) {
                $query2 = ScanDocument::find()
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['employee_id' => str_replace('_employee_', '', $findBySender)])
                    ->andWhere(["is_from_employee" => 1]);
                if ($scansIds = $query2->asArray()->select('id')->column()) {
                    $query->andWhere(["$F.owner_id" => $scansIds, "$F.owner_model" => ScanDocument::class]);
                } else {
                    $query->where('0=1');
                }
            } elseif (strpos($findBySender, 'contractor')) {
                $query2 = ScanDocument::find()
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['contractor_id' => str_replace('_contractor_', '', $findBySender)])
                    ->andWhere(["is_from_contractor" => 1]);
                if ($scansIds = $query2->asArray()->select('id')->column()) {
                    $query->andWhere(["$F.owner_id" => $scansIds, "$F.owner_model" => ScanDocument::class]);
                } else {
                    $query->where('0=1');
                }
            } elseif (strpos($findBySender, 'email')) {
                $query2 = ScanDocument::find()
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['from_email' => str_replace('_email_', '', $findBySender)])
                    ->andWhere(["is_from_contractor" => 0])
                    ->andWhere(["is_from_employee" => 0]);
                if ($scansIds = $query2->asArray()->select('id')->column()) {
                    $query->andWhere(["$F.owner_id" => $scansIds, "$F.owner_model" => ScanDocument::class]);
                } else {
                    $query->where('0=1');
                }
            } else {

                //$query2 = ScanDocument::find()
                //    ->andWhere(['company_id' => $company->id])
                //    ->andWhere(["is_from_contractor" => 0])
                //    ->andWhere(["is_from_employee" => 0])
                //    ->andWhere(["from_email" => '']);
                //if ($scansIds = $query2->asArray()->select('id')->column()) {
                //    $query->andWhere(["$F.owner_id" => $scansIds, "$F.owner_model" => ScanDocument::class]);
                //} else {
                //    $query->where('0=1');
                //}

                $query->andFilterWhere([
                    "$F.created_at_author_id" => $this->created_at_author_id
                ]);
            }
        }

        /** Filters */
        if ($this->date_from && $this->date_to) {
            $query->andWhere(['between', "$F.created_at", strtotime($this->date_from . ' 00:00:00'), strtotime($this->date_to . ' 23:59:59')]);
        }
        if ($this->filter_doc_type) {
            if ($filterClass = ArrayHelper::getValue(Documents::$docMap, $this->filter_doc_type)) {
                $query3 = ScanDocument::find();
                $query3->andWhere(['company_id' => $company->id]);
                $query3->andWhere(['owner_model' => $filterClass]);
                $inScans = $query3->asArray()->select('id')->column();
                if ($inScans) {
                    $query->andWhere(['or',
                        ["$F.owner_model" => $filterClass],
                        ['and', ["$F.owner_id" => $inScans], ["$F.owner_model" => ScanDocument::className()]]]);
                } else {
                    $query->andWhere(["$F.owner_model" => $filterClass]);
                }
            }
        }

        if ($this->filter_contractor || $this->filter_type) {
            $invoicesIds = Invoice::find()
                ->select('id')
                ->where(['company_id' => $company->id])
                ->andFilterWhere(['contractor_id' => $this->filter_contractor])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $agreementsIds = Agreement::find()
                ->where(['company_id' => $company->id])
                ->andFilterWhere(['contractor_id' => $this->filter_contractor])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $invoicesFacturesIds = InvoiceFacture::find()
                ->select('id')
                ->where(['invoice_id' => $invoicesIds])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $packingListsIds = PackingList::find()
                ->select('id')
                ->where(['invoice_id' => $invoicesIds])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $updsIds = Upd::find()
                ->select('id')
                ->where(['invoice_id' => $invoicesIds])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $actsIds = Act::find()
                ->select('id')
                ->where(['invoice_id' => $invoicesIds])
                ->andFilterWhere(['type' => $this->filter_type])
                ->column();
            $scanDocumentsIds = ScanDocument::find()
                ->select('id')
                ->andWhere(['or',
                    ['and', ["owner_model" => Invoice::class], ["owner_id" => $invoicesIds]],
                    ['and', ["owner_model" => Agreement::class], ["owner_id" => $agreementsIds]],
                    ['and', ["owner_model" => InvoiceFacture::class], ["owner_id" => $invoicesFacturesIds]],
                    ['and', ["owner_model" => PackingList::class], ["owner_id" => $packingListsIds]],
                    ['and', ["owner_model" => Upd::class], ["owner_id" => $updsIds]],
                    ['and', ["owner_model" => Act::class], ["owner_id" => $actsIds]]
                ])
                ->column();

            $query->andWhere(['or',
                ['and', ["$F.owner_model" => Invoice::class], ["$F.owner_id" => $invoicesIds]],
                ['and', ["$F.owner_model" => Agreement::class], ["$F.owner_id" => $agreementsIds]],
                ['and', ["$F.owner_model" => InvoiceFacture::class], ["$F.owner_id" => $invoicesFacturesIds]],
                ['and', ["$F.owner_model" => PackingList::class], ["$F.owner_id" => $packingListsIds]],
                ['and', ["$F.owner_model" => Upd::class], ["$F.owner_id" => $updsIds]],
                ['and', ["$F.owner_model" => Act::class], ["$F.owner_id" => $actsIds]],
                ['and', ["$F.owner_model" => ScanDocument::class], ["$F.owner_id" => $scanDocumentsIds]],
            ]);
        }

        $this->_query = clone $query;

        $countQuery = clone $query;
        $count = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'created_at',
                    'filename_full',
                    'filesize',
                    "$F.directory_id"
                ],
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);
    }

    public function getFilterContractors($type = null)
    {
        if ($this->is_deleted)
            return [];

        $company = Yii::$app->user->identity->company;

        // get owners
        $query = File::find()
            ->select(['owner_model', 'owner_id'])
            ->where(['company_id' => $company->id])
            ->andWhere(['not', ['owner_model' => ScanDocument::class]])
            ->andWhere(['not', ['owner_model' => SpecificDocument::class]])
            ->andWhere(['is_deleted' => (int)$this->is_deleted])->union(
                ScanDocument::find()
                    ->select(['owner_model', 'owner_id'])
                    ->where(['company_id' => $company->id])
            );

        $docs = $query->asArray()->all();
        $documentsIds = [];

        foreach ($docs as $doc) {
            $documentsIds[$doc['owner_model']][] = $doc['owner_id'];
        }

        // get invoices & agreements
        $invoicesIds = [];
        $agreementsIds = [];
        foreach ($documentsIds as $className => $docIds) {
            if (!$className)
                continue;
            if ($className == Invoice::class) {
                $invoicesIds = array_merge($invoicesIds, $docIds);
                continue;
            }
            if ($className == Agreement::class) {
                $agreementsIds = $docIds;
                continue;
            }

            $currIds = $className::find()
                ->select('invoice_id')
                ->where(['id' => $docIds])
                ->column();

            $invoicesIds = array_merge($invoicesIds, $currIds);
        }

        // get contractors
        $contractorIds = Invoice::find()
            ->select('contractor_id')
            ->distinct()
            ->where(['id' => $invoicesIds])
            ->union(Agreement::find()->select('contractor_id')->distinct()->where(['id' => $agreementsIds]))
            ->asArray()
            ->column();

        if ($contractorIds) {
            $queryContractor = Contractor::getSorted()
                ->select([
                    'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                    'contractor.id',
                ])
                ->andWhere([
                    'contractor.id' => $contractorIds,
                ])
                ->andFilterWhere([
                    'type' => $type
                ])
                ->indexBy('id');

            return ($queryContractor->column());
        }

        return [];
    }

    public function searchFilesIds()
    {
        $F = static::tableName();

        $query = new Query;
        $query->select([
            "$F.id",
        ])->from($F)->where([
            "$F.company_id" => Yii::$app->user->identity->company->id,
            "$F.is_deleted" => 0
        ]);

        if ($this->find_new_scans) {
            $query->andWhere(['is', "$F.directory_id", null]);
            $query->andWhere(["$F.owner_table" => 'scan_document']);
        }

        if ($this->find_free_scans) {
            $query2 = ScanDocument::find();
            $query2->andWhere(['company_id' => Yii::$app->user->identity->company->id]);
            $query2->andWhere(['is', 'owner_id', null]);
            if ($freeScansIds = $query2->asArray()->select('id')->column()) {
                $query->andWhere(["$F.owner_id" => $freeScansIds, "$F.owner_model" => ScanDocument::className()]);
            } else {
                $query->where('0=1');
            }
        }

        if ($this->by_directory) {
            $query->andFilterWhere(["$F.directory_id" => $this->directory_id]);
        }

        return $query->column();
    }


    /**
     * @param Query $query
     * @return array
     */
    public function getCreators(Query $query)
    {
        $S = ScanDocument::tableName();
        $E = Employee::tableName();
        $C = Contractor::tableName();
        $F = self::tableName();

        $scansIds = (clone $query)->leftJoin($S, "$F.owner_id = $S.id")->andWhere(["$F.owner_model" => ScanDocument::className()])->select("$S.id")->column();

        $employers = ArrayHelper::map(
            Employee::find()
                ->leftJoin($S, "$S.employee_id = $E.id")
                ->where(["$S.company_id" => Yii::$app->user->identity->company->id])
                ->andWhere(["$S.id" => $scansIds])
                ->andWhere(["$S.is_from_employee" => 1])
                ->indexBy('id')
                ->all(),
            function ($model) { return "_employee_{$model->id}"; },
            function ($model) { return $model->getShortFio(true); }
        );

        $contractors = ArrayHelper::map(
            Contractor::find()
                ->leftJoin($S, "$S.contractor_id = $C.id")
                ->where(["$S.company_id" => Yii::$app->user->identity->company->id])
                ->andWhere(["$S.id" => $scansIds])
                ->andWhere(["$S.is_from_contractor" => 1])
                ->indexBy('id')
                ->all(),
            function ($model) { return "_contractor_{$model->id}"; },
            function ($model) { return $model->getShortName(true); }
        );

        $emails = ArrayHelper::map(
            ScanDocument::find()
                ->where(["$S.company_id" => Yii::$app->user->identity->company->id])
                ->andWhere(["$S.id" => $scansIds])
                ->andWhere(["$S.is_from_contractor" => 0])
                ->andWhere(["$S.is_from_employee" => 0])
                ->andWhere(['!=', "$S.from_email", ''])
                ->indexBy('id')
                ->all(),
            function ($model) { return "_email_{$model->from_email}"; },
            function ($model) { return $model->from_email ?: '---'; }
        );

        $pc_employers = ArrayHelper::map(
            //Employee::find()
                //->leftJoin($S, "$S.employee_id = $E.id")
                //->where(["$S.company_id" => Yii::$app->user->identity->company->id])
                //->andWhere(["$S.id" => $scansIds])
                //->andWhere(["$S.is_from_employee" => 0])
                //->andWhere(["$S.is_from_contractor" => 0])
                //->andWhere(["$S.from_email" => ''])
                //->indexBy('id')
                //->all(),
            Employee::find()
                ->leftJoin($F, "$F.created_at_author_id = $E.id")
                ->where(["$F.company_id" => Yii::$app->user->identity->company->id])
                ->indexBy('id')
                ->all(),
            function ($model) { return "{$model->id}"; },
            function ($model) { return $model->getShortFio(true); });

        return ['' => 'Все'] + $employers + $pc_employers + $contractors + $emails;
    }
}
