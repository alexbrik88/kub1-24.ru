<?php

namespace frontend\modules\documents\models;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\SearchPeriodTrait;
use common\components\TextHelper;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\document\Invoice;
use common\models\document\PaymentOrder;

/**
 * PaymentOrderSearch represents the model behind the search form about `common\models\document\PaymentOrder`.
 */
class PaymentOrderSearch extends PaymentOrder
{
    use SearchPeriodTrait;

    public $byNumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_name', 'byNumber', 'payment_order_status_id', 'document_author_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param int $dateRange
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->searchQuery();

        $query->joinWith([
            'invoice',
            'invoice.contractor',
        ])->andWhere([
            'or',
            [Invoice::tableName() . '.is_deleted' => false],
            [Invoice::tableName() . '.is_deleted' => null],
        ]);

        if (!empty($this->contractor_name)) {
            $query->andWhere([
                self::tableName() . '.contractor_name' => $this->contractor_name,
            ]);
        }

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    self::tableName() . '.`document_number` * 1',
                    'sum',
                    'id',
                ],
                'defaultOrder' => ['document_date_init' => SORT_DESC],
            ],
        ]);

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.contractor_name', $param],
                ]);
            }
        }

        $query->andFilterWhere(['payment_order_status_id' => $this->payment_order_status_id]);

        $query->andFilterWhere([PaymentOrder::tableName() . '.document_author_id' => $this->document_author_id]);

        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function searchQuery()
    {
        return self::find()
            ->andWhere([self::tableName() . '.company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['between', self::tableName() . '.document_date', $this->periodFrom, $this->periodTo]);
    }

    /**
     * @return array
     */
    public function getContractorArray()
    {
        return $this->searchQuery()
            ->select(self::tableName() . '.contractor_name')
            ->distinct()
            ->orderBy([self::tableName() . '.contractor_name' => SORT_ASC])
            ->createCommand()
            ->queryAll();
    }

    /**
     * @return array
     */
    public function getStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(PaymentOrderStatus::find()->all(), 'id', 'name'));
    }

    /**
     * @return array
     */
    public function getAuthorFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->searchQuery()
            ->joinWith('documentAuthor')
            ->orderBy([
                Employee::tableName() . '.firstname' => SORT_ASC,
                Employee::tableName() . '.lastname' => SORT_ASC,
                Employee::tableName() . '.patronymic' => SORT_ASC,
            ])
            ->all(), 'document_author_id', 'documentAuthor.shortFio'));
    }

    /**
     * @param $sortParam
     *
     * @return mixed
     */
    public function getExcelWriter(ActiveDataProvider $dataProvider, $type = 'Xlsx')
    {
        $modelArray = $dataProvider->models;
        $count = $dataProvider->count;
        $dataArray = [
            [
                'Дата ПП',
                '№ ПП',
                'Сумма',
                'Контрагент',
                'Статус',
                'Счета в ПП',
                'Ответственный',
            ],
        ];
        foreach ($modelArray as $model) {
            $invoices = [];
            foreach ($model->paymentOrderInvoices as $paymentOrderInvoice) {
                if ($paymentOrderInvoice->invoice !== null) {
                    $invoices[] = '№ ' . $paymentOrderInvoice->invoice->fullNumber;
                }
            }

            $dataArray[] = [
                DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                $model->document_number,
                TextHelper::invoiceMoneyFormat($model->sum, 2),
                $model->contractor_name,
                $model->paymentOrderStatus->name,
                $invoices ? implode(', ', $invoices) : '--',
                $model->documentAuthor->getFio(true),
            ];
        }

        $i = $count + 1;
        $objPHPExcel = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
        $objPHPExcel->getActiveSheet()->fromArray($dataArray);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:G{$i}")->applyFromArray([
            'borders' => array(
                'allborders' => array(
                    'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                )
            ),
        ]);

        return \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, $type);
    }
}
