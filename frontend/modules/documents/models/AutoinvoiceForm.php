<?php

namespace frontend\modules\documents\models;

use Yii;

/**
 * This is the model class for table "autoinvoice".
 *
 * @property integer $id
 * @property integer $period
 * @property integer $day
 * @property integer $date_from
 * @property integer $date_to
 * @property integer $add_month_and_year
 * @property integer $type
 * @property integer $company_id
 * @property integer $document_author_id
 * @property string $production_type
 * @property integer $contractor_id
 * @property integer $invoice_expenditure_item_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $document_number
 * @property string $all_sum
 * @property integer $last_invoice_date
 * @property integer $next_invoice_date
 * @property integer $status
 * @property integer $payment_delay
 * @property integer $date_stop
 * @property integer $send_to_director
 * @property integer $send_to_chief_accountant
 * @property integer $send_to_contact
 *
 * @property Company $company
 * @property Contractor $contractor
 * @property InvoiceExpenditureItem $invoiceExpenditureItem
 * @property Autoorder[] $autoorders
 */
class AutoinvoiceForm extends \common\models\document\Autoinvoice
{
    /**
     * send to
     */
    const TO_DIRECTOR = 'director';
    const TO_ACCOUNTANT = 'chief_accountant';
    const TO_CONTACT = 'contact';

    public $send_to;
    public $director_name;
    public $director_email;
    public $chief_accountant_name;
    public $chief_accountant_email;
    public $contact_name;
    public $contact_email;

    /**
     * @var array
     */
    public static $sendItems = [
        self::TO_DIRECTOR,
        self::TO_ACCOUNTANT,
        self::TO_CONTACT,
    ];

    /**
     * @var array
     */
    public static $sendLabels = [
        self::TO_DIRECTOR => 'Руководителю',
        self::TO_ACCOUNTANT => 'Бухгалтеру',
        self::TO_CONTACT => 'Контакту',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['send_to'], 'required'],
            [['send_to'], 'each', 'rule' => ['in', 'range' => [
                self::TO_CONTACT,
                self::TO_DIRECTOR,
                self::TO_ACCOUNTANT,
            ]]],
            [['director_name', 'chief_accountant_name', 'contact_name'], 'string', 'max' => 45],
            [['director_email', 'chief_accountant_email', 'contact_email'], 'email'],
            [['director_name'], 'required', 'when' => function ($model) {
                return $model->needDirectorName();
            }],
            [['director_email'], 'required', 'when' => function ($model) {
                return $model->needDirectorEmail();
            }],
            [['chief_accountant_name'], 'required', 'when' => function ($model) {
                return $model->needAccountantName();
            }],
            [['chief_accountant_email'], 'required', 'when' => function ($model) {
                return $model->needAccountantEmail();
            }],
            [['contact_name'], 'required', 'when' => function ($model) {
                return $model->needContactName();
            }],
            [['contact_email'], 'required', 'when' => function ($model) {
                return $model->needContactEmail();
            }],
        ]);
    }

    /**
     * @return bool
     */
    public function needDirectorName()
    {
        return in_array(self::TO_DIRECTOR, (array) $this->send_to) && empty($this->invoice->contractor->director_name);
    }

    /**
     * @return bool
     */
    public function needDirectorEmail()
    {
        return in_array(self::TO_DIRECTOR, (array) $this->send_to) && empty($this->invoice->contractor->director_email);
    }

    /**
     * @return bool
     */
    public function needAccountantName()
    {
        return in_array(self::TO_ACCOUNTANT, (array) $this->send_to) && empty($this->invoice->contractor->chief_accountant_name);
    }

    /**
     * @return bool
     */
    public function needAccountantEmail()
    {
        return in_array(self::TO_ACCOUNTANT, (array) $this->send_to) && empty($this->invoice->contractor->chief_accountant_email);
    }

    /**
     * @return bool
     */
    public function needContactName()
    {
        return in_array(self::TO_CONTACT, (array) $this->send_to) && empty($this->invoice->contractor->contact_name);
    }

    /**
     * @return bool
     */
    public function needContactEmail()
    {
        return in_array(self::TO_CONTACT, (array) $this->send_to) && empty($this->invoice->contractor->contact_email);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'send_to' => 'Отправлять Счета',
            'director_name' => 'ФИО руководителя',
            'director_email' => 'E-mail руководителя',
            'chief_accountant_name' => 'ФИО бухгалтера',
            'chief_accountant_email' => 'E-mail бухгалтера',
            'contact_name' => 'ФИО контакта',
            'contact_email' => 'E-mail контакта',
        ]);
    }

    /**
     * @return array
     */
    public function getSendItems()
    {
        return self::$sendLabels;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->send_to_director = in_array(self::TO_DIRECTOR, $this->send_to);
            $this->send_to_chief_accountant = in_array(self::TO_ACCOUNTANT, $this->send_to);
            $this->send_to_contact = in_array(self::TO_CONTACT, $this->send_to);

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->send_to = [];
        foreach (self::$sendItems as $item) {
            if ($this->{'send_to_' . $item}) {
                $this->send_to[] = $item;
            }
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $attributes = [];
        if ($this->needDirectorName()) {
            $attributes['director_name'] = $this->director_name;
        }
        if ($this->needDirectorEmail()) {
            $attributes['director_email'] = $this->director_email;
        }
        if ($this->needAccountantName()) {
            $attributes['chief_accountant_name'] = $this->chief_accountant_name;
            $attributes['chief_accountant_is_director'] = false;
        }
        if ($this->needAccountantEmail()) {
            $attributes['chief_accountant_email'] = $this->chief_accountant_email;
            $attributes['chief_accountant_is_director'] = false;
        }
        if ($this->needContactName()) {
            $attributes['contact_name'] = $this->contact_name;
            $attributes['contact_is_director'] = false;
        }
        if ($this->needContactEmail()) {
            $attributes['contact_email'] = $this->contact_email;
            $attributes['contact_is_director'] = false;
        }

        if ($attributes) {
            $this->invoice->contractor->updateAttributes($attributes);
        }
    }
}
