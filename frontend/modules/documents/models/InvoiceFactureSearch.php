<?php

namespace frontend\modules\documents\models;

use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\status\InvoiceFactureStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * InvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class InvoiceFactureSearch extends InvoiceFacture
{
    /**
     * @var
     */
    public $contractor_id;
    /**
     * @var
     */
    public $byNumber;
    /**
     * @var
     */
    public $industry_id;
    public $sale_point_id;
    public $project_id;

    /**
     * @var
     */
    public $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'status_out_id', 'contractor_id'], 'integer'],
            [['document_date', 'byNumber', 'fileCount'], 'safe'],
            [['industry_id', 'sale_point_id', 'project_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param $params
     * @param $dateRange
     * @param $forExport
     * @return ActiveDataProvider
     * @throws NotFoundHttpException
     */
    public function search($params, $dateRange = null, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $query = Documents::getSearchQuery(Documents::DOCUMENT_INVOICE_FACTURE, $this->type);

        $dateRange = $dateRange ?: StatisticPeriod::getSessionPeriod();

        $query->joinWith([
            'files',
            'invoice',
            'invoice.contractor',
            'ownOrders.order',
        ])->with([
            'statusOut',
            'scanDocuments',
        ])->andWhere([
            Invoice::tableName() . '.is_deleted' => false,
        ])->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        $priceField = $this->type == Documents::IO_TYPE_IN ? 'purchase_price_with_vat' : 'selling_price_with_vat';
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'has_file' => [
                        'asc' => ['file.owner_id'=> SORT_ASC],
                        'desc' => ['file.owner_id' => SORT_DESC],
                    ],
                    'totalAmountWithNds' => [
                        'asc' => ["SUM({{order}}.[[$priceField]] * {{order_invoice_facture}}.[[quantity]])" => SORT_ASC],
                        'desc' => ["SUM({{order}}.[[$priceField]] * {{order_invoice_facture}}.[[quantity]])" => SORT_DESC],
                    ],
                    'invoice_document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => [($this->type == 1 ? 'document_date' : 'document_date_init') => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->addSelect([
                self::tableName() . '.*',
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        $query->andFilterWhere([
            'document_date' => $this->document_date,
            'id' => $this->id,
            Invoice::tableName() . '.contractor_id' => $this->contractor_id,
            'status_out_id' => $this->status_out_id,
            self::tableName() . '.invoice_id' => $this->invoice_id,
        ]);

        $query->andWhere(['between', InvoiceFacture::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        if ($forExport) {
            return $query->orderBy(['`' . self::tableName() .'`.`document_date`' => SORT_DESC,
                '`' . self::tableName() .'`.`document_number` * 1' => SORT_DESC])->all();
        }

        if (strlen($this->project_id)) {
            $query->andWhere(['invoice.project_id' => $this->project_id ?: null]);
        }
        if (strlen($this->industry_id)) {
            $query->andWhere(['invoice.industry_id' => $this->industry_id ?: null]);
        }
        if (strlen($this->sale_point_id)) {
            $query->andWhere(['invoice.sale_point_id' => $this->sale_point_id ?: null]);
        }

        $this->_query = clone $query;

        return $dataProvider;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function getStatusArray()
    {
        if ($this->_query === null) {
            $this->search([]);
        }

        $query = clone $this->getQuery();
        $statusArray = $query
            ->innerJoin(['status' => 'invoice_facture_status'], "{{invoice_facture}}.[[status_out_id]] = {{status}}.[[id]]")
            ->select(['status.name', 'status.id'])
            ->andWhere(['NOT IN', 'status.id', [InvoiceFactureStatus::STATUS_REJECTED]])
            ->distinct()
            ->indexBy('id')
            ->column();

        return ['' => 'Все'] + $statusArray;
    }

    /**
     * @param array $models
     * @param $key
     * @param $value
     * @param bool|true $addAll
     * @return array
     */
    public static function getFilterArray(array $models, $key, $value, $addAll = true)
    {
        $filterArray = [];

        if ($addAll !== false) {
            $filterArray[null] = 'Все';
        }

        $filterArray += ArrayHelper::map($models, $key, $value);

        return $filterArray;
    }
}
