<?php

namespace frontend\modules\documents\models;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\DocumentImportType;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\UpdStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\document\Invoice;
use common\models\document\Upd;
use frontend\models\Documents;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * UpdSearch represents the model behind the search form about `common\models\document\Upd`.
 */
class UpdSearch extends Upd
{
    /**
     * @var
     */
    public $upd_source;
    public $upd_responsible;
    public $contractor_id;
    public $byNumber;

    public $payment_status;

    public $industry_id;
    public $sale_point_id;
    public $project_id;

    protected $searchQuery;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_out_id', 'contractor_id', 'invoice_id'], 'integer'],
            [['byNumber'], 'string'],
            [['fileCount'], 'safe'],
            [['upd_responsible', 'upd_source'], 'safe'],
            [['payment_status'], 'safe'],
            [['industry_id', 'sale_point_id', 'project_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @param $forExport boolean
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange = null, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $query = Documents::getSearchQuery(Documents::DOCUMENT_UPD, $this->type);

        $dateRange = StatisticPeriod::getSessionPeriod();

        $query
            ->addSelect([
                Upd::tableName() . '.*',
                Invoice::tableName() . '.total_amount_no_nds as order_sum_without_nds',
                Invoice::tableName() . '.total_amount_nds as order_sum_nds',
                'IF('.CashBankFlows::tableName() . '.date , ' . CashBankFlows::tableName() . '.date, ' . CashOrderFlows::tableName() . '.date) as upd_payment_date',
            ])
            ->joinWith([
                'file',
                'invoice',
                'ownOrders',
                'ownOrders.order',
                'invoice.contractor',
                'invoice.cashBankFlows',
                'invoice.cashOrderFlows',
            ])
            ->andWhere(['between', 'upd.document_date', $dateRange['from'], $dateRange['to']])
            ->andWhere([Invoice::tableName() . '.is_deleted' => false])
            ->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        $priceField = $this->type == Documents::IO_TYPE_IN ? 'purchase_price_with_vat' : 'selling_price_with_vat';
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'has_file' => [
                        'asc' => ['file.owner_id'=> SORT_ASC],
                        'desc' => ['file.owner_id' => SORT_DESC],
                    ],
                    'totalAmountWithNds' => [
                        'asc' => ["SUM({{order}}.[[$priceField]] * {{order_upd}}.[[quantity]])" => SORT_ASC],
                        'desc' => ["SUM({{order}}.[[$priceField]] * {{order_upd}}.[[quantity]])" => SORT_DESC],
                    ],
                    'order_sum_without_nds',
                    'order_sum_nds',
                    'invoice.document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'upd_payment_date',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if ($this->payment_status) {
            $this->filterByPaymentStatus($query, $this->payment_status);
        }


        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->addSelect([
                self::tableName() . '.*',
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->upd_responsible) {
            $query->andFilterWhere([Invoice::tableName().'.responsible_employee_id' => $this->upd_responsible]);
        }

        if (strlen($this->upd_source)) {
            $query->andWhere([ self::tableName() . '.one_c_imported' => (int)$this->upd_source]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'upd.status_out_id' => $this->status_out_id,
            'invoice.contractor_id' => $this->contractor_id,
            self::tableName() . '.invoice_id' => $this->invoice_id,
        ]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        if ($forExport) {
            return $query->orderBy(['`' . self::tableName() .'`.`document_date`' => SORT_DESC])->all();
        }

        if (strlen($this->project_id)) {
            $query->andWhere(['invoice.project_id' => $this->project_id ?: null]);
        }
        if (strlen($this->industry_id)) {
            $query->andWhere(['invoice.industry_id' => $this->industry_id ?: null]);
        }
        if (strlen($this->sale_point_id)) {
            $query->andWhere(['invoice.sale_point_id' => $this->sale_point_id ?: null]);
        }

        $this->searchQuery = $query;

        return $dataProvider;
    }


    /**
     * @return ActiveQuery
     */
    public function getQuery()
    {
        return clone $this->searchQuery;
    }

    /**
     * @return mixed
     */
    public function getStatusArray()
    {
        if ($this->searchQuery === null) {
            $this->search([]);
        }

        $query = clone $this->getQuery();
        $statusArray = $query
            ->innerJoin(['status' => 'upd_status'], "{{upd}}.[[status_out_id]] = {{status}}.[[id]]")
            ->select(['status.name', 'status.id'])
            ->andWhere(['NOT IN', 'status.id', [UpdStatus::STATUS_REJECTED]])
            ->distinct()
            ->indexBy('id')
            ->column();

        return ['' => 'Все'] + $statusArray;
    }

    /**
     * @return Employee[]
     */
    public function filterUpdResponsible()
    {
        $employeeCompany = \common\models\EmployeeCompany::find()
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->orderBy('lastname, firstname, patronymic')
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($employeeCompany, 'employee_id', 'shortFio');
    }

    /**
     * @return string[]
     */
    public function filterUpdSource()
    {
        return ['' => 'Все'] + DocumentImportType::getList();
    }

    /**
     * @param ActiveQuery $query
     * @param string $payment_status
     */
    private function filterByPaymentStatus(ActiveQuery &$query, $payment_status)
    {
        switch ($payment_status) {
            case 'all':
            default:
                break;
            case 'not_payment':
                $query->andFilterWhere(['NOT IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED]]);
                break;
            case 'payment':
                $query->andFilterWhere(['IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]]);
                break;

        }
    }

}
