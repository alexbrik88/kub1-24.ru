<?php

namespace frontend\modules\documents\models;

use frontend\rbac\permissions\Employee;
use Yii;
use frontend\modules\tax\models\Kudir;
use common\models\Contractor;
use common\models\company\CompanyType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\data\SqlDataProvider;
use yii\db\Query;

/**
 * KudirSearch represents the model behind the search form about `common\models\Kudir`.
 */
class KudirSearch extends Kudir
{
    protected $query;

    public $find_by;
    public $type;
    public $status;
    public $document_author_id;

    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'tax_year', 'status_updated_at', 'status_author_id', 'document_author_id', 'find_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $A = static::tableName();

        $query = new Query;
        $query->select([
            "$A.id",
            "$A.document_date",
            "$A.tax_year",
            "$A.status_id",
            "$A.document_author_id",
            "$A.created_at"
            ])
            ->from($A)
            ->where(["$A.company_id" => Yii::$app->user->identity->company->id]);

        $query->andFilterWhere([
            'kudir.status_id' => $this->status,
            'kudir.document_author_id' => $this->document_author_id,
            'kudir.tax_year' => $this->find_by
        ]);

        $countQuery = clone $query;
        $count = (int) $countQuery->limit(-1)->offset(-1)->orderBy([])->count('*');

        return new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'tax_year',
                    'document_date',
                ],
                'defaultOrder' => [
                    'tax_year' => SORT_DESC,
                ],
            ],
        ]);
    }

    public function getCreators() {

        return ['' => 'Все'] + ArrayHelper::map(
                \common\models\employee\Employee::find()
                    ->innerJoin('kudir', 'kudir.document_author_id = employee.id')
                    ->where(['kudir.company_id'=>Yii::$app->user->identity->company->id])
                    ->indexBy('id')
                    ->all(), 'id', function ($model) {
                return $model->getShortFio(true);
            });
    }
}
