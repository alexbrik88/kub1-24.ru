<?php

namespace frontend\modules\documents\models;

use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\document\Act;
use common\models\document\DocumentImportType;
use common\models\document\Invoice;
use common\models\document\query\ActQuery;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * ActSearch represents the model behind the search form about `common\models\Act`.
 */
class ActSearch extends Act
{
    /**
     * @var
     */
    public $contractor_id;
    public $contractor_inn;
    public $byNumber;
    public $dateFrom;
    public $dateTo;
    public $act_payment_date;
    public $act_responsible;
    public $act_source;

    public $payment_status;

    public $industry_id;
    public $sale_point_id;
    public $project_id;

    protected $_query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invoice_id', 'status_out_id', 'contractor_id', 'contractor_inn'], 'integer'],
            [['document_date', 'byNumber', 'fileCount'], 'safe'],
            [['act_responsible', 'act_source'], 'safe'],
            [['payment_status'], 'safe'],
            [['industry_id', 'sale_point_id', 'project_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param null $dateRange array
     * @param $forExport boolean
     * @return array|ActiveDataProvider|ActiveRecord[]
     */
    public function search($params, $dateRange = null, $forExport = false)
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'document_date_init' => [
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            Invoice::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'document_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'has_file',
                    'invoice.total_amount_with_nds',
                    'order_sum_without_nds',
                    'order_sum_nds',
                    'invoice_document_number' => [
                        'asc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [Invoice::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'act_payment_date',
                    'upd_payment_date',
                ],
                'defaultOrder' => [($this->type == 1 ? 'document_date' : 'document_number') => SORT_DESC],
            ],
        ]);

        if ($forExport) {
            return $query->orderBy(['`' . Act::tableName() . '`.`document_date`' => SORT_DESC,
                '`' . self::tableName() . '`.`document_number` * 1' => SORT_DESC,
                '`' . self::tableName() . '`.`document_additional_number` * 1' => SORT_DESC])->all();
        }

        if (strlen($this->project_id)) {
            $query->andWhere(['invoice.project_id' => $this->project_id ?: null]);
        }
        if (strlen($this->industry_id)) {
            $query->andWhere(['invoice.industry_id' => $this->industry_id ?: null]);
        }
        if (strlen($this->sale_point_id)) {
            $query->andWhere(['invoice.sale_point_id' => $this->sale_point_id ?: null]);
        }

        $this->_query = $query;

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery()
    {
        return clone $this->_query;
    }

    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var ActQuery $query */
        $query = Documents::getSearchQuery(Documents::DOCUMENT_ACT, $this->type);

        $query
            ->addSelect([
                Act::tableName() . '.*',
                Invoice::tableName() . '.total_amount_nds as order_sum_nds',
                Invoice::tableName() . '.payment_limit_date as upd_payment_date',
                'IF('.CashBankFlows::tableName() . '.date , ' . CashBankFlows::tableName() . '.date, ' . CashOrderFlows::tableName() . '.date) as act_payment_date',
            ])
            ->joinWith([
                'statusOut',
                'invoice',
                'invoice.contractor',
                'invoice.payment',
                'invoice.cashBankFlows',
                'invoice.cashOrderFlows',
            ]);

        $query
            ->andWhere([
                Invoice::tableName() . '.is_deleted' => false,
            ])
            ->groupBy(self::tableName() . '.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_STRANGERS)) {
            $query->andWhere(['or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if (Yii::$app->user->identity->currentEmployeeCompany->document_access_own_only) {
            $query->andWhere([
                'or',
                [Invoice::tableName().'.document_author_id' => Yii::$app->user->id],
                [self::tableName().'.document_author_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere([Contractor::tableName() . '.not_accounting' => false]);
        }

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->joinWith([
                'files file',
            ])->addSelect([
                self::tableName() . '.*',
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        $query->andFilterWhere([
            'document_date' => $this->document_date,
            'id' => $this->id,
            Invoice::tableName() . '.contractor_id' => $this->contractor_id,
            'status_out_id' => $this->status_out_id,
            self::tableName() . '.invoice_id' => $this->invoice_id,
        ]);

        if ($this->act_responsible) {
            $query->andFilterWhere(['invoice.responsible_employee_id' => $this->act_responsible]);
        }

        if (isset($this->act_source) && 1 == $this->act_source) {
            $query->andFilterWhere(['IS NOT', Act::tableName() . '.object_guid', new Expression('NULL')]);
        } else if (isset($this->act_source) && '' !== $this->act_source) {
            $query->andFilterWhere(['IS', Act::tableName() . '.object_guid', new Expression('NULL')]);
        }


        $query->andWhere(['between', Act::tableName() . '.document_date', $dateRange['from'], $dateRange['to']]);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',
                    [Invoice::tableName() . '.contractor_inn' => $param],
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        if ($this->payment_status) {
            $this->filterByPaymentStatus($query, $this->payment_status);
        }

        return $query;
    }

    /**
     * @return mixed
     */
    public function getStatusArray()
    {
        if ($this->_query === null) {
            $this->search([]);
        }

        $query = clone $this->getQuery();
        $statusArray = $query
            ->innerJoin(['status' => 'act_status'], "{{act}}.[[status_out_id]] = {{status}}.[[id]]")
            ->select(['status.name', 'status.id'])
            ->andWhere(['NOT IN', 'status.id', [ActStatus::STATUS_REJECTED]])
            ->distinct()
            ->indexBy('id')
            ->column();

        return ['' => 'Все'] + $statusArray;
    }

    /**
     * @return Employee[]
     */
    public function filterActResponsible()
    {
        $employeeCompany = EmployeeCompany::find()
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->orderBy('lastname, firstname, patronymic')
            ->all();

        return ['' => 'Все'] + ArrayHelper::map($employeeCompany, 'employee_id', 'shortFio');
    }

    /**
     * @return string[]
     */
    public function filterActSource()
    {
        return ['' => 'Все'] + DocumentImportType::getList();
    }

    /**
     * @param ActiveQuery $query
     * @param string $by_payment_status
     */
    private function filterByPaymentStatus(ActiveQuery &$query, $by_payment_status)
    {
        switch ($by_payment_status) {
            case 'all':
            default:
                break;
            case 'not_payment':
                $query->andFilterWhere(['NOT IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED]]);
                break;
            case 'payment':
                $query->andFilterWhere(['IN', 'invoice.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]]);
                break;

        }
    }

}
