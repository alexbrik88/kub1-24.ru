<?php

namespace frontend\modules\documents\models;

use common\models\evotor\Receipt;
use frontend\components\StatisticPeriod;
use yii\base\Model;

/**
 * Форма поиска и хелпер для отчёта по розничным продажам
 */
class EvotorReceiptSearch extends Model
{

    /** @var int ID компании КУБ */
    public $companyId;
    /** @var string|null Ключевая фраза для поиска */
    public $keyword;
    /** @var int|null Дата ОТ */
    public $dateFrom;
    /** @var int|null Дата ДО */
    public $dateTo;
    /** @var string */
    public $uuid;

    public function __construct($config = [])
    {
        if (isset($config['dateRange']) === true) {
            $this->dateFrom = strtotime($config['dateRange']['from']);
            $this->dateTo = strtotime($config['dateRange']['to']);
            unset($config['dateRange']);
        }
        parent::__construct($config);
        if ($this->dateFrom === null || $this->dateTo === null) {
            $dateRange = StatisticPeriod::getDefaultPeriod();
            if ($this->dateFrom === null) {
                $this->dateFrom = strtotime($dateRange['from']);
            }
            if ($this->dateTo === null) {
                $this->dateTo = strtotime($dateRange['to']);
            }
        }
    }

    public function rules(): array
    {
        return [
            ['keyword', 'string'],
            [['dateFrom', 'dateTo'], 'integer'],
            ['uuid', 'string', 'length' => 36],
        ];
    }

    /**
     * Возвращает провайдер данных для страницы "Отчёты" (с группировкой по товару)
     *
     * @param string[]|null Список UUID товаров
     * @return RetailDataProvider
     */
    public function dataProviderByProduct(array $productIDs = null): RetailDataProvider
    {
        $provider = RetailDataProvider::instance($this->companyId, $this->dateFrom, $this->dateTo, $this->keyword);
        $provider->prepareForReport($productIDs);
        return $provider;
    }

    public function dataProviderFlat(): RetailDataProvider
    {
        $provider = RetailDataProvider::instance($this->companyId, $this->dateFrom, $this->dateTo);
        if ($this->uuid) {
            $provider->query->andWhere(['ri.uuid' => $this->uuid]);
        }
        $provider->prepareForFlat();
        return $provider;
    }


    /**
     * Возвращает массив суммарных значений по платежам за указанный период
     *
     * @return array
     */
    public function paymentTotal(): array
    {
        $total = Receipt::find()->select(['count' => 'COUNT(total_amount)', 'card' => 'SUM(IF(cash=1,0,total_amount))', 'cash' => 'SUM(IF(cash=1,total_amount,0))'])
            ->andWhere(['company_id' => $this->companyId])
            ->andWhere(['between', 'date_time', $this->dateFrom, $this->dateTo])
            ->asArray(true)->one();
        $total['cash'] = round($total['cash'], 2);
        $total['card'] = round($total['card'], 2);
        return $total;
    }

    /**
     * Возвращает строку, содержащую диапазон дат (с дд.мм.гггг до дд.мм.гггг)
     *
     * @return string
     */
    public function dateRangeLabel(): string
    {
        if ($this->dateFrom !== null) {
            $s = 'с ' . date('d.m.Y', $this->dateFrom) . ' ';
        } else {
            $s = '';
        }
        if ($this->dateTo !== null) {
            $s .= 'до ' . date('d.m.Y', $this->dateTo);
        }
        return $s;
    }
}
