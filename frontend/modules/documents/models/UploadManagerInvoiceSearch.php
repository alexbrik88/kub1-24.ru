<?php

namespace frontend\modules\documents\models;

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\document\query\InvoiceQuery;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\Product;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * UploadManagerInvoiceSearch represents the model behind the search form about `common\models\Invoice`.
 */
class UploadManagerInvoiceSearch extends Invoice
{
    public $act;
    public $packingList;
    public $upd;
    public $byNumber;

    protected $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_date', 'document_number', 'contractor_id'], 'integer'],
            [['total_amount_with_nds'], 'number'],
            [['byNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $dateRange
     *
     * @return ActiveDataProvider
     */
    public function search($params, $dateRange = null, $ioType = null, $documentType = null)
    {
        $this->period = $dateRange;
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        /* @var InvoiceQuery $query */
        $paidStatuses = implode(',', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]);
        $query = self::find()
            ->addSelect([
                'invoice.*',
                'payDate' => new \yii\db\Expression("
                    IF({{invoice}}.[[invoice_status_id]] in ({$paidStatuses}), {{invoice}}.[[invoice_status_updated_at]], NULL)
                "),
            ])
            ->andWhere([
                'invoice.company_id' => $this->company_id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
                'invoice.from_demo_out_invoice' => false
            ]);

        $query->joinWith([
            'invoiceStatus',
            'company',
            'contractor.companyType',
        ])->with([
            'author',
            'acts',
            'packingLists',
            'invoiceFactures',
            'upds',
        ])->groupBy('invoice.id');

        if (!Yii::$app->user->can(\frontend\rbac\permissions\document\Document::INDEX_ALL)) {
            $query->andWhere([
                'or',
                ['invoice.document_author_id' => Yii::$app->user->id],
                ['contractor.responsible_employee_id' => Yii::$app->user->id],
            ]);
        }

        if ($user->currentEmployeeCompany->employee_role_id !== EmployeeRole::ROLE_CHIEF) {
            $query->andWhere(['or',
                [Contractor::tableName() . '.not_accounting' => false],
                ['and',
                    [Contractor::tableName() . '.not_accounting' => true],
                    [Contractor::tableName() . '.responsible_employee_id' => $user->id],
                ],
                ['and',
                    [Contractor::tableName() . '.not_accounting' => true],
                    [Invoice::tableName() . '.document_author_id' => $user->id],
                ],
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => (!empty($sortParamTitle)) ? $sortParamTitle : 'sort',
                'attributes' => [
                    'document_date' => [
                        'asc' => [
                            self::tableName() . '.`document_date`' => SORT_ASC,
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_date`' => SORT_DESC,
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                    'payDate' => [
                        'default' => SORT_DESC,
                    ],
                    'document_number' => [
                        'asc' => [self::tableName() . '.`document_number` * 1' => SORT_ASC],
                        'desc' => [self::tableName() . '.`document_number` * 1' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'total_amount_with_nds',
                    'has_file',
                ],
                'defaultOrder' => ['document_date' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (isset($this->fields()['fileCount']) || isset($this->_fileCount)) {
            $query->joinWith([
                'files file',
            ])->addSelect([
                'fileCount' => new \yii\db\Expression("
                    COUNT({{file}}.[[id]])
                "),
            ]);

            if (isset($this->_fileCount)) {
                $fileCount = is_string($this->_fileCount) ?
                    preg_split('/\s*,\s*/', $this->fileCount, -1, PREG_SPLIT_NO_EMPTY) :
                    $this->fileCount;

                $query->andFilterHaving(['fileCount' => $fileCount]);
            }
        }

        if ($ioType == Documents::IO_TYPE_IN) {
            if ($documentType == Documents::DOCUMENT_ACT) {
                $query->andWhere(['or', ['can_add_act' => true], ['has_act' => true]]);
            } elseif ($documentType == Documents::DOCUMENT_PACKING_LIST) {
                $query->andWhere(['or', ['can_add_packing_list' => true], ['has_packing_list' => true]]);
            } elseif ($documentType == Documents::DOCUMENT_INVOICE_FACTURE) {
                $query->andWhere(['or', ['can_add_invoice_facture' => true], ['has_invoice_facture' => true]]);
            } elseif ($documentType == Documents::DOCUMENT_UPD) {
                $query->andWhere(['or', ['can_add_upd' => true], ['has_upd' => true]]);
            } elseif ($documentType == Documents::DOCUMENT_INVOICE) {
                //
            } else {
                $query->andWhere('1=0');
            }
        }
        if ($ioType == Documents::IO_TYPE_OUT) {
            if ($documentType == Documents::DOCUMENT_ACT) {
                $query->andWhere(['has_act' => true]);
            } elseif ($documentType == Documents::DOCUMENT_PACKING_LIST) {
                $query->andWhere(['has_packing_list' => true]);
            } elseif ($documentType == Documents::DOCUMENT_INVOICE_FACTURE) {
                $query->andWhere(['has_invoice_facture' => true]);
            } elseif ($documentType == Documents::DOCUMENT_UPD) {
                $query->andWhere(['has_upd' => true]);
            } elseif ($documentType == Documents::DOCUMENT_INVOICE) {
                //
            } else {
                $query->andWhere('1=0');
            }
        }

        if (empty($this->period)) {
            $this->period = StatisticPeriod::getDefaultPeriod();
        }

        $query->andWhere(['between', Invoice::tableName() . '.document_date', $this->period['from'], $this->period['to']]);

        $this->byNumber = trim($this->byNumber);
        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', $this->byNumber);
            foreach ($paramArray as $param) {
                $query->andFilterWhere([
                    'or',
                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                ]);
            }
        }
        $query->andFilterWhere(['invoice.contractor_id' => $this->contractor_id,]);

        return $dataProvider;
    }

    /**
     * @param $type
     * @return array
     */
    public function getContractorItems()
    {
        $query = Contractor::getSorted()
            ->innerJoin(Invoice::tableName(), '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]')
            ->select([
                'contractor_name' => 'IF(
                    {{company_type}}.[[id]] IS NULl,
                    {{contractor}}.[[name]],
                    CONCAT({{company_type}}.[[name_short]], " ", {{contractor}}.[[name]])
                )',
                'contractor.id',
            ])
            ->andWhere([
                'invoice.company_id' => Yii::$app->user->identity->company->id,
                'invoice.type' => $this->type,
                'invoice.is_deleted' => false,
            ])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->groupBy('contractor.id')
            ->indexBy('id');

        return (['' => 'Все'] + [self::CONTRACTOR_DUPLICATE => 'Показать дубли'] + $query->column());
    }
}
