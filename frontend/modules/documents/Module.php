<?php

namespace frontend\modules\documents;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\documents\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
