<?php

namespace frontend\modules\documents\forms;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\Cashbox;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashFactory;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrdersReasonsTypes;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * Class PaymentForm
 *
 * @property Invoice invoice
 *
 * @package frontend\modules\documents\forms
 */
class InvoiceFlowForm extends Model
{
    /**
     * The name of the default scenario.
     */
    const SCENARIO_API_PAID = 'api_paid';

    /**
     * @var boolean
     */
    public $amount;

    public $date_pay;
    /**
     * @var boolean
     */
    public $flowType;
    /**
     * @var boolean
     */
    public $isAccounting = 1;
    /**
     * @var boolean
     */
    public $createNew;
    /**
     * @var array
     */
    public $selectedFlows;
    /**
     * @var integer
     */
    public $selectedFlowsAmount = 0;
    /**
     * @var integer
     */
    public $cashbox_id;
    /**
     * @var integer
     */
    public $emoney_id;
    /**
     * @var integer
     */
    public $project_id;
    /**
     * @var array
     */
    protected $_cashboxList;

    /**
     * @var array
     */
    protected $_emoneyList;

    /**
     * @var Invoice
     */
    protected $_invoice;

    /**
     * @var Employee
     */
    protected $_employee;

    /**
     * @var array 
     */
    public $cashboxListIsAccounting;

    /**
     * @var array
     */
    public $emoneyListIsAccounting;

    /**
     * @param array $config
     * @throws NotFoundHttpException
     */
    public function init()
    {
        parent::init();

        if ($this->invoice === null) {
            throw new NotFoundHttpException('Счёт не найден.');
        }

        $this->cashbox_id = key($this->cashboxList);
        $this->emoney_id = key($this->emoneyList);
        $this->amount = $this->invoice->getAvailablePaymentAmount();
        $this->project_id = $this->invoice->project_id;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'amount',
                'flowType',
                'date_pay',
                'isAccounting',
                'createNew',
                'cashbox_id',
                'emoney_id',
                'selectedFlows',
                'project_id',
            ],
            self::SCENARIO_API_PAID => [
                'amount',
                'flowType',
                'date_pay',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'На сумму',
            'flowType' => 'Движение',
            'isAccounting' => 'Учитывать в бухгалтерии',
            'date_pay' => 'Дата оплаты',
            'createNew' => 'Создать новую операцию',
            'cashbox_id' => 'Через кассу',
            'emoney_id' => 'Через E-money',
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'date_pay'], 'trim',],
            [['amount', 'flowType', 'date_pay'], 'required',],
            [['date_pay'], 'date', 'format' => 'dd.MM.yyyy'],
            [['amount'], function ($attribute) {
                /* @var InvoiceFlowForm $this */
                if (!$this->invoice->invoiceStatus->paymentAllowed($this->invoice->type)) {
                    $this->addError('amount', 'Внесение оплаты запрещено.');
                }
            }],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100,
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum']) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['flowType'], 'in', 'range' => [
                CashFactory::TYPE_BANK,
                CashFactory::TYPE_ORDER,
                CashFactory::TYPE_EMONEY,
            ],],
            [['isAccounting'], 'boolean', 'when' => function (InvoiceFlowForm $model) {
                return $model->flowType != CashFactory::TYPE_BANK;
            },],
            [['createNew'], 'boolean', 'when' => function (InvoiceFlowForm $model) {
                return $model->flowType == CashFactory::TYPE_BANK;
            },],
            [
                ['selectedFlows'], 'each', 'rule' => ['in', 'range' => $this->invoice->getPossibleBankFlows()->column()],
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_BANK && $model->selectedFlows;
                },
            ],
            [
                ['selectedFlows'], 'each', 'rule' => ['in', 'range' => $this->invoice->getPossibleOrderFlows()->column()],
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_ORDER && $model->selectedFlows;
                },
            ],
            [
                ['selectedFlows'], 'each', 'rule' => ['in', 'range' => $this->invoice->getPossibleEmoneyFlows()->column()],
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_EMONEY && $model->selectedFlows;
                },
            ],
            [
                ['createNew'], 'compare', 'compareValue' => '1',
                'when' => function (InvoiceFlowForm $model) {
                    return empty($model->selectedFlows);
                },
                'message' => 'Выберите операцию или создайте новую.',
            ],
            [
                ['cashbox_id'], 'required',
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_ORDER;
                },
            ],
            [
                ['cashbox_id'], 'in',
                'range' => array_keys($this->cashboxList),
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_ORDER;
                },
            ],
            [
                ['emoney_id'], 'required',
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_EMONEY;
                },
            ],
            [
                ['emoney_id'], 'in',
                'range' => array_keys($this->emoneyList),
                'when' => function (InvoiceFlowForm $model) {
                    return $model->flowType == CashFactory::TYPE_EMONEY;
                },
            ],
        ];
    }

    /**
     *
     */
    public function beforeValidate()
    {
        $this->amount = round(TextHelper::parseMoneyInput($this->amount) * 100);

        return parent::beforeValidate();
    }


    /**
     * Sends invoices to participants
     *
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function save()
    {
        if (!intval($this->createNew)) {
            switch ($this->flowType) {
                case CashFactory::TYPE_BANK:
                    $flowModelArray = $this->invoice->getPossibleBankFlows()->andWhere(['flow.id' => $this->selectedFlows])->all();
                    break;
                case CashFactory::TYPE_ORDER:
                    $flowModelArray = $this->invoice->getPossibleOrderFlows()->andWhere(['flow.id' => $this->selectedFlows])->all();
                    break;
                case CashFactory::TYPE_EMONEY:
                    $flowModelArray = $this->invoice->getPossibleEmoneyFlows()->andWhere(['flow.id' => $this->selectedFlows])->all();
                    break;
                default:
                    $flowModelArray = [];
                    break;
            }
        } else {
            $flowModelArray = [$this->createFlow()];
        }

        $useTransaction = LogHelper::$useTransaction;
        LogHelper::$useTransaction = false;

        $save = Yii::$app->db->transaction(function ($db) use ($flowModelArray) {
            foreach ($flowModelArray as $flowModel) {
                if (!$flowModel->isNewRecord || LogHelper::save($flowModel, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                    \common\models\company\CompanyFirstEvent::checkEvent($this->invoice->company, 15);

                    if (!$flowModel->linkInvoice($this->invoice, $this->amount)) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if ($flowModel->hasErrors()) {
                    \common\components\helpers\ModelHelper::logErrors($flowModel, __METHOD__);
                    $db->transaction->rollBack();

                    return false;
                }

            }

            return true;
        });

        LogHelper::$useTransaction = $useTransaction;

        return $save;
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice)
    {
        $this->_invoice = $invoice;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->_employee = $employee;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        if ($this->_employee === null) {
            if (Yii::$app->user->identity instanceof Employee) {
                $this->_employee = Yii::$app->user->identity;
            } else { //if method called from backend
                $this->_employee = $this->invoice->company->employeeChief;
                if ($this->_employee) {
                    $this->_employee->setCompany($this->invoice->company->id);
                }
            }

        }
        return $this->_employee;
    }

    /**
     * @return array
     */
    public function getCashboxList()
    {
        if ($this->_cashboxList === null) {
            $_cashboxes = $this->employee->getCashboxes()->select(['id', 'name', 'is_accounting'])->orderBy([
                'is_main' => SORT_DESC,
                'IF([[responsible_employee_id]] = :uid, 0, 1)' => SORT_ASC,
            ])->params([':uid' => $this->employee->id])->asArray()->all();

            $this->_cashboxList = ArrayHelper::map($_cashboxes, 'id', 'name');
            $this->cashboxListIsAccounting = ArrayHelper::map($_cashboxes, 'id', 'is_accounting');
        }
        return $this->_cashboxList;
    }

    /**
     * @return array
     */
    public function getEmoneyList()
    {
        if ($this->_emoneyList === null) {
            $_emoneys = $this->invoice->company->getEmoneys()->select(['id', 'name', 'is_accounting'])->andWhere([
                'is_closed' => false,
            ])->orderBy([
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])->asArray()->all();

            $this->_emoneyList = ArrayHelper::map($_emoneys, 'id', 'name');
            $this->emoneyListIsAccounting = ArrayHelper::map($_emoneys, 'id', 'is_accounting');
        }
        return $this->_emoneyList;
    }

    /**
     * @param Employee|null $user
     * @return CashFlowsBase|CashOrderFlows
     * @throws NotFoundHttpException
     */
    public function createFlow()
    {
        $company_id = $this->invoice->company_id;

        /* @var CashFlowsBase $flowClass */
        $flowClass = CashFactory::getCashClass($this->flowType);
        /* @var CashFlowsBase $flowModel */
        $flowModel = new $flowClass();
        $flowModel->company_id = $company_id;
        $flowModel->contractor_id = $this->invoice->contractor_id;

        $flowModel->date = $this->date_pay ? $this->date_pay : date(DateHelper::FORMAT_USER_DATE);
        $flowModel->description =
            'Оплата счета №' . $this->invoice->fullNumber
            . ' от '
            . DateHelper::format($this->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $flowModel->flow_type = CashFactory::$documentToFlowType[$this->invoice->type];
        $flowModel->expenditure_item_id = $this->invoice->invoice_expenditure_item_id;

        if ($flowModel instanceof CashBankFlows) {
            $flowModel->rs = $this->invoice->company->mainAccountant->rs;
            $flowModel->bank_name = $this->invoice->company->mainAccountant->bank_name;
        }

        $flowModel->income_item_id = ($flowModel->flow_type == CashFlowsBase::FLOW_TYPE_INCOME)?
            InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER:
            null;

        $flowModel->amount = $this->amount / 100; // cash flow takes float on input.

        if ($flowModel->hasAttribute('is_accounting')) {
            $flowModel->is_accounting = $this->isAccounting;
        }

        if ($flowModel instanceof CashOrderFlows) {
            /* @var CashOrderFlows $flowModel */
            $flowModel->author_id = $this->employee->id;
            $flowModel->cashbox_id = $this->cashbox_id;
            $flowModel->number = CashOrderFlows::getNextNumber($company_id, $flowModel->flow_type, $flowModel->date);
            $flowModel->reason_id = $this->flowType == CashFlowsBase::FLOW_TYPE_INCOME
                ? CashOrdersReasonsTypes::VALUE_PROCEEDS : CashOrdersReasonsTypes::VALUE_OTHER;

            if ($flowModel->reason_id == CashOrdersReasonsTypes::VALUE_OTHER) {
                $flowModel->other = $flowModel->description;
            }
        }

        if ($flowModel instanceof CashEmoneyFlows) {
            $flowModel->emoney_id = $this->emoney_id;
        }

        if (isset($this->invoice->project_id)) {
            $flowModel->project_id = $this->invoice->project_id;
        }

        return $flowModel;
    }
}
