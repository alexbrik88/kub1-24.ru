<?php

namespace frontend\modules\documents\forms;


use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\document\Act;
use common\models\document\EmailFile;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\GoodsCancellation;
use common\models\document\GoodsCancellationStatus;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\OrderDocument;
use common\models\document\Proxy;
use common\models\document\SalesInvoice;
use common\models\document\status\AgentReportStatus;
use common\models\document\status\ProxyStatus;
use common\models\document\status\SalesInvoiceStatus;
use common\models\document\Upd;
use common\models\Agreement;
use common\models\document\Waybill;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\status\AgreementStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\AgentReport;
use common\models\driver\Driver;
use common\models\document\status\WaybillStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\product\PriceList;
use common\models\product\PriceListNotification;
use common\models\product\PriceListStatus;
use frontend\models\Documents;
use frontend\models\log\LogDocumentEmail;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\crm\models\Client;
use Yii;
use yii\base\Model;
use common\models\vehicle\Vehicle;
use common\models\logisticsRequest\LogisticsRequest;

/**
 * Class InvoiceSendForm
 *
 * @property Invoice|Act|GoodsCancellation model
 *
 * @package frontend\modules\documents\forms
 */
class InvoiceSendForm extends Model
{
    /**
     * @var boolean
     */
    public $sendToChief;
    /**
     * @var boolean
     */
    public $sendToChiefAccountant;
    /**
     * @var boolean
     */
    public $sendToContact;

    /**
     * @var boolean
     */
    public $sendToOther;
    /**
     * @var string
     */
    public $otherEmail;
    /**
     * @var string
     */
    public $chiefEmail = null;
    /**
     * @var string
     */
    public $chiefAccountantEmail = null;
    /**
     * @var null
     */
    public $contactAccountantEmail = null;

    public $textRequired = false;
    public $emailText;
    public $sendFrom;

    public $sendToAll = null;
    public $subject = null;
    public $sendEmailFiles = null;

    protected $_employeeCompany;
    protected $_emailArray;
    protected $_contractor = false;

    /**
     * @var Invoice|Act|PackingList
     */
    private $_model;

    /**
     * InvoiceSendForm constructor.
     * @param EmployeeCompany $employeeCompany
     * @param array $config
     */
    public function __construct(EmployeeCompany $employeeCompany, $config = [])
    {
        $this->_employeeCompany = $employeeCompany;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emailText'], 'trim'],
            [['sendTo'], 'required', 'message' => 'Нужно добавить получателя.'],
            ['sendTo', function ($attribute, $params) {
                $validator = new yii\validators\EmailValidator;
                $emailArray = $this->emailList;

                foreach ($emailArray as $email) {
                    $validator->validate($email) ?: $this->addError($attribute, "Значение «Кому» не является правильным email адресом.");
                }
            }],
            [['emailText', 'subject', 'sendEmailFiles'], 'string'],
            [
                ['emailText'], 'required', 'when' => function (InvoiceSendForm $model) {
                return (bool)$model->textRequired;
            },
                'enableClientValidation' => false,
            ],
            [['sendToChief', 'sendToChiefAccountant', 'sendToContact', 'sendToOther'], 'boolean'],
            [['subject'], 'required', 'message' => 'Необходимо заполнить.',],
            [
                ['otherEmail'], 'required', 'when' => function (InvoiceSendForm $model) {
                return (bool)$model->sendToOther;
            }, 'enableClientValidation' => false,
            ],
            [
                ['chiefEmail'], 'required', 'when' => function (InvoiceSendForm $model) {
                    return (bool)($model->sendToChief && $this->contractor && empty($this->contractor->director_email));
                },
                'message' => 'Для отправки счета руководителю, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [
                ['chiefAccountantEmail'], 'required', 'when' => function (InvoiceSendForm $model) {
                    return (bool)($model->sendToChiefAccountant && $this->contractor && empty($this->contractor->chief_accountant_email));
                },
                'message' => 'Для отправки счета главному бухгалтеру, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [
                ['contactAccountantEmail'], 'required', 'when' => function (InvoiceSendForm $model) {
                    return (bool)($model->sendToContact && $this->contractor && empty($this->contractor->contact_email));
                },
                'message' => 'Для отправки счета контакту, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [['chiefEmail', 'chiefAccountantEmail', 'otherEmail', 'contactAccountantEmail'], 'email'],
        ];
    }

    /**
     * @return array
     */
    protected function getContractor()
    {
        if ($this->_contractor === false) {
            $this->_contractor = null;
            if (isset($this->model->contractor)) {
                $this->_contractor = $this->model->contractor;
            } elseif (isset($this->model->invoice)) {
                $this->_contractor = $this->model->invoice->contractor;
            } elseif ($this->model instanceof AgentReport) {
                $this->_contractor = $this->model->agent;
            }
        }

        return $this->_contractor;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'sendToChief' => 'Отправить руководителю',
            'sendToChiefAccountant' => 'Отправить главному бухгалтеру',
            'sendToContact' => 'Отправить контактному лицу',
            'sendToOther' => 'Другому',
            'chiefEmail' => 'e-mail',
            'chiefAccountantEmail' => 'e-mail',
            'otherEmail' => 'e-mail',
            'emailText' => 'Текст письма',
            'subject' => 'Тема',
            'sendTo' => 'Кому',
            'sendFrom' => 'От кого',
        ];
    }

    /**
     * @inheritdoc
     */
    public function setSendTo($value)
    {
        $value = is_string($value) ? preg_split('/\s*,\s*/', $value, -1, PREG_SPLIT_NO_EMPTY) : [];

        $this->_emailArray = array_unique($value);
    }

    /**
     * @return string
     */
    public function getSendTo()
    {
        return is_array($this->_emailArray) ? implode(',', $this->_emailArray) : '';
    }

    /**
     * @return array|bool
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function send(Employee $user = null)
    {
        /** @var EmployeeCompany $sender */
        $sender = $this->employeeCompany;

        $event = null;
        $saved = false;
        $savedCount = 0;
        $emailArray = $this->getEmailList();
        $totalCount = count($emailArray);
        $attachFiles = $this->getAttachFiles();

        if ($this->model instanceof Invoice) {
            foreach ($emailArray as $email) {
                if ($this->model->sendAsEmail($sender, $email, $this->emailText, $this->subject, null, null, $attachFiles, true)) {
                    $savedCount++;
                    LogDocumentEmail::saveEmail($email, $this->company->id, $this->model->id, get_class($this->model));
                }
            }
            $this->clearAttachFiles();

            return $savedCount ? [$savedCount, $totalCount] : false;
        } elseif ($this->model instanceof ForeignCurrencyInvoice) {
            foreach ($emailArray as $email) {
                if ($this->model->sendAsEmail($sender, $email, $this->emailText, $this->subject, null, null, $attachFiles, true)) {
                    $savedCount++;
                    LogDocumentEmail::saveEmail($email, $this->company->id, $this->model->id, get_class($this->model));
                }
            }
            $this->clearAttachFiles();

            return $savedCount ? [$savedCount, $totalCount] : false;
        } elseif ($this->model instanceof Act) {
            foreach ($emailArray as $email) {
                if ($this->model->sendAsEmail($sender, $email, $this->emailText, $this->subject, $attachFiles, true)) {
                    $savedCount++;
                    LogDocumentEmail::saveEmail($email, $this->company->id, $this->model->id, get_class($this->model));
                }
            }
            $this->clearAttachFiles();

            return $savedCount ? [$savedCount, $totalCount] : false;
        } elseif ($this->model instanceof PackingList) {
            $event = 24;
            $saved = self::setPackingListSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof SalesInvoice) {
            $event = 24;
            $saved = self::setSalesInvoiceSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof Proxy) {
            $event = null;
            $saved = self::setProxySendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof InvoiceFacture) {
            $event = 29;
            $saved = self::setInvoiceFactureSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof Upd) {
            $event = 34;
            $saved = self::setUpdSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof PriceList) {
            $event = null;
            $saved = true;
            //$saved = self::setPriceListSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof Agreement) {
            $saved = $saved = self::setAgreementSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof AgentReport) {
            $saved = self::setAgentReportSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof OrderDocument) {
            $saved = self::setOrderDocumentSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof Client) {
            $saved = true;
        } elseif ($this->model instanceof Driver) {
            $saved = true;
        } elseif ($this->model instanceof Vehicle) {
            $saved = true;
        } elseif ($this->model instanceof LogisticsRequest) {
            $saved = true;
            $requestType = Yii::$app->request->get('type');
        } elseif ($this->model instanceof Waybill) {
            $event = null;
            $saved = self::setWaybillSendStatus($this->model, $this->employee);
        } elseif ($this->model instanceof GoodsCancellation) {
            $event = null;
            $saved = self::setGoodsCancellationSendStatus($this->model, $this->employee);
        }

        if ($saved) {
            $emailParams = $this->getMessageParams();
            $params = [
                'model' => $this->model,
                'employeeCompany' => $sender,
                'employee' => $sender->employee,
                'subject' => $this->subject,
                'emailText' => $this->emailText,
                'pixel' => [
                    'company_id' => $sender->company_id,
                    'email' => implode(',', $emailArray),
                ],
            ];
            if (isset($requestType)) {
                $params['requestType'] = $requestType;
            }

            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose($emailParams['emailMessagePath'], $params)
                ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
                ->setSubject($this->subject);

            if ($attachFiles) {
                foreach ($attachFiles as $file) {
                    if ($file['is_file']) {
                        if (exif_imagetype($file['content']) === false) {
                            $message->attach($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        } else {
                            $message->embed($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    } else {
                        $message->attachContent($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                }
            }

            foreach ($emailArray as $email) {
                if ($message->setTo($email)->send()) {
                    $savedCount++;
                    LogDocumentEmail::saveEmail($email, $this->company->id, $this->model->id, get_class($this->model));
                }
            }
            $this->clearAttachFiles();
        }

        if ($saved) {
            if ($savedCount && $event) {
                \common\models\company\CompanyFirstEvent::checkEvent($this->_employeeCompany->company, $event);
            }

            return [$savedCount, $totalCount,];
        } else {
            return $saved;
        }
    }

    /**
     * @return array|bool
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendPriceList()
    {
        /** @var EmployeeCompany $sender */
        $sender = $this->employeeCompany;

        $event = null;
        $saved = false;
        $savedCount = 0;
        $emailArray = $this->getEmailList();
        $totalCount = count($emailArray);
        $attachFiles = $this->getAttachFiles();

        if ($this->model instanceof PriceList) {
            $event = null;
            $saved = true;
        }

        if ($saved) {

            $params = [
                'model' => $this->model,
                'employeeCompany' => $sender,
                'employee' => $sender->employee,
                'subject' => $this->subject,
                'emailText' => $this->emailText,
                'pixel' => [
                    'company_id' => $sender->company_id,
                    'email' => implode(',', $emailArray),
                ],
            ];

            foreach ($emailArray as $email) {

                $params['sendTo'] = $email;

                $emailParams = $this->getMessageParams();

                if (isset($requestType)) {
                    $params['requestType'] = $requestType;
                }

                Yii::$app->mailer->htmlLayout = 'layouts/document-html';
                $message = Yii::$app->mailer->compose($emailParams['emailMessagePath'], $params)
                    ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                    ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
                    ->setSubject($this->subject);

                if ($attachFiles) {
                    foreach ($attachFiles as $file) {
                        if ($file['is_file']) {
                            if (exif_imagetype($file['content']) === false) {
                                $message->attach($file['content'], [
                                    'fileName' => $file['fileName'],
                                    'contentType' => $file['contentType'],
                                ]);
                            } else {
                                $message->embed($file['content'], [
                                    'fileName' => $file['fileName'],
                                    'contentType' => $file['contentType'],
                                ]);
                            }
                        } else {
                            $message->attachContent($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    }
                }

                if ($message->setTo($email)->send()) {
                    $savedCount++;
                    LogDocumentEmail::saveEmail($email, $this->company->id, $this->model->id, get_class($this->model));
                }

                $this->clearAttachFiles();
            }
        }

        if ($saved) {
            if ($savedCount && $event) {
                \common\models\company\CompanyFirstEvent::checkEvent($this->_employeeCompany->company, $event);
            }

            return [$savedCount, $totalCount,];
        } else {
            return $saved;
        }
    }

    /**
     * @return array
     */
    public function getMessageParams()
    {
        $emailMessagePath = null;
        $subject = null;

        if ($this->model instanceof Invoice) {
            $emailMessagePath = [
                'html' => 'system/documents/invoice-out/html',
                'text' => 'system/documents/invoice-out/text',
            ];
        } elseif ($this->model instanceof Act) {
            $emailMessagePath = [
                'html' => 'system/documents/act-out/html',
                'text' => 'system/documents/act-out/text',
            ];
        } elseif ($this->model instanceof PackingList) {
            $emailMessagePath = [
                'html' => 'system/documents/packing-list-out/html',
                'text' => 'system/documents/packing-list-out/text',
            ];
        } elseif ($this->model instanceof SalesInvoice) {
            $emailMessagePath = [
                'html' => 'system/documents/sales-invoice/html',
                'text' => 'system/documents/sales-invoice/text',
            ];
        } elseif ($this->model instanceof InvoiceFacture) {
            $emailMessagePath = [
                'html' => 'system/documents/invoice-facture-out/html',
                'text' => 'system/documents/invoice-facture-out/text',
            ];
        } elseif ($this->model instanceof Upd) {
            $emailMessagePath = [
                'html' => 'system/documents/upd-out/html',
                'text' => 'system/documents/upd-out/text',
            ];
        } elseif ($this->model instanceof Agreement) {
            $emailMessagePath = [
                'html' => 'system/documents/agreement-out/html',
                'text' => 'system/documents/agreement-out/text',
            ];
        } elseif ($this->model instanceof PriceList) {
            $emailMessagePath = [
                'html' => 'system/price-list/html',
                'text' => 'system/price-list/text',
            ];
        } elseif ($this->model instanceof Proxy) {
            $emailMessagePath = [
                'html' => 'system/documents/proxy-in/html',
                'text' => 'system/documents/proxy-in/text',
            ];
        } elseif ($this->model instanceof AgentReport) {
            $emailMessagePath = [
                'html' => 'system/agent-report/html',
                'text' => 'system/agent-report/text',
            ];
        } elseif ($this->model instanceof OrderDocument) {
            $emailMessagePath = [
                'html' => 'system/documents/order-document/html',
                'text' => 'system/documents/order-document/text',
            ];
        } elseif ($this->model instanceof Client) {
            $emailMessagePath = [
                'html' => 'system/crm/message/html',
                'text' => 'system/crm/message/text',
            ];
        } elseif ($this->model instanceof Driver) {
            $emailMessagePath = [
                'html' => 'system/logistics/driver/html',
                'text' => 'system/logistics/driver/text',
            ];
        } elseif ($this->model instanceof Vehicle) {
            $emailMessagePath = [
                'html' => 'system/logistics/vehicle/html',
                'text' => 'system/logistics/vehicle/text',
            ];
        } elseif ($this->model instanceof LogisticsRequest) {
            $emailMessagePath = [
                'html' => 'system/logistics/request/html',
                'text' => 'system/logistics/request/text',
            ];
        } elseif ($this->model instanceof Waybill) {
            $emailMessagePath = [
                'html' => 'system/documents/waybill-out/html',
                'text' => 'system/documents/waybill-out/text',
            ];
        } elseif ($this->model instanceof GoodsCancellation) {
            $emailMessagePath = [
                'html' => 'system/documents/goods-cancellation/html',
                'text' => 'system/documents/goods-cancellation/text',
            ];
        }

        return [
            'emailMessagePath' => $emailMessagePath,
            'subject' => $subject,
        ];
    }

    /**
     * Return array of emails.
     * @return array
     */
    public function getEmailList()
    {
        return is_array($this->_emailArray) ? $this->_emailArray : [];
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function getAttachFiles()
    {
        $attachFiles = [];
        if ($this->sendEmailFiles) {
            $emailFilesID = explode(', ', $this->sendEmailFiles);
            foreach ($emailFilesID as $emailFileID) {
                /* @var $emailFile EmailFile */
                $emailFile = EmailFile::findOne($emailFileID);
                if ($emailFile) {
                    if ($emailFile->type === null) {
                        $attachFiles[] = [
                            'content' => $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name,
                            'fileName' => $emailFile->file_name,
                            'contentType' => $emailFile->mime,
                            'is_file' => true,
                        ];
                    } else {
                        switch ($emailFile->type) {
                            case EmailFile::TYPE_INVOICE_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => Invoice::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING, null)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_FOREIGN_CURRENCY_INVOICE:
                                $attachFiles[] = [
                                    'content' => ForeignCurrencyInvoice::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING, null)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_ACT_DOCUMENT:
                                $act = $emailFile->act_id ? $emailFile->act : $this->model;
                                $attachFiles[] = [
                                    'content' => Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $act->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_PACKING_LIST_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => PackingList::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_WAYBILL_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => Waybill::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_INVOICE_FACTURE_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => InvoiceFacture::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_UPD_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => Upd::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_AGREEMENT_DOCUMENT:
                                $attachFiles[] = [
                                    'content' => Agreement::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_PROXY:
                                $attachFiles[] = [
                                    'content' => Proxy::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_AGENT_REPORT:
                                $attachFiles[] = [
                                    'content' => AgentReport::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                            case EmailFile::TYPE_ONE_C_FILE:
                                $oneCFilePath = $this->model->generateOneCFile();
                                if ($oneCFilePath) {
                                    $attachFiles[] = [
                                        'content' => $oneCFilePath,
                                        'fileName' => 'Для загрузки в 1С ' . $this->model->getOneCFileName(),
                                        'contentType' => 'application/xml',
                                        'is_file' => true,
                                    ];
                                }
                                break;
                            case EmailFile::TYPE_INVOICE_PAYMENT_ORDER:
                                if ($this->model->contractor_bik && $this->model->contractor_rs) {
                                    $name = ($this->model->is_invoice_contract && $this->model->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
                                    $attachFiles[] = [
                                        'content' => Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                                            'model' => $this->model,
                                        ]),
                                        'fileName' => 'Платежное_поручение_для_' . $name . '_№' .
                                            mb_ereg_replace("([^\w\s\d\-_])", '', $this->model->fullNumber) . '.txt',
                                        'contentType' => 'text/plain',
                                        'is_file' => false,
                                    ];
                                }
                                break;
                            case EmailFile::TYPE_GOODS_CANCELLATION:
                                $attachFiles[] = [
                                    'content' => GoodsCancellation::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
                                    'fileName' => str_replace(['\\', '/'], '_', $this->model->pdfFileName),
                                    'contentType' => 'application/pdf',
                                    'is_file' => false,
                                ];
                                break;
                        }
                    }
                }
            }
        }

        return $attachFiles;
    }

    /**
     * @throws \yii\db\StaleObjectException
     */
    public function clearAttachFiles()
    {
        if ($this->sendEmailFiles) {
            $emailFilesID = explode(', ', $this->sendEmailFiles);
            foreach ($emailFilesID as $emailFileID) {
                /* @var $emailFile EmailFile */
                $emailFile = EmailFile::findOne($emailFileID);
                if ($emailFile) {
                    $emailFile->_delete();
                }
            }
        }
    }


    /**
     * @return Invoice|Act|PackingList
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * @param Invoice|Act|PackingList $model
     */
    public function setModel($model)
    {
        $this->_model = $model;
    }

    /**
     * @param Invoice $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setInvoiceSendStatus(Invoice $model, Employee $user = null)
    {
        if (in_array($model->invoice_status_id, [InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_SEND])) {
            $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Invoice $model) use ($user) {
                $model->invoice_status_id = InvoiceStatus::STATUS_SEND;
                $model->invoice_status_updated_at = time();
                $model->invoice_status_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
            });
        } else {
            $currentStatus = $model->invoice_status_id;
            $model->invoice_status_id = InvoiceStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->invoice_status_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param Act $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setActSendStatus(Act $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [ActStatus::STATUS_CREATED, ActStatus::STATUS_PRINTED, ActStatus::STATUS_SEND]) || !$model->getIsWasSent()) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = ActStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = ActStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param Agreement $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setAgreementSendStatus(Agreement $model, Employee $user = null)
    {
        if (in_array($model->status_id, [AgreementStatus::STATUS_CREATED, AgreementStatus::STATUS_PRINTED, AgreementStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_id = AgreementStatus::STATUS_SEND;
                $model->status_updated_at = time();
                $model->status_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_id', 'status_updated_at', 'status_author_id']);
            });
        } else {
            $currentStatus = $model->status_id;
            $model->status_id = AgreementStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param PackingList $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setPackingListSendStatus(PackingList $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [PackingListStatus::STATUS_CREATED, PackingListStatus::STATUS_PRINTED, PackingListStatus::STATUS_SEND]) || !$model->getIsWasSent()) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = PackingListStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }

        return true;
    }

    /**
     * @param Waybill $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setWaybillSendStatus(Waybill $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [WaybillStatus::STATUS_CREATED, WaybillStatus::STATUS_PRINTED, WaybillStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = WaybillStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = PackingListStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param SalesInvoice $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setSalesInvoiceSendStatus(SalesInvoice $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [SalesInvoiceStatus::STATUS_CREATED, SalesInvoiceStatus::STATUS_PRINTED, SalesInvoiceStatus::STATUS_SEND]) || !$model->getIsWasSent()) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = SalesInvoiceStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = SalesInvoiceStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param Proxy $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setProxySendStatus(Proxy $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [ProxyStatus::STATUS_CREATED, ProxyStatus::STATUS_PRINTED, ProxyStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = ProxyStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = ProxyStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param Waybill $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setPriceListSendStatus(PriceList $model, Employee $user = null)
    {
        if (in_array($model->status_id, [PriceListStatus::STATUS_CREATED, PriceListStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_id = PriceListStatus::STATUS_SEND;
                $model->status_updated_at = time();
                $model->status_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_id', 'status_updated_at', 'status_author_id']);
            });
        } else {
            $currentStatus = $model->status_id;
            $model->status_id = PriceListStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param InvoiceFacture $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setInvoiceFactureSendStatus(InvoiceFacture $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [InvoiceFactureStatus::STATUS_CREATED, InvoiceFactureStatus::STATUS_PRINTED, InvoiceFactureStatus::STATUS_DELIVERED])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = InvoiceFactureStatus::STATUS_DELIVERED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = InvoiceFactureStatus::STATUS_DELIVERED;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param Upd $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setUpdSendStatus(Upd $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [UpdStatus::STATUS_CREATED, UpdStatus::STATUS_PRINTED, UpdStatus::STATUS_SEND]) || !$model->getIsWasSent()) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = UpdStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = UpdStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param AgentReport $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setAgentReportSendStatus(AgentReport $model, Employee $user = null)
    {
        if (in_array($model->status_out_id, [AgentReportStatus::STATUS_CREATED, AgentReportStatus::STATUS_PRINTED, AgentReportStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_out_id = AgentReportStatus::STATUS_SEND;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = $user ? $user->id : \Yii::$app->user->id;

                return $model->save(false, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        } else {
            $currentStatus = $model->status_out_id;
            $model->status_out_id = AgentReportStatus::STATUS_SEND;
            LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $model->status_out_id = $currentStatus;
        }

        return true;
    }

    /**
     * @param OrderDocument $model
     * @param Employee|null $user
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function setOrderDocumentSendStatus(OrderDocument $model, Employee $user = null)
    {
        return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (OrderDocument $model) use ($user) {
            $model->status_id = OrderDocumentStatus::STATUS_SEND;
            $model->status_updated_at = time();
            $model->status_author_id = $user ? $user->id : Yii::$app->user->id;

            return $model->save(false, ['status_id', 'status_updated_at', 'status_author_id']);
        });
    }

    /**
     * @param Waybill $model
     * @param Employee|null $user
     * @return bool
     * @throws \Exception
     */
    public static function setGoodsCancellationSendStatus(GoodsCancellation $model, Employee $user = null)
    {
        if (in_array($model->status_id, [GoodsCancellationStatus::STATUS_CREATED, GoodsCancellationStatus::STATUS_PRINTED, GoodsCancellationStatus::STATUS_SEND])) {
            return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($user) {
                $model->status_id = GoodsCancellationStatus::STATUS_SEND;
                return $model->save(false, ['status_id']);
            });
        }

        return true;
    }

    /**
     * @return EmployeeCompany
     */
    public function getEmployeeCompany()
    {
        return $this->_employeeCompany;
    }

    /**
     * @return null|Company
     */
    public function getCompany()
    {
        return $this->employeeCompany ? $this->employeeCompany->company : null;
    }

    /**
     * @return null|Employee
     */
    public function getEmployee()
    {
        return $this->employeeCompany ? $this->employeeCompany->employee : null;
    }


    /**
     * @param array $models
     * @param array $attachPdfByModel
     * @return array|bool
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendManyInOne(array $models, $sendWithDocs = false)
    {
        /** @var EmployeeCompany $sender */
        $sender = $this->employeeCompany;

        $event = null;
        $saved = false;
        $savedCount = 0;
        $emailArray = $this->getEmailList();
        $totalCount = count($emailArray);
        $attachFiles = $this->getAttachFiles();

        if ($this->model instanceof Invoice) {
            $event = 11;
            foreach ($models as $model) {
                $saved = self::setInvoiceSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof Act) {
            $event = 19;
            foreach ($models as $model) {
                $saved = self::setActSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof PackingList) {
            $event = 24;
            foreach ($models as $model) {
                $saved = self::setPackingListSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof Proxy) {
            $event = null;
            foreach ($models as $model) {
                $saved = self::setProxySendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof InvoiceFacture) {
            $event = 29;
            foreach ($models as $model) {
                $saved = self::setInvoiceFactureSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof Upd) {
            $event = 34;
            foreach ($models as $model) {
                $saved = self::setUpdSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof PriceList) {
            $saved = true;
        } elseif ($this->model instanceof Agreement) {
            foreach ($models as $model) {
                $saved = self::setAgreementSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof Waybill) {
            $event = null;
            foreach ($models as $model) {
                $saved = self::setWaybillSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof Driver) {
            $saved = true;
        } elseif ($this->model instanceof Vehicle) {
            $saved = true;
        } elseif ($this->model instanceof AgentReport) {
            foreach ($models as $model) {
                $saved = self::setAgentReportSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof OrderDocument) {
            foreach ($models as $model) {
                $saved = self::setOrderDocumentSendStatus($model, $this->employee);
            }
        } elseif ($this->model instanceof LogisticsRequest) {
            $saved = true;
            $requestType = Yii::$app->request->get('type');
        }

        if ($saved) {
            $messageModels = $models;
            foreach ($models as $model) {
                if ($model instanceof Invoice && $sendWithDocs) {
                    foreach ($model->acts as $doc) {
                        $messageModels[] = $doc;
                    }
                    foreach ($model->packingLists as $doc) {
                        $messageModels[] = $doc;
                    }
                    foreach ($model->invoiceFactures as $doc) {
                        $messageModels[] = $doc;
                    }
                    foreach ($model->upds as $doc) {
                        $messageModels[] = $doc;
                    }
                }
            }
            $emailParams = $this->getManyInOneMessageParams();
            $params = [
                //'model' => $this->model,
                'models' => $messageModels,
                'employeeCompany' => $sender,
                'employee' => $sender->employee,
                'subject' => $this->subject,
                'emailText' => $this->emailText,
                'pixel' => [
                    'company_id' => $sender->company_id,
                    'email' => implode(',', $emailArray),
                ],
            ];
            if (isset($requestType)) {
                $params['requestType'] = $requestType;
            }

            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose($emailParams['emailMessagePath'], $params)
                ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
                ->setSubject($this->subject);

            if ($attachFiles) {
                foreach ($attachFiles as $file) {
                    if ($file['is_file']) {
                        if (exif_imagetype($file['content']) === false) {
                            $message->attach($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        } else {
                            $message->embed($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    } else {
                        $message->attachContent($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                }
            }

            // document self PDF files
            $modelsArr = [];
            foreach ($models as $model) {
                $modelClass = $model::className();
                $message->attachContent($modelClass::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $model->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);

                $modelsArr[] = [
                    'class' => get_class($model),
                    'id' => $model->id
                ];

                if ($model instanceof Invoice && $sendWithDocs) {
                    foreach ($model->acts as $doc) {
                        $message->attachContent(Act::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($model->packingLists as $doc) {
                        $message->attachContent(PackingList::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($model->invoiceFactures as $doc) {
                        $message->attachContent(InvoiceFacture::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($model->upds as $doc) {
                        $message->attachContent(Upd::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                }
            }



            foreach ($emailArray as $email) {
                if ($message->setTo($email)->send()) {
                    $savedCount++;
                    foreach ($modelsArr as $modelArr) {
                        LogDocumentEmail::saveEmail($email, $this->company->id, $modelArr['id'], $modelArr['class']);
                    }
                }
            }
            $this->clearAttachFiles();
        }

        if ($saved) {
            if ($savedCount && $event) {
                \common\models\company\CompanyFirstEvent::checkEvent($this->_employeeCompany->company, $event);
            }

            return [$savedCount, $totalCount,];
        } else {
            return $saved;
        }
    }

    /**
     * @return array
     */
    public function getManyInOneMessageParams()
    {
        $emailMessagePath = null;
        $subject = null;

        if ($this->model instanceof Invoice) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/invoice-out/html',
                'text' => 'system/documents/many-in-one/invoice-out/text',
            ];
        } elseif ($this->model instanceof Act) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/act-out/html',
                'text' => 'system/documents/many-in-one/act-out/text',
            ];
        } elseif ($this->model instanceof PackingList) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/packing-list-out/html',
                'text' => 'system/documents/many-in-one/packing-list-out/text',
            ];
        } elseif ($this->model instanceof InvoiceFacture) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/invoice-facture-out/html',
                'text' => 'system/documents/many-in-one/invoice-facture-out/text',
            ];
        } elseif ($this->model instanceof Upd) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/upd-out/html',
                'text' => 'system/documents/many-in-one/upd-out/text',
            ];
        } elseif ($this->model instanceof Agreement) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/agreement-out/html',
                'text' => 'system/documents/many-in-one/agreement-out/text',
            ];
        } elseif ($this->model instanceof PriceList) {
            $emailMessagePath = [
                'html' => 'system/price-list/html',
                'text' => 'system/price-list/text',
            ];
        } elseif ($this->model instanceof Waybill) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/waybill-out/html',
                'text' => 'system/documents/many-in-one/waybill-out/text',
            ];
        } elseif ($this->model instanceof Driver) {
            $emailMessagePath = [
                'html' => 'system/logistics/driver/html',
                'text' => 'system/logistics/driver/text',
            ];
        } elseif ($this->model instanceof Vehicle) {
            $emailMessagePath = [
                'html' => 'system/logistics/vehicle/html',
                'text' => 'system/logistics/vehicle/text',
            ];
        } elseif ($this->model instanceof LogisticsRequest) {
            $emailMessagePath = [
                'html' => 'system/logistics/request/html',
                'text' => 'system/logistics/request/text',
            ];
        } elseif ($this->model instanceof Proxy) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/proxy-in/html',
                'text' => 'system/documents/many-in-one/proxy-in/text',
            ];
        } elseif ($this->model instanceof AgentReport) {
            $emailMessagePath = [
                'html' => 'system/agent-report/html',
                'text' => 'system/agent-report/text',
            ];
        } elseif ($this->model instanceof OrderDocument) {
            $emailMessagePath = [
                'html' => 'system/documents/many-in-one/order-document/html',
                'text' => 'system/documents/many-in-one/order-document/text',
            ];
        }

        return [
            'emailMessagePath' => $emailMessagePath,
            'subject' => $subject,
        ];
    }
}
