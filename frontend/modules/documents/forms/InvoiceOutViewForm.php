<?php

namespace frontend\modules\documents\forms;

use common\components\validators\PasswordValidator;
use common\models\Company;
use common\models\CompanyHelper;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderHelper;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\LoginForm;
use frontend\models\RegistrationForm;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Class InvoiceOutViewForm
 *
 * @property Employee $employee
 * @property Invoice $invoice
 *
 * @package frontend\modules\documents\forms
 */
class InvoiceOutViewForm extends Model
{
    const SCENARIO_SIGNUP = 'signup';
    const SCENARIO_COMPANY = 'company';
    const SCENARIO_SAVE = 'save';

    public $username;
    public $password;
    public $taxationType = [];
    public $create;

    /**
     * @var Invoice
     */
    protected $_invoice;

    /**
     * @var Employee
     */
    protected $_employee;

    /**
     * Invoice view Url
     * @var string
     */
    protected $_invoiceViewUrl = '';

    public static $taxation = [
        'osno' => 'ОСНО (общая)',
        'usn' => 'УСН (упращенка)',
        'envd' => 'ЕНВД (вмененка)',
        'psn' => 'ПСН (патент)',
    ];

    /**
     * @inheritdoc
     */
    /*public function __construct(Invoice $invoice, Employee $employee = null, $params = [])
    {
        $this->_invoice = $invoice;

        parent::__construct($params);
    }*/

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_SIGNUP => [
                'invoice',
                'username',
                'password',
                'taxationType',
            ],
            self::SCENARIO_COMPANY => [
                'invoice',
                'employee',
                'taxationType',
            ],
            self::SCENARIO_SAVE => [
                'invoice',
                'employee',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['signup', 'company', 'save'], 'safe'],
            [['invoice', 'employee', 'username', 'password'], 'required'],
            [
                ['taxationType'], 'required',
                'message' => 'Необходимо выбрать систему налогобложения.'
            ],
            [
                ['taxationType'], 'each',
                'rule' => ['in', 'range' => array_keys(self::$taxation)],
            ],
            [['username'], 'email'],
            [
                ['username'], 'unique',
                'targetClass' => Employee::class,
                'targetAttribute' => 'email',
                'filter' => ['is_deleted' => false],
            ],
            [['password'], PasswordValidator::className(),],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Email',
            'password' => 'Пароль',
            'taxationType' => 'Система налогобложения',
        ];
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice)
    {
        $this->_invoice = $invoice;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @param Invoice $invoice
     */
    public function setEmployee(Employee $employee)
    {
        $this->_employee = $employee;
    }

    /**
     * @return Invoice
     */
    public function getEmployee()
    {
        return $this->_employee;
    }

    /**
     * Invoice view Url
     * @return string
     */
    public function getInvoiceViewUrl()
    {
        return $this->_invoiceViewUrl;
    }

    /**
     * @return Company
     */
    public function createCompany(Employee $employee)
    {
        if ($employee->isNewRecord && !$employee->save(false)) {
            return null;
        }

        $company = RegistrationForm::getNewCompany([
            'owner_employee_id' => $employee->id,
            'created_by' => $employee->id,
            'email' => $employee->email,
        ]);
        $contractor = $this->invoice->contractor;
        $company->setParamsByContractor($this->invoice->contractor);
        $attributes = CompanyHelper::getCompanyData($company->inn);
        if (is_array($attributes)) {
            foreach ($attributes as $attributeName => $value) {
                if (!empty($value)) {
                    $company->$attributeName = $value;
                }
            }
        }
        if ($company->save() && $company->createTrialSubscribe() && Contractor::createFounder($company)) {
            $company->updateAttributes(['main_id' => $employee->company ? $employee->company->main_id : $company->id]);

            $employeeCompany = new EmployeeCompany(['company_id' => $company->id]);
            $employeeCompany->setEmployee($employee);
            $employeeCompany->employee_role_id = EmployeeRole::ROLE_CHIEF;

            $companyTaxationType = new CompanyTaxationType([
                'company_id' => $company->id,
                'osno' => in_array('osno', $this->taxationType),
                'usn' => in_array('usn', $this->taxationType),
                'envd' => in_array('envd', $this->taxationType),
                'psn' => in_array('psn', $this->taxationType),
            ]);


            $account = new CheckingAccountant([
                'type' => CheckingAccountant::TYPE_MAIN,
                'company_id' => $company->id,
                'bik' => $contractor->BIC,
                'bank_name' => $contractor->bank_name,
                'bank_city' => $contractor->bank_city,
                'ks' => $contractor->corresp_account,
                'rs' => $contractor->current_account,
            ]);

            if ($employeeCompany->save(false) &&
                $companyTaxationType->save() &&
                $employee->setCompany($company->id) !== null
            ) {
                $employee->updateAttributes([
                    'company_id' => $company->id,
                    'main_company_id' => $employee->main_company_id ? : $company->id,
                ]);
                $account->save(); // Банковские реквизиты могут быть не заполнены, потому не влияет на создание компании

                return $company;
            } else {
                if ($employeeCompany->errors) {
                    \common\components\helpers\ModelHelper::logErrors($employeeCompany, __METHOD__);
                }
                if ($companyTaxationType->errors) {
                    \common\components\helpers\ModelHelper::logErrors($companyTaxationType, __METHOD__);
                }
            }
        } elseif ($company->errors) {
            \common\components\helpers\ModelHelper::logErrors($company, __METHOD__);
        }

        return null;
    }

    /**
     * @return Company
     */
    public function createContractor(Company $formCompany = null)
    {
        if ($formCompany === null) {
            return null;
        }
        $company = $this->invoice->company;
        $account = $company->mainCheckingAccountant;
        $model = new Contractor([
            'scenario' => Contractor::SCENARIO_LEGAL_FACE,
            'type' => Contractor::TYPE_SELLER,
            'status' => Contractor::ACTIVE,
            'face_type' => Contractor::TYPE_LEGAL_PERSON,
            'company_type_id' => $company->company_type_id,
            'taxation_system' => $company->hasNds() ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS,
            'ITN' => $company->inn,
            'PPC' => $company->kpp,
            'name' => $company->name_short,
            'director_post_name' => $company->chief_post_name,
            'director_name' => $company->chief_fio,
            'director_email' => $company->email,
            'legal_address' => $company->address_legal,
            'BIC' => $account->bik,
            'bank_name' => $account->bank_name,
            'bank_city' => $account->bank_city,
            'corresp_account' => $account->ks,
            'current_account' => $account->rs,
            'company_id' => $formCompany->id,
            'employee_id' => $this->employee->id,
            'object_guid' => OneCExport::generateGUID(),
            'chief_accountant_is_director' => 1,
            'contact_is_director' => 1,
        ]);

        if ($model->save()) {
            return $model;
        }

        return null;
    }

    /**
     * @return Invoice
     */
    public function findSavedInvoice(Company $company, Contractor $contractor)
    {
        $outInvoice = $this->invoice;

        $query = Invoice::find()->andWhere([
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'is_deleted' => false,
            'type' => Documents::IO_TYPE_IN,
            'document_date' => $outInvoice->document_date,
            'document_number' => $outInvoice->document_number,
        ]);

        if ($outInvoice->document_additional_number) {
            $query->andWhere([
                'document_additional_number' => $outInvoice->document_additional_number,
            ]);
        } else {
            $query->andWhere([
                'or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if (($inInvoice = $query->one()) !== null) {
            Yii::$app->session->setFlash(
                'error',
                "Данный счет уже загружен в вашу компанию."
            );
        }

        return $inInvoice;
    }

    /**
     * @return Invoice
     */
    public function getNewInvoice(Company $company, Contractor $contractor)
    {
        $outInvoice = $this->invoice;

        $inInvoice = new Invoice($outInvoice->getAttributes([
            'production_type',
            'document_date',
            'payment_limit_date',
            'has_markup',
            'has_discount',
            'nds_view_type_id',
            'price_precision',
            'currency_name',
            'currency_amount',
            'currency_rate',
            'currency_rate_type',
            'currency_rate_date',
            'currency_rate_amount',
            'currency_rate_value',
            'total_mass_gross',
            'total_place_count',
        ]));
        $inInvoice->allowed_strict_mode = true;
        $inInvoice->type = Documents::IO_TYPE_IN;
        $inInvoice->company_id = $company->id;
        $inInvoice->contractor_id = $contractor->id;
        $inInvoice->document_number = $outInvoice->fullNumber;
        $inInvoice->object_guid = OneCExport::generateGUID();
        $inInvoice->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_OTHER;
        $inInvoice->populateRelation('company', $company);
        $inInvoice->populateRelation('contractor', $contractor);

        $inOrderArray = [];
        foreach ($outInvoice->orders as $outOrder) {
            $outProduct = $outOrder->product;
            $productTitle = $outProduct->production_type == Product::PRODUCTION_TYPE_SERVICE &&
                            strpos($outOrder->product_title, $outProduct->title) !== false ?
                            $outProduct->title : $outOrder->product_title;

            $product = $company->getProducts()->andWhere([
                'production_type' => $outProduct->production_type,
                'title' => $productTitle,
                'product_unit_id' => $outProduct->product_unit_id,
                'is_deleted' => false,
            ])->one();

            if ($product === null) {
                $product = new Product($outProduct->getAttributes([
                    'production_type',
                    'product_unit_id',
                    'box_type',
                    'count_in_place',
                    'place_count',
                    'mass_gross',
                    'country_origin_id',
                    'weight',
                    'volume',
                ]));
                $product->title = $productTitle;
                $product->price_for_buy_nds_id = $product->price_for_sell_nds_id = $outOrder->sale_tax_rate_id;
                $product->price_for_buy_with_nds = $outOrder->price;
                $product->creator_id = $this->employee->id;
                $product->company_id = $company->id;
                if (!$product->save()) {
                    return null;
                }
            }

            $inOrder = OrderHelper::createOrderByProduct(
                $product,
                $inInvoice,
                $outOrder->quantity,
                $outOrder->price * 100,
                $outOrder->discount
            );
            $inOrder->product_title = $outOrder->product_title;

            $inOrderArray[] = $inOrder;
        }

        $inInvoice->populateRelation('orders', $inOrderArray);

        return $inInvoice;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->db->transaction(function ($db) {
            $this->employee = RegistrationForm::getNewEmployee([
                'email' => $this->username,
            ]);
            $this->employee->setPassword($this->password);
            if ($this->createCompany($this->employee) && Yii::$app->user->login($this->employee, 3600 * 24 * 30)) {
                $mailer = clone \Yii::$app->mailer;
                $mailer->htmlLayout = 'layouts/html2';
                $mailer->compose([
                    'html' => 'system/new-registration/html',
                    'text' => 'system/new-registration/text',
                ], [
                    'login' => $this->employee->email,
                    'password' => $this->password,
                    'subject' => 'Добро пожаловать в КУБ24',
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo($this->employee->email)
                    ->setSubject('Добро пожаловать в КУБ24')
                    ->send();

                Yii::$app->session->set('show_example_popup', 1);

                return true;
            }

            if ($db->getTransaction()->isActive) {
                $db->getTransaction()->rollBack();
            }

            return false;
        });
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->db->transaction(function ($db) {
            if (($invoice = $this->createInvoice()) !== null) {
                $this->_invoiceViewUrl = Url::to([
                    '/documents/invoice/view',
                    'type' => $invoice->type,
                    'id' => $invoice->id,
                ]);

                return true;
            }

            if ($db->getTransaction()->isActive) {
                $db->getTransaction()->rollBack();
            }

            return false;
        });
    }

    /**
     * @return boolean
     */
    protected function createInvoice()
    {
        $company = $this->employee->getCompanies()->andWhere([
            'inn' => $this->invoice->contractor_inn,
            'blocked' => false,
        ])->andFilterWhere([
            'kpp' => empty($this->invoice->contractor_kpp) ? null : $this->invoice->contractor_kpp,
        ])->one();
        if ($company === null) {
            if ($this->employee->getCanAddCompany()) {
                $company = $this->createCompany($this->employee);
            } else {
                Yii::$app->session->setFlash(
                    'error',
                    "Вы не можете добавить компанию {$this->invoice->contractor_name_short}."
                );

                return null;
            }
        }

        if ($company !== null) {
            if (!$this->employee->company || $this->employee->company->id !== $company->id) {
                $this->employee->updateAttributes(['company_id' => $company->id]);
                $this->employee->setCompany($company->id);
            }
            if ($company->createInvoiceAllowed($this->invoice->type)) {
                $contractor = $company->getContractors()->andWhere([
                    'ITN' => $this->invoice->company_inn,
                    'is_deleted' => false,
                ])->andFilterWhere([
                    'PPC' => $this->invoice->company_kpp,
                ])->one() ? : $this->createContractor($company);

                if ($contractor) {
                    $invoice = $this->findSavedInvoice($company, $contractor) ? : $this->getNewInvoice($company, $contractor);
                    if ($invoice !== null && (!$invoice->getIsNewRecord() || InvoiceHelper::save($invoice))) {
                        return $invoice;
                    } elseif (isset($invoice->errors)) {
                        \common\components\helpers\ModelHelper::logErrors($invoice, __METHOD__);
                    }
                }
            } else {
                Yii::$app->session->setFlash(
                    'error',
                    "Вы не можете добавить входящий счет для {$this->invoice->contractor_name_short}."
                );
            }
        }

        return null;
    }
}
