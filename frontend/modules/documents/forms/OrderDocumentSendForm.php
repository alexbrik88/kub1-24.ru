<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2018
 * Time: 6:35
 */

namespace frontend\modules\documents\forms;


use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use yii\base\Model;
use common\models\document\OrderDocument;
use Yii;

/* @property EmployeeCompany $employeeCompany
 * @property Company $company
 * @property Employee $employee
 * @property OrderDocument $model
 */
class OrderDocumentSendForm extends Model
{
    /**
     * @var boolean
     */
    public $sendToChief;
    /**
     * @var boolean
     */
    public $sendToChiefAccountant;
    /**
     * @var boolean
     */
    public $sendToContact;

    /**
     * @var boolean
     */
    public $sendToOther;
    /**
     * @var string
     */
    public $otherEmail;
    /**
     * @var string
     */
    public $chiefEmail = null;
    /**
     * @var string
     */
    public $chiefAccountantEmail = null;
    /**
     * @var null
     */
    public $contactAccountantEmail = null;

    public $textRequired = false;
    public $emailText;

    /**
     * @var EmployeeCompany
     */
    protected $_employeeCompany;

    /**
     * @var OrderDocument
     */
    private $_model;

    /**
     * @param EmployeeCompany $employeeCompany
     * @param array $config
     */
    public function __construct(EmployeeCompany $employeeCompany, $config = [])
    {
        $this->_employeeCompany = $employeeCompany;

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emailText'], 'trim'],
            [['emailText'], 'string'],
            [
                ['emailText'], 'required', 'when' => function (OrderDocumentSendForm $model) {
                return (bool)$model->textRequired;
            },
                'enableClientValidation' => false,
            ],
            [['sendToChief', 'sendToChiefAccountant', 'sendToContact', 'sendToOther'], 'boolean'],
            [
                ['sendToChief'], function ($attribute) {
                /* @var InvoiceSendForm $this */
                if (!$this->sendToChief && !$this->sendToChiefAccountant
                    && !$this->sendToContact && !$this->sendToOther
                ) {
                    $this->addError($attribute, 'Необходимо выбрать хотя бы 1 пункт для отправки счета.');
                }
            },
            ],
            [
                ['otherEmail'], 'required', 'when' => function (OrderDocumentSendForm $model) {
                return (bool)$model->sendToOther;
            }, 'enableClientValidation' => false,
            ],
            [
                ['chiefEmail'], 'required', 'when' => function (OrderDocumentSendForm $model) {
                return (bool)($model->sendToChief && empty($this->model->contractor->director_email));
            },
                'message' => 'Для отправки счета руководителю, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [
                ['chiefAccountantEmail'], 'required', 'when' => function (OrderDocumentSendForm $model) {
                return (bool)($model->sendToChiefAccountant && empty($this->model->contractor->chief_accountant_email));
            },
                'message' => 'Для отправки счета главному бухгалтеру, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [
                ['contactAccountantEmail'], 'required', 'when' => function (OrderDocumentSendForm $model) {
                return (bool)($model->sendToContact && empty($this->model->contractor->contact_email));
            },
                'message' => 'Для отправки счета контакту, необходимо заполнить email.',
                'enableClientValidation' => false,
            ],
            [['chiefEmail', 'chiefAccountantEmail', 'otherEmail', 'contactAccountantEmail'], 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'sendToChief' => 'Отправить руководителю',
            'sendToChiefAccountant' => 'Отправить главному бухгалтеру',
            'sendToContact' => 'Отправить контактному лицу',
            'sendToOther' => 'Другому',
            'chiefEmail' => 'e-mail',
            'chiefAccountantEmail' => 'e-mail',
            'otherEmail' => 'e-mail',
            'emailText' => 'Текст письма',
        ];
    }


    /**
     * @return array|bool
     */
    public function send()
    {
        /** @var EmployeeCompany $sender */
        $sender = $this->employeeCompany;
        $savedCount = 0;
        $emailArray = $this->getEmailList();
        $totalCount = count($emailArray);
        $emailParams = $this->getMessageParams();
        $params = [
            'model' => $this->model,
            'employeeCompany' => $sender,
            'employee' => $sender->employee,
            'subject' => $emailParams['subject'],
            'emailText' => $this->textRequired ? $this->emailText : null,
            'pixel' => [
                'company_id' => $sender->company_id,
                'email' => implode(',', $emailArray),
            ],
        ];
        Yii::$app->mailer->htmlLayout = 'layouts/document-html';
        if ($this->model->uid == null) {
            $this->model->updateAttributes([
                'uid' => OrderDocument::generateUid(),
            ]);
        }

        $message = Yii::$app->mailer->compose($emailParams['emailMessagePath'], $params)
            ->setFrom([Yii::$app->params['emailList']['docs'] => $sender->getFio()])
            ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
            ->setSubject($emailParams['subject']);

        foreach ($emailParams['attachContent'] as $item) {
            $message->attachContent($item['content'], [
                'fileName' => $item['fileName'],
                'contentType' => $item['contentType'],
            ]);
        }

        foreach ($emailArray as $email) {
            if ($message->setTo($email)->send()) {
                $this->setOrderDocumentSendStatus($this->model, $sender->employee);
                $savedCount++;
            }
        }

        return $savedCount ? [$savedCount, $totalCount] : false;
    }

    /**
     * @return array
     */
    public function getMessageParams()
    {
        $emailMessagePath = null;
        $subject = null;
        $attachContent = [];

        $emailMessagePath = [
            'html' => 'system/documents/order-document/html',
            'text' => 'system/documents/order-document/text',
        ];
        $subject = 'Заказ No ' . $this->model->fullNumber
            . ' от ' . DateHelper::format($this->model->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->model->company->getTitle(true);
        $attachContent[] = [
            'content' => OrderDocument::getRenderer(null, $this->model, PdfRenderer::DESTINATION_STRING)->output(false),
            'fileName' => $this->model->pdfFileName,
            'contentType' => 'application/pdf',
        ];

        return [
            'emailMessagePath' => $emailMessagePath,
            'subject' => $subject,
            'attachContent' => $attachContent,
        ];
    }

    /**
     * Return array of emails.
     * @return array
     */
    public function getEmailList()
    {
        $emailArray = [];
        $contractor = $this->model->contractor;
        if ($this->sendToChief) {
            if ($this->chiefEmail) {
                $contractor->director_email = $this->chiefEmail;
                if ($contractor->save(true, ['director_email'])) {
                    $emailArray[] = $this->chiefEmail;
                }
            } else {
                $emailArray[] = $contractor->director_email;
            }
        }

        if ($this->sendToChiefAccountant) {
            if ($this->chiefAccountantEmail) {
                $contractor->chief_accountant_email = $this->chiefAccountantEmail;
                if ($contractor->save(true, ['chief_accountant_email'])) {
                    $emailArray[] = $this->chiefAccountantEmail;
                }
            } else {
                $emailArray[] = $contractor->chief_accountant_email;
            }
        }

        if ($this->sendToContact) {
            if ($this->contactAccountantEmail) {
                $contractor->contact_email = $this->contactAccountantEmail;
                if ($contractor->save(true, ['contact_email'])) {
                    $emailArray[] = $this->contactAccountantEmail;
                }
            } else {
                $emailArray[] = $contractor->contact_email;
            }
        }

        if ($this->sendToOther) {
            $emailArray[] = $this->otherEmail;
        }

        return $emailArray;
    }


    /**
     * @return OrderDocument
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * @param OrderDocument $model
     */
    public function setModel($model)
    {
        $this->_model = $model;
    }

    /**
     * @param OrderDocument $model
     * @param Employee|null $user
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function setOrderDocumentSendStatus(OrderDocument $model, Employee $user = null)
    {
        return LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (OrderDocument $model) use ($user) {
            $model->status_id = OrderDocumentStatus::STATUS_SEND;
            $model->status_updated_at = time();
            $model->status_author_id = $user ? $user->id : Yii::$app->user->id;

            return $model->save(false, ['status_id', 'status_updated_at', 'status_author_id']);
        });
    }

    /**
     * @return EmployeeCompany
     */
    public function getEmployeeCompany()
    {
        return $this->_employeeCompany;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->employeeCompany ? $this->employeeCompany->company : null;
    }

    /**
     * @return Employee
     */
    public function getEmployee()
    {
        return $this->employeeCompany ? $this->employeeCompany->employee : null;
    }
}