<?php
namespace frontend\modules\documents\widgets;

use common\models\document\status\InvoiceStatus;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\base\Widget;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;

/**
 * Class CostsStatistics
 * @package frontend\modules\documents\widgets
 */
class CostsStatistics extends Widget
{

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        $datePeriod = StatisticPeriod::getSessionPeriod();

        $items = InvoiceStatistic::getCostsStatistics(
            Documents::IO_TYPE_IN,
            \Yii::$app->user->identity->company->id,
            $datePeriod,
            [InvoiceStatus::STATUS_PAYED,]
        );
        /**
         *  $totalPrice = InvoiceStatistic::getTotalTypePrices(
         *      Documents::IO_TYPE_IN,
         *      \Yii::$app->user->identity->company->id,
         *      $datePeriod,
         *      [InvoiceStatus::STATUS_PAYED,]
         *  );
         */
        $totalPrice = array_sum(ArrayHelper::getColumn($items, 'sum'));

        $rowArray = [];
        $pieData = [];

        foreach (array_values($items) as $key => $item) {
            $rowArray[] = [
                'name' => $item['name'],
                'sum' => $item['sum'],
                'percent' => $totalPrice > 0 ? round($item['sum'] / $totalPrice * 100, 2) : 0,
                'color' => isset(InvoiceStatistic::$donutChartColor[$key]) ? InvoiceStatistic::$donutChartColor[$key] : null,
                'rowClass' => '',
            ];

            $pieData[] = [
                'label' => $item['name'],
                'value' => $item['sum'],
                'color' => isset(InvoiceStatistic::$donutChartColor[$key]) ? InvoiceStatistic::$donutChartColor[$key] : null,
            ];
        }

        $rowArray[] = [
            'name' => 'ИТОГО',
            'sum' => $totalPrice,
            'percent' => 100,
            'color' => '',
            'rowClass' => 'font-bold',
        ];

        return $this->render('costsStatistics', [
            'rowArray' => $rowArray,
            'pieData' => $pieData,
        ]);
    }
}
