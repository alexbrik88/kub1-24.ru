<?php
/* @var \yii\web\View $this */
/* @var array $rowArray */
/* @var array $pieData */

use yii\helpers\Json;
?>

<div class="row" role="row">
    <div class="col-md-6">
        <table class="expenses-payment-table table">
            <!-- <thead>
            <tr class="heading" role="row">
                <th width="4%"></th>
                <th width="10%">Статья</th>
                <th width="10%">Сумма</th>
                <th width="10%">%</th>
            </tr>
            </thead> -->
            <tbody>
                <?php foreach ($rowArray as $row) {
                    echo $this->render('_partial_cost_item', $row);
                } ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <div class="portlet-body">
            <?php if (!empty($pieData)) : ?>
                <div id="donut-expenses-invoice" style="width:100%; margin: 0 auto; max-width: 175px; height: 175px;"></div>
                <script type="text/javascript">
                $(document).ready(function() {
                    var morrisDonut = Morris.Donut({
                        element: "donut-expenses-invoice",
                        formatter: function (y, data) { return y.toLocaleString('ru-RU', { minimumFractionDigits: 2, maximumFractionDigits: 2 }); },
                        data: <?= Json::encode($pieData) ?>
                    });
                    /*
                    morrisDonut.setLabels = function(label1, label2) {
                      var inner, maxHeightBottom, maxWidth, text2bbox, text2scale;
                      inner = (Math.min(this.el.width() / 2, this.el.height() / 2) - 10) * 2 / 3;
                      maxWidth = 1.8 * inner;
                      maxHeightBottom = inner / 3;
                      this.text1.attr({
                        text: label1,
                        transform: ''
                      });
                      this.text2.attr({
                        text: label2,
                        transform: ''
                      });
                      text2bbox = this.text2.getBBox();
                      text2scale = Math.min(maxWidth / text2bbox.width, maxHeightBottom / text2bbox.height);
                      return this.text2.attr({
                        transform: "S" + text2scale + "," + text2scale + "," + (text2bbox.x + text2bbox.width / 2) + "," + text2bbox.y
                      });
                    };
                    */
                    morrisDonut.select(0);
                });
                </script>
            <?php endif; ?>
        </div>
    </div>
</div>
