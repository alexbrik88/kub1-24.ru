<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $widget frontend\modules\documents\widgets\SummarySelectWidget */

?>

<style>
    .summary-info {
        font-weight: bold;
        width: calc(100% - 275px);
        border: 1px solid #4276a4;
        border-bottom-color: rgb(66, 118, 164);
        margin: 0 20px;
    }
    .buttons-scroll-fixed {
        position: fixed;
        bottom: 0px;
        z-index: 9999;
        background:#fff;
        margin-left: -20px;
    }
    @media (max-width: 992px) {
        .summary-info {  width: calc(100% - 40px);}
    }
    @media(max-width:767px){
        .summary-info {  width: calc(100% - 20px);} .total-count-select{ padding:0 } .total-amount-select{padding:0}
    }
</style>
<div id="summary-container" class=" hidden buttons-scroll-fixed col-xs-12 col-sm-12 col-lg-12  pad0">
    <div class="summary-info">
        <div style="padding: 5px 10px;">
            <table class="pull-left">
                <tbody>
                    <tr>
                        <td style="width: 50px; height: 34px; text-align: center;">
                            <input type="checkbox" class="joint-operation-main-checkbox" name="" value="1">
                        </td>
                        <td style="width: 185px;">
                            <div class="total-count-select" style="padding-left: 10px;">Выбрано: <span>0</span></div>
                        </td>
                        <td style="width: 170px;">
                            <div class="total-amount-select" style="padding-left: 10px;">Сумма: <span>0</span></div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="actions-many-items pull-right">
                <tbody>
                    <tr>
                        <?php foreach ($widget->buttons as $key => $button) : ?>
                            <?= Html::tag('td', $button, [
                                'style' => 'width: 80px; height: 34px; color: #4276a4;' . ($key ? ' padding-left: 15px;' : ''),
                            ]) ?>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.joint-operation-checkbox').on('change', function(){
            var countChecked = 0;
            var summary = 0;
            $('.joint-operation-checkbox:checked').each(function(){
                var price = $(this).closest('tr').find('.price').data('price');
                price = price.replace(",",".");
                summary += parseFloat(price);
                countChecked++;
            });
            if (countChecked > 0) {
                $('#summary-container').removeClass('hidden');
                $('#summary-container .total-count-select span').text(countChecked);
                $('#summary-container .total-amount-select span').text(number_format(summary, 2, ',', ' '));
            } else {
                $('#summary-container').addClass('hidden');
            }
        });
    });
</script>