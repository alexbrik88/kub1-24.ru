<?php

use yii\helpers\Html;
use common\components\ImageHelper;
use yii\helpers\Url;
use frontend\rbac\permissions;
use common\models\file\FileDir;
use frontend\modules\documents\components\UploadManagerHelper;
use yii\bootstrap\Dropdown;
use frontend\models\Documents;
use frontend\rbac\UserRole;

/** @var array $images */
/** @var bool $isFullScreen */
/** @var integer $startFileId */

$dropDirs = [];
$dropDocs = [];

$canIndex = $canCreate = $canDelete = $canUpdate = $canRestore =
    Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
    Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
    Yii::$app->user->can(UserRole::ROLE_SUPERVISOR);

if ($isFullScreen) {
    if ($dirs = FileDir::getEmployeeAllowedDirs()) {

        $dropDirs[] = [
            'label' => 'В папку',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'group-attach-files-to',
            ],
        ];

        /** @var FileDir $dir */
        foreach ($dirs as $dir) {
            $dropDirs[] = [
                'label' => $dir->name,
                'url' => '#move-files-to',
                'linkOptions' => [
                    'class' => 'move-files-to',
                    'data-directory_id' => $dir->id
                ],
            ];
        }
    }

    $docTypesList = UploadManagerHelper::getDocumentTypesList($company);
    foreach ($docTypesList as $label => $docTypes) {
        $dropDocs[] = [
            'label' => $label,
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'group-attach-files-to',
            ],
        ];
        foreach ($docTypes as $key => $value) {
            $dropDocs[] = [
                'label' => $value,
                'url' => '#attach-files-to',
                'linkOptions' => [
                    'class' => 'attach-files-to',
                    'data-doc_type' => $key
                ],
            ];
        }
    }
}

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar page-scan-preview <?= $isFullScreen ? 'full-screen' : '' ?>">
        <div class="col-md-12">
            <?php if ($isFullScreen): ?>
                <div class="slider-header">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="slide-file-name">
                                <div class="file-name"></div>
                            </div>
                        </div>
                        <div class="actions col-sm-6">
                            <?php if ($canIndex): ?>
                                <?= Html::a('<i class="glyphicon glyphicon-print"></i> Печать', 'javascript:;', [
                                    'class' => 'print-scan btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                                ]); ?>
                                <?= Html::a('<i class="glyphicon glyphicon-print"></i>', 'javascript:;', [
                                    'class' => 'print-scan btn btn-default btn-sm hidden-lg',
                                ]); ?>
                            <?php endif; ?>
                            <?php if ($canIndex): ?>
                                <?= Html::a('<i class="glyphicon glyphicon-download-alt"></i> Скачать', 'javascript:;', [
                                    'class' => 'download-scan btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                                    'download' => 'download',
                                ]); ?>
                                <?= Html::a('<i class="glyphicon glyphicon-download-alt"></i>', 'javascript:;', [
                                    'class' => 'download-scan btn btn-default btn-sm hidden-lg',
                                    'download' => 'download',
                                ]); ?>
                            <?php endif; ?>
                            <?php if ($canDelete) : ?>
                                <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                                    'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                                    'data-toggle' => 'modal',
                                ]); ?>
                                <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                                    'class' => 'btn btn-default btn-sm hidden-lg',
                                    'data-toggle' => 'modal',
                                ]); ?>
                            <?php endif ?>
                            <?php if ($canUpdate && $dropDirs) : ?>
                                <div class="dropdown">
                                    <?= Html::a('Переместить  <span class="caret"></span>', null, [
                                        'class' => 'btn btn-default btn-sm dropdown-toggle',
                                        'id' => 'dropdownMenu1',
                                        'data-toggle' => 'dropdown',
                                        'aria-expanded' => true,
                                        'style' => 'height: 28px;',
                                    ]); ?>
                                    <?= Dropdown::widget([
                                        'items' => $dropDirs,
                                        'options' => [
                                            'style' => 'right: -20px!important; left: auto; top: 28px;',
                                            'aria-labelledby' => 'dropdownMenu1',
                                        ],
                                    ]); ?>
                                </div>
                            <?php endif ?>
                            <?php if ($canUpdate && $dropDocs) : ?>
                                <div class="dropdown">
                                    <?= Html::a('Прикрепить  <span class="caret"></span>', null, [
                                        'class' => 'btn btn-default btn-sm dropdown-toggle btn-many-attach',
                                        'id' => 'dropdownMenu2',
                                        'data-toggle' => 'dropdown',
                                        'aria-expanded' => true,
                                        'style' => 'height: 28px; display:none',
                                    ]); ?>
                                    <?= Dropdown::widget([
                                        'items' => $dropDocs,
                                        'options' => [
                                            'style' => 'right: -70px!important; left: auto; top: 28px;',
                                            'aria-labelledby' => 'dropdownMenu1',
                                        ],
                                    ]); ?>
                                </div>
                            <?php endif ?>

                            <?= $this->render('@documents/views/upload-manager/_partial/_joint_operations_modals', [
                                'canUpdate' => $canUpdate,
                                'canRestore' => $canRestore,
                                'canDelete' => $canDelete
                            ]) ?>

                        </div>
                        <div class="col-sm-3">
                        <div class="closePage" style="position: relative">
                            <a href="<?= Url::previous('lastPreviewScansPage') ?>"><i class="close-preview"></i></a>
                        </div>
                        <div class="nextPage">
                            <img src="/img/documents/blue-arrow-right.png" width="24"/>
                        </div>
                        <div class="num-page-wrap">
                            <span class="num-page">1</span> /
                            <?= count($images) ?>
                        </div>
                        <div class="prevPage">
                            <img src="/img/documents/blue-arrow-left.png" width="24"/>
                        </div>
                    </div>
                    </div>
                </div>
            <?php else: ?>
                <div class="slider-header">
                    <div class="slide-file-name">
                        <div class="file-name"></div>
                    </div>
                    <div class="nextPage">
                        <img src="/img/documents/blue-arrow-right.png" width="24"/>
                    </div>
                    <div class="num-page-wrap">
                        <span class="num-page">1</span> /
                        <?= count($images) ?>
                    </div>
                    <div class="prevPage">
                        <img src="/img/documents/blue-arrow-left.png" width="24"/>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12">
            <div class="slider-body">
                <div id="slide-img" class="slide issuu-container issuu-img-container img" style="display: none; min-height: 551px;"></div>
                <div id="slide-pdf" class="slide issuu-container issuu-embed-container pdf" style="display: none; padding-bottom: 551px;"></div>
                <div id="slide-other" class="slide issuu-container" style="display: none; padding-bottom: 551px; padding-top: 15px; padding-left: 20px;"></div>
            </div>
        </div>

        <?= Html::tag('span', '<i class="fa fa-plus"></i>', [
            'class' => 'zoomIn'
        ]); ?>
        <?= Html::tag('span', '<i class="fa fa-minus"></i>', [
            'class' => 'zoomOut'
        ]); ?>

    </div>
</div>

<?php if ($isFullScreen): ?>

    <?= $this->render('@frontend/modules/documents/views/upload-manager/_partial/_kub_doc_js') ?>

    <?= $this->render('@frontend/modules/documents/views/upload-manager/_partial/_invoices_modal', [
        'company' => $company,
    ]) ?>
<?php else: ?>
    <div class="close-attach-page">
        <div class="close-attach-page-left">
            <a href="<?= \yii\helpers\Url::previous('lastUploadManagerPage') ?>">Назад</a>
        </div>
        <div class="close-attach-page-right">
            <a href="<?= \yii\helpers\Url::previous('lastUploadManagerPage') ?>"><i class="close-preview"></i></a>
        </div>
    </div>
<?php endif; ?>

<script>
    window.previewScansFromId = <?= (int)$startFileId ?>;
    window.previewScansList = {};
    window.previewScansPos = 0;
    window.previewScansTotal = <?= count($images) ?>;

    <?php $i = 0; foreach ($images as $imageInfo): ?>
    <?php if ($imageInfo['ext'] == 'pdf') {
        $showIn = '#slide-pdf';
        $img = Html::tag('embed', '', [
            'class' => 'source',
            'src' => Url::to($imageInfo['src']),
            'name' => 'plugin',
            'type' => 'application/pdf',
        ]);
    } elseif (in_array($imageInfo['ext'], ['gif', 'png', 'jpg', 'jpeg'])) {
        $showIn = '#slide-img';
        $img = Html::img($imageInfo['src'], [
            'class' => 'source',
        ]);
    } else {
        $showIn = '#slide-other';
        $img = 'Изображение не поддерживается';
    }
    $filename = htmlspecialchars($imageInfo['filename_full']) .'<br/>'
        . '<span>'.UploadManagerHelper::formatSizeUnits($imageInfo['filesize']).'</span>';
    ?>

    previewScansList[<?= $i ?>] =
        {
            'file_id':  <?= (int)$imageInfo['id'] ?>,
            'showIn':   '<?= addcslashes($showIn, "'") ?>',
            'filename': '<?= addcslashes($filename, "'") ?>',
            'html':     '<?= addcslashes($img, "'") ?>',
            'src':      '<?= addcslashes(Url::to($imageInfo['src']), "'") ?>'
        };

    <?php if ($startFileId == $imageInfo['id']): ?>
    window.previewScansPos = <?= (int)$i ?>;
    <?php endif; ?>


    <?php $i++; endforeach; ?>

    window.currScan = window.previewScansList[window.previewScansPos];

    ///////////////////////////////////////////////////////////////
    // console.log(previewScansList);
    ///////////////////////////////////////////////////////////////

    $('.move-files-to').on('click', function(e) {
        e.preventDefault();
        $('#move-files-to').find('.modal-move-files-to').attr('data-url', '/documents/upload-manager/move-files-to?directory_id=' + $(this).data('directory_id'));
        $('#move-files-to').modal('show');
    });
    $(document).on('click', '.modal-move-files-to, .modal-many-delete', function () {
        if ($(this).attr('data-toggle') || $(this).hasClass('no-ajax-loading')) return;
        $('#ajax-loading').show();
        var $this = $(this);

        if (!$this.hasClass('clicked')) {
            $this.addClass('clicked');
            $.post($this.data('url'), {'File[]': $('.page-scan-preview').find('.slide:visible').attr('data-id')}, function (data) {
                $('#ajax-loading').hide();
                if (data.message !== undefined) {
                    if ($('.alert-success').length > 0) {
                        $('.alert-success').remove();
                    }
                    $('.page-content').prepend('<div id="w2-success-0" class="alert-success alert fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        data.message + '</div>');
                }
            });
        }
    });

    $(document).on('click', '.group-attach-files-to', function(e) {
        return false;
    });

    $(document).on("click", ".attach-files-to", function (e) {
        e.preventDefault();
        var addToDoc = $(this).data('doc_type');
        var files = [ $('.page-scan-preview').find('.slide:visible').attr('data-id') ];

        if (KubDoc.setParams(addToDoc, files)) {
            KubDoc.showModal();
        }

    });

    $('.modal-attach-image-yes').on('click', function () {
        $('#invoices-list').modal();
        $('#modal-attach-image').modal('hide');
        $.pjax({
            url: '/documents/upload-manager/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {
                type: KubDoc.currentType,
                doc_type: KubDoc.currentDocType,
                files: KubDoc.currentFiles
            },
            push: false,
            timeout: 5000
        });
    });

    $('.modal-attach-image-no').on('click', function () {
        $.post('/documents/upload-manager/redirect-to-create-document', {
            type: KubDoc.currentType,
            doc_type: KubDoc.currentDocType,
            files: KubDoc.currentFiles
        }, function(data) {
            $('#modal-attach-image').modal('hide');
            if (data['redirect_url']) {
                location.href = data['redirect_url'];
            }
        });
    });

</script>