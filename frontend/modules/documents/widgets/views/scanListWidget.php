<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var \common\models\document\AbstractDocument $model */

$modelName = empty($model->urlPart) ? 'model' : $model->urlPart;
$ident = "{$modelName}-{$model->id}";
$listId = "{$ident}-scan-list";
?>

<div id="<?= $listId ?>" class="document-scan-list">
    <?php foreach ($model->scanDocuments as $scan) : ?>
        <?php if ($scan->file && !$scan->file->is_deleted) : ?>
            <div id="scan-<?= $scan->id; ?>" class="scan" data-id="<?= $scan->id; ?>">
                <?= '<span class="icon icon-paper-clip m-r-10"></span>' . Html::a($scan->file->filename_full, [
                    '/documents/scan-document/file',
                    'id' => $scan->id,
                    'name' => $scan->file->filename_full,
                ], [
                    'class' => 'scan-link',
                    'target' => '_blank',
                    'download' => '',
                ]); ?>
                <?php if (empty($deleteAccessCallback) || call_user_func($deleteAccessCallback, $scan)) : ?>
                    <?php Modal::begin([
                        'id' => 'scan-del-confirm-' . $scan->id,
                        'closeButton' => false,
                        'toggleButton' => [
                            'label' => '',
                            'tag' => 'span',
                            'class' => 'icon-close m-l-10',
                        ],
                        'options' => [
                            'class' => 'confirm-modal',
                        ],
                    ]); ?>
                        <div style="margin-bottom: 15px; text-align: center; font-size: 16px;">
                            Вы уверены, что хотите удалить файл?
                        </div>
                        <div class="form-actions row">
                            <div class="col-xs-6">
                                <?= Html::button('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', [
                                    'class' => 'btn darkblue text-white pull-right mt-ladda-btn ladda-button scan-list-del-button',
                                    'data-style' => 'expand-right',
                                    'style' => 'width: 80px;',
                                    'data' => [
                                        'list' => "#{$listId}",
                                        'params' => [
                                            'model-id' => 0,
                                            'scan-id' => $scan->id,
                                            '_csrf' => Yii::$app->request->csrfToken,
                                        ],
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-xs-6">
                                <?= Html::button('НЕТ', [
                                    'class' => 'btn darkblue text-white',
                                    'style' => 'width: 80px;',
                                    'data' => [
                                        'dismiss' => 'modal',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    <?php Modal::end(); ?>
                <?php endif ?>
            </div>
        <?php endif ?>
    <?php endforeach; ?>
</div>
