<?php
use common\components\TextHelper;

/* @var $outerClass string */
/* @var $innerClass string */
/* @var $text string */
/* @var $sum float */
/* @var $count int */
?>
<div class="<?= $outerClass; ?>">
    <div class="dashboard-stat <?= $innerClass; ?>" style="position: relative;">
        <div class="visual">
            <i class="fa fa-comments"></i>
        </div>
        <div class="details">
            <div class="number fontSizeSmall">
                <span class="details-sum" data-value="<?= $sum ?>"><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></span> <i class="fa fa-rub"></i>
            </div>
            <div class="desc">
                <?= $text; ?>
            </div>
        </div>
        <div class="more">
            Количество счетов: <?= $count; ?>
        </div>
        <?php if (!empty($url)) : ?>
            <a href="<?= $url ?>" style="display: inline-block; position: absolute; top: 0; bottom: 0; left: 0; right: 0;"></a>
        <?php endif; ?>
    </div>
</div>