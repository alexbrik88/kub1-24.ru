<?php
use common\components\TextHelper;

/* @var $name string */
/* @var $sum float */
/* @var $percent int */
/* @var $color string */
/* @var $rowClass string */
?>

<tr class="<?= $rowClass; ?>" role="row">
    <td>
        <?php if (!empty($color)): ?>
            <span class="color-expense" style="background-color: <?= $color; ?>"></span>
        <?php endif; ?>
    </td>
    <td><?= $name; ?></td>
    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></td>
    <td class="text-right"><?= $percent; ?></td>
</tr>