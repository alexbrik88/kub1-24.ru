<?php

use frontend\modules\documents\widgets\DocumentFileScanWidget;
use frontend\modules\documents\widgets\ScanListWidget;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\web\View;

/**
 * @var View $this
 * @var int $maxFileCount
 * @var DocumentFileScanWidget $widget
 */

$widget = $this->context;
$model = $widget->model;
$ident = "{$model->urlPart}-{$model->id}";
$containerId = "{$ident}-file-scan-container";

$options = [
    'id' => $containerId,
    'class' => 'document-file-scan-container row',
    'data' => [
        'upload-url' => $widget->uploadUrl,
        'delete-url' => $widget->deleteUrl,
        'list-url' => $widget->listUrl,
        'free-url' => $widget->scanFreeUrl,
        'bind-url' => $widget->scanBindUrl,
        'scan-url' => $widget->scanListUrl,
        'model-id' => $model->id,
        'csrf-parameter' => Yii::$app->request->csrfParam,
        'csrf-token' => Yii::$app->request->csrfToken,
    ],
];
$btnOptions = [
    'class' => 'btn yellow btn-sm document-file-scan-button upl_btn',
    'style' => 'width:33px; height: 27px; margin: 5px 0 0;',
];
$buttonsAreaOptions = [
    'class' => 'document-file-scan-buttons-area col-sm-12 hidden',
];

Html::addCssClass($btnOptions, $widget->hasFreeScan ? 'document-file-scan-source-btn' : $widget->uploadBtnClass);

$fileCount = \common\models\file\File::find()->andWhere([
    'owner_model' => $model->className(),
    'owner_id' => $model->id,
])->count();
$scanCount = \common\models\document\ScanDocument::find()->andWhere([
    'owner_model' => $model->className(),
    'owner_id' => $model->id,
])->count();

$filesCount = $fileCount + $scanCount;

?>

<?= Html::beginTag('div', $options) ?>

    <div class="col-sm-12">
        <div class="list-container hidden">
            <div class="document-file-list file-list"></div>
            <?php if (method_exists($model, 'getScanDocuments')) : ?>
                <?= ScanListWidget::widget([
                    'model' => $model,
                ]); ?>
            <?php endif ?>
        </div>
        <div class="document-file-loading" style="display: none;"><img src="/img/loading.gif"></div>
    </div>

    <?= Html::beginTag('div', $buttonsAreaOptions) ?>

        <?= Html::button(Html::icon('download-alt', ['class' => 'upl_btn']), $btnOptions); ?>

        <?php if ($widget->hasFreeScan) : ?>
            <div class="select-source-popover popover bottom hidden">
                <div class="arrow" style="left: 14%;"></div>
                <div style="text-align: center;">
                    Загрузить скан документа
                </div>
                <table class="file-scan-buttons">
                    <tr>
                        <td style="width:50%">
                            <?= Html::button(Html::tag('div', Html::tag('div' , '', ['class' => 'scan-pc-icon'])).'Из компьютера', [
                                'class' => "{$widget->uploadBtnClass} btn btn-sm load-button",
                                'style'=>"",
                            ]); ?>
                        </td>
                        <td style="width:50%">
                            <?php Modal::begin([
                                'id' => "{$ident}-free-scan-modal",
                                'header' => Html::tag('h1', Html::tag('div', '', [
                                                'class' => 'pull-left scan-folder-icon',
                                                'style' => '',
                                            ]) . 'Выберите скан документа', [
                                                'style' => 'text-align: center;'
                                            ]),
                                'toggleButton' => [
                                    'label' => Html::tag('div', '', ['class' => 'scan-folder-icon']).'Из "Документы"',
                                    'tag' => 'button',
                                    'class' => 'btn btn-sm load-button',
                                    'style'=>"",
                                ],
                                'options' => [
                                    'class' => 'free-scan-list-modal fade',
                                    'data' => [
                                        'container' => "#{$containerId}",
                                    ],
                                ],
                                'headerOptions' => [
                                    'style' => 'background-color: #00b7af; color: #fff;'
                                ],
                            ]); ?>
                            <?php Modal::end(); ?>
                        </td>
                    </tr>
                </table>
                <div style="text-align: center; font-size: 10px; padding-top:5px;">
                    Вы можете загрузить еще
                    <span class="uploaded-scans-count">
                        <?= \php_rutils\RUtils::numeral()->getPlural($maxFileCount - $filesCount, ['файл', 'файла', 'файлов']) ?>
                    </span>
                    объемом 5 MB
                </div>
            </div>
        <?php endif ?>

    <?php Html::endTag('div') ?>

<?php Html::endTag('div') ?>

<?= $this->registerJs(<<<JS

$(document).ready(function () {
    initDocFileScan('{$maxFileCount}');
});

JS);
