<?php

use frontend\rbac\permissions;
use common\models\Company;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\widgets\Pjax;
use common\widgets\Modal;
use frontend\widgets\BtnConfirmModalWidget;
use common\models\file\FileDir;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var array $items */
/** @var Company $newCompany */
/** @var integer $sideBarStatus */
/** @var $companyTaxation common\models\company\CompanyTaxationType */
/** @var FileDir[] $dirs */

$status = $sideBarStatus ? 'page-sidebar-menu-closed' : '';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-upload-dir',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'zIndex' => 10000,
        'position' => 'right',
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-dir-options',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'custom',
        'triggerOpen' => ['click' => true],
        'triggerClose' => ['originClick' => true, 'click' => true],
        'trackOrigin' => true,
        'trackerInterval' => 50,
        'delay' => 0,
        'animationDuration' => 0,
        'contentAsHTML' => true,
        'debug' => false,
        'zIndex' => 10000,
        'position' => 'bottom',
        'interactive' => true,
        'functionReady' => new \yii\web\JsExpression('function(origin, tooltip) { $(origin._$origin).parent().addClass("active-opt") }'),
        'functionAfter' => new \yii\web\JsExpression('function(origin, tooltip) { $(origin._$origin).parent().removeClass("active-opt") }')
    ],
]);

$dirActive = (Yii::$app->controller->action->id == 'directory') ? Yii::$app->request->get('id') : false;
$trashActive = (Yii::$app->controller->action->id == 'trash') ? true : false;

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <?php if (Yii::$app->user->can(permissions\Service::HOME_PAGE)) {
            echo \common\widgets\SideMenuNav::widget([
                'items' => $items,
                'options' => [
                    'class' => 'page-sidebar-menu page-sidebar-menu-light metismenu ' . $status,
                    'id' => 'side-menu-docs',
                    'data' => [
                        'keep-expanded' => 'false',
                        'auto-scroll' => 'true',
                        'slide-speed' => '200',
                    ],
                    //'style' => 'padding-top: 20px',
                ],
            ]);
        } ?>

        <?= $this->render('@frontend/widgets/views/modal/_create_company', [
            'newCompany' => $newCompany,
            'companyTaxation' => $companyTaxation,
        ]); ?>
        <?= $this->render('@frontend/widgets/views/modal/menu_no_rules'); ?>

        <div id="side-menu-dirs">
            <div class="scrollable">
                <?php foreach($dirs as $dir): ?>
                    <?php
                        $dirClass  = 'directory';
                        $dirClass .= (mb_strlen($dir->name, 'utf-8') > 16) ? ' tooltip-upload-dir' : '';
                        $dirClass .= ($dirActive == $dir->id) ? ' active' : '';
                        $dirFilesCount = count($dir->files);
                    ?>
                    <div id="dir-<?= $dir->id ?>" class="directory-wrapper">
                        <?= Html::beginTag('a', [
                            'href' => \yii\helpers\Url::to("/documents/upload-manager/directory/{$dir->id}"),
                            'class' => $dirClass,
                            'data-tooltip-content' => "#tooltip-upload-dir-{$dir->id}",
                            'data-id' => $dir->id
                        ]); ?>
                            <?= (($dirActive == $dir->id) ?
                                Html::img('/img/documents/main-folder-2.png', [
                                'class' => 'img-doc-folder',
                                'width' => 16]) :
                                Html::img('/img/documents/folder-2.png', [
                                    'class' => 'img-doc-folder',
                                    'width' => 14])).
                                Html::tag('span', $dir->name); ?>
                                <?= ($dirFilesCount) ? Html::tag('span', $dirFilesCount, ['class' => 'badge']) : '' ?>
                        <?= Html::endTag('a') ?>
                        <?= Html::tag('i', null, [
                        'class' => 'directory-options fa fa-gear tooltip-dir-options',
                        'data-tooltip-content' => "#dir-options-{$dir->id}"
                        ]) ?>
                    </div>
                <?php endforeach; ?>
                <?php if (empty($dirs)): ?>
                <div class="new-directory-wrapper">
                    <span>У вас пока нет папок</span><br/>
                    <a href="javascript:" class="add-folder">Создать папку</a>
                </div>
                <?php endif; ?>
            </div>
            <?= Html::a("<i class='fa fa-trash'/></i><span>Корзина</span>", '/documents/upload-manager/trash', [
                'class' => 'directory' . ($trashActive ? ' active' : '')
            ]) ?>
        </div>
    </div>
</div>

<div class="tooltip_templates" style="display: none;">
    <?php foreach($dirs as $dir): ?>
        <?php $dirFilesCount = count($dir->files); ?>
        <span id="<?= "tooltip-upload-dir-{$dir->id}" ?>" style="display: inline-block;">
            <?= $dir->name; ?>
        </span>
        <span id="<?= "dir-options-{$dir->id}" ?>">
            <?= Html::a('Редактировать', '#', [
                    'class' => 'dir-opts edit-folder',
                    'data-id' => $dir->id
            ]) ?><br/>
            <?php if ($dirFilesCount == 0): ?>
                <?= BtnConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Удалить',
                            'class' => 'dir-opts remove-folder',
                            'tag' => 'a',
                        ],
                        'confirmUrl' => Url::to(['/documents/upload-manager/remove-dir', 'id' => $dir->id]),
                        'message' => 'Вы уверены, что хотите удалить папку?',
                ]); ?>
            <?php else: ?>
                <?= Html::a('Удалить', '#unable-delete-folder-modal', [
                    'class' => 'dir-opts remove-folder',
                    'data-toggle' => 'modal',
                ]); ?>
            <?php endif; ?>
            <br/>
            <?= Html::a('Вверх', '#', [
                    'class' => 'dir-opts move-folder',
                    'data-direction' => 'up',
                    'data-id' => $dir->id
            ]) ?><br/>
            <?= Html::a('Вниз', '#', [
                    'class' => 'dir-opts move-folder',
                    'data-direction' => 'down',
                    'data-id' => $dir->id
            ]) ?>
        </span>

    <?php endforeach; ?>
</div>

<?php
    Modal::begin([
        'id' => 'add-directory-modal',
        'header' => Html::tag('h1', 'Новая папка'),
        'options' => ['style' => 'max-width: 600px;']
    ]);

    Pjax::begin([
        'id' => 'add-directory-form',
        'enablePushState' => false,
        'linkSelector' => false,
    ]);

    Pjax::end();

    Modal::end();
?>

<?php
$header = 'Нельзя удалить папку';

Modal::begin([
    'id' => 'unable-delete-folder-modal',
    'class' => 'confirm-modal ',
    'header' => Html::tag('h1', Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-info-sign pull-left',
            'style' => 'font-size:inherit;line-height:inherit;',
        ]) . $header),
    'headerOptions' => ['style' => 'display:none'],
    'footerOptions' => ['style' => 'text-align: center; border:none; padding-top:0; '],
    'footer' => Html::button('Ок', [
        'class' => 'btn darkblue',
        'data-dismiss' => 'modal',
        'aria-hidden' => 'true',
    ]),
]);

echo Html::beginTag('div', ['class' => 'text-center']);
echo Html::tag('span', 'Перед удалением папки, просьба удалить все документы из папки или перенести их в другую папку', ['style' => 'font-size:16px']);
echo Html::endTag('div');
Modal::end();
?>
