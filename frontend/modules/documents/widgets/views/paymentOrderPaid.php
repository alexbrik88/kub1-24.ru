<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var \yii\web\View $this */

?>

<div class="form-body">
    <div class="form-group row">
        <?= Html::label('Дата оплаты', '', [
            'class' => 'col-md-3 p-t-5 text-bold',
        ]) ?>
        <div class="col-md-5">
            <div class="input-icon">
                <i class="fa fa-calendar"></i>
                <?= Html::textInput('date', date('d.m.Y'), [
                    'id' => 'date_of_payment',
                    'class' => 'form-control date-picker',
                    'style' => 'width: 100%; max-width: 125px; cursor: pointer; background-color: #ffffff;',
                    'readonly' => true,
                ]) ?>
            </div>
        </div>
    </div>
</div>
<div class="form-actions row">
    <div class="col-xs-6">
        <?= Html::button('Сохранить', [
            'class' => 'btn darkblue text-white confirm',
            'data-url' => Url::to(['/documents/payment-order/paid',]),
        ]); ?>
    </div>
    <div class="col-xs-6">
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'data-dismiss' => 'modal',
        ]); ?>
    </div>
</div>
