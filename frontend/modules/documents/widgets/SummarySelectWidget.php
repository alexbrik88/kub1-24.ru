<?php

namespace frontend\modules\documents\widgets;

use yii\base\Widget;

class SummarySelectWidget extends Widget
{
    public $buttons = [];
    public $hideCalculatedFields = false;

    public function run()
    {
        return $this->render('summary_select', ['widget' => $this]);
    }
}
