<?php

namespace frontend\modules\documents\widgets;

use yii\base\Widget;

/**
 * Class StatisticWidget
 * @package frontend\modules\documents\widgets
 */
class StatisticWidget extends Widget
{
    /**
     * @var
     */
    public $type;
    /**
     * @var string
     */
    public $outerClass;
    /**
     * @var integer
     */
    public $contractorId;
    /**
     * @var integer
     */
    public $invoiceStatusId;
    /**
     * set contractor active or not
     *
     * @var integer
     */
    public $contractorStatus;

    public $searchModel;

    public $clickable = false;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('statisticWidget', [
            'ioType' => $this->type,
            'outerClass' => $this->outerClass,
            'contractorId' => $this->contractorId,
            'contractorStatus' => $this->contractorStatus,
            'invoiceStatusId' => $this->invoiceStatusId,
            'searchModel' => $this->searchModel,
            'clickable' => $this->clickable,
        ]);
    }
}
