<?php

namespace frontend\modules\documents\widgets;


use Closure;
use common\models\file\File;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class ScanListWidget extends Widget
{
    /**
     * @var common\models\document\AbstractDocument
     */
    public $model;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('scanListWidget', ['model' => $this->model]);
    }
}
