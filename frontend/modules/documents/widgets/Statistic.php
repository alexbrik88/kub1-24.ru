<?php

namespace frontend\modules\documents\widgets;

use common\models\document\Invoice;
use frontend\components\StatisticPeriod;
use frontend\modules\documents\components\InvoiceStatistic;
use frontend\models\Documents;
use Yii;
use yii\base\Widget;

/**
 * Class Statistic
 * @package frontend\modules\documents\widgets
 */
class Statistic extends Widget
{
    /**
     * @var array
     */
    public $items = [];
    /**
     * @var
     */
    public $type;

    /**
     * @var
     */
    public $contractorId;

    /**
     * @var
     */
    public $invoiceStatusId;

    /**
     * set contractor active or not
     *
     * @var integer
     */
    public $contractorStatus;

    public $searchModel;

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        $content = '';

        $dateRange = StatisticPeriod::getSessionPeriod();

        if (!empty($this->items)) {
            foreach ($this->items as $item => $data) {
                $info = InvoiceStatistic::getStatisticInfo(
                    $this->type,
                    \Yii::$app->user->identity->company->id,
                    $item,
                    $dateRange,
                    $this->contractorId,
                    $this->contractorStatus,
                    $this->invoiceStatusId,
                    $this->searchModel
                );

                $content .= $this->render('_partial_statistic_item', [
                    'outerClass' => $data['outerClass'],
                    'innerClass' => $data['innerClass'],
                    'text' => $data['text'],
                    'count' => $info['count'],
                    'sum' => $info['sum'],
                    'url' => !empty($data['url'])? $data['url']: null,
                ]);
            }
        }

        return $content;
    }
}
