<?php

namespace frontend\modules\documents\widgets;

/**
 * Class PaymentOrderPaid
 * @package frontend\modules\documents\widgets
 */
class PaymentOrderPaid extends \yii\bootstrap\Modal
{
    public $header = '<h1>Выбранные платежки оплачены</h1>';
    public $toggleButton = [
        'tag' => 'a',
        'label' => 'Оплачено',
        'class' => 'btn btn-default btn-sm',
        'data-pjax' => 0,
    ];
    public $itemsContainer = '#payment-order-search tbody';
    public $pjaxContainer = '#payment-order-search-pjax';

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->getView()->registerJs(<<<JS
            $(document).on('click', '#{$this->getId()} button.confirm', function(e) {
                e.preventDefault();
                var modal = $('#{$this->getId()}');
                var ids = [];
                $('{$this->itemsContainer} tr:has(.joint-operation-checkbox:checked)').each(function() {
                    ids.push($(this).data('key'));
                });
                if (ids.length > 0) {
                    $.post($(this).data('url'), {id: ids, date: $('#date_of_payment', modal).val()}, function (data) {
                        $.pjax.reload('{$this->pjaxContainer}', {timeout : false});
                    });
                }
                $('.modal.in').modal('hide');
            });
JS
        );
    }

    /**
     * @return string
     */
    public function run()
    {
        echo $this->render('paymentOrderPaid');

        parent::run();
    }
}
