<?php

namespace frontend\modules\documents\widgets;

use yii\base\Widget;

class CostsStatisticsWidget extends Widget
{
    public function run()
    {
        return $this->render('costsStatisticsWidget');
    }
}
