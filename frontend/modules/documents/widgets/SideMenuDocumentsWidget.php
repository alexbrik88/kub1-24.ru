<?php

namespace frontend\modules\documents\widgets;

use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\document\ScanDocument;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\file\FileDir;
use common\models\product\Product;
use common\models\product\ProductSearch;
use frontend\models\Documents;
use frontend\modules\documents\assets\SideMenuDocumentsAsset;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use yii\helpers\Url;

/**
 * Class SideMenuWidget
 * @package frontend\widgets
 */
class SideMenuDocumentsWidget extends Widget
{
    public $allowedDirs;
    public $newFilesCount;

    public function init()
    {
        parent::init();

        $this->allowedDirs = FileDir::getEmployeeAllowedDirs();
        $this->newFilesCount = ScanDocument::getNewScansCount(Yii::$app->user->identity->company);
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        SideMenuDocumentsAsset::register($this->view);

        $session = Yii::$app->session;
        $sideBarStatus = !empty($session['sidebar']) ? $session['sidebar']['status'] : 0;

        return $this->render('sideMenuDocumentsWidget', [
            'items' => $this->getItems(),
            'newCompany' => new Company([
                'company_type_id' => CompanyType::TYPE_OOO,
                'scenario' => Company::SCENARIO_CREATE_COMPANY,
            ]),
            'sideBarStatus' => $sideBarStatus,
            'companyTaxation' => new CompanyTaxationType(),
            'dirs' => $this->allowedDirs
        ]);
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function getItems()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $module = Yii::$app->controller->module->id;
        $paramType = Yii::$app->request->getQueryParam('type');

        $companies = [];
        if (!Yii::$app->user->isGuest) {
            /* @var Employee $user */
            $user = Yii::$app->user->identity;
            $company = $user->currentEmployeeCompany->company;
            $companyName = $company->getTitle(true);
            $companies[$user->currentEmployeeCompany->company_id] = [
                'label' => !empty($companyName) ? $companyName : '(не задано)',
                'encode' => false,
                'url' => ['/'],
                'active' => false,
            ];
            $typeTable = CompanyType::tableName();
            $employeeCompanyArray = $user->getEmployeeCompany()
                ->joinWith('company')
                ->leftJoin($typeTable, "{{company}}.[[company_type_id]] = {{{$typeTable}}}.[[id]]")
                ->andWhere([
                    'employee_company.is_working' => true,
                    'company.blocked' => false,
                ])->orderBy([
                    new \yii\db\Expression("ISNULL({{{$typeTable}}}.[[name_short]])"),
                    "$typeTable.name_short" => SORT_ASC,
                    "company.name_short" => SORT_ASC,
                ])->all();
            /* @var EmployeeCompany $employeeCompany */
            foreach ($employeeCompanyArray as $employeeCompany) {
                if ($user->company_id !== $employeeCompany->company->id) {
                    $companyName = $employeeCompany->company->getTitle(true);
                    $companies[$employeeCompany->company->id] = [
                        'label' => !empty($companyName) ? $companyName : '(не задано)',
                        'encode' => false,
                        'url' => ['/site/change-company', 'id' => $employeeCompany->company->id],
                        'active' => false,
                        'linkOptions' => [
                            'class' => 'hidden-companies',
                        ],
                    ];
                }
            }
            $companies[] = [
                'label' => '<i class="glyphicon glyphicon-plus-sign"></i>Добавить',
                'encode' => false,
                'url' => '#create-company',
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle hidden-companies',
                    'data-toggle' => 'modal',
                ],
            ];
            $companyName = $company->getTitle(true);
            $currentCompanyMenuItem = !empty($companyName) ? $companyName : '(не задано)';

            return [
                [
                    'label' => '<i class="fa fa-gear multiuser-icon" style="opacity: 1;"></i>Мои компании<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'active' => false,
                    'items' => $companies,
                    'options' => [
                        'class' => 'nav-item_accordeon my-companies',
                    ],
                ],
                [
                    'label' => '<i class="icon-home"></i><span class="title">' . $currentCompanyMenuItem . '</span>',
                    'encode' => false,
                    'url' => ['/site/index'],
                    'active' => false,
                    'linkOptions' => [
                        'style' => 'height: auto;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;',
                        'title' => $currentCompanyMenuItem,
                    ],
                    'options' => [
                        'class' => 'current-company nav-item',
                        'id' => $user->company->id,
                    ],
                ],
                [
                    'label' => '<i style="padding-left:3px"><img src="/img/documents/arrow-left.png" width="16"/></i><span class="title">Назад в меню</span>',
                    'encode' => false,
                    'url' => Url::previous('UploadManagerReturn'),
                    'linkOptions' => [
                        'style' => 'height: auto;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;',
                        'title' => 'Назад в меню',
                    ],
                    'options' => [
                        'class' => 'nav-item',
                    ],
                ],
                [
                    'label' => '<i style="padding-left:3px"><img src="/img/documents/main-folder-2.png" width="17"/></i><span class="title">Документы</span><span class="arrow">',
                    'encode' => false,
                    'active' => true,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle documents-nav-link',
                    ],
                    'items' => [
                        [
                            'label' => '<i><img src="/img/documents/all-documents-2.png" width="17"/></i><span class="title">Все документы</span>',
                            'encode' => false,
                            'url' => ['/documents/upload-manager/all-documents'],
                        ],
                        [
                            'label' => '<i><img src="/img/documents/recognize-w.png" width="17"/></i><span class="title">Распознавание</span>' .
                                ($this->newFilesCount ? ('<span class="badge badge-blue">'.$this->newFilesCount.'</span>') : ''),
                            'encode' => false,
                            'url' => ['/documents/upload-manager/upload'],
                        ],
                        [
                            'label' => '<span class="title">Папки</span><span class="fa fa-plus">',
                            'encode' => false,
                            'url' => 'javascript:;',
                            'linkOptions' => [
                                'class' => 'nav-link nav-toggle add-folder',
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none active_menu open',
                    ],
                ],
            ];
        }

        return [];
    }
}
