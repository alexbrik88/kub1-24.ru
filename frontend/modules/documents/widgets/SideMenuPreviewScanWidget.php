<?php

namespace frontend\modules\documents\widgets;

use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\file\FileDir;
use frontend\models\Documents;
use frontend\modules\documents\assets\SideMenuPreviewScanAsset;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class SideMenuPreviewScanWidget
 * @package frontend\widgets
 */
class SideMenuPreviewScanWidget extends Widget
{
    protected $_company;
    protected $_employee;
    protected $_isChief;
    public $allowedDirsIds;

    public $filesIds;
    public $startFileId;
    public $isFullScreen;

    public function init()
    {
        parent::init();
        $this->_employee = Yii::$app->user->identity;
        $this->_company = Yii::$app->user->identity->company;
        $this->_isChief = Yii::$app->user->identity->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF;
        $this->allowedDirsIds = ArrayHelper::getColumn(FileDir::getEmployeeAllowedDirs(), 'id');
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        SideMenuPreviewScanAsset::register($this->view);

        return $this->render('sideMenuPreviewScanWidget', [
            'company' => $this->_company,
            'images' => $this->getImages(),
            'startFileId' => $this->startFileId,
            'isFullScreen' => $this->isFullScreen
        ]);
    }

    public function getImages()
    {
        $filesIds = $this->filesIds ?: Yii::$app->request->get('files');

        $query = File::find()->where([
            'company_id' => $this->_company->id,
            'id' => $filesIds,
        ]);
        if (!$this->_isChief) {
            $query->andWhere(['or', ['directory_id' => $this->allowedDirsIds], ['created_at_author_id' => $this->_employee->id]]);
        }
        $files = $query->asArray()->all();

        $images = [];
        foreach ($files as $file) {
            $images[] = [
                'id' => $file['id'],
                'ext' => $file['ext'],
                'filename_full' => $file['filename_full'],
                'filesize' => $file['filesize'],
                'src' => UploadManagerHelper::getImgPreviewUrl($file),
            ];
        }

        return $images;
    }
}
