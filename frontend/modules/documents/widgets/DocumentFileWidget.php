<?php

namespace frontend\modules\documents\widgets;

use common\models\document\AbstractDocument;
use frontend\modules\documents\assets\TooltipAsset;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * DocumentFileWidget
 */
class DocumentFileWidget extends Widget
{
    protected $_model;
    /**
     * additional css class
     * @var string|array
     */
    public $cssClass;
    /**
     * additional css style
     * @var string
     */
    public $cssStyle;

    public static $JS = <<<JS
    var initDocFileWidget = function(item, event) {
        $("a.file-link-tooltip", item).tooltipster({
            theme: ["tooltipster-kub"],
            contentCloning: true,
            contentAsHTML: true,
            trigger: "custom",
            side: ["right", "left", "top", "bottom"],
            content: '',
        });
    }
    $(document).ready(function() {
        $('.document-file-widget-inner').each(function() {
            var item = $(this).parent();
            item.load(item.data("url"), function() {

                // Preload Image
                url = item.find('.file-link-tooltip').attr('data-src');
                var img = new Image();
                img.src = url;

                initDocFileWidget(item);
            });
        });
    });

    //$(document).on("mouseenter click", ".document-file-widget-inner", function(e) {
    //    console.log(e);
    //    var item = $(this).parent();
    //    item.load(item.data("url"), function() {
    //        initDocFileWidget(item, e);
    //    });
    //});

    $(document).on("click", "a.file-link-tooltip.tooltipstered", function () {
        var link = $(this);
        link.tooltipster("content", $("#"+link.data("linksid")));
        link.removeClass("content-hover");
        link.addClass("contend-click");

        setTimeout(function () {
            link.tooltipster("show");
        }, 100);
    });
    $(document).on({
        mouseenter: function () {
            $('a.file-link-tooltip.tooltipstered').tooltipster('hide');
            var link = $(this);
            var src = link.data("src");
            if (!link.hasClass("content-hover")) {
                var viewid = link.data("viewid");
                if($("#"+viewid).length == 0) {
                    $('<img alt="" />').attr("id",viewid).attr("src",src).insertAfter("#"+link.data("linksid"));
                }
                link.tooltipster("content", $("#"+viewid));
                link.addClass("content-hover");
            }
            if (link.hasClass("contend-click")) {
                link.removeClass("contend-click");
            }
            if (src) {
                setTimeout(function () {
                    link.tooltipster("show");
                }, 100);
            }
        },
        mouseleave: function () {
            $('.tooltipstered.content-hover').tooltipster('hide');
        }
    }, "a.file-link-tooltip.tooltipstered");
    $(document).on('click', function() {
        $('a.file-link-tooltip.tooltipstered').tooltipster('hide');
    });
JS;

    public function run()
    {
        if ($this->model->has_file) {
            TooltipAsset::register($this->view);
            $this->view->registerJs(self::$JS, View::POS_READY, 'jsDocumentFileWidget');

            $content = Html::tag('span', '', [
                'class' => 'icon icon-paper-clip document-file-widget-inner',
                'style' => 'color: #5b9bd1; cursor: pointer;',
            ]);

            $options = [
                'class' => 'document-file-widget-item',
                'data' => [
                    'url' => Url::to([
                        "/documents/{$this->model->urlPart}/files",
                        'type' => $this->model->type,
                        'id' => $this->model->id,
                    ]),
                ],
            ];
            if ($this->cssClass) {
                Html::addCssClass($options, $this->cssClass);
            }
            if ($this->cssStyle) {
                $options['style'] = $this->cssStyle;
            }

            return Html::tag('span', $content, $options);
        }

        return '';
    }

    public function setModel(AbstractDocument $model)
    {
        $this->_model = $model;
    }

    public function getModel()
    {
        return $this->_model;
    }
}
