<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */
use common\components\date\DateHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\Invoice $model */
/* @var \common\models\employee\Employee $employee */
?>
<?= $model->company_name_short; ?>
<?= str_repeat('=', mb_strlen($model->company_name_short)); ?>


Здравствуйте!

Счёт № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $model->company->hasNds() && $model->company->hasViewNds()
    ? \common\components\TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2)
    : \common\components\TextHelper::invoiceMoneyFormat($model->total_amount_no_nds, 2); ?> р.
Ссылка на счёт: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/invoice/' . $model->uid]); ?>


--
С уважением,
<?= $employee->getFio(true); ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
