<?php

use common\components\date\DateHelper;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\Invoice $model */
/* @var \common\models\employee\Employee $employee */
?>

<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body style="margin: 0px">
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0" style="min-width:768px; font-family: sans-serif;">
    <tr>
        <td valign="top" align="center">
            
            <table width="800" border="0" cellspacing="0" cellpadding="0" class="campaign" style="background-color: #ffffff; border-width: 0; border-top-width: 0; border-left-width: 0; border-right-width: 0;  border-bottom: solid 4px #0077A7;">
                <tr>
                    <td width="768" align="left" valign="middle" style="padding-left:20px; font-family:Arial; font-size: 11px; color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:11px; padding-left: .8cm;" class="campaign">

                            <p style="font-size: 24px; padding-left: 0px; margin-top: 15px; line-height: 21px; margin-bottom: 5px; color: #000;"><?= $model->company_name_short; ?></p>
                    </td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" style="background-color: #f6f6f6; border: 20px solid #f6f6f6;">
                <tr>
                    <td align="center" width="768">
                        
                        <table cellspacing="0" cellpadding="0" width="768" style="background-color: #f6f6f6; border: 20px solid #f6f6f6;">
                            <tr style="border: 0;">
                                <td colspan="2" align="left"  class="headerimage" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff;">
                                    <p style="font-size: 24px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000; margin-left: 0.6cm">Здравствуйте!</p>
                                </td>
                            </tr>
                            <tr style="border: 0;">
                                <td colspan="2" align="left" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;  margin-left: 0.6cm;">Счет No <?= $model->fullNumber; ?>
                                        от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                        на сумму <?= $model->company->hasNds() && $model->company->hasViewNds()
                                            ? \common\components\TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2)
                                            : \common\components\TextHelper::invoiceMoneyFormat($model->total_amount_no_nds, 2); ?> р.</p>
                                </td>
                            </tr>
                            <tr style="border: 0;">
                                <td colspan="2" align="left" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;  margin-left: 0.6cm;">
                                        <?= Html::a('Ссылка на счет', Yii::$app->urlManager->createAbsoluteUrl(['/bill/invoice/' . $model->uid]), [
                                            'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7; padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
                                        ]); ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;  margin-left: 0.6cm;">С уважением,<br> <?= $employee->getFio(true); ?></p>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0" cellpadding="0" class="table" style="">

                            <tr>
                                <td height="20"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="80%" align="left" valign="top" style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:12px;" class="cell">
                                    <p style="margin-left: 0.6cm; margin-bottom: 10px;">Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ<br>
                                        <?= Html::a('Попробовать бесплатно', Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=vizitka', [
                                            'target' => '_blank',
                                            'style' => 'color: #055EC3;',
                                        ]); ?>
                                    </p>
                                </td>
                                <td width="20%" align="right" valign="middle" style="border-right: 20px solid #f6f6f6; font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;" class="campaign"><?= Html::a(Html::img($message->embed(Yii::getAlias('@frontend/email/company/checking-accountant/assets/logo.png')), [
                                        'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                                    ]), Yii::$app->params['serviceSite'], [
                                        'target' => '_blank',
                                    ]); ?></td>
                            </tr>
                            <tr>
                                <td height="7"></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
