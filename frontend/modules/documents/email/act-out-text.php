<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */
use common\components\date\DateHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\Act $model */
/* @var \common\models\employee\Employee $employee */
?>
<?= $model->invoice->company_name_short; ?>
<?= str_repeat('=', mb_strlen($model->invoice->company_name_short)); ?>


Здравствуйте!

Счёт № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $model->invoice->company->hasNds() && $model->invoice->company->hasViewNds()
    ? \common\components\TextHelper::invoiceMoneyFormat($model->getTotalAmount(true), 2)
    : \common\components\TextHelper::invoiceMoneyFormat($model->getTotalAmount(false), 2); ?> р.
Ссылка на акт: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/act/' . $model->uid]); ?>


--
С уважением,
<?= $employee->getFio(true); ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
