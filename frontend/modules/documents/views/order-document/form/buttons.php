<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2018
 * Time: 7:04
 */

use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $company Company */
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button'
                    . (($company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg create-invoice'
                    . (($company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <?php $cancelUrl = Url::previous('lastPage');
            $permission = $model->isNewRecord ?
                frontend\rbac\permissions\document\Document::INDEX :
                frontend\rbac\permissions\document\Document::VIEW;

            if (Yii::$app->user->can($permission, ['model' => $model])) {
                echo Html::a('Отменить', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]);
                echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]);
            } ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>