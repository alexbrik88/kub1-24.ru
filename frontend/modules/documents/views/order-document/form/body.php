<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 17:22
 */

use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\select2\Select2;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\Company;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 * @var $company Company
 */

$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => !$model->isNewRecord,
    'prompt' => '',
]; ?>
<?= Html::activeHiddenInput($model, 'production_type', [
    'class' => 'order_document-production-type-hidden',
    'value' => '',
]); ?>
<div class="row">
    <div class="col-xs-12 pad0 col-lg-7">
        <div class="col-xs-12 pad0" id="tooltip_requisites_block" style="width: 500px;">
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px">
                        <label class="control-label">
                            Покупатель
                            <span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div id="select-existing-contractor" class="col-sm-8"
                         style="max-width: 500px;">
                        <?= $form->field($model, 'contractor_id', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->widget(Select2::classname(), [
                            'data' => Contractor::getALLContractorList(Contractor::TYPE_CUSTOMER),
                            'options' => $contractorDropDownConfig,
                        ]); ?>
                    </div>
                    <?php if (!$model->isNewRecord && $model->checkContractor()): ?>
                        <?= ConfirmModalWidget::widget([
                            'toggleButton' => [
                                'label' => '<i class="icon-refresh refresh-contractor-invoice tooltip3" data-tooltip-content="#tooltip_refresh_contractor" style="right: -10px;"></i>',
                                'class' => '',
                                'tag' => 'i',
                            ],
                            'confirmUrl' => Url::to(['update-contractor-fields', 'id' => $model->id,]),
                            'confirmParams' => [],
                            'message' => 'Вы уверены, что хотите обновить информацию по покупателю в данном заказе?',
                        ]); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-xs-12" style="max-width: 590px">
                <div class="form-group required row">
                    <div class="col-xs-12 col-sm-4"
                         style="max-width: 175px">
                        <label class="control-label">
                            Основание
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-8"
                         style="max-width: 500px;">
                        <?= $this->render('basis_document', [
                            'model' => $model,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px">
                        <label class="control-label">
                            Склад
                            <span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div id="select-existing-contractor" class="col-sm-8"
                         style="max-width: 500px;">
                        <?= $form->field($model, 'store_id', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->widget(Select2::classname(), [
                            'data' => OrderDocument::getStoreArray(),
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="max-width: 590px">
                <!-- start -->
                <div class="row">
                    <div class="col-xs-12 pad0" style="margin-bottom: 15px; max-width: 328px">
                        <div class="col-xs-12" style="max-width: 175px;">
                            <label for="invoice-payment_limit_date" class="control-label">
                                Отгрузить до
                                <span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-xs-12" style="max-width: 153px;padding-left: 7px;">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= Html::activeTextInput($model, 'ship_up_to_date', [
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                    'value' => DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0"
                 style="max-width: 590px;margin-bottom: 15px;">
                <div class="col-xs-7 col-sm-4" style="max-width: 175px">
                    <label class="control-label" for="orderdocument-has_discount"
                           style="padding-top: 0px !important;">
                        Указать скидку:
                    </label>
                </div>
                <div class="col-xs-5 col-xs-8">
                    <?= Html::activeCheckbox($model, 'has_discount', [
                        'class' => 'form-control',
                        'label' => false,
                    ]); ?>
                </div>
            </div>
            <?php if ($company->companyTaxationType->usn) : ?>
                <div class="col-xs-12 pad0" style="max-width: 590px;margin-bottom: 15px;">
                    <div class="col-xs-7 col-sm-4" style="max-width: 175px">
                        <label class="control-label" style="padding-top: 0px !important;" for="orderdocument-has_nds">
                            Счет с НДС
                        </label>
                    </div>
                    <div class="col-xs-5 col-xs-8">
                        <?= Html::activeCheckbox($model, 'has_nds', [
                            'class' => 'form-control',
                            'label' => false,
                        ]); ?>
                    </div>
                </div>
                <?php $this->registerJs('
                $(document).on("change", "#orderdocument-has_nds", function() {
                    if ($(this).is(":checked")) {
                        $(".with-nds-item").removeClass("hidden");
                        $(".without-nds-item").addClass("hidden");
                    } else {
                        $(".with-nds-item").addClass("hidden");
                        $(".without-nds-item").removeClass("hidden");
                    }
                });
            ') ?>
            <?php endif; ?>
        </div>
    </div>
</div>