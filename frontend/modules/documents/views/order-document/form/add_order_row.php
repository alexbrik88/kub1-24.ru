<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 18:28
 */

use common\models\product\Product;
use frontend\models\Documents;
use frontend\rbac\permissions;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\models\Company;
use common\models\employee\Employee;

/* @var boolean $hasOrders */
/* @var boolean $hasDiscount */
/* @var boolean $ndsCellClass */
/* @var $company Company */
/* @var $user Employee */

$serviceCount = $company->getProducts()->byDeleted()->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_SERVICE,
])->notForSale(false)->count();
$goodsCount = $company->getProducts()->byDeleted()->andWhere([
    'production_type' => Product::PRODUCTION_TYPE_GOODS,
])->notForSale(false)->count();
$productCount = $serviceCount + $goodsCount;
$canAdd = Yii::$app->user->can(permissions\Product::CREATE);
?>
<tr id="from-new-add-row" class="from-new-add disabled-row"
    role="row" <?= $hasOrders ? 'style="display: none;"' : ''; ?> >
    <td class="">
        <span class="icon-close remove-product-from-order-document from-new"></span>
    </td>
    <td class="">
        <?php if (!$productCount && $canAdd) : ?>
            <div id="tooltip_add_product_block" style="position: relative">
                <?= Html::textInput('add_new', '', [
                    'class' => 'add-first-product-button add-modal-produc-new form-control',
                    'style' => 'width: 100%; background-color: #fff; cursor: pointer;',
                    'placeholder' => ' + Добавить новый товар/услугу',
                    'onfocus' => 'this.blur();',
                    'readonly' => true,
                ]) ?>
                <a style="position: absolute;top: 0;right:15px" class="tooltip-first-r"
                   data-tooltip-content="#tooltip_help_add_product">&nbsp;</a>

                <div class="not-clickable ">
                    &nbsp;
                </div>
            </div>
        <?php endif ?>
        <div class="add-exists-product<?= $productCount == 0 ? ' hidden' : '' ?>">
            <?= Select2::widget([
                'id' => 'order-add-select',
                'name' => 'addOrder',
                'initValueText' => '',
                'options' => [
                    'placeholder' => '',
                    'class' => 'form-control ',
                    'data-doctype' => Documents::IO_TYPE_OUT,
                ],
                'pluginOptions' => [
                    'allowClear' => false,
                    'minimumInputLength' => 1,
                    'dropdownCssClass' => 'product-search-dropdown',
                    'ajax' => [
                        'url' => "/product/search",
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'processResults' => new JsExpression('function(data, page) { return { results: data };}'),
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(product) { return product.text; }'),
                    'templateSelection' => new JsExpression('function (product) { return product.text; }'),
                ],
            ]); ?>
            <div id="order-add-static-items" style="display: none;">
                <ul class="order-add-static-items select2-results__options">
                    <?php if ($canAdd) : ?>
                        <li class="select2-results__option add-modal-product-new-order-document yellow" aria-selected="false">
                            [ + Добавить новый товар/услугу ]
                        </li>
                    <?php endif ?>
                    <li class="select2-results__option add-modal-services<?= $serviceCount == 0 ? ' hidden' : '' ?>"
                        aria-selected="false">
                        Перечень ваших услуг (<span class="service-count-value"><?= $serviceCount ?></span>)
                    </li>
                    <li class="select2-results__option add-modal-products<?= $goodsCount == 0 ? ' hidden' : '' ?>"
                        aria-selected="false">
                        Перечень ваших товаров (<span class="product-count-value"><?= $goodsCount ?></span>)
                    </li>
                </ul>
            </div>
            <script type="text/javascript">
                $('#order-add-select').on('select2:open', function (evt) {
                    var prodItems = $('#select2-order-add-select-results').parent().children('.order-add-static-items');
                    if (prodItems.length) {
                        prodItems.remove();
                    }
                    $('.order-add-static-items').clone().insertAfter('#select2-order-add-select-results');
                });
                $(document).on('mouseenter', '.select2-dropdown.product-search-dropdown li.select2-results__option', function () {
                    $('.product-search-dropdown li.select2-results__option').removeClass('select2-results__option--highlighted');
                    $(this).addClass('select2-results__option--highlighted');
                });
            </script>
        </div>
    </td>
    <td class="col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>"></td>
    <td class=""></td>
    <td class=""></td>
    <td class=""></td>
    <td class="col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>"></td>
    <td class="col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>"></td>
    <td class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>"></td>
    <td class="col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>"></td>
    <td class="<?= $ndsCellClass ?>"></td>
    <td class=""></td>
    <td class="discount_column<?= $hasDiscount ? '' : ' hidden'; ?>"></td>
    <td class="discount_column<?= $hasDiscount ? '' : ' hidden'; ?>"></td>
    <td class=""></td>
</tr>