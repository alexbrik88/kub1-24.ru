<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 18:03
 */

use common\models\document\OrderDocument;
use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use frontend\widgets\TableConfigWidget;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $company Company */
/* @var $user Employee */

$ndsCellClass = 'with-nds-item';
if ($model->has_nds == OrderDocument::HAS_NO_NDS) {
    $ndsCellClass .= ' hidden';
}
?>
<?php if (!($company->companyTaxationType->osno || $company->companyTaxationType->usn)) : ?>
    <style type="text/css">
        #table-for-order-document tr > th:nth-child(5),
        #table-for-order-document tr > td:nth-child(5) {
            display: none;
        }
    </style>
<?php endif ?>
<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-order-document',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
    ],
]); ?>
    <div class="row" id="widgets">
        <div class="col-md-12 col-sm-12">
            <div class="table-icons" style="margin-top: -20px;">
                <?= TableConfigWidget::widget([
                    'items' => [
                        ['attribute' => 'order_document_product_article',],
                        ['attribute' => 'order_document_product_reserve',],
                        ['attribute' => 'order_document_product_quantity',],
                        ['attribute' => 'order_document_product_weigh',],
                        ['attribute' => 'order_document_product_volume',],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <table id="table-for-order-document"
           class="table table-striped table-bordered table-hover account_table last-line-table">
        <thead>
        <tr class="heading" role="row">
            <th width="5%" tabindex="0" rowspan="1" colspan="1">
            </th>
            <th width="25%" tabindex="0" rowspan="1" colspan="1">
                Наименование
            </th>
            <th width="15%" class="col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                Артикул
            </th>
            <th width="10%" class="" tabindex="0" rowspan="1" colspan="1">
                Количество
            </th>
            <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                Ед.измерения
            </th>
            <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                Доступно
            </th>
            <th width="5%" class="col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                Резерв
            </th>
            <th width="5%" class="col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                Остаток
            </th>
            <th width="5%" class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                Вес (кг)
            </th>
            <th width="5%" class="col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>" tabindex="0" rowspan="1" colspan="1">
                Объем
            </th>
            <th width="10%" class="<?= $ndsCellClass ?>" tabindex="0" rowspan="1" colspan="1">
                НДС
            </th>
            <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                Цена
            </th>
            <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                rowspan="1" colspan="1">
                Скидка %
            </th>
            <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                rowspan="1" colspan="1">
                Цена со скидкой
            </th>
            <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                Сумма
            </th>
        </tr>
        </thead>
        <tbody id="table-product-list-body">
        <?php foreach ($model->orderDocumentProducts as $key => $orderDocumentProduct): ?>
            <?= $this->render('order_document_product_row', [
                'orderDocumentProduct' => $orderDocumentProduct,
                'number' => $key,
                'model' => $model,
                'ndsCellClass' => $ndsCellClass,
                'company' => $company,
                'user' => $user,
            ]); ?>
        <?php endforeach; ?>
        <?= $this->render('add_order_row', [
            'hasOrders' => (boolean)$model->orderDocumentProducts,
            'hasDiscount' => (boolean)$model->has_discount,
            'ndsCellClass' => $ndsCellClass,
            'company' => $company,
            'user' => $user,
        ]) ?>
        <tr class="template disabled-row" role="row">
            <td class="product-delete">
                <span class="icon-close remove-product-from-order-document"></span>
            </td>
            <td>
                <input type="text" class="product-title form-control tooltip-product" name="orderArray[][title]"
                       style="width: 100%;">
            </td>
            <td class="product-article col_order_document_product_article <?= $user->config->order_document_product_article ? null : 'hidden'; ?>"></td>
            <td>
                <input disabled="disabled" type="hidden" class="tax-rate"
                       value="0"/>

                <input disabled="disabled" type="hidden" class="product-model"
                       name="orderArray[][model]"
                       value="<?= Product::tableName(); ?>"/>
                <input disabled="disabled" type="hidden" class="product-id"
                       name="orderArray[][id]" value="0"/>
                <input disabled="disabled" type="number" min="0" step="any"
                       class="form-control product-count"
                       name="orderArray[][count]" value="1"/>
                <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
            </td>
            <td class="product-unit-name"></td>
            <td class="product-available"></td>
            <td class="product-reserve col_order_document_product_reserve <?= $user->config->order_document_product_reserve ? null : 'hidden'; ?>"></td>
            <td class="product-quantity col_order_document_product_quantity <?= $user->config->order_document_product_quantity ? null : 'hidden'; ?>"></td>
            <td class="product-weight col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>"></td>
            <td class="product-volume col_order_document_product_volume <?= $user->config->order_document_product_volume ? null : 'hidden'; ?>"></td>
            <td class="price-for-sell-nds-name <?= $ndsCellClass ?>"></td>
            <td class="price-one">
                <?= Html::input('number', 'orderArray[][price]', 0, [
                    'class' => 'form-control price-input',
                    'disabled' => 'disabled',
                    'min' => 0,
                    'step' => 'any',
                ]); ?>
            </td>
            <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                <?= Html::input('number', 'orderArray[][discount]', 0, [
                    'class' => 'form-control discount-input',
                    'disabled' => 'disabled',
                    'min' => 0,
                    'max' => 99.9999,
                    'step' => 'any',
                ]); ?>
            </td>
            <td class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                <span class="price-one-with-nds">0</span>
                <?= Html::hiddenInput('orderArray[][priceOneWithVat]', 0, [
                    'class' => 'form-control price-one-with-nds-input',
                    'disabled' => 'disabled',
                ]); ?>
            </td>
            <td class="price-with-nds" style="text-align: right;border-bottom: 1px solid #ddd;">0</td>
        </tr>
        <tr class="button-add-line" role="row">
            <td class="" colspan="" style="border-right: none;">
                <div class="portlet pull-left control-panel button-width-table">
                    <div class="btn-group pull-right" style="display: inline-block; padding-top: 7px">
                        <span class="btn yellow btn-add-line-table">
                            <i class="pull-left fa icon fa-plus-circle"></i>
                        </span>
                    </div>
                </div>
            </td>
            <td class="" colspan="8" style="padding-top: 12px;">
            </td>
        </tr>
        </tbody>
    </table>
<?php Pjax::end(); ?>