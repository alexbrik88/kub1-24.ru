<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 18:07
 */

use yii\bootstrap\Html;
use common\components\date\DateHelper;
use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use common\models\document\status\OrderDocumentStatus;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 */

?>
<div class="portlet-title">
    <div class="caption" style="width: 100%">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td valign="middle" style="width:1%; white-space:nowrap;">
                    <span>
                        <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                            'id' => 'isNewRecord',
                        ]) ?>

                        Заказ покупателя №

                        <?= Html::activeTextInput($model, 'document_number', [
                            'id' => 'account-number',
                            'data-required' => 1,
                            'class' => 'form-control',
                        ]); ?>

                        <br class="box-br">
                    </span>
                    <?= Html::activeTextInput($model, 'document_additional_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control invoice-block',
                        'placeholder' => 'доп. номер',
                        'style' => 'display: inline;',
                    ]); ?>
                    <span class="box-margin-top-t">от</span>

                    <div class="input-icon box-input-icon-top"
                         style="display: inline-block; vertical-align: top; margin-left: 6px; margin-right: 8px;">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'document_date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date' => '12-02-2012',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->document_date,
                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                    <select id="account-number" class="form-control order-document-status" name="OrderDocument[status_id]"
                            style="width: 150px;color: #ffffff;">
                        <?php /* @var $orderDocumentNumber OrderDocumentStatus */
                        foreach (OrderDocumentStatus::find()->all() as $orderDocumentNumber): ?>
                            <option data-color="<?= $orderDocumentNumber->color; ?>"
                                    value="<?= $orderDocumentNumber->id; ?>">
                                <?= $orderDocumentNumber->name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $this->registerJs('
    $(document).ready(function() {
        var $orderDocumentStatus = $(".order-document-status");
        $orderDocumentStatus.select2({
            theme: "krajee",
            templateResult: formatState,
            minimumResultsForSearch: -1,
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        var $select2container = $("#select2-account-number-container").closest(".select2-selection");
        $("#select2-account-number-container").closest(".select2").css("display", "inline-block");
        $select2container.css("background-color", $(".order-document-status option[value=" + $orderDocumentStatus.val() + "]").data("color"));

        function formatState (state) {
            if (!state.id) {
              return state.text;
            }
            var $orderDocumentStatusOption = $(".order-document-status option[value=" + state.id + "]");
            var $state = "<span class=\"order-document-status-option\" style=\"background-color: " + $orderDocumentStatusOption.data("color") + "\"></span>" + state.text

            return $state;
        };

        $orderDocumentStatus.on("select2:select", function (e) {
            $select2container.css("background-color", $(".order-document-status option[value=" + e.params.data.id + "]").data("color"));
        });
    });
'); ?>
