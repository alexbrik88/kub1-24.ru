<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.02.2018
 * Time: 17:46
 */

use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

$sum = $model->total_amount;
$discount = $model->getDiscountSum();
?>
<div class="portlet pull-right" id="invoice-sum">
    <table class="table table-resume">
        <tbody>
        <tr role="row" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
            <td><b>Сумма скидки:</b></td>
            <td id="discount_sum"><?= TextHelper::moneyFormatFromIntToFloat($discount); ?></td>
        </tr>
        <tr role="row">
            <td><b>Итого:</b></td>
            <td id="total_price"><?= TextHelper::moneyFormatFromIntToFloat($sum); ?></td>
        </tr>
        <tr class="col_order_document_product_weigh <?= $user->config->order_document_product_weigh ? null : 'hidden'; ?>" role="row">
            <td><b>Вес (кг):</b></td>
            <td id="total_mass">
                <?= $model->getTotalWeight(); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>