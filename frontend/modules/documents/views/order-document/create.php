<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 17:59
 */

use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use common\models\Company;
use common\models\employee\Employee;
use common\widgets\Modal;
use yii\widgets\Pjax;
use common\components\helpers\Html;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $company Company
 * @var $user Employee
 */

$this->title = 'Создать заказ';
$this->context->layoutWrapperCssClass = 'create-order-document';
$errorModelArray = [$model];
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'order-document-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
    ],
    'enableClientValidation' => false,
])); ?>
    <div class="form-body form-body_sml form-body-order-document">
        <?= $form->errorSummary($errorModelArray); ?>
        <div class="portlet">
            <?= $this->render('form/header', [
                'model' => $model,
                'form' => $form,
            ]); ?>
            <div class="portlet-body">
                <?= $this->render('form/body', [
                    'model' => $model,
                    'form' => $form,
                    'company' => $company,
                ]); ?>
                <div class="col-xs-12 pad0" style="margin-top: 5px !important">
                    <?= $this->render('form/product_table', [
                        'model' => $model,
                        'company' => $company,
                        'user' => $user,
                    ]); ?>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <?= $this->render('form/total_block', [
                            'model' => $model,
                            'user' => $user,
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('form/buttons', [
            'model' => $model,
            'company' => $company,
        ]); ?>
    </div>
<?php ActiveForm::end(); ?>

<?= $this->render('form/product_new_modal'); ?>

<?= $this->render('form/product_existed_modal', [
    'model' => $model,
]); ?>

<?= $this->render('form/remove_product_modal'); ?>

<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);
Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);
// content
Pjax::end();
Modal::end(); ?>

<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_refresh_contractor" style="display: inline-block; text-align: center;">
        Информация по покупателю была изменена.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
</div>
<?php $this->registerJs(<<<JS
    $(document).on('blur', '.product-count', function(){
        if ($(this).val() == 0 || $(this).val() == ''){
            $(this).val($(this).attr('data-value'));
            $(this).closest('tr').find('.price-with-nds').text(parseFloat($(this).attr('data-value') * $(this).closest('tr').find('.price-one-with-nds-input').val()).toFixed(2));
        } else {
           $(this).attr('data-value', $(this).val());
        }
    });
    $(document).on('blur','.product-title', function(){
        var name = $(this).val().replace(/\s+/g, '').length;
        if (name == 0) {
            $(this).closest('tr').remove();
            $('#from-new-add-row').show();
            ORDERDOCUMENT.recalculateOrderDocumentTable();
        }

    });
    $(document).on('click', '.product-title-clear', function(){
        $(this).closest('td').find('.product-title').val('');
        $(this).closest('td').find('.product-title').focus();
    });
    $(document).on("change", "#orderdocument-contractor_id", function() {
    var form = this.form;
    var pjax = $("#agreement-pjax-container", form);
    if (pjax.length && this.value != "add-modal-contractor") {
        $.pjax({
            url: pjax.data("url"),
            data: {contractorId: $(this).val()},
            container: "#agreement-pjax-container",
            push: false,
            timeout: 10000,
        });
    }
});
    $(document).on("change", "#orderdocument-agreement", function (e) {
        var value = $(this).val() || $(this).text();
        if (value == "add-modal-agreement") {
            e.preventDefault();
    
            $.pjax({
                url: "/documents/agreement/create?contractor_id=" + $("#orderdocument-contractor_id").val() + "&type=2&returnTo=order-document&container=agreement-select-container",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });
    
            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));
                $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);
    
                function dateChanged(ev) {
                    if (ev.bubbles == undefined) {
                        var input = $("[name='" + ev.currentTarget.name +"']");
                        if (ev.currentTarget.value == "") {
                            if (input.data("last-value") == null) {
                                input.data("last-value", ev.currentTarget.defaultValue);
                            }
                            var lastDate = input.data("last-value");
                            input.datepicker("setDate", lastDate);
                        } else {
                            input.data("last-value", ev.currentTarget.value);
                        }
                    }
                }
            });
            $("#agreement-modal-container").modal("show");
            $("#orderdocument-agreement").val("").trigger("change");
    }
    $("#agreement-pjax-container").on("pjax:complete", function() {
        if (window.AgreementValue) {
            $("#orderdocument-agreement").val(window.AgreementValue).trigger("change");
        }
    });
});
JS
    , $this::POS_READY);
