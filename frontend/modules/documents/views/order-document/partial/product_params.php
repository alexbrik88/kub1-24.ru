<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 19:26
 */

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use yii\helpers\ArrayHelper;

/* @var $model common\models\product\Product */
/* @var $thinFieldOptions Array */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
?>

<?= $form->field($model, 'code', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'box_type', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'count_in_place', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'place_count', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?= $form->field($model, 'mass_gross', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
    'disabled' => isset(ProductUnit::$countableUnits[$model->mass_gross]),
]); ?>

<?= $form->field($model, 'country_origin_id', $thinFieldOptions)
    ->dropDownList(ArrayHelper::map(Country::getCountries(), 'id', 'name_short')); ?>

<?= $form->field($model, 'customs_declaration_number', $thinFieldOptions)->textInput([
    'placeholder' => Product::DEFAULT_VALUE,
]); ?>

<?php
$massGrossId = \yii\helpers\Html::getInputId($model, 'mass_gross');
$productUnitId = \yii\helpers\Html::getInputId($model, 'product_unit_id');
$countableUnits = json_encode(array_keys(ProductUnit::$countableUnits));
$js = <<<JS
    var productMassGross = $('#$massGrossId');
    var countableUnits = $countableUnits;
    $('#$productUnitId').on('change init', '', function (e) {
        productMassGross.attr('disabled', countableUnits.indexOf(parseInt(this.value)) === -1);
    }).trigger('init');

JS;
$this->registerJs($js);
?>