<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 19:15
 */

use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\widgets\ProductGroupDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Company;

/* @var $model common\models\product\Product */
/* @var $isService bool */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $this yii\web\View */
/* @var $documentType integer */
/* @var $company Company */

$taxItems = ArrayHelper::map(TaxRate::sortedArray(), 'id', 'name');
$thinFieldOptions = [
    'wrapperOptions' => [
        'class' => 'col-md-6 inp_one_line-product',
    ],
];

if ($documentType == Documents::IO_TYPE_IN) {
    $checkbox = Html::label('Не для продажи ' . Html::activeCheckbox($model, 'not_for_sale', ['label' => false]), null, [
        'class' => 'control-label bold-text content-right',
        'style' => 'width: 100%; text-align: right !important;',
    ]);
    $notForSaleTemplate = "{label}\n{beginWrapper}\n<div class=\"row\">\n";
    $notForSaleTemplate .= "<div class=\"col-xs-6\">{input}</div>\n";
    $notForSaleTemplate .= "<div class=\"col-xs-6\">{$checkbox}</div>\n";
    $notForSaleTemplate .= "</div>\n{error}\n{hint}\n{endWrapper}\n";
    $notForSaleConf = [
        'template' => $notForSaleTemplate,
    ];
} else {
    $notForSaleConf = [];
}

$isService = ((string)$model->production_type == (string)Product::PRODUCTION_TYPE_SERVICE);

echo Html::activeHiddenInput($model, 'production_type', [
    'id' => 'production_type_input',
]);

echo $form->errorSummary($model);

$types = Product::$productionTypesOne;

unset($types[2]);

$types = array_reverse($types, true);

$this->registerJs('
$(document).on("change", "#product-not_for_sale", function() {
    $("input.for_sale_input").prop("disabled", this.checked);
    $("input.for_sale_input:radio:not(.md-radiobtn)").uniform("refresh");
    if (this.checked) {
        $("#product-price_for_sell_with_nds").val("");
        $(".field-product-price_for_sell_with_nds").removeClass("has-error");
        $(".field-product-price_for_sell_with_nds .help-block-error").html("");
    }
});
');
?>
<style>
    .radio_yellow input {
        display: none;
    }

    .radio_yellow input + label {
        cursor: pointer;
        border: 2px solid #FF9800;
        border-radius: 100px !important;
        font-size: 8px;
        padding: 3px !important;
        margin-left: 7px;
    }

    .radio_yellow input + label + span {
        margin-right: 13px;
        margin-left: 6px;
    }

    .radio_yellow input + label c {
        display: block;
        border-radius: 100px;
        padding: 3px;
    }

    .radio_yellow input:checked + label c {
        background-color: black;
    }
</style>
<?= $form->field($model, 'production_type')->label('Тип')->radioList($types, [
    //'class' => 'radio-list type lastItemMrgRight0',
    'item' => function ($index, $label, $name, $checked, $value) {
        return Html::label(Html::radio($name, $checked, ['value' => $value]) . ' ' . $label, null, [
            'class' => 'radio-inline m-l-20 control-label',
        ]);
    }
]); ?>
<?= $form->field($model, 'title')->textInput([
    'maxlength' => true,
]); ?>

<div class="forProduct<?= $model->production_type ? '' : ' selectedDopColumns' ?>">
    <?php
    if (!$isService) {
        if ($model->isNewRecord && $model->group_id === null) {
            $model->group_id = ProductGroup::WITHOUT;
        }
        echo $form->field($model, 'group_id', [
            'options' => [
                'class' => 'form-group'
            ],
        ])->widget(ProductGroupDropdownWidget::classname());
    } ?>
</div>
<?php if ($documentType == Documents::IO_TYPE_OUT) {
    echo $form->field($model, 'price_for_sell_with_nds', $thinFieldOptions)->textInput([
        'value' => $model->price_for_sell_with_nds == null ? '' :
            TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds),
        'class' => 'form-control js_input_to_money_format width100',
    ]);

    if ($company->companyTaxationType->osno || $company->companyTaxationType->usn) {
        echo $form->field($model, 'price_for_sell_nds_id')
            ->label()
            ->radioList($taxItems, [
                    'class' => 'radio-list lastItemMrgRight0',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                            'class' => 'radio-inline m-l-20 radio-block',
                        ]);
                    },
                ]
            );
    }
} else {
    echo $form->field($model, 'price_for_buy_with_nds', $thinFieldOptions)->textInput([
        'value' => $model->price_for_buy_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds),
        'class' => 'form-control js_input_to_money_format bbb',
    ]);

    echo $form->field($model, 'price_for_buy_nds_id')->label()->radioList($taxItems, [
        'class' => 'radio-list lastItemMrgRight0',
        'item' => function ($index, $label, $name, $checked, $value) {
            return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                'class' => 'radio-inline m-l-20 radio-block',
            ]);
        },
    ]);
} ?>
<?php $units = ProductUnit::findSorted()
    ->andWhere(($model->production_type == Product::PRODUCTION_TYPE_SERVICE) ? ['services' => 1] : ['goods' => 1])
    ->all();
$unitsOptions = [];
foreach ($units as $unit) {
    $unitsOptions[$unit->id] = ['title' => $unit->title];
}
echo $form->field($model, 'product_unit_id', [
    'options' => [
        'class' => 'form-group required'
    ],
])->widget(Select2::classname(), [
    'data' => ArrayHelper::map($units, 'id', 'name'),
    'options' => [
        'prompt' => '',
        'options' => $unitsOptions,
    ]
]); ?>

<?= Html::input('hidden', 'title', Url::toRoute('/product/get-units'), ['id' => 'get-product-units-url']) ?>

<div class="forProduct <?= $model->production_type ? '' : 'selectedDopColumns' ?>">
    <span class="addColumns">Дополнительные поля</span>

    <div class="dopColumns selectedDopColumns">
        <?php if ($documentType == Documents::IO_TYPE_IN) {
            echo $form->field($model, 'price_for_sell_with_nds', array_merge($thinFieldOptions, $notForSaleConf))->textInput([
                'value' => $model->price_for_sell_with_nds == null ? '' :
                    TextHelper::moneyFormatFromIntToFloat($model->price_for_sell_with_nds),
                'class' => 'form-control js_input_to_money_format for_sale_input ccc ',
                'disabled' => (boolean)$model->not_for_sale,
            ]);

            if ($company->hasNds()) {
                echo $form->field($model, 'price_for_sell_nds_id')->label()->radioList($taxItems, [
                    'class' => 'radio-list lastItemMrgRight0',
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $radio = Html::radio($name, $checked, [
                            'value' => $value,
                            'class' => 'for_sale_input',
                            'disabled' => (boolean)$model->not_for_sale,
                        ]);

                        return Html::tag('label', $radio . $label, [
                            'class' => 'radio-inline m-l-20 radio-block',
                        ]);
                    },
                ]);
            }
        } else {
            echo $form->field($model, 'price_for_buy_with_nds', $thinFieldOptions)
                ->textInput([
                        'value' => $model->price_for_buy_with_nds == null ? '' : TextHelper::moneyFormatFromIntToFloat($model->price_for_buy_with_nds),
                        'class' => 'form-control js_input_to_money_format width100',
                    ]
                );

            echo $form->field($model, 'price_for_buy_nds_id')->label()->radioList($taxItems, [
                'class' => 'radio-list lastItemMrgRight0',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                        'class' => 'radio-inline m-l-20 radio-block',
                    ]);
                },
            ]);
        }
        if (!$isService) {
            echo $this->render('product_params', [
                'model' => $model,
                'form' => $form,
                'thinFieldOptions' => $thinFieldOptions,
            ]);
        } ?>
    </div>
</div>
<script type="text/javascript">
    if (!$('#product-production_type input:checked').length) {
        $('#new-product-order-document-form input, #new-product-order-document-form select').attr('disabled', true);
        $('#new-product-order-document-form input[name="Product[production_type]"]').attr('disabled', false);
    }
</script>