<?php
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model Contractor */
?>
<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
<h1>Добавить <?= $model->type == Contractor::TYPE_SELLER ? 'поставщика' : 'покупателя'; ?></h1>