<?php
use common\models\product\Product;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\Company;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $company Company */

$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;
?>
<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'new-product-order-document-form',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-sm-4 col-md-5 control-label bold-text width-next-input',
        ],
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
])); ?>
    <div class="form-body form-horizontal">
        <?= Html::hiddenInput('documentType', $documentType); ?>
        <?= $this->render('main_product_form', [
            'model' => $model,
            'form' => $form,
            'documentType' => $documentType,
            'company' => $company,
        ]); ?>
        <div class="form-actions">
            <div class="row action-buttons">
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <?= Html::submitButton('Сохранить', [
                        'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs' : 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs'
                    ]) ?>
                    <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                        'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn btn-primary widthe-100 hidden-lg'
                    ]) ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                    <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal">
                        Отменить
                    </span>
                    <span class="btn darkblue widthe-100 hidden-lg" title="Отменить" data-dismiss="modal">
                        <i class="fa fa-reply fa-2x"></i>
                    </span>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
$("#new-product-order-document-form input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
$("#new-product-order-document-form input:radio:not(.md-radiobtn)").uniform();
</script>