<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2018
 * Time: 18:42
 */

use frontend\models\Documents;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\widgets\RangeButtonWidget;
use common\components\TextHelper;
use yii\widgets\ActiveForm;
use frontend\modules\documents\models\OrderDocumentSearch;
use common\models\document\OrderDocument;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use common\components\grid\DropDownDataColumn;
use frontend\widgets\TableConfigWidget;
use common\models\employee\Employee;

/* @var $this yii\web\View
 * @var $dashBoardData []
 * @var $searchModel OrderDocumentSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $user Employee
 */

$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);

$this->title = 'Заказы покупателей';
?>
<div class="portlet box">
    <div class="btn-group pull-right title-buttons">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
            'ioType' => Documents::IO_TYPE_OUT,
        ])
        ): ?>
            <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), [
                'class' => 'btn yellow',
            ]); ?>
        <?php endif; ?>
    </div>
    <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
</div>

<div class="row statistic-block" id="widgets">
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #dfba49; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[0]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?=$dashBoardData[0]['name']?>
            </div>
            <div class="stat-footer" style="background-color: #d4af46;">
                Количество заказов: <?= $dashBoardData[0]['count']; ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #55c797; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[1]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?=$dashBoardData[1]['name']?>
            </div>
            <div class="stat-footer" style="background-color: #26C281;">
                Количество заказов: <?= $dashBoardData[1]['count']; ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #45b6af; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[2]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?=$dashBoardData[2]['name']?>
            </div>
            <div class="stat-footer" style="background-color: #43a9a2;">
                Количество заказов: <?= $dashBoardData[2]['count']; ?>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-3">
        <div class="dashboard-stat" style="height: 122px; position: relative;">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
        <div class="table-icons" style="margin-top: -20px;">
            <?= TableConfigWidget::widget([
                'items' => [
                    ['attribute' => 'order_document_payment_sum',],
                    ['attribute' => 'order_document_ship_up_to_date',],
                    ['attribute' => 'order_document_stock_id',],
                    ['attribute' => 'order_document_invoice',],
                    ['attribute' => 'order_document_act',],
                    ['attribute' => 'order_document_packing_list',],
                    ['attribute' => 'order_document_invoice_facture',],
                    ['attribute' => 'order_document_responsible_employee_id',],
                ],
            ]); ?>
        </div>
    </div>
</div>

<div class="portlet box darkblue" id="invoices-table">
    <div class="portlet-title">
        <div class="caption">
            Список заказов
        </div>
        <div class="tools search-tools tools_button col-md-4 col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'placeholder' => 'Номер заказа или название контрагента',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
        <div class="actions joint-operations col-md-6 col-sm-6" style="display:none;">
            <?= $this->render('index/operations'); ?>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap invoice-table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('//layouts/grid/layout', [
                    'totalCount' => $dataProvider->totalCount,
                    'scroll' => true,
                ]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return Html::checkbox('OrderDocument[' . $model->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                                'data-contractor' => $model->contractor_id,
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'label' => '№ заказа',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'document_number link-view',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                                'model' => $model,
                            ]) ? Html::a($model->fullNumber, ['/documents/order-document/view', 'id' => $model->id,])
                                : $model->fullNumber;
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'label' => 'Дата заказа',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'link-view',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'contractor_id',
                        'label' => 'Контр&shy;агент',
                        'encodeLabel' => false,
                        'class' => DropDownSearchDataColumn::className(),
                        'contentOptions' => [
                            'class' => 'contractor-cell',
                            'style' => 'overflow: hidden;text-overflow: ellipsis;',
                        ],
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '30%',
                        ],
                        'filter' => FilterHelper::getContractorList(Documents::IO_TYPE_OUT, OrderDocument::tableName(), true, false, false),
                        'format' => 'raw',
                        'value' => function (OrderDocument $data) {
                            return '<span data-id="'.$data->contractor_id.'" title="' . htmlspecialchars($data->contractor_name_short) . '">' .
                                $data->contractor_name_short .
                                '</span>';
                        },
                    ],
                    [
                        'attribute' => 'total_amount',
                        'label' => 'Сумма',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'link-view',
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            $price = TextHelper::invoiceMoneyFormat($model->total_amount, 2);
                            return '<span class="price" data-price="'.str_replace(" ", "", $price).'">'.$price.'</span>';
                        },
                    ],
                    [
                        'label' => 'Оплачено',
                        'attribute' => 'paymentSum',
                        'headerOptions' => [
                            'class' => 'sorting col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_payment_sum' . ($user->config->order_document_payment_sum ? '' : ' hidden'),
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return $model->invoice ?
                                TextHelper::invoiceMoneyFormat($model->invoice->getPaidAmount(), 2) : '---';
                        },
                    ],
                    [
                        'attribute' => 'shipped',
                        'label' => 'Отгружено',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return $model->getShippedCount();
                        },
                    ],
                    [
                        'attribute' => 'reserve',
                        'label' => 'Резерв',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return $model->getOrderDocumentProducts()
                                ->sum('quantity') * 1 - $model->getShippedCount();
                        },
                    ],
                    [
                        'attribute' => 'status_id',
                        'label' => 'Статус',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'filter' => $searchModel->getStatusFilter(),
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return $model->status->name;
                        },
                    ],
                    [
                        'attribute' => 'ship_up_to_date',
                        'label' => 'План. дата отгрузки',
                        'headerOptions' => [
                            'class' => 'sorting col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_ship_up_to_date' . ($user->config->order_document_ship_up_to_date ? '' : ' hidden'),
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'class' => DropDownSearchDataColumn::className(),
                        'attribute' => 'store_id',
                        'label' => 'Со склада',
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_stock_id' . ($user->config->order_document_stock_id ? '' : ' hidden'),
                        ],
                        'filter' => $searchModel->getStoreFilter(),
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            return $model->store ? $model->store->name : '---';
                        },
                    ],
                    [
                        'attribute' => 'invoiceState',
                        'label' => 'Счет',
                        'class' => DropDownDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_invoice' . ($user->config->order_document_invoice ? '' : ' hidden'),
                            'style' => 'white-space: initial;text-align: center;',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getInvoiceFilter(),
                        'value' => function (OrderDocument $model) {
                            $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);

                            return $model->invoice ?
                                Html::a($model->invoice->fullNumber, Url::to(['/documents/invoice/view', 'id' => $model->invoice_id, 'type' => Documents::IO_TYPE_OUT])) :
                                Html::a('<div class="btn-group" style="display: inline-block;">
                                            <span class="btn yellow btn-add-line-table" style="padding: 5px 8px;margin-right: 0;">
                                                <i class="fa icon fa-plus-circle" style="padding-left: 0;font-size: 23px;"></i>
                                            </span>
                                        </div>', Url::to(['/documents/invoice/create-from-order',
                                    'type' => Documents::IO_TYPE_OUT,
                                    'orderDocumentID' => $model->id]), [
                                    'title' => 'Добавить счет',
                                    'class' => 'btn' . ($canCreate ? null : ' disabled no-rights-link'),
                                    'style' => 'padding: 0;margin: 0;',
                                ]);
                        },
                    ],
                    [
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'actState',
                        'label' => 'Акт',
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_act' . ($user->config->order_document_act ? '' : ' hidden'),
                            'style' => 'text-align: center;',
                        ],
                        'filter' => $searchModel->getActFilter(),
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            $content = '';
                            if ($model->invoice) {
                                foreach ($model->invoice->acts as $doc) {
                                    $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                                    $docLink = Html::a($doc->fullNumber, [
                                        '/documents/act/view',
                                        'type' => $doc->type,
                                        'id' => $doc->id,
                                    ], [
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]);
                                    $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                        '/documents/act/file-get',
                                        'type' => $model->invoice->type,
                                        'id' => $doc->id,
                                        'file-id' => $doc->file->id,
                                    ], [
                                        'target' => '_blank',
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]) : '';
                                    $content .= Html::tag('div', $docLink . $fileLink);
                                }
                                if ($model->invoice->getCanAddAct()) {
                                    $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model, 'type' => Documents::IO_TYPE_OUT]);
                                    $content .= Html::a('<div class="btn-group" style="display: inline-block;">
                                                            <span class="btn yellow btn-add-line-table" style="padding: 5px 8px;margin-right: 0;">
                                                                <i class="fa icon fa-plus-circle" style="padding-left: 0;font-size: 23px;"></i>
                                                            </span>
                                                        </div>', Url::to([
                                        '/documents/act/create',
                                        'type' => $model->invoice->type,
                                        'invoiceId' => $model->invoice->id,
                                    ]), [
                                        'class' => 'btn' . ($model->invoice->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                        'style' => 'padding: 0;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null) . ($content ? 'margin: 5px 0 0 0;' : 'margin: 0;'),
                                        'title' => 'Добавить акт',
                                    ]);
                                }
                            }

                            return $content;
                        },
                    ],
                    [
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'packingListState',
                        'label' => 'ТН',
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_packing_list' . ($user->config->order_document_packing_list ? '' : ' hidden'),
                            'style' => 'text-align: center;',
                        ],
                        'filter' => $searchModel->getPackingListFilter(),
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            $content = '';
                            if ($model->invoice) {
                                foreach ($model->invoice->packingLists as $doc) {
                                    $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                                    $docLink = Html::a($doc->fullNumber, [
                                        '/documents/packing-list/view',
                                        'type' => $doc->type,
                                        'id' => $doc->id,
                                    ], [
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]);
                                    $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                        '/documents/packing-list/file-get',
                                        'type' => $model->invoice->type,
                                        'id' => $doc->id,
                                        'file-id' => $doc->file->id,
                                    ], [
                                        'target' => '_blank',
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]) : '';
                                    $content .= Html::tag('div', $docLink . $fileLink);
                                }
                                if ($model->invoice->getCanAddPackingList()) {
                                    $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                                    $content .= Html::a('<div class="btn-group" style="display: inline-block;">
                                                            <span class="btn yellow btn-add-line-table" style="padding: 5px 8px;margin-right: 0;">
                                                                <i class="fa icon fa-plus-circle" style="padding-left: 0;font-size: 23px;"></i>
                                                            </span>
                                                        </div>', Url::to([
                                        '/documents/packing-list/create',
                                        'type' => $model->invoice->type,
                                        'invoiceId' => $model->invoice->id,
                                    ]), [
                                        'title' => 'Добавить ТН',
                                        'class' => 'btn' . ($model->invoice->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                        'style' => 'padding: 0;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : null) . ($content ? 'margin: 5px 0 0 0;' : 'margin: 0;'),
                                    ]);
                                }
                            }

                            return $content;
                        }
                    ],
                    [
                        'class' => DropDownDataColumn::className(),
                        'attribute' => 'invoiceFactureState',
                        'label' => 'Счёт-фактура',
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_invoice_facture' . ($user->config->order_document_invoice_facture ? '' : ' hidden'),
                            'style' => 'text-align: center;',
                        ],
                        'filter' => $searchModel->getInvoiceFactureFilter(),
                        'format' => 'raw',
                        'value' => function (OrderDocument $model) {
                            $content = '';
                            if ($model->has_nds == OrderDocument::HAS_NO_NDS) {
                                return $content;
                            }
                            if ($model->invoice) {
                                foreach ($model->invoice->invoiceFactures as $doc) {
                                    $canView = Yii::$app->user->can(permissions\document\Document::VIEW, ['model' => $doc,]);
                                    $docLink = Html::a($doc->fullNumber, [
                                        '/documents/invoice-facture/view',
                                        'type' => $doc->type,
                                        'id' => $doc->id,
                                    ], [
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]);

                                    $fileLink = $doc->file ? Html::a('<span class="pull-right icon icon-paper-clip"></span>', [
                                        '/documents/invoice-facture/file-get',
                                        'type' => $model->invoice->type,
                                        'id' => $doc->id,
                                        'file-id' => $doc->file->id,
                                    ], [
                                        'target' => '_blank',
                                        'class' => $canView ? '' : 'no-rights-link',
                                    ]) : '';

                                    $content .= Html::tag('div', $docLink . $fileLink);
                                }
                                if ($model->invoice->getCanAddInvoiceFacture()) {
                                    $canCreate = Yii::$app->user->can(permissions\document\Document::CREATE, ['model' => $model,]);
                                    $content .= Html::a('<div class="btn-group" style="display: inline-block;">
                                                            <span class="btn yellow btn-add-line-table" style="padding: 5px 8px;margin-right: 0;">
                                                                <i class="fa icon fa-plus-circle" style="padding-left: 0;font-size: 23px;"></i>
                                                            </span>
                                                        </div>', Url::to([
                                        '/documents/invoice-facture/create',
                                        'type' => $model->invoice->type,
                                        'invoiceId' => $model->invoice->id,
                                    ]), [
                                        'class' => 'btn' . ($model->invoice->isRejected ? ' disabled' : '') . ($canCreate ? '' : ' no-rights-link'),
                                        'style' => 'padding: 0;' . ($model->invoice->isRejected ? 'background-color: #a2a2a2;' : '') . ($content ? 'margin: 5px 0 0 0;' : 'margin: 0;'),
                                        'title' => 'Добавить СФ',
                                    ]);
                                }
                            }

                            return $content;
                        },
                    ],
                    [
                        'attribute' => 'author_id',
                        'label' => 'Ответственный',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                            'width' => '15%',
                        ],
                        'contentOptions' => [
                            'class' => 'col_order_document_responsible_employee_id' . ($user->config->order_document_responsible_employee_id ? '' : ' hidden'),
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'filter' => $searchModel->getAuthorFilter(),
                        'value' => function (OrderDocument $model) {
                            return $model->author ? $model->author->getFio(true) : $model->contractor_name_short;
                        },
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
<?=\frontend\modules\documents\widgets\SummarySelectWidget::widget();?>

<?php if ($canSend): ?>
    <?= $this->render('@frontend/modules/documents/views/invoice/view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_ORDER
    ]); ?>
<?php endif; ?>
