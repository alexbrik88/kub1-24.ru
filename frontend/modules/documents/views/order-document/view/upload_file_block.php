<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.02.2018
 * Time: 16:19
 */

use yii\bootstrap\Html;
use common\models\document\OrderDocument;
use common\components\date\DateHelper;
use common\models\file\widgets\FileUpload;
use yii\helpers\Url;
use frontend\models\Documents;
use frontend\rbac\permissions;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
?>
<div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
    <div class="portlet">
        <div class="customer-info" style="min-height:auto!important;">
            <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                <table class="table no_mrg_bottom">
                    <tr>
                        <td>
                            <span class="customer-characteristic">
                                Покупатель:
                            </span>
                            <span>
                                <?= Html::a($model->contractor->getTitle(true), [
                                    '/contractor/view',
                                    'type' => $model->contractor->type,
                                    'id' => $model->contractor->id,
                                ]); ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span
                                class="customer-characteristic">Отгрузить до:</span>
                            <span><?= DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                                <span class="customer-characteristic">
                                        <?= $model->agreementType ? $model->agreementType->name : 'Договор'; ?>:
                                </span>
                                № <?= Html::encode($model->basis_document_number); ?>
                                от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            <?php endif ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-bottom: -5px;">
                                <?= FileUpload::widget([
                                    'uploadUrl' => Url::to(['file-upload', 'type' => Documents::IO_TYPE_OUT, 'id' => $model->id,]),
                                    'deleteUrl' => Url::to(['file-delete', 'type' => Documents::IO_TYPE_OUT, 'id' => $model->id,]),
                                    'listUrl' => Url::to(['file-list', 'type' => Documents::IO_TYPE_OUT, 'id' => $model->id,]),
                                ]); ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="">
            <div style="margin: 15px 0 0;">
                <span style="font-weight: bold;">Комментарий</span>
                <?= Html::tag('span', '', [
                    'id' => 'comment_internal_update',
                    'class' => 'glyphicon glyphicon-pencil',
                    'style' => 'cursor: pointer;',
                ]); ?>
            </div>
            <div id="comment_internal_view" class="">
                <?= Html::encode($model->comment_internal) ?>
            </div>
            <?php if ($canUpdate) : ?>
                <?= Html::beginTag('div', [
                    'id' => 'comment_internal_form',
                    'class' => 'hidden',
                    'style' => 'position: relative;',
                    'data-url' => Url::to(['comment-internal', 'id' => $model->id]),
                ]) ?>
                <?= Html::tag('i', '', [
                    'id' => 'comment_internal_save',
                    'class' => 'fa fa-floppy-o',
                    'style' => 'position: absolute; top: 1px; right: 3px; cursor: pointer; font-size: 20px;',
                ]); ?>
                <?= Html::textarea('comment_internal', $model->comment_internal, [
                    'id' => 'comment_internal_input',
                    'rows' => 3,
                    'maxlength' => true,
                    'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                ]); ?>
                <?= Html::endTag('div') ?>
            <?php endif ?>
        </div>
    </div>
</div>
<?php if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });
    ');
}; ?>
