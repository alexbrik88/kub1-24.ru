<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.02.2018
 * Time: 17:03
 */

use common\models\document\OrderDocument;
use frontend\rbac\permissions;
use yii\bootstrap\Html;
use yii\bootstrap\Dropdown;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\models\document\status\OrderDocumentStatus;
use frontend\models\Documents;

/* @var $this yii\web\View
 * @var $model OrderDocument
 */

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);
?>
<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if ($canUpdate && !$model->is_deleted): ?>
            <?= Html::button('Отправить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger',
            ]); ?>
            <?= Html::button('<span class="ico-Send-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg send-message-panel-trigger',
                'title' => 'Отправить',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT, 'filename' => $model->getPrintTitle(),];
        echo Html::a('Печать', $printUrl, [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0;
                text-align: center;
            }
        </style>
        <span class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => Documents::IO_TYPE_OUT,],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE)): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать этот заказ?',
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="fa fa-files-o fa-2x"></i>',
                    'title' => 'Копировать',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['copy', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите скопировать этот заказ?',
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && $canUpdate): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['update-status', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите отклонить этот заказ?',
                'confirmParams' => [
                    'status_id' => OrderDocumentStatus::STATUS_CANCELED,
                ],
            ]); ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<span class="ico-Denied-smart-pls fs"></span>',
                    'title' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['update-status', 'id' => $model->id,]),
                'message' => 'Вы уверены, что хотите отклонить этот заказ?',
                'confirmParams' => [
                    'status_id' => OrderDocumentStatus::STATUS_CANCELED,
                ],
            ]); ?>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)): ?>
            <?= Html::button('Удалить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
            ]); ?>
            <?= Html::button('<i class="fa fa-trash-o fa-2x"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'data-toggle' => 'modal',
                'href' => '#delete-confirm',
                'title' => 'Удалить',
            ]); ?>
        <?php endif; ?>
    </div>
</div>

<?php if ($canUpdate && !$model->is_deleted): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => false,
    ]); ?>
<?php endif; ?>

<?php if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить этот заказ?",
    ]);
}; ?>
