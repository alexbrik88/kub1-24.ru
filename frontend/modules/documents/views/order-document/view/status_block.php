<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 7:48
 */

use common\models\product\Product;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\document\status\OrderDocumentStatus;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$status = $model->status;

$productionTypeArray = explode(', ', $model->production_type);
$hasInvoice = $model->getInvoice()->exists();
if ($hasInvoice) {
    $serviceCountActs = $model->invoice->getActs()->joinWith('orderActs')->select('SUM({{order_act}}.[[quantity]])')->scalar();
    $serviceCountInvoice = $model->invoice->getOrders()->joinWith('product')->andWhere([
        'product.production_type' => Product::PRODUCTION_TYPE_SERVICE,
    ])->select('SUM({{order}}.[[quantity]])')->scalar();

    $productCountPL = $model->invoice->getPackingLists()->joinWith('orderPackingLists')->select('SUM({{order_packing_list}}.[[quantity]])')->scalar();
    $productCountInvoice = $model->invoice->getOrders()->joinWith('product')->andWhere([
        'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
    ])->select('SUM({{order}}.[[quantity]])')->scalar();
}
?>
<div class="control-panel col-xs-12 pad0 pull-right">
    <div class="status-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg order-document_status_date"
                 style="padding-left:0; padding-right:0;text-align: center;background-color: <?= $model->status->color; ?>"
                 title="Дата изменения статуса">
                <?= date("d.m.Y", $model->status_updated_at); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-12 pad3">
                <select id="account-number" class="form-control order-document-status" name="OrderDocument[status_id]"
                        data-url="<?= Url::to(['update-status', 'id' => $model->id]); ?>"
                        style="color: #ffffff;width: 100%;background-color: <?= $model->status->color; ?>">
                    <?php /* @var $orderDocumentStatus OrderDocumentStatus */
                    foreach (OrderDocumentStatus::find()->all() as $orderDocumentStatus): ?>
                        <option <?= $model->status_id == $orderDocumentStatus->id ? 'selected' : null; ?>
                            data-color="<?= $orderDocumentStatus->color; ?>"
                            value="<?= $orderDocumentStatus->id; ?>">
                            <?= $orderDocumentStatus->name; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
        <div class="col-xs-3"> &nbsp; </div>
        <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
            <div class="document-panel btn-group tooltip2" style="width: 99.9% !important; margin-bottom: 0px;">
                <?php if (!$model->invoice): ?>
                    <?= $this->render('status_rows/_first_row', [
                        'document' => 'invoice',
                        'title' => 'Счет',
                        'model' => $model,
                        'action' => 'create-from-order',
                    ]); ?>
                <?php else: ?>
                    <?= $this->render('status_rows/_first_row', [
                        'document' => 'invoice',
                        'title' => $model->canUpdateInvoice() ? 'Обновить счет' : 'Счет',
                        'model' => $model,
                        'action' => $model->canUpdateInvoice() ? 'update-from-order' : 'view',
                        'id' => $model->invoice->id,
                    ]); ?>
                <?php endif; ?>
                <?php if (in_array(Product::PRODUCTION_TYPE_SERVICE, $productionTypeArray) && $hasInvoice): ?>
                    <?php if (!$model->invoice->acts): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'act',
                            'title' => 'акт',
                            'model' => $model,
                            'action' => 'create',
                            'id' => 0,
                        ]);
                        ?>
                    <?php elseif (count($model->invoice->acts) == 1 && $serviceCountActs == $serviceCountInvoice): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'act',
                            'title' => 'акт',
                            'model' => $model,
                            'action' => 'view',
                            'id' => $model->invoice->acts[0]->id,
                        ]);
                        ?>
                    <?php else: ?>
                        <?= $this->render('status_rows/_many_rows', [
                            'document' => 'act',
                            'title' => 'акт',
                            'model' => $model,
                            'addAvailable' => $serviceCountInvoice != $serviceCountActs,
                        ]); ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray) && $hasInvoice): ?>
                    <?php if (!$model->invoice->packingLists): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'action' => 'create',
                            'id' => 0,
                        ]);
                        ?>
                    <?php elseif (count($model->invoice->packingLists) == 1 && $productCountPL == $productCountInvoice): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'action' => 'view',
                            'id' => $model->invoice->packingLists[0]->id,
                        ]);
                        ?>
                    <?php else: ?>
                        <?= $this->render('status_rows/_many_rows', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'addAvailable' => $productCountPL != $productCountInvoice,
                        ]);
                        ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $this->registerJs('
    $(document).ready(function() {
        var $orderDocumentStatus = $(".order-document-status");
        $orderDocumentStatus.select2({
            theme: "krajee",
            templateResult: formatState,
            minimumResultsForSearch: -1,
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        var $select2container = $("#select2-account-number-container").closest(".select2-selection");
        $("#select2-account-number-container").closest(".select2").css("display", "inline-block");
        $select2container.css("background-color", $(".order-document-status option[value=" + $orderDocumentStatus.val() + "]").data("color"));

        function formatState (state) {
            if (!state.id) {
              return state.text;
            }
            var $orderDocumentStatusOption = $(".order-document-status option[value=" + state.id + "]");
            var $state = "<span class=\"order-document-status-option\" style=\"background-color: " + $orderDocumentStatusOption.data("color") + "\"></span>" + state.text

            return $state;
        };

        $orderDocumentStatus.on("select2:select", function (e) {
            var $color = $(".order-document-status option[value=" + e.params.data.id + "]").data("color");
            $select2container.css("background-color", $color);
            $(".order-document_status_date").css("background-color", $color);
            $.post($orderDocumentStatus.data("url"), {status_id: e.params.data.id}, function ($data) {
                if ($data.result == true) {
                    $(".order-document_status_date").text($data.date);
                }
            });
        });
    });

'); ?>
