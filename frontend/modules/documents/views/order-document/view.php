<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.02.2018
 * Time: 6:07
 */

use common\models\document\OrderDocument;
use yii\bootstrap\Html;
use yii\helpers\Url;
use php_rutils\RUtils;
use frontend\rbac\permissions;
use common\models\employee\Employee;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $user Employee
 * @var $type integer
 */

$dateFormatted = RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
$this->context->layoutWrapperCssClass = 'order-document-view out-document';

$this->title = 'Заказ №' . $model->fullNumber . ' от ' . $dateFormatted;
$previousOrderDocument = $model->getPreviousOrderDocument();
$nextOrderDocument = $model->getNextOrderDocument();
?>
<div class="page-content-in" style="padding-bottom: 30px;">
    <div style="margin-top: -13px;">
        <?= Html::a('Назад к списку', Url::to(['index', 'type' => $type]), [
            'class' => 'back-to-customers',
            'style' => 'display: inline-block;',
        ]); ?>
        <div class="change-order-document">
            <?php if ($previousOrderDocument !== null): ?>
                <span class="fa fa-long-arrow-left previous"
                      data-url="<?= Url::to(['view', 'id' => $previousOrderDocument->id]); ?>"></span>
            <?php endif; ?>
            <?php if ($nextOrderDocument !== null): ?>
                <span class="fa fa-long-arrow-right next"
                      data-url="<?= Url::to(['view', 'id' => $nextOrderDocument->id]); ?>"></span>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($model->is_deleted): ?>
        <h1 class="text-warning">Заказ удалён</h1>
    <?php endif; ?>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-7 pad0">
            <?= $this->render('view/main_info', [
                'model' => $model,
                'dateFormatted' => $dateFormatted,
                'user' => $user,
            ]); ?>
        </div>
        <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
            <div class="col-xs-12" style="padding-right:0px !important;">
                <?= $this->render('view/status_block', [
                    'model' => $model,
                ]); ?>
                <?= $this->render('view/upload_file_block', [
                    'model' => $model,
                    'dateFormatted' => $dateFormatted,
                ]); ?>
            </div>
        </div>
    </div>
    <div id="buttons-bar-fixed">
        <?= $this->render('view/action_buttons', [
            'model' => $model,
        ]); ?>
    </div>
</div>
