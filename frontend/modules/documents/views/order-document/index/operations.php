<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 20.02.2018
 * Time: 7:54
 */

use frontend\rbac\permissions;
use frontend\models\Documents;
use yii\bootstrap\Html;
use common\models\document\status\OrderDocumentStatus;
use common\components\date\DateHelper;
use yii\helpers\Url;

/* @var $orderDocumentStatus OrderDocumentStatus
 */

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => Documents::IO_TYPE_OUT]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = Yii::$app->getUser()->can(permissions\document\Document::UPDATE_STATUS) &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = Yii::$app->getUser()->can(permissions\document\Document::VIEW);
?>
<?php if ($canIndex || $canCreate) : ?>
    <div class="dropdown">
        <?= Html::a('Создать  <span class="caret"></span>', null, [
            'class' => 'btn btn-default btn-sm dropdown-toggle',
            'id' => 'dropdownMenu1',
            'data-toggle' => 'dropdown',
            'aria-expanded' => true,
            'style' => 'height: 28px;',
        ]); ?>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right: -30px;left: auto;top: 28px;">
            <li>
                <?= Html::a('Счет', '#many-create-invoice', [
                    'data-toggle' => 'modal',
                ]); ?>
            </li>
            <li>
                <?= Html::a('Акт', '#many-create-act', [
                    'data-toggle' => 'modal',
                ]); ?>
            </li>
            <li>
                <?= Html::a('ТН', '#many-create-packing-list', [
                    'data-toggle' => 'modal',
                ]); ?>
            </li>
            <li>
                <?= Html::a('УПД', '#many-create-upd', [
                    'data-toggle' => 'modal',
                ]); ?>
            </li>
        </ul>
    </div>
    <div class="dropdown">
        <?= Html::a('Статус  <span class="caret"></span>', null, [
            'class' => 'btn btn-default btn-sm dropdown-toggle',
            'id' => 'dropdownMenu2',
            'data-toggle' => 'dropdown',
            'aria-expanded' => true,
            'style' => 'height: 28px;',
        ]); ?>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2" style="right: 100px!important;left: auto;top: 28px;">
            <?php foreach (OrderDocumentStatus::find()->all() as $orderDocumentStatus): ?>
                <li>
                    <?= Html::a($orderDocumentStatus->name, null, [
                        'data-url' => Url::to(['many-update-status', 'statusID' => $orderDocumentStatus->id]),
                    ]); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>
<?php if ($canDelete) : ?>
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
        'class' => 'btn btn-default btn-sm',
        'data-toggle' => 'modal',
    ]); ?>
<?php endif ?>
<?php if ($canCreate) : ?>
    <div id="many-create-invoice" class="confirm-modal fade modal"
         role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label for="under-date" class="col-md-3 col-sm-3 control-label label-acts-establish1">
                                    Счета создать от даты:
                                </label>

                                <div class="col-md-3 col-sm-3 inp_one_line_min cal-acts-establish">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::textInput('Invoice[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                            'id' => 'under-date',
                                            'class' => 'form-control date-picker modal-document-date',
                                            'data-date-viewmode' => 'years',
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-3">
                            <?= Html::a('Создать', null, [
                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                'data-url' => Url::to(['/documents/invoice/many-create', 'type' => Documents::IO_TYPE_OUT]),
                                'style' => 'width: 100px;',
                            ]); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal" style="width: 100px;">Отменить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="many-create-act" class="confirm-modal fade modal"
         role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label for="under-date" class="col-md-3 col-sm-3 control-label label-acts-establish1">
                                    Акты создать от даты:
                                </label>

                                <div class="col-md-3 col-sm-3 inp_one_line_min cal-acts-establish">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                            'id' => 'under-date',
                                            'class' => 'form-control date-picker modal-document-date',
                                            'data-date-viewmode' => 'years',
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-3">
                            <?= Html::a('Создать', null, [
                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                'data-url' => Url::to(['/documents/act/many-create', 'type' => Documents::IO_TYPE_OUT]),
                                'style' => 'width: 100px;',
                            ]); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal" style="width: 100px;">Отменить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="many-create-packing-list" class="confirm-modal fade modal"
         role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label for="under-date"
                                       class="col-md-4 col-sm-4 control-label label-acts-establish2">Товарные Накладные
                                    создать от даты:</label>

                                <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                            'id' => 'under-date',
                                            'class' => 'form-control date-picker modal-document-date',
                                            'data-date-viewmode' => 'years',
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-3">
                            <?= Html::a('Создать', null, [
                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => Documents::IO_TYPE_OUT]),
                                'style' => 'width: 100px;',
                            ]); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal" style="width: 100px;">Отменить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="many-create-upd" class="confirm-modal fade modal"
         role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label for="under-date" class="col-md-2 control-label label-acts-establish3">
                                    УПД создать от даты:
                                </label>

                                <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::textInput('documentDate', date(DateHelper::FORMAT_USER_DATE), [
                                            'class' => 'form-control date-picker modal-document-date',
                                            'data-date-viewmode' => 'years',
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-3">
                            <?= Html::a('Создать', null, [
                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                'data-url' => Url::to(['/documents/upd/many-create', 'type' => Documents::IO_TYPE_OUT]),
                                'style' => 'width: 100px;',
                            ]); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3">
                            <button type="button" class="btn darkblue" data-dismiss="modal" style="width: 100px;">
                                Отменить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($canDelete) : ?>
    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
         tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -51.5px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">Вы уверены, что хотите удалить
                            выбранные заказы?
                        </div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                'data-url' => Url::to(['many-delete']),
                                'data-style' => 'expand-right',
                            ]); ?>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn darkblue"
                                    data-dismiss="modal">НЕТ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($canSend) : ?>
    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
        'class' => 'btn btn-default btn-sm document-many-send',
        'data-url' => Url::to(['many-send']),
    ]); ?>
    <div class="modal fade confirm-modal" id="many-send-error"
         tabindex="-1"
         role="modal"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-hidden="true"></button>
                    <h3 style="text-align: center; margin: 0">Ошибка
                        при отправке счетов</h3>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
<?php if ($canPrint) : ?>
    <?= Html::a('<i class="fa fa-print"></i> Печать', [
        'many-document-print',
        'actionType' => 'pdf',
        'type' => Documents::IO_TYPE_OUT,
        'multiple' => '',
    ], [
        'class' => 'btn btn-default btn-sm multiple-print',
        'target' => '_blank',
    ]); ?>
<?php endif ?>
