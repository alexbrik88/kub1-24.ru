<?php

use common\models\document\status\InvoiceStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\Documents;

/** @var $this View */
/** @var \common\models\document\Invoice $model */
/* @var $useContractor boolean */
/* @var $showSendPopup integer */

$contractorId = $useContractor ? $model->contractor_id : null;

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);
?>

<div class="container-first-account-table" style="display: none">

    <div class="container-add-logo-first-account-table">
        <div class="file_upload">
            <button type="button">+ Добавить логотип</button>
            <div>Файл не выбран</div>
            <input type="file">
        </div>
    </div>

    <div class="clr"></div>

    <div class="page-content-in pad-pdf-p">

        <table class="table table-bordered t-p m-t-m table-receipt-first-account">

            <tr>
                <td colspan="2"
                    style="border-bottom: none"><input type="text" class="border-input-gray"></td>
                <td style="text-align: left; padding-left: 2px; width: 8%; vertical-align: top;">
                    БИК
                </td>
                <td style="border-bottom: none; text-align: left; padding: 6px 0 0 8px; vertical-align: top;">
                    <input type="text" class="border-input-white" placeholder="Введите БИК"><br/>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="border-top: none"><br/>Банк получателя</td>
                <td style="text-align: left; padding-left: 2px; vertical-align: top;">
                    Сч. №
                </td>
                <td style="border-top: 0;text-align: left; padding: 6px 0 0 8px; vertical-align: top;">
                    <input type="text" class="border-input-white">
                </td>
            </tr>

            <tr>
                <td style="width: 185px;">ИНН <input type="text" class="border-input-white" placeholder="Введите ИНН"
                                                     style="width: 135px"></td>
                <td style="width: 189px;">КПП <input type="text" class="border-input-gray" style="width: 135px"></td>
                <td rowspan="2"
                    style="text-align: left; padding-left: 2px; vertical-align: top;">
                    Сч. №
                </td>
                <td rowspan="2"
                    style="text-align: left; padding: 6px 0 0 8px; vertical-align: top;"><input type="text"
                                                                                                class="border-input-white">
                </td>
            </tr>
            <tr>
                <td colspan="2"><input type="text" class="border-input-gray"><br/><br/>Получатель
                </td>
            </tr>
        </table>

        <h3 class="align-center">Счет № <input type="text" class="box-txt-score-input"> от <span
                    class="box-score-cal"><i class="fa fa-calendar"></i><input type="text"
                                                                               class="form-control date-picker" name=""
                                                                               value="" data-date-viewmode=""
                                                                               data-delay=""></span></h3>

        <table class="table" width="200">
            <tr>
                <td style="border-right: 0px;border-top: 0px; border-left: 0px;border-bottom: 2px solid #000000;">
                </td>
            </tr>
        </table>

        <table class="table table-bordered no-b"
               style="margin-bottom: 5px; margin-top: -13px;">
            <tr>
                <td class="txt-t" style="width: 150px;">Поставщик<br/>(Исполнитель):</td>
                <td class="txt-b">
                    <input type="text" style="width: 100%; height: 35px; padding: 0 7px">
                </td>
            </tr>
            <tr>
                <td class="txt-t" style="width: 150px;">Покупатель<br/>(Заказчик):</td>
                <td class="txt-b">
                    <input type="text" style="width: 100%; height: 35px; padding: 0 7px" placeholder="Введите ИНН">
                </td>
            </tr>
        </table>

        <div class="portlet">
            <table class="table table-bordered" style="margin-bottom: 8px; border: 2px solid #000000">
                <thead>
                <tr>
                    <th>
                        №
                    </th>
                    <th>
                        Товары (работы, услуги)
                    </th>
                    <th>
                        Кол-во
                    </th>
                    <th>
                        Ед.
                    </th>
                    <th>
                        Цена
                    </th>
                    <th>
                        Сумма
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="text-align: center; width: 5%">1</td>
                    <td style=" width: 0">
                        <select class="form-control">
                            <option>123</option>
                        </select>
                    </td>
                    <td style="text-align: right; width: 10%">
                        <input class="form-control" type="text">
                    </td>
                    <td style="text-align: right; width: 7%">
                        <input class="form-control" type="text">
                    </td>
                    <td style="text-align: right; width: 13%">
                        <input class="form-control" type="text">
                    </td>
                    <td style="text-align: right; width: 15%">
                        1 200,00
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="it-b">
                <tbody>
                <tr>
                    <td width="80%" style="border: none">
                        <div class="portlet pull-left control-panel button-width-table" style="text-align: left;">
                            <div class="btn-group pull-right" style="display: inline-block;">
                        <span class="btn yellow btn-add-line-table">
                            <i class="pull-left fa icon fa-plus-circle"></i>
                        </span>
                            </div>
                        </div>
                    </td>
                    <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Итого:</td>
                    <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                        <b>1 200,00</b>
                    </td>
                </tr>
                <tr>
                    <td width="430px" style="border: none"></td>
                    <td class="txt-b2" style="text-align: right; border: none">В том числе НДС:</td>
                    <td class="txt-b2" style="text-align: right; border: none;">
                        <b>183,05</b>
                    </td>
                </tr>
                <tr>
                    <td width="430px" style="border: none"></td>
                    <td class="txt-b2" style="text-align: right; border: none">Всего к оплате:</td>
                    <td class="txt-b2" style="text-align: right; border: none;">
                        <b>1 200,00</b>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="row m-t-n">
                    <div class="col-md-12">
                        <div class="txt-9">Всего
                            наименований 1, на
                            сумму 1 200,00 руб.
                        </div>

                        <div class="txt-9-b">Одна тысяча двести рублей 00 копеек</div>
                    </div>
                </div>
                <table class="table">
                    <tbody>
                    <tr>
                        <td style="border-right: 0px;border-top: 0px; border-left: 0px;border-bottom: 2px solid #000000;">
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="bg-seal"
                     style="; background-repeat: no-repeat; background-position: 490px 20px; margin-top: -20px; height: 230px; width: 674px">
                    <div class="row m-t-xl">
                        <div class="col-md-12">
                            <br>
                            <br>
                            <table class="podp-r va-bottom message" style=" no-repeat 300px top; width: 674px">
                                <tbody>
                                <tr>
                                    <td class="txt-9-b" style="border: none;padding: 0;width: 15%; padding-top: 40px;">
                                        Руководитель:
                                    </td>
                                    <td style="border: none;width: 4%"></td>
                                    <td class="pad-line"
                                        style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 20px; width: 25%; padding-top: 40px;">
                                        Генеральный директор
                                    </td>
                                    <td style="border: none;width: 4%;">
                                    </td>
                                    <td class="pad-line"
                                        style="border: none;border-bottom: 1px solid #000000;text-align: center; width: 25%; position: relative; height: 40px!important; padding-bottom: 5px;">
                                        <div class="file_upload">
                                            <button type="button">+ Добавить подпись</button>
                                            <div>Файл не выбран</div>
                                            <input type="file">
                                        </div>
                                    </td>
                                    <td style="border: none;width: 4%;"></td>
                                    <td class="pad-line"
                                        style="border: none;border-bottom: 1px solid #000000;text-align: center; width: 27%; padding-bottom: 5px;">
                                        <div class="file_upload">
                                            <button type="button">+ Добавить печать</button>
                                            <div>Файл не выбран</div>
                                            <input type="file">
                                        </div>
                                    </td>
                                </tr>
                                <tr class="small-txt va-top">
                                    <td style="border: none;padding: 0;width: 15%"></td>
                                    <td style="border: none;width: 4%"></td>
                                    <td style="border: none;text-align: center; padding-top: 0;margin-top: 0; vertical-align: top;">
                                        должность
                                    </td>
                                    <td style="border: none;width: 4%;"></td>
                                    <td style="border: none;text-align: center; vertical-align: top;">подпись
                                    </td>
                                    <td style="border: none;width: 4%"></td>
                                    <td style="border: none;text-align: center; vertical-align: top;">
                                        расшифровка
                                        подписи
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row m-t">
                        <div class="col-md-12">
                            <br>
                            <br>
                            <br>
                            <table class="podp-r va-bottom" style=" no-repeat  255px 2px;  width: 674px">
                                <tbody>
                                <tr>
                                    <td class="txt-9-b" style="padding: 0 0 0 0;width: 25%;border: none;">
                                        Главный
                                        (старший)
                                        бухгалтер:
                                    </td>
                                    <td style="width: 4%;border: none;"></td>
                                    <td class="pad-line"
                                        style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 27%; height: 40px!important;"></td>
                                    <td style="border: none;width: 4%"></td>
                                    <td class="pad-line"
                                        style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 27%;"></td>
                                </tr>
                                <tr class="small-txt">
                                    <td style="border: none;padding: 0 0 0 0;width: 25%"></td>
                                    <td style="border: none;width: 4%"></td>
                                    <td style="border: none;text-align: center; margin: 0 0 0 0; width: 27%; vertical-align: top;">
                                        подпись
                                    </td>
                                    <td style="border: none;width: 4%"></td>
                                    <td style="border: none;text-align: center; margin: 0 0 0 0; width: 27%; vertical-align: top;">
                                        расшифровка подписи
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row action-buttons margin-no-icon">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if (!$model->is_deleted && $model->invoiceStatus->sendAllowed()): ?>
            <span id="tooltip-send-button" <?= isset($_COOKIE["tooltip_send_{$model->company_id}"]) && !$showSendPopup ?
                'class="tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_send"' :
                null; ?> >
                <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger" id="send-btn">
                    Отправить
                </button>
                <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                    <span class="ico-Send-smart-pls fs"></span>
                </button>
            </span>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php $printUrl = ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type,
            'filename' => $model->getPrintTitle(),];
        echo Html::a('Печать', $printUrl, [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', $printUrl, [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span <?= isset($_COOKIE["tooltip_pdf_{$model->company_id}"]) ?
            'class="dropup tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_pdf_hide"' : 'class="dropup"'; ?>>
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs',
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                    [
                        'label' => 'Отправить на мой e-mail',
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'send-message-panel-trigger send-to-me',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE)): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($model->type)) : ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="fa fa-files-o fa-2x"></i>',
                        'title' => 'Копировать',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
            <?php else : ?>
                <span class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs action-is-limited">Копировать</span>
                <span class="btn darkblue widthe-100 hidden-lg action-is-limited" title="Копировать"><i
                            class="fa fa-files-o fa-2x"></i></span>
            <?php endif ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($model->isFullyPaid) : ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => 'Снять оплату',
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<span class="ico-Paid-smart-pls fs"></span>',
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
            <?php endif; ?>
        <?php else : ?>
            <?php if (!$model->is_deleted
                && $model->invoiceStatus->paymentAllowed($model->type)
                && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
            ): ?>
                <span id="tooltip-pay-button" <?= isset($_COOKIE["tooltip_paid_{$model->company_id}"]) && !$showSendPopup ?
                    'class="tooltip2 tooltipstered-main" data-tooltip-content = "#tooltip_paid"' :
                    null; ?>>
                    <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                            data-toggle="modal" href="#paid">
                        Оплачен
                    </button>
                    <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" title="Оплачен"
                            href="#paid">
                        <span class="ico-Paid-smart-pls fs"></span>
                    </button>
                </span>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && $model->invoiceStatus->rejectAllowed() && $canUpdate): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<span class="ico-Denied-smart-pls fs"></span>',
                    'title' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
        <?php endif; ?>
    </div>

    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)
            && in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed)): ?>
            <button type="button"
                    class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs"
                    data-toggle="modal" href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg"
                    data-toggle="modal" title="Удалить" href="#delete-confirm">
                <i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>
<div class="tooltip_templates container-tooltip_templates">
    <div id="tooltip_send" class="box-tooltip-templates">
        <div class="box-my-account">
            2
        </div>
        Нажмите на кнопку, и выберете, кому отправить счет
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>

    <div id="tooltip_pdf" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Нажмите на кнопку, что бы скачать в формате PDF
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>


    <div id="tooltip_paid" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Если счет оплачен, нажмите на кнопку и выберите тип оплаты
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>
</div>
<?php if ($model->type == Documents::IO_TYPE_OUT): ?>
<?= $this->registerJs('
    $(document).ready(function (e) {
        var $left = Math.ceil((+$(".dropdown-centerjs").width() - +$(".dropdown-linkjs:visible").width()) / 2);
        console.log($left);
        $(".dropdown-centerjs").css("left", "-" + $left + "px");
    });
'); ?>
<?php endif; ?>
