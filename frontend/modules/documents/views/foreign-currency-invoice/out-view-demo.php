<?php

use common\components\ImageHelper;
use common\models\bank\Bank;
use common\models\company\CompanyType;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\models\LoginForm;
use frontend\models\RegistrationForm;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\documents\forms\InvoiceOutViewForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $model frontend\modules\documents\forms\InvoiceOutViewForm */
/* @var $invoice common\models\document\Invoice */
/* @var $userExists boolean */

$this->params['invoice'] = $invoice;
$this->params['canSave'] = $invoice->contractor->companyType && $invoice->contractor->companyType->in_company;

$example = $invoice->uid == (YII_ENV_PROD ? 'fGkg0' : '7ubrQ');
$isDemo = isset($isDemo) ? $isDemo : false;

$script = <<<JS
var needPayModalOpen = function() {
    $("#out-view-shading").removeClass("hidden");
    $("#document-save-wrapper").animate({right: 0}, 200);
}
var needPayModalClose = function() {
    $("#document-save-wrapper").animate({right: "-400px"}, 200, function() {
        $("#out-view-shading").addClass("hidden");
    });
}
$(document).on("click", "#document-save-link", function (e) {
    e.preventDefault();
    $('.need-pay-text').html('сохранения в личном кабинете');
    needPayModalOpen();
});
$(document).on("click", "#document-pay-link", function (e) {
    e.preventDefault();
    $('.need-pay-text').html('оплаты счета');
    needPayModalOpen();
});
$(document).on("click", "#download-pdf-link", function (e) {
    e.preventDefault();    
    $('.need-pay-text').html('скачивания счета в PDF');
    needPayModalOpen();
});
$(document).on("click", "#download-word-link", function (e) {
    e.preventDefault();
    $('.need-pay-text').html('скачивания счета в Word');
    needPayModalOpen();
});
$(document).on("click", "#document-save-close, .close-panel", function (e) {
    e.preventDefault();
    needPayModalClose();
});

$(document).on("change", "#invoiceoutviewform-taxationtype input", function (e) {
    if (this.value == "osno") {
        if (this.checked == true) {
            $("#invoiceoutviewform-taxationtype input[value='usn']")
                .prop("checked", false)
                .prop("disabled", true)
                .uniform("refresh");
        } else {
            $("#invoiceoutviewform-taxationtype input[value='usn']")
                .prop("disabled", false)
                .uniform("refresh");
        }
    }
    if (this.value == "usn") {
        if (this.checked == true) {
            $("#invoiceoutviewform-taxationtype input[value='osno']")
                .prop("checked", false)
                .prop("disabled", true)
                .uniform("refresh");
        } else {
            $("#invoiceoutviewform-taxationtype input[value='osno']")
                .prop("disabled", false)
                .uniform("refresh");
        }
    }
});
$("form input:checkbox, form input:radio").uniform();
JS;

$this->registerJs($script);
?>

<style>
    #background-example-block {
        position: absolute;
        z-index: 0;
        display: block;
        min-height: 50%;
        min-width: 50%;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        bottom: 10%;
        top:unset;
    }
    #document-save-wrapper .flaticon-growth::before,
    #document-save-wrapper .flaticon-growth::after {
        font-size: 27px;
        margin-left:0;
        margin-top:7px;
    }
    #document-save-wrapper .b2b-example-text {
        font-size: 13px;
        text-transform: none;
    }
</style>

<?php if ($example || $isDemo): ?>
    <div id="background-example-block">
        <p id="example-text" style="color:red">ОБРАЗЕЦ</p>
    </div>
<?php endif; ?>
<div>
    <?= $this->render('pdf-view', [
        'model' => $invoice,
        'ioType' => $invoice->type,
        'addStamp' => (boolean) $invoice->company->pdf_signed,
    ]); ?>
</div>

<div id="out-view-shading" class="hidden"></div>

<div id="document-save-wrapper">
    <div id="document-save-header" class="border-bottom-e4">
        <button id="document-save-close" type="button" class="close" aria-hidden="true">×&times;</button>
        <table style="width: 100%;">
            <tr>
                <td style="width: 10%; vertical-align: middle; text-align: center; border: none;">
                    <?= Html::tag('div', '<i class="flaticon-growth" style="color:#fff;"></i>', [
                        'style' => '
                                display: inline-block;
                                padding: 14px 9px;
                                background-color: #4276a4;
                                border-radius: 30px!important;
                                width:60px;
                                height:60px;
                            ',
                    ]) ?>
                </td>
                <td style="padding-left: 10px; font-size: 18px;  border: none;">
                    Модуль B2B оплат
                </td>
            </tr>
        </table>
    </div>

    <div id="document-save-content">
        <?php if ($example || $isDemo): ?>
            <div class="row">
                <div class="col-sm-12">
                    <span class="b2b-example-text">
                        Чтобы активировать функцию <span class="need-pay-text">скачивания счета в PDF</span>,
                        нужно оплатить модуль В2В платежей.
                    </span>
                    <div style="text-align: center; margin-top:25px;">
                        <a class="btn darkblue text-white" href="/b2b/module/?modal=1">Оплатить Модуль В2В</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

