<?php

use common\assets\SortableAsset;
use common\components\helpers\ArrayHelper;
use common\models\document\Invoice;
use common\models\document\NdsViewType;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\widgets\TableConfigWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\ImageHelper;
use common\components\TextHelper;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $company common\models\Company */
/* @var $invoiceContractEssence \common\models\document\InvoiceContractEssence */
/* @var $document string */

SortableAsset::register($this);

$userConfig = Yii::$app->user->identity->config;

if (empty($ndsCellClass)) {
    $ndsCellClass = 'with-nds-item';
    if (!$model->hasNds &&
        (
            $model->type == Documents::IO_TYPE_IN ||
            $model->company->nds_view_type_id == NdsViewType::NDS_VIEW_WITHOUT
        )
    ) {
        $ndsCellClass .= ' hidden';
    }
}
$priceOneCss = ($model->has_discount ? ' has_discount' : '') . ($model->has_markup ? ' has_markup' : '');

$unitItems = ArrayHelper::map(ProductUnit::findSorted()->all(), 'id', 'name');
$taxRates = TaxRate::sortedArray();
$taxItems = ArrayHelper::map($taxRates, 'id', 'name');
$taxOptions = [];
foreach ($taxRates as $rate) {
    $taxOptions[$rate->id] = ['data-rate' => $rate->rate];
}

$js = <<<JS
$("#table-for-invoice").sortable({
    containerSelector: "table",
    handle: "img.sortable-row-icon",
    itemPath: "> tbody",
    itemSelector: "tr",
    placeholder: "<tr class=\"placeholder\"/>",
    onDrag: function (item, position, _super, event) {
        position.left -= 45;
        position.top -= 25;
        item.css(position);
    },
});
JS;
$this->registerJs($js);
?>

<?= Html::hiddenInput('documentType', $ioType, [
    'id' => 'documentType',
]); ?>

<div class="row">
    <div class="col-sm-12 table-icons" style="margin-top: -12px;">
        <?= TableConfigWidget::widget([
            'items' => [
                [
                    'attribute' => 'invoice_form_article',
                ],
            ],
        ]); ?>
    </div>
</div>

<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
        'style' => 'margin-bottom: 0;',
    ],
]); ?>

<table id="table-for-invoice" class="table table-striped table-bordered account_table last-line-table"
       style="margin-bottom: 0;">
    <thead>
    <tr class="heading" role="row">
        <th class="delete-column-left" width="5%" tabindex="0" rowspan="1" colspan="1">
        </th>
        <th width="10%" tabindex="0" rowspan="1" colspan="1"
            class="col_invoice_form_article<?= $userConfig->invoice_form_article ? '' : ' hidden'; ?>">
            Артикул
        </th>
        <th width="30%" tabindex="0" rowspan="1" colspan="1">
            <?php if ($model->isNewRecord) : ?>
                <?= Html::tag('span', 'Наименование', [
                    'id' => 'title_conf_toggle',
                ]); ?>
                <div id="title_conf" class="popover top hidden" style="
                    display: inline-block;
                    width: 210px;
                    padding: 10px;
                    background-color: #fff;
                    position: absolute;
                    top: -100px;
                    left: 50px;
                    border: 1px solid #ddd;
                    text-align: center;">
                    <div class="arrow" style="left: 50%;"></div>
                    <div style="text-align: center;">
                        Заполнить наименования
                        <br>
                        как в предыдщем счете?
                    </div>
                    <div class="btn-group" role="group" style="margin: 10px 0 0; border: 1px solid #e5e5e5;">
                        <button id="refresh-product-table"
                                class="btn btn-sm yellow<?= $model->price_precision == 4 ? '-text' : ''; ?>"
                                type="button" style="margin: 0;" value="4">
                            ДА
                        </button>
                    </div>
                </div>
            <?php else: ?>
                Наименование
            <?php endif; ?>
        </th>
        <th width="10%" class="" tabindex="0" rowspan="1" colspan="1" style="min-width: 90px">
            <?= Yii::$app->request->get('mode') == 'previewScan' ? 'Кол-во' : 'Количество' ?>
        </th>
        <th width="8%" class="" tabindex="0" rowspan="1" colspan="1" style="min-width: 70px">
            <?= Yii::$app->request->get('mode') == 'previewScan' ? 'Ед. изм-ия' : 'Ед.измерения' ?>
        </th>
        <th width="10%" class="<?= $ndsCellClass ?>" tabindex="0" rowspan="1" colspan="1">
            НДС
        </th>
        <th width="15%" class="" tabindex="0" rowspan="1" colspan="1" style="position: relative;">
            <?= Html::tag('span', 'Цена', [
                'id' => 'price_conf_toggle',
                'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                'onclick' => "$('#invoice_price_conf').toggleClass('hidden');",
            ]) ?>

            <?= Html::activeHiddenInput($model, 'price_precision') ?>

            <div id="invoice_price_conf" class="popover top hidden" style="
                    display: inline-block;
                    width: 170px;
                    padding: 10px;
                    background-color: #fff;
                    position: absolute;
                    top: -100px;
                    left: -50px;
                    border: 1px solid #ddd;
                    text-align: center;
                ">
                <div class="arrow" style="left: 50%;"></div>
                <div style="text-align: center;">
                    Количество знаков
                    <br>
                    после запятой
                </div>
                <div class="btn-group" role="group" style="margin: 10px 0 0; border: 1px solid #e5e5e5;">
                    <button class="btn btn-sm yellow<?= $model->price_precision == 2 ? '-text' : ''; ?>"
                            type="button" style="margin: 0;" value="2">
                        2 знака
                    </button>
                    <button class="btn btn-sm yellow<?= $model->price_precision == 4 ? '-text' : ''; ?>"
                            type="button" style="margin: 0;" value="4">
                        4 знака
                    </button>
                </div>
            </div>
        </th>
        <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
            rowspan="1" colspan="1" style="position: relative;">
            <?php $discountText = (YII_ENV_PROD && $company->id == 9888 ?
                'Агентское вознаграждение' :
                ('Скидка' . ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'))); ?>
            <?= Html::tag('span', $discountText, [
                'id' => 'all_discount_toggle',
                'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                'onclick' => "$('#invoice_all_discount').toggleClass('hidden');",
            ]) ?>

            <div id="invoice_all_discount" class="popover top hidden" style="
                    display: inline-block;
                    width: 276px;
                    padding: 10px;
                    background-color: #fff;
                    position: absolute;
                    top: -230px;
                    left: -67px;
                    border: 1px solid #ddd;
                    text-align: center;
                ">
                <div class="arrow" style="left: 50%;"></div>
                <div class="text-center" style="font-weight: bold;text-transform: uppercase;margin-bottom: 10px;">
                    Настройка скидки
                </div>
                <div style="text-align: left;float: left;display: inline-block;">
                    Скидка для всех
                    <br>
                    позиций счета
                </div>
                <div style="margin: 5px;display: inline-block;">
                    <?= Html::input('number', 'all-discount', 0, [
                        'id' => 'all-discount',
                        'min' => 0,
                        'max' => 99.9999,
                        'step' => 'any',
                        'style' => 'width: 65px; padding: 2px 5px 1px; border: 1px solid #ccc;margin-left: 51px;',
                        'disabled' => $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE,
                    ]); ?>
                    %
                </div>
                <div class="discount_ruble-block <?= $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? 'hidden' : null; ?>">
                    <div style="text-align: left;float: left;display: inline-block;margin-top: 10px;min-width: 159px;">
                        Использовать формат
                        <br>
                        скидки "сумма в руб."
                    </div>
                    <div class="pull-left" style="margin: 10px 5px 5px 6px;display: inline-block;">
                        <?= Html::checkbox('discount_ruble', $model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE, [
                            'class' => 'discount-ruble',
                        ]); ?>
                    </div>
                </div>
                <div class="discount_percent-block <?= $model->discount_type == Invoice::DISCOUNT_TYPE_PERCENT ? 'hidden' : null; ?>">
                    <div style="text-align: left;float: left;display: inline-block;margin-top: 10px;min-width: 159px;">
                        Использовать формат
                        <br>
                        скидки "%"
                    </div>
                    <div class="pull-left" style="margin: 10px 5px 5px 6px;display: inline-block;">
                        <?= Html::checkbox('discount_percent', $model->discount_type == Invoice::DISCOUNT_TYPE_PERCENT, [
                            'class' => 'discount-percent',
                        ]); ?>
                    </div>
                </div>
                <div class="discount_hidden-block">
                    <div style="text-align: left;float: left;display: inline-block;margin-top: 10px;min-width: 159px;">
                        "Скрытая скидка"
                        <br>
                        Скидка видна только в
                        <br>
                        режиме редактирования
                    </div>
                    <div class="pull-left" style="margin: 10px 5px 5px 6px;display: inline-block;">
                        <?= Html::checkbox('discount_hidden', $model->is_hidden_discount, [
                            'class' => 'discount-hidden',
                        ]); ?>
                    </div>
                    <span class="tooltip2 ico-question valign-middle"
                          style="position: absolute; top: 135px; right: 45px;"
                          data-tooltip-content="#tooltip_hidden-discount"></span>
                </div>
                <?= Html::activeHiddenInput($model, 'discount_type'); ?>
                <?= Html::activeHiddenInput($model, 'is_hidden_discount'); ?>
                <div class="col-md-12" style="margin-top: 10px;">
                    <span id="discount_submit" class="btn btn-sm darkblue text-white" type="button"
                          style="margin: 0;" value="4">
                        Применить
                    </span>
                </div>
            </div>
        </th>
        <th width="15%" class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>" tabindex="0"
            rowspan="1" colspan="1" style="position: relative;">
            <?= Html::tag('span', 'Наценка %', [
                'id' => 'all_markup_toggle',
                'style' => 'cursor: pointer; border-bottom: 1px dashed #333333;',
                'onclick' => "$('#invoice_all_markup').toggleClass('hidden');",
            ]) ?>

            <div id="invoice_all_markup" class="popover top hidden" style="
                    display: inline-block;
                    width: 170px;
                    padding: 10px;
                    background-color: #fff;
                    position: absolute;
                    top: -124px;
                    left: -50px;
                    border: 1px solid #ddd;
                    text-align: center;
                ">
                <div class="arrow" style="left: 50%;"></div>
                <div style="text-align: center;">
                    Наценка для всех
                    <br>
                    позиций счета
                </div>
                <div style="margin: 5px;">
                    <input id="all-markup" type="number" name="all-markup" value="0" min="0" max="99.9999" step="any"
                           style="width: 74px; padding: 2px 5px 1px; border: 1px solid #ccc;">
                    %
                </div>
                <div style="">
                        <span id="markup_submit" class="btn btn-sm darkblue text-white" type="button" style="margin: 0;"
                              value="4">
                            Применить
                        </span>
                </div>
            </div>
        </th>
        <th width="15%" tabindex="0" rowspan="1" colspan="1"
            class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
            <?php if (YII_ENV_PROD && $company->id == 9888): ?>
                Цена с агентским вознаграждением
            <?php else: ?>
                Цена со скидкой
            <?php endif; ?>
        </th>
        <th width="15%" tabindex="0" rowspan="1" colspan="1"
            class="markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
            Цена с наценкой
        </th>
        <th width="10%" class="weight_column<?= $model->has_weight ? '' : ' hidden'; ?>" tabindex="0" rowspan="1" colspan="1" style="position: relative;">
            Вес, кг
        </th>
        <th width="10%" class="volume_column<?= $model->has_volume ? '' : ' hidden'; ?>" tabindex="0" rowspan="1" colspan="1" style="position: relative;">
            Объем, м2
        </th>
        <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
            Сумма
        </th>
        <th class="delete-column-right" width="6%" tabindex="0" rowspan="1" colspan="1">
        </th>
    </tr>
    </thead>
    <tbody id="table-product-list-body" data-number="<?= count($model->orders) ?>">
    <?php if (count($model->orders) > 0) : ?>
        <?php
        foreach ($model->orders as $key => $order) {
            echo $this->render('_form_order_row', [
                'order' => $order,
                'ioType' => $ioType,
                'number' => $key,
                'model' => $model,
                'ndsCellClass' => $ndsCellClass,
                'precision' => $model->price_precision,
                'userConfig' => $userConfig,
            ]);
        }
        ?>
    <?php endif; ?>
    <?= $this->render('_form_add_order_row', [
        'ioType' => $ioType,
        'hasOrders' => count($model->orders) > 0,
        'hasDiscount' => (boolean)$model->has_discount,
        'hasMarkup' => (boolean)$model->has_markup,
        'hasWeight' => (boolean)$model->has_weight,
        'hasVolume' => (boolean)$model->has_volume,
        'ndsCellClass' => $ndsCellClass,
        'userConfig' => $userConfig,
    ]) ?>
    </tbody>
    <tfoot>
    <tr class="template disabled-row" role="row">
        <td class="product-delete delete-column-left" style="white-space: nowrap;">
            <span class="icon-close remove-product-from-invoice"></span>
            <!-- <span class="glyphicon glyphicon-menu-hamburger sortable-row-icon"></span> -->
            <?= ImageHelper::getThumb('img/menu-humburger.png', [20, 14], [
                'class' => 'sortable-row-icon',
                'style' => 'padding-bottom: 9px;',
            ]); ?>
        </td>
        <td class="col_invoice_form_article<?= $userConfig->invoice_form_article ? '' : ' hidden'; ?>">
            <div class="order-param-value product-article">&nbsp;</div>
        </td>
        <td>
            <input disabled="disabled" type="text" class="product-title form-control tooltip-product"
                   name="orderArray[][title]"
                   style="width: 100%;">
        </td>
        <td>
            <input disabled="disabled" type="hidden" class="tax-rate"
                   value="0"/>

            <input disabled="disabled" type="hidden" class="order-id"
                   name="orderArray[][id]" value=""/>
            <input disabled="disabled" type="hidden" class="product-id"
                   name="orderArray[][product_id]" value=""/>
            <input disabled="disabled" type="number" min="0" step="any" lang="en"
                   class="form-control product-count"
                   name="orderArray[][count]" value="1"/>
            <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
        </td>
        <td class="">
            <div class="order-param-value product-unit-name">
                <?= Html::dropDownList('orderArray[][unit_id]',
                    null,
                    ['' => Product::DEFAULT_VALUE] + $unitItems,
                    [
                        'class' => '',
                        'disabled' => 'disabled'
                    ]
                ); ?>
            </div>
        </td>
        <td class="<?= $ndsCellClass ?>">
            <div class="order-param-value price-for-sell-nds-name">
                <?= Html::dropDownList(
                    ($ioType == Documents::IO_TYPE_OUT ? 'orderArray[][sale_tax_rate_id]' : 'orderArray[][purchase_tax_rate_id]'),
                    null,
                    $taxItems,
                    [
                        'class' => 'order_product_tax_rate',
                        'options' => $taxOptions,
                        'disabled' => 'disabled'
                    ]
                ); ?>
            </div>
        </td>
        <td class="price-one">
            <?= Html::input('number', 'orderArray[][price]', 0, [
                'class' => 'form-control price-input',
                'disabled' => 'disabled',
                'min' => 0,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
            <?= Html::input('number', 'orderArray[][discount]', 0, [
                'class' => 'form-control discount-input',
                'disabled' => 'disabled',
                'min' => 0,
                'max' => 99.9999,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
            <div class="order-param-value">
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
        <td class="markup markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
            <?= Html::input('number', 'orderArray[][markup]', 0, [
                'class' => 'form-control markup-input',
                'disabled' => 'disabled',
                'min' => 0,
                'max' => 9999.9999,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="markup markup_column<?= $model->has_markup ? '' : ' hidden'; ?>">
            <div class="order-param-value">
                <span class="price-one-with-nds">0</span>
            </div>
        </td>
        <td class="weight weight_column<?= $model->has_weight ? '' : ' hidden'; ?>">
            <?= Html::input('number', 'orderArray[][weight]', 0, [
                'class' => 'form-control weight-input',
                'disabled' => 'disabled',
                'min' => 0,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="volume volume_column<?= $model->has_volume ? '' : ' hidden'; ?>">
            <?= Html::input('number', 'orderArray[][volume]', 0, [
                'class' => 'form-control volume-input',
                'disabled' => 'disabled',
                'min' => 0,
                'step' => 'any',
            ]); ?>
        </td>
        <td class="" style="text-align: right;">
            <div class="order-param-value price-with-nds">0</div>
        </td>
        <td class="product-delete delete-column-right">
            <span class="icon-close remove-product-from-invoice"></span>
            <!-- <span class="glyphicon glyphicon-menu-hamburger sortable-row-icon"></span> -->
            <?= ImageHelper::getThumb('img/menu-humburger.png', [20, 14], [
                'class' => 'sortable-row-icon',
                'style' => 'padding-bottom: 9px;',
            ]); ?>
        </td>
    </tr>
    <tr class="button-add-line" role="row">
        <td class="delete-column-left" colspan="2" style="border-right: none; position: relative;display: table-cell;">
            <div class="portlet pull-left control-panel button-width-table" style="text-align:left">
                <div class="btn-group pull-right" style="display: inline-block; padding-top: 7px">
                    <span id="add-one-more-position" class="btn yellow btn-add-line-table">
                        <i class="pull-left fa icon fa-plus-circle"></i>
                    </span>
                </div>
            </div>
            <?php if ($ioType == \frontend\models\Documents::IO_TYPE_OUT): ?>
                <?php if (!$model->company->getInvoices()->where(['type' => $ioType, 'is_deleted' => false])->exists()) : ?>
                    <span id="hide-when-tooltip" style="position: absolute; top: 12px; right: -60px;">
                            <span class="tooltip2  ico-question valign-middle"
                                  data-tooltip-content="#tooltip_add_product"></span>
                            <span class="ico-video valign-middle" data-toggle="modal"
                                  data-target="#video_modal_add_product"></span>
                        </span>
                <?php endif; ?>
            <?php endif; ?>
            <div class="portlet pull-left" id="invoice-weight-volume" style="margin-bottom: 0;">
                <table class="table table-resume">
                    <tbody>
                    <tr role="row" class="weight_column<?= $model->has_weight ? '': ' hidden'; ?>">
                        <td><b>Общий вес:</b></td>
                        <td class="val">
                            <span class="total_weight">
                                <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalWeight($model), $model->weightPrecision) ?>
                            </span> <span style="padding-right:4px;">кг</span>
                        </td>
                    </tr>
                    <tr role="row" class="volume_column<?= $model->has_volume ? '': ' hidden'; ?>">
                        <td><b>Общий объем:</b></td>
                        <td class="val">
                            <span class="total_volume">
                               <?= TextHelper::invoiceMoneyFormat(100 * InvoiceHelper::getTotalVolume($model), $model->volumePrecision) ?>
                            </span> м2
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
        <td class="cell-invoice_total_block" colspan="10" style="padding-top: 12px; border-right-width: 0;">
            <?= Html::tag('span', '<i class="pull-left fa icon fa-plus" style="margin: 4px 5px 0 0; padding: 0;"></i> Добавить', [
                'class' => 'btn-add-line-table pull-left delete-column-right',
                'style' => 'color: #ffb848; font-size: 16px; font-weight: bold; cursor: pointer;',
            ]); ?>
            <div class="">
                <?= $this->render('_invoice_total_block', [
                    'model' => $model,
                ]) ?>
            </div>
        </td>
    </tr>
    </tfoot>
</table>

<?php Pjax::end(); ?>

<?php if (isset($invoiceContractEssence) && $document == null): ?>
    <div class="row">
        <div class="col-sm-12 field-invoice_contract-essence <?= $model->is_invoice_contract ? null : 'hidden'; ?>">
            <label class="text-bold">
                Предмет договора
            </label>
            <div class="contract-essence-template_block">
                <?= Html::activeRadioList($model, 'contract_essence_template', $model::$contractEssenceTemplates); ?>
            </div>
            <div class="contract-essence-template-tooltip">
                Данные шаблоны договоров являются типовыми. Вы можете вносить любые правки согласно вашим условиям
                работы с покупателями.
            </div>
            <div style="margin-bottom: 15px;">
                <?= Html::activeTextarea($model, 'contract_essence', [
                    'maxlength' => true,
                    'class' => 'form-control',
                    'style' => 'width: 100%;',
                    'rows' => 6,
                    'value' => $model->getContractEssenceText(),
                ]); ?>
            </div>
        </div>
        <div class="col-sm-12 field-invoice_contract-essence <?= $model->is_invoice_contract ? null : 'hidden'; ?>">
            <div class="row">
                <div class="col-xs-1" style="width: 3%;">
                <span style="position: relative;">
                    <?= Html::activeCheckbox($invoiceContractEssence, 'is_checked', [
                        'class' => 'form-control',
                        'label' => false,
                    ]); ?>
                </span>
                </div>
                <div class="col-xs-11">
                    <label for="invoicecontractessence-is_checked" class="control-label"
                           style="padding-top: 0px !important;">
                        Сохранить текст для всех счет-договоров
                    </label>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<script type="text/javascript">

    $(document).on('change', 'select.order_product_tax_rate', function() {

        var $row = $(this).parents('tr');
        var $totalNds = $('.nds-view-item');
        var $invoiceNdsViewType = $('#invoice-nds_view_type_id');
        var showNdsType = $invoiceNdsViewType.attr('data-id') != 2 ?
            $invoiceNdsViewType.attr('data-id') : $('#company-nds_view_type_id').val();

        $row.find('.tax-rate').val($(this).find('option:selected').data('rate'));
        $totalNds.addClass('hidden').filter('.type-' + showNdsType).removeClass('hidden');
        $invoiceNdsViewType.val(showNdsType);

        INVOICE.recalculateInvoiceTable();
    });

    var contractorEssenceTemplateText = <?= json_encode($model->getContractEssenceTemplates()); ?>;
    $(document).mouseup(function (e) {
        if ($("#price_conf_toggle").is(e.target)) return;
        var container = $("#invoice_price_conf:not(hidden)");
        if (container && !container.is(e.target) && container.has(e.target).length === 0) {
            container.addClass("hidden");
        }
    });
    $(document).mouseup(function (e) {
        if ($("#all_discount_toggle").is(e.target)) return;
        var container = $("#invoice_all_discount:not(hidden)");
        if (container && !container.is(e.target) && container.has(e.target).length === 0) {
            container.addClass("hidden");
        }
    });
    $(document).mouseup(function (e) {
        if ($("#all_markup_toggle").is(e.target)) return;
        var container = $("#invoice_all_markup:not(hidden)");
        if (container && !container.is(e.target) && container.has(e.target).length === 0) {
            container.addClass("hidden");
        }
    });
    $(document).on('change', '.discount-ruble', function (e) {
        var $defaultHeader = true;
        if ($('#all_discount_toggle').text().indexOf('Агентское') + 1) {
            $defaultHeader = false
        }
        if ($(this).is(':checked')) {
            $(this).closest('.discount_ruble-block').addClass('hidden');
            $('#all-discount').val('').attr('disabled', true);
            if ($('.discount-percent').is(':checked')) {
                $('.discount-percent').click();
            }
            $('#discount_submit').click();
            $('.discount_percent-block').removeClass('hidden');
            if ($defaultHeader) {
                $('#all_discount_toggle').text('Скидка (руб.)');
            }
            $("#invoice-discount_type").val(1);
        }
    });
    $(document).on('change', '.discount-percent', function (e) {
        var $defaultHeader = true;
        if ($('#all_discount_toggle').text().indexOf('Агентское') + 1) {
            $defaultHeader = false
        }
        if ($(this).is(':checked')) {
            $(this).closest('.discount_percent-block').addClass('hidden');
            $('#all-discount').val('').removeAttr('disabled');
            if ($('.discount-ruble').is(':checked')) {
                $('.discount-ruble').click();
            }
            $('#discount_submit').click();
            $('.discount_ruble-block').removeClass('hidden');
            if ($defaultHeader) {
                $('#all_discount_toggle').text('Скидка %');
            }
            $("#invoice-discount_type").val(0);
        }
    });
    $(document).on('change', '.discount-hidden', function (e) {
        $('#invoice-is_hidden_discount').val(+$(this).is(':checked'));
    });
    $(document).on("input change", "input.discount-input", function(e) {
        var p = (parseFloat(this.value) * 1).precision();
        if (p > 2 && $("input.discount-ruble").is(":checked")) {
            $("#invoice_price_conf button[value=4]").click();
        }
    });
    $(document).on('change', '#invoice-contract_essence_template', function (e) {
        var $templateType = $(this).find('input:checked').val();
        var $template = contractorEssenceTemplateText[$templateType].trim();
        var $productRows = $('#table-for-invoice #table-product-list-body tr.product-row');
        var $services = [];
        if ($templateType == 1) {
            if ($productRows.length > 0) {
                $productRows.each(function (e) {
                    $services.push($(this).find('.product-title').val());
                });
                $template = $template.replace('{Наименование услуги}', $services.join(', '));
            }
        }
        $('#invoice-contract_essence').val($template);
    });

    function checkContractEssenceTemplate() {
        var $templateType = $('#invoice-contract_essence_template input:checked').val();
        if ($templateType == 1) {
            var $template = contractorEssenceTemplateText[$templateType].trim();
            var $productRows = $('#table-for-invoice #table-product-list-body tr.product-row');
            var $services = [];
            if ($productRows.length > 0) {
                $productRows.each(function (e) {
                    $services.push($(this).find('.product-title').val());
                });
                $template = $template.replace('{Наименование услуги}', $services.join(', '));
            }
            $('#invoice-contract_essence').val($template);
        }
    }
</script>
