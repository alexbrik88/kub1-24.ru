<?php

use common\models\currency\Currency;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\document\Invoice */

$currencyName = Html::tag('span', $model->currency_name, ['class' => 'currency_name']);
$currencyAmount = Html::tag('span', $model->currency_amount, ['class' => 'currency_amount currency_rate_amount']);
$currencyRate = Html::tag('span', $model->currency_rate, ['class' => 'currency_rate currency_rate_value']);

$rateText = "{$currencyAmount} {$currencyName} = {$currencyRate} RUB";

$radioName = Html::getInputName($model, 'currency_rate_type');
$radioId = Html::getInputId($model, 'currency_rate_type');

$isHidden = $model->currency_name == Currency::DEFAULT_NAME;
$collapseState = $isHidden ? '' : 'in';

$this->registerJs("
    $('#invoice-currencyratedate').datepicker({
        format: 'dd.mm.yyyy',
        language:'ru',
        autoclose: true,
        endDate: new Date(),
    });
    $('.tooltip_currency_rate').click(function(event) {
        event.stopPropagation();
    });
");

$help = Html::tag('span', '', [
    'class' => 'tooltip_currency_rate ico-question valign-middle',
    'style' => 'position: absolute; top: -6px; right: -26px;',
    'data-tooltip-content' => '#tooltip_currency_rate',
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip_currency_rate',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="row currency-config-form">
    <div class="col-sm-5 currency-select-block" style="margin-bottom: 15px;">
        <?= $form->field($model, 'currency_name', [
            'template' => "{label}\n{input}",
            'options' => ['class' => ''],
            'labelOptions' => [
                'class' => 'text-bold',
                'style' => 'position: relative;',
            ],
        ])->label('Валюта счета ' . Html::tag('span', '', [
            'class' => 'tooltip2 ico-question valign-middle',
            'style' => 'position: absolute; top: -6px; right: -26px;',
            'data-tooltip-content' => '#tooltip_currency_select',
        ]))->widget(Select2::classname(), [
            'data' => Currency::find()
                ->select(['CONCAT([[label]], ", ", [[name]])'])
                ->orderBy(['sort' => SORT_ASC])
                ->indexBy('name')->column(),
            'pluginOptions' => [
                'minimumResultsForSearch' => 10,
            ],
            'options' => [
                'data' => [
                    'default' => Currency::DEFAULT_NAME,
                    'initial' => $model->currency_name,
                ]
            ]
        ]); ?>
    </div>
    <div class="col-sm-7 currency-view-toggle collapse <?= $collapseState ?>">
        <div style="margin-bottom: 15px;">
            <label class="text-bold" style="position: relative;">Обменный курс <?= $help ?></label>
            <div class="currency-rate-link-wrap">
                <?= Html::A("$rateText <span class=\"caret\"></span>", '#currency-rate-box', [
                    'class' => 'collapse-toggle-link currency-rate-link',
                    'style' => 'position: relative;',
                    'data-toggle' => 'collapse',
                    'aria-expanded' => 'false',
                    'aria-controls' => 'currency-rate-box',
                ]) ?>
                <?= Html::activeHiddenInput($model, 'currency_amount', [
                    'class' => 'currency_amount currency_rate_amount',
                    'data' => [
                        'initial' => $model->currency_amount,
                    ],
                ]) ?>
                <?= Html::activeHiddenInput($model, 'currency_rate', [
                    'class' => 'currency_rate currency_rate_value',
                    'data' => [
                        'initial' => $model->currency_rate,
                    ],
                ]) ?>
            </div>
            <div class="hidden">
                <span id="tooltip_currency_rate">
                    Установите обменный курс для данного счета.
                </span>
            </div>
        </div>
        <div class="currency-view-toggle collapse <?= $collapseState ?>">
            <div class="collapse" id="currency-rate-box">
                <div class="field-invoice-currency_rate_type" style="margin-bottom: 15px;">
                    <?= $this->render('/invoice/_set_currency', ['model' => $model]) ?>
                </div>
            </div>
        </div>
    </div>
</div>