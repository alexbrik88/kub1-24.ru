<?php

use common\components\date\DateHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureGroup;
use common\models\document\InvoiceExpenditureItem;
use common\models\NdsOsno;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\models\AutoinvoiceForm;
use frontend\widgets\ContractorDropdown;
use frontend\widgets\ConfirmModalWidget;
use frontend\widgets\ExpenditureDropdownWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\models\bank\BankingParams;
use common\models\employee\EmployeeRole;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $fixedContractor boolean */
/* @var $message Message */
/* @var $ioType integer */
/* @var $isAuto boolean */
/* @var $invoiceEssence \common\models\document\InvoiceEssence */
/* @var $document string */
/* @var $user \common\models\employee\Employee */

$user = Yii::$app->user->identity;
$invoiceBlock = 'invoice-block' . ($isAuto ? ' hidden' : '');
$autoinvoiceBlock = 'autoinvoice-block' . ($isAuto ? '' : ' hidden');
$contractorType = ($ioType == Documents::IO_TYPE_IN) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER;
$contractorDropDownConfig = [
    'class' => 'form-control contractor-select',
    'disabled' => !$model->getIsContractorEditable(),
];

$addNewName = $contractorType == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика';
$staticData = ["add-modal-contractor" => '[ + Добавить ' . $addNewName . ' ]'];

$contractorArray = $company->getContractors()
    ->byContractor($contractorType)
    ->byIsDeleted(Contractor::NOT_DELETED)
    ->byStatus(Contractor::ACTIVE)
    ->exists();

if (empty($model->contractor_id)) {
    $contractorDropDownConfig['prompt'] = '';
}
$autoDisplay = $model->isAutoinvoice ? 'block' : 'none';
$canEditRs = $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF;
$isFirstCreate = \Yii::$app->controller->action->id === 'first-create';
$currentRs = $model->company_rs ? [$model->company_rs => $model->company_bank_name] : [];
$companyRs = $model->company->getForeignCurrencyAccounts()
    ->select(['bank_name', 'rs', 'id', 'bik'])
    ->andWhere(['not', ['type' => CheckingAccountant::TYPE_CLOSED]])
    ->orderBy(['type' => SORT_ASC])
    ->indexBy('rs')
    ->asArray()->all();

foreach ($companyRs as $key => $rs) {
    $rsData[$rs['rs']] = $rs['bank_name'];
    $companyRs[$key]['bank_logo'] = null;
    $companyRs[$key]['bank_link'] = null;
    /*if ($alias = Banking::aliasByBik($rs['bik'])) {
        $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
        $banking = new $bankingClass($company, [
            'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
        ]);
        if ($banking->getHasAutoload() && !$banking->isValidToken()) {
            $companyRs[$key]['bank_link'] = Html::a("Загрузка выписки из банка автоматически", [
                "/cash/banking/{$alias}/default/index",
            ], [
                'class' => 'bank-link',
                'style' => 'position: absolute;font-size: 13px;margin-left: 40px;top: 5px;width: 100%',
            ]);

            if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
                $companyRs[$key]['bank_logo'] = ImageHelper::getThumbSrc($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32]);
            }
        }
    }*/
}
//$rsData = array_merge($currentRs, $companyRs);
if ($canEditRs) {
    $rsData["add-modal-rs"] = '[ + Добавить расчетный счет ]';
}
$hasAdditionals = $model->has_discount || $model->has_markup || $model->hasNds ||
    $model->show_paylimit_info || $model->comment ||
    $model->currency_name != Currency::DEFAULT_NAME;
?>

<?= Html::activeHiddenInput($model, 'store_id'); ?>

<?= Html::hiddenInput('is_first_invoice', (int)$isFirstCreate, ['id' => 'is_first_invoice']); ?>

<?= Html::activeHiddenInput($model, 'isAutoinvoice'); ?>

<?= Html::activeHiddenInput($model, 'production_type', [
    'class' => 'invoice-production-type-hidden',
    'value' => '',
]); ?>

<?= Html::hiddenInput('company-nds_view_type_id', $model->company->nds_view_type_id, ['id' => 'company-nds_view_type_id']) ?>

<?php
// Set Agreement data
$agreementBasis = null;
if ($model->isNewRecord && $fromAgreement = \Yii::$app->request->get('fromAgreement')) {
    if ($agreementBasis = Agreement::findOne(['id' => $fromAgreement, 'company_id' => $user->company->id])) {
        $model->company_rs = $agreementBasis->company_rs;
        $model->contractor_id = $agreementBasis->contractor_id;
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+{$agreementBasis->payment_delay} day"));
        $contractorDropDownConfig['disabled'] = true;
    }
}
$contractorDropDownConfig['options'] = Contractor::getAllContractorSelect2Options($contractorType, Yii::$app->user->identity->company);
?>
<?php if ($canEditRs): ?>
    <style>
        #select2-invoice-company_rs-results .select2-results__option:last-child {
            background: #ffb848;
            color: #fff;
        }
    </style>
<?php endif; ?>
    <style>
        .form-group .btn.yellow {
            position: absolute;
        }

        @media (max-width: 770px) {
            .form-group .btn.yellow {
                position: static;
                margin: 10px 0 0 15px;
            }

        }
    </style>
    <div class="row">
        <div class="col-xs-12 pad0 <?= $isFirstCreate ? 'col-lg-6 col-md-12' : 'col-lg-7'; ?>">
            <div class="col-xs-12 col-sm-12 pad0" id="tooltip_requisites_block" style="max-width: 613px;">
                <a style="margin-top: -6px;position: absolute;top: 45px;right: 16px;"
                   class="tooltip-first-r"
                   data-tooltip-content="#tooltip_requisites">

                </a>
                <?php if (count($rsData) > 1) : ?>
                    <div class="col-lg-12" style="max-width: 590px">
                        <div class="form-group row">
                            <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label">
                                    Мой расч/счет в<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-sm-8">
                                <?= $form->field($model, 'company_rs', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                    ],
                                ])->widget(Select2::classname(), [
                                    'data' => $rsData,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'templateResult' => new JsExpression('formatCompanyRsTemplateResult'),
                                    ],
                                    'options' => [
                                        'class' => 'form-control',
                                        'data-update-url' => Url::to(['update-checking-accountant']),
                                        'data-create-url' => Url::to(['create-checking-accountant']),
                                        //       'data-company-rs' => $companyRs,
                                    ],
                                ])->label(false); ?>
                            </div>
                            <?php if (!$model->isNewRecord && $model->company->requisites_updated_at > $model->updated_at) : ?>
                                <?= ConfirmModalWidget::widget([
                                    'toggleButton' => [
                                        'label' => '<i class="icon-refresh refresh-contractor-invoice tooltip3" data-tooltip-content="#tooltip_refresh_company"></i>',
                                        'class' => '',
                                        'tag' => 'i',
                                    ],
                                    'confirmUrl' => Url::to(['update-company-fields', 'id' => $model->id, 'type' => $model->type, 'isAuto' => $model->isAutoinvoice]),
                                    'confirmParams' => [],
                                    'message' => 'Вы уверены, что хотите обновить ваши реквизиты в данном счете?',
                                ]); ?>
                            <?php endif; ?>
                            <img src="" class="little_logo_bank" style="position: absolute;">
                        </div>
                    </div>
                <?php endif ?>
                <div class="col-lg-12" style="max-width: 590px">
                    <div class="form-group row">
                        <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                            <label class="control-label">
                                <?= $message->get(Message::CONTRACTOR); ?><span class="required"
                                                                                aria-required="true">*</span>:
                            </label>
                        </div>
                        <div id="invoice-contractor_id-container" class="col-sm-8">
                            <div id="add-first-contractor" class="details"
                                 style="display: <?= $contractorArray ? 'none' : 'block'; ?>;">
                                <div style="display: inline-block; width:100%; max-width:300px">
                                    <?= Html::textInput('new_contractor_inn', '', [
                                        'id' => 'new_contractor_inn',
                                        'data-required' => 1,
                                        'class' => 'form-control',
                                        'style' => 'display:inline-block',
                                        'placeholder' => 'Автозаполнение по ИНН ' . ($ioType == Documents::IO_TYPE_OUT ? 'покупателя' : 'продавца'),
                                    ]); ?>
                                    <?= $this->render('_new_contractor_inn_api') ?>
                                </div>

                                <span class="tooltip2 ico-question valign-middle"
                                      data-tooltip-content="#tooltip_contractor"></span>
                                <span class="ico-video valign-middle" data-toggle="modal"
                                      data-target="#video_modal_contractor"></span>
                            </div>
                            <div id="select-existing-contractor"
                                 style="display: <?= $contractorArray ? 'block' : 'none'; ?>;">
                                <?php if ($model->getIsContractorEditable()): ?>
                                    <?= $form->field($model, 'contractor_id', [
                                        'enableClientValidation' => true,
                                        'validateOnChange' => true,
                                        'template' => "{input}",
                                        'options' => [
                                            'class' => 'show-contractor-type-in-fields',
                                        ],
                                    ])->widget(ContractorDropdown::class, [
                                        'company' => $company,
                                        'contractorType' => $contractorType,
                                        'staticData' => $staticData,
                                        'options' => $contractorDropDownConfig,
                                    ]); ?>
                                <?php else: ?>
                                    <?= Html::textInput('contractor_update_button', $model->contractor_name_short, [
                                        'readonly' => true,
                                        'class' => 'form-control',
                                        'id' => 'contractor_update_button',
                                        'data-invoice-id' => $model->id,
                                        'data-contractor-id' => $model->contractor_id,
                                        'style' => 'width:100%; cursor:pointer; background-color:#fff;',
                                    ]) ?>
                                    <?= Html::activeHiddenInput($model, 'contractor_id'); ?>
                                <?php endif; ?>
                            </div>
                        </div>

                        <?php if (!$model->isNewRecord && $model->checkContractor()): ?>
                            <?= ConfirmModalWidget::widget([
                                'toggleButton' => [
                                    'label' => '<i class="icon-refresh refresh-contractor-invoice tooltip3" data-tooltip-content="#tooltip_refresh_contractor"></i>',
                                    'class' => '',
                                    'tag' => 'i',
                                ],
                                'confirmUrl' => Url::to(['update-contractor-fields', 'id' => $model->id, 'type' => $model->type, 'isAuto' => $model->isAutoinvoice]),
                                'confirmParams' => [],
                                'message' => 'Вы уверены, что хотите обновить информацию по ' . ($model->type == Documents::IO_TYPE_OUT ? 'покупателю' : 'продавцу') . ' в данном счете?',
                            ]); ?>
                        <?php endif; ?>

                        <?php /*
                        <?= $this->render('//dossier/_popup-confirm', [
                            'link' => '/dossier?id=' . $model->contractor_id,
                            'button' => [
                                'label' => 'Проверить ' . ($model->type == Documents::IO_TYPE_OUT ? 'покупателя' : 'продавца'),
                                'class' => 'btn darkblue-invert contractor-invoice-dossier not-verified' . ($model->contractor === null || $model->contractor->verified ? ' hidden' : ''),
                                'style' => 'position: absolute;'
                            ],
                            'newTab' => true
                        ]) ?>
                        <a class="btn darkblue-invert contractor-invoice-dossier verified<?= $model->contractor === null || !$model->contractor->verified ? ' hidden' : '' ?>"
                           href="/dossier?id=<?= $model->contractor_id ?>" style="position: absolute;" target="_blank">Досье <?= ($model->type == Documents::IO_TYPE_OUT ? 'покупателя' : 'продавца') ?></a>
                        */ ?>

                        <span class="hidden contractor-invoice-debt" style="position: absolute;width: 100%;top: -2px;margin-left:223px;">
                            Есть неоплаченные счета: <span class="count"></span><br>
                            на сумму <span class="amount"></span> Р
                    </span>
                    </div>
                </div>

                <div class="col-xs-12" style="max-width: 590px">
                    <!-- start -->
                    <div class="row">
                        <div class="col-xs-12 pad0 <?= $invoiceBlock ?>" style="margin-bottom: 15px;">
                            <div class="col-xs-4" style="max-width: 175px; padding-right: 0;;">
                                <label for="invoice-payment_limit_date" class="control-label">
                                    Оплатить до<span class="required" aria-required="true">*</span>:
                                </label>
                            </div>
                            <div class="col-xs-8" style="max-width: 153px;">
                                <div class="input-icon">
                                    <i class="fa fa-calendar"></i>
                                    <?= Html::activeTextInput($model, 'payment_limit_date', [
                                        'class' => 'form-control date-picker',
                                        'data-date-viewmode' => 'years',
                                        'data-delay' => $model->contractor ? (
                                            $model->type == Documents::IO_TYPE_IN ?
                                            $model->contractor->seller_payment_delay :
                                            $model->contractor->customer_payment_delay
                                        ) : 10,
                                        'value' => DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                    ]); ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($autoinvoice) : ?>
                            <div class="col-xs-12 pad0 <?= $autoinvoiceBlock ?>"
                                 style="margin-bottom: 15px; max-width: 328px">
                                <div class="col-xs-12" style="max-width: 175px; padding-right: 0;;">
                                    <label for="autoinvoice-payment_delay"
                                           class="control-label">
                                        Отсрочка оплаты:
                                    </label>
                                </div>
                                <div class="col-xs-12" style="max-width: 153px;">
                                    <div style="display: inline-block; width: 65px; margin-right: 5px;">
                                        <?= Html::activeDropDownList($autoinvoice, 'payment_delay', Autoinvoice::monthDays(), [
                                            'class' => 'form-control',
                                        ]); ?>
                                    </div>
                                    <label for="autoinvoice-payment_delay"
                                           class="control-label">
                                        дней.
                                    </label>
                                </div>
                            </div>
                        <?php endif ?>

                        <?php if (!$isFirstCreate && $ioType == Documents::IO_TYPE_OUT) : ?>
                            <div class="col-xs-12 <?= $invoiceBlock ?>"
                                 style="max-width: 260px;margin-bottom: 15px !important;padding-left: 14px !important;padding-right: 0px;">
                                <div class="form-group pad0 marg">
                                    <label for="invoice-remind_contractor"
                                           class="control-label">
                                        Отправить напоминание:
                                        <?= Html::activeCheckbox($model, 'remind_contractor', [
                                            'class' => 'form-control',
                                            'label' => false,
                                        ]); ?>
                                    </label>
                                    <span id="tooltip_remind_ico"
                                          style="margin-left: 0px !important;"
                                          class="tooltip2  ico-question valign-middle"
                                          data-tooltip-content="#tooltip_remind"></span>
                                </div>
                            </div>
                        <?php endif ?>
                    </div>
                    <!-- end -->
                </div>

                <div class="col-xs-12" style="max-width: 590px">
                    <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                        <div class="form-group required row">
                            <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label">
                                    Статья расходов
                                </label>
                            </div>
                            <div class="col-sm-8">
                                <?= $form->field($model, 'invoice_expenditure_item_id', [
                                    'template' => "{input}",
                                    'options' => [
                                        'class' => '',
                                        'style' => 'max-width: 500px;',
                                    ],
                                ])->widget(ExpenditureDropdownWidget::classname(), [
                                    'exclude' => [
                                        'group' => InvoiceExpenditureGroup::TAXES,
                                    ],
                                    'options' => [
                                        'prompt' => '',
                                    ],
                                ]);
                                ?>
                            </div>

                            <?= $this->render('@frontend/modules/cash/views/default/_expenditure_item_form', [
                                'inputId' => 'invoice-invoice_expenditure_item_id',
                            ]) ?>
                        </div>
                    <?php endif; ?>
                    <?php //if (!$isFirstCreate) : ?>
                    <div class="form-group required row">
                        <div class="col-xs-12 col-sm-4"
                             style="max-width: 175px; padding-right: 0;">
                            <label class="control-label">
                                Основание
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8"
                             style="max-width: 500px;">

                            <?= $this->render('_basis_document', ['model' => $model, 'agreementBasis' => isset($agreementBasis) ? $agreementBasis : null]) ?>
                        </div>
                    </div>
                    <?php //endif; ?>
                    <?php if ($company->strict_mode == Company::ON_STRICT_MODE) : ?>
                        <div class="form-group required your-company row">
                            <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label">
                                    Ваша компания
                                    <?php if ($isFirstCreate) : ?>
                                        <br>
                                        <span style="color: #ccc; font-weight: normal;">
                                    (<?= $ioType == Documents::IO_TYPE_IN ? 'Покупатель' : 'Продавец'; ?>)
                                </span>
                                    <?php endif; ?>
                                </label>
                            </div>
                            <div class="col-sm-8 details"
                                 style="padding-right:0; display: <?php Company::hasRequiredParams() ? 'none' : 'block'; ?>">
                                <?php /*<?= Html::button('Заполнить реквизиты по ИНН', ['class' => 'btn yellow first-invoice-company']); ?>*/ ?>
                                <div style="display: inline-block; width:100%; max-width:300px">
                                    <?php if ($company->self_employed) : ?>
                                        <?= Html::button('Заполнить реквизиты', [
                                            'class' => 'btn yellow first-invoice-company',
                                        ]) ?>
                                    <?php else : ?>
                                        <?= Html::textInput('new_company_inn', '', [
                                            'id' => 'new_company_inn',
                                            'data-required' => 1,
                                            'class' => 'form-control',
                                            'placeholder' => 'Автозаполнение по ИНН',
                                        ]); ?>
                                        <?= $this->render('_new_company_inn_api') ?>
                                    <?php endif; ?>
                                </div>

                                <span class="tooltip2 ico-question valign-middle"
                                      data-tooltip-content="#tooltip_company"></span>
                                <span class="ico-video valign-middle" data-toggle="modal"
                                      data-target="#video_modal_company"></span>

                            </div>
                            <?= Html::hiddenInput('Company[strict_mode]'); ?>
                        </div>
                    <?php endif; ?>

                    <?php if ($autoinvoice) : ?>
                        <?php
                        $isMonthly = $autoinvoice->period == Autoinvoice::MONTHLY;
                        $isQuarterly = $autoinvoice->period == Autoinvoice::QUARTERLY;
                        $this->registerJs('
                            $(document).on("change", "#autoinvoiceform-period", function () {
                                var period = $(this).val();
                                var monthly = "' . Autoinvoice::MONTHLY . '";
                                var quarterly = "' . Autoinvoice::QUARTERLY . '";
                                $(".period-dependent.period-monthly").toggleClass("hidden", period == quarterly);
                                $(".period-dependent.period-monthly select").prop("disabled", period == quarterly);
                                $(".period-dependent.period-quarterly").toggleClass("hidden", period == monthly);
                                $(".period-dependent.period-quarterly select").prop("disabled", period == monthly);
                            });
                        ');
                        ?>
                        <div id="autoinvoice-body" class="<?= $autoinvoiceBlock ?>">
                            <div class="form-group row">
                                <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                    <label for="under-date" class="control-label">
                                        Периодичность
                                        <span class="required" aria-required="true">*</span>:
                                    </label>
                                </div>
                                <div class="col-sm-8" style="white-space: nowrap;">
                                    <?= Html::activeDropDownList($autoinvoice, 'period', Autoinvoice::$PERIOD, [
                                        'class' => 'form-control',
                                        'style' => 'display: inline-block; width: auto; padding: 6px;',
                                    ]); ?>
                                    <span class="period-dependent period-quarterly<?= $isQuarterly ? '' : ' hidden'; ?>">
                                        <?= Html::activeDropDownList($autoinvoice, 'month', Autoinvoice::quarterMonths(), [
                                            'class' => 'form-control',
                                            'style' => 'display: inline-block; width: auto; padding: 6px; margin-left: 10px;',
                                            'disabled' => !$isQuarterly,
                                        ]); ?>
                                        <label class="control-label">
                                            месяц
                                        </label>
                                    </span>
                                    <?= Html::activeDropDownList($autoinvoice, 'day', Autoinvoice::monthDays(), [
                                        'class' => 'form-control',
                                        'style' => 'display: inline-block; width: auto; padding: 6px; margin-left: 10px;',
                                    ]); ?>
                                    <label class="control-label">
                                        числа
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                    <label for="under-date" class="control-label">
                                        Начало
                                        <span class="required" aria-required="true">*</span>:
                                    </label>
                                </div>
                                <div class="col-sm-8" style="max-width: 175px; padding-right: 0;">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::activeTextInput($autoinvoice, 'dateFrom', [
                                            'class' => 'form-control date-picker',
                                            'data-date-viewmode' => 'years',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                    <label for="under-date" class="control-label">
                                        Окончание
                                        <span class="required" aria-required="true">*</span>:
                                    </label>
                                </div>
                                <div class="col-sm-8" style="max-width: 175px; padding-right: 0;">
                                    <div class="input-icon">
                                        <i class="fa fa-calendar"></i>
                                        <?= Html::activeTextInput($autoinvoice, 'dateTo', [
                                            'class' => 'form-control date-picker',
                                            'data-date-viewmode' => 'years',
                                        ]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6" style="max-width: 235px;">
                                    <label for="under-date" class="control-label">
                                        В названии услуг, автоматически добавлять:
                                    </label>
                                </div>
                                <div class="col-sm-6 period-dependent period-monthly<?= $isMonthly ? '' : ' hidden'; ?>"
                                     style="margin-top: 8px; position: relative;">
                                    <?= Html::activeDropDownList($autoinvoice, 'add_month_and_year', Autoinvoice::$addToTitle1, [
                                        'id' => 'autoinvoice-add-monthly',
                                        'class' => 'form-control',
                                        'disabled' => !$isMonthly,
                                    ]); ?>
                                    <?= Html::tag('span', '', [
                                        'class' => 'tooltip3 ico-question valign-middle',
                                        'data-tooltip-content' => '#product_services_title',
                                        'style' => 'position: absolute; top: 2px; right: -5px;',
                                    ]) ?>
                                </div>
                                <div class="col-sm-6 period-dependent period-quarterly<?= $isQuarterly ? '' : ' hidden'; ?>"
                                     style="margin-top: 8px; position: relative;">
                                    <?= Html::activeDropDownList($autoinvoice, 'add_month_and_year', Autoinvoice::$addToTitle2, [
                                        'id' => 'autoinvoice-add-quarterly',
                                        'class' => 'form-control',
                                        'disabled' => !$isQuarterly,
                                    ]); ?>
                                    <?= Html::tag('span', '', [
                                        'class' => 'tooltip3 ico-question valign-middle',
                                        'data-tooltip-content' => '#product_services_title2',
                                        'style' => 'position: absolute; top: 2px; right: -5px;',
                                    ]) ?>
                                </div>
                            </div>
                            <div class="autoinvoice_send_to collapse <?= $model->contractor ? 'in' : '' ?>">
                                <div class="form-group row">
                                    <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                                        <label for="under-date" class="control-label">
                                            <?= $autoinvoice->getAttributeLabel('send_to') ?>
                                            <span class="required" aria-required="true">*</span>:
                                        </label>
                                    </div>
                                    <div class="col-sm-8" style="padding-top: 8px;">
                                        <?= Html::activeCheckboxList($autoinvoice, 'send_to', $autoinvoice->sendItems, [
                                            'item' => function ($index, $label, $name, $checked, $value) use ($model, $autoinvoice) {
                                                $isDirector = $value == AutoinvoiceForm::TO_DIRECTOR ? null : (
                                                isset($model->contractor->{$value . '_is_director'}) ?
                                                    $model->contractor->{$value . '_is_director'} :
                                                    1
                                                );
                                                return Html::checkbox($name, $checked, [
                                                    'value' => $value,
                                                    'class' => 'item-send-to',
                                                    'label' => $this->render('_form_send_to_item', [
                                                        'model' => $model,
                                                        'autoinvoice' => $autoinvoice,
                                                        'index' => $index,
                                                        'label' => $label,
                                                        'name' => $name,
                                                        'checked' => $checked,
                                                        'value' => $value,
                                                        'isDirector' => $isDirector,
                                                    ]),
                                                    'data-is-director' => $isDirector,
                                                    'labelOptions' => [
                                                        'style' => 'width: 100%;',
                                                    ],
                                                ]);
                                            },
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                </div>

                <div class="not-clickable ">
                    &nbsp;
                </div>
            </div>


            <?php if ($isFirstCreate) : ?>
                <?php if ($company->companyTaxationType->osno) : ?>
                    <div class="col-xs-12 pad0" style="margin-bottom: 15px;">
                        <div class="col-xs-12 col-sm-4" style="max-width: 175px; padding-right: 0;">
                            <label for="invoice-payment_limit_date" class="control-label">
                                Тип
                                <?php if ($document == null): ?>
                                    счета
                                <?php endif; ?>
                                <span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-8" style="max-width: 320px;">
                            <?php
                            if ($company->nds === null) {
                                $company->nds = NdsOsno::WITH_NDS;
                            }
                            echo Html::activeRadioList($company, 'nds', ArrayHelper::map(NdsOsno::find()->all(), 'id', 'name'), [
                                'class' => 'inp_one_line_company checkbox-width',
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    return '<div class="col-xs-12 pad0">' .
                                        Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                                            'class' => 'marg pad0-l radio-inline radio-padding',
                                        ]) . Html::a('Пример счета', [
                                            Url::to('/company/view-example-invoice'),
                                            'nds' => $value == 1 ? true : false,
                                        ], [
                                            'class' => 'marg radio-inline radio-padding',
                                            'target' => '_blank',
                                        ]) .
                                        "</div>";
                                },
                            ]);
                            ?>
                        </div>
                    </div>

                <?php endif; ?>
            <?php endif; ?>
        </div>

        <?php if ($isFirstCreate) : ?>
            <div class="col-lg-6 visible-lg-block mt-element-step">
                <div class="row step-default pull-right" style="max-width: 600px;">
                    <div id="invoise-step-1"
                         class="col-sm-4 mt-step-col first-invoice-contractor invoice-step<?= $model->contractor ? ' done' : ''; ?>"
                         data-invoice-ready="35">
                        <div class="mt-step-number bg-white font-grey">1</div>
                        <div class="mt-step-title uppercase font-grey-cascade">
                            <?= ($ioType == \frontend\models\Documents::IO_TYPE_IN) ? 'Поставщик' : 'Покупатель' ?>
                        </div>
                        <div class="mt-step-content font-grey-cascade" style="height: 36px;">Заполнить реквизиты</div>
                    </div>
                    <div id="invoise-step-2"
                         class="col-sm-4 mt-step-col first-invoice-company invoice-step<?= !$company->strict_mode ? ' done' : ''; ?>"
                         data-invoice-ready="35">
                        <div class="mt-step-number bg-white font-grey">2</div>
                        <div class="mt-step-title uppercase font-grey-cascade">
                            <?php if ($model->company->self_employed) : ?>
                                Вы
                            <?php else : ?>
                                Ваше <?= $model->company->companyType->name_short ?>
                            <?php endif ?>
                        </div>
                        <div class="mt-step-content font-grey-cascade" style="height: 36px;">Заполнить реквизиты</div>
                    </div>
                    <div id="invoise-step-3"
                         class="col-sm-4 mt-step-col first-invoice-product invoice-step<?= $model->orders ? ' done' : ''; ?>"
                         data-invoice-ready="30">
                        <div class="mt-step-number bg-white font-grey">3</div>
                        <div class="mt-step-title uppercase font-grey-cascade" style="margin: 0 -10px;">Товар/Услуга
                        </div>
                        <div class="mt-step-content font-grey-cascade" style="height: 36px; margin: 0 -10px;">Ввести
                            название,
                            количество и цену
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <div class="col-xs-12" style="margin-bottom: 15px">
            <?= Html::A('Дополнительные параметры <span class="caret"></span>', '#additionalSettings', [
                'class' => 'collapse-toggle-link text-bold',
                'data-toggle' => 'collapse',
                'aria-expanded' => 'false',
                'aria-controls' => 'additionalSettings',
            ]) ?>
        </div>
        <div class="col-xs-12 collapse" id="additionalSettings">
            <div class="row">
                <div class="col-sm-12" style="max-width: 231px;">
                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                            <label class="control-label" style="padding-top: 0px !important;">
                                <?php if (YII_ENV_PROD && $model->company_id == 9888): ?>
                                    Агентское вознаграждение
                                <?php else: ?>
                                    Указать скидку
                                <?php endif; ?>
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <?= Html::activeCheckbox($model, 'has_discount', [
                                'class' => 'form-control',
                                'label' => false,
                            ]); ?>
                        </div>
                    </div>

                    <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label" style="padding-top: 0px !important;">
                                    Указать наценку
                                </label>
                            </div>
                            <div class="col-xs-3">
                                <?= Html::activeCheckbox($model, 'has_markup', [
                                    'class' => 'form-control',
                                    'label' => false,
                                ]); ?>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if ($ioType == Documents::IO_TYPE_OUT && $company->companyTaxationType->usn) : ?>
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label" style="padding-top: 0px !important;">
                                    <?php if ($document == Documents::SLUG_ACT): ?>
                                        Акт
                                    <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                        ТН
                                    <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                        СФ
                                    <?php elseif ($document == Documents::SLUG_UPD): ?>
                                        УПД
                                    <?php else: ?>
                                        Счет
                                    <?php endif; ?>
                                    с НДС
                                </label>
                            </div>
                            <div class="col-xs-3">
                                <?= Html::activeCheckbox($model, 'hasNds', [
                                    'class' => 'form-control',
                                    'label' => false,
                                ]); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($ioType == Documents::IO_TYPE_OUT && $document == null) : ?>
                        <div class="row" style="margin-bottom: 15px;">
                            <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                                <label class="control-label" style="padding-top: 0px !important;">
                                    Указать срок оплаты
                                </label>
                            </div>
                            <div class="col-xs-3">
                            <span style="position: relative;">
                                <?= Html::activeCheckbox($model, 'show_paylimit_info', [
                                    'class' => 'form-control',
                                    'label' => false,
                                ]); ?>
                                <?= Html::tag('span', '', [
                                    'class' => 'tooltip2 ico-question valign-middle',
                                    'style' => 'position: absolute; top: -6px; right: -22px;',
                                    'data-tooltip-content' => '#tooltip_paydate_limit',
                                ]) ?>
                            </span>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                            <label class="control-label" style="padding-top: 0px !important;">
                                Указать вес
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <?= Html::activeCheckbox($model, 'has_weight', [
                                'class' => 'form-control',
                                'label' => false,
                            ]); ?>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom: 15px;">
                        <div class="col-xs-9" style="max-width: 175px; padding-right: 0;">
                            <label class="control-label" style="padding-top: 0px !important;">
                                Указать объем
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <?= Html::activeCheckbox($model, 'has_volume', [
                                'class' => 'form-control',
                                'label' => false,
                            ]); ?>
                        </div>
                    </div>

                </div>
                <?php if ($document == null): ?>
                    <div class="col-sm-12" style="max-width: 600px;">
                        <?= $this->render('_form_currency', [
                            'form' => $form,
                            'model' => $model,
                        ]) ?>
                    </div>
                <?php endif; ?>
                <?php if (!$model->is_invoice_contract && $document == null): ?>
                    <div class="col-sm-12 field-invoice_comment">
                        <label class="text-bold">
                            Комментарий в счете для покупателя
                            <?= Html::tag('span', '', [
                                'class' => 'tooltip2 ico-question valign-middle',
                                'style' => 'display: inline-block; margin: -6px 0;',
                                'data-tooltip-content' => '#tooltip_comment_client',
                            ]) ?>
                        </label>
                        <div style="margin-bottom: 15px;">
                            <?= Html::activeTextarea($model, 'comment', [
                                'maxlength' => true,
                                'class' => 'form-control',
                                'style' => 'width: 100%;',
                                'value' => $invoiceEssence->is_checked_en && $model->isNewRecord && $model->type == Documents::IO_TYPE_OUT ?
                                    $invoiceEssence->text_en : $model->comment,
                            ]); ?>
                        </div>
                    </div>
                    <div class="col-sm-12 field-invoice-essence">
                        <div class="row">
                            <div class="col-xs-1" style="width: 3%;">
                            <span style="position: relative;">
                                <?= Html::activeCheckbox($invoiceEssence, 'is_checked_en', [
                                    'class' => 'form-control',
                                    'label' => false,
                                ]); ?>
                            </span>
                            </div>
                            <div class="col-xs-11">
                                <label for="invoiceessence-is_checked" class="control-label"
                                       style="padding-top: 0px !important;">
                                    Сохранить текст для всех счетов
                                </label>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php $this->registerJs('
    $companyRs = ' . json_encode($companyRs) . ';
    function formatCompanyRsTemplateResult(data, container) {
        if (data.id !== "add-modal-rs" && data.disabled !== true) {
            var input = $("#invoice-company_rs");
            var content = \'<div class="company_rs-item-name-label">\';
            if (' . (int)$canEditRs . ') {
                content += \'<i class="icon icon-pencil pull-right edit-company_rs-item ajax-modal-btn" data-id="\' + data.id + \'" data-url="\' + input.data("update-url") + \'?id=\' + $companyRs[data.id].id + \'" title="Обновить"></i>\';
            }
            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            container.innerHTML = content;
        } else {
            var input = $("#invoice-company_rs");
            $(container).addClass("ajax-modal-btn").attr("title", "Добавить расчетный счет").attr("data-url", input.data("create-url"));
            container.innerHTML = data.text;
        }

        return container;
    };

    $(document).ready(function () {
        loadBankLogo($("#invoice-company_rs").val());
    });

    $(document).on("change", "#invoice-company_rs", function () {
        loadBankLogo($(this).val());
    });

    function loadBankLogo(bankRs) {
        if (+$("#is_first_invoice").val() !== 1 && +$("#invoiceauto-isautoinvoice").val() !== 1 && $companyRs[bankRs] !== undefined) {
            var $logo = $companyRs[bankRs].bank_logo;
            var $link = $companyRs[bankRs].bank_link;

            if ($logo !== null) {
                $(".little_logo_bank").attr("src", $logo);
            } else {
                $(".little_logo_bank").attr("src", "");
            }
            if ($link !== undefined) {
                $(".little_logo_bank").after($link);
            } else {
                $(".little_logo_bank").siblings(".bank-link").remove();
            }
        }
    }

    $(document).on("submit", "form.form-checking-accountant", function () {
        $this = $(this);
        $.post($this.attr("action"), $(this).serialize(), function (data) {
            if (data.result == true) {
                var select = $("#invoice-company_rs");
                select.find("option").remove();
                for (var key in data.options) {
                    select.append($("<option></option>").attr("value", key).text(data.options[key]));
                }
                $companyRs = data.companyRs;
                if (data.val) {
                    select.val(data.val);
                    loadBankLogo(data.val);
                }
                $this.closest(".modal").modal("hide");
                Ladda.stopAll();

            } else {
                $this.html($(data.html).find("form").html());
            }
        });

        return false;
    });

    function htmlEscape(str) {
        return str
            .replace(/&/g, \'&amp;\')
            .replace(/"/g, \'&quot;\')
            .replace(/\'/g, \'&#39;\')
            .replace(/</g, \'&lt;\')
            .replace(/>/g, \'&gt;\');
    }
', \yii\web\View::POS_HEAD);

$this->registerJs('

    $(document).on("select2:selecting", "#invoice-agreement", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-agreement-item")) {
            e.preventDefault();

            $("#invoice-agreement").select2("close");

            $.pjax({
                url: "/documents/agreement/update" +
                    "?id=" + $(target).data("id") +
                    "&contractor_id=" + $("#invoice-contractor_id").val() +
                    "&type=" + $("#documentType").val() +
                    "&returnTo=invoice" +
                    "&container=agreement-select-container" +
                    "&old_record=1" +
                    "&hide_template_fields=1",
                container: "#agreement-form-container",
                push: false,
                timeout: 5000,
            });

            $(document).on("pjax:success", function() {
                $("#agreement-modal-header").html($("[data-header]").data("header"));
                $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

                function dateChanged(ev) {
                    if (ev.bubbles == undefined) {
                        var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                        if (ev.currentTarget.value == "") {
                            if ($input.data("last-value") == null) {
                                $input.data("last-value", ev.currentTarget.defaultValue);
                            }
                            var $lastDate = $input.data("last-value");
                            $input.datepicker("setDate", $lastDate);
                        } else {
                            $input.data("last-value", ev.currentTarget.value);
                        }
                    }
                }
            });
            $("#agreement-modal-container").modal("show");
            $("#invoice-agreement").val("").trigger("change");

        }
    });
    $(document).on("select2:open", "#invoice-agreement", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });

    $(document).on("select2:open", "#invoice-company_rs", function(e) {
        setTimeout(func, 100);
        function func() {
            $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
                .attr("aria-selected", "false")
                .addClass("selected");
        }
    });
    $(document).on("select2:selecting", "#invoice-company_rs", function(e) {
        var target = e.params.args.originalEvent.target;

        if (target.classList.contains("edit-company_rs-item") || target.classList.contains("ajax-modal-btn")) {
            e.preventDefault();
        }
    });
    $(document).on("keyup", "input.select2-search__field", function (e) {
        $(".select2-results__options:visible .select2-results__option[aria-selected=true]")
            .attr("aria-selected", "false")
            .addClass("selected");
    });

    $(document).on("shown.bs.modal", ".modal#ajax-modal-box", function () {
        $("#invoice-company_rs").select2("close");
        $(this).find("input[type=\"checkbox\"]").uniform();
    });

    $(document).on("click", "#contractor_update_button", function() {
            var url  = "add-modal-contractor";
            var form = {
                documentType: INVOICE.documentIoType,
                contractorId: $(this).data("contractor-id") || 0,
                invoiceId: $(this).data("invoice-id") || 0,
        };
        INVOICE.addNewContractor("add-modal-contractor", form);
    });

    $("#add-from-exists").on("click", ".store-opt", function(e) {
        var store_id = $(this).data("id");
        var store_name = $(this).html();
        $("#add-from-exists").find(".store-name").html(store_name);
        $("#invoice-store_id").val(store_id);
        $("#add-from-exists").find("#storeIdHidden").val(store_id);

        $("#products_in_order").submit();
    });
');