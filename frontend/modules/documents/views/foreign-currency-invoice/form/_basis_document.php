<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Agreement;
use common\components\date\DateHelper;

$agreementDropDownList = [];
$companyAgreementID = [];
$outnds = 0;
if (isset ($agreementBasis) && $agreementBasis instanceof Agreement) {
    if ($agreementBasis->contractor) {
        $itemValue = $agreementBasis->getListItemValue();
        $model->agreement = $itemValue;
        $agreementDropDownList = [$itemValue => $agreementBasis->getListItemName()];

        if ($agreementBasis->contractor->isOutInvoiceHasNds) {
            $outnds = 1;
        }
    }

} else {
    if ($model->contractor) {
        $agreementArray = $model->contractor->getAgreements()->joinWith('agreementType')->andWhere([
            'agreement.type' => $model->type,
        ])->orderBy([
            'agreement_type.name' => SORT_ASC,
            'agreement.document_date' => SORT_DESC,
        ])->all();
        $agreementDropDownList += ['add-modal-agreement' => '[ + Добавить договор ]'];

        /** @var $agreement \common\models\Agreement */
        foreach ($agreementArray as $agreement) {
            $key = $agreement->getListItemValue();
            $agreementDropDownList[$key] = $agreement->getListItemName();
            $companyAgreementID[$key] = $agreement->id;
        }
        if ($model->contractor->isOutInvoiceHasNds) {
            $outnds = 1;
        }
    }
}

Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['/documents/invoice/basis-document']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'invoice-agreement',
    'model' => $model,
    'attribute' => 'agreement',
    'data' => $agreementDropDownList,
    'hideSearch' => true,
    'pluginOptions' => [
        'allowClear' => true,
        'placeholder' => '',
        'templateResult' => new \yii\web\JsExpression('formatAgreementResult'),
        'language' => [
            'noResults' =>  new \yii\web\JsExpression('function(){
                return $("<span />").attr("style", "color:red").html("Сначала выберите покупателя");
            }'),
        ]
    ],
    'options' => [
        'data' => [
            'delay' => !empty($delay) ? $delay : 10,
            'refresh' => empty($refresh) ? 'false' : 'true',
            'invoice_count' => empty($invoiceCount) ? 0 : $invoiceCount,
            'invoice_sum' => empty($invoiceSum) ? 0 : $invoiceSum,
            'expenditure' => $model->contractor ? $model->contractor->invoice_expenditure_item_id : '',
            'outnds' => $outnds,
            'contacts' => $model->contractor ? $model->contractor->getAttributes([
                'director_name',
                'director_email',
                'chief_accountant_is_director',
                'chief_accountant_name',
                'chief_accountant_email',
                'contact_is_director',
                'contact_name',
                'contact_email',
            ]) : null,
        ]
    ]
]);

$this->registerJs('

    $companyAgreementID = ' . json_encode($companyAgreementID) . ';
    function formatAgreementResult(data, container) {
        if (data.id && data.id !== "add-modal-agreement" && data.disabled !== true) {
            var input = $("#invoice-agreement");
            var content = \'<div class="agreement-item-name-label">\';

            content += \'<i class="icon icon-pencil pull-right edit-agreement-item" data-id="\' + $companyAgreementID[data.id] + \'" title="Обновить"></i>\';
            content += \'<div class="item-name" title="\' + htmlEscape(data.text) + \'">\' + data.text + \'</div>\';
            content += \'</div>\';
            container.innerHTML = content;
        } else if (data.id) {
            var input = $("#invoice-agreement");
            $(container).addClass("ajax-modal-btn").attr("title", "Добавить договор").attr("data-url", input.data("create-url"));
            container.innerHTML = data.text;
        } else {
            container.innerHTML = data.text;
        }

        return container;
    };
', \yii\web\View::POS_HEAD);

Pjax::end();

$this->registerJs('
    $(document).on("select2:open", "#invoice-agreement", function(e) {
        var $this = $(this);
        setTimeout(func, 100);
        function func() {
            var $items = $(".select2-results__options:visible .select2-results__option");
            if ($items.length == 1 && $($items[0]).text().trim() == "[ + Добавить договор ]") {
                $this.val("add-modal-agreement");
                $this.trigger("change")
                $this.select2("close");;
            }
        }
    });
');

