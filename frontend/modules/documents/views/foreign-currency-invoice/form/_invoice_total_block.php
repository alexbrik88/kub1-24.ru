<?php
use common\components\TextHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\NdsOsno;
use common\models\document\Invoice;
use frontend\models\Documents;
use yii\bootstrap\Html;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model Invoice */

$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";
?>

<div class="portlet pull-right" id="invoice-sum" style="margin-bottom: 0;">

    <table class="table table-resume">
        <tbody>
            <tr role="row" class="discount_column<?= $model->has_discount ? '': ' hidden'; ?>">
                <td>
                    <b>
                        <?= YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Сумма скидки'; ?><span class="total_currency_label"><?= $currCode ?></span>:
                    </b>
                </td>
                <td class="discount_sum"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_discount); ?></td>
            </tr>
            <tr role="row">
                <td><b>Итого<span class="total_currency_label"><?= $currCode ?></span>:</b></td>
                <td class="total_price"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_amount); ?></td>
            </tr>
        <?php if ($model->type == Documents::IO_TYPE_IN): ?>
            <tr role="row">
                <td>
                    <?php \yii\widgets\Pjax::begin([
                        'id' => 'nds_view_type_id-pjax-container',
                        'enablePushState' => false,
                        'linkSelector' => false,
                        'options' => ['style' => 'display: inline-block;']
                    ]); ?>
                        <?= Html::activeDropDownList($model, 'nds_view_type_id', Invoice::$ndsViewList, [
                            'class' => 'nds-select',
                            'data' => ['id' => $model->nds_view_type_id],
                        ]) ?>
                    <?php \yii\widgets\Pjax::end(); ?>
                </td>
                <td class="including_nds"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?></td>
            </tr>
        <?php else: ?>
            <tr role="row" class="nds-view-item type-0 <?= $model->nds_view_type_id == 0 ? '' : 'hidden'; ?>">
                <td>
                    <b>
                        В том числе НДС:
                    </b>
                </td>
                <td class="including_nds">
                    <?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?>
                </td>
            </tr>
            <tr role="row" class="nds-view-item type-1 <?= $model->nds_view_type_id == 1 ? '' : 'hidden'; ?>">
                <td>
                    <b>
                        НДС сверху:
                    </b>
                </td>
                <td class="including_nds">
                    <?= TextHelper::moneyFormatFromIntToFloat($model->view_total_nds); ?>
                </td>
            </tr>
            <tr role="row" class="nds-view-item type-2 <?= $model->nds_view_type_id == 2 ? '' : 'hidden'; ?>">
                <td>
                    Без налога (НДС)
                    <?= Html::activeHiddenInput($model, 'nds_view_type_id', [
                        'data' => ['id' => $model->nds_view_type_id],
                    ]) ?>
                </td>
                <td>-</td>
            </tr>
        <?php endif; ?>
        <tr role="row">
            <td><b>Всего к оплате<span class="total_currency_label"><?= $currCode ?></span>:</b></td>
            <td class="amount"><?= TextHelper::moneyFormatFromIntToFloat($model->view_total_with_nds); ?></td>
        </tr>
        </tbody>
    </table>
</div>

<?php
if ($model->type == Documents::IO_TYPE_IN) {
    $this->registerJs('
    var checkInInvoiceNds = function() {
        var noNds = $("#invoice-nds_view_type_id").val() == "2";
        $(".with-nds-item").toggleClass("hidden", noNds);
        $(".without-nds-item").toggleClass("hidden", !noNds);
    }
    $(document).on("change", "#invoice-nds_view_type_id", function() {
        checkInInvoiceNds();
        INVOICE.recalculateInvoiceTable();
    });
    $(document).on("change", "#invoice-contractor_id", function(){
        $.pjax.reload("#nds_view_type_id-pjax-container", {"type": "post", "data": $(this).closest("form").serialize()});
    });
    $(document).on("pjax:complete", "#nds_view_type_id-pjax-container", function(){
        checkInInvoiceNds();
        /*$(".nds-select").select2();*/
        INVOICE.recalculateInvoiceTable();
    });
    ');
}
?>
