<?php

use common\components\grid\DropDownDataColumn;
use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\modules\documents\components\FilterHelper;
use yii\helpers\Html;
use common\components\grid\GridView;
use \frontend\components\StatisticPeriod;
use common\components\date\DateHelper;
use yii\helpers\Url;

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\documents\models\InvoiceSearch */
/* @var string $documentNumber */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h3 style="text-align: center; margin: 0">Добавить <?= $documentTypeName ?></h3>
</div>
<div class="invoice-list">
    <?= Html::beginForm(["/documents/{$documentType}/get-invoices"], 'get', [
        'id' => 'search-invoice-form',
        'class' => 'add-to-payment-order',
        'data' => [
            'pjax' => true,
        ],
    ]); ?>

    <?= Html::hiddenInput('type', $ioType) ?>

    <div class="portlet box btn-invoice m-b-0 shadow-t">

        <?php if ($canChangePeriod): ?>

            <?= Html::hiddenInput('date_from', DateHelper::format($dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('date_to', DateHelper::format($dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)) ?>
            <?= Html::hiddenInput('label_name', $labelName) ?>

            <div class="search-form-default">
                <div class="col-xs-8 col-md-8 serveces-search m-l-n-sm m-t-10">
                    <div class="input-group">
                        <div class="input-cont inp_pad">
                            <?php echo Html::activeTextInput($searchModel, 'document_number', [
                                'id' => 'invoice-number-search',
                                'placeholder' => '№ счёта...',
                                'class' => 'form-control',
                            ]); ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
            <div id="range-ajax-button">
                <div
                    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom ajax m-r-10 m-t-10"
                    data-pjax="get-vacant-invoices-pjax" id="reportrange2">

                    <?php
                    $this->registerJs("
                    $('#reportrange2').daterangepicker({
                        format: 'DD.MM.YYYY',
                        ranges: {" . StatisticPeriod::getRangesJs() . "},
                        startDate: '" . $dateFrom . "',
                        endDate: '" . $dateTo . "',
                        locale: {
                            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                            firstDay: 1
                        }
                    });

                    "); ?>

                    <?= ($labelName == 'Указать диапазон') ? "{$dateFrom}-{$dateTo}" : $labelName ?>
                    <b class="fa fa-angle-down"></b>
                </div>

            </div>

        <?php endif; ?>

        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <div id="in_invoice_table" class="dataTables_wrapper dataTables_extended_wrapper">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table',
                            'id' => 'datatable_ajax',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'options' => [
                            'id' => 'invoice-payment-order',
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right m-t',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'columns' => [
                            [
                                'attribute' => 'invoice_id',
                                'label' => '',
                                'format' => 'raw',
                                'contentOptions' => [
                                    'style' => 'padding: 7px 5px 5px 5px;',
                                ],
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::checkbox(Html::getInputName($data, 'id') . '[]', false, [
                                        'value' => $data->id,
                                        'class' => 'checkbox-invoice-id',
                                        'style' => 'margin-left: -9px;',
                                    ]);
                                },
                                'headerOptions' => [
                                    'width' => '4%',
                                ],
                            ],
                            [
                                'attribute' => 'document_date',
                                'label' => 'Дата счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => ['date', 'php:' . \common\components\date\DateHelper::FORMAT_USER_DATE],
                            ],
                            [
                                'attribute' => 'document_number',
                                'label' => '№ счёта',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    /** @var Invoice $data */
                                    return Html::a($data->fullNumber, [
                                            'invoice/view',
                                            'type' => $data->type,
                                            'id' => $data->id], [
                                            'data-pjax' => '0',
                                            'target' => '_blank'
                                        ]);
                                },
                            ],
                            [
                                'label' => 'Сумма',
                                'headerOptions' => [
                                    'class' => 'sorting',
                                    'width' => '24%',
                                ],
                                'attribute' => 'total_amount_with_nds',
                                'value' => function (Invoice $model) {
                                    return TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2);
                                },
                            ],
                            [
                                'attribute' => 'contractor_id',
                                'label' => 'Контрагент',
                                'class' => \common\components\grid\DropDownSearchDataColumn::className(),
                                'headerOptions' => [
                                    'width' => '24%',
                                ],
                                'filter' => FilterHelper::getContractorList($searchModel->type, Invoice::tableName(), true, false, false),
                                'format' => 'raw',
                                'value' => 'contractor_name_short',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
</div>
<div class="row action-buttons pad-10">
    <div class="col-sm-12" style="padding-bottom: 10px">
        <?= \yii\helpers\Html::checkbox('add-new-invoice', false, [
            'id' => 'add-new-invoice',
            'label' => "Создать {$documentTypeName} и новый счет",
        ]); ?>
    </div>
    <div class="col-sm-12">
        <button id="add-act-to-invoice"
                class="btn btn-add darkblue"
                data-dismiss="modal"
                data-url="<?= Url::to(["/documents/{$documentType}/create", 'type' => $ioType]) ?>"
                data-url-new-invoice="<?= Url::to(['/documents/invoice/create', 'type' => $ioType, 'document' => $documentType]) ?>"
                disabled="">Добавить</button>
        <button class="btn btn-add darkblue pull-right" data-dismiss="modal">Отменить</button>
    </div>
</div>