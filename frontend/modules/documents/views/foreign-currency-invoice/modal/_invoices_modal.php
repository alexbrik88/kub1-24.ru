<?php
use yii\widgets\Pjax;
?>
<div class="modal fade t-p-f" id="accounts-list" tabindex="1" role="modal" aria-hidden="true">
    <div class="modal-dialog" style="width:700px;">
        <div class="modal-content">
            <?php Pjax::begin([
                'id' => 'get-vacant-invoices-pjax',
                'linkSelector' => false,
                'formSelector' => '#search-invoice-form',
                'timeout' => 5000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3 style="text-align: center; margin: 0">Добавить <?= $documentTypeName ?></h3>
            </div>
            <div class="invoice-list"></div>
            <div class="row action-buttons pad-10">
                <div class="col-sm-12">
                    <button id="add-act-to-invoice" class="btn btn-add darkblue" data-dismiss="modal" disabled="">Добавить</button>
                    <button class="btn btn-add darkblue pull-right" data-dismiss="modal">Отменить</button>
                </div>
            </div>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$this->registerJs("

    $('.add-document').on('click', function () {
        $('#accounts-list').modal();
        $.pjax({
            url: '/documents/{$documentType}/get-invoices',
            container: '#get-vacant-invoices-pjax',
            data: {type: {$ioType}},
            push: false,
            timeout: 5000
        });
    });

    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange2', function (ev, picker) {
        var form = $('#search-invoice-form');
        $(form).find('input[name=\"date_from\"]').val(picker.startDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"date_to\"]').val(picker.endDate.format('YYYY-MM-DD'));
        $(form).find('input[name=\"label_name\"]').val(picker.chosenLabel);
        $('#search-invoice-form').submit();
    });

    $(document).on('click', '.checkbox-invoice-id, #add-new-invoice', function() {
        var checkboxes = $('.checkbox-invoice-id');
        var button = $('#add-act-to-invoice');
        var addNew = $('#add-new-invoice');
        if ($(checkboxes).filter(':checked').length || $(addNew).prop('checked')) {
            $(button).removeAttr('disabled');
            if ($(checkboxes).filter(':checked').length) {
                $(addNew).prop('checked', false).attr('disabled', true).uniform();
            }
        } else {
            $(button).attr('disabled', 'disabled');
            $(addNew).removeAttr('disabled').uniform();
        }
        if ($(checkboxes).filter(':checked').length > 1) {
            $(checkboxes).filter(':checked').prop('checked', false);
            $(this).prop('checked', true);
            $(checkboxes).uniform();
        }
    });

    $(document).on('click', '#add-act-to-invoice', function () {
        var checkboxes = $('.checkbox-invoice-id');
        var isAddNew = $('#add-new-invoice').filter(':checked').length;

        if (isAddNew) {
            location.href = $(this).data('url-new-invoice')
        } else {
            var invoiceId = $(checkboxes).filter(':checked').first().val();
            location.href = $(this).data('url') + '&invoiceId=' + invoiceId;
        }
    });

"); ?>

