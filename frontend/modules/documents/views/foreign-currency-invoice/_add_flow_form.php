<?php

use common\components\date\DateHelper;
use common\models\cash\CashFactory;
use common\models\currency\Currency;
use frontend\models\Documents;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\models\bank\BankingParams;

/* @var $this \yii\web\View */
/* @var $model \common\models\document\Invoice */
/* @var $useContractor string */
/* @var $company \common\models\Company */

$addFlowForm = new \frontend\modules\documents\forms\InvoiceFlowForm([
    'invoice' => $model,
    'employee' => Yii::$app->user->identity,
    'date_pay' => date(DateHelper::FORMAT_USER_DATE),
]);
$user = Yii::$app->user->identity;
$company = $user->company;
$link = null;
$image = null;
if ($alias = Banking::aliasByBik($model->company_bik)) {
    $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
    $banking = new $bankingClass($company, [
        'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
    ]);
    if ($banking->getHasAutoload() && !$banking->isValidToken()) {
        $link = Html::a("Получать информацию об оплате счетов из банка автоматически", Url::to([
            "/cash/banking/{$alias}/default/index",
        ]), [
            'style' => 'font-size: 13px;margin-left: 10px;',
        ]);

        if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
            $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                'class' => 'little_logo_bank',
                'style' => 'display: inline-block;',
            ]);
        }
    }

}

if (!empty($_GET['InvoiceFlowForm']['amount'])) {
    $addFlowForm->amount = $_GET['InvoiceFlowForm']['amount'] * 100;
}

$flowType = (int)\Yii::$app->request->get('flowType');
if (!in_array($flowType, CashFactory::$flowTypeArray)) {
    $flowType = CashFactory::TYPE_BANK;
}

$action = [
    'add-flow',
    'type' => $model->type,
    'id' => Yii::$app->request->get('id'),
];

if ($useContractor) {
    $action['contractorId'] = $model->contractor_id;
}
?>

<?php
$form = ActiveForm::begin([
    'action' => $action,
    'options' => [
        'id' => 'add-flow-form',
        'class' => 'add-to-invoice',
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
]);

$this->registerJs('
$("#invoiceflowform-date_pay").datepicker({
    language: "ru",
    autoclose: true,
}).on("change.dp", dateChanged);

function dateChanged(ev) {
    if (ev.bubbles == undefined) {
        var $input = $("#" + ev.currentTarget.id);
        if (ev.currentTarget.value == "") {
            if ($input.data("last-value") == null) {
                $input.data("last-value", ev.currentTarget.defaultValue);
            }
            var $lastDate = $input.data("last-value");
            $input.datepicker("setDate", $lastDate);
        } else {
            $input.data("last-value", ev.currentTarget.value);
        }
    }
}

$(document).on("click", "#flow-list-pjax a", function(e) {
    e.preventDefault();
    $.pjax.reload("#flow-list-pjax", {
        "url":$(this).attr("href"),
        "data":$("#invoiceflowform-amount").serialize(),
        "push":false,
        "replace":false,
        "timeout":10000,
        "scrollTo":false,
        "container":"#flow-list-pjax"
    });
});
$(document).on("pjax:complete", "#flow-list-pjax", function() {
    $("#flow-list-pjax input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
    $("#flow-list-pjax input:radio:not(.md-radiobtn)").uniform();
});
$(document).on("change", ".flow_item_check", function() {
    var flowsSum = 0;
    $(".flow_item_check:checked").each(function(i, item) {
        flowsSum += parseFloat($(item).closest("tr").find("span.sum").text());
    });
    $("#selectedFlowsAmount").val(flowsSum);
    if (flowsSum == parseFloat($("#invoiceflowform-amount").val())) {
        $("#invoiceflowform-amount").css("color", "green");
    } else {
        $("#invoiceflowform-amount").css("color", "red");
    }
});
$(document).on("change", "#invoiceflowform-createnew", function() {
    if ($("#invoiceflowform-createnew").is(":checked")) {
        $(".flow_item_check").prop( "checked", false ).prop("disabled", true).parent().removeClass("checked");
        $("#selectedFlowsAmount").val(0);
    } else {
        $(".flow_item_check").prop("disabled", false);
    }
});
$(document).on("change", "#invoiceflowform-date_pay", function() {
    var $input = $(this);
    if ($input.data("request-amount")) {
        var result;
        result = INVOICE.getInvoicesWithAmountOnDate($input.data("amount-url"), $input.data("model-id"), $input.val());
        if(result.available) {
            $("#invoiceflowform-amount").val(result.available.toFixed(2));
        }
    }
});
');
?>

<div class="form-body">

    <div class="form-group row block_left width_50">
        <label for="under-date" class="col-md-2 control-label width_100">Дата оплаты</label>

        <div class="col-md-5">
            <div class="input-icon" style="position: relative;">
                <i class="fa fa-calendar"></i>
                <?= $form->field($addFlowForm, 'date_pay')->label(false)->textInput([
                    'class' => 'form-control width_date',
                    'style' => 'padding-left: 35px;',
                    'size' => 10,
                    'data' => [
                        'date-viewmode' => 'years',
                        'request-amount' => $model->currency_rate_type == Currency::RATE_PAYMENT_DATE,
                        'amount-url' => Url::to([
                            '/documents/invoice/amount-on-date',
                            'type' => $model->type,
                            'id' => $model->id,
                        ]),
                        'model-id' => $model->id,
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'id' => 'flow-list-pjax',
        'enablePushState' => false,
        'linkSelector' => false,
        'timeout' => 10000,
    ]); ?>

    <div class="form-group row block_left width_50<?= $model->isNewRecord ? ' hidden' : ''; ?>">
        <?= Html::activeLabel($addFlowForm, 'amount', [
            'class' => 'col-md-2 control-label width_100',
        ]); ?>

        <div class="col-md-8 width-col-7">
            <?= $form->field($addFlowForm, 'amount')->textInput([
                'class' => 'form-control js_input_to_money_format',
                'value' => $addFlowForm->amount / 100,
                'data' => [
                    'amount' => $addFlowForm->amount / 100,
                ],
            ])->label(false); ?>
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row form-group">
        <?php $flowTypeName = Html::getInputName($addFlowForm, 'flowType'); ?>
        <div class="col-sm-4" style="max-width: 130px;">
            <a href="<?= Url::current(['flowType' => CashFactory::TYPE_BANK]) ?>"
               style="text-decoration: none;color: #333333!important;">
                <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_BANK, [
                    'value' => CashFactory::TYPE_BANK,
                ]) ?>
                через банк
            </a>
        </div>
        <?php if ($addFlowForm->cashboxList) : ?>
            <div class="col-sm-4" style="max-width: 130px;">
                <a href="<?= Url::current(['flowType' => CashFactory::TYPE_ORDER]) ?>"
                   style="text-decoration: none;color: #333333!important;">
                    <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_ORDER, [
                        'value' => CashFactory::TYPE_ORDER,
                    ]) ?>
                    через кассу
                </a>
            </div>
        <?php endif ?>
        <div class="col-sm-4" style="max-width: 150px;">
            <a href="<?= Url::current(['flowType' => CashFactory::TYPE_EMONEY]) ?>"
               style="text-decoration: none;color: #333333!important;">
                <?= Html::radio($flowTypeName, $flowType == CashFactory::TYPE_EMONEY, [
                    'value' => CashFactory::TYPE_EMONEY,
                ]) ?>
                через e-money
            </a>
        </div>
    </div>

    <?php if ($flowType == CashFactory::TYPE_ORDER && $addFlowForm->cashboxList) : ?>
        <?= $form->field($addFlowForm, 'cashbox_id', [
            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group',
            ],
            'labelOptions' => [
                'class' => 'col-sm-4',
                'style' => 'display: block; max-width: 130px;',
            ]
        ])->radioList($addFlowForm->cashboxList, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif ?>

    <?php if ($flowType == CashFactory::TYPE_EMONEY && $addFlowForm->emoneyList) : ?>
        <?= $form->field($addFlowForm, 'emoney_id', [
            'template' => "{label}\n<div class=\"col-sm-8\">\n{input}\n</div>\n<div class=\"col-sm-12\">\n{hint}\n{error}\n</div>",
            'options' => [
                'class' => 'row form-group',
            ],
            'labelOptions' => [
                'class' => 'col-sm-4',
                'style' => 'display: block; max-width: 130px;',
            ]
        ])->radioList($addFlowForm->emoneyList, [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::radio($name, $checked, [
                    'value' => $value,
                    'label' => $label,
                    'labelOptions' => [
                        'style' => 'display: block',
                    ],
                ]);
            },
        ]); ?>
    <?php endif ?>

    <?php
    $isOutInvoice = ($model->type == Documents::IO_TYPE_OUT);

    switch ($flowType) {
        case CashFactory::TYPE_BANK:
            $query = $model->getPossibleBankFlows();
            break;
        case CashFactory::TYPE_ORDER:
            $query = $model->getPossibleOrderFlows();
            break;
        case CashFactory::TYPE_EMONEY:
            $query = $model->getPossibleEmoneyFlows();
            break;
        default:
            $query = null;
            break;
    }

    if ($query) {
        $flowProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $flowProvider->pagination->pageSize = 0;

        if ($flowProvider->totalCount) {
            echo \common\components\grid\GridView::widget([
                'dataProvider' => $flowProvider,
                'filterModel' => $model,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable documents_table status_nowrap ',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper bank-scroll-table',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => "{items}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\CheckboxColumn',
                        'cssClass' => 'flow_item_check',
                        'name' => Html::getInputName($addFlowForm, 'selectedFlows'),
                        'header' => false,
                        'headerOptions' => [
                            'width' => '40px',
                        ],
                    ],
                    [
                        'attribute' => 'date',
                        'label' => 'Дата',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'label' => $isOutInvoice ? 'Приход' : 'Расход',
                        'attribute' => 'amount',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '20%',
                        ],
                        'contentOptions' => [
                            'style' => 'text-align:right;',
                        ],
                        'value' => function ($model) {
                            return '<span class="sum" style="display:none;">' . intval($model->availableAmount) / 100 . '</span>' .
                                \common\components\TextHelper::invoiceMoneyFormat($model->amount, 2);
                        },
                        'format' => 'raw'
                    ],
                    [
                        'attribute' => 'description',
                        'label' => 'Назначение',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '60%',
                        ],
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($invoiceArray = $data->getInvoices()->all()) {
                                $linkArray = [];
                                foreach ($invoiceArray as $invoice) {
                                    $linkArray[] = Html::a($data->formattedDescription . $invoice->fullNumber, [
                                        '/documents/invoice/view',
                                        'type' => $invoice->type,
                                        'id' => $invoice->id,
                                    ]);
                                }
                                return join(', ', $linkArray);
                            } else {
                                $description = mb_substr($data->description, 0, 50) . '<br>' . mb_substr($data->description, 50, 50);
                                return Html::label(strlen($data->description) > 100 ? $description . '...' : $description, null, ['title' => $data->description]);
                            }
                        },
                    ],
                ],
            ]);
            echo $form->field($addFlowForm, 'selectedFlowsAmount')->hiddenInput(['id' => 'selectedFlowsAmount'])->label(false);
            echo $form->field($addFlowForm, 'createNew', ['enableClientValidation' => false,])->checkbox();
        } else {
            echo $form->field($addFlowForm, 'createNew')->hiddenInput(['value' => 1])->label(false);
        }
    }

    if ($flowType != CashFactory::TYPE_BANK) {
        echo $form->field($addFlowForm, 'isAccounting')->checkbox();
    }
    ?>
    <?php if ($flowType == CashFactory::TYPE_BANK && $link): ?>
        <div class="main-bank_logo" style="margin-bottom: 20px;">
            <?= $image . $link; ?>
        </div>
    <?php endif; ?>
    <?php \yii\widgets\Pjax::end(); ?>
</div>

<div class="clearfix"></div>
<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::submitButton('<span class="ladda-label">Добавить</span><span class="ladda-spinner"></span>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button' : 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width: 130px!important;',
            ]) ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn btn-primary widthe-100 hidden-lg'
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <a href="#"
               class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal" aria-hidden="true">Отменить</a>
            <a href="#" class="btn darkblue widthe-100 hidden-lg"
               title="Отменить" data-dismiss="modal" aria-hidden="true"><i class="fa fa-reply fa-2x"></i></a>
        </div>
    </div>
</div>

<?php $form->end(); ?>
