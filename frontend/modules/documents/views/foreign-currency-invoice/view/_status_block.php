<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use frontend\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\widgets\BoolleanSwitchWidget;
use common\models\employee\EmployeeRole;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $invoiceFlowCalc common\models\document\InvoiceFlowCalculator */
/* @var $useContractor string */
/* @var $user \common\models\employee\Employee */

$status = $model->invoiceStatus;
$styleClass = $model->isOverdue() ? 'red' : $status->getStyleClass();
$user = Yii::$app->user->identity;

$createdInvoicesCount = $model->company->getInvoices()->where(['from_demo_out_invoice' => 0])->count();
?>
    <style>
        .update-attribute-tooltip-content {
            text-align: center;
        }
    </style>
    <!-- _status_block -->
    <div class="control-panel col-xs-12 pad0 pull-right">
        <!--    <div class="status-panel pull-right"> -->
        <div class="status-panel col-xs-12 pad0">
            <div class="col-xs-12 col-sm-3 pad3">
                <div class="btn full_w marg <?= $styleClass; ?>"
                     style="padding-left:0px; padding-right:0px;text-align: center; "
                     title="Дата изменения статуса">
                    <?= date("d.m.Y", $model->invoice_status_updated_at); ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 pad0">
                <div class="col-xs-7 pad3">
                    <div class="btn full_w marg <?= $styleClass; ?>"
                         title="Статус">
                        <?php if ($model->getIsFullyPaid()) : ?>
                            <span class="pull-left"><?= $model->getPaymentIcon() ?></span>
                        <?php else : ?>
                            <span class="icon pull-left <?= $status->getIcon(); ?>"></span>
                        <?php endif; ?>
                        <?= $status->name; ?>
                    </div>
                </div>
                <div class="col-xs-5 pad3">
                    <div class="btn full_w marg <?= $styleClass; ?>"
                         style="padding-left:0px; padding-right:0px;text-align: center; "
                         title="Сумма оплаты">
                        <i class="fa fa-rub"></i> <?= TextHelper::invoiceMoneyFormat($model->getPaidAmount(), 2); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
            <div class="col-xs-3"> &nbsp;</div>
            <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
                <div class="document-panel btn-group tooltip2" id="document-panel-pulsate"
                     style="width: 99.9% !important; margin-bottom: 0px;"
                     data-tooltip-content="#status-block-tooltip">
                    <?php if (!$model->has_upd) : ?>
                        <?php if ($model->has_services) { ?>
                            <?php if (!$model->need_act): ?>
                                <div class="clearfix">
                                    <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                        BoolleanSwitchWidget::widget([
                                            'id' => 'add-act',
                                            'action' => [
                                                '/documents/act/create',
                                                'type' => $model->type,
                                                'invoiceId' => $model->id,
                                            ],
                                            'inputName' => 'add_act',
                                            'inputValue' => 1,
                                            'label' => 'Выставить Акт?',
                                            'addFalseForm' => false,
                                            'toggleButton' => [
                                                'tag' => 'span',
                                                'label' => 'БЕЗ АКТа',
                                                'class' => 'btn yellow full_w disabled',
                                                'style' => 'height: 34px;margin-left: 0;background-color: #a2a2a2;',
                                            ],
                                            'options' => [
                                                'style' => 'width: 110px;',
                                            ],
                                        ]) : '<span class="btn yellow full_w disabled" style="height: 34px;margin-left: 0;background-color: #a2a2a2;">БЕЗ АКТа</span>'; ?>
                                </div>
                            <?php else: ?>
                                <?php if (!$model->acts) { ?>
                                    <?= $this->render('status_rows/_first_row', [
                                        'document' => 'act',
                                        'title' => 'акт',
                                        'model' => $model,
                                        'action' => 'create',
                                        'id' => 0,
                                    ]);
                                    ?>
                                <?php } elseif (count($model->acts) == 1 && !$model->canAddAct) { ?>
                                    <?= $this->render('status_rows/_first_row', [
                                        'document' => 'act',
                                        'title' => 'акт',
                                        'model' => $model,
                                        'action' => 'view',
                                        'id' => $model->acts[0]->id,
                                    ]);
                                    ?>
                                <?php } else { ?>
                                    <?= $this->render('status_rows/_many_rows', [
                                        'document' => 'act',
                                        'title' => 'акт',
                                        'model' => $model,
                                        'addAvailable' => $model->canAddAct,
                                    ]); ?>
                                <?php } ?>
                            <?php endif; ?>
                        <?php } ?>

                        <?php if ($model->has_goods) { ?>
                            <?php if (!$model->need_packing_list): ?>
                                <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                    BoolleanSwitchWidget::widget([
                                        'id' => 'add-packing-list',
                                        'action' => [
                                            '/documents/packing-list/create',
                                            'type' => $model->type,
                                            'invoiceId' => $model->id,
                                        ],
                                        'inputName' => 'add_packing_list',
                                        'inputValue' => 1,
                                        'label' => 'Выставить ТН?',
                                        'addFalseForm' => false,
                                        'toggleButton' => [
                                            'tag' => 'span',
                                            'label' => 'Без ТН',
                                            'class' => 'btn yellow full_w disabled',
                                            'style' => 'height: 34px;margin-left: 0;background-color: #a2a2a2;',
                                        ],
                                        'options' => [
                                            'style' => 'width: 110px;',
                                        ],
                                    ]) : '<span class="btn yellow full_w disabled" style="height: 34px;margin-left: 0;background-color: #a2a2a2;">БЕЗ ТН</span>'; ?>
                            <?php else: ?>
                                <?php if (!$model->packingLists) { ?>
                                    <?= $this->render('status_rows/_first_row', [
                                        'document' => 'packing-list',
                                        'title' => 'тов. накладная',
                                        'model' => $model,
                                        'action' => 'create',
                                        'id' => 0,
                                    ]);
                                    ?>
                                <?php } elseif (count($model->packingLists) == 1 && !$model->canAddPackingList) { ?>
                                    <?= $this->render('status_rows/_first_row', [
                                        'document' => 'packing-list',
                                        'title' => 'тов. накладная',
                                        'model' => $model,
                                        'action' => 'view',
                                        'id' => $model->packingLists[0]->id,
                                    ]);
                                    ?>
                                <?php } else { ?>
                                    <?= $this->render('status_rows/_many_rows', [
                                        'document' => 'packing-list',
                                        'title' => 'тов. накладная',
                                        'model' => $model,
                                        'addAvailable' => $model->canAddPackingList,
                                    ]);
                                    ?>
                                <?php } ?>
                            <?php endif; ?>
                        <?php } ?>

                        <?php if ($model->has_goods && $ioType == Documents::IO_TYPE_OUT) { ?>
                            <?php if (!$model->waybills) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'waybill',
                                    'title' => 'трансп. накладная',
                                    'model' => $model,
                                    'action' => 'create',
                                    'id' => 0,
                                ]);
                                ?>
                            <?php } elseif (count($model->waybills) == 1 && !$model->canAddWaybill) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'waybill',
                                    'title' => 'трансп. накладная',
                                    'model' => $model,
                                    'action' => 'view',
                                    'id' => $model->waybills[0]->id,
                                ]);
                                ?>
                            <?php } else { ?>
                                <?= $this->render('status_rows/_many_rows', [
                                    'document' => 'waybill',
                                    'title' => 'трансп. накладная',
                                    'model' => $model,
                                    'addAvailable' => $model->canAddWaybill,
                                ]);
                                ?>
                            <?php } ?>
                        <?php } ?>
                        <?php if ($model->hasNds): ?>
                            <?php if (!$model->invoiceFactures) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'invoice-facture',
                                    'title' => 'счёт-фактура',
                                    'model' => $model,
                                    'action' => 'create',
                                    'id' => 0,
                                ]);
                                ?>
                            <?php } elseif (count($model->invoiceFactures) == 1 && !$model->canAddInvoiceFacture) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'invoice-facture',
                                    'title' => 'счёт-фактура',
                                    'model' => $model,
                                    'action' => 'view',
                                    'id' => $model->invoiceFactures[0]->id,
                                ]);
                                ?>
                            <?php } else { ?>
                                <?= $this->render('status_rows/_many_rows', [
                                    'document' => 'invoice-facture',
                                    'title' => 'счёт-фактура',
                                    'model' => $model,
                                    'addAvailable' => $model->canAddInvoiceFacture,
                                ]);
                                ?>
                            <?php } ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if (!$model->hasDocs) : ?>
                        <?php if (!$model->need_upd): ?>
                            <?= $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF ?
                                BoolleanSwitchWidget::widget([
                                    'id' => 'add-upd',
                                    'action' => [
                                        '/documents/upd/create',
                                        'type' => $model->type,
                                        'invoiceId' => $model->id,
                                    ],
                                    'inputName' => 'add_upd',
                                    'inputValue' => 1,
                                    'label' => 'Выставить УПД?',
                                    'addFalseForm' => false,
                                    'toggleButton' => [
                                        'tag' => 'span',
                                        'label' => 'Без УПД',
                                        'class' => 'btn yellow full_w disabled',
                                        'style' => 'height: 34px;margin-left: 0;background-color: #a2a2a2;',
                                    ],
                                    'options' => [
                                        'style' => 'width: 115px;',
                                    ],
                                ]) : '<span class="btn yellow full_w disabled" style="height: 34px;margin-left: 0;background-color: #a2a2a2;">БЕЗ УПД</span>'; ?>
                        <?php else: ?>
                            <?php if (!$model->upds) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'upd',
                                    'title' => 'УПД',
                                    'model' => $model,
                                    'action' => 'create',
                                    'id' => 0,
                                ]);
                                ?>
                            <?php } elseif (count($model->upds) == 1 && !$model->canAddUpd) { ?>
                                <?= $this->render('status_rows/_first_row', [
                                    'document' => 'upd',
                                    'title' => 'УПД',
                                    'model' => $model,
                                    'action' => 'view',
                                    'id' => $model->upds[0]->id,
                                ]);
                                ?>
                            <?php } else { ?>
                                <?= $this->render('status_rows/_many_rows', [
                                    'document' => 'upd',
                                    'title' => 'УПД',
                                    'model' => $model,
                                    'addAvailable' => $model->canAddUpd,
                                ]);
                                ?>
                            <?php } ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php if ($model->has_goods && $ioType == Documents::IO_TYPE_IN) { ?>
                        <?php if (!$model->proxies) { ?>
                            <?= $this->render('status_rows/_first_row', [
                                'document' => 'proxy',
                                'title' => 'доверенность',
                                'model' => $model,
                                'action' => 'create',
                                'id' => 0,
                            ]);
                            ?>
                        <?php } elseif (count($model->proxies) == 1 && !$model->canAddProxy) { ?>
                            <?= $this->render('status_rows/_first_row', [
                                'document' => 'proxy',
                                'title' => 'доверенность',
                                'model' => $model,
                                'action' => 'view',
                                'id' => $model->proxies[0]->id,
                            ]);
                            ?>
                        <?php } else { ?>
                            <?= $this->render('status_rows/_many_rows', [
                                'document' => 'proxy',
                                'title' => 'доверенность',
                                'model' => $model,
                                'addAvailable' => $model->canAddProxy,
                            ]);
                            ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if ($model->orderDocument): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'order-document',
                            'title' => 'Заказ',
                            'model' => $model,
                            'action' => 'view',
                            'id' => $model->orderDocument->id,
                        ]); ?>
                    <?php endif; ?>
                    <?php if ($model->agentReport): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'agent-report',
                            'title' => 'Отчет Агента',
                            'model' => $model,
                            'action' => 'view',
                            'id' => $model->agentReport->id,
                        ]); ?>
                    <?php endif; ?>

                    <?php if ($ioType == Documents::IO_TYPE_IN && $model->paymentOrderInvoices) : ?>
                        <?php if (count($model->paymentOrderInvoices) == 1): ?>
                            <?= Html::a('<i class="pull-left icon icon-doc"></i> ПЛАТЁЖНОЕ ПОРУЧЕНИЕ', [
                                '/documents/payment-order/view',
                                'id' => $model->paymentOrderInvoices[0]->payment_order_id,
                            ], [
                                'class' => 'btn yellow width317 tooltipstered' . ($model->isRejected ? ' disabled' : ''),
                                'style' => 'width: 100%!important;' . ($model->isRejected ? ' background-color: #a2a2a2;' : ''),
                                'title' => 'Платежное поручение',
                            ]) ?>
                        <?php else: ?>
                            <div class="clearfix" style="display: block; position: relative;">
                                <div class="col-xs-12 pad0">
                                    <a data-toggle="dropdown"
                                       class="btn yellow dropdown-toggle tooltipstered<?= $model->isRejected ? ' disabled' : ''; ?>"
                                        <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
                                       style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
                                       title="ПЛАТЁЖНОЕ ПОРУЧЕНИЕ">
                                        <i class="pull-left icon icon-doc"></i>
                                        ПЛАТЁЖНОЕ ПОРУЧЕНИЕ
                                    </a>
                                    <ul class="dropdown-menu" style="width: 317px;">
                                        <?php foreach ($model->paymentOrderInvoices as $paymentOrderInvoice): ?>
                                            <li>
                                                <a class="dropdown-item" style="position: relative;"
                                                   href="<?= Url::to(['payment-order/view', 'id' => $paymentOrderInvoice->payment_order_id]); ?>">
                                            <span class="dropdown-span-left">
                                                № <?= $paymentOrderInvoice->paymentOrder->document_number ?>
                                                от <?= Yii::$app->formatter->asDate($paymentOrderInvoice->paymentOrder->document_date); ?>
                                            </span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>

<?php if ($createdInvoicesCount <= 3) {
    $this->registerJs('
        // Pulsate block
        (function($) {
            jQuery.fn.weOffset = function () {
                var de = document.documentElement;
                var box = $(this).get(0).getBoundingClientRect();
                var top = box.top + window.pageYOffset - de.clientTop;
                var left = box.left + window.pageXOffset - de.clientLeft;
                var width = $(this).width();
                var height = $(this).height();
                return { top: top, left: left, width: width, height: height };
            };
        }(jQuery));
        
        $(document).ready(function() {
        
            var elementOffset = $("#document-panel-pulsate").weOffset();
            
            jQuery("<div/>", {
                class: "pulsate-block",
            }).css({
                "position": "absolute",
                "top": elementOffset.top + 10, 
                "left": elementOffset.left, 
                "width": elementOffset.width, 
                "height": elementOffset.height - 10
            }).appendTo("body").pulsate({
                color: "#bf1c56",
                reach: 20,
                repeat: 2
            });

            setTimeout(\'jQuery(".pulsate-block").remove()\', 2500);
        });
        
        $(window).resize(function() {
            if (jQuery(".pulsate-block").length) {
                var elementOffset = $("#document-panel-pulsate").weOffset();

                jQuery(".pulsate-block").css({
                    "position": "absolute",
                    "top": elementOffset.top + 10, 
                    "left": elementOffset.left, 
                    "width": elementOffset.width, 
                    "height": elementOffset.height - 10
                }).appendTo("body");
            }
        });
    ');
}
?>