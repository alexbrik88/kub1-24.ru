<?php

use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\WaybillStatus;
use common\models\document\status\InvoiceFactureStatus;
use frontend\models\Documents;
use yii\helpers\Url;
use common\models\document\status\ProxyStatus;

/**
 * @var $model \common\models\document\Invoice
 * @var $id integer
 * @var $document string
 * @var $title string
 * @var $action string
 */

$url = $action == 'create' ?
    Url::to([$document . '/' . $action, 'type' => $model->type, 'invoiceId' => $model->id]) :
    Url::to([$document . '/' . $action, 'type' => $model->type, 'id' => $id]);

$iconClass = 'fa fa-plus-circle';

$isCreateProxy = ($document == 'proxy' && $action == 'create');

if ($document == 'waybill') {
    if ($model->getWaybill()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            WaybillStatus::getStatusIcon($model->getWaybill()->min('status_out_id'));
    }
} elseif ($document == 'packing-list') {
    if ($model->getPackingList()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            PackingListStatus::getStatusIcon($model->getPackingList()->min('status_out_id'));
    }
} elseif ($document == 'proxy') {
    if ($model->getProxies()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            ProxyStatus::getStatusIcon($model->getProxy()->min('status_out_id'));
    }
} elseif ($document == 'act') {
    if ($model->getAct()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            ActStatus::getStatusIcon($model->getAct()->min('status_out_id'));
    }
} elseif ($document == 'invoice-facture') {
    if ($model->getInvoiceFactures()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            InvoiceFactureStatus::getStatusIcon($model->getInvoiceFactures()->min('status_out_id'));
    }
} elseif ($document == 'upd') {
    if ($model->getUpds()->exists()) {
        $iconClass = 'icon-doc';
    }
} elseif ($document == 'order-document') {
    $iconClass = 'icon-doc';
} elseif ($document == 'agent-report') {
    $iconClass = 'icon-doc';
    $url = Url::to([$document . '/' . $action, 'type' => $model->agentReport->type, 'id' => $id]);
}
?>
<div class="clearfix">
    <a href="<?= $url ?>"
       class="btn yellow full_w <?= $model->isRejected ? ' disabled' : ''; ?> <?= $isCreateProxy ? 'add-proxy' : '' ?>"
        <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
       style="height: 34px;margin-left: 0; text-transform: uppercase;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
       title="<?= mb_convert_case(($action == 'create' ? 'Добавить ' : '') . $title, MB_CASE_TITLE) ?>">
        <i class="pull-left icon <?= $iconClass; ?>"></i>
        <?= $title; ?>
    </a>
</div>

<?php if ($isCreateProxy): ?>
    <?= $this->render('@frontend/modules/documents/views/proxy/_viewPartials/_invoices_modal', ['fromInvoice' => $model->id]) ?>
<?php endif; ?>