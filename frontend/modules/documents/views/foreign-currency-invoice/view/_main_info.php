<?php

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\models\Bank;
use common\components\ImageHelper;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use common\models\bank\BankingParams;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string */
/* @var $newCompanyTemplate bool */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */
/* @var $company \common\models\Company */
/* @var $user \common\models\employee\Employee */

$cashFlowData = [];
if ($model->cashBankFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashBankFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashBankFlows, 'id');
    $data = Html::beginForm(['/cash/bank/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Банку', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashOrderFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashOrderFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashOrderFlows, 'id');
    $cashBox = ArrayHelper::getColumn($model->getCashOrderFlows()->groupBy('cashbox_id')->all(), 'cashbox_id');
    $cashBox = $cashBox ? current($cashBox) : null;
    $data = Html::beginForm(['/cash/order/index', 'cashbox' => $cashBox], 'post',
        ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Кассе', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashEmoneyFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'id');
    $data = Html::beginForm(['/cash/e-money/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по E-money', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
$user = Yii::$app->user->identity;
$company = $user->company;
$alias = null;
$link = null;
$image = null;
if ($alias = Banking::aliasByBik($model->company_bik)) {
    $bankingClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
    $bankName = $bankingClass::NAME;
    $banking = new $bankingClass($company, [
        'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
    ]);
    if ($banking->getHasAutoload() && !$banking->isValidToken()) {
        $link = Html::a("Автоматическая загрузка выписки из банка с авто проставлением оплат у счетов", Url::to([
            "/cash/banking/{$alias}/default/index",
        ]), [
            'style' => 'font-size: 13px;',
        ]);

        if ($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) {
            $image = ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                'class' => 'little_logo_bank',
                'style' => 'display: inline-block;',
            ]);
        }
    }
}
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
$canCreateOut = Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
    'ioType' => Documents::IO_TYPE_OUT,
]);
$hasAutoinvoice = $model->contractor->getInvoicesAuto()->exists();
?>

    <style>
        .main_inf_no-bord td {
            border: none !important;
        }
    </style>

    <!-- _main_info -->
    <!--col-md-5 block-left-->
    <div class="col-xs-12 <?= $newCompanyTemplate ? 'col-lg-9 pull-right' : '' ?> pad3" style="max-width: 720px;">
        <div class="portlet">
            <div class="customer-info" <?= $newCompanyTemplate ? 'style="min-height:auto !important"' : '' ?>>
                <?php if (!$newCompanyTemplate) : ?>
                    <div class="portlet-title">
                        <!--col-md-10 caption-->

                        <div class="col-md-9 pad0 caption">
                            <?= $message->get(Message::TITLE_SINGLE); ?>
                            № <?php echo $model->fullNumber; ?>
                            от <?= $dateFormatted; ?>
                        </div>

                        <div class="actions">
                            <?php if ($model->updateAllowed() && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE,
                                    [
                                        'model' => $model,
                                    ])
                            ): ?>
                                <a href="<?= Url::to([
                                    'update',
                                    'type' => $ioType,
                                    'id' => $model->id,
                                    'contractorId' => ($useContractor ? $model->contractor_id : null),
                                ]); ?>"
                                   title="Редактировать" class="btn darkblue btn-sm pull-right"
                                   style="height: 27px;margin-left: 3px">
                                    <i class="icon-pencil"></i>
                                </a>
                            <?php endif; ?>

                            <span class="pull-right">
                        <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                            'model' => $model,
                        ]); ?>
                        </span>
                        </div>

                    </div>
                <?php endif; ?>

                <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                    <table class="table no_mrg_bottom">
                        <?php if ($model->type == Documents::IO_TYPE_OUT && !$newCompanyTemplate): ?>
                            <tr>
                                <td>
                                    <?= \yii\helpers\Html::a('Просмотр счета, который получит клиент',
                                        ['/bill/invoice/' . $model->uid], [
                                            'target' => '_blank',
                                            'class' => isset($_COOKIE["tooltip_account-view_{$model->company_id}"]) ? 'tooltipstered-main  tooltip-right' : null,
                                            'data-tooltip-content' => isset($_COOKIE["tooltip_account-view_{$model->company_id}"]) ? '#tooltip_account-view' : null,
                                        ]); ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                            <span
                                    class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>
                                :</span>
                                <span><?= Html::a($model->contractor_name_short, [
                                        '/contractor/view',
                                        'type' => $model->contractor->type,
                                        'id' => $model->contractor->id,
                                    ]) ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <span
                                    class="customer-characteristic">Оплатить до:</span>
                                <span><?= DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE,
                                        DateHelper::FORMAT_DATE); ?></span>
                            </td>
                        </tr>
                        <?php if (!$newCompanyTemplate) : ?>
                            <tr>
                                <td>
                            <span class="customer-characteristic"
                                  style="display:inline-block;">Тип счёта: </span>
                                    <span><?= $model->getProductionType(); ?></span>
                                </td>
                            </tr>
                        <?php endif ?>

                        <?php if ($ioType == Documents::IO_TYPE_IN) : ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Статья расходов:</span>
                                    <span><?= $model->invoiceExpenditureItem !== null ? $model->invoiceExpenditureItem->name : 'не указано'; ?></span>
                                </td>
                            </tr>
                        <?php else : ?>
                            <tr>
                                <td>
                                    <?php if (isset($model->agreement_new_id) && $model->agreement_new_id > 0) : ?>
                                        <span class="customer-characteristic">Договор №</span>
                                        <?= Html::a(
                                            Html::encode($model->basis_document_number) .
                                            ' от ' .
                                            DateHelper::format($model->basis_document_date,
                                                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                            '/documents/agreement/view?id=' . $model->agreement_new_id); ?>
                                    <?php elseif ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                                        <span class="customer-characteristic">
                                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                                            :
                                    </span>
                                        № <?= Html::encode($model->basis_document_number) ?>
                                        от <?= DateHelper::format($model->basis_document_date,
                                            DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?php if ($cashFlowData) : ?>
                            <tr>
                                <td>
                                    <span class="customer-characteristic">Оплата:</span>
                                    <span><?= implode(', ', $cashFlowData) ?></span>
                                </td>
                            </tr>
                        <?php endif ?>

                        <?php if ($model->currency_name != Currency::DEFAULT_NAME) : ?>
                            <tr>
                                <td id="invoice_currency_rate_box">
                                    <?= $this->render('_currency_info', ['model' => $model]) ?>
                                </td>
                            </tr>
                        <?php endif ?>
                        <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT): ?>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <span class="customer-characteristic contractor-store-label"
                                          style="line-height: 28px;vertical-align: top;">
                                        <?= $invoiceContractorSignature->is_active ? 'Убрать подпись покупателя' : 'Подпись покупателя'; ?>
                                    </span>
                                    <label class="rounded-switch" for="activate_contractor_signature"
                                           style="margin: 4px 0 0 10px;;vertical-align: top;">
                                        <?= Html::checkbox('contractor_signature',
                                            $invoiceContractorSignature->is_active, [
                                                'id' => 'activate_contractor_signature',
                                                'class' => 'switch',
                                                'data-url' => Url::to([
                                                    'change-contractor-signature',
                                                    'type' => $ioType,
                                                    'id' => $model->id
                                                ]),
                                            ]); ?>
                                        <span class="sliderr no-gray yes-yellow round"></span>
                                    </label>
                                    <span class="tooltip2-left ico-question valign-middle"
                                          data-tooltip-content="#tooltip_contractor_signature"
                                          style="vertical-align: top;">
                                    </span>
                                    <div class="tooltip-template" style="display: none;">
                                        <span id="tooltip_contractor_signature" style="display: inline-block;">
                                            <?php if (!$invoiceContractorSignature->is_active): ?>
                                                Если вам нужно, что бы Счет-договор был подписан и со стороны <br>
                                                Покупателя, то нажмите на кнопку и внизу появятся реквизиты <br>
                                                вашего Покупателя
                                            <?php else: ?>
                                                Если вам не нужна подпись Покупателя в счет-договоре,<br>
                                                то нажмите кнопку и останется только место для вашей подписи
                                            <?php endif; ?>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td>
                                <div style="margin-bottom: 5px;">
                                    <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                        'model' => $model,
                                        'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                        'uploadUrl' => Url::to([
                                            'file-upload',
                                            'type' => $model->type,
                                            'id' => $model->id,
                                        ]),
                                        'deleteUrl' => Url::to([
                                            'file-delete',
                                            'type' => $model->type,
                                            'id' => $model->id,
                                        ]),
                                        'listUrl' => Url::to([
                                            'file-list',
                                            'type' => $model->type,
                                            'id' => $model->id,
                                        ]),
                                        'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                        'scanListUrl' => Url::to([
                                            'scan-list',
                                            'type' => $model->type,
                                            'id' => $model->id
                                        ]),
                                        'scanBindUrl' => Url::to([
                                            'scan-bind',
                                            'type' => $model->type,
                                            'id' => $model->id
                                        ]),
                                    ]); ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="">
                <div style="margin: 15px 0 0;">
                    <span style="font-weight: bold;">Комментарий</span>
                    <?= Html::tag('span', '', [
                        'id' => 'comment_internal_update',
                        'class' => 'glyphicon glyphicon-pencil',
                        'style' => 'cursor: pointer;',
                    ]); ?>
                </div>
                <div id="comment_internal_view" class="">
                    <?= Html::encode($model->comment_internal) ?>
                </div>
                <?php if ($canUpdate) : ?>
                    <?= Html::beginTag('div', [
                        'id' => 'comment_internal_form',
                        'class' => 'hidden',
                        'style' => 'position: relative;',
                        'data-url' => Url::to(['comment-internal', 'type' => $model->type, 'id' => $model->id]),
                    ]) ?>
                    <?= Html::tag('i', '', [
                        'id' => 'comment_internal_save',
                        'class' => 'fa fa-floppy-o',
                        'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                    ]); ?>
                    <?= Html::textarea('comment_internal', $model->comment_internal, [
                        'id' => 'comment_internal_input',
                        'rows' => 3,
                        'maxlength' => true,
                        'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                    ]); ?>
                    <?= Html::endTag('div') ?>
                <?php endif ?>
            </div>
            <?php if ($alias && $link): ?>
                <div class="main-bank_logo" style="margin-top: 15px;">
                    <?= $image; ?>
                    <span class="bank-name" style="margin-left: 5px;">
                        <?= $bankName; ?>
                    </span>
                    <div>
                        <?= $link; ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($model->type == Documents::IO_TYPE_OUT && !$hasAutoinvoice) : ?>
                <div style="margin-top: 15px;">
                    <?php if ($canCreateOut && $company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) : ?>
                        <?= Html::a($this->render('_alarm_icon') . ' АвтоСчет', [
                            'create',
                            'type' => Documents::IO_TYPE_OUT,
                            'clone' => $model->id,
                            'auto' => 1,
                            'returnUrl' => Url::to([
                                '/contractor/view',
                                'type' => $model->contractor->type,
                                'id' => $model->contractor->id,
                                'tab' => 'autoinvoice',
                            ]),
                        ], [
                            'class' => 'btn darkblue darkblue-invert',
                        ]) ?>
                    <?php else : ?>
                        <button class="btn yellow action-is-limited">
                            <i class="fa fa-plus"></i> АвтоСчет
                        </button>
                    <?php endif ?>
                    <span class="tooltip2 ico-question valign-middle"
                          data-tooltip-content="#tooltip-autoinvoice-create">
                </div>
            <?php endif; ?>

            <?php /*if (
                $model->contractor_id
                && $model->contractor->face_type != Contractor::TYPE_PHYSICAL_PERSON
                && $model->contractor->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON
            ) {
                if ($model->contractor->verified) { ?>
                    <a class="btn darkblue-invert contractor-invoice-dossier verified" target="_blank" href="<?= Url::to([
                        '/dossier',
                        'id' => $model->contractor_id
                    ]) ?>"
                       style="margin-top:15px;">Досье покупателя</a>
                <?php } else {
                    echo $this->render('../../../../../views/dossier/_popup-confirm', [
                        'link' => Url::to([
                            '/dossier',
                            'type' => $model->type,
                            'id' => $model->contractor_id
                        ]),
                        'button' => [
                            'label' => 'Проверить покупателя',
                            'class' => 'btn darkblue-invert contractor-invoice-dossier',
                            'style' => 'margin-top:15px;'
                        ],
                        'newTab' => true
                    ]);
                }
            }*/ ?>
        </div>
    </div>

    <div class="tooltip_templates container-tooltip_templates">
        <div id="tooltip_account-view" class="box-tooltip-templates">
            <div class="box-my-account">
                1
            </div>
            Для просмотра вашего счета, кликните по ссылке
            <div class="btn-close-tooltip-templates fa fa-times"></div>
        </div>
        <div id="tooltip-autoinvoice-create" class="box-tooltip-templates">
            Создайте АвтоСчёт – счета будут сами создаваться и отправляться клиенту в указанный вами день
        </div>
    </div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });
        $(document).on("change", "#activate_contractor_signature", function () {
            $.post($(this).data("url"), {status: +$(this).is(":checked")}, function (data) {});
        });
    ');
}
?>