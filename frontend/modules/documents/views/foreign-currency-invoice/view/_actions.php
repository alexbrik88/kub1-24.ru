<?php

use frontend\modules\documents\widgets\DocumentLogWidget;
use frontend\rbac\permissions\document\Document;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $ioType integer */
/* @var $useContractor string */

?>

<div class="actions" style="margin-top: -8px;">
    <?= DocumentLogWidget::widget([
        'model' => $model,
    ]); ?>

    <?php if (Yii::$app->user->can(Document::UPDATE, ['model' => $model])) : ?>
        <?= Html::a('<i class="icon-pencil"></i>', $model->updateAllowed() ? [
            'update',
            'type' => $ioType,
            'id' => $model->id,
            'contractorId' => ($useContractor ? $model->contractor_id : null),
        ] : 'javascript:;', [
            'data-tooltip-content' => '#unpaid-invoice',
            'data-placement' => 'right',
            'title' => 'Редактировать',
            'class' => 'btn darkblue btn-sm '.(!$model->updateAllowed() ? 'tooltip2-right' : null),
            'style' => 'padding-bottom: 4px !important;',
        ]) ?>
    <?php endif; ?>
</div>