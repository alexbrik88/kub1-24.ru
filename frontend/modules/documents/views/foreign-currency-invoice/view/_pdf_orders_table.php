<?php
use common\components\TextHelper;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $ioType integer */

$company = $model->company;
$contractor = $model->contractor;

$ndsName = $model->ndsViewType->name;
$ndsValue = $model->hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';
$precision = $model->price_precision;
$isArticle = $model->show_article;
$currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";
?>

<div style="margin-bottom: 10px;">
    <table style=" width: 100%; margin-bottom: 8px; border: 2px solid #000000">
        <thead>
        <tr>
            <th>№</th>
            <?php if ($isArticle) : ?>
                <th>Артикул</th>
            <?php endif ?>
            <th>Товары (работы, услуги)</th>
            <th>Кол-во</th>
            <th>Ед.</th>
            <?php if ($model->has_weight): ?>
                <th> Вес, кг</th>
            <?php endif; ?>
            <?php if ($model->has_volume): ?>
                <th> Объем, м2</th>
            <?php endif; ?>
            <th>Цена</th>
            <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                <th>
                    <?= ((YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Скидка') .
                        ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %')); ?>
                </th>
                <th>
                    <?php if (YII_ENV_PROD && $model->company_id == 9888): ?>
                        Цена с агентским вознаграждением
                    <?php else: ?>
                        Цена со скидкой
                    <?php endif; ?>
                </th>
            <?php endif ?>
            <th>Сумма</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($model->orders as $order) : ?>
                <?php
                $productTitle = $order->product_title;
                if ($isAuto && $model->auto->add_month_and_year &&
                    $order->product->production_type == Product::PRODUCTION_TYPE_SERVICE
                ) {
                    switch ($model->auto->period) {
                        case Autoinvoice::MONTHLY:
                            $productTitle .= '<i class="auto_tpl"> за месяц 20##г.</i>';
                            break;

                        case Autoinvoice::QUARTERLY:
                            $productTitle .= '<i class="auto_tpl"> за # квартал 20##г.</i>';
                            break;
                    }
                }
                $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                if ($order->quantity != intval($order->quantity)) {
                    $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
                } ?>
                <tr>
                    <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                    <?php if ($isArticle) : ?>
                        <td><?= $order->article; ?></td>
                    <?php endif ?>
                    <td style=" width: 0"><?= $productTitle ?></td>
                    <td style="text-align: right; width: 10%">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                    </td>
                    <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                    <?php if ($model->has_weight): ?>
                        <td style="text-align: right; width: 7%"><?= ($order->weight) ? TextHelper::invoiceMoneyFormat($order->weight * 100, $model->weightPrecision) : ''; ?></td>
                    <?php endif; ?>
                    <?php if ($model->has_volume): ?>
                        <td style="text-align: right; width: 7%"><?= ($order->volume) ? TextHelper::invoiceMoneyFormat($order->volume * 100, $model->volumePrecision) : ''; ?></td>
                    <?php endif; ?>
                    <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                        <td style="text-align: right; width: 13%">
                            <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                        </td>
                        <td style="text-align: right; width: 13%">
                            <?php if ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE): ?>
                                <?= TextHelper::invoiceMoneyFormat($order->discount * 100, 2); ?>
                            <?php else: ?>
                                <?= strtr($order->discount, ['.' => ',']) ?>
                            <?php endif; ?>
                        </td>
                    <?php endif ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                    </td>
                    <td style="text-align: right; width: 15%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table class="it-b">
        <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                    <?= YII_ENV_PROD && $model->company_id == 9888 ? 'Агентское вознаграждение' : 'Сумма скидки'; ?><?= $currCode ?>:
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;">Итого<?= $currCode ?>:</td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none"><?= $ndsName ?>:</td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= $ndsValue ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">Всего к оплате<?= $currCode ?>:</td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></b>
            </td>
        </tr>
    </table>
</div>


