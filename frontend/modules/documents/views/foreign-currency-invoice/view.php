<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.15
 * Time: 16:42
 */

use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->registerAssetBundle(TooltipAsset::className(), $this::POS_END);
$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$newCompanyTemplate = (boolean)$model->company->new_template_order;

$backUrl = null;
if ($useContractor && Yii::$app->user->can(permissions\Contractor::VIEW)) {
    if (!isset($contractorTab)) $contractorTab = null;
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $model->contractor_id, 'tab' => $contractorTab];
} elseif (Yii::$app->user->can(permissions\document\Document::INDEX, [
    'ioType' => $ioType,
])
) {
    $backUrl = (\Yii::$app->request->referrer && strpos(\Yii::$app->request->referrer, 'documents/invoice/index')) ?
        \Yii::$app->request->referrer :
        ['index', 'type' => $model->type,];
}
$showSendPopup = Yii::$app->session->remove('show_send_popup');
?>

<?= Yii::$app->session->getFlash('invoiceFactureError'); ?>

    <div class="page-content-in" style="padding-bottom: 30px;">
        <div style="col-xs-12 pad3">
            <?php if ($backUrl !== null) {
                echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
                    'class' => 'back-to-customers',
                ]);
            } ?>
        </div>

        <?php if ($model->is_deleted): ?>
            <h1 class="text-warning">Счёт удалён</h1>
        <?php endif; ?>

        <div class="col-xs-12 pad0">
            <div class="col-xs-12 col-lg-7 pad0">
                <?php if ($newCompanyTemplate) { ?>
                    <?= $this->render('pre-view-' . Documents::$ioTypeToUrl[$ioType], [
                        'model' => $model,
                        'ioType' => $model->type,
                        'orders' => $model->orders,
                        'useContractor' => $useContractor,
                        'invoiceContractorSignature' => $invoiceContractorSignature,
                    ]); ?>
                <?php } else { ?>
                    <?= $this->render('view/_main_info', [
                        'model' => $model,
                        'ioType' => $model->type,
                        'message' => $message,
                        'dateFormatted' => $dateFormatted,
                        'useContractor' => $useContractor,
                        'newCompanyTemplate' => $newCompanyTemplate,
                        'invoiceContractorSignature' => $invoiceContractorSignature,
                    ]); ?>
                <?php } ?>
            </div>

            <div class="col-xs-12 col-lg-5 pad0 pull-right"
                 style="padding-bottom: 5px !important; max-width: 480px;">

                <div class="col-xs-12"
                     style="padding-right:0px !important;">
                    <?= $this->render('view/_status_block', [
                        'model' => $model,
                        'ioType' => $model->type,
                        'useContractor' => $useContractor,
                    ]); ?>
                    <?php if ($newCompanyTemplate) { ?>
                        <?= $this->render('view/_main_info', [
                            'model' => $model,
                            'ioType' => $model->type,
                            'message' => $message,
                            'dateFormatted' => $dateFormatted,
                            'useContractor' => $useContractor,
                            'newCompanyTemplate' => $newCompanyTemplate,
                            'invoiceContractorSignature' => $invoiceContractorSignature,
                        ]); ?>
                    <?php } ?>
                </div>

            </div>
        </div>

        <?php if (!$newCompanyTemplate): ?>
            <div class="col-xs-12 pad3">
                <?= $this->render('view/_orders_table', [
                    'model' => $model,
                    'ioType' => $model->type,
                ]); ?>
            </div>
        <?php endif ?>

        <div <?= $newCompanyTemplate ? 'id="buttons-bar-fixed"' : '' ?> >
            <?= $this->render('_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'useContractor' => $useContractor,
                'showSendPopup' => $showSendPopup,
            ]); ?>
        </div>
    </div>
<?php if ($model->invoiceStatus->sendAllowed()): ?>
    <?= $this->render('view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
        'showSendPopup' => $showSendPopup,
    ]); ?>
<?php endif; ?>
<?php
if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'type' => $model->type, 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить исходящий счёт?",
    ]);
}

echo $this->render('/invoice-facture/_form_create', [
    'invoice' => $model,
    'useContractor' => $useContractor,
]);

if ($model->invoiceStatus->paymentAllowed($model->type)
    && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
) {
    echo $this->render('_modal_add_flow', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]);
}

echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'payment-order',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['create-payment-order', 'type' => $model->type, 'id' => $model->id]),
    'confirmParams' => [],
    'message' => "Вы уверены, что хотите подготовить платежку в банк?",
]); ?>

<?php echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-bottom',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'bottom',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-left',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'side' => 'left',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'zIndex' => 10000,
    ],
]);
$urlActivateScanPopup = \yii\helpers\Url::to(['/documents/invoice/run-scan-popup']);
$canShowScan = Yii::$app->user->identity->show_scan_popup;
$this->registerJs(<<<JS
setTimeout(function() {
    $('.tooltipstered-main').tooltipster('open');
}, 1000);
$('#send').on('shown.bs.modal', function() {
    $('.tooltip-self-send').tooltipster({
        "theme":["tooltipster-noir","tooltipster-noir-customized"],
        "trigger":"custom",
        "side":"right",
        "zIndex":$.topZIndex('.modal-scrollable'),
    });
    setTimeout(function(){
        $('.tooltip-self-send').tooltipster('open');
    }, 500);
});
$('#send').on('hide.bs.modal', function() {
    if ($canShowScan) {
        $.post("{$urlActivateScanPopup}", function(){
            location.reload();
        });
    }
    $('.tooltip-self-send').tooltipster('destroy');
});
$(document).on('click','.btn-close-tooltip-templates',function() {
    var name = $(this).closest('.box-tooltip-templates').attr('id') + "_{$model->company_id}";
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $(this).closest('.tooltipster-base').hide();
});
JS
);
