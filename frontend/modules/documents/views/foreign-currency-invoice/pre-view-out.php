<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\rbac\permissions\document\Document;
use frontend\modules\documents\components\InvoiceHelper;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

//$addStamp = 1;

TooltipAsset::register($this);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'side' => 'right',
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

$company = $model->company;

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$signatureLink = EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
$printLink = EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);
$logoLink = EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

/* add logo */
$images['logo'] = ['tab' => 0, 'link' => $logoLink, 'name' => '+ Добавить логотип'];
$images['print'] = ['tab' => 1, 'link' => $printLink, 'name' => '+ Добавить печать'];
$images['chief'] = ['tab' => 2, 'link' => $signatureLink, 'name' => '+ Добавить подпись'];
?>

<!-- Modal: add logo -->
<?php if ($images) {
    $this->registerJs('
        $(document).on("click", ".companyImages-button", function(event) {
            var tabId = parseInt($(this).data("tab"));
            $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
        });

        $(document).on("hidden.bs.modal", "#modal-companyImages", function() {
            location.reload();
        });
    ');
}
?>

<?php if ($images) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'header' => '<h1>Загрузить логотип, печать и подпись</h1>',
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>
<!-- Modal: add logo. end -->

<style type="text/css">
    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }

    .pre-view-table h3 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }

    .auto_tpl {
        font-style: italic;
        font-weight: normal;
    }

    .m-size-div div {
        font-size: 12px;
    }

    .m-size-div div span {
        font-size: 9px;
    }

    .file_upload_block {
        text-align: center;
        border: 2px dashed #ffb848;
        color: #ffb848;
        padding: 12px;
    }

    .no_min_h {
        min-height: 0px !important;
    }
</style>

<div class="pre-view-box" style="min-width: 520px; max-width:735px; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table m-size-div">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-8 pad0 font-bold" style="height: inherit">
                <?= $this->render('/invoice/view/_actions', [
                    'model' => $model,
                    'ioType' => $ioType,
                    'useContractor' => $useContractor,
                ]) ?>
                <div style="padding-top: 12px !important;">
                    <p style="font-size: 17px">
                        <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                            $model->company_name_full :
                            $model->company_name_short; ?>
                    </p>
                </div>
            </div>

            <div class="col-xs-4 pad3 v_middle" style="height: inherit; text-align: right;">
                <div>
                    <?php if (isset($images['logo']['link'])) { ?>
                        <img style="max-height: 70px;max-width: 170px;" src="<?= $logoLink ?>" alt="">
                    <?php } else { ?>
                        <div style="width: auto !important;">
                            <div class="file_upload_block companyImages-button"
                                 data-toggle='modal'
                                 data-target='#modal-companyImages'
                                 data-tab='<?= $images['logo']['tab'] ?>'>
                                <?= $images['logo']['name'] ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad3">
            <?= $this->render('/invoice/template', [
                'model' => $model,
                'addStamp' => true,
                'showHeader' => false,
            ]); ?>
        </div>
    </div>
</div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>
<div class="hidden">
    <span id="unpaid-invoice">
        Что бы отредактировать счет, вам нужно снять оплату.<br>
        Для этого нажмите внизу на кнопку "Снять оплату"
    </span>
</div>
<?php $this->registerJs('
    $(document).ready(function () {
        var $contractorAddress = $(".contractor-address-legal");
        var $companyAddress = $(".company-address-legal");
        var $companyNameShort = $(".company-name-short");
        var $contractorNameShort = $(".contractor-name-short");

        if ($contractorAddress.height() > $companyAddress.height()) {
            $companyAddress.css("height", $contractorAddress.height() + "px");
        } else {
            $contractorAddress.css("height", $companyAddress.height() + "px");
        }

        if ($contractorNameShort.height() > $companyNameShort.height()) {
            $companyNameShort.css("height", $contractorNameShort.height() + "px");
        } else {
            $contractorNameShort.css("height", $companyNameShort.height() + "px");
        }
    });
');
?>
