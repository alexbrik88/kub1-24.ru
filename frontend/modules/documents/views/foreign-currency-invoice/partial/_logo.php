<?php
/* @var $company common\models\Company */
use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\models\Company;

/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-body form-horizontal">
    <style>
        .company_img img {
            height: 70px;
            width: 200px;
        }
        .company_img_dummy {
            height: 70px;
            border: solid 1px #e5e5e5;
            box-sizing: border-box;
        }
    </style>
    <?= \yii\helpers\Html::hiddenInput('Invoice[type]', 1); ?>
    <?= $form->errorSummary($company); ?>
    <div class="row small-boxes">
        <div class="col-md-4 pad-none">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Логотип компании</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <?php $imgPath = $company->getImagePath($company->logo_link);
                        if (is_file($imgPath)) : ?>
                            <div class="company_img">
                                <div class="portlet align-center" id="company_logo">
                                    <?= ImageHelper::getThumb(
                                        $imgPath,
                                        ImageHelper::THUMB_MEDIUM
                                    ); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="col-md-12">
                                <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 1090х370</div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($company, 'logoImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'data-button-text' => 'Добавить логотип',
                                'accept' => 'image/gif, image/jpeg, image/png, image/bmp, ',
                            ]); ?>

                            <?= $company->logo_link ? \yii\helpers\Html::activeCheckbox($company, 'deleteLogoImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]) : ''; ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($company->getImagePath($company->logo_link));
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 pad-none">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Печать компании</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <?php $imgPath = $company->getImagePath($company->print_link);
                        if (is_file($imgPath)) : ?>
                            <div class="company_img">
                                <div class="portlet align-center" id="company_print">
                                    <?= ImageHelper::getThumb(
                                        $imgPath,
                                        ImageHelper::THUMB_MEDIUM
                                    ); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="col-md-12">
                                <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 400х400</div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($company, 'printImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'data-button-text' => 'Добавить печать',
                                'accept' => 'image/gif, image/jpeg, image/png',
                            ]); ?>

                            <?= $company->print_link ? \yii\helpers\Html::activeCheckbox($company, 'deletePrintImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]) : ''; ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($company->getImagePath($company->print_link));
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 pad-none">
            <div class="portlet box darkblue">
                <div class="portlet-title">
                    <div class="caption">Подпись руководителя</div>
                </div>
                <div class="portlet-body">
                    <div class="form-group">
                        <?php
                        $imgPath = $company->getImagePath($company->chief_signature_link);
                        if (is_file($imgPath)) : ?>
                            <div class="company_img">
                                <div class="portlet align-center" id="company_chief">
                                    <?= ImageHelper::getThumb(
                                        $company->getImagePath(
                                            DIRECTORY_SEPARATOR . Company::IMAGE_CHIEF_SIGNATURE . '-resize.' .
                                            pathinfo($imgPath)['extension']
                                        ),
                                        ImageHelper::THUMB_MEDIUM
                                    ); ?>
                                </div>
                            </div>
                        <?php else : ?>
                            <div class="col-md-12">
                                <div class="portlet align-center company_img_dummy">Рекомендуемый размер для загрузки: 330х100</div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-9">
                            <?= \yii\helpers\Html::activeFileInput($company, 'chiefSignatureImage', [
                                'label' => false,
                                'class' => 'js_filetype',
                                'data-button-text' => 'Добавить подпись',
                                'accept' => 'image/gif, image/jpeg, image/png',
                            ]); ?>

                            <?= $company->chief_signature_link ? \yii\helpers\Html::activeCheckbox($company, 'deleteChiefSignatureImage', [
                                'label' => 'Удалить',
                                'class' => 'radio-inline p-o radio-padding',
                            ]) : ''; ?>
                        </div>
                    </div>
                    <div>
                        <?php
                        $modificationDate = ImageHelper::getModificationDate($company->getImagePath($company->chief_signature_link));
                        if ($modificationDate) {
                            echo date(DateHelper::FORMAT_USER_DATE, $modificationDate);
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php $this->render('_company_update_files');?>
</div>
