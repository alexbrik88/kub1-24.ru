<?php

use common\components\helpers\Html;
use common\models\company\CompanyType;
use \frontend\models\Documents;

/* @var $model common\models\Contractor */
/* @var $form yii\widgets\ActiveForm */


$inputListConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4 label-width',
    ],
];
$face_type_opt = $face_type_opt ? ['disabled' => ''] : [];

$showContractorType = Yii::$app->request->post('show_contractor_type');
$innPlaceholder = 'Автозаполнение по ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ? ' покупателя' : ' продавца') : '');
$innLabel = 'ИНН' . ($showContractorType ? ($documentType == Documents::IO_TYPE_OUT ?  ' покупателя' : ' продавца') : '') . ':';

?>
<?= Html::activeHiddenInput($model, 'type'); ?>
<?= Html::hiddenInput(null, $model->isNewRecord, [
    'id' => 'contractor-is_new_record',
]); ?>

<?= $form->field($model, 'ITN', array_merge($textInputConfig, [
    'options' => [
        'class' => 'legal form-group required',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}<p class='exists-contractor'></p>\n{endWrapper}",
]))->label($innLabel)->textInput([
    'placeholder' => $innPlaceholder,
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'prod' => YII_ENV_PROD ? 1 : 0,
    ],
    'disabled' => !$model->isNewRecord
]); ?>
<?= $form->field($model, 'name', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group required',
    ],
]))->label('Название контрагента:')->textInput([
    'placeholder' => 'Без ООО/ИП',
    'maxlength' => true,
    'data' => [
        'toggle' => 'popover',
        'trigger' => 'focus',
        'placement' => 'bottom',
        'content' => 'Впишите название компании без организационно-правовой формы (ИП, ООО, ЗАО, ПАО).',
    ],
]); ?>

<?= $form->field($model, 'companyTypeId', array_merge($textInputConfig, [
    'options' => [
        'id' => 'contractor_company_type',
        'class' => 'form-group legal row required',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4 control-label_no-marg',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-4 inp_one_line width-inp',
    ],
]))->label('Форма:')->dropDownList($model->getTypeArray(), [
    'disabled' => !$model->isNewRecord
]); ?>

<?= $form->field($model, 'PPC', array_merge($textInputConfig, [
    'options' => [
        'class' => 'form-group field-contractor-ppc required',
    ],
]))->label('КПП:')->textInput([
    'maxlength' => true,
]); ?>

<?= $form->field($model, 'director_email', $textInputConfig)
    ->label('Email покупателя:<br><span>(для отправки счетов):</span>')
    ->textInput([
        'maxlength' => true,
    ]); ?>

<?php
$companyType = yii\helpers\Json::encode(array_flip($model->getTypeArray()));
$ipId = yii\helpers\Json::encode(CompanyType::find()->select('id')->where(['like_ip' => true])->column());
$this->registerJs(<<<JS
    var contractorIsNewRecord = $("#contractor-is_new_record").val();
    var companyType = $companyType;
    $('[id="contractor-itn"]').suggestions({
        serviceUrl: 'https://dadata.ru/api/v2',
        token: '78497656dfc90c2b00308d616feb9df60c503f51',
        type: 'PARTY',
        count: 10,

        beforeRender: function (container) {
            $(".field-contractor-itn p.exists-contractor").empty();
            if (contractorIsNewRecord) {
                $.post("/contractor/check-availability", {
                    inn: this.value,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
        },
        onSelect: function(suggestion) {
            var companyTypeId = '-1';
            var ipId = $ipId;

            if (contractorIsNewRecord) {
                $.post("/contractor/check-availability", {
                    inn: suggestion.data.inn,
                    type: $("#contractor-type").val()
                }, function (data) {
                    if (data.result == true) {
                        $(".field-contractor-itn p.exists-contractor").html(data.contractorLink);
                    }
                });
            }
            if (!empty(suggestion.data.opf) && !empty(companyType[suggestion.data.opf.short])) {
                companyTypeId = companyType[suggestion.data.opf.short];
            }
            $('#contractor-name').val(suggestion.data.name.full);
            $('#contractor-itn').val(suggestion.data.inn);
            $('#contractor-ppc').val(suggestion.data.kpp);
            $('#contractor-bin').val(suggestion.data.ogrn);
            if (!empty(suggestion.data.management)) {
                $('#contractor-director_name').val(suggestion.data.management.name);
                $('#contractor-director_post_name').val(suggestion.data.management.post);
            }
            $('#contractor-companytypeid').val(companyTypeId);
            if (ipId.includes(companyTypeId)) {
                $('#contractor-director_name').val(suggestion.data.name.full);
                $('.field-contractor-ppc').hide();
            } else {
                $('.field-contractor-ppc').show();
            }
            var address = '';
            if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
                if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                    address += suggestion.data.address.data.postal_code + ', ';
                }
            }
            address += suggestion.data.address.value;
            $('#contractor-legal_address').val(address);
            $('#contractor-actual_address').val(address);
        }
    });
JS
);
