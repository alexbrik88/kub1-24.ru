<?php
use common\models\product\Product;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */
/* @var $form yii\widgets\ActiveForm */
/* @var $documentType int */
/* @var $document string */

$document = isset($document) ? $document : null;

$isService = $model->production_type == Product::PRODUCTION_TYPE_SERVICE;

$submitLabel = '<span class="hidden-md hidden-sm hidden-xs">Сохранить</span>';
$submitLabel .= '<i class="hidden-lg fa fa-floppy-o fa-2x"></i>';
?>

<?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'new-product-invoice-form',
    'fieldConfig' => [
        'labelOptions' => [
            'class' => 'col-sm-4 col-md-5 control-label bold-text width-next-input',
        ],
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation' => true,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
])); ?>

    <div class="form-body form-horizontal">

        <?= Html::hiddenInput('documentType', $documentType); ?>

        <?= $this->render('_mainForm', [
            'model' => $model,
            'form' => $form,
            'documentType' => $documentType,
            'document' => $document,
        ]); ?>

        <div class="form-actions">
            <div class="row action-buttons">
                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:23.45%">
                    <?= Html::submitButton('<span class="ladda-label">'.$submitLabel.'</span>', [
                        'id' => 'product-form-submit',
                        'class' => $model->isNewRecord ? 'btn darkblue widthe-100' : 'btn btn-primary widthe-100',
                        'data-style' => 'expand-right',
                    ]); ?>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width:23.45%">
                    <span class="btn darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs"  style="width:100%!important" data-dismiss="modal">
                        Отменить
                    </span>
                    <span class="btn darkblue gray-darkblue widthe-100 hidden-lg" title="Отменить" data-dismiss="modal">
                        <i class="fa fa-reply fa-2x"></i>
                    </span>
                </div>
                <div class="button-bottom-page-lg col-sm-1 col-xs-1">
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<script type="text/javascript">
$("#new-product-invoice-form input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
$("#new-product-invoice-form input:radio:not(.md-radiobtn)").uniform();
</script>