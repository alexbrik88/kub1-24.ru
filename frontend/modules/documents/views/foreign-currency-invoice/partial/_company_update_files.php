<?php
$this->registerJS(<<<JS
$('#company-logoimage').change(function () {
        var preview = document.querySelector('div[id=company_logo] img');
        var file    = document.querySelector('input[id=company-logoimage]').files[0];
        previewFile(preview, file);
});
$('#company-printimage').change(function () {
        var preview = document.querySelector('div[id=company_print] img');
        var file    = document.querySelector('input[id=company-printimage]').files[0];
        previewFile(preview, file);
});
$('#company-chiefsignatureimage').change(function () {
        var preview = document.querySelector('div[id=company_chief] img');
        var file    = document.querySelector('input[id=company-chiefsignatureimage]').files[0];
        previewFile(preview, file);
});
function previewFile(preview, file) {
    var reader  = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            preview.src = "";
        }
}
$('.js_filetype , .js_select').each(function () {
        var buttonText = $(this).data('button-text') !== undefined ? $(this).data('button-text') : 'Добавить документ';
        $(this).styler({
            'fileBrowse' : buttonText
        });
    });
$(':checkbox:not(.md-check)').uniform({checkboxClass: "checker"});
JS
);