<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
<?php if (!Yii::$app->request->post('isB2B')): ?>
    <h1>Добавить товар/услугу</h1>
<?php else: ?>
    <?php if ($productionType == 1): ?>
        <h1>Добавить товар</h1>
    <?php else: ?>
        <h1>Добавить услугу</h1>
    <?php endif; ?>
<?php endif; ?>