<?php

use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\components\date\DateHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Contractor */
/* @var $cancelUrl string */

/* @var $form ActiveForm */
$textInputConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line width-inp',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];
$checkboxConfig = [
    'labelOptions' => [
        'class' => 'control-label col-md-4',
        'style' => 'padding-top: 0px;'
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control m-l-n sel-w',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

$partialDirectorTextInputConfig = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'control-label col-md-4',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8 field-width inp_one_line',
    ],
    'inputOptions' => [
        'class' => 'form-control',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{endWrapper}",
];

if ($model->isNewRecord) {
    $contractorTypeList = [
        Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо',
        Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
} else {
    if ($model->face_type == Contractor::TYPE_LEGAL_PERSON)
        $contractorTypeList = [Contractor::TYPE_LEGAL_PERSON => 'Юридическое лицо'];
    else
        $contractorTypeList = [Contractor::TYPE_PHYSICAL_PERSON => 'Физическое лицо'];
}

?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => '',
        'data-ip-ids' => CompanyType::likeIpId(),
    ],
    'id' => 'new-contractor-invoice-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'validateOnSubmit' => true,
    'validateOnBlur' => false,
]); ?>
<?= Html::hiddenInput('documentType', $documentType); ?>

<?php if (!$model->isNewRecord) {
    $invoiceId = Yii::$app->request->post('invoiceId');
    echo Html::hiddenInput('contractorId', $model->id);
    echo Html::hiddenInput('invoiceId', $invoiceId);
} ?>

<?php echo $form->errorSummary($model);
$model->face_type = isset($model->face_type) ? $model->face_type : 0;
echo $form->field($model, 'face_type', [
    'options' => [
        'class' => 'form-group row',
    ],
])
    ->label($model->type == Contractor::TYPE_SELLER ? 'Тип поставщика: ' : 'Тип покупателя: ', ['class' => 'col-md-4 col-sm-4'])
    ->radioList($contractorTypeList,
        [
            'class' => 'radio-list type lastItemMrgRight0 col-sm-7 col-md-7',
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('label',
                    Html::radio($name, $checked, ['value' => $value]) . $label,
                    [
                        'class' => 'radio-inline m-l-20',
                    ]);
            },
        ]); ?>
<div
    class="form-group forContractor row <?= !$model->face_type ? '' : 'selectedDopColumns' ?>">
    <div class="form-body form-horizontal col-md-12">
        <?= $this->render('_partial_main_info', [
            'model' => $model,
            'form' => $form,
            'class' => 'required',
            'textInputConfigDirector' => $partialDirectorTextInputConfig,
            'face_type_opt' => $face_type_opt,
            'textInputConfig' => $textInputConfig,
            'documentType' => $documentType,
        ]) ?>


    </div>
    <div class="clearfix"></div>
    <?= $this->render('_partial_details', [
        'model' => $model,
        'form' => $form,
    ]) ?>
</div>
<div
    class="form-group forContractor row <?= $model->face_type ? '' : 'selectedDopColumns' ?>">
    <div class="form-body form-horizontal col-md-12">
        <?= $form->field($model, 'physical_lastname', array_merge($textInputConfig, [
            'options' => [
                'class' => 'form-group row required',
            ],
        ]))->label('Фамилия:')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_firstname', array_merge($textInputConfig, [
            'options' => [
                'class' => 'form-group row required',
            ],
        ]))->label('Имя:')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_patronymic', array_merge($textInputConfig, [
            'options' => [
                'class' => 'form-group row required',
            ],
        ]))->label('Отчество:')->textInput([
            'maxlength' => true,
            'readonly' => (boolean)$model->physical_no_patronymic,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]); ?>
        <?= $form->field($model, 'physical_no_patronymic', array_merge($checkboxConfig, [
            'options' => [
                'class' => 'form-group row',
            ],
        ]))->label('Нет отчества:')->checkbox([], false); ?>
        <?= $form->field($model, 'physical_address', array_merge($textInputConfig, [
            'options' => [
                'class' => 'form-group row required',
            ],
        ]))->label('Адрес регистрации:')->textInput([
            'maxlength' => true,
            'data' => [
                'toggle' => 'popover',
                'trigger' => 'focus',
                'placement' => 'bottom',
            ],
        ]);
        ?>
        <?= $form->field($model, 'contact_email', $textInputConfig)
            ->label('Email покупателя:<br><span>(для отправки счетов):</span>')
            ->textInput([
                'maxlength' => true,
            ]); ?>
    </div>
        <span class="addColumns long-message-input">
            Указать дополнительные данные необходимые для акта, товарной накладной, счет-фактуры
        </span>

    <div class="dopColumns selectedDopColumns">
        <div class="form-body form-horizontal col-md-12">
            <?= $form->field($model, 'physical_passport_series', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group row',
                ],
            ]))->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{2} 9{2}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XX XX',
                ],
            ]); ?>
            <?= $form->field($model, 'physical_passport_number', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group row',
                ],
            ]))->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{6}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XXXXXX',
                ],
            ]); ?>
            <div class="form-group row">

                <label for="under-date" class="col-md-4 control-label">Дата
                    выдачи:</label>

                <div class="col-md-8 inp_one_line width-inp">
                    <div class="input-icon">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'physical_passport_date_output', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->physical_passport_date_output, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </div>

            </div>
            <?= $form->field($model, 'physical_passport_issued_by', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group row',
                ],
            ]))->label('Кем выдан:')->textInput([
                'maxlength' => false,
                'data' => [
                    'toggle' => 'popover',
                    'trigger' => 'focus',
                    'placement' => 'bottom',
                ],
            ]); ?>
            <?= $form->field($model, 'physical_passport_department', array_merge($textInputConfig, [
                'options' => [
                    'class' => 'form-group row',
                ],
            ]))->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '9{3}-9{3}',
                'options' => [
                    'class' => 'form-control',
                    'placeholder' => 'XXX-XXX',
                ],
            ]); ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="form-actions">
    <div class="row action-buttons">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 23.45%;">
            <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs ladda-button' : 'btn btn-primary widthe-100 hidden-md hidden-sm hidden-xs ladda-button',
                'data-style' => 'expand-right',
                'style' => 'width: 100%!important;',
            ]) ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => $model->isNewRecord ? 'btn darkblue widthe-100 hidden-lg' : 'btn btn-primary widthe-100 hidden-lg'
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 23.45%;">
            <?php
            if (!isset($cancelUrl)) {
                $cancelUrl = $model->isNewRecord
                    ? Url::to(['/contractor/index', 'type' => $model->type])
                    : Url::to(['/contractor/view', 'type' => $model->type, 'id' => $model->id,]);
            }
            ?>
            <a href="#" style="width:100% !important;"
               class="btn darkblue gray-darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</a>
            <a href="#" class="btn darkblue gray-darkblue widthe-100 hidden-lg"
               title="Отменить"><i class="fa fa-reply fa-2x"></i></a>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?php if (!$model->isNewRecord
                && (\Yii::$app->authManager
                    && Yii::$app->user->can(\frontend\rbac\permissions\Contractor::DELETE))
            ) {
                if ($model->getDeleteItemCondition()) {
                    echo ConfirmModalWidget::widget([
                        'toggleButton' => [
                            'label' => 'Удалить',
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        ],
                        'confirmUrl' => Url::to(['delete', 'type' => $model->type, 'id' => $model->id,]),
                        'message' => 'Вы уверены, что хотите удалить контрагента?',
                    ]);
                } else {
                    echo \frontend\widgets\ModalWidget::widget([
                        'toggleButton' => [
                            'label' => '<i class="fa fa-trash-o fa-2x"></i>',
                            'title' => 'Удалить',
                            'class' => 'btn darkblue widthe-100 hidden-lg',
                        ],
                        'message' => 'Вы не можете удалить этого контрагента, потому что у него имеются активные счета!',
                        'options' => [
                            'textCssClass' => 'red-alert',
                        ],
                    ]);
                }
            } ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<script type="text/javascript">
    function contractorIsIp() {
        var ipIds = $('#new-contractor-invoice-form').data('ip-ids');

        return ipIds.includes($('#contractor-companytypeid').val());
    }

    function contractorIsNotIp() {
        return $('#contractor-companytypeid').val() && !contractorIsIp();
    }

    function contractorIsPhysical() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_PHYSICAL_PERSON ?>";
    }
    function contractorIsLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_LEGAL_PERSON ?>";
    }
    function contractorIsForeignLegal() {
        return $("#contractor-face_type input:checked").val() == "<?= Contractor::TYPE_FOREIGN_LEGAL_PERSON ?>";
    }
    function contractorIsAgent() {
        return $("#contractor_is_agent_input:checked").length;
    }

    $('#contractor-companytypeid').change(function () {
        if (contractorIsIp()) {
            $('.field-contractor-ppc').hide();
        } else {
            $('.field-contractor-ppc').show();
        }
        $('#new-contractor-invoice-form').yiiActiveForm('validateAttribute', 'contractor-itn');
    });
    $('#contractor-name').on('input', function () {
        if (contractorIsIp()) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
    $('#contractor-companytypeid').on('change', function () {
        if (contractorIsIp()) {
            $('#contractor-director_name').val($('#contractor-name').val());
        }
    });
</script>
