<?php

use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\rbac\permissions;
use common\models\product\Product;
use yii\helpers\Url;
use frontend\components\XlsHelper;
use common\components\ImageHelper;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $company Company */
/* @var $fixedContractor boolean */
/* @var $ioType integer */
/* @var $form ActiveForm */
/* @var $invoiceContractEssence \common\models\document\InvoiceContractEssence */
/* @var $invoiceEssence \common\models\document\InvoiceEssence */
/* @var $create string|null */
/* @var $document string */

$this->title = 'Создать ' . $model->printablePrefix;
$this->context->layoutWrapperCssClass = 'create-out-invoice';

if (!isset($returnUrl)) {
    $returnUrl = '';
}

$errorModelArray = [$model];
if (empty($autoinvoice)) {
    $autoinvoice = null;
} else {
    $errorModelArray[] = $autoinvoice;
}
$errorModelArray = array_merge($errorModelArray, $model->orders);

$isFirstCreate = \Yii::$app->controller->action->id === 'first-create';
$isAuto = false;
if (!isset($create)) {
    $create = null;
}
if (!isset($document)) {
    $document = null;
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'click',
        'contentAsHTML' => true,
    ],
]);

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);

$fileButton = '';
if ($model->type == Documents::IO_TYPE_OUT) {
    if (!$company->getImage('logoImage')) {
        $fileButton = [0, 'ЛОГОТИП'];
    } elseif ($company->company_type_id != CompanyType::TYPE_IP && !$company->getImage('printImage')) {
        $fileButton = [1, 'ПЕЧАТЬ'];
    } elseif (!$company->getImage('chiefSignatureImage')) {
        $fileButton = [2, 'ПОДПИСЬ'];
    }
}

if (0) {
    $fileButton = Html::a(
        Html::tag('span', '', [
            'class' => 'glyphicon glyphicon-plus',
            'style' => 'font-size: 10px; vertical-align: 1px;',
            'aria-hidden' => 'true',
        ]) . ' ' . Html::tag('span', $fileButton[1]),
        '#',
        [
            'id' => 'companyImages-button',
            'class' => 'btn yellow companyImages-button',
            'data-toggle' => 'modal',
            'data-target' => '#modal-companyImages',
            'data-tab' => $fileButton[0],
        ]
    );
}

if ($document == null) {
    $logoTooltipLine = '/img/tips/arrow/2.png';
    $logoPositionTooltip = 'top: -20px;left: 325px;';
} else {
    $logoTooltipLine = '/img/tips/arrow/3.png';
    $logoPositionTooltip = 'top: 19px;left: 345px;';
}

$this->registerJs('
    $(document).on("click", ".companyImages-button", function() {
        var tabId = parseInt($(this).data("tab"));
        $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
    });
    $(document).on("hidden.bs.modal", ".modal-companyImages", function() {
        $.pjax.reload("#companyImages-button-pjax", {timeOut: 5000});
    });
    ');
?>

<?php
$form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
    'id' => 'invoice-form',
    'options' => [
        'enctype' => 'multipart/form-data',
        'class' => 'add-avtoschet',
        'data' => [
            'iotype' => $ioType,
        ]
    ],
    'enableClientValidation' => false,
]));
?>

<?= Html::hiddenInput('returnUrl', $returnUrl) ?>

    <!-- helpers tooltip -->
<?php echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-first-r',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-light'],
        'contentAsHTML' => true,
        'position' => 'right',
    ],
]);

?>
    <style type="text/css">
        .delete-column-right {
            display: none;
        }

        .btn-add-line-table.delete-column-right {
            display: none;
        }
    </style>
    <div>
        <style>
            .black-screen {
                position: absolute;
                width: 100%;
                background-color: #000000ab;
                z-index: 9999;
                height: 100%;
                top: 0;
                display: none;
            }

            .tooltip-block-show {
                z-index: 10000;
                background-color: white;
            }

            .not-clickable {
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0;
                z-index: 99999;
                display: none;
            }

            .tooltip-block-show .not-clickable {
                display: block;
            }

            .tooltipster-light {
                z-index: 10050 !important;
            }

            .tooltipster-light .tooltipster-box {
                background: transparent;
                border: none;
                margin-left: 0px !important;
            }

            .tooltipster-light .tooltipster-content {
                /* color: #4276a4 !important;
                 font-weight: bold;
                 padding: 0px !important;
                 font-family: "Segoe Print";*/
            }

            .tooltipster-light .tooltipster-arrow-background, .tooltipster-light .tooltipster-arrow-border {
                display: none !important;
            }

            .hidden1024 {
                display: none;
            }

            @media (min-width: 1024px) {
                .hidden1024 {
                    display: block;
                }
            }

        </style>

        <?php if ($isFirstCreate): ?>
            <script>

                window.addEventListener('load', function () {
                    var tooltipFirst = (function () {
                        var wight = 1;
                        var t = $('.tooltip-first-r');
                        var currentComp = <?= $company->id ?: 0 ?>;

                        var elements = {
                            requisites: 'tooltip_requisites',
                            addProduct: 'tooltip_help_add_product',
                            logo: 'tooltip_help_logo'
                        };

                        var blocks = {
                            requisites: 'tooltip_requisites_block',
                            addProduct: 'tooltip_add_product_block',
                            logo: 'tooltip_help_logo_block'
                        };

                        var reposition = function () {
                            t.tooltipster('reposition');
                        };

                        var init = function () {
                            t.tooltipster('open');
                            $('.black-screen').addClass('show');

                            hide.noActive();

                            $(document).on('click', '#' + elements.requisites + " a", function () {
                                hide.element(elements.requisites);
                                hide.noActive();
                            });

                            $(document).on('click', '#' + elements.addProduct + " a", function () {
                                hide.element(elements.addProduct);
                                hide.noActive();
                            });

                            $(document).on('click', '#' + elements.logo + " a", function () {
                                hide.element(elements.logo);
                                hide.noActive();
                            });

                            // Esc key
                            $(document).on('keyup', function (evt) {
                                if (evt.keyCode == 27) {
                                    hide.element(elements.requisites);
                                    hide.element(elements.addProduct);
                                    hide.element(elements.logo);
                                    hide.noActive();
                                }
                            });

                            // click black screen
                            $(document).on('click', '.black-screen', function () {
                                hide.element(elements.requisites);
                                hide.element(elements.addProduct);
                                hide.element(elements.logo);
                                hide.noActive();
                            });

                            // click white blocks
                            $(document).on('click', '.not-clickable', function () {
                                hide.element(elements.requisites);
                                hide.element(elements.addProduct);
                                hide.element(elements.logo);
                                hide.noActive();
                            });

                        };

                        var hide = {
                            element: function (idTooltip, hideAll) {
                                if (hideAll == null) {
                                    Company.add(idTooltip);
                                }
                                $('#' + idTooltip).remove();
                            },

                            noActive: function (hideAll) {
                                if (hideAll == null) {
                                    hideAll = false;
                                }

                                var countHide = 0;
                                $.each(elements, function (def, idElements) {
                                    if (hideAll || (idElements && Company.has(idElements, currentComp))) {
                                        hide.element(idElements, hideAll);
                                        countHide++;
                                    }
                                });

                                $.each(blocks, function (def, idElements) {
                                    if (hideAll || (idElements && Company.has(elements[def], currentComp))) {
                                        $('#' + idElements).removeClass('tooltip-block-show');
                                    } else {
                                        $('#' + idElements).addClass('tooltip-block-show');
                                        //$('#'+idElements+' .not-clickable').addClass('show');
                                    }
                                });

                                if (hideAll || countHide == 3) {
                                    $('.black-screen').removeClass('show');
                                }

                            }
                        };

                        var Company = {
                            has: function (idTooltip, idComp) {
                                var companies = Company.getCompanies(idTooltip);
                                var isHas = false;
                                $.each(companies, function (def, idCompany) {
                                    if (idComp == idCompany) {
                                        isHas = true;
                                        return true;
                                    }
                                });
                                return isHas;
                            },

                            add: function (idTooltip) {
                                if (!Company.has(idTooltip, currentComp)) {
                                    var companies = Company.getCompanies(idTooltip);
                                    companies.push(currentComp);
                                    companies = companies.join(',');
                                    document.cookie = idTooltip + "=" + companies;
                                }
                            },

                            getCompanies: function (idTooltip) {
                                if (!idTooltip) {
                                    return [];
                                }
                                var companies = Company.getCookie(idTooltip);
                                if (companies) {
                                    return companies.split(',')
                                }
                                return [];
                            },

                            getCookie: function (name) {
                                var matches = document.cookie.match(new RegExp(
                                    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                                ));
                                return matches ? decodeURIComponent(matches[1]) : false;
                            }

                        };

                        return {
                            init: init,
                            reposition: reposition,
                            hide: hide,
                            wight: wight
                        }
                    })();

                    tooltipFirst.init();

                    if ($(window).width() < 1024) {
                        tooltipFirst.hide.noActive(true);
                    }

                    $(window).resize(function () {
                        if ($(window).width() < 1024) {
                            if (tooltipFirst.wight == 1) {
                                tooltipFirst.hide.noActive(true);
                            }
                            tooltipFirst.wight = 0;
                        } else {
                            tooltipFirst.wight = 1;
                        }
                    });

                });


            </script>
        <?php endif ?>

        <!-- helpers tooltip.end -->

        <div class="tooltip_help_templates" style="display:none">
            <div id="tooltip_requisites">
                <div class="hidden1024 pad0" style="width: 578px; position: absolute; color: white">
                    <div style="display: block; width: 35px">
                        <img style="width: 55px;height: 37px;" src="/img/tips/arrow/1.png">
                    </div>
                    <div style="display: block;padding-left: 29px;width: 341px;">
                        <div style="float: left;width: 45px">
                            <img style="height:25px;" src="/img/tips/arrow/circle.png">
                            <div style="margin-top:-22px; text-align: center;width: inherit;"> 1</div>
                        </div>
                        <div style="float: left;width: 221px;">
                            <h4 style="margin: 0px; padding-top:3px"><strong> Автозаполнение </strong></h4>
                            <p style="padding-top: 10px;padding-bottom: 10px;"> Заполните реквизиты клиента и профиль
                                Вашей компании, что бы данные автоматически подтягивались в акты и накладные</p>
                            <a href="javascript:void(0);"
                               style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                                Понятно </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tooltip_help_add_product">
                <div class="hidden1024 pad0" style="width: 578px;position: absolute;color: white;margin-top: -11px;">
                    <div style="display: block;width: 92px;float: left;margin-top: -10px !important;">
                        <img style="width: 88px;height: 39px;" src="/img/tips/arrow/2.png">
                    </div>
                    <div style="width: 304px;float: left;">
                        <div style="float: left;width: 45px">
                            <img style="height:25px;" src="/img/tips/arrow/circle.png">
                            <div style="margin-top:-22px; text-align: center;width: inherit;"> 2</div>
                        </div>
                        <div style="float: left;width: 221px;">
                            <h4 style="margin: 0px; padding-top:3px"><strong> Товары и услуги </strong></h4>
                            <p style="padding-top: 10px;padding-bottom: 10px;"> Добавьте Ваш товар или услугу. Далее вы
                                сможе выставлять <br> счета выбрав данное <br> наименование из списка </p>
                            <a href="javascript:void(0);"
                               style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                                Понятно </a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tooltip_help_logo">
                <div class="hidden1024 pad0" style="width: 471px;position: absolute;color: white;">
                    <div style="display: block;width: 92px;float: left;">
                        <img style="width: 85px;height: 35px;" src="<?= $logoTooltipLine; ?>">
                    </div>
                    <div style="width: 304px;float: left;padding-top: 13px;">
                        <div style="float: left;width: 45px">
                            <img style="height:25px;" src="/img/tips/arrow/circle.png">
                            <div style="margin-top:-22px; text-align: center;width: inherit;"> 3</div>
                        </div>
                        <div style="float: left;width: 250px;">
                            <h4 style="margin: 0px; padding-top:3px">
                                <strong> Профессиональный вид
                                    <?php if ($document == Documents::SLUG_ACT): ?>
                                        акта
                                    <?php elseif ($document == Documents::SLUG_PACKING_LIST): ?>
                                        ТН
                                    <?php elseif ($document == Documents::SLUG_INVOICE_FACTURE): ?>
                                        СФ
                                    <?php elseif ($document == Documents::SLUG_UPD): ?>
                                        УПД
                                    <?php else: ?>
                                        счета
                                    <?php endif; ?>
                                </strong>
                            </h4>
                            <p style="padding-top: 10px;padding-bottom: 10px;"> Загрузите логотип, печать и подпись
                                <br> и они будут подгружаться <br> автоматически в счета </p>
                            <a href="javascript:void(0);" class="companyImages-button"
                               style="padding: 7px 25px;border: 1px solid white;border-radius: 3px !important;color:white;text-transform: uppercase;">
                                Понятно </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- remove product -->

    <div id="modal-remove-one-product" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -45px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                        <div class="row">Вы уверены, что хотите удалить эту позицию из счета?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                    style="width: 80px;color: white;">ДА
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue"
                                    style="width: 80px;color: white;">НЕТ
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="form-body form-body_sml">
        <?= $form->errorSummary($errorModelArray); ?>
        <?= Html::hiddenInput('document', $document, [
            'id' => 'create-document',
        ]); ?>
        <div class="portlet">
            <?= $this->render('form/_form_header', [
                'model' => $model,
                //'message' => $message,
                'form' => $form,
                'isAuto' => $isAuto,
                'autoinvoice' => $autoinvoice,
                'create' => $create,
                'fileButton' => false,
                'document' => $document,
                'ioType' => $ioType,
                'newDoc' => isset($newDoc) ? $newDoc : null
            ]); ?>

            <div class="portlet-body">
                <?= $this->render('form/_form_body', [
                    'model' => $model,
                    //'message' => $message,
                    'fixedContractor' => $fixedContractor,
                    'ioType' => $ioType,
                    'company' => $company,
                    'form' => $form,
                    'isAuto' => $isAuto,
                    'autoinvoice' => $autoinvoice,
                    'invoiceEssence' => $invoiceEssence,
                    'document' => $document,
                ]); ?>

                <div class="col-xs-12 pad0" style="margin-top: 5px !important">
                    <?= $this->render('form/_product_table_invoice', [
                        'model' => $model,
                        'ioType' => $ioType,
                        'company' => $company,
                        'invoiceContractEssence' => $invoiceContractEssence,
                        'document' => $document,
                    ]); ?>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <?php $this->registerJs('
                            function textareaResize(input) {
                                $(input).height(1);
                                $(input).height(input.scrollHeight);
                            }
                            $(".textarea-collapse-block textarea").each(function(i, item) {
                                textareaResize(item);
                            });
                            $(document).on("click", ".textarea-collapse-label", function() {
                                $(this).parent().find(".textarea-collapse-block").fadeToggle();
                                textareaResize(document.getElementById(this.htmlFor));
                            });
                            $(document).on("keyup", ".textarea-collapse-block textarea", function() {
                                textareaResize(this);
                            });
                        '); ?>

                        <?php if ($fileButton || $company->strict_mode == Company::ON_STRICT_MODE) : ?>
                            <?php $docName = $create ?
                                Documents::$slugToName[$create] :
                                ($document ? Documents::$slugToName[$document] : 'Счет'); ?>
                            <div class="col-xs-12 col-sm-8 pad0 cont-checkbox-logo">
                                <div class="checkbox-add-logo">
                                    <div id="tooltip_help_logo_block" style="padding: 1px; position: absolute">
                                        <div class="file_upload_block"
                                             style="width: 341px; padding: 11px; border: 2px dashed #ffb848; text-align: center; font-weight: 100;"
                                             data-toggle='modal'
                                             data-target='#modal-companyImages'
                                             data-tab='0'>
                                            <span>Добавить в <?= $docName ?> логотип, подпись, печать</span>

                                            <a style="position: absolute;<?= $logoPositionTooltip; ?>"
                                               class="tooltip-first-r"
                                               data-tooltip-content="#tooltip_help_logo">&nbsp;</a>
                                        </div>
                                        <div class="not-clickable">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>

                                <div id="log" style="display: none">
                                    чтобы в счете были логотип, подпись и печать -
                                    <span data-toggle="modal" data-target="#modal-companyImages1" class="addLogo">загрузите их</span>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('form/_form_buttons', [
            'model' => $model,
            'isAuto' => $isAuto,
            'ioType' => $ioType,
            'fixedContractor' => $fixedContractor,
            'autoinvoice' => empty($autoinvoice) ? null : $autoinvoice,
        ]); ?>
    </div>
<?php ActiveForm::end(); ?>

<?= $this->render('form/_form_product_new'); ?>

<?= $this->render('form/_form_product_existed', [
    'model' => $model,
]); ?>

<?php if ($fileButton || $company->strict_mode == Company::ON_STRICT_MODE) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'header' => '<h1>Загрузить логотип, печать и подпись</h1>',
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>

    <div class="tooltip_templates" style="display: none;">
    <span id="tooltip_contractor"
          style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите по вашему покупателю ИНН,<br>
        остальные реквизиты заполнятся автоматически.<br>
        Данные по покупателю сохранятся, в дальнейшем вы<br>
        будите выбирать его из списка.
    </span>
        <span id="tooltip_company"
              style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите ИНН, БИК и расч. счет<br>
        по вашей компании или ИП,<br>
        остальные реквизиты заполнятся автоматически.
    </span>
        <span id="tooltip_add_product"
              style="display: inline-block; text-align: center;">
        В наименовании нажмите «Добавить новый товар/услугу»,<br>
        после заполните информацию по вашему товару или услуге.<br>
        Данные сохранятся, в дальнейшем вы<br>
        будите выбирать товар или услугу  из списка.
    </span>
        <span id="tooltip_upload"
              style="display: inline-block; text-align: center;">
        Что бы счет выглядел профессионально и красиво<br>
        добавьте ваш логотип, а так же подпись и печать
    </span>
        <span id="tooltip_service_no_delete"
              style="display: inline-block; text-align: center;">
        Нельзя удалить услугу,<br>
        т.к. она используется в акте<br>
        к данному счету
    </span>
        <span id="tooltip_goods_no_delete"
              style="display: inline-block; text-align: center;">
        Нельзя удалить товар, который уже<br>
        используется в товарной накладной
    </span>
        <span id="tooltip_service_no_edit"
              style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одном Акте
    </span>
        <span id="tooltip_goods_no_edit"
              style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одной ТН
    </span>
        <span id="tooltip_remind"
              style="display: inline-block; text-align: center;">
        Если счет не будет оплачен, то<br>
        <span id="when_remind"></span>
        будет автоматически<br>
        отправлено письмо вашему покупателю<br>
        с напоминанием об оплате счета
    </span>
    <span id="tooltip_refresh_company" style="display: inline-block; text-align: center;">
        Реквизиты вашей компании были изменены.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
        <span id="tooltip_refresh_contractor" style="display: inline-block; text-align: center;">
        Информация по покупателю была изменена.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
        <span id="product_services_title" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий месяц,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за февраль 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Месяц и год»</span>.<br>
        Если вы выставляете счета за следующий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за март 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий месяц и год»</span>.<br>
        Если вы выставляете счета за предыдущий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за январь 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий месяц и год»</span>.
    </span>
        <span id="product_services_title2" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий кватрал,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Кватрал и год»</span>.<br>
        Если вы выставляете счета за следующий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 2-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий кватрал и год»</span>.<br>
        Если вы выставляете счета за предыдущий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 01.04.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий кватрал и год»</span>.
    </span>
        <span id="tooltip_paydate_limit"
              style="display: inline-block;">
        Срок оплаты подставляется автоматически в
        <br>
        комментарий к счету из поля "Оплатить до"
    </span>
        <span id="tooltip_comment_client"
              style="display: inline-block;">
        Добавьте к счету комментарий для покупателя.
        <br>
        Покупатель получит дополнительную информацию
        <br>
        о деталях оплаты, условиях договора и тд.
    </span>
        <span id="tooltip_currency_select"
              style="display: inline-block; text-align: center;">
        Выставляйте счет в любой валюте.
        <br>
        Выберите валюту из списка.
    </span>
        <span id="invoice-contract-tooltip">
            <?= ImageHelper::getThumb('img/invoice-contract.jpg', [480, 157]); ?>
        </span>
        <span id="tooltip_hidden-discount" class="text-center" style="display: inline-block;">
            Используйте, когда нужно посчитать скидку от текущей цены,<br>
            но клиент не должен знать, что есть скидка.<br>
            В счете для клиента столбцов со скидкой не будет,<br>
            но цена будет указана со скидкой.
        </span>
    </div>
    <div class="video_modals" style="display: none;">
        <?php
        $videoModalArray = [
            'video_modal_contractor' => 'https://player.vimeo.com/video/199833243?title=0&byline=0&portrait=0',
            'video_modal_company' => 'https://player.vimeo.com/video/199827240?title=0&byline=0&portrait=0',
            'video_modal_add_product' => 'https://player.vimeo.com/video/199833591?title=0&byline=0&portrait=0',
            'video_modal_upload' => 'https://player.vimeo.com/video/199833809?title=0&byline=0&portrait=0',
        ];
        foreach ($videoModalArray as $id => $src) {
            echo Modal::widget([
                'id' => $id,
                'toggleButton' => false,
                'size' => 'modal-lg',
                'header' => null,
                'closeButton' => false,
                'content' => '<div class="video-container"><iframe width="480" height="270" src="" frameborder="0" allowfullscreen></iframe></div>',
                'content' => Html::tag('div', Html::tag('iframe', '', [
                    'width' => 480,
                    'height' => 370,
                    'allowfullscreen' => true,
                    'src' => $src,
                ]), ['class' => 'video-container']),
            ]);
        }
        ?>
    </div>
<?php if (Yii::$app->user->can(permissions\Product::CREATE)): ?>
    <?= $this->render('//xls/_import_xls', [
        'header' => 'Загрузка товаров из Excel',
        'text' => '<p>Для загрузки списка товаров из Excel,
                                    <br>
                                    заполните шаблон таблицы и загрузите ее тут.
                                    </p>',
        'formData' => Html::hiddenInput('className', 'Product') .
            Html::hiddenInput('addToInvoice', 1) .
            Html::hiddenInput('Product[production_type]', Product::PRODUCTION_TYPE_GOODS),
        'uploadXlsTemplateUrl' => Url::to(['/xls/download-template', 'type' => XlsHelper::PRODUCT_GOODS]),
    ]); ?>
<?php endif; ?>
<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

// content

Pjax::end();

Modal::end();
?>

<?= $this->render('_modal_zero_submit', ['form' => 'invoice-form']); ?>

<?php
$this->registerJs('
$(document).on("change", "#invoice-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({
            url: "/documents/agreement/create?contractor_id=" + $("#invoice-contractor_id").val() + "&type=" + $("#documentType").val() + "&returnTo=invoice" + "&container=agreement-select-container",
            container: "#agreement-form-container",
            push: false,
            timeout: 5000,
        });

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            }
        });
        $("#agreement-modal-container").modal("show");
        $("#invoice-agreement").val("").trigger("change");
    }
});
$(document).on("change", "select#invoice-contractor_id, #invoiceauto-contractor_id", function() {
    var form = this.form;
    var pjax = $("#agreement-pjax-container", form);

    if (this.value != "add-modal-contractor") {
        if (pjax.length) {
            $.pjax({
                url: pjax.data("url"),
                data: {contractorId: $(this).val()},
                container: "#agreement-pjax-container",
                push: false,
                timeout: 10000,
            });
        }

        $.post("get-contractor-info", {contractor_id: this.value}, function (data) {
            if (data.discount > 0) {

                $("#all-discount").val(data.discount);
                $(".discount-input").val(data.discount);

                $("#invoice-has_discount").prop("checked", true).uniform();
                $("#invoice-has_markup").prop("checked", false);
                $("#invoice-has_markup:not(.md-check)").uniform("refresh");
                $(".markup_column").addClass("hidden");
                $("#invoice_all_markup input").val(0);
                $(".markup-input").val(0);
                $(".discount_column").removeClass("hidden");
            } else {

                $("#all-discount").val(0);
                $(".discount-input").val(0);

                $("#invoice-has_discount").prop("checked", false).uniform();
                $(".discount_column").addClass("hidden");
            }

            $("table .discount-input").val(data.discount).each(function() {
                INVOICE.recalculateItemPrice($(this));
            });
            INVOICE.recalculateInvoiceTable();

        });
    }

    $(".contractor-invoice-dossier").addClass("hidden");
    $(".verified,#popup-confirm a.pull-right").attr("href", "/dossier?id=" + this.value);
    if (this.value && this.value != "add-modal-contractor") {
        let option=$(this.options[this.selectedIndex]);
        let faceType=option.data("face_type");
        if(faceType!='.Contractor::TYPE_PHYSICAL_PERSON.' && faceType!='.Contractor::TYPE_FOREIGN_LEGAL_PERSON.') {
            if(option.data("verified") == 0) {
                $(".contractor-invoice-dossier.not-verified").removeClass("hidden");
            }
        }
    }

});
$(document).on("pjax:success", "#agreement-pjax-container", function() {
    if (window.AgreementValue) {
        $("#invoice-agreement").val(window.AgreementValue).trigger("change");
    }
    var delay = parseInt($("#invoice-agreement").data("delay"));
    var payTo = moment().add(delay,"days").format("DD.MM.YYYY");
    var contacts = $("#invoice-agreement").data("contacts");
    var contactItems = [
        "director",
        "chief_accountant",
        "contact",
    ]
    $("#autoinvoice-payment_delay").val(delay).trigger("change");
    $("#invoice-payment_limit_date").val(payTo).trigger("change");
    if ($("#invoice-hasnds").length) {
     //   $("#invoice-hasnds").prop("checked", $("#invoice-agreement").data("outnds") == "1" ? true : false).trigger("change");
    }
    if ($("#invoice-agreement").data("refresh")) {
        $("#title_conf_toggle").attr("style", "cursor: pointer; border-bottom: 1px dashed #333333;");
        $("#title_conf_toggle").addClass("title_conf-toggle");
    } else {
        $("#title_conf_toggle").removeAttr("style", "cursor: pointer; border-bottom: 1px dashed #333333;");
        $("#title_conf_toggle").removeClass("title_conf-toggle");
    }
    if (+$("#invoice-agreement").data("invoice_count") > 0) {

        let select=document.getElementById("invoice-contractor_id");
        let option=$(select.options[select.selectedIndex]);
        let faceType=option.data("face_type");
        if(faceType!='.Contractor::TYPE_PHYSICAL_PERSON.' && faceType!='.Contractor::TYPE_FOREIGN_LEGAL_PERSON.') {
            if(option.data("verified") == 1) {
                $(".contractor-invoice-dossier.verified").removeClass("hidden");
            }
        }

        $(".contractor-invoice-debt").removeClass("hidden");
        $(".contractor-invoice-debt .count").text($("#invoice-agreement").data("invoice_count"));
        $(".contractor-invoice-debt .amount").text($("#invoice-agreement").data("invoice_sum"));
    } else {
        $(".contractor-invoice-debt").addClass("hidden");
        $(".contractor-invoice-debt .count").text("");
        $(".contractor-invoice-debt .amount").text("");
    }
    var sendTo = $("#autoinvoiceform-send_to");
    $(".autoinvoice_send_to:not(.in)").collapse("show");
    contactItems.forEach(function(contact) {
        var name = contacts[contact + "_name"] || "";
        var email = contacts[contact + "_email"] || "";
        var input = $("input.item-send-to[type=checkbox][value="+contact+"]");
        if (contact != "director") {
            var isDirector = contacts[contact + "_is_director"];
            $(".send_to_" + contact + " .toggle-is_director.yes", sendTo).toggleClass("hidden", isDirector == 0);
            $(".send_to_" + contact + " .toggle-is_director.no", sendTo).toggleClass("hidden", isDirector == 1);
            input.data("is-director", isDirector);
        }
        $("." + contact + "_name span", sendTo).html(name).toggleClass("hidden", name == "");
        $("." + contact + "_name input", sendTo).val("").toggleClass("hidden", name != "").prop("disabled", name != "");
        $("." + contact + "_email span", sendTo).html(email).toggleClass("hidden", email == "");
        $("." + contact + "_email input", sendTo).val("").toggleClass("hidden", email != "").prop("disabled", email != "");
        input.prop("checked", input.val() == "director").trigger("change").uniform("refresh");
    });
    $("#invoice-invoice_expenditure_item_id").val($("#invoice-agreement").data("expenditure")).trigger("change");
});
$(document).on("click", ".toggle-is_director-btn", function (e) {
    e.preventDefault();
    $($(this).data("target")).toggleClass("hidden");
    $("input", $($(this).data("target")+".no")).prop("disabled", $($(this).data("target")+".no").hasClass("hidden"));
});
$(document).on("change", "input.item-send-to[data-is-director]", function (e) {
    var isDirector = $(this).data("is-director");
    var label = $(this).closest("label");
    if (this.checked) {
        $(".toggle-is_director.yes", label).addClass("hidden");
        $(".toggle-is_director.no", label).removeClass("hidden");
        $("input[type=text]", label).prop("disabled", false);
    } else {
        $(".toggle-is_director.yes", label).toggleClass("hidden", isDirector != 1);
        $(".toggle-is_director.no", label).toggleClass("hidden", isDirector == 1);
        $("input[type=text]", label).prop("disabled", true);
    }
});
$(document).on("click", ".title_conf-toggle", function () {
    $("#title_conf").toggleClass("hidden");
});
$(document).on("click", "#refresh-product-table", function(e) {
    e.preventDefault();
    $.pjax({
        url: $("#product-table-invoice").data("url"),
        data: {
            type: $("#documentType").val(),
            id: $("#select-existing-contractor select").val(),
            nds: ($(".with-nds-item").hasClass("hidden") ? 0 : 1)
        },
        container: "#product-table-invoice",
        push: false,
        timeout: 5000,
    });
    $(document).on("pjax:success", "#product-table-invoice", function() {
        $("#invoice-has_discount").trigger("change");
    })
});
');

if (Yii::$app->session->remove('show_example_popup')) {
    if ($document == Documents::SLUG_ACT) {
        $header = ' Так будет выглядеть ваш акт';
    } elseif ($document == Documents::SLUG_PACKING_LIST) {
        $header = ' Так будет выглядеть ваша ТН';
    } elseif ($document == Documents::SLUG_INVOICE_FACTURE) {
        $header = ' Так будет выглядеть ваша СФ';
    } elseif ($document == Documents::SLUG_UPD) {
        $header = ' Так будет выглядеть ваш УПД';
    } else {
        $header = ' Так будет выглядеть ваш счёт';
    }
    Modal::begin([
        'header' => Html::tag('h1', Html::tag('span', '', [
                'class' => 'glyphicon glyphicon-info-sign pull-left',
                'style' => 'font-size:inherit;line-height:inherit;',
            ]) . $header),
        'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
        'footerOptions' => ['style' => 'text-align: center;'],
        'clientOptions' => ['show' => true],
        'footer' => Html::button('Ок', [
            'class' => 'btn yellow',
            'data-dismiss' => 'modal',
            'aria-hidden' => 'true',
        ]),
    ]);
    $company = $model->company;
    switch ($document) {
        case Documents::SLUG_ACT:
            $src = '/img/examples/act.jpg';
            break;
        case Documents::SLUG_PACKING_LIST:
            $src = '/img/examples/packing_list.jpg';
            break;
        case Documents::SLUG_UPD:
            $src = '/img/examples/act.jpg';
            break;
        case Documents::SLUG_INVOICE_FACTURE:
            $src = '/img/examples/invoice_facture.jpg';
            break;
        default:
            if ($model->is_invoice_contract) {
                $src = '/img/examples/invoice_contract.jpg';
            } else {
                if ($company->company_type_id == CompanyType::TYPE_IP) {
                    $src = '/img/examples/ip_na_usn.jpg';
                } elseif ($company->companyTaxationType->usn) {
                    $src = '/img/examples/ooo_na_usn.jpg';
                } else {
                    $src = '/img/examples/ooo_na_osno_with-nds.jpg';
                }
            }
            break;
    }
    echo Html::beginTag('div', ['class' => 'text-center']);
    echo Html::img($src, ['style' => 'max-width: 100%;']);
    echo Html::endTag('div');
    Modal::end();
}
if (\Yii::$app->controller->action->id == 'first-create') {
    $this->registerJs('INVOICE.recalculateInvoiceReady();');
}
if (\Yii::$app->request->get('contractorId') && !\Yii::$app->request->get('fromAgreement')) {
    $this->registerJs('$("select#invoice-contractor_id").trigger("change")');
}
?>
<?php
$urlAdd = \yii\helpers\Url::to(['invoice/autoinvoice']);
$urlRedirect = \yii\helpers\Url::to(['invoice/index', 'type' => 2]);
$this->registerJs(<<<JS
    var checkRemindTooltip = function() {
        if (parseInt($('#invoice-isautoinvoice').val()) == 1) {
            var remindDay = parseInt($('#autoinvoice-payment_delay option:selected').val()) - 1;
            $('#tooltip_remind #when_remind').text('на ' + remindDay + ' день');
        } else {
            var remindDate = moment($('#invoice-payment_limit_date').val(), 'DD.MM.YYYY').subtract(1, 'days');
            if (remindDate.isValid()) {
                $('#tooltip_remind #when_remind').text(remindDate.format('DD.MM.YYYY'));
            }
        }
        $('#tooltip_remind_ico').tooltipster('content', $('#tooltip_remind')[0].outerHTML);
    };
    checkRemindTooltip();
    $(document).on('change','#invoice-payment_limit_date, #autoinvoice-payment_delay',function() {
        checkRemindTooltip();
    });
    $(document).on('click','.autoinvoice',function() {
        $('.invoice-block').addClass('hidden');
        $('.autoinvoice-block').removeClass('hidden');
        $('#invoice-isautoinvoice').val('1');
        checkRemindTooltip();
    });
    $(document).on('click','.autoinvoice-cancel',function() {
        $('.invoice-block').removeClass('hidden');
        $('.autoinvoice-block').addClass('hidden');
        $('#invoice-isautoinvoice').val('0');
        checkRemindTooltip();
    });
    $(document).on('click','#autoinvoice-submit',function() {
        $.ajax({
            url: '$urlAdd',
            type: 'POST',
            data: $('.add-avtoschet').serialize(),
            success : function(data) {
                if(data == 1){
                    window.location.href = "$urlRedirect";
                } else{
                    $('#autoinvoice-flash').text(data);
                }
            },
        });
    });
    $(document).on('blur', '.product-count', function(){
        if ($(this).val() == 0 || $(this).val() == ''){
            $(this).val($(this).attr('data-value'));
            $(this).closest('tr').find('.price-with-nds').text(parseFloat($(this).attr('data-value') * $(this).closest('tr').find('.price-one-with-nds-input').val()).toFixed(2));
        } else {
           $(this).attr('data-value', $(this).val());
        }
    });

    $(document).on('blur','.product-title', function(){
        var name = $(this).val().replace(/\s+/g, '').length;
        if (name == 0) {
            $(this).closest('tr').remove();
            $('#from-new-add-row').show();
            INVOICE.recalculateInvoiceTable();
        }

    });
    $(document).on('click', '.product-title-clear', function(){
        $(this).closest('td').find('.product-title').val('');
        $(this).closest('td').find('.product-title').focus();
    });

JS
    , $this::POS_READY);


// Autofill fields from ScanRecognizeDocument
if ('previewScan' == Yii::$app->request->get('mode')) {
    echo $this->render('@frontend/modules/documents/views/upload-manager/_autofill/autofill', [
        'model' => $model,
        'company' => $company,
        'ioType' => $ioType
    ]);
}
