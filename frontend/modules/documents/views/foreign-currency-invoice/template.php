<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\InvoiceContractorSignature;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\Invoice;
use common\models\document\Autoinvoice;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $addStamp boolean */
/* @var $showHeader boolean */

common\assets\DocumentTemplateAsset::register($this);
$this->params['asset'] = common\assets\DocumentTemplateAsset::class;

$company = $model->company;
$contractor = $model->contractor;

$signatureHeight = $accountantSignatureHeight = $defaultSignatureHeight = 50;

$showHeader = ArrayHelper::getValue($_params_, 'showHeader', true);
$addStamp = ArrayHelper::getValue($_params_, 'addStamp', false);
$accountantSignatureLink = $signatureLink = $printLink = $logoLink = null;
$isOut = $model->type == Documents::IO_TYPE_OUT;
if ($isOut) {
    $logoLink = EasyThumbnailImage::thumbnailSrc(
        $company->getImage('logoImage'),
        300,
        100,
        EasyThumbnailImage::THUMBNAIL_INSET_BOX
    );
    if ($addStamp) {
        if ($model->signed_by_name) {
            if ($model->employeeSignature) {
                $accountantSignatureHeight = $signatureHeight = ImageHelper::getSignatureHeight($model->employeeSignature->file ?? null, 50, 100);
                $accountantSignatureLink = $signatureLink = $model->employeeSignature ? EasyThumbnailImage::thumbnailSrc(
                    $model->employeeSignature->file,
                    165,
                    $accountantSignatureHeight,
                    EasyThumbnailImage::THUMBNAIL_INSET_BOX
                ) : null;
            }
        } else {
            $signatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefSignatureImage'), 50, 100);
            $signatureLink = EasyThumbnailImage::thumbnailSrc(
                $model->getImage('chiefSignatureImage'),
                165,
                $signatureHeight,
                EasyThumbnailImage::THUMBNAIL_INSET_BOX
            );

            if (!$company->chief_is_chief_accountant) {
                $accountantSignatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefAccountantSignatureImage'), 50, 100);
                $accountantSignatureLink = EasyThumbnailImage::thumbnailSrc(
                    $model->getImage('chiefAccountantSignatureImage'),
                    165,
                    $accountantSignatureHeight,
                    EasyThumbnailImage::THUMBNAIL_INSET_BOX
                );
            } else {
                $accountantSignatureHeight = $signatureHeight;
                $accountantSignatureLink = $signatureLink;
            }
        }
        $printLink = EasyThumbnailImage::thumbnailSrc(
            $model->getImage('printImage'),
            150,
            150,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );
    }
}
$invoiceContractorSignature = $company->invoiceContractorSignature;
if (!$invoiceContractorSignature) {
    $invoiceContractorSignature = new InvoiceContractorSignature();
    $invoiceContractorSignature->company_id = $model->company_id;
}

$ndsName = $model->getNdsViewTypeName();
$ndsValue = $model->hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-';
$precision = $model->price_precision;
$isArticle = $model->show_article;
$currCode = " {$model->currency_name}";
if ($isOut) {
    $buyerName = $model->contractor_name_short;
    $buyerAddress = $model->contractor_address_legal_full;
    $sellerName = $model->company_name_short ?: $model->company_name_full;
    $sellerNameFull = $model->company_name_full ?: $model->company_name_short;
    $sellerAddress = $model->company_address_legal_full;
    $sellerEmail = $model->document_author_email;
    $sellerPhone = $model->document_author_phone;
    $rs = $model->company_rs;
    $iban = $model->company_iban;
    $bankName = $model->company_bank_name;
    $bankAddress = $model->company_bank_address;
    $bankSwift = $model->company_bank_swift;
    $corrRs = $model->company_corr_rs;
    $corrBankName = $model->company_corr_bank_name;
    $corrBankAddress = $model->company_corr_bank_address;
    $corrBankSwift = $model->company_corr_bank_swift;
} else {
    $buyerName = $model->company_name_short;
    $buyerAddress = $model->company_address_legal_full;
    $sellerName = $model->contractor_name_short;
    $sellerNameFull = $model->contractor_name_full;
    $sellerAddress = $model->contractor_address_legal_full;
    $sellerEmail = $model->contractor_director_email;
    $sellerPhone = $model->contractor_director_phone;
    $rs = $model->contractor_rs;
    $iban = null;
    $bankName = $model->contractor_bank_name;
    $bankAddress = $model->contractor_bank_address;
    $bankSwift = $model->contractor_bank_swift;
    $corrRs = $model->contractor_corr_rs;
    $corrBankName = $model->contractor_corr_bank_name;
    $corrBankAddress = $model->contractor_corr_bank_address;
    $corrBankSwift = $model->contractor_corr_bank_swift;
}
$showCorrDetails = $corrRs || $corrBankName || $corrBankAddress || $corrBankSwift;
$docDate = date_create($model->document_date);
$dueDate = date_create($model->payment_limit_date);
?>

<div class="document-template invoice-template">
    <?PHP if ($showHeader) : ?>
        <table>
            <tbody>
                <tr>
                    <td class="p0ad doc-title" style="width: 60%;">
                        <?php if ($isOut && empty($sellerName)) : ?>
                            <div>
                                <?= Html::tag('i', 'Нет названия компании на английском', [
                                    'class' => 'link ajax-modal-btn',
                                    'data' => [
                                        'url' => Url::to(['update-company', 'type' => $model->type, 'id' => $model->id]),
                                        'title' => 'Заполнить реквизиты на английском',
                                    ],
                                ]) ?>
                            </div>
                        <?php else : ?>
                            <div style="font-size: 20px; font-weight: bold;">
                                <?= Html::encode($sellerName) ?>
                            </div>
                        <?php endif ?>
                        <div style="line-height: 24px;">
                            <?= Html::encode($sellerAddress) ?>
                        </div>
                        <?php if ($sellerPhone) : ?>
                            <div>
                                <?= Html::encode($sellerPhone) ?>
                            </div>
                        <?php endif ?>
                        <?php if ($sellerEmail) : ?>
                            <div>
                                <?= Html::encode($sellerEmail) ?>
                            </div>
                        <?php endif ?>
                    </td>
                    <td class="p0ad text-r" style="width: 40%;">
                        <?php if ($logoLink) : ?>
                            <img src="<?= $logoLink ?>" alt="" style="max-height: 100px; max-width: 100%">
                        <?php endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?PHP endif ?>

    <table style="margin-top: 5px; -webkit-print-color-adjust: exact;">
        <tr>
            <td style="width: 55%; padding: 10px 0 0;">
            </td>
            <td rowspan="3" style="width: 30%;
                        font-weight: bold;
                        font-size: 45px;
                        line-height: 1;
                        padding: 0;
                        text-align: center;">
                INVOICE
            </td>
            <td style="width: 15%; padding: 10px 0 0;">
            </td>
        </tr>
        <tr>
            <td style="width: 55%; padding: 30px 0 0; background-color: #00bbe8; -webkit-print-color-adjust: exact;">
            </td>
            <td style="width: 15%; padding: 30px 0 0; background-color: #00bbe8; -webkit-print-color-adjust: exact;">
            </td>
        </tr>
        <tr>
            <td style="width: 55%; padding: 15px 0 0;">
            </td>
            <td style="width: 15%; padding: 15px 0 0;">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 400px;">
                <table style="width: 100%; font-size: 18px;">
                    <tr>
                        <td style="font-size: 18px; font-weight: bold;">
                            Bill to:
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            <?= Html::encode($buyerName) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="">
                            <?= Html::encode($buyerAddress) ?>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 241px">
                <table style="width: 100%; font-size: 18px;">
                    <tr>
                        <td style="font-size: 18px; font-weight: bold;">
                            Invoice #
                        </td>
                        <td style="font-size: 18px; text-align: right;">
                            <?= Html::encode($model->fullNumber) ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            Invoice Date
                        </td>
                        <td style="text-align: right;">
                            <?= $docDate->format('d / m / Y') ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;">
                            Due Date
                        </td>
                        <td style="text-align: right;">
                            <?= $dueDate->format('d / m / Y') ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="margin-top: 20px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <div style="font-size: 18px; font-weight: bold;">
                        Bank details for payment:
                    </div>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; vertical-align: top;">
                    <strong>Beneficiary Bank</strong>
                </td>
                <td style="width: 241px; vertical-align: top;">
                    <?php if ($showCorrDetails) : ?>
                        <strong>Intermediary Bank</strong>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; vertical-align: top;">
                    <?= Html::encode($bankName) ?>
                </td>
                <td style="width: 241px; vertical-align: top;">
                    <?php if ($showCorrDetails) : ?>
                        <?= Html::encode($corrBankName) ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; vertical-align: top;">
                    <?= Html::encode($bankAddress) ?>
                </td>
                <td style="width: 241px; vertical-align: top;">
                    <?php if ($showCorrDetails) : ?>
                        <?= Html::encode($corrBankAddress) ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; vertical-align: top;">
                    SWIFT: <?= Html::encode($bankSwift) ?>
                </td>
                <td style="width: 241px; vertical-align: top;">
                    <?php if ($showCorrDetails) : ?>
                        SWIFT: <?= Html::encode($corrBankSwift) ?>
                    <?php endif ?>
                </td>
            </tr>
            <tr>
                <td style="width: 400px; vertical-align: top;">
                    <?php if (!empty($rs)) : ?>
                        Acc. # <?= Html::encode($rs) ?>
                    <?php elseif (!empty($iban)) : ?>
                        IBAN: <?= Html::encode($iban) ?>
                    <?php endif ?>
                </td>
                <td style="width: 241px; vertical-align: top;">
                    <?php if ($showCorrDetails) : ?>
                        Acc. # <?= Html::encode($corrRs) ?>
                    <?php endif ?>
                </td>
            </tr>
            <?php if (!empty($rs) && !empty($iban)) : ?>
                <tr>
                    <td style="width: 400px; vertical-align: top;">
                        IBAN: <?= Html::encode($iban) ?>
                    </td>
                    <td style="width: 241px;">
                    </td>
                </tr>
            <?php endif ?>
        </tbody>
    </table>

    <table class="" style="margin: 20px 0 10px; border: 2px solid #000000;">
        <thead>
            <tr>
                <th class="bor">##</th>
                <?php if ($isArticle) : ?>
                    <th class="bor">VENDOR CODE</th>
                <?php endif ?>
                <th class="bor">ITEM DESCRIPTION</th>
                <th class="bor">QTY</th>
                <th class="bor">UNIT</th>
                <?php if ($model->has_weight) : ?>
                    <th class="bor"> WEIGHT, kg</th>
                <?php endif; ?>
                <?php if ($model->has_volume) : ?>
                    <th class="bor"> SIZE, m&sup3;</th>
                <?php endif; ?>
                <th class="bor">UNIT PRICE</th>
                <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                    <th class="bor">
                        DISCOUNT
                        <?= ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'); ?>
                    </th>
                    <th class="bor">
                        PRICE WITH DISCOUNT
                    </th>
                <?php endif ?>
                <th class="bor">AMOUNT,&nbsp;<?= $model->currency_name ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($model->orders as $order) : ?>
                <?php
                $productTitle = $order->product_title;
                $unitName = $order->unit ? $order->unit->foreignName : Product::DEFAULT_VALUE;
                ?>
                <tr>
                    <td class="bor" style="text-align: center; width: 5%">
                        <?= $order->number; ?>
                    </td>
                    <?php if ($isArticle) : ?>
                        <td class="bor" style="width: 15%">
                            <?= Html::encode($order->article); ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="word-break: break-word;">
                        <?= Html::encode($productTitle) ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 10%; white-space: nowrap;">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : $order->quantity*1; ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 7%; white-space: nowrap;">
                        <?= $unitName ?>
                    </td>
                    <?php if ($model->has_weight) : ?>
                        <td class="bor" style="text-align: right; width: 7%; white-space: nowrap;">
                            <?= ($order->weight) ? TextHelper::invoiceMoneyFormat($order->weight * 100, $model->weightPrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_volume) : ?>
                        <td class="bor" style="text-align: right; width: 7%; white-space: nowrap;">
                            <?= ($order->volume) ? TextHelper::invoiceMoneyFormat($order->volume * 100, $model->volumePrecision) : ''; ?>
                        </td>
                    <?php endif; ?>
                    <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                        <td class="bor" style="text-align: right; width: 10%; white-space: nowrap;">
                            <?= TextHelper::invoiceMoneyFormat($order->price_base, $precision); ?>
                        </td>
                        <td class="bor" style="text-align: right; width: 10%; white-space: nowrap;">
                            <?= $order->getDiscountViewValue() ?>
                        </td>
                    <?php endif ?>
                    <td class="bor" style="text-align: right; width: 13%; white-space: nowrap;">
                        <?= TextHelper::invoiceMoneyFormat($order->price_one, $precision); ?>
                    </td>
                    <td class="bor" style="text-align: right; width: 13%; white-space: nowrap;">
                        <?= TextHelper::invoiceMoneyFormat($order->amount, $precision); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <table>
            <tr>
                <td style="width: 400px; vertical-align: top; padding-right: 20px;">
                    <!-- <b>
                        Thank you for your business
                    </b>
                    <br>
                    <br> -->
                    <?= nl2br(Html::encode($model->comment)) ?>
                </td>
                <td style="width: 241px; vertical-align: top; text-align: right;">
                    <table>
                        <tbody>
                            <?php if ($model->has_discount && !$model->is_hidden_discount) : ?>
                                <tr>
                                    <td class="text-r text-nowrap">
                                        DISCOUNT TOTAL:
                                    </td>
                                    <td class="text-r text-nowrap">
                                        <b><?= TextHelper::invoiceMoneyFormat($model->total_discount, 2); ?></b>
                                    </td>
                                </tr>
                            <?php endif ?>
                            <tr>
                                <td class="text-r text-nowrap">SUB TOTAL:</td>
                                <td class="text-r text-nowrap">
                                    <b><?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-r text-nowrap"><?= $ndsName ?>:</td>
                                <td class="text-r text-nowrap">
                                    <b><?= $ndsValue ?></b>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-r text-nowrap">INVOICE TOTAL <strong><?= $currCode ?></strong>:</td>
                                <td class="text-r text-nowrap">
                                    <b><?= TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2); ?></b>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <?php if ($isOut) : ?>
        <div style="height: 150px;
                    <?php if ($printLink) : ?>
                    background-image: url('<?= $printLink ?>');
                    background-repeat: no-repeat;
                    background-position: 95% top;
                    <?php endif ?>
                    -webkit-print-color-adjust: exact;">
            <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
                <tr>
                    <td class="text-c v-al-b" style="border-bottom: 1px solid #000000; width: 30%;">
                        <?= Html::encode($model->company_chief_post_name) ?>
                    </td>
                    <td style="width: 5%;">
                    </td>
                    <td class="text-c p0ad v-al-b"
                        style="border-bottom: 1px solid #000000; position: relative; width: 30%;">
                        <?php if ($signatureLink) : ?>
                            <img src="<?= $signatureLink ?>" style="max-width: 100%; max-height: 100%; <?= 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px' ?>">
                        <?php endif; ?>
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="text-c v-al-b"
                        style="border-bottom: 1px solid #000000; width: 30%;">
                        <?= Html::encode($model->getCompanyChiefFio(true)) ?>
                    </td>
                </tr>
                <tr class="small-txt va-top">
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                        Signature
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                    </td>
                </tr>
            </table>
        </div>
    <?php else : ?>
        <div style="height: 150px; -webkit-print-color-adjust: exact;">
            <table style="table-layout: fixed; margin-top: 20px; -webkit-print-color-adjust: exact;">
                <tr>
                    <td class="text-c v-al-b" style="border-bottom: 1px solid #000000; width: 30%;">
                        <?= Html::encode($model->contractor_director_post_name) ?>
                    </td>
                    <td style="width: 5%;">
                    </td>
                    <td class="text-c p0ad v-al-b"
                        style="border-bottom: 1px solid #000000; position: relative; width: 30%;">
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="text-c v-al-b"
                        style="border-bottom: 1px solid #000000; width: 30%;">
                        <?= Html::encode($model->contractor_director_name) ?>
                    </td>
                </tr>
                <tr class="small-txt va-top">
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                        Authorised Sign
                    </td>
                    <td style="width: 5%;"></td>
                    <td class="p0ad v-al-t text-c" style="width: 30%;">
                    </td>
                </tr>
            </table>
        </div>
    <?php endif ?>
</div>
