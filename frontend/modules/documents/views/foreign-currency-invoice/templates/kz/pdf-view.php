<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use frontend\models\Documents;
use common\models\company\CompanyType;
use common\models\document\InvoiceContractorSignature;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */

//$addStamp = 1;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (empty($addStamp) || !$model->employeeSignature) ? null :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (empty($addStamp) || !$model->company->chief_signature_link) ? null :
        EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$model->company->chief_is_chief_accountant) {
        $accountantSignatureLink = (empty($addStamp) || !$model->company->chief_accountant_signature_link) ? null :
            EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefAccountantSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}

$printLink = (empty($addStamp) || !$model->company->print_link) ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);

$isAuto = $model->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE;
$invoiceContractorSignature = $model->company->invoiceContractorSignature;
if (!$invoiceContractorSignature) {
    $invoiceContractorSignature = new InvoiceContractorSignature();
    $invoiceContractorSignature->company_id = $model->company_id;
}

$printLink = $logoLink = $signatureLink = ''; // temp

?>

    <style type="text/css">
        <?php
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            echo $this->renderFile($file);
        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            echo $this->renderFile($file);
        }
        ?>
        .auto_tpl {
            font-style: italic;
            font-weight: normal;
        }
    </style>

    <div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
        <table class="table no-border">
            <tr>
                <td width="3%"></td>
                <td class="text-center font-size-8">
                    Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате
                    обязательно, в противном случае не гарантируется наличие товара на складе. Товар отпусукается по факту прихода
                    денег на р/с Поставщика, самовывозом, при наличии доверенности и документов, удостоверяющих личность.
                </td>
                <td width="3%"></td>
            </tr>
        </table>
        <table class="table-no-margin">
            <tr>
                <td width="50%" class="font-bold" style="border-bottom:none">Бенефициар:</td>
                <td class="font-bold text-center" colspan="2" style="border-bottom:none">ИИК</td>
                <td class="font-bold text-center" style="border-bottom:none">Кбе</td>
            </tr>
            <tr>
                <td style="border-top:0">
                    <span class="font-bold">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= strpos($model->company_name_short, 'ИП ') === 0 ?
                            $model->company_name_full :
                            $model->company_name_short; ?>
                    <?php else: ?>
                        <?= $model->contractor_name_short; ?>
                    <?php endif; ?>
                    </span>
                    <br/>
                    БИН: ############
                </td>
                <td class="text-center font-bold" colspan="2" style="border-top:0">####################</td>
                <td class="text-center font-bold" style="border-top:0">##</td>
            </tr>
            <tr>
                <td style="border-bottom:none">Банк бенефициара:</td>
                <td style="border-bottom:none" class="text-center font-bold">БИК</td>
                <td style="border-bottom:none" class="text-center font-bold" colspan="2">Код назначения платежа</td>
            </tr>
            <tr>
                <td style="border-top:none">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->company_bank_name; ?>
                    <?php else: ?>
                        <?= $model->contractor->bank_name; ?>
                    <?php endif; ?>
                </td>
                <td class="text-center font-bold" style="border-top:none">##############</td>
                <td class="text-center font-bold" colspan="2" style="border-top:none">###</td>
            </tr>
        </table>

        <br/>
        <h3 style="text-align: left">
            <?php if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT): ?>
                Счет-договор на оплату
            <?php else: ?>
                Счет на оплату
            <?php endif; ?>
            №
            <?= $isAuto ? '<i class="auto_tpl">##</i>' : $model->fullNumber; ?>
            от
            <?= $isAuto ? '<i class="auto_tpl">## ###</i> 20<i class="auto_tpl">##</i> г.' : $dateFormatted; ?>
        </h3>

        <div style="border-bottom: 2px solid #000000; padding: 2px 4px; margin-bottom: 10px;"></div>

        <table class="no-b" style="width: 100%; margin-bottom: 5px;">
            <tr>
                <td class="txt-t ver-top">Поставщик:</td>
                <td class="txt-b">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->company_name_short; ?>,
                        ИНН <?= $model->company_inn; ?><?= !empty($model->company_kpp) ? ', КПП ' . $model->company_kpp : ''; ?>
                        , <?= $model->company_address_legal_full; ?>
                    <?php else: ?>
                        <?= $model->contractorRequisites; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td class="txt-t ver-top">Покупатель:</td>
                <td class="txt-b">
                    <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                        <?= $model->contractorRequisites; ?>
                    <?php else: ?>
                        <?= $model->company_name_short; ?>,
                        ИНН <?= $model->company_inn; ?><?= !empty($model->company_kpp) ? ', КПП ' . $model->company_kpp : ''; ?>
                        , <?= $model->company_address_legal_full; ?>
                    <?php endif; ?>
                </td>
            </tr>
            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                <tr>
                    <td class="txt-t ver-top">Договор:</td>
                    <td class="txt-b">
                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </td>
                </tr>
            <?php endif ?>
        </table>

        <?= $this->render('_pdf_orders_table', [
            'model' => $model,
            'ioType' => $ioType,
            'isAuto' => $isAuto,
        ]); ?>

        <div class="txt-9">
            Всего наименований <?= count($model->orders) ?>, на сумму
            <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
            <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
        </div>
        <div class="txt-9-b">
            Всего к оплате: <?= Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name); ?>
        </div>

        <div style="border-bottom: 2px solid #000000; padding: 2px 4px; margin-top: 10px;"></div>

        <div class="row m-t-xl" style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: right center; height: 200px; -webkit-print-color-adjust: exact;">
            <div class="col-md-12">
                <br/>
                <table class="podp-r" style="background: url('<?= $model->type == Documents::IO_TYPE_OUT ? $signatureLink : null; ?>'); background-repeat: no-repeat; background-position: 175px 5px; margin-top: -20px; width: 675px; -webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="txt-9-b"
                            style="padding: 0 0 0 0;width: 20%; vertical-align: bottom;">
                            Руководитель
                        </td>
                        <td style="width: 4%"></td>
                        <td class="pad-line"
                            style="border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 35%; padding-top: 48px!important;"></td>
                        <td style="width: 4%"></td>
                        <td class="pad-line" style="border-bottom: 1px solid #000000;text-align: center; vertical-align: bottom; margin: 0 0 0 0; width: 41%;">
                            <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            <?php else: ?>
                                <?= $model->contractor->director_name; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr class="small-txt">
                        <td style="padding: 0 0 0 0;width: 20%"></td>
                        <td style="width: 4%"></td>
                        <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                            подпись
                        </td>
                        <td style="width: 4%"></td>
                        <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                            расшифровка подписи
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>