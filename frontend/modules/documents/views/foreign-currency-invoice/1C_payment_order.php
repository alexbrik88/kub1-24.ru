<?php

use frontend\models\Documents;

$amount = number_format($model->total_amount_with_nds/100, 2, '.', '');
$sum = number_format($model->total_amount_with_nds/100, 2, '-', '');
$nds = $model->total_amount_nds ? number_format($model->total_amount_nds/100, 2, '-', '') : null;
$date = date('d.m.Y');
$time = date('H:i:s');
$documentDate = date('d.m.Y', strtotime($model->document_date));
$name = ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) ? 'счет-договору' : 'счету';
$purpose1 = "Оплата по {$name} № {$model->FullNumber} от {$documentDate}г. {$model->getOrders()->one()->product_title}";
$purpose2 = "Сумма $sum";
$purpose3 = $nds ? 'В т.ч. НДС (18%) ' . $nds : 'Без налога (НДС)';

$content = "1CClientBankExchange\r\n";
$content .= "ВерсияФормата=1.02\r\n";
$content .= "Кодировка=Windows\r\n";
$content .= "Отправитель=Бухгалтерия предприятия, редакция 3.0\r\n";
$content .= "Получатель=\r\n";
$content .= "ДатаСоздания={$date}\r\n";
$content .= "ВремяСоздания={$time}\r\n";
$content .= "ДатаНачала={$date}\r\n";
$content .= "ДатаКонца={$date}\r\n";
$content .= "РасчСчет={$model->contractor_rs}\r\n";
$content .= "Документ=Платежное поручение\r\n";
$content .= "Документ=Платежное требование\r\n";
$content .= "СекцияДокумент=Платежное поручение\r\n";
$content .= "Номер=1\r\n";
$content .= "Дата={$date}\r\n";
$content .= "Сумма={$amount}\r\n";
$content .= "ПлательщикСчет={$model->contractor_rs}\r\n";
$content .= "Плательщик=ИНН {$model->contractor_inn} {$model->contractor_name_short}\r\n";
$content .= "ПлательщикИНН={$model->contractor_inn}\r\n";
$content .= "Плательщик1={$model->contractor_name_short}\r\n";
$content .= "ПлательщикРасчСчет={$model->contractor_rs}\r\n";
$content .= "ПлательщикБанк1={$model->contractor_bank_name}\r\n";
$content .= "ПлательщикБанк2={$model->getContractorBankCity()}\r\n";
$content .= "ПлательщикБИК={$model->contractor_bik}\r\n";
$content .= "ПлательщикКорсчет={$model->contractor_ks}\r\n";
$content .= "ПолучательСчет={$model->company_rs}\r\n";
$content .= "Получатель=ИНН {$model->company_inn} {$model->company_name_short}\r\n";
$content .= "ПолучательИНН={$model->company_inn}\r\n";
$content .= "Получатель1={$model->company_name_short}\r\n";
$content .= "ПолучательРасчСчет={$model->company_rs}\r\n";
$content .= "ПолучательБанк1={$model->company_bank_name}\r\n";
$content .= "ПолучательБанк2={$model->getCompanyBankCity()}\r\n";
$content .= "ПолучательБИК={$model->company_bik}\r\n";
$content .= "ПолучательКорсчет={$model->company_ks}\r\n";
$content .= "ВидОплаты=01\r\n";
$content .= "ПлательщикКПП={$model->contractor_kpp}\r\n";
$content .= "ПолучательКПП={$model->company_kpp}\r\n";
$content .= "Очередность=5\r\n";
$content .= "НазначениеПлатежа={$purpose1}. {$purpose2}. {$purpose3}.\r\n";
$content .= "НазначениеПлатежа1={$purpose1}\r\n";
$content .= "НазначениеПлатежа2={$purpose2}\r\n";
$content .= "НазначениеПлатежа3={$purpose3}\r\n";
$content .= "Код=\r\n";
$content .= "КонецДокумента\r\n";
$content .= "КонецФайла";

echo mb_convert_encoding($content, 'windows-1251');
