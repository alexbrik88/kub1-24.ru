<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.06.2017
 * Time: 19:56
 */
?>
<div class="wrapper"
     style="margin: 0 auto;">
    <div class="first_table">
        <div class="left_wrap">
            <div class="left_1">
                Универсальный передаточный документ
            </div>
            <div class="left_2">
                Статус:
            </div>
            <ul class="left_3">
                <li>1 - счет-фактура и передаточный документ (акт)</li>
                <li>2 - передаточный документ (акт)</li>
            </ul>
        </div>
        <div class="right_wrap">
            <div class="right_1">
                <div class="right_left">
                    <div>Счет-фактура N_______от__________(1)</div>
                    <div>Исправление N _______от__________(1а)</div>
                </div>
                <div class="right_right">
                    Приложение N 1 к письму ФНС России от 21.10.2013 N
                    ММВ-20-3/96@<br>
                    Приложение N 1<br>
                    к постановлению Правительства Российской Федерации<br>
                    от 26 декабря 2011 года N 1137
                </div>
            </div>
            <div class="right2" style="margin-top:10px;">
                <div class="right2_line">
                    <div class="right2_line_first" style="font-weight: bold;">
                        Продавец
                    </div>
                    <div class="line"></div>
                    <div class="right2_line_last">(2)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">Адрес</div>
                    <div class="line"></div>
                    <div class="right2_line_last">(2а)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">ИНН/КПП продавца</div>
                    <div class="line"></div>
                    <div class="right2_line_last">(2б)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">Грузоотправитель и его
                        адрес
                    </div>
                    <div class="line"></div>
                    <div class="right2_line_last">(3)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">Грузополучатель и его адрес
                    </div>
                    <div class="line"></div>
                    <div class="right2_line_last">(4)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">К платежно-расчетному
                        документу N
                    </div>
                    <div class="line" style="width: 10%;"></div>
                    <div class="right2_line_first" style="width: 1%;">от</div>
                    <div class="line" style="width: 72%;"></div>
                    <div class="right2_line_last">(5)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first"
                         style="font-weight:bold;	">Покупатель
                    </div>
                    <div class="line"></div>
                    <div class="right2_line_last">(6)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">Адрес</div>
                    <div class="line"></div>
                    <div class="right2_line_last">(6а)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">ИНН/КПП покупателя</div>
                    <div class="line"></div>
                    <div class="right2_line_last">(6б)</div>
                </div>
                <div class="right2_line">
                    <div class="right2_line_first">Валюта: наименование, код
                    </div>
                    <div class="line"></div>
                    <div class="right2_line_last">(7)</div>
                </div>
            </div>
        </div>
    </div>
    <div class="two_table">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
               style="border: 1px solid #000;">
            <tr>
                <th width="35"
                    style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    N <br> п/п
                </th>
                <th width="35"
                    style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 36px;">
                    Код товара/ работ, услуг
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;padding: 0 5px 0 5px;">
                    Наименование товара (описание выполненных работ, оказанных
                    услуг), имущественного права
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 7%;">
                    Единица измерения
                    <table cellspacing="0" cellpadding="0"
                           style="border-top: 1px solid #000;">
                        <tr>
                            <th style="border-right: 1px solid #000;padding:0 5px 0 5px;">
                                код
                            </th>
                            <th style="padding: 0 5px 0 5px;">условное
                                обозначение (национальное)
                            </th>
                        </tr>
                    </table>
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;padding: 0 5px 0 5px;">
                    Коли-чество (объем)
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 4%;padding: 0 5px 0 5px;">
                    Цена (тариф) за единицу измерения
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 9%;">
                    Стоимость товаров (работ, услуг), имущественных прав без
                    налога - всего
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 4%;">
                    В том числе сумма акциза
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 6%;">
                    Налоговая ставка
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 7%;">
                    Сумма налога, предъявляемая покупателю
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;width: 9%;">
                    Стоимость товаров(работ, услуг), имущественных прав с
                    налогом - всего
                </th>
                <th style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    Страна происхождения товара
                    <table cellspacing="0" cellpadding="0"
                           style="border-top: 1px solid #000;">
                        <tr>
                            <th style="border-right: 1px solid #000;padding: 0 5px 0 5px;">
                                Цифро-вой код
                            </th>
                            <th style="padding: 0 5px 0 5px;">Краткое
                                наименование
                            </th>
                        </tr>
                    </table>
                </th>
                <th style="border-bottom: 1px solid #000;width:5%;padding: 0 5px 0 5px;">
                    Номер таможенной декларации
                </th>
            </tr>

            <!--// copy this tr elements for added more table-->
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; min-width: 35px;">
                    А
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 2px solid #000; min-width: 35px;">
                    Б
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width:26%;">
                                2
                            </td>
                            <td>2а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    5
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    7
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    8
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    9
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                10
                            </td>
                            <td>10а</td>
                        </tr>
                    </table>
                </td>
                <td style="border-bottom: 1px solid #000;">11</td>
            </tr>
            <tr>
                <td style=" border-right: 1px solid #000; min-width: 35px;"></td>
                <td style=" border-right: 2px solid #000; min-width: 35px;"></td>
                <td style=" border-right: 1px solid #000;font-weight: bold;text-align: left;"
                    colspan="4">Всего к оплате
                </td>
                <td style=" border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td style=" border-right: 1px solid #000;"></td>
                <td style=" border-right: 1px solid #000;"></td>
                <td style=" border-right: 1px solid #000;text-align: center;"
                    colspan="2">Х
                </td>
                <td style=" border-right: 1px solid #000;">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="border-right: 1px solid #000;width: 41%;">
                                1
                            </td>
                            <td>2</td>
                        </tr>
                    </table>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="first_table" style="margin-bottom: 10px;">
        <div class="left_wrap"
             style="border-bottom: 2px solid transparent;margin-top: 10px;">
            <div class="left_1">
                Документ составлен на
            </div>
            <div class="left_2" style="margin-top: 0;">
                _____ листах
            </div>
        </div>
        <div class="right_wrap" style="border-bottom: 2px solid #000;">
            <div class="right2_wrap" style="margin-top: 10px;">
                <div class="right2_inside">
                    <div class="right3_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;">Руководитель
                            организации или иное уполномоченное лицо
                        </div>
                        <div style="min-width: 20px;"></div>
                        <div style="border-bottom: none;width: 5%;"></div>
                        <div style="min-width: 50px;"></div>
                    </div>
                    <div class="right4_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;"></div>
                        <div style="min-width: 20px;max-width: 20px;">подпись
                        </div>
                        <div
                            style="border-bottom: none;color: transparent;width: 5%;"></div>
                        <div style="min-width: 50px;max-width: 50px;">(ф.и.о)
                        </div>
                    </div>
                </div>
                <div class="right2_inside">
                    <div class="right3_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;">
                            Главный бухгалтер или иное уполномоченное лицо
                        </div>
                        <div style="min-width: 20px;"></div>
                        <div style="border-bottom: none;width: 5%;"></div>
                        <div style="min-width: 50px;"></div>
                    </div>
                    <div class="right4_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;"></div>
                        <div style="min-width: 20px;max-width: 20px;">подпись
                        </div>
                        <div
                            style="border-bottom: none;color: transparent;width: 5%;"></div>
                        <div style="min-width: 50px;max-width: 50px;">(ф.и.о)
                        </div>
                    </div>
                </div>
            </div>
            <div class="right2_wrap">
                <div class="right2_inside">
                    <div class="right3_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;">Индивидуальный
                            предприниматель
                        </div>
                        <div style="min-width: 20px;"></div>
                        <div style="border-bottom: none;width: 5%;"></div>
                        <div style="min-width: 50px;"></div>
                    </div>
                    <div class="right4_line">
                        <div class="right2_line_first"
                             style="border-bottom: none;width: 25%;"></div>
                        <div style="min-width: 20px;max-width: 20px;">подпись
                        </div>
                        <div
                            style="border-bottom: none;color: transparent;width: 5%;"></div>
                        <div style="min-width: 50px;max-width: 50px;">(ф.и.о)
                        </div>
                    </div>
                </div>
                <div class="right2_inside">
                    <div class="right3_line">
                        <div style="border-bottom: none;width: 5%;"></div>
                        <div style="min-width: 50px;"></div>
                    </div>
                    <div class="right4_line">
                        <div style="min-width: 20px;max-width: 20px;">(реквизиты
                            свидетельства о государственной регистрации
                            индивидуального предпринимателя)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right3_inside">
        <div class="right3_line">
            <div class="right2_line_first"
                 style="border-bottom: none;width: 240px;">
                Основание передачи (сдачи) / получения (приемки)
            </div>
            <div style="min-width: 50px;"></div>
            <div
                style="width: 250px;border-bottom: none; width: 40px; text-align: center;">
                [8]
            </div>
        </div>
        <div class="right4_line">
            <div class="right2_line_first"
                 style="border-bottom: none;width: 240px;"></div>
            <div style="min-width: 50px;max-width: 50px;">(договор; доверенность
                и др.)
            </div>
        </div>
    </div>
    <div class="right3_inside">
        <div class="right3_line">
            <div class="right2_line_first"
                 style="border-bottom: none;width: 180px;">
                Данные о транспортировке и грузе
            </div>
            <div style="min-width: 50px;"></div>
            <div
                style="width: 250px;border-bottom: none; width: 40px; text-align: center;">
                [9]
            </div>
        </div>
        <div class="right4_line">
            <div class="right2_line_first"
                 style="border-bottom: none;width: 180px;"></div>
            <div style="min-width: 50px;max-width: 50px;">
                (транспортная накладная, поручение экспедитору, экспедиторская /
                складская
                расписка и др. / масса нетто/ брутто груза, если не приведены
                ссылки на
                транспортные документы, содержащие эти сведения)
            </div>
        </div>
    </div>
    <div class="right5">
        <div class="right5_inside" style="border-right: 2px solid #000;">
            <p>Товар (груз) передал / услуги, результаты работ, права сдал</p>

            <div class="" style="width: 100%;margin-bottom: 7px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="min-width: 50px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 20px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 50px;"></div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: right;">
                        [10]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">(должность)
                    </div>
                    <div style="min-width: 20px;max-width: 20px;">подпись</div>
                    <div style="border-bottom: none;color: transparent;">c</div>
                    <div style="min-width: 50px;max-width: 50px;">(ф.и.о)</div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: center;"></div>
                </div>
            </div>
            <div class="" style="width: 100%;margin-bottom: 7px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="max-width: 60px; border-bottom: none;">Дата
                        отгрузки, передачи (сдачи)
                    </div>
                    <div style="border-bottom: none;">"____"_______________
                        20____г.
                    </div>
                    <div
                        style="width: 40px;border-bottom: none;text-align: right;">
                        [11]
                    </div>
                </div>
            </div>
            <p style="width: 100%;margin-bottom: 5px;">Иные сведения об
                отгрузке, передаче</p>

            <div class="right3_inside" style="margin-bottom: 10px;">
                <div class="right3_line"
                     style="width: 100%;padding-right: 10px;">
                    <div style="min-width: 100%;"></div>
                    <div
                        style="width: 250px;border-bottom: none; width: 30px; text-align: right;">
                        [12]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">
                        (ссылки на неотъемлемые приложения, сопутствующие
                        документы, иные документы и т.п.)
                    </div>
                </div>
            </div>
            <p>Ответственный за правильность оформления факта хозяйственной
                жизни</p>

            <div class="" style="width: 100%;margin-bottom: 5px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="min-width: 50px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 20px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 50px;"></div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: right;">
                        [13]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">(должность)
                    </div>
                    <div style="min-width: 20px;max-width: 20px;">подпись</div>
                    <div style="border-bottom: none;color: transparent;">c</div>
                    <div style="min-width: 50px;max-width: 50px;">(ф.и.о)</div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: center;"></div>
                </div>
            </div>
            <p style="width: 100%;margin-bottom: 5px;">
                Наименование экономического субъекта – составителя
                документа (в т.ч. комиссионера / агента)
            </p>

            <div class="right3_inside" style="margin-bottom: 10px;">
                <div class="right3_line"
                     style="width: 100%;padding-right: 10px;">
                    <div style="min-width: 100%;"></div>
                    <div
                        style="width: 250px;border-bottom: none; width: 30px; text-align: right;">
                        [14]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">
                        (может не заполняться при проставлении печати в М.П.,
                        может быть указан ИНН / КПП)
                    </div>
                </div>
            </div>
            <p style="width: 38%;text-align: center;">
                M.П.
            </p>
        </div>
        <div class="right5_inside" style="padding-left: 10px;">
            <p>Товар (груз) передал / услуги, результаты работ, права сдал</p>

            <div class="" style="width: 100%;margin-bottom: 7px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="min-width: 50px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 20px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 50px;"></div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: right;">
                        [15]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">(должность)
                    </div>
                    <div style="min-width: 20px;max-width: 20px;">подпись</div>
                    <div style="border-bottom: none;color: transparent;">c</div>
                    <div style="min-width: 50px;max-width: 50px;">(ф.и.о)</div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: center;"></div>
                </div>
            </div>
            <div class="" style="width: 100%;margin-bottom: 7px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="max-width: 60px; border-bottom: none;">Дата
                        отгрузки, передачи (сдачи)
                    </div>
                    <div style="border-bottom: none;">"____"_______________
                        20____г.
                    </div>
                    <div
                        style="width: 40px;border-bottom: none;text-align: right;">
                        [16]
                    </div>
                </div>
            </div>
            <p style="width: 100%;margin-bottom: 5px;">Иные сведения об
                отгрузке, передаче</p>

            <div class="right3_inside" style="margin-bottom: 10px;">
                <div class="right3_line"
                     style="width: 100%;padding-right: 10px;">
                    <div style="min-width: 100%;"></div>
                    <div
                        style="width: 250px;border-bottom: none; width: 30px; text-align: right;">
                        [17]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">
                        (ссылки на неотъемлемые приложения, сопутствующие
                        документы, иные документы и т.п.)
                    </div>
                </div>
            </div>
            <p>Ответственный за правильность оформления факта хозяйственной
                жизни</p>

            <div class="" style="width: 100%;margin-bottom: 5px;">
                <div class="right3_line" style="padding-right: 10px;">
                    <div style="min-width: 50px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 20px;"></div>
                    <div style="border-bottom: none;">c</div>
                    <div style="min-width: 50px;"></div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: right;">
                        [18]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">(должность)
                    </div>
                    <div style="min-width: 20px;max-width: 20px;">подпись</div>
                    <div style="border-bottom: none;color: transparent;">c</div>
                    <div style="min-width: 50px;max-width: 50px;">(ф.и.о)</div>

                    <div
                        style="width: 30px;border-bottom: none;text-align: center;"></div>
                </div>
            </div>
            <p style="width: 100%;margin-bottom: 5px;">
                Наименование экономического субъекта – составителя
                документа (в т.ч. комиссионера / агента)
            </p>

            <div class="right3_inside" style="margin-bottom: 10px;">
                <div class="right3_line"
                     style="width: 100%;padding-right: 10px;">
                    <div style="min-width: 100%;"></div>
                    <div
                        style="width: 250px;border-bottom: none; width: 30px; text-align: right;">
                        [19]
                    </div>
                </div>
                <div class="right4_line">
                    <div style="min-width: 50px;max-width: 50px;">
                        (может не заполняться при проставлении печати в М.П.,
                        может быть указан ИНН / КПП)
                    </div>
                </div>
            </div>
            <p style="width: 38%;text-align: center;">
                M.П.
            </p>
        </div>
    </div>
</div>
