<?php

use common\components\date\DateHelper;
use common\components\grid\DropDownDataColumn;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\OrderPackingList;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\FilterHelper;
use frontend\rbac\permissions;
use PhpOffice\PhpWord\IOFactory;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/*
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \frontend\modules\documents\models\AutoinvoiceSearch
 */

if (isset($id)) {
    $emptyMessage = 'Вы еще не создали ни одного шаблона. ' . Html::a('Создать шаблон.', ['/documents/invoice/create', 'type' => 2, 'auto' => 1, 'contractorId' => $id]);
} else {
    $emptyMessage = 'Вы еще не создали ни одного шаблон. ' . Html::a('Создать шаблон.', ['/documents/invoice/create', 'type' => 2, 'auto' => 1]);
}
$useContractor = isset($useContractor) ? $useContractor : false;
?>

<?= common\components\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'emptyText' => $emptyMessage,
    'tableOptions' => [
        'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap overfl_text_hid invoice-table td-nowrap fix-thead',
        'aria-describedby' => 'datatable_ajax_info',
        'role' => 'grid',
    ],
    'headerRowOptions' => [
        'class' => 'heading',
    ],
    'options' => [
        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
    ],
    'pager' => [
        'options' => [
            'class' => 'pagination pull-right',
        ],
    ],
    'layout' => $this->render('//layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
    'columns' => [
        /*[
            'header' => Html::checkbox('', false, [
                'class' => 'joint-operation-main-checkbox',
            ]),
            'headerOptions' => [
                'class' => 'text-center',
                'width' => '5%',
            ],
            'contentOptions' => [
                'class' => 'text-center',
            ],
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return Html::checkbox('Autoinvoice[' . $model->id . '][checked]', false, [
                    'class' => 'joint-operation-checkbox',
                ]);
            },
        ],*/
        [
            'attribute' => 'created_at',
            'label' => 'Дата создания',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '8%',
            ],
            'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
        ],
        [
            'attribute' => 'document_number',
            'label' => '№ шаблона',
            'headerOptions' => [
                'class' => 'sorting',
                'width' => '8%',
            ],
            'format' => 'raw',
            'value' => function (Autoinvoice $data) use ($useContractor) {
                return Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, [
                    'model' => $data->invoice,
                ]) ? Html::a($data->document_number, [
                    '/documents/invoice/view-auto',
                    'id' => $data->id,
                    'contractorId' => $useContractor ? $data->contractor_id : null,
                ]) : $data->document_number;
            },
        ],
        [
            'class' => DropDownSearchDataColumn::className(),
            'attribute' => 'period',
            'label' => 'Периодичность',
            'headerOptions' => [
                'class' => 'dropdown-filter',
                'width' => '13%',
            ],
            'filter' => Autoinvoice::$PERIOD,
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return Autoinvoice::$PERIOD[$model->period];
            },
        ],
        [
            'class' => DropDownSearchDataColumn::className(),
            'attribute' => 'contractor_id',
            'label' => 'Покупатель',
            'headerOptions' => [
                'width' => '13%',
            ],
            'contentOptions' => [
                'class' => 'text-left text-ellipsis',
            ],
            'filter' => $searchModel->getContractorList(),
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return $model->invoice->contractor->getShortName();
            },
            'visible' => $this->context->id != 'contractor',
        ],
        [
            'attribute' => 'amount',
            'label' => 'Сумма',
            'headerOptions' => [
                'width' => '8%',
            ],
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                return TextHelper::invoiceMoneyFormat($model->amount, 2);
            },
        ],
        [
            'class' => DropDownSearchDataColumn::className(),
            'attribute' => 'product_id',
            'selectPluginOptions' => [
                'templateResult' => new JsExpression('
                    function(data, container) {
                        if (data.id != "") {
                            data.title = data.text;
                            container.setAttribute("title", data.text);
                        }
                        container.innerHTML = data.text;
                        return container;
                    }
                '),
            ],
            'options' => [
                'class' => 'text-left text-ellipsis',
            ],
            'label' => 'Наименование',
            'headerOptions' => [
                'width' => '20%',
            ],
            'filter' => $searchModel->getProductList(),
            'format' => 'raw',
            'value' => function (Autoinvoice $model) {
                $list = '<ul style="list-style: none; margin-left: -40px;">';
                foreach ($model->invoice->orders as $autoorder){
                    $list .= '<li class="bounded-string">'. $autoorder->product_title . '</li>';
                }
                $list .= '</ul>';

                return $list;
            },
        ],
        [
            'attribute' => 'last_invoice_date',
            'label' => 'Последний счёт',
            'headerOptions' => [
                'width' => '11%',
            ],
            'format' => 'raw',
            'value' => function(Autoinvoice $model){
                return $model->last_invoice_date != null ? Yii::$app->formatter->asDate($model->last_invoice_date) : '';
            },
        ],
        [
            'attribute' => 'next_invoice_date',
            'label' => 'Следующий счёт',
            'headerOptions' => [
                'width' => '11%',
            ],
            'format' => 'raw',
            'value' => function(Autoinvoice $model) {
                if (!$model->next_invoice_date || $model->status !== Autoinvoice::ACTIVE || $model->next_invoice_date > $model->date_to) {
                    return '';
                }

                return date_create_from_format('Y-m-d', $model->next_invoice_date)->format('d.m.Y');
            },
        ],
        [
            'class' => DropDownSearchDataColumn::className(),
            'attribute' => 'status',
            'label' => 'Статус',
            'value' => 'statusName',
            'headerOptions' => [
                'width' => '8%',
            ],
            'contentOptions' => [
                'class' => 'text-left text-ellipsis',
            ],
            'filter' => [
                '' => 'Все',
                1 => 'Активные',
                2 => 'Остановленные',
            ]
        ],
    ],
]);

echo $this->render('../invoice-facture/_form_create', [
    'useContractor' => $useContractor,
]);

?>
