<?php

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\NdsOsno;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $company Company */
/* @var $account CheckingAccountant */
/* @var $contractor Contractor */
/* @var $form ActiveForm */

$hasAdditionals = $model->has_discount || $model->has_markup || $model->hasNds ||
                  $model->show_paylimit_info || $model->comment ||
                  $model->currency_name != Currency::DEFAULT_NAME;
$faceTypeId = Html::getInputId($contractor, 'face_type');
?>

<div class="form-first-line">
    <div class="pull-left">
        <?= Html::hiddenInput('', (int) $model->isNewRecord, [
            'id' => 'isNewRecord',
        ]) ?>
        <label style="font-size: 18px; line-height: 18px; font-weight: bold;">Счет №</label>
        <?= Html::activeTextInput($model, 'document_number', [
            'class' => 'input-underline',
            'style' => 'width: 100px; margin-right: 8px; font-size: 18px;',
        ]); ?>
        <?php if ($model->type == Documents::IO_TYPE_OUT) : ?>
            <?= Html::activeTextInput($model, 'document_additional_number', [
                'maxlength' => true,
                'class' => 'input-underline invoice-block',
                'placeholder' => 'доп. номер',
                'style' => 'width: 100px; margin-right: 8px; font-size: 18px;',
            ]); ?>
        <?php endif ?>
    </div>
    <div class="pull-left">
        <?= Html::label('от', null, [
            'style' => 'margin-right: 8px;'
        ]) ?>
        <span style="display: inline-block; position: relative;">
            <?= Html::activeTextInput($model, 'document_date', [
                'id' => 'under-date',
                'class' => 'input-underline date-picker invoice_document_date',
                'style' => 'width: 110px; font-size: 18px;',
                'data-date' => '12-02-2012',
                'data-date-viewmode' => 'years',
                'autocomplete' => 'off',
                'value' => DateHelper::format($model->document_date,
                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]); ?>
            <?= Html::tag('i', '', [
                'class' => 'fa fa-calendar',
                'style' => 'position: absolute; top: 4px; right: 0; color: #cecece; cursor: pointer;',
                'onclick' => "
                    $('#under-date').focus().datepicker('show');
                    var _d = $('#under-date').val();
                    $('#under-date').val('');
                    $('#under-date').val(_d);
                ",
            ]) ?>
        </span>
    </div>
    <div class="clearfix"></div>
</div>

<label class="control-label-group">Ваши реквизиты для получения платежа</label>

<div class="invoice-form-company" style="max-width: 600px;">
    <?= $this->render('form_company', [
        'form' => $form,
        'company' => $company,
        'account' => $account,
    ]) ?>
</div>

<label class="control-label-group" style="display: inline-block; margin-right: 20px;">
    Реквизиты покупателя
</label>

<div class="md-radio-list" id="<?= $faceTypeId ?>" style="display: inline-block; margin: 0">
    <div class="md-radio" style="display: inline-block; margin: 0 20px 0 0;">
        <?= Html::radio(Html::getInputName($contractor, 'face_type'), $contractor->face_type == '0', $options = [
            'id' => 'face_type_0',
            'value' => 0,
            'class' => 'md-radiobtn',
        ]) ?>
        <label for="face_type_0" style="margin: 0;">
            <span class="inc"></span>
            <span class="check"></span>
            <span class="box"></span> Юридическое лицо
        </label>
    </div>
    <div class="md-radio" style="display: inline-block; margin: 0;">
        <?= Html::radio(Html::getInputName($contractor, 'face_type'), $contractor->face_type == '1', $options = [
            'id' => 'face_type_1',
            'value' => 1,
            'class' => 'md-radiobtn',
        ]) ?>
        <label for="face_type_1" style="margin: 0;">
            <span></span>
            <span class="check"></span>
            <span class="box"></span> Физическое лицо
        </label>
    </div>
</div>

<div class="invoice-form-contractor" style="max-width: 600px;">
    <?= $this->render('form_contractor', [
        'form' => $form,
        'contractor' => $contractor,
    ]) ?>
</div>

<div style="max-width: 600px; margin: 30px 0 20px;">
    <div class="row">
        <div class="col-sm-6" style="max-width: 165px; position: relative;">
            <?= $form->field($model, 'payment_limit_date')->textInput([
                'maxlength' => true,
                'class' => 'form-control input-sm date-picker' . ($model->payment_limit_date ? ' edited' : ''),
                'autocomplete' => 'off',
                'value' => DateHelper::format($model->payment_limit_date,
                    DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ])->label('Оплатить счет до') ?>
            <?= Html::tag('i', '', [
                'class' => 'fa fa-calendar',
                'style' => 'position: absolute; top: 30px; right: 15px; color: #cecece; cursor: pointer;',
                'onclick' => "
                    $('#invoice-payment_limit_date').focus().datepicker('show');
                    var _d = $('#invoice-payment_limit_date').val();
                    $('#invoice-payment_limit_date').val('');
                    $('#invoice-payment_limit_date').val(_d);
                ",
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'remind_contractor', [
                'template' => "{label}\n{input}",
                'options' => [
                    'class' => 'form-group form-md-line-input form-md-floating-label',
                    'style' => 'display: inline-block;',
                ],
                'labelOptions' => [
                    'class' => 'control-label',
                    'style' => 'padding: 6px 0; margin-right: 9px;',
                ]
            ])->checkbox([], false) ?>
            <?= Html::tag('span', '', [
                'id' => 'tooltip_remind_ico',
                'class' => 'tooltip2  ico-question valign-middle',
                'style' => 'margin-left: -8px !important;',
                'data-tooltip-content' => '#tooltip_remind',
            ]) ?>
        </div>
    </div>
    <?php if ($company->companyTaxationType->osno) : ?>

        <div class="row" style="margin-top: 15px;">
            <div class="col-xs-12 col-sm-4" style="max-width: 175px">
                <label for="invoice-payment_limit_date" class="control-label">
                    Тип счета:
                </label>
            </div>
            <div class="col-xs-12 col-sm-8" style="max-width: 320px;">
                <?php
                if ($company->nds === null) {
                    $company->nds = NdsOsno::WITH_NDS;
                }
                echo Html::activeRadioList($company, 'nds', ArrayHelper::map(NdsOsno::find()->all(), 'id', 'name'), [
                    'class' => 'inp_one_line_company checkbox-width',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return '<div class="col-xs-12 pad0">' .
                        Html::tag('label', Html::radio($name, $checked, ['value' => $value]) . $label, [
                            'class' => 'marg pad0-l radio-inline radio-padding',
                        ]) . Html::a('Пример счета', [
                            Url::to('/company/view-example-invoice'),
                            'nds' => $value == 1 ? true : false,
                        ], [
                            'class' => 'marg radio-inline radio-padding',
                            'target' => '_blank',
                        ]) .
                        "</div>";
                    },
                ]);
                ?>
            </div>
        </div>

    <?php endif; ?>
</div>

<div class="row">
    <div class="col-xs-12" style="margin-bottom: 15px">
        <?= Html::A('Дополнительные параметры', '#additionalSettings', [
            'class' => 'collapse-toggle-link text-bold',
            'data-toggle' => 'collapse',
            'aria-expanded' => 'false',
            'aria-controls' => 'additionalSettings',
        ]) ?>
    </div>
    <div class="col-xs-12 collapse<?= $hasAdditionals ? ' in' : ''; ?>" id="additionalSettings">
        <div class="row">
            <div class="col-sm-3 invoice-additional-check">
                <div class="row">
                    <div class="col-xs-9 label-block">
                        <label class="control-label" style="padding-top: 0px !important;">
                            Указать скидку
                        </label>
                    </div>
                    <div class="col-xs-3">
                        <?= $form->field($model, 'has_discount', [
                            'options' => ['class' => ''],
                        ])->checkbox(['label' => false], false) ?>
                    </div>
                </div>

                <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                    <div class="row">
                        <div class="col-xs-9 label-block">
                            <label class="control-label" style="padding-top: 0px !important;">
                                Указать наценку
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <?= $form->field($model, 'has_markup', [
                                'options' => ['class' => ''],
                            ])->checkbox(['label' => false], false) ?>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($ioType == Documents::IO_TYPE_OUT && $company->companyTaxationType->usn) : ?>
                    <div class="row">
                        <div class="col-xs-9 label-block">
                            <label class="control-label" style="padding-top: 0px !important;">
                                Счет с НДС
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <?= $form->field($model, 'hasNds', [
                                'options' => ['class' => ''],
                            ])->checkbox(['label' => false], false) ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($ioType == Documents::IO_TYPE_OUT) : ?>
                    <div class="row">
                        <div class="col-xs-9 label-block">
                            <label class="control-label" style="padding-top: 0px !important;">
                                Указать срок оплаты
                            </label>
                        </div>
                        <div class="col-xs-3">
                            <span style="display: inline-block; position: relative;">
                                <?= $form->field($model, 'show_paylimit_info', [
                                    'options' => ['class' => ''],
                                ])->checkbox(['label' => false], false) ?>
                                <?= Html::tag('span', '', [
                                    'class' => 'tooltip2 ico-question valign-middle',
                                    'style' => 'position: absolute; top: -5px; right: -17px;',
                                    'data-tooltip-content' => '#tooltip_paydate_limit',
                                ]) ?>
                            </span>
                        </div>
                    </div>
                <?php endif ?>
            </div>
            <div class="col-sm-9">
                <?= $this->render('/invoice/form/_form_currency', [
                    'form' => $form,
                    'model' => $model,
                ]) ?>
            </div>
            <div class="col-sm-12">
                <label class="text-bold">
                    Комментарий в счете для клиента
                    <?= Html::tag('span', '', [
                        'class' => 'tooltip2 ico-question valign-middle',
                        'style' => 'display: inline-block; margin: -6px 0;',
                        'data-tooltip-content' => '#tooltip_comment_client'
                    ]) ?>
                </label>
                <div style="margin-bottom: 15px;">
                    <?= Html::activeTextarea($model, 'comment', [
                        'maxlength' => true,
                        'class' => 'form-control',
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
