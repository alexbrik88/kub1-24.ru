<?php

/* @var $this yii\web\View */

?>

<div class="hidden">
    <span id="tooltip_contractor" style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите по вашему покупателю ИНН,<br>
        остальные реквизиты заполнятся автоматически.<br>
        Данные по покупателю сохранятся, в дальнейшем вы<br>
        будите выбирать его из списка.
    </span>
    <span id="tooltip_contractor_additional" style="display: inline-block; text-align: center;">
        Данные, необходимые для подготовки акта,<br>
        товарной накладной, счет-фактуры.
    </span>
    <span id="tooltip_company" style="display: inline-block; text-align: center;">
        Нажмите на кнопку и введите ИНН, БИК и расч. счет<br>
        по вашей компании или ИП,<br>
        остальные реквизиты заполнятся автоматически.
    </span>
    <span id="tooltip_add_product" style="display: inline-block; text-align: center;">
        В наименовании нажмите «Добавить новый товар/услугу»,<br>
        после заполните информацию по вашему товару или услуге.<br>
        Данные сохранятся, в дальнейшем вы<br>
        будите выбирать товар или услугу  из списка.
    </span>
    <span id="tooltip_upload" style="display: inline-block; text-align: center;">
        Что бы счет выглядел профессионально и красиво<br>
        добавьте ваш логотип, а так же подпись и печать
    </span>
    <span id="tooltip_service_no_delete" style="display: inline-block; text-align: center;">
        Нельзя удалить услугу,<br>
        т.к. она используется в акте<br>
        к данному счету
    </span>
    <span id="tooltip_goods_no_delete" style="display: inline-block; text-align: center;">
        Нельзя удалить товар, который уже<br>
        используется в товарной накладной
    </span>
    <span id="tooltip_service_no_edit" style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одном Акте
    </span>
    <span id="tooltip_goods_no_edit" style="display: inline-block; text-align: center;">
        Нельзя изменить количество наименования,<br>
        которое присутствует в более чем одной ТН
    </span>
    <span id="tooltip_remind" style="display: inline-block; text-align: center;">
        Если счет не будет оплачен, то<br>
        <span id="when_remind"></span>
        будет автоматически<br>
        отправлено письмо вашему покупателю<br>
        с напоминанием об оплате счета
    </span>
    <span id="tooltip_refresh_contractor" style="display: inline-block; text-align: center;">
        Информация по покупателю была изменена.<br>
        Нажмите, что бы применить изменения к данному счету
    </span>
    <span id="product_services_title" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий месяц,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за февраль 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Месяц и год»</span>.<br>
        Если вы выставляете счета за следующий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за март 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий месяц и год»</span>.<br>
        Если вы выставляете счета за предыдущий месяц,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 10.02.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за январь 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий месяц и год»</span>.
    </span>
    <span id="product_services_title2" style="display: inline-block; text-align: center;">
        Если вы выставляете счета за текущий кватрал,<br>
        <span style="font-weight: bold; font-style: italic;"></span>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Кватрал и год»</span>.<br>
        Если вы выставляете счета за следующий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 30.03.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 2-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Следующий кватрал и год»</span>.<br>
        Если вы выставляете счета за предыдущий кватрал,<br>
        т.е. <span style="font-weight: bold; font-style: italic;">дата счета 01.04.2017</span>,
        и в назначении <span style="font-weight: bold; font-style: italic;">услуга за 1-й кватрал 2017г.</span>,<br>
        то выбирайте <span style="font-weight: bold; font-style: italic;">«Предыдущий кватрал и год»</span>.
    </span>
    <span id="tooltip_paydate_limit" style="display: inline-block;">
        Если вы хотите указать в счете, до какой даты он действителен, то поставьте галочку.
        <br>
        Срок оплаты преставиться в комментарии из поля «Оплатить счет до».
    </span>
    <span id="tooltip_comment_client" style="display: inline-block;">
        Добавьте в счет комментарий для покупателя.
        <br>
        Текст комментария будет размещен под суммой счета прописью.
    </span>
    <span id="tooltip_currency_select" style="display: inline-block; text-align: center;">
        Выставляйте счет в любой валюте.
        <br>
        Выберите нужную валюту из списка.
    </span>
</div>