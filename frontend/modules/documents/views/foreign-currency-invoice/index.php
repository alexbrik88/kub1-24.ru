<?php

use common\components\date\DateHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\TableConfigWidget;
use frontend\rbac\UserRole;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\employee\EmployeeRole;
use common\models\employee\Employee;
use yii\widgets\ActiveForm;
use common\models\document\Invoice;

/* @var $this yii\web\View */
/* @var $searchModel common\models\document\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ioType int */
/* @var $message Message */
/* @var $prompt backend\models\Prompt */
/* @var $user Employee */

$this->title = $message->get(Message::TITLE_PLURAL);

$user = Yii::$app->user->identity;
$company = $user->company;

$countInvoice = \common\models\document\Invoice::find()->where(['and',
    ['invoice_status_author_id' => Yii::$app->user->identity->id],
    ['type' => $ioType],
])->count();

$canIndex = Yii::$app->getUser()->can(permissions\document\Document::INDEX, ['ioType' => $ioType]);
$canCreate = Yii::$app->getUser()->can(permissions\document\Document::CREATE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canDelete = Yii::$app->getUser()->can(permissions\document\Document::DELETE, [
    'ioType' => $ioType,
]) && Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canSend = $ioType == \frontend\models\Documents::IO_TYPE_OUT &&
    Yii::$app->user->can(permissions\document\Document::STRICT_MODE);
$canPrint = $ioType == \frontend\models\Documents::IO_TYPE_OUT;
$canPay = Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW);

$dropItems = [];
if ($canCreate) {
    $dropItems[] = [
        'label' => 'Создать Акты',
        'url' => '#many-create-act',
        'linkOptions' => [
            'class' => 'create-acts',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один Акт',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-act',
            'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать ТН',
        'url' => '#many-create-packing-list',
        'linkOptions' => [
            'class' => 'create-packing-list',
            'data-toggle' => 'modal',
        ],
    ];
    if ($company->companyTaxationType->osno) {
        $dropItems[] = [
            'label' => 'Создать СФ',
            'url' => '#many-create-invoice-facture',
            'linkOptions' => [
                'class' => 'create-invoice-factures',
                'data-toggle' => 'modal',
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать одну СФ',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-invoice-facture',
                'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Создать УПД',
        'url' => '#many-create-upd',
        'linkOptions' => [
            'class' => 'create-upd',
            'data-toggle' => 'modal',
        ],
    ];
    $dropItems[] = [
        'label' => 'Создать один УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'create-one-upd',
            'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
        ],
    ];
}
if ($canIndex) {
    if (!$canCreate && $user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_MANAGER) {
        $dropItems[] = [
            'label' => 'Создать один Акт',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-act',
                'data-url' => Url::to(['/documents/act/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        $dropItems[] = [
            'label' => 'Создать один УПД',
            'url' => 'javascript:;',
            'linkOptions' => [
                'class' => 'create-one-upd',
                'data-url' => Url::to(['/documents/upd/create-for-several-invoices', 'type' => $ioType]),
            ],
        ];
        if ($company->companyTaxationType->osno) {
            $dropItems[] = [
                'label' => 'Создать одну СФ',
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'create-one-invoice-facture',
                    'data-url' => Url::to(['/documents/invoice-facture/create-for-several-invoices', 'type' => $ioType]),
                ],
            ];
        }
    }
    if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF) {
        $dropItems[] = [
            'label' => 'Без Акта',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'act-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'Акт',
                    'attribute_text' => 'Без Акта',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_ACT,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без ТН',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'packing-list-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'ТН',
                    'attribute_text' => 'Без ТН',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_PACKING_LIST,
                ],
            ],
        ];
        $dropItems[] = [
            'label' => 'Без УПД',
            'url' => '#not-need-document-modal',
            'linkOptions' => [
                'class' => 'upd-not-need',
                'data' => [
                    'toggle' => 'modal',
                    'name' => 'УПД',
                    'attribute_text' => 'Без УПД',
                    'param' => Invoice::NEED_DOCUMENT_TYPE_UPD,
                ],
            ],
        ];
    }
    $dropItems[] = [
        'label' => 'Скачать в Excel',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'get-xls-link generate-xls-many_actions',
        ],
    ];
}

$sendDropItems = [];
if ($canSend) {
    $sendDropItems[] = [
        'label' => 'Счета',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ],
    ];
    $sendDropItems[] = [
        'label' => 'Счета + Акт/ТН/СФ/УПД',
        'url' => 'javascript:;',
        'linkOptions' => [
            'class' => 'document-many-send-with-docs',
            'data-url' => Url::to(['many-send', 'type' => $ioType, 'send_with_documents' => true]),
        ],
    ];
}
$invCount = $company->getInvoiceLeft($ioType);
?>

<?php /*Pjax::begin([
    'id' => 'invoice-auto-toggle-pjax',
    'timeout' => 5000,
    'linkSelector' => '.invoice-auto-toggle',
])*/ ?>

<?= Yii::$app->session->getFlash('invoiceFactureError'); ?>

    <div class="portlet box">

        <div class="btn-group pull-right title-buttons">
            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                'ioType' => $ioType,
            ])
            ): ?>
                <?php if ($company->createInvoiceAllowed($ioType)) : ?>
                    <a href="<?= Url::toRoute(['create', 'type' => $ioType]); ?>"
                       class="btn yellow">
                        <i class="fa fa-plus"></i> <?= $message->get(Message::DOCUMENT_CREATE); ?>
                    </a>
                <?php else : ?>
                    <button class="btn yellow action-is-limited"
                        <?= $invCount === 0 ? 'data-toggle="modal" data-target="#modal-invoice-payment"' : null; ?>>
                        <i class="fa fa-plus"></i> <?= $message->get(Message::DOCUMENT_CREATE); ?>
                    </button>
                <?php endif ?>
            <?php endif; ?>
        </div>

        <h3 class="page-title"><?= $message->get(Message::TITLE_PLURAL); ?></h3>

    </div>

    <div class="row">

    </div>

    <div class="row" id="widgets">
        <?= frontend\modules\documents\widgets\StatisticWidget::widget([
            'type' => $ioType,
            'outerClass' => 'col-md-3 col-sm-3',
            'contractorId' => $searchModel->contractor_id,
            'invoiceStatusId' => array_filter(is_array($searchModel->invoice_status_id) ? $searchModel->invoice_status_id : explode(',', $searchModel->invoice_status_id)),
            'searchModel' => $searchModel,
            'clickable' => true,
            'id' => 'widgets',
        ]); ?>
        <div class="col-md-3 col-sm-3">
            <div class="dashboard-stat" style="height: 122px; position: relative;">
                <?= frontend\widgets\RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
                <?php if ($ioType == Documents::IO_TYPE_OUT): ?>
                    <?php if ((YII_ENV_DEV || in_array($user->company->id, [486, 1, 2031, 7642, 1871])) && in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])): ?>
                        <?= Html::a('АвтоСбор Долгов', Url::to(['/payment-reminder/index']), [
                            'class' => 'btn yellow',
                            'style' => 'position: absolute;right: 0;bottom: 42px;width: 153px;',
                        ]); ?>
                    <?php endif; ?>
                    <?= $this->render('_invoice-auto-toggle') ?>
                <?php endif ?>
            </div>
            <div class="table-icons" style="margin-top: -20px;">
                <?= TableConfigWidget::widget([
                    'items' => [
                        [
                            'attribute' => 'invoice_scan',
                        ],
                        [
                            'attribute' => 'invoice_paylimit',
                        ],
                        [
                            'attribute' => 'invoice_paydate',
                        ],
                        [
                            'attribute' => 'invoice_act',
                        ],
                        [
                            'attribute' => 'invoice_paclist',
                        ],
                        [
                            'attribute' => 'invoice_waybill',
                        ],
                        [
                            'attribute' => 'invoice_invfacture',
                        ],
                        [
                            'attribute' => 'invoice_upd',
                        ],
                        [
                            'attribute' => 'invoice_author',
                            'visible' => !$user->currentEmployeeCompany->document_access_own_only,
                        ],
                        [
                            'attribute' => 'invoice_comment',
                        ],
                    ],
                ]); ?>
                <?= Html::a('<i class="fa fa-file-excel-o"></i>', array_merge(['get-xls'], Yii::$app->request->queryParams), [
                    'class' => 'get-xls-link pull-right',
                    'title' => 'Скачать в Excel',
                ]); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::begin([
    'method' => 'POST',
    'action' => ['generate-xls'],
    'id' => 'generate-xls-form',
]); ?>
<?php ActiveForm::end(); ?>

    <div class="portlet box darkblue" id="invoices-table">
        <div class="portlet-title">
            <div class="caption">
                Список счетов
            </div>
            <div class="tools search-tools tools_button col-sm-4">
                <div class="form-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'method' => 'GET',
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="search_cont">
                        <div class="wimax_input ">
                            <?= $form->field($searchModel, 'byNumber')->textInput([
                                'placeholder' => 'Номер счета, название или ИНН контрагента',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
            <div class="actions joint-operations col-sm-6" style="display:none;">
                <?php if ($dropItems) : ?>
                    <div class="dropdown">
                        <?= Html::a('Еще  <span class="caret"></span>', null, [
                            'class' => 'btn btn-default btn-sm dropdown-toggle',
                            'id' => 'dropdownMenu1',
                            'data-toggle' => 'dropdown',
                            'aria-expanded' => true,
                            'style' => 'height: 28px;',
                        ]); ?>
                        <?= Dropdown::widget([
                            'items' => $dropItems,
                            'options' => [
                                'style' => 'right: -30px; left: auto; top: 28px;',
                                'aria-labelledby' => 'dropdownMenu1',
                            ],
                        ]); ?>
                    </div>
                <?php endif ?>
                <?php if ($canDelete) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
                        'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                        'data-toggle' => 'modal',
                    ]); ?>
                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', '#many-delete', [
                        'class' => 'btn btn-default btn-sm hidden-lg',
                        'data-toggle' => 'modal',
                    ]); ?>
                <?php endif ?>
                <?php if ($canPay) : ?>
                    <?= Html::a('Оплачены', '#many-paid-modal', [
                        'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                        'data-toggle' => 'modal',
                    ]); ?>
                    <?= Html::a('<span class="ico-Paid-smart-pls fs" style="font-size: 1em;"></span>', '#many-paid-modal', [
                        'class' => 'btn btn-default btn-sm hidden-lg',
                        'data-toggle' => 'modal',
                    ]); ?>
                <?php endif ?>
                <?php if ($canCreate) : ?>
                    <div id="many-create-act" class="confirm-modal fade modal"
                         role="dialog" tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="under-date"
                                                       class="col-md-3 col-sm-3 control-label label-acts-establish1">Акты
                                                    создать от даты:</label>

                                                <div class="col-md-3 col-sm-3 inp_one_line_min cal-acts-establish">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <?= Html::textInput('Act[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                            'id' => 'under-date',
                                                            'class' => 'form-control date-picker modal-document-date',
                                                            'data-date-viewmode' => 'years',
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-3">
                                            <?= Html::a('Создать', null, [
                                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                                'data-url' => Url::to(['/documents/act/many-create', 'type' => $ioType]),
                                                'style' => 'width: 100px;',
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal" style="width: 100px;">Отменить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="many-create-packing-list" class="confirm-modal fade modal"
                         role="dialog" tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="under-date"
                                                       class="col-md-4 col-sm-4 control-label label-acts-establish2">Товарные
                                                    Накладные создать от даты:</label>

                                                <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <?= Html::textInput('PackingList[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                            'id' => 'under-date',
                                                            'class' => 'form-control date-picker modal-document-date',
                                                            'data-date-viewmode' => 'years',
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-3">
                                            <?= Html::a('Создать', null, [
                                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                                'data-url' => Url::to(['/documents/packing-list/many-create', 'type' => $ioType]),
                                                'style' => 'width: 100px;',
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal" style="width: 100px;">Отменить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="many-create-invoice-facture" class="confirm-modal fade modal"
                         role="dialog" tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="under-date"
                                                       class="col-md-2 control-label label-acts-establish3">Счета-Фактуры
                                                    создать от даты:</label>

                                                <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <?= Html::textInput('InvoiceFacture[document_date]', date(DateHelper::FORMAT_USER_DATE), [
                                                            'id' => 'under-date',
                                                            'class' => 'form-control date-picker modal-document-date',
                                                            'data-date-viewmode' => 'years',
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-3">
                                            <?= Html::a('Создать', null, [
                                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                                'data-url' => Url::to(['/documents/invoice-facture/many-create', 'type' => $ioType]),
                                                'style' => 'width: 100px;',
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal" style="width: 100px;">Отменить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="many-create-upd" class="confirm-modal fade modal"
                         role="dialog" tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label for="under-date"
                                                       class="col-md-2 control-label label-acts-establish3">
                                                    УПД создать от даты:
                                                </label>

                                                <div class="col-md-3 inp_one_line_min cal-acts-establish">
                                                    <div class="input-icon">
                                                        <i class="fa fa-calendar"></i>
                                                        <?= Html::textInput('documentDate', date(DateHelper::FORMAT_USER_DATE), [
                                                            'class' => 'form-control date-picker modal-document-date',
                                                            'data-date-viewmode' => 'years',
                                                        ]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-3">
                                            <?= Html::a('Создать', null, [
                                                'class' => 'btn darkblue pull-right modal-many-create-act',
                                                'data-url' => Url::to(['/documents/upd/many-create', 'type' => $ioType]),
                                                'style' => 'width: 100px;',
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3"></div>
                                        <div class="col-xs-3">
                                            <button type="button" class="btn darkblue" data-dismiss="modal"
                                                    style="width: 100px;">
                                                Отменить
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canDelete) : ?>
                    <div id="many-delete" class="confirm-modal fade modal" role="dialog"
                         tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">Вы уверены, что хотите удалить
                                            выбранные счета?
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="col-xs-6">
                                            <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                                'class' => 'btn darkblue pull-right modal-many-delete ladda-button',
                                                'data-url' => Url::to(['many-delete', 'type' => $ioType]),
                                                'data-style' => 'expand-right',
                                            ]); ?>
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" class="btn darkblue"
                                                    data-dismiss="modal">НЕТ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
                <?php if ($user->currentEmployeeCompany->employee_role_id == EmployeeRole::ROLE_CHIEF): ?>
                    <?php Modal::begin([
                        'id' => 'not-need-document-modal',
                    ]); ?>
                    <div class="col-md-12">
                        <div style="margin-bottom: 20px;">
                            <div class="bold">
                                Для выбранных счетов в столбце <span class="document-name"></span> проставить
                                «<span class="document-attribute_text"></span>».
                            </div>
                            Это можно будет отменить, кликнув на «<span class="document-attribute_text"></span>»
                        </div>
                        <div>
                            <div class="bold uppercase">Внимание!</div>
                            Это делается для того, чтобы в разделе финансы у вас правильно строился Управленческий
                            Баланс. Это позволит убрать суммы из строки “Предоплата покупателей (авансы)”<br>
                            Ситуации, когда <b>НУЖНО</b> проставить, что «<span class="document-attribute_text"></span>»:
                            <ul style="list-style: decimal;padding-inline-start: 15px;">
                                <li>
                                    <b><i>Счет покупатель оплатил наличными</i></b>. Вы эти деньги проводить по
                                    бухгалтерии НЕ будете. Соответственно покупателю выставлять
                                    <span class="document-name"></span> не будете.
                                </li>
                                <li>
                                    <b><i>Счет покупатель оплатил через Банк</i></b>. Вы проводить эти деньги по
                                    бухгалтерии обязаны. Но <span class="document-name"></span> выставляете не в КУБ,
                                    а в КУБ хотите получить правильный Управленческий Баланс.
                                </li>
                                <li>
                                    <b><i>Счет покупатель оплатил наличными, и вы пробили кассовый чек</i></b>. Вы
                                    проводить эти деньги по бухгалтерии обязаны. Но <span class="document-name"></span>
                                    выставляете не в КУБ, а в КУБ хотите получить правильный Управленческий
                                    Баланс.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-actions row col-md-12 p-r-0" style="margin-top: 15px;">
                        <div class="col-xs-3">
                            <?= Html::a('Применить', null, [
                                'class' => 'btn darkblue pull-left modal-not-need-document-submit',
                                'data-url' => Url::to(['not-need-document', 'type' => $ioType, 'param' => '']),
                                'style' => 'width: 120px;color: #ffffff;',
                            ]); ?>
                        </div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3 p-r-0">
                            <?= Html::button('Отменить', [
                                'class' => 'btn darkblue pull-right',
                                'data-dismiss' => 'modal',
                                'style' => 'width: 100px;color: #ffffff;',
                            ]); ?>
                        </div>
                    </div>
                    <?php Modal::end(); ?>
                <?php endif; ?>
                <?php if ($canPay) : ?>
                    <?php Modal::begin([
                        'id' => 'many-paid-modal',
                        'options' => [
                            'data-url' => Url::to(['add-flow-form', 'type' => $ioType]),
                        ]
                    ]); ?>
                    <div id="many-paid-content"></div>
                    <?php Modal::end(); ?>

                    <?php $this->registerJs('
                    $(document).on("shown.bs.modal", "#many-paid-modal", function() {
                        var idArray = $(".joint-operation-checkbox:checked").map(function () {
                            return $(this).closest("tr").data("key");
                        }).get();
                        var url = this.dataset.url + "&id=" + idArray.join();
                        $.post(url, function(data) {
                            $("#many-paid-content").html(data);
                            $("#many-paid-content input:checkbox:not(.md-check)").uniform({checkboxClass: "checker"});
                            $("#many-paid-content input:radio:not(.md-radiobtn)").uniform();
                        })
                    });
                    $(document).on("hidden.bs.modal", "#many-paid-modal", function() {
                        $("#many-paid-content").html("");
                    });
                ') ?>
                <?php endif ?>

                <?php if ($ioType == Documents::IO_TYPE_IN): ?>
                    <?= Html::a('<i class="fa fa-bank m-r-sm"></i> Платежка', '#many-charge', [
                        'class' => 'btn btn-default btn-sm hidden-md hidden-sm hidden-xs',
                        'data-toggle' => 'modal',
                    ]); ?>
                    <?= Html::a('<i class="fa fa-bank m-r-sm"></i>', '#many-charge', [
                        'class' => 'btn btn-default btn-sm hidden-lg',
                        'data-toggle' => 'modal',
                    ]); ?>
                    <div id="many-charge" class="confirm-modal fade modal" role="dialog"
                         tabindex="-1" aria-hidden="true"
                         style="display: none; margin-top: -51.5px;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8 text-left many-charge-text main-block"
                                                 style="font-size: 15px;">
                                                <span style="display: block;">Вы уверены, что хотите подготовить платежки в банк:</span>
                                                <span class="template" style="display: none;">
                                                    <span class="number"></span>. <span class="contractor-title"></span>:
                                                    счета <span class="invoice-count"></span> шт., сумма <span
                                                            class="summary"></span>
                                                    <i class="fa fa-rub"></i>
                                                </span>
                                                <span class="total-row" style="display: block;font-weight: bold;">
                                                    Итого на оплату: Счета <span class="total-invoice-count"></span> шт., Сумма
                                                    <span class="total-summary"></span> <i class="fa fa-rub"></i>
                                                </span>
                                            </div>
                                            <div class="col-md-8 text-left many-charge-text payment-invoices-block"
                                                 style="font-size: 15px;display: none;">
                                                <span style="display: block;">
                                                    Эти счета уже оплачены:
                                                </span>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>
                                    <div class="form-actions row">
                                        <div class="payment-invoices-block" style="display: none;">
                                            <div class="col-xs-12 text-center">
                                                <button type="button" class="btn darkblue"
                                                        data-dismiss="modal">ОК
                                                </button>
                                            </div>
                                        </div>
                                        <div class="main-block">
                                            <div class="col-xs-6">
                                                <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', null, [
                                                    'class' => 'btn darkblue pull-right modal-many-charge ladda-button',
                                                    'data-url' => Url::to(['/documents/payment-order/many-create',]),
                                                    'data-style' => 'expand-right',
                                                ]); ?>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn darkblue"
                                                        data-dismiss="modal">НЕТ
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($canSend) : ?>
                    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
                        'class' => 'btn btn-default btn-sm document-many-send hidden-md hidden-sm hidden-xs',
                        'data-url' => Url::to(['many-send', 'type' => $ioType]),
                    ]); ?>
                    <?= Html::a('<i class="glyphicon glyphicon-envelope"></i>', null, [
                        'class' => 'btn btn-default btn-sm document-many-send hidden-lg',
                        'data-url' => Url::to(['many-send', 'type' => $ioType]),
                    ]); ?>

                    <?php if ($sendDropItems) : ?>
                        <div class="dropdown document-many-send-dropdown hidden">
                            <?= Html::a('Отправить <span class="caret"></span>', null, [
                                'class' => 'btn btn-default btn-sm dropdown-toggle',
                                'id' => 'sendDropdownMenu1',
                                'data-toggle' => 'dropdown',
                                'aria-expanded' => true,
                                'style' => 'height: 28px;',
                            ]); ?>
                            <?= Dropdown::widget([
                                'items' => $sendDropItems,
                                'options' => [
                                    'style' => 'right: -30px; left: auto; top: 28px;',
                                    'aria-labelledby' => 'sendDropdownMenu1',
                                ],
                            ]); ?>
                        </div>
                    <?php endif ?>

                    <div class="modal fade confirm-modal" id="many-send-error"
                         tabindex="-1"
                         role="modal"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true"></button>
                                    <h3 style="text-align: center; margin: 0">Ошибка
                                        при отправке счетов</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <?php if ($canPrint) : ?>
                    <?= Html::a('<i class="fa fa-print"></i> Печать', [
                        'many-document-print',
                        'actionType' => 'pdf',
                        'type' => $ioType,
                        'multiple' => '',
                    ], [
                        'class' => 'btn btn-default btn-sm multiple-print hidden-md hidden-sm hidden-xs',
                        'target' => '_blank',
                    ]); ?>
                    <?= Html::a('<i class="fa fa-print"></i>', [
                        'many-document-print',
                        'actionType' => 'pdf',
                        'type' => $ioType,
                        'multiple' => '',
                    ], [
                        'class' => 'btn btn-default btn-sm multiple-print hidden-lg',
                        'target' => '_blank',
                    ]); ?>
                <?php endif ?>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container" style="">
                <?= $this->render('_invoices_table', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'type' => $ioType,
                    'company' => $company,
                ]); ?>
            </div>
        </div>

        <?php if ($countInvoice == 0) : ?>
            <?= \frontend\widgets\PromptWidget::widget([
                'prompt' => $prompt,
            ]); ?>
        <?php endif; ?>
    </div>
<?= \frontend\modules\documents\widgets\SummarySelectWidget::widget([
    'buttons' => [
        $canPrint ? Html::a('<i class="fa fa-print"></i> Печать', [
            'many-document-print',
            'actionType' => 'pdf',
            'type' => $ioType,
            'multiple' => ''
        ], [
            'class' => 'btn btn-sm darkblue text-white multiple-print',
            'target' => '_blank',
        ]) : null,
        $canSend ? (Html::a('<i class="glyphicon glyphicon-envelope"></i> Отправить', null, [
            'class' => 'btn btn-sm darkblue text-white document-many-send',
            'data-url' => Url::to(['many-send', 'type' => $ioType]),
        ]) . ($sendDropItems ? Html::tag('div', Html::a('Отправить  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu3',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle document-many-send-dropdown hidden',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $sendDropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null)
        ): ($ioType == Documents::IO_TYPE_IN ? Html::a('<i class="fa fa-bank m-r-sm"></i> Платежка', '#many-charge', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null),
        $canPay ? Html::a('Оплачены', '#many-paid-modal', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $canDelete ? Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить', '#many-delete', [
            'class' => 'btn btn-sm darkblue text-white',
            'data-toggle' => 'modal',
        ]) : null,
        $dropItems ? Html::tag('div', Html::a('Еще  <b class="caret"></b>', '#', [
                'id' => 'dropdownMenu2',
                'class' => 'btn btn-sm darkblue text-white dropdown-toggle',
                'data-toggle' => 'dropdown',
            ]) . Dropdown::widget([
                'items' => $dropItems,
                'options' => [
                    'style' => 'left: auto; right: 0;'
                ],
            ]), ['class' => 'dropup']) : null,
    ],
]); ?>
<?php /*Pjax::end();*/ ?>

<?php if ($canSend): ?>
    <?= $this->render('view/_many_send_message', [
        'models' => [],
        'useContractor' => false,
        'showSendPopup' => false,
        'typeDocument' => Documents::DOCUMENT_INVOICE
    ]); ?>
<?php endif; ?>

<?php $this->registerJs('
    $(document).on("shown.bs.modal", "#many-charge", function () {
        var countChecked = 0;
        var totalSummary = 0;
        var data = {};
        var payedInvoices = {};
        var mainBlock = $(".main-block");
        var paymentInvoiceBlock = $(".payment-invoices-block");

        $(this).find(".generated-row").remove();
        mainBlock.show();
        paymentInvoiceBlock.hide();

        $(".joint-operation-checkbox:checked").each(function(){
            var price = $(this).closest("tr").find(".price").data("remaining-amount");
            var contractor = $(this).closest("tr").find(".contractor-cell a");
            var contractorID = contractor.data("id");
            var contractorTitle = contractor.attr("title");
            var invoiceNumber = $(this).closest("tr").find(".document_number span").data("full-name");

            price = parseFloat(price.replace(",", "."));
            if (price > 0) {
                if (data[contractorID] == undefined) {
                    data[contractorID] = {
                        "id": contractorID,
                        "title": contractorTitle,
                        "invoiceCount": 1,
                        "summary": price,
                    };
                } else {
                    data[contractorID]["summary"] = data[contractorID]["summary"] + price;
                    data[contractorID]["invoiceCount"] = data[contractorID]["invoiceCount"] + 1;
                }
                totalSummary += price;
                countChecked++;
            } else {
                payedInvoices[invoiceNumber] = invoiceNumber;
            }
        });

        var number = 1;
        for (var key in data) {
            var $template = $("#many-charge .many-charge-text .template").clone();
            $template.find(".number").text(number);
            $template.find(".contractor-title").text(data[key]["title"]);
            $template.find(".invoice-count").text(data[key]["invoiceCount"]);
            $template.find(".summary").text(number_format(data[key]["summary"], 2, ",", " "));
            $template.css("display", "block");
            $template.removeClass("template");
            $template.addClass("generated-row");
            $("#many-charge .many-charge-text.main-block").append($template);
            number++;
        };
        var $totalRow = $("#many-charge .many-charge-text.main-block .total-row").clone();
        $("#many-charge .many-charge-text.main-block .total-row").remove();
        $totalRow.find(".total-invoice-count").text(countChecked);
        $totalRow.find(".total-summary").text(number_format(totalSummary, 2, ",", " "));
        $("#many-charge .many-charge-text.main-block").append($totalRow);

        if (countChecked == 0) {
            mainBlock.hide();
            if (Object.keys(payedInvoices).length > 0) {
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.payment-invoices-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
            paymentInvoiceBlock.show();
        } else {
            if (Object.keys(payedInvoices).length > 0) {
               $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;margin-top:15px;>Эти счета уже оплачены:</span>");
               for (var key in payedInvoices) {
                   $("#many-charge .many-charge-text.main-block").append("<span class=generated-row style=display:block;>" + key + "</span>");
               }
            }
        }
    });

    $(".act-not-need, .packing-list-not-need, .upd-not-need").click(function (e) {
        $("#not-need-document-modal span.document-name").text($(this).data("name"));
        $("#not-need-document-modal span.document-attribute_text").text($(this).data("attribute_text"));
        $("#not-need-document-modal .modal-not-need-document-submit").data("url",  $("#not-need-document-modal .modal-not-need-document-submit").data("url") + $(this).data("param"));
    });
'); ?>
