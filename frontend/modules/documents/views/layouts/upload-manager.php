<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->context->layoutWrapperCssClass = 'upload-manager';
$this->beginContent('@frontend/views/layouts/main.php');
$activePage = Yii::$app->controller->action->id;
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

<div class="navbar-report navbar-default navbar">
    <div class="container-fluid" style="padding: 0;">
        <div class="navbar-header">
            <span class="navbar-brand" style="margin-left: 0;">
                <?php if ($activePage == 'upload'): ?>
                    <i><img src="/img/documents/recognize-b.png" width="32" style="margin-top:-4px;"></i>
                <?php elseif ($activePage == 'all-documents'): ?>
                    <i><img src="/img/documents/all-documents-black.png" width="28" style="margin-top:-4px"></i>
                <?php elseif ($activePage == 'directory'): ?>
                    <i><img src="/img/documents/folder-black-2.png" width="28" style="margin-top:-7px"></i>
                <?php endif; ?>
                <?= $this->title ?>
                <?php if ($activePage == 'upload'): ?>
                    <i><img class="odds-panel-trigger" src="/img/open-book.svg" style="margin-left:5px; width:30px; cursor:pointer"></i>
                <?php endif; ?>
            </span>
        </div>
    </div>
</div>

<?php echo $content ?>

</div>

<?php $this->endContent(); ?>
