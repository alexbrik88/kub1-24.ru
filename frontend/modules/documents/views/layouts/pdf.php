<?php
/**
 * Created by konstantin.
 * Date: 15.7.15
 * Time: 14.49
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $asset \yii\web\AssetBundle */

$asset = ArrayHelper::getValue($this->params, 'asset',
    \frontend\modules\documents\assets\DocumentPrintAsset::className());
$asset::register($this);
?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
</head>
<body>

<div class="page-content">
    <?php echo $content; ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
