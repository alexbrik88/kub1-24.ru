<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

<?php
NavBar::begin([
    'brandLabel' => 'Документы',
    'brandUrl' => null,
    'options' => [
        'class' => 'navbar-report navbar-default',
    ],
    'brandOptions' => [
        'style' => 'margin-left: 0;'
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
        'style' => 'padding: 0;'
    ],
]);
echo Nav::widget([
    'items' => [
        ['label' => 'Разное', 'url' => ['/documents/specific-document/index']],
        [
            'label' => 'Из моб. приложения',
            'url' => ['/documents/scan-document/index'],
            'visible' => (Yii::$app->user->can(UserRole::ROLE_CHIEF) ||
                         Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT) ||
                         Yii::$app->user->can(UserRole::ROLE_SUPERVISOR)),
        ],
    ],
    'options' => ['class' => 'navbar-nav navbar-right'],
]);
NavBar::end();
?>

<?php echo $content ?>

</div>

<?php $this->endContent(); ?>
