<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;

$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="invoice-report-content container-fluid" style="padding: 0; margin-top: -10px;">

    <?php echo $content ?>

</div>

<?php $this->endContent(); ?>
