<?php //todo: temp for new agreements
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php if (in_array(Yii::$app->controller->id, ['agreement-template', 'agreement'])) : ?>
    <div class="portlet box">
        <div class="btn-group pull-right">
            <span style="border: 1px solid #e5e5e5; padding: 3px 0 7px 0;">
                <?php if (\Yii::$app->controller->id == 'agreement-template') : ?>
                    <?= Html::a('Шаблоны', ['/documents/agreement-template/', 'type' => $type], [
                        'class' => 'btn yellow-text',
                        'style' => 'width: 80px; padding: 4px;',
                    ]) ?>
                    <?= Html::a('Договоры', ['/documents/agreement/', 'type' => $type], [
                        'class' => 'btn yellow',
                        'style' => 'width: 80px; padding: 4px;',
                    ]) ?>
                <?php else : ?>
                    <?= Html::a('Договоры', ['/documents/agreement/', 'type' => $type], [
                        'class' => 'btn yellow-text',
                        'style' => 'width: 80px; padding: 4px;',
                    ]) ?>
                    <?= Html::a('Шаблоны', ['/documents/agreement-template/', 'type' => $type], [
                        'class' => 'btn yellow',
                        'style' => 'width: 80px; padding: 4px;',
                    ]) ?>
                <?php endif ?>
            </span>
        </div>
    </div>
    <div class="clearfix" style="margin-bottom: 25px"></div>
<?php endif; ?>