<?php

use frontend\rbac\UserRole;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;

$this->beginContent('@frontend/views/layouts/main.php');
?>

<div class="agreements-templates-content container-fluid" style="padding: 0;">

    <?php echo $content ?>

</div>

<?php $this->endContent(); ?>
