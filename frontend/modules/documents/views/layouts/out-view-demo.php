<?php

use frontend\assets\AppAsset;
use frontend\modules\cash\modules\banking\components\Banking;
use lavrentiev\widgets\toastr\NotificationFlash;
use yii\bootstrap\Modal;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$controllerId = Yii::$app->controller->id;
$uid = Yii::$app->request->get('uid');
$canSave = ArrayHelper::getValue($this->params, 'canSave', false);
$canPay = ArrayHelper::getValue($this->params, 'canPay', false);
$invoice = ArrayHelper::getValue($this->params, 'invoice');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    <link rel="icon" href="/img/fav.svg?i=2" type="image/x-icon">
    <?= $this->render('_out-view-style') ?>
</head>
<body class="out-view-page">
<?php $this->beginBody() ?>

<?php NavBar::begin([
    'brandLabel' => $this->render('out_view/brand_label'),
    'brandUrl' => Yii::$app->params['serviceSite'],
    'brandOptions' => [
        'target' => '_blank',
    ],
    'options' => [
        'class' => 'navbar-default navbar-fixed-top',
    ],
    'innerContainerOptions' => [
        'class' => 'container-fluid',
    ],
]); ?>

<?= Nav::widget([
    'options' => [
        'id' => 'out-view-navbar',
        'class' => 'navbar-nav',
    ],
    'encodeLabels' => false,
    'items' => [
        [
            'label' => $this->render('out_view/pdf_label'),
            'url' => '#',
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'id' => 'download-pdf-link',
                'data' => [
                    'toggle' => 'modal',
                ],
            ],
        ],
        [
            'label' => $this->render('out_view/word_label'),
            'url' => '#',
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'id' => 'download-word-link',
                'data' => [
                    'toggle' => 'modal',
                ],
            ],
        ],
        [
            'label' => $this->render('out_view/save_label'),
            'url' => '#',
            'options' => [
                'class' => 'out-view-item',
            ],
            'linkOptions' => [
                'id' => 'document-save-link',
                'data' => [
                    'toggle' => 'modal',
                ],
            ],
        ],
        [
            'label' => $this->render('out_view/pay_label'),
            'url' => '#',
            'options' => [
                'class' => 'out-view-item-pay',
            ],
            'linkOptions' => [
                'id' => 'document-pay-link',
                'class' => 'document-pay-link',
            ],
        ],
    ],
]); ?>

<?php NavBar::end(); ?>

<?= NotificationFlash::widget([
        'options' => [
            'closeButton' => true,
            'showDuration' => 1000,
            'hideDuration' => 1000,
            'timeOut' => 5000,
            'extendedTimeOut' => 1000,
            'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
        ],
]); ?>

<div style="margin-top: 30px;width: 675px;margin-left: auto;margin-right: auto;position: relative;">
    <?= $content ?>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
