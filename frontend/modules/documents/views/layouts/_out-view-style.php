
<style type="text/css">
    .out-view-page .navbar-default.navbar {
        background-color: #f7f7f7;
        background-image: none;
        border-bottom-color: #cccccc;
    }
    .out-view-page .navbar-default.navbar .navbar-header a.navbar-brand,
    .out-view-page .navbar-default.navbar .navbar-nav > li > a {
        padding-top: 4px;
        padding-bottom: 4px;
    }
    .out-view-page .navbar-default .navbar-brand {
        height: auto;
        font-size: 11pt;
        font-weight: 700;
        color: #122c49;
    }
    .out-view-page .navbar-default .menu_label_wrapper {
        display: table;
        width: 100%;
    }
    .out-view-page .navbar-default .menu_label_inner {
        display: table-row;
        width: 100%;
    }
    .out-view-page .navbar-default .menu_label_icon {
        display: table-cell;
        width: 120px;
        padding-left: 20px;
        vertical-align: middle;
        text-align: center;
    }
    .out-view-page .navbar-default .menu_label_icon .icon_circle {
        width: 72px;
        height: 72px;
        margin: 0 auto;
        padding: 16px 0;
        text-align: center;
        border-radius: 36px!important;
        background-color: #f1f1f1;
    }
    .out-view-page .navbar-default .menu_label_content {
        display: table-cell;
        height: 72px;
        vertical-align: middle;
        text-align: left;
        line-height: 20px;
    }
    .out-view-page .navbar-default .navbar-collapse {
        margin-left: 270px;
    }
    .out-view-page .navbar-default .navbar-nav {
        margin: 0 auto;
        width: 100%;
        float: none;
    }
    .out-view-page .navbar-default .navbar-nav > li {
        width: 24%;
        text-align: center;
        border-left: 1px solid #cccccc;
    }
    .out-view-page .navbar-default .navbar-nav > li > a {
        font-size: 11pt;
        color: #4276a4;
        font-weight: 700;
        padding-left: 0;
        padding-right: 0;
        color: #122c49;
    }
    .out-view-page .navbar-default .navbar-nav > li.out-view-item-pay {
        width: 25%;
        margin-right: -15px;
        float: right;
        background-color: #3a8bf7;
    }
    .out-view-page .navbar-default .navbar-nav > li.out-view-item-pay > a {
        color: #fff;
        text-transform: uppercase;
    }

    @media (min-width: 1350px) {
        .out-view-page .navbar-default .navbar-collapse {
            margin-left: calc((100vw - 900px)/2);
        }
    }
    @media (max-width: 1119px) {
        .out-view-page .navbar-default .navbar-collapse {
            margin-left: 0;
        }
        .out-view-page .navbar-collapse.collapse {
            display: none !important;
        }
        .out-view-page .navbar-collapse.collapse.in {
            display: block !important;
        }
        .out-view-page .navbar-header .collapse, .navbar-toggle {
            display:block !important;
        }
        .out-view-page .navbar-header {
            float:none;
        }
        .out-view-page .navbar-nav,
        .out-view-page .navbar-nav.nav > li {
            width: 100%;
            max-width: none;
            float: none;
            text-align: left;
            border-width: 0;
        }
        .out-view-page .navbar-nav.nav > li > a {
            position: relative;
            display: block;
            width: 100%;
            margin: 10px 0;
            padding: 0;
        }
        .out-view-page .navbar-nav.nav > li:last-child {
            margin-bottom: 10px;
        }
        .out-view-page .navbar-default .menu_label_icon {
            width: 60px;
            padding: 0;
        }
        .out-view-page .navbar-default .menu_label_content {
            height: auto;
        }
        .out-view-page .navbar-default .menu_label_content .content_part {
            display: inline;
        }
        .out-view-page .navbar-default .navbar-nav > li.out-view-item-pay {
            float: none;
            background-color: inherit;
        }
        .out-view-page .navbar-default .navbar-nav > li.out-view-item-pay > a {
            color: #122c49;
        }
        .out-view-page .navbar-default .menu_label_icon .icon_circle {
            display: inline;
            width: auto;
            height: auto;
            margin: 0;
            padding: 0;
            border-radius: none!important;
            background-color: inherit;
        }
        .out-view-page .navbar-default .menu_label_icon .icon_circle .cls-2 {
            fill: #122c49;
        }
    }

    @media (min-width: 1120px) {
        .out-view-page .navbar-nav > li.out-view-item-save {
            width: calc((100vw - 857px)/2);
        }
        .out-view-page .navbar-default .navbar-nav > li > a > span.save-link {
            bottom: 13px;
            position: relative;
            width: 160px;
            display: inline-table;
        }
    }

    #out-view-shading {
        z-index: 10050;
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        overflow: auto;
        opacity: 0;
    }

    #document-save-wrapper, #document-pay-wrapper {
        font-family: "Open Sans", sans-serif;
        z-index: 10051;
        position: fixed;
        top: 0;
        right: -400px;
        bottom: 0;
        left: auto;
        width: 400px;
        background-color: #fff;
        max-height: 100%;
        overflow-y: auto;
        box-shadow: rgba(51, 45, 179, 0.098) 0px 10px 30px, rgba(32, 46, 113, 0.1) 0px 5px 5px;
    }

    #document-save-header, #document-pay-header {
        position: relative;
        padding: 25px 15px 15px 15px;
        font-size: 21px;
        font-weight: 700;
        line-height: 27px;
    }

    #document-save-content {
        padding: 15px;
    }

    #document-save-content .form-header {
        padding-bottom: 15px;
        margin-bottom: 15px;
    }

    #document-save-close, #document-pay-close {
        position: absolute;
        top: 10px;
        right: 10px;
        background-size: 12px 12px;
        width: 12px;
        height: 12px;
        opacity: 1;
    }

    #invoiceoutviewform-taxationtype label {
        display: block;
    }

    input#invoiceoutviewform-password, input#invoiceoutviewform-username {
        border-top: 0;
        border-left: 0;
        border-right: 0;
        padding-left: 0;
        padding-right: 0;
    }

    #document-pay-content .pay-block {
        padding: 10px 15px;
    }

    #document-pay-content .pay-block.pay-link {
        cursor: pointer;
    }

    #document-pay-content .pay-block table {
        width: 100%;
        vertical-align: middle;
    }
    #document-pay-content .pay-block table td {
        padding: 0;
        margin: 0;
        border-width: 0;
        vertical-align: middle;
    }
    #document-pay-content .pay-block .main-text {
        font-size: 14px;
        font-weight: 600;
    }

    #document-pay-content .pay-block .additional-text {
        font-size: 12px;
    }

    #document-pay-content .pay-block .hover-text {
        opacity: 0;
    }
    #document-pay-content .pay-block:hover .hover-text {
        opacity: 1;
    }

    #document-pay-content .pay-block.pay-link:hover {
        background-color: #3a8bf7;
    }
</style>