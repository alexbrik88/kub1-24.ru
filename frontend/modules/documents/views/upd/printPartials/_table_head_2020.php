<tr>
    <td rowspan="3" class="text-c bor bor2-t bor2-l" style="width: 3%;">
        N п/п
    </td>
    <td rowspan="3" class="text-c bor bor2-t bor2-r" style="width: 5.5%;">
        Код товара/ работ, услуг
    </td>
    <td rowspan="3" class="text-c bor bor2-t bor2-l" style="width: 28.5%;">
        Наименование товара (описание выполненных работ,
        оказанных услуг), имущественного права
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 3%;">
        Код вида товара
    </td>
    <td colspan="2" class="text-c bor bor2-t" style="width: 8%;">
        Единица измерения
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 3%;">
        Коли-чество (объем)
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 8%;">
        Цена (тариф) за единицу измерения
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 8%;">
        Стоимость товаров (работ, услуг), имущественных прав без
        налога - всего
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 5%;">
        В том числе сумма акциза
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 4%;">
        Налоговая ставка
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 5%;">
        Сумма налога, предъявляемая покупателю
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 8%;">
        Стоимость товаров(работ, услуг), имущественных прав с
        налогом - всего
    </td>
    <td colspan="2" rowspan="2" class="text-c bor bor2-t" style="width: 9%;">
        Страна происхождения товара
    </td>
    <td rowspan="3" class="text-c bor bor2-t bor2-r" style="width: 5%;">
        Регистрационный номер таможенной декларации
    </td>
</tr>
<tr>
    <td rowspan="2" class="text-c bor" style="border-right: 1px solid #000;padding: 0 5px;">
        код
    </td>
    <td rowspan="2" class="text-c bor" style="width:4%; padding: 0 5px;">услов&shy;ное обозна&shy;чение (национа&shy;льное)
    </td>
</tr>
<tr>
    <td class="text-c bor" style="border-right: 1px solid #000;">
        Цифро-вой код
    </td>
    <td class="text-c bor">
        Краткое наименование
    </td>
</tr>
<tr class="">
    <td class="text-c bor bor2-l">
        А
    </td>
    <td class="text-c bor bor2-r">
        Б
    </td>
    <td class="text-c bor">
        1
    </td>
    <td class="text-c bor">
        1a
    </td>
    <td class="text-c bor">
        2
    </td>
    <td class="text-c bor">
        2а
    </td>
    <td class="text-c bor">
        3
    </td>
    <td class="text-c bor">
        4
    </td>
    <td class="text-c bor">
        5
    </td>
    <td class="text-c bor">
        6
    </td>
    <td class="text-c bor">
        7
    </td>
    <td class="text-c bor">
        8
    </td>
    <td class="text-c bor">
        9
    </td>
    <td class="text-c bor">
        10
    </td>
    <td class="text-c bor">
        10а
    </td>
    <td class="text-c bor bor2-r">
        11
    </td>
</tr>
