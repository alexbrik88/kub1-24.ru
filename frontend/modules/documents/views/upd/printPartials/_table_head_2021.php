<tr class="font-size-6">
    <td rowspan="3" class="text-c bor bor2-t bor2-l bor2-r" style="width: 6%;">
        Код товара/ работ, услуг
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 2%;">
        N п/п
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 20%;">
        Наименование товара (описание выполненных работ,
        оказанных услуг), имущественного права
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 3%;">
        Код вида товара
    </td>
    <td colspan="2" class="text-c bor bor2-t">
        Единица измерения
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 3%;">
        Коли-чество (объем)
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 7%;">
        Цена (тариф) за единицу измерения
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 7%;">
        Стоимость товаров (работ, услуг), имущественных прав без
        налога - всего
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 4%;">
        В том числе сумма акциза
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 3%;">
        Налоговая ставка
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 5%;">
        Сумма налога, предъявляемая покупателю
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 7%;">
        Стоимость товаров(работ, услуг), имущественных прав с
        налогом - всего
    </td>
    <td colspan="2" rowspan="2" class="text-c bor bor2-t">
        Страна происхож&shy;дения товара
    </td>
    <td rowspan="3" class="text-c bor bor2-t" style="width: 6%;">
        Регистра&shy;ционный номер декла&shy;рации
        на товары или реги&shy;страцион&shy;ный номер партии товара,
        подлежа&shy;щего просле&shy;живаемо&shy;сти
    </td>
    <td rowspan="2" colspan="2" class="text-c bor bor2-t">
        Количествен&shy;ная едини&shy;ца измере&shy;ния това&shy;ра, используе&shy;мая в целях осуществле&shy;ния просле&shy;живае&shy;мости
    </td>
    <td rowspan="3" class="text-c bor bor2-t bor2-r" style="width: 7%;">
        Количе&shy;ство товара, подле&shy;жаще&shy;го просле&shy;живаемо&shy;сти, в коли&shy;чествен&shy;ной едини&shy;це измере&shy;ния това&shy;ра, используе&shy;мой в целях осуществле&shy;ния прослежи&shy;ваемости
    </td>
</tr>
<tr class="font-size-6">
    <td rowspan="2" class="text-c bor" style="width:3%; border-right: 1px solid #000;padding: 0 5px;">
        код
    </td>
    <td rowspan="2" class="text-c bor" style="width:4%; padding: 0 5px;">услов&shy;ное обозна&shy;чение (национа&shy;льное)
    </td>
</tr>
<tr class="font-size-6">
    <td class="text-c bor" style="width:2%; border-right: 1px solid #000;">
        Цифро&shy;вой код
    </td>
    <td class="text-c bor" style="width:3%; ">
        Краткое наимено&shy;вание
    </td>
    <td class="text-c bor" style="width:4%; border-right: 1px solid #000;">
        Код
    </td>
    <td class="text-c bor" style="width:4%; ">
        Услов&shy;ное обозна&shy;чение
    </td>
</tr>
<tr class="">
    <td class="text-c bor bor2-l bor2-r">
        А
    </td>
    <td class="text-c bor">
        1
    </td>
    <td class="text-c bor">
        1а
    </td>
    <td class="text-c bor">
        1б
    </td>
    <td class="text-c bor">
        2
    </td>
    <td class="text-c bor">
        2а
    </td>
    <td class="text-c bor">
        3
    </td>
    <td class="text-c bor">
        4
    </td>
    <td class="text-c bor">
        5
    </td>
    <td class="text-c bor">
        6
    </td>
    <td class="text-c bor">
        7
    </td>
    <td class="text-c bor">
        8
    </td>
    <td class="text-c bor">
        9
    </td>
    <td class="text-c bor">
        10
    </td>
    <td class="text-c bor">
        10а
    </td>
    <td class="text-c bor">
        11
    </td>
    <td class="text-c bor">
        12
    </td>
    <td class="text-c bor">
        12а
    </td>
    <td class="text-c bor bor2-r">
        13
    </td>
</tr>
