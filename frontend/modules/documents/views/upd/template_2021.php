<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Upd;
use common\models\product\Product;
use frontend\assets\UpdAsset;
use frontend\models\Documents;
use php_rutils\RUtils;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */

common\assets\DocumentTemplateAsset::register($this);

$this->title = $model->printTitle;

$model->isGroupOwnOrdersByProduct = true;

$documentDate = RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$payment = $model->getPaymentDocuments()->one();
$paymentNumber = $payment ? $payment->payment_document_number : '';
$paymentDate = $payment ? RUtils::dt()->ruStrFTime([
    'date' => $payment->payment_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]) : '';
$invoice = $model->invoice;
$isIp = $invoice->company->getIsLikeIp();
$hasNds = $invoice->hasNds;
$totalAmountNoNds = $hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds();
$totalNds = $hasNds ? $model->totalNds : 0;
$isOsno = $invoice->company->companyTaxationType->osno;

$paymentDocuments = [];
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
}
$shippingDocuments = [];
foreach ([$model] as $doc) {
    $date = date('d.m.Y', strtotime($doc->document_date));
    $positionsCount = count($model->ownOrders);
    $positionNumber = ($positionsCount > 1) ? "1 - $positionsCount" : $positionsCount;
    $shippingDocuments[] = "№ п/п {$positionNumber} № {$model->fullNumber} от {$date}";
}
$precision = $invoice->price_precision;

$company = $model->company;
$signatureHeight = $accountantSignatureHeight = $defaultSignatureHeight = 30;
$signatureLink = $accountantSignatureLink = $printLink = null;

$isOut = $model->type == Documents::IO_TYPE_OUT;
$addStamp = $isOut && $model->add_stamp;
if ($model->signed_by_employee_id) {
    $accountantSignatureHeight = $signatureHeight = ImageHelper::getSignatureHeight($model->employeeSignature->file ?? null, 30, 60);
    $accountantSignatureLink = $signatureLink = $model->employeeSignature ? EasyThumbnailImage::thumbnailSrc(
        $model->employeeSignature->file,
        100,
        $accountantSignatureHeight,
        EasyThumbnailImage::THUMBNAIL_INSET_BOX
    ) : null;
} else {
    $signatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefSignatureImage'), 30, 60);
    $signatureLink = EasyThumbnailImage::thumbnailSrc(
        $model->getImage('chiefSignatureImage'),
        100,
        $signatureHeight,
        EasyThumbnailImage::THUMBNAIL_INSET_BOX
    );

    if ($company->chief_is_chief_accountant) {
        $accountantSignatureHeight = $signatureHeight;
        $accountantSignatureLink = $signatureLink;
    } else {
        $accountantSignatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefAccountantSignatureImage'), 30, 60);
        $accountantSignatureLink = EasyThumbnailImage::thumbnailSrc(
            $model->getImage('chiefAccountantSignatureImage'),
            100,
            $accountantSignatureHeight,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );
    }
}
$printLink = EasyThumbnailImage::thumbnailSrc(
    $model->getImage('printImage'),
    200,
    200,
    EasyThumbnailImage::THUMBNAIL_INSET_BOX
);

$printUrl = $addStamp ? $printLink : null;
$printCss = "
    background-image: url('{$printUrl}');
    background-position: 33% 100%;
    background-repeat: no-repeat, no-repeat;
    -webkit-print-color-adjust: exact;
";

if ($model->signed_by_employee_id) {
    $chief = $accountant = $model->signed_by_name . '<br>по ' .
        ($model->signBasisDocument ? mb_strtolower($model->signBasisDocument->name2) : 'доверенности') .
        ' №' . $model->sign_document_number . '<br>от ' .
        date("d.m.Y", strtotime($model->sign_document_date)) . 'г.';
    $chiefPosition = $model->signedByEmployee->currentEmployeeCompany->position;
} else {
    $chief = $invoice->getCompanyChiefFio(true);
    $accountant = $invoice->getCompanyChiefAccountantFio(true);
    $chiefPosition = $invoice->company_chief_post_name;
}

if ($invoice->production_type) {
    $sender = $model->consignor ? $model->consignor->shortName.', '.$model->consignor->legal_address : 'он же';
    if ($model->consignee) {
        $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
            $model->consignee->legal_address : $model->consignee->actual_address;
        $recipient = $model->consignee->shortName . ', ' . $address;
    } else {
        if ($isOut) {
            $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
                $model->invoice->contractor_address_legal_full : $model->invoice->contractor->actual_address;
            $recipient = $invoice->contractor_name_short . ', ' . $address;
        } else {
            $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
                $model->invoice->company_address_legal_full : $model->invoice->company->address_actual;
            $recipient = $invoice->company_name_short . ', ' . $address;
        }
    }
} else {
    $sender = $recipient = Product::DEFAULT_VALUE;
}

$tplVer = $company->invoice_facture_print_template == 2 ? 2 : 1;
?>

<style>
    div.document-template tr.font-size-6 td, div.document-template tr td.font-size-6 { font-size: 9px!important; }
</style>

<div class="document-template upd-template block-with-print" style="font-size: 9px; <?= $printCss ?>" data-image="<?= $printLink ?>">
    <table class="" style="font-size: 9px;">
        <tbody>
            <tr>
                <td colspan="1" class="v-al-t pad-r-10 bor2-b bor2-r">
                    <div>
                        Универсальный передаточный документ
                    </div>
                    <table class="mar-b-5 mar-t-5">
                        <tr>
                            <td style="padding-right: 7px;">
                                Статус:
                            </td>
                            <td style="padding: 3px 6px; text-align: center; border: 2px solid #000000;">
                                <?= $isOsno ? 1 : 2; ?>
                            </td>
                        </tr>
                    </table>
                    <div style="font-size: 7px;">
                        1 - счет-фактура и передаточный документ (акт)
                    </div>
                    <div style="font-size: 7px;">
                        2 - передаточный документ (акт)
                    </div>
                </td>
                <td colspan="18" class="v-al-t pad-0 pad-b-10 bor2-b bor2-l">
                    <?= $this->render("templatePartials/_template_head_2021_v{$tplVer}", ['model' => $model]) ?>
                </td>
            </tr>

            <?php echo $this->render('printPartials/_table_head_2021') ?>

            <?php foreach ($model->ownOrders as $key => $orderUpd) : ?>
                <?php
                $order = $orderUpd->order;
                $product = $order->product;
                $amountNoNds = $model->getPrintOrderAmount($orderUpd->order_id, $hasNds);
                $amountWithNds = $model->getPrintOrderAmount($orderUpd->order_id);
                $isGoods = ($product->production_type == Product::PRODUCTION_TYPE_GOODS);
                $orderUpd->quantity *= 1;
                $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
                ?>
                <tr>
                    <td class="bor text-c bor2-l bor2-r">
                        <?= $product->code ? : Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= $key + 1 ?>
                    </td>
                    <td class="bor">
                        <?= $order->product_title; ?>
                    </td>
                    <td class="bor text-c">
                        <?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?>
                    </td>
                    <td class="bor text-c">
                        <?= (!$hideUnits && $order->unit) ?
                            $order->unit->code_okei :
                            Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= (!$hideUnits && $order->unit) ?
                            $order->unit->name :
                            Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= ($hideUnits && $orderUpd->quantity == 1) ?
                            Product::DEFAULT_VALUE :
                            strtr($orderUpd->quantity, ['.' => ',']); ?>
                    </td>
                    <td class="bor text-c">
                        <?= $hideUnits ? Product::DEFAULT_VALUE :
                            TextHelper::invoiceMoneyFormat($orderUpd->priceNoNds, $precision) ?>
                    </td>
                    <td class="bor text-c">
                        <?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?>
                    </td>
                    <td class="bor text-c">
                        <?= $order->excise ?
                            TextHelper::invoiceMoneyFormat($order->excise_price, 2) :
                            'без&nbsp;акциза'; ?>
                    </td>
                    <td class="bor text-c">
                        <?php if ($hasNds): ?>
                            <?= ($model->type == Documents::IO_TYPE_OUT) ? $order->saleTaxRate->name : $order->purchaseTaxRate->name ?>
                        <?php else: ?>
                            без НДС
                        <?php endif; ?>
                    </td>
                    <td class="bor text-c">
                        <?= $hasNds ?
                            TextHelper::invoiceMoneyFormat($orderUpd->amountNds, $precision) :
                            'без НДС' ?>
                    </td>
                    <td class="bor text-c">
                        <?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision) ?>
                    </td>
                    <td class="bor text-c">
                        <?= $orderUpd->country->code ? : Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= $orderUpd->country->name_short ? : Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= ($orderUpd->custom_declaration_number) ?: Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= ($order->product->is_traceable && $order->unit) ? $order->unit->code_okei : Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c">
                        <?= ($order->product->is_traceable && $order->unit) ? $order->unit->name : Product::DEFAULT_VALUE ?>
                    </td>
                    <td class="bor text-c bor2-r">
                        <?= ($order->product->is_traceable) ?  strtr($orderUpd->quantity, ['.' => ',']) : Product::DEFAULT_VALUE; ?>
                    </td>
                </tr>
            <?php endforeach ?>
            <tr>
                <td class="bor bor2-b bor2-l bor2-r"></td>
                <td class="bor bor2-b"></td>
                <td class="bor bor2-b" colspan="6">
                    Всего к оплате
                </td>
                <td class="bor bor2-b text-c">
                    <?= TextHelper::invoiceMoneyFormat($totalAmountNoNds, 2); ?>
                </td>
                <td class="bor bor2-b text-c" colspan="2">
                    X
                </td>
                <td class="bor bor2-b text-c">
                    <?= TextHelper::invoiceMoneyFormat($totalNds, 2); ?>
                </td>
                <td class="bor bor2-b text-c">
                    <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?>
                </td>
                <td class="bor bor2-b bor2-r" colspan="6"></td>
            </tr>
            <tr>
                <td colspan="1" class="bor2-t bor2-r pad-b-10 pad-0 v-al-t">
                    <table style="margin: 10px 10px 0 0;">
                        <tr>
                            <td class=""  colspan="2">
                                Документ
                                <br>
                                составлен на
                            </td>
                        </tr>
                        <tr>
                            <td class="bor-b text-c">
                                1
                            </td>
                            <td class="">
                                листах
                            </td>
                        </tr>
                    </table>
                </td>
                <td colspan="18" class="bor2-l bor2-t bor2-b pad-0">
                    <table>
                        <tbody>
                            <tr>
                                <td class="pad-r-10 v-al-b" width="15%">
                                    Руководитель организации или иное уполномоченное лицо
                                </td>
                                <td class="bor-b v-al-b pad-0 text-c" width="13%">
                                    <?php if ($isOut && !$isIp)
                                        echo Html::tag('img', null, [
                                            'src' => $signatureLink,
                                            'class' => 'signature_image',
                                            'style' =>
                                                'margin-bottom: '. ($defaultSignatureHeight - $signatureHeight) . 'px;'
                                                . 'display: ' . ($addStamp ? 'block;' : 'none;'),
                                        ]);
                                    ?>
                                </td>
                                <td class="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="bor-b v-al-b" width="19%">
                                    <?php if ($isOut && !$isIp) : ?>
                                        <?= $chief ?>
                                    <?php endif ?>
                                </td>
                                <td class="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="pad-r-10 v-al-b" width="15%">
                                    Главный бухгалтер или иное уполномоченное лицо
                                </td>
                                <td class="bor-b v-al-b pad-0 text-c" height="50px" width="13%">
                                    <?php if ($isOut && !$isIp)
                                        echo Html::tag('img', null, [
                                            'src' =>$accountantSignatureLink,
                                            'class' => 'signature_image',
                                            'style' =>
                                                'margin-bottom:' . ($defaultSignatureHeight - $accountantSignatureHeight) .'px;'
                                                . 'display: ' . ($addStamp ? 'block;' : 'none;'),
                                        ])
                                    ?>
                                </td>
                                <td class="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="bor-b v-al-b" width="19%">
                                    <?php if ($isOut && !$isIp) : ?>
                                        <?= $accountant ?>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="" width="15%">
                                </td>
                                <td class="v-al-t text-c text-xs" width="13%">
                                    подпись
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="v-al-t text-c text-xs" width="19%">
                                    (ф.и.о)
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="" width="15%">
                                </td>
                                <td class="v-al-t text-c text-xs" width="13%">
                                    подпись
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="v-al-t text-c text-xs" width="19%">
                                    (ф.и.о)
                                </td>
                            </tr>
                            <tr>
                                <td class="pad-r-10 v-al-b" width="15%">
                                    Индивидуальный предприниматель или иное уполномоченное лицо
                                </td>
                                <td class="bor-b v-al-b pad-0 text-c" height="50px" width="13%">
                                    <?php if ($isOut && $isIp)
                                        echo Html::tag('img', null, [
                                            'src' =>$signatureLink,
                                            'class' => 'signature_image',
                                            'style' =>
                                                'margin-bottom:' . ($defaultSignatureHeight - $signatureHeight) . 'px;'
                                                . 'display: ' . ($addStamp ? 'block;' : 'none;'),
                                        ])
                                    ?>
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="bor-b v-al-b" width="19%">
                                    <?php if ($isOut && $isIp): ?>
                                        <?= $chief ?>
                                    <?php endif; ?>
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td colspan="4" class="bor-b v-al-b" width="49%">
                                    <?= $invoice->company->certificate ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="" width="15%">
                                </td>
                                <td class="v-al-t text-c text-xs" width="13%">
                                    подпись
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td class="v-al-t text-c text-xs" width="19%">
                                    (ф.и.о)
                                </td>
                                <td style="" width="2%">
                                    &nbsp;
                                </td>
                                <td colspan="4" class="v-al-t text-c text-xs" width="49%">
                                    (реквизиты свидетельства о государственной регистрации индивидуального предпринимателя)
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="mar-t-10" style="font-size: 9px;">
        <tbody>
            <tr>
                <td class="fs-9" width="20%">
                    Основание передачи (сдачи) / получения (приемки)
                </td>
                <td class="fs-9 bor-b" width="78%">
                    <?= $model->getBasisName() ?>
                </td>
                <td class="fs-9 text-c" width="2%">
                    [8]
                </td>
            </tr>
            <tr>
                <td class="fs-9" width="20%"></td>
                <td class="fs-9" width="78%">(договор; доверенность и др.)</td>
                <td class="fs-9" width="2%"></td>
            </tr>
            <tr>
                <td class="fs-9" width="20%">
                    Данные о транспортировке и грузе
                </td>
                <td class="fs-9 bor-b" width="78%">
                    <?= ($model->waybill_date && $model->waybill_number) ? 'транспортная накладная № ' . $model->waybill_number . ' от ' .
                        date_create_from_format('Y-m-d', $model->waybill_date)->format('d.m.Y') : ''; ?>
                </td>
                <td class="fs-9 text-c" width="2%">
                    [9]
                </td>
            </tr>
            <tr>
                <td class="fs-9" width="20%"></td>
                <td class="fs-9" width="78%">
                    (транспортная накладная, поручение экспедитору,
                    экспедиторская / складская
                    расписка и др. / масса нетто/брутто груза, если не
                    приведены ссылки на
                    транспортные документы, содержащие эти сведения)
                </td>
                <td class="fs-9" width="2%"></td>
            </tr>
        </tbody>
    </table>
    <table class="mar-t-10" style="font-size: 9px;">
        <tbody>
            <tr>
                <td class="bor2-r v-al-t" width="50%">
                    <table style="font-size: 9px;">
                        <tbody>
                            <tr>
                                <td colspan="6" class="pad-t-0 pad-b-0">
                                    Товар (груз) передал / услуги, результаты
                                    работ, права сдал
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="bor-b v-al-b">
                                    <?= $isOut ? $chiefPosition : ''; ?>
                                </td>
                                <td width="1%"></td>
                                <td width="29%" class="bor-b v-al-b pad-0" height="30px">
                                    <?php if ($isOut)
                                        echo Html::tag('img', null, [
                                            'src' => $signatureLink,
                                            'class' => 'signature_image',
                                            'style' =>
                                                'margin-bottom:' . ($defaultSignatureHeight - $signatureHeight) . 'px;'
                                                . 'display: ' . ($addStamp ? 'block;' : 'none;'),
                                        ])
                                    ?>
                                </td>
                                <td width="1%"></td>
                                <td width="35%" class="bor-b v-al-b">
                                    <?= $isOut ? $chief : '' ?>
                                </td>
                                <td width="5%" class="v-al-b text-c">
                                    [10]
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-0 text-c">(должность)</td>
                                <td width="1%"></td>
                                <td width="29%" class="pad-t-0 text-c">подпись</td>
                                <td width="1%"></td>
                                <td width="35%" class="pad-t-0 text-c">(ф.и.о)</td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-10">
                                    Дата отгрузки, передачи (сдачи)
                                </td>
                                <td width="1%"></td>
                                <td width="65%" colspan="3" class="bor-b pad-t-10">
                                    <?= $documentDate; ?>
                                </td>
                                <td width="5%" class="text-c pad-t-10">[11]</td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-10 pad-b-5">
                                    Иные сведения об отгрузке, передаче
                                </td>
                            </tr>
                            <tr>
                                <td width="95%" colspan="5" class="bor-b"></td>
                                <td width="5%" class="text-c">[12]</td>
                            </tr>
                            <tr>
                                <td width="95%" class="text-c pad-t-0" colspan="5">
                                    (ссылки на неотъемлемые приложения, сопутствующие документы, иные документы и т.п.)
                                </td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-0 pad-b-0">
                                    Ответственный за правильность оформления факта хозяйственной жизни
                                </td>
                            </tr>

                            <tr>
                                <td width="29%" class="bor-b v-al-b">
                                    <?= $isOut ? $chiefPosition : ''; ?>
                                </td>
                                <td width="1%"></td>
                                <td width="29%" class="bor-b v-al-b pad-0" height="30px">
                                    <?php if ($isOut)
                                        echo Html::tag('img', null, [
                                            'src' => $signatureLink,
                                            'class' => 'signature_image',
                                            'style' =>
                                                'margin-bottom:' . ($defaultSignatureHeight - $signatureHeight) . 'px;'
                                                . 'display: ' . ($addStamp ? 'block;' : 'none;'),
                                        ])
                                    ?>
                                </td>
                                <td width="1%"></td>
                                <td width="35%" class="bor-b v-al-b">
                                    <?= $isOut ? $chief : ''; ?>
                                </td>
                                <td width="5%" class="v-al-b text-c">
                                    [13]
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-0 text-c">(должность)</td>
                                <td width="1%"></td>
                                <td width="29%" class="pad-t-0 text-c">подпись</td>
                                <td width="1%"></td>
                                <td width="35%" class="pad-t-0 text-c">(ф.и.о)</td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-5 pad-b-5">
                                    Наименование экономического субъекта – составителя документа (в т.ч. комиссионера / агента)
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="bor-b">
                                    <?php if ($isOut) : ?>
                                        <?= $invoice->company_name_short ?>,
                                        ИНН/КПП
                                        <?= $invoice->company_inn ?>/<?= $invoice->company_kpp; ?>
                                    <?php else : ?>
                                        <?= $invoice->contractor_name_short ?>
                                        ИНН/КПП
                                        <?= $invoice->contractor_inn ?>/<?= $invoice->contractor_kpp ?>
                                    <?php endif ?>
                                </td>
                                <td width="5%" class="text-c">[14]</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="center">
                                    (может не заполняться при проставлении печати в М.П., может быть указан ИНН / КПП)
                                </td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-15 text-c">
                                    M.П.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td class="bor2-l v-al-t pad-l-5" width="50%">
                    <table style="font-size: 9px;">
                        <tbody>
                            <tr>
                                <td colspan="6" class="pad-t-0 pad-b-0">
                                    Товар (груз) получил / услуги, результаты
                                    работ, права принял
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="bor-b v-al-b"></td>
                                <td width="1%"></td>
                                <td width="29%" class="bor-b v-al-b" height="30px"></td>
                                <td width="1%"></td>
                                <td width="35%" class="bor-b v-al-b"></td>
                                <td width="5%" class="text-c">
                                    [15]
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-0 text-c">(должность)</td>
                                <td width="1%"></td>
                                <td width="29%" class="pad-t-0 text-c">подпись</td>
                                <td width="1%"></td>
                                <td width="35%" class="pad-t-0 text-c">(ф.и.о)</td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-10">
                                    Дата получения (приемки)
                                </td>
                                <td width="1%"></td>
                                <td width="65%" colspan="3" class="bor-b pad-t-10"></td>
                                <td width="5%" class="pad-t-10 text-c">
                                    [16]
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-10 pad-b-5">
                                    Иные сведения об отгрузке, передаче
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="bor-b"></td>
                                <td width="5%" class="text-c">
                                    [17]
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="pad-t-0 text-c">
                                    (ссылки на неотъемлемые приложения, сопутствующие документы, иные документы и т.п.)
                                </td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-0 pad-b-0">
                                    Ответственный за правильность оформления факта хозяйственной жизни
                                </td>
                            </tr>

                            <tr>
                                <td width="29%" class="bor-b v-al-b"></td>
                                <td width="1%"></td>
                                <td width="29%" class="bor-b v-al-b pad-0" height="30px"></td>
                                <td width="1%"></td>
                                <td width="35%" class="bor-b v-al-b"></td>
                                <td width="5%" class="text-c v-al-b">
                                    [18]
                                </td>
                            </tr>
                            <tr>
                                <td width="29%" class="pad-t-0 text-c">(должность)</td>
                                <td width="1%"></td>
                                <td width="29%" class="pad-t-0 text-c">подпись</td>
                                <td width="1%"></td>
                                <td width="35%" class="pad-t-0 text-c">(ф.и.о)</td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-5 pad-b-5">
                                    Наименование экономического субъекта – составителя документа (в т.ч. комиссионера / агента)
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="bor-b">
                                    <?php if ($isOut) : ?>
                                        <?= $invoice->contractor_name_short .
                                        (($invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
                                            ', ИНН/КПП ' . $invoice->contractor_inn . '/' . $invoice->contractor_kpp : '') ?>
                                    <?php else : ?>
                                        <?= $invoice->company_name_short ?>,
                                        ИНН/КПП
                                        <?= $invoice->company_inn ?>/<?= $invoice->company_kpp; ?>
                                    <?php endif ?>
                                </td>
                                <td width="5%" class="text-c">
                                    [19]
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" align="center">
                                    (может не заполняться при проставлении печати в М.П., может быть указан ИНН / КПП)
                                </td>
                                <td width="5%" align="center"></td>
                            </tr>
                            <tr>
                                <td colspan="6" class="pad-t-15 text-c">
                                    M.П.
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
