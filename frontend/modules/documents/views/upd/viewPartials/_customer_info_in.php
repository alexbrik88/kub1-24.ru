<?php

use common\components\date\DateHelper;
use common\models\document\UpdPaymentDocument;
use frontend\modules\documents\components\Message;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\product\Product;
use kartik\select2\Select2;
use common\models\Contractor;

/* @var \yii\web\View $this */
/* @var \common\models\document\Upd $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$basisDocumentDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$consignorArray = $model->consignorArray;

$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = 'Счет&' . $model->invoice->fullNumber . '&' . $model->invoice->document_date . '&';
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');

$paymentDocumentsArray = $model->paymentDocuments;
$paymentDocuments = [];
if ($paymentDocumentsArray) {
    foreach ($paymentDocumentsArray as $doc) {
        $date = date('d.m.Y', strtotime($doc->payment_document_date));
        $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
    }
} else {
    $paymentDocumentsArray[] = new UpdPaymentDocument;
}

$hasService = $model->getOrders()->joinWith('product product')
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])->exists();
?>

<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="actions">
            <button type="submit" class="btn-save btn darkblue btn-sm" style="color: #FFF;" title="Сохранить">
                <span class="ico-Save-smart-pls"></span>
            </button>
            <?= Html::a('<span class="ico-Cancel-smart-pls"></span>', $cancelUrl = Url::previous('lastPage'), [
                'class' => 'btn darkblue btn-sm',
                'style' => "color: #FFF;",
                'title' => "Отменить"
            ]) ?>
        </div>
        <div class="caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'data-required' => 1,
                'class' => 'form-control ',
                'style' => 'max-width: 60px; display:inline-block; margin-right: 10px;',
                'value' => $model->getDocumentNumber(),
            ]) ?>
            от
            <span class="input-icon input-calendar  margin-top-15">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'documentDate', [
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                ]); ?>
            </span>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            Грузоотправитель:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <div class="">
                                <?= Select2::widget([
                                    'model' => $model,
                                    'attribute' => 'consignor_id',
                                    'data' => ['add-modal-contractor' => '[ + Добавить грузоотправителя ]'] + $consignorArray,
                                    'options' => [
                                        'value' => $model->consignor_id ?: '',
                                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                                    ],
                                    'pluginOptions' => [
                                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 20); }'),
                                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <span>
                                <?= Html::a($model->invoice->contractor_name_short, [
                                    '/contractor/view',
                                    'type' => $model->invoice->contractor->type,
                                    'id' => $model->invoice->contractor->id,
                                ]) ?>
                            </span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr class="box-tr-pac-list">
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            Транспортная накладная:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <?= Html::activeTextInput($model, 'waybill_number', [
                                'maxlength' => true,
                                'data-required' => 1,
                                'class' => 'form-control  ',
                                'placeholder' => 'номер',
                                'style' => 'width:120px; display: inline-block;',
                            ]); ?>
                            от
                            <div class="input-icon input-calendar ">
                                <i class="fa fa-calendar"></i>
                                <?= Html::activeTextInput($model, 'waybillDate', [
                                    'id' => 'under-date',
                                    'class' => 'form-control date-picker',
                                    'size' => 16,
                                    'data-date-viewmode' => 'years',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            К платежно-расчётному документу:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <div class="">
                                <div id="payment-inputs-block">
                                    <?php if ($paymentDocumentsArray) : ?>
                                        <?php foreach ($paymentDocumentsArray as $doc) : ?>
                                            <div class="payment-row" style="margin-bottom: 2px;">
                                                <?= Html::textInput('payment[number][]', $doc->payment_document_number, [
                                                    'class' => 'form-control',
                                                    'style' => 'width:120px; display: inline-block;',
                                                    // 'placeholder' => '№',
                                                ]) ?>
                                                от
                                                <div class="input-icon input-calendar ">
                                                    <i class="fa fa-calendar"></i>
                                                    <?= Html::textInput(
                                                        'payment[date][]',
                                                        $doc->payment_document_date ? date('d.m.Y', strtotime($doc->payment_document_date)) : '',
                                                        [
                                                            'class' => 'form-control date-picker',
                                                            'size' => 16,
                                                            'data-date-viewmode' => 'years',
                                                            'style' => 'width:120px; display: inline-block;',
                                                        ]
                                                    ) ?>
                                                </div>
                                                <span class="close del-payment-doc"
                                                      style="float: none; font-size: inherit; padding: 3px; line-height: 5px;">×</span>
                                            </div>
                                        <?php endforeach ?>
                                    <?php else : ?>

                                    <?php endif ?>
                                </div>
                                <div>
                                    <div class="portlet pull-left control-panel button-width-table">
                                        <div id="add-payment-doc" class="btn-group pull-left"
                                             style="display: inline-block;">
                                            <span class="btn yellow btn-add-line-table">
                                                <i class="pull-left fa icon fa-plus-circle"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            <?= $model->getAttributeLabel('state_contract') ?>:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <div class="">
                                <?= Html::activeTextInput($model, 'state_contract', [
                                    'maxlength' => true,
                                    'class' => 'form-control',
                                    'style' => 'width: 100%;',
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            Основание:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <div class="">
                                <?php
                                Pjax::begin([
                                    'id' => 'agreement-pjax-container',
                                    'enablePushState' => false,
                                    'linkSelector' => false,
                                ]);

                                echo Select2::widget([
                                    'model' => $model,
                                    'attribute' => 'agreement',
                                    'data' => $agreementDropDownList,
                                    'hideSearch' => true,
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'placeholder' => '',
                                    ],
                                ]);

                                Pjax::end();
                                ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-sm-6 upd-info-label">
                            Грузополучатель:
                        </div>
                        <div class="col-sm-6 upd-info-value">
                            <div class="" style="margin-bottom: 8px;">
                                <?= Select2::widget([
                                    'model' => $model,
                                    'attribute' => 'consignee_id',
                                    'data' => ['add-modal-contractor' => '[ + Добавить грузополучателя ]'] + $consignorArray,
                                    'options' => [
                                        'value' => $model->consignee_id ?: '',
                                        'options' => Contractor::getAllContractorSelect2Options([Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER])
                                    ],
                                    'pluginOptions' => [
                                        'templateResult' => new \yii\web\JsExpression('function(data, container) { return invoiceTemplateResult(data, container, 20); }'),
                                        'matcher' => new \yii\web\JsExpression('function(params, data) { return invoiceContractorMatcher(params, data); }'),
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>

        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <div style="margin-bottom: 5px;">
                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                            'model' => $model,
                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
