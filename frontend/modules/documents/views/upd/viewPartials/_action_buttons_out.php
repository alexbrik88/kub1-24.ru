<?php

use yii\helpers\Html;
use common\models\document\status\UpdStatus;
use \yii\bootstrap\Dropdown;

/** @var \common\models\document\PackingList $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$canUpdate = $model->status_out_id != UpdStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);
?>

<div class="row action-buttons view-action-buttons">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if (true || $canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (true || $canUpdate) {
            echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                'target' => '_blank',
            ]);
            echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'target' => '_blank',
                'title' => 'Печать',
            ]);
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span class="dropup">
        <?= Html::a('Скачать', '#', [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
            'data-toggle' => 'dropdown'
        ]); ?>
        <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
            'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
            'data-toggle' => 'dropdown'
        ]); ?>
        <?= Dropdown::widget([
            'options' => [
                'style' => '',
                'class' => 'dropdown-menu-mini'
            ],
            'items' => [
                [
                    'label' => '<span style="display: inline-block;">PDF</span> файл',
                    'encode' => false,
                    'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                    'linkOptions' => [
                        'target' => '_blank',
                    ]
                ],
                [
                    'label' => '<span style="display: inline-block;">Word</span> файл',
                    'encode' => false,
                    'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                    'linkOptions' => [
                        'target' => '_blank',
                        'class' => 'get-word-link',
                    ]
                ],
            ],
        ]); ?>
    </span>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) {
            echo Html::a(UpdStatus::findOne(UpdStatus::STATUS_SEND)->name,
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => UpdStatus::STATUS_SEND,
                        ],
                    ],
                ]);
            echo Html::a('<i class="fa fa-paper-plane-o fa-2x"></i>',
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Передан',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => UpdStatus::STATUS_SEND,
                        ],
                    ],
                ]);
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate && Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) {
            echo Html::a(UpdStatus::findOne(UpdStatus::STATUS_RECEIVED)->name,
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => UpdStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]);
            echo Html::a('<i class="fa fa-pencil-square-o fa-2x"></i>',
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Подписан',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => UpdStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]);
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                    href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal"
                    href="#delete-confirm" title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>

<?php if ($canUpdate): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>