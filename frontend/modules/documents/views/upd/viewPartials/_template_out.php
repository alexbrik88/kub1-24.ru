<?php

use common\components\TextHelper;
use common\models\document\OrderUpd;
use common\models\product\Product;
use yii\helpers\Html;

/** @var \common\models\document\PackingList $model */
/** @var \common\models\document\OrderPackingList $order */
/** @var [] $product */

$nds = TextHelper::invoiceMoneyFormat($order->amountNds, 2);

$product = $order->product;
$isGoods = ($product->production_type == Product::PRODUCTION_TYPE_GOODS);

$params = $order->getOrderUpd($model->id);
if (!$params) {
    $params = new OrderUpd([
        'empty_unit_code' => 0,
        'empty_unit_name' => 0,
        'empty_product_code' => 0,
        'empty_box_type' => 0,
    ]);
}
?>

<?php if (isset($order)) : ?>
    <tr role="row" class="odd order">
        <td><span class="editable-field"><?= $key + 1; ?></span></td>
        <td><?= $order->product_title ?></td>
        <td><?= $product->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
        <td>
            <?php if ($isGoods) : ?>
                <?php $value = $product->productUnit ? $product->productUnit->name : Product::DEFAULT_VALUE; ?>
                <span class="editable-field"><?= $params->empty_unit_name ? Product::DEFAULT_VALUE : $value ?></span>
                <span class="input-editable-field hide">
                    <?= Html::dropDownList("orderParams[{$order->id}][empty_unit_name]", $params->empty_unit_name, array_unique([
                        1 => Product::DEFAULT_VALUE,
                        0 => $value,
                    ]), ['class' => 'invoise-facture-order-params']) ?>
                </span>
            <?php else : ?>
                <?= Product::DEFAULT_VALUE ?>
            <?php endif ?>
        </td>
        <td>
            <?php if ($isGoods) : ?>
                <?php $value = $product->code ? $product->code : Product::DEFAULT_VALUE; ?>
                <span class="editable-field"><?= $params->empty_product_code ? Product::DEFAULT_VALUE : $value ?></span>
                <span class="input-editable-field hide">
                    <?= Html::dropDownList("orderParams[{$order->id}][empty_product_code]", $params->empty_product_code, array_unique([
                        1 => Product::DEFAULT_VALUE,
                        0 => $value,
                    ]), ['class' => 'invoise-facture-order-params']) ?>
                </span>
            <?php else : ?>
                <?= Product::DEFAULT_VALUE ?>
            <?php endif ?>
        </td>
        <td>
            <?php if ($isGoods) : ?>
                <?php $value = $product->productUnit ? $product->productUnit->code_okei : Product::DEFAULT_VALUE; ?>
                <span class="editable-field"><?= $params->empty_unit_code ? Product::DEFAULT_VALUE : $value ?></span>
                <span class="input-editable-field hide">
                    <?= Html::dropDownList("orderParams[{$order->id}][empty_unit_code]", $params->empty_unit_code, array_unique([
                        1 => Product::DEFAULT_VALUE,
                        0 => $value,
                    ]), ['class' => 'invoise-facture-order-params']) ?>
                </span>
            <?php else : ?>
                <?= Product::DEFAULT_VALUE ?>
            <?php endif ?>
        </td>
        <td>
            <?php if ($isGoods) : ?>
                <?php $value = $order->box_type ? $order->box_type : Product::DEFAULT_VALUE; ?>
                <span class="editable-field"><?= $params->empty_box_type ? Product::DEFAULT_VALUE : $value ?></span>
                <span class="input-editable-field hide">
                    <?= Html::dropDownList("orderParams[{$order->id}][empty_box_type]", $params->empty_box_type, array_unique([
                        1 => Product::DEFAULT_VALUE,
                        0 => $value,
                    ]), ['class' => 'invoise-facture-order-params']) ?>
                </span>
            <?php else : ?>
                <?= Product::DEFAULT_VALUE ?>
            <?php endif ?>
        </td>
        <td><?= $isGoods && $order->count_in_place ? $order->count_in_place : Product::DEFAULT_VALUE; ?></td>
        <td><?= $isGoods && $order->place_count ? $order->place_count : Product::DEFAULT_VALUE; ?></td>
        <td><?= $isGoods && $order->mass_gross ? $order->mass_gross : Product::DEFAULT_VALUE; ?></td>
        <td><?= $isGoods && $order->quantity ? $order->quantity : Product::DEFAULT_VALUE; ?></td>
        <td class="text-right"><?= $isGoods ? TextHelper::invoiceMoneyFormat($order->priceNoNds, 2) : Product::DEFAULT_VALUE; ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountNoNds, 2); ?></td>
        <td><?= $order->saleTaxRate->name; ?></td>
        <td class="text-right"><?= $nds; ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, 2); ?></td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td><span class="editable-field"><?= $key + 1; ?></span><span
                    class="icon-close input-editable-field delete-row hide"></span>
        </td>
        <td><?=  \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?=  \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td><?= common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-right"></td>
    </tr>
<?php endif; ?>
