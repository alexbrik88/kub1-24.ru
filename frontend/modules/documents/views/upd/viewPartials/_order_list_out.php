<?php

use common\components\TextHelper;
use common\models\address\Country;
use yii\bootstrap\Html;

/* @var $model \common\models\document\Upd */
/* @var $company \common\models\Company */
/* @var $this \yii\web\View */

$tableClass = isset($tableClass) ? $tableClass : 'table table-striped table-bordered table-hover customers_table';
$countryList = Country::find()->select('name_short')->indexBy('id')->column();
?>

<table class="<?= $tableClass; ?>">
    <thead>
    <tr class="heading" role="row">
        <th width="10%">Наименование</th>
        <th width="5%">Код<br>вида<br>товара</th>
        <th width="6%">Ед.изм.</th>
        <th width="5%">Код</th>
        <th width="5%">Код по ОКЕИ</th>
        <th width="3%">Вид упаковки</th>
        <th width="3%">Кол-во в одном месте</th>
        <th width="3%">Кол-во мест, штук</th>
        <th width="3%">Масса брутто</th>
        <th width="3%">Кол-во (объем)</th>
        <th width="5%">Цена</th>
        <th width="5%">Сумма без НДС</th>
        <th width="5%">Ставка НДС</th>
        <th width="5%">Сумма НДС</th>
        <th width="8%">Сумма с учётом НДС</th>
        <th width="5%">Страна происх. то&shy;ва&shy;ра</th>
        <th width="5%">
            Регистрацион&shy;ный но&shy;мер та&shy;мо&shy;жен&shy;ной де&shy;кла&shy;ра&shy;ции
        </th>
        <th width="3%"></th>
    </tr>
    </thead>
    <tbody id="tbody" class="document-orders-rows">
    <?php foreach ($model->ownOrders as $key => $order) : ?>
        <?= $this->render('_order_list_out_row', [
            'key' => $key,
            'orderUpd' => $order,
            'model' => $model,
            'company' => $company,
            'precision' => $precision,
            'countryList' => $countryList,
        ]); ?>
    <?php endforeach; ?>
    </tbody>
</table>
