<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?= Html::beginForm('', 'post', [
    'id' => 'select-invoice-product-form',
]); ?>

    <?= common\components\grid\GridView::widget([
        'id' => 'invoice-available-product-list',
        'dataProvider' => $dataProvider,
        'emptyText' => 'Нет доступных для выбора позиций',
        'tableOptions' => [
            'class' => 'table table-striped table-bordered table-hover customers_table overfl_text_hid',
        ],
        'headerRowOptions' => [
            'class' => 'heading',
        ],
        'options' => [
            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
        ],
        'rowOptions' => [
            'role' => 'row',
        ],
        'layout' => "{items}",
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'name' => 'orders',
                'cssClass' => 'add-product-checkbox',
                //'header' => '',
                'headerOptions' => [
                    'width' => '50px',
                ],
                'checkboxOptions' => function ($model) {
                    return [
                        'value' => $model->id,

                    ];
                }
            ],
            [
                'attribute' => 'product_title',
                'label' => 'Наименование',
            ],
        ],
    ]); ?>

<?= Html::endForm(); ?>

<?php
if ($dataProvider->totalCount) {
    echo Html::button('Добавить отмеченные', [
        'id' => 'add-selected-button',
        'class' => 'btn darkblue pull-right btn-w button-add-marked',
        'data-dismiss' => 'modal',
        'data-url' => Url::to(['add-product', 'type' => $model->type, 'id' => $model->id]),
    ]);
}
?>

<?php $this->registerJs('
$("#select-invoice-product-form input:not(.md-check, .md-radiobtn)").uniform();
$(document).on("change", "#select-invoice-product-form input", function() {
    $.uniform.update("#select-invoice-product-form input:not(.md-check, .md-radiobtn)");
})
') ?>