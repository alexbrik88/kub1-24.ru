<?php

use common\models\Company;
use common\models\document\Invoice;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $fixedContractor boolean */
/* @var $isAuto boolean */

$cancelUrl = Url::previous('lastPage');
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]); ?>
            <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                'class' => 'btn darkblue widthe-100 hidden-lg create-invoice',
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <?= Html::a('Отменить', $cancelUrl, [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]) ?>
            <?= Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Отменить',
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>
