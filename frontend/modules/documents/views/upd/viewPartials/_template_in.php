<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 20.01.2017
 * Time: 3:12
 */
use common\components\TextHelper;
use common\models\product\Product;
$plus = 0;
?>
<?php if (isset($order)): ?>
<tr role="row" class="odd order">
    <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test',null,$result, ['class'=>'dropdownlist form-control', 'prompt'=>'']); ?></td>
    <td><span
            class="editable-field"><?= $order->quantity; ?></span><input
            class="input-editable-field quantity hide form-control" type="number"
            value="<?= $order->quantity ?>" min="1"
            max="<?= $order ? $order->getAvailableQuantity() : 1?>"
            id="<?= $order->order_id ?>"
            name="<?= $order ? 'OrderPackingList[' . $order->order_id . '][quantity]' : ''?>"
            style="padding: 6px 6px;">
    </td>
    <td><?= $order->product->productUnit ? $order->product->productUnit->name : Product::DEFAULT_VALUE; ?></td>
    <?php if ($hasNds) : ?>
        <td><?= $order->order->purchaseTaxRate->name; ?></td>
    <?php endif; ?>
    <input class="status" type="hidden" name="<?= 'OrderPackingList['. $order->order_id  .'][status]' ?>" value="active">
    <td class="text-right">
        <?= TextHelper::invoiceMoneyFormat($order->priceWithNds, 2); ?>
    </td>
    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, 2); ?></td>
    <td class="text-center"><span class="editable-field"><?= $key + 1; ?></span><span
                class="icon-close input-editable-field hide delete-row"></span>
    </td>
</tr>
<?php else : ?>
<tr role="row" class="odd order">
    <td><?= \common\components\helpers\Html::dropDownList('test',null,$result, ['class'=>'dropdownlist form-control', 'prompt'=>'']); ?></td>
    <td></td>
    <td></td>
    <?php if ($hasNds) : ?>
        <th width="5%"></th>
    <?php endif; ?>
    <input class="status" type="hidden" name="" value="active">
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-center"><span class="editable-field"><?= $key + 1; ?></span><span
                class="icon-close input-editable-field hide delete-row"></span>
    </td>
</tr>
<?php endif; ?>