<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var \common\models\document\Upd $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            $.pjax.reload("#upd-template-pjax");
        });
    });
');
?>

<div class="doc-view-additional" style="">
    <?php if ($model->type == \frontend\models\Documents::IO_TYPE_OUT): ?>
        <div class="status-row">
            <div class="status-date">
                <div class="btn darkblue text-white" title="Дата изменения статуса">
                    <?= date("d.m.Y", $model->status_out_updated_at); ?>
                </div>
            </div>
            <div class="btn darkblue w_100 text-white" title="Статус">
                <i class="icon pull-left <?= $model->statusOut->getIcon(); ?>"></i>
                <?= $model->statusOut->name; ?>
            </div>
        </div>
    <?php else: ?>
        <?php \yii\widgets\Pjax::begin([
            'id' => 'is-original-pjax-container',
            'enablePushState' => false,
            'linkSelector' => '.is-original-button',
        ]); ?>
        <div class="status-row">
            <div class="status-date">
                <div class="btn darkblue text-white" title="Дата изменения статуса">
                    <?=  date("d.m.Y", $model->is_original_updated_at ? $model->is_original_updated_at : $model->created_at); ?>
                </div>
            </div>
            <div class="btn darkblue w_100 text-white" title="Статус">
                <?php if ($model->is_original) : ?>
                    <span class="icon pull-left icon-original-document"></span>Оригинал
                <?php else : ?>
                    <span class="icon pull-left fa fa-copy"></span>Скан
                <?php endif; ?>
            </div>
        </div>
        <?php \yii\widgets\Pjax::end(); ?>
    <?php endif; ?>

    <?php if ($model->getInvoices()->count() == 1): ?>
        <?= Html::a('<i class="icon pull-left '.$model->invoice->invoiceStatus->getIcon().'"></i> СЧЕТ', [
            'invoice/view',
            'type' => $model->type,
            'id' => $model->invoice->id,
            'contractorId' => $contractorId,
        ], [
            'class' => 'btn yellow w_100',
            'style' => 'padding: 7px 4px;'
        ]) ?>
    <?php else : ?>
        <div class="dropdown">
            <div data-toggle="dropdown" class="btn yellow dropdown-toggle w-100" title="Счет">
                <i class="pull-left icon icon-doc"></i>
                СЧЕТ
            </div>
            <ul class="dropdown-menu documents-dropdown" style="right: 20px;left: auto;">
                <?php foreach ($model->invoices as $invoice): ?>
                    <li>
                        <a href="<?= Url::to(['invoice/view', 'type' => $model->type, 'id' => $invoice->id, 'contractorId' => $contractorId,]) ?>"
                           class="dropdown-item " style="position: relative;">
                            <span class="dropdown-span-left">
                                № <?= $invoice->document_number; ?>
                                от <?= DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            </span>
                            <span class="dropdown-span-right">
                                <i class="fa fa-rub"></i>
                                <?= TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) ?>
                            </span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <div class="portlet customer-info" style="margin: 10px 0 0;">
        <div class="portlet-body no_mrg_bottom">
            <table class="table no_mrg_bottom">
                <tr>
                    <td>
                        <span class="customer-characteristic">
                            <?= $message->get(Message::CONTRACTOR); ?>:
                        </span>
                        <span>
                            <?= Html::a($model->invoice->contractor_name_short, [
                                '/contractor/view',
                                'type' => $model->invoice->contractor->type,
                                'id' => $model->invoice->contractor->id,
                            ]) ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="customer-characteristic">
                            Основание:
                        </span>
                        <span>
                            <?= $model->getBasisName() ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="color:#333; display:block; float:left; margin:5px 8px 0 0; font-weight: bold;">
                            Печать и подпись:
                        </div>
                        <label class="rounded-switch" for="add_stamp" style="margin: 4px 0 0;">
                            <?= Html::checkbox('add_stamp', $model->add_stamp, [
                                'id' => 'add_stamp',
                                'class' => 'switch'
                            ]) ?>
                            <span class="sliderr no-gray yes-yellow round"></span>
                        </label>
                        <span class="tooltip-hover ico-question valign-middle"
                              data-tooltip-content="#tooltip_add_stamp"
                              style="vertical-align: top;">
                        </span>
                        <div class="hidden">
                            <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
                                Добавить в УПД печать и подпись
                                <br>
                                при отправке по e-mail и
                                <br>
                                при скачивании в PDF
                            </span>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td>
                        <div style="margin-bottom: 5px;">
                            <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                'model' => $model,
                                'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                            ]); ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>