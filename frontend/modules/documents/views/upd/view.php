<?php

use common\components\helpers\ArrayHelper;
use common\models\document\Order;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$company = Yii::$app->user->identity->company;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>
<div class="page-content-in out-document" style="padding-bottom: 30px;">
    <div class="pad3">
        <?php if ($backUrl !== null) : ?>
            <?= Html::a('Назад к списку', $backUrl, [
                'class' => 'back-to-customers',
            ]) ?>
        <?php endif; ?>
    </div>

    <div style="margin-top: 3px; padding: 0 8px 8px; border: 1px solid #4276a4; position: relative;">
        <div class="portlet-title">
            <div class="actions">
                <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                    'model' => $model,
                ]); ?>

                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                    <?= Html::a('<i class="icon-pencil"></i>', [
                        'update',
                        'type' => $model->type,
                        'id' => $model->id,
                    ], [
                        'class' => 'btn darkblue btn-sm',
                        'title' => 'Редактировать',
                        'style' => 'padding-bottom: 4px;'
                    ]) ?>
                <?php endif; ?>
            </div>
            <div class="ff-pdf-sans" style="margin: 10px 0 15px; font-size: 21px; font-weight: bold;">
                <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
                №
                <?= $model->fullNumber; ?>
            </div>
        </div>

        <div style="max-width: 100%; overflow-x: auto; padding-bottom: 5px;">
            <?php Pjax::begin([
                'id' => 'upd-template-pjax',
                'enablePushState' => false,
                'linkSelector' => false,
            ]); ?>

            <?= $this->render('template', [
                'model' => $model,
                'addStamp' => true,
            ]); ?>

            <?php Pjax::end(); ?>
        </div>

        <?= $this->render('viewPartials/_additional_info', [
                'model' => $model,
                'useContractor' => $useContractor,
                'contractorId' => $contractorId,
                'message' => $message
        ]); ?>
    </div>

    <div id="buttons-bar-fixed">
        <?= $this->render('viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
        ]); ?>
    </div>
</div>

<?= \frontend\widgets\ConfirmModalWidget::widget([
    'options' => [
        'id' => 'delete-confirm',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id, 'contractorId' => $contractorId,]),
    'confirmParams' => [],
    'message' => 'Вы уверены, что хотите удалить УПД?',
]); ?>
