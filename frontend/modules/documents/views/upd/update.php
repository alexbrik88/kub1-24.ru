<?php

use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\components\helpers\ArrayHelper;
use common\models\document\Order;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$company = Yii::$app->user->identity->company;
$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}
$invoiceOrderCount = Order::find()
    ->andWhere(['in', 'invoice_id', ArrayHelper::getColumn($model->invoices, 'id')])
    ->count();
$hideAddBtn = $invoiceOrderCount === count($model->ownOrders);
$precision = $model->invoice->price_precision;
?>

<div class="page-content-in">
    <?php if ($backUrl !== null) {
        echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
            'class' => 'back-to-customers',
        ]);
    } ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'edit-upd',
        'class' => 'form-horizontal',
        'novalidate' => 'novalidate',
        'enctype' => 'multipart/form-data',
    ]); ?>

        <div class="row">
            <div class="customer_info_wrapper col-lg-7 col-w-lg-8 col-md-7 widthe-i_md" style="max-width: 720px;">
                <?php echo $this->render('viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'message' => $message,
                    'dateFormatted' => $dateFormatted,
                ]);
                ?>
            </div>

            <?= $this->render('viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'useContractor' => $useContractor,
                'contractorId' => $contractorId,
            ]); ?>
        </div>
        <div class="portlet wide_table overflow-x">
            <?= $this->render('viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'company' => $company,
                'precision' => $precision,
            ]); ?>
        </div>
        <div class="portlet pull-left control-panel button-width-table" style="display: inline-block; width: 30px;">
            <div class="btn-group pull-right" style="display: inline-block;">
                <?php Modal::begin([
                    'header' => Html::tag('h1', 'Выбрать позицию из списка'),
                    'toggleButton' => [
                        'id' => 'plusbtn',
                        'tag' => 'span',
                        'label' => '<i class="pull-left fa icon fa-plus-circle"></i>',
                        'class' => 'btn yellow btn-add-line-table' . ($hideAddBtn ? ' hide' : ''),
                        'data-maxrows' => $invoiceOrderCount,
                        'data-url' => Url::to(['select-product', 'type' => $model->type, 'id' => $model->id]),
                    ],
                ]); ?>
                <div id="available-product-list"></div>
                <?php Modal::end(); ?>
            </div>
        </div>

        <div class="portlet pull-right">
            <?= $this->render('viewPartials/_summary_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'company' => $company,
            ]); ?>
        </div>

        <?= $this->render('viewPartials/_form_buttons', [
            'model' => $model,
            'useContractor' => $useContractor,
            'contractorId' => $contractorId,
        ]); ?>

    <?= Html::endForm(); ?>
</div>

<?= Html::hiddenInput(null, null, ['id' => 'adding-contractor-from-input']); ?>
<div class="modal fade t-p-f modal_scroll_center mobile-modal" id="add-new" tabindex="-1" role="modal"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body" id="block-modal-new-product-form">

            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]); ?>

    <?php Pjax::begin([
        'id' => 'agreement-form-container',
        'enablePushState' => false,
        'linkSelector' => false,
    ]); ?>

    <?php Pjax::end(); ?>

<?php Modal::end(); ?>

<?php
$this->registerJs('
var checkPlusbtn = function() {
    if ($("tbody.document-orders-rows tr").length < $("#plusbtn").data("maxrows")) {
        $("#plusbtn").removeClass("hide");
    } else {
        $("#plusbtn").addClass("hide");
    }
};
var checkRowNumbers = function() {
    $(".document-orders-rows").children("tr").each(function(i, row) {
        $(row).find("span.row-number").text(++i);
    });
};
var toggleFieldsToForm = function() {
    $(".input-editable-field").removeClass("hide");
    $(".editable-field").addClass("hide");
};
var checkServiceUnits = function() {
    if ($("#upd-show_service_units").is(":checked")) {
        $(".service_units_visible").removeClass("hide");
        $(".service_units_hidden").addClass("hide");
    } else {
        $(".service_units_visible").addClass("hide");
        $(".service_units_hidden").removeClass("hide");
    }
};
checkPlusbtn();
checkRowNumbers();

$(document).on("change", "#upd-show_service_units", function() {
    checkServiceUnits();
});
$(document).on("change", "#upd-agreement", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
            '/documents/agreement/create',
            'contractor_id' => $model->invoice->contractor_id,
            'type' => $model->type,
            'returnTo' => 'upd',
            'container' => 'agreement-select-container',
        ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
        })
        $("#agreement-modal-container").modal("show");
        $("#upd-agreement").val("").trigger("change");
    }
});
$("#agreement-pjax-container").on("pjax:complete", function() {
    if (window.AgreementValue) {
        $("#upd-agreement").val(window.AgreementValue).trigger("change");
    }
})

$(document).on("click", "tr .delete-row", function() {
    var row = $(this).closest("tr");
    var table = row.parent();
    row.remove();
    checkRowNumbers();
    checkPlusbtn();
});
$(document).on("click", "#plusbtn", function() {
    $.post($(this).data("url"), $("#edit-upd").serialize(), function(data) {
        $("#available-product-list").html(data);
    });
});
$(document).on("click", "#add-selected-button", function() {
    $.post($(this).data("url"), $("#select-invoice-product-form").serialize(), function(data) {
        console.log(data);
        if (data.rows.length) {
            $.each(data.rows, function(i, item) {
                $(".document-orders-rows").append(item);
            });
        }
        toggleFieldsToForm();
        checkPlusbtn();
        checkRowNumbers();
        $("#available-product-list").html("");
        $(".document-orders-rows input:not(.md-check, .md-radiobtn)").uniform();
    });
});
$(document).on("click", "#add-payment-doc", function() {
    $("#payment-inputs-block .payment-row:first").clone().appendTo("#payment-inputs-block").find("input[type=text]").val("");
    $("#payment-inputs-block .date-picker:last").datepicker({
        keyboardNavigation: false,
        forceParse: false,
        language: "ru",
        autoclose: true
    }).on("change.dp", dateChanged);

    function dateChanged(ev) {
        if (ev.bubbles == undefined) {
            var $input = $("[name=\'" + ev.currentTarget.name +"\']");
            if (ev.currentTarget.value == "") {
                if ($input.data("last-value") == null) {
                    $input.data("last-value", ev.currentTarget.defaultValue);
                }
                var $lastDate = $input.data("last-value");
                $input.datepicker("setDate", $lastDate);
            } else {
                $input.data("last-value", ev.currentTarget.value);
            }
        }
    };
});
$(document).on("click", ".del-payment-doc", function() {
    if ($("#payment-inputs-block .payment-row").length == 1) {
        $(this).closest(".payment-row").find("input[type=text]").val("");
    } else {
        $(this).closest(".payment-row").remove();
    }
    return false;
});
');
?>
