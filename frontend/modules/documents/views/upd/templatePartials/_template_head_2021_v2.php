<?php
use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Upd;
use common\models\product\Product;
use frontend\assets\UpdAsset;
use frontend\models\Documents;
use php_rutils\RUtils;
use yii\helpers\Html;

$isOut = $model->type == Documents::IO_TYPE_OUT;
$invoice = $model->invoice;
$documentDate = RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

if ($invoice->production_type) {
    $sender = $model->consignor ? $model->consignor->shortName.', '.$model->consignor->legal_address : 'он же';
    if ($model->consignee) {
        $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
            $model->consignee->legal_address : $model->consignee->actual_address;
        $recipient = $model->consignee->shortName . ', ' . $address;
    } else {
        if ($isOut) {
            $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
                $model->invoice->contractor_address_legal_full : $model->invoice->contractor->actual_address;
            $recipient = $invoice->contractor_name_short . ', ' . $address;
        } else {
            $address = $model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL ?
                $model->invoice->company_address_legal_full : $model->invoice->company->address_actual;
            $recipient = $invoice->company_name_short . ', ' . $address;
        }
    }
} else {
    $sender = $recipient = Product::DEFAULT_VALUE;
}

$paymentDocuments = [];
foreach ($model->paymentDocuments as $doc) {
    $date = date('d.m.Y', strtotime($doc->payment_document_date));
    $paymentDocuments[] = "№&nbsp;{$doc->payment_document_number}&nbsp;от&nbsp;{$date}";
}
$shippingDocuments = [];
foreach ([$model] as $doc) {
    $date = date('d.m.Y', strtotime($doc->document_date));
    $positionsCount = count($model->ownOrders);
    $positionNumber = ($positionsCount > 1) ? "1 - $positionsCount" : $positionsCount;
    $shippingDocuments[] = "№ п/п {$positionNumber} № {$model->fullNumber} от {$date}";
}
?>

<table>
    <tbody>
        <tr>
            <td>
                <div>
                    Счет-фактура
                    N <?= $model->fullNumber; ?>
                    от <?= $documentDate; ?> (1)
                </div>
                <div>Исправление
                    N________от_________(1а)
                </div>
            </td>
            <td class="text-r font-size-6">
                Приложение N 1
                <br>
                к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137
                <br>
                (в ред. Постановления Правительства РФ от 02.04.2021 № 534)
            </td>
        </tr>
    </tbody>
</table>
<table>
    <tbody>
        <tr>
            <td class="pad-l-4" style="width: 22%;">
                <span style="font-weight: bold">Продавец</span>
            </td>
            <td class="pad-l-4 bor-b" style="width: 78%">
                <?= $isOut ? $invoice->company_name_short : $invoice->contractor_name_short ?>
            </td>
            <td class="pad-l-4">(2)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="pad-l-4">
                Адрес
            </td>
            <td class="pad-l-4 bor-b">
                <?= $isOut ? $invoice->company_address_legal_full : $invoice->contractor_address_legal_full ?>
            </td>
            <td class="pad-l-4">(2а)
            </td>
        </tr>
        <tr>
            <td class="pad-l-4">
                ИНН/КПП продавца
            </td>
            <td class="pad-l-4 bor-b">
                <?= $isOut ? $invoice->company_inn.'/'.$invoice->company_kpp :
                    $invoice->contractor_inn.'/'.$invoice->contractor_kpp ?>
            </td>
            <td class="pad-l-4">(2б)
            </td>
        </tr>
        <tr>
            <td class="pad-l-4">
                Грузоотправитель и его адрес
            </td>
            <td class="pad-l-4 bor-b">
                <?= $sender ?>
            </td>
            <td class="pad-l-4">(3)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="">
                Грузополучатель и его адрес
            </td>
            <td class="bor-b">
                <?= $recipient ?>
            </td>
            <td class="">(4)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="right2_line_first">
                К платежно-расчетному документу
            </td>
            <td class="bor-b">
                <?= $paymentDocuments ? implode(', ', $paymentDocuments) : Product::DEFAULT_VALUE ?>
            </td>
            <td class="right2_line_last">(5)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="right2_line_first">
                Документ об отгрузке
            </td>
            <td class="bor-b">
                <?= $shippingDocuments ? implode(', ', $shippingDocuments) : Product::DEFAULT_VALUE ?>
            </td>
            <td class="right2_line_last">(5а)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="">
                <span style="font-weight: bold">Покупатель</span>
            </td>
            <td class="bor-b">
                <?= $isOut ? $invoice->contractor_name_short : $invoice->company_name_short; ?>
            </td>
            <td class="">(6)&nbsp;
            </td>
        </tr>
        <tr>
            <td class="">
                Адрес
            </td>
            <td class="bor-b">
                <?= $isOut ? $invoice->contractor_address_legal_full : $invoice->company_address_legal_full; ?>
            </td>
            <td class="">(6а)
            </td>
        </tr>
        <tr>
            <td class="">
                ИНН/КПП покупателя
            </td>
            <td class="bor-b">
                <?php if ($isOut) : ?>
                    <?= ($invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
                        $invoice->contractor_inn . '/' . $invoice->contractor_kpp : ''; ?>
                <?php else : ?>
                    <?= $invoice->company_inn.'/'.$invoice->company_kpp ?>
                <?php endif ?>
            </td>
            <td class="">(6б)
            </td>
        </tr>
        <tr>
            <td class="">
                Валюта: наименование, код
            </td>
            <td class="bor-b">
                Российский рубль, 643
            </td>
            <td class="">(7)&nbsp;
            </td>
        </tr>
        <tr class="tr-pad-1">
            <td class="">
                Идентификатор государственного контракта, договора (соглашения)(при наличии)
            </td>
            <td class="bor-b" style="vertical-align: bottom;">
                <?= $model->state_contract ? $model->state_contract : Product::DEFAULT_VALUE ?>
            </td>
            <td class="" style="vertical-align: bottom;">
                (8)&nbsp;
            </td>
        </tr>
    </tbody>
</table>
