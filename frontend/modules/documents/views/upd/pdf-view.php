<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.06.2017
 * Time: 19:51
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\product\Product;
use php_rutils\RUtils;
use frontend\assets\UpdAsset;
use yii\helpers\Html;
use common\models\document\Upd;
use common\components\image\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model common\models\document\Upd */

$this->title = $model->printTitle;

$addStamp = (boolean) $model->add_stamp;

?>
<style type="text/css">
    @media print{
        @page {size: landscape}
        <?php if (Yii::$app->request->get('actionType') == 'print'): ?>
            div.document-template th, div.document-template td {
                padding: 1px 4px;
            }
        <?php endif; ?>
    }
</style>

<div class="page-content-in p-center-album pad-pdf-p-album">
    <?= $this->render('template-print', [
        'model' => $model,
        'addStamp' => $addStamp,
    ]); ?>
</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id) : ?>
<pagebreak/>
<?php endif; ?>
