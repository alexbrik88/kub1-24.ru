<?php

if ($model->document_date < '2021-07-01') {
    echo $this->render('template-print-2020', $_params_);
} else {
    echo $this->render('template-print-2021', $_params_);
}