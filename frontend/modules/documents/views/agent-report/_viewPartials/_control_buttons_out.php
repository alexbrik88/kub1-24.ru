<?php

use common\components\date\DateHelper;
use yii\helpers\Url;
use \common\components\helpers\Html;
use common\components\TextHelper;
use frontend\rbac\permissions;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
/** @var \common\models\document\AgentReport $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
    ],
]);
$status = $model->statusOut;
$styleClass = ($model->invoice && $model->invoice->isOverdue()) ? 'red' : $status->getStyleClass();
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
?>

<div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
    <div class="col-xs-12" style="padding-right:0px !important;">
        <div class="control-panel col-xs-12 pad0 pull-right">
            <div class="status-panel col-xs-12 pad0">
                <div class="col-xs-12 col-sm-3 pad3">
                    <div class="btn full_w marg <?= $styleClass ?>"
                         style="padding-left:0px; padding-right:0px;text-align: center; "
                         title="Дата изменения статуса">
                        <?= date("d.m.Y", $model->status_out_updated_at); ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9 pad0">
                    <div class="col-xs-12 pad3">
                        <div class="btn full_w marg btn-status full_w  <?= $styleClass ?>"
                             title="Статус">
                            <i class="pull-left icon <?= $model->statusOut->getIcon(); ?>"></i>
                            <?= $model->statusOut->name; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-bottom: 5px !important;">
                <div class="col-xs-3"> &nbsp;</div>
                <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
                    <div class="document-panel widthe-i overflow-hide"
                         style="width: 99.9% !important; margin-bottom: 0px;">

                        <?php if ($model->invoice): ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::VIEW, ['model' => $model->invoice])): ?>
                                <a href="<?php echo Url::toRoute(['/documents/invoice/view', 'type' => $model->invoice->type, 'id' => $model->invoice->id, 'contractorId' => $model->agent->id, 'contractorTab' => 'agent_report']); ?>"
                                   class="btn btn-account yellow pull-right full_w"
                                   style="height: 34px;margin-left: 0; text-transform: uppercase;">
                                    <?php $class = $model->invoice->invoiceStatus->className(); ?>
                                    <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                                    СЧЕТ
                                </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if ($model->total_sum): ?>
                                <a href="<?php echo Url::toRoute(['/documents/agent-report/create-invoice', 'reportId' => $model->id])?>"
                                   class="btn yellow" style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;"
                                   title="Счет">
                                    <i class="pull-left icon fa fa-plus-circle"></i>
                                    Счет
                                </a>
                            <?php else: ?>
                                <span
                                   class="btn yellow no-sum tooltip2" style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;" data-tooltip-content="#tooltip2_text">
                                    <i class="pull-left icon fa fa-plus-circle"></i>
                                    Счет
                                </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
            <div class="portlet">
                <div class="customer-info" style="min-height:auto !important">
                    <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                        <table class="table no_mrg_bottom">
                            <tr>
                                <td>
                                    <span class="customer-characteristic color-black">Агент:</span>
                                    <span><?= Html::a($model->agent->getShortName(), [
                                            '/contractor/view',
                                            'type' => $model->agent->type,
                                            'id' => $model->agent->id,
                                        ]) ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="customer-characteristic color-black">Оплатить до:</span>
                                    <span class="color-black">
                                        <?= DateHelper::format($model->pay_up_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="customer-characteristic color-black">Основание:</span>
                                    <span class="color-black">
                                        <?= $model->getAgreementName(); ?>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="margin-bottom: 5px;">
                                        <?= \frontend\modules\documents\widgets\DocumentFileScanWidget::widget([
                                            'model' => $model,
                                            'hasFreeScan' => $model->company->getScanDocuments()->andWhere(['owner_id' => null])->exists(),
                                            'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                                            'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                                            'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                                            'scanFreeUrl' => Url::to(['/documents/scan-document/index-free']),
                                            'scanListUrl' => Url::to(['scan-list', 'type' => $model->type, 'id' => $model->id]),
                                            'scanBindUrl' => Url::to(['scan-bind', 'type' => $model->type, 'id' => $model->id]),
                                        ]); ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <div style="margin: 15px 0 0;">
                        <span style="font-weight: bold;">Комментарий</span>
                        <?= Html::tag('span', '', [
                            'id' => 'comment_internal_update',
                            'class' => 'glyphicon glyphicon-pencil',
                            'style' => 'cursor: pointer;',
                        ]); ?>
                    </div>
                    <div id="comment_internal_view" class="">
                        <?= Html::encode($model->comment_internal) ?>
                    </div>
                    <?php if ($canUpdate) : ?>
                        <?= Html::beginTag('div', [
                            'id' => 'comment_internal_form',
                            'class' => 'hidden',
                            'style' => 'position: relative;',
                            'data-url' => Url::to(['comment-internal', 'id' => $model->id, 'type' => $model->type]),
                        ]) ?>
                        <?= Html::tag('i', '', [
                            'id' => 'comment_internal_save',
                            'class' => 'fa fa-floppy-o',
                            'style' => 'position: absolute; top: -22px; right: 0px; cursor: pointer; font-size: 20px;',
                        ]); ?>
                        <?= Html::textarea('comment_internal', $model->comment_internal, [
                            'id' => 'comment_internal_input',
                            'rows' => 3,
                            'maxlength' => true,
                            'style' => 'width: 100%; padding-right: 35px; border: 1px solid #ddd;',
                        ]); ?>
                        <?= Html::endTag('div') ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display:none">
    <div id="tooltip2_text">
        Сумма по Отчету агента равна нулю,<br/>
        поэтому нельзя создать счет
    </div>
</div>

<?php
if ($canUpdate) {
    $this->registerJs('
        $(document).on("click", "#comment_internal_update", function () {
            $("#comment_internal_view").toggleClass("hidden");
            $("#comment_internal_form").toggleClass("hidden");
        });
        $(document).on("click", "#comment_internal_save", function () {
            $.post($("#comment_internal_form").data("url"), $("#comment_internal_input").serialize(), function (data) {
                $("#comment_internal_view").text(data.value);
                $("#comment_internal_form").addClass("hidden");
                $("#comment_internal_view").removeClass("hidden");
            })
        });

    ');
}
?>