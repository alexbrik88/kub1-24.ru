<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use \common\widgets\Modal;
use common\models\company\CompanyType;

/* @var string $dateFormatted */
/* @var $this yii\web\View */
/* @var $model common\models\document\AgentReport */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $agent Contractor */
/* @var $company \common\models\Company */
/** @var $order \common\models\document\AgentReportOrder */

$company = $model->company;
$agent = $model->agent;
$isCompanyIP = $company->company_type_id == \common\models\company\CompanyType::TYPE_IP;
$isAgentIP = $agent->company_type_id == \common\models\company\CompanyType::TYPE_IP;
$agentAgreementTitle = $model->agreement ? '№ '.$model->agreement->fullNumber.' от '.DateHelper::format($model->agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
$startDate = DateHelper::format($model->document_date, '01.m.Y', DateHelper::FORMAT_DATE);
$endDate = DateHelper::format($model->document_date, 't.m.Y', DateHelper::FORMAT_DATE);

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$director_name = $agent->getDirectorFio();

$hasNds = $agent->taxation_system == Contractor::WITH_NDS;
$isNdsExclude = false;
$ndsName = ($hasNds) ? 'В том числе НДС' : 'Без налога (НДС)';
$ndsValue = $hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-';
$precision = 2;

$agent_director_name = '';
if (!empty($agent->director_name) && $agent->director_name != 'ФИО Руководителя') {
    $agent_director_name = TextHelper::nameShort($agent->director_name);
}
?>

<style type="text/css">
    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }
    .pre-view-table h3 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }
    .auto_tpl {
        font-style: italic;
        font-weight: normal;
    }
    .m-size-div div {
        font-size: 12px;
    }
    .m-size-div div span {
        font-size: 9px;
    }
    .file_upload_block {
        text-align: center;
        border: 2px dashed #ffb848;
        color: #ffb848;
        padding: 12px;
    }
    .no_min_h {
        min-height: 0 !important;
    }
    .bord-dark tr, .bord-dark td, .bord-dark th {
        border: 1px solid #000000;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="min-width: 520px; max-width: 735px; border: 1px solid rgb(66, 118, 164); margin-top: 3px; min-height: 430px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 30px;">
            <div class="col-xs-6 pad0">
                <div class="col-xs-12 pad0 actions mart-m-8">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                    ]); ?>

                    <?php if ($canUpdate) : ?>
                        <a href="<?= \yii\helpers\Url::to(['update', 'type' => $ioType, 'id' => $model->id]); ?>"
                           title="Редактировать" class="btn darkblue btn-sm" style="height: 27px;margin-left: 3px">
                            <i class="icon-pencil"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad0" style="padding-top: 5px !important; border-bottom: 2px solid #000000">
            <h3 class="font-bold" style="padding: 0; margin: 0; text-align: center; padding-bottom:10px">
                Отчет агента № <?= $model->fullNumber; ?>
            </h3>
            <?php if ($agentAgreementTitle): ?>
                <div style="text-align:center;font-size:14px;font-weight:bold">
                    По агентскому договору
                    <?= $agentAgreementTitle ?>
                </div>
            <?php endif; ?>
            <div class="text-right"><?=(DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE))?></div>
        </div>
        <div class="col-xs-12 pad0" style="margin:15px 0">
            <?=($company->getShortName()) ?>, именуемое далее «Принципал», в лице <?= $company->chief_post_name ?> <?= $company->getChiefFio() ?>,
            <?=(!$isCompanyIP ? 'действующего на основании Устава' : '')?>, с одной стороны и
            <?=($agent->getShortName())?>, именуемое далее «Агент», в лице <?= $agent->director_post_name ?> <?= $agent_director_name ?>
            <?=(!$isAgentIP ? 'действующего на основании Устава' : '')?>, с другой стороны, в дальнейшем именуемые Стороны, настоящим Отчетом удостоверяют, что
            <?=($agentAgreementTitle ? "в соответствии с условиями Договора {$agentAgreementTitle}, " : '')?>
            в период с <?=$startDate?> по <?=$endDate?>, были получены оплаты от клиентов Агента:
        </div>
        <div class="col-xs-12 pad3" style="border-bottom: 2px solid #000000;">
            <div>
                <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
                    <thead>
                    <tr style="text-align: center;">
                        <th class="text-center" width="5%"> №</th>
                        <th class="text-center" width="40%"> Покупатель</th>
                        <th class="text-center" width="25%"> Сумма оплат за месяц (руб.)</th>
                        <th class="text-center" width="15%"> Комиссионное вознаграждение %</th>
                        <th class="text-center" width="15%"> Комиссионное вознаграждение (руб.)</th>
                    </tr>
                    </thead>
                    <tbody class="bord-dark">
                    <?php foreach ($model->agentReportOrders as $key => $order): ?>
                        <?php if ($order->is_exclude) continue; ?>
                        <tr>
                            <td style="text-align: center;"><?= $key + 1; ?></td>
                            <td style="text-align: left;"><?= $order->contractor->getShortName(); ?></td>
                            <td style="text-align: right;"><?= TextHelper::invoiceMoneyFormat($order->payments_sum, $precision) ?></td>
                            <td style="text-align: right;"><?= TextHelper::numberFormat($order->agent_percent, $precision) ?></td>
                            <td style="text-align: right;"><?= TextHelper::invoiceMoneyFormat($order->agent_sum, $precision); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <table class="it-b" style="width: 99.9%; margin-top:10px">
                    <tbody>
                    <tr>
                        <td width="450px" style="border: none"></td>
                        <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;">
                            Итого:
                        </td>
                        <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                            <b><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td width="430px" style="border: none"></td>
                        <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;"><?= $ndsName; ?>
                            :
                        </td>
                        <td class="txt-b2" style="text-align: right; border: none; width: 124px;"><?= $ndsValue; ?></td>
                    </tr>
                    <tr>
                        <td width="450px" style="border: none"></td>
                        <td class="txt-b2 bold" style="text-align: right; border: none; width: 150px;">
                            Всего к оплате:
                        </td>
                        <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                            <b><?= TextHelper::invoiceMoneyFormat($model->total_sum, 2); ?></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <br/>
                            Общая сумма вознаграждения по данному отчету
                            <b><?= TextHelper::amountToWords($model->total_sum / 100) ?></b>
                            <?= ($hasNds) ? ', в том числе НДС 20%' : ', НДС не облагается, в связи с применением Упрощенной Системы Налогообложения' ?>.
                            <br/>Услуги оказаны полностью. Стороны не имеют претензий по оказанию услуг.
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-12 padd10x0">
            <div style="height: 200px;">
                <div style="position: relative">
                    <table style="width: 100%;">
                        <tr>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                АГЕНТ
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                                ПРИНЦИПАЛ
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $agent->getShortName(); ?></td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="position:relative; z-index:2; width: 49%; padding-top: 5px; font-size: 10pt; border: none;"><?= $company->getShortName(); ?></td>
                        </tr>
                        <tr>
                            <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                                <?= !empty($agent_director_name)? '/ ' . $agent_director_name . ' /': ''; ?>
                            </td>
                            <td style="width: 2%; border: none;"></td>
                            <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                                / <?= $agent->company->getChiefFio(true) ?> /
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin-bottom:100px"></div>
