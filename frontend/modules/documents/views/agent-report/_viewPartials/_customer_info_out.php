<?php

use common\components\date\DateHelper;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Modal;
use frontend\models\Documents;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use common\components\helpers\ArrayHelper;

/* @var \common\models\document\AgentReport $model */
/* @var $message Message */
/* @var $actEssence \common\models\document\ActEssence */

$cancelUrl = Url::previous('lastPage');
$agreementArray = $model->agent->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] + ArrayHelper::map($agreementArray, 'id', 'listItemName');
?>
<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-10 caption" style="padding-bottom:15px;">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №
            <?= Html::activeTextInput($model, 'document_number', [
                'id' => 'account-number',
                'data-required' => 1,
                'class' => 'form-control input-editable-field ',
                'style' => 'max-width: 60px; display:inline-block;',
                'value' => $model->getDocumentNumber(),
            ]); ?>

            от

            <div class="input-icon input-calendar input-editable-field">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'class' => 'form-control date-picker',
                    'size' => 16,
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">
            <?php if ($canUpdate) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm " style="color: #FFF;"
                        title="Сохранить">
                    <span class="ico-Save-smart-pls"></span></button>
                <a href="<?= Url::to($cancelUrl) ?>"
                   class="btn-cancell btn darkblue btn-sm " style="color: #FFF;"
                   title="Отменить">
                    <span class="ico-Cancel-smart-pls"></span></a>

            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body" style="margin-bottom:0;">
        <div class="row" style="padding-left:8px">
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                        <label class="control-label" style="font-weight: bold">
                            Агент<span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'agent_id',
                            'data' => [$model->agent_id => $model->agent->getShortName()],
                            'disabled' => true,
                            'pluginOptions' => [
                                'width' => '97%'
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left:8px">
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                        <label class="control-label" style="font-weight: bold">
                            Оплатить до<span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <div class="input-icon input-calendar input-editable-field">
                            <i class="fa fa-calendar"></i>
                            <?= Html::activeTextInput($model, 'pay_up_date', [
                                'class' => 'form-control date-picker',
                                'size' => 16,
                                'data-date-viewmode' => 'years',
                                'value' => DateHelper::format($model->pay_up_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left:8px">
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px; padding-right: 0;">
                        <label class="control-label" style="font-weight: bold">
                            Основание:
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <?php echo Select2::widget([
                            'model' => $model,
                            'attribute' => 'agreement_id',
                            'data' => $agreementDropDownList,
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'placeholder' => '',
                                'width' => '97%',
                                'minimumResultsForSearch' => -1,
                                'language' => []
                            ],
                            'options' => [],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'id' => 'agreement-modal-container',
    'header' => '<h3 id="agreement-modal-header">Добавить договор</h3>',
    'options' => ['style' => 'max-width: 600px;']
]);

Pjax::begin([
    'id' => 'agreement-form-container',
    'enablePushState' => false,
    'linkSelector' => false,
]);

Pjax::end();

Modal::end();

$this->registerJs('
$(document).on("change", "#agentreport-agreement_id", function(e) {
    var value = $(this).val() || $(this).text();
    if (value == "add-modal-agreement") {
        e.preventDefault();

        $.pjax({url: "' . Url::to([
        '/contractor/agreement-create',
        'id' => $model->agent_id,
        'container' => 'agreement-select-container',
    ]) . '", container: "#agreement-form-container", push: false});

        $(document).on("pjax:success", function() {
            $("#agreement-modal-header").html($("[data-header]").data("header"));
            $(".date-picker").datepicker({format: "dd.mm.yyyy", language:"ru", autoclose: true}).on("change.dp", dateChanged);

            function dateChanged(ev) {
                if (ev.bubbles == undefined) {
                    var $input = $("[name=\'" + ev.currentTarget.name +"\']");
                    if (ev.currentTarget.value == "") {
                        if ($input.data("last-value") == null) {
                            $input.data("last-value", ev.currentTarget.defaultValue);
                        }
                        var $lastDate = $input.data("last-value");
                        $input.datepicker("setDate", $lastDate);
                    } else {
                        $input.data("last-value", ev.currentTarget.value);
                    }
                }
            };
        });
        $("#agreement-modal-container").modal("show");
        $("#contractor-agent_agreement_id").val("").trigger("change");
    }
}); 
');