<thead>
<tr>
    <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Но&shy;мер по по&shy;рядку</td>
    <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Товар</td>
    <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Единица измерения</td>
    <td width="3%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Вид упаков&shy;ки</td>
    <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">Количество</td>
    <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Масса брутто</td>
    <td width="5%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Коли&shy;чество (масса нетто)</td>
    <td width="8%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Цена, руб. коп.</td>
    <td width="8%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма без учета НДС, руб. коп.</td>
    <td colspan="2" class="text-center font-size-7" style="padding: 1px 2px">НДС</td>
    <td width="8%" rowspan="2" class="text-center font-size-7" style="padding: 1px 2px">Сумма с учётом НДС, руб. коп.</td>
</tr>
<tr>
    <td class="text-center font-size-7 font-size-7" style="padding: 1px 2px; width:24.5%">наименование, характеристика, сорт, артикул товара</td>
    <td width="4%" class="text-center font-size-7" style="padding: 1px 2px">код</td>
    <td width="5%" class="text-center font-size-7" style="padding: 1px 2px">наиме&shy;нование</td>
    <td width="4%" class="text-center font-size-7" style="padding: 1px 2px">код по ОКЕИ</td>
    <td width="4%" class="text-center font-size-7" style="padding: 1px 2px">в одном месте</td>
    <td width="4%" class="text-center font-size-7" style="padding: 1px 2px">мест, штук</td>
    <td width="4%" class="text-center font-size-7" style="padding: 1px 2px">ставка, %</td>
    <td width="8%" class="text-center font-size-7" style="padding: 1px 2px">сумма, руб. коп.</td>
</tr>
<tr>
    <td class="text-center  font-size-6">1</td>
    <td class="text-center  font-size-6">2</td>
    <td class="text-center  font-size-6">3</td>
    <td class="text-center  font-size-6">4</td>
    <td class="text-center  font-size-6">5</td>
    <td class="text-center  font-size-6">6</td>
    <td class="text-center  font-size-6">7</td>
    <td class="text-center  font-size-6">8</td>
    <td class="text-center  font-size-6">9</td>
    <td class="text-center  font-size-6">10</td>
    <td class="text-center  font-size-6">11</td>
    <td class="text-center  font-size-6">12</td>
    <td class="text-center  font-size-6">13</td>
    <td class="text-center  font-size-6">14</td>
    <td class="text-center  font-size-6">15</td>
</tr>
</thead>