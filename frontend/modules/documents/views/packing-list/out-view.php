<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use php_rutils\RUtils;
use common\models\document\PackingList;

/* @var $this yii\web\View */
/* @var $model common\models\document\PackingList */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SHORT_SINGLE) . ' №' . $model->invoice->fullNumber;
$this->context->layoutWrapperCssClass = 'ot-tn out-document out-act';
$hasProxy = !empty(trim($model->proxy_number)) &&
            !empty(trim($model->proxy_date)) &&
            !empty(trim($model->given_out_position)) &&
            !empty(trim($model->given_out_fio));
?>

<div class="page-content-in p-center p-t width-1500" style="padding: 0!important">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right font-size-10 text-right m-b-md m-r-10">
                <div>Унифицированная форма № ТОРГ-12</div>
                <div>Утверждена постановлением Госкомстата России от 25.12.98 № 132</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-10">
            <div class="b-b org-gruz-pseudo p-r-100">
                <?= $model->invoice->company_name_short; ?>, ИНН <?= $model->invoice->company_inn; ?>
                , <?= $model->invoice->company_address_legal_full; ?>, р/с <?= $model->invoice->company_rs; ?>, в
                банке <?= $model->invoice->company_bank_name; ?>, БИК <?= $model->invoice->company_bik; ?>,
                к/с <?= $model->invoice->company_ks; ?>
            </div>
            <div class="b-b structure-pseudo m-t-md"></div>
            <div class="m-t-md clearfix">
                <div class="col-xs-2 text-right">Грузополучатель</div>
                <div class="col-xs-10 b-b org-pseudo">
                    <?php if ($model->consignee) {
                        if ($model->contractor_address == PackingList::CONTRACTOR_ADDRESS_LEGAL) {
                            $address = $model->consignee->legal_address;
                        } else {
                            $address = $model->consignee->actual_address;
                        }
                        echo $model->consignee->getRequisitesFull($address);
                    } elseif ($model->type == Documents::IO_TYPE_OUT) {
                        if ($model->contractor_address == PackingList::CONTRACTOR_ADDRESS_LEGAL) {
                            $address = $model->invoice->contractor->legal_address;
                        } else {
                            $address = $model->invoice->contractor->actual_address;
                        }
                        echo $model->invoice->contractor_name_short,
                        $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '',
                        $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '',
                        $address ? ", {$address}" : '',
                        $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '',
                        $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '',
                        $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '',
                        $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : '';
                    } else {
                        echo $model->invoice->company_name_short, ', ИНН ', $model->invoice->company_inn,
                        $model->invoice->company_kpp ? ", КПП {$model->invoice->company_kpp}" : '',
                        ", {$model->invoice->company_address_legal_full}",
                        ", р/с {$model->invoice->company_rs}",
                        ", в банке {$model->invoice->company_bank_name}",
                        ", БИК {$model->invoice->company_bik}",
                        $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';
                    } ?>
                </div>
            </div>
            <div class="m-t-md clearfix">
                <div class="col-xs-2 text-right">Поставщик</div>
                <div class="col-xs-10 b-b org-pseudo">
                    <?= $model->invoice->company_name_short; ?>, ИНН <?= $model->invoice->company_inn; ?>
                    , <?= $model->invoice->company_address_legal_full; ?>, р/с <?= $model->invoice->company_rs; ?>, в
                    банке <?= $model->invoice->company_bank_name; ?>, БИК <?= $model->invoice->company_bik; ?>,
                    к/с <?= $model->invoice->company_ks; ?>
                </div>
            </div>
            <div class="m-t-md clearfix">
                <div class="col-xs-2 text-right">Плательщик</div>
                <div class="col-xs-10 b-b org-pseudo">
                    <?= $model->invoice->contractor_name_short; ?>, ИНН <?= $model->invoice->contractor_inn; ?>
                    , <?= $model->invoice->contractor_address_legal_full; ?>, р/с <?= $model->invoice->contractor_rs; ?>
                    , в банке <?= $model->invoice->contractor_bank_name; ?>, БИК <?= $model->invoice->contractor_bik; ?>
                    , к/с <?= $model->invoice->contractor_ks; ?>
                </div>
            </div>
            <div class="m-t-md clearfix">
                <div class="col-xs-2 text-right">Основание</div>
                <div class="col-xs-10 b-b doc-pseudo">

                </div>
            </div>
        </div>

        <div class="col-xs-3 pull-right m-t-n-258">
            <table class="code-table m-t-2">
                <tr>
                    <td width="70%" class="text-right">Форма по ОКУД</td>
                    <td width="100" class="tn-code text-center">330212</td>
                </tr>
                <tr>
                    <td class="text-right">по ОКПО</td>
                    <td class="text-center"><?= $model->invoice->company_okpo; ?></td>
                </tr>
                <tr>
                    <td class="height-40"></td>
                    <td></td>
                </tr>
                <tr></tr>
                <tr>
                    <td class="text-right">Вид деятельности по ОКДП</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-right height-22">по ОКПО</td>
                    <td class="text-center"><?= $model->invoice->contractor->okpo; ?></td>
                </tr>
                <tr>
                    <td class="text-right text-bottom height-57">по ОКПО</td>
                    <td class="text-center"><?= $model->invoice->company_okpo; ?></td>
                </tr>
                <tr>
                    <td class="text-right height-40 text-bottom">по ОКПО</td>
                    <td><?= $model->invoice->contractor->okpo; ?></td>
                </tr>
                <tr>
                    <td class="text-right height-40"><span class="b-l-t-b height-40">номер</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-right"><span class="b-l-t-b b-p-l">дата</span></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-right"><span class="pull-left">Транспортная накладная</span> <span class="b-l-t-b">номер</span>
                    </td>
                    <td><?= $model->waybill_number; ?></td>
                </tr>
                <tr>
                    <td class="text-right"><span class="b-l-t-b b-p-l b-b">дата</span></td>
                    <td><?= $model->waybill_date; ?></td>
                </tr>
                <tr>
                    <td class="text-right">Вид операции</td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row pos-t">
        <div class="col-xs-2 offset-2-m-l-n-10">
            <h4 class="font-bold">ТОВАРНАЯ НАКЛАДНАЯ</h4>
        </div>
        <span class="m-t-10 text-center b-tn-l tn-number-label">
            <?= $model->fullNumber; ?>
        </span>
        <span class="m-t-10 text-center b-tn tn-date-label">
            <?= $dateFormatted; ?>
        </span>
    </div>

    <div class="portlet wide_table pos-t-n-38">
        <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType] . '_out-view', [
            'model' => $model,
        ]); ?>
    </div>

    <div class="row m-t-n-xl">
        <div class="col-xs-12">
            <div class="row m-r-n-65">
                <div class="col-xs-10 col-xs-offset-2">
                    <div>
                        <span>Товарная накладная имеет приложение на </span>
                        <span class="b-b prilog"></span>
                    </div>
                    <div>
                        <span>и содержит </span>
                        <span class="b-b prilog prilog_big propis-pseudo">
                            <?= TextHelper::mb_ucfirst(RUtils::numeral()->getInWords(count($model->invoice->orders))); ?>
                        </span>
                        <span> порядковых номеров записей</span>
                    </div>
                </div>
            </div>
            <div class="row m-t-md m-r-n-65">
                <div class="col-xs-2 col-xs-offset-2">
                    <span>Всего мест</span>
                    <span class="b-b propis-pseudo prilog prilog_sm">
                        <?php if ($model->invoice->total_place_count > 0) {
                            echo TextHelper::mb_ucfirst(TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($model->invoice->total_place_count, RUtils::NEUTER)));
                        } ?>
                    </span>
                </div>

                <div class="col-xs-8">
                    <div class="row">
                        <?php $realMassNet = $model->invoice->getRealMassNet(); ?>
                        <div class="col-xs-3">Масса груза (нетто)</div>
                        <div class="col-xs-5 b-b propis-pseudo prilog pos-l">
                            <?php if ($realMassNet > 0) {
                                echo TextHelper::mb_ucfirst(TextHelper::getNumberInWords($realMassNet, [
                                    'килограмм', 'килограмма', 'килограммов',
                                ], [
                                    'грамм', 'грамма', 'граммов',
                                ]));
                            } ?>
                        </div>
                        <div class="col-xs-3 b-2 m-l">
                            <?php if ($realMassNet > 0) {
                                echo TextHelper::moneyFormat($realMassNet, 2) . ' КГ';
                            } ?>
                        </div>
                    </div>
                    <div class="row m-t-md">
                        <?php $realMassGross = $model->invoice->getRealMassGross(); ?>
                        <div class="col-xs-3">Масса груза (брутто)</div>
                        <div class="col-xs-5 b-b propis-pseudo prilog pos-l">
                            <?php if ($realMassGross > 0) {
                                echo TextHelper::mb_ucfirst(TextHelper::getNumberInWords($realMassGross, [
                                    'килограмм', 'килограмма', 'килограммов',
                                ], [
                                    'грамм', 'грамма', 'граммов',
                                ]));
                            } ?>
                        </div>
                        <div class="col-xs-3 b-2 b-2-t m-l">
                            <?php if ($realMassGross > 0) {
                                echo TextHelper::moneyFormat($realMassGross, 2) . ' КГ';
                            } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-t-md">
                <div class="col-xs-6">
                    <div>
                        <span>Приложение (паспорт, сертификаты и т.п.) на</span>
                        <span class="b-b propis-pseudo prilog prilog_sm"></span>
                        <span class="m-l">листах</span>
                    </div>
                    <div class="row m-t-lg m-r-n-83">
                        <div class="col-xs-11">
                            <div class="font-bold">Всего отпущено на сумму</div>
                            <div class="b-b propis-pseudo width-plus-15">
                                <?= \common\components\TextHelper::amountToWords($model->invoice->total_amount_with_nds / 100); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t m-r-n-83" style="margin-top: 0!important;">
                        <div class="col-xs-3">Отпуск груза разрешил</div>
                        <div class="col-xs-2 b-b dol-pseudo prilog prilog_sm m-l-8 min-h37">
                            <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                                <?= ($model->invoice->signEmployeeCompany) ?
                                    $model->invoice->signEmployeeCompany->position : $model->invoice->company_chief_post_name; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-2 b-b podp-pseudo prilog prilog_sm m-l-lg m-r-lg min-h37" >
                        </div>
                        <div class="col-xs-3 b-b podp-ras-pseudo min-h37">
                            <?= $model->invoice->getCompanyChiefFio(true); ?>
                        </div>
                    </div>
                    <div class="row m-t m-r-n-83">
                        <div class="col-xs-5 font-bold m-r-8">Главный (старший) бухгалтер</div>
                        <div class="col-xs-2 b-b podp-pseudo prilog prilog_sm m-l-lg m-r-lg"></div>
                        <div class="col-xs-3 b-b podp-ras-pseudo">
                            <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                                <?= $model->invoice->getCompanyChiefAccountantFio(true); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row m-t m-r-n-83">
                        <div class="col-xs-3">Отпуск груза произвел</div>
                        <div class="col-xs-2 b-b dol-pseudo prilog prilog_sm m-l-8 min-h37">
                            <?php if ($model->invoice->company->company_type_id != company\CompanyType::TYPE_IP): ?>
                                <?= ($model->invoice->signEmployeeCompany) ?
                                    $model->invoice->signEmployeeCompany->position : $model->invoice->company_chief_post_name; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-2 b-b podp-pseudo prilog prilog_sm m-l-lg m-r-lg"></div>
                        <div class="col-xs-3 b-b podp-ras-pseudo min-h37">
                            <?= $model->invoice->getCompanyChiefFio(true); ?>
                        </div>
                    </div>
                    <div class="row m-t-lg m-r-n-83">
                        <div class="col-xs-4 text-center">М.П.</div>
                        <div class="col-xs-1 m-l-xl">"<?= DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE); ?>"</div>
                        <div class="col-xs-2 b-b -pseudo prilog prilog_sm">
                            <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                'date' => $model->document_date,
                                'format' => 'F', // month in word
                                'monthInflected' => true,
                            ]);; ?>
                        </div>
                        <div class="col-xs-4"><?= DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE); ?> года</div>
                    </div>
                </div>

                <div class="col-xs-6 b-l">
                    <div class="row m-r-n-60">
                        <div class="col-xs-3">По доверенности №</div>
                        <div class="col-xs-3 b-b -pseudo">
                            <?= $hasProxy ? $model->proxy_number : '' ?>
                        </div>
                        <div class="col-xs-1 text-center">от</div>
                        <div class="col-xs-4 b-b -pseudo">
                            <?= $hasProxy ? Yii::$app->formatter->asDate($model->proxy_date, 'long') : '' ?>
                        </div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-2">выданной</div>
                        <div class="col-xs-9 b-b kem-pseudo">
                            <?php if ($hasProxy) : ?>
                                <?= $model->consignee ?
                                    $model->consignee->getTitle(true) :
                                    $model->invoice->contractor_name_short; ?>,
                                <?= $model->given_out_position ?>,
                                <?= $model->given_out_fio ?>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-9 col-xs-offset-2 b-b -pseudo"></div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-9 col-xs-offset-2 b-b -pseudo"></div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-2">Груз принял</div>
                        <div class="col-xs-3 b-b dol-pseudo"></div>
                        <div class="col-xs-3 b-b podp-pseudo m-l-32 m-r-32"></div>
                        <div class="col-xs-2 b-b podp-ras-pseudo"></div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-2">Груз получил<br>грузополучатель</div>
                        <div class="col-xs-3 b-b dol-pseudo"></div>
                        <div class="col-xs-3 b-b podp-pseudo m-l-32 m-r-32"></div>
                        <div class="col-xs-2 b-b podp-ras-pseudo"></div>
                    </div>
                    <div class="row m-t m-r-n-60">
                        <div class="col-xs-5 text-center">М.П.</div>
                        <div class="col-xs-1">" &nbsp;&nbsp;&nbsp;&nbsp; "</div>
                        <div class="col-xs-2 b-b -pseudo prilog prilog_sm"></div>
                        <div class="col-xs-4">20 &nbsp;&nbsp;&nbsp;&nbsp; года</div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>