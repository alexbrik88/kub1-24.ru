<?php
use common\components\TextHelper;
use common\models\document\OrderPackingList;

/** @var \common\models\document\PackingList $model */
/** @var \common\models\document\OrderPackingList $order */
$order = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);
?>

<table class="table table-resume">
    <tbody>
    <tr role="row" class="odd">
        <td><b>Итого:</b></td>
    </tr>
    <tr role="row" class="even">
        <td>Кол-во (масса нетто):</td>
        <td><?= $order->sum('quantity') * 1 ?></td>
    </tr>
    <?php if ($model->invoice->hasNds): ?>
        <tr role="row" class="odd">
            <td>Сумма без учёта НДС:</td>
            <td><?php echo TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2); ?></td>
        </tr>
        <tr role="row" class="odd">
            <td>Сумма НДС:</td>
            <td><?php echo TextHelper::invoiceMoneyFormat($model->totalPLNds, 2); ?></td>
        </tr>
    <?php else: ?>
        <tr role="row" class="odd">
            <td>Без налога (НДС):</td>
            <td>-</td>
        </tr>
    <?php endif; ?>
    <tr role="row" class="even">
        <td>Сумма с учётом НДС:</td>
        <td><?php echo TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
    </tr>
    </tbody>
</table>
