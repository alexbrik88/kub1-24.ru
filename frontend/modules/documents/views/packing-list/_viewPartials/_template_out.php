<?php

use common\components\TextHelper;
use common\models\product\Product;

/** @var \common\models\document\OrderPackingList $order */
/** @var [] $product */

$plus = 0;

?>
<?php if (isset($order)) : ?>
    <?php
    if (!isset($model)) {
        $model = $order->packingList;
    }
    if ($model->invoice->hasNds) {
        $priceNoNds = $order->priceNoNds;
        $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
        $amountWithNds = $model->getPrintOrderAmount($order->order_id);
        $TaxRateName = $order->order->saleTaxRate->name;
        $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
    } else {
        $priceNoNds = $order->priceWithNds;
        $amountNoNds = $model->getPrintOrderAmount($order->order_id);
        $amountWithNds = $model->getPrintOrderAmount($order->order_id);
        $TaxRateName = 'Без НДС';
        $ndsAmount = Product::DEFAULT_VALUE;
    }
    $quantity = $order ? $order->getAvailableQuantity() : 1;
    if ($quantity != intval($quantity)) {
        $quantity = rtrim(number_format($quantity, 10, '.', ''), 0);
    }
    ?>
    <tr role="row" class="odd order">
        <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= $order->order->unit ? $order->order->unit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->product_code; ?></td>
        <td><?= $order->order->unit ? $order->order->unit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->box_type; ?></td>
        <td><?= $order->order->count_in_place; ?></td>
        <td><?= $order->order->place_count; ?></td>
        <td><?= $order->order->mass_gross; ?></td>
        <td><input
                    class="input-editable-field quantity  form-control" type="number"
                    value="<?= $quantity ?>" min="1"
                    max="<?= $quantity ?>"
                    id="<?= $order->order_id ?>"
                    name="<?= $order ? 'OrderPackingList[' . $order->order_id . '][quantity]' : '' ?>"
                    style="padding: 6px 6px;">
        </td>
        <input class="status" type="hidden"
               name="<?= 'OrderPackingList[' . $order->order_id . '][status]' ?>"
               value="active">
        <td><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
        <td><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
        <td>
            <?= $TaxRateName ?>
        </td>
        <td>
            <?= $ndsAmount ?>
        </td>
        <td><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
        <td><span
                    class="icon-close input-editable-field delete-row"></span>
        </td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td><?= \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <input class="status" type="hidden"
               name=""
               value="active">
        <td></td>
        <td
        </td>
        <td>
        </td>
        <td>
        </td>
        <td></td>
        <td class="text-right"></td>
        <td><span class="editable-field"><?= $key + 1; ?></span><span
                    class="icon-close input-editable-field delete-row hide"></span>
        </td>
    </tr>
<?php endif; ?>
