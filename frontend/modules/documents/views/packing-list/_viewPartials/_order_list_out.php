<?php
/** @var \common\models\document\PackingList $model */
use common\components\TextHelper;
use yii\bootstrap\Html;

$tableClass = isset($tableClass) ? $tableClass : 'table table-striped table-bordered table-hover customers_table';
/* @var $this \yii\web\View */
    $plus = 0;

?>

<table class="<?= $tableClass; ?>">
    <thead>
    <tr class="heading" role="row">
        <th width="10%">Наименование</th>
        <th width="6%">Ед.изм.</th>
        <th width="5%">Код</th>
        <th width="5%">Код по ОКЕИ</th>
        <th width="3%">Вид упаковки</th>
        <th width="3%">Кол-во в одном месте</th>
        <th width="3%">Кол-во мест, штук</th>
        <th width="3%">Масса брутто</th>
        <th width="3%">Кол-во (масса нетто)</th>
        <th width="5%">Цена</th>
        <th width="5%">Сумма без НДС</th>
        <th width="5%">Ставка НДС</th>
        <th width="5%">Сумма НДС</th>
        <th width="8%">Сумма с учётом НДС</th>
        <th width="3%"></th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php
    foreach (\common\models\document\OrderPackingList::findAll(['packing_list_id' => $model->id]) as $key => $order): ?>

        <?php echo $this->render('_template_out', [
            'key' => $key,
            'order' => $order,
            'model' => $model,
            'precision' => $precision,
        ]);
        ?>

    <?php endforeach; ?>
    </tbody>

</table>


<?php

$count = count(\common\models\document\OrderPackingList::findAll(['packing_list_id' => $model->id]));
$urlAdd = \yii\helpers\Url::to('/documents/ajax/add-new-row');
$urlDel = \yii\helpers\Url::to('/documents/ajax/delete-row');
$urlSub = \yii\helpers\Url::to('/documents/ajax/subsitution');
$urlClose = \yii\helpers\Url::to('/documents/ajax/close');
$urlEdit = \yii\helpers\Url::to('/documents/ajax/edit');
$script = <<< JS
var count = parseInt('$count');

$('#plusbtn').click(function() {
    var active = [];
    $('#plusbtn').addClass('hide');
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlAdd',
        data : {key : count, invoice_id: '$model->invoice_id', packing_list_id: '$model->id', active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
        }
    });
    count++;
});

$(document).on('click', '.delete-row', function() {
    $(this).closest('tr').find('.status').val('deleted');
    var active = [];
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlDel',
        data : {packing_list_id : '$model->id',active : active},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
        }
    });

    if ($(this).closest('tr').find('.status').val() == ''){
        $(this).closest('tr').remove();
    } else {
        $(this).closest('tr').hide();
    }

    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
});

$(document).on('change','.dropdownlist',function() {
    var active = [];
    $(".status[value='active']").each(function( index, value ) {
        active.push($(value).closest('tr').find('.quantity').attr('id'));
    });
    jQuery.post({
        url : '$urlSub',
        data : {order_id: $(this).val(), packing_list_id : '$model->id',active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
            if( $('.order:visible').length == 1){
                $('.delete-row').hide();
            }
            if( $('.order:visible').length > 1){
                $('.delete-row').show();
            }
        }
    });
    $(this).closest('tr').remove();
});

$(document).on('click','.btn-cancel', function() {
    $('.edit-in').hide();
    $('#plusbtn').addClass('hide');
    jQuery.get({
        url : '$urlClose',
        data : {packing_list_id : '$model->id',ioType : 2} ,
        success : function(data) {
            jQuery('.customers_table').replaceWith(data);
            $('.edit-in').show();
        }
    })
});

$(document).ready(function () {
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {packing_list_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});

$(document).on('click','.edit-in', function() {
    $('#plusbtn').addClass('hide');
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {packing_list_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});
JS;
$this->registerJs($script, $this::POS_READY);
?>


