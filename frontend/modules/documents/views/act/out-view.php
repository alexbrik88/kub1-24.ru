<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->id;
$this->context->layoutWrapperCssClass = 'out-document out-act';

?>

<div class="page-content-in p-center width-990-print">
    <div>
        <h3 class="font-bold m-b">Акт № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?></h3>
    </div>
    <table class="table table-bordered font-bold">
        <tr>
            <td class="text-middle" width="15%">Исполнитель:</td>
            <td width="85%">
                <?= $model->invoice->company_name_short; ?>, ИНН <?= $model->invoice->company_inn; ?>, <?= $model->invoice->company_address_legal_full; ?>, р/с <?= $model->invoice->company_rs; ?>, в банке <?= $model->invoice->company_bank_name; ?>, БИК <?= $model->invoice->company_bik; ?>, к/с <?= $model->invoice->company_ks; ?>
            </td>
        </tr>
        <tr>
            <td class="text-middle" width="15%">Заказчик:</td>
            <td width="85%">
                <?= $model->invoice->contractor_name_short; ?>, ИНН <?= $model->invoice->contractor_inn; ?>, <?= $model->invoice->contractor_address_legal_full; ?>, р/с <?= $model->invoice->contractor_rs; ?>, в банке <?= $model->invoice->contractor_bank_name; ?>, БИК <?= $model->invoice->contractor_bik; ?>, к/с <?= $model->invoice->contractor_ks; ?>
            </td>
        </tr>
        <tr>
            <td width="15%">Основание:</td>
            <td width="85%">
                 <?= $model->getBasisName(); ?>
            </td>
        </tr>
    </table>
    <div class="portlet">
        <table class="table table-striped table-bordered table-hover out-act_table">
            <thead>
            <tr class="heading" role="row">
                <th width="5%">№</th>
                <th width="25%">Наименование</th>
                <th width="10%">Количество</th>
                <th width="5%">Ед</th>
                <th width="15%">Цена</th>
                <th width="15%">Сумма</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->invoice->getOrdersByProductType(\common\models\product\Product::PRODUCTION_TYPE_SERVICE) as $key => $order): ?>
                <tr role="row" class="odd">
                    <td><?= $key + 1; ?></td>
                    <td><?= $order->product_title; ?></td>
                    <td><?= $order->quantity; ?></td>
                    <td><?= $order->product->productUnit ? $order->product->productUnit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
                    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->selling_price_with_vat, 2); ?></td>
                    <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amount_sales_with_vat, 2); ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="portlet pull-right">
        <table class="table table-resume">
            <tbody>
            <tr role="row" class="even">
                <td><b>Итого:</b></td>
                <td><?= TextHelper::invoiceMoneyFormat($model->getTotalAmount(true), 2); ?></td>
            </tr>
            <?php if ($model->invoice->hasNds): ?>
                <tr role="row" class="odd">
                    <td><b>В том числе НДС:</b></td>
                    <td><?= TextHelper::invoiceMoneyFormat($model->getTotalNds(), 2); ?></td>
                </tr>
            <?php else: ?>
                <tr role="row" class="odd">
                    <td><b>Без налога (НДС):</b></td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="row print-act">
        <div class="col-md-12">
            <div>Всего наименований <?= count($model->invoice->getOrdersByProductType(\common\models\product\Product::PRODUCTION_TYPE_SERVICE)); ?>, на сумму <?= TextHelper::invoiceMoneyFormat($model->getTotalAmount(true), 2); ?> руб.</div>
            <div class="font-bold"><?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->getTotalAmount(true) / 100)); ?></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 m-t m-b-lg">
            Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания услуг не имеет.
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-6">
                    <h4 class="font-bold uppercase">исполнитель</h4>
                    <div class="m-b-xl"> <?= $model->invoice->company_name_short; ?></div>
                    <div class="b-b"></div>
                </div>
                <div class="col-xs-6">
                    <h4 class="font-bold uppercase">заказчик</h4>
                    <div class="m-b-xl"> <?= $model->invoice->contractor_name_short; ?></div>
                    <div class="b-b"></div>
                </div>
            </div>
        </div>
    </div>

</div>
