<?php

use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}

$canUpdate = $model->status_out_id != ActStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$actCount = $model->getOrderActs()->count();
$invoiceCount = $model->invoice->getOrders()
    ->joinWith('product', false)
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])
    ->count();

$isAddCan = $invoiceCount > $actCount;

$invoice = $model->invoice;
$hasNds = $invoice->hasNds;
$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;

if ($isNdsExclude) {
    $sum = $model->totalAmountNoNds;
} else {
    $sum = $model->totalAmountWithNds;
}
$precision = $model->invoice->price_precision;
$newCompanyTemplate = (boolean)$model->company->new_template_order;
?>

<div class="page-content-in">
    <?php
    if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
        echo \frontend\widgets\ConfirmModalWidget::widget([
            'options' => [
                'id' => 'delete-confirm',
            ],
            'toggleButton' => false,
            'confirmUrl' => Url::toRoute(['delete', 'type' => $model->type, 'id' => $model->id]),
            'confirmParams' => [],
            'message' => "Вы уверены, что хотите удалить {$this->title}?",
        ]);
    } ?>
    <?php if ($backUrl !== null) {
        echo \yii\helpers\Html::a('Назад к списку', $backUrl, [
            'class' => 'back-to-customers',
        ]);
    } ?>
    <div class="col-xs-12 col-lg-7 pad0">
        <div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
             style="min-width: 520px; max-width: 735px; border: 1px solid rgb(66, 118, 164); margin-top: 3px; min-height: 430px;">
            <div class="col-xs-12 pad5 pre-view-table">
                <div class="col-xs-12 pad3" style="height: 30px;">
                    <div class="col-xs-6 pad0">
                        <div class="col-xs-12 pad0 actions mart-m-8">
                            <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                                'model' => $model,
                            ]); ?>

                            <?php if ($canUpdate) : ?>
                                <a href="<?= \yii\helpers\Url::to(['update', 'type' => $ioType, 'id' => $model->id]); ?>"
                                   title="Редактировать" class="btn darkblue btn-sm" style="height: 27px;margin-left: 3px">
                                    <i class="icon-pencil"></i>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?= $this->render('template', [
                    'model' => $model,
                    'addStamp' => true,
                ]); ?>
            </div>
        </div>
    </div>
    <?= $this->render('_viewPartials/_control_buttons_' . Documents::$ioTypeToUrl[$ioType], ['model' => $model,
        'message' => $message,
        'dateFormatted' => $dateFormatted,
        'useContractor' => $useContractor,
        'contractorId' => $contractorId,
    ]); ?>

    <span class="<?= $isAddCan ? 'input-editable-field ' : '' ?>hide btn yellow btn-add-line-table" id="plusbtn">
        <i class="fa icon fa-plus-circle fa-2x" style="margin: 0; padding: 0;"></i>
    </span>

    <div <?= $newCompanyTemplate ? 'id="buttons-bar-fixed"' : '' ?> >
        <?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
            'model' => $model,
            'useContractor' => $useContractor,
            'canUpdate' => $canUpdate,
            'contractorId' => $contractorId,
            'ioType' => $ioType
        ]); ?>
    </div>
</div>


<?php if ($model->type == Documents::IO_TYPE_OUT && $canUpdate): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]); ?>
<?php endif; ?>
