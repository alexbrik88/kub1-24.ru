<?php

use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\ActStatus;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\DocConverter;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
/* @var $actEssence \common\models\document\ActEssence */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->document_number;
$this->context->layoutWrapperCssClass = 'out-document out-act';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(frontend\rbac\permissions\Contractor::VIEW)) {
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $contractorId,];
} elseif (Yii::$app->user->can(frontend\rbac\permissions\document\Document::INDEX)) {
    $backUrl = ['index', 'type' => $model->type,];
}

$canUpdate = $model->status_out_id != ActStatus::STATUS_REJECTED
    &&
    Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE_STATUS, [
        'model' => $model,
    ]);

$actCount = count($model->orderActs);
$invoiceCount = $model->invoice->getOrders()
    ->joinWith('product', false)
    ->andWhere(['product.production_type' => Product::PRODUCTION_TYPE_SERVICE])
    ->count();

$isAddCan = $invoiceCount > $actCount;

$invoice = $model->invoice;
$hasNds = $invoice->hasNds;
$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;

if ($isNdsExclude) {
    $sum = $model->getPrintAmountNoNds();
} else {
    $sum = $model->getPrintAmountWithNds();
}
$precision = $model->invoice->price_precision;
$this->registerJs(<<<JS
 $(function(){
  $('.btn-save').removeClass('hide');
  $('.btn-cancel').removeClass('hide');
  $('.edit').addClass('hide');
  $('.view-action-buttons').addClass('hide');
  $('.form-action-buttons').removeClass('hide');
  $('.editable-field').addClass('hide');
  $('.input-editable-field').removeClass('hide');
  $('.control-panel-pre-edit').addClass('hide');
  $('.info-button').addClass('hide');
})
JS
);
?>
    <div class="page-content-in">

        <?= Html::errorSummary($model, ['class' => 'error-summary']) ?>

        <?= Html::beginForm('', 'post', [
            'id' => 'edit-act',
            'class' => 'form-horizontal',
            'novalidate' => 'novalidate',
            'enctype' => 'multipart/form-data',
        ]); ?>

        <div class="row">
            <div class="col-xs-12 col-lg-7 col-w-lg-8 col-md-7" style="max-width: 720px;">
                <?= $this->render('_viewPartials/_customer_info_' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'message' => $message,
                    'dateFormatted' => $dateFormatted,
                    'canUpdate' => $canUpdate,
                    'actEssence' => $actEssence,
                ]); ?>
            </div>

        </div>
        <div class="portlet">
            <?= Html::hiddenInput("OrderAct") ?>

            <?= $this->render('_viewPartials/_order_list_' . Documents::$ioTypeToUrl[$ioType], [
                'model' => $model,
                'precision' => $precision,
            ]); ?>
        </div>
        <span class="<?= $isAddCan ? 'input-editable-field ' : 'hide' ?> btn yellow btn-add-line-table" id="plusbtn">
        <i class="fa icon fa-plus-circle fa-2x" style="margin: 0; padding: 0;"></i>
    </span>

        <div class="portlet pull-right">
            <table class="table table-resume">
                <tbody>
                <tr role="row" class="even">
                    <td><b>Итого:</b></td>
                    <td><?= TextHelper::invoiceMoneyFormat($sum, 2); ?></td>
                </tr>
                <tr role="row" class="odd">
                    <td>
                        <b>
                            <?php if ($hasNds) : ?>
                                <?php if ($isNdsExclude) : ?>
                                    НДС сверху:
                                <?php else : ?>
                                    В том числе НДС:
                                <?php endif ?>
                            <?php else : ?>
                                Без налога (НДС):
                            <?php endif ?>
                        </b>
                    </td>
                    <td><?= $hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-'; ?></td>
                </tr>
                <tr role="row" class="even">
                    <td><b>Всего к оплате:</b></td>
                    <td><?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <?= Html::endForm(); ?>
    </div>
<?= $this->render('_viewPartials/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
    'model' => $model,
    'useContractor' => $useContractor,
    'contractorId' => $contractorId,
    'canUpdate' => $canUpdate,
    'ioType'    => $ioType
]); ?>
    <!-- remove product -->

    <div id="modal-remove-one-product" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
         style="display: none; margin-top: -45px;">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                        <div class="row">Вы уверены, что хотите удалить эту позицию из акта?</div>
                    </div>
                    <div class="form-actions row">
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                    style="width: 80px;color: white;">ДА
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" data-dismiss="modal" class="btn darkblue"
                                    style="width: 80px;color: white;">НЕТ
                            </button>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
<?php
$count = $model->getOrderActs()->count();
$urlAdd = Url::to('/documents/act-ajax/add-new-row');
$urlDel = Url::to('/documents/act-ajax/delete-row');
$urlSub = Url::to('/documents/act-ajax/subsitution');
$urlClose = Url::to('/documents/act-ajax/close');
$urlEdit = Url::to('/documents/act-ajax/edit');
$script = <<< JS
var invoiceCount = $invoiceCount;
var actCount = $actCount;

$('#plusbtn').click( function() {
    $('#plusbtn').addClass('hide');
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);

    jQuery.post({
        url : '$urlAdd',
        data : {key : actCount, invoice_id: '$model->invoice_id', act_id: '$model->id', active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            actCount++;
            $(".editable-field").addClass("hide");
            $(".input-editable-field").removeClass("hide");
            if (invoiceCount > $('tr.order').length) {
                $('#plusbtn').removeClass('hide');
            } else {
                $('#plusbtn').addClass('hide');
            }
        }
    });
});

$(document).on('click', '.delete-row', function() {
    $(this).closest('tr').remove();
    actCount--;
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);
    jQuery.post({
        url : '$urlDel',
        data : {act_id : '$model->id',active : active},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
        }
    });
});

$(document).on('change','.dropdownlist',function() {
    var active = Object.keys($("form#edit-act").serializeFormAsObject().OrderAct);
    var addRow = $(this).closest('tr');
    jQuery.post({
        url : '$urlSub',
        data : {order_id: $(this).val(), act_id : '$model->id',active : active},
        success : function(data) {
            jQuery('#tbody').append(data);
            $('.input-editable-field').removeClass('hide');
            $('.editable-field').addClass('hide');
            if (invoiceCount > $('tr.order').length) {
                $('#plusbtn').removeClass('hide');
            } else {
                $('#plusbtn').addClass('hide');
            }
            addRow.remove();
        }
    });
});

$(document).on('click','.btn-cancel', function() {
    $('.edit-in').hide();
    $('#plusbtn').addClass('hide');
    jQuery.get({
        url : '$urlClose',
        data : {act_id : '$model->id',ioType : 2} ,
        success : function(data) {
            $('.out-act_table').replaceWith(data);
            $('.edit-in').show();
        }
    })
});

$(document).on('click','.edit-in', function() {
    $('#plusbtn').addClass('hide');
    if( $('.order:visible').length == 1){
        $('.delete-row').hide();
    }
    if( $('.order:visible').length > 1){
        $('.delete-row').show();
    }
    $('.btn-cancel').hide();
    jQuery.get({
        url: '$urlEdit',
        data : {act_id : '$model->id',ioType : 2},
        success : function(data) {
            if (data == 0) $('#plusbtn').addClass('hide');
            if (data == 1) $('#plusbtn').removeClass('hide');
            $('.btn-cancel').show();
        }
    });
});
JS;
$this->registerJs($script, $this::POS_READY);