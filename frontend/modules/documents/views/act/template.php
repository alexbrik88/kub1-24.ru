<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;
use yii\helpers\Html;
use frontend\models\Documents;
use common\models\company\CompanyType;
use common\models\Contractor;

/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $addStamp boolean */

common\assets\DocumentTemplateAsset::register($this);

$invoice = $model->invoice;
$company = $invoice->company;
$contractor = $invoice->contractor;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);

$director_name = '';
if ($contractor->director_in_act) {
    if (!empty($invoice->contractor_director_name) && $invoice->contractor_director_name != 'ФИО Руководителя') {
        $director_name = TextHelper::nameShort($invoice->contractor_director_name);
    } elseif (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
        $director_name = TextHelper::nameShort($contractor->director_name);
    }
} elseif ($contractor->chief_accountant_in_act) {
    if (!empty($contractor->chief_accountant_name)) {
        $director_name = TextHelper::nameShort($contractor->chief_accountant_name);
    }
} elseif ($contractor->contact_in_act) {
    if (!empty($contractor->contact_name)) {
        $director_name = TextHelper::nameShort($contractor->contact_name);
    }
}

$signatureHeight = $defaultSignatureHeight = 50;

if ($model->signed_by_name) {
    $signatureHeight = ImageHelper::getSignatureHeight($model->employeeSignature->file ?? null, 50, 100);
    $signatureLink = (empty($addStamp) || $model->employeeSignature === null) ? false :
        (EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 145, $signatureHeight, EasyThumbnailImage::THUMBNAIL_INSET_BOX) ? : false);
} else {
    $signatureHeight = ImageHelper::getSignatureHeight($model->getImage('chiefSignatureImage'), 50, 100);
    $signatureLink = empty($addStamp) ? false :
        (EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 145, $signatureHeight, EasyThumbnailImage::THUMBNAIL_INSET_BOX) ? : false);
}

$printLink = empty($addStamp)? false :
    (EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET_BOX) ? : false);

$signaturePosition = $model->type == Documents::IO_TYPE_OUT ? 'background-position: 0 40px;' : 'background-position: 350px 40px;';
$printPosition = $model->type == Documents::IO_TYPE_OUT ? 'background-position: 145px 0;' : 'background-position: 500px 0;';
$signatureLinkStyle = $signatureLink ? " background: url($signatureLink); background-repeat: no-repeat; {$signaturePosition}" : '';
$printLinkStyle = $printLink ? " background: url($printLink); background-repeat: no-repeat; {$printPosition}" : '';

$isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
$amount = $isNdsExclude ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds();
$ndsName = $invoice->ndsViewType->name;
$ndsValue = $invoice->hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-';
$precision = $model->invoice->price_precision;
?>

<div class="document-template act-template">
    <div class="doc-title text-c" style="margin: 12px 0;">
        Акт
        № <?= $model->fullNumber; ?>
        от <?= $dateFormatted; ?>
    </div>
    <table class="" style="">
        <tr class="pad-t-10">
            <td class="v-al-t">
                Исполнитель:
            </td>
            <td>
                <b><?= $model->getExecutorFullRequisites() ?></b>
            </td>
        </tr>
        <tr class="pad-t-10">
            <td class="v-al-t">
                Заказчик:
            </td>
            <td>
                <b><?= $model->getCustomerFullRequisites() ?></b>
            </td>
        </tr>
        <tr class="pad-t-10 pad-b-10">
            <td class="v-al-t">
                Основание:
            </td>
            <td>
                <b><?= $model->getBasisName(); ?></b>
            </td>
        </tr>
    </table>

    <table class="" style="border: 2px solid #000000;">
        <thead>
            <tr class="heading">
                <th class="bor" style="width: 5%;">№</th>
                <th class="bor" style="width: 45%;">Наименование работ, услуг</th>
                <th class="bor" style="width: 10%;">Кол-вo</th>
                <th class="bor" style="width: 10%;">Ед</th>
                <th class="bor" style="width: 15%;">Цена</th>
                <th class="bor" style="width: 15%;">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <?php $model->isGroupOwnOrdersByProduct = true; ?>
            <?php foreach ($model->ownOrders as $key => $order): ?>
                <?php
                $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;
                $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
                $orderAmount = $model->getPrintOrderAmount($order->order_id, $isNdsExclude);
                ?>
                <tr>
                    <td class="bor" style="width: 5%; text-align: center"><?= $key + 1; ?></td>
                    <td class="bor" style="width: 45%;"><?= $order->order->product_title_chunked; ?></td>
                    <td class="bor" style="width: 10%; text-align: right">
                        <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                    </td>
                    <td class="bor" style="width: 10%; text-align: right"><?= $unitName ?></td>
                    <td class="bor" style="width: 15%; text-align: right; white-space: nowrap"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
                    <td class="bor" style="width: 15%; text-align: right; white-space: nowrap;"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <table class="mar-t-5">
        <tbody>
            <tr>
                <td style="width: 85%; text-align: right;">
                    <b>Итого:</b>
                </td>
                <td style="width: 15%; text-align: right;">
                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </td>
            </tr>
            <tr>
                <td style="width: 85%; text-align: right;">
                    <b><?= $ndsName; ?>:</b>
                </td>
                <td style="width: 15%; text-align: right;">
                    <?= $ndsValue; ?>
                </td>
            </tr>
        </tbody>
    </table>

    <div>
        <div class="mar-t-5">
            Всего оказано услуг <?= $model->getOrderActs()->count(); ?>,
            на сумму <?= TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2); ?> руб.
        </div>
        <div>
            <b><?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->getPrintAmountWithNds() / 100)); ?></b>
        </div>
    </div>

    <div class="mar-t-15">
        <?= nl2br(Html::encode($model->comment)) ?>
    </div>
    <br/>

    <div style="height: 200px;<?= $printLinkStyle ?> -webkit-print-color-adjust: exact;">
        <div style="height: 200px;<?= $signatureLinkStyle ?> -webkit-print-color-adjust: exact;">
            <table style="width: 100%; -webkit-print-color-adjust: exact;">
                <tr>
                    <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                        ИСПОЛНИТЕЛЬ
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td class="font-bold" style="width: 49%; font-size: 10pt; font-weight: bold; border: none;">
                        ЗАКАЗЧИК
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            <?= ($company->getIsLikeIP() ? '' : ($company->chief_post_name .' ')) . $invoice->company_name_short; ?>
                        <?php else: ?>
                            <?= ($invoice->contractor->company_type_id == CompanyType::TYPE_IP || $invoice->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON ? '' : ($invoice->contractor_director_post_name .' '))
                            . $invoice->contractor_name_short; ?>
                        <?php endif; ?>
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td style="width: 49%; padding-top: 5px; font-size: 10pt; border: none;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            <?= ($invoice->contractor->company_type_id == CompanyType::TYPE_IP || $invoice->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON ? '' : ($invoice->contractor_director_post_name .' '))
                            . $invoice->contractor_name_short; ?>
                        <?php else: ?>
                            <?= ($company->getIsLikeIP() ? '' : ($company->chief_post_name .' ')) . $invoice->company_name_short; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_OUT): ?>
                            / <?= $model->signed_by_name ? : $invoice->getCompanyChiefFio(true) ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                            /
                        <?php else: ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php endif; ?>
                    </td>
                    <td style="width: 2%; border: none;"></td>
                    <td style="width: 49%; height: 34px; border: none; border-bottom: 1px solid #000; text-align: right; vertical-align: bottom;">
                        <?php if ($model->type == Documents::IO_TYPE_IN): ?>
                            / <?= $model->signed_by_name ? : $invoice->getCompanyChiefFio(true) ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                            /
                        <?php else: ?>
                            <?= !empty($director_name) ? '/ ' . $director_name . ' /' : ''; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
