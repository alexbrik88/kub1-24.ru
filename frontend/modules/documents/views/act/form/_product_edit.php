<?php
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;
use yii\helpers\Html;

/**
 * @var $order \common\models\document\OrderAct
 */
?>

<?php if (isset($order)): ?>
    <?php
    $invoice = $order->act->invoice;
    $isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
    $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
    $orderAmount = $isNdsExclude ? $order->amountNoNds : $order->amountWithNds;
    ?>
    <tr role="row" class="odd order">
        <td class="text-center">
            <span class="icon-close tooltip2" data-tooltip-content="#tooltip_<?= $order->product->getType() ? 'goods' : 'service' ?>_no_delete"></span>
        </td>
        <td><?= $order->order->product_title ?></td>
        <td>
            <?= Html::input('number', "OrderAct[{$order->order_id}][quantity]", $order->quantity, [
                'min' => 1,
                'class' => 'input-editable-field quantity form-control',
                'style' => 'padding: 6px 6px;',
                'max' => $order->order->quantity,
            ]) ?>
        </td>
        <td><?= $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
    </tr>

<?php endif ?>
