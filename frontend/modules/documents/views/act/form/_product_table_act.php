<?php

/**
 * @var $order \common\models\document\OrderAct
 * @var \common\models\document\Act $model
 */
?>
<?php \yii\widgets\Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-invoice',
        'class' => 'portlet',
        'data-url' => \yii\helpers\Url::to(['contractor-product']),
    ],
]); ?>
    <table id="product-table-invoice" class="table table-striped table-bordered table-hover out-act_table">
        <thead>
        <tr class="heading" role="row">
            <th width="5%"></th>
            <th width="25%">Наименование</th>
            <th width="10%">Количество</th>
            <th width="5%">Ед</th>
            <th width="15%">Цена</th>
            <th width="15%">Сумма</th>
        </tr>
        </thead>
        <tbody id="tbody">
        <?php foreach ($model->orderActs as $key => $order): ?>
            <?= $this->render('_product_edit', [
                'key' => $key,
                'order' => $order,
                'precision' => $precision,
            ]);
            ?>
        <?php endforeach; ?>
        </tbody>
    </table>
