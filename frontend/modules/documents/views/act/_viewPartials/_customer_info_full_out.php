<?php

use common\components\date\DateHelper;
use common\components\ImageHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;
use \common\widgets\Modal;
use common\models\company\CompanyType;

/* @var \common\models\document\Act $model */
/* @var $message Message */
/* @var $this yii\web\View */
/* @var $model common\models\document\Act */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

$invoice = $model->invoice;
$company = $model->company;
$contractor = $invoice->contractor;

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-document out-act';

if ($model->signed_by_name) {
    $signatureLink = (!$model->employeeSignature) ? false :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = EasyThumbnailImage::thumbnailSrc($model->getImage('chiefSignatureImage'), 145, 50, EasyThumbnailImage::THUMBNAIL_INSET);
}

$printLink = EasyThumbnailImage::thumbnailSrc($model->getImage('printImage'), 150, 150, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

/* add logo */
$images['logo'] = ['tab' => 0, 'link' => $logoLink, 'name' => '+ Добавить логотип'];
$images['print'] = ['tab' => 1, 'link' => $printLink, 'name' => '+ Добавить печать'];
$images['chief'] = ['tab' => 2, 'link' => $signatureLink, 'name' => '+ Добавить подпись'];
?>

<!-- Modal: add logo -->
<?php if ($images) {
    $this->registerJs('
        $(document).on("click", ".companyImages-button", function(event) {
            var tabId = parseInt($(this).data("tab"));
            $("#modal-companyImages a:eq(" + tabId + ")").tab("show");
        });

        $(document).on("hidden.bs.modal", "#modal-companyImages", function() {
            location.reload();
        });
    ');
}
?>
<?php if ($images) {
    Modal::begin([
        'id' => 'modal-companyImages',
        'header' => '<h1>Загрузить логотип, печать и подпись</h1>',
        'toggleButton' => false,
        'options' => [
            'data' => [
                'backdrop' => 'static',
                'keyboard' => 'false',
            ],
        ],
    ]);

    echo $this->render('//company/form/_partial_files_tabs', [
        'model' => $model->company,
    ]);

    Modal::end();
} ?>
<!-- Modal: add logo. end -->

<div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
     style="min-width: 520px; max-width: 735px; border: 1px solid rgb(66, 118, 164); margin-top: 3px; min-height: 430px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 30px;">
            <div class="col-xs-6 pad0">
                <div class="col-xs-12 pad0 actions mart-m-8">
                    <?= \frontend\modules\documents\widgets\DocumentLogWidget::widget([
                        'model' => $model,
                    ]); ?>

                    <?php if ($canUpdate) : ?>
                        <a href="<?= \yii\helpers\Url::to(['update', 'type' => $ioType, 'id' => $model->id]); ?>"
                           title="Редактировать" class="btn darkblue btn-sm" style="height: 27px;margin-left: 3px">
                            <i class="icon-pencil"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?= $this->render('template', [
            'model' => $model,
            'addStamp' => true,
        ]); ?>
    </div>
</div>
<div style="margin-bottom:100px"></div>
