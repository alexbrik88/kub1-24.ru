<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;

/** @var \common\models\document\Act $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
?>

<div class="row action-buttons form-action-buttons hide">
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'form-action-button btn darkblue btn-save widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
            'form' => 'edit-act',
            'data-style' => 'expand-right',
        ]); ?>

        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
            'class' => 'form-action-button btn darkblue btn-save widthe-100 hidden-lg',
            'title' => 'Сохранить',
            'form' => 'edit-act',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <div class="invoice-block">
            <?php
            $cancelUrl = Url::previous('lastPage');
            $permission = $model->isNewRecord ?
                frontend\rbac\permissions\document\Document::INDEX :
                frontend\rbac\permissions\document\Document::VIEW;

            if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                echo Html::a('Отменить', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]);
                echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]);
            } ?>
        </div>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>

<div class="row action-buttons buttons-fixed view-action-buttons" id="buttons-fixed">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            'target' => '_blank',
        ]);
        echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
            'class' => 'btn darkblue widthe-100 hidden-lg',
            'target' => '_blank',
            'title' => 'Печать',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <div class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type,],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 0]) ?>"
           class="is-original-button btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Скан</a>
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 0]) ?>"
           class="is-original-button btn darkblue widthe-100 hidden-lg" title="Скан"><span
                    class="icon fa fa-copy"></span></a>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 1]) ?>"
           class="is-original-button btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Оригинал</a>
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 1]) ?>"
           class="is-original-button btn darkblue widthe-100 hidden-lg min-btn-original" title="Оригинал"><span
                    class="icon ml-10 icon-original-document "></span></a>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                    href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" href="#delete-confirm"
                    title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>