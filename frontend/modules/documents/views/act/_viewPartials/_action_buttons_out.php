<?php

use common\models\document\status;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\document\Act $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

?>
<style>

    .dropdown-menu-mini {
        width: 100%;
        min-width: 98px;
        border-color: #4276a4 !important;
    }

    .dropdown-menu-mini a {
        padding: 7px 0px;
        text-align: center;
    }

</style>
<div class="row action-buttons form-action-buttons hide">
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
            'form' => 'edit-act',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
            'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-lg',
            'title' => 'Сохранить',
            'form' => 'edit-act',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="spinner-button col-sm-1 col-xs-1">
        <div class="invoice-block">
            <?php
            $cancelUrl = Url::previous('lastPage');
            $permission = $model->isNewRecord ?
                frontend\rbac\permissions\document\Document::INDEX :
                frontend\rbac\permissions\document\Document::VIEW;

            if (Yii::$app->user->can($permission, ['model' => $model, 'ioType' => $ioType,])) {
                echo Html::a('Отменить', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                ]);
                echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Отменить',
                ]);
            } ?>
        </div>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>

<div class="row action-buttons view-action-buttons">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
        <?php if (true || $canUpdate): ?>
            <button class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs send-message-panel-trigger">
                Отправить
            </button>
            <button class="btn darkblue widthe-100 hidden-lg send-message-panel-trigger" title="Отправить">
                <span class="ico-Send-smart-pls fs"></span>
            </button>
        <?php endif; ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (true || $canUpdate) {
            if ($model->status_out_id == status\ActStatus::STATUS_CREATED) {
                echo Html::a('Печать',
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                        'target' => '_blank',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\ActStatus::STATUS_PRINTED,
                            ],
                        ],
                    ]);
                echo Html::a('<i class="fa fa-print fa-2x"></i>',
                    ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                        'class' => 'btn darkblue widthe-100 hidden-lg',
                        'target' => '_blank',
                        'data' => [
                            'toggle' => 'modal',
                            'method' => 'post',
                            'title' => 'Печать',
                            'params' => [
                                '_csrf' => Yii::$app->request->csrfToken,
                                'status' => status\ActStatus::STATUS_PRINTED,
                            ],
                        ],
                    ]);
            } else {
                echo Html::a('Печать', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'target' => '_blank',
                ]);
                echo Html::a('<i class="fa fa-print fa-2x"></i>', ['document-print', 'actionType' => 'print', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPrintTitle(),], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'target' => '_blank',
                    'title' => 'Печать',
                ]);
            }
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <div class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini'
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type,],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate) {
            echo Html::a(status\ActStatus::findOne(status\ActStatus::STATUS_SEND)->name,
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\ActStatus::STATUS_SEND,
                        ],
                    ],
                ]);
            echo Html::a('<i class="fa fa-paper-plane-o fa-2x"></i>',
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Передан',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\ActStatus::STATUS_SEND,
                        ],
                    ],
                ]);
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if ($canUpdate) {
            echo Html::a(status\ActStatus::findOne(status\ActStatus::STATUS_RECEIVED)->name,
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\ActStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]);
            echo Html::a('<i class="fa fa-pencil-square-o fa-2x"></i>',
                ['update-status', 'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,], [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'title' => 'Подписан',
                    'data' => [
                        'toggle' => 'modal',
                        'method' => 'post',
                        'params' => [
                            '_csrf' => Yii::$app->request->csrfToken,
                            'status' => status\ActStatus::STATUS_RECEIVED,
                        ],
                    ],
                ]);
        } ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal"
                    href="#delete-confirm">Удалить
            </button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" href="#delete-confirm"
                    title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>
