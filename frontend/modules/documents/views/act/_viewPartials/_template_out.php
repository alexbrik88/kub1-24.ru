<?php
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\Html;

/**
 * @var $order \common\models\document\OrderAct
 */

?>

<?php if (isset($order)): ?>
    <?php
    $order_id = $order->order_id;
    $invoice = $order->act->invoice;
    $hasNds = $invoice->hasNds;
    $isNdsExclude = $invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT;
    $orderPrice = $isNdsExclude ? $order->priceNoNds : $order->priceWithNds;
    $orderAmount = $model->getPrintOrderAmount($order->order_id, $isNdsExclude);
    ?>
    <tr role="row" class="odd order">
        <?php $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE; ?>
        <td class="text-center">
            <span class="editable-field"><?= $key + 1; ?></span>
            <span class="remove-product-from-invoice icon-close input-editable-field delete-row hide"></span>
        </td>
        <td><?= $order->order->product_title ?></td>
        <td>
            <?= Html::hiddenInput("OrderAct[{$order_id}][order_id]", $order_id, [
                'class' => 'quantity',
                'data-value' => 1,
            ]) ?>
            <?php if ($unitName == Product::DEFAULT_VALUE) : ?>
                <?= Html::hiddenInput("OrderAct[{$order_id}][quantity]", 1, [
                    'class' => 'quantity',
                    'data-value' => 1,
                ]) ?>
                <span><?= Product::DEFAULT_VALUE ?></span>
            <?php else : ?>
                <span class="editable-field"><?= $order->quantity; ?></span>
                <?= Html::input('number', "OrderAct[{$order_id}][quantity]", $order->quantity, [
                    'min' => 1,
                    'class' => 'input-editable-field quantity hide form-control',
                    'style' => 'padding: 6px 6px;',
                ]) ?>
            <?php endif ?>
        </td>
        <td><?= $unitName ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($orderPrice, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($orderAmount, $precision); ?></td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td class="text-center">
            <span class="editable-field"><?= $key + 1; ?></span>
            <span class="icon-close input-editable-field delete-row hide"></span>
        </td>
        <td><?= Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
    </tr>
<?php endif; ?>